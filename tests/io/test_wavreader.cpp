/*!
 ** \file test_wavreader.cpp
 ** \brief %WavReader unit tests
 ** \date 2013/05/05
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#include "WavReader_qc.hpp"
#include "utils_compilation.hpp"
/*!
 * \brief %WavReader read method testing with only one container
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_audio_file name of the input audio wav file.
 */
template<typename test_type>
int wavgreader_read_single_test(bool verbose, std::ostream & outstream,
				const std::string & UNUSED(parent_test_name), std::string in_audio_file){
	if (!def_wavreader_read_property1<test_type>(in_audio_file,verbose,outstream))
	{
		if (verbose){
			outstream << "-----Single audio wave file read failed\n";
		}
		return 1;
	}
	return 0;
}

/*!
 * \brief %WavReader read method testing with many containers
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_audio_file name of the input audio wav file.
 */
template<typename test_type>
int wavreader_read_multi_test(bool verbose, std::ostream & outstream,
		const std::string & parent_test_name, std::string in_audio_file){

	if (!def_wavreader_read_property2<test_type>(in_audio_file,verbose,outstream))
	{
		if (verbose){
			outstream << "-----Multiple audio wave file read failed\n";
		}
		return 1;
	}
	return 0;
}

/*!
 * \brief %WavReader read method maxone and minone values checks
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param maxone_wav_file name of the input audio wav file with all values set to max.
 * \param minone_wav_file name of the input audio wav file with all values set to min.
 */
template<typename test_type>
int wavreader_value_check_test(bool verbose, std::ostream & outstream,
			       const std::string & UNUSED(parent_test_name), std::string maxone_wav_file, std::string minone_wav_file){

	slip::WavReader<slip::Vector<test_type>,test_type,1> audioread(maxone_wav_file);
	slip::Vector<test_type> data;
	audioread.read(data);
	if(verbose){
		outstream << "MaxOne file header : " << audioread.get_header();
		outstream << std::endl;
	}
	typename slip::Vector<test_type>::iterator it = std::find_if(data.begin(), data.end(),
			std::bind2nd(std::not_equal_to<test_type>(),
					static_cast<test_type>(std::numeric_limits<int16>::max())));
	if (it != data.end()){
		if (verbose){
			outstream << "-----Audio wave reader value check failed (maxone)" << std::endl;
			outstream << "--> Value that failed : " << *it << " | should be : "
					<< static_cast<test_type>(std::numeric_limits<int16>::max());
			outstream << std::endl;
		}
		return 1;
	}
	audioread.release();
	audioread.set_data_filename(minone_wav_file);
	audioread.initialize();
	audioread.read(data);
	if(verbose){
		outstream << "MinOne file header : " << audioread.get_header();
		outstream << std::endl;
	}
	it = std::find_if(data.begin(), data.end(), std::bind2nd(std::not_equal_to<test_type>(),
			static_cast<test_type>(std::numeric_limits<int16>::min())));
	if (it != data.end()){
		if (verbose){
			outstream << "-----Audio wave reader value check failed (minone)" << std::endl;
			outstream << "--> Value that failed : " << *it << " | should be : "
					<< static_cast<test_type>(std::numeric_limits<int16>::min());
			outstream << std::endl;
		}
		return 1;
	}
	return 0;

}

/*!
 ** \brief usage : cmd.exe args
 **  ./test_wavreader my_arg
 **  \note MaxOne_WavFile and MinOne_WaveFile should be 16bits wav files with all values
 **  set respectively to the maximum and to the minimum.
 */
void usage(const char *name) {
	std::cout << "usage :" << std::endl << std::endl;
	std::cout << "\t " << name << " verbose(1/0) MaxOne_WavFile MinOne_WaveFile Some_WaveFile"
			<< std::endl;
	std::cout << "MaxOne_WavFile and MinOne_WaveFile should be 16bits wav files with all values"
			<< "set respectively to the maximum and to the minimum." << std::endl << std::endl;
}

int main(int argc, char *argv[])
{
	if (argc < 5) {
		usage(*argv);
		return 1;
	}
	std::string verb_str(argv[1]);
	bool verbose = false;
	if(verb_str == "1")
		verbose = true;
	std::string maxone_wav_file(argv[2]);
	std::string minone_wav_file(argv[3]);
	std::string wav_file(argv[4]);
	std::string test_name(*argv);
	std::ostream & outstream = std::cout;

//	//------------------------------
//	//single read function
//	//------------------------------
//	if(verbose){
//		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
//		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
//		outstream << test_name << " - single read function" << std::endl;
//		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
//	}
//	//single read function
//	{
//		if (wavgreader_read_single_test<double>(verbose,outstream,test_name,wav_file))
//			return 1;
//		if (wavgreader_read_single_test<float>(verbose,outstream,test_name,wav_file))
//			return 1;
//		if (wavgreader_read_single_test<int>(verbose,outstream,test_name,wav_file))
//			return 1;
//	}
//
//	//------------------------------
//	//multi read function
//	//------------------------------
//	if(verbose){
//		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
//		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
//		outstream << test_name << " - multi read function" << std::endl;
//		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
//	}
//	//single read function
//	{
//		if (wavreader_read_multi_test<double>(verbose,outstream,test_name,wav_file))
//			return 1;
//		if (wavreader_read_multi_test<float>(verbose,outstream,test_name,wav_file))
//			return 1;
//		if (wavreader_read_multi_test<int>(verbose,outstream,test_name,wav_file))
//			return 1;
//	}

	//--------------------------------------------------------
	//Some specific tests with maxone and minone files
	//-------------------------------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - Specific tests  with maxone and minone files" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//maxone and minone values checks
	{
		if (wavreader_value_check_test<double>(verbose,outstream,test_name,maxone_wav_file,minone_wav_file))
			return 1;
		if (wavreader_value_check_test<float>(verbose,outstream,test_name,maxone_wav_file,minone_wav_file))
			return 1;
		if (wavreader_value_check_test<int>(verbose,outstream,test_name,maxone_wav_file,minone_wav_file))
			return 1;
	}



	return 0;
}



