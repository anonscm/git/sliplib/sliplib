/*!
 ** \file test_netcdfwriter.cpp
 ** \brief %NetCDFWriter unit tests
 ** \date 2013/05/14
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \todo testing writer with 3D signals.
 */
#include <iostream>
#include <iomanip>

#include "NetCDFReader.hpp"
#include "NetCDFWriter.hpp"
#include "JpegReader.hpp"
#include "AvReader.hpp"
#include "ColorImage.hpp"
#include "ColorVolume.hpp"

/*!
 * \brief %NetCDFWriter write method testing with 1D containers and 1 Component
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_mono_signal name of the input mono signal netcdf file.
 * \param in_mono_var name of the input mono signal netcdf variable.
 * \param mono_outputfile name of the output file
 */
template<typename test_type>
int netcdfwriter1D1C_write_test(bool verbose, std::ostream & outstream,
		const std::string & parent_test_name, std::string in_mono_signal,
		std::string in_mono_var, std::string mono_outputfile){

	std::vector<std::string> var_names(1,in_mono_var);

	slip::NetCDFReader<slip::Vector<test_type>,test_type,1,1,1> reader1D1C;
	reader1D1C.set_data_filename(in_mono_signal);
	reader1D1C.initialize(var_names);
	slip::Vector<test_type> sig1D;
	reader1D1C.read(sig1D);
	reader1D1C.release();

	std::vector<std::size_t> output_signal_dim(1,sig1D.size());

	slip::NetCDFWriter<slip::Vector<test_type>,test_type,1,1> writer1D1C;
	writer1D1C.set_output_filename(mono_outputfile);
	writer1D1C.initialize(output_signal_dim,std::vector<std::string>());
	writer1D1C.write(sig1D);
	writer1D1C.release();

	reader1D1C.set_data_filename(mono_outputfile);
	reader1D1C.initialize(std::vector<std::string>());
	slip::Vector<test_type> sig1D2;
	reader1D1C.read(sig1D2);
	reader1D1C.release();

	if (sig1D2 != sig1D){
		if (verbose){
			outstream << "-----netcdfwriter1D1C_write_test failed : 1 container\n";
		}
		return 1;
	}

	std::size_t s1 = sig1D.size() / 2;
	std::size_t s2 = sig1D.size() - s1;
	slip::Vector<test_type> sig1D_1(s1);
	slip::Vector<test_type> sig1D_2(s2);
	std::copy(sig1D.begin(),sig1D.begin() + s1,sig1D_1.begin());
	std::copy(sig1D.begin() + s1,sig1D.end(),sig1D_2.begin());
	slip::NetCDFWriter<slip::Vector<test_type>,test_type,1,1> writer1D1C_2(mono_outputfile,output_signal_dim,
			std::vector<std::string>());
	writer1D1C_2.write(sig1D_1);
	writer1D1C_2.write(sig1D_2);
	writer1D1C_2.release();

	reader1D1C.initialize(std::vector<std::string>());
	reader1D1C.read(sig1D2);
	reader1D1C.release();


	if (sig1D != sig1D2){
		if (verbose){
			outstream << "-----netcdfwriter1D1C_write_test failed : 2 containers\n";
			outstream << "sig1D.size() = " << sig1D.size();
			outstream << std::endl;
			outstream << "sig1D2.size() = " << sig1D2.size();
			outstream << std::endl;
			//			outstream << "->sig1D " << sig1D_1 << std::endl;
			//			outstream << "->sig1D_1 " << sig1D_1 << std::endl;
			//			outstream << "->sig1D_2 " << sig1D_2 << std::endl;
			//outstream << "->sig1D2 = " << sig1D2 << std::endl;
		}
		return 1;
	}
	return 0;
}

/*!
 * \brief %NetCDFWriter write method testing in 2D containers with 3 Components
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_multi_signal name of the input multi component (3) signal netcdf file.
 * \param in_multi_var1 name of the first variable (component) of the signal.
 * \param in_multi_var1 name of the second variable (component) of the signal.
 * \param in_multi_var1 name of the thirdt variable (component) of the signal.
 * \param multi_outputfile output file name
 * \param size of the signal
 */
template<typename test_type>
int netcdfwriter2D3C_write_test(bool verbose, std::ostream & outstream,
		const std::string & parent_test_name, std::string in_multi_signal,
		std::string in_multi_var1,std::string in_multi_var2,std::string in_multi_var3,
		std::string multi_outputfile){


	std::vector<std::string> var_names(3);
	var_names[0] = in_multi_var1;
	var_names[1] = in_multi_var2;
	var_names[2] = in_multi_var3;

	slip::NetCDFReader<slip::ColorImage<test_type>,test_type,3,1,2> reader2D3C;
	reader2D3C.set_data_filename(in_multi_signal);
	reader2D3C.initialize(var_names);
	slip::ColorImage<test_type> sig2D;
	reader2D3C.read(sig2D);
	reader2D3C.release();

	std::vector<std::size_t> output_signal_dim(2);
	output_signal_dim[0] = sig2D.dim1();
	output_signal_dim[1] = sig2D.dim2();

	slip::NetCDFWriter<slip::ColorImage<test_type>,test_type,3,2> writer2D3C;
	writer2D3C.set_output_filename(multi_outputfile);
	writer2D3C.initialize(output_signal_dim,std::vector<std::string>());
	writer2D3C.write(sig2D);
	writer2D3C.release();

	reader2D3C.set_data_filename(multi_outputfile);
	reader2D3C.initialize(std::vector<std::string>());
	slip::ColorImage<test_type> sig2D2;
	reader2D3C.read(sig2D2);
	reader2D3C.release();

	if (sig2D != sig2D2){
		if (verbose){
			outstream << "-----netcdfwriter2D3C_write_test failed : 1 container\n";
		}
		return 1;
	}

	std::size_t s1 = sig2D.dim1() / 2;
	std::size_t s2 = sig2D.dim1() - s1;
	slip::Box2d<int> box1(0,0,s1-1,sig2D.dim2()-1);
	slip::Box2d<int> box2(s1,0,sig2D.dim1()-1,sig2D.dim2()-1);
	slip::ColorImage<test_type> sig2D_1(s1,sig2D.dim2(),sig2D.upper_left(box1),sig2D.bottom_right(box1));
	slip::ColorImage<test_type> sig2D_2(s2,sig2D.dim2(),sig2D.upper_left(box2),sig2D.bottom_right(box2));
	slip::NetCDFWriter<slip::ColorImage<test_type>,test_type,3,2> writer2D3C_2(multi_outputfile,
			output_signal_dim,std::vector<std::string>());
	writer2D3C_2.write(sig2D_1);
	writer2D3C_2.write(sig2D_2);
	writer2D3C_2.release();

	reader2D3C.initialize(std::vector<std::string>());
	reader2D3C.read(sig2D2);
	reader2D3C.release();

	if (sig2D != sig2D2){
		if (verbose){
			outstream << "-----netcdfwriter2D3C_write_test failed : 2 containers" << std::endl;
			outstream << "sig2D.dim1() = " << sig2D.dim1();
			outstream << std::endl;
			outstream << "sig2D.dim2() = " << sig2D.dim2();
			outstream << std::endl;
			outstream << "sig2D2.dim1() = " << sig2D2.dim1();
			outstream << std::endl;
			outstream << "sig2D2.dim2() = " << sig2D2.dim2();
			outstream << std::endl;
		}
		return 1;
	}

	return 0;
}

/*!
 * \brief %NetCDFWriter write method testing on a color jpg image
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_jpg_image name of the input jpg file file.
 * \param multi_outputfile output file name
 * \param size of the signal
 */
template<typename test_type>
int netcdfwriter_colorimage_write_test(bool verbose, std::ostream & outstream,
		const std::string & parent_test_name, std::string in_jpg_image,
		std::string multi_outputfile){

	std::vector<std::string> var_names(3);
	var_names[0] = "red";
	var_names[1] = "green";
	var_names[2] = "blue";

	slip::JpegReader<slip::ColorImage<test_type>,test_type,3,1> jpgreader(in_jpg_image);
	slip::ColorImage<test_type> sig2D;
	jpgreader.read(sig2D);
	jpgreader.release();

	std::vector<std::size_t> output_signal_dim(2);
	output_signal_dim[0] = sig2D.dim1();
	output_signal_dim[1] = sig2D.dim2();

	slip::NetCDFWriter<slip::ColorImage<test_type>,test_type,3,2> writer2D3C;
	writer2D3C.set_output_filename(multi_outputfile);
	writer2D3C.initialize(output_signal_dim,std::vector<std::string>());
	writer2D3C.write(sig2D);
	writer2D3C.release();

	slip::NetCDFReader<slip::ColorImage<test_type>,test_type,3,1,2> reader2D3C;
	reader2D3C.set_data_filename(multi_outputfile);
	reader2D3C.initialize(std::vector<std::string>());
	slip::ColorImage<test_type> sig2D2;
	reader2D3C.read(sig2D2);
	reader2D3C.release();

	if (sig2D != sig2D2){
		if (verbose){
			outstream << "-----netcdfwriter_colorimage_write_test failed : 1 container\n";
		}
		return 1;
	}

	std::size_t s1 = sig2D.dim1() / 2;
	std::size_t s2 = sig2D.dim1() - s1;
	slip::Box2d<int> box1(0,0,s1-1,sig2D.dim2()-1);
	slip::Box2d<int> box2(s1,0,sig2D.dim1()-1,sig2D.dim2()-1);
	slip::ColorImage<test_type> sig2D_1(s1,sig2D.dim2(),sig2D.upper_left(box1),sig2D.bottom_right(box1));
	slip::ColorImage<test_type> sig2D_2(s2,sig2D.dim2(),sig2D.upper_left(box2),sig2D.bottom_right(box2));
	slip::NetCDFWriter<slip::ColorImage<test_type>,test_type,3,2> writer2D3C_2(multi_outputfile,
			output_signal_dim,std::vector<std::string>());
	writer2D3C_2.write(sig2D_1);
	writer2D3C_2.write(sig2D_2);
	writer2D3C_2.release();

	reader2D3C.initialize(std::vector<std::string>());
	reader2D3C.read(sig2D2);
	reader2D3C.release();

	if (sig2D != sig2D2){
		if (verbose){
			outstream << "-----netcdfwriter_colorimage_write_test failed : 2 containers" << std::endl;
			outstream << "sig2D.dim1() = " << sig2D.dim1();
			outstream << std::endl;
			outstream << "sig2D.dim2() = " << sig2D.dim2();
			outstream << std::endl;
			outstream << "sig2D2.dim1() = " << sig2D2.dim1();
			outstream << std::endl;
			outstream << "sig2D2.dim2() = " << sig2D2.dim2();
			outstream << std::endl;
		}
		return 1;
	}

	return 0;
}


/*!
 * \brief %NetCDFWriter write method testing on a color movie
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_video name of the input video file.
 * \param multi_outputfile output file name
 * \param size of the signal
 */
template<typename test_type>
int netcdfwriter_colormovie_write_test(bool verbose, std::ostream & outstream,
		const std::string & parent_test_name, std::string in_movie,
		std::string multi_outputfile){

	std::vector<std::string> var_names(3);
	var_names[0] = "red";
	var_names[1] = "green";
	var_names[2] = "blue";

	slip::AvReader<slip::ColorVolume<test_type>,test_type,3,1> avreader(in_movie);
	slip::ColorVolume<test_type> sig3D;
	avreader.read(sig3D);
	avreader.release();

	std::vector<std::size_t> output_signal_dim(3);
	output_signal_dim[0] = sig3D.dim1();
	output_signal_dim[1] = sig3D.dim2();
	output_signal_dim[2] = sig3D.dim3();

	slip::NetCDFWriter<slip::ColorVolume<test_type>,test_type,3,3> writer3D3C;
	writer3D3C.set_output_filename(multi_outputfile);
	writer3D3C.initialize(output_signal_dim,std::vector<std::string>());
	writer3D3C.write(sig3D);
	writer3D3C.release();

	slip::NetCDFReader<slip::ColorVolume<test_type>,test_type,3,1,3> reader3D3C;
	reader3D3C.set_data_filename(multi_outputfile);
	reader3D3C.initialize(std::vector<std::string>());
	slip::ColorVolume<test_type> sig3D2;
	reader3D3C.read(sig3D2);
	reader3D3C.release();

	if (sig3D != sig3D2){
		if (verbose){
			outstream << "-----netcdfwriter_colormovie_write_test failed : 1 container\n";
		}
		return 1;
	}

	std::size_t s1 = sig3D.dim1() / 2;
	std::size_t s2 = sig3D.dim1() - s1;
	slip::Box3d<int> box1(0,0,0,s1-1,sig3D.dim2()-1,sig3D.dim3()-1);
	slip::Box3d<int> box2(s1,0,0,sig3D.dim1()-1,sig3D.dim2()-1,sig3D.dim3()-1);
	slip::ColorVolume<test_type> sig3D_1(s1,sig3D.dim2(),sig3D.dim3(),sig3D.front_upper_left(box1),sig3D.back_bottom_right(box1));
	slip::ColorVolume<test_type> sig3D_2(s2,sig3D.dim2(),sig3D.dim3(),sig3D.front_upper_left(box2),sig3D.back_bottom_right(box2));
	slip::NetCDFWriter<slip::ColorVolume<test_type>,test_type,3,3> writer3D3C_2(multi_outputfile,
			output_signal_dim,std::vector<std::string>());
	writer3D3C_2.write(sig3D_1);
	writer3D3C_2.write(sig3D_2);
	writer3D3C_2.release();

	reader3D3C.initialize(std::vector<std::string>());
	reader3D3C.read(sig3D2);
	reader3D3C.release();

	if (sig3D != sig3D2){
		if (verbose){
			outstream << "-----netcdfwriter_colormovie_write_test failed : 2 containers" << std::endl;
			outstream << "sig2D.dim1() = " << sig3D.dim1();
			outstream << std::endl;
			outstream << "sig2D.dim2() = " << sig3D.dim2();
			outstream << std::endl;
			outstream << "sig2D.dim3() = " << sig3D.dim3();
			outstream << std::endl;
			outstream << "sig2D2.dim1() = " << sig3D2.dim1();
			outstream << std::endl;
			outstream << "sig2D2.dim2() = " << sig3D2.dim2();
			outstream << std::endl;
			outstream << "sig2D2.dim3() = " << sig3D2.dim3();
			outstream << std::endl;
		}
		return 1;
	}

	return 0;
}

/*!
 ** \brief usage :
 **  cmd.exe args
 **  ex : ./test_netcdfwriter my_arg
 */
void usage(const char *name) {
	std::cout << "usage :\n\n";
	std::cout << "\t " << name << " verbose(1/0) Input_MonoSignal_NetCDFFile"
			<< " Input_MonoSignal_VarName Input_MultiSignal_NetCDFFile"
			<< " Input_MultiSignal_VarName1 Input_MultiSignal_VarName2 Input_MultiSignal_VarName3"
			<< " Input_ColorJpgImage Input_ColorMovie"
			<< " Output_MonoSignal_NetCDFFile Output_MultiSignal_NetCDFFile\n\n";
}

int main(int argc, char *argv[])
{
	if (argc < 12) {
		usage(*argv);
		return 1;
	}
	std::string verb_str(argv[1]);
	bool verbose = false;
	if(verb_str == "1")
		verbose = true;
	std::string monosignal_file(argv[2]);
	std::string monosignal_var(argv[3]);
	std::string multisignal_file(argv[4]);
	std::string multisignal_var1(argv[5]);
	std::string multisignal_var2(argv[6]);
	std::string multisignal_var3(argv[7]);
	std::string in_jpg_image(argv[8]);
	std::string in_movie(argv[9]);
	std::string mono_outputfile(argv[10]);
	std::string multi_outputfile(argv[11]);
	std::string test_name(*argv);
	std::ostream & outstream = std::cout;

	//------------------------------
	//1D write function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - 1D write function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//1D write function
	{
		if (netcdfwriter1D1C_write_test<float>(verbose,outstream,
				test_name,monosignal_file,monosignal_var,mono_outputfile))
			return 1;
		if (netcdfwriter1D1C_write_test<int>(verbose,outstream,
				test_name,monosignal_file,monosignal_var,mono_outputfile))
			return 1;
		if (netcdfwriter1D1C_write_test<double>(verbose,outstream,
				test_name,monosignal_file,monosignal_var,mono_outputfile))
			return 1;
	}

	//------------------------------
	//2D write function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - 2D write function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//2D write function
	{
		if (netcdfwriter2D3C_write_test<float>(verbose,outstream,
				test_name,multisignal_file,multisignal_var1,multisignal_var2,
				multisignal_var3,multi_outputfile))
			return 1;
		if (netcdfwriter2D3C_write_test<int>(verbose,outstream,
				test_name,multisignal_file,multisignal_var1,multisignal_var2,
				multisignal_var3,multi_outputfile))
			return 1;
		if (netcdfwriter2D3C_write_test<double>(verbose,outstream,
				test_name,multisignal_file,multisignal_var1,multisignal_var2,
				multisignal_var3,multi_outputfile))
			return 1;
		if (netcdfwriter_colorimage_write_test<int>(verbose,outstream,test_name,
				in_jpg_image,multi_outputfile))
			return 1;
	}

	//------------------------------
	//3D write function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - 3D write function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//3D write function
	{
		if (netcdfwriter_colormovie_write_test<int>(verbose,outstream,test_name,
				in_movie,multi_outputfile))
			return 1;
	}
	return 0;
}
