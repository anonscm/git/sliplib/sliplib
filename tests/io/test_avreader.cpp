/*!
 ** \file test_avreader.cpp
 ** \brief %AvReader unit tests
 ** \date 2013/04/26rrivault_AT_inria.fr>
 */

#include "AvReader_qc.hpp"

/*!
 * \brief %AvReader read method testing with only one container
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_col_video_file name of the input color av file.
 * \param col_cols dim3 of the color video
 * \param col_rows dim2 of the color video
 * \param col_slices dim1 = nb of frames of the color video
 * \param in_gray_video_file name of the input grayscale av file.
 * \param gray_cols dim3 of the grayscale video
 * \param gray_rows dim2 of the grayscale video
 * \param gray_slices dim1 = nb of frames of the grayscale video
 */
template<typename test_type>
int avreader_read_single_test(bool verbose, std::ostream & outstream,
		const std::string & parent_test_name, std::string in_col_video_file,
		std::size_t col_cols, std::size_t col_rows, std::size_t col_slices, std::string in_gray_video_file,
		std::size_t gray_cols, std::size_t gray_rows, std::size_t gray_slices){
	if (!def_avreader_read_colorvideo_property1<test_type>(in_col_video_file,col_cols,col_rows,col_slices
			, verbose, outstream)){
		if (verbose){
			outstream << "-----ColorVideo failed\n";
		}
		return 1;
	}
	if (!def_avreader_read_grayscalevideo_property1<test_type>(in_gray_video_file,gray_cols,gray_rows,gray_slices
			, verbose, outstream)){
		if (verbose){
			outstream << "-----GrayscaleVideo failed\n";
		}
		return 1;
	}
	return 0;
}

/*!
 * \brief %AvReader read method testing with many containers
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_col_video_file name of the input color av file.
 * \param col_cols dim3 of the color video
 * \param col_rows dim2 of the color video
 * \param col_slices dim1 = nb of frames of the color video
 * \param in_gray_video_file name of the input grayscale av file.
 * \param gray_cols dim3 of the grayscale video
 * \param gray_rows dim2 of the grayscale video
 * \param gray_slices dim1 = nb of frames of the grayscale video
 */
template<typename test_type>
int avreader_read_multi_test(bool verbose, std::ostream & outstream,
		const std::string & parent_test_name, std::string in_col_video_file,
		std::size_t col_cols, std::size_t col_rows, std::size_t col_slices, std::string in_gray_video_file,
		std::size_t gray_cols, std::size_t gray_rows, std::size_t gray_slices){

		if (!def_avreader_read_colorvideo_property2<test_type>(in_col_video_file,col_cols,col_rows,col_slices
				,verbose,outstream)){
			if (verbose){
				outstream << "-----ColorVideo failed\n";
			}
			return 1;
		}
		if (!def_avreader_read_grayscalevideo_property2<test_type>(in_gray_video_file,gray_cols,gray_rows,
				gray_slices,verbose,outstream)){
			if (verbose){
				outstream << "-----GrayscaleVideo failed\n";
			}
			return 1;
		}
	return 0;
}


/*!
 ** \brief usage :
 **  cmd.exe args
 **  ex : ./test_avreader my_arg
 */
void usage(const char *name) {
	std::cout << "usage :\n\n";
	std::cout << "\t " << name << " verbose(1/0) InputColorVideo_AvFile "
			<< "InputColorVideo_Width InputColorVideo_Height InputColorVideo_Slide"
			<< " InputGrayscaleVideo_AvFile"
			<< " InputGrayscaleVideo_Width InputGrayscaleVideo_Height InputGrayscaleVideo_Slide"
			<< "\n\n";
}

int main(int argc, char *argv[])
{
	if (argc < 10) {
		usage(*argv);
		return 1;
	}
	std::string verb_str(argv[1]);
	bool verbose = false;
	if(verb_str == "1")
		verbose = true;
	std::string col_video_file(argv[2]);
	std::size_t col_cols = atoi(argv[3]);
	std::size_t col_rows = atoi(argv[4]);
	std::size_t col_slices = atoi(argv[5]);
	std::string gray_video_file(argv[6]);
	std::size_t gray_cols = atoi(argv[7]);
	std::size_t gray_rows = atoi(argv[8]);
	std::size_t gray_slices = atoi(argv[9]);
	std::string test_name(*argv);
	std::ostream & outstream = std::cout;
	//int nb_qc_tests = 100;
//	outstream << "color video dimensions : " << col_cols << " " << col_rows << " " << col_slices << "\n";
//	outstream << "gray video dimensions : " << gray_cols << " " << gray_rows << " " << gray_slices << "\n";
	//------------------------------
	//single read function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - single read function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//single read function
	{
		if (avreader_read_single_test<double>(verbose,outstream,
				test_name,col_video_file,col_cols,col_rows,col_slices,gray_video_file,
				gray_cols,gray_rows,gray_slices))
			return 1;
		if (avreader_read_single_test<int>(verbose,outstream,
				test_name,col_video_file,col_cols,col_rows,col_slices,gray_video_file,
				gray_cols,gray_rows,gray_slices))
			return 1;
		if (avreader_read_single_test<unsigned char>(verbose,outstream,
				test_name,col_video_file,col_cols,col_rows,col_slices,gray_video_file,
				gray_cols,gray_rows,gray_slices))
			return 1;
	}

		//------------------------------
		//multi read function
		//------------------------------
		if(verbose){
			outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
			outstream <<  std::setfill(' ') << std::setw(20) << ' ';
			outstream << test_name << " - multi read function" << std::endl;
			outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		}
		//single read function
		{
			if (avreader_read_multi_test<double>(verbose,outstream,
					test_name,col_video_file,col_cols,col_rows,col_slices,gray_video_file,
					gray_cols,gray_rows,gray_slices))
				return 1;
			if (avreader_read_multi_test<int>(verbose,outstream,
					test_name,col_video_file,col_cols,col_rows,col_slices,gray_video_file,
					gray_cols,gray_rows,gray_slices))
				return 1;
			if (avreader_read_multi_test<unsigned char>(verbose,outstream,
					test_name,col_video_file,col_cols,col_rows,col_slices,gray_video_file,
					gray_cols,gray_rows,gray_slices))
				return 1;
		}


	return 0;
}
