/*!
 ** \file test_pngreader.cpp
 ** \brief %PngReader unit tests
 ** \date 2013/04/23
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#include "PngReader_qc.hpp"
#include "utils_compilation.hpp"

/*!
 * \brief %PngReader read method testing with only one container
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_col_image_file name of the input color png file.
 * \param col_width of the color image
 * \param col_height of the color image
 * \param in_gray_image_file name of the input grayscale png file.
 * \param gray_width of the grayscale image
 * \param gray_height of the grayscale image
 */
template<typename test_type>
int pngreader_read_single_test(bool verbose, std::ostream & outstream,
			       const std::string & UNUSED(parent_test_name), std::string in_col_image_file,
		std::size_t col_width, std::size_t col_height, std::string in_gray_image_file,
		std::size_t gray_width, std::size_t gray_height){
	if (!def_pngreader_read_colorimage_property1<test_type>(in_col_image_file,col_width,col_height
			, verbose, outstream)){
		if (verbose){
			outstream << "-----ColorImage failed\n";
		}
		return 1;
	}
	if (!def_pngreader_read_grayscaleimage_property1<test_type>(in_gray_image_file,gray_width,gray_height
			, verbose, outstream)){
		if (verbose){
			outstream << "-----GrayscaleImage failed\n";
		}
		return 1;
	}
	return 0;
}

/*!
 * \brief %PngReader read method testing with many containers
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_col_image_file name of the input color png file.
 * \param col_width of the color image
 * \param col_height of the color image
 * \param in_gray_image_file name of the input grayscale png file.
 * \param gray_width of the grayscale image
 * \param gray_height of the grayscale image
 */
template<typename test_type>
int pngreader_read_multi_test(bool verbose, std::ostream & outstream,
			      const std::string & UNUSED(parent_test_name), std::string in_col_image_file,
		std::size_t col_width, std::size_t col_height, std::string in_gray_image_file,
		std::size_t gray_width, std::size_t gray_height){

	if (!def_pngreader_read_colorimage_property2<test_type>(in_col_image_file,col_width,col_height
			, verbose, outstream)){
		if (verbose){
			outstream << "-----ColorImage failed\n";
		}
		return 1;
	}
	if (!def_pngreader_read_grayscaleimage_property2<test_type>(in_gray_image_file,gray_width,gray_height
			, verbose, outstream)){
		if (verbose){
			outstream << "-----GrayscaleImage failed\n";
		}
		return 1;
	}
	return 0;
}


/*!
 ** \brief usage :
 **  cmd.exe args
 **  ex : ./test_pngreader my_arg
 */
void usage(const char *name) {
	std::cout << "usage :\n\n";
	std::cout << "\t " << name << " verbose(1/0) InputColorImage_PngFile "
			<< "InputColorImage_Width InputColorImage_Height "
			<< "InputGrayscaleImage_PngFile "
			<< "InputGrayscaleImage_Width InputGrayscaleImage_Height"
			<< "\n\n";
}

int main(int argc, char *argv[])
{
	if (argc < 8) {
		usage(*argv);
		return 1;
	}
	std::string verb_str(argv[1]);
	bool verbose = false;
	if(verb_str == "1")
		verbose = true;
	std::string col_image_file(argv[2]);
	std::size_t col_width = atoi(argv[3]);
	std::size_t col_height = atoi(argv[4]);
	std::string gray_image_file(argv[5]);
	std::size_t gray_width = atoi(argv[6]);
	std::size_t gray_height = atoi(argv[7]);
	std::string test_name(*argv);
	std::ostream & outstream = std::cout;
	//int nb_qc_tests = 100;

	//------------------------------
	//single read function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - single read function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//single read function
	{
		if (pngreader_read_single_test<double>(verbose,outstream,
				test_name,col_image_file,col_width,col_height,gray_image_file,
				gray_width,gray_height))
			return 1;
		if (pngreader_read_single_test<int>(verbose,outstream,
				test_name,col_image_file,col_width,col_height,gray_image_file,
				gray_width,gray_height))
			return 1;
		if (pngreader_read_single_test<unsigned char>(verbose,outstream,
				test_name,col_image_file,col_width,col_height,gray_image_file,
				gray_width,gray_height))
			return 1;
	}

	//------------------------------
	//multi read function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - multi read function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//single read function
	{
		if (pngreader_read_multi_test<double>(verbose,outstream,
				test_name,col_image_file,col_width,col_height,gray_image_file,
				gray_width,gray_height))
			return 1;
		if (pngreader_read_multi_test<int>(verbose,outstream,
				test_name,col_image_file,col_width,col_height,gray_image_file,
				gray_width,gray_height))
			return 1;
		if (pngreader_read_multi_test<unsigned char>(verbose,outstream,
				test_name,col_image_file,col_width,col_height,gray_image_file,
				gray_width,gray_height))
			return 1;
	}


	return 0;
}
