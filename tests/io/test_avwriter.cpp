/*!
** \file test_avwriter.cpp
** \brief %AvWriter unit tests
** \date 2013/05/02
** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
*/

#include "AvReader_qc.hpp"
#include "AvWriter_qc.hpp"


#define BIT_RATE 400000
#define FRAME_RATE 50 //25 images/s


/*!
 * \brief %AvWriter write method testing with only one container
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param nb_qc_tests number of quickcheck tests to run (if defined)
 * \param in_col_video_file name of the input color av file.
 * \param in_gray_video_file name of the input grayscale av file.
 * \param out_col_video_file name of the output color av file.
 * \param out_gray_video_file name of the output grayscale av file.
 */
template<typename test_type>
int avwriter_write_single_test(bool verbose, std::ostream & outstream,
			       const std::string & parent_test_name, int nb_qc_tests, std::string in_col_video_file,
			       std::string in_gray_video_file, std::string out_col_video_file, std::string out_gray_video_file){
  slip::AvReader<slip::ColorVolume<test_type>,test_type,3,1> coul_avread(in_col_video_file);
  slip::ColorVolume<test_type> coul_video;
  coul_avread.read(coul_video);
  if (!def_avwriter_write_colorvideo_property1<test_type>(out_col_video_file, coul_video
							  , FRAME_RATE, static_cast<int64_t>(BIT_RATE), verbose, outstream)){
    if (verbose){
      outstream << "-----ColorVideo failed\n";
    }
    return 1;
  }

  slip::AvReader<slip::Volume<test_type>,test_type,1,1> gray_avread(in_gray_video_file);
  slip::Volume<test_type> gray_video;
  gray_avread.read(gray_video);
  if (!def_avwriter_write_grayscalevideo_property1<test_type>(out_gray_video_file, gray_video
							      , FRAME_RATE, static_cast<int64_t>(BIT_RATE), verbose, outstream)){
    if (verbose){
      outstream << "-----GrayScale failed\n";
    }
    return 1;
  }

#ifdef HAVE_QCHECK
  quickcheck::AvWriterProperty1<test_type> qc;
  qc.set_col_filename(out_col_video_file);
  qc.set_gray_filename(out_gray_video_file);
  qc.set_verbose(verbose);
  qc.set_outstream(&outstream);
  qc.check(nb_qc_tests,5*nb_qc_tests,verbose,outstream);
#endif

  return 0;
}

/*!
 * \brief %AvWriter write method testing with many containers
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param nb_qc_tests number of quickcheck tests to run (if defined)
 * \param in_col_video_file name of the input color av file.
 * \param in_gray_video_file name of the input grayscale av file.
 * \param out_col_video_file name of the output color av file.
 * \param out_gray_video_file name of the output grayscale av file.
 */
template<typename test_type>
int avwriter_write_many_test(bool verbose, std::ostream & outstream,
			     const std::string & parent_test_name, int nb_qc_tests, std::string in_col_video_file,
			     std::string in_gray_video_file, std::string out_col_video_file, std::string out_gray_video_file){

  slip::AvReader<slip::ColorVolume<test_type>,test_type,3,4> coul_avread(in_col_video_file);
  std::vector<slip::ColorVolume<test_type> > coul_video_list;
  slip::ColorVolume<test_type> coul_video;
  while(coul_avread.read(coul_video)){
    coul_video_list.push_back(coul_video);
  }
  coul_video_list.push_back(coul_video);
  if (!def_avwriter_write_colorvideo_property2<test_type>(out_col_video_file, coul_video_list
							  , FRAME_RATE, static_cast<int64_t>(BIT_RATE), verbose, outstream)){
    if (verbose){
      outstream << "-----ColorVideo failed\n";
    }
    return 1;
  }

  slip::AvReader<slip::Volume<test_type>,test_type,1,4> gray_avread(in_gray_video_file);
  std::vector<slip::Volume<test_type> > gray_video_list;
  slip::Volume<test_type> gray_video;
  while(gray_avread.read(gray_video)){
    gray_video_list.push_back(gray_video);
  }
  gray_video_list.push_back(gray_video);
  if (!def_avwriter_write_grayscalevideo_property2<test_type>(out_gray_video_file, gray_video_list
  							      , FRAME_RATE, static_cast<int64_t>(BIT_RATE), verbose, outstream)){
    if (verbose){
      outstream << "-----GrayScale failed\n";
    }
    return 1;
  }

#ifdef HAVE_QCHECK
  quickcheck::AvWriterProperty2<test_type> qc;
  qc.set_col_filename(out_col_video_file);
  qc.set_gray_filename(out_gray_video_file);
  qc.set_verbose(verbose);
  qc.set_outstream(&outstream);
  qc.check(nb_qc_tests,5*nb_qc_tests,verbose,outstream);
#endif

  return 0;
}

/*!
** \brief usage :
**  cmd.exe args
**  ex : ./test_avwriter my_arg
*/
void usage(const char *name) {
  std::cout << "usage :\n\n";
  std::cout << "\t " << name << " verbose(1/0) InputColorAvVideoFile "
	    << "InputGrayscaleAvVideoFile OutputColorAvVideoFile OutputGrayscaleAvVideoFile\n\n";
}

int main(int argc, char *argv[])
{
  if (argc < 6) {
    usage(*argv);
    return 1;
  }
  std::string verb_str(argv[1]);
  bool verbose = false;
  if(verb_str == "1")
    verbose = true;
  std::string in_col_video_file(argv[2]);
  std::string in_gray_video_file(argv[3]);
  std::string out_col_video_file(argv[4]);
  std::string out_gray_video_file(argv[5]);
  std::string test_name(*argv);
  std::ostream & outstream = std::cout;
  int nb_qc_tests = 5;

  //------------------------------
  //single write function
  //------------------------------
  if(verbose){
    outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
    outstream <<  std::setfill(' ') << std::setw(20) << ' ';
    outstream << test_name << " - single write function" << std::endl;
    outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  }
  //single write function
  {

    if (avwriter_write_single_test<double>(verbose, outstream, test_name, nb_qc_tests,
    					   in_col_video_file,in_gray_video_file, out_col_video_file,out_gray_video_file))
      return 1;
    if (avwriter_write_single_test<int>(verbose, outstream, test_name, nb_qc_tests,
    					in_col_video_file,in_gray_video_file, out_col_video_file,out_gray_video_file))
      return 1;
    if (avwriter_write_single_test<unsigned char>(verbose, outstream, test_name, nb_qc_tests,
    						  in_col_video_file,in_gray_video_file, out_col_video_file,out_gray_video_file))
      return 1;
  }

  //------------------------------
  //many write function
  //------------------------------
  if(verbose){
    outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
    outstream <<  std::setfill(' ') << std::setw(20) << ' ';
    outstream << test_name << " - many write function" << std::endl;
    outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  }
  //many write function
  {

    		if (avwriter_write_many_test<double>(verbose, outstream, test_name, nb_qc_tests,
    				in_col_video_file,in_gray_video_file, out_col_video_file,out_gray_video_file))
    			return 1;
    		if (avwriter_write_many_test<int>(verbose, outstream, test_name, nb_qc_tests,
    				in_col_video_file,in_gray_video_file, out_col_video_file,out_gray_video_file))
    			return 1;
    		if (avwriter_write_many_test<unsigned char>(verbose, outstream, test_name, nb_qc_tests,
    				in_col_video_file,in_gray_video_file, out_col_video_file,out_gray_video_file))
    			return 1;
  }


  return 0;
}



