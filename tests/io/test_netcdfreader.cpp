/*!
 ** \file test_netcdfreader.cpp
 ** \brief %NetCDFReader unit tests
 ** \date 2013/05/14
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \todo testing reader with 3D signals.
 */
#include <iostream>
#include <iomanip>

#include "NetCDFReader.hpp"
//#include "netcdf_io.hpp"
#include "Vector.hpp"
#include "ColorImage.hpp"
#include "utils_compilation.hpp"

/*!
 * \brief %NetCDFReader read method testing in 1D containers with 1 Component
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_mono_signal name of the input mono signal netcdf file.
 * \param size of the signal
 */
template<typename test_type>
int netcdfreader1D1C_read_test(bool verbose, std::ostream & outstream,
			       const std::string & UNUSED(parent_test_name), std::string in_mono_signal,
		std::string in_mono_var, std::size_t size){

	std::vector<std::string> var_names(1,in_mono_var);
	//	slip::Vector<test_type> sig1;
	//	slip::read_netcdf_1d(sig1,in_mono_signal,var_names[0]);
	slip::NetCDFReader<slip::Vector<test_type>,test_type,1,1,1> reader1D1C;
	reader1D1C.set_data_filename(in_mono_signal);
	reader1D1C.initialize(var_names);
	slip::Vector<test_type> sig1D;
	reader1D1C.read(sig1D);
	reader1D1C.release();
	if (sig1D.empty() || sig1D.size() != size){
		if (verbose){
			outstream << "-----netcdfreader1D1C_read_test failed : 1 container\n";
		}
		return 1;
	}
	slip::Vector<test_type> sig1D_1;
	slip::Vector<test_type> sig1D_2;
	slip::NetCDFReader<slip::Vector<test_type>,test_type,1,2,1> reader1D1C_2(in_mono_signal,var_names);
	reader1D1C_2.read(sig1D_1);
	reader1D1C_2.read(sig1D_2);
	slip::Vector<test_type> sig1D_rec(size,test_type());
	std::copy(sig1D_1.begin(),sig1D_1.end(),sig1D_rec.begin());
	std::copy(sig1D_2.rbegin(),sig1D_2.rend(),sig1D_rec.rbegin());
	if (sig1D != sig1D_rec){
		if (verbose){
			outstream << "-----netcdfreader1D1C_read_test failed : 2 containers\n";
		}
		return 1;
	}
	return 0;
}

/*!
 * \brief %NetCDFReader read method testing in 2D containers with 3 Components
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_mono_signal name of the input mono signal netcdf file.
 * \param size of the signal
 */
template<typename test_type>
int netcdfreader2D3C_read_test(bool verbose, std::ostream & outstream,
			       const std::string & UNUSED(parent_test_name), std::string in_multi_signal,
		std::string in_multi_var1,std::string in_multi_var2,std::string in_multi_var3,
		std::size_t dim1,std::size_t dim2){

	std::vector<std::string> var_names(3);
	var_names[0] = in_multi_var1;
	var_names[1] = in_multi_var2;
	var_names[2] = in_multi_var3;
	slip::NetCDFReader<slip::ColorImage<test_type>,test_type,3,1,2> reader2D3C;
	reader2D3C.set_data_filename(in_multi_signal);
	reader2D3C.initialize(var_names);
	slip::ColorImage<test_type> sig2D;
	reader2D3C.read(sig2D);
	reader2D3C.release();
	if (sig2D.empty() || sig2D.height() != dim1 || sig2D.width() != dim2){
		if (verbose){
			outstream << "-----netcdfreader2D3C_read_test failed : 1 container\n";
		}
		return 1;
	}


	slip::ColorImage<test_type> sig2D_1;
	slip::ColorImage<test_type> sig2D_2;
	slip::NetCDFReader<slip::ColorImage<test_type>,test_type,3,2,2> reader2D3C_2(in_multi_signal,var_names);
	reader2D3C_2.read(sig2D_1);
	reader2D3C_2.read(sig2D_2);
	slip::ColorImage<test_type> sig2D_rec(dim1,dim2);
	std::copy(sig2D_1.begin(),sig2D_1.end(),sig2D_rec.begin());
	std::copy(sig2D_2.rbegin(),sig2D_2.rend(),sig2D_rec.rbegin());
	if (sig2D != sig2D_rec){
		if (verbose){
			outstream << "-----netcdfreader2D3C_read_test failed : 2 containers\n";
		}
		return 1;
	}

	return 0;
}




/*!
 ** \brief usage :
 **  cmd.exe args
 **  ex : ./test_netcdfreader my_arg
 */
void usage(const char *name) {
	std::cout << "usage :\n\n";
	std::cout << "\t " << name << " verbose(1/0) Input_MonoSignal_NetCDFFile"
			<< " Input_MonoSignal_VarName Input_MonoSignal_VarSize Input_MultiSignal_NetCDFFile"
			<< " Input_MultiSignal_VarName1 Input_MultiSignal_VarName2 Input_MultiSignal_VarName3"
			<< " Input_MultiSignal_VarSize1 Input_MultiSignal_VarSize2\n\n";
}

int main(int argc, char *argv[])
{
	if (argc < 10) {
		usage(*argv);
		return 1;
	}
	std::string verb_str(argv[1]);
	bool verbose = false;
	if(verb_str == "1")
		verbose = true;
	std::string monosignal_file(argv[2]);
	std::string monosignal_var(argv[3]);
	std::size_t monosignal_size = atoi(argv[4]);
	std::string multisignal_file(argv[5]);
	std::string multisignal_var1(argv[6]);
	std::string multisignal_var2(argv[7]);
	std::string multisignal_var3(argv[8]);
	std::size_t multisignal_size1 = atoi(argv[9]);
	std::size_t multisignal_size2 = atoi(argv[10]);
	std::string test_name(*argv);
	std::ostream & outstream = std::cout;

	//------------------------------
	//1D read function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - 1D read function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//1D read function
	{
		if (netcdfreader1D1C_read_test<float>(verbose,outstream,
				test_name,monosignal_file,monosignal_var,monosignal_size))
			return 1;
		if (netcdfreader1D1C_read_test<int>(verbose,outstream,
				test_name,monosignal_file,monosignal_var,monosignal_size))
			return 1;
		if (netcdfreader1D1C_read_test<double>(verbose,outstream,
				test_name,monosignal_file,monosignal_var,monosignal_size))
			return 1;
	}

	//------------------------------
	//2D read function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - 2D read function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//single read function
	{
		if (netcdfreader2D3C_read_test<float>(verbose,outstream,
				test_name,multisignal_file,multisignal_var1,multisignal_var2,
				multisignal_var3,multisignal_size1,multisignal_size2))
			return 1;
		if (netcdfreader2D3C_read_test<int>(verbose,outstream,
				test_name,multisignal_file,multisignal_var1,multisignal_var2,
				multisignal_var3,multisignal_size1,multisignal_size2))
			return 1;
		if (netcdfreader2D3C_read_test<double>(verbose,outstream,
				test_name,multisignal_file,multisignal_var1,multisignal_var2,
				multisignal_var3,multisignal_size1,multisignal_size2))
			return 1;
	}


	return 0;
}
