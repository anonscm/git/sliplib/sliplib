/*!
 ** \file test_wavwriter.cpp
 ** \file test_wavwriter.cpp
 ** \brief %WavWriter unit tests
 ** \date 2013/04/19
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#include "WavReader_qc.hpp"
#include "WavWriter_qc.hpp"
#include "utils_compilation.hpp"

/*!
 * \brief %WavWriter write method testing with only one container
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param nb_qc_tests number of quickcheck tests to run (if defined)
 * \param in_file name of the input wav file.
 * \param out_file name of the output wav file.
 */
template<typename test_type>
int wavwriter_write_single_test(bool verbose, std::ostream & outstream,
				const std::string & UNUSED(parent_test_name), int UNUSED(nb_qc_tests),
		std::string in_file, std::string out_file){
	slip::WavReader<slip::Vector<test_type>,test_type,1> reader(in_file);
	slip::Vector<test_type> audio;
	reader.read(audio);
	WAVE_HEADER head = reader.get_header();
	if (!def_wavwriter_write_single_property<test_type>(out_file, head, audio
			, verbose, outstream)){
		if (verbose){
			outstream << "-----Single audio read failed\n";
		}
		return 1;
	}

#ifdef HAVE_QCHECK
	quickcheck::WavWriterProperty1<test_type> qc;
	qc.set_filename(out_file);
	qc.set_verbose(verbose);
	qc.set_outstream(&outstream);
	qc.check(nb_qc_tests,5*nb_qc_tests,verbose,outstream);
#endif

	return 0;
}

/*!
 * \brief %WavWriter write method testing with many containers
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param nb_qc_tests number of quickcheck tests to run (if defined)
 * \param in_file name of the input wav file.
 * \param out_file name of the output wav file.
 */
template<typename test_type>
int wavwriter_write_many_test(bool verbose, std::ostream & outstream,
			      const std::string & UNUSED(parent_test_name), int UNUSED(nb_qc_tests), std::string in_file,
		std::string out_file){

	slip::WavReader<slip::Vector<test_type>,test_type,1> reader(in_file);
	slip::Vector<test_type> audio;
	reader.read(audio);
	WAVE_HEADER head = reader.get_header();
	std::vector<slip::Vector<test_type> > audio_list(4,audio);
	if (!def_wavwriter_write_many_property<test_type>(out_file, head, audio_list
			, verbose, outstream)){
		if (verbose){
			outstream << "-----ColorImage failed\n";
		}
		return 1;
	}

#ifdef HAVE_QCHECK
	quickcheck::WavWriterProperty2<test_type> qc;
	qc.set_filename(out_file);
	qc.set_verbose(verbose);
	qc.set_outstream(&outstream);
	qc.check(nb_qc_tests,5*nb_qc_tests,verbose,outstream);
#endif

	return 0;
}

/*!
 * Basic test that checks that the input file can be written and reread without any change.
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param nb_qc_tests number of quickcheck tests to run (if defined)
 * \param in_file name of the input wav file.
 * \param out_file name of the output wav file.
 */
template<typename test_type>
int wavwriter_write_basic_test(bool verbose, std::ostream & outstream,
			       const std::string & UNUSED(parent_test_name), int UNUSED(nb_qc_tests), std::string in_file,
		std::string out_file){

	slip::WavReader<slip::Vector<test_type>,test_type,1> reader(in_file);
	slip::Vector<test_type> audio1;
	reader.read(audio1);
	WAVE_HEADER head = reader.get_header();
	if (verbose){
		outstream << head << std::endl;
	}
	reader.release();
	slip::WavWriter<slip::Vector<test_type>,test_type> writer(out_file,head,audio1.size());
	writer.write(audio1);
	head = writer.get_header();
	if (verbose){
		outstream << head << std::endl;
	}
	writer.release();
	slip::WavReader<std::vector<test_type>,test_type,1> reader_std(out_file);
	std::vector<test_type> audio2;
	reader_std.read(audio2);
	head = reader_std.get_header();
	if (verbose){
		outstream << head << std::endl;
	}

	typename slip::Vector<test_type>::iterator i1 = audio1.begin(), i1end = audio1.end();
	typename std::vector<test_type>::iterator i2 = audio2.begin(), i2end = audio2.end();
	while ((i1 != i1end) && (i2 != i2end)) {
		test_type diff = *i1 - *i2;
		double norm = static_cast<double>(diff*diff);
		if (norm > 1e-200) {
			outstream << "-----Basic test failed\n";
			return 1;
		}
		i1++;
		i2++;
	}
	return 0;
}

/*!
 ** \brief usage :
 **  cmd.exe args
 **  ex : ./test_wavwriter my_arg
 */
void usage(const char *name) {
	std::cout << "usage :\n\n";
	std::cout << "\t " << name << " verbose(1/0) InputWavFile "
			<< "OutputWavFile\n\n";
}

int main(int argc, char *argv[])
{
	if (argc < 4) {
		usage(*argv);
		return 1;
	}
	std::string verb_str(argv[1]);
	bool verbose = false;
	if(verb_str == "1")
		verbose = true;
	std::string in_file(argv[2]);
	std::string out_file(argv[3]);
	std::string test_name(*argv);
	std::ostream & outstream = std::cout;
	int nb_qc_tests = 100;

	//------------------------------
	//single write function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - single write function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}

	//single write function
	{

		if (wavwriter_write_single_test<double>(verbose, outstream, test_name, nb_qc_tests,
				in_file,out_file))
			return 1;
		if (wavwriter_write_single_test<int>(verbose, outstream, test_name, nb_qc_tests,
				in_file,out_file))
			return 1;
		if (wavwriter_write_single_test<unsigned char>(verbose, outstream, test_name, nb_qc_tests,
				in_file,out_file))
			return 1;
	}

	//------------------------------
	//many write function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - many write function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//many write function
	{

		if (wavwriter_write_many_test<double>(verbose, outstream, test_name, nb_qc_tests,
				in_file, out_file))
			return 1;
		if (wavwriter_write_many_test<int>(verbose, outstream, test_name, nb_qc_tests,
				in_file, out_file))
			return 1;
		if (wavwriter_write_many_test<unsigned char>(verbose, outstream, test_name, nb_qc_tests,
				in_file, out_file))
			return 1;
	}

	//------------------------------
	//basic write function test
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - basic write function test" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//basic write function test
	{
		if (wavwriter_write_basic_test<double>(verbose, outstream, test_name, nb_qc_tests,
				in_file, out_file))
			return 1;
		if (wavwriter_write_basic_test<int>(verbose, outstream, test_name, nb_qc_tests,
				in_file, out_file))
		return 1;
		if (wavwriter_write_basic_test<unsigned char>(verbose, outstream, test_name, nb_qc_tests,
				in_file, out_file))
			return 1;
	}

	return 0;
}


