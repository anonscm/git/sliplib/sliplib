/*!
 ** \file test_pngwriter.cpp
 ** \brief %PngWriter unit tests
 ** \date 2013/04/23
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#include "PngReader_qc.hpp"
#include "PngWriter_qc.hpp"
#include "utils_compilation.hpp"

/*!
 * \brief %PngWriter write method testing with only one container
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param nb_qc_tests number of quickcheck tests to run (if defined)
 * \param in_col_image_file name of the input color png file.
 * \param in_gray_image_file name of the input grayscale png file.
 * \param out_col_image_file name of the output color png file.
 * \param out_gray_image_file name of the output grayscale png file.
 */
template<typename test_type>
int pngwriter_write_single_test(bool verbose, std::ostream & outstream,
				const std::string & UNUSED(parent_test_name), int UNUSED(nb_qc_tests), std::string in_col_image_file,
		std::string in_gray_image_file, std::string out_col_image_file, std::string out_gray_image_file){
	slip::PngReader<slip::ColorImage<test_type>,test_type,3,1> coul_jpgread(in_col_image_file);
	slip::ColorImage<test_type> coul_image;
	coul_jpgread.read(coul_image);
	if (!def_pngwriter_write_colorimage_property1<test_type>(out_col_image_file, coul_image
			, verbose, outstream)){
		if (verbose){
			outstream << "-----ColorImage failed\n";
		}
		return 1;
	}

	slip::PngReader<slip::GrayscaleImage<test_type>,test_type,1,1> gray_jpgread(in_gray_image_file);
	slip::GrayscaleImage<test_type> gray_image;
	gray_jpgread.read(gray_image);
	if (!def_pngwriter_write_grayscaleimage_property1<test_type>(out_gray_image_file, gray_image
			, verbose, outstream)){
		if (verbose){
			outstream << "-----GrayScale failed\n";
		}
		return 1;
	}

#ifdef HAVE_QCHECK
	quickcheck::PngWriterProperty1<test_type> qc;
	qc.set_col_filename(out_col_image_file);
	qc.set_gray_filename(out_gray_image_file);
	qc.set_verbose(verbose);
	qc.set_outstream(&outstream);
	qc.check(nb_qc_tests,5*nb_qc_tests,verbose,outstream);
#endif

	return 0;
}

/*!
 * \brief %PngWriter write method testing with many containers
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param nb_qc_tests number of quickcheck tests to run (if defined)
 * \param in_col_image_file name of the input color png file.
 * \param in_gray_image_file name of the input grayscale png file.
 * \param out_col_image_file name of the output color png file.
 * \param out_gray_image_file name of the output grayscale png file.
 */
template<typename test_type>
int pngwriter_write_many_test(bool verbose, std::ostream & outstream,
			      const std::string & UNUSED(parent_test_name), int UNUSED(nb_qc_tests), std::string in_col_image_file,
		std::string in_gray_image_file, std::string out_col_image_file, std::string out_gray_image_file){

	slip::PngReader<slip::ColorImage<test_type>,test_type,3,1> coul_jpgread(in_col_image_file);
	slip::ColorImage<test_type> coul_image;
	coul_jpgread.read(coul_image);
	std::vector<slip::ColorImage<test_type> > coul_image_list(4,coul_image);
	if (!def_pngwriter_write_colorimage_property2<test_type>(out_col_image_file, coul_image_list
			, verbose, outstream)){
		if (verbose){
			outstream << "-----ColorImage failed\n";
		}
		return 1;
	}

	slip::PngReader<slip::GrayscaleImage<test_type>,test_type,1,1> gray_jpgread(in_gray_image_file);
	slip::GrayscaleImage<test_type> gray_image;
	gray_jpgread.read(gray_image);
	std::vector<slip::GrayscaleImage<test_type> > gray_image_list(4,gray_image);
	if (!def_pngwriter_write_grayscaleimage_property2<test_type>(out_gray_image_file, gray_image_list
			, verbose, outstream)){
		if (verbose){
			outstream << "-----GrayScale failed\n";
		}
		return 1;
	}

#ifdef HAVE_QCHECK
	quickcheck::PngWriterProperty2<test_type> qc;
	qc.set_col_filename(out_col_image_file);
	qc.set_gray_filename(out_gray_image_file);
	qc.set_verbose(verbose);
	qc.set_outstream(&outstream);
	qc.check(nb_qc_tests,5*nb_qc_tests,verbose,outstream);
#endif

	return 0;
}

/*!
 ** \brief usage :
 **  cmd.exe args
 **  ex : ./test_pngwriter my_arg
 */
void usage(const char *name) {
	std::cout << "usage :\n\n";
	std::cout << "\t " << name << " verbose(1/0) InputColorPngImageFile "
			<< "InputGrayscalePngImageFile OutputColorPngImageFile OutputGrayscalePngImageFile\n\n";
}

int main(int argc, char *argv[])
{
	if (argc < 6) {
		usage(*argv);
		return 1;
	}
	std::string verb_str(argv[1]);
	bool verbose = false;
	if(verb_str == "1")
		verbose = true;
	std::string in_col_image_file(argv[2]);
	std::string in_gray_image_file(argv[3]);
	std::string out_col_image_file(argv[4]);
	std::string out_gray_image_file(argv[5]);
	std::string test_name(*argv);
	std::ostream & outstream = std::cout;
	int nb_qc_tests = 100;

	//------------------------------
	//single write function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - single write function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//single write function
	{

		if (pngwriter_write_single_test<double>(verbose, outstream, test_name, nb_qc_tests,
				in_col_image_file,in_gray_image_file, out_col_image_file,out_gray_image_file))
			return 1;
		if (pngwriter_write_single_test<int>(verbose, outstream, test_name, nb_qc_tests,
				in_col_image_file,in_gray_image_file, out_col_image_file,out_gray_image_file))
			return 1;
		if (pngwriter_write_single_test<unsigned char>(verbose, outstream, test_name, nb_qc_tests,
			in_col_image_file,in_gray_image_file, out_col_image_file,out_gray_image_file))
			return 1;
	}

	//------------------------------
	//many write function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - many write function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//many write function
	{

		if (pngwriter_write_many_test<double>(verbose, outstream, test_name, nb_qc_tests,
				in_col_image_file,in_gray_image_file, out_col_image_file,out_gray_image_file))
			return 1;
		if (pngwriter_write_many_test<int>(verbose, outstream, test_name, nb_qc_tests,
				in_col_image_file,in_gray_image_file, out_col_image_file,out_gray_image_file))
			return 1;
		if (pngwriter_write_many_test<unsigned char>(verbose, outstream, test_name, nb_qc_tests,
				in_col_image_file,in_gray_image_file, out_col_image_file,out_gray_image_file))
			return 1;
	}


	return 0;
}



