/*!
 ** \file test_fitswriter.cpp
 ** \brief %FITSWriter unit tests
 ** \date 2013/05/14
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \todo testing writer with 3D signals.
 */
#include <iostream>
#include <iomanip>

#include "FITSReader.hpp"
#include "FITSWriter.hpp"
#include "WavReader.hpp"
#include "Vector.hpp"
#include "JpegReader.hpp"
#include "GrayscaleImage.hpp"
#include "ColorImage.hpp"
#include "AvReader.hpp"
#include "ColorVolume.hpp"

/*!
 * \brief %FITSWriter write method testing with 1D containers
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_1d_wav_signal name of the input signal wav file.
 * \param out_1d_file name of the output file
 */
template<typename test_type>
int fitswriter1D_write_test(bool verbose, std::ostream & outstream,
		const std::string & parent_test_name, std::string in_1d_wav_signal,
		std::string out_1d_file){

	slip::WavReader<slip::Vector<test_type>,test_type,1> reader1D;
	reader1D.set_data_filename(in_1d_wav_signal);
	reader1D.initialize();
	slip::Vector<test_type> sig1D;
	reader1D.read(sig1D);
	reader1D.release();

	std::vector<std::size_t> output_signal_dim(1,sig1D.size());

	slip::FITSWriter<slip::Vector<test_type>,test_type,1,1> writer1D;
	writer1D.set_output_filename(out_1d_file);
	try{
		writer1D.initialize(output_signal_dim);
	}catch(slip::slip_exception & e){
		std::cerr << e.what();
		if (verbose){
			outstream << "-----fitswriter1D_write_test failed : 1 container\n";
		}
		return 1;
	}
	try{
		writer1D.write(sig1D);
	}catch(slip::slip_exception & e){
		std::cerr << e.what();
		if (verbose){
			outstream << "-----fitswriter1D_write_test failed : 1 container\n";
		}
		return 1;
	}
	writer1D.release();
	slip::FITSReader<slip::Vector<test_type>,test_type,1,1,1> fitsreader;
	fitsreader.set_data_filename(out_1d_file);
	fitsreader.initialize();
	slip::Vector<test_type> sig1D2;
	fitsreader.read(sig1D2);
	fitsreader.release();

	if (sig1D2 != sig1D){
		if (verbose){
			outstream << "-----fitswriter1D_write_test failed : 1 container\n";
		}
		return 1;
	}

	std::size_t s1 = sig1D.size() / 2;
	std::size_t s2 = sig1D.size() - s1;
	slip::Vector<test_type> sig1D_1(s1);
	slip::Vector<test_type> sig1D_2(s2);
	std::copy(sig1D.begin(),sig1D.begin() + s1,sig1D_1.begin());
	std::copy(sig1D.begin() + s1,sig1D.end(),sig1D_2.begin());
	slip::FITSWriter<slip::Vector<test_type>,test_type,1,1> writer1D_2(out_1d_file,output_signal_dim);
	writer1D_2.write(sig1D_1);
	writer1D_2.write(sig1D_2);
	writer1D_2.release();

	fitsreader.initialize();
	fitsreader.read(sig1D2);
	fitsreader.release();


	if (sig1D != sig1D2){
		if (verbose){
			outstream << "-----fitswriter1D_write_test failed : 2 containers\n";
			outstream << "sig1D.size() = " << sig1D.size();
			outstream << std::endl;
			outstream << "sig1D2.size() = " << sig1D2.size();
			outstream << std::endl;
			//			outstream << "->sig1D " << sig1D_1 << std::endl;
			//			outstream << "->sig1D_1 " << sig1D_1 << std::endl;
			//			outstream << "->sig1D_2 " << sig1D_2 << std::endl;
			//outstream << "->sig1D2 = " << sig1D2 << std::endl;
		}
		return 1;
	}
	return 0;
}

/*!
 * \brief %FITSWriter write method testing with 2D containers
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_2d_jpeg_gray_signal name of the input grayscale image jpeg file.
 * \param out_2d_file output file name
 */
template<typename test_type>
int fitswriter2D_write_test(bool verbose, std::ostream & outstream,
		const std::string & parent_test_name, std::string in_2d_jpeg_gray_signal,
		std::string out_2d_file){

	slip::JpegReader<slip::GrayscaleImage<test_type>,test_type,1,1> jpgreader;
	jpgreader.set_data_filename(in_2d_jpeg_gray_signal);
	jpgreader.initialize();
	slip::GrayscaleImage<test_type> sig2D;
	jpgreader.read(sig2D);
	jpgreader.release();

	std::vector<std::size_t> output_signal_dim(2);
	output_signal_dim[0] = sig2D.dim1();
	output_signal_dim[1] = sig2D.dim2();

	slip::FITSWriter<slip::GrayscaleImage<test_type>,test_type,1,2> writer2D;
	writer2D.set_output_filename(out_2d_file);
	writer2D.initialize(output_signal_dim);
	writer2D.write(sig2D);
	writer2D.release();
	slip::FITSReader<slip::GrayscaleImage<test_type>,test_type,1,1,2> reader2D;
	reader2D.set_data_filename(out_2d_file);
	reader2D.initialize();
	slip::GrayscaleImage<test_type> sig2D2;
	reader2D.read(sig2D2);
	reader2D.release();

	if (sig2D != sig2D2){
		if (verbose){
			outstream << "-----fitswriter2D_write_test failed : 1 container\n";
		}
		return 1;
	}

	std::size_t s1 = sig2D.dim1() / 2;
	std::size_t s2 = sig2D.dim1() - s1;
	slip::Box2d<int> box1(0,0,s1-1,sig2D.dim2()-1);
	slip::Box2d<int> box2(s1,0,sig2D.dim1()-1,sig2D.dim2()-1);
	slip::GrayscaleImage<test_type> sig2D_1(s1,sig2D.dim2(),sig2D.upper_left(box1),sig2D.bottom_right(box1));
	slip::GrayscaleImage<test_type> sig2D_2(s2,sig2D.dim2(),sig2D.upper_left(box2),sig2D.bottom_right(box2));
	slip::FITSWriter<slip::GrayscaleImage<test_type>,test_type,1,2> writer2D_2(out_2d_file,output_signal_dim);
	writer2D_2.write(sig2D_1);
	writer2D_2.write(sig2D_2);
	writer2D_2.release();

	reader2D.initialize();
	reader2D.read(sig2D2);
	reader2D.release();

	if (sig2D != sig2D2){
		if (verbose){
			outstream << "-----fitswriter2D_write_test failed : 2 containers" << std::endl;
			outstream << "sig2D.dim1() = " << sig2D.dim1();
			outstream << std::endl;
			outstream << "sig2D.dim2() = " << sig2D.dim2();
			outstream << std::endl;
			outstream << "sig2D2.dim1() = " << sig2D2.dim1();
			outstream << std::endl;
			outstream << "sig2D2.dim2() = " << sig2D2.dim2();
			outstream << std::endl;
		}
		return 1;
	}

	return 0;
}

/*!
 * \brief %FITSWriter write method testing on a color jpg image
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_jpg_image name of the input jpg file file.
 * \param multi_outputfile output file name
 * \param size of the signal
 */
template<typename test_type>
int fitswriter_colorimage_write_test(bool verbose, std::ostream & outstream,
		const std::string & parent_test_name, std::string in_jpg_image,
		std::string multi_outputfile){

	slip::JpegReader<slip::ColorImage<test_type>,test_type,3,1> jpgreader(in_jpg_image);
	slip::ColorImage<test_type> sig2D;
	jpgreader.read(sig2D);
	jpgreader.release();

	std::vector<std::size_t> output_signal_dim(2);
	output_signal_dim[0] = sig2D.dim1();
	output_signal_dim[1] = sig2D.dim2();

	slip::FITSWriter<slip::ColorImage<test_type>,test_type,3,2> writer2D3C;
	writer2D3C.set_output_filename(multi_outputfile);
	writer2D3C.initialize(output_signal_dim);
	writer2D3C.write(sig2D);
	writer2D3C.release();

	slip::FITSReader<slip::ColorImage<test_type>,test_type,3,1,2> reader2D3C;
	reader2D3C.set_data_filename(multi_outputfile);
	reader2D3C.initialize();
	slip::ColorImage<test_type> sig2D2;
	reader2D3C.read(sig2D2);
	reader2D3C.release();
	typedef typename slip::ColorImage<test_type>::component_iterator iter;
	std::pair<iter,iter> pair_it;
	pair_it = std::mismatch(sig2D.begin(0), sig2D.end(0), sig2D2.begin(0));
	if (pair_it.first != sig2D.end(0)){
		if (verbose){
			outstream << "-----fitswriter_colorimage_write_test failed : 1 container\n";
		}
		return 1;
	}

	std::size_t s1 = sig2D.dim1() / 2;
	std::size_t s2 = sig2D.dim1() - s1;
	slip::Box2d<int> box1(0,0,s1-1,sig2D.dim2()-1);
	slip::Box2d<int> box2(s1,0,sig2D.dim1()-1,sig2D.dim2()-1);
	slip::ColorImage<test_type> sig2D_1(s1,sig2D.dim2(),sig2D.upper_left(box1),sig2D.bottom_right(box1));
	slip::ColorImage<test_type> sig2D_2(s2,sig2D.dim2(),sig2D.upper_left(box2),sig2D.bottom_right(box2));
	slip::FITSWriter<slip::ColorImage<test_type>,test_type,3,2> writer2D3C_2(multi_outputfile,
			output_signal_dim);
	writer2D3C_2.write(sig2D_1);
	writer2D3C_2.write(sig2D_2);
	writer2D3C_2.release();

	reader2D3C.initialize();
	reader2D3C.read(sig2D2);
	reader2D3C.release();

	pair_it = std::mismatch(sig2D.begin(0), sig2D.end(0), sig2D2.begin(0));
	if (pair_it.first != sig2D.end(0)){
		if (verbose){
			outstream << "-----fitswriter_colorimage_write_test failed : 2 containers" << std::endl;
			outstream << "sig2D.dim1() = " << sig2D.dim1();
			outstream << std::endl;
			outstream << "sig2D.dim2() = " << sig2D.dim2();
			outstream << std::endl;
			outstream << "sig2D2.dim1() = " << sig2D2.dim1();
			outstream << std::endl;
			outstream << "sig2D2.dim2() = " << sig2D2.dim2();
			outstream << std::endl;
		}
		return 1;
	}

	return 0;
}


/*!
 * \brief %FITSWriter write method testing on a color movie
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_video name of the input video file.
 * \param multi_outputfile output file name
 * \param size of the signal
 */
template<typename test_type>
int fitswriter_colormovie_write_test(bool verbose, std::ostream & outstream,
		const std::string & parent_test_name, std::string in_movie,
		std::string multi_outputfile){

	slip::AvReader<slip::ColorVolume<test_type>,test_type,3,1> avreader(in_movie);
	slip::ColorVolume<test_type> sig3D;
	avreader.read(sig3D);
	avreader.release();

	std::vector<std::size_t> output_signal_dim(3);
	output_signal_dim[0] = sig3D.dim1();
	output_signal_dim[1] = sig3D.dim2();
	output_signal_dim[2] = sig3D.dim3();

	slip::FITSWriter<slip::ColorVolume<test_type>,test_type,3,3> writer3D3C;
	writer3D3C.set_output_filename(multi_outputfile);
	writer3D3C.initialize(output_signal_dim);
	writer3D3C.write(sig3D);
	writer3D3C.release();

	slip::FITSReader<slip::ColorVolume<test_type>,test_type,3,1,3> reader3D3C;
	reader3D3C.set_data_filename(multi_outputfile);
	reader3D3C.initialize();
	slip::ColorVolume<test_type> sig3D2;
	reader3D3C.read(sig3D2);
	reader3D3C.release();

	typedef typename slip::ColorVolume<test_type>::component_iterator iter;
	std::pair<iter,iter> pair_it;
	pair_it = std::mismatch(sig3D.begin(0), sig3D.end(0), sig3D2.begin(0));
	if (pair_it.first != sig3D.end(0)){
		if (verbose){
			outstream << "-----fitswriter_colormovie_write_test failed : 1 container\n";
		}
		return 1;
	}

	std::size_t s1 = sig3D.dim1() / 2;
	std::size_t s2 = sig3D.dim1() - s1;
	slip::Box3d<int> box1(0,0,0,s1-1,sig3D.dim2()-1,sig3D.dim3()-1);
	slip::Box3d<int> box2(s1,0,0,sig3D.dim1()-1,sig3D.dim2()-1,sig3D.dim3()-1);
	slip::ColorVolume<test_type> sig3D_1(s1,sig3D.dim2(),sig3D.dim3(),sig3D.front_upper_left(box1),
			sig3D.back_bottom_right(box1));
	slip::ColorVolume<test_type> sig3D_2(s2,sig3D.dim2(),sig3D.dim3(),sig3D.front_upper_left(box2),
			sig3D.back_bottom_right(box2));
	slip::FITSWriter<slip::ColorVolume<test_type>,test_type,3,3> writer3D3C_2(multi_outputfile,
			output_signal_dim);
	writer3D3C_2.write(sig3D_1);
	writer3D3C_2.write(sig3D_2);
	writer3D3C_2.release();

	reader3D3C.initialize();
	reader3D3C.read(sig3D2);
	reader3D3C.release();

	pair_it = std::mismatch(sig3D.begin(0), sig3D.end(0), sig3D2.begin(0));
	if (pair_it.first != sig3D.end(0)){
		if (verbose){
			outstream << "-----fitswriter_colormovie_write_test failed : 2 containers" << std::endl;
			outstream << "sig2D.dim1() = " << sig3D.dim1();
			outstream << std::endl;
			outstream << "sig2D.dim2() = " << sig3D.dim2();
			outstream << std::endl;
			outstream << "sig2D.dim3() = " << sig3D.dim3();
			outstream << std::endl;
			outstream << "sig2D2.dim1() = " << sig3D2.dim1();
			outstream << std::endl;
			outstream << "sig2D2.dim2() = " << sig3D2.dim2();
			outstream << std::endl;
			outstream << "sig2D2.dim3() = " << sig3D2.dim3();
			outstream << std::endl;
		}
		return 1;
	}

	return 0;
}

/*!
 ** \brief usage :
 **  cmd.exe args
 **  ex : ./test_fitswriter my_arg
 */
void usage(const char *name) {
	std::cout << "usage :\n\n";
	std::cout << "\t " << name << " verbose(1/0) Input_1D_Wav_File"
			<< " Input_2D_Gray_Jpg_File Input_2D_Color_Jpg_File Input_ColorMovie_File"
			<< " Output_1D_FITS_File Output_2D_Gray_FITS_File Output_2D_Color_FITS_File"
			<< " Output_3D_FITS_File\n\n";
}

int main(int argc, char *argv[])
{
	if (argc < 10) {
		usage(*argv);
		return 1;
	}
	std::string verb_str(argv[1]);
	bool verbose = false;
	if(verb_str == "1")
		verbose = true;
	std::string Input_1D_Wav_File(argv[2]);
	std::string Input_2D_Gray_Jpg_File(argv[3]);
	std::string Input_2D_Color_Jpg_File(argv[4]);
	std::string Input_ColorMovie_File(argv[5]);
	std::string Output_1D_FITS_File(argv[6]);
	std::string Output_2D_Gray_FITS_File(argv[7]);
	std::string Output_2D_Color_FITS_File(argv[8]);
	std::string Output_3D_FITS_File(argv[9]);
	std::string test_name(*argv);
	std::ostream & outstream = std::cout;

	//------------------------------
	//1D write function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - 1D write function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//1D write function
	{
		if (fitswriter1D_write_test<float>(verbose,outstream,
				test_name,Input_1D_Wav_File,Output_1D_FITS_File))
			return 1;
		if (fitswriter1D_write_test<int>(verbose,outstream,
				test_name,Input_1D_Wav_File,Output_1D_FITS_File))
			return 1;
		if (fitswriter1D_write_test<double>(verbose,outstream,
				test_name,Input_1D_Wav_File,Output_1D_FITS_File))
			return 1;
	}

	//------------------------------
	//2D write function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - 2D write function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//2D write function
	{
		if (fitswriter2D_write_test<float>(verbose,outstream,
				test_name,Input_2D_Gray_Jpg_File,Output_2D_Gray_FITS_File))
			return 1;
		if (fitswriter2D_write_test<int>(verbose,outstream,
				test_name,Input_2D_Gray_Jpg_File,Output_2D_Gray_FITS_File))
			return 1;
		if (fitswriter2D_write_test<double>(verbose,outstream,
				test_name,Input_2D_Gray_Jpg_File,Output_2D_Gray_FITS_File))
			return 1;
		if (fitswriter_colorimage_write_test<float>(verbose,outstream,
				test_name,Input_2D_Color_Jpg_File,Output_2D_Color_FITS_File))
			return 1;
		if (fitswriter_colorimage_write_test<int>(verbose,outstream,
				test_name,Input_2D_Color_Jpg_File,Output_2D_Color_FITS_File))
			return 1;
		if (fitswriter_colorimage_write_test<double>(verbose,outstream,
				test_name,Input_2D_Color_Jpg_File,Output_2D_Color_FITS_File))
			return 1;
	}

	//------------------------------
	//3D write function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - 3D write function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//3D write function
	{
		if (fitswriter_colormovie_write_test<float>(verbose,outstream,test_name,
				Input_ColorMovie_File,Output_3D_FITS_File))
			return 1;
		if (fitswriter_colormovie_write_test<int>(verbose,outstream,test_name,
				Input_ColorMovie_File,Output_3D_FITS_File))
			return 1;
		if (fitswriter_colormovie_write_test<double>(verbose,outstream,test_name,
				Input_ColorMovie_File,Output_3D_FITS_File))
			return 1;
	}
	return 0;
}
