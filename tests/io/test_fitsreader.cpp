/*!
 ** \file test_fitsreader.cpp
 ** \brief %FITSReader unit tests
 ** \date 2013/09/04
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */
#include <iostream>
#include <iomanip>
#include <sstream>
#include "io_tools.hpp"
#include "Volume.hpp"
#include "dynamic.hpp"
#include "AvReader.hpp"
#include "AvWriter.hpp"
#include "FITSReader.hpp"


/*!
 * \brief %FITSReader read method testing  : set 3D input files to video output file.
 * \param verbose true if a verbose mode is asking.
 * \param outstream output ostream for verbose mode.
 * \param parent_test_name name of the calling test function for verbose mode.
 * \param in_mono_signal name of the input mono signal fits file.
 * \param size of the signal
 */
template<typename test_type>
int fitsreader_read_test(bool verbose, std::ostream & outstream,
		const std::string & parent_test_name, std::string input_file, std::string output_base, std::string ext){

	slip::FITSReader<slip::Volume<test_type>,test_type,1,5,3> reader(input_file);
	for(int i=0; i<5; ++i){
		slip::Volume<test_type> vol;
		reader.read(vol);
		slip::range_fun_interab<test_type,test_type> funf(vol.min(),vol.max(),0,255);
		slip::change_dynamic(vol.begin(),vol.end(),vol.begin(),funf);
		AVRational t_base = {1,25};
		std::ostringstream oss;
		oss << i;
		std::string out_file_name = output_base + oss.str() + ext;
		slip::AvWriter<slip::Volume<test_type>,test_type,1> writer(out_file_name,
				vol.cols(),vol.rows(),vol.slices(),400000,t_base);
		writer.write(vol);
	}
	return 0;
}

/*!
 ** \brief usage :
 **  cmd.exe args
 **  ex : ./test_fitsreader my_arg
 */
void usage(const char *name) {
	std::cout << "usage :\n\n";
	std::cout << "\t " << name << " verbose(1/0) Input_3D_FITSFile OutputVideoFile\n\n";
}

int main(int argc, char *argv[])
{
	if (argc < 4) {
		usage(*argv);
		return 1;
	}
	std::string verb_str(argv[1]);
	bool verbose = false;
	if(verb_str == "1")
		verbose = true;
	std::string input_file(argv[2]);
	std::string output_video_file(argv[3]);
	std::string test_name(*argv);
	std::ostream & outstream = std::cout;
	if (verbose)
		CCfits::FITS::setVerboseMode(true);
	//------------------------------
	//Read function
	//------------------------------
	if(verbose){
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
		outstream <<  std::setfill(' ') << std::setw(20) << ' ';
		outstream << test_name << " - Read function" << std::endl;
		outstream <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
	}
	//Read function
	{
		std::string ext;
		std::string output_base;
		slip::split_extension(output_video_file,output_base,ext);
		if (fitsreader_read_test<float>(verbose,outstream,
				test_name,input_file,output_base,ext))
			return 1;
	}

	return 0;
}
