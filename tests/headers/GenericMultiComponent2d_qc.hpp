/*!
 ** \file GenericMultiComponent2d_qc.hpp
 ** \date 2013/07/19
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief Defining properties test functions that could be tested for %GenericMultiComponent2d class
 ** \note If QuickCheck++ is available, QuickCheck properties are also defined.
 */

#ifndef GENRICMULBLOCKICOMPONENT2D_QC_HPP_
#define GENRICMULBLOCKICOMPONENT2D_QC_HPP_

#include <limits>
#include <functional>
#include <vector>

#include "GenericMultiComponent2d.hpp"

namespace slip
{
template <class BLOCK>
class GenericMultiComponent2d;
}//namespace slip

/*!
 * \brief comparing two %GenericMultiComponent2d and return true if they are almost equal
 * \param A1 first %GenericMultiComponent2d to compare
 * \param A2 second %GenericMultiComponent2d to compare
 * \return true if equal
 */
template <class BLOCK>
bool are_genericmulticomponent2d_almost_equal(const slip::GenericMultiComponent2d<BLOCK> & A1, const slip::GenericMultiComponent2d<BLOCK> & A2) {
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_iterator const_it;
	const_it i1 = A1.begin(), i1end = A1.end();
	const_it i2 = A2.begin(), i2end = A2.end();
	typedef typename BLOCK::value_type T;
	while ((i1 != i1end) && (i2 != i2end)) {
		BLOCK diff;
		std::transform(i1->begin(),i1->end(),i2->begin(),diff.begin(),std::minus<T>());
		double norm = static_cast<double>(std::inner_product(diff.begin(),diff.end(),
				diff.begin(),static_cast<T>(0)));
		if (norm > 1e-5) {
			return 0;
		}
	}
	return 1;
}

/*!
 * \brief first constructor property definition function
 * \param d1 first dimension
 * \param d2 second dimension
 * \return true if the property is verified
 */
template<typename size_type, typename BLOCK>
bool def_genericmulticomponent2d_constructor_property1(const size_type & d1, const size_type & d2){
	slip::GenericMultiComponent2d<BLOCK> xt(d1,d2);
	return !(xt.empty());
}

/*!
 * \brief second constructor property definition function
 * \param d1 first dimension
 * \param d2 second dimension
 * \param val initial value
 * \return true if the property is verified
 */
template<typename size_type, typename BLOCK>
bool def_genericmulticomponent2d_constructor_property2(const size_type & d1, const size_type & d2
		, const BLOCK & val){
	slip::GenericMultiComponent2d<BLOCK> x2(d1,d2,val);
	typename slip::GenericMultiComponent2d<BLOCK>::iterator itx2 = std::find_if(x2.begin(), x2.end(),
			std::bind2nd(std::not_equal_to<BLOCK>(), val));
	return (itx2 == x2.end());
}

/*!
 * \brief third constructor property definition function
 * \param d1 first dimension
 * \param d2 second dimension
 * \param val initial array of BLOCK values
 * \return true if the property is verified
 */
template<typename size_type, typename BLOCK, typename T>
bool def_genericmulticomponent2d_constructor_property3(const size_type & d1, const size_type & d2,
		const std::vector<T> & val){
	bool res = true;
	size_type dim = d1*d2;
	size_type b_size = static_cast<size_type>(BLOCK::SIZE);
	BLOCK * array = new BLOCK[dim];
	for(size_type i=0; i<dim; i++){
		for(size_type j=0; j<b_size;++j){
			(array[i])[j] = val[b_size*i + j];
		}
	}
	slip::GenericMultiComponent2d<BLOCK> x1(d1,d2,array);
	typedef typename slip::GenericMultiComponent2d<BLOCK>::iterator iter;
	std::pair<iter,BLOCK*> pair_itx1;
	pair_itx1 = std::mismatch(x1.begin(), x1.end(), array);
	res = res && (pair_itx1.first == x1.end());

	std::vector<BLOCK> vec3(dim);
	std::copy(array,array+dim,vec3.begin());
	slip::GenericMultiComponent2d<BLOCK> x3(d1,d2,vec3.begin(),vec3.end());
	std::pair<iter,iter> pair_itx3;
	pair_itx3 = std::mismatch(x3.begin(), x3.end(), x1.begin());
	res = res && (pair_itx3.first == x3.end());

	slip::GenericMultiComponent2d<BLOCK> x5(x3);
	std::pair<iter,iter> pair_itx5;
	pair_itx5 = std::mismatch(x5.begin(), x5.end(), x3.begin());
	res = res && (pair_itx5.first == x5.end());

	BLOCK inc,zer;
	for(size_type j=0; j<b_size;++j){
		inc[j] = T(1);
		zer[j] = T(0);
	}
	*(x5.begin()) = *(x3.begin()) + inc;
	if (*(x5.begin()) == *(x3.begin()))
		*(x5.begin()) = zer;
	std::pair<iter,iter> pair_itx6;
	pair_itx6 = std::mismatch(x5.begin(), x5.end(), x3.begin());
	res = res && (pair_itx6.first == x5.begin());

	T * array_val = new T[val.size()];
	std::copy(val.begin(),val.end(),array_val);
	slip::GenericMultiComponent2d<BLOCK> x7(d1,d2,array_val);
	std::pair<iter,iter> pair_itx7;
	pair_itx7 = std::mismatch(x7.begin(), x7.end(), x1.begin());
	res = res && (pair_itx7.first == x7.end());
	if(!res)
	{
		std::cerr << x1;
		std::cerr << std::endl << " != " << std::endl << x7;
		std::cerr << std::endl;
	}
	delete[] array_val;
	delete[] array;
	return res;
}

/*!
 * \brief third fill and assign properties definition function
 * \param V input %GenericMultiComponent2d
 * \return true if the properties are verified
 */
template<typename BLOCK>
bool def_genericmulticomponent2d_assign_fill_property
(const slip::GenericMultiComponent2d<BLOCK> & V){
	bool res = true;
	typedef typename BLOCK::value_type T;
	std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
	slip::GenericMultiComponent2d<BLOCK> A;
	A = V;
	res = res && (A==V);
	BLOCK val;
	for(std::size_t j=0; j<b_size;++j){
		val[j] = T(1);
	}
	A.fill(val);
	slip::GenericMultiComponent2d<BLOCK> A2(V);
	A2 = val;
	res = res && (A==A2);

	std::size_t dim = V.dim1()*V.dim2();
	BLOCK * array = new BLOCK[dim];
	std::fill(array,array+dim,val);
	A.fill(array,array+dim);
	typedef typename slip::GenericMultiComponent2d<BLOCK>::iterator iter;
	std::pair<iter,BLOCK*> pair_itA;
	pair_itA = std::mismatch(A.begin(), A.end(), array);
	res = res && (pair_itA.first == A.end());
	delete[] array;
	return res;
}


/*!
 * \brief resize property definition function
 * \param V input %GenericMultiComponent2d
 * \param d1 new first dimension
 * \param d2 new second dimension
 * \return true if the property is verified
 */
template<typename BLOCK>
bool def_genericmulticomponent2d_resize_property(const slip::GenericMultiComponent2d<BLOCK>& V
		, const std::size_t & d1, const std::size_t & d2){
	typedef typename BLOCK::value_type T;
	std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
	slip::GenericMultiComponent2d<BLOCK> W(V);
	W.resize(d1,d2);
	bool res = ((W.dim1() == d1) && (W.rows() == d1) &&
			(W.dim2() == d2) && (W.cols() == d2) && (W.columns() == d2) &&
			(W.size() == d1*d2) && b_size == slip::GenericMultiComponent2d<BLOCK>::COMPONENTS
			&& 2 == slip::GenericMultiComponent2d<BLOCK>::DIM);

	W.resize(V.dim1(),V.dim2());
	for(std::size_t i=0; i<W.dim1(); ++i)
		for(std::size_t j=0; j<W.dim2(); ++j){
			for(std::size_t c=0; c<b_size;++c){
				W[i][j][c] = V[i][j][c];
			}
		}
	res = res && (W == V);

	W.resize(V.dim2(),V.dim1());
	for(std::size_t i=0; i<W.dim1(); ++i)
		for(std::size_t j=0; j<W.dim2(); ++j){
			for(std::size_t c=0; c<b_size;++c){
				W(i,j)[c] = V(j,i)[c];
			}
			res = res && (W(i,j) == V(j,i));
		}

	return res;
}

/*!
 * \brief swap method property and comparison operator properties function
 * \param V1 %GenericMultiComponent2d
 * \param V2 %GenericMultiComponent2d
 * \return true if the properties are verified
 */
template<typename BLOCK>
bool def_genericmulticomponent2d_swap_comparison_property
(const slip::GenericMultiComponent2d<BLOCK>& V1, const slip::GenericMultiComponent2d<BLOCK>& V2){
	bool res = true;
	typedef typename BLOCK::value_type T;
	std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
	slip::GenericMultiComponent2d<BLOCK> V1t(V1);
	slip::GenericMultiComponent2d<BLOCK> V2t(V2);
	V1t.swap(V2t);
	V1t.swap(V2t);
	res = res && (V1 == V1t) && (V2 == V2t);
	BLOCK val;
	for(std::size_t j=0; j<b_size;++j){
		val[j] = T(1);
	}
	V1t.fill(val);
	std::transform(V1t.begin(),V1t.end(),V1.begin(),V1t.begin(),std::plus<BLOCK>());
	res = res && (V1 != V1t);
	return res;
}

/*!
 * \brief external 1d iterators properties function
 * \param CV input %GenericMultiComponent2d
 * \return true if the property is verified
 */
template<typename BLOCK>
bool def_genericmulticomponent2d_iterators1d_property
(const slip::GenericMultiComponent2d<BLOCK>& VC){
	bool res = true;
	int step = 1;
	slip::GenericMultiComponent2d<BLOCK> V(VC);
	std::size_t d1 = V.dim1();
	std::size_t d2 = V.dim2();


	//global one dimensional iterators

	typedef typename slip::GenericMultiComponent2d<BLOCK>::iterator iter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_iterator const_iter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::reverse_iterator riter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_reverse_iterator const_riter;

	std::pair<iter,const_iter> pair_it1;
	pair_it1 = std::mismatch(V.begin(), V.end(), VC.begin());
	res = res && (pair_it1.first == V.end());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_iter,iter> pair_it2;
	pair_it2 = std::mismatch(VC.begin(), VC.end(), V.begin());
	res = res && (pair_it2.first == VC.end());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	std::pair<riter,const_riter> pair_it3;
	pair_it3 = std::mismatch(V.rbegin(), V.rend(), VC.rbegin());
	res = res && (pair_it3.first == V.rend());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_riter,riter> pair_it4;
	pair_it4 = std::mismatch(VC.rbegin(), VC.rend(), V.rbegin());
	res = res && (pair_it4.first == VC.rend());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	//row and col iterator

	typedef typename slip::GenericMultiComponent2d<BLOCK>::row_iterator row_iter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_row_iterator const_row_iter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::col_iterator col_iter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_col_iterator const_col_iter;

	typedef typename slip::GenericMultiComponent2d<BLOCK>::reverse_row_iterator row_riter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_reverse_row_iterator const_row_riter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::reverse_col_iterator col_riter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_reverse_col_iterator const_col_riter;


	if(d1>0 && d2>0){
		std::pair<row_iter,const_row_iter> pair_it13;
		pair_it13 = std::mismatch(V.row_begin(d1-1), V.row_end(d1-1), VC.row_begin(d1-1));
		res = res && (pair_it13.first == V.row_end(d1-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_row_iter,row_iter> pair_it14;
		pair_it14 = std::mismatch(VC.row_begin(d1-1), VC.row_end(d1-1), V.row_begin(d1-1));
		res = res && (pair_it14.first == VC.row_end(d1-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		std::pair<row_riter,const_row_riter> pair_it15;
		pair_it15 = std::mismatch(V.row_rbegin(d1-1), V.row_rend(d1-1), VC.row_rbegin(d1-1));
		res = res && (pair_it15.first == V.row_rend(d1-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;//15
		}
		step++;
		std::pair<const_row_riter,row_riter> pair_it16;
		pair_it16 = std::mismatch(VC.row_rbegin(d1-1), VC.row_rend(d1-1), V.row_rbegin(d1-1));
		res = res && (pair_it16.first == VC.row_rend(d1-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		std::pair<col_iter,const_col_iter> pair_it17;
		pair_it17 = std::mismatch(V.col_begin(d2-1), V.col_end(d2-1), VC.col_begin(d2-1));
		res = res && (pair_it17.first == V.col_end(d2-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_col_iter,col_iter> pair_it18;
		pair_it18 = std::mismatch(VC.col_begin(d2-1), VC.col_end(d2-1), V.col_begin(d2-1));
		res = res && (pair_it18.first == VC.col_end(d2-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		std::pair<col_riter,const_col_riter> pair_it19;
		pair_it19 = std::mismatch(V.col_rbegin(d2-1), V.col_rend(d2-1), VC.col_rbegin(d2-1));
		res = res && (pair_it19.first == V.col_rend(d2-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_col_riter,col_riter> pair_it20;
		pair_it20 = std::mismatch(VC.col_rbegin(d2-1), VC.col_rend(d2-1), V.col_rbegin(d2-1));
		res = res && (pair_it20.first == VC.col_rend(d2-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;//20
		}
		step++;
	}


	typedef typename slip::GenericMultiComponent2d<BLOCK>::row_range_iterator row_range_iter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_row_range_iterator const_row_range_iter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::col_range_iterator col_range_iter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_col_range_iterator const_col_range_iter;

	typedef typename slip::GenericMultiComponent2d<BLOCK>::reverse_row_range_iterator row_range_riter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_reverse_row_range_iterator const_row_range_riter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::reverse_col_range_iterator col_range_riter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_reverse_col_range_iterator const_col_range_riter;

	if(d1>2 && d2>2){
		slip::Range<int> row_range(0,d2-1,2);
		std::pair<row_range_iter,const_row_range_iter> pair_it13;
		pair_it13 = std::mismatch(V.row_begin(d1-1,row_range), V.row_end(d1-1,row_range),
				VC.row_begin(d1-1,row_range));
		res = res && (pair_it13.first == V.row_end(d1-1,row_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.row_begin(d1-1,row_range), V.row_end(d1-1,row_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
		std::pair<const_row_range_iter,row_range_iter> pair_it14;
		pair_it14 = std::mismatch(VC.row_begin(d1-1,row_range), VC.row_end(d1-1,row_range),
				V.row_begin(d1-1,row_range));
		res = res && (pair_it14.first == VC.row_end(d1-1,row_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;//30
			std::copy(VC.row_begin(d1-1,row_range), VC.row_end(d1-1,row_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;

		std::pair<row_range_riter,const_row_range_riter> pair_it15;
		pair_it15 = std::mismatch(V.row_rbegin(d1-1,row_range), V.row_rend(d1-1,row_range),
				VC.row_rbegin(d1-1,row_range));
		res = res && (pair_it15.first == V.row_rend(d1-1,row_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.row_rbegin(d1-1,row_range), V.row_rend(d1-1,row_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
		std::pair<const_row_range_riter,row_range_riter> pair_it16;
		pair_it16 = std::mismatch(VC.row_rbegin(d1-1,row_range), VC.row_rend(d1-1,row_range),
				V.row_rbegin(d1-1,row_range));
		res = res && (pair_it16.first == VC.row_rend(d1-1,row_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(VC.row_rbegin(d1-1,row_range), VC.row_rend(d1-1,row_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;

		slip::Range<int> col_range(0,d1-1,2);
		std::pair<col_range_iter,const_col_range_iter> pair_it17;
		pair_it17 = std::mismatch(V.col_begin(d2-1,col_range), V.col_end(d2-1,col_range),
				VC.col_begin(d2-1,col_range));
		res = res && (pair_it17.first == V.col_end(d2-1,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.col_begin(d2-1,col_range), V.col_end(d2-1,col_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
		std::pair<const_col_range_iter,col_range_iter> pair_it18;
		pair_it18 = std::mismatch(VC.col_begin(d2-1,col_range), VC.col_end(d2-1,col_range),
				V.col_begin(d2-1,col_range));
		res = res && (pair_it18.first == VC.col_end(d2-1,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(VC.col_begin(d2-1,col_range), VC.col_end(d2-1,col_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;

		std::pair<col_range_riter,const_col_range_riter> pair_it19;
		pair_it19 = std::mismatch(V.col_rbegin(d2-1,col_range), V.col_rend(d2-1,col_range),
				VC.col_rbegin(d2-1,col_range));
		res = res && (pair_it19.first == V.col_rend(d2-1,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;//35
			std::copy(V.col_rbegin(d2-1,col_range), V.col_rend(d2-1,col_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
		std::pair<const_col_range_riter,col_range_riter> pair_it20;
		pair_it20 = std::mismatch(VC.col_rbegin(d2-1,col_range), VC.col_rend(d2-1,col_range),
				V.col_rbegin(d2-1,col_range));
		res = res && (pair_it20.first == VC.col_rend(d2-1,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(VC.col_rbegin(d2-1,col_range), VC.col_rend(d2-1,col_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
	}

	return res;
}


/*!
 * \brief external 2d iterators properties function
 * \param CV input %GenericMultiComponent2d
 * \return true if the property is verified
 */
template<typename BLOCK>
bool def_genericmulticomponent2d_iterators2d_property
(const slip::GenericMultiComponent2d<BLOCK>& VC){
	bool res = true;
	int step = 1;
	slip::GenericMultiComponent2d<BLOCK> V(VC);

	std::size_t d1 = V.dim1();
	std::size_t d2 = V.dim2();


	slip::DPoint2d<int> d = V.bottom_right() - V.upper_left();
	res = res && (d == slip::DPoint2d<int>(d1,d2));
	if(!res){
		std::cerr << "error #" << step << std::endl;
		std::cerr << d << std::endl;
	}
	step++;

	//Global three dimensional iterators

	typedef typename slip::GenericMultiComponent2d<BLOCK>::iterator2d iter2d;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_iterator2d const_iter2d;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::reverse_iterator2d riter2d;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_reverse_iterator2d const_riter2d;

	typedef typename slip::GenericMultiComponent2d<BLOCK>::iterator iter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_iterator const_iter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::reverse_iterator riter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_reverse_iterator const_riter;


	std::pair<iter2d,const_iter> pair_it1;
	pair_it1 = std::mismatch(V.upper_left(), V.bottom_right(), VC.begin());
	res = res && (pair_it1.first == V.bottom_right());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_iter2d,iter> pair_it2;
	pair_it2 = std::mismatch(VC.upper_left(), VC.bottom_right(), V.begin());
	res = res && (pair_it2.first == VC.bottom_right());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	std::pair<riter2d,const_riter> pair_it3;
	pair_it3 = std::mismatch(V.rupper_left(), V.rbottom_right(), VC.rbegin());
	res = res && (pair_it3.first == V.rbottom_right());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_riter2d,riter> pair_it4;
	pair_it4 = std::mismatch(VC.rupper_left(), VC.rbottom_right(), V.rbegin());
	res = res && (pair_it4.first == VC.rbottom_right());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	//box three dimensional iterators

	slip::Box2d<int> box(1,1,d1-1,d2-1);
	std::pair<iter2d,const_iter2d> pair_it5;
	pair_it5 = std::mismatch(V.upper_left(box), V.bottom_right(box), VC.upper_left(box));
	res = res && (pair_it5.first == V.bottom_right(box));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_iter2d,iter2d> pair_it6;
	pair_it6 = std::mismatch(VC.upper_left(box), VC.bottom_right(box), V.upper_left(box));
	res = res && (pair_it6.first == VC.bottom_right(box));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	std::pair<riter2d,const_riter2d> pair_it7;
	pair_it7 = std::mismatch(V.rupper_left(box), V.rbottom_right(box), VC.rupper_left(box));
	res = res && (pair_it7.first == V.rbottom_right(box));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_riter2d,riter2d> pair_it8;
	pair_it8 = std::mismatch(VC.rupper_left(box), VC.rbottom_right(box), V.rupper_left(box));
	res = res && (pair_it8.first == VC.rbottom_right(box));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	//Range three dimensional iterators

	typedef typename slip::GenericMultiComponent2d<BLOCK>::iterator2d_range range_iter2d;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_iterator2d_range const_range_iter2d;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::reverse_iterator2d_range rrange_iter2d;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_reverse_iterator2d_range const_rrange_iter2d;

	slip::Range<int> row_range(0,d1-1,2);
	slip::Range<int> col_range(0,d2-1,2);

	std::pair<range_iter2d,const_range_iter2d> pair_it9;
	pair_it9 = std::mismatch(V.upper_left(row_range,col_range),
			V.bottom_right(row_range,col_range),
			VC.upper_left(row_range,col_range));
	res = res && (pair_it9.first == V.bottom_right(row_range,col_range));
	if(!res){
		std::cerr << "error #" << step << std::endl;
		std::copy(V.upper_left(row_range,col_range),
				V.bottom_right(row_range,col_range),
				std::ostream_iterator<BLOCK>(std::cerr," "));

		std::copy(VC.upper_left(row_range,col_range),
				VC.bottom_right(row_range,col_range),
				std::ostream_iterator<BLOCK>(std::cerr," "));
		std::cerr << std::endl;
	}
	step++;
	std::pair<const_range_iter2d,range_iter2d> pair_it10;
	pair_it10 = std::mismatch(VC.upper_left(row_range,col_range),
			VC.bottom_right(row_range,col_range),
			V.upper_left(row_range,col_range));
	res = res && (pair_it10.first == VC.bottom_right(row_range,col_range));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	std::pair<rrange_iter2d,const_rrange_iter2d> pair_it11;
	pair_it11 = std::mismatch(V.rupper_left(row_range,col_range),
			V.rbottom_right(row_range,col_range),
			VC.rupper_left(row_range,col_range));
	res = res && (pair_it11.first == V.rbottom_right(row_range,col_range));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_rrange_iter2d,rrange_iter2d> pair_it12;
	pair_it12 = std::mismatch(VC.rupper_left(row_range,col_range),
			VC.rbottom_right(row_range,col_range),
			V.rupper_left(row_range,col_range));
	res = res && (pair_it12.first == VC.rbottom_right(row_range,col_range));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	return res;
}

/*!
 * \brief external 2d component iterators properties function
 * \param CV input %GenericMultiComponent2d
 * \return true if the property is verified
 */
template<typename BLOCK>
bool def_genericmulticomponent2d_component_iterators2d_property
(const slip::GenericMultiComponent2d<BLOCK>& VC){
	bool res = true;
	int step = 1;
	slip::GenericMultiComponent2d<BLOCK> V(VC);

	typedef typename BLOCK::value_type T;
	std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);

	std::size_t d1 = V.dim1();
	std::size_t d2 = V.dim2();

	//global four dimensional component iterators

	typedef typename slip::GenericMultiComponent2d<BLOCK>::component_iterator2d iter2d;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_component_iterator2d const_iter2d;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::reverse_component_iterator2d riter2d;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_reverse_component_iterator2d const_riter2d;

	typedef typename slip::GenericMultiComponent2d<BLOCK>::component_iterator iter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_component_iterator const_iter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::reverse_component_iterator riter;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_reverse_component_iterator const_riter;

	for(std::size_t component=0; component < b_size; ++component){

		std::pair<iter2d,const_iter> pair_it1;
		pair_it1 = std::mismatch(V.upper_left(component), V.bottom_right(component), VC.begin(component));
		res = res && (pair_it1.first == V.bottom_right(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.upper_left(component),
					V.bottom_right(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
			std::copy(VC.begin(component),
					VC.end(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
		}
		step++;
		std::pair<const_iter2d,iter> pair_it2;
		pair_it2 = std::mismatch(VC.upper_left(component), VC.bottom_right(component), V.begin(component));
		res = res && (pair_it2.first == VC.bottom_right(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.upper_left(component),
					V.bottom_right(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
			std::copy(VC.begin(component),
					VC.end(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
		}
		step++;

		std::pair<riter2d,const_riter> pair_it3;
		pair_it3 = std::mismatch(V.rupper_left(component), V.rbottom_right(component), VC.rbegin(component));
		res = res && (pair_it3.first == V.rbottom_right(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::cerr << V;
			std::cerr << std::endl;
			std::copy(V.rupper_left(component),
					V.rbottom_right(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
			std::copy(VC.rbegin(component),
					VC.rend(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
		}
		step++;
		std::pair<const_riter2d,riter> pair_it4;
		pair_it4 = std::mismatch(VC.rupper_left(component), VC.rbottom_right(component), V.rbegin(component));
		res = res && (pair_it4.first == VC.rbottom_right(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::cerr << V;
			std::cerr << std::endl;
			std::copy(V.rupper_left(component),
					V.rbottom_right(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
			std::copy(VC.rbegin(component),
					VC.rend(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
		}
		step++;
	}

	//box three dimensional component iterators

	for(std::size_t component=0; component < b_size; ++component){

		slip::Box2d<int> box(1,1,d1-1,d2-1);
		std::pair<iter2d,const_iter2d> pair_it5;
		pair_it5 = std::mismatch(V.upper_left(component,box), V.bottom_right(component,box),
				VC.upper_left(component,box));
		res = res && (pair_it5.first == V.bottom_right(component,box));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_iter2d,iter2d> pair_it6;
		pair_it6 = std::mismatch(VC.upper_left(component,box), VC.bottom_right(component,box),
				V.upper_left(component,box));
		res = res && (pair_it6.first == VC.bottom_right(component,box));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		std::pair<riter2d,const_riter2d> pair_it7;
		pair_it7 = std::mismatch(V.rupper_left(component,box), V.rbottom_right(component,box),
				VC.rupper_left(component,box));
		res = res && (pair_it7.first == V.rbottom_right(component,box));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::cerr << V;
			std::cerr << std::endl;
			std::copy(V.rupper_left(component,box),
					V.rbottom_right(component,box),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
			std::copy(VC.rupper_left(component,box),
					VC.rbottom_right(component,box),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
		}
		step++;
		std::pair<const_riter2d,riter2d> pair_it8;
		pair_it8 = std::mismatch(VC.rupper_left(component,box), VC.rbottom_right(component,box),
				V.rupper_left(component,box));
		res = res && (pair_it8.first == VC.rbottom_right(component,box));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
	}

	//Range three dimensional component iterators

	typedef typename slip::GenericMultiComponent2d<BLOCK>::component_iterator2d_range range_iter2d;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_component_iterator2d_range const_range_iter2d;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::reverse_component_iterator2d_range rrange_iter2d;
	typedef typename slip::GenericMultiComponent2d<BLOCK>::const_reverse_component_iterator2d_range const_rrange_iter2d;

	slip::Range<int> row_range(0,d1-1,2);
	slip::Range<int> col_range(0,d2-1,2);

	for(std::size_t component=0; component < b_size; ++component){
		std::pair<range_iter2d,const_range_iter2d> pair_it9;
		pair_it9 = std::mismatch(V.upper_left(component,row_range,col_range),
				V.bottom_right(component,row_range,col_range),
				VC.upper_left(component,row_range,col_range));
		res = res && (pair_it9.first == V.bottom_right(component,row_range,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.upper_left(component,row_range,col_range),
					V.bottom_right(component,row_range,col_range),
					std::ostream_iterator<T>(std::cerr," "));

			std::copy(VC.upper_left(component,row_range,col_range),
					VC.bottom_right(component,row_range,col_range),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
		}
		step++;
		std::pair<const_range_iter2d,range_iter2d> pair_it10;
		pair_it10 = std::mismatch(VC.upper_left(component,row_range,col_range),
				VC.bottom_right(component,row_range,col_range),
				V.upper_left(component,row_range,col_range));
		res = res && (pair_it10.first == VC.bottom_right(component,row_range,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		std::pair<rrange_iter2d,const_rrange_iter2d> pair_it11;
		pair_it11 = std::mismatch(V.rupper_left(component,row_range,col_range),
				V.rbottom_right(component,row_range,col_range),
				VC.rupper_left(component,row_range,col_range));
		res = res && (pair_it11.first == V.rbottom_right(component,row_range,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_rrange_iter2d,rrange_iter2d> pair_it12;
		pair_it12 = std::mismatch(VC.rupper_left(component,row_range,col_range),
				VC.rbottom_right(component,row_range,col_range),
				V.rupper_left(component,row_range,col_range));
		res = res && (pair_it12.first == VC.rbottom_right(component,row_range,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
	}
	return res;
}


//If QuickCheck++ is available, the properties functions are encapsulated in quickcheck properties.
#ifdef HAVE_QCHECK

namespace quickcheck
{
/*!
 * \brief quickcheck generate function for a %block
 * \param n the quickcheck size hint
 * \param out the %block to generate
 */
template<typename T, std::size_t S>
void generate(size_t n, slip::block<T,S>& out);

/*!
 * \brief quickcheck generate function for %GenericMultiComponent2d
 * \param n the quickcheck size hint
 * \param out the %GenericMultiComponent2d to generate
 */
template<typename BLOCK>
void generate(size_t n, slip::GenericMultiComponent2d<BLOCK>& out);
}//namespace quickcheck

#include "Color_qc.hpp"

#include "quickcheck/quickcheck.hh"

namespace quickcheck
{

template<typename T, std::size_t S>
void generate(size_t n, slip::block<T,S>& out){
	for(std::size_t i=0; i<S; ++i){
		T a;
		generate(n,a);
		out[i] = a;
	}
}

template<typename BLOCK>
void generate(size_t n, slip::GenericMultiComponent2d<BLOCK>& out){
	unsigned d1,d2;
	d1 = generateInRange<unsigned>(1,20);
	d2 = generateInRange<unsigned>(1,20);
	typedef typename BLOCK::value_type T;
	std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);

	out.resize(d1,d2);
	for (size_t i = 0; i < d1; ++i)
		for (size_t j = 0; j < d2; ++j)
		{
			BLOCK val;
			for(std::size_t c=0; c<b_size;++c){
				T a;
				generate(n, a);
				val[c] = a;
			}
			out(i,j) = val;
		}
}

/*! \class ConstructorProperty1
 * \brief quickcheck property of %GenericMultiComponent2d that holds for the constructor with
 * three dimension parameters
 * \param size_type type of %GenericMultiComponent2d dimensions
 * \param BLOCK type of %GenericMultiComponent2d elements
 */
template<class BLOCK>
class GenericMultiComponent2dConstructorProperty1 : public Property<slip::GenericMultiComponent2d<BLOCK> > {
	/*!
	 * \brief property definition function
	 * \param x input %GenericMultiComponent2d
	 */
	bool holdsFor(const slip::GenericMultiComponent2d<BLOCK> &x){
		return def_genericmulticomponent2d_constructor_property1
				<typename slip::GenericMultiComponent2d<BLOCK>::size_type,BLOCK>
		(x.dim1(),x.dim2());
	}
};

/*! \class ConstructorProperty2
 * \brief quickcheck property of %GenericMultiComponent2d that holds for the constructors with
 * three dimension parameters and an initial value
 * \param size_type type of %GenericMultiComponent2d dimensions
 * \param BLOCK type of %GenericMultiComponent2d elements
 */
template<class BLOCK>
class GenericMultiComponent2dConstructorProperty2 : public Property<BLOCK,slip::GenericMultiComponent2d<BLOCK> > {
	/*!
	 * \brief property definition function
	 * \param val initial value
	 * \param x input %GenericMultiComponent2d
	 */
	bool holdsFor(const BLOCK &val, const slip::GenericMultiComponent2d<BLOCK> &x){
		return def_genericmulticomponent2d_constructor_property2
				<typename slip::GenericMultiComponent2d<BLOCK>::size_type,BLOCK>
		(x.dim1(),x.dim2(),val);
	}
};

/*! \class ConstructorProperty3
 * \brief quickcheck property of %GenericMultiComponent2d that holds for the constructors with
 * three dimension parameters and initial arrays of values.
 * \param size_type type of %GenericMultiComponent2d dimensions
 * \param BLOCK type of %GenericMultiComponent2d elements
 */
template<class BLOCK, typename T>
class GenericMultiComponent2dConstructorProperty3 : public Property<std::vector<T>,slip::GenericMultiComponent2d<BLOCK> > {
	/*!
	 * \brief property definition function
	 * \param val initial array of values
	 * \param x input %GenericMultiComponent2d
	 */
	bool holdsFor(const std::vector<T> & val, const slip::GenericMultiComponent2d<BLOCK> &x){
		return def_genericmulticomponent2d_constructor_property3
				<typename slip::GenericMultiComponent2d<BLOCK>::size_type,BLOCK,T>
		(x.dim1(),x.dim2(),val);
	}
	/*!
	 * \brief Inputs generation method
	 * \param n the quickcheck size hint
	 * \param initial array of values
	 * \param x input %GenericMultiComponent2d (it is not used by this test)
	 */
	void generateInput(size_t n, std::vector<T> & val, slip::GenericMultiComponent2d<BLOCK> &x) {
		std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
		generate(n, x);
		int dim = x.size() * b_size;
		for (int i = 0; i < dim; i++) {
			T a;
			generate(n-1, a);
			val.push_back(a);
		}
	}
};


/*!
 * \class AssignFillProperty
 * \brief quickcheck property of %GenericMultiComponent2d that holds for the assign and fill methods.
 * \param BLOCK type of %GenericMultiComponent2d elements
 */
template<class BLOCK>
class GenericMultiComponent2dAssignFillProperty : public Property<slip::GenericMultiComponent2d<BLOCK> > {
	bool holdsFor(const slip::GenericMultiComponent2d<BLOCK> &x){
		return def_genericmulticomponent2d_assign_fill_property<BLOCK>(x);
	}
};

/*!
 * \class ResizeProperty
 * \brief quickcheck property of %GenericMultiComponent2d that holds for the resize method.
 * \param BLOCK type of %GenericMultiComponent2d elements
 */
template<class BLOCK>
class GenericMultiComponent2dResizeProperty : public Property<slip::GenericMultiComponent2d<BLOCK> > {
	bool holdsFor(const slip::GenericMultiComponent2d<BLOCK> &x){
		unsigned d1,d2;
		d1 = generateInRange<unsigned>(1,20);
		d2 = generateInRange<unsigned>(1,20);
		return def_genericmulticomponent2d_resize_property<BLOCK>(x,d1,d2);
	}
};

/*!
 * \class SwapComparisonProperty
 * \brief quickcheck property of %GenericMultiComponent2d that holds for the swap and comparison methods.
 * \param BLOCK type of %GenericMultiComponent2d elements
 */
template<class BLOCK>
class GenericMultiComponent2dSwapComparisonProperty : public Property<slip::GenericMultiComponent2d<BLOCK>,
slip::GenericMultiComponent2d<BLOCK> > {
	bool holdsFor(const slip::GenericMultiComponent2d<BLOCK> &x,
			const slip::GenericMultiComponent2d<BLOCK> &y){
		return def_genericmulticomponent2d_swap_comparison_property<BLOCK>(x,y);
	}

	/*!
	 * \brief Inputs generation method
	 */
	void generateInput(size_t n, slip::GenericMultiComponent2d<BLOCK> &x,
			slip::GenericMultiComponent2d<BLOCK> &y) {
		typedef typename BLOCK::value_type T;
		std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
		generate(n, x);
		y.resize(x.dim1(),x.dim2());
		for (size_t i = 0; i < x.dim1(); ++i)
			for (size_t j = 0; j < x.dim2(); ++j)
			{
				BLOCK val;
				for(std::size_t c=0; c<b_size;++c){
					T a;
					generate(n-1, a);
					val[c] = a;
				}
				y(i,j) = val;
			}
	}
};

/*!
 * \class Iterators1dProperty
 * \brief quickcheck property of %GenericMultiComponent2d that holds for the iterators 1d definitions methods.
 * \param BLOCK type of %GenericMultiComponent2d elements
 */
template<class BLOCK>
class GenericMultiComponent2dIterators1dProperty : public Property<slip::GenericMultiComponent2d<BLOCK> > {
	bool holdsFor(const slip::GenericMultiComponent2d<BLOCK> &x){
		return def_genericmulticomponent2d_iterators1d_property<BLOCK>(x);
	}

	/*!
	 * \brief Inputs generation method
	 */
	void generateInput(size_t n, slip::GenericMultiComponent2d<BLOCK> &x) {
		typedef typename BLOCK::value_type T;
		std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
		unsigned d1,d2;
		d1 = generateInRange<unsigned>(3,20);
		d2 = generateInRange<unsigned>(3,20);

		x.resize(d1,d2);
		for (size_t i = 0; i < d1; ++i)
			for (size_t j = 0; j < d2; ++j)
			{
				BLOCK val;
				for(std::size_t c=0; c<b_size;++c){
					T a;
					generate(n-1, a);
					val[c] = a;
				}
				x(i,j) = val;
			}
	}
};

/*!
 * \class Iterators2dProperty
 * \brief quickcheck property of %GenericMultiComponent2d that holds for the iterators 2d related methods.
 * \param BLOCK type of %GenericMultiComponent2d elements
 */
template<class BLOCK>
class GenericMultiComponent2dIterators2dProperty : public Property<slip::GenericMultiComponent2d<BLOCK> > {
	bool holdsFor(const slip::GenericMultiComponent2d<BLOCK> &x){
		return def_genericmulticomponent2d_iterators2d_property<BLOCK>(x);
	}

	/*!
	 * \brief Inputs generation method
	 */
	void generateInput(size_t n, slip::GenericMultiComponent2d<BLOCK> &x) {
		typedef typename BLOCK::value_type T;
		std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
		unsigned d1,d2;
		d1 = generateInRange<unsigned>(3,20);
		d2 = generateInRange<unsigned>(3,20);

		x.resize(d1,d2);
		for (size_t i = 0; i < d1; ++i)
			for (size_t j = 0; j < d2; ++j)
			{
				BLOCK val;
				for(std::size_t c=0; c<b_size;++c){
					T a;
					generate(n-1, a);
					val[c] = a;
				}
				x(i,j) = val;
			}
	}
};

/*!
 * \class ComponentIterators2dProperty
 * \brief quickcheck property of %GenericMultiComponent2d that holds for the Component iterators 2d related methods.
 * \param BLOCK type of %GenericMultiComponent2d elements
 */
template<class BLOCK>
class GenericMultiComponent2dComponentIterators2dProperty :
		public Property<slip::GenericMultiComponent2d<BLOCK> > {
	bool holdsFor(const slip::GenericMultiComponent2d<BLOCK> &x){
		return def_genericmulticomponent2d_component_iterators2d_property<BLOCK>(x);
	}

	/*!
	 * \brief Inputs generation method
	 */
	void generateInput(size_t n, slip::GenericMultiComponent2d<BLOCK> &x) {
		typedef typename BLOCK::value_type T;
		std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
		unsigned d1,d2;
		d1 = generateInRange<unsigned>(3,20);
		d2 = generateInRange<unsigned>(3,20);

		x.resize(d1,d2);
		for (size_t i = 0; i < d1; ++i)
			for (size_t j = 0; j < d2; ++j)
			{
				BLOCK val;
				for(std::size_t c=0; c<b_size;++c){
					T a;
					generate(n-1, a);
					val[c] = a;
				}
				x(i,j) = val;
			}
	}
};

}//namespace quickcheck


#endif //HAVE_QCHECK

#endif /* GENRICMULBLOCKICOMPONENT2D_QC_HPP_ */
