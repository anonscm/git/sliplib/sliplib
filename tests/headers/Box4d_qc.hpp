/*!
 ** \file Box4d_qc.hpp
 ** \brief Defining properties test functions that could be tested for %Box4d class
 ** \date 2013/07/01
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef BOX4D_QC_HPP_
#define BOX4D_QC_HPP_

#include "Box4d.hpp"

namespace slip
{
  template <typename T>
  class Box4d;
}//namespace slip


#ifdef HAVE_QCHECK

namespace quickcheck
{
  /*!
   * \brief quickcheck generate function for %Box4d
   * \param n the quickcheck size hint
   * \param out the %Box4d to generate
   */
  template<typename T>
  void generate(size_t n, slip::Box4d<T>& out);
}//namespace quickcheck


#include "quickcheck/quickcheck.hh"

namespace quickcheck
{
  template<typename T>
  inline
  void generate(size_t n, slip::Box4d<T>& out)
  {
    T x11, x12, x13, x14;
    T x21, x22, x23, x24;
    generate(n, x11);
    generate(n, x12);
    generate(n, x13);
    generate(n, x14);
    generate(n, x21);
    generate(n, x22);
    generate(n, x23);
    generate(n, x24);
    out.set_coord(x11,x12,x13,x14,x21,x22,x23,x24);
  }

  /*!
   * \class Box4d Property 1
   * \brief quickcheck property 1 of %Box4d
   * \param T type of %Box4d elements
   */
  template<typename T>
  class Box4dProperty1 : public Property<slip::Box4d<T> > {
    /*!
     * \brief property definition function
     * \param b input %Box4d
     */
    bool holdsFor(const slip::Box4d<T> &b){
      slip::Box4d<T> b1(b);
      slip::Box4d<T> b2;
      b2 = b;
      return (b1 == b2);
    }
  };

  /*!
     * \class Box4d Property 2
     * \brief quickcheck property 2 of %Box4d
     * \param T type of %Box4d elements
     */
    template<typename T>
    class Box4dProperty2 : public Property<slip::Box4d<T> > {
      /*!
       * \brief property definition function
       * \param b input %Box4d
       */
      bool holdsFor(const slip::Box4d<T> &b){
        T hv = b.hypervolume();
        T w  = b.width();
        T h = b.height();
        T de = b.depth();
        T du = b.duration();
        T hvb = w * h * de * du;
        if(hv <= std::numeric_limits<T>::epsilon())
          return 1;
        int coef = 10;
        if (std::abs(1-hvb/hv) > coef*std::numeric_limits<T>::epsilon()){
            std::cerr << "Hypervolume = " << hv;
            std::cerr << std::endl;
            std::cerr << std::abs(1 - hvb/hv) << " > " <<
            		coef*std::numeric_limits<T>::epsilon();
            std::cerr << std::endl;
            return 0;
        }
        return 1;
      }
    };

}//namespace quickcheck

#endif //HAVE_QCHECK

#endif /* BOX4D_QC_HPP_ */
