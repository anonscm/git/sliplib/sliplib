/*!
 ** \file Vector4d_qc.hpp
 ** \date 2013/07/11
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief Defining properties test functions that could be tested for %Vector4d class
 ** \note If QuickCheck++ is available, QuickCheck properties are also defined.
 */

#ifndef VECTOR4D_QC_HPP_
#define VECTOR4D_QC_HPP_

#include <limits>
#include <functional>
#include <vector>

#include "Vector4d.hpp"
#include "complex_cast.hpp"

namespace slip
{
  template <class T>
  class Vector4d;
}//namespace slip

/*!
 * \brief comparing two %Vector4d and return true if they are almost equal
 * \param A1 first %Vector4d to compare
 * \param A2 second %Vector4d to compare
 * \return true if equal
 */
template <class T>
bool are_vector4d_almost_equal(const slip::Vector4d<T> & A1, const slip::Vector4d<T> & A2) {
  typedef typename slip::Vector4d<T>::const_iterator const_it;
  const_it i1 = A1.begin(), i1end = A1.end();
  const_it i2 = A2.begin(), i2end = A2.end();
  while ((i1 != i1end) && (i2 != i2end)) {
      T diff = *i1 - *i2;
      double norm = static_cast<double>(diff*diff);
      if (norm > 1e-5) {
          return 0;
      }
      i1++;
      i2++;
  }
  return 1;
}

/*!
 * \brief first constructor property definition function
 * \param x1 first component
 * \param x2 second component
 * \param x3 third component
 * \param x4 fourth component
 * \return true if the property is verified
 */
template<typename T>
bool def_vector4d_constructor_property1(const T & x1, const T & x2,
    const T & x3, const T & x4){
  slip::Vector4d<T> xt(x1,x2,x3,x4);
  return !(xt.empty());
}

/*!
 * \brief second constructor property definition function
 * \param val initial value
 * \return true if the property is verified
 */
template<typename T>
bool def_vector4d_constructor_property2(const T & val){
  slip::Vector4d<T> v2(val);
  typename slip::Vector4d<T>::iterator itv2 = std::find_if(v2.begin(), v2.end(),
      std::bind2nd(std::not_equal_to<T>(), val));
  return (itv2 == v2.end());
}

/*!
 * \brief third constructor property definition function
 * \param val initial vector of T values
 * \return true if the property is verified
 */
template<typename T>
bool def_vector4d_constructor_property3(const std::vector<T> & val){
  bool res = true;
  std::size_t dim = 4;
  T * vector = new T[dim];
  std::copy(val.begin(), val.end(),vector);
  slip::Vector4d<T> v1(vector);
  //std::cerr << v1 << std::endl;
  typedef typename slip::Vector4d<T>::iterator iter;
  std::pair<iter,T*> pair_itv1;
  pair_itv1 = std::mismatch(v1.begin(), v1.end(), vector);
  res = res && (pair_itv1.first == v1.end());
  if(!res){
      std::cerr << "error #1" << std::endl;
      std::copy(vector, vector + 4,
          std::ostream_iterator<T>(std::cout," "));
  }
  slip::Vector4d<T> v5(v1);
  std::pair<iter,iter> pair_itv5;
  pair_itv5 = std::mismatch(v5.begin(), v5.end(), v1.begin());
  res = res && (pair_itv5.first == v5.end());
  if(!res){
      std::cerr << "error #2" << std::endl;
      std::cerr << v5;
      std::cerr << std::endl;
  }

  *(v5.begin()) = *(v1.begin()) + T(1);
  if (*(v5.begin()) == *(v1.begin()))
    *(v5.begin()) = T(0);
  std::pair<iter,iter> pair_itv6;
  pair_itv6 = std::mismatch(v5.begin(), v5.end(), v1.begin());
  res = res && (pair_itv6.first == v5.begin());
  if(!res){
      std::cerr << "error #3" << std::endl;
      std::cerr << v5;
      std::cerr << std::endl;
  }

  delete[] vector;
  return res;
}

/*!
 * \brief third fill and assign properties definition function
 * \param V input %Vector4d
 * \return true if the properties are verified
 */
template<typename T>
bool def_vector4d_assign_fill_property(const slip::Vector4d<T> & V){
  bool res = true;
  slip::Vector4d<T> A;
  A = V;
  res = res && (A==V);
  T val(1);
  A.fill(val);
  slip::Vector4d<T> A2(V);
  A2 = val;
  res = res && (A==A2);

  std::size_t dim = 4;
  T * vector = new T[dim];
  std::fill(vector,vector+dim,val);
  A.fill(vector,vector+dim);
  typedef typename slip::Vector4d<T>::iterator iter;
  std::pair<iter,T*> pair_itA;
  pair_itA = std::mismatch(A.begin(), A.end(), vector);
  res = res && (pair_itA.first == A.end());
  delete[] vector;
  return res;
}

/*!
 * \brief name property definition function
 * \param V the %Vector4d we'd like to check the name
 * \return true if the property is verified
 */
template<typename T>
bool def_vector4d_name_property(const slip::Vector4d<T>& V){
  return (V.name() == "Vector4d");
}

/*!
 * \brief get/set property definition function
 * \param V input %Vector4d
 * \return true if the property is verified
 */
template<typename T>
bool def_vector4d_accessor_property(const slip::Vector4d<T>& V){
  int step = 1;
  slip::Vector4d<T> W(V);
  bool res = ((W.get_x1() == V.get_x1()) && (W.get_x() == V.get_x())  && (W.u() == V.u())
      && (W.get_x2() == V.get_x2()) && (W.get_y() == V.get_y())  && (W.v() == V.v())
      && (W.get_x3() == V.get_x3()) && (W.get_z() == V.get_z())  && (W.w() == V.w())
      && (W.get_x4() == V.get_x4()) && (W.get_t() == V.get_t())  && (W.t() == V.t())
      && (W.size() == 4) && (V.size() == 4));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "W = " << W;
      std::cerr << std::endl;
      std::cerr << "V = " << V;
      std::cerr << std::endl;
  }
  step++;
  W.set(V.get_x4(),V.get_x3(),V.get_x2(),V.get_x1());
  res = (res
      && (W.get_x1() == V.get_x4()) && (W.get_x() == V.get_t())  && (W.u() == V.t())
      && (W.get_x2() == V.get_x3()) && (W.get_y() == V.get_z())  && (W.v() == V.w())
      && (W.get_x3() == V.get_x2()) && (W.get_z() == V.get_y())  && (W.w() == V.v())
      && (W.get_x4() == V.get_x1()) && (W.get_t() == V.get_x())  && (W.t() == V.u())
      && (W.size() == 4) && (V.size() == 4));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "W = " << W;
      std::cerr << std::endl;
      std::cerr << "V = " << V;
      std::cerr << std::endl;
  }
  step++;
  W.set_x1(V.get_x1());
  W.set_x2(V.get_x2());
  W.set_x3(V.get_x3());
  W.set_x4(V.get_x4());
  res = (res
      && (W.get_x1() == V.get_x1()) && (W.get_x() == V.get_x())  && (W.u() == V.u())
      && (W.get_x2() == V.get_x2()) && (W.get_y() == V.get_y())  && (W.v() == V.v())
      && (W.get_x3() == V.get_x3()) && (W.get_z() == V.get_z())  && (W.w() == V.w())
      && (W.get_x4() == V.get_x4()) && (W.get_t() == V.get_t())  && (W.t() == V.t())
      && (W.size() == 4) && (V.size() == 4));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "W = " << W;
      std::cerr << std::endl;
      std::cerr << "V = " << V;
      std::cerr << std::endl;
  }
  step++;
  W.set_x(V.get_t());
  W.set_y(V.get_z());
  W.set_z(V.get_y());
  W.set_t(V.get_x());
  res = (res
      && (W.get_x1() == V.get_x4()) && (W.get_x() == V.get_t())  && (W.u() == V.t())
      && (W.get_x2() == V.get_x3()) && (W.get_y() == V.get_z())  && (W.v() == V.w())
      && (W.get_x3() == V.get_x2()) && (W.get_z() == V.get_y())  && (W.w() == V.v())
      && (W.get_x4() == V.get_x1()) && (W.get_t() == V.get_x())  && (W.t() == V.u())
      && (W.size() == 4) && (V.size() == 4));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "W = " << W;
      std::cerr << std::endl;
      std::cerr << "V = " << V;
      std::cerr << std::endl;
  }
  step++;
  W.u(V.u());
  W.v(V.v());
  W.w(V.w());
  W.t(V.t());
  res = (res
      && (W.get_x1() == V.get_x1()) && (W.get_x() == V.get_x())  && (W.u() == V.u())
      && (W.get_x2() == V.get_x2()) && (W.get_y() == V.get_y())  && (W.v() == V.v())
      && (W.get_x3() == V.get_x3()) && (W.get_z() == V.get_z())  && (W.w() == V.w())
      && (W.get_x4() == V.get_x4()) && (W.get_t() == V.get_t())  && (W.t() == V.t())
      && (W.size() == 4) && (V.size() == 4));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "W = " << W;
      std::cerr << std::endl;
      std::cerr << "V = " << V;
      std::cerr << std::endl;
  }
  step++;
  W[0] = V[3];
  W[1] = V[2];
  W[2] = V[1];
  W[3] = V[0];
  res = (res
      && (W.get_x1() == V.get_x4()) && (W.get_x() == V.get_t())  && (W.u() == V.t())
      && (W.get_x2() == V.get_x3()) && (W.get_y() == V.get_z())  && (W.v() == V.w())
      && (W.get_x3() == V.get_x2()) && (W.get_z() == V.get_y())  && (W.w() == V.v())
      && (W.get_x4() == V.get_x1()) && (W.get_t() == V.get_x())  && (W.t() == V.u())
      && (W.size() == 4) && (V.size() == 4));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "W = " << W;
      std::cerr << std::endl;
      std::cerr << "V = " << V;
      std::cerr << std::endl;
  }
  step++;
  W(0)= V(0);
  W(1)= V(1);
  W(2)= V(2);
  W(3)= V(3);
  res = (res
      && (W.get_x1() == V.get_x1()) && (W.get_x() == V.get_x())  && (W.u() == V.u())
      && (W.get_x2() == V.get_x2()) && (W.get_y() == V.get_y())  && (W.v() == V.v())
      && (W.get_x3() == V.get_x3()) && (W.get_z() == V.get_z())  && (W.w() == V.w())
      && (W.get_x4() == V.get_x4()) && (W.get_t() == V.get_t())  && (W.t() == V.t())
      && (W.size() == 4) && (V.size() == 4));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "W = " << W;
      std::cerr << std::endl;
      std::cerr << "V = " << V;
      std::cerr << std::endl;
  }
  step++;
  return res;
}

/*!
 * \brief swap method property and comparison operator properties function
 * \param V1 %Vector4d
 * \param V2 %Vector4d
 * \return true if the properties are verified
 */
template<typename T>
bool def_vector4d_swap_comparison_property(const slip::Vector4d<T>& V1,
    const slip::Vector4d<T>& V2){
  bool res = true;
  slip::Vector4d<T> V1t(V1);
  slip::Vector4d<T> V2t(V2);
  V1t.swap(V2t);
  V1t.swap(V2t);
  res = res && (V1 == V1t) && (V2 == V2t);
  V1t.fill(T(1));
  std::transform(V1t.begin(),V1t.end(),V1.begin(),V1t.begin(),std::plus<T>());
  res = res && (V1 != V1t);
  V1t.fill(V1.begin(),V1.end());
  res = res && (V1 == V1t);

  return res;
}

/*!
 * \brief external 1d iterators properties function
 * \param VC input %Vector4d
 * \return true if the property is verified
 */
template<typename T>
bool def_vector4d_iterators1d_property(const slip::Vector4d<T>& VC){
  bool res = true;
  int step = 1;
  slip::Vector4d<T> V(VC);


  typedef typename slip::Vector4d<T>::iterator iter;
  typedef typename slip::Vector4d<T>::const_iterator const_iter;
  typedef typename slip::Vector4d<T>::reverse_iterator riter;
  typedef typename slip::Vector4d<T>::const_reverse_iterator const_riter;

  std::pair<iter,const_iter> pair_it1;
  pair_it1 = std::mismatch(V.begin(), V.end(), VC.begin());
  res = res && (pair_it1.first == V.end());
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;
  std::pair<const_iter,iter> pair_it2;
  pair_it2 = std::mismatch(VC.begin(), VC.end(), V.begin());
  res = res && (pair_it2.first == VC.end());
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;

  std::pair<riter,const_riter> pair_it3;
  pair_it3 = std::mismatch(V.rbegin(), V.rend(), VC.rbegin());
  res = res && (pair_it3.first == V.rend());
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;
  std::pair<const_riter,riter> pair_it4;
  pair_it4 = std::mismatch(VC.rbegin(), VC.rend(), V.rbegin());
  res = res && (pair_it4.first == VC.rend());
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;


  typedef typename slip::Vector4d<T>::iterator_range range_iter;
  typedef typename slip::Vector4d<T>::const_iterator_range const_range_iter;
  typedef typename slip::Vector4d<T>::reverse_iterator_range range_riter;
  typedef typename slip::Vector4d<T>::const_reverse_iterator_range const_range_riter;

  slip::Range<int> range(0,3,2);
  std::pair<range_iter,const_range_iter> pair_it5;
  pair_it5 = std::mismatch(V.begin(range), V.end(range),
      VC.begin(range));
  res = res && (pair_it5.first == V.end(range));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::copy(V.begin(range), V.end(range),
          std::ostream_iterator<T>(std::cout," "));
  }
  step++;
  std::pair<const_range_iter,range_iter> pair_it6;
  pair_it6 = std::mismatch(VC.begin(range), VC.end(range),
      V.begin(range));
  res = res && (pair_it6.first == VC.end(range));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::copy(VC.begin(range), VC.end(range),
          std::ostream_iterator<T>(std::cout," "));
  }
  step++;

  std::pair<range_riter,const_range_riter> pair_it7;
  pair_it7 = std::mismatch(V.rbegin(range), V.rend(range),
      VC.rbegin(range));
  res = res && (pair_it7.first == V.rend(range));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::copy(V.rbegin(range), V.rend(range),
          std::ostream_iterator<T>(std::cout," "));
  }
  step++;
  std::pair<const_range_riter,range_riter> pair_it8;
  pair_it8 = std::mismatch(VC.rbegin(range), VC.rend(range),
      V.rbegin(range));
  res = res && (pair_it8.first == VC.rend(range));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::copy(VC.rbegin(range), VC.rend(range),
          std::ostream_iterator<T>(std::cout," "));
  }
  step++;

  return res;
}


/*!
 * \brief arithmetic operators property
 * \param VC input %Vector4d
 * \param val input T value
 * \return true if the property is verified
 */
template<typename T>
bool def_vector4d_arithmetic_property(const slip::Vector4d<T>& VC, const T& val){
  bool res = true;
  int step = 1;
  slip::Vector4d<T> V(VC);
  slip::Vector4d<T> W(val);
  T val2(val);
  V += val;
  V -= val;
  res = res && (are_vector4d_almost_equal(V,VC));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "V = " << V;
      std::cerr << std::endl;
      std::cerr << "VC = " << VC;
      std::cerr << std::endl;
  }
  step++;
  V += W;
  V -= W;
  res = res && (are_vector4d_almost_equal(V,VC));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "V = " << V;
      std::cerr << std::endl;
      std::cerr << "VC = " << VC;
      std::cerr << std::endl;
  }
  step++;
  if (val == T(0))
    {
      val2 += T(1);
      W += T(1);
    }

  V *= val2;
  V /= val2;
  V *= W;
  V /= W;

  res = res && (are_vector4d_almost_equal(V,VC));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "V = " << V;
      std::cerr << std::endl;
      std::cerr << "VC = " << VC;
      std::cerr << std::endl;
  }
  step++;
  return res;
}

/*!
 * \brief mathematical operators property
 * \param VC input %Vector4d
 * \param val input T value
 * \return true if the property is verified
 */
template<typename T>
bool def_vector4d_mathematical_property(const slip::Vector4d<T>& VC, const T& val){
  bool res = true;
  int step = 1;
  slip::Vector4d<T> V(VC);
  slip::Vector4d<T> W(val);
  T min = V.min();
  T max = V.max();
  T sum = V.sum();
  T min_c(V[0]),max_c(V[0]),sum_c(V[0]);
  for(int i=1; i<4; ++i){
      min_c = (V[i] < min_c ? V[i] : min_c);
      max_c = (V[i] > max_c ? V[i] : max_c);
      sum_c += V[i];
  }
  res = res && (min_c == min) && (max_c == max) && (sum_c == sum);
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "min = " << min << " != " << min_c;
      std::cerr << std::endl;
      std::cerr << "max = " << max << " != " << max_c;
      std::cerr << std::endl;
      std::cerr << "sum = " << sum << " != " << sum_c;
      std::cerr << std::endl;
  }
  step++;

  typedef typename slip::lin_alg_traits<T>::value_type Tn;
  Tn euc_norm = V.Euclidean_norm();
  Tn L2_norm = V.L2_norm();
  Tn L1_norm = V.L1_norm();
  Tn L22_norm = V.L22_norm();
  Tn inf_norm = V.infinite_norm();
  Tn L2_norm_c(Tn(0)),L1_norm_c(Tn(0)),L22_norm_c(Tn(0)),inf_norm_c(Tn(0));
  for(int i=0; i<4; ++i){
      L2_norm_c += V[i] * std::conj(V[i]);
      L1_norm_c += std::abs(V[i]);
      L22_norm_c += V[i] * V[i];
      inf_norm_c = (std::abs(V[i]) > inf_norm_c ? std::abs(V[i]) : inf_norm_c);
  }
  L2_norm_c = std::sqrt(L2_norm_c);

  res = res && (L2_norm_c == L2_norm) && (L2_norm_c == euc_norm) && (L1_norm_c == L1_norm)
                                          && (L22_norm_c == L22_norm) && (inf_norm_c == inf_norm);
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "L2_norm = " << L2_norm << " != " << L2_norm_c;
      std::cerr << std::endl;
      std::cerr << "euc_norm = " << euc_norm << " != " << L2_norm_c;
      std::cerr << std::endl;
      std::cerr << "L1_norm = " << L1_norm<< " != " << L1_norm_c;
      std::cerr << std::endl;
      std::cerr << "L22_norm = " << L22_norm << " != " << L22_norm_c;
      std::cerr << std::endl;
      std::cerr << "inf_norm = " << inf_norm << " != " << inf_norm_c;
      std::cerr << std::endl;
  }
  step++;
  T dot = V.template dot<T>(W);
  T dot_c(T(0));
  for(int i=0; i<4; ++i){
      dot_c += V[i] * W[i];
  }
  res = res && (dot_c == dot);
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "dot = " << dot << " != " << dot_c;
      std::cerr << std::endl;
  }
  step++;
  return res;
}

//If QuickCheck++ is available, the properties functions are encapsulated in quickcheck properties.
#ifdef HAVE_QCHECK

namespace quickcheck
{
  /*!
   * \brief quickcheck generate function for %Vector4d
   * \param n the quickcheck size hint
   * \param out the %Vector4d to generate
   */
  template<typename T>
  void generate(size_t n, slip::Vector4d<T>& out);
}//namespace quickcheck

#include "quickcheck/quickcheck.hh"

namespace quickcheck
{

  template<typename T>
  void generate(size_t n, slip::Vector4d<T>& out){
    for (size_t l = 0; l < 4; ++l)
      {
        T a;
        generate(n, a);
        out(l) = a;
      }
  }

  /*! \class ConstructorProperty1
   * \brief quickcheck property of %Vector4d
   * \param T type of %Vector4d elements
   */
  template<class T>
  class Vector4dConstructorProperty1 : public Property<T,T,T,T> {
    /*!
     * \brief property definition function
     * \param x1 first input component
     * \param x2 second input component
     * \param x3 third input component
     * \param x4 fourth input component
     */
    bool holdsFor(const T & x1, const T & x2,
        const T & x3, const T & x4){
      return def_vector4d_constructor_property1<T>(x1,x2,x3,x4);
    }
  };

  /*! \class ConstructorProperty2
   * \brief quickcheck property of %Vector4d
   * \param T type of %Vector4d elements
   */
  template<class T>
  class Vector4dConstructorProperty2 : public Property<T> {
    /*!
     * \brief property definition function
     * \param val initial value
     */
    bool holdsFor(const T &val){
      return def_vector4d_constructor_property2<T>(val);
    }
  };

  /*! \class ConstructorProperty3
   * \brief quickcheck property of %Vector4d
   * \param T type of %Vector4d elements
   */
  template<class T>
  class Vector4dConstructorProperty3 : public Property<std::vector<T> > {
    /*!
     * \brief property definition function
     * \param val initial vector of values
     * \param v input %Vector4d
     */
    bool holdsFor(const std::vector<T> & val){
      return def_vector4d_constructor_property3<T>(val);
    }

    /*!
     * \brief Inputs generation method
     */
    void generateInput(size_t n,std::vector<T> & val) {
      val.resize(4);
      for (size_t l = 0; l < 4; ++l)
        {
          T a;
          generate(n, a);
          val[l] = a;
        }
    }
  };

  /*! \class NameProperty
   * \brief quickcheck property of %Vector4d that holds for the name returned by the name() method.
   * \param T type of %Vector4d elements
   */
  template<class T>
  class Vector4dNameProperty : public Property<slip::Vector4d<T> > {
    bool holdsFor(const slip::Vector4d<T> &x){
      return def_vector4d_name_property<T>(x);
    }
  };

  /*!
   * \class AssignFillProperty
   * \brief quickcheck property of %Vector4d that holds for the assign and fill methods.
   * \param T type of %Vector4d elements
   */
  template<class T>
  class Vector4dAssignFillProperty : public Property<slip::Vector4d<T> > {
    bool holdsFor(const slip::Vector4d<T> &x){
      return def_vector4d_assign_fill_property<T>(x);
    }
  };

  /*!
   * \class AccessorProperty
   * \brief quickcheck property of %Vector4d that holds for the get/set methods.
   * \param T type of %Vector4d elements
   */
  template<class T>
  class Vector4dAccessorProperty : public Property<slip::Vector4d<T> > {
    bool holdsFor(const slip::Vector4d<T> &x){
      return def_vector4d_accessor_property<T>(x);
    }
  };

  /*!
   * \class SwapComparisonProperty
   * \brief quickcheck property of %Vector4d that holds for the swap and comparison methods.
   * \param T type of %Vector4d elements
   */
  template<class T>
  class Vector4dSwapComparisonProperty : public Property<slip::Vector4d<T>, slip::Vector4d<T> > {
    bool holdsFor(const slip::Vector4d<T> &x,const slip::Vector4d<T> &y){
      return def_vector4d_swap_comparison_property<T>(x,y);
    }
  };

  /*!
   * \class Iterators1dProperty
   * \brief quickcheck property of %Vector4d that holds for the iterators 1d definitions methods.
   * \param T type of %Vector4d elements
   */
  template<class T>
  class Vector4dIterators1dProperty : public Property<slip::Vector4d<T> > {
    bool holdsFor(const slip::Vector4d<T> &v){
      return def_vector4d_iterators1d_property<T>(v);
    }
  };

  /*!
   * \class ArithmeticProperty
   * \brief quickcheck property of %Vector4d that holds for arithmetic operators.
   * \param T type of %Vector4d elements
   */
  template<class T>
  class Vector4dArithmeticProperty : public Property<slip::Vector4d<T>,T> {
    bool holdsFor(const slip::Vector4d<T> &v, const T & val){
      return def_vector4d_arithmetic_property<T>(v,val);
    }
  };

  /*!
   * \class MathematicalProperty
   * \brief quickcheck property of %Vector4d that holds for arithmetic operators.
   * \param T type of %Vector4d elements
   */
  template<class T>
  class Vector4dMathematicalProperty : public Property<slip::Vector4d<T>,T> {
    bool holdsFor(const slip::Vector4d<T> &v, const T & val){
      return def_vector4d_mathematical_property<T>(v,val);
    }
  };

}//namespace quickcheck


#endif //HAVE_QCHECK

#endif /* VECTOR4D_QC_HPP_ */
