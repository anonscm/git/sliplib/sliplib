/*!
 ** \file Matrix4d_qc.hpp
 ** \date 2013/07/12
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief Defining properties test functions that could be tested for %Matrix4d class
 ** \note If QuickCheck++ is available, QuickCheck properties are also defined.
 */

#ifndef MATRIX4D_QC_HPP_
#define MATRIX4D_QC_HPP_

#include <limits>
#include <functional>
#include <vector>

#include "Matrix4d.hpp"

namespace slip
{
  template <class T>
  class Matrix4d;
}//namespace slip

/*!
 * \brief comparing two %Matrix4d and return true if they are almost equal
 * \param A1 first %Matrix4d to compare
 * \param A2 second %Matrix4d to compare
 * \return true if equal
 */
template <class T>
bool are_matrix4d_almost_equal(const slip::Matrix4d<T> & A1, const slip::Matrix4d<T> & A2) {
  typedef typename slip::Matrix4d<T>::const_iterator const_it;
  const_it i1 = A1.begin(), i1end = A1.end();
  const_it i2 = A2.begin(), i2end = A2.end();
  while ((i1 != i1end) && (i2 != i2end)) {
      T diff = *i1 - *i2;
      double norm = static_cast<double>(diff*diff);
      if (norm > 1e-5) {
          return 0;
      }
      i1++;
      i2++;
  }
  return 1;
}

/*!
 * \brief first constructor property definition function
 * \param d1 first dimension
 * \param d2 second dimension
 * \param d3 third dimension
 * \param d4 fourth dimension
 * \return true if the property is verified
 */
template<typename size_type, typename T>
bool def_matrix4d_constructor_property1(const size_type & d1, const size_type & d2,
    const size_type & d3, const size_type & d4){
  slip::Matrix4d<T> xt(d1,d2,d3,d4);
  return !(xt.empty());
}

/*!
 * \brief second constructor property definition function
 * \param d1 first dimension
 * \param d2 second dimension
 * \param d3 third dimension
 * \param d4 fourth dimension
 * \param val initial value
 * \return true if the property is verified
 */
template<typename size_type, typename T>
bool def_matrix4d_constructor_property2(const size_type & d1, const size_type & d2,
    const size_type & d3, const size_type & d4, const T & val){
  slip::Matrix4d<T> x2(d1,d2,d3,d4,val);
  typename slip::Matrix4d<T>::iterator itx2 = std::find_if(x2.begin(), x2.end(),
      std::bind2nd(std::not_equal_to<T>(), val));
  return (itx2 == x2.end());
}

/*!
 * \brief third constructor property definition function
 * \param d1 first dimension
 * \param d2 second dimension
 * \param d3 third dimension
 * \param d4 fourth dimension
 * \param val initial array of T values
 * \return true if the property is verified
 */
template<typename size_type, typename T>
bool def_matrix4d_constructor_property3(const size_type & d1, const size_type & d2,
    const size_type & d3, const size_type & d4, const std::vector<T> & val){
  bool res = true;
  size_type dim = d1*d2*d3*d4;
  T * array = new T[dim];
  std::fill(array,array+dim,val[0]);
  slip::Matrix4d<T> x1(d1,d2,d3,d4,array);
  typedef typename slip::Matrix4d<T>::iterator iter;
  std::pair<iter,T*> pair_itx1;
  pair_itx1 = std::mismatch(x1.begin(), x1.end(), array);
  res = res && (pair_itx1.first == x1.end());

  std::vector<T> vec3(dim);
  std::copy(array,array+dim,vec3.begin());
  slip::Matrix4d<T> x3(d1,d2,d3,d4,vec3.begin(),vec3.end());
  std::pair<iter,iter> pair_itx3;
  pair_itx3 = std::mismatch(x3.begin(), x3.end(), x1.begin());
  res = res && (pair_itx3.first == x3.end());

  slip::Matrix4d<T> x5(x3);
  std::pair<iter,iter> pair_itx5;
  pair_itx5 = std::mismatch(x5.begin(), x5.end(), x3.begin());
  res = res && (pair_itx5.first == x5.end());

  *(x5.begin()) = *(x3.begin()) + T(1);
  if (*(x5.begin()) == *(x3.begin()))
    *(x5.begin()) = T(0);
  std::pair<iter,iter> pair_itx6;
  pair_itx6 = std::mismatch(x5.begin(), x5.end(), x3.begin());
  res = res && (pair_itx6.first == x5.begin());

  delete[] array;
  return res;
}

/*!
 * \brief third fill and assign properties definition function
 * \param V input %Matrix4d
 * \return true if the properties are verified
 */
template<typename T>
bool def_matrix4d_assign_fill_property(const slip::Matrix4d<T> & V){
  bool res = true;
  slip::Matrix4d<T> A;
  A = V;
  res = res && (A==V);
  T val(1);
  A.fill(val);
  slip::Matrix4d<T> A2(V);
  A2 = val;
  res = res && (A==A2);

  std::size_t dim = V.dim1()*V.dim2()*V.dim3()*V.dim4();
  T * array = new T[dim];
  std::fill(array,array+dim,val);
  A.fill(array,array+dim);
  typedef typename slip::Matrix4d<T>::iterator iter;
  std::pair<iter,T*> pair_itA;
  pair_itA = std::mismatch(A.begin(), A.end(), array);
  res = res && (pair_itA.first == A.end());
  delete[] array;
  return res;
}

/*!
 * \brief name property definition function
 * \param V the %Matrix4d we'd like to check the name
 * \return true if the property is verified
 */
template<typename T>
bool def_matrix4d_name_property(const slip::Matrix4d<T>& V){
  return (V.name() == "Matrix4d");
}

/*!
 * \brief resize property definition function
 * \param V input %Matrix4d
 * \param d1 new first dimension
 * \param d2 new second dimension
 * \param d3 new third dimension
 * \param d4 new fourth dimension
 * \return true if the property is verified
 */
template<typename T>
bool def_matrix4d_resize_property(const slip::Matrix4d<T>& V, const std::size_t & d1, const std::size_t & d2,
    const std::size_t & d3, const std::size_t & d4){
  slip::Matrix4d<T> W(V);
  W.resize(d1,d2,d3,d4);
  bool res = ((W.dim1() == d1) && (W.slabs() == d1) && (W.dim2() == d2) && (W.slices() == d2) &&
      (W.dim3() == d3) && (W.rows() == d3) && (W.dim4() == d4) && (W.cols() == d4) && (W.columns() == d4) &&
      (W.size() == d1*d2*d3*d4) && slip::Matrix4d<T>::DIM == 4);

  W.resize(V.dim1(),V.dim2(),V.dim3(),V.dim4());
  for(std::size_t l=0; l<W.dim1(); ++l)
    for(std::size_t k=0; k<W.dim2(); ++k)
      for(std::size_t i=0; i<W.dim3(); ++i)
        for(std::size_t j=0; j<W.dim4(); ++j){
            W[l][k][i][j] = V[l][k][i][j];
        }
  res = res && (W == V);

  W.resize(V.dim4(),V.dim3(),V.dim2(),V.dim1());
  for(std::size_t l=0; l<W.dim1(); ++l)
    for(std::size_t k=0; k<W.dim2(); ++k)
      for(std::size_t i=0; i<W.dim3(); ++i)
        for(std::size_t j=0; j<W.dim4(); ++j){
            W(l,k,i,j) = V(j,i,k,l);
            res = res && (W(l,k,i,j) == V(j,i,k,l));
        }

  return res;
}

/*!
 * \brief swap method property and comparison operator properties function
 * \param V1 %Matrix4d
 * \param V2 %Matrix4d
 * \return true if the properties are verified
 */
template<typename T>
bool def_matrix4d_swap_comparison_property(const slip::Matrix4d<T>& V1,
    const slip::Matrix4d<T>& V2){
  bool res = true;
  slip::Matrix4d<T> V1t(V1);
  slip::Matrix4d<T> V2t(V2);
  V1t.swap(V2t);
  V1t.swap(V2t);
  res = res && (V1 == V1t) && (V2 == V2t);
  V1t.fill(T(1));
  std::transform(V1t.begin(),V1t.end(),V1.begin(),V1t.begin(),std::plus<T>());
  res = res && (V1 != V1t);
  return res;
}

/*!
 * \brief external 1d iterators properties function
 * \param CV input %Matrix4d
 * \return true if the property is verified
 */
template<typename T>
bool def_matrix4d_iterators1d_property(const slip::Matrix4d<T>& VC){
  bool res = true;
  int step = 1;
  slip::Matrix4d<T> V(VC);
  std::size_t d1 = V.dim1();
  std::size_t d2 = V.dim2();
  std::size_t d3 = V.dim3();
  std::size_t d4 = V.dim4();


  //global one dimensional iterators

  typedef typename slip::Matrix4d<T>::iterator iter;
  typedef typename slip::Matrix4d<T>::const_iterator const_iter;
  typedef typename slip::Matrix4d<T>::reverse_iterator riter;
  typedef typename slip::Matrix4d<T>::const_reverse_iterator const_riter;

  std::pair<iter,const_iter> pair_it1;
  pair_it1 = std::mismatch(V.begin(), V.end(), VC.begin());
  res = res && (pair_it1.first == V.end());
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;
  std::pair<const_iter,iter> pair_it2;
  pair_it2 = std::mismatch(VC.begin(), VC.end(), V.begin());
  res = res && (pair_it2.first == VC.end());
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;

  std::pair<riter,const_riter> pair_it3;
  pair_it3 = std::mismatch(V.rbegin(), V.rend(), VC.rbegin());
  res = res && (pair_it3.first == V.rend());
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;
  std::pair<const_riter,riter> pair_it4;
  pair_it4 = std::mismatch(VC.rbegin(), VC.rend(), V.rbegin());
  res = res && (pair_it4.first == VC.rend());
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;

  //slab, slice, row and col iterator

  typedef typename slip::Matrix4d<T>::slab_iterator slab_iter;
  typedef typename slip::Matrix4d<T>::const_slab_iterator const_slab_iter;
  typedef typename slip::Matrix4d<T>::slice_iterator slice_iter;
  typedef typename slip::Matrix4d<T>::const_slice_iterator const_slice_iter;
  typedef typename slip::Matrix4d<T>::row_iterator row_iter;
  typedef typename slip::Matrix4d<T>::const_row_iterator const_row_iter;
  typedef typename slip::Matrix4d<T>::col_iterator col_iter;
  typedef typename slip::Matrix4d<T>::const_col_iterator const_col_iter;

  typedef typename slip::Matrix4d<T>::reverse_slab_iterator slab_riter;
  typedef typename slip::Matrix4d<T>::const_reverse_slab_iterator const_slab_riter;
  typedef typename slip::Matrix4d<T>::reverse_slice_iterator slice_riter;
  typedef typename slip::Matrix4d<T>::const_reverse_slice_iterator const_slice_riter;
  typedef typename slip::Matrix4d<T>::reverse_row_iterator row_riter;
  typedef typename slip::Matrix4d<T>::const_reverse_row_iterator const_row_riter;
  typedef typename slip::Matrix4d<T>::reverse_col_iterator col_riter;
  typedef typename slip::Matrix4d<T>::const_reverse_col_iterator const_col_riter;


  if(d1>0 && d2>0 && d3>0 && d4>0){
      std::pair<slab_iter,const_slab_iter> pair_it5;
      pair_it5 = std::mismatch(V.slab_begin(d2-1,d3-1,d4-1), V.slab_end(d2-1,d3-1,d4-1), VC.slab_begin(d2-1,d3-1,d4-1));
      res = res && (pair_it5.first == V.slab_end(d2-1,d3-1,d4-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;//5
      }
      step++;
      std::pair<const_slab_iter,slab_iter> pair_it6;
      pair_it6 = std::mismatch(VC.slab_begin(d2-1,d3-1,d4-1), VC.slab_end(d2-1,d3-1,d4-1), V.slab_begin(d2-1,d3-1,d4-1));
      res = res && (pair_it6.first == VC.slab_end(d2-1,d3-1,d4-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;
      }
      step++;

      std::pair<slab_riter,const_slab_riter> pair_it7;
      pair_it7 = std::mismatch(V.slab_rbegin(d2-1,d3-1,d4-1), V.slab_rend(d2-1,d3-1,d4-1), VC.slab_rbegin(d2-1,d3-1,d4-1));
      res = res && (pair_it7.first == V.slab_rend(d2-1,d3-1,d4-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;
      }
      step++;
      std::pair<const_slab_riter,slab_riter> pair_it8;
      pair_it8 = std::mismatch(VC.slab_rbegin(d2-1,d3-1,d4-1), VC.slab_rend(d2-1,d3-1,d4-1), V.slab_rbegin(d2-1,d3-1,d4-1));
      res = res && (pair_it8.first == VC.slab_rend(d2-1,d3-1,d4-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;
      }
      step++;

      std::pair<slice_iter,const_slice_iter> pair_it9;
      pair_it9 = std::mismatch(V.slice_begin(d1-1,d3-1,d4-1), V.slice_end(d1-1,d3-1,d4-1), VC.slice_begin(d1-1,d3-1,d4-1));
      res = res && (pair_it9.first == V.slice_end(d1-1,d3-1,d4-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;
      }
      step++;
      std::pair<const_slice_iter,slice_iter> pair_it10;
      pair_it10 = std::mismatch(VC.slice_begin(d1-1,d3-1,d4-1), VC.slice_end(d1-1,d3-1,d4-1), V.slice_begin(d1-1,d3-1,d4-1));
      res = res && (pair_it10.first == VC.slice_end(d1-1,d3-1,d4-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;//10
      }
      step++;

      std::pair<slice_riter,const_slice_riter> pair_it11;
      pair_it11 = std::mismatch(V.slice_rbegin(d1-1,d3-1,d4-1), V.slice_rend(d1-1,d3-1,d4-1), VC.slice_rbegin(d1-1,d3-1,d4-1));
      res = res && (pair_it11.first == V.slice_rend(d1-1,d3-1,d4-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;
      }
      step++;
      std::pair<const_slice_riter,slice_riter> pair_it12;
      pair_it12 = std::mismatch(VC.slice_rbegin(d1-1,d3-1,d4-1), VC.slice_rend(d1-1,d3-1,d4-1), V.slice_rbegin(d1-1,d3-1,d4-1));
      res = res && (pair_it12.first == VC.slice_rend(d1-1,d3-1,d4-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;
      }
      step++;

      std::pair<row_iter,const_row_iter> pair_it13;
      pair_it13 = std::mismatch(V.row_begin(d1-1,d2-1,d3-1), V.row_end(d1-1,d2-1,d3-1), VC.row_begin(d1-1,d2-1,d3-1));
      res = res && (pair_it13.first == V.row_end(d1-1,d2-1,d3-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;
      }
      step++;
      std::pair<const_row_iter,row_iter> pair_it14;
      pair_it14 = std::mismatch(VC.row_begin(d1-1,d2-1,d3-1), VC.row_end(d1-1,d2-1,d3-1), V.row_begin(d1-1,d2-1,d3-1));
      res = res && (pair_it14.first == VC.row_end(d1-1,d2-1,d3-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;
      }
      step++;

      std::pair<row_riter,const_row_riter> pair_it15;
      pair_it15 = std::mismatch(V.row_rbegin(d1-1,d2-1,d3-1), V.row_rend(d1-1,d2-1,d3-1), VC.row_rbegin(d1-1,d2-1,d3-1));
      res = res && (pair_it15.first == V.row_rend(d1-1,d2-1,d3-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;//15
      }
      step++;
      std::pair<const_row_riter,row_riter> pair_it16;
      pair_it16 = std::mismatch(VC.row_rbegin(d1-1,d2-1,d3-1), VC.row_rend(d1-1,d2-1,d3-1), V.row_rbegin(d1-1,d2-1,d3-1));
      res = res && (pair_it16.first == VC.row_rend(d1-1,d2-1,d3-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;
      }
      step++;

      std::pair<col_iter,const_col_iter> pair_it17;
      pair_it17 = std::mismatch(V.col_begin(d1-1,d2-1,d4-1), V.col_end(d1-1,d2-1,d4-1), VC.col_begin(d1-1,d2-1,d4-1));
      res = res && (pair_it17.first == V.col_end(d1-1,d2-1,d4-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;
      }
      step++;
      std::pair<const_col_iter,col_iter> pair_it18;
      pair_it18 = std::mismatch(VC.col_begin(d1-1,d2-1,d4-1), VC.col_end(d1-1,d2-1,d4-1), V.col_begin(d1-1,d2-1,d4-1));
      res = res && (pair_it18.first == VC.col_end(d1-1,d2-1,d4-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;
      }
      step++;

      std::pair<col_riter,const_col_riter> pair_it19;
      pair_it19 = std::mismatch(V.col_rbegin(d1-1,d2-1,d4-1), V.col_rend(d1-1,d2-1,d4-1), VC.col_rbegin(d1-1,d2-1,d4-1));
      res = res && (pair_it19.first == V.col_rend(d1-1,d2-1,d4-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;
      }
      step++;
      std::pair<const_col_riter,col_riter> pair_it20;
      pair_it20 = std::mismatch(VC.col_rbegin(d1-1,d2-1,d4-1), VC.col_rend(d1-1,d2-1,d4-1), V.col_rbegin(d1-1,d2-1,d4-1));
      res = res && (pair_it20.first == VC.col_rend(d1-1,d2-1,d4-1));
      if(!res){
          std::cerr << "error #" << step << std::endl;//20
      }
      step++;
  }


  typedef typename slip::Matrix4d<T>::slab_range_iterator slab_range_iter;
  typedef typename slip::Matrix4d<T>::const_slab_range_iterator const_slab_range_iter;
  typedef typename slip::Matrix4d<T>::slice_range_iterator slice_range_iter;
  typedef typename slip::Matrix4d<T>::const_slice_range_iterator const_slice_range_iter;
  typedef typename slip::Matrix4d<T>::row_range_iterator row_range_iter;
  typedef typename slip::Matrix4d<T>::const_row_range_iterator const_row_range_iter;
  typedef typename slip::Matrix4d<T>::col_range_iterator col_range_iter;
  typedef typename slip::Matrix4d<T>::const_col_range_iterator const_col_range_iter;

  typedef typename slip::Matrix4d<T>::reverse_slab_range_iterator slab_range_riter;
  typedef typename slip::Matrix4d<T>::const_reverse_slab_range_iterator const_slab_range_riter;
  typedef typename slip::Matrix4d<T>::reverse_slice_range_iterator slice_range_riter;
  typedef typename slip::Matrix4d<T>::const_reverse_slice_range_iterator const_slice_range_riter;
  typedef typename slip::Matrix4d<T>::reverse_row_range_iterator row_range_riter;
  typedef typename slip::Matrix4d<T>::const_reverse_row_range_iterator const_row_range_riter;
  typedef typename slip::Matrix4d<T>::reverse_col_range_iterator col_range_riter;
  typedef typename slip::Matrix4d<T>::const_reverse_col_range_iterator const_col_range_riter;

  if(d1>2 && d2>2 && d3>2 && d4>2){
      slip::Range<int> slab_range(0,d1-1,2);
      std::pair<slab_range_iter,const_slab_range_iter> pair_it5;
      pair_it5 = std::mismatch(V.slab_begin(d2-1,d3-1,d4-1,slab_range), V.slab_end(d2-1,d3-1,d4-1,slab_range),
          VC.slab_begin(d2-1,d3-1,d4-1,slab_range));
      res = res && (pair_it5.first == V.slab_end(d2-1,d3-1,d4-1,slab_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;
          std::copy(V.slab_begin(d2-1,d3-1,d4-1,slab_range), V.slab_end(d2-1,d3-1,d4-1,slab_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;
      std::pair<const_slab_range_iter,slab_range_iter> pair_it6;
      pair_it6 = std::mismatch(VC.slab_begin(d2-1,d3-1,d4-1,slab_range), VC.slab_end(d2-1,d3-1,d4-1,slab_range),
          V.slab_begin(d2-1,d3-1,d4-1,slab_range));
      res = res && (pair_it6.first == VC.slab_end(d2-1,d3-1,d4-1,slab_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;
          std::copy(VC.slab_begin(d2-1,d3-1,d4-1,slab_range), VC.slab_end(d2-1,d3-1,d4-1,slab_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;

      std::pair<slab_range_riter,const_slab_range_riter> pair_it7;
      pair_it7 = std::mismatch(V.slab_rbegin(d2-1,d3-1,d4-1,slab_range), V.slab_rend(d2-1,d3-1,d4-1,slab_range),
          VC.slab_rbegin(d2-1,d3-1,d4-1,slab_range));
      res = res && (pair_it7.first == V.slab_rend(d2-1,d3-1,d4-1,slab_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;
          std::copy(V.slab_rbegin(d2-1,d3-1,d4-1,slab_range), V.slab_rend(d2-1,d3-1,d4-1,slab_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;
      std::pair<const_slab_range_riter,slab_range_riter> pair_it8;
      pair_it8 = std::mismatch(VC.slab_rbegin(d2-1,d3-1,d4-1,slab_range), VC.slab_rend(d2-1,d3-1,d4-1,slab_range),
          V.slab_rbegin(d2-1,d3-1,d4-1,slab_range));
      res = res && (pair_it8.first == VC.slab_rend(d2-1,d3-1,d4-1,slab_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;
          std::copy(VC.slab_rbegin(d2-1,d3-1,d4-1,slab_range), VC.slab_rend(d2-1,d3-1,d4-1,slab_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;

      slip::Range<int> slice_range(0,d2-1,2);
      std::pair<slice_range_iter,const_slice_range_iter> pair_it9;
      pair_it9 = std::mismatch(V.slice_begin(d1-1,d3-1,d4-1,slice_range), V.slice_end(d1-1,d3-1,d4-1,slice_range),
          VC.slice_begin(d1-1,d3-1,d4-1,slice_range));
      res = res && (pair_it9.first == V.slice_end(d1-1,d3-1,d4-1,slice_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;//25
          std::copy(V.slice_begin(d1-1,d3-1,d4-1,slice_range), V.slice_end(d1-1,d3-1,d4-1,slice_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;
      std::pair<const_slice_range_iter,slice_range_iter> pair_it10;
      pair_it10 = std::mismatch(VC.slice_begin(d1-1,d3-1,d4-1,slice_range), VC.slice_end(d1-1,d3-1,d4-1,slice_range),
          V.slice_begin(d1-1,d3-1,d4-1,slice_range));
      res = res && (pair_it10.first == VC.slice_end(d1-1,d3-1,d4-1,slice_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;
          std::copy(VC.slice_begin(d1-1,d3-1,d4-1,slice_range), VC.slice_end(d1-1,d3-1,d4-1,slice_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;

      std::pair<slice_range_riter,const_slice_range_riter> pair_it11;
      pair_it11 = std::mismatch(V.slice_rbegin(d1-1,d3-1,d4-1,slice_range), V.slice_rend(d1-1,d3-1,d4-1,slice_range),
          VC.slice_rbegin(d1-1,d3-1,d4-1,slice_range));
      res = res && (pair_it11.first == V.slice_rend(d1-1,d3-1,d4-1,slice_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;
          std::copy(V.slice_rbegin(d1-1,d3-1,d4-1,slice_range), V.slice_rend(d1-1,d3-1,d4-1,slice_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;
      std::pair<const_slice_range_riter,slice_range_riter> pair_it12;
      pair_it12 = std::mismatch(VC.slice_rbegin(d1-1,d3-1,d4-1,slice_range), VC.slice_rend(d1-1,d3-1,d4-1,slice_range),
          V.slice_rbegin(d1-1,d3-1,d4-1,slice_range));
      res = res && (pair_it12.first == VC.slice_rend(d1-1,d3-1,d4-1,slice_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;
          std::copy(VC.slice_rbegin(d1-1,d3-1,d4-1,slice_range), VC.slice_rend(d1-1,d3-1,d4-1,slice_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;

      slip::Range<int> row_range(0,d4-1,2);
      std::pair<row_range_iter,const_row_range_iter> pair_it13;
      pair_it13 = std::mismatch(V.row_begin(d1-1,d2-1,d3-1,row_range), V.row_end(d1-1,d2-1,d3-1,row_range),
          VC.row_begin(d1-1,d2-1,d3-1,row_range));
      res = res && (pair_it13.first == V.row_end(d1-1,d2-1,d3-1,row_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;
          std::copy(V.row_begin(d1-1,d2-1,d3-1,row_range), V.row_end(d1-1,d2-1,d3-1,row_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;
      std::pair<const_row_range_iter,row_range_iter> pair_it14;
      pair_it14 = std::mismatch(VC.row_begin(d1-1,d2-1,d3-1,row_range), VC.row_end(d1-1,d2-1,d3-1,row_range),
          V.row_begin(d1-1,d2-1,d3-1,row_range));
      res = res && (pair_it14.first == VC.row_end(d1-1,d2-1,d3-1,row_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;//30
          std::copy(VC.row_begin(d1-1,d2-1,d3-1,row_range), VC.row_end(d1-1,d2-1,d3-1,row_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;

      std::pair<row_range_riter,const_row_range_riter> pair_it15;
      pair_it15 = std::mismatch(V.row_rbegin(d1-1,d2-1,d3-1,row_range), V.row_rend(d1-1,d2-1,d3-1,row_range),
          VC.row_rbegin(d1-1,d2-1,d3-1,row_range));
      res = res && (pair_it15.first == V.row_rend(d1-1,d2-1,d3-1,row_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;
          std::copy(V.row_rbegin(d1-1,d2-1,d3-1,row_range), V.row_rend(d1-1,d2-1,d3-1,row_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;
      std::pair<const_row_range_riter,row_range_riter> pair_it16;
      pair_it16 = std::mismatch(VC.row_rbegin(d1-1,d2-1,d3-1,row_range), VC.row_rend(d1-1,d2-1,d3-1,row_range),
          V.row_rbegin(d1-1,d2-1,d3-1,row_range));
      res = res && (pair_it16.first == VC.row_rend(d1-1,d2-1,d3-1,row_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;
          std::copy(VC.row_rbegin(d1-1,d2-1,d3-1,row_range), VC.row_rend(d1-1,d2-1,d3-1,row_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;

      slip::Range<int> col_range(0,d3-1,2);
      std::pair<col_range_iter,const_col_range_iter> pair_it17;
      pair_it17 = std::mismatch(V.col_begin(d1-1,d2-1,d4-1,col_range), V.col_end(d1-1,d2-1,d4-1,col_range),
          VC.col_begin(d1-1,d2-1,d4-1,col_range));
      res = res && (pair_it17.first == V.col_end(d1-1,d2-1,d4-1,col_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;
          std::copy(V.col_begin(d1-1,d2-1,d4-1,col_range), V.col_end(d1-1,d2-1,d4-1,col_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;
      std::pair<const_col_range_iter,col_range_iter> pair_it18;
      pair_it18 = std::mismatch(VC.col_begin(d1-1,d2-1,d4-1,col_range), VC.col_end(d1-1,d2-1,d4-1,col_range),
          V.col_begin(d1-1,d2-1,d4-1,col_range));
      res = res && (pair_it18.first == VC.col_end(d1-1,d2-1,d4-1,col_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;
          std::copy(VC.col_begin(d1-1,d2-1,d4-1,col_range), VC.col_end(d1-1,d2-1,d4-1,col_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;

      std::pair<col_range_riter,const_col_range_riter> pair_it19;
      pair_it19 = std::mismatch(V.col_rbegin(d1-1,d2-1,d4-1,col_range), V.col_rend(d1-1,d2-1,d4-1,col_range),
          VC.col_rbegin(d1-1,d2-1,d4-1,col_range));
      res = res && (pair_it19.first == V.col_rend(d1-1,d2-1,d4-1,col_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;//35
          std::copy(V.col_rbegin(d1-1,d2-1,d4-1,col_range), V.col_rend(d1-1,d2-1,d4-1,col_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;
      std::pair<const_col_range_riter,col_range_riter> pair_it20;
      pair_it20 = std::mismatch(VC.col_rbegin(d1-1,d2-1,d4-1,col_range), VC.col_rend(d1-1,d2-1,d4-1,col_range),
          V.col_rbegin(d1-1,d2-1,d4-1,col_range));
      res = res && (pair_it20.first == VC.col_rend(d1-1,d2-1,d4-1,col_range));
      if(!res){
          std::cerr << "error #" << step << std::endl;
          std::copy(VC.col_rbegin(d1-1,d2-1,d4-1,col_range), VC.col_rend(d1-1,d2-1,d4-1,col_range),
              std::ostream_iterator<T>(std::cout," "));
      }
      step++;
  }

  return res;
}


/*!
 * \brief external 4d iterators properties function
 * \param CV input %Matrix4d
 * \return true if the property is verified
 */
template<typename T>
bool def_matrix4d_iterators4d_property(const slip::Matrix4d<T>& VC){
  bool res = true;
  int step = 1;
  slip::Matrix4d<T> V(VC);
  std::size_t d1 = V.dim1();
  std::size_t d2 = V.dim2();
  std::size_t d3 = V.dim3();
  std::size_t d4 = V.dim4();

  slip::DPoint4d<int> d = V.last_back_bottom_right() - V.first_front_upper_left();
  res = res && (d == slip::DPoint4d<int>(d1,d2,d3,d4));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << d << std::endl;
  }
  step++;

  //global four dimensional iterators

  typedef typename slip::Matrix4d<T>::iterator4d iter4d;
  typedef typename slip::Matrix4d<T>::const_iterator4d const_iter4d;
  typedef typename slip::Matrix4d<T>::reverse_iterator4d riter4d;
  typedef typename slip::Matrix4d<T>::const_reverse_iterator4d const_riter4d;

  typedef typename slip::Matrix4d<T>::iterator iter;
  typedef typename slip::Matrix4d<T>::const_iterator const_iter;
  typedef typename slip::Matrix4d<T>::reverse_iterator riter;
  typedef typename slip::Matrix4d<T>::const_reverse_iterator const_riter;


  std::pair<iter4d,const_iter> pair_it1;
  pair_it1 = std::mismatch(V.first_front_upper_left(), V.last_back_bottom_right(), VC.begin());
  res = res && (pair_it1.first == V.last_back_bottom_right());
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;
  std::pair<const_iter4d,iter> pair_it2;
  pair_it2 = std::mismatch(VC.first_front_upper_left(), VC.last_back_bottom_right(), V.begin());
  res = res && (pair_it2.first == VC.last_back_bottom_right());
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;

  std::pair<riter4d,const_riter> pair_it3;
  pair_it3 = std::mismatch(V.rfirst_front_upper_left(), V.rlast_back_bottom_right(), VC.rbegin());
  res = res && (pair_it3.first == V.rlast_back_bottom_right());
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;
  std::pair<const_riter4d,riter> pair_it4;
  pair_it4 = std::mismatch(VC.rfirst_front_upper_left(), VC.rlast_back_bottom_right(), V.rbegin());
  res = res && (pair_it4.first == VC.rlast_back_bottom_right());
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;

  //box four dimensional iterators
  slip::Box4d<int> box(1,1,1,1,d1-1,d2-1,d3-1,d4-1);
  std::pair<iter4d,const_iter4d> pair_it5;
  pair_it5 = std::mismatch(V.first_front_upper_left(box), V.last_back_bottom_right(box), VC.first_front_upper_left(box));
  res = res && (pair_it5.first == V.last_back_bottom_right(box));
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;
  std::pair<const_iter4d,iter4d> pair_it6;
  pair_it6 = std::mismatch(VC.first_front_upper_left(box), VC.last_back_bottom_right(box), V.first_front_upper_left(box));
  res = res && (pair_it6.first == VC.last_back_bottom_right(box));
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;

  std::pair<riter4d,const_riter4d> pair_it7;
  pair_it7 = std::mismatch(V.rfirst_front_upper_left(box), V.rlast_back_bottom_right(box), VC.rfirst_front_upper_left(box));
  res = res && (pair_it7.first == V.rlast_back_bottom_right(box));
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;
  std::pair<const_riter4d,riter4d> pair_it8;
  pair_it8 = std::mismatch(VC.rfirst_front_upper_left(box), VC.rlast_back_bottom_right(box), V.rfirst_front_upper_left(box));
  res = res && (pair_it8.first == VC.rlast_back_bottom_right(box));
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;

  //Range four dimensional iterators

  typedef typename slip::Matrix4d<T>::iterator4d_range range_iter4d;
  typedef typename slip::Matrix4d<T>::const_iterator4d_range const_range_iter4d;
  typedef typename slip::Matrix4d<T>::reverse_iterator4d_range rrange_iter4d;
  typedef typename slip::Matrix4d<T>::const_reverse_iterator4d_range const_rrange_iter4d;

  slip::Range<int> slab_range(0,d1-1,2);
  slip::Range<int> slice_range(0,d2-1,2);
  slip::Range<int> row_range(0,d3-1,2);
  slip::Range<int> col_range(0,d4-1,2);
  std::pair<range_iter4d,const_range_iter4d> pair_it9;
  pair_it9 = std::mismatch(V.first_front_upper_left(slab_range,slice_range,row_range,col_range),
      V.last_back_bottom_right(slab_range,slice_range,row_range,col_range),
      VC.first_front_upper_left(slab_range,slice_range,row_range,col_range));
  res = res && (pair_it9.first == V.last_back_bottom_right(slab_range,slice_range,row_range,col_range));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::copy(V.first_front_upper_left(slab_range,slice_range,row_range,col_range),
          V.last_back_bottom_right(slab_range,slice_range,row_range,col_range),
          std::ostream_iterator<T>(std::cerr," "));

      std::copy(VC.first_front_upper_left(slab_range,slice_range,row_range,col_range),
          VC.last_back_bottom_right(slab_range,slice_range,row_range,col_range),
          std::ostream_iterator<T>(std::cerr," "));
      std::cerr << std::endl;
  }
  step++;
  std::pair<const_range_iter4d,range_iter4d> pair_it10;
  pair_it10 = std::mismatch(VC.first_front_upper_left(slab_range,slice_range,row_range,col_range),
      VC.last_back_bottom_right(slab_range,slice_range,row_range,col_range),
      V.first_front_upper_left(slab_range,slice_range,row_range,col_range));
  res = res && (pair_it10.first == VC.last_back_bottom_right(slab_range,slice_range,row_range,col_range));
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;

  std::pair<rrange_iter4d,const_rrange_iter4d> pair_it11;
  pair_it11 = std::mismatch(V.rfirst_front_upper_left(slab_range,slice_range,row_range,col_range),
      V.rlast_back_bottom_right(slab_range,slice_range,row_range,col_range),
      VC.rfirst_front_upper_left(slab_range,slice_range,row_range,col_range));
  res = res && (pair_it11.first == V.rlast_back_bottom_right(slab_range,slice_range,row_range,col_range));
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;
  std::pair<const_rrange_iter4d,rrange_iter4d> pair_it12;
  pair_it12 = std::mismatch(VC.rfirst_front_upper_left(slab_range,slice_range,row_range,col_range),
      VC.rlast_back_bottom_right(slab_range,slice_range,row_range,col_range),
      V.rfirst_front_upper_left(slab_range,slice_range,row_range,col_range));
  res = res && (pair_it12.first == VC.rlast_back_bottom_right(slab_range,slice_range,row_range,col_range));
  if(!res){
      std::cerr << "error #" << step << std::endl;
  }
  step++;

  return res;
}

/*!
 * \brief arithmetic operators property
 * \param MC input %Matrix4d
 * \param val input T value
 * \return true if the property is verified
 */
template<typename T>
bool def_matrix4d_arithmetic_property(const slip::Matrix4d<T>& MC, const T& val){
  bool res = true;
  int step = 1;
  slip::Matrix4d<T> M(MC);
  slip::Matrix4d<T> W(MC.dim1(),MC.dim2(),MC.dim3(),MC.dim4(),val);
  T val2(val);
  M += val;
  M -= val;
  res = res && (are_matrix4d_almost_equal(M,MC));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "M = " << M;
      std::cerr << std::endl;
      std::cerr << "MC = " << MC;
      std::cerr << std::endl;
  }
  step++;
  M += W;
  M -= W;
  res = res && (are_matrix4d_almost_equal(M,MC));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "M = " << M;
      std::cerr << std::endl;
      std::cerr << "MC = " << MC;
      std::cerr << std::endl;
  }
  step++;
  if (val == T(0))
    {
      val2 += T(1);
      W += T(1);
    }

  M *= val2;
  M /= val2;
  M *= W;
  M /= W;

  res = res && (are_matrix4d_almost_equal(M,MC));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "val2 = " << val2;
      std::cerr << std::endl;
      std::cerr << "M = " << M;
      std::cerr << std::endl;
      std::cerr << "MC = " << MC;
      std::cerr << std::endl;
  }
  step++;

  slip::Matrix4d<T> R(MC.dim1(),MC.dim2(),MC.dim3(),MC.dim4());
  R = M + W;
  R = R - W;
  res = res && (are_matrix4d_almost_equal(R,MC));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "R = " << M;
      std::cerr << std::endl;
      std::cerr << "MC = " << MC;
      std::cerr << std::endl;
  }

  R = M + val;
  R = R - val;
  res = res && (are_matrix4d_almost_equal(R,MC));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "R = " << M;
      std::cerr << std::endl;
      std::cerr << "MC = " << MC;
      std::cerr << std::endl;
  }

  R = M * W;
  R = R / W;
  res = res && (are_matrix4d_almost_equal(R,MC));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "R = " << M;
      std::cerr << std::endl;
      std::cerr << "MC = " << MC;
      std::cerr << std::endl;
  }

  R = M * val2;
  R = R / val2;
  res = res && (are_matrix4d_almost_equal(R,MC));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "R = " << M;
      std::cerr << std::endl;
      std::cerr << "MC = " << MC;
      std::cerr << std::endl;
  }

  return res;
}

/*!
 * \brief mathematical operators property
 * \param MC input %Matrix4d
 * \param val input T value
 * \return true if the property is verified
 */
template<typename T>
bool def_matrix4d_mathematical_property(const slip::Matrix4d<T>& MC, const T& val){
  bool res = true;
  int step = 1;
  slip::Matrix4d<T> M(MC);
  slip::Matrix4d<T> W(MC.dim1(),MC.dim2(),MC.dim3(),MC.dim4(),val);
  T min = M.min();
  T max = M.max();
  T sum = M.sum();
  T min_c(M[0][0][0][0]),max_c(M[0][0][0][0]),sum_c(M[0][0][0][0]);

  for(std::size_t l=0; l<M.dim1(); ++l)
    for(std::size_t k=0; k<M.dim2(); ++k)
      for(std::size_t i=0; i<M.dim3(); ++i)
        for(std::size_t j=0; j<M.dim4(); ++j){
            min_c = (M[l][k][i][j] < min_c ? M[l][k][i][j] : min_c);
            max_c = (M[l][k][i][j] > max_c ? M[l][k][i][j] : max_c);
            sum_c += M[l][k][i][j];
        }
  res = res && (min_c == min) && (max_c == max) && (sum_c == sum) &&
      (min_c == slip::min(M)) && (max_c == slip::max(M));
  if(!res){
      std::cerr << "error #" << step << std::endl;
      std::cerr << "min = " << min << " ?= " << min_c << " ?= " << slip::min(M);
      std::cerr << std::endl;
      std::cerr << "max = " << max << " ?= " << max_c << " ?= " << slip::max(M);
      std::cerr << std::endl;
      std::cerr << "sum = " << sum << " ?= " << sum_c;
      std::cerr << std::endl;
  }
  step++;

  slip::Matrix4d<T> ABS = slip::abs(M);
  slip::Matrix4d<T> SQRT = slip::sqrt(M);
  slip::Matrix4d<T> COS = slip::cos(M);
  slip::Matrix4d<T> ACOS = slip::acos(M);
  slip::Matrix4d<T> SIN = slip::sin(M);
  slip::Matrix4d<T> ASIN = slip::asin(M);
  slip::Matrix4d<T> TAN = slip::tan(M);
  slip::Matrix4d<T> ATAN = slip::atan(M);
  slip::Matrix4d<T> EXP = slip::exp(M);
  slip::Matrix4d<T> LOG = slip::log(M);
  slip::Matrix4d<T> COSH = slip::cosh(M);
  slip::Matrix4d<T> SINH = slip::sinh(M);
  slip::Matrix4d<T> TANH = slip::tanh(M);
  slip::Matrix4d<T> LOG10 = slip::log10(M);

  for(std::size_t l=0; l<M.dim1(); ++l)
     for(std::size_t k=0; k<M.dim2(); ++k)
       for(std::size_t i=0; i<M.dim3(); ++i)
         for(std::size_t j=0; j<M.dim4(); ++j){
             res = res && (ABS[l][k][i][j] == std::abs(M[l][k][i][j]));
             res = res && (SQRT[l][k][i][j] == std::sqrt(M[l][k][i][j]));
             res = res && (COS[l][k][i][j] == std::cos(M[l][k][i][j]));
             res = res && (ACOS[l][k][i][j] == std::acos(M[l][k][i][j]));
             res = res && (SIN[l][k][i][j] == std::sin(M[l][k][i][j]));
             res = res && (ASIN[l][k][i][j] == std::asin(M[l][k][i][j]));
             res = res && (TAN[l][k][i][j] == std::tan(M[l][k][i][j]));
             res = res && (ATAN[l][k][i][j] == std::atan(M[l][k][i][j]));
             res = res && (EXP[l][k][i][j] == std::exp(M[l][k][i][j]));
             res = res && (LOG[l][k][i][j] == std::log(M[l][k][i][j]));
             res = res && (COSH[l][k][i][j] == std::cosh(M[l][k][i][j]));
             res = res && (SINH[l][k][i][j] == std::sinh(M[l][k][i][j]));
             res = res && (TANH[l][k][i][j] == std::tanh(M[l][k][i][j]));
             res = res && (LOG10[l][k][i][j] == std::log10(M[l][k][i][j]));
         }

  return res;
}


//If QuickCheck++ is available, the properties functions are encapsulated in quickcheck properties.
#ifdef HAVE_QCHECK

namespace quickcheck
{
  /*!
   * \brief quickcheck generate function for %Matrix4d
   * \param n the quickcheck size hint
   * \param out the %Matrix4d to generate
   */
  template<typename T>
  void generate(size_t n, slip::Matrix4d<T>& out);
}//namespace quickcheck

#include "quickcheck/quickcheck.hh"

namespace quickcheck
{

  template<typename T>
  void generate(size_t n, slip::Matrix4d<T>& out){
    unsigned d1,d2,d3,d4;
    d1 = generateInRange<unsigned>(1,20);
    d2 = generateInRange<unsigned>(1,20);
    d3 = generateInRange<unsigned>(1,20);
    d4 = generateInRange<unsigned>(1,20);

    out.resize(d1,d2,d3,d4);
    for (size_t i = 0; i < d1; ++i)
      for (size_t j = 0; j < d2; ++j)
        for (size_t k = 0; k < d3; ++k)
          for (size_t l = 0; l < d4; ++l)
            {
              T a;
              generate(n, a);
              out(i,j,k,l) = a;
            }
  }

  /*! \class ConstructorProperty1
   * \brief quickcheck property of %Matrix4d that holds for the constructor with
   * three dimension parameters
   * \param size_type type of %Matrix4d dimensions
   * \param T type of %Matrix4d elements
   */
  template<class T>
  class Matrix4dConstructorProperty1 : public Property<slip::Matrix4d<T> > {
    /*!
     * \brief property definition function
     * \param x input %Matrix4d
     */
    bool holdsFor(const slip::Matrix4d<T> &x){
      return def_matrix4d_constructor_property1<typename slip::Matrix4d<T>::size_type,T>(x.dim1(),x.dim2(),x.dim3(),x.dim4());
    }
  };

  /*! \class ConstructorProperty2
   * \brief quickcheck property of %Matrix4d that holds for the constructors with
   * three dimension parameters and an initial value
   * \param size_type type of %Matrix4d dimensions
   * \param T type of %Matrix4d elements
   */
  template<class T>
  class Matrix4dConstructorProperty2 : public Property<T,slip::Matrix4d<T> > {
    /*!
     * \brief property definition function
     * \param val initial value
     * \param x input %Matrix4d
     */
    bool holdsFor(const T &val, const slip::Matrix4d<T> &x){
      return def_matrix4d_constructor_property2<typename slip::Matrix4d<T>::size_type,T>(x.dim1(),x.dim2(),x.dim3(),x.dim4(),val);
    }
  };

  /*! \class ConstructorProperty3
   * \brief quickcheck property of %Matrix4d that holds for the constructors with
   * three dimension parameters and initial arrays of values.
   * \param size_type type of %Matrix4d dimensions
   * \param T type of %Matrix4d elements
   */
  template<class T>
  class Matrix4dConstructorProperty3 : public Property<std::vector<T>,slip::Matrix4d<T> > {
    /*!
     * \brief property definition function
     * \param val initial array of values
     * \param x input %Matrix4d
     */
    bool holdsFor(const std::vector<T> & val, const slip::Matrix4d<T> &x){
      return def_matrix4d_constructor_property3<typename slip::Matrix4d<T>::size_type,T>(x.dim1(),x.dim2(),x.dim3(),x.dim4(),val);
    }
    /*!
     * \brief Inputs generation method
     * \param n the quickcheck size hint
     * \param initial array of values
     * \param x input %Matrix4d (it is not used by this test)
     */
    void generateInput(size_t n, std::vector<T> & val, slip::Matrix4d<T> &x) {
      generate(n, x);
      int dim = x.size();
      for (int i = 0; i < dim; i++) {
          T a;
          generate(n - 1, a);
          val.push_back(a);
      }
    }
  };

  /*! \class NameProperty
   * \brief quickcheck property of %Matrix4d that holds for the name returned by the name() method.
   * \param T type of %Matrix4d elements
   */
  template<class T>
  class Matrix4dNameProperty : public Property<slip::Matrix4d<T> > {
    bool holdsFor(const slip::Matrix4d<T> &x){
      return def_matrix4d_name_property<T>(x);
    }
  };

  /*!
   * \class AssignFillProperty
   * \brief quickcheck property of %Matrix4d that holds for the assign and fill methods.
   * \param T type of %Matrix4d elements
   */
  template<class T>
  class Matrix4dAssignFillProperty : public Property<slip::Matrix4d<T> > {
    bool holdsFor(const slip::Matrix4d<T> &x){
      return def_matrix4d_assign_fill_property<T>(x);
    }
  };

  /*!
   * \class ResizeProperty
   * \brief quickcheck property of %Matrix4d that holds for the resize method.
   * \param T type of %Matrix4d elements
   */
  template<class T>
  class Matrix4dResizeProperty : public Property<slip::Matrix4d<T> > {
    bool holdsFor(const slip::Matrix4d<T> &x){
      unsigned d1,d2,d3,d4;
      d1 = generateInRange<unsigned>(1,20);
      d2 = generateInRange<unsigned>(1,20);
      d3 = generateInRange<unsigned>(1,20);
      d4 = generateInRange<unsigned>(1,20);
      return def_matrix4d_resize_property<T>(x,d1,d2,d3,d4);
    }
  };

  /*!
   * \class SwapComparisonProperty
   * \brief quickcheck property of %Matrix4d that holds for the swap and comparison methods.
   * \param T type of %Matrix4d elements
   */
  template<class T>
  class Matrix4dSwapComparisonProperty : public Property<slip::Matrix4d<T>, slip::Matrix4d<T> > {
    bool holdsFor(const slip::Matrix4d<T> &x,const slip::Matrix4d<T> &y){
      return def_matrix4d_swap_comparison_property<T>(x,y);
    }

    /*!
     * \brief Inputs generation method
     */
    void generateInput(size_t n, slip::Matrix4d<T> &x, slip::Matrix4d<T> &y) {
      generate(n, x);
      y.resize(x.dim1(),x.dim2(),x.dim3(),x.dim4());
      for (size_t i = 0; i < x.dim1(); ++i)
        for (size_t j = 0; j < x.dim2(); ++j)
          for (size_t k = 0; k < x.dim3(); ++k)
            for (size_t l = 0; l < x.dim4(); ++l)
              {
                T a;
                generate(n, a);
                y(i,j,k,l) = a;
              }
    }
  };

  /*!
   * \class Iterators1dProperty
   * \brief quickcheck property of %Matrix4d that holds for the iterators 1d definitions methods.
   * \param T type of %Matrix4d elements
   */
  template<class T>
  class Matrix4dIterators1dProperty : public Property<slip::Matrix4d<T> > {
    bool holdsFor(const slip::Matrix4d<T> &x){
      return def_matrix4d_iterators1d_property<T>(x);
    }

    /*!
     * \brief Inputs generation method
     */
    void generateInput(size_t n, slip::Matrix4d<T> &x) {
      unsigned d1,d2,d3,d4;
      d1 = generateInRange<unsigned>(3,20);
      d2 = generateInRange<unsigned>(3,20);
      d3 = generateInRange<unsigned>(3,20);
      d4 = generateInRange<unsigned>(3,20);

      x.resize(d1,d2,d3,d4);
      for (size_t i = 0; i < d1; ++i)
        for (size_t j = 0; j < d2; ++j)
          for (size_t k = 0; k < d3; ++k)
            for (size_t l = 0; l < d4; ++l)
              {
                T a;
                generate(n, a);
                x(i,j,k,l) = a;
              }
    }
  };

  /*!
   * \class Iterators4dProperty
   * \brief quickcheck property of %Matrix4d that holds for the iterators 4d related methods.
   * \param T type of %Matrix4d elements
   */
  template<class T>
  class Matrix4dIterators4dProperty : public Property<slip::Matrix4d<T> > {
    bool holdsFor(const slip::Matrix4d<T> &x){
      return def_matrix4d_iterators4d_property<T>(x);
    }

    /*!
     * \brief Inputs generation method
     */
    void generateInput(size_t n, slip::Matrix4d<T> &x) {
      unsigned d1,d2,d3,d4;
      d1 = generateInRange<unsigned>(3,20);
      d2 = generateInRange<unsigned>(3,20);
      d3 = generateInRange<unsigned>(3,20);
      d4 = generateInRange<unsigned>(3,20);

      x.resize(d1,d2,d3,d4);
      for (size_t i = 0; i < d1; ++i)
        for (size_t j = 0; j < d2; ++j)
          for (size_t k = 0; k < d3; ++k)
            for (size_t l = 0; l < d4; ++l)
              {
                T a;
                generate(n, a);
                x(i,j,k,l) = a;
              }
    }
  };

  /*!
   * \class ArithmeticProperty
   * \brief quickcheck property of %Matrix4d that holds for arithmetic operators.
   * \param T type of %Matrix4d elements
   */
  template<class T>
  class Matrix4dArithmeticProperty : public Property<slip::Matrix4d<T>,T> {
    bool holdsFor(const slip::Matrix4d<T> &M, const T & val){
      return def_matrix4d_arithmetic_property<T>(M,val);
    }
  };

}//namespace quickcheck


#endif //HAVE_QCHECK

#endif /* MATRIX4D_QC_HPP_ */
