/*!
 ** \file DPoint4d_qc.hpp
 ** \brief Defining properties test functions that could be tested for %DPoint4d class
 ** \date 2013/07/01
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef DPOINT4D_QC_HPP_
#define DPOINT4D_QC_HPP_

#include "DPoint4d.hpp"

namespace slip
{
  template <typename T>
  class DPoint4d;
}//namespace slip


#ifdef HAVE_QCHECK

namespace quickcheck
{
  /*!
   * \brief quickcheck generate function for %DPoint4d
   * \param n the quickcheck size hint
   * \param out the %DPoint4d to generate
   */
  template<typename T>
  void generate(size_t n, slip::DPoint4d<T>& out);
}//namespace quickcheck


#include "quickcheck/quickcheck.hh"

namespace quickcheck
{
  template<typename T>
  inline
  void generate(size_t n, slip::DPoint4d<T>& out)
  {
    T dx1, dx2, dx3, dx4;
    generate(n, dx1);
    generate(n, dx2);
    generate(n, dx3);
    generate(n, dx4);
    out.dx1(dx1);
    out.dx2(dx2);
    out.dx3(dx3);
    out.dx4(dx4);
  }

  /*!
   * \class DPoint4d copy Property
   * \brief quickcheck property of %DPoint4d that holds for the copy operators
   * \param T type of %DPoint4d elements
   */
  template<typename T>
  class DPoint4dCopyProperty : public Property<slip::DPoint4d<T> > {
    /*!
     * \brief property definition function
     * \param p input %DPoint4d
     */
    bool holdsFor(const slip::DPoint4d<T> &p){
      slip::DPoint4d<T> p1(p);
      slip::DPoint4d<T> p2;
      p2 = p;
      return (p1 == p2);
    }
  };


}//namespace quickcheck

#endif //HAVE_QCHECK

#endif /* DPOINT4D_QC_HPP_ */
