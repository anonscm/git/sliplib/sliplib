/*!
 ** \file Point4d_qc.hpp
 ** \brief Defining properties test functions that could be tested for %Point4d class
 ** \date 2013/07/01
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef POINT4D_QC_HPP_
#define POINT4D_QC_HPP_

#include "Point4d.hpp"

namespace slip
{
  template <typename T>
  class Point4d;
}//namespace slip


#ifdef HAVE_QCHECK

namespace quickcheck
{
  /*!
   * \brief quickcheck generate function for %Point4d
   * \param n the quickcheck size hint
   * \param out the %Point4d to generate
   */
  template<typename T>
  void generate(size_t n, slip::Point4d<T>& out);
}//namespace quickcheck


#include "quickcheck/quickcheck.hh"

namespace quickcheck
{
  template<typename T>
  inline
  void generate(size_t n, slip::Point4d<T>& out)
  {
    T x1, x2, x3, x4;
    generate(n, x1);
    generate(n, x2);
    generate(n, x3);
    generate(n, x4);
    out.x1(x1);
    out.x2(x2);
    out.x3(x3);
    out.x4(x4);
  }

  /*!
   * \class Point4d copy Property
   * \brief quickcheck property of %Point4d that holds for the copy operators
   * \param T type of %Point4d elements
   */
  template<typename T>
  class Point4dCopyProperty : public Property<slip::Point4d<T> > {
    /*!
     * \brief property definition function
     * \param p input %Point4d
     */
    bool holdsFor(const slip::Point4d<T> &p){
      slip::Point4d<T> p1(p);
      slip::Point4d<T> p2;
      p2 = p;
      return (p1 == p2);
    }
  };


}//namespace quickcheck

#endif //HAVE_QCHECK

#endif /* POINT4D_QC_HPP_ */
