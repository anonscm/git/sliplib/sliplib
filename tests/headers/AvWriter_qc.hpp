/*!
** \file AvWriter_qc.hpp
** \brief Properties functions for the %AvWriter class.
** Those functions are used by unit testing.
** \date 2013/05/02
** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
*/

#ifndef AVWRITER_QC_HPP_
#define AVWRITER_QC_HPP_

#include <iostream>
#include <iomanip>
#include <algorithm>
#include "ColorVolume.hpp"
#include "Volume.hpp"
#include "AvWriter.hpp"

namespace slip
{
  template<class Container2d, typename T, std::size_t Nb_components>
  class AvWriter;
}//namespace slip


/*!
 * \brief properties of the %ColorVolume writing
 * \param filename of output file
 * \param video video to save
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_avwriter_write_colorvideo_property1(std::string filename, const slip::ColorVolume<T> & video,
					     int frame_rate, int64_t bit_rate, bool verbose = 0, std::ostream & outstream = std::cerr){

  std::size_t cols(video.cols()), rows(video.rows()), slices(video.slices());
  slip::AvWriter<slip::ColorVolume<T>,T,3> VideoCoulWriter;
  AVRational t_base = {1,frame_rate};
  try{
    VideoCoulWriter.initialize(cols, rows, slices, bit_rate, t_base);
  }
  catch(slip::exception &e){
    std::ostringstream err;
    err << "slip::" << "AvWriter::initialize()::" << slip::FILE_OPEN_ERROR;
    if (err.str().compare(e.what_str()) != 0){
      if(verbose){
	outstream << "->" << err.str() << std::endl;
	outstream <<  "->" << e.what_str() << std::endl;
      }
      return 0;
    }
  }
  VideoCoulWriter.set_output_filename(filename);
  if(cols == 0 || rows == 0 || slices == 0){
    try{
      VideoCoulWriter.initialize(cols, rows, slices, bit_rate, t_base);
    }
    catch(slip::exception &e){
      std::ostringstream err;
      err << "slip::" << "AvWriter::initialize()::" << slip::FILE_WRITE_ERROR
	  << filename << " | AV code has signaled the error : Empty AV video (DNL not supported)";
      if (err.str().compare(e.what_str()) != 0){
	if(verbose){
	  outstream << "->" << err.str() << std::endl;
	  outstream <<  "->" << e.what_str() << std::endl;
	}
	return 0;
      }
    }
  }else{
    VideoCoulWriter.initialize(cols, rows, slices, bit_rate, t_base);
    VideoCoulWriter.write(video);
    VideoCoulWriter.release();
    try{
      VideoCoulWriter.write(video);
    }
    catch(slip::exception &e){
      std::ostringstream err;
      err << "slip::" << "AvWriter::write()::" << slip::FILE_WRITE_ERROR
	  << filename << " | The writer to be initialized before writing.";
      if (err.str().compare(e.what_str()) != 0){
	if(verbose){
	  outstream << "->" << err.str() << std::endl;
	  outstream <<  "->" << e.what_str() << std::endl;
	}
	return 0;
      }
    }
  }
  VideoCoulWriter.release();
  return 1;
}

/*!
 * \brief properties of the %Volume writing
 * \param filename of output file
 * \param video video to save
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_avwriter_write_grayscalevideo_property1(std::string filename,
						 const slip::Volume<T> & video, int frame_rate, int64_t bit_rate,
						 bool verbose = 0, std::ostream & outstream = std::cerr){
  AVRational t_base = {1,frame_rate};
  std::size_t cols(video.cols()), rows(video.rows()), slices(video.slices());
  slip::AvWriter<slip::Volume<T>,T,1> VideoGrayWriter;
  try{
    VideoGrayWriter.initialize(cols, rows, slices, bit_rate, t_base);
  }
  catch(slip::exception &e){
    std::ostringstream err;
    err << "slip::" << "AvWriter::initialize()::" << slip::FILE_OPEN_ERROR;
    if (err.str().compare(e.what_str()) != 0){
      if(verbose){
	outstream << "->" << err.str() << std::endl;
	outstream << "->" << e.what_str() << std::endl;
      }
      return 0;
    }
  }
  VideoGrayWriter.set_output_filename(filename);
  if(cols == 0 || rows == 0){
    try{
      VideoGrayWriter.initialize(cols, rows, slices, bit_rate, t_base);
    }
    catch(slip::exception &e){
      std::ostringstream err;
      err << "slip::" << "AvWriter::initialize()::" << slip::FILE_WRITE_ERROR
	  << filename << " | AV code has signaled the error : Empty AV video (DNL not supported)";
      if (err.str().compare(e.what_str()) != 0){
	if(verbose){
	  outstream << "->" << err.str() << std::endl;
	  outstream << "->" << e.what_str() << std::endl;
	}
	return 0;
      }
    }
  }else{
    VideoGrayWriter.initialize(cols, rows, slices, bit_rate, t_base);
    VideoGrayWriter.write(video);
    VideoGrayWriter.release();
    try{
      VideoGrayWriter.write(video);
    }
    catch(slip::exception &e){
      std::ostringstream err;
      err << "slip::" << "AvWriter::write()::" << slip::FILE_WRITE_ERROR
	  << filename << " | The writer to be initialized before writing.";
      std::string mess = e.what_str();
      if (err.str().compare(mess) != 0){
	if(verbose){
	  outstream << "->" << err.str() << std::endl;
	  outstream << "->" << mess << std::endl;
	}
	return 0;
      }
    }
  }
  VideoGrayWriter.release();
  return 1;
}

/*!
 * \brief properties of the %ColorVolume writing with many containers
 * \param filename of output file
 * \param video_list list of videos to save :
 * 		- the list should have at least one element
 * 		- all the elements of the list should have the same dimensions
 * 		.
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_avwriter_write_colorvideo_property2(std::string filename,
					     const std::vector<slip::ColorVolume<T> > & video_list,
					     int frame_rate, int64_t bit_rate,
					     bool verbose = 0, std::ostream & outstream = std::cerr){
  AVRational t_base = {1,frame_rate};
  std::size_t cols(video_list.begin()->cols()),rows(video_list.begin()->rows()), slices(0);
  typename std::vector<slip::ColorVolume<T> >::const_iterator it_l = video_list.begin();
  while(it_l!=video_list.end()){
    slices += it_l->slices();
    it_l++;
  }
  // outstream << "global dim = (" << cols << "," << rows << "," << slices << ")\n";
  slip::AvWriter<slip::ColorVolume<T>,T,3> VideoCoulWriter(filename,cols, rows, slices,bit_rate,
							     t_base,1);
  it_l = video_list.begin();
  while(VideoCoulWriter.write(*it_l)){
    it_l++;
    //  std::cerr << "#### iteration \n" << std::endl;
  }
  return 1;
}

/*!
 * \brief properties of the %Volume writing with many containers
 * \param filename of output file
 * \param video_list list of videos to save:
 * 		- the list should have at least one element
 * 		- all the elements of the list should have the same dimensions
 * 		.
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_avwriter_write_grayscalevideo_property2(std::string filename,
						 const std::vector<slip::Volume<T> >& video_list,
						 int frame_rate, int64_t bit_rate,
						 bool verbose = 0, std::ostream & outstream = std::cerr){
  AVRational t_base = {1,frame_rate};
  std::size_t cols(video_list.begin()->cols()),
    rows(video_list.begin()->rows()), slices(0);
  typename std::vector<slip::Volume<T> >::const_iterator it_l = video_list.begin();
  while(it_l!=video_list.end()){
    slices += it_l->slices();
    it_l++;
  }
  slip::AvWriter<slip::Volume<T>,T,1> VideoGrayWriter(filename,cols, rows, slices, bit_rate,
						       t_base,1);
  it_l = video_list.begin();
  while(VideoGrayWriter.write(*it_l)){it_l++;};
  return 1;
}

//If QuickCheck++ is available, the properties functions are encapsulated in quickcheck properties.
#ifdef HAVE_QCHECK

namespace quickcheck
{
  template<typename T>
  void generate(size_t n, slip::ColorVolume<T>& out);
  template<typename T>
  void generate(size_t n, slip::Volume<T>& out);
  static void generate(size_t n, long long int & i);
  //template<typename T>
  //void generate(size_t n, std::vector<slip::ColorVolume<T> >& out);
  //template<typename T>
  //void generate(size_t n, std::vector<slip::Volume<T> >& out);
}//namespace quickcheck

#include "ColorVolume_qc.hpp"
#include "Volume_qc.hpp"
#include "quickcheck/quickcheck.hh"

namespace quickcheck
{

 static inline void generate(size_t n, long long int & i){
    int ii;
    generate(n,ii);
    i = static_cast<int64_t>(ii);
  }

  /*! \class AvWriterProperty1
   * \brief quickcheck property of %AvWriter that holds for the write methods with a single container
   * \param T type of %ColorVolumeSignal elements
   * \param col_filename the filename for saving the color videos
   * \param gray_filename the filename for saving the grayscale videos
   * \param verbose true if a verbose mode is asking, default is false.
   * \param outstream output ostream for verbose mode, default is std::cerr.
   */
  template<typename T>
  class AvWriterProperty1 : public Property<slip::ColorVolume<T>, slip::Volume<T>, int, int64_t> {

  public:
    typedef Property<slip::ColorVolume<T>, slip::Volume<T> , int, int64_t> base;
    AvWriterProperty1()
      :base(),col_filename(""),gray_filename(""),verbose(0), outstream(&std::cerr)
    {}
    /*!
     * \brief property definition function
     * \param x input %ColorVolumeSignal
     */
    bool holdsFor(const slip::ColorVolume<T> &video_coul,
		  const slip::Volume<T> &video_gray, const int & frame_rate, const int64_t & bit_rate){
      bool res = true;
      res = res && def_avwriter_write_colorvideo_property1(col_filename,video_coul, frame_rate, bit_rate, verbose, *outstream);
      res = res && def_avwriter_write_grayscalevideo_property1(gray_filename,video_gray, frame_rate, bit_rate, verbose, *outstream);
      return res;
    }

    const std::string& get_col_filename() const {
      return col_filename;
    }

    void set_col_filename(const std::string& col_filename) {
      this->col_filename = col_filename;
    }

    const std::string& get_gray_filename() const {
      return gray_filename;
    }

    void set_gray_filename(const std::string& gray_filename) {
      this->gray_filename = gray_filename;
    }

    bool isverbose() const {
      return verbose;
    }

    void set_verbose(bool verbose) {
      this->verbose = verbose;
    }

    void set_outstream(std::ostream * strin) {
      this->outstream = strin;
    }

    void generateInput(size_t n,slip::ColorVolume<T> &video_coul,
		       slip::Volume<T> &video_gray, int & frame_rate, int64_t & bit_rate) {

      unsigned d1,d2,d3;
      d1 = generateInRange<unsigned>(2,20);
      d2 = generateInRange<unsigned>(10,100) * 2;
      d3 = generateInRange<unsigned>(10,100) * 2;
      //Color video
      video_coul.resize(d1,d2,d3);
      for (size_t i = 0; i < d1; ++i)
	for (size_t j = 0; j < d2; ++j)
	  for (size_t k = 0; k < d3; ++k)
	    {
	      slip::Color<T> a;
	      generate(n, a);
	      video_coul(i,j,k) = a;
	    }

      //Grayscale video
      video_gray.resize(d1,d2,d3);
      for (size_t i = 0; i < d1; ++i)
	for (size_t j = 0; j < d2; ++j)
	  for (size_t k = 0; k < d3; ++k)
	    {
	      T a;
	      generate(n, a);
	      video_gray(i,j,k) = a;
	    }
      //int possible_fr[9] = {24,25,30,48,50,60,72,120,300};
      frame_rate = 50;//oneOf<int,int*>(possible_fr,possible_fr+9);
      bit_rate = 1500000;//generateInRange<int64_t>(400000,1500000);
    }

  protected:

    std::string col_filename;//filename for saving the color videos
    std::string gray_filename;//filename for saving the grayscale videos
    bool verbose;
    std::ostream * outstream;
  };


  /*! \class AvWriterProperty2
   * \brief quickcheck property of %AvWriter that holds for the write methods with a many container
   * \param T type of %ColorVolumeSignal elements
   * \param col_filename the filename for saving the color videos
   * \param gray_filename the filename for saving the grayscale videos
   * \param verbose true if a verbose mode is asking, default is false.
   * \param outstream output ostream for verbose mode, default is std::cerr.
   */
  template<typename T>
  class AvWriterProperty2 : public Property<std::vector<slip::ColorVolume<T> >,std::vector<slip::Volume<T> >,int,int64_t>
  {
  public:
    typedef std::vector<slip::ColorVolume<T> > qc_colorlist;
    typedef std::vector<slip::Volume<T> > qc_graylist;
    typedef Property<qc_colorlist,qc_graylist,int,int64_t> base;

    AvWriterProperty2()
      :base(),col_filename(""),gray_filename(""),verbose(0), outstream(&std::cerr)
    {}
    /*!
     * \brief property definition function
     * \param colim_list input %ColorVolume list
     * \param grayim_list input %Volume list
     */
    bool holdsFor(const qc_colorlist &colvideo_list, const qc_graylist &grayvideo_list,
		  const int & frame_rate, const int64_t & bit_rate){
      bool res = true;
      res = res && def_avwriter_write_colorvideo_property2(col_filename,colvideo_list,frame_rate,bit_rate,
							   verbose,*outstream);
      res = res && def_avwriter_write_grayscalevideo_property2(gray_filename,grayvideo_list,frame_rate,bit_rate,
							       verbose,*outstream);
      return res;
    }

    const std::string& get_col_filename() const {
      return col_filename;
    }

    void set_col_filename(const std::string& col_filename) {
      this->col_filename = col_filename;
    }

    const std::string& get_gray_filename() const {
      return gray_filename;
    }

    void set_gray_filename(const std::string& gray_filename) {
      this->gray_filename = gray_filename;
    }

    bool isverbose() const {
      return verbose;
    }

    void set_verbose(bool verbose) {
      this->verbose = verbose;
    }

    void set_outstream(std::ostream * strin) {
      this->outstream = strin;
    }

    void generateInput(size_t n, qc_colorlist &colvideo_list, qc_graylist &grayvideo_list, int & frame_rate,
		       int64_t & bit_rate) {
      unsigned d1,d2,d3;
      d1 = generateInRange<unsigned>(2,20);
      d2 = generateInRange<unsigned>(10,100);
      d3 = generateInRange<unsigned>(10,100);
      //Color list
      unsigned int nbvideo_coul = generateInRange<unsigned int>(2,4);
      slip::ColorVolume<T> video_coul(d1,d2,d3);
      for (size_t i = 0; i < d1; ++i)
	for (size_t j = 0; j < d2; ++j)
	  for (size_t k = 0; k < d3; ++k)
	    {
	      slip::Color<T> a;
	      generate(n, a);
	      video_coul(i,j,k) = a;
	    }
      colvideo_list.resize(nbvideo_coul);
      std::fill_n(colvideo_list.begin(),nbvideo_coul,video_coul);

      //Grayscale list
      unsigned int nbvideo_gray = generateInRange<unsigned int>(2,4);
      slip::Volume<T> video_gray(d1,d2,d3);
      for (size_t i = 0; i < d1; ++i)
	for (size_t j = 0; j < d2; ++j)
	  for (size_t k = 0; k < d3; ++k)
	    {
	      T a;
	      generate(n, a);
	      video_gray(i,j,k) = a;
	    }
      grayvideo_list.resize(nbvideo_gray);
      std::fill_n(grayvideo_list.begin(),nbvideo_gray,video_gray);

      //int possible_fr[9] = {24,25,30,48,50,60,72,120,300};
      frame_rate = 50;//oneOf<int,int*>(possible_fr,possible_fr+9);
      bit_rate = 1500000;//generateInRange<int64_t>(400000,1500000);
    }
  protected:

    std::string col_filename;//filename for saving the color videos
    std::string gray_filename;//filename for saving the grayscale videos
    bool verbose;
    std::ostream * outstream;
  };

}//namespace quickcheck

#endif //HAVE_QCHECK



#endif /* AVWRITER_QC_HPP_ */
