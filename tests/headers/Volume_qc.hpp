/*!
 ** \file Volume_qc.hpp
 ** \brief Defining properties test functions that could be tested for %Volume class
 ** \date 2013/05/02
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef VOLUME_QC_HPP_
#define VOLUME_QC_HPP_

#include "Volume.hpp"

namespace slip
{
template <class T>
class Volume;
}//namespace slip


#ifdef HAVE_QCHECK

namespace quickcheck
{
/*!
 * \brief quickcheck generate function for %Volume
 * \param n the quickcheck size hint
 * \param out the %Volume to generate
 */
template<typename T>
void generate(size_t n, slip::Volume<T>& out);
}//namespace quickcheck


#include "quickcheck/quickcheck.hh"

namespace quickcheck
{
template<typename T>
inline
void generate(size_t n, slip::Volume<T>& out)
{
	unsigned d1,d2,d3;
	generate(n, d1);
	generate(n, d2);
	generate(n, d3);
	unsigned prodd = d1 * d2 * d3;
	if (prodd > 0){
		out.resize(d1,d2,d3);
		for (size_t i = 0; i < d1; ++i)
			for (size_t j = 0; j < d2; ++j)
				for (size_t z = 0; z < d3; ++z)
				{
					T a;
					generate(n, a);
					out(i,j,z) = a;
				}
	}
}
}//namespace quickcheck

#endif //HAVE_QCHECK

#endif /* VOLUME_QC_HPP_ */
