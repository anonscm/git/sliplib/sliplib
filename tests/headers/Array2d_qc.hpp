/*!
 ** \file Array2d_qc.hpp
 ** \brief Defining properties test functions that could be tested for %Array2d class
 ** \date 2013/09/12
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef ARRAY2D_QC_HPP_
#define ARRAY2D_QC_HPP_

#include "Array2d.hpp"

namespace slip
{
template <class T>
class Array2d;
}//namespace slip


#ifdef HAVE_QCHECK

namespace quickcheck
{
/*!
 * \brief quickcheck generate function for %Array2d
 * \param n the quickcheck size hint
 * \param out the %Array2d to generate
 */
template<typename T>
void generate(size_t n, slip::Array2d<T>& out);
}//namespace quickcheck


#include "quickcheck/quickcheck.hh"

namespace quickcheck
{
template<typename T>
inline
void generate(size_t n, slip::Array2d<T>& out)
{
	unsigned d1,d2;
	generate(n, d1);
	generate(n, d2);
	unsigned prodd = d1 * d2;
	if (prodd > 0){
		out.resize(d1,d2);
		for (size_t i = 0; i < d1; ++i)
			for (size_t j = 0; j < d2; ++j)
			{
				T a;
				generate(n, a);
				out(i,j) = a;
			}
	}
}
}//namespace quickcheck

#endif //HAVE_QCHECK

#endif /* ARRAY2D_QC_HPP_ */
