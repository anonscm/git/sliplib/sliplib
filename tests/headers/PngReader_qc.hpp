/*!
 ** \file PngReader_qc.hpp
 ** \brief Properties functions for the %PngReader class.
 ** Those functions are used by unit testing.
 ** \date 2013/04/23
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef PNGREADER_QC_HPP_
#define PNGREADER_QC_HPP_

#include <iostream>
#include <iomanip>
#include "ColorImage.hpp"
#include "GrayscaleImage.hpp"
#include "PngReader.hpp"


namespace slip
{
template<class Container2d, typename T, std::size_t Nb_components, std::size_t Nb_block>
class PngReader;
}//namespace slip


/*!
 * \brief properties of the %ColorImage reading
 * \param filename of the image to read
 * \param width of the image
 * \param height of the image
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_pngreader_read_colorimage_property1(std::string filename, std::size_t width, std::size_t height,
		bool verbose = 0, std::ostream & outstream = std::cerr){
	slip::PngReader<slip::ColorImage<T>,T,3,1> ImageCoulReader;
	try{
		ImageCoulReader.initialize();
	}
	catch(slip::slip_exception &e){
		std::ostringstream err;
		err << "slip::" << "PngReader::initialize()::" << slip::FILE_OPEN_ERROR;
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << "\n";
				outstream <<  "->" << e.what_str() << "\n";
			}
			return 0;
		}
	}
	ImageCoulReader.set_data_filename(filename);
	ImageCoulReader.initialize();
	slip::ColorImage<T> im;
	ImageCoulReader.read(im);
	if(im.width() != width || im.height() != height){
		if(verbose){
			outstream << "-->" << im.width() << " , " << im.height() << "\n";
			outstream << "-->" << width << " , " << height << "\n";
		}
		return 0;
	}
	ImageCoulReader.release();
	try{
		ImageCoulReader.read(im);
	}
	catch(slip::slip_exception &e){
		std::ostringstream err;
		err << "slip::" << "PngReader::read()::" << slip::FILE_READ_ERROR << filename
				<< " | The reader needs to be initialized before reading.";
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << "\n";
				outstream <<  "->" << e.what_str() << "\n";
			}
			return 0;
		}
	}
	ImageCoulReader.release();
	return 1;
}

/*!
 * \brief properties of the %GrayscaleImage reading
 * \param filename of the image to read
 * \param width of the image
 * \param height of the image
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_pngreader_read_grayscaleimage_property1(std::string filename, std::size_t width, std::size_t height,
		bool verbose = 0, std::ostream & outstream = std::cerr){
	slip::PngReader<slip::GrayscaleImage<T>,T,1,1> ImageGrayReader;
	try{
		ImageGrayReader.initialize();
	}
	catch(slip::slip_exception &e){
		std::ostringstream err;
		err << "slip::" << "PngReader::initialize()::" << slip::FILE_OPEN_ERROR;
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << "\n";
				outstream <<  "->" << e.what_str() << "\n";
			}
			return 0;
		}
	}
	ImageGrayReader.set_data_filename(filename);
	ImageGrayReader.initialize();
	slip::GrayscaleImage<T> im;
	ImageGrayReader.read(im);
	if(im.width() != width || im.height() != height){
		if(verbose){
			outstream << "-->" << im.width() << " , " << im.height() << "\n";
			outstream << "-->" << width << " , " << height << "\n";
		}
		return 0;
	}
	ImageGrayReader.release();
	try{
		ImageGrayReader.read(im);
	}
	catch(slip::slip_exception &e){
		std::ostringstream err;
		err << "slip::" << "PngReader::read()::" << slip::FILE_READ_ERROR << filename
				<< " | The reader needs to be initialized before reading.";
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << "\n";
				outstream <<  "->" << e.what_str() << "\n";
			}
			return 0;
		}
	}
	ImageGrayReader.release();
	return 1;
}

/*!
 * \brief properties of the %ColorImage reading with many containers
 * \param filename of the image to read
 * \param width of the image
 * \param height of the image
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_pngreader_read_colorimage_property2(std::string filename, std::size_t width, std::size_t height,
		bool verbose = 0, std::ostream & outstream = std::cerr){
	slip::PngReader<slip::ColorImage<T>,T,3,4> ImageCoulReader(filename);
	slip::ColorImage<T> im1,im2,im3,im4;
	ImageCoulReader.read(im1);
	ImageCoulReader.read(im2);
	ImageCoulReader.read(im3);
	ImageCoulReader.read(im4);

	if((im1.width() != width) || (im2.width() != width) || (im3.width() != width) || (im4.width() != width)
			|| ((im1.height() + im2.height() + im3.height() + im4.height()) != height)){
		if(verbose){
			outstream << im1.width() << " " << im1.height() << "\n";
			outstream << im2.width() << " " << im2.height() << "\n";
			outstream << im3.width() << " " << im3.height() << "\n";
			outstream << im4.width() << " " << im4.height() << "\n";
		}
		return 0;
	}
	ImageCoulReader.release();
	try{
		ImageCoulReader.read(im1);
	}
	catch(slip::slip_exception &e){
		std::ostringstream err;
		err << "slip::" << "PngReader::read()::" << slip::FILE_READ_ERROR << filename
				<< " | The reader needs to be initialized before reading.";
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << "\n";
				outstream <<  "->" << e.what_str() << "\n";
			}
			return 0;
		}
	}
	ImageCoulReader.release();
	return 1;
}

/*!
 * \brief properties of the %GrayscaleImage reading with many containers
 * \param filename of the image to read
 * \param width of the image
 * \param height of the image
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_pngreader_read_grayscaleimage_property2(std::string filename, std::size_t width, std::size_t height,
		bool verbose = 0, std::ostream & outstream = std::cerr){
	slip::PngReader<slip::GrayscaleImage<T>,T,1,4> ImageGrayReader(filename);
	slip::GrayscaleImage<T> im1,im2,im3,im4;
	ImageGrayReader.read(im1);
	ImageGrayReader.read(im2);
	ImageGrayReader.read(im3);
	ImageGrayReader.read(im4);

	if((im1.width() != width) || (im2.width() != width) || (im3.width() != width) || (im4.width() != width)
			|| ((im1.height() + im2.height() + im3.height() + im4.height()) != height)){
		if(verbose){
			outstream << im1.width() << " " << im1.height() << "\n";
			outstream << im2.width() << " " << im2.height() << "\n";
			outstream << im3.width() << " " << im3.height() << "\n";
			outstream << im4.width() << " " << im4.height() << "\n";
		}
		return 0;
	}
	ImageGrayReader.release();
	try{
		ImageGrayReader.read(im1);
	}
	catch(slip::slip_exception &e){
		std::ostringstream err;
		err << "slip::" << "PngReader::read()::" << slip::FILE_READ_ERROR << filename
				<< " | The reader needs to be initialized before reading.";
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << "\n";
				outstream <<  "->" << e.what_str() << "\n";
			}
			return 0;
		}
	}
	ImageGrayReader.release();
	return 1;
}



#endif /* PNGREADER_QC_HPP_ */
