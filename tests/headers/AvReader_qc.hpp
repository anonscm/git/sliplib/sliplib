/*!
 ** \file AvReader_qc.hpp
 ** \brief Properties functions for the %AvReader class.
 ** Those functions are used by unit testing.
 ** \date 2013/04/26
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef AVREADER_QC_HPP_
#define AVREADER_QC_HPP_

#include <iostream>
#include <iomanip>
#include "ColorVolume.hpp"
#include "Volume.hpp"
#include "AvReader.hpp"


namespace slip
{
template<class ContainerVideo, typename T, std::size_t Nb_components, std::size_t Nb_block>
class AvReader;
}//namespace slip


/*!
 * \brief properties of the %ColorVideo reading
 * \param filename of the video to read
 * \param cols of the video
 * \param rows of the video
 * \param slices of the video
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_avreader_read_colorvideo_property1(std::string filename, std::size_t cols, std::size_t rows,
		std::size_t slices, bool verbose = 0, std::ostream & outstream = std::cerr){
	slip::AvReader<slip::ColorVolume<T>,T,3,1> VideoCoulReader;
	if (VideoCoulReader.get_codec_id() != CODEC_ID_NONE){
		if(verbose){
			outstream << "Codec Id should be null." << std::endl;
		}
		return 0;
	}
	for(int i=1; i<3; i++){
		try{
			VideoCoulReader.initialize();
		}
		catch(slip::exception &e){
			std::ostringstream err;
			err << "slip::" << "AvReader::initialize()::" << slip::FILE_OPEN_ERROR;
			if (err.str().compare(e.what_str()) != 0){
				if(verbose){
					outstream << "->" << err.str() << std::endl;
					outstream <<  "->" << e.what_str() << std::endl;
				}
				return 0;
			}
		}
		VideoCoulReader.set_data_filename(filename);
		VideoCoulReader.initialize();
		slip::ColorVolume<T> vol;
		VideoCoulReader.read(vol);
		if(vol.cols() != cols || vol.rows() != rows || vol.slices() != slices){
			if(verbose){
				outstream << "-->" << vol.cols() << " , " << vol.rows() << " , " << vol.slices();
				outstream << std::endl;
				outstream << "-->" << cols << " , " << rows << " , " << slices << std::endl;
			}
			return 0;
		}
		VideoCoulReader.release();
		try{
			VideoCoulReader.read(vol);
		}
		catch(slip::exception &e){
			std::ostringstream err;
			err << "slip::" << "AvReader::read()::" << slip::FILE_READ_ERROR << filename
					<< " | The reader needs to be initialized before reading.";
			if (err.str().compare(e.what_str()) != 0){
				if(verbose){
					outstream << "->" << err.str() << std::endl;
					outstream <<  "->" << e.what_str() << std::endl;
				}
				return 0;
			}
		}
		VideoCoulReader.release();
	}
	return 1;
}

/*!
 * \brief properties of the %GrayscaleVideo reading
 * \param filename of the video to read
 * \param cols of the video
 * \param rows of the video
 * \param slices of the video
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_avreader_read_grayscalevideo_property1(std::string filename, std::size_t cols, std::size_t rows,
		std::size_t slices, bool verbose = 0, std::ostream & outstream = std::cerr){
	slip::AvReader<slip::Volume<T>,T,1,1> VideoGrayReader;
	for(int i=1; i<3; i++){
		try{
			VideoGrayReader.initialize();
		}
		catch(slip::exception &e){
			std::ostringstream err;
			err << "slip::" << "AvReader::initialize()::" << slip::FILE_OPEN_ERROR;
			if (err.str().compare(e.what_str()) != 0){
				if(verbose){
					outstream << "->" << err.str() << std::endl;
					outstream <<  "->" << e.what_str() << std::endl;
				}
				return 0;
			}
		}
		VideoGrayReader.set_data_filename(filename);
		VideoGrayReader.initialize();
		slip::Volume<T> vol;
		VideoGrayReader.read(vol);
		if(vol.cols() != cols || vol.rows() != rows || vol.slices() != slices){
			if(verbose){
				outstream << "-->" << vol.cols() << " , " << vol.rows() << " , " << vol.slices();
				outstream << std::endl;
				outstream << "-->" << cols << " , " << rows << " , " << slices << std::endl;
			}
			return 0;
		}
		VideoGrayReader.release();
		try{
			VideoGrayReader.read(vol);
		}
		catch(slip::exception &e){
			std::ostringstream err;
			err << "slip::" << "AvReader::read()::" << slip::FILE_READ_ERROR << filename
					<< " | The reader needs to be initialized before reading.";
			if (err.str().compare(e.what_str()) != 0){
				if(verbose){
					outstream << "->" << err.str() << std::endl;
					outstream <<  "->" << e.what_str() << std::endl;
				}
				return 0;
			}
		}
		VideoGrayReader.release();
	}
	return 1;
}

/*!
 * \brief properties of the %ColorVideo reading with many containers
 * \param filename of the video to read
 * \param cols of the video
 * \param rows of the video
 * \param slices of the video
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_avreader_read_colorvideo_property2(std::string filename, std::size_t cols, std::size_t rows,
		std::size_t slices, bool verbose = 0, std::ostream & outstream = std::cerr){
	slip::AvReader<slip::ColorVolume<T>,T,3,4> VideoCoulReader(filename);
	slip::ColorVolume<T> vol1,vol2,vol3,vol4;
	VideoCoulReader.read(vol1);
	VideoCoulReader.read(vol2);
	VideoCoulReader.read(vol3);
	VideoCoulReader.read(vol4);

	if((vol1.cols() != cols) || (vol2.cols() != cols) || (vol3.cols() != cols) || (vol4.cols() != cols)
			||	(vol1.rows() != rows) || (vol2.rows() != rows) || (vol3.rows() != rows) || (vol4.rows() != rows)
			|| ((vol1.slices() + vol2.slices() + vol3.slices() + vol4.slices()) != slices)){
		if(verbose){
			outstream << cols << " " << rows << " " << slices << "\n";
			outstream << vol1.cols() << " " << vol1.rows() << " " << vol1.slices();
			outstream << std::endl;
			outstream << vol2.cols() << " " << vol2.rows() << " " << vol2.slices();
			outstream << std::endl;
			outstream << vol3.cols() << " " << vol3.rows() << " " << vol3.slices();
			outstream << std::endl;
			outstream << vol4.cols() << " " << vol4.rows() << " " << vol4.slices();
			outstream << std::endl;
		}
		return 0;
	}
	VideoCoulReader.release();
	try{
		VideoCoulReader.read(vol1);
	}
	catch(slip::exception &e){
		std::ostringstream err;
		err << "slip::" << "AvReader::read()::" << slip::FILE_READ_ERROR << filename
				<< " | The reader needs to be initialized before reading.";
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << std::endl;
				outstream <<  "->" << e.what_str() << std::endl;
			}
			return 0;
		}
	}
	VideoCoulReader.release();
	return 1;
}

/*!
 * \brief properties of the %GrayscaleVideo reading with many containers
 * \param filename of the video to read
 * \param cols of the video
 * \param rows of the video
 * \param slices of the video
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_avreader_read_grayscalevideo_property2(std::string filename, std::size_t cols, std::size_t rows,
		std::size_t slices, bool verbose = 0, std::ostream & outstream = std::cerr){
	slip::AvReader<slip::Volume<T>,T,1,4> VideoGrayReader(filename);
	slip::Volume<T> im1,im2,im3,im4;
	slip::Volume<T> vol1,vol2,vol3,vol4;
	VideoGrayReader.read(vol1);
	VideoGrayReader.read(vol2);
	VideoGrayReader.read(vol3);
	VideoGrayReader.read(vol4);

	if((vol1.cols() != cols) || (vol2.cols() != cols) || (vol3.cols() != cols) || (vol4.cols() != cols)
			||	(vol1.rows() != rows) || (vol2.rows() != rows) || (vol3.rows() != rows) || (vol4.rows() != rows)
			|| ((vol1.slices() + vol2.slices() + vol3.slices() + vol4.slices()) != slices)){
		if(verbose){
			outstream << vol1.cols() << " " << vol1.rows() << " " << vol1.slices();
			outstream << std::endl;
			outstream << vol2.cols() << " " << vol2.rows() << " " << vol2.slices();
			outstream << std::endl;
			outstream << vol3.cols() << " " << vol3.rows() << " " << vol3.slices();
			outstream << std::endl;
			outstream << vol4.cols() << " " << vol4.rows() << " " << vol4.slices();
			outstream << std::endl;
		}
		return 0;
	}
	VideoGrayReader.release();
	try{
		VideoGrayReader.read(im1);
	}
	catch(slip::exception &e){
		std::ostringstream err;
		err << "slip::" << "AvReader::read()::" << slip::FILE_READ_ERROR << filename
				<< " | The reader needs to be initialized before reading.";
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << std::endl;
				outstream <<  "->" << e.what_str() << std::endl;
			}
			return 0;
		}
	}
	VideoGrayReader.release();
	return 1;
}

#endif /* AVREADER_QC_HPP_ */
