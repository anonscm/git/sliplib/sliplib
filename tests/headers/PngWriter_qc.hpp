/*!
 ** \file PngWriter_qc.hpp
 ** \brief Properties functions for the %PngWriter class.
 ** Those functions are used by unit testing.
 ** \date 2013/04/23
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef PNGWRITER_QC_HPP_
#define PNGWRITER_QC_HPP_

#include <iostream>
#include <iomanip>
#include <algorithm>
#include "ColorImage.hpp"
#include "GrayscaleImage.hpp"
#include "PngWriter.hpp"
#include "utils_compilation.hpp"

namespace slip
{
template<class Container2d, typename T, std::size_t Nb_components>
class PngWriter;
}//namespace slip


/*!
 * \brief properties of the %ColorImage writing
 * \param filename of output file
 * \param image image to save
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_pngwriter_write_colorimage_property1(std::string filename, const slip::ColorImage<T> & image,
		bool verbose = 0, std::ostream & outstream = std::cerr){

	std::size_t width(image.width()), height(image.height());
	slip::PngWriter<slip::ColorImage<T>,T,3> ImageCoulWriter;
	try{
		ImageCoulWriter.initialize(width,height);
	}
	catch(slip::slip_exception &e){
		std::ostringstream err;
		err << "slip::" << "PngWriter::initialize()::" << slip::FILE_OPEN_ERROR;
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << std::endl;
				outstream <<  "->" << e.what_str() << std::endl;
			}
			return 0;
		}
	}
	ImageCoulWriter.set_output_filename(filename);
	if(width == 0 || height == 0){
		try{
			ImageCoulWriter.initialize(width,height);
		}
		catch(slip::slip_exception &e){
			std::ostringstream err;
			err << "slip::" << "PngWriter::initialize()::" << slip::FILE_WRITE_ERROR
					<< filename << " | PNG code has signaled an error.";
			if (err.str().compare(e.what_str()) != 0){
				if(verbose){
					outstream << "->" << err.str() << std::endl;
					outstream <<  "->" << e.what_str() << std::endl;
				}
				return 0;
			}
		}
	}else{
		ImageCoulWriter.initialize(width,height);
		ImageCoulWriter.write(image);
		ImageCoulWriter.release();
		try{
			ImageCoulWriter.write(image);
		}
		catch(slip::slip_exception &e){
			std::ostringstream err;
			err << "slip::" << "PngWriter::write()::" << slip::FILE_WRITE_ERROR
					<< filename << " | The writer to be initialized before writing.";
			if (err.str().compare(e.what_str()) != 0){
				if(verbose){
					outstream << "->" << err.str() << std::endl;
					outstream <<  "->" << e.what_str() << std::endl;
				}
				return 0;
			}
		}
	}
	ImageCoulWriter.release();
	return 1;
}

/*!
 * \brief properties of the %GrayscaleImage writing
 * \param filename of output file
 * \param image image to save
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_pngwriter_write_grayscaleimage_property1(std::string filename,
		const slip::GrayscaleImage<T> & image, bool verbose = 0, std::ostream & outstream = std::cerr){

	std::size_t width(image.width()), height(image.height());
	slip::PngWriter<slip::GrayscaleImage<T>,T,1> ImageGrayWriter;
	try{
		ImageGrayWriter.initialize(width,height);
	}
	catch(slip::slip_exception &e){
		std::ostringstream err;
		err << "slip::" << "PngWriter::initialize()::" << slip::FILE_OPEN_ERROR;
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << std::endl;
				outstream << "->" << e.what_str() << std::endl;
			}
			return 0;
		}
	}
	ImageGrayWriter.set_output_filename(filename);
	if(width == 0 || height == 0){
		try{
			ImageGrayWriter.initialize(width,height);
		}
		catch(slip::slip_exception &e){
			std::ostringstream err;
			err << "slip::" << "PngWriter::initialize()::" << slip::FILE_WRITE_ERROR
					<< filename << " | PNG code has signaled an error.";
			if (err.str().compare(e.what_str()) != 0){
				if(verbose){
					outstream << "->" << err.str() << std::endl;
					outstream << "->" << e.what_str() << std::endl;
				}
				return 0;
			}
		}
	}else{
		ImageGrayWriter.initialize(width,height);
		ImageGrayWriter.write(image);
		ImageGrayWriter.release();
		try{
			ImageGrayWriter.write(image);
		}
		catch(slip::slip_exception &e){
			std::ostringstream err;
			err << "slip::" << "PngWriter::write()::" << slip::FILE_WRITE_ERROR
					<< filename << " | The writer to be initialized before writing.";
			std::string mess = e.what_str();
			if (err.str().compare(mess) != 0){
				if(verbose){
					outstream << "->" << err.str() << std::endl;
					outstream << "->" << mess << std::endl;
				}
				return 0;
			}
		}
	}
	ImageGrayWriter.release();
	return 1;
}

/*!
 * \brief properties of the %ColorImage writing with many containers
 * \param filename of output file
 * \param image_list list of images to save :
 * 		- the list should have at least one element
 * 		- all the elements of the list should have the same dimensions
 * 		.
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_pngwriter_write_colorimage_property2(std::string filename,
		const std::vector<slip::ColorImage<T> > & image_list,
					      bool UNUSED(verbose) = 0, std::ostream & UNUSED(outstream) = std::cerr){
	std::size_t list_size = image_list.size();
	std::size_t width(image_list.begin()->width()),
			height(image_list.begin()->height()  * list_size);
//	outstream << "list size = " << list_size << "\n";
//	outstream << "global dim = (" << width << "," << height << ")\n";
	slip::PngWriter<slip::ColorImage<T>,T,3> ImageCoulWriter(filename,width,height);
	typename std::vector<slip::ColorImage<T> >::const_iterator it_l = image_list.begin();
	while(ImageCoulWriter.write(*it_l)){it_l++;};
	return 1;
}

/*!
 * \brief properties of the %GrayscaleImage writing with many containers
 * \param filename of output file
 * \param image_list list of images to save:
 * 		- the list should have at least one element
 * 		- all the elements of the list should have the same dimensions
 * 		.
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_pngwriter_write_grayscaleimage_property2(std::string filename,
		const std::vector<slip::GrayscaleImage<T> >& image_list,
						  bool UNUSED(verbose) = 0, std::ostream & UNUSED(outstream) = std::cerr){
	std::size_t list_size = image_list.size();
	std::size_t width(image_list.begin()->width()),
			height(image_list.begin()->height()  * list_size);
	slip::PngWriter<slip::GrayscaleImage<T>,T,1> ImageGrayWriter(filename,width,height);
	typename std::vector<slip::GrayscaleImage<T> >::const_iterator it_l = image_list.begin();
	while(ImageGrayWriter.write(*it_l)){it_l++;};
	return 1;
}

//If QuickCheck++ is available, the properties functions are encapsulated in quickcheck properties.
#ifdef HAVE_QCHECK

namespace quickcheck
{
template<typename T>
void generate(size_t n, slip::ColorImage<T>& out);
template<typename T>
void generate(size_t n, slip::GrayscaleImage<T>& out);
}//namespace quickcheck


#include "GrayscaleImage_qc.hpp"
#include "ColorImage_qc.hpp"


#include "quickcheck/quickcheck.hh"

namespace quickcheck
{

/*! \class PngWriterProperty1
 * \brief quickcheck property of %PngWriter that holds for the write methods with a single container
 * \param T type of %ColorImageSignal elements
 * \param col_filename the filename for saving the color images
 * \param gray_filename the filename for saving the grayscale images
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 */
template<typename T>
class PngWriterProperty1 : public Property<slip::ColorImage<T>, slip::GrayscaleImage<T> > {

public:
	typedef Property<slip::ColorImage<T>, slip::GrayscaleImage<T> > base;
	PngWriterProperty1()
	:base(),col_filename(""),gray_filename(""),verbose(0), outstream(&std::cerr)
	{}
	/*!
	 * \brief property definition function
	 * \param x input %ColorImageSignal
	 */
	bool holdsFor(const slip::ColorImage<T> &colim, const slip::GrayscaleImage<T> &grayim){
		bool res = true;
		res = res && def_pngwriter_write_colorimage_property1(col_filename,colim, verbose, *outstream);
		res = res && def_pngwriter_write_grayscaleimage_property1(gray_filename,grayim, verbose, *outstream);
		return res;
	}

	const std::string& get_col_filename() const {
		return col_filename;
	}

	void set_col_filename(const std::string& col_filename) {
		this->col_filename = col_filename;
	}

	const std::string& get_gray_filename() const {
		return gray_filename;
	}

	void set_gray_filename(const std::string& gray_filename) {
		this->gray_filename = gray_filename;
	}

	bool isverbose() const {
		return verbose;
	}

	void set_verbose(bool verbose) {
		this->verbose = verbose;
	}

	void set_outstream(std::ostream * strin) {
		this->outstream = strin;
	}
protected:

	std::string col_filename;//filename for saving the color images
	std::string gray_filename;//filename for saving the grayscale images
	bool verbose;
	std::ostream * outstream;
};

/*! \class PngWriterProperty2
 * \brief quickcheck property of %PngWriter that holds for the write methods with a many container
 * \param T type of %ColorImageSignal elements
 * \param col_filename the filename for saving the color images
 * \param gray_filename the filename for saving the grayscale images
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 */
template<typename T>
class PngWriterProperty2 : public Property<std::vector<slip::ColorImage<T> >, std::vector<slip::GrayscaleImage<T> > > {

public:
	typedef Property<std::vector<slip::ColorImage<T> >, std::vector<slip::GrayscaleImage<T> > > base;
	PngWriterProperty2()
	:base(),col_filename(""),gray_filename(""),verbose(0), outstream(&std::cerr)
	{}
	/*!
	 * \brief property definition function
	 * \param x input %ColorImageSignal
	 */
	bool holdsFor(const std::vector<slip::ColorImage<T> > &colim_list,
			const std::vector<slip::GrayscaleImage<T> > &grayim_list){
		bool res = true;
		res = res && def_pngwriter_write_colorimage_property2(col_filename,colim_list, verbose, *outstream);
		res = res && def_pngwriter_write_grayscaleimage_property2(gray_filename,grayim_list, verbose, *outstream);
		return res;
	}

	const std::string& get_col_filename() const {
		return col_filename;
	}

	void set_col_filename(const std::string& col_filename) {
		this->col_filename = col_filename;
	}

	const std::string& get_gray_filename() const {
		return gray_filename;
	}

	void set_gray_filename(const std::string& gray_filename) {
		this->gray_filename = gray_filename;
	}

	bool isverbose() const {
		return verbose;
	}

	void set_verbose(bool verbose) {
		this->verbose = verbose;
	}

	void set_outstream(std::ostream * strin) {
		this->outstream = strin;
	}

	void generateInput(size_t n, std::vector<slip::ColorImage<T> > &colim_list,
			std::vector<slip::GrayscaleImage<T> > &grayim_list) {
		unsigned int nbim_coul;
		generate(n,nbim_coul);
		nbim_coul ++; //at least one
		slip::ColorImage<T> im_coul;
		generate(n,im_coul);
		while(im_coul.width() == 0 || im_coul.height() == 0){
			generate(n,im_coul);
		}
		colim_list.resize(nbim_coul);
		std::fill_n(colim_list.begin(),nbim_coul,im_coul);
		unsigned int nbim_gray;
		generate(n,nbim_gray);
		nbim_gray ++; //at least one
		slip::GrayscaleImage<T> im_gray;
		generate(n,im_gray);
		while(im_gray.width() == 0 || im_gray.height() == 0)
			generate(n,im_gray);
		grayim_list.resize(nbim_gray);
		std::fill_n(grayim_list.begin(),nbim_gray,im_gray);
	}
protected:

	std::string col_filename;//filename for saving the color images
	std::string gray_filename;//filename for saving the grayscale images
	bool verbose;
	std::ostream * outstream;
};

}//namespace quickcheck

#endif //HAVE_QCHECK



#endif /* PNGWRITER_QC_HPP_ */
