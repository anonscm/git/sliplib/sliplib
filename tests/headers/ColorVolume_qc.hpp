/*!
 ** \file ColorVolume_qc.hpp
 ** \date 2013/03/25
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief Defining properties test functions that could be tested for %ColorVolume class
 ** \note If QuickCheck++ is available, QuickCheck properties are also defined.
 */

#ifndef COLORVOLUME_QC_HPP_
#define COLORVOLUME_QC_HPP_

#include <limits>
#include <functional>

#include "ColorVolume.hpp"

namespace slip
{
template <class T>
class ColorVolume;
}//namespace slip

/*!
 * \brief comparing two %ColorVolume and return true if they are almost equal
 * \param A1 first %ColorVolume to compare
 * \param A2 second %ColorVolume to compare
 * \return true if equal
 */
template <class T>
bool are_colorvolumes_almost_equal(const slip::ColorVolume<T> & A1, const slip::ColorVolume<T> & A2) {
	typedef typename slip::ColorVolume<T>::const_iterator const_it;
	const_it i1 = A1.begin(), i1end = A1.end();
	const_it i2 = A2.begin(), i2end = A2.end();
	while ((i1 != i1end) && (i2 != i2end)) {
		slip::Color<T> diff;
		std::transform(i1->begin(),i1->end(),i2->begin(),diff.begin(),std::minus<T>());
		double norm = static_cast<double>(std::inner_product(diff.begin(),diff.end(),
				diff.begin(),static_cast<T>(0)));
		if (norm > 1e-5) {
			return 0;
		}
		i1++;
		i2++;
	}
	return 1;
}

/*!
 * \brief first constructor property definition function
 * \param d1 first dimension
 * \param d2 second dimension
 * \param d3 third dimension
 * \return true if the property is verified
 */
template<typename size_type, typename T>
bool def_colorvolume_constructor_property1(const size_type & d1, const size_type & d2, const size_type & d3){
	slip::ColorVolume<T> xt(d1,d2,d3);
	return !(xt.empty());
}

/*!
 * \brief second constructor property definition function
 * \param d1 first dimension
 * \param d2 second dimension
 * \param d3 third dimension
 * \param val %Color initial value
 * \return true if the property is verified
 */
template<typename size_type, typename T>
bool def_colorvolume_constructor_property2(const size_type & d1, const size_type & d2, const size_type & d3,
		const slip::Color<T> & val){
	slip::ColorVolume<T> x2(d1,d2,d3,val);
	typename slip::ColorVolume<T>::iterator itx2 = std::find_if(x2.begin(), x2.end(),
			std::bind2nd(std::not_equal_to<slip::Color<T> >(), val));
	return (itx2 == x2.end());

}

/*!
 * \brief third constructor property definition function
 * \param d1 first dimension
 * \param d2 second dimension
 * \param d3 third dimension
 * \param val initial array of %Color values
 * \return true if the property is verified
 */
template<typename size_type, typename T>
bool def_colorvolume_constructor_property3(const size_type & d1, const size_type & d2, const size_type & d3,
		const std::vector<T> & val){
	bool res = true;
	size_type dim = d1*d2*d3;
	slip::Color<T> * array_val1 = new slip::Color<T>[dim];
	for(size_type i=0; i<dim; i++){
		array_val1[i].set_x1(val[3*i]);
		array_val1[i].set_x2(val[3*i + 1]);
		array_val1[i].set_x3(val[3*i + 2]);
	}
	slip::ColorVolume<T> x1(d1,d2,d3,array_val1);
	typedef typename slip::ColorVolume<T>::iterator iter;
	std::pair<iter,slip::Color<T> *> pair_itx1;
	pair_itx1 = std::mismatch(x1.begin(), x1.end(), array_val1);
	res = res && (pair_itx1.first == x1.end());

	size_type full_dim = 3 * dim;
	T* array_val2 = new T[full_dim];
	std::copy(val.begin(),val.end(),array_val2);
	slip::ColorVolume<T> x2(d1,d2,d3,array_val2);
	std::pair<iter,iter> pair_itx2;
	pair_itx2 = std::mismatch(x2.begin(), x2.end(), x1.begin());
	res = res && (pair_itx2.first == x2.end());

	std::vector<slip::Color<T> > vec3(dim);
	std::copy(array_val1,array_val1+dim,vec3.begin());
	slip::ColorVolume<T> x3(d1,d2,d3,vec3.begin(),vec3.end());
	std::pair<iter,iter> pair_itx3;
	pair_itx3 = std::mismatch(x3.begin(), x3.end(), x1.begin());
	res = res && (pair_itx3.first == x3.end());

	std::vector<T> vec4_r, vec4_g, vec4_b;
	for(size_type i=0; i<dim; i++){
		vec4_r.push_back(val[3*i]);
		vec4_g.push_back(val[3*i + 1]);
		vec4_b.push_back(val[3*i + 2]);
	}
	slip::ColorVolume<T> x4(d1,d2,d3,vec4_r.begin(),vec4_r.end(),
			vec4_g.begin(),vec4_b.begin());
	std::pair<iter,iter> pair_itx4;
	pair_itx4 = std::mismatch(x4.begin(), x4.end(), x1.begin());
	res = res && (pair_itx4.first == x4.end());

	slip::ColorVolume<T> x5(x4);
	std::pair<iter,iter> pair_itx5;
	pair_itx5 = std::mismatch(x5.begin(), x5.end(), x4.begin());
	res = res && (pair_itx5.first == x5.end());

	*(x5.begin()) = *(x4.begin()) + slip::Color<T>(1,1,1);
	if (*(x5.begin()) == *(x4.begin()))
		*(x5.begin()) = slip::Color<T>(0,0,0);
	std::pair<iter,iter> pair_itx6;
	pair_itx6 = std::mismatch(x5.begin(), x5.end(), x4.begin());
	res = res && (pair_itx6.first == x5.begin());

	delete[] array_val1;
	delete[] array_val2;

	return res;
}

/*!
 * \brief name property definition function
 * \param V the %ColorVolume we'd like to check the name
 * \return true if the property is verified
 */
template<typename T>
bool def_colorvolume_name_property(const slip::ColorVolume<T>& V){
	return (V.name() == "ColorVolume");
}

/*!
 * \brief affectation property definition function
 * \param V input %ColorVolume
 * \param val %Color value to affect to all elements of v
 * \return true if the property is verified
 */
template<typename T>
bool def_colorvolume_affectation_property(const slip::ColorVolume<T>& V, const slip::Color<T> & val){
	slip::ColorVolume<T> V1(V);
	V1 = val;

	typename slip::ColorVolume<T>::iterator itV1 = std::find_if(V1.begin(), V1.end(),
			std::bind2nd(std::not_equal_to<slip::Color<T> >(), val));

	V1 = val.get_x1();
	typename slip::ColorVolume<T>::iterator itV2 = std::find_if(V1.begin(), V1.end(),
			std::bind2nd(std::not_equal_to<slip::Color<T> >(), slip::Color<T>(val.get_x1(),val.get_x1(),val.get_x1())));

	return ((itV1 == V1.end()) && (itV2 == V1.end()));
}

/*!
 * \brief internal arithmetical operators properties function
 * \param V1in input %ColorVolume
 * \param V2in input %ColorVolume
 * \param valin input %Color value
 * \return true if the property is verified
 */
template<typename T>
bool def_colorvolume_internal_arithmetical_property(const slip::ColorVolume<T>& V1in,
		const slip::ColorVolume<T>& V2in, const slip::Color<T> & valin){

	bool res = true;
	typedef typename slip::ColorVolume<T>::iterator iter;
	slip::Color<T> val(valin);
	slip::ColorVolume<T> V1(V1in);
	slip::ColorVolume<T> V2(V2in);
	slip::ColorVolume<T> Vt(V1);

	Vt += val;
	Vt -= val;
	res = res && are_colorvolumes_almost_equal(Vt,V1);
	Vt *= val;
	Vt /= val;
	res = res && are_colorvolumes_almost_equal(Vt,V1);

	T scal = val.get_x1();

	Vt += scal;
	Vt -= scal;
	res = res && are_colorvolumes_almost_equal(Vt,V1);
	Vt *= val;
	Vt /= val;
	res = res && are_colorvolumes_almost_equal(Vt,V1);

	Vt += V2;
	Vt -= V2;
	res = res && are_colorvolumes_almost_equal(Vt,V1);
	Vt *= V2;
	Vt /= V2;
	res = res && are_colorvolumes_almost_equal(Vt,V1);

	return res;
}

/*!
 * \brief external arithmetical operators properties function
 * \param V1in input %ColorVolume
 * \param V2in input %ColorVolume
 * \param valin input %Color value
 * \return true if the property is verified
 */
template<typename T>
bool def_colorvolume_external_arithmetical_property(const slip::ColorVolume<T>& V1in,
		const slip::ColorVolume<T>& V2in, const slip::Color<T> & valin){

	bool res = true;
	slip::Color<T> val(valin);
	slip::ColorVolume<T> V1(V1in);
	slip::ColorVolume<T> V2(V2in);
	slip::ColorVolume<T> Vt(V1);
	slip::ColorVolume<T> Vtt(V1);

	Vt = V1 + val;
	Vtt = val + V1;
	res = res && are_colorvolumes_almost_equal(Vt,Vtt);
	Vt = Vtt - val;
	res = res && are_colorvolumes_almost_equal(Vt,V1);
	Vt = V1 * val;
	Vtt = val * V1;
	res = res && are_colorvolumes_almost_equal(Vt,Vtt);
	Vt = Vtt / val;
	res = res && are_colorvolumes_almost_equal(Vt,V1);

	T scal = val.get_x1();

	Vt = V1 + scal;
	Vtt = scal + V1;
	res = res && are_colorvolumes_almost_equal(Vt,Vtt);
	Vt = Vtt - scal;
	res = res && are_colorvolumes_almost_equal(Vt,V1);
	Vt = V1 * val;
	Vtt = val * V1;
	res = res && are_colorvolumes_almost_equal(Vt,Vtt);
	Vt = Vtt / val;
	res = res && are_colorvolumes_almost_equal(Vt,V1);

	Vt = V1 + V2;
	Vtt = V2 + V1;
	res = res && are_colorvolumes_almost_equal(Vt,Vtt);
	Vt = Vtt - V2;
	res = res && are_colorvolumes_almost_equal(Vt,V1);
	Vt = V1 * V2;
	Vtt = V2 * V1;
	res = res && are_colorvolumes_almost_equal(Vt,Vtt);
	Vt = Vtt / V2;
	res = res && are_colorvolumes_almost_equal(Vt,V1);

	return res;
}

//If QuickCheck++ is available, the properties functions are encapsulated in quickcheck properties.
#ifdef HAVE_QCHECK

namespace quickcheck
{
/*!
 * \brief quickcheck generate function for %ColorVolume
 * \param n the quickcheck size hint
 * \param out the %ColorVolume to generate
 */
template<typename T>
void generate(size_t n, slip::ColorVolume<T>& out);
}//namespace quickcheck

#include "Color_qc.hpp"
#include "quickcheck/quickcheck.hh"

namespace quickcheck
{

template<typename T>
void generate(size_t n, slip::ColorVolume<T>& out){
	unsigned d1,d2,d3;
	generate(n, d1);
	generate(n, d2);
	generate(n, d3);
	// unsigned sumd = d1 + d2 + d3;
	unsigned prodd = d1 * d2 * d3;
	if (prodd == 0){
		if (d1 == 0)
			d1++;
		if (d2 == 0)
			d2++;
		if (d3 == 0)
			d3++;
	}
	out.resize(d1,d2,d3);
	for (size_t i = 0; i < d1; ++i)
		for (size_t j = 0; j < d2; ++j)
			for (size_t k = 0; k < d3; ++k)
			{
				slip::Color<T> a;
				generate(n, a);
				out(i,j,k) = a;
			}
}

/*! \class ConstructorProperty1
 * \brief quickcheck property of %ColorVolume that holds for the constructor with
 * three dimension parameters
 * \param size_type type of %ColorVolume dimensions
 * \param T type of %ColorVolume elements
 */
template<typename size_type, class T>
class ColorVolumeConstructorProperty1 : public Property<slip::ColorVolume<T> > {
	/*!
	 * \brief property definition function
	 * \param x input %ColorVolume (it is not used by this test)
	 */
	bool holdsFor(const slip::ColorVolume<T> &x){
		return def_colorvolume_constructor_property1<size_type,T>(x.dim1(),x.dim2(),x.dim3());
	}
};

/*! \class ConstructorProperty2
 * \brief quickcheck property of %ColorVolume that holds for the constructors with
 * three dimension parameters and an initial %Color value
 * \param size_type type of %ColorVolume dimensions
 * \param T type of %ColorVolume elements
 */
template<typename size_type, class T>
class ColorVolumeConstructorProperty2 : public Property<slip::Color<T>,slip::ColorVolume<T> > {
	/*!
	 * \brief property definition function
	 * \param val %Color initial value
	 * \param x input %ColorVolume (it is not used by this test)
	 */
	bool holdsFor(const slip::Color<T> &val, const slip::ColorVolume<T> &x){
		return def_colorvolume_constructor_property2<size_type,T>(x.dim1(),x.dim2(),x.dim3(),val);
	}
	/*!
	 * \brief Inputs generation method
	 * \param n the quickcheck size hint
	 * \param val %Color initial value
	 * \param x input %ColorVolume (it is not used by this test)
	 */
	void generateInput(size_t n, slip::Color<T> &val, slip::ColorVolume<T> &x) {
		generate(n,x);
		generate(n,val);
	}
};

/*! \class ConstructorProperty3
 * \brief quickcheck property of %ColorVolume that holds for the constructors with
 * three dimension parameters and initial arrays of %Color values.
 * \param size_type type of %ColorVolume dimensions
 * \param T type of %ColorVolume elements
 */
template<typename size_type, class T>
class ColorVolumeConstructorProperty3 : public Property<std::vector<T>,slip::ColorVolume<T> > {
	/*!
	 * \brief property definition function
	 * \param val initial array of %Color values
	 * \param x input %ColorVolume (it is not used by this test)
	 */
	bool holdsFor(const std::vector<T> & val, const slip::ColorVolume<T> &x){
		return def_colorvolume_constructor_property3<size_type,T>(x.dim1(),x.dim2(),x.dim3(),val);
	}
	/*!
	 * \brief Inputs generation method
	 * \param n the quickcheck size hint
	 * \param initial array of %Color values
	 * \param x input %ColorVolume (it is not used by this test)
	 */
	void generateInput(size_t n, std::vector<T> & val, slip::ColorVolume<T> &x) {
		generate(n, x);
		size_type dim = x.size();
		for (size_type i = 0; i < dim; i++) {
			slip::Color<T> a;
			generate(n - 1, a);
			val.push_back(a.get_x1());
			val.push_back(a.get_x2());
			val.push_back(a.get_x3());
		}
	}
};

/*! \class NameProperty
 * \brief quickcheck property of %ColorVolume that holds for the name returned by the name() method.
 * \param T type of %ColorVolume elements
 */
template<class T>
class ColorVolumeNameProperty : public Property<slip::ColorVolume<T> > {
	bool holdsFor(const slip::ColorVolume<T> &x){
		return def_colorvolume_name_property<T>(x);
	}
};

/*! \class AffectationProperty
 * \brief quickcheck property of %ColorVolume that holds for the affectation method.
 * \param T type of %ColorVolume elements
 */
template<class T>
class ColorVolumeAffectationProperty : public Property<slip::ColorVolume<T>, slip::Color<T> > {
	bool holdsFor(const slip::ColorVolume<T> &x, const slip::Color<T> & val){
		return def_colorvolume_affectation_property<T>(x,val);
	}
};

/*! \class InternalArithmeticalProperty
 * \brief quickcheck property of %ColorVolume that holds for the internal arithmetical operators.
 * \param T type of %ColorVolume elements
 */
template<class T>
class ColorVolumeInternalArithmeticalProperty : public Property<slip::ColorVolume<T>, slip::ColorVolume<T>,
slip::Color<T> > {
	bool holdsFor(const slip::ColorVolume<T>& V1,
			const slip::ColorVolume<T>& V2, const slip::Color<T> & val){
		return def_colorvolume_internal_arithmetical_property<T>(V1,V2,val);
	}
	/*!
	 * \brief Inputs generation method
	 * \param n the quickcheck size hint
	 * \param V1 input %ColorVolume
	 * \param V2 input %ColorVolume
	 * \param val input %Color value
	 */
	void generateInput(size_t n, slip::ColorVolume<T>& V1, slip::ColorVolume<T>& V2,
			slip::Color<T> & val) {
		generate(n, V1);
		T r(static_cast<T>(0)),g(static_cast<T>(0)),b(static_cast<T>(0));
		while(r == static_cast<T>(0)){
			generate(n,r);
		}
		while(g == static_cast<T>(0)){
			generate(n,g);
		}
		while(b == static_cast<T>(0)){
			generate(n,b);
		}
		val = slip::Color<T>(r,g,b);
		V2.resize(V1.dim1(),V1.dim2(),V1.dim3());
		for (size_t i = 0; i < V1.dim1(); ++i)
			for (size_t j = 0; j < V1.dim2(); ++j)
				for (size_t k = 0; k < V1.dim3(); ++k)
				{
					T r2(static_cast<T>(0)),g2(static_cast<T>(0)),b2(static_cast<T>(0));
					while(r2 == static_cast<T>(0)){
						generate(n,r2);
					}
					while(g2 == static_cast<T>(0)){
						generate(n,g2);
					}
					while(b2 == static_cast<T>(0)){
						generate(n,b2);
					}
					slip::Color<T> a(r2,g2,b2);
					V2(i,j,k) = a;
				}
	}
};

/*! \class ExternalArithmeticalProperty
 * \brief quickcheck property of %ColorVolume that holds for the external arithmetical operators.
 * \param T type of %ColorVolume elements
 */
template<class T>
class ColorVolumeExternalArithmeticalProperty : public Property<slip::ColorVolume<T>, slip::ColorVolume<T>,
slip::Color<T> > {
	bool holdsFor(const slip::ColorVolume<T>& V1,
			const slip::ColorVolume<T>& V2, const slip::Color<T> & val){
		return def_colorvolume_external_arithmetical_property<T>(V1,V2,val);
	}
	/*!
	 * \brief Inputs generation method
	 * \param n the quickcheck size hint
	 * \param V1 input %ColorVolume
	 * \param V2 input %ColorVolume
	 * \param val input %Color value
	 */
	void generateInput(size_t n, slip::ColorVolume<T>& V1, slip::ColorVolume<T>& V2,
			slip::Color<T> & val) {
		generate(n, V1);
		T r(static_cast<T>(0)),g(static_cast<T>(0)),b(static_cast<T>(0));
		while(r == static_cast<T>(0)){
			generate(n,r);
		}
		while(g == static_cast<T>(0)){
			generate(n,g);
		}
		while(b == static_cast<T>(0)){
			generate(n,b);
		}
		val = slip::Color<T>(r,g,b);
		V2.resize(V1.dim1(),V1.dim2(),V1.dim3());
		for (size_t i = 0; i < V1.dim1(); ++i)
			for (size_t j = 0; j < V1.dim2(); ++j)
				for (size_t k = 0; k < V1.dim3(); ++k)
				{
					T r2(static_cast<T>(0)),g2(static_cast<T>(0)),b2(static_cast<T>(0));
					while(r2 == static_cast<T>(0)){
						generate(n,r2);
					}
					while(g2 == static_cast<T>(0)){
						generate(n,g2);
					}
					while(b2 == static_cast<T>(0)){
						generate(n,b2);
					}
					slip::Color<T> a(r2,g2,b2);
					V2(i,j,k) = a;
				}
	}
};

}//namespace quickcheck


#endif //HAVE_QCHECK

#endif /* COLORVOLUME_QC_HPP_ */
