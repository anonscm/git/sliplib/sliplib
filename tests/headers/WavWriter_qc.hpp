/*!
 ** \file WavWriter_qc.hpp
 ** \brief Properties functions for the %WavWriter class.
 ** Those functions are used by unit testing.
 ** \date 2013/04/19
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef WAVWRITER_QC_HPP_
#define WAVWRITER_QC_HPP_

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>
#include "Vector.hpp"
#include "WavWriter.hpp"
#include "utils_compilation.hpp"

namespace slip
{
template<class Container1d, typename T>
class WavWriter;
}//namespace slip

/*!
 * \brief properties of the %WavWriters for single audio vector saving
 * \param filename of output file
 * \param wave file header
 * \param vector to save
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_wavwriter_write_single_property(std::string filename,const WAVE_HEADER & file_header,
		const slip::Vector<T> & audio, bool verbose = 0, std::ostream & outstream = std::cerr){

	std::size_t size = audio.size();
	slip::WavWriter<slip::Vector<T>,T> writer;
	try{
		writer.initialize(file_header,size);
	}
	catch(slip::slip_exception &e){
		std::ostringstream err;
		err << "slip::" << "WavWriter::initialize()::" << slip::FILE_OPEN_ERROR;
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << std::endl;
				outstream << "->" << e.what_str() << std::endl;
			}
			return 0;
		}
	}
	writer.set_output_filename(filename);

	if(size == 0){
		try{
			writer.initialize(file_header, size);
		}
		catch(slip::slip_exception &e){
			std::ostringstream err;
			err << "slip::" << "JpegWriter::initialize()::" << slip::FILE_WRITE_ERROR
					<< filename << " | JPEG code has signaled the error : Empty JPEG image (DNL not supported)";
			if (err.str().compare(e.what_str()) != 0){
				if(verbose){
					outstream << "->" << err.str() << std::endl;
					outstream << "->" << e.what_str() << std::endl;
				}
				return 0;
			}
		}
	}else{
		writer.initialize(file_header, size);
		writer.write(audio);
		writer.release();
		try{
			writer.write(audio);
		}
		catch(slip::slip_exception &e){
			std::ostringstream err;
			err << "slip::" << "WavWriter::write()::" << slip::FILE_WRITE_ERROR
					<< filename << " | The writer to be initialized before writing.";
			std::string mess = e.what_str();
			if (err.str().compare(mess) != 0){
				if(verbose){
					outstream << "->" << err.str() << std::endl;
					outstream << "->" << mess << std::endl;
				}
				return 0;
			}
		}
	}
	writer.release();
	return 1;
}

/*!
 * \brief properties of the %WavWriters for many audio vectors saving into a single file
 * \param filename of output file
 * \param wave file header
 * \param audio_list list of audio vectors to save:
 * 		- the list should have at least one element
 * 		- all the elements of the list should have the same dimension
 * 		.
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_wavwriter_write_many_property(std::string filename,const WAVE_HEADER & file_header,
		const std::vector<slip::Vector<T> >& audio_list,
				       bool UNUSED(verbose) = 0, std::ostream & UNUSED(outstream) = std::cerr){
	std::size_t list_size = audio_list.size();
	std::size_t size = audio_list.begin()->size()  * list_size;
	slip::WavWriter<slip::Vector<T>,T> writer(filename,file_header,size);
	typename std::vector<slip::Vector<T> >::const_iterator it_l = audio_list.begin();
	while(writer.write(*it_l)){it_l++;};
	return 1;
}


//If QuickCheck++ is available, the properties functions are encapsulated in quickcheck properties.
#ifdef HAVE_QCHECK

namespace quickcheck
{
template<typename T>
void generate(size_t n, slip::Vector<T>& out);
/*!
 * \brief quickcheck generate function for %WAVE_HEADER
 * \param n the quickcheck size hint
 * \param out the %WAVE_HEADER to generate
 */
void generate(size_t n, WAVE_HEADER& out);
}//namespace quickcheck

#include "Vector_qc.hpp"
#include "quickcheck/quickcheck.hh"

namespace quickcheck
{

inline
void generate(size_t n, WAVE_HEADER& out)
{
	int16 possible_chan[6] = {1,2,3,4,5,6};
	int32 possible_srate[5] = {11025,22050,44100,48000,96000};
	int16 possible_bps[3] = {8,16,32};
	out.format_header.format = 1;
	out.format_header.num_channels = oneOf<int16,int16*>(possible_chan,possible_chan+6);
	out.format_header.sample_rate = oneOf<int32,int32*>(possible_srate,possible_srate+5);
	out.format_header.bits_per_sample = oneOf<int16,int16*>(possible_bps,possible_bps+3);
}

/*! \class WavWriterProperty1
 * \brief quickcheck property of %WavWriter that holds for the write methods with a single container
 * \param T type of %Vector elements
 * \param filename the filename for saving the audio vector
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 */
template<typename T>
class WavWriterProperty1 : public Property<slip::Vector<T>, WAVE_HEADER > {

public:
	typedef Property<slip::Vector<T>, WAVE_HEADER > base;
	WavWriterProperty1()
	:base(),filename(""),verbose(0),outstream(&std::cerr)
	{}
	/*!
	 * \brief property definition function
	 * \param audio input %Vector
	 */
	bool holdsFor(const slip::Vector<T> &audio, const WAVE_HEADER &head){
		return def_wavwriter_write_single_property(filename,head,audio,verbose,*outstream);
	}

	const std::string& get_filename() const {
		return filename;
	}

	void set_filename(const std::string& filename) {
		this->filename = filename;
	}

	bool isverbose() const {
		return verbose;
	}

	void set_verbose(bool verbose) {
		this->verbose = verbose;
	}

	void set_outstream(std::ostream * strin) {
		this->outstream = strin;
	}
protected:

	std::string filename;//filename for saving the audio
	bool verbose;
	std::ostream * outstream;
};

/*! \class WavWriterProperty2
 * \brief quickcheck property of %WavWriter that holds for the write methods with a many container
 * \param T type of %Vector elements
 * \param filename the filename for saving the audio vectors
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 */
template<typename T>
class WavWriterProperty2 : public Property<std::vector<slip::Vector<T> >, WAVE_HEADER> {

public:
	typedef Property<std::vector<slip::Vector<T> >, WAVE_HEADER> base;
	WavWriterProperty2()
	:base(),filename(""),verbose(0), outstream(&std::cerr)
	{}
	/*!
	 * \brief property definition function
	 * \param x input %ColorImageSignal
	 */
	bool holdsFor(const std::vector<slip::Vector<T> > & audio_list, const WAVE_HEADER & head){
		return def_wavwriter_write_many_property(filename,head,audio_list,verbose,*outstream);
	}

	const std::string& get_filename() const {
		return filename;
	}

	void set_filename(const std::string& filename) {
		this->filename = filename;
	}

	bool isverbose() const {
		return verbose;
	}

	void set_verbose(bool verbose) {
		this->verbose = verbose;
	}

	void set_outstream(std::ostream * strin) {
		this->outstream = strin;
	}

	void generateInput(size_t n, std::vector<slip::Vector<T> > &audio_list, WAVE_HEADER & head) {
		generate(n,head);
		unsigned int nb_container;
		generate(n,nb_container);
		nb_container ++; //at least one
		slip::Vector<T> audio;
		generate(n,audio);
		while(audio.size() == 0){
			generate(n,audio);
		}
		audio_list.resize(nb_container);
		std::fill_n(audio_list.begin(),nb_container,audio);
	}
protected:

	std::string filename;//filename for saving the audio vectors
	bool verbose;
	std::ostream * outstream;
};

}//namespace quickcheck

#endif //HAVE_QCHECK


#endif /* WAVWRITER_QC_HPP_ */
