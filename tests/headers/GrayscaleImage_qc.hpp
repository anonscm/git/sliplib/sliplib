/*!
 ** \file GrayscaleImage_qc.hpp
 ** \brief Defining properties test functions that could be tested for %GrayscaleImage class
 ** \date 2013/04/11
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef GRAYSCALEIMAGE_QC_HPP_
#define GRAYSCALEIMAGE_QC_HPP_

#include "GrayscaleImage.hpp"

namespace slip
{
template <class T>
class GrayscaleImage;
}//namespace slip


#ifdef HAVE_QCHECK

namespace quickcheck
{
/*!
 * \brief quickcheck generate function for %GrayscaleImage
 * \param n the quickcheck size hint
 * \param out the %GrayscaleImage to generate
 */
template<typename T>
void generate(size_t n, slip::GrayscaleImage<T>& out);
}//namespace quickcheck


#include "quickcheck/quickcheck.hh"

namespace quickcheck
{
template<typename T>
inline
void generate(size_t n, slip::GrayscaleImage<T>& out)
{
	unsigned d1,d2;
	generate(n, d1);
	generate(n, d2);
	unsigned prodd = d1 * d2;
	if (prodd > 0){
		out.resize(d1,d2);
		for (size_t i = 0; i < d1; ++i)
			for (size_t j = 0; j < d2; ++j)
			{
				T a;
				generate(n, a);
				out(i,j) = a;
			}
	}
}
}//namespace quickcheck

#endif //HAVE_QCHECK

#endif /* GRAYSCALEIMAGE_QC_HPP_ */
