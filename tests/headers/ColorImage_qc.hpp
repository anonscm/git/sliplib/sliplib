/*!
 ** \file ColorImage_qc.hpp
 ** \brief Defining properties test functions that could be tested for %ColorImage class
 ** \date 2013/04/11
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>

 */

#ifndef COLORIMAGE_QC_HPP_
#define COLORIMAGE_QC_HPP_

#include "ColorImage.hpp"

namespace slip
{
template <class T>
class ColorImage;
}//namespace slip

#ifdef HAVE_QCHECK

namespace quickcheck
{
/*!
 * \brief quickcheck generate function for %ColorImage
 * \param n the quickcheck size hint
 * \param out the %ColorImage to generate
 */
template<typename T>
void generate(size_t n, slip::ColorImage<T>& out);
}//namespace quickcheck

#include "Color_qc.hpp"


#include "quickcheck/quickcheck.hh"

namespace quickcheck
{
template<typename T>
inline
void generate(size_t n, slip::ColorImage<T>& out)
{
	unsigned d1,d2;
	generate(n, d1);
	generate(n, d2);
	unsigned prodd = d1 * d2;
	if (prodd > 0){
		out.resize(d1,d2);
		for (size_t i = 0; i < d1; ++i)
			for (size_t j = 0; j < d2; ++j)
			{
				slip::Color<T> a;
				generate(n, a);
				out(i,j) = a;
			}
	}
}
}//namespace quickcheck

#endif //HAVE_QCHECK

#endif /* COLORIMAGE_QC_HPP_ */
