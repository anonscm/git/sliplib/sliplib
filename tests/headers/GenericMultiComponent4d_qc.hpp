/*!
 ** \file GenericMultiComponent4d_qc.hpp
 ** \date 2013/08/21
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief Defining properties test functions that could be tested for %GenericMultiComponent4d class
 ** \note If QuickCheck++ is available, QuickCheck properties are also defined.
 */

#ifndef GENRICMULBLOCKICOMPONENT4D_QC_HPP_
#define GENRICMULBLOCKICOMPONENT4D_QC_HPP_

#include <limits>
#include <functional>
#include <vector>

#include "GenericMultiComponent4d.hpp"

namespace slip
{
template <class BLOCK>
class GenericMultiComponent4d;
}//namespace slip

/*!
 * \brief comparing two %GenericMultiComponent4d and return true if they are almost equal
 * \param A1 first %GenericMultiComponent4d to compare
 * \param A2 second %GenericMultiComponent4d to compare
 * \return true if equal
 */
template <class BLOCK>
bool are_genericmulticomponent4d_almost_equal(const slip::GenericMultiComponent4d<BLOCK> & A1, const slip::GenericMultiComponent4d<BLOCK> & A2) {
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_iterator const_it;
	const_it i1 = A1.begin(), i1end = A1.end();
	const_it i2 = A2.begin(), i2end = A2.end();
	typedef typename BLOCK::value_type T;
	while ((i1 != i1end) && (i2 != i2end)) {
		BLOCK diff;
		std::transform(i1->begin(),i1->end(),i2->begin(),diff.begin(),std::minus<T>());
		double norm = static_cast<double>(std::inner_product(diff.begin(),diff.end(),
				diff.begin(),static_cast<T>(0)));
		if (norm > 1e-5) {
			return 0;
		}
	}
	return 1;
}

/*!
 * \brief first constructor property definition function
 * \param d1 first dimension
 * \param d2 second dimension
 * \param d3 third dimension
 * \param d4 fourth dimension
 * \return true if the property is verified
 */
template<typename size_type, typename BLOCK>
bool def_genericmulticomponent4d_constructor_property1(const size_type & d1, const size_type & d2,
		const size_type & d3, const size_type & d4){
slip::GenericMultiComponent4d<BLOCK> xt(d1,d2,d3,d4);
	return !(xt.empty());
}

/*!
 * \brief second constructor property definition function
 * \param d1 first dimension
 * \param d2 second dimension
 * \param d3 third dimension
 * \param d4 fourth dimension
 * \param val initial value
 * \return true if the property is verified
 */
template<typename size_type, typename BLOCK>
bool def_genericmulticomponent4d_constructor_property2(const size_type & d1, const size_type & d2,
		const size_type & d3, const size_type & d4, const BLOCK & val){
	slip::GenericMultiComponent4d<BLOCK> x2(d1,d2,d3,d4,val);
	typename slip::GenericMultiComponent4d<BLOCK>::iterator itx2 = std::find_if(x2.begin(), x2.end(),
			std::bind2nd(std::not_equal_to<BLOCK>(), val));
	return (itx2 == x2.end());
}

/*!
 * \brief third constructor property definition function
 * \param d1 first dimension
 * \param d2 second dimension
 * \param d3 third dimension
 * \param d4 fourth dimension
 * \param val initial array of BLOCK values
 * \return true if the property is verified
 */
template<typename size_type, typename BLOCK, typename T>
bool def_genericmulticomponent4d_constructor_property3(const size_type & d1, const size_type & d2,
		const size_type & d3, const size_type & d4, const std::vector<T> & val){
	bool res = true;
	size_type dim = d1*d2*d3*d4;
	size_type b_size = static_cast<size_type>(BLOCK::SIZE);
	BLOCK * array = new BLOCK[dim];
	for(size_type i=0; i<dim; i++){
		for(size_type j=0; j<b_size;++j){
			(array[i])[j] = val[b_size*i + j];
		}
	}
	slip::GenericMultiComponent4d<BLOCK> x1(d1,d2,d3,d4,array);
	typedef typename slip::GenericMultiComponent4d<BLOCK>::iterator iter;
	std::pair<iter,BLOCK*> pair_itx1;
	pair_itx1 = std::mismatch(x1.begin(), x1.end(), array);
	res = res && (pair_itx1.first == x1.end());

	std::vector<BLOCK> vec3(dim);
	std::copy(array,array+dim,vec3.begin());
	slip::GenericMultiComponent4d<BLOCK> x3(d1,d2,d3,d4,vec3.begin(),vec3.end());
	std::pair<iter,iter> pair_itx3;
	pair_itx3 = std::mismatch(x3.begin(), x3.end(), x1.begin());
	res = res && (pair_itx3.first == x3.end());

	slip::GenericMultiComponent4d<BLOCK> x5(x3);
	std::pair<iter,iter> pair_itx5;
	pair_itx5 = std::mismatch(x5.begin(), x5.end(), x3.begin());
	res = res && (pair_itx5.first == x5.end());

	BLOCK inc,zer;
	for(size_type j=0; j<b_size;++j){
		inc[j] = T(1);
		zer[j] = T(0);
	}
	*(x5.begin()) = *(x3.begin()) + inc;
	if (*(x5.begin()) == *(x3.begin()))
		*(x5.begin()) = zer;
	std::pair<iter,iter> pair_itx6;
	pair_itx6 = std::mismatch(x5.begin(), x5.end(), x3.begin());
	res = res && (pair_itx6.first == x5.begin());

	T * array_val = new T[val.size()];
	std::copy(val.begin(),val.end(),array_val);
	slip::GenericMultiComponent4d<BLOCK> x7(d1,d2,d3,d4,array_val);
	std::pair<iter,iter> pair_itx7;
	pair_itx7 = std::mismatch(x7.begin(), x7.end(), x1.begin());
	res = res && (pair_itx7.first == x7.end());
	if(!res)
	{
		std::cerr << x1;
		std::cerr << std::endl << " != " << std::endl;
		std::cerr << x7;
		std::cerr << std::endl;
	}
	delete[] array_val;
	delete[] array;
	return res;
}

/*!
 * \brief third fill and assign properties definition function
 * \param V input %GenericMultiComponent4d
 * \return true if the properties are verified
 */
template<typename BLOCK>
bool def_genericmulticomponent4d_assign_fill_property(const slip::GenericMultiComponent4d<BLOCK> & V){
	bool res = true;
	typedef typename BLOCK::value_type T;
	std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
	slip::GenericMultiComponent4d<BLOCK> A;
	A = V;
	res = res && (A==V);
	BLOCK val;
	for(std::size_t j=0; j<b_size;++j){
		val[j] = T(1);
	}
	A.fill(val);
	slip::GenericMultiComponent4d<BLOCK> A2(V);
	A2 = val;
	res = res && (A==A2);

	std::size_t dim = V.dim1()*V.dim2()*V.dim3()*V.dim4();
	BLOCK * array = new BLOCK[dim];
	std::fill(array,array+dim,val);
	A.fill(array,array+dim);
	typedef typename slip::GenericMultiComponent4d<BLOCK>::iterator iter;
	std::pair<iter,BLOCK*> pair_itA;
	pair_itA = std::mismatch(A.begin(), A.end(), array);
	res = res && (pair_itA.first == A.end());
	delete[] array;
	return res;
}


/*!
 * \brief resize property definition function
 * \param V input %GenericMultiComponent4d
 * \param d1 new first dimension
 * \param d2 new second dimension
 * \param d3 new third dimension
 * \param d4 fourth dimension
 * \return true if the property is verified
 */
template<typename BLOCK>
bool def_genericmulticomponent4d_resize_property(const slip::GenericMultiComponent4d<BLOCK>& V, const std::size_t & d1, const std::size_t & d2,
		const std::size_t & d3, const std::size_t & d4){
	typedef typename BLOCK::value_type T;
	std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
	slip::GenericMultiComponent4d<BLOCK> W(V);
	W.resize(d1,d2,d3,d4);
	bool res = ((W.dim1() == d1) && (W.slabs() == d1) &&
			(W.dim2() == d2) && (W.slices() == d2) &&
			(W.dim3() == d3) && (W.rows() == d3) &&
			(W.dim4() == d4) && (W.cols() == d4) && (W.columns() == d4) &&
			(W.size() == d1*d2*d3*d4) && b_size == slip::GenericMultiComponent4d<BLOCK>::COMPONENTS
			&& 4 == slip::GenericMultiComponent4d<BLOCK>::DIM);

	W.resize(V.dim1(),V.dim2(),V.dim3(),V.dim4());
	for(std::size_t t=0; t<W.dim1(); ++t)
		for(std::size_t k=0; k<W.dim2(); ++k)
			for(std::size_t i=0; i<W.dim3(); ++i)
				for(std::size_t j=0; j<W.dim4(); ++j){
					for(std::size_t c=0; c<b_size;++c){
						W[t][k][i][j][c] = V[t][k][i][j][c];
					}
				}
	res = res && (W == V);

	W.resize(V.dim4(),V.dim3(),V.dim2(),V.dim1());
	for(std::size_t t=0; t<W.dim1(); ++t)
		for(std::size_t k=0; k<W.dim2(); ++k)
			for(std::size_t i=0; i<W.dim3(); ++i)
				for(std::size_t j=0; j<W.dim4(); ++j){
					for(std::size_t c=0; c<b_size;++c){
						W(t,k,i,j)[c] = V(j,i,k,t)[c];
					}
					res = res && (W(t,k,i,j) == V(j,i,k,t));
				}

	return res;
}

/*!
 * \brief swap method property and comparison operator properties function
 * \param V1 %GenericMultiComponent4d
 * \param V2 %GenericMultiComponent4d
 * \return true if the properties are verified
 */
template<typename BLOCK>
bool def_genericmulticomponent4d_swap_comparison_property(const slip::GenericMultiComponent4d<BLOCK>& V1,
		const slip::GenericMultiComponent4d<BLOCK>& V2){
	bool res = true;
	typedef typename BLOCK::value_type T;
	std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
	slip::GenericMultiComponent4d<BLOCK> V1t(V1);
	slip::GenericMultiComponent4d<BLOCK> V2t(V2);
	V1t.swap(V2t);
	V1t.swap(V2t);
	res = res && (V1 == V1t) && (V2 == V2t);
	BLOCK val;
	for(std::size_t j=0; j<b_size;++j){
		val[j] = T(1);
	}
	V1t.fill(val);
	std::transform(V1t.begin(),V1t.end(),V1.begin(),V1t.begin(),std::plus<BLOCK>());
	res = res && (V1 != V1t);
	return res;
}

/*!
 * \brief external 1d iterators properties function
 * \param CV input %GenericMultiComponent4d
 * \return true if the property is verified
 */
template<typename BLOCK>
bool def_genericmulticomponent4d_iterators1d_property(const slip::GenericMultiComponent4d<BLOCK>& VC){
	bool res = true;
	int step = 1;
	slip::GenericMultiComponent4d<BLOCK> V(VC);
	std::size_t d1 = V.dim1();
	std::size_t d2 = V.dim2();
	std::size_t d3 = V.dim3();
	std::size_t d4 = V.dim4();

	//global one dimensional iterators

	typedef typename slip::GenericMultiComponent4d<BLOCK>::iterator iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_iterator const_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_iterator riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_iterator const_riter;

	std::pair<iter,const_iter> pair_it1;
	pair_it1 = std::mismatch(V.begin(), V.end(), VC.begin());
	res = res && (pair_it1.first == V.end());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_iter,iter> pair_it2;
	pair_it2 = std::mismatch(VC.begin(), VC.end(), V.begin());
	res = res && (pair_it2.first == VC.end());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	std::pair<riter,const_riter> pair_it3;
	pair_it3 = std::mismatch(V.rbegin(), V.rend(), VC.rbegin());
	res = res && (pair_it3.first == V.rend());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_riter,riter> pair_it4;
	pair_it4 = std::mismatch(VC.rbegin(), VC.rend(), V.rbegin());
	res = res && (pair_it4.first == VC.rend());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	//slab, slice, row and col iterator
	typedef typename slip::GenericMultiComponent4d<BLOCK>::slab_iterator slab_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_slab_iterator const_slab_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::slice_iterator slice_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_slice_iterator const_slice_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::row_iterator row_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_row_iterator const_row_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::col_iterator col_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_col_iterator const_col_iter;

	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_slab_iterator slab_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_slab_iterator const_slab_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_slice_iterator slice_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_slice_iterator const_slice_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_row_iterator row_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_row_iterator const_row_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_col_iterator col_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_col_iterator const_col_riter;


	if(d1>0 && d2>0 && d3>0 && d4>0){
		std::pair<slab_iter,const_slab_iter> pair_it5;
		pair_it5 = std::mismatch(V.slab_begin(d2-1,d3-1,d4-1), V.slab_end(d2-1,d3-1,d4-1), VC.slab_begin(d2-1,d3-1,d4-1));
		res = res && (pair_it5.first == V.slab_end(d2-1,d3-1,d4-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;//5
		}
		step++;
		std::pair<const_slab_iter,slab_iter> pair_it6;
		pair_it6 = std::mismatch(VC.slab_begin(d2-1,d3-1,d4-1), VC.slab_end(d2-1,d3-1,d4-1), V.slab_begin(d2-1,d3-1,d4-1));
		res = res && (pair_it6.first == VC.slab_end(d2-1,d3-1,d4-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		std::pair<slab_riter,const_slab_riter> pair_it7;
		pair_it7 = std::mismatch(V.slab_rbegin(d2-1,d3-1,d4-1), V.slab_rend(d2-1,d3-1,d4-1), VC.slab_rbegin(d2-1,d3-1,d4-1));
		res = res && (pair_it7.first == V.slab_rend(d2-1,d3-1,d4-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_slab_riter,slab_riter> pair_it8;
		pair_it8 = std::mismatch(VC.slab_rbegin(d2-1,d3-1,d4-1), VC.slab_rend(d2-1,d3-1,d4-1), V.slab_rbegin(d2-1,d3-1,d4-1));
		res = res && (pair_it8.first == VC.slab_rend(d2-1,d3-1,d4-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		std::pair<slice_iter,const_slice_iter> pair_it9;
		pair_it9 = std::mismatch(V.slice_begin(d1-1,d3-1,d4-1), V.slice_end(d1-1,d3-1,d4-1), VC.slice_begin(d1-1,d3-1,d4-1));
		res = res && (pair_it9.first == V.slice_end(d1-1,d3-1,d4-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_slice_iter,slice_iter> pair_it10;
		pair_it10 = std::mismatch(VC.slice_begin(d1-1,d3-1,d4-1), VC.slice_end(d1-1,d3-1,d4-1), V.slice_begin(d1-1,d3-1,d4-1));
		res = res && (pair_it10.first == VC.slice_end(d1-1,d3-1,d4-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;//10
		}
		step++;

		std::pair<slice_riter,const_slice_riter> pair_it11;
		pair_it11 = std::mismatch(V.slice_rbegin(d1-1,d3-1,d4-1), V.slice_rend(d1-1,d3-1,d4-1), VC.slice_rbegin(d1-1,d3-1,d4-1));
		res = res && (pair_it11.first == V.slice_rend(d1-1,d3-1,d4-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_slice_riter,slice_riter> pair_it12;
		pair_it12 = std::mismatch(VC.slice_rbegin(d1-1,d3-1,d4-1), VC.slice_rend(d1-1,d3-1,d4-1), V.slice_rbegin(d1-1,d3-1,d4-1));
		res = res && (pair_it12.first == VC.slice_rend(d1-1,d3-1,d4-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		std::pair<row_iter,const_row_iter> pair_it13;
		pair_it13 = std::mismatch(V.row_begin(d1-1,d2-1,d3-1), V.row_end(d1-1,d2-1,d3-1), VC.row_begin(d1-1,d2-1,d3-1));
		res = res && (pair_it13.first == V.row_end(d1-1,d2-1,d3-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_row_iter,row_iter> pair_it14;
		pair_it14 = std::mismatch(VC.row_begin(d1-1,d2-1,d3-1), VC.row_end(d1-1,d2-1,d3-1), V.row_begin(d1-1,d2-1,d3-1));
		res = res && (pair_it14.first == VC.row_end(d1-1,d2-1,d3-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		std::pair<row_riter,const_row_riter> pair_it15;
		pair_it15 = std::mismatch(V.row_rbegin(d1-1,d2-1,d3-1), V.row_rend(d1-1,d2-1,d3-1), VC.row_rbegin(d1-1,d2-1,d3-1));
		res = res && (pair_it15.first == V.row_rend(d1-1,d2-1,d3-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;//15
		}
		step++;
		std::pair<const_row_riter,row_riter> pair_it16;
		pair_it16 = std::mismatch(VC.row_rbegin(d1-1,d2-1,d3-1), VC.row_rend(d1-1,d2-1,d3-1), V.row_rbegin(d1-1,d2-1,d3-1));
		res = res && (pair_it16.first == VC.row_rend(d1-1,d2-1,d3-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		std::pair<col_iter,const_col_iter> pair_it17;
		pair_it17 = std::mismatch(V.col_begin(d1-1,d2-1,d4-1), V.col_end(d1-1,d2-1,d4-1), VC.col_begin(d1-1,d2-1,d4-1));
		res = res && (pair_it17.first == V.col_end(d1-1,d2-1,d4-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_col_iter,col_iter> pair_it18;
		pair_it18 = std::mismatch(VC.col_begin(d1-1,d2-1,d4-1), VC.col_end(d1-1,d2-1,d4-1), V.col_begin(d1-1,d2-1,d4-1));
		res = res && (pair_it18.first == VC.col_end(d1-1,d2-1,d4-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		std::pair<col_riter,const_col_riter> pair_it19;
		pair_it19 = std::mismatch(V.col_rbegin(d1-1,d2-1,d4-1), V.col_rend(d1-1,d2-1,d4-1), VC.col_rbegin(d1-1,d2-1,d4-1));
		res = res && (pair_it19.first == V.col_rend(d1-1,d2-1,d4-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_col_riter,col_riter> pair_it20;
		pair_it20 = std::mismatch(VC.col_rbegin(d1-1,d2-1,d4-1), VC.col_rend(d1-1,d2-1,d4-1), V.col_rbegin(d1-1,d2-1,d4-1));
		res = res && (pair_it20.first == VC.col_rend(d1-1,d2-1,d4-1));
		if(!res){
			std::cerr << "error #" << step << std::endl;//20
		}
		step++;
	}

	typedef typename slip::GenericMultiComponent4d<BLOCK>::slab_range_iterator slab_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_slab_range_iterator const_slab_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::slice_range_iterator slice_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_slice_range_iterator const_slice_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::row_range_iterator row_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_row_range_iterator const_row_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::col_range_iterator col_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_col_range_iterator const_col_range_iter;

	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_slab_range_iterator slab_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_slab_range_iterator const_slab_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_slice_range_iterator slice_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_slice_range_iterator const_slice_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_row_range_iterator row_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_row_range_iterator const_row_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_col_range_iterator col_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_col_range_iterator const_col_range_riter;

	if(d1>2 && d2>2 && d3>2 && d4>2){
		slip::Range<int> slab_range(0,d1-1,2);
		std::pair<slab_range_iter,const_slab_range_iter> pair_it5;
		pair_it5 = std::mismatch(V.slab_begin(d2-1,d3-1,d4-1,slab_range), V.slab_end(d2-1,d3-1,d4-1,slab_range),
				VC.slab_begin(d2-1,d3-1,d4-1,slab_range));
		res = res && (pair_it5.first == V.slab_end(d2-1,d3-1,d4-1,slab_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.slab_begin(d2-1,d3-1,d4-1,slab_range), V.slab_end(d2-1,d3-1,d4-1,slab_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
		std::pair<const_slab_range_iter,slab_range_iter> pair_it6;
		pair_it6 = std::mismatch(VC.slab_begin(d2-1,d3-1,d4-1,slab_range), VC.slab_end(d2-1,d3-1,d4-1,slab_range),
				V.slab_begin(d2-1,d3-1,d4-1,slab_range));
		res = res && (pair_it6.first == VC.slab_end(d2-1,d3-1,d4-1,slab_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(VC.slab_begin(d2-1,d3-1,d4-1,slab_range), VC.slab_end(d2-1,d3-1,d4-1,slab_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;

		std::pair<slab_range_riter,const_slab_range_riter> pair_it7;
		pair_it7 = std::mismatch(V.slab_rbegin(d2-1,d3-1,d4-1,slab_range), V.slab_rend(d2-1,d3-1,d4-1,slab_range),
				VC.slab_rbegin(d2-1,d3-1,d4-1,slab_range));
		res = res && (pair_it7.first == V.slab_rend(d2-1,d3-1,d4-1,slab_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.slab_rbegin(d2-1,d3-1,d4-1,slab_range), V.slab_rend(d2-1,d3-1,d4-1,slab_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
		std::pair<const_slab_range_riter,slab_range_riter> pair_it8;
		pair_it8 = std::mismatch(VC.slab_rbegin(d2-1,d3-1,d4-1,slab_range), VC.slab_rend(d2-1,d3-1,d4-1,slab_range),
				V.slab_rbegin(d2-1,d3-1,d4-1,slab_range));
		res = res && (pair_it8.first == VC.slab_rend(d2-1,d3-1,d4-1,slab_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(VC.slab_rbegin(d2-1,d3-1,d4-1,slab_range), VC.slab_rend(d2-1,d3-1,d4-1,slab_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;

		slip::Range<int> slice_range(0,d2-1,2);
		std::pair<slice_range_iter,const_slice_range_iter> pair_it9;
		pair_it9 = std::mismatch(V.slice_begin(d1-1,d3-1,d4-1,slice_range), V.slice_end(d1-1,d3-1,d4-1,slice_range),
				VC.slice_begin(d1-1,d3-1,d4-1,slice_range));
		res = res && (pair_it9.first == V.slice_end(d1-1,d3-1,d4-1,slice_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;//25
			std::copy(V.slice_begin(d1-1,d3-1,d4-1,slice_range), V.slice_end(d1-1,d3-1,d4-1,slice_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
		std::pair<const_slice_range_iter,slice_range_iter> pair_it10;
		pair_it10 = std::mismatch(VC.slice_begin(d1-1,d3-1,d4-1,slice_range), VC.slice_end(d1-1,d3-1,d4-1,slice_range),
				V.slice_begin(d1-1,d3-1,d4-1,slice_range));
		res = res && (pair_it10.first == VC.slice_end(d1-1,d3-1,d4-1,slice_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(VC.slice_begin(d1-1,d3-1,d4-1,slice_range), VC.slice_end(d1-1,d3-1,d4-1,slice_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;

		std::pair<slice_range_riter,const_slice_range_riter> pair_it11;
		pair_it11 = std::mismatch(V.slice_rbegin(d1-1,d3-1,d4-1,slice_range), V.slice_rend(d1-1,d3-1,d4-1,slice_range),
				VC.slice_rbegin(d1-1,d3-1,d4-1,slice_range));
		res = res && (pair_it11.first == V.slice_rend(d1-1,d3-1,d4-1,slice_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.slice_rbegin(d1-1,d3-1,d4-1,slice_range), V.slice_rend(d1-1,d3-1,d4-1,slice_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
		std::pair<const_slice_range_riter,slice_range_riter> pair_it12;
		pair_it12 = std::mismatch(VC.slice_rbegin(d1-1,d3-1,d4-1,slice_range), VC.slice_rend(d1-1,d3-1,d4-1,slice_range),
				V.slice_rbegin(d1-1,d3-1,d4-1,slice_range));
		res = res && (pair_it12.first == VC.slice_rend(d1-1,d3-1,d4-1,slice_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(VC.slice_rbegin(d1-1,d3-1,d4-1,slice_range), VC.slice_rend(d1-1,d3-1,d4-1,slice_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;

		slip::Range<int> row_range(0,d4-1,2);
		std::pair<row_range_iter,const_row_range_iter> pair_it13;
		pair_it13 = std::mismatch(V.row_begin(d1-1,d2-1,d3-1,row_range), V.row_end(d1-1,d2-1,d3-1,row_range),
				VC.row_begin(d1-1,d2-1,d3-1,row_range));
		res = res && (pair_it13.first == V.row_end(d1-1,d2-1,d3-1,row_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.row_begin(d1-1,d2-1,d3-1,row_range), V.row_end(d1-1,d2-1,d3-1,row_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
		std::pair<const_row_range_iter,row_range_iter> pair_it14;
		pair_it14 = std::mismatch(VC.row_begin(d1-1,d2-1,d3-1,row_range), VC.row_end(d1-1,d2-1,d3-1,row_range),
				V.row_begin(d1-1,d2-1,d3-1,row_range));
		res = res && (pair_it14.first == VC.row_end(d1-1,d2-1,d3-1,row_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;//30
			std::copy(VC.row_begin(d1-1,d2-1,d3-1,row_range), VC.row_end(d1-1,d2-1,d3-1,row_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;

		std::pair<row_range_riter,const_row_range_riter> pair_it15;
		pair_it15 = std::mismatch(V.row_rbegin(d1-1,d2-1,d3-1,row_range), V.row_rend(d1-1,d2-1,d3-1,row_range),
				VC.row_rbegin(d1-1,d2-1,d3-1,row_range));
		res = res && (pair_it15.first == V.row_rend(d1-1,d2-1,d3-1,row_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.row_rbegin(d1-1,d2-1,d3-1,row_range), V.row_rend(d1-1,d2-1,d3-1,row_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
		std::pair<const_row_range_riter,row_range_riter> pair_it16;
		pair_it16 = std::mismatch(VC.row_rbegin(d1-1,d2-1,d3-1,row_range), VC.row_rend(d1-1,d2-1,d3-1,row_range),
				V.row_rbegin(d1-1,d2-1,d3-1,row_range));
		res = res && (pair_it16.first == VC.row_rend(d1-1,d2-1,d3-1,row_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(VC.row_rbegin(d1-1,d2-1,d3-1,row_range), VC.row_rend(d1-1,d2-1,d3-1,row_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;

		slip::Range<int> col_range(0,d3-1,2);
		std::pair<col_range_iter,const_col_range_iter> pair_it17;
		pair_it17 = std::mismatch(V.col_begin(d1-1,d2-1,d4-1,col_range), V.col_end(d1-1,d2-1,d4-1,col_range),
				VC.col_begin(d1-1,d2-1,d4-1,col_range));
		res = res && (pair_it17.first == V.col_end(d1-1,d2-1,d4-1,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.col_begin(d1-1,d2-1,d4-1,col_range), V.col_end(d1-1,d2-1,d4-1,col_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
		std::pair<const_col_range_iter,col_range_iter> pair_it18;
		pair_it18 = std::mismatch(VC.col_begin(d1-1,d2-1,d4-1,col_range), VC.col_end(d1-1,d2-1,d4-1,col_range),
				V.col_begin(d1-1,d2-1,d4-1,col_range));
		res = res && (pair_it18.first == VC.col_end(d1-1,d2-1,d4-1,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(VC.col_begin(d1-1,d2-1,d4-1,col_range), VC.col_end(d1-1,d2-1,d4-1,col_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;

		std::pair<col_range_riter,const_col_range_riter> pair_it19;
		pair_it19 = std::mismatch(V.col_rbegin(d1-1,d2-1,d4-1,col_range), V.col_rend(d1-1,d2-1,d4-1,col_range),
				VC.col_rbegin(d1-1,d2-1,d4-1,col_range));
		res = res && (pair_it19.first == V.col_rend(d1-1,d2-1,d4-1,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;//35
			std::copy(V.col_rbegin(d1-1,d2-1,d4-1,col_range), V.col_rend(d1-1,d2-1,d4-1,col_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
		std::pair<const_col_range_riter,col_range_riter> pair_it20;
		pair_it20 = std::mismatch(VC.col_rbegin(d1-1,d2-1,d4-1,col_range), VC.col_rend(d1-1,d2-1,d4-1,col_range),
				V.col_rbegin(d1-1,d2-1,d4-1,col_range));
		res = res && (pair_it20.first == VC.col_rend(d1-1,d2-1,d4-1,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(VC.col_rbegin(d1-1,d2-1,d4-1,col_range), VC.col_rend(d1-1,d2-1,d4-1,col_range),
					std::ostream_iterator<BLOCK>(std::cout," "));
		}
		step++;
	}

	return res;
}


/*!
 * \brief external 4d iterators properties function
 * \param CV input %GenericMultiComponent4d
 * \return true if the property is verified
 */
template<typename BLOCK>
bool def_genericmulticomponent4d_iterators4d_property(const slip::GenericMultiComponent4d<BLOCK>& VC){
	bool res = true;
	int step = 1;
	slip::GenericMultiComponent4d<BLOCK> V(VC);

	std::size_t d1 = V.dim1();
	std::size_t d2 = V.dim2();
	std::size_t d3 = V.dim3();
	std::size_t d4 = V.dim4();

	slip::DPoint4d<int> d = V.last_back_bottom_right() - V.first_front_upper_left();
	res = res && (d == slip::DPoint4d<int>(d1,d2,d3,d4));
	if(!res){
		std::cerr << "error #" << step << std::endl;
		std::cerr << d << std::endl;
	}
	step++;

	//Global four dimensional iterators

	typedef typename slip::GenericMultiComponent4d<BLOCK>::iterator4d iter4d;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_iterator4d const_iter4d;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_iterator4d riter4d;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_iterator4d const_riter4d;

	typedef typename slip::GenericMultiComponent4d<BLOCK>::iterator iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_iterator const_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_iterator riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_iterator const_riter;


	std::pair<iter4d,const_iter> pair_it1;
	pair_it1 = std::mismatch(V.first_front_upper_left(), V.last_back_bottom_right(), VC.begin());
	res = res && (pair_it1.first == V.last_back_bottom_right());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_iter4d,iter> pair_it2;
	pair_it2 = std::mismatch(VC.first_front_upper_left(), VC.last_back_bottom_right(), V.begin());
	res = res && (pair_it2.first == VC.last_back_bottom_right());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	std::pair<riter4d,const_riter> pair_it3;
	pair_it3 = std::mismatch(V.rfirst_front_upper_left(), V.rlast_back_bottom_right(), VC.rbegin());
	res = res && (pair_it3.first == V.rlast_back_bottom_right());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_riter4d,riter> pair_it4;
	pair_it4 = std::mismatch(VC.rfirst_front_upper_left(), VC.rlast_back_bottom_right(), V.rbegin());
	res = res && (pair_it4.first == VC.rlast_back_bottom_right());
	if(!res){
		std::cerr << "error #" << step << std::endl;//5
	}
	step++;

	//box four dimensional iterators

	slip::Box4d<int> box(1,1,1,1,d1-1,d2-1,d3-1,d4-1);
	std::pair<iter4d,const_iter4d> pair_it5;
	pair_it5 = std::mismatch(V.first_front_upper_left(box), V.last_back_bottom_right(box), VC.first_front_upper_left(box));
	res = res && (pair_it5.first == V.last_back_bottom_right(box));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_iter4d,iter4d> pair_it6;
	pair_it6 = std::mismatch(VC.first_front_upper_left(box), VC.last_back_bottom_right(box), V.first_front_upper_left(box));
	res = res && (pair_it6.first == VC.last_back_bottom_right(box));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	std::pair<riter4d,const_riter4d> pair_it7;
	pair_it7 = std::mismatch(V.rfirst_front_upper_left(box), V.rlast_back_bottom_right(box), VC.rfirst_front_upper_left(box));
	res = res && (pair_it7.first == V.rlast_back_bottom_right(box));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_riter4d,riter4d> pair_it8;
	pair_it8 = std::mismatch(VC.rfirst_front_upper_left(box), VC.rlast_back_bottom_right(box), V.rfirst_front_upper_left(box));
	res = res && (pair_it8.first == VC.rlast_back_bottom_right(box));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	//Range four dimensional iterators

	typedef typename slip::GenericMultiComponent4d<BLOCK>::iterator4d_range range_iter4d;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_iterator4d_range const_range_iter4d;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_iterator4d_range rrange_iter4d;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_iterator4d_range const_rrange_iter4d;

	slip::Range<int> slab_range(0,d1-1,2);
	slip::Range<int> slice_range(0,d2-1,2);
	slip::Range<int> row_range(0,d3-1,2);
	slip::Range<int> col_range(0,d4-1,2);
	std::pair<range_iter4d,const_range_iter4d> pair_it9;
	pair_it9 = std::mismatch(V.first_front_upper_left(slab_range,slice_range,row_range,col_range),
			V.last_back_bottom_right(slab_range,slice_range,row_range,col_range),
			VC.first_front_upper_left(slab_range,slice_range,row_range,col_range));
	res = res && (pair_it9.first == V.last_back_bottom_right(slab_range,slice_range,row_range,col_range));
	if(!res){
		std::cerr << "error #" << step << std::endl;//10
		std::copy(V.first_front_upper_left(slab_range,slice_range,row_range,col_range),
				V.last_back_bottom_right(slab_range,slice_range,row_range,col_range),
				std::ostream_iterator<BLOCK>(std::cerr," "));

		std::copy(VC.first_front_upper_left(slab_range,slice_range,row_range,col_range),
				VC.last_back_bottom_right(slab_range,slice_range,row_range,col_range),
				std::ostream_iterator<BLOCK>(std::cerr," "));
		std::cerr << std::endl;
	}
	step++;
	std::pair<const_range_iter4d,range_iter4d> pair_it10;
	pair_it10 = std::mismatch(VC.first_front_upper_left(slab_range,slice_range,row_range,col_range),
			VC.last_back_bottom_right(slab_range,slice_range,row_range,col_range),
			V.first_front_upper_left(slab_range,slice_range,row_range,col_range));
	res = res && (pair_it10.first == VC.last_back_bottom_right(slab_range,slice_range,row_range,col_range));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	std::pair<rrange_iter4d,const_rrange_iter4d> pair_it11;
	pair_it11 = std::mismatch(V.rfirst_front_upper_left(slab_range,slice_range,row_range,col_range),
			V.rlast_back_bottom_right(slab_range,slice_range,row_range,col_range),
			VC.rfirst_front_upper_left(slab_range,slice_range,row_range,col_range));
	res = res && (pair_it11.first == V.rlast_back_bottom_right(slab_range,slice_range,row_range,col_range));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	std::pair<const_rrange_iter4d,rrange_iter4d> pair_it12;
	pair_it12 = std::mismatch(VC.rfirst_front_upper_left(slab_range,slice_range,row_range,col_range),
			VC.rlast_back_bottom_right(slab_range,slice_range,row_range,col_range),
			V.rfirst_front_upper_left(slab_range,slice_range,row_range,col_range));
	res = res && (pair_it12.first == VC.rlast_back_bottom_right(slab_range,slice_range,row_range,col_range));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	//iterator4d advance functions

	slip::DPoint4d<int> saut(d1,d2,d3,d4);
	iter4d it_s = V.first_front_upper_left() + saut;
	res = res && (it_s == V.last_back_bottom_right());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	it_s = it_s - saut;
	res = res && (it_s == V.first_front_upper_left());
	if(!res){
		std::cerr << "error #" << step << std::endl;//15
	}
	step++;
	it_s += saut;
	res = res && (it_s == V.last_back_bottom_right());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	it_s -= saut;
	res = res && (it_s == V.first_front_upper_left());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	//const iterator4d advance functions

	const_iter4d it_cs = VC.first_front_upper_left() + saut;
	res = res && (it_cs == VC.last_back_bottom_right());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	it_cs = it_cs - saut;
	res = res && (it_cs == VC.first_front_upper_left());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	it_cs += saut;
	res = res && (it_cs == VC.last_back_bottom_right());
	if(!res){
		std::cerr << "error #" << step << std::endl;//20
	}
	step++;
	it_cs -= saut;
	res = res && (it_cs == VC.first_front_upper_left());
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	//range iterator4d advance functions

	slip::Range<int> slab_range_s(0,d1-1,1);
	slip::Range<int> slice_range_s(0,d2-1,1);
	slip::Range<int> row_range_s(0,d3-1,1);
	slip::Range<int> col_range_s(0,d4-1,1);

	range_iter4d it_r_s = V.first_front_upper_left(slab_range_s,slice_range_s,row_range_s,col_range_s) + saut;
	res = res && (it_r_s == V.last_back_bottom_right(slab_range_s,slice_range_s,row_range_s,col_range_s));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	it_r_s = it_r_s - saut;
	res = res && (it_r_s == V.first_front_upper_left(slab_range_s,slice_range_s,row_range_s,col_range_s));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	it_r_s += saut;
	res = res && (it_r_s == V.last_back_bottom_right(slab_range_s,slice_range_s,row_range_s,col_range_s));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	it_r_s -= saut;
	res = res && (it_r_s == V.first_front_upper_left(slab_range_s,slice_range_s,row_range_s,col_range_s));
	if(!res){
		std::cerr << "error #" << step << std::endl;//25
	}
	step++;

	//const range iterator4d advance functions

	const_range_iter4d it_r_cs = VC.first_front_upper_left(slab_range_s,slice_range_s,row_range_s,col_range_s) + saut;
	res = res && (it_r_cs == VC.last_back_bottom_right(slab_range_s,slice_range_s,row_range_s,col_range_s));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	it_r_cs = it_r_cs - saut;
	res = res && (it_r_cs == VC.first_front_upper_left(slab_range_s,slice_range_s,row_range_s,col_range_s));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	it_r_cs += saut;
	res = res && (it_r_cs == VC.last_back_bottom_right(slab_range_s,slice_range_s,row_range_s,col_range_s));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;
	it_r_cs -= saut;
	res = res && (it_r_cs == VC.first_front_upper_left(slab_range_s,slice_range_s,row_range_s,col_range_s));
	if(!res){
		std::cerr << "error #" << step << std::endl;
	}
	step++;

	return res;
}

/*!
 * \brief external 1d component iterators properties function
 * \param CV input %GenericMultiComponent4d
 * \return true if the property is verified
 */
template<typename BLOCK>
bool def_genericmulticomponent4d_component_iterators1d_property(const slip::GenericMultiComponent4d<BLOCK>& VC){
	bool res = true;
	int step = 1;
	slip::GenericMultiComponent4d<BLOCK> V(VC);
	typedef typename BLOCK::value_type T;
	std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
	std::size_t d1 = V.dim1();
	std::size_t d2 = V.dim2();
	std::size_t d3 = V.dim3();
	std::size_t d4 = V.dim4();

	//global one dimensional iterators

	typedef typename slip::GenericMultiComponent4d<BLOCK>::component_iterator iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_component_iterator const_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_component_iterator riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_component_iterator const_riter;

	for(std::size_t component=0; component < b_size; ++component){
		std::pair<iter,const_iter> pair_it1;
		pair_it1 = std::mismatch(V.begin(component), V.end(component), VC.begin(component));
		res = res && (pair_it1.first == V.end(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_iter,iter> pair_it2;
		pair_it2 = std::mismatch(VC.begin(component), VC.end(component), V.begin(component));
		res = res && (pair_it2.first == VC.end(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		std::pair<riter,const_riter> pair_it3;
		pair_it3 = std::mismatch(V.rbegin(component), V.rend(component), VC.rbegin(component));
		res = res && (pair_it3.first == V.rend(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_riter,riter> pair_it4;
		pair_it4 = std::mismatch(VC.rbegin(component), VC.rend(component), V.rbegin(component));
		res = res && (pair_it4.first == VC.rend(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
	}
	//slab, slice, row and col iterator
	typedef typename slip::GenericMultiComponent4d<BLOCK>::component_slab_iterator slab_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_component_slab_iterator const_slab_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::component_slice_iterator slice_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_component_slice_iterator const_slice_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::component_row_iterator row_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_component_row_iterator const_row_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::component_col_iterator col_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_component_col_iterator const_col_iter;

	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_component_slab_iterator slab_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_component_slab_iterator const_slab_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_component_slice_iterator slice_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_component_slice_iterator const_slice_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_component_row_iterator row_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_component_row_iterator const_row_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_component_col_iterator col_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_component_col_iterator const_col_riter;


	if(d1>0 && d2>0 && d3>0 && d4>0){
		for(std::size_t component=0; component < b_size; ++component){
			std::pair<slab_iter,const_slab_iter> pair_it5;
			pair_it5 = std::mismatch(V.slab_begin(component,d2-1,d3-1,d4-1), V.slab_end(component,d2-1,d3-1,d4-1),
					VC.slab_begin(component,d2-1,d3-1,d4-1));
			res = res && (pair_it5.first == V.slab_end(component,d2-1,d3-1,d4-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;//5
			}
			step++;
			std::pair<const_slab_iter,slab_iter> pair_it6;
			pair_it6 = std::mismatch(VC.slab_begin(component,d2-1,d3-1,d4-1), VC.slab_end(component,d2-1,d3-1,d4-1),
					V.slab_begin(component,d2-1,d3-1,d4-1));
			res = res && (pair_it6.first == VC.slab_end(component,d2-1,d3-1,d4-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;
			}
			step++;

			std::pair<slab_riter,const_slab_riter> pair_it7;
			pair_it7 = std::mismatch(V.slab_rbegin(component,d2-1,d3-1,d4-1), V.slab_rend(component,d2-1,d3-1,d4-1),
					VC.slab_rbegin(component,d2-1,d3-1,d4-1));
			res = res && (pair_it7.first == V.slab_rend(component,d2-1,d3-1,d4-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;
			}
			step++;
			std::pair<const_slab_riter,slab_riter> pair_it8;
			pair_it8 = std::mismatch(VC.slab_rbegin(component,d2-1,d3-1,d4-1), VC.slab_rend(component,d2-1,d3-1,d4-1),
					V.slab_rbegin(component,d2-1,d3-1,d4-1));
			res = res && (pair_it8.first == VC.slab_rend(component,d2-1,d3-1,d4-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;
			}
			step++;

			std::pair<slice_iter,const_slice_iter> pair_it9;
			pair_it9 = std::mismatch(V.slice_begin(component,d1-1,d3-1,d4-1), V.slice_end(component,d1-1,d3-1,d4-1),
					VC.slice_begin(component,d1-1,d3-1,d4-1));
			res = res && (pair_it9.first == V.slice_end(component,d1-1,d3-1,d4-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;
			}
			step++;
			std::pair<const_slice_iter,slice_iter> pair_it10;
			pair_it10 = std::mismatch(VC.slice_begin(component,d1-1,d3-1,d4-1), VC.slice_end(component,d1-1,d3-1,d4-1),
					V.slice_begin(component,d1-1,d3-1,d4-1));
			res = res && (pair_it10.first == VC.slice_end(component,d1-1,d3-1,d4-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;//10
			}
			step++;

			std::pair<slice_riter,const_slice_riter> pair_it11;
			pair_it11 = std::mismatch(V.slice_rbegin(component,d1-1,d3-1,d4-1), V.slice_rend(component,d1-1,d3-1,d4-1),
					VC.slice_rbegin(component,d1-1,d3-1,d4-1));
			res = res && (pair_it11.first == V.slice_rend(component,d1-1,d3-1,d4-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;
			}
			step++;
			std::pair<const_slice_riter,slice_riter> pair_it12;
			pair_it12 = std::mismatch(VC.slice_rbegin(component,d1-1,d3-1,d4-1), VC.slice_rend(component,d1-1,d3-1,d4-1),
					V.slice_rbegin(component,d1-1,d3-1,d4-1));
			res = res && (pair_it12.first == VC.slice_rend(component,d1-1,d3-1,d4-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;
			}
			step++;

			std::pair<row_iter,const_row_iter> pair_it13;
			pair_it13 = std::mismatch(V.row_begin(component,d1-1,d2-1,d3-1), V.row_end(component,d1-1,d2-1,d3-1),
					VC.row_begin(component,d1-1,d2-1,d3-1));
			res = res && (pair_it13.first == V.row_end(component,d1-1,d2-1,d3-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;
			}
			step++;
			std::pair<const_row_iter,row_iter> pair_it14;
			pair_it14 = std::mismatch(VC.row_begin(component,d1-1,d2-1,d3-1), VC.row_end(component,d1-1,d2-1,d3-1),
					V.row_begin(component,d1-1,d2-1,d3-1));
			res = res && (pair_it14.first == VC.row_end(component,d1-1,d2-1,d3-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;
			}
			step++;

			std::pair<row_riter,const_row_riter> pair_it15;
			pair_it15 = std::mismatch(V.row_rbegin(component,d1-1,d2-1,d3-1), V.row_rend(component,d1-1,d2-1,d3-1),
					VC.row_rbegin(component,d1-1,d2-1,d3-1));
			res = res && (pair_it15.first == V.row_rend(component,d1-1,d2-1,d3-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;//15
			}
			step++;
			std::pair<const_row_riter,row_riter> pair_it16;
			pair_it16 = std::mismatch(VC.row_rbegin(component,d1-1,d2-1,d3-1), VC.row_rend(component,d1-1,d2-1,d3-1),
					V.row_rbegin(component,d1-1,d2-1,d3-1));
			res = res && (pair_it16.first == VC.row_rend(component,d1-1,d2-1,d3-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;
			}
			step++;

			std::pair<col_iter,const_col_iter> pair_it17;
			pair_it17 = std::mismatch(V.col_begin(component,d1-1,d2-1,d4-1), V.col_end(component,d1-1,d2-1,d4-1),
					VC.col_begin(component,d1-1,d2-1,d4-1));
			res = res && (pair_it17.first == V.col_end(component,d1-1,d2-1,d4-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;
			}
			step++;
			std::pair<const_col_iter,col_iter> pair_it18;
			pair_it18 = std::mismatch(VC.col_begin(component,d1-1,d2-1,d4-1), VC.col_end(component,d1-1,d2-1,d4-1),
					V.col_begin(component,d1-1,d2-1,d4-1));
			res = res && (pair_it18.first == VC.col_end(component,d1-1,d2-1,d4-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;
			}
			step++;

			std::pair<col_riter,const_col_riter> pair_it19;
			pair_it19 = std::mismatch(V.col_rbegin(component,d1-1,d2-1,d4-1), V.col_rend(component,d1-1,d2-1,d4-1),
					VC.col_rbegin(component,d1-1,d2-1,d4-1));
			res = res && (pair_it19.first == V.col_rend(component,d1-1,d2-1,d4-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;
			}
			step++;
			std::pair<const_col_riter,col_riter> pair_it20;
			pair_it20 = std::mismatch(VC.col_rbegin(component,d1-1,d2-1,d4-1), VC.col_rend(component,d1-1,d2-1,d4-1),
					V.col_rbegin(component,d1-1,d2-1,d4-1));
			res = res && (pair_it20.first == VC.col_rend(component,d1-1,d2-1,d4-1));
			if(!res){
				std::cerr << "error #" << step << std::endl;//20
			}
			step++;
		}
	}

	typedef typename slip::GenericMultiComponent4d<BLOCK>::component_slab_range_iterator slab_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_component_slab_range_iterator const_slab_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::component_slice_range_iterator slice_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_component_slice_range_iterator const_slice_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::component_row_range_iterator row_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_component_row_range_iterator const_row_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::component_col_range_iterator col_range_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_component_col_range_iterator const_col_range_iter;

	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_component_slab_range_iterator slab_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_component_slab_range_iterator const_slab_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_component_slice_range_iterator slice_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_component_slice_range_iterator const_slice_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_component_row_range_iterator row_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_component_row_range_iterator const_row_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_component_col_range_iterator col_range_riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_component_col_range_iterator const_col_range_riter;

	if(d1>2 && d2>2 && d3>2 && d4>2){
		for(std::size_t component=0; component < b_size; ++component){
			slip::Range<int> slab_range(0,d1-1,2);
			std::pair<slab_range_iter,const_slab_range_iter> pair_it5;
			pair_it5 = std::mismatch(V.slab_begin(component,d2-1,d3-1,d4-1,slab_range), V.slab_end(component,d2-1,d3-1,d4-1,slab_range),
					VC.slab_begin(component,d2-1,d3-1,d4-1,slab_range));
			res = res && (pair_it5.first == V.slab_end(component,d2-1,d3-1,d4-1,slab_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;
				std::copy(V.slab_begin(component,d2-1,d3-1,d4-1,slab_range), V.slab_end(component,d2-1,d3-1,d4-1,slab_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;
			std::pair<const_slab_range_iter,slab_range_iter> pair_it6;
			pair_it6 = std::mismatch(VC.slab_begin(component,d2-1,d3-1,d4-1,slab_range), VC.slab_end(component,d2-1,d3-1,d4-1,slab_range),
					V.slab_begin(component,d2-1,d3-1,d4-1,slab_range));
			res = res && (pair_it6.first == VC.slab_end(component,d2-1,d3-1,d4-1,slab_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;
				std::copy(VC.slab_begin(component,d2-1,d3-1,d4-1,slab_range), VC.slab_end(component,d2-1,d3-1,d4-1,slab_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;

			std::pair<slab_range_riter,const_slab_range_riter> pair_it7;
			pair_it7 = std::mismatch(V.slab_rbegin(component,d2-1,d3-1,d4-1,slab_range), V.slab_rend(component,d2-1,d3-1,d4-1,slab_range),
					VC.slab_rbegin(component,d2-1,d3-1,d4-1,slab_range));
			res = res && (pair_it7.first == V.slab_rend(component,d2-1,d3-1,d4-1,slab_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;
				std::copy(V.slab_rbegin(component,d2-1,d3-1,d4-1,slab_range), V.slab_rend(component,d2-1,d3-1,d4-1,slab_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;
			std::pair<const_slab_range_riter,slab_range_riter> pair_it8;
			pair_it8 = std::mismatch(VC.slab_rbegin(component,d2-1,d3-1,d4-1,slab_range), VC.slab_rend(component,d2-1,d3-1,d4-1,slab_range),
					V.slab_rbegin(component,d2-1,d3-1,d4-1,slab_range));
			res = res && (pair_it8.first == VC.slab_rend(component,d2-1,d3-1,d4-1,slab_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;
				std::copy(VC.slab_rbegin(component,d2-1,d3-1,d4-1,slab_range), VC.slab_rend(component,d2-1,d3-1,d4-1,slab_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;

			slip::Range<int> slice_range(0,d2-1,2);
			std::pair<slice_range_iter,const_slice_range_iter> pair_it9;
			pair_it9 = std::mismatch(V.slice_begin(component,d1-1,d3-1,d4-1,slice_range), V.slice_end(component,d1-1,d3-1,d4-1,slice_range),
					VC.slice_begin(component,d1-1,d3-1,d4-1,slice_range));
			res = res && (pair_it9.first == V.slice_end(component,d1-1,d3-1,d4-1,slice_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;//25
				std::copy(V.slice_begin(component,d1-1,d3-1,d4-1,slice_range), V.slice_end(component,d1-1,d3-1,d4-1,slice_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;
			std::pair<const_slice_range_iter,slice_range_iter> pair_it10;
			pair_it10 = std::mismatch(VC.slice_begin(component,d1-1,d3-1,d4-1,slice_range), VC.slice_end(component,d1-1,d3-1,d4-1,slice_range),
					V.slice_begin(component,d1-1,d3-1,d4-1,slice_range));
			res = res && (pair_it10.first == VC.slice_end(component,d1-1,d3-1,d4-1,slice_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;
				std::copy(VC.slice_begin(component,d1-1,d3-1,d4-1,slice_range), VC.slice_end(component,d1-1,d3-1,d4-1,slice_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;

			std::pair<slice_range_riter,const_slice_range_riter> pair_it11;
			pair_it11 = std::mismatch(V.slice_rbegin(component,d1-1,d3-1,d4-1,slice_range), V.slice_rend(component,d1-1,d3-1,d4-1,slice_range),
					VC.slice_rbegin(component,d1-1,d3-1,d4-1,slice_range));
			res = res && (pair_it11.first == V.slice_rend(component,d1-1,d3-1,d4-1,slice_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;
				std::copy(V.slice_rbegin(component,d1-1,d3-1,d4-1,slice_range), V.slice_rend(component,d1-1,d3-1,d4-1,slice_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;
			std::pair<const_slice_range_riter,slice_range_riter> pair_it12;
			pair_it12 = std::mismatch(VC.slice_rbegin(component,d1-1,d3-1,d4-1,slice_range), VC.slice_rend(component,d1-1,d3-1,d4-1,slice_range),
					V.slice_rbegin(component,d1-1,d3-1,d4-1,slice_range));
			res = res && (pair_it12.first == VC.slice_rend(component,d1-1,d3-1,d4-1,slice_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;
				std::copy(VC.slice_rbegin(component,d1-1,d3-1,d4-1,slice_range), VC.slice_rend(component,d1-1,d3-1,d4-1,slice_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;

			slip::Range<int> row_range(0,d4-1,2);
			std::pair<row_range_iter,const_row_range_iter> pair_it13;
			pair_it13 = std::mismatch(V.row_begin(component,d1-1,d2-1,d3-1,row_range), V.row_end(component,d1-1,d2-1,d3-1,row_range),
					VC.row_begin(component,d1-1,d2-1,d3-1,row_range));
			res = res && (pair_it13.first == V.row_end(component,d1-1,d2-1,d3-1,row_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;
				std::copy(V.row_begin(component,d1-1,d2-1,d3-1,row_range), V.row_end(component,d1-1,d2-1,d3-1,row_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;
			std::pair<const_row_range_iter,row_range_iter> pair_it14;
			pair_it14 = std::mismatch(VC.row_begin(component,d1-1,d2-1,d3-1,row_range), VC.row_end(component,d1-1,d2-1,d3-1,row_range),
					V.row_begin(component,d1-1,d2-1,d3-1,row_range));
			res = res && (pair_it14.first == VC.row_end(component,d1-1,d2-1,d3-1,row_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;//30
				std::copy(VC.row_begin(component,d1-1,d2-1,d3-1,row_range), VC.row_end(component,d1-1,d2-1,d3-1,row_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;

			std::pair<row_range_riter,const_row_range_riter> pair_it15;
			pair_it15 = std::mismatch(V.row_rbegin(component,d1-1,d2-1,d3-1,row_range), V.row_rend(component,d1-1,d2-1,d3-1,row_range),
					VC.row_rbegin(component,d1-1,d2-1,d3-1,row_range));
			res = res && (pair_it15.first == V.row_rend(component,d1-1,d2-1,d3-1,row_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;
				std::copy(V.row_rbegin(component,d1-1,d2-1,d3-1,row_range), V.row_rend(component,d1-1,d2-1,d3-1,row_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;
			std::pair<const_row_range_riter,row_range_riter> pair_it16;
			pair_it16 = std::mismatch(VC.row_rbegin(component,d1-1,d2-1,d3-1,row_range), VC.row_rend(component,d1-1,d2-1,d3-1,row_range),
					V.row_rbegin(component,d1-1,d2-1,d3-1,row_range));
			res = res && (pair_it16.first == VC.row_rend(component,d1-1,d2-1,d3-1,row_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;
				std::copy(VC.row_rbegin(component,d1-1,d2-1,d3-1,row_range), VC.row_rend(component,d1-1,d2-1,d3-1,row_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;

			slip::Range<int> col_range(0,d3-1,2);
			std::pair<col_range_iter,const_col_range_iter> pair_it17;
			pair_it17 = std::mismatch(V.col_begin(component,d1-1,d2-1,d4-1,col_range), V.col_end(component,d1-1,d2-1,d4-1,col_range),
					VC.col_begin(component,d1-1,d2-1,d4-1,col_range));
			res = res && (pair_it17.first == V.col_end(component,d1-1,d2-1,d4-1,col_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;
				std::copy(V.col_begin(component,d1-1,d2-1,d4-1,col_range), V.col_end(component,d1-1,d2-1,d4-1,col_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;
			std::pair<const_col_range_iter,col_range_iter> pair_it18;
			pair_it18 = std::mismatch(VC.col_begin(component,d1-1,d2-1,d4-1,col_range), VC.col_end(component,d1-1,d2-1,d4-1,col_range),
					V.col_begin(component,d1-1,d2-1,d4-1,col_range));
			res = res && (pair_it18.first == VC.col_end(component,d1-1,d2-1,d4-1,col_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;
				std::copy(VC.col_begin(component,d1-1,d2-1,d4-1,col_range), VC.col_end(component,d1-1,d2-1,d4-1,col_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;

			std::pair<col_range_riter,const_col_range_riter> pair_it19;
			pair_it19 = std::mismatch(V.col_rbegin(component,d1-1,d2-1,d4-1,col_range), V.col_rend(component,d1-1,d2-1,d4-1,col_range),
					VC.col_rbegin(component,d1-1,d2-1,d4-1,col_range));
			res = res && (pair_it19.first == V.col_rend(component,d1-1,d2-1,d4-1,col_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;//35
				std::copy(V.col_rbegin(component,d1-1,d2-1,d4-1,col_range), V.col_rend(component,d1-1,d2-1,d4-1,col_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;
			std::pair<const_col_range_riter,col_range_riter> pair_it20;
			pair_it20 = std::mismatch(VC.col_rbegin(component,d1-1,d2-1,d4-1,col_range), VC.col_rend(component,d1-1,d2-1,d4-1,col_range),
					V.col_rbegin(component,d1-1,d2-1,d4-1,col_range));
			res = res && (pair_it20.first == VC.col_rend(component,d1-1,d2-1,d4-1,col_range));
			if(!res){
				std::cerr << "error #" << step << std::endl;
				std::copy(VC.col_rbegin(component,d1-1,d2-1,d4-1,col_range), VC.col_rend(component,d1-1,d2-1,d4-1,col_range),
						std::ostream_iterator<BLOCK>(std::cout," "));
			}
			step++;
		}
	}
	return res;
}

/*!
 * \brief external 4d component iterators properties function
 * \param CV input %GenericMultiComponent4d
 * \return true if the property is verified
 */
template<typename BLOCK>
bool def_genericmulticomponent4d_component_iterators4d_property
(const slip::GenericMultiComponent4d<BLOCK>& VC){
	bool res = true;
	int step = 1;
	slip::GenericMultiComponent4d<BLOCK> V(VC);

	typedef typename BLOCK::value_type T;
	std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);

	std::size_t d1 = V.dim1();
	std::size_t d2 = V.dim2();
	std::size_t d3 = V.dim3();
	std::size_t d4 = V.dim4();


	//global four dimensional component iterators

	typedef typename slip::GenericMultiComponent4d<BLOCK>::component_iterator4d iter4d;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_component_iterator4d const_iter4d;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_component_iterator4d riter4d;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_component_iterator4d const_riter4d;

	typedef typename slip::GenericMultiComponent4d<BLOCK>::component_iterator iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_component_iterator const_iter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_component_iterator riter;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_component_iterator const_riter;

	for(std::size_t component=0; component < b_size; ++component){

		std::pair<iter4d,const_iter> pair_it1;
		pair_it1 = std::mismatch(V.first_front_upper_left(component), V.last_back_bottom_right(component), VC.begin(component));
		res = res && (pair_it1.first == V.last_back_bottom_right(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.first_front_upper_left(component),
					V.last_back_bottom_right(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
			std::copy(VC.begin(component),
					VC.end(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
		}
		step++;
		std::pair<const_iter4d,iter> pair_it2;
		pair_it2 = std::mismatch(VC.first_front_upper_left(component), VC.last_back_bottom_right(component), V.begin(component));
		res = res && (pair_it2.first == VC.last_back_bottom_right(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.first_front_upper_left(component),
					V.last_back_bottom_right(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
			std::copy(VC.begin(component),
					VC.end(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
		}
		step++;

		std::pair<riter4d,const_riter> pair_it3;
		pair_it3 = std::mismatch(V.rfirst_front_upper_left(component), V.rlast_back_bottom_right(component), VC.rbegin(component));
		res = res && (pair_it3.first == V.rlast_back_bottom_right(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::cerr << V;
			std::cerr << std::endl;
			std::copy(V.rfirst_front_upper_left(component),
					V.rlast_back_bottom_right(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
			std::copy(VC.rbegin(component),
					VC.rend(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
		}
		step++;
		std::pair<const_riter4d,riter> pair_it4;
		pair_it4 = std::mismatch(VC.rfirst_front_upper_left(component), VC.rlast_back_bottom_right(component), V.rbegin(component));
		res = res && (pair_it4.first == VC.rlast_back_bottom_right(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::cerr << V;
			std::cerr << std::endl;
			std::copy(V.rfirst_front_upper_left(component),
					V.rlast_back_bottom_right(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
			std::copy(VC.rbegin(component),
					VC.rend(component),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
		}
		step++;
	}

	//box four dimensional component iterators

	for(std::size_t component=0; component < b_size; ++component){

		slip::Box4d<int> box(1,1,1,1,d1-1,d2-1,d3-1,d4-1);
		std::pair<iter4d,const_iter4d> pair_it5;
		pair_it5 = std::mismatch(V.first_front_upper_left(component,box), V.last_back_bottom_right(component,box),
				VC.first_front_upper_left(component,box));
		res = res && (pair_it5.first == V.last_back_bottom_right(component,box));
		if(!res){
			std::cerr << "error #" << step << std::endl;//5
		}
		step++;
		std::pair<const_iter4d,iter4d> pair_it6;
		pair_it6 = std::mismatch(VC.first_front_upper_left(component,box), VC.last_back_bottom_right(component,box),
				V.first_front_upper_left(component,box));
		res = res && (pair_it6.first == VC.last_back_bottom_right(component,box));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		std::pair<riter4d,const_riter4d> pair_it7;
		pair_it7 = std::mismatch(V.rfirst_front_upper_left(component,box), V.rlast_back_bottom_right(component,box),
				VC.rfirst_front_upper_left(component,box));
		res = res && (pair_it7.first == V.rlast_back_bottom_right(component,box));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::cerr << V;
			std::cerr << std::endl;
			std::copy(V.rfirst_front_upper_left(component,box),
					V.rlast_back_bottom_right(component,box),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
			std::copy(VC.rfirst_front_upper_left(component,box),
					VC.rlast_back_bottom_right(component,box),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
		}
		step++;
		std::pair<const_riter4d,riter4d> pair_it8;
		pair_it8 = std::mismatch(VC.rfirst_front_upper_left(component,box), VC.rlast_back_bottom_right(component,box),
				V.rfirst_front_upper_left(component,box));
		res = res && (pair_it8.first == VC.rlast_back_bottom_right(component,box));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
	}

	//Range four dimensional component iterators

	typedef typename slip::GenericMultiComponent4d<BLOCK>::component_iterator4d_range range_iter4d;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_component_iterator4d_range const_range_iter4d;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::reverse_component_iterator4d_range rrange_iter4d;
	typedef typename slip::GenericMultiComponent4d<BLOCK>::const_reverse_component_iterator4d_range const_rrange_iter4d;

	slip::Range<int> slab_range(0,d1-1,2);
	slip::Range<int> slice_range(0,d2-1,2);
	slip::Range<int> row_range(0,d3-1,2);
	slip::Range<int> col_range(0,d4-1,2);

	for(std::size_t component=0; component < b_size; ++component){
		std::pair<range_iter4d,const_range_iter4d> pair_it9;
		pair_it9 = std::mismatch(V.first_front_upper_left(component,slab_range,slice_range,row_range,col_range),
				V.last_back_bottom_right(component,slab_range,slice_range,row_range,col_range),
				VC.first_front_upper_left(component,slab_range,slice_range,row_range,col_range));
		res = res && (pair_it9.first == V.last_back_bottom_right(component,slab_range,slice_range,row_range,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
			std::copy(V.first_front_upper_left(component,slab_range,slice_range,row_range,col_range),
					V.last_back_bottom_right(component,slab_range,slice_range,row_range,col_range),
					std::ostream_iterator<T>(std::cerr," "));

			std::copy(VC.first_front_upper_left(component,slab_range,slice_range,row_range,col_range),
					VC.last_back_bottom_right(component,slab_range,slice_range,row_range,col_range),
					std::ostream_iterator<T>(std::cerr," "));
			std::cerr << std::endl;
		}
		step++;
		std::pair<const_range_iter4d,range_iter4d> pair_it10;
		pair_it10 = std::mismatch(VC.first_front_upper_left(component,slab_range,slice_range,row_range,col_range),
				VC.last_back_bottom_right(component,slab_range,slice_range,row_range,col_range),
				V.first_front_upper_left(component,slab_range,slice_range,row_range,col_range));
		res = res && (pair_it10.first == VC.last_back_bottom_right(component,slab_range,slice_range,row_range,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;//10
		}
		step++;

		std::pair<rrange_iter4d,const_rrange_iter4d> pair_it11;
		pair_it11 = std::mismatch(V.rfirst_front_upper_left(component,slab_range,slice_range,row_range,col_range),
				V.rlast_back_bottom_right(component,slab_range,slice_range,row_range,col_range),
				VC.rfirst_front_upper_left(component,slab_range,slice_range,row_range,col_range));
		res = res && (pair_it11.first == V.rlast_back_bottom_right(component,slab_range,slice_range,row_range,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		std::pair<const_rrange_iter4d,rrange_iter4d> pair_it12;
		pair_it12 = std::mismatch(VC.rfirst_front_upper_left(component,slab_range,slice_range,row_range,col_range),
				VC.rlast_back_bottom_right(component,slab_range,slice_range,row_range,col_range),
				V.rfirst_front_upper_left(component,slab_range,slice_range,row_range,col_range));
		res = res && (pair_it12.first == VC.rlast_back_bottom_right(component,slab_range,slice_range,row_range,col_range));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
	}

	for(std::size_t component=0; component < b_size; ++component){
		//component iterator4d advance functions

		slip::DPoint4d<int> saut(d1,d2,d3,d4);
		iter4d it_s = V.first_front_upper_left(component) + saut;
		res = res && (it_s == V.last_back_bottom_right(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		it_s = it_s - saut;
		res = res && (it_s == V.first_front_upper_left(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		it_s += saut;
		res = res && (it_s == V.last_back_bottom_right(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;//15
		}
		step++;
		it_s -= saut;
		res = res && (it_s == V.first_front_upper_left(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		//const component iterator4d advance functions

		const_iter4d it_cs = VC.first_front_upper_left(component) + saut;
		res = res && (it_cs == VC.last_back_bottom_right(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		it_cs = it_cs - saut;
		res = res && (it_cs == VC.first_front_upper_left(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		it_cs += saut;
		res = res && (it_cs == VC.last_back_bottom_right(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		it_cs -= saut;
		res = res && (it_cs == VC.first_front_upper_left(component));
		if(!res){
			std::cerr << "error #" << step << std::endl;//20
		}
		step++;

		//range component iterator4d advance functions

		slip::Range<int> slab_range_s(0,d1-1,1);
		slip::Range<int> slice_range_s(0,d2-1,1);
		slip::Range<int> row_range_s(0,d3-1,1);
		slip::Range<int> col_range_s(0,d4-1,1);

		range_iter4d it_r_s = V.first_front_upper_left(component,slab_range_s,slice_range_s,row_range_s,col_range_s) + saut;
		res = res && (it_r_s == V.last_back_bottom_right(component,slab_range_s,slice_range_s,row_range_s,col_range_s));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		it_r_s = it_r_s - saut;
		res = res && (it_r_s == V.first_front_upper_left(component,slab_range_s,slice_range_s,row_range_s,col_range_s));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		it_r_s += saut;
		res = res && (it_r_s == V.last_back_bottom_right(component,slab_range_s,slice_range_s,row_range_s,col_range_s));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		it_r_s -= saut;
		res = res && (it_r_s == V.first_front_upper_left(component,slab_range_s,slice_range_s,row_range_s,col_range_s));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;

		//const range component iterator4d advance functions

		const_range_iter4d it_r_cs = VC.first_front_upper_left(component,slab_range_s,slice_range_s,row_range_s,col_range_s) + saut;
		res = res && (it_r_cs == VC.last_back_bottom_right(component,slab_range_s,slice_range_s,row_range_s,col_range_s));
		if(!res){
			std::cerr << "error #" << step << std::endl;//25
		}
		step++;
		it_r_cs = it_r_cs - saut;
		res = res && (it_r_cs == VC.first_front_upper_left(component,slab_range_s,slice_range_s,row_range_s,col_range_s));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		it_r_cs += saut;
		res = res && (it_r_cs == VC.last_back_bottom_right(component,slab_range_s,slice_range_s,row_range_s,col_range_s));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
		it_r_cs -= saut;
		res = res && (it_r_cs == VC.first_front_upper_left(component,slab_range_s,slice_range_s,row_range_s,col_range_s));
		if(!res){
			std::cerr << "error #" << step << std::endl;
		}
		step++;
	}

	return res;
}



//If QuickCheck++ is available, the properties functions are encapsulated in quickcheck properties.
#ifdef HAVE_QCHECK

namespace quickcheck
{
/*!
 * \brief quickcheck generate function for a %block
 * \param n the quickcheck size hint
 * \param out the %block to generate
 */
template<typename T, std::size_t S>
void generate(size_t n, slip::block<T,S>& out);

/*!
 * \brief quickcheck generate function for %GenericMultiComponent4d
 * \param n the quickcheck size hint
 * \param out the %GenericMultiComponent4d to generate
 */
template<typename BLOCK>
void generate(size_t n, slip::GenericMultiComponent4d<BLOCK>& out);
}//namespace quickcheck

#include "Color_qc.hpp"

#include "quickcheck/quickcheck.hh"

namespace quickcheck
{

template<typename T, std::size_t S>
void generate(size_t n, slip::block<T,S>& out){
	for(std::size_t i=0; i<S; ++i){
		T a;
		generate(n,a);
		out[i] = a;
	}
}

template<typename BLOCK>
void generate(size_t n, slip::GenericMultiComponent4d<BLOCK>& out){
	unsigned d1,d2,d3,d4;
	d1 = generateInRange<unsigned>(1,20);
	d2 = generateInRange<unsigned>(1,20);
	d3 = generateInRange<unsigned>(1,20);
	d4 = generateInRange<unsigned>(1,20);
	typedef typename BLOCK::value_type T;
	std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);

	out.resize(d1,d2,d3,d4);
	for (size_t i = 0; i < d1; ++i)
		for (size_t j = 0; j < d2; ++j)
			for (size_t k = 0; k < d3; ++k)
				for (size_t t = 0; t < d4; ++t)
				{
					BLOCK val;
					for(std::size_t c=0; c<b_size;++c){
						T a;
						generate(n, a);
						val[c] = a;
					}
					out(i,j,k,t) = val;
				}
}

/*! \class ConstructorProperty1
 * \brief quickcheck property of %GenericMultiComponent4d that holds for the constructor with
 * three dimension parameters
 * \param size_type type of %GenericMultiComponent4d dimensions
 * \param BLOCK type of %GenericMultiComponent4d elements
 */
template<class BLOCK>
class GenericMultiComponent4dConstructorProperty1 : public Property<slip::GenericMultiComponent4d<BLOCK> > {
	/*!
	 * \brief property definition function
	 * \param x input %GenericMultiComponent4d
	 */
	bool holdsFor(const slip::GenericMultiComponent4d<BLOCK> &x){
		return def_genericmulticomponent4d_constructor_property1
				<typename slip::GenericMultiComponent4d<BLOCK>::size_type,BLOCK>
		(x.dim1(),x.dim2(),x.dim3(),x.dim4());
	}
};

/*! \class ConstructorProperty2
 * \brief quickcheck property of %GenericMultiComponent4d that holds for the constructors with
 * three dimension parameters and an initial value
 * \param size_type type of %GenericMultiComponent4d dimensions
 * \param BLOCK type of %GenericMultiComponent4d elements
 */
template<class BLOCK>
class GenericMultiComponent4dConstructorProperty2 : public Property<BLOCK,slip::GenericMultiComponent4d<BLOCK> > {
	/*!
	 * \brief property definition function
	 * \param val initial value
	 * \param x input %GenericMultiComponent4d
	 */
	bool holdsFor(const BLOCK &val, const slip::GenericMultiComponent4d<BLOCK> &x){
		return def_genericmulticomponent4d_constructor_property2
				<typename slip::GenericMultiComponent4d<BLOCK>::size_type,BLOCK>
		(x.dim1(),x.dim2(),x.dim3(),x.dim4(),val);
	}
};

/*! \class ConstructorProperty3
 * \brief quickcheck property of %GenericMultiComponent4d that holds for the constructors with
 * three dimension parameters and initial arrays of values.
 * \param size_type type of %GenericMultiComponent4d dimensions
 * \param BLOCK type of %GenericMultiComponent4d elements
 */
template<class BLOCK, typename T>
class GenericMultiComponent4dConstructorProperty3 : public Property<std::vector<T>,slip::GenericMultiComponent4d<BLOCK> > {
	/*!
	 * \brief property definition function
	 * \param val initial array of values
	 * \param x input %GenericMultiComponent4d
	 */
	bool holdsFor(const std::vector<T> & val, const slip::GenericMultiComponent4d<BLOCK> &x){
		return def_genericmulticomponent4d_constructor_property3
				<typename slip::GenericMultiComponent4d<BLOCK>::size_type,BLOCK,T>
		(x.dim1(),x.dim2(),x.dim3(),x.dim4(),val);
	}
	/*!
	 * \brief Inputs generation method
	 * \param n the quickcheck size hint
	 * \param initial array of values
	 * \param x input %GenericMultiComponent4d (it is not used by this test)
	 */
	void generateInput(size_t n, std::vector<T> & val, slip::GenericMultiComponent4d<BLOCK> &x) {
		std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
		generate(n, x);
		int dim = x.size() * b_size;
		for (int i = 0; i < dim; i++) {
			T a;
			generate(n-1, a);
			val.push_back(a);
		}
	}
};


/*!
 * \class AssignFillProperty
 * \brief quickcheck property of %GenericMultiComponent4d that holds for the assign and fill methods.
 * \param BLOCK type of %GenericMultiComponent4d elements
 */
template<class BLOCK>
class GenericMultiComponent4dAssignFillProperty : public Property<slip::GenericMultiComponent4d<BLOCK> > {
	bool holdsFor(const slip::GenericMultiComponent4d<BLOCK> &x){
		return def_genericmulticomponent4d_assign_fill_property<BLOCK>(x);
	}
};

/*!
 * \class ResizeProperty
 * \brief quickcheck property of %GenericMultiComponent4d that holds for the resize method.
 * \param BLOCK type of %GenericMultiComponent4d elements
 */
template<class BLOCK>
class GenericMultiComponent4dResizeProperty : public Property<slip::GenericMultiComponent4d<BLOCK> > {
	bool holdsFor(const slip::GenericMultiComponent4d<BLOCK> &x){
		unsigned d1,d2,d3,d4;
		d1 = generateInRange<unsigned>(1,20);
		d2 = generateInRange<unsigned>(1,20);
		d3 = generateInRange<unsigned>(1,20);
		d4 = generateInRange<unsigned>(1,20);
		return def_genericmulticomponent4d_resize_property<BLOCK>(x,d1,d2,d3,d4);
	}
};

/*!
 * \class SwapComparisonProperty
 * \brief quickcheck property of %GenericMultiComponent4d that holds for the swap and comparison methods.
 * \param BLOCK type of %GenericMultiComponent4d elements
 */
template<class BLOCK>
class GenericMultiComponent4dSwapComparisonProperty : public Property<slip::GenericMultiComponent4d<BLOCK>,
slip::GenericMultiComponent4d<BLOCK> > {
	bool holdsFor(const slip::GenericMultiComponent4d<BLOCK> &x,
			const slip::GenericMultiComponent4d<BLOCK> &y){
		return def_genericmulticomponent4d_swap_comparison_property<BLOCK>(x,y);
	}

	/*!
	 * \brief Inputs generation method
	 */
	void generateInput(size_t n, slip::GenericMultiComponent4d<BLOCK> &x,
			slip::GenericMultiComponent4d<BLOCK> &y) {
		typedef typename BLOCK::value_type T;
		std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
		generate(n, x);
		y.resize(x.dim1(),x.dim2(),x.dim3(),x.dim4());
		for (size_t i = 0; i < x.dim1(); ++i)
			for (size_t j = 0; j < x.dim2(); ++j)
				for (size_t k = 0; k < x.dim3(); ++k)
					for (size_t t = 0; t < x.dim4(); ++t)
					{
						BLOCK val;
						for(std::size_t c=0; c<b_size;++c){
							T a;
							generate(n-1, a);
							val[c] = a;
						}
						y(i,j,k,t) = val;
					}
	}
};

/*!
 * \class Iterators1dProperty
 * \brief quickcheck property of %GenericMultiComponent4d that holds for the iterators 1d definitions methods.
 * \param BLOCK type of %GenericMultiComponent4d elements
 */
template<class BLOCK>
class GenericMultiComponent4dIterators1dProperty : public Property<slip::GenericMultiComponent4d<BLOCK> > {
	bool holdsFor(const slip::GenericMultiComponent4d<BLOCK> &x){
		return def_genericmulticomponent4d_iterators1d_property<BLOCK>(x);
	}

	/*!
	 * \brief Inputs generation method
	 */
	void generateInput(size_t n, slip::GenericMultiComponent4d<BLOCK> &x) {
		typedef typename BLOCK::value_type T;
		std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
		unsigned d1,d2,d3,d4;
		d1 = generateInRange<unsigned>(3,20);
		d2 = generateInRange<unsigned>(3,20);
		d3 = generateInRange<unsigned>(3,20);
		d4 = generateInRange<unsigned>(3,20);

		x.resize(d1,d2,d3,d4);
		for (size_t i = 0; i < d1; ++i)
			for (size_t j = 0; j < d2; ++j)
				for (size_t k = 0; k < d3; ++k)
					for (size_t t = 0; t < d4; ++t)
					{
						BLOCK val;
						for(std::size_t c=0; c<b_size;++c){
							T a;
							generate(n-1, a);
							val[c] = a;
						}
						x(i,j,k,t) = val;
					}
	}
};

/*!
 * \class Iterators4dProperty
 * \brief quickcheck property of %GenericMultiComponent4d that holds for the iterators 4d related methods.
 * \param BLOCK type of %GenericMultiComponent4d elements
 */
template<class BLOCK>
class GenericMultiComponent4dIterators4dProperty : public Property<slip::GenericMultiComponent4d<BLOCK> > {
	bool holdsFor(const slip::GenericMultiComponent4d<BLOCK> &x){
		return def_genericmulticomponent4d_iterators4d_property<BLOCK>(x);
	}

	/*!
	 * \brief Inputs generation method
	 */
	void generateInput(size_t n, slip::GenericMultiComponent4d<BLOCK> &x) {
		typedef typename BLOCK::value_type T;
		std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
		unsigned d1,d2,d3,d4;
		d1 = generateInRange<unsigned>(3,20);
		d2 = generateInRange<unsigned>(3,20);
		d3 = generateInRange<unsigned>(3,20);
		d4 = generateInRange<unsigned>(3,20);

		x.resize(d1,d2,d3,d4);
		for (size_t i = 0; i < d1; ++i)
			for (size_t j = 0; j < d2; ++j)
				for (size_t k = 0; k < d3; ++k)
					for (size_t t = 0; t < d4; ++t)
					{
						BLOCK val;
						for(std::size_t c=0; c<b_size;++c){
							T a;
							generate(n-1, a);
							val[c] = a;
						}
						x(i,j,k,t) = val;
					}
	}
};

/*!
 * \class ComponentIterators1dProperty
 * \brief quickcheck property of %GenericMultiComponent4d that holds for the component iterators 1d definitions methods.
 * \param BLOCK type of %GenericMultiComponent4d elements
 */
template<class BLOCK>
class GenericMultiComponent4dComponentIterators1dProperty : public Property<slip::GenericMultiComponent4d<BLOCK> > {
	bool holdsFor(const slip::GenericMultiComponent4d<BLOCK> &x){
		return def_genericmulticomponent4d_component_iterators1d_property<BLOCK>(x);
	}

	/*!
	 * \brief Inputs generation method
	 */
	void generateInput(size_t n, slip::GenericMultiComponent4d<BLOCK> &x) {
		typedef typename BLOCK::value_type T;
		std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
		unsigned d1,d2,d3,d4;
		d1 = generateInRange<unsigned>(3,20);
		d2 = generateInRange<unsigned>(3,20);
		d3 = generateInRange<unsigned>(3,20);
		d4 = generateInRange<unsigned>(3,20);

		x.resize(d1,d2,d3,d4);
		for (size_t i = 0; i < d1; ++i)
			for (size_t j = 0; j < d2; ++j)
				for (size_t k = 0; k < d3; ++k)
					for (size_t t = 0; t < d4; ++t)
					{
						BLOCK val;
						for(std::size_t c=0; c<b_size;++c){
							T a;
							generate(n-1, a);
							val[c] = a;
						}
						x(i,j,k,t) = val;
					}
	}
};

/*!
 * \class ComponentIterators4dProperty
 * \brief quickcheck property of %GenericMultiComponent4d that holds for the Component iterators 4d related methods.
 * \param BLOCK type of %GenericMultiComponent4d elements
 */
template<class BLOCK>
class GenericMultiComponent4dComponentIterators4dProperty :
		public Property<slip::GenericMultiComponent4d<BLOCK> > {
	bool holdsFor(const slip::GenericMultiComponent4d<BLOCK> &x){
		return def_genericmulticomponent4d_component_iterators4d_property<BLOCK>(x);
	}

	/*!
	 * \brief Inputs generation method
	 */
	void generateInput(size_t n, slip::GenericMultiComponent4d<BLOCK> &x) {
		typedef typename BLOCK::value_type T;
		std::size_t b_size = static_cast<std::size_t>(BLOCK::SIZE);
		unsigned d1,d2,d3,d4;
		d1 = generateInRange<unsigned>(3,20);
		d2 = generateInRange<unsigned>(3,20);
		d3 = generateInRange<unsigned>(3,20);
		d4 = generateInRange<unsigned>(3,20);

		x.resize(d1,d2,d3,d4);
		for (size_t i = 0; i < d1; ++i)
			for (size_t j = 0; j < d2; ++j)
				for (size_t k = 0; k < d3; ++k)
					for (size_t t = 0; t < d4; ++t)
					{
						BLOCK val;
						for(std::size_t c=0; c<b_size;++c){
							T a;
							generate(n-1, a);
							val[c] = a;
						}
						x(i,j,k,t) = val;
					}
	}
};

}//namespace quickcheck


#endif //HAVE_QCHECK

#endif /* GENRICMULBLOCKICOMPONENT4D_QC_HPP_ */
