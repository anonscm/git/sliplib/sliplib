/*!
 ** \file Array3d_qc.hpp
 ** \brief Defining properties test functions that could be tested for %Array3d class
 ** \date 2013/09/17
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef ARRAY3D_QC_HPP_
#define ARRAY3D_QC_HPP_

#include "Array3d.hpp"

namespace slip
{
template <class T>
class Array3d;
}//namespace slip


#ifdef HAVE_QCHECK

namespace quickcheck
{
/*!
 * \brief quickcheck generate function for %Array3d
 * \param n the quickcheck size hint
 * \param out the %Array3d to generate
 */
template<typename T>
void generate(size_t n, slip::Array3d<T>& out);
}//namespace quickcheck


#include "quickcheck/quickcheck.hh"

namespace quickcheck
{
template<typename T>
inline
void generate(size_t n, slip::Array3d<T>& out)
{
	unsigned d1,d2,d3;
	generate(n, d1);
	generate(n, d2);
	generate(n, d3);
	unsigned prodd = d1 * d2 * d3;
	if (prodd > 0){
		out.resize(d1,d2,d3);
		for (size_t k = 0; k < d1; ++k)
			for (size_t i = 0; i < d2; ++i)
				for (size_t j = 0; j < d3; ++j)
				{
					T a;
					generate(n, a);
					out(k,i,j) = a;
				}
	}
}
}//namespace quickcheck

#endif //HAVE_QCHECK

#endif /* ARRAY3D_QC_HPP_ */
