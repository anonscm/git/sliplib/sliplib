/*!
 ** \file Block4d_qc.hpp
 ** \brief Defining properties test functions that could be tested for %Block4d class
 ** \date 2013/07/02
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef BLOCK4D_QC_HPP_
#define BLOCK4D_QC_HPP_

#include "Block4d.hpp"

template <typename T>
bool equal_real__(const T & a, const T & b){
  if(b <= std::numeric_limits<T>::epsilon()){
      if(a <= std::numeric_limits<T>::epsilon())
        return 1;
      else{
          std::cerr << a << " != " << b;
          std::cerr << std::endl;
          return 0;
      }
  }
  int coef = 10;
  if (std::abs(1-a/b) > coef*std::numeric_limits<T>::epsilon()){
      std::cerr << a << " != " << b;
      std::cerr << std::endl;
      std::cerr << std::abs(1 - a/b) << " > " << coef*std::numeric_limits<T>::epsilon();
      std::cerr << std::endl;
      return 0;
  }
  return 1;
}


namespace slip
{
  template <typename T, std::size_t NS, std::size_t NP, std::size_t NR, std::size_t NC>
  class Block4d;

}//namespace slip


#ifdef HAVE_QCHECK

namespace quickcheck
{
  /*!
   * \brief quickcheck generate function for %Block4d
   * \param n the quickcheck size hint
   * \param out the %Block4d to generate
   */
  template<typename T, std::size_t NS, std::size_t NP, std::size_t NR, std::size_t NC>
  void generate(size_t n, slip::block4d<T,NS,NP,NR,NC>& out);
}//namespace quickcheck


#include "quickcheck/quickcheck.hh"

namespace quickcheck
{
  template<typename T, std::size_t NS, std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  void generate(size_t n, slip::block4d<T,NS,NP,NR,NC>& out)
  {
    for(std::size_t l=0; l<NS; ++l)
      for(std::size_t k=0; k<NP; ++k)
        for(std::size_t i=0; i<NR; ++i)
          for(std::size_t j=0; j<NC; ++j)
            {
              T elem;
              generate(n, elem);
              out[l][k][i][j] = elem;
            }
  }

  /*!
   * \class Block4d Property 1
   * \brief quickcheck property 1 of %Block4d
   * \param T type of %Block4d elements
   */
  template<typename T, std::size_t NS, std::size_t NP, std::size_t NR, std::size_t NC>
  class Block4dProperty1 : public Property<slip::block4d<T,NS,NP,NR,NC> > {
    /*!
     * \brief property definition function
     * \param b input %Block4d
     */
    bool holdsFor(const slip::block4d<T,NS,NP,NR,NC> &b){
      std::size_t size = NS*NP*NR*NC;
      if((b.dim() != size) || (b.size() != size) || (b.size_max() != size) || b.empty()){
          std::cerr << "error 1." << std::endl;
          std::cerr << "size = " << size << " , " << b.dim() <<
              " , " << b.size() << " , "  << b.size_max();
          std::cerr << std::endl;
          return 0;
      }

      if((b.dim1() != NS) || (b.dim2() != NP) || (b.dim3() != NR) || (b.dim4() != NC)){
          std::cerr << "error 2." << std::endl;
          return 0;
      }

      slip::block4d<T,NS,NP,NR,NC> b1(b);
      slip::block4d<T,NS,NP,NR,NC> b2;
      b2 = b;
      if (b1 != b2){
          std::cerr << "error 3." << std::endl;
          return 0;
      }
      for(std::size_t l=0; l<NS; ++l)
        for(std::size_t k=0; k<NP; ++k)
          for(std::size_t i=0; i<NR; ++i)
            for(std::size_t j=0; j<NC; ++j){
                if (!equal_real__<T>(b1[l][k][i][j],b2[l][k][i][j])){
                    std::cerr << "error 4." << std::endl;
                    return 0;
                }
            }
      *b1(0,0,0) += T(1);
      if (b1(0,0,0,0) != b2(0,0,0,0)+T(1)){
          std::cerr << "error 5." << std::endl;
          return 0;
      }

      return 1;
    }
  };

  /*!
   * \class Block4d Property 2
   * \brief quickcheck property 2 of %Block4d
   * \param T type of %Block4d elements
   */
  template<typename T, std::size_t NS, std::size_t NP, std::size_t NR, std::size_t NC>
  class Block4dProperty2 : public Property<slip::block4d<T,NS,NP,NR,NC> > {
    /*!
     * \brief property definition function
     * \param b input %Block4d
     */
    bool holdsFor(const slip::block4d<T,NS,NP,NR,NC> &b){
      slip::block4d<T,NS,NP,NR,NC> bl;
      T val = T(1);
      bl.fill(val);
      typename slip::block4d<T,NS,NP,NR,NC>::iterator it;
      for(it = bl.begin();it!=bl.end();++it){
          if (*it != val){
              std::cerr << "error 1." << std::endl;
              std::cerr << *it << " != " << val;
              std::cerr << std::endl;
              return 0;
          }
      }
      typename slip::block4d<T,NS,NP,NR,NC>::reverse_iterator itr = bl.rbegin();
      for(;itr!=bl.rend();++itr){
          if (*itr != val){
              std::cerr << "error 2." << std::endl;
              std::cerr << *itr << " != " << val;
              std::cerr << std::endl;
              return 0;
          }
      }

      std::size_t size = NS*NP*NR*NC;
      T array[size];
      slip::iota(array,array+size,0,1);
      bl.fill(array);
      typename slip::block4d<T,NS,NP,NR,NC>::const_iterator it_c =
          static_cast<const slip::block4d<T,NS,NP,NR,NC> *>(&bl)->begin();
      typename slip::block4d<T,NS,NP,NR,NC>::const_iterator it_ce =
          static_cast<const slip::block4d<T,NS,NP,NR,NC> *>(&bl)->end();
      int i=0;
      for(;it_c!=it_ce;++it_c,++i){
          if (*it_c != i){
              std::cerr << "error 3." << std::endl;
              std::cerr << *it_c << " != " << i;
              std::cerr << std::endl;
              return 0;
          }
      }
      typename slip::block4d<T,NS,NP,NR,NC>::const_reverse_iterator it_rc =
          static_cast<const slip::block4d<T,NS,NP,NR,NC> *>(&bl)->rbegin();
      typename slip::block4d<T,NS,NP,NR,NC>::const_reverse_iterator it_rce =
          static_cast<const slip::block4d<T,NS,NP,NR,NC> *>(&bl)->rend();
      --i;
      for(;it_rc!=it_rce;++it_rc,--i)
        if (*it_rc != i){
            std::cerr << "error 4." << std::endl;
            std::cerr << *it_rc << " != " << i;
            std::cerr << std::endl;
            return 0;
        }


      return 1;
    }
  };

}//namespace quickcheck

#endif //HAVE_QCHECK

#endif /* BLOCK4D_QC_HPP_ */
