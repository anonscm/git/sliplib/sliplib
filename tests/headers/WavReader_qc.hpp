/*!
 ** \file WavReader_qc.hpp
 ** \brief Properties functions for the %WavReader class.
 ** Those functions are used by unit testing.
 ** \date 2013/04/17
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef WAVEREADER_QC_HPP_
#define WAVEREADER_QC_HPP_

#include <iostream>
#include <iomanip>
#include "Vector.hpp"
#include "WavReader.hpp"


namespace slip
{
template<class Container1d, typename T, std::size_t Nb_block>
class WavReader;
}//namespace slip


/*!
 * \brief properties of the wave file reader with only one container
 * \param filename of the signal to read
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_wavreader_read_property1(std::string filename,
		bool verbose = 0, std::ostream & outstream = std::cerr){
	slip::WavReader<slip::Vector<T>,T,1> audioread;
	try{
		audioread.initialize();
	}
	catch(slip::slip_exception &e){
		std::ostringstream err;
		err << "slip::" << "WavReader::initialize()::" << slip::FILE_OPEN_ERROR;
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << "\n";
				outstream <<  "->" << e.what_str() << "\n";
			}
			return 0;
		}
	}
	audioread.set_data_filename(filename);
	audioread.initialize();
	if(verbose){
		outstream << "File header : " << audioread.get_header();
		outstream << std::endl;
	}
	slip::Vector<T> sig;
	audioread.read(sig);
	if(sig.size() != audioread.get_output_size()){
		if(verbose){
			outstream << "-->" << sig.size();
			outstream << std::endl;
			outstream << "-->" << audioread.get_output_size();
			outstream << std::endl;
		}
		return 0;
	}
	audioread.release();
	try{
		audioread.read(sig);
	}
	catch(slip::slip_exception &e){
		std::ostringstream err;
		err << "slip::" << "WavReader::read()::" << slip::FILE_READ_ERROR << filename
				<< " | The reader needs to be initialized before reading.";
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << "\n";
				outstream <<  "->" << e.what_str() << "\n";
			}
			return 0;
		}
	}
	audioread.release();
	return 1;
}

/*!
 * \brief properties of the wave file reader with many containers
 * \param filename of the signal to read
 * \param verbose true if a verbose mode is asking, default is false.
 * \param outstream output ostream for verbose mode, default is std::cerr.
 * \return true if the property is verified
 */
template<typename T>
bool def_wavreader_read_property2(std::string filename,
		bool verbose = 0, std::ostream & outstream = std::cerr){
	slip::WavReader<slip::Vector<T>,T,4> audioread(filename);
	if(verbose){
		outstream << "File header : " << audioread.get_header();
		outstream << std::endl;
	}
	slip::Vector<T> sig1,sig2,sig3,sig4;
	audioread.read(sig1);
	audioread.read(sig2);
	audioread.read(sig3);
	audioread.read(sig4);
	std::size_t size = audioread.get_output_size();
	if((sig1.size()  + sig2.size() + sig3.size() + sig4.size()) != size){
		if(verbose){
			outstream << sig1.size() << "\n";
			outstream << sig2.size() << "\n";
			outstream << sig3.size() << "\n";
			outstream << sig4.size() << "\n";
		}
		return 0;
	}
	audioread.release();
	try{
		audioread.read(sig1);
	}
	catch(slip::slip_exception &e){
		std::ostringstream err;
		err << "slip::" << "WavReader::read()::" << slip::FILE_READ_ERROR << filename
				<< " | The reader needs to be initialized before reading.";
		if (err.str().compare(e.what_str()) != 0){
			if(verbose){
				outstream << "->" << err.str() << "\n";
				outstream <<  "->" << e.what_str() << "\n";
			}
			return 0;
		}
	}
	audioread.release();
	return 1;
}


#endif /* WAVEREADER_QC_HPP_ */
