/*!
 ** \file FFT4d_qc.hpp
 ** \date 2013/08/26
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief Defining properties test functions that could be tested for 4D FFT algorithms
 ** \note If QuickCheck++ is available, QuickCheck properties are also defined.
 */

#ifndef FFT4D_QC_HPP_
#define FFT4D_QC_HPP_

#include <limits>
#include <functional>
#include <vector>

#include "HyperVolume.hpp"
#include "FFT4d.hpp"

namespace slip
{
template <class T>
class HyperVolume;
}//namespace slip

template<class T>
T __real_part(const std::complex<T>& a){
	return std::real(a);
}

/*!
 * \brief comparing two %HyperVolume and return true if they are almost equal
 * \param A1 first %HyperVolume to compare
 * \param A2 second %HyperVolume to compare
 * \return true if equal
 */
template <class T>
bool are_hypervolume_almost_equal(const slip::HyperVolume<T> & A1, const slip::HyperVolume<T> & A2) {
	typedef typename slip::HyperVolume<T>::const_iterator const_it;
	const_it i1 = A1.begin(), i1end = A1.end();
	const_it i2 = A2.begin(), i2end = A2.end();
	while ((i1 != i1end) && (i2 != i2end)) {
		T diff = *i1 - *i2;
		double norm = static_cast<double>(diff*diff);
		if (norm > 1e-5) {
			return 0;
		}
		i1++;
		i2++;
	}
	return 1;
}

/*!
 * \brief first constructor property definition function
 * \param d1 first dimension
 * \param d2 second dimension
 * \param d3 third dimension
 * \param d4 fourth dimension
 * \return true if the property is verified
 */
template<typename size_type, typename T>
bool def_fft4d_property1(const slip::HyperVolume<T> &x){
	typedef typename std::complex<T> Complex;
	slip::HyperVolume<Complex> X(x.dim1(),x.dim2(),x.dim3(),x.dim4());
	slip::real_fft4d(x,X);
	slip::HyperVolume<Complex> O(x.dim1(),x.dim2(),x.dim3(),x.dim4());
	slip::ifft4d(X,O);
	slip::HyperVolume<T> Or(x.dim1(),x.dim2(),x.dim3(),x.dim4());
	std::transform(O.begin(),O.end(),Or.begin(), std::ptr_fun<const Complex &, T>(__real_part));
	return (are_hypervolume_almost_equal(x,Or));
}



//If QuickCheck++ is available, the properties functions are encapsulated in quickcheck properties.
#ifdef HAVE_QCHECK

namespace quickcheck
{
/*!
 * \brief quickcheck generate function for %HyperVolume
 * \param n the quickcheck size hint
 * \param out the %HyperVolume to generate
 */
template<typename T>
void generate(size_t n, slip::HyperVolume<T>& out);
}//namespace quickcheck

#include "quickcheck/quickcheck.hh"

namespace quickcheck
{

template<typename T>
void generate(size_t n, slip::HyperVolume<T>& out){
	unsigned d1,d2,d3,d4;
	d1 = generateInRange<unsigned>(2,20);
	d2 = generateInRange<unsigned>(2,20);
	d3 = generateInRange<unsigned>(2,20);
	d4 = generateInRange<unsigned>(2,20);

	out.resize(d1,d2,d3,d4);
	for (size_t i = 0; i < d1; ++i)
		for (size_t j = 0; j < d2; ++j)
			for (size_t k = 0; k < d3; ++k)
				for (size_t l = 0; l < d4; ++l)
				{
					T a;
					generate(n, a);
					out(i,j,k,l) = a;
				}
}

/*! \class ConstructorProperty1
 * \brief quickcheck property of 4D FFT algorithms with %HyperVolume
 * \param size_type type of %HyperVolume dimensions
 * \param T type of %HyperVolume elements
 */
template<class T>
class FFT4dProperty1 : public Property<slip::HyperVolume<T> > {
	/*!
	 * \brief property definition function
	 * \param x input %HyperVolume
	 */
	bool holdsFor(const slip::HyperVolume<T> &x){
		return def_fft4d_property1<typename slip::HyperVolume<T>::size_type,T>(x);
	}
};


}//namespace quickcheck


#endif //HAVE_QCHECK

#endif /* FFT4D_QC_HPP_ */
