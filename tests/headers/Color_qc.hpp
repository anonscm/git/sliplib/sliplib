/*!
 ** \file Color_qc.hpp
 ** \date 2013/03/25
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief Defining properties test functions that could be tested for %Color class
 */

#ifndef COLOR_QC_HPP_
#define COLOR_QC_HPP_

#include "Color.hpp"

namespace slip
{
  template <class T>
  class Color;
}//namespace slip

#ifdef HAVE_QCHECK

namespace quickcheck
{
  /*!
   * \brief quickcheck generate function for %Color
   * \param n the quickcheck size hint
   * \param out the %Color to generate
   */
  template<typename T>
  void generate(size_t n, slip::Color<T>& out);
}//namespace quickcheck

#include "quickcheck/quickcheck.hh"

namespace quickcheck
{
  template<typename T>
  void generate(size_t n, slip::Color<T>& out)
  {
    T r,g,b;
    generate(n,r);
    generate(n,g);
    generate(n,b);
    out.set(r,g,b);
  }
}//namespace quickcheck

#endif //HAVE_QCHECK
#endif /* COLOR_QC_HPP_ */
