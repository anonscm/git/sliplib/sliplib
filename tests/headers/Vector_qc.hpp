/*!
 ** \file Vector_qc.hpp
 ** \brief Defining properties test functions that could be tested for %Vector class
 ** \date 2013/04/19
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef VECTOR_QC_HPP_
#define VECTOR_QC_HPP_

#include "Vector.hpp"

namespace slip
{
template <class T>
class Vector;
}//namespace slip


#ifdef HAVE_QCHECK

namespace quickcheck
{
/*!
 * \brief quickcheck generate function for %Vector
 * \param n the quickcheck size hint
 * \param out the %Vector to generate
 */
template<typename T>
void generate(size_t n, slip::Vector<T>& out);
}//namespace quickcheck


#include "quickcheck/quickcheck.hh"

namespace quickcheck
{
template<typename T>
inline
void generate(size_t n, slip::Vector<T>& out)
{
	std::vector<T> v;
	generate(n, v);
	if (v.size() > 0){
		out.resize(v.size());
		std::copy(v.begin(),v.end(),out.begin());
	}
}
}//namespace quickcheck

#endif //HAVE_QCHECK



#endif /* VECTOR_QC_HPP_ */
