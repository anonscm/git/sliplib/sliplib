#!/bin/bash

TMP_DIRECTORY="$HOME/tmp/slip_test/"
DEST_DIRECTORY="./V1.0.0/"
FILE_BASE="backend_files.txt"
#run programs

echo "create $TMP_DIRECTORY"
mkdir $TMP_DIRECTORY

echo ""
echo ""
echo "---------------------------------------------------"
echo "      Run results                                  "
echo "---------------------------------------------------"
echo ""
for i in $(cat $FILE_BASE); do
    echo "--------------------------------------------"
    echo "run  $(basename $i) ..."
    $i > $TMP_DIRECTORY$(basename $i).res
    echo "write result in $TMP_DIRECTORY$(basename $i).res"
done

echo ""
echo ""
echo "---------------------------------------------------"
echo "      Check results                                "
echo "---------------------------------------------------"
echo ""
for i in $(cat $FILE_BASE); do
    echo "--------------------------------------------"
    echo "check  $(basename $i) ..."
    diff $TMP_DIRECTORY$(basename $i).res $DEST_DIRECTORY$(basename $i).res
done

