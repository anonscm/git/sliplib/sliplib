#include "Color.hpp"
#include <iostream>
#include <fstream>
#include <complex>
#include <algorithm>
#include <numeric>

#include "arithmetic_op.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


int main()
{

  slip::Color<double> C;
  std::cout<<C<<std::endl;
 
  slip::Color<float> C2(255.0);
  std::cout<<C2<<std::endl;

  double d[] = {125.0,145.0,123.0};
  
  slip::Color<double> C3(d);
  std::cout<<C3<<std::endl;

  slip::Color<double> C4 = C3;
  std::cout<<C4<<std::endl;
  
  std::cout<<(C3 == C)<<" "<<(C3 != C)<<std::endl;
  std::cout<<(C3 == C4)<<" "<<(C3 != C4)<<std::endl;
  
  
  C = C3;
  std::cout<<C<<std::endl;

  C[0] = 128.0;
  C[1] = 127.0;
  C[2] = 129.0;
  std::cout<<C<<std::endl;

  for(int i = 0; i < 3; ++i)
    std::cout<<C[i]<<" ";
  std::cout<<std::endl;

  C.swap(C3);
  std::cout<<C<<std::endl;
  std::cout<<C3<<std::endl;
  
  std::cout<<C.get_x1()<<" "<<C.get_x2()<<" "<<C.get_x3()<<std::endl;
  std::cout<<C.get_red()<<" "<<C.get_green()<<" "<<C.get_blue()<<std::endl;
  C.set_x1(243.0);
  C.set_x2(244.0);
  C.set_x3(245.0);
  std::cout<<C<<std::endl;
  C.set_red(212.0);
  C.set_green(213.0);
  C.set_blue(214.0);
  std::cout<<C<<std::endl;
  C.set(12.0,13.0,14.0);
  std::cout<<C<<std::endl;
 
  std::cout<<C.size()<<std::endl;
  std::cout<<C.max_size()<<std::endl;
  std::cout<<C.empty()<<std::endl;

  slip::Color<float> C5;
  C5.fill(128.0);
  std::cout<<C5<<std::endl;
  float tabf[] = {124.0,125.0,126.0};
  slip::Color<float> C6;
  C6.fill(tabf);
  std::cout<<C6<<std::endl;

  //Tests iterators
  std::cout<<std::endl;
  std::cout<<"Test iterators " << std::endl;
  std::cout<<C3<<std::endl;
  std::cout<<std::endl;
  std::cout<<"simple iteration " << std::endl;
  std::copy(C3.begin(),C3.end(),std::ostream_iterator<double>(std::cout," "));
  slip::Color<double> const C3c(C3);
  std::cout<<std::endl;
  std::cout<<"const iteration " << std::endl;
  std::copy(C3c.begin(),C3c.end(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
 //------------------------------
   //Artithmetic an mathematical operators
   //------------------------------
  slip::Color<float> MM;
  MM.fill(5.5);
  slip::Color<float> MM2;
  MM2.fill(2.2);
  std::cout<<"MM  = "<<MM<<std::endl;
  std::cout<<"MM2 = "<<MM2<<std::endl;
 
  MM += MM2;
  std::cout<<MM<<std::endl;
    
  MM -= MM2;
    std::cout<<MM<<std::endl;

    MM *= MM2;
    std::cout<<MM<<std::endl;

    MM /= MM2;
    std::cout<<MM<<std::endl;

    slip::Color<float> MM3;
    MM3 = MM + MM2;
    std::cout<<MM<<"+"<<MM2<<" = "<<MM3<<std::endl;

    MM3 = MM - MM2;
    std::cout<<MM<<"-"<<MM2<<" = "<<MM3<<std::endl;

    MM3 = MM * MM2;
    std::cout<<MM<<"*"<<MM2<<" = "<<MM3<<std::endl;

    MM3 = MM / MM2;
    std::cout<<MM<<"/"<<MM2<<" = "<<MM3<<std::endl;

    std::cout<<"- MM3 = "<<-MM3<<std::endl;


    std::cout<<"MM2 = "<<std::endl;
    std::cout<<MM2<<std::endl;
    MM2+=1.1;
    std::cout<<MM2<<std::endl;
    MM2-=1.1;
    std::cout<<MM2<<std::endl;
    MM2*=1.1;
    std::cout<<MM2<<std::endl;
    MM2/=1.1;
    std::cout<<MM2<<std::endl;


    //------------------------------
    //is grayscale
   //------------------------------
    slip::Color<double> CC1(1.0,0.5,0.3);
    std::cout<<"CC1 = "<<CC1<<std::endl;
    std::cout<<"CC1.is_grayscale() "<<CC1.is_grayscale()<<std::endl;
    slip::Color<double> CC2(0.2,0.2,0.2);
     std::cout<<"CC2 = "<<CC2<<std::endl;
    std::cout<<"CC2.is_grayscale() "<<CC2.is_grayscale()<<std::endl;
  
      
    //-------------------------------
    //serialization
    //-------------------------------
     // create and open a character archive for output
    std::ofstream ofs("color.txt");
    slip::Color<double> p(1.3,2.4,4.5);
        
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<p;
    }//to close archive
    std::ifstream ifs("color.txt");
    slip::Color<double> new_p;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_p;
    }//to close archive
    std::cout<<"new_p\n"<<new_p<<std::endl;

    std::ofstream ofsb("color.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<p;
    }//to close archive

    std::ifstream ifsb("color.bin");
    slip::Color<double> new_pb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_pb;
    }//to close archive
    std::cout<<"new_pb\n"<<new_pb<<std::endl;
   return 0;
}
