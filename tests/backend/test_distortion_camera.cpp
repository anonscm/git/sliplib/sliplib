#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <cmath>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <string>

#include "Vector.hpp"
#include "Matrix.hpp"
#include "io_tools.hpp"

#include "DistortionCamera.hpp"
#include "arithmetic_op.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

//using namespace::std;

// main program
//int main(int argc, char *argv[])
int main()
{
  //typedef float Type_Calcul;
  typedef double Type_Calcul;


  // --------------------------------------------------      
  // ----- constructors          ----------------------
  // --------------------------------------------------   
  std::cout<<"--Default constructor ---"<<std::endl;
  slip::DistortionCamera<Type_Calcul> distortion_camera1;
  std::cout<<distortion_camera1<<std::endl;
  std::cout<<std::endl;
 
  std::cerr << "reading calibration files" <<std::endl;
  distortion_camera1.read("default__cam1__distortion_model.dat");
  std::cout<<distortion_camera1<<std::endl;
  std::cout<<std::endl;
 
  std::cout<<"--Copy constructor ---"<<std::endl;
  slip::DistortionCamera<Type_Calcul> distortion_camera2(distortion_camera1);
  std::cout<<distortion_camera2<<std::endl;
  std::cout<<std::endl;

  std::cout<<"-- operator= ---"<<std::endl;
  slip::DistortionCamera<Type_Calcul> distortion_camera3;
  distortion_camera3 = distortion_camera1;
  std::cout<<distortion_camera3<<std::endl;
  std::cout<<std::endl;

  std::cout<<"-- self assignment ---"<<std::endl;
  distortion_camera1 = distortion_camera1;
  std::cout<<distortion_camera1<<std::endl;
  std::cout<<std::endl;


  // std::cout<<"-- compute ---"<<std::endl;
  // slip::Matrix<Type_Calcul> data_distortion(x,x);
  // slip::DistortionCamera<Type_Calcul> distortion_camera4;
  // distortion_camera4.compute(P);
  // std::cout<<distortion_camera4<<std::endl;
  //std::cout<<std::endl;
  
  // std::cout<<"-- undistort ---"<<std::endl;
  // slip::Matrix<Type_Calcul> P(x,x);
  // slip::Matrix<Type_Calcul> Q;
  // distortion_camera1.undistort(P,Q);
  // std::cout<<"P = \n"<<P<<std::endl;
  // std::cout<<"Q = \n"<<Q<<std::endl;
  // std::cout<<std::endl;

  std::cout<<"-- distort ---"<<std::endl;
  slip::Point2d<Type_Calcul> p(1.0,2.0);
  slip::Point2d<Type_Calcul> pd;
  distortion_camera1.distort(p,pd);
  std::cout<<"p = \n"<<p<<std::endl;
  std::cout<<"pd = \n"<<pd<<std::endl;
  std::cout<<std::endl;
  
  // --------------------------------------------------      
  // ----- camera models reading ----------------------
  // --------------------------------------------------      
  std::cerr << "reading calibration files" <<std::endl;

  slip::CameraModel<Type_Calcul>* distortion_camera;

  std::cerr << "reading distortion camera model" << std::endl;
  distortion_camera = new slip::DistortionCamera<Type_Calcul>();
  distortion_camera->read("default__cam1__distortion_model.dat");

  
  //int ni=100000000;
  int ni = 10;
  std::cerr<<"computes "<<ni<<" projection/backprojection "<<std::endl;
  std::cerr<<std::endl;
  std::time_t H1,H2;
  // computing distortion
  time(&H1);
  slip::Point2d<Type_Calcul> pixp(Type_Calcul(1),Type_Calcul(1));
  Type_Calcul z=Type_Calcul(1);
  slip::Point3d<Type_Calcul> voxp;
  
  for (int i=0; i<ni; ++i)
    {
      //pixp=slip::Point2d<Type_Calcul>(Type_Calcul(1),Type_Calcul(1));
      pixp=slip::Point2d<Type_Calcul>(Type_Calcul(2),Type_Calcul(3));
      
      voxp=(*distortion_camera).backprojection(pixp,z);
      //std::cout<<voxp<<std::endl;
      pixp=(*distortion_camera).projection(voxp);
      //std::cout<<pixp<<std::endl;
    }

  time(&H2);
 std::cerr <<std::endl;
 std::cerr << "####### distortion computation time ###################" <<std::endl;
  if ((H2-H1)% 86400 / 3600 !=0 )
   std::cerr << ((H2-H1)% 86400 / 3600) << " h ";
  if ((H2-H1) % 3600 / 60 !=0)
   std::cerr << ((H2-H1) % 3600 / 60) << " min ";
  if ((H2-H1) % 60 !=0)
   std::cerr << ((H2-H1) % 60) << " s";
 std::cerr <<std::endl;
 std::cerr << "#####################################################" <<std::endl;
 std::cerr <<std::endl;

 std::cout<<"--------- serializarion ---------------"<<std::endl;
 std::cout<<"--Default constructor ---"<<std::endl;
  slip::DistortionCamera<Type_Calcul> distortion_camera_s;
  std::cout<<distortion_camera_s<<std::endl;
  std::cout<<std::endl;
 
  std::cerr << "reading calibration files" <<std::endl;
  distortion_camera_s.read("default__cam1__distortion_model.dat");
  std::cout<<distortion_camera_s<<std::endl;
  std::cout<<std::endl;

  // create and open a character archive for output
    std::ofstream ofs("distortioncamera.txt");
      
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<distortion_camera_s;
    }//to close archive
    std::ifstream ifs("distortioncamera.txt");
    slip::DistortionCamera<Type_Calcul>  new_pinhole;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_pinhole;
    }//to close archive
    std::cout<<"new_pinhole\n"<<new_pinhole<<std::endl;

    std::ofstream ofsb("distortioncamera.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<distortion_camera_s;
    }//to close archive

    std::ifstream ifsb("distortioncamera.bin");
    slip::DistortionCamera<Type_Calcul> new_pinholeb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_pinholeb;
    }//to close archive
    std::cout<<"new_pinholeb\n"<<new_pinholeb<<std::endl;

  delete distortion_camera;

 
}





