#include<algorithm>
#include<iostream>
#include<iterator>
#include<complex>
#include "kstride_iterator.hpp"
#include "Array2d.hpp"
#include "Array.hpp"
int main()
{

  int t[] = {0,1,2,3,4,5,6,7,8,9};

  slip::kstride_iterator<int*,2> first(t);
  slip::kstride_iterator<int*,2> last(t+10);
  std::copy(first,last,std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;


  std::cout<<*first<<std::endl;
  for(std::size_t i = 0; i < 5; ++i)
   std::cout<<*(first + i)<<" "; 
 std::cout<<std::endl;
 for(std::size_t i = 1; i < 6; ++i)
   std::cout<<*(last - i)<<" ";
 std::cout<<std::endl;

 for(std::size_t i = 0; i < 5; ++i)
   std::cout<<first[i]<<" "; 
 std::cout<<std::endl;

 for(std::size_t i = 0; i < 5; ++i)
   std::cout<<((first + i) - first)<<" "; 
 std::cout<<std::endl;

 std::cout<<"nb element = "<<(last - first)<<std::endl;

for(std::size_t i = 0; i < 5; ++i)
   first[i] = i + 4; 

for(std::size_t i = 0; i < 5; ++i)
   std::cout<<first[i]<<" "; 
 std::cout<<std::endl;


  slip::Array2d<double> M(3,4);
  std::generate(M.begin(),M.end(),std::rand);
  std::cout<<M<<std::endl;
 

  slip::kstride_iterator<slip::Array2d<double>::iterator,2> firstM(M.begin());
  slip::kstride_iterator<slip::Array2d<double>::iterator,2> lastM(M.end());
  std::copy(firstM,lastM,std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;

slip::Array<std::complex<double> > Mcomp(10);
 slip::kstride_iterator<slip::Array2d<std::complex<double> >::iterator,2> itc(Mcomp.begin());
 for(std::size_t i = 0; i < 5; ++i)
   {
     //itc->real() = 2.0;
     *itc = std::complex<double>(2.0,0);
   }
 std::cout<<std::endl;
  slip::kstride_iterator<slip::Array2d<std::complex<double> >::const_iterator,2> itcc(Mcomp.begin());
 for(std::size_t i = 0; i < 5; ++i)
   {
     std::cout<<itcc->real()<<", ";
   }
 std::cout<<std::endl;

  return 0;
}
