#include <iostream>
#include <iomanip>
#include <fstream>
#include <iterator>

#include "Point2d.hpp"
#include "DPoint2d.hpp"

#include "arithmetic_op.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

int main()
{

  slip::Point2d<int> p;
  std::cout<<p<<std::endl;
   std::cout<<"name = "<<p.name()<<std::endl;
 
  slip::Point2d<float> pf(2.3,4.5);
  std::cout<<pf<<std::endl;
 
  slip::Point2d<float> pf2 = pf;
  std::cout<<pf2<<std::endl;
  
  slip::Point2d<float> pf3;
  pf3 = pf;
  std::cout<<pf3<<std::endl;
    
  std::cout<<pf3[0]<<" "<<pf3[1]<<std::endl;
  std::cout<<pf3.x1()<<" "<<pf3.x2()<<std::endl;
  
  std::cout<<"dim = "<<pf3.dim()<<std::endl;
  std::cout<<"size = "<<pf3.size()<<std::endl;
  std::cout<<"empty = "<<pf3.empty()<<std::endl;

  slip::Point2d<float> pf4(3.0,4.0);
  pf4.swap(pf3);
  std::cout<<pf3<<std::endl;
  std::cout<<pf4<<std::endl;
 
  pf4[0] = 6.0;
  pf4[1] = 7.0;
  std::cout<<pf4<<std::endl;
 
  pf4.x1() = 8.0;
  pf4.x2() = 9.0;
  std::cout<<pf4<<std::endl;
 
 
  // pf4[3] = 6.0;
  std::cout<<-pf4<<std::endl;
  
  std::cout<<"pf3 == pf4 :"<<(pf3==pf4)<<std::endl;
  std::cout<<"pf3 != pf4 :"<<(pf3!=pf4)<<std::endl;

  float tab[] = {1.0,2.0};
  slip::Point2d<float> pf5(tab);
  std::cout<<pf5<<std::endl;
  
  slip::Point2d<std::size_t> pf6(4,5);
  std::cout<<-pf6<<std::endl;

  slip::Point2d<int> pf7(1,2);
  slip::DPoint2d<int> dpf(1,3);
  std::cout<<pf7<<" += "<<dpf<<" = ";
  pf7+=dpf;
  std::cout<<pf7<<std::endl;
  std::cout<<pf7<<" -= "<<dpf<<" = ";
  pf7-=dpf;
  std::cout<<pf7<<std::endl;
  std::cout<<pf7<<" + "<<dpf<<" = "<<pf7+dpf<<std::endl;
  std::cout<<pf7<<" - "<<dpf<<" = "<<pf7-dpf<<std::endl;

  slip::Point2d<int> pf8(2,4);
  std::cout<<pf7<<" - "<<pf8<<" = "<<(pf7-pf8)<<std::endl;

    
  //----------------------------
  //iterators
  //----------------------------
  slip::Point2d<double> pi1(1.3,2.4);
  std::cout<<"pi1 = \n"<<pi1<<std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Global iterators on pi1" << std::endl;
  std::copy(pi1.begin(),pi1.end(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(pi1.rbegin(),pi1.rend(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  slip::Point2d<double> pi2(4.3,5.4);
  slip::Point2d<double> pi3;
  std::transform(pi1.begin(),pi1.end(),pi2.begin(),pi3.begin(),std::plus<double>());
 
  std::cout<<"std::transform(pi1.begin(),pi1.end(),pi2.begin(),pi3.begin(),std::plus<double>())"<<std::endl;
  std::cout<<"pi1 = \n"<<pi1<<std::endl;
  std::cout<<"pi2 = \n"<<pi2<<std::endl;
  std::cout<<"pi3 = \n"<<pi3<<std::endl;


  const slip::Point2d<double> pi4(7.8,9.6);
  std::copy(pi4.begin(),pi4.end(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;

  slip::Point<double,10> pi10;
  slip::iota(pi10.begin(),pi10.end(),1.0);
  std::cout<<"pi10 = \n "<<pi10<<std::endl;
  slip::Range<int> range(1,5,2);
  std::cout<<"Range = \n"<<range<<std::endl;
  std::copy(pi10.begin(range),pi10.end(range),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  
  slip::iota(pi10.rbegin(),pi10.rend(),1.0);
  std::cout<<"pi10 = \n "<<pi10<<std::endl;
  std::copy(pi10.rbegin(range),pi10.rend(range),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  
  
  // create and open a character archive for output
    std::ofstream ofs("point2d.txt");
    slip::Point2d<double> ps(1.3,2.4);
        
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<ps;
    }//to close archive
    std::ifstream ifs("point2d.txt");
    slip::Point2d<double> new_p;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_p;
    }//to close archive
    std::cout<<"new_p\n"<<new_p<<std::endl;

    std::ofstream ofsb("point2d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<ps;
    }//to close archive

    std::ifstream ifsb("point2d.bin");
    slip::Point2d<double> new_pb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_pb;
    }//to close archive
    std::cout<<"new_pb\n"<<new_pb<<std::endl;
return 0;
}
