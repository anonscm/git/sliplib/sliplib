#include "Array.hpp"
#include "Range.hpp"
#include "Box1d.hpp"
#include "Point1d.hpp"
#include "arithmetic_op.hpp"
#include <iostream>
#include <iomanip>
#include <vector>
#include <complex>
#include <algorithm>
#include <fstream>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


int main()
{

  //------------------------------
  //constructors
  //------------------------------
  slip::Array<float> A;
  std::cout<<"name = "<<A.name()<<" empty = "<<A.empty()<<" size = "<<A.size()<<" max_size = "<<A.max_size()<<std::endl;
  std::cout<<"A = "<<std::endl;
  std::cout<<A<<std::endl;
  
  

  slip::Array<int> A2(7);
  std::cout<<"empty = "<<A2.empty()<<" size = "<<A2.size()<<" max_size = "<<A2.max_size()<<std::endl;
  std::cout<<"A2 = "<<std::endl;
  std::cout<<A2<<std::endl;

  slip::Array<double> A3(7,5.0);
  std::cout<<"empty = "<<A3.empty()<<" size = "<<A3.size()<<" max_size = "<<A3.max_size()<<std::endl;
  std::cout<<"A3 = "<<std::endl;
  std::cout<<A3<<std::endl;
  

  double d[] = {1.2,3.4,5.5,6.6,7.7};
  slip::Array<double> A4(5,d);
  std::cout<<"empty = "<<A4.empty()<<" size = "<<A4.size()<<" max_size = "<<A4.max_size()<<std::endl;
  std::cout<<"A4 = "<<std::endl;
  std::cout<<A4<<std::endl;
 

  std::vector<int> V(8);
  for(std::size_t i = 0; i < 8; ++i)
    V[i] = i;
  slip::Array<int> A5(8,V.begin(),V.end());
  std::cout<<"empty = "<<A5.empty()<<" size = "<<A5.size()<<" max_size = "<<A5.max_size()<<std::endl;
  std::cout<<"A5 = "<<std::endl;
  std::cout<<A5<<std::endl;
 
  
  slip::Array<int> A6 = A5;
  std::cout<<"empty = "<<A6.empty()<<" size = "<<A6.size()<<" max_size = "<<A6.max_size()<<std::endl;
  std::cout<<"A6 = "<<std::endl;
  std::cout<<A6<<std::endl;
  //------------------------------


  //------------------------------
  //resize
  //------------------------------
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout<<"Resize A2 = "<<std::endl;
  A2.resize(10,4);
  std::cout<<A2<<std::endl;
  std::cout<<"Resize A = "<<std::endl;
  A.resize(12);
  std::cout<<A<<std::endl;
  //------------------------------

  //------------------------------
  //Affectation
  //------------------------------
  std::cout<<std::endl;
  std::cout<<std::endl;
  slip::Array<double> A7;
  A7 = A3;
  std::cout<<"empty = "<<A7.empty()<<" size = "<<A7.size()<<" max_size = "<<A7.max_size()<<std::endl;
  std::cout<<"A7 = "<<std::endl;
  std::cout<<A7<<std::endl;
    
  A7 = A7;
  std::cout<<"empty = "<<A7.empty()<<" size = "<<A7.size()<<" max_size = "<<A7.max_size()<<std::endl;
  std::cout<<"A7 = "<<std::endl;
  std::cout<<A7<<std::endl;  

  A4 = 12.2;
  std::cout<<"empty = "<<A4.empty()<<" size = "<<A4.size()<<" max_size = "<<A4.max_size()<<std::endl;
  std::cout<<"A4 = "<<std::endl;
  std::cout<<A4<<std::endl;  
  //------------------------------

 
  //------------------------------
  //Element access operators
  //------------------------------
  std::cout<<std::endl;
  std::cout<<std::endl;
  A7[2] = 123;
  A7(3) = 156;
 std::cout<<"A7 = "<<std::endl;
  for(std::size_t i = 0; i < A7.size();++i)
    {
      std::cout<<A7[i]<<" ";
    }
  std::cout<<std::endl;
  std::cout<<"A7 = "<<std::endl;
  for(std::size_t i = 0; i < A7.size();++i)
    {
      std::cout<<A7(i)<<" ";
    }
  std::cout<<std::endl;
  std::cout<<"Arange = "<<std::endl;
  slip::Array<double> Arange(10);
  double val = 0.0;
  for(size_t i = 0; i < Arange.size(); ++i)
    {
      Arange(slip::Point1d<std::size_t>(i)) = val;
      val = val + 1.0;
    }
  for(size_t i = 0; i < Arange.size(); ++i)
    {
      std::cout<<Arange(slip::Point1d<std::size_t>(i))<<" ";
    }
  std::cout<<std::endl;
  
  slip::Range<int> range_c(0,Arange.size()-1,2);
  std::cout<<"range = "<< range_c << std::endl;
  slip::Array<double> Aext = Arange(range_c);
  std::cout<<"Arange(range_c) = "<<std::endl;
  std::cout<<Aext<<std::endl;
  //------------------------------

  //------------------------------
  //swap
  //------------------------------
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout<<"A7.swap(A3)"<<std::endl;
  A7.swap(A3);
  std::cout<<"A3 = "<<std::endl;
  std::cout<<A3<<std::endl;
  std::cout<<"A7 = "<<std::endl;
  std::cout<<A7<<std::endl;
  //------------------------------

 //----------------------------
  //iterators
  //----------------------------
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Global iterators on A3" << std::endl;
  std::copy(A3.begin(),A3.end(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(A3.rbegin(),A3.rend(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::transform(A3.rbegin(),A3.rend(),A3.begin(),A7.begin(),std::plus<double>());
  std::cout<<"std::transform(A3.rbegin(),A3.rend(),A3.begin(),A7.begin(),std::plus<double>())"<<std::endl;
  std::cout<<"A7 = "<<std::endl;
  std::cout<<A7<<std::endl;
  std::cout<<std::endl;
  std::cout<<"A5 = "<<std::endl;
  std::cout<<A5<<std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "range iterators on A5" << std::endl;
  slip::Range<int> range(1,5,2);
  std::cout << "range : " << range << std::endl;
  std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  
  slip::Range<int> range2(0,7,3);
  std::cout << "range2 : " << range2 << std::endl;
  std::copy(A5.begin(range2),A5.end(range2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(A5.rbegin(range2),A5.rend(range2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;

  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "box iterators on A5" << std::endl;
  slip::Box1d<int> box(2,6);
  std::cout << "box : " << box << std::endl;
  std::copy(A5.begin(box),A5.end(box),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(A5.rbegin(box),A5.rend(box),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  
  slip::Box1d<int> box2d(0,7);
  std::cout << "box2d : " << box2d << std::endl;
  std::copy(A5.begin(box2d),A5.end(box2d),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(A5.rbegin(box2d),A5.rend(box2d),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;

  //----------------------------  


  //------------------------------
  //Comparison operators
  //------------------------------
  std::cout<<" A3 == A7 "<<(A3 == A7)<<std::endl;
  std::cout<<" A3 != A7 "<<(A3 != A7)<<std::endl;
  std::cout<<" A3 > A7 "<<(A3 > A7)<<std::endl;
  std::cout<<" A3 < A7 "<<(A3 < A7)<<std::endl;
  std::cout<<" A3 >= A7 "<<(A3 >= A7)<<std::endl;
  std::cout<<" A3 <= A7 "<<(A3 <= A7)<<std::endl;
  //------------------------------

  //------------------------------
  //fill methods
  //------------------------------
  std::cout<<std::endl;
  std::cout<<std::endl;
  A3.fill(4.0);
  std::cout << "A3.fill(4.0) = " << std::endl;
  std::cout<<A3<<std::endl;
  double d2[] = {1.2,3.4,5.5,6.6,7.7,8.8,9.9};
  std::cout << "A3.fill({1.2,3.4,5.5,6.6,7.7,8.8,9.9}) = " << std::endl;
  A3.fill(d2);
  std::cout<<A3<<std::endl;

  std::vector<int> V2(7);
  for(std::size_t i = 0; i < 7; ++i)
    V2[i] = i;
  std::cout << "A3.fill(std::vector<int> V2.begin(),std::vector<int> V2.end()) = " << std::endl;
  A3.fill(V2.begin(),V2.end());
  std::cout<<A3<<std::endl;
  //------------------------------

  std::cout<<std::endl;
  std::cout<<std::endl;
  slip::Array<slip::Array<std::complex<double> > > T(5);
  for(std::size_t i = 0; i < 5; ++i)
    {
      T[i].resize(i);
    }
  
  for(std::size_t i = 0; i < 5; ++i)
    {
      for(std::size_t  j = 0; j < T[i].size();++j)
	{
	  T[i][j] = std::complex<double>(i,j);
	}
    }

  for(std::size_t i = 0; i < 5; ++i)
    {
      for(std::size_t  j = 0; j < T[i].size();++j)
	{
	  std::cout<<T[i][j]<<" ";
	}
      std::cout<<std::endl;
      
    }
  
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Array ascii serialization " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   // create and open a character archive for output
    std::ofstream ofs("array.txt");
    slip::Array<float> b(10);
    slip::iota(b.begin(),b.end(),0.0f);
    
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<b;
    }//to close archive
    std::ifstream ifs("array.txt");
    slip::Array<float> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "Array binary serialization " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
     std::ofstream ofsb("array.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<b;
    }//to close archive

    std::ifstream ifsb("array.bin");
    slip::Array<float> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "complex Array ASCII serialization " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    slip::Array<std::complex<float> > bc(10);
    slip::iota(bc.begin(),bc.end(),std::complex<float>(1.0f,2.0f),
    	       std::complex<float>(1.0f,1.0f));
    std::ofstream ofsc("arrayc.txt");
     {
       
       boost::archive::text_oarchive oac(ofsc);
       oac<<bc;
     }//to close archive
    std::ifstream ifsc("arrayc.txt");
    slip::Array<std::complex<float> > new_pc;
    {
    boost::archive::text_iarchive iac(ifsc);
    iac>>new_pc;
    }//to close archive
    std::cout<<"new_pc = \n"<<new_pc<<std::endl;
    
  return 0;
}
