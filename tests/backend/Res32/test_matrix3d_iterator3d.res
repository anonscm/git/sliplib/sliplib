0 1 2 
3 4 5 
6 7 8 
9 10 11 

12 13 14 
15 16 17 
18 19 20 
21 22 23 

24 25 26 
27 28 29 
30 31 32 
33 34 35 

36 37 38 
39 40 41 
42 43 44 
45 46 47 

48 49 50 
51 52 53 
54 55 56 
57 58 59 



slice iterator
8 20 32 44 56 

56 44 32 20 8 

slice range iterator
8 32 56 

56 32 8 

slice range iterator + 1
20 44 

44 20 

row iterator
30 31 32 

32 31 30 

row range iterator
30 32 

32 30 

row range iterator + 1
31 

31 

col iterator
26 29 32 35 

35 32 29 26 

col range iterator
26 32 

32 26 

col range iterator + 1
29 35 

35 29 

global plane iterator
24 25 26 27 28 29 30 31 32 33 34 35 

35 34 33 32 31 30 29 28 27 26 25 24 

global plane 2d iterator
2 5 8 11 14 17 20 23 26 29 32 35 38 41 44 47 50 53 56 59 

59 56 53 50 47 44 41 38 35 32 29 26 23 20 17 14 11 8 5 2 

box plane 2d iterator
14 17 20 23 26 29 32 35 38 41 44 47 

47 44 41 38 35 32 29 26 23 20 17 14 

Global 3d iterator
0 1 2 
3 4 5 
6 7 8 
9 10 11 

12 13 14 
15 16 17 
18 19 20 
21 22 23 

24 25 26 
27 28 29 
30 31 32 
33 34 35 

36 37 38 
39 40 41 
42 43 44 
45 46 47 

48 49 50 
51 52 53 
54 55 56 
57 58 59 




59 58 57 
56 55 54 
53 52 51 
50 49 48 

47 46 45 
44 43 42 
41 40 39 
38 37 36 

35 34 33 
32 31 30 
29 28 27 
26 25 24 

23 22 21 
20 19 18 
17 16 15 
14 13 12 

11 10 9 
8 7 6 
5 4 3 
2 1 0 




3d box iterator
13 14 
16 17 
19 20 

25 26 
28 29 
31 32 

37 38 
40 41 
43 44 




44 43 
41 40 
38 37 

32 31 
29 28 
26 25 

20 19 
17 16 
14 13 




######################## CONST ####################


0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 

Const slice iterator
8 20 32 44 56 

56 44 32 20 8 

Const slice range iterator
8 32 56 

56 32 8 

const slice range iterator + 1
20 44 

44 20 

const row iterator
30 31 32 

32 31 30 

const row range iterator
30 32 

32 30 

const row range iterator + 1
31 

31 

const col iterator
26 29 32 35 

35 32 29 26 

const col range iterator
26 32 

32 26 

const col range iterator + 1
29 35 

35 29 

global const plane iterator
24 25 26 27 28 29 30 31 32 33 34 35 

35 34 33 32 31 30 29 28 27 26 25 24 

global const plane 2d iterator
2 5 8 11 14 17 20 23 26 29 32 35 38 41 44 47 50 53 56 59 

59 56 53 50 47 44 41 38 35 32 29 26 23 20 17 14 11 8 5 2 

box const plane 2d iterator
14 17 20 23 26 29 32 35 38 41 44 47 

47 44 41 38 35 32 29 26 23 20 17 14 

Global const 3d iterator
0 1 2 
3 4 5 
6 7 8 
9 10 11 

12 13 14 
15 16 17 
18 19 20 
21 22 23 

24 25 26 
27 28 29 
30 31 32 
33 34 35 

36 37 38 
39 40 41 
42 43 44 
45 46 47 

48 49 50 
51 52 53 
54 55 56 
57 58 59 




59 58 57 
56 55 54 
53 52 51 
50 49 48 

47 46 45 
44 43 42 
41 40 39 
38 37 36 

35 34 33 
32 31 30 
29 28 27 
26 25 24 

23 22 21 
20 19 18 
17 16 15 
14 13 12 

11 10 9 
8 7 6 
5 4 3 
2 1 0 




3d const box iterator
13 14 
16 17 
19 20 

25 26 
28 29 
31 32 

37 38 
40 41 
43 44 




44 43 
41 40 
38 37 

32 31 
29 28 
26 25 

20 19 
17 16 
14 13 




