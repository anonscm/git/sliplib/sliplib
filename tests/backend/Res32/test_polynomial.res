name = Polynomial degree = 0 max_size = 536870911 empty = 1


degree = 6 size = 7 max_size = 536870911 empty = 0
0 + 0 x^1 + 0 x^2 + 0 x^3 + 0 x^4 + 0 x^5 + 0 x^6

degree = 6 max_size = 536870911 empty = 0
2.2 + 2.2 x^1 + 2.2 x^2 + 2.2 x^3 + 2.2 x^4 + 2.2 x^5 + 2.2 x^6

degree = 6 max_size = 536870911 empty = 0
1.2 + 3.4 x^1 + 5.5 x^2 + 6.6 x^3 + 7.7 x^4 + 8.8 x^5 + 9.9 x^6

empty = 0 size = 6 max_size = 1073741823
(0,1,2,3,4,5)
degree = 6 max_size = 536870911 empty = 0
1.2 + 3.4 x^1 + 5.5 x^2 + 6.6 x^3 + 7.7 x^4 + 8.8 x^5 + 9.9 x^6

(2.3,2.3)
(2.3,2.3) + (2.3,2.3) x^1 + (2.3,2.3) x^2 + (2.3,2.3) x^3

(2.3,2.3)
degree = 6 max_size = 536870911 empty = 0
0 + 0 x^1 + 0 x^2 + 0 x^3 + 0 x^4 + 0 x^5 + 0 x^6

degree = 6 max_size = 536870911 empty = 0
0 + 0 x^1 + 0 x^2 + 0 x^3 + 0 x^4 + 0 x^5 + 0 x^6

degree = 5 max_size = 1073741823 empty = 0
4.4 + 4.4 x^1 + 4.4 x^2 + 4.4 x^3 + 4.4 x^4 + 4.4 x^5

5.5 + 5.5 x^1 + 5.5 x^2 + 5.5 x^3 + 5.5 x^4 + 5.5 x^5 + 5.5 x^6

2.2 + 2.2 x^1 + 2.2 x^2 + 2.2 x^3 + 2.2 x^4 + 2.2 x^5 + 2.2 x^6

0 + 0 x^1 + 2.1 x^2 + 1.2 x^3 + 0 x^4 + 0 x^5 + 0 x^6

0 0 2.1 1.2 0 0 0 
0 0 2.1 1.2 0 0 0 
4.4 + 4.4 x^1 + 4.4 x^2 + 4.4 x^3 + 4.4 x^4 + 4.4 x^5 + 4.4 x^6

1.2 + 3.4 x^1 + 5.5 x^2 + 6.6 x^3 + 7.7 x^4 + 8.8 x^5 + 9.9 x^6

1.2 + 3.4 x^1 + 5.5 x^2 + 6.6 x^3 + 7.7 x^4 + 8.8 x^5 + 9.9 x^6

4.4 + 4.4 x^1 + 4.4 x^2 + 4.4 x^3 + 4.4 x^4 + 4.4 x^5 + 4.4 x^6

Iterators 
1.2 3.4 5.5 6.6 7.7 8.8 9.9 
9.9 8.8 7.7 6.6 5.5 3.4 1.2 
Const Iterators 
1.2 3.4 5.5 6.6 7.7 8.8 9.9 
9.9 8.8 7.7 6.6 5.5 3.4 1.2 
11.1 + 12.2 x^1 + 13.2 x^2 + 13.2 x^3 + 13.2 x^4 + 12.2 x^5 + 11.1 x^6

 V2 == V7 0
 V2 != V7 1
 V2 > V7  0
 V2 < V7  1
 V2 >= V7 0
 V2 <= V7 1
4 + 4 x^1 + 4 x^2 + 4 x^3 + 4 x^4 + 4 x^5

1.2 + 3.4 x^1 + 5.5 x^2 + 6.6 x^3 + 7.7 x^4 + 8.8 x^5

0 + 1 x^1 + 2 x^2 + 3 x^3 + 4 x^4 + 8.8 x^5

1.2
3.4
5.5
6.6
7.7
8.8
9.9
9.9
8.8
7.7
6.6
5.5
3.4
1.2
1.80429e+09 + 8.46931e+08 x^1 + 1.68169e+09 x^2 + 1.71464e+09 x^3 + 1.95775e+09 x^4 + 4.24238e+08 x^5 + 7.19885e+08 x^6

7.7 + 7.7 x^1 + 7.7 x^2 + 7.7 x^3 + 7.7 x^4 + 7.7 x^5 + 7.7 x^6

5.5 + 5.5 x^1 + 5.5 x^2 + 5.5 x^3 + 5.5 x^4 + 5.5 x^5 + 5.5 x^6

5.5 + 5.5 x^1 + 5.5 x^2 + 5.5 x^3 + 5.5 x^4 + 5.5 x^5 + 5.5 x^6
+2.2 + 2.2 x^1 + 2.2 x^2 + 2.2 x^3 + 2.2 x^4 + 2.2 x^5 + 2.2 x^6
 = 7.7 + 7.7 x^1 + 7.7 x^2 + 7.7 x^3 + 7.7 x^4 + 7.7 x^5 + 7.7 x^6

5.5 + 5.5 x^1 + 5.5 x^2 + 5.5 x^3 + 5.5 x^4 + 5.5 x^5 + 5.5 x^6
-2.2 + 2.2 x^1 + 2.2 x^2 + 2.2 x^3 + 2.2 x^4 + 2.2 x^5 + 2.2 x^6
 = 3.3 + 3.3 x^1 + 3.3 x^2 + 3.3 x^3 + 3.3 x^4 + 3.3 x^5 + 3.3 x^6

12.1 + 24.2 x^1 + 36.3 x^2 + 48.4 x^3 + 60.5 x^4 + 72.6 x^5 + 84.7 x^6 + 72.6 x^7 + 60.5 x^8 + 48.4 x^9 + 36.3 x^10 + 24.2 x^11 + 12.1 x^12

12.1 + 24.2 x^1 + 36.3 x^2 + 48.4 x^3 + 60.5 x^4 + 72.6 x^5 + 84.7 x^6 + 72.6 x^7 + 60.5 x^8 + 48.4 x^9 + 36.3 x^10 + 24.2 x^11 + 12.1 x^12
*2.2 + 2.2 x^1 + 2.2 x^2 + 2.2 x^3 + 2.2 x^4 + 2.2 x^5 + 2.2 x^6
 = 26.62 + 79.86 x^1 + 159.72 x^2 + 266.2 x^3 + 399.3 x^4 + 559.02 x^5 + 745.36 x^6 + 878.46 x^7 + 958.32 x^8 + 984.94 x^9 + 958.32 x^10 + 878.46 x^11 + 745.36 x^12 + 559.02 x^13 + 399.3 x^14 + 266.2 x^15 + 159.72 x^16 + 79.86 x^17 + 26.62 x^18

- MM3 = -26.62 + -79.86 x^1 + -159.72 x^2 + -266.2 x^3 + -399.3 x^4 + -559.02 x^5 + -745.36 x^6 + -878.46 x^7 + -958.32 x^8 + -984.94 x^9 + -958.32 x^10 + -878.46 x^11 + -745.36 x^12 + -559.02 x^13 + -399.3 x^14 + -266.2 x^15 + -159.72 x^16 + -79.86 x^17 + -26.62 x^18

MM2 = 
2.2 + 2.2 x^1 + 2.2 x^2 + 2.2 x^3 + 2.2 x^4 + 2.2 x^5 + 2.2 x^6

3.3 + 2.2 x^1 + 2.2 x^2 + 2.2 x^3 + 2.2 x^4 + 2.2 x^5 + 2.2 x^6

2.2 + 2.2 x^1 + 2.2 x^2 + 2.2 x^3 + 2.2 x^4 + 2.2 x^5 + 2.2 x^6

2.42 + 2.42 x^1 + 2.42 x^2 + 2.42 x^3 + 2.42 x^4 + 2.42 x^5 + 2.42 x^6

2.2 + 2.2 x^1 + 2.2 x^2 + 2.2 x^3 + 2.2 x^4 + 2.2 x^5 + 2.2 x^6

2.4 + 6.8 x^1 + 11 x^2 + 13.2 x^3 + 15.4 x^4 + 17.6 x^5 + 19.8 x^6

4.4 + 6.8 x^1 + 11 x^2 + 13.2 x^3 + 15.4 x^4 + 17.6 x^5 + 19.8 x^6

4.4 + 6.8 x^1 + 11 x^2 + 13.2 x^3 + 15.4 x^4 + 17.6 x^5 + 19.8 x^6

2.4 + 6.8 x^1 + 11 x^2 + 13.2 x^3 + 15.4 x^4 + 17.6 x^5 + 19.8 x^6

0.4 + 6.8 x^1 + 11 x^2 + 13.2 x^3 + 15.4 x^4 + 17.6 x^5 + 19.8 x^6

-0.4 + -6.8 x^1 + -11 x^2 + -13.2 x^3 + -15.4 x^4 + -17.6 x^5 + -19.8 x^6

2.4 + 6.8 x^1 + 11 x^2 + 13.2 x^3 + 15.4 x^4 + 17.6 x^5 + 19.8 x^6

4.8 + 13.6 x^1 + 22 x^2 + 26.4 x^3 + 30.8 x^4 + 35.2 x^5 + 39.6 x^6

4.8 + 13.6 x^1 + 22 x^2 + 26.4 x^3 + 30.8 x^4 + 35.2 x^5 + 39.6 x^6

2.4 + 6.8 x^1 + 11 x^2 + 13.2 x^3 + 15.4 x^4 + 17.6 x^5 + 19.8 x^6

1.2 + 3.4 x^1 + 5.5 x^2 + 6.6 x^3 + 7.7 x^4 + 8.8 x^5 + 9.9 x^6

P1 = 1 + 2 x^1 + 3 x^2 + 4 x^3 + 5 x^4

P2 = 2 + 3 x^1 + 4 x^2

P1*P2 = 
2 + 7 x^1 + 16 x^2 + 25 x^3 + 34 x^4 + 31 x^5 + 20 x^6

P2*P1 = 
2 + 7 x^1 + 16 x^2 + 25 x^3 + 34 x^4 + 31 x^5 + 20 x^6

P2*=P1 : 
2 + 7 x^1 + 16 x^2 + 25 x^3 + 34 x^4 + 31 x^5 + 20 x^6

P10 = 2.1

P20 = 1.5

P10*P20 = 
3.15

P20*P10 = 
3.15

P1 = 1 + 2 x^1 + 3 x^2 + 4 x^3 + 5 x^4

P2 = 2 + 7 x^1 + 16 x^2 + 25 x^3 + 34 x^4 + 31 x^5 + 20 x^6

P2+=P1 : 
3 + 9 x^1 + 19 x^2 + 29 x^3 + 39 x^4 + 31 x^5 + 20 x^6

P1 = 1 + 2 x^1 + 3 x^2 + 4 x^3 + 5 x^4

P2 = 3 + 9 x^1 + 19 x^2 + 29 x^3 + 39 x^4 + 31 x^5 + 20 x^6

P1-=P2 : 
-2 + -7 x^1 + -16 x^2 + -25 x^3 + -34 x^4 + -31 x^5 + -20 x^6

P10 = 2.1

P20 = 1.5

P20+=P10 : 
3.6

P10 = 2.1

P20 = 3.6

P10-=P20 : 
-1.5

P1 = -2 + -7 x^1 + -16 x^2 + -25 x^3 + -34 x^4 + -31 x^5 + -20 x^6

P2 = 3 + 9 x^1 + 19 x^2 + 29 x^3 + 39 x^4 + 31 x^5 + 20 x^6

P1 + P2 : 
1 + 2 x^1 + 3 x^2 + 4 x^3 + 5 x^4

P2 + P1 : 
1 + 2 x^1 + 3 x^2 + 4 x^3 + 5 x^4

P1 = -2 + -7 x^1 + -16 x^2 + -25 x^3 + -34 x^4 + -31 x^5 + -20 x^6

P2 = 3 + 9 x^1 + 19 x^2 + 29 x^3 + 39 x^4 + 31 x^5 + 20 x^6

P1 - P2 : 
-5 + -16 x^1 + -35 x^2 + -54 x^3 + -73 x^4 + -62 x^5 + -40 x^6

P2 - P1 : 
5 + 16 x^1 + 35 x^2 + 54 x^3 + 73 x^4 + 62 x^5 + 40 x^6

P10 = -1.5

P20 = 3.6

P10 + P20 : 
2.1

P20 + P10 : 
2.1

P10 = -1.5

P20 = 3.6

P10 - P20 : 
-5.1

P20 - P10 : 
5.1

P2 = 3 + 9 x^1 + 19 x^2 + 29 x^3 + 39 x^4 + 31 x^5 + 20 x^6

P2 derivative 
9 + 38 x^1 + 87 x^2 + 156 x^3 + 155 x^4 + 120 x^5

P2 integral 
0 + 9 x^1 + 19 x^2 + 29 x^3 + 39 x^4 + 31 x^5 + 20 x^6

P3 = 1 + 2 x^1 + 3 x^2

P3(2.3) = 21.47
P3(2.3) = 21.47
P3(0) = 1
P3(1) = 6
P3(-1) = 2
(1,2,4,8,16,32)
(1,0,0,0,0,0)
(1,1,1,1,1,1)
(1,-1,1,-1,1,-1)
(1,2,5.5,17,55.375,185.75)
(1,2,4,8,16,32)
(1,2,5.5,17,55.375,185.75)
1 + 2 x^1

P4(2.3) = 5.6
2

P5(2.3) = 2
2 + 5 x^1 + 7.60297e-16 x^2

error = 2.81623e-28
-0.0160367 + 2.16312 x^1 + -1.3091 x^2 + 0.177219 x^3

error = 0.0180023
(0.0160367,-0.026078,-0.062028,-0.092193,-0.116951,-0.136676,-0.15174,-0.162509,-0.169343,-0.172596,-0.172615,-0.169737,-0.164291,-0.156597,-0.146961,-0.135682,-0.123044,-0.109319,-0.0947665,-0.079632,-0.0641469,-0.0485279,-0.0329771,-0.0176817,-0.00281321,0.0114721,0.0250334,0.0377457,0.0494994,0.0602003,0.0697696,0.0781436,0.0852734,0.0911244,0.0956763,0.0989225,0.100869,0.101536,0.100953,0.0991636,0.0962203,0.0921861,0.0871333,0.0811423,0.0743011,0.0667043,0.0584524,0.0496504,0.0404074,0.0308353,0.0210482,0.0111608,0.00128808,-0.00845606,-0.0179597,-0.0271137,-0.035813,-0.0439572,-0.0514516,-0.0582083,-0.0641469,-0.069195,-0.0732899,-0.0763783,-0.078418,-0.0793778,-0.0792387,-0.0779943,-0.0756513,-0.0722301,-0.067765,-0.0623051,-0.0559141,-0.0486707,-0.0406692,-0.0320191,-0.0228455,-0.0132892,-0.00350638,0.00633126,0.0160367)
approximation error = 0.763577
Pc = 1 + 2 x^1 + 3 x^2 + 4 x^3

-0.75 -0.5 -0.25 
1 0 0 
0 1 0 

((-0.0720851,0.638326),(-0.0720851,-0.638326),(-0.60583,0))
(10,49,142)
