#include <iostream>
#include <iomanip>
#include <vector>
#include "Signal.hpp"

#include "ContinuousLegendreBase1d.hpp"
#include "PolySupport.hpp"

// template <typename T>
// struct poly_fun : public std::unary_function<T,T>
// {
//   T operator()(const T& x)
//   {
//     return -4.2*x*x*x*x - 6.7*x*x*x + 9.7*x*x - x + 4.5;
//   }
// };

// template <typename T>
// struct sin_fun : public std::unary_function<T,T>
// {
//   T operator()(const T& x)
//    {
//      return 2.2*std::sin(0.125*x)-4.5*std::cos(0.125*x) + 8.9*std::sin(0.25*x) + 3.4;
//    }
// };

int main()
{
  typedef double T;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Default constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousLegendreBase1d<T,2> default_base2d;
  std::cout<<"default_base2d = \n"<<default_base2d<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Default constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousLegendreBase1d<T,3> default_base3d;
  std::cout<<"default_base3d = \n "<<default_base3d<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Constructor 2d" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  const std::size_t base2d1_range_size = 6;
  const std::size_t base2d1_degree = 5;
  const std::size_t base2d1_dim = 1;
  // slip::PolySupport<T> base2d1_support(0.0-(1.0/base2d1_range_size),5.0-(1.0/base2d1_range_size));
  slip::PolySupport<T> base2d1_support(-1.0,1.0);
  slip::POLY_BASE_GENERATION_METHOD generation_method = slip::POLY_BASE_GENERATION_FILE;
  //slip::Hardi<T> hardi;
  // slip::Rectangular<T> rect;
  // slip::Simpson<T> simpson;
  //slip::NewtonCotesMethod<T>* ptr_integration_method = &simpson;
  //slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_RECTANGULAR;
  //slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_SIMPSON;
  //slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_HOSNY2007;
  slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_EXACT;
  //    slip::ContinuousLegendreBase1d<T,2> base2d1(base2d1_dim,base2d1_degree,false,false,"file",base2d1_range_size,base2d1_support,*ptr_integration_method);
  slip::ContinuousLegendreBase1d<T,2> base2d1(base2d1_dim,base2d1_degree,true,true,generation_method,base2d1_range_size,base2d1_support,integration_method);
  std::cout<<"base2d1 = \n"<<base2d1<<std::endl;
  base2d1.generate();
  std::cout<<base2d1<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Analytic orthogonality matrix" << std::endl;
  std::cout <<  std::setfill('_') <<std::setw(60) << '_' << std::endl;
  slip::Matrix<T> Gram_base2d1;
  base2d1.analytic_orthogonality_matrix(Gram_base2d1);
  std::cout<<"Gram_base2d1 = \n"<<Gram_base2d1<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Orthogonality matrix" << std::endl;
  std::cout <<  std::setfill('_') <<std::setw(60) << '_' << std::endl;
  Gram_base2d1 = T();
  base2d1.orthogonality_matrix(Gram_base2d1);
  std::cout<<"Gram_base2d1 = \n"<<Gram_base2d1<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "is_analytically_orthogonal" << std::endl;
  std::cout <<  std::setfill('_') <<std::setw(60) << '_' << std::endl;
  std::cout<<"base2d1.is_analytically_orthogonal(1e-16) = "<<base2d1.is_analytically_orthogonal(1e-16)<<std::endl;
  std::cout<<"base2d1.is_analytically_orthogonal(1e-15) = "<<base2d1.is_analytically_orthogonal(1e-15)<<std::endl;
  std::cout<<"base2d1.is_analytically_orthogonal(1e-14) = "<<base2d1.is_analytically_orthogonal(1e-14)<<std::endl;
 std::cout<<"base2d1.is_analytically_orthogonal(1e-13) = "<<base2d1.is_analytically_orthogonal(1e-13)<<std::endl;
 
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "is_orthogonal" << std::endl;
 std::cout <<  std::setfill('_') <<std::setw(60) << '_' << std::endl;
 std::cout<<"base2d1.is_orthogonal(1e-16) = "<<base2d1.is_orthogonal(1e-16)<<std::endl;
 std::cout<<"base2d1.is_orthogonal(1e-15) = "<<base2d1.is_orthogonal(1e-15)<<std::endl;
 std::cout<<"base2d1.is_orthogonal(1e-14) = "<<base2d1.is_orthogonal(1e-14)<<std::endl;
 std::cout<<"base2d1.is_orthogonal(1e-13) = "<<base2d1.is_orthogonal(1e-13)<<std::endl;
 

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "analytic_orthogonality_precision" << std::endl;
  std::cout <<  std::setfill('_') <<std::setw(60) << '_' << std::endl;
  std::cout<<"base2d1.orthogonality_precision = "<<base2d1.analytic_orthogonality_precision()<<std::endl;
 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "orthogonality_precision" << std::endl;
  std::cout <<  std::setfill('_') <<std::setw(60) << '_' << std::endl;
  std::cout<<"base2d1.precision = "<<base2d1.orthogonality_precision()<<std::endl;
 

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "analytic_inner_product" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"base2d1.analytic_inner_product(0,0) = "<<base2d1.analytic_inner_product(0,0)<<std::endl;
  std::cout<<"base2d1.analytic_inner_product(1,1) = "<<base2d1.analytic_inner_product(1,1)<<std::endl;
  std::cout<<"base2d1.analytic_inner_product(1,0) = "<<base2d1.analytic_inner_product(1,0)<<std::endl;

 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "inner_product" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"base2d1.inner_product(0,0) = "<<base2d1.inner_product(0,0)<<std::endl;
  std::cout<<"base2d1.inner_product(1,1) = "<<base2d1.inner_product(1,1)<<std::endl;
  std::cout<<"base2d1.inner_product(1,0) = "<<base2d1.inner_product(1,0)<<std::endl;

  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "alpha and beta iterators" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(base2d1.alpha_cbegin(),base2d1.alpha_cend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::copy(base2d1.beta_cbegin(),base2d1.beta_cend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "poly iterators" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  for(std::size_t k = 0; k < base2d1.size(); ++k)
    {
      std::cout<<"P"<<k<<std::endl;
      std::copy(base2d1.poly_begin(k),base2d1.poly_end(k),std::ostream_iterator<T>(std::cout," "));
      std::cout<<std::endl;
    }

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Jacobi matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<T> base2d1_jacobi;
  base2d1.jacobi_matrix(base2d1_jacobi);
  std::cout<<"base2d1.jacobi_matrix(base2d1_jacobi) =\n"<<base2d1_jacobi<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Copy constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousLegendreBase1d<T,2> copy_base2d(base2d1);
  std::cout<<"copy_base2d = \n"<<copy_base2d<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "operator=" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousLegendreBase1d<T,2> equal_base2d;
  equal_base2d = base2d1;
  std::cout<<"equal_base2d = \n"<<equal_base2d<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "project" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<T> data(base2d1_range_size,T(5.12));
  // slip::Array<T> data(base2d1_range_size);
  // slip::iota(data.begin(),data.end(),T(0.0));
  std::cout<<"data =\n"<<data<<std::endl;
  for(std::size_t k = 0; k < base2d1.size(); ++k)
    {
      std::cout<<"(P"<<k<<",data) ="<<base2d1.project(data.begin(),data.end(),k)<<std::endl;
    }
  std::cout << "Discrete Gram matrix" << std::endl;
  for(std::size_t k = 0; k < base2d1.size(); ++k)
    {
       for(std::size_t l = 0; l < base2d1.size(); ++l)
	 {
	   //std::cout<<"(P"<<k<<","<<"P"<<l<<") ="<<base2d1.project(base2d1.poly_begin(k),base2d1.poly_end(k),l)<<std::endl;
	   //std::cout<<base2d1.project(base2d1.poly_begin(k),base2d1.poly_end(k),l)<<" ";
	   std::cout<<base2d1.inner_product(k,l)<<" ";
	 }
       std::cout<<std::endl;
    }
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "projection coefficients" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<T> base2d1_coef(base2d1.size());
  base2d1.project(data.begin(),data.end(),base2d1_coef.begin(),base2d1_coef.end());
  std::cout<<"base2d1_coef = \n"<<base2d1_coef<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "reconstruction" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<T> base2d1_reconstruct(base2d1.range_size());
  base2d1.reconstruct(base2d1_coef.begin(),base2d1_coef.end(),base2d1.size(),
		      base2d1_reconstruct.begin(),base2d1_reconstruct.end());
  std::cout<<"base2d1_reconstruct = \n"<<base2d1_reconstruct<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "analytic reconstruction " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::MultivariatePolynomial<T,2> base2d1_recont_poly =
    base2d1.reconstruct(base2d1_coef.begin(),base2d1_coef.end(),base2d1.size());
  std::cout<<"base2d1_recont_poly =\n"<<base2d1_recont_poly<<std::endl;
 
  slip::iota(data.begin(),data.end(),-T(1.0)+static_cast<T>(1.0/double(data.size())),static_cast<T>(2.0/double(data.size())));
  std::cout<<"data =\n"<<data<<std::endl;
  base2d1_coef.fill(T());
  base2d1.project(data.begin(),data.end(),base2d1_coef.begin(),base2d1_coef.end());

  std::cout<<"base2d1_coef = \n"<<base2d1_coef<<std::endl;
  //  base2d1_reconstruct(base2d1.range_size());
  base2d1_reconstruct.fill(T(0.0));
  base2d1.reconstruct(base2d1_coef.begin(),base2d1_coef.end(),base2d1_coef.size(),
		      base2d1_reconstruct.begin(),base2d1_reconstruct.end());
  std::cout<<"base2d1_reconstruct = \n"<<base2d1_reconstruct<<std::endl;

  slip::MultivariatePolynomial<T,2> base2d1_recont_poly2 =
    base2d1.reconstruct(base2d1_coef.begin(),base2d1_coef.end(),base2d1_coef.size());
  std::cout<<"base2d1_recont_poly2 =\n"<<base2d1_recont_poly2<<std::endl;
 

  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "accessors/mutators" << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  data[0] = static_cast<T>(3.0);
  data[1] = static_cast<T>(2.0);
  data[2] = static_cast<T>(1.0);
  data[3] = static_cast<T>(5.0);
  data[4] = static_cast<T>(6.0);
  data[5] = static_cast<T>(1.0);
  std::cout<<"data =\n"<<data<<std::endl;
  base2d1_coef.fill(T(0.0));
  base2d1.project(data.begin(),data.end(),base2d1_coef.begin(),base2d1_coef.end());

  std::cout<<"base2d1_coef = \n"<<base2d1_coef<<std::endl;
  
  
  base2d1_reconstruct.fill(T(0.0));
  base2d1.reconstruct(base2d1_coef.begin(),base2d1_coef.end(),base2d1_coef.size(),
		      base2d1_reconstruct.begin(),base2d1_reconstruct.end());
  std::cout<<"base2d1_reconstruct = \n"<<base2d1_reconstruct<<std::endl;

  slip::Signal<T> ramp_step;
  ramp_step.read_ascii("./../../../../tests/images/ramp_step_signal.dat");
  ramp_step.write_ascii("./ramp_step_signal.dat");
  const std::size_t ramp_step_size = ramp_step.size();
  const std::size_t ramp_degree = 30;//ramp_step_size/3;
  std::cout<<"ramp_step_size = "<<ramp_step_size<<std::endl;
  std::cout<<"ramp_degree    = "<<ramp_degree<<std::endl;
   
  slip::ContinuousLegendreBase1d<T,2> base2d_ramp(1,
  						      ramp_degree,
  						      true,
  						      false,
  						      generation_method,
  						      ramp_step_size,
  						      base2d1_support,
  						      integration_method);

  
  
  base2d_ramp.generate();
  //std::cout<<"base2d_ramp = \n"<<base2d_ramp<<std::endl;
  slip::Signal<T> ramp_step_coef(ramp_degree+1);
  base2d_ramp.project(ramp_step.begin(),ramp_step.end(),
		      ramp_step_coef.begin(),ramp_step_coef.end());

 
  slip::Signal<T> ramp_reconstruct(ramp_step_size);
  base2d_ramp.reconstruct(ramp_step_coef.begin(),ramp_step_coef.end(),
			  ramp_step_coef.size(),
			  ramp_reconstruct.begin(),ramp_reconstruct.end());

  ramp_reconstruct.write_ascii("ramp_reconstruct30.dat");


  slip::Signal<T> ramp_step_coef2(ramp_degree+1);
  base2d_ramp.project_non_ortho(ramp_step.begin(),ramp_step.end(),
				ramp_step_coef2.begin(),ramp_step_coef2.end());

  slip::Signal<T> ramp_reconstruct2(ramp_step_size);
  base2d_ramp.reconstruct(ramp_step_coef2.begin(),ramp_step_coef2.end(),
			  ramp_step_coef2.size(),
			  ramp_reconstruct2.begin(),ramp_reconstruct2.end());

  ramp_reconstruct2.write_ascii("ramp_reconstruct2_30.dat");

  // slip::Signal<T> poly_signal(ramp_step_size);
  // slip::Signal<T> x(ramp_step_size);
  // slip::iota(x.begin(),x.end(),0.0);
  // std::transform(x.begin(),x.end(),poly_signal.begin(),poly_fun<T>());
  // poly_signal.write_ascii("poly_signal.gnu");
  slip::Signal<T> poly_signal;
  poly_signal.read_ascii("./../../../../tests/images/poly_signal.gnu");
   slip::Signal<T> poly_signal_coef(ramp_degree+1);
   base2d_ramp.project(poly_signal.begin(),poly_signal.end(),
		       poly_signal_coef.begin(),poly_signal_coef.end());

   slip::Signal<T> poly_signal_reconstruct(poly_signal.size());
  base2d_ramp.reconstruct(poly_signal_coef.begin(),poly_signal_coef.end(),
			  poly_signal_coef.size(),
			  poly_signal_reconstruct.begin(),poly_signal_reconstruct.end());

  poly_signal_reconstruct.write_ascii("poly_signal_reconstruct30.dat");

  // slip::Signal<T> sin_signal(ramp_step_size);
  // slip::Signal<T> x(ramp_step_size);
  // slip::iota(x.begin(),x.end(),0.0);
  // std::transform(x.begin(),x.end(),sin_signal.begin(),sin_fun<T>());
  // sin_signal.write_ascii("sin_signal.gnu");


  slip::Signal<T> sin_signal;
  sin_signal.read_ascii("./../../../../tests/images/sin_signal.gnu");
   slip::Signal<T> sin_signal_coef(ramp_degree+1);
   base2d_ramp.project(sin_signal.begin(),sin_signal.end(),
		       sin_signal_coef.begin(),sin_signal_coef.end());

   slip::Signal<T> sin_signal_reconstruct(sin_signal.size());
  base2d_ramp.reconstruct(sin_signal_coef.begin(),sin_signal_coef.end(),
			  sin_signal_coef.size(),
			  sin_signal_reconstruct.begin(),sin_signal_reconstruct.end());

  sin_signal_reconstruct.write_ascii("sin_signal_reconstruct30.dat");

 // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 //  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 //  std::cout << "Big base test" << std::endl;
 //  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 //  const std::size_t big_base2d_range_size = 200;
 //  const std::size_t big_base2d_degree = 199;
 //  const std::size_t big_base2d_dim = 1;
 //  slip::PolySupport<T> big_base2d_support(T(),static_cast<T>(big_base2d_range_size-1));
 //  // slip::POLY_BASE_GENERATION_METHOD big_generation_method = slip::POLY_BASE_GENERATION_FILE;
 //   slip::POLY_BASE_GENERATION_METHOD big_generation_method = slip::POLY_BASE_GENERATION_STIELTJES;
 //  //slip::Hardi<T> hardi;
 //  // slip::Rectangular<T> rect;
 //  // slip::Simpson<T> simpson;
 //  //slip::NewtonCotesMethod<T>* ptr_integration_method = &simpson;
 //  //slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_RECTANGULAR;
 //  //slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_SIMPSON;
 //  //slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_HOSNY2007;
 //  slip::POLY_INTEGRATION_METHOD big_integration_method = slip::POLY_INTEGRATION_EXACT;
  
 //  slip::ContinuousLegendreBase1d<T,2> big_base2d(big_base2d_dim,big_base2d_degree,true,false,big_generation_method,big_base2d_range_size,big_base2d_support,big_integration_method);
 //  //std::cout<<"big_base2d = \n"<<big_base2d<<std::endl;
 //  big_base2d.generate();
 //  std::cout<<big_base2d<<std::endl;
 //  slip::Signal<T> P48(big_base2d_range_size);
 //  std::copy(big_base2d.poly_begin(99),big_base2d.poly_end(99),P48.begin());
 //  P48.write_ascii("P98.gnu");
  
  return 0;
}
