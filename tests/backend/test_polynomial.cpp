#include "Polynomial.hpp"
#include "Vector.hpp"
#include "Range.hpp"
#include "arithmetic_op.hpp"
#include "compare.hpp"
#include "io_tools.hpp"
#include "polynomial_algo.hpp"
#include <iostream>
#include <complex>
#include <algorithm>
#include <numeric>
#include <vector>
#include <cmath>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

double sinus_5(double x)
{
  return std::sin((2*3.141592653589793238462643383279 * x) / 5.0);
}


int main()
{

  //----------------------------------
  //  Constructors
  //----------------------------------
  slip::Polynomial<double> V;
  std::cout<<"name = "<<V.name()<<" degree = "<<V.degree()<<" max_size = "<<V.max_size()<<" empty = "<<V.empty()<<std::endl; 
  std::cout<<V<<std::endl;
 

  slip::Polynomial<double> V2(6);
  std::cout<<"degree = "<<V2.degree()<<" size = "<<V2.size()<<" max_size = "<<V2.max_size()<<" empty = "<<V2.empty()<<std::endl;
  std::cout<<V2<<std::endl;

   slip::Polynomial<double> V6(6,2.2);
  std::cout<<"degree = "<<V6.degree()<<" max_size = "<<V6.max_size()<<" empty = "<<V6.empty()<<std::endl;
  std::cout<<V6<<std::endl;
  
   double d[] = {1.2,3.4,5.5,6.6,7.7,8.8,9.9};
  slip::Polynomial<double> V4(6,d);
  std::cout<<"degree = "<<V4.degree()<<" max_size = "<<V4.max_size()<<" empty = "<<V4.empty()<<std::endl;
  std::cout<<V4<<std::endl;


  std::vector<int> VV(6);
  for(std::size_t i = 0; i < 6; ++i)
    VV[i] = i;
  slip::Array<int> V8(6,VV.begin(),VV.end());
  std::cout<<"empty = "<<V8.empty()<<" size = "<<V8.size()<<" max_size = "<<V8.max_size()<<std::endl;
  std::cout<<V8<<std::endl;

  slip::Polynomial<double> V5 = V4;
 std::cout<<"degree = "<<V5.degree()<<" max_size = "<<V5.max_size()<<" empty = "<<V5.empty()<<std::endl;
  std::cout<<V5<<std::endl;
  
  std::complex<double> c(2.3,2.3);
  std::cout<<c<<std::endl;
  slip::Polynomial<std::complex<double> > VC(3,c);
  
  std::cout<<VC<<std::endl;
  

  std::cout<<VC[1+1]<<std::endl;
  //----------------------------------
   
 //  //------------------------------
//   //resize
//   //------------------------------
//   V2.resize(10,5.2);
//   std::cout<<V2<<std::endl;
//   //-----------------------------

  //----------------------------------
  //  Affectations
  //----------------------------------
  V = V2;
  std::cout<<"degree = "<<V.degree()<<" max_size = "<<V.max_size()<<" empty = "<<V.empty()<<std::endl; 
  std::cout<<V<<std::endl;
  std::cout<<"degree = "<<V2.degree()<<" max_size = "<<V2.max_size()<<" empty = "<<V2.empty()<<std::endl;
  std::cout<<V2<<std::endl;
  slip::Polynomial<float> V3(5,4.4);
  V3 = V3;
  std::cout<<"degree = "<<V3.degree()<<" max_size = "<<V3.max_size()<<" empty = "<<V3.empty()<<std::endl; 
  std::cout<<V3<<std::endl;  
  slip::Polynomial<float> MM(6);
  MM = 5.5;
  std::cout<<MM<<std::endl;
  
  slip::Polynomial<float> MM2(6);
  MM2 = 2.2;
  std::cout<<MM2<<std::endl;
  //----------------------------------
 
  //------------------------------
  //Element access operators
  //------------------------------
  V2(2) = 2.1;
  V2[3] = 1.2;

  std::cout<<V2<<std::endl;
  
  for(size_t i = 0; i < V2.size(); ++i)
    {
      std::cout<<V2[i]<<" ";
    }
  std::cout<<std::endl;
  for(size_t i = 0; i < V2.size(); ++i)
    {
      std::cout<<V2(i)<<" ";
    }
  std::cout<<std::endl;
  //----------------------------------

  
  
  //------------------------------
  //swap
  //------------------------------
  slip::Polynomial<double> V7(6,4.4);
  
  std::cout<<V7<<std::endl;
  std::cout<<V5<<std::endl;
   
  V7.swap(V5);
  std::cout<<V7<<std::endl;
  std::cout<<V5<<std::endl;
  //------------------------------
  
  //----------------------------
  //iterators
  //----------------------------
  std::cout << "Iterators " << std::endl;
  std::copy(V4.begin(),V4.end(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(V4.rbegin(),V4.rend(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout << "Const Iterators " << std::endl;
  slip::Polynomial<double> const V4c(V4);
  std::copy(V4c.begin(),V4c.end(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(V4c.rbegin(),V4c.rend(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;

  std::transform(V4.rbegin(),V4.rend(),V4.begin(),V5.begin(),std::plus<double>());
  std::cout<<V5<<std::endl;

  //----------------------------   
 
  //------------------------------
  //Comparison operators
  //------------------------------
  std::cout<<" V2 == V7 "<<(V2 == V7)<<std::endl;
  std::cout<<" V2 != V7 "<<(V2 != V7)<<std::endl;
  std::cout<<" V2 > V7  "<<(V2 > V7)<<std::endl;
  std::cout<<" V2 < V7  "<<(V2 < V7)<<std::endl;
  std::cout<<" V2 >= V7 "<<(V2 >= V7)<<std::endl;
  std::cout<<" V2 <= V7 "<<(V2 <= V7)<<std::endl;
  //------------------------------
  
  
  //------------------------------
  //fill methods
  //------------------------------
  V3.fill(4.0);
  std::cout<<V3<<std::endl;
  float d2[] = {1.2,3.4,5.5,6.6,7.7,8.8,9.9};
  V3.fill(d2);
  std::cout<<V3<<std::endl;

  std::vector<int> Vec(7);
  for(std::size_t i = 0; i < 7; ++i)
    Vec[i] = i;
  V3.fill(Vec.begin(),Vec.begin()+5);
  std::cout<<V3<<std::endl;
  //------------------------------


 
  

  
  
 
  //------------------------------
  //iterators
  //-----------------------------
  //affichage lin�aire 
  std::copy(V7.begin(),V7.end(),std::ostream_iterator<double>(std::cout,"\n"));
  //a l'envers
  std::copy(V7.rbegin(),V7.rend(),std::ostream_iterator<double>(std::cout,"\n")); 


   std::generate(V.begin(),V.end(),std::rand);
   std::cout<<V<<std::endl;

   //------------------------------

  
   

   //------------------------------
   //Artithmetic an mathematical operators
   //------------------------------
    MM += MM2;
    std::cout<<MM<<std::endl;
    
    MM -= MM2;
    std::cout<<MM<<std::endl;

   

   
    slip::Polynomial<float> MM3(6);
    MM3 = MM + MM2;
    std::cout<<MM<<"+"<<MM2<<" = "<<MM3<<std::endl;

    MM3 = MM - MM2;
    std::cout<<MM<<"-"<<MM2<<" = "<<MM3<<std::endl;


    MM *= MM2;
    std::cout<<MM<<std::endl;

    MM3 = MM * MM2;
    std::cout<<MM<<"*"<<MM2<<" = "<<MM3<<std::endl;

   
    std::cout<<"- MM3 = "<<-MM3<<std::endl;


  //   double d11[] = {1.0,2.0,3.0};
//     slip::Polynomial<double> P11(2,d11);
//     double d12[] = {1.0,2.0,3.0,4.0,5.0};
//     slip::Polynomial<double> P12(4,d12);
//     std::cout<<P11<<std::endl;
//     P11+=P12;
    
//     std::cout<<"+="<<std::endl;
//     std::cout<<P12<<std::endl;
//     std::cout<<"="<<std::endl;
//     std::cout<<P11<<std::endl;
//     std::cout<<"-="<<std::endl;
//     std::cout<<P12<<std::endl;
//     std::cout<<"="<<std::endl;
//     std::cout<<(P11-=P12)<<std::endl;
  
    std::cout<<"MM2 = "<<std::endl;
    std::cout<<MM2<<std::endl;
    MM2+=1.1;
    std::cout<<MM2<<std::endl;
    MM2-=1.1;
    std::cout<<MM2<<std::endl;
    MM2*=1.1;
    std::cout<<MM2<<std::endl;
    MM2/=1.1;
    std::cout<<MM2<<std::endl;
     
    
    slip::Polynomial<double> MM5(6);
    MM5 = V4 + V4;
    std::cout<<MM5<<std::endl;
    std::cout<<MM5+2.0<<std::endl;
    std::cout<<(2.0+MM5)<<std::endl;
    
    std::cout<<MM5<<std::endl;
    std::cout<<MM5-2.0<<std::endl;
    std::cout<<(2.0-MM5)<<std::endl;
     
    std::cout<<MM5<<std::endl;
    std::cout<<MM5*2.0<<std::endl;
    std::cout<<(2.0*MM5)<<std::endl;
    
    std::cout<<MM5<<std::endl;
    std::cout<<MM5/2.0<<std::endl;
    //std::cout<<(2.0/MM5)<<std::endl;
    


    //----------------------------------

    float p1[] = {1.0, 2.0, 3.0, 4.0, 5.0};
    slip::Polynomial<float> P1(4,p1);
    std::cout<<"P1 = "<<P1<<std::endl;
    float p2[] = {2.0, 3.0, 4.0};
    slip::Polynomial<float> P2(2,p2);
    std::cout<<"P2 = "<<P2<<std::endl;

    std::cout<<"P1*P2 = "<<std::endl;
    std::cout<<P1*P2<<std::endl;
    std::cout<<"P2*P1 = "<<std::endl;
    std::cout<<P2*P1<<std::endl;
    
    P2*=P1;
    std::cout<<"P2*=P1 : "<<std::endl;
    std::cout<<P2<<std::endl;
    

    slip::Polynomial<float> P10(0,2.1);
    slip::Polynomial<float> P20(0,1.5);
    std::cout<<"P10 = "<<P10<<std::endl;
    std::cout<<"P20 = "<<P20<<std::endl;
    std::cout<<"P10*P20 = "<<std::endl;
    std::cout<<P10*P20<<std::endl;
    std::cout<<"P20*P10 = "<<std::endl;
    std::cout<<P20*P10<<std::endl;

    std::cout<<"P1 = "<<P1<<std::endl;
    std::cout<<"P2 = "<<P2<<std::endl;
    std::cout<<"P2+=P1 : "<<std::endl;
    P2+=P1;
    std::cout<<P2<<std::endl;
    std::cout<<"P1 = "<<P1<<std::endl;
    std::cout<<"P2 = "<<P2<<std::endl;
    std::cout<<"P1-=P2 : "<<std::endl;
    P1-=P2;
    std::cout<<P1<<std::endl;

    std::cout<<"P10 = "<<P10<<std::endl;
    std::cout<<"P20 = "<<P20<<std::endl;
    std::cout<<"P20+=P10 : "<<std::endl;
    P20+=P10;
    std::cout<<P20<<std::endl;
    std::cout<<"P10 = "<<P10<<std::endl;
    std::cout<<"P20 = "<<P20<<std::endl;
    std::cout<<"P10-=P20 : "<<std::endl;
    P10-=P20;
    std::cout<<P10<<std::endl;
    
    std::cout<<"P1 = "<<P1<<std::endl;
    std::cout<<"P2 = "<<P2<<std::endl;
    std::cout<<"P1 + P2 : "<<std::endl;
    std::cout<<P1 + P2<<std::endl;
    std::cout<<"P2 + P1 : "<<std::endl;
    std::cout<<(P2 + P1)<<std::endl;

    std::cout<<"P1 = "<<P1<<std::endl;
    std::cout<<"P2 = "<<P2<<std::endl;
    std::cout<<"P1 - P2 : "<<std::endl;
    std::cout<<(P1 - P2)<<std::endl;
    std::cout<<"P2 - P1 : "<<std::endl;
    std::cout<<(P2 - P1)<<std::endl;

    std::cout<<"P10 = "<<P10<<std::endl;
    std::cout<<"P20 = "<<P20<<std::endl;
    std::cout<<"P10 + P20 : "<<std::endl;
    std::cout<<P10 + P20<<std::endl;
    std::cout<<"P20 + P10 : "<<std::endl;
    std::cout<<(P20 + P10)<<std::endl;

    std::cout<<"P10 = "<<P10<<std::endl;
    std::cout<<"P20 = "<<P20<<std::endl;
    std::cout<<"P10 - P20 : "<<std::endl;
    std::cout<<(P10 - P20)<<std::endl;
    std::cout<<"P20 - P10 : "<<std::endl;
    std::cout<<(P20 - P10)<<std::endl;

    std::cout<<"P2 = "<<P2<<std::endl;
    std::cout<<"P2 derivative "<<std::endl;
    P2.derivative();
    std::cout<<P2<<std::endl;

    std::cout<<"P2 integral "<<std::endl;
    P2.integral();
    std::cout<<P2<<std::endl;

    float p3[] = {1.0, 2.0, 3.0};
    slip::Polynomial<float> P3(2,p3);
    std::cout<<"P3 = "<<P3<<std::endl;
    std::cout<<"P3("<<2.3<<") = "<<P3.evaluate(2.3f)<<std::endl;
    std::cout<<"P3("<<2.3<<") = "<<slip::eval_horner(P3.begin(),P3.end(),2.3f)<<std::endl;
    std::cout<<"P3("<<0.0<<") = "<<slip::eval_horner(P3.begin(),P3.end(),0.0f)<<std::endl;
    std::cout<<"P3("<<1.0<<") = "<<slip::eval_horner(P3.begin(),P3.end(),1.0f)<<std::endl;
    std::cout<<"P3("<<-1.0<<") = "<<slip::eval_horner(P3.begin(),P3.end(),-1.0f)<<std::endl;
    slip::Vector<float> power(6);
    slip::eval_power_basis(2.0f,5,power.begin(),power.end());
    std::cout<<power<<std::endl;
    slip::eval_power_basis(0.0f,5,power.begin(),power.end());
    std::cout<<power<<std::endl;
    slip::eval_power_basis(1.0f,5,power.begin(),power.end());
    std::cout<<power<<std::endl;
    slip::eval_power_basis(-1.0f,5,power.begin(),power.end());
    std::cout<<power<<std::endl;

    slip::eval_legendre_basis(2.0f,5,power.begin(),power.end());
    std::cout<<power<<std::endl;

    slip::EvalPowerBasis<float,slip::Vector<float>::iterator> power_basis;
    power_basis(2.0f,5,power.begin(),power.end());
    std::cout<<power<<std::endl;

    slip::EvalLegendreBasis<float,slip::Vector<float>::iterator> leg_basis;
    leg_basis(2.0f,5,power.begin(),power.end());
    std::cout<<power<<std::endl;

    float p4[] = {1.0, 2.0};
    slip::Polynomial<float> P4(1,p4);
    std::cout<<P4<<std::endl;
    std::cout<<"P4("<<2.3<<") = "<<P4.evaluate(2.3f)<<std::endl;

    float p5[] = {2.0};
    slip::Polynomial<float> P5(0,p5);
    std::cout<<P5<<std::endl;
    std::cout<<"P5("<<2.3<<") = "<<P5.evaluate(2.3f)<<std::endl;


    slip::Polynomial<double> P6;
    slip::Vector<double> xi(4);
    xi[0] = 0.0;
    xi[1] = 1.0;
    xi[2] = 4.0;
    xi[3] = 5.0;
    
    slip::Vector<double> yi(4);
    yi[0] = 2.0;
    yi[1] = 7.0;
    yi[2] = 22.0;
    yi[3] = 27.0;
   
    double err_P6 = P6.fit(xi.begin(),xi.end(),yi.begin(),2);
    std::cout<<P6<<std::endl;
    std::cout<<"error = "<<err_P6<<std::endl;

    //a sinusoidal function
    slip::Vector<double> xf((std::size_t)(4/0.05 + 1));
    slip::iota(xf.begin(),xf.end(),0.0,0.05);
    slip::Vector<double> yf(xf);
    yf.apply(sinus_5);

    //a subsampling of the sinusoidal function
    slip::Vector<double> xp((std::size_t)(4 + 1));
    slip::iota(xp.begin(),xp.end(),0.0,1.0);
    slip::Vector<double> yp(xp);
    yp.apply(sinus_5);

    slip::Polynomial<double> Psin5;
    double err_Psin5 = Psin5.fit(xp.begin(),xp.end(),yp.begin(),3);
    std::cout<<Psin5<<std::endl;
    std::cout<<"error = "<<err_Psin5<<std::endl;

    slip::Vector<double> ya(yf);
    for(std::size_t i = 0; i < ya.size(); ++i)
      {
	ya[i] = Psin5.evaluate(xf[i]);
      }
    std::cout<<(yf - ya)<<std::endl;
    std::cout<<"approximation error = "<<slip::L2_distance<double>(yf.begin(),yf.end(),ya.begin())<<std::endl;

    slip::write_ascii_1d<slip::Vector<double>::iterator,double>(yf.begin(),yf.end(
),"yf.dat");
    slip::write_ascii_1d<slip::Vector<double>::iterator,double>(ya.begin(),ya.end(
),"ya.dat");


    float pc[] = {1.0, 2.0, 3.0, 4.0};
    slip::Polynomial<float> Pc(3,pc);
    std::cout<<"Pc = "<<Pc<<std::endl;
    std::cout<<Pc.companion()<<std::endl;
    slip::Vector<std::complex<float> > roots = Pc.roots();
    std::cout<<roots<<std::endl;

    float e[] = {1.0, 2.0, 3.0};
    slip::Vector<float> ev(3);
    Pc.multi_evaluate(e,e+3,ev.begin());
    std::cout<<ev<<std::endl;

    //
    //serialization
    //
    std::ofstream ofs("polynomial.txt");
    slip::Array<float> a(9 );

    
    for(std::size_t i = 0; i < a.size(); ++i)
      {
	a[i] = static_cast<float>(i);
      }
    
    slip::Polynomial<float> b(a.size(),a.begin());
    std::cout<<b<<std::endl;
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<b;
    }//to close archive
    std::ifstream ifs("polynomial.txt");
    slip::Polynomial<float> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("polynomial.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<b;
    }//to close archive

    std::ifstream ifsb("polynomial.bin");
    slip::Polynomial<float> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;
    return 0;
}
