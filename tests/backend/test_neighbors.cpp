#include<iostream>
#include<iterator>

#include "DPoint1d.hpp"
#include "Array.hpp"
#include "Box1d.hpp"
#include "DPoint2d.hpp"
#include "Array2d.hpp"
#include "Box2d.hpp"
#include "DPoint3d.hpp"
#include "Array3d.hpp"
#include "Box3d.hpp"

#include "neighbors.hpp"
#include "arithmetic_op.hpp"
int main()
{
  //---classical 1d displacements
   slip::Array<float> Af(6);
   slip::iota(Af.begin(),Af.end(),10.0f);
   std::cout<<Af<<std::endl;
   slip::Array<float>::iterator itaf = Af.begin() + 2;
   
   std::cout<<"2c neighborhood of : "<<*itaf<<std::endl;
  for(std::size_t i = 0; i < slip::n_2c.size(); ++i)
    {
      std::cout<<slip::n_2c[i]<<" = "<<itaf[slip::n_2c[i]]<<std::endl;
    }
 std::cout<<std::endl;
  std::cout<<"2c previous of : "<<*itaf<<std::endl;
  for(std::size_t i = 0; i < slip::prev_2c.size(); ++i)
    {
      std::cout<<slip::prev_2c[i]<<" = "<<itaf[slip::prev_2c[i]]<<std::endl;
    }
  std::cout<<std::endl;
 std::cout<<"2c next of : "<<*itaf<<std::endl;
  for(std::size_t i = 0; i < slip::next_2c.size(); ++i)
    {
      std::cout<<slip::next_2c[i]<<" = "<<itaf[slip::next_2c[i]]<<std::endl;
    }
   std::cout<<std::endl; 
   std::cout<<std::endl;

  //---classical 2d displacements
   slip::Array2d<float> Mf(5,6);
  slip::iota(Mf.begin(),Mf.end(),30.0f);

  std::cout<<Mf<<std::endl;
    
  slip::Box2d<int> boxf(1,1,2,3);
  slip::Array2d<float>::iterator2d itf = Mf.upper_left(boxf);

  std::cout<<"4c neighborhood of : "<<*itf<<std::endl;
  for(std::size_t i = 0; i < slip::n_4c.size(); ++i)
    {
      std::cout<<slip::n_4c[i]<<" = "<<itf[slip::n_4c[i]]<<std::endl;
    }
   std::cout<<std::endl;
   std::cout<<"8c neighborhood of : "<<*itf<<std::endl;
   for(std::size_t i = 0; i < slip::n_8c.size(); ++i)
    {
      std::cout<<slip::n_8c[i]<<" = "<<itf[slip::n_8c[i]]<<std::endl;
    }
   std::cout<<std::endl;

   std::cout<<"6c even neighborhood of : "<<*itf<<std::endl;
   for(std::size_t i = 0; i < slip::n_6ce.size(); ++i)
    {
      std::cout<<slip::n_6ce[i]<<" = "<<itf[slip::n_6ce[i]]<<std::endl;
    }
   std::cout<<std::endl;

   std::cout<<"6c odd neighborhood of : "<<*itf<<std::endl;
   for(std::size_t i = 0; i < slip::n_6co.size(); ++i)
    {
      std::cout<<slip::n_6co[i]<<" = "<<itf[slip::n_6co[i]]<<std::endl;
    }
   std::cout<<std::endl;

   std::cout<<"4c previous of : "<<*itf<<std::endl;
   for(std::size_t i = 0; i < slip::prev_4c.size(); ++i)
    {
      std::cout<<slip::prev_4c[i]<<" = "<<itf[slip::prev_4c[i]]<<std::endl;
    }
   std::cout<<std::endl;
   std::cout<<"4c next of : "<<*itf<<std::endl;
   for(std::size_t i = 0; i < slip::next_4c.size(); ++i)
    {
      std::cout<<slip::next_4c[i]<<" = "<<itf[slip::next_4c[i]]<<std::endl;
    }
   std::cout<<std::endl;
    std::cout<<"8c previous of : "<<*itf<<std::endl;
     for(std::size_t i = 0; i < slip::prev_8c.size(); ++i)
   {
   std::cout<<slip::prev_8c[i]<<" = "<<itf[slip::prev_8c[i]]<<std::endl;
   }
     std::cout<<std::endl;
   std::cout<<"8c next of : "<<*itf<<std::endl;
   for(std::size_t i = 0; i < slip::next_8c.size(); ++i)
   {
   std::cout<<slip::next_8c[i]<<" = "<<itf[slip::next_8c[i]]<<std::endl;   
   }
   std::cout<<std::endl; 
   std::cout<<std::endl;

   //---classical 3d displacements
   slip::Array3d<float> A3f(3,5,6);
   slip::iota(A3f.begin(),A3f.end(),10.0f);
   std::cout<<A3f<<std::endl;
   slip::Box3d<int> box3f(1,1,1,2,2,2);
   slip::Array3d<float>::iterator3d it3f = A3f.front_upper_left(box3f);


   std::cout<<"6c neighborhood of : "<<*it3f<<std::endl;
   for(std::size_t i = 0; i < slip::n_6c.size(); ++i)
     {
       std::cout<<slip::n_6c[i]<<" = "<<it3f[slip::n_6c[i]]<<std::endl;
     }
   std::cout<<std::endl;

   std::cout<<"18c neighborhood of : "<<*it3f<<std::endl;
   for(std::size_t i = 0; i < slip::n_18c.size(); ++i)
     {
       std::cout<<slip::n_18c[i]<<" = "<<it3f[slip::n_18c[i]]<<std::endl;
     }
   std::cout<<std::endl;
   
   std::cout<<"26c neighborhood of : "<<*it3f<<std::endl;
   for(std::size_t i = 0; i < slip::n_26c.size(); ++i)
     {
       std::cout<<slip::n_26c[i]<<" = "<<it3f[slip::n_26c[i]]<<std::endl;
     }
   std::cout<<std::endl;

   std::cout<<"6c previous of : "<<*it3f<<std::endl;
   for(std::size_t i = 0; i < slip::prev_6c.size(); ++i)
     {
       std::cout<<slip::prev_6c[i]<<" = "<<it3f[slip::prev_6c[i]]<<std::endl;
     }
   std::cout<<std::endl;
   std::cout<<"6c next of : "<<*it3f<<std::endl;
   for(std::size_t i = 0; i < slip::next_6c.size(); ++i)
     {
       std::cout<<slip::next_6c[i]<<" = "<<it3f[slip::next_6c[i]]<<std::endl;
     }
   std::cout<<std::endl;

   std::cout<<"18c previous of : "<<*it3f<<std::endl;
   for(std::size_t i = 0; i < slip::prev_18c.size(); ++i)
     {
       std::cout<<slip::prev_18c[i]<<" = "<<it3f[slip::prev_18c[i]]<<std::endl;
     }
   std::cout<<std::endl;
   std::cout<<"18c next of : "<<*it3f<<std::endl;
   for(std::size_t i = 0; i < slip::next_18c.size(); ++i)
     {
       std::cout<<slip::next_18c[i]<<" = "<<it3f[slip::next_18c[i]]<<std::endl;
     }
   std::cout<<std::endl;

   std::cout<<"26c previous of : "<<*it3f<<std::endl;
   for(std::size_t i = 0; i < slip::prev_26c.size(); ++i)
     {
       std::cout<<slip::prev_26c[i]<<" = "<<it3f[slip::prev_26c[i]]<<std::endl;
     }
   std::cout<<std::endl;
   std::cout<<"26c next of : "<<*it3f<<std::endl;
   for(std::size_t i = 0; i < slip::next_26c.size(); ++i)
     {
       std::cout<<slip::next_26c[i]<<" = "<<it3f[slip::next_26c[i]]<<std::endl;
     }
   std::cout<<std::endl;

return 0;
}
