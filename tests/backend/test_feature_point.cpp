#include <iostream>
#include <iomanip>
#include <vector>
#include "Point2d.hpp"
#include "Point3d.hpp"

#include "FeaturePoint.hpp"

template<typename Real>
class MyFeatures;

template <typename Real>
std::ostream& operator<<(std::ostream & out, 
			 const MyFeatures<Real>& F);
template <typename Real>
bool operator==(const MyFeatures<Real>& x, 
		const MyFeatures<Real>& y);

template <typename Real>
bool operator!=(const MyFeatures<Real>& x, 
		const MyFeatures<Real>& y);

template <typename Real>
bool operator<(const MyFeatures<Real>& x, 
		const MyFeatures<Real>& y);
template <typename Real>
bool operator>(const MyFeatures<Real>& x, 
		const MyFeatures<Real>& y);
template <typename Real>
bool operator<=(const MyFeatures<Real>& x, 
		const MyFeatures<Real>& y);

template <typename Real>
bool operator>=(const MyFeatures<Real>& x, 
		const MyFeatures<Real>& y);

template <typename Real>
class MyFeatures
{
public:
  typedef MyFeatures<Real> self;
  MyFeatures():
    sigma_(Real()),
    response_(Real())
  {}
  MyFeatures(const Real& sigma,
	     const Real& response):
    sigma_(sigma),
    response_(response)
  {}

 friend std::ostream& operator<< <>(std::ostream & out, 
				    const self& F);

  const Real& get_sigma() const
  {
    return this->sigma_;
  }
  void set_sigma(const Real& sigma)
  {
    this->sigma_ = sigma;
  }
  const Real& get_response() const
  {
    return this->response_;
  }
  void set_response(const Real& response)
  {
    this->response_ = response;
  }
   
  friend bool operator== <>(const self& x, 
			    const self& y);
  
  friend bool operator!= <>(const self& x, 
  			    const self& y);

  friend bool operator< <>(const self& x, 
  			    const self& y);
  friend bool operator> <>(const self& x, 
  			    const self& y);

  friend bool operator<= <>(const self& x, 
  			    const self& y);
  friend bool operator>= <>(const self& x, 
  			    const self& y);

  std::string descriptor() const
  {
    return "sigma response";
  }

private:
  Real sigma_;
  Real response_;
    
};

template<typename Real>
inline
 std::ostream& operator<<(std::ostream & out, 
			  const MyFeatures<Real>& F)
{
  out<<F.sigma_<<" "<<F.response_;
  return out;
}

template<typename Real>
inline
bool operator==(const MyFeatures<Real>& x, 
		const MyFeatures<Real>& y)
{
  return (x.sigma_ == y.sigma_) && (x.response_ == y.response_);
}
template<typename Real>
inline
bool operator!=(const MyFeatures<Real>& x, 
		const MyFeatures<Real>& y)
{
  return !(x == y);
}

template<typename Real>
inline
bool operator<(const MyFeatures<Real>& x, 
	       const MyFeatures<Real>& y)
{
  return (x.sigma_ < y.sigma_);
}

template<typename Real>
inline
bool operator>(const MyFeatures<Real>& x, 
	       const MyFeatures<Real>& y)
{
  return (y < x);
}

template<typename Real>
inline
bool operator<=(const MyFeatures<Real>& x, 
	       const MyFeatures<Real>& y)
{
  return !(y < x);
}

template<typename Real>
inline
bool operator>=(const MyFeatures<Real>& x, 
	       const MyFeatures<Real>& y)
{
  return !(x < y);
}


int main()
{

  typedef double T;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Default constructor " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::FeaturePoint<slip::Point2d<int>, MyFeatures<T> > fp_default;
  std::cout<<"default feature point = "<<fp_default<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Constructor with point only" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Point2d<int> p(3,4);
  slip::FeaturePoint<slip::Point2d<int>, MyFeatures<T> > fp_ponly(p);
  std::cout<<"default feature point at p = "<<p<<" : "<<fp_ponly<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "General constructor " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  MyFeatures<T> features(3.4,6.5);
  std::cout<<"features = "<<features<<std::endl;
  slip::FeaturePoint<slip::Point2d<int>, MyFeatures<T> > ip(p,features);
  std::cout<<"generic feature point =  at p = "<<p<<" with feaures "<<features<< " : \n"<<ip<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Copy constructor " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::FeaturePoint<slip::Point2d<int>, MyFeatures<T> > fp_copy(ip);
  std::cout<<"fp_copy = "<<fp_copy<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "operator = " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::FeaturePoint<slip::Point2d<int>, MyFeatures<T> > fp_equal;
  fp_equal = ip;
  std::cout<<"fp_equal = "<<fp_equal<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "self assignment " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  fp_equal = fp_equal;
  std::cout<<"fp_equal = "<<fp_equal<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "accessors/mutators " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"ip = "<<ip<<std::endl;
  std::cout<<"ip.get_features() = "<<ip.get_features()<<std::endl;
  std::cout<<"ip.get_point()    = "<<ip.get_point()<<std::endl;
  std::cout<<"ip.set_features(MyFeatures<T>("<<2.2<<","<<3.3<<")) "<<std::endl;
  ip.set_features(MyFeatures<T>(2.2,3.3));
  std::cout<<ip<<std::endl;
  std::cout<<"ip.set_point(slip::Point2d<int>("<<5<<","<<6<<")) "<<std::endl;
  ip.set_point(slip::Point2d<int>(5,6));
  std::cout<<ip<<std::endl;
  std::cout<<"ip.get_point().x1()  = 7"<<std::endl;
  ip.get_point().x1()  = 7;
  std::cout<<ip<<std::endl;
  std::cout<<"ip.get_features().set_sigma(1.234)"<<std::endl;
  ip.get_features().set_sigma(1.234);
  
  std::cout<<ip<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Append feature point in ip.txt file " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  ip.write("ip.txt");
  ip.write("ip.txt");
   
   slip::Point2d<int> p2(5,6);
   MyFeatures<T> features2(2.1,4.5);
   slip::FeaturePoint<slip::Point2d<int>, MyFeatures<T> > ip2(p2,features2);


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Comparison operators " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<fp_equal<<" == "<<fp_copy<<" ? "<<(fp_equal==fp_copy)<<std::endl;
  std::cout<<fp_equal<<" != "<<fp_copy<<" ? "<<(fp_equal!=fp_copy)<<std::endl;
  std::cout<<fp_equal<<" == "<<ip<<" ? "<<(fp_equal==ip)<<std::endl;
  std::cout<<fp_equal<<" != "<<ip<<" ? "<<(fp_equal!=ip)<<std::endl;
  std::cout<<fp_equal<<" < "<<fp_copy<<" ? "<<(fp_equal<fp_copy)<<std::endl;
  std::cout<<fp_equal<<" > "<<fp_copy<<" ? "<<(fp_equal>fp_copy)<<std::endl;
  std::cout<<fp_equal<<" <= "<<fp_copy<<" ? "<<(fp_equal<=fp_copy)<<std::endl;
  std::cout<<fp_equal<<" >= "<<fp_copy<<" ? "<<(fp_equal>=fp_copy)<<std::endl;
  
 std::cout<<fp_equal<<" < "<<ip<<" ? "<<(fp_equal<ip)<<std::endl;
  std::cout<<fp_equal<<" > "<<ip<<" ? "<<(fp_equal>ip)<<std::endl;
  std::cout<<fp_equal<<" <= "<<ip<<" ? "<<(fp_equal<=ip)<<std::endl;
  std::cout<<fp_equal<<" >= "<<ip<<" ? "<<(fp_equal>=ip)<<std::endl;

  
  
  
    
   typedef typename slip::EmptyFeature Feature;
   typedef slip::FeaturePoint <slip::Point2d<T>,Feature> FPoint;
   FPoint fp(slip::Point2d<T>(T(-0.5),T(5.0)),slip::EmptyFeature());
   FPoint fp2(slip::Point2d<T>(T(0.0),T(5.0)),slip::EmptyFeature());
   std::cout<<fp<<" == "<<fp2<<" ? "<<(fp==fp2)<<std::endl;
   std::cout<<fp<<" == "<<fp<<" ? "<<(fp==fp)<<std::endl;
   std::cout<<fp<<" != "<<fp2<<" ? "<<(fp!=fp2)<<std::endl;
   std::cout<<fp<<" != "<<fp<<" ? "<<(fp!=fp)<<std::endl;

      
   std::vector<FPoint> plist;
   plist.push_back(fp);
   plist.push_back(fp2);
   auto it_plist = std::find(plist.begin(),plist.end(),fp);
   if(it_plist != plist.end())
     {
       std::cout<<fp<<" is in the list"<<std::endl;
     }
   else
     {
       std::cout<<fp<<" is not in the list"<<std::endl;
     }


   slip::FeaturePointList<slip::Point2d<int>, MyFeatures<T> > ipl;
   ipl.push_back(ip);
   ipl.push_back(ip2);
   ipl.write("ipl.txt");
   
   
   typedef slip::FeaturePoint <slip::Point3d<T>,Feature> FPoint3d;
   FPoint3d fp3d(slip::Point3d<T>(T(-0.5),T(5.0),T(2.0)),slip::EmptyFeature());
   FPoint3d fp3d2(slip::Point3d<T>(T(0.0),T(5.0), T(-2.0)),slip::EmptyFeature());
   std::cout<<fp3d<<" == "<<fp3d2<<" ? "<<(fp3d==fp3d2)<<std::endl;
   std::cout<<fp3d<<" == "<<fp3d<<" ? "<<(fp3d==fp3d)<<std::endl;
   std::cout<<fp3d<<" != "<<fp3d2<<" ? "<<(fp3d!=fp3d2)<<std::endl;
   std::cout<<fp3d<<" != "<<fp3d<<" ? "<<(fp3d!=fp3d)<<std::endl;
   std::vector<FPoint3d> plist3d;
   plist3d.push_back(fp3d);
   plist3d.push_back(fp3d2);
   auto it_plist3d = std::find(plist3d.begin(),plist3d.end(),fp3d);
   if(it_plist3d != plist3d.end())
     {
       std::cout<<fp3d<<" is in the list"<<std::endl;
     }
   else
     {
       std::cout<<fp3d<<" is not in the list"<<std::endl;
     }
   
   typedef slip::FeaturePoint <slip::Point3d<T>,MyFeatures<T> > FPoint3d2;
   slip::Point3d<T> p3d(T(1.0), T(2.0), T(3.0));
   MyFeatures<T> features3d(3.4,6.5);
   FPoint3d2 ip3d(p3d,features);
   std::cout<<"generic feature point =  at p3d = "<<p3d<<" with feaures "<<features3d<< " : \n"<<ip3d<<std::endl;
   slip::Point3d<T> p3d2(T(4.0), T(5.0), T(6.0));
   MyFeatures<T> features3d2(4.5,6.7);
   FPoint3d2 ip3d2(p3d2,features3d2);
   std::cout<<"generic feature point =  at p3d2 = "<<p3d2<<" with feaures "<<features3d2<< " : \n"<<ip3d2<<std::endl;
  
   slip::FeaturePointList<slip::Point3d<T>, MyFeatures<T> > ipl3d;
   ipl3d.push_back(ip3d);
   ipl3d.push_back(ip3d2);
   ipl3d.write("ipl3d.txt");
   
  return 0;
}
