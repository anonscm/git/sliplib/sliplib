#include<algorithm>
#include<iostream>
#include<iomanip>
#include<iterator>
#include <numeric>
#include <complex>

#include "Vector.hpp"


template<typename T>
void daccum(T& a){static T init = T(0); a = init++;}
template<typename T>
void disp(T const& a){std::cout << a <<" ";}

int main()
{

  typedef double T; 
  //Vectord
  std::cout <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Vectord for tests and test of begin() end end() iterators" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  slip::Vector<T> M(8);
  std::for_each(M.begin(),M.end(),daccum<T>);
  std::cout<<M<<std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;

  //range
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Range for tests" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Range<int> r(0,M.size()-1,2);
  slip::Range<int> rd(1,M.size()-1,2);
  std::cout << "range : " << r << std::endl;
  std::cout << "range + 1 : " << rd << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;

  //boxes
  slip::Box1d<int> b1(2,6);
  slip::Box1d<int> b2(0,7);
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Boxes for tests" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout << "box1 : " << b1 << std::endl;
  std::cout << "box2 : " << b2 << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;

  // Global iterators
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "global iterators" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  std::copy(M.begin(),M.end(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rbegin(),M.rend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  // range :
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "range iterators" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.begin(r),M.end(r),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rbegin(r),M.rend(r),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "range iterator + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.begin(rd),M.end(rd),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rbegin(rd),M.rend(rd),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //box1 iterators : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "box1 iterators" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.begin(b1),M.end(b1),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rbegin(b1),M.rend(b1),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //box2 iterators : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "box2 iterators" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.begin(b2),M.end(b2),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rbegin(b2),M.rend(b2),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;


 
  //****************************** Const ***********************************//
  
  
  std::cout << std::setfill('#') << std::setw(60) << '#' << std::endl;
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout << "Const Iterator" <<std::endl;
  std::cout <<  std::setfill('#') << std::setw(60) << '#' << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  slip::Vector<T> const Mc(M);
  
  std::for_each(Mc.begin(),Mc.end(),disp<T>);
  std::cout<<std::endl;
  std::cout<<std::endl;

 // Global iterators
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "global iterators" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  std::copy(Mc.begin(),Mc.end(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rbegin(),Mc.rend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  // range :
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "range iterators" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.begin(r),Mc.end(r),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rbegin(r),Mc.rend(r),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "range iterator + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.begin(rd),Mc.end(rd),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rbegin(rd),Mc.rend(rd),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //box1 iterators : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "box1 iterators" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.begin(b1),Mc.end(b1),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rbegin(b1),Mc.rend(b1),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //box2 iterators : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "box2 iterators" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.begin(b2),Mc.end(b2),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rbegin(b2),Mc.rend(b2),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;


 return 0;
}

