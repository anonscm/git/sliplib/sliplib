#include "Vector.hpp"
#include "Range.hpp"
#include "Box1d.hpp"
#include "Point1d.hpp"
#include "arithmetic_op.hpp"

#include <iostream>
#include <fstream>
#include <complex>
#include <algorithm>
#include <numeric>
#include <vector>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

int main()
{

  //----------------------------------
  //  Constructors
  //----------------------------------
  slip::Vector<double> V;
  std::cout<<"name = "<<V.name()<<" size = "<<V.size()<<" max_size = "<<V.max_size()<<" empty = "<<V.empty()<<std::endl; 
  std::cout<<V<<std::endl;
 

  slip::Vector<double> V2(6);
  std::cout<<"size = "<<V2.size()<<" max_size = "<<V2.max_size()<<" empty = "<<V2.empty()<<std::endl;
  std::cout<<V2<<std::endl;

   slip::Vector<double> V6(6,2.2);
  std::cout<<"size = "<<V6.size()<<" max_size = "<<V6.max_size()<<" empty = "<<V6.empty()<<std::endl;
  std::cout<<V6<<std::endl;
  
  double d[] = {1.2,3.4,5.5,6.6,7.7,8.8};
  slip::Vector<double> V4(6,d);
  std::cout<<"size = "<<V4.size()<<" max_size = "<<V4.max_size()<<" empty = "<<V4.empty()<<std::endl;
  std::cout<<V4<<std::endl;


  std::vector<int> VV(6);
  for(std::size_t i = 0; i < 6; ++i)
    VV[i] = i;
  slip::Array<int> V8(6,VV.begin(),VV.end());
  std::cout<<"empty = "<<V8.empty()<<" size = "<<V8.size()<<" max_size = "<<V8.max_size()<<std::endl;
  std::cout<<V8<<std::endl;

  slip::Vector<double> V5 = V4;
 std::cout<<"size = "<<V5.size()<<" max_size = "<<V5.max_size()<<" empty = "<<V5.empty()<<std::endl;
  std::cout<<V5<<std::endl;
  
  std::complex<double> c(2.3,2.3);
  std::cout<<c<<std::endl;
  slip::Vector<std::complex<double> > VC(3,c);
  
  std::cout<<VC<<std::endl;
  

  std::cout<<VC[1+1]<<std::endl;
  //----------------------------------
   
  //------------------------------
  //resize
  //------------------------------
  V.resize(10,5.2);
  std::cout<<V<<std::endl;
  
  //-----------------------------

  //----------------------------------
  //  Affectations
  //----------------------------------
  V = V2;
  std::cout<<"size = "<<V.size()<<" max_size = "<<V.max_size()<<" empty = "<<V.empty()<<std::endl; 
  std::cout<<V<<std::endl;
  std::cout<<"size = "<<V2.size()<<" max_size = "<<V2.max_size()<<" empty = "<<V2.empty()<<std::endl;
  std::cout<<V2<<std::endl;
  slip::Vector<float> V3(5,4.4);
  V3 = V3;
  std::cout<<"size = "<<V3.size()<<" max_size = "<<V3.max_size()<<" empty = "<<V3.empty()<<std::endl; 
  std::cout<<V3<<std::endl;  
  slip::Vector<float> MM(6);
  MM = 5.5;
  std::cout<<MM<<std::endl;
  
  slip::Vector<float> MM2(6);
  MM2 = 2.2;
  std::cout<<MM2<<std::endl;
  //----------------------------------
 
  //------------------------------
  //Element access operators
  //------------------------------
  V2(2) = 2.1;
  V2[3] = 1.2;

  std::cout<<V2<<std::endl;
  
  for(size_t i = 0; i < V2.size(); ++i)
    {
      std::cout<<V2[i]<<" ";
    }
  for(size_t i = 0; i < V2.size(); ++i)
    {
      std::cout<<V2(i)<<" ";
    }

  slip::Vector<double> Arange(10);
  double val = 0.0;
  for(size_t i = 0; i < Arange.size(); ++i)
    {
      Arange(slip::Point1d<std::size_t>(i)) = val;
      val = val + 1.0;
    }
  for(size_t i = 0; i < Arange.size(); ++i)
    {
      std::cout<<Arange(slip::Point1d<std::size_t>(i))<<" ";
    }
  std::cout<<std::endl;
  
  slip::Range<int> range_c(0,Arange.size()-1,2);
  slip::Vector<double> Aext = Arange(range_c);
  std::cout<<Aext<<std::endl;

  //----------------------------------

  
  
  //------------------------------
  //swap
  //------------------------------
  slip::Vector<double> V7(6,4.4);
  
   std::cout<<V7<<std::endl;
   std::cout<<V5<<std::endl;
   
   V7.swap(V5);
   std::cout<<V7<<std::endl;
   std::cout<<V5<<std::endl;
  //------------------------------
  
  //----------------------------
  //iterators
  //----------------------------
  std::copy(V4.begin(),V4.end(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(V4.rbegin(),V4.rend(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;

  std::transform(V4.rbegin(),V4.rend(),V4.begin(),V5.begin(),std::plus<double>());
  
  std::cout<<V5<<std::endl;
  
  std::cout<<Arange<<std::endl;
  slip::Range<int> range(1,5,2);
  std::copy(Arange.begin(range),Arange.end(range),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(Arange.rbegin(range),Arange.rend(range),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;

   slip::Range<int> range2(0,9,3);
   std::copy(Arange.begin(range2),Arange.end(range2),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(Arange.rbegin(range2),Arange.rend(range2),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   slip::Box1d<int> box(2,6);
   std::copy(Arange.begin(box),Arange.end(box),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(Arange.rbegin(box),Arange.rend(box),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;

   slip::Box1d<int> box2d(0,9);
   std::copy(Arange.begin(box2d),Arange.end(box2d),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(Arange.rbegin(box2d),Arange.rend(box2d),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
  //----------------------------   
 
   //------------------------------
   //Comparison operators
   //------------------------------
   std::cout<<" V2 == V7 "<<(V2 == V7)<<std::endl;
   std::cout<<" V2 != V7 "<<(V2 != V7)<<std::endl;
   std::cout<<" V2 > V7  "<<(V2 > V7)<<std::endl;
   std::cout<<" V2 < V7  "<<(V2 < V7)<<std::endl;
   std::cout<<" V2 >= V7 "<<(V2 >= V7)<<std::endl;
   std::cout<<" V2 <= V7 "<<(V2 <= V7)<<std::endl;
   //------------------------------
 
  
  //------------------------------
  //fill methods
  //------------------------------
  V3.fill(4.0);
  std::cout<<V3<<std::endl;
  float d2[] = {1.2,3.4,5.5,6.6,7.7,8.8,9.9};
  V3.fill(d2);
  std::cout<<V3<<std::endl;

  std::vector<int> Vec(7);
  for(std::size_t i = 0; i < 7; ++i)
    Vec[i] = i;
  V3.fill(Vec.begin(),Vec.begin()+5);
  std::cout<<V3<<std::endl;
  //------------------------------


 
  

  
  
 
  //------------------------------
  //iterators
  //-----------------------------
  //linear print 
  std::copy(V7.begin(),V7.end(),std::ostream_iterator<double>(std::cout,"\n"));
  //reverse linear print
  std::copy(V7.rbegin(),V7.rend(),std::ostream_iterator<double>(std::cout,"\n")); 


   std::generate(V.begin(),V.end(),std::rand);
   std::cout<<V<<std::endl;

   //------------------------------

  
   

   //------------------------------
   //Artithmetic an mathematical operators
   //------------------------------
    MM += MM2;
    std::cout<<MM<<std::endl;
    
    MM -= MM2;
    std::cout<<MM<<std::endl;

    MM *= MM2;
    std::cout<<MM<<std::endl;

    MM /= MM2;
    std::cout<<MM<<std::endl;

    slip::Vector<float> MM3(6);
    MM3 = MM + MM2;
    std::cout<<MM<<"+"<<MM2<<" = "<<MM3<<std::endl;

    MM3 = MM - MM2;
    std::cout<<MM<<"-"<<MM2<<" = "<<MM3<<std::endl;

    MM3 = MM * MM2;
    std::cout<<MM<<"*"<<MM2<<" = "<<MM3<<std::endl;

    MM3 = MM / MM2;
    std::cout<<MM<<"/"<<MM2<<" = "<<MM3<<std::endl;

    std::cout<<"- MM3 = "<<-MM3<<std::endl;


    std::cout<<"MM2 = "<<std::endl;
    std::cout<<MM2<<std::endl;
    MM2+=1.1;
    std::cout<<MM2<<std::endl;
    MM2-=1.1;
    std::cout<<MM2<<std::endl;
    MM2*=1.1;
    std::cout<<MM2<<std::endl;
    MM2/=1.1;
    std::cout<<MM2<<std::endl;
     
    std::cout<<V4.min()<<std::endl;
    std::cout<<V4.max()<<std::endl;
    std::cout<<V4.sum()<<std::endl;
    
    std::cout<<min(V4)<<std::endl;
    std::cout<<max(V4)<<std::endl;

    std::cout<<V4.Euclidean_norm()<<std::endl;
    std::cout<<V4.L2_norm()<<std::endl;
    std::cout<<V4.L1_norm()<<std::endl;
    std::cout<<V4.L22_norm()<<std::endl;
    std::cout<<V4.infinite_norm()<<std::endl;
    
    
    slip::Vector<double> MM5(6);
    MM5 = V4 + V4;
    std::cout<<MM5<<std::endl;
    std::cout<<MM5+2.0<<std::endl;
    std::cout<<(2.0+MM5)<<std::endl;
    
    std::cout<<MM5<<std::endl;
    std::cout<<MM5-2.0<<std::endl;
    std::cout<<(2.0-MM5)<<std::endl;
     
    std::cout<<MM5<<std::endl;
    std::cout<<MM5*2.0<<std::endl;
    std::cout<<(2.0*MM5)<<std::endl;
    
    std::cout<<MM5<<std::endl;
    std::cout<<MM5/2.0<<std::endl;
    //std::cout<<(2.0/MM5)<<std::endl;
    


    std::cout<<abs(-V4)<<std::endl;
    std::cout<<sqrt(V4)<<std::endl;
    std::cout<<cos(V4)<<std::endl;
    std::cout<<acos(V4)<<std::endl;
    std::cout<<sin(V4)<<std::endl;
    std::cout<<asin(V4)<<std::endl;
    std::cout<<tan(V4)<<std::endl;
    std::cout<<atan(V4)<<std::endl;
    std::cout<<exp(V4)<<std::endl;
    std::cout<<log(V4)<<std::endl;
    std::cout<<cosh(V4)<<std::endl;
    std::cout<<sinh(V4)<<std::endl;
    std::cout<<tanh(V4)<<std::endl;
    std::cout<<log10(V4)<<std::endl;
    
    V4.apply(std::sqrt);
    std::cout<<V4<<std::endl;
    //----------------------------------
    // complex test
    //---------------------------------
    slip::Vector<std::complex<float> > MMc(6);
    MMc = std::complex<float>(5.5f,4.4f);
    std::cout<<MMc<<std::endl;
  
    slip::Vector<std::complex<float> > MM2c(6);
    MM2c = std::complex<float>(2.2f,3.3f);
    std::cout<<MM2c<<std::endl;
    

    //------------------------------
   //Artithmetic an mathematical operators
   //------------------------------
    MMc += MM2c;
    std::cout<<MMc<<std::endl;
    
    MMc -= MM2c;
    std::cout<<MMc<<std::endl;

    MMc *= MM2c;
    std::cout<<MMc<<std::endl;

    MMc /= MM2c;
    std::cout<<MMc<<std::endl;

    slip::Vector<std::complex<float> > MM3c(6);
    MM3c = MMc + MM2c;
    std::cout<<MMc<<"+"<<MM2c<<" = "<<MM3c<<std::endl;

    MM3c = MMc - MM2c;
    std::cout<<MMc<<"-"<<MM2c<<" = "<<MM3c<<std::endl;

    MM3c = MMc * MM2c;
    std::cout<<MMc<<"*"<<MM2c<<" = "<<MM3c<<std::endl;

    MM3c = MMc / MM2c;
    std::cout<<MMc<<"/"<<MM2c<<" = "<<MM3c<<std::endl;

    std::cout<<"- MM3c = "<<-MM3c<<std::endl;


    std::cout<<"MM2c = "<<std::endl;
    std::cout<<MM2c<<std::endl;
    MM2c+=std::complex<float>(1.1f,2.2f);
    std::cout<<MM2c<<std::endl;
    MM2c-=std::complex<float>(1.1f,2.2f);
    std::cout<<MM2c<<std::endl;
    MM2c*=std::complex<float>(1.1f,2.2f);
    std::cout<<MM2c<<std::endl;
    MM2c/=std::complex<float>(1.1f,2.2f);
    std::cout<<MM2c<<std::endl;

    slip::Vector<std::complex<float> > V4c(5);
    slip::iota(V4c.begin(),V4c.end(),
	       std::complex<float>(1.0f,1.0f),
	       std::complex<float>(1.0f,2.0f));
     
    std::cout<<V4c.min()<<std::endl;
    std::cout<<V4c.max()<<std::endl;
    std::cout<<V4c.sum()<<std::endl;
    
    std::cout<<min(V4c)<<std::endl;
    std::cout<<max(V4c)<<std::endl;

     std::cout<<V4c.Euclidean_norm()<<std::endl;
     std::cout<<V4c.L2_norm()<<std::endl;
     std::cout<<V4c.L1_norm()<<std::endl;
     std::cout<<V4c.L22_norm()<<std::endl;
     std::cout<<V4c.infinite_norm()<<std::endl;
    
    
    slip::Vector<std::complex<float> > MM5c(6);
    MM5c = V4c + V4c;
    std::cout<<MM5c<<std::endl;
    std::cout<<MM5c+std::complex<float>(2.0f,1.0f)<<std::endl;
    std::cout<<(std::complex<float>(2.0f,1.0f)+MM5c)<<std::endl;
    
    std::cout<<MM5c<<std::endl;
    std::cout<<MM5c-std::complex<float>(2.0f,1.0f)<<std::endl;
    std::cout<<(std::complex<float>(2.0f,1.0f)-MM5c)<<std::endl;
     
    std::cout<<MM5c<<std::endl;
    std::cout<<MM5c*std::complex<float>(2.0f,1.0f)<<std::endl;
    std::cout<<(std::complex<float>(2.0f,1.0f)*MM5c)<<std::endl;
    
    std::cout<<MM5c<<std::endl;
    std::cout<<MM5c/std::complex<float>(2.0f,1.0f)<<std::endl;
    //std::cout<<(std::complex<float>(2.0f,1.0f)/MM5c)<<std::endl;
    


    std::cout<<abs(-V4c)<<std::endl;
    std::cout<<sqrt(V4c)<<std::endl;
    std::cout<<cos(V4c)<<std::endl;
    //std::cout<<acos(V4c)<<std::endl;
    std::cout<<sin(V4c)<<std::endl;
    //std::cout<<asin(V4c)<<std::endl;
    std::cout<<tan(V4c)<<std::endl;
    //std::cout<<atan(V4c)<<std::endl;
    std::cout<<exp(V4c)<<std::endl;
    std::cout<<log(V4c)<<std::endl;
    std::cout<<cosh(V4c)<<std::endl;
    std::cout<<sinh(V4c)<<std::endl;
    //std::cout<<tanh(V4c)<<std::endl;
    std::cout<<log10(V4c)<<std::endl;
    
    V4c.apply(std::sqrt);
    std::cout<<V4c<<std::endl;

    // create and open a character archive for output
    std::ofstream ofs("vector.txt");
    slip::Vector<double> p(10);
    slip::iota(p.begin(),p.end(),1.0);
    std::cout<<"p = "<<p<<std::endl;
        
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<p;
    }//to close archive
    std::ifstream ifs("vector2d.txt");
    slip::Vector<double> new_p;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_p;
    }//to close archive
    std::cout<<"new_p\n"<<new_p<<std::endl;

    std::ofstream ofsb("vector2d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<p;
    }//to close archive

    std::ifstream ifsb("vector2d.bin");
    slip::Vector<double> new_pb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_pb;
    }//to close archive
    std::cout<<"new_pb\n"<<new_pb<<std::endl;
    
     return 0;
}
