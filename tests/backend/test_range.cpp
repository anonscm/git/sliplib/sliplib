#include<iostream>
#include<iterator>
#include <fstream>
#include "Range.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

int main()
{

 slip::Range<int> range;
 std::cout<<range<<std::endl;
 std::cout<<"name  = "<<range.name()<<std::endl;
 std::cout<<"start = "<<range.start()<<" stop = "<<range.stop()<<" stride = "<<range.stride()<<" iterations = "<<range.iterations()<<std::endl;

 slip::Range<std::size_t> range2;
 std::cout<<range2<<std::endl;
 std::cout<<"start = "<<range2.start()<<" stop = "<<range2.stop()<<" stride = "<<range2.stride()<<" iterations = "<<range2.iterations()<<std::endl;

 slip::Range<int> range3(1,6,2);
 std::cout<<range3<<std::endl;
 std::cout<<"start = "<<range3.start()<<" stop = "<<range3.stop()<<" stride = "<<range3.stride()<<" iterations = "<<range3.iterations()<<std::endl;

 slip::Range<std::size_t> range4(1,6,2);
 std::cout<<range4<<std::endl;
 std::cout<<"start = "<<range4.start()<<" stop = "<<range4.stop()<<" stride = "<<range4.stride()<<" iterations = "<<range4.iterations()<<std::endl;

 slip::Range<int> range5(1,8);
 std::cout<<range5<<std::endl;
 std::cout<<"start = "<<range5.start()<<" stop = "<<range5.stop()<<" stride = "<<range5.stride()<<" iterations = "<<range5.iterations()<<std::endl;
 
 std::cout<<(range == range5)<<std::endl;
 std::cout<<(range != range5)<<std::endl;
 
 slip::Range<int> range6 = range5;
 std::cout<<range6<<std::endl;
 std::cout<<"start = "<<range6.start()<<" stop = "<<range6.stop()<<" stride = "<<range6.stride()<<" iterations = "<<range6.iterations()<<std::endl;
 std::cout<<range6.is_valid()<<std::endl;
 
 slip::Range<int> range7(8,1,1);
 std::cout<<range7<<std::endl;
 std::cout<<"start = "<<range7.start()<<" stop = "<<range7.stop()<<" stride = "<<range7.stride()<<" iterations = "<<range7.iterations()<<std::endl;
 std::cout<<range7.is_valid()<<std::endl;

  slip::Range<int> range8(8,1,-1);
 std::cout<<range8<<std::endl;
 std::cout<<"start = "<<range8.start()<<" stop = "<<range8.stop()<<" stride = "<<range8.stride()<<" iterations = "<<range8.iterations()<<std::endl;
 std::cout<<range8.is_valid()<<std::endl;

 //
 //serialization
 //
 // create and open a character archive for output
    std::ofstream ofs("ranges.txt");
    slip::Range<int> ranges(1,10,2);
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<ranges;
    }//to close archive
    std::ifstream ifs("ranges.txt");
    slip::Range<int> new_ranges;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_ranges;
    }//to close archive
    std::cout<<"new_ranges\n"<<new_ranges<<std::endl;

    std::ofstream ofsb("ranges.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<ranges;
    }//to close archive

    std::ifstream ifsb("ranges.bin");
    slip::Range<int> new_rangesb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_rangesb;
    }//to close archive
    std::cout<<"new_rangesb\n"<<new_rangesb<<std::endl;
 return 0;
}
