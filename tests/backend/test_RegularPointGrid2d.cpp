#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

#include "RegularPointGrid2d.hpp"
#include "QuadTreeBox.hpp"
#include "Point2d.hpp"
#include "Matrix.hpp"
#include "compare.hpp"
#include "FeaturePoint.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

template <typename T>
class MyFeatures;

template <typename T>
std::ostream& operator<<(std::ostream & out, 
			 const MyFeatures<T>& F);
template <typename T>
bool operator==(const MyFeatures<T>& x, 
		const MyFeatures<T>& y);
template <typename T>
bool operator!=(const MyFeatures<T>& x, 
		const MyFeatures<T>& y);

template <typename T>
class MyFeatures
{
  public:
  typedef MyFeatures self;

  friend std::ostream& operator<< <>(std::ostream & out, 
				  const self& F);
  friend bool operator== <>(const self& x, 
			    const self& y);
  
  friend bool operator!= <>(const self& x, 
			    const self& y);
  std::string descriptor() const
  {
    return "radius";
  }


  MyFeatures():
    radius_(T(1))
  {}

  MyFeatures(const T& radius):
    radius_(radius)
  {}

  void set_radius(const T& radius)
  {
    this->radius_ = radius;
  }

  const T& get_radius() const
  {
    return this->radius_;
  }
  
private:
  T radius_;
};

template <typename T>
 inline
 std::ostream& operator<<(std::ostream & out, 
			  const MyFeatures<T>& F)
{
  out<<F.radius_;
  return out;
}

 template <typename T>
 inline
bool operator==(const MyFeatures<T>& x, 
		const MyFeatures<T>& y)
{
  return x.radius_ == y.radius_;
}

template <typename T>
inline
bool operator!=(const MyFeatures<T>& x, 
		const MyFeatures<T>& y)
{
  return !(x == y);
}

int main()
{
  
  typedef double T;
  typedef slip::Point2d<T> Point;
  //typedef T Feature;
  typedef typename slip::EmptyFeature Feature;
  typedef slip::FeaturePoint <Point,Feature> FPoint;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Default constructor " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::RegularPointGrid2d<FPoint> rpg;
  std::cout<<"rpg = \n"<<rpg<<std::endl;

 
 
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Constructor with points list" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  slip::Point2d<T> min_p(T(-2.00),T(-1.00));
  slip::Point2d<T> max_p(T(3.0),T(2.5));
  FPoint p1(min_p,slip::EmptyFeature());
  FPoint p2(max_p,slip::EmptyFeature());
  FPoint p3(slip::Point2d<T>(T(0.0),T(0.0)),slip::EmptyFeature());
  FPoint p4(slip::Point2d<T>(T(-0.7),T(0.5)),slip::EmptyFeature());
  FPoint p5(slip::Point2d<T>(T(-0.8),T(0.6)),slip::EmptyFeature());
  FPoint p6(slip::Point2d<T>(T(1.0),T(2.0)),slip::EmptyFeature());
  FPoint p7(slip::Point2d<T>(T(-0.1),T(2.5)),slip::EmptyFeature());
  FPoint p8(slip::Point2d<T>(T(-2.0),T(2.5)),slip::EmptyFeature());
  FPoint p9(slip::Point2d<T>(T(3.0),T(-1.0)),slip::EmptyFeature());


  std::vector<FPoint> plist;
  plist.push_back(p1);
  plist.push_back(p2);
  plist.push_back(p3);
  plist.push_back(p4);
  plist.push_back(p5);
  plist.push_back(p6);
  plist.push_back(p7);
  plist.push_back(p8);
  plist.push_back(p9);
  
  slip::RegularPointGrid2d<FPoint> rpg2(plist.begin(),plist.end(),
  					std::size_t(1));
  std::cout<<"rpg2 = \n"<<rpg2<<std::endl;
  for(auto it_rpg2 = rpg2.begin();it_rpg2 != rpg2.end(); ++it_rpg2)
    {
      std::cout<<*it_rpg2<<std::endl;
    }
  rpg2.write_svg("rpg2.svg");
  rpg2.write_vtk("rpg2.vtk");
  slip::RegularPointGrid2d<FPoint> rpg3(plist.begin(),plist.end(),
  					std::size_t(1),
  					static_cast<T>(0.1));
  
  std::cout<<"rpg3 = \n"<<rpg3<<std::endl;
  for(auto it_rpg3 = rpg3.begin();it_rpg3 != rpg3.end(); ++it_rpg3)
    {
      std::cout<<*it_rpg3<<std::endl;
    }
  rpg3.write_svg("rpg3.svg");
  rpg3.write_vtk("rpg3.vtk");
  //rpg3.test_compute_indices(T(0.0),T(0.0));
  //rpg3.test_compute_indices(rpg3.dx()/T(2.0),rpg3.dy()/T(2.0));
  //rpg3.test_compute_indices(rpg3.dx(),rpg3.dy());
  
 

  
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Copy constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  slip::RegularPointGrid2d<FPoint> rpg_copy(rpg3);
  std::cout<<"rpg_copy = "<<rpg_copy<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "operator=" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::RegularPointGrid2d<FPoint> rpg_equal;
  rpg_equal = rpg3;
  std::cout<<"rpg_equal = "<<rpg_equal<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "operator= self assignment" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  rpg_equal = rpg_equal;

  std::cout<<"rpg_equal = "<<rpg_equal<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "iterators " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"points in rpg3"<<std::endl;
  slip::RegularPointGrid2d<FPoint>::const_iterator it_rpg3_b = rpg3.cbegin();
  slip::RegularPointGrid2d<FPoint>::const_iterator it_rpg3_e = rpg3.cend();
  for(;it_rpg3_b != it_rpg3_e; ++it_rpg3_b)
    {
      std::cout<<*it_rpg3_b<<std::endl;
    }
   

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Constructor with random points list" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::QuadTreeBox<slip::Point2d<T> > range_rnd(-50.0,-60.0,50.0,60.0);
  // slip::RegularPointGrid2d<FPoint> rpg4(50,
  // 					range_rnd,
  // 					std::size_t(1),
  // 					static_cast<T>(0.1));
  slip::RegularPointGrid2d<FPoint> rpg4(1000,
  					range_rnd,
  					std::size_t(5),
  					static_cast<T>(0.1));
  std::cout<<"points in rpg4"<<std::endl;
  for(auto it_rpg4 = rpg4.begin(); it_rpg4 != rpg4.end(); ++it_rpg4)
    {
      std::cout<<*it_rpg4<<std::endl;
    }
  std::cout<<"rpg4 = \n"<<rpg4<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "write_svg " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  rpg4.write_svg("rpg4.svg");
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "write_vtk " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  rpg4.write_vtk("rpg4.vtk");


    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "load factor " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"rpg4.load_factor() = "<<rpg4.load_factor()<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Random selection " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::RegularPointGrid2d<FPoint>::iterator it_rs1 = rpg4.random_selection();
  std::cout<<"*it_rs1 = "<<*it_rs1<<std::endl;

  

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Points in range " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::QuadTreeBox<slip::Point2d<T> > range(-2.0,0.0,0.0,2.5);
   std::size_t query_points_nb = 0;
  std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range =
    rpg3.points_in_range(range,query_points_nb);
  std::cout<<query_points_nb<<" points in range "<<range<<std::endl;
  for(;it_range.first != it_range.second; ++it_range.first)
    {
      std::cout<<*it_range.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_range(range) = "<<rpg3.count_in_range(range)<<std::endl;

  std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_full =
    rpg3.points_in_range(rpg3.box(),query_points_nb);
  std::cout<<query_points_nb<<" points in range "<<rpg3.box()<<std::endl;
  for(;it_range_full.first != it_range_full.second; ++it_range_full.first)
    {
      std::cout<<*it_range_full.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_range(rpg3.box()) = "<<rpg3.count_in_range(rpg3.box())<<std::endl;
  
   slip::QuadTreeBox<slip::Point2d<T> > range_partial1(0.0,0.0,4.0,5.0);
   std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_partial1 =
    rpg3.points_in_range(range_partial1,query_points_nb);
   std::cout<<query_points_nb<<" points in range "<<range_partial1<<std::endl;
   for(;it_range_partial1.first != it_range_partial1.second; ++it_range_partial1.first)
    {
      std::cout<<*it_range_partial1.first<<std::endl;
    }
   std::cout<<" rpg3.count_in_range(range_partial1) = "<<rpg3.count_in_range(range_partial1)<<std::endl;

  
   slip::QuadTreeBox<slip::Point2d<T> > range_partial2(-3.0,-4.0,0.0,0.0);
   std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_partial2 =
    rpg3.points_in_range(range_partial2,query_points_nb);
   std::cout<<query_points_nb<<" points in range "<<range_partial2<<std::endl;
   for(;it_range_partial2.first != it_range_partial2.second; ++it_range_partial2.first)
    {
      std::cout<<*it_range_partial2.first<<std::endl;
    }
   std::cout<<" rpg3.count_in_range(range_partial2) = "<<rpg3.count_in_range(range_partial2)<<std::endl;
 
   slip::QuadTreeBox<slip::Point2d<T> > range_partial3(-3.0,0.0,-0.5,4.0);
   std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_partial3 =
    rpg3.points_in_range(range_partial3,query_points_nb);
   std::cout<<query_points_nb<<" points in range "<<range_partial3<<std::endl;
   for(;it_range_partial3.first != it_range_partial3.second; ++it_range_partial3.first)
    {
      std::cout<<*it_range_partial3.first<<std::endl;
    }
    std::cout<<" rpg3.count_in_range(range_partial3) = "<<rpg3.count_in_range(range_partial3)<<std::endl;
    
   slip::QuadTreeBox<slip::Point2d<T> > range_partial4(0.0,-3.0,4.0,1.0);
   std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_partial4 =
    rpg3.points_in_range(range_partial4,query_points_nb);
   std::cout<<query_points_nb<<" points in range "<<range_partial4<<std::endl;
   for(;it_range_partial4.first != it_range_partial4.second; ++it_range_partial4.first)
    {
      std::cout<<*it_range_partial4.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_range(range_partial4) = "<<rpg3.count_in_range(range_partial4)<<std::endl;
  
 slip::QuadTreeBox<slip::Point2d<T> > range_empty(4.0,3.0,8.0,10.0);
   std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_empty =
    rpg3.points_in_range(range_empty,query_points_nb);
   std::cout<<query_points_nb<<" points in range "<<range_empty<<std::endl;
   for(;it_range_empty.first != it_range_empty.second; ++it_range_empty.first)
    {
      std::cout<<*it_range_empty.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_range(range_empty) = "<<rpg3.count_in_range(range_empty)<<std::endl;
 
  slip::QuadTreeBox<slip::Point2d<T> > range_around(-3.0,-3.0,4.0,4.0);
   std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_around =
    rpg3.points_in_range(range_around,query_points_nb);
   std::cout<<query_points_nb<<" points in range "<<range_around<<std::endl;
   for(;it_range_around.first != it_range_around.second; ++it_range_around.first)
     {
       std::cout<<*it_range_around.first<<std::endl;
     }
   std::cout<<" rpg3.count_in_range(range_around) = "<<rpg3.count_in_range(range_around)<<std::endl;
  
   slip::QuadTreeBox<slip::Point2d<T> > range_inside(-1.0,-1.0,1.0,1.0);
   std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_inside =
    rpg3.points_in_range(range_inside,query_points_nb);
   std::cout<<query_points_nb<<" points in range "<<range_inside<<std::endl;
   for(;it_range_inside.first != it_range_inside.second; ++it_range_inside.first)
    {
      std::cout<<*it_range_inside.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_range(range_inside) = "<<rpg3.count_in_range(range_inside)<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Points in circle " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Point2d<T> circle_center(rpg3.box().middle());
  T circle_radius = T(5.0);
  std::size_t query_points_number = 0;
  std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_circle5 =
    rpg3.points_in_circle(circle_center,circle_radius,query_points_number);
  std::cout<<query_points_number<<" points in circle of center "<<circle_center<<" and radius "<<circle_radius<<std::endl;
  for(;it_range_circle5.first != it_range_circle5.second; ++it_range_circle5.first)
    {
      std::cout<<*it_range_circle5.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_circle = "<<rpg3.count_in_circle(circle_center,circle_radius)<<std::endl;
 
  circle_radius = T(4.0);
  query_points_number = 0;
  std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_circle4 =
    rpg3.points_in_circle(circle_center,circle_radius,query_points_number);
  std::cout<<query_points_number<<" points in circle of center "<<circle_center<<" and radius "<<circle_radius<<std::endl;
  for(;it_range_circle4.first != it_range_circle4.second; ++it_range_circle4.first)
    {
      std::cout<<*it_range_circle4.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_circle = "<<rpg3.count_in_circle(circle_center,circle_radius)<<std::endl;
 
  circle_radius = T(2.0);
 query_points_number = 0;
  std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_circle2 =
    rpg3.points_in_circle(circle_center,circle_radius,query_points_number);
  std::cout<<query_points_number<<" points in circle of center "<<circle_center<<" and radius "<<circle_radius<<std::endl;
  for(;it_range_circle2.first != it_range_circle2.second; ++it_range_circle2.first)
    {
      std::cout<<*it_range_circle2.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_circle = "<<rpg3.count_in_circle(circle_center,circle_radius)<<std::endl;
  
  circle_radius = T(0.5);
  query_points_number = 0;
  std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_circle05 =
    rpg3.points_in_circle(circle_center,circle_radius,query_points_number);
  std::cout<<query_points_number<<" points in circle of center "<<circle_center<<" and radius "<<circle_radius<<std::endl;
  for(;it_range_circle05.first != it_range_circle05.second; ++it_range_circle05.first)
    {
      std::cout<<*it_range_circle05.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_circle = "<<rpg3.count_in_circle(circle_center,circle_radius)<<std::endl;

  slip::Point2d<T> circle_center_outside(T(8.0),T(10.0));
  circle_radius = T(2.0);
 query_points_number = 0;
  std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_circle_outside =
    rpg3.points_in_circle(circle_center_outside,circle_radius,query_points_number);
  std::cout<<query_points_number<<" points in circle of center "<<circle_center_outside<<" and radius "<<circle_radius<<std::endl;
  for(;it_range_circle_outside.first != it_range_circle_outside.second; ++it_range_circle_outside.first)
    {
      std::cout<<*it_range_circle_outside.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_circle = "<<rpg3.count_in_circle(circle_center_outside,circle_radius)<<std::endl;

  circle_radius = T(3.0);
  slip::Point2d<T> circle_center_partial1(rpg3.box().maximal());
  query_points_number = 0;
  std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_circle_partial1 =
    rpg3.points_in_circle(circle_center_partial1,circle_radius,query_points_number);
  std::cout<<query_points_number<<" points in circle of center "<<circle_center_partial1<<" and radius "<<circle_radius<<std::endl;
  for(;it_range_circle_partial1.first != it_range_circle_partial1.second; ++it_range_circle_partial1.first)
    {
      std::cout<<*it_range_circle_partial1.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_circle = "<<rpg3.count_in_circle(circle_center_partial1,circle_radius)<<std::endl;
 
   slip::Point2d<T> circle_center_partial2(rpg3.box().minimal());
  query_points_number = 0;
  std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_circle_partial2 =
    rpg3.points_in_circle(circle_center_partial2,circle_radius,query_points_number);
  std::cout<<query_points_number<<" points in circle of center "<<circle_center_partial2<<" and radius "<<circle_radius<<std::endl;
  for(;it_range_circle_partial2.first != it_range_circle_partial2.second; ++it_range_circle_partial2.first)
    {
      std::cout<<*it_range_circle_partial2.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_circle = "<<rpg3.count_in_circle(circle_center_partial2,circle_radius)<<std::endl;
  
  slip::Point2d<T> circle_center_partial3(rpg3.box().upper_left());
  query_points_number = 0;
  std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_circle_partial3 =
    rpg3.points_in_circle(circle_center_partial3,circle_radius,query_points_number);
  std::cout<<query_points_number<<" points in circle of center "<<circle_center_partial3<<" and radius "<<circle_radius<<std::endl;
  for(;it_range_circle_partial3.first != it_range_circle_partial3.second; ++it_range_circle_partial3.first)
    {
      std::cout<<*it_range_circle_partial3.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_circle = "<<rpg3.count_in_circle(circle_center_partial3,circle_radius)<<std::endl;
 
  slip::Point2d<T> circle_center_partial4(rpg3.box().bottom_right());
  query_points_number = 0;
  std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_circle_partial4 =
    rpg3.points_in_circle(circle_center_partial4,circle_radius,query_points_number);
  std::cout<<query_points_number<<" points in circle of center "<<circle_center_partial4<<" and radius "<<circle_radius<<std::endl;
  for(;it_range_circle_partial4.first != it_range_circle_partial4.second; ++it_range_circle_partial4.first)
    {
      std::cout<<*it_range_circle_partial4.first<<std::endl;
    }
  std::cout<<" rpg3.count_in_circle = "<<rpg3.count_in_circle(circle_center_partial4,circle_radius)<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "find " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::RegularPointGrid2d<FPoint>::iterator it_find_rpg3 = rpg3.find(p4);
  if(it_find_rpg3 != rpg3.end())
    {
      std::cout<<p4.get_point()<<" is in the rpg3 feature point list"<<std::endl;
    }
  else
    {
      std::cout<<p4.get_point()<<" is not in the rpg3 feature point list"<<std::endl;
    }

  FPoint pfind(slip::Point2d<T>(T(0.0),T(1.0)),slip::EmptyFeature());
  slip::RegularPointGrid2d<FPoint>::iterator it2_find_rpg3 = rpg3.find(pfind);
  if(it2_find_rpg3 != rpg3.end())
    {
      std::cout<<pfind.get_point()<<" is in the rpg3 feature point list"<<std::endl;
    }
  else
    {
      std::cout<<pfind.get_point()<<" is not in the rpg3 feature point list"<<std::endl;
    }

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "erase " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  slip::RegularPointGrid2d<FPoint>::iterator it_find_rpg33 = rpg3.erase(rpg3.find(p4));
  std::cout << "erase " <<p4<<std::endl;
  std::cout<<"rpg3 = \n"<<rpg3<<std::endl;
  std::cout<<"& points in rpg3"<<std::endl;
  slip::RegularPointGrid2d<FPoint>::iterator it_rpg3_b1 = rpg3.begin();
  slip::RegularPointGrid2d<FPoint>::iterator it_rpg3_e1 = rpg3.end();
  for(;it_rpg3_b1 != it_rpg3_e1; ++it_rpg3_b1)
    {
      std::cout<<*it_rpg3_b1<<"\t"<<&(*it_rpg3_b1)<<std::endl;
    }
  rpg3.write_svg("rpg3_2.svg");

 

 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "erase a random selected point " << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 slip::RegularPointGrid2d<FPoint>::iterator it_rs_rpg3= rpg3.random_selection();
 std::cout<<"erase *it_rs_rpg3 = "<<*it_rs_rpg3<<std::endl;
 rpg3.erase(it_rs_rpg3);
 for(auto it_rpg3 = rpg3.begin(); it_rpg3 != rpg3.end(); ++it_rpg3)
   {
     std::cout<<*it_rpg3<<"\t"<<&(*it_rpg3)<<std::endl;
   }
 
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "erase a range " << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 slip::QuadTreeBox<slip::Point2d<T> > range_rpg4(0.0,0.0,rpg4.box().xmax(),rpg4.box().ymax());
 std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
	   slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_rpg4 =
   rpg4.points_in_range(range_rpg4,query_points_nb);
 slip::RegularPointGrid2d<FPoint>::const_iterator it_range_rpg4b = it_range_rpg4.first;
 std::cout<<query_points_nb<<" points in range "<<range_rpg4<<std::endl;
 for(;it_range_rpg4.first != it_range_rpg4.second; ++it_range_rpg4.first)
   {
     std::cout<<*it_range_rpg4.first<<std::endl;
   }
 std::cout<<"rpg4.estimated_count_in_range(range_rpg4) "<<rpg4.estimated_count_in_range(range_rpg4)<<std::endl;
 it_range_rpg4.first = it_range_rpg4b;
 rpg4.erase(it_range_rpg4.first,it_range_rpg4.second);
 std::cout<<"rpg4.size() = "<<rpg4.size()<<std::endl;
 rpg4.write_svg("rpg4_2.svg");
 rpg4.write_vtk("rpg4_2.vtk");


  slip::Point2d<T> circle_center4(rpg4.box().middle());
  T circle_radius4 = T(10.0);
  std::pair<slip::RegularPointGrid2d<FPoint>::const_iterator,
  	    slip::RegularPointGrid2d<FPoint>::const_iterator> it_range_circle_rpg4 =
    rpg4.points_in_circle(circle_center4,circle_radius4,query_points_number);
  slip::RegularPointGrid2d<FPoint>::const_iterator it_range_circle_rpg4b = it_range_circle_rpg4.first;
  std::cout<<query_points_number<<" points in circle of center "<<circle_center4<<" and radius "<<circle_radius4<<std::endl;
  for(;it_range_circle_rpg4.first != it_range_circle_rpg4.second; ++it_range_circle_rpg4.first)
    {
      std::cout<<*it_range_circle_rpg4.first<<std::endl;
    }
  it_range_circle_rpg4.first = it_range_circle_rpg4b;
  rpg4.erase(it_range_circle_rpg4.first,it_range_rpg4.second);
  rpg4.write_svg("rpg4_3.svg");
  rpg4.write_vtk("rpg4_3.vtk");


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "distance " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  for(auto it_rpg3 = rpg3.begin();it_rpg3 != rpg3.end(); ++it_rpg3)
    {
      std::cout<<*it_rpg3<<std::endl;
    }
  slip::Matrix<double> Drpg3_L2;
  rpg3.distance(Drpg3_L2,slip::L22_dist<double,double>());
  std::cout<<"Drpg3_L2 = \n"<<Drpg3_L2<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "insert update " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"rpg3 = "<<rpg3<<std::endl;
  for(auto it_rpg3 = rpg3.begin();it_rpg3 != rpg3.end(); ++it_rpg3)
    {
      std::cout<<*it_rpg3<<std::endl;
    }
  FPoint p10(slip::Point2d<T>(T(4.0),T(3.5)),slip::EmptyFeature());
  rpg3.insert(p10);
  std::cout<<" insert "<<p10<<std::endl;
  
   std::cout<<"rpg3 = \n"<<rpg3<<std::endl;
   for(auto it_rpg3 = rpg3.begin();it_rpg3 != rpg3.end(); ++it_rpg3)
    {
      std::cout<<*it_rpg3<<std::endl;
    }

   std::cout<<" insert update "<<p10<<std::endl;
   rpg3.insert_update(p10);
   std::cout<<"rpg3 = \n"<<rpg3<<std::endl;
   for(auto it_rpg3 = rpg3.begin();it_rpg3 != rpg3.end(); ++it_rpg3)
    {
      std::cout<<*it_rpg3<<std::endl;
    }

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "insert update a range" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   FPoint p11(slip::Point2d<T>(T(-3.0),T(-2.0)),slip::EmptyFeature());
   FPoint p12(slip::Point2d<T>(T(4.1),T(2.0)),slip::EmptyFeature());
   FPoint p13(slip::Point2d<T>(T(-5.0),T(1.5)),slip::EmptyFeature());
   FPoint p14(slip::Point2d<T>(T(3.0),T(-2.0)),slip::EmptyFeature());


  std::vector<FPoint> plist2;
  plist2.push_back(p11);
  plist2.push_back(p12);
  plist2.push_back(p13);
  plist2.push_back(p14);
  rpg3.insert_update(plist2.begin(),plist2.end());
  std::cout<<"rpg3 = \n"<<rpg3<<std::endl;
   for(auto it_rpg3 = rpg3.begin();it_rpg3 != rpg3.end(); ++it_rpg3)
    {
      std::cout<<*it_rpg3<<std::endl;
    }

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "clear " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   rpg3.clear();
   std::cout<<"rpg3 = \n"<<rpg3<<std::endl;
   for(auto it_rpg3 = rpg3.begin();it_rpg3 != rpg3.end(); ++it_rpg3)
     {
       std::cout<<*it_rpg3<<std::endl;
    }
   
   
   rpg3.insert(p3);
   rpg3.insert(p4);
   std::cout<<"rpg3 = \n"<<rpg3<<std::endl;
   for(auto it_rpg3 = rpg3.begin();it_rpg3 != rpg3.end(); ++it_rpg3)
     {
       std::cout<<*it_rpg3<<std::endl;
     }
   

   typedef slip::FeaturePoint <Point,MyFeatures<T> > FPoint2;
   FPoint2 p15(slip::Point2d<T>(T(1.0),T(-1.0)),MyFeatures<T>(T(4.0)));
   FPoint2 p16(slip::Point2d<T>(T(1.5),T(-0.5)),MyFeatures<T>(T(2.0)));
   FPoint2 p17(slip::Point2d<T>(T(1.0),T(0.0)));
   
   std::vector<FPoint2> plist3;
   plist3.push_back(p15);
   plist3.push_back(p16);
   plist3.push_back(p17);

   slip::QuadTreeBox<slip::Point2d<T> > range_plist3(-4.0,-4.0,4.0,4.0);
   slip::RegularPointGrid2d<FPoint2> rpg5(range_plist3,
					  std::size_t(10),
					  std::size_t(1),
					  static_cast<T>(0.1));
   rpg5.insert(plist3.begin(),plist3.end());
    std::cout<<"rpg5 = \n"<<rpg5<<std::endl;
   for(auto it_rpg5 = rpg5.cbegin();it_rpg5 != rpg5.cend(); ++it_rpg5)
     {
       std::cout<<*it_rpg5<<std::endl;
     }

   
   for(auto it_rpg5 = rpg5.cbegin();it_rpg5 != rpg5.cend(); ++it_rpg5)
     {
       std::cout<<it_rpg5->get_features().get_radius()<<std::endl;
     }
   for(auto it_rpg5 = rpg5.begin();it_rpg5 != rpg5.end(); ++it_rpg5)
     {
       it_rpg5->get_features().set_radius(2.0); 
     }


   std::cout<<"rpg.insert_update("<<p14<<")"<<std::endl;
   rpg.insert_update(p14);
   for(auto it_rpg = rpg.begin();it_rpg != rpg.end(); ++it_rpg)
    {
      std::cout<<*it_rpg<<std::endl;
    }
   

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "find with non EmptyFeatures " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   FPoint2 p18(p16.get_point());
   slip::RegularPointGrid2d<FPoint2>::iterator it_find_rpg5 = rpg5.find(p18);
   if(it_find_rpg5 != rpg5.end())
    {
      std::cout<<p18<<" is in the rpg5 feature point list"<<std::endl;
    }
  else
    {
      std::cout<<p18<<" is not in the rpg5 feature point list"<<std::endl;
    }
   it_find_rpg5 = rpg5.find(p17);
   if(it_find_rpg5 != rpg5.end())
    {
      std::cout<<p17<<" is in the rpg5 feature point list"<<std::endl;
    }
  else
    {
      std::cout<<p17<<" is not in the rpg5 feature point list"<<std::endl;
    }
   it_find_rpg5 = rpg5.find(p15);
   if(it_find_rpg5 != rpg5.end())
    {
      std::cout<<p15<<" is in the rpg5 feature point list"<<std::endl;
    }
  else
    {
      std::cout<<p15<<" is not in the rpg5 feature point list"<<std::endl;
    }
   

   return 0;
}
