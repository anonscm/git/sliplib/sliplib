#include <iostream>
#include <iomanip>
#include <iterator>
#include <fstream>
#include "arithmetic_op.hpp"
#include "Point3d.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

int main()
{

  slip::Point3d<int> p;
  std::cout<<p<<std::endl;
  std::cout<<"name = "<<p.name()<<std::endl;


  slip::Point3d<float> pf(2.3,4.5,6.7);
  std::cout<<pf<<std::endl;
 
  slip::Point3d<float> pf2 = pf;
  std::cout<<pf2<<std::endl;
  
  slip::Point3d<float> pf3;
  pf3 = pf;
  std::cout<<pf3<<std::endl;
    
  std::cout<<pf3[0]<<" "<<pf3[1]<<" "<<pf3[2]<<std::endl;
  std::cout<<pf3.x1()<<" "<<pf3.x2()<<" "<<pf3.x3()<<std::endl;
  
  std::cout<<"dim = "<<pf3.dim()<<std::endl;
  std::cout<<"size = "<<pf3.size()<<std::endl;
  std::cout<<"empty = "<<pf3.empty()<<std::endl;

  slip::Point3d<float> pf4(3.0,4.0,5.0);
  pf4.swap(pf3);
  std::cout<<pf3<<std::endl;
  std::cout<<pf4<<std::endl;
 
  pf4[0] = 6.0;
  pf4[1] = 7.0;
  pf4[2] = 8.0;
  
  std::cout<<pf4<<std::endl;
 
  pf4.x1() = 8.0;
  pf4.x2() = 9.0;
  pf4.x3() = 10.0;
  
  std::cout<<pf4<<std::endl;
 
 
  // pf4[3] = 6.0;
  std::cout<<-pf4<<std::endl;
  
  std::cout<<"pf3 == pf4 :"<<(pf3==pf4)<<std::endl;
  std::cout<<"pf3 != pf4 :"<<(pf3!=pf4)<<std::endl;

  float tab[] = {1.0,2.0,3.0};
  slip::Point3d<float> pf5(tab);
  std::cout<<pf5<<std::endl;

// create and open a character archive for output
    std::ofstream ofs("point3d.txt");
    slip::Point3d<double> ps(1.3,2.4,4.5);
        
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<ps;
    }//to close archive
    std::ifstream ifs("point3d.txt");
    slip::Point3d<double> new_p;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_p;
    }//to close archive
    std::cout<<"new_p\n"<<new_p<<std::endl;

    std::ofstream ofsb("point3d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<ps;
    }//to close archive

    std::ifstream ifsb("point3d.bin");
    slip::Point3d<double> new_pb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_pb;
    }//to close archive
    std::cout<<"new_pb\n"<<new_pb<<std::endl;
  
return 0;
}
