#include <iostream>
#include <iomanip>
#include <vector>
#include <complex>
#include "DiscretePolyBase1d.hpp"
#include "Array.hpp"
#include "Vector.hpp"
#include "Polynomial.hpp"
#include "polynomial_algo.hpp"
#include "arithmetic_op.hpp"
#include "Vector2d.hpp"


double fun(const double& x)
{
  //return (1 + x*x - x*x*x)*std::exp(x/5);
  //return x;
  return (1.0 + x*x - x*x*x);
}


double runge_fun(const double& x)
{
 
  return (1.0 / (1.0 + x*x));
}


double exp_dec(const double & x)
{
  return std::exp(-x);
}

double ln2px(const double & x)
{
  return std::log(2.0+x);
}
double sqrt1px(const double & x)
{
  return std::sqrt(1.0+x);
}
double sqrt2px(const double & x)
{
  return std::sqrt(2.0+x);
}

double absx(const double & x)
{
  return std::abs(x);
}

double x_fun(const double& x)
{
  return x;
}

double step_fun(const double& x)
{
  if( x >= 0.0)
    return 1.0;
  else
    return 0.0;
}



int main()
{
  typedef double T;
  //typedef slip::ChebyshevWeight<T> Weight;
  typedef slip::LegendreWeight<T> Weight;
  //typedef slip::ChebyshevCollocations<T> Collocations;
  typedef slip::UniformCollocations<T> Collocations;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "base construction and generation" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  std::size_t range_size = 10;
  //std::size_t order = 9;
std::size_t order = 2;
  Weight weight_fun;
  Collocations collocation_fun;
  //Collocations collocation_fun(T(0.0),T(order));
  slip::DiscretePolyBase1d<T,
                               Weight,
                               Collocations> 
                               base(range_size,
					     order,
					     weight_fun, 
					     collocation_fun,
					     true);

  
  base.generate();
  base.print();

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "projection of data onto the basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Array<double> A(range_size);
  slip::iota(A.begin(),A.end(),1.0,1.0);
  //A.fill(2.1);
  std::cout<<"A = \n"<<A<<std::endl;
  
  slip::Array<double> coef(base.size());

  base.project(A.begin(),A.end(),
			coef.begin(),coef.end());
  
   
  
  std::cout<<"coef = \n"<<coef<<std::endl;
  
  for(std::size_t k = 0; k <= order; ++k)
    {
      std::cout<<"(A,P"<<k<<") = "<<base.project(A.begin(),A.end(),k)<<std::endl;
    }
 
 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "reconstruction of the data from the basis" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Array<double> Ar(A.size());
  std::size_t K = order;
  base.reconstruct(coef.begin(),coef.end(),
			    K,
			    Ar.begin(),
			    Ar.end());
  std::cout<<"Ar = \n"<<Ar<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "evaluate" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout<<"evaluate at x = -1.0: "<<base.evaluate(-1.0,coef.begin(),coef.end())<<std::endl;
  std::cout<<"evaluate at x = -0.111: "<<base.evaluate(-0.111,coef.begin(),coef.end())<<std::endl;
  std::cout<<"evaluate at x = 0.2: "<<base.evaluate(0.2,coef.begin(),coef.end())<<std::endl;
  std::cout<<"evaluate at x = 0.555: "<<base.evaluate(0.555,coef.begin(),coef.end())<<std::endl;
  
std::cout<<"evaluate at x = 1.0: "<<base.evaluate(1.0,coef.begin(),coef.end())<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "evaluate range" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Vector<double> x(20);
  slip::Vector<double> y(x.size());
 
  slip::iota(x.begin(),x.end(),-1.0,2.0/(x.size()-1));
  std::cout<<"x = \n"<<x<<std::endl;
  base.evaluate(x.begin(),x.end(),
			 coef.begin(),coef.end(),
			 y.begin(),y.end());
  std::cout<<"y= \n"<<y<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Orthogonality test" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Array2d<double> Gram;
  base.orthogonality_matrix(Gram);
  std::cout<<"orthogonality matrix:\n"<<Gram<<std::endl;
  std::cout<<"orthogonality test: "<<base.is_orthogonal(1e-14)<<std::endl;


  

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "copy constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::DiscretePolyBase1d<T,Weight,Collocations> base2(base);
  base2.print();

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "operator=" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::DiscretePolyBase1d<T,Weight,Collocations> base3;
  base3 = base;
  base3.print();
  

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "get_poly" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  T* pib;
  T* pie;
  base.get_poly(2,pib,pie);
  slip::Array<T> pi(base.range_size(),pib,pie)  ;
  std::cout<<"pi = \n"<<pi<<std::endl;

   



//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   // std::cout << "complex base construction and generation" << std::endl;
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

//   // std::size_t range_sizev = 10;
//   // std::size_t orderv = 5;
//   // slip::LegendreWeight<T> weight_funv;
//   // slip::UniformCollocations<T> collocation_funv;
//   // slip::DiscretePolyBase1d<T,
//   //                              slip::LegendreWeight<T>,
//   //                              slip::UniformCollocations<T> > 
//   //                              base_legendrev(range_sizev,
//   // 					     orderv,
//   // 					     weight_funv, 
//   // 					     collocation_funv,
//   // 					     false);

//   // slip::Array<std::complex<T> > av(range_size);
//   // slip::iota(av.begin(),av.end(),std::complex<T>(0.0,1.0),std::complex<T>(1.0,1.0));
//   // std::cout<<"av = \n"<<av<<std::endl;

//   // base_legendrev.generate();
//   // slip::Array<std::complex<T> > coefv(base_legendre.size());

//   // base_legendrev.project(av.begin(),av.end(),
//   // 			coefv.begin(),coefv.end());
  
//   // std::cout<<"coefv = \n"<<coefv<<std::endl;

//   // slip::Array<std::complex<T> > Arv(av.size());
//   // std::size_t Kv = coefv.size();//order;
//   // base_legendre.reconstruct(coefv.begin(),coefv.end(),
//   // 			    Kv,
//   // 			    Arv.begin(),
//   // 			    Arv.end());
//   // std::cout<<"Arv = \n"<<Arv<<std::endl;


 
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   // std::cout << "marginal complex base construction and generation" << std::endl;
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

//   // slip::Array<T> av_real(range_size);
//   // slip::Array<T> av_im(range_size);
//   // slip::iota(av_real.begin(),av_real.end(),T(0.0),T(1.0));
//   // slip::iota(av_im.begin(),av_im.end(),T(1.0),T(1.0));
 
//   // std::cout<<"av_real = \n"<<av_real<<std::endl;
//   // std::cout<<"av_im   = \n"<<av_im<<std::endl;
//   // slip::Array<T> coef_real(base_legendre.size());
//   // slip::Array<T> coef_im(base_legendre.size());
//   // base_legendrev.project(av_real.begin(),av_real.end(),
//   // 			 coef_real.begin(),coef_real.end());
//   // base_legendrev.project(av_im.begin(),av_im.end(),
//   // 			 coef_im.begin(),coef_im.end());

//   // std::cout<<"coef_real = \n"<<coef_real<<std::endl;
//   // std::cout<<"coef_im   = \n"<<coef_im<<std::endl;
  
//   // slip::Array<T> Ar_real(av.size());
//   // base_legendre.reconstruct(coef_real.begin(),coef_real.end(),
//   // 			    Kv,
//   // 			    Ar_real.begin(),Ar_real.end());
//   // std::cout<<"Ar_real = \n"<<Ar_real<<std::endl;
 
//   // slip::Array<T> Ar_im(av.size());
//   // base_legendre.reconstruct(coef_im.begin(),coef_im.end(),
//   // 			    Kv,
//   // 			    Ar_im.begin(),Ar_im.end());
//   // std::cout<<"Ar_im = \n"<<Ar_im<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Interpolation of a function" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  slip::Vector<double> xint(range_size);
  slip::uniform_collocations(xint.begin(),xint.end(),-1.0,1.0);
  std::cout<<"xint = \n "<<xint<<std::endl;
  slip::Vector<double> yint(xint);
  //yint.apply(x_fun);
  //yint.apply(exp_dec);
  //yint.apply(runge_fun);
  //yint.apply(ln2px);
  //yint.apply(sqrt1px);
  yint.apply(sqrt2px);
  //yint.apply(fun);
  //yint.apply(absx);
  //yint.apply(step_fun);
  
  std::cout<<"yint = \n "<<yint<<std::endl;
  //base_chebyshev.print();
  slip::Vector<double> coefint(base.size());
  base.project(yint.begin(),yint.end(),
			coefint.begin(),coefint.end());
  std::cout<<"coefint = \n"<<coefint<<std::endl;
  slip::Vector<double> xint_reconst(xint.size());
  base.reconstruct(coefint.begin(),coefint.end(),
		   coefint.size(),
		   xint_reconst.begin(),
		   xint_reconst.end());
  std::cout<<"xint_reconst = \n"<<xint_reconst<<std::endl;
  std::cout<<"evaluate at x = "<<xint[1]<<" : "<<base.evaluate(xint[1],coefint.begin(),coefint.end())<<std::endl;
  
  std::cout<<"alpha = \n";
  std::copy(base.alpha_begin(),base.alpha_end(),std::ostream_iterator<T>(std::cout," "));
   std::cout<<std::endl;

   slip::Array<T> sqrt_beta(base.beta_end()-base.beta_begin(),base.beta_begin(),base.beta_end());
   slip::apply(sqrt_beta.begin(),sqrt_beta.end(),sqrt_beta.begin(),std::sqrt);

  std::cout<<"beta = \n";
  std::copy(base.beta_begin(),base.beta_end(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;

  for(std::size_t i = 0; i < xint.size() ; ++i)
    {
      std::cout<<"Evaluate at x = "<<xint[i]<<" : \t"<<
	base.evaluate(xint[i],coefint.begin(),coefint.end())<<std::endl;
// 	slip::clenshaw_poly_norm(xint[i], 
// 				 base.alpha_begin(),base.alpha_end(),
// 				 base.beta_begin(),base.beta_end(),
// 				 coefint.begin(),coefint.end(),
// 				 coefint.size())<<std::endl;

    }
  slip::Array<T> pderx(base.size());
  for(std::size_t i = 0; i < xint.size() ; ++i)
    {
  // slip::derivative_stieltjes_basis(xint[i], 
  // 				   base.alpha_begin(),base.alpha_end(),
  // 				   base.beta_begin(),base.beta_end(),
  // 				   (base.size()-1),
  // 				   pderx.begin(),pderx.end());
  slip::derivative_stieltjes_basis_norm(xint[i], 
				   base.alpha_begin(),base.alpha_end(),
				   base.beta_begin(),base.beta_end(),
				   (base.size()-1),
				   pderx.begin(),pderx.end());
  std::cout<<"pderx = \n"<<pderx<<std::endl;
  std::cout<<"derivative("<<xint[i]<<") = "<<std::inner_product(pderx.begin(),pderx.end(),coefint.begin(),T(0.0))<<std::endl;
  std::cout<<"derivative2("<<xint[i]<<") = "<<
    slip::derivative_poly_norm(xint[i],
			  base.alpha_begin(),base.alpha_end(),
			  base.beta_begin(),base.beta_end(),
			  coefint.begin(),coefint.end(),
			  coefint.size()-1)<<std::endl;
  std::cout<<"derivative3("<<xint[i]<<") = "<<base.derivative(xint[i],
							      coefint.begin(),coefint.end())<<std::endl;
  std::cout<<"derivative3("<<(xint[i]+0.2)<<") = "<<base.derivative(xint[i]+0.2,
							      coefint.begin(),coefint.end())<<std::endl;
  // std::cout<<"eval_poly_stieltjes("<<xint[i]<<") = "<<
//     slip::eval_poly_stieltjes(xint[i],
// 			  base.alpha_begin(),base.alpha_end(),
// 			  base.beta_begin(),base.beta_end(),
// 			  coefint.begin(),coefint.end(),
// 			  coefint.size()-1)<<std::endl;
  std::cout<<"eval_poly_stieltjes("<<xint[i]<<") = "<<
    slip::eval_poly_stieltjes_norm(xint[i],
			  base.alpha_begin(),base.alpha_end(),
			  base.beta_begin(),base.beta_end(),
			  coefint.begin(),coefint.end(),
			  coefint.size()-1)<<std::endl;
    }

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Jacobi matrix" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Array2d<T> Jacobi;
   base.jacobi_matrix(Jacobi);
   std::cout<<"Jacobi matrix:\n"<<Jacobi<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Gram Schmidt orthogonalisation" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  std::cout<<"orthogonality matrix:\n"<<Gram<<std::endl;
  base.gram_schmidt();
  //base.print();
  base.orthogonality_matrix(Gram);
  std::cout<<"orthogonality matrix:\n"<<Gram<<std::endl;
  //base.orthogonality_matrix(Gram);


   slip::DiscretePolyBase1d<T,
                               Weight,
                               Collocations> 
     canonical_base;
   canonical_base.init_canonical(range_size,
				 //order,
				 2,
				 weight_fun, 
				 collocation_fun);
   std::cout<<"before orthogonalization ---------------"<<std::endl;
   canonical_base.print();
   std::cout<<"alpha = \n";
  std::copy(canonical_base.alpha_begin(),canonical_base.alpha_end(),std::ostream_iterator<T>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<"beta = \n";
  std::copy(canonical_base.beta_begin(),canonical_base.beta_end(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;

   canonical_base.gram_schmidt();
   
   std::cout<<"after orthogonalization  ---------------"<<std::endl;
   canonical_base.print();
   std::cout<<"alpha = \n";
   std::copy(canonical_base.alpha_begin(),canonical_base.alpha_end(),std::ostream_iterator<T>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<"beta = \n";
  std::copy(canonical_base.beta_begin(),canonical_base.beta_end(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"alpha legendre= \n";
  std::copy(base.alpha_begin(),base.alpha_end(),std::ostream_iterator<T>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<"beta legendre = \n";
  std::copy(base.beta_begin(),base.beta_end(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
   //canonical_base.gram_schmidt_normalized();
   canonical_base.orthogonality_matrix(Gram);
   std::cout<<"orthogonality matrix:\n"<<Gram<<std::endl;
    

    slip::DiscretePolyBase1d<T,
                               Weight,
                               Collocations> 
    * canonical_base2 = new slip::DiscretePolyBase1d<T,Weight,Collocations>();
    delete canonical_base2;

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "base Legendre 8" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    typedef slip::LegendreWeight<T> Weight8;
    std::size_t range_size8 = 8;
    std::size_t order8 = 1;
    Weight8 weight_fun8;
    //Collocations collocation_fun(T(0.0),T(order));
    slip::DiscretePolyBase1d<T,
				 Weight8,
				 Collocations> 
      base_leg8(range_size8,
	   order8,
	   weight_fun8, 
	   collocation_fun,
	   false);
   
  
  base_leg8.generate();
  base_leg8.print();
   slip::Matrix<T> Gram8;
   base_leg8.orthogonality_matrix(Gram8);
  std::cout<<"Orthogonality matrix :\n"<<Gram8<<std::endl;


  return 0;
}
