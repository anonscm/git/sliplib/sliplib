#include<algorithm>
#include<iostream>
#include<iterator>
#include <numeric>
#include <complex>
//#include "Array2d.hpp"
#include "Array3d.hpp"

#include "Point3d.hpp"
#include "DPoint3d.hpp"
//#include "DPoint2d.hpp"
#include "Range.hpp"
#include "iterator3d_range.hpp"
#include "stride_iterator.hpp"

void accum(int& a){static int init = 0; a = init++;}

int main()
{
 
  //Tests of array3d iterators
  slip::Array3d<int> M(4,4,6,0);
  slip::Array3d<int> Md(4,4,6,0);
  //M filling
  std::for_each(M.begin(),M.end(),accum);
  std::cout<<M<<std::endl;
  std::cout<<std::endl<<std::endl;
  //Copy of the 3rd and the 4th slices of M in Mb (3rd and 2nd slices)
//   std::copy(M.begin(2),M.end(2),Md.begin(3));
//   std::copy(M.rbegin(3),M.rend(3),Md.begin(2));
  //Copy of the 2nd and the 3rd rows of the 2nd slice of M in Md (2nd slice, 1st and 3rd rows)
  std::copy(M.row_begin(1,1),M.row_end(1,1),Md.row_begin(1,1));
  std::copy(M.row_rbegin(1,2),M.row_rend(1,2),Md.row_rbegin(1,3));
  //Copy of the 4th and the 5th columns of the 1st slice of M (except first elements, 5th and 4th columns)
  std::copy(M.col_begin(0,4)+1,M.col_end(0,4),Md.col_begin(0,4));
  std::copy(M.col_rbegin(0,3),M.col_rend(0,3)-1,Md.col_rbegin(0,3));
  std::cout<<Md<<std::endl;
  std::cout<<std::endl<<std::endl;


  // slip::Point3d<int> db(0,2,2);
  slip::Range<int> slrg(1,3,2);
  slip::Range<int> rwrg(0,2,2);
  slip::Range<int> clrg(3,5,2);

  slip::DPoint3d<int> de(1,1,1);
  slip::iterator3d_range<slip::Array3d<int> > beg(&M,slrg,rwrg,clrg);
  slip::iterator3d_range<slip::Array3d<int> > e = beg + de;
  std::cout<<*beg << " - " << *e<<std::endl<<std::endl;

  slip::iterator3d_range<slip::Array3d<int> > it;

  
   for(it = beg; it <= e; ++it)
     {
       std::cout<<*it<<" ";
     }
   std::cout<<std::endl;
   std::cout<<std::endl;
 
  for(it = M.front_upper_left(slrg,rwrg,clrg); it < M.back_bottom_right(slrg,rwrg,clrg); ++it)
    {
      std::cout<<*it<<" ";
    }
  std::cout<<std::endl;
  std::cout<<std::endl;
  for(it = M.back_bottom_right(slrg,rwrg,clrg) - de; it >= M.front_upper_left(slrg,rwrg,clrg); --it)
    {
      std::cout<<*it<<" ";
    }
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::reverse_iterator<slip::iterator3d_range<slip::Array3d<int> > > it2;

  for(it2 = M.rfront_upper_left(slrg,rwrg,clrg); it2 != M.rback_bottom_right(slrg,rwrg,clrg); ++it2)
    {
      std::cout<<*it2<<" ";
    }
  std::cout<<std::endl;
  std::cout<<std::endl;


  //////////////////////////// 

  std::cout<<"e - beg = "<<(e-beg)<<std::endl;
  std::cout<<beg[de]<<std::endl<<std::endl;
    
  std::cout<<" beg == beg "<<(beg == beg)<<std::endl;
  std::cout<<" beg != beg "<<(beg != beg)<<std::endl;
  std::cout<<" beg == e "<<(beg == e)<<std::endl;
  std::cout<<" beg != e "<<(beg != e)<<std::endl;
  std::cout<<" beg < e "<<(beg < e)<<std::endl;
  std::cout<<" beg <= e "<<(beg <= e)<<std::endl;
  std::cout<<" beg > e "<<(beg > e)<<std::endl;
  std::cout<<" beg >= e "<<(beg >= e)<<std::endl;
    
  slip::iterator3d_range<slip::Array3d<int> > it3 = beg;
  for(it3 = beg; it3 != e; ++it3)
    {
      std::cout<<" (k,i,j) = ("<<it3.x1()<<","<<it3.x2()<<","<<it3.x3()<<") ";
    }
  std::cout<<std::endl;
    
  slip::Array3d<int> Ex(2,2,2);
  e+=de;
  std::copy(beg,e,Ex.begin());
  std::cout<<Ex<<std::endl;
    
    
  std::cout<<"const upper_left - back_bottom_right "<<std::endl;
  std::copy(M.front_upper_left(slrg,rwrg,clrg),M.back_bottom_right(slrg,rwrg,clrg),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"const rupper_left - rback_bottom_right "<<std::endl;
  std::copy(M.rfront_upper_left(slrg,rwrg,clrg),M.rback_bottom_right(slrg,rwrg,clrg),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
    
  slip::Range<int> r1(0,4,2);
  std::cout<<r1<<std::endl;
  slip::Range<int> r2(1,4,1);
  std::cout<<r2<<std::endl;
  slip::Range<int> r3(3,6,2);
  std::cout<<r3<<std::endl;
  slip::Array3d<int> M2(5,5,8,0);
  std::cout<<"front_upper_left - back_bottom_right "<<std::endl;
  std::copy(M.front_upper_left(slrg,rwrg,clrg),M.back_bottom_right(slrg,rwrg,clrg),M2.front_upper_left(r1,r2,r3));
  std::cout<<M2<<std::endl;
  std::cout<<"rupper_left - rback_bottom_right "<<std::endl;
  std::copy(M.front_upper_left(slrg,rwrg,clrg),M.back_bottom_right(slrg,rwrg,clrg),M2.rfront_upper_left(r1,r2,r3));
    
  std::cout<<M2<<std::endl;
    
    
    
  slip::Array3d<int>::iterator3d_range itc = M.front_upper_left(slrg,rwrg,clrg);
 //  std::cout<<"slice of the range "<<std::endl; 
//   for(std::size_t k = 0; k <= slrg.iterations();++k)
//     {
//       std::copy(itc.begin(k),itc.end(k),std::ostream_iterator<int>(std::cout," "));
//       std::cout<<std::endl;
//       //     std::cout<<*itc.end(k)<<std::endl;
//     }
  std::cout<<std::endl;
  std::cout<<"rows of the range "<<std::endl; 
  for(std::size_t k = 0; k <= slrg.iterations();++k)
    {
      std::cout<<std::endl;
      for(std::size_t i = 0; i <= rwrg.iterations();++i)
	{
	  std::copy(itc.row_begin(k,i),itc.row_end(k,i),std::ostream_iterator<int>(std::cout," "));
	  std::cout<<std::endl;
	}
    }
  std::cout<<std::endl;
  std::cout<<"cols of the range "<<std::endl; 
 for(std::size_t k = 0; k <= slrg.iterations();++k)
   { 
     std::cout<<std::endl;
     for(std::size_t j = 0; j <= clrg.iterations();++j)
       {
	 std::copy(itc.col_begin(k,j),itc.col_end(k,j),std::ostream_iterator<int>(std::cout," "));
	 std::cout<<std::endl;
       }
   }
  std::cout<<std::endl;
    
  slip::Range<int> all1(0,M.dim1()-1,2);
  std::cout<<all1<<std::endl;
  std::cout<<all1.iterations()<<std::endl;
  slip::Range<int> all2(0,M.dim2()-1,2);
  std::cout<<all2<<std::endl;
  std::cout<<all2.iterations()<<std::endl;
 slip::Range<int> all3(0,M.dim3()-1,2);
  std::cout<<all3<<std::endl;
  std::cout<<all3.iterations()<<std::endl;


  std::cout<<"Forward copy"<<std::endl;
  std::copy(M.front_upper_left(all1,all2,all3),M.back_bottom_right(all1,all2,all3), std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;

  std::cout<<"Backward copy"<<std::endl;
  it2 = M.rfront_upper_left(all1,all2,all3);
  std::copy(M.rfront_upper_left(all1,all2,all3),M.rback_bottom_right(all1,all2,all3), std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  
  return 0;
}
