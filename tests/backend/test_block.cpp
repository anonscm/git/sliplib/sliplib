#include "Block.hpp"
#include "Range.hpp"
#include "arithmetic_op.hpp"
#include <algorithm>
#include <iterator>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <numeric>
#include <cmath>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

//foncion unaire qui affiche une valeur
template<class T> struct print : public std::unary_function<T,void>
{
  print(std::ostream& out):os(out),count(0){}
  void operator() (T x){os<<x<<" ";++count;}
  std::ostream& os;
  int count;
};

double C()
{return -4.0;}

template<class T> struct test_func : public std::unary_function<T,T>
{
  test_func(){}
  T operator() (T x){return x + 2.2;}
};


int main(){

  //----------------------------
  //fill
  //----------------------------
  slip::block<int,8> B = {8,7,6,5,3,3,2,1};
  std::cout<<B<<std::endl;
  
  slip::block<int,8> B2;
  B2.fill(4);
  std::cout<<B2<<std::endl;
  int tabi[] = {1,2,3,4,5,6};
  slip::block<int,6> B3;
  B3.fill(tabi);
  std::cout<<B3<<std::endl;
  
  slip::block<float,6> B4;
  B4.fill(tabi, tabi+6);
  std::cout<<B4<<std::endl;
   
 
  slip::block<int,5> B5;
  B5.fill(tabi, tabi+5);
  std::cout<<B5<<std::endl;
  //----------------------------
  
  
  //----------------------------
  //Accessors
  //----------------------------
  std::cout<<"empty : "<<B.empty()<<" size = "<<B.size()<<" max_size = "<<B.max_size()<<std::endl;
  //----------------------------

  //----------------------------
  //Swap
  //----------------------------
  B.swap(B2);
  std::cout<<B<<std::endl;
  std::cout<<B2<<std::endl;
  //----------------------------  

  //----------------------------
  //iterators
  //----------------------------
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Global Iterators : " << std::endl;
  std::copy(B2.begin(),B2.end(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::copy(B2.rbegin(),B2.rend(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;

  slip::block<int,8> const B2c(B2);
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Global Const Iterators : " << std::endl;
  std::copy(B2c.begin(),B2c.end(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::copy(B2c.rbegin(),B2c.rend(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Transform : B = B2 + Reverse(B2)" << std::endl;
  std::transform(B2.rbegin(),B2.rend(),B2.begin(),B.begin(),std::plus<int>());
  
  std::cout<<B<<std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "B3 : " << std::endl;
  std::cout<<B3<<std::endl;
  std::cout<<std::endl;
  std::cout << "Range1  :" << std::endl;
  slip::Range<int> range(1,5,2);
  std::cout<<range << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Range Iterators : " << std::endl;
  std::copy(B3.begin(range),B3.end(range),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(B3.rbegin(range),B3.rend(range),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  slip::block<int,6> const B3c(B3);
  std::cout << "Range Const Iterators : " << std::endl;
  std::copy(B3c.begin(range),B3c.end(range),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(B3c.rbegin(range),B3c.rend(range),std::ostream_iterator<double>(std::cout," "));

  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Range2  :" << std::endl;
  slip::Range<int> range2(0,5,3);
  std::cout<<range2 << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Range Iterators : " << std::endl;
  std::copy(B3.begin(range2),B3.end(range2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(B3.rbegin(range2),B3.rend(range2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Range Const Iterators : " << std::endl;
  std::copy(B3c.begin(range2),B3c.end(range2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(B3c.rbegin(range2),B3c.rend(range2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  //----------------------------  


  //----------------------------
  //comparison operators
  //----------------------------
  std::cout<<" B == B2 "<<(B == B2)<<std::endl;
  std::cout<<" B != B2 "<<(B != B2)<<std::endl;
  std::cout<<" B < B2  "<<(B < B2)<<std::endl;
  std::cout<<" B > B2  "<<(B > B2)<<std::endl;
  std::cout<<" B <= B2 "<<(B <= B2)<<std::endl;
  std::cout<<" B >= B2 "<<(B >= B2)<<std::endl;
  //----------------------------  


  //----------------------------
  //elements access operators
  //----------------------------
  B[2] = 3;
  B[3] = 7;
  std::cout<<B<<std::endl;
  B(2) = 7;
  B(3) = 3;
  std::cout<<B<<std::endl;

  for(std::size_t i = 0; i < B.size(); ++i)
    {
      std::cout<<B[i]<<" ";
    }
  std::cout<<std::endl;
  //----------------------------  

  //----------------------------
  //Affectation
  //----------------------------
  slip::block<float,6> b = B4;
  std::cout<<b<<std::endl;
  b = 9.9;
  std::cout<<b<<std::endl;
  //----------------------------
  

//   std::sort(B.begin(),B.end());
//   std::cout<<B<<std::endl;
//   //recherche de l'element 6
//   slip::block<int,8>::iterator result = std::find(B.begin(),B.end(),6);
//   if(result == B.end())
//     {
//       std::cout<<"Element non trouve"<<std::endl;
//     }
//   else
//     {
//     std::cout<<"result = "<<*result<<std::endl;
//     }
//   //recherche du premier element plus grand que 3
//   slip::block<int,8>::iterator result2 = std::find_if(B.begin(),B.end(),std::bind2nd(std::greater<int>(),3));
//   if(result2 == B.end())
//     {
//       std::cout<<"Element non trouve"<<std::endl;
//     }
//   else
//     {
//     std::cout<<"result = "<<*result2<<std::endl;
//     }
//   //recherche de deux elements adjacent dans le container egaux 
//   //retourne le premier itérateur 
//    slip::block<int,8>::iterator result3 = std::adjacent_find(B.begin(),B.end());
//   if(result3 == B.end())
//     {
//       std::cout<<"Element non trouve"<<std::endl;
//     }
//   else
//     {
//     std::cout<<"result = "<<*result3<<std::endl;
//     }

//    //recherche le premier element qui est plus petit que le suivant 
//    slip::block<int,8>::iterator result4 = std::adjacent_find(B.begin(),B.end(),std::less<int>());
//   if(result4 == B.end())
//     {
//       std::cout<<"Element non trouve"<<std::endl;
//     }
//   else
//     {
//     std::cout<<"result = "<<*result4<<std::endl;
//     }

 
//   std::cout<<"Il y a "<<std::count(B.begin(),B.end(),3)<<" 3 dans le slip::block"<<std::endl;
 
//   std::cout<<"Il y a "<<std::count_if(B.begin(),B.end(),std::bind2nd(std::greater<int>(),2))<<" valeur plus grande que 2 dans le slip::block"<<std::endl;


//   //algorithme for_each
//   print<int> P = std::for_each(B.begin(),B.end(),print<int>(std::cout));
//   std::cout<<std::endl;
//   std::cout<<P.count<<" elements imprimes"<<std::endl;

//   char* commands[] = {"pwd","ls","date"};
//   const int N = sizeof(commands)/sizeof(char*);
  
//   std::for_each(commands,commands+N,system);


//   //comparaison de deux intervalles
//   //....
  
//   //elements max et min d'un container
//   //en utilisant <
//   slip::block<int,8>::iterator min = std::min_element(B.begin(),B.end());
//   slip::block<int,8>::iterator max = std::max_element(B.begin(),B.end());
//   std::cout<<"min = "<<*min<<" max = "<<*max<<std::endl;
  
//   //en utilisant d'autres regles
//   slip::block<int,8>::iterator min2 = std::min_element(B.begin(),B.end(),std::less_equal<int>());
//   slip::block<int,8>::iterator max2 = std::max_element(B.begin(),B.end(),std::less_equal<int>());
//   std::cout<<"min2 = "<<*min2<<" max2 = "<<*max2<<std::endl;
 
//   //accumulate avec valeur initiale nulle
//   std::cout<<std::accumulate(B.begin(),B.end(),0)<<std::endl;
//   //accumulate avec valeur initiale egale a 10
//   std::cout<<std::accumulate(B.begin(),B.end(),10)<<std::endl;

  

//   //copy
//   slip::block<int,8> B2;

//   std::copy(B.begin(),B.end(),B2.begin());
//   std::cout<<B2<<std::endl;
  
//   //copy est affichage direct
//   std::copy(B.begin(),B.end(),std::ostream_iterator<int>(std::cout,"\n"));

//   //swap de deux elements
//   std::iter_swap(B.begin(),max);
//   std::cout<<B<<std::endl;

//   //partial_sum
//   std::cout<<"somme partielle "<<std::endl;
//   std::partial_sum(B.begin(),B.end(),B2.begin());
//   std::cout<<B2<<std::endl;
//   //partial_sum avec une fonction binaire autre que std::plus<int>()
//   std::partial_sum(B.begin(),B.end(),B2.begin(),std::multiplies<int>());
//   std::cout<<B2<<std::endl;
//   //produit scalaire
//   //standard
//   std::cout<<std::inner_product(B.begin(),B.end(),B2.begin(),0)<<std::endl;
//   //plus une valeur initiale (10 ici)
//   std::cout<<std::inner_product(B.begin(),B.end(),B2.begin(),10)<<std::endl;
//   //version tres generale
//   std::cout<<std::inner_product(B.begin(),B.end(),B2.begin(),10,std::plus<int>(),std::multiplies<int>())<<std::endl;

//   //transformation d'un container
//   slip::block<int,8> B3;
//   std::transform(B.begin(),B.end(),B3.begin(),std::negate<int>());
//   std::cout<<B3<<std::endl;
//   std::transform(B3.begin(),B3.end(),B2.begin(),B3.begin(),std::plus<int>());
//   std::cout<<B3<<std::endl;
//   //replace
//   std::replace(B3.begin(),B3.end(),0,10);
//   std::cout<<B3<<std::endl;


  

//   //fill
//   std::fill(B3.begin(),B3.end(),5);
//   std::cout<<B3<<std::endl;
//   std::fill_n(B3.begin(),3,7);
//   std::cout<<B3<<std::endl;


//    //reverse 
//    std::reverse(B3.begin(),B3.end());
//    std::cout<<B3<<std::endl;
//    //generate applique une fonction sur tous les elements du container
//    slip::block<double,8> B4;
//    std::cout<<std::endl<<"generate..."<<std::endl;
//    //std::generate(B4.begin(),B4.end(),std::rand);
//    // std::cout<<B4<<std::endl;
//    std::generate(B4.begin(),B4.end(),&C);
//    std::cout<<B4<<std::endl;
//    slip::block<double,8> B5;
//    std::transform(B4.begin(),B4.end(),B5.begin(),test_func<double>());
//    std::cout<<B5<<std::endl;

//    slip::block<int,6> B8;
//    B8.fill(4);
//    std::cout<<B8<<std::endl;
  

//    std::cout<<"sqrt"<<std::endl;
//    std::transform(B10.begin(),B10.end(),B10.begin(),std::ptr_fun(sqrt));
//    std::cout<<B10<<std::endl;
   

// create and open a character archive for output
    std::ofstream ofs("block.txt");
    slip::block<float,10> bs;
    for(std::size_t i = 0; i < bs.size(); ++i)
      {
	bs[i] = static_cast<float>(i);
      }
    
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<bs;
    }//to close archive
    std::ifstream ifs("block.txt");
    slip::block<float,10> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("block.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<b;
    }//to close archive

    std::ifstream ifsb("block.bin");
    slip::block<float,10> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;
    

    slip::block<std::complex<float>,10> bc;
    slip::iota(bc.begin(),bc.end(),std::complex<float>(1.0f,2.0f),
	       std::complex<float>(1.0f,1.0f));
    std::ofstream ofsc("blockc.txt");
     {
       
       boost::archive::text_oarchive oac(ofsc);
       oac<<bc;
     }//to close archive
    std::ifstream ifsc("blockc.txt");
    slip::block<std::complex<float>,10> new_pc;
    {
    boost::archive::text_iarchive iac(ifsc);
    iac>>new_pc;
    }//to close archive
    std::cout<<"new_pc = \n"<<new_pc<<std::endl;
    
    
  
  return EXIT_SUCCESS;

}
