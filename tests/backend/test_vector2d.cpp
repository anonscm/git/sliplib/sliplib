#include "Vector2d.hpp"
#include "arithmetic_op.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <complex>
#include <algorithm>
#include <numeric>



#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

int main()
{


  //------------------------------
  //constructors
  //------------------------------
  slip::Vector2d<double> V;
  std::cout<<V<<std::endl;
  std::cout<<"name = "<<V.name()<<std::endl;
 
  slip::Vector2d<float> V2(255.0);
  std::cout<<V2<<std::endl;

  double d[] = {125.0,126.0};
  
  slip::Vector2d<double> V3(d);
  std::cout<<V3<<std::endl;

  slip::Vector2d<double> V4 = V3;
  std::cout<<V4<<std::endl;

  slip::Vector2d<double> V5(2.2,2.3);
  std::cout<<V5<<std::endl;
  
  

//   V = V3;
//   std::cout<<V<<std::endl;
  
  //------------------------------
  //elements access
  //------------------------------


  V5[0] = 128.0;
  V5[1] = 127.0;

  std::cout<<V5<<std::endl;

  for(int i = 0; i < 2; ++i)
    std::cout<<V5[i]<<" ";
  std::cout<<std::endl;

  std::cout<<V5.get_x1()<<" "<<V5.get_x2()<<std::endl;
  std::cout<<V5.get_x()<<" "<<V5.get_y()<<std::endl;
  std::cout<<V5.u()<<" "<<V5.v()<<std::endl;
  V5.set_x1(243.0);
  V5.set_x2(244.0);
  std::cout<<V5<<std::endl;
  V5.set_x(212.0);
  V5.set_y(213.0);

  std::cout<<V5<<std::endl;
  V5.set(12.0,13.0);
  std::cout<<V5<<std::endl;

  std::cout<<V5.size()<<std::endl;
  std::cout<<V5.max_size()<<std::endl;
  std::cout<<V5.empty()<<std::endl;
  //------------------------------


  //------------------------------
  //swap
  //------------------------------

  V5.swap(V3);
  std::cout<<V5<<std::endl;
  std::cout<<V3<<std::endl;
  //------------------------------



  //----------------------------
  //fill
  //----------------------------

  slip::Vector2d<float> V6;
  V6.fill(128.0);
  std::cout<<V6<<std::endl;
  float tabf[] = {124.0,125};
  slip::Vector2d<float> V7;
  V7.fill(tabf);
  std::cout<<V7<<std::endl;
  //------------------------------

  
  //----------------------------
  //comparison operators
  //----------------------------
  std::cout<<V2<<std::endl;
  std::cout<<V6<<std::endl;

  std::cout<<"(V2 == V6) "<<(V2 == V6)<<std::endl;
  std::cout<<"(V2 != V6) "<<(V2 != V6)<<std::endl;
  std::cout<<" V2 <  V6 "<<(V2 < V6)<<std::endl;
  std::cout<<" V2 >  V6 "<<(V2 > V6)<<std::endl;
  std::cout<<" V2 <= V6 "<<(V2 <= V6)<<std::endl;
  std::cout<<" V2 >= V6 "<<(V2 >= V6)<<std::endl;
  //------------------------------


  std::cout<<V2<<std::endl;
  std::cout<<V6<<std::endl;
  slip::Vector2d<float> V8;
  V8 = V2 + V6;
  //std::cout<<V2 + V6<<std::endl;
  std::cout<<V8<<std::endl;

  V8 = V2 + 2.0f;
  std::cout<<V8<<std::endl;

  V8 = 2.0f + V2;
  std::cout<<V8<<std::endl;

   V8 = V2 - V6;
  std::cout<<V8<<std::endl;

  V8 = V2 - 2.0f;
  std::cout<<V8<<std::endl;

  V8 = 2.0f - V2;
  std::cout<<V8<<std::endl;

  V8 = V2 * V6;
  std::cout<<V8<<std::endl;

  V8 = V2 * 2.0f;
  std::cout<<V8<<std::endl;

  V8 = 2.0f * V2;
  std::cout<<V8<<std::endl;

  V8 = V2 / V6;
  std::cout<<V8<<std::endl;

  V8 = V2 / 2.0f;
  std::cout<<V8<<std::endl;

  
  std::cout<<-V2<<std::endl;

  V2.apply(std::sqrt);
  std::cout<<V2<<std::endl;
   
  std::cout<<V2.Euclidean_norm()<<std::endl;

  std::cout<<(-V2).angle()<<std::endl;
  std::cout<<(-V2).angle_degree()<<std::endl;
  

  slip::Vector2d<double> V10(2.2,2.3);
  std::cout<<V10<<std::endl;
  slip::Vector2d<double> V11(1.2,-0.5);
  std::cout<<V11<<std::endl;
  std::cout<<"angular error radian= "<<slip::AE_rad<double>()(V10,V11)<<std::endl;
  std::cout<<"angular error degree= "<<slip::AE_deg<double>()(V10,V11)<<std::endl;
  
  std::cout<<"L2 dist= "<<slip::L2_dist_vect<slip::Vector2d<double> >()(V10,V11)<<std::endl;
  std::cout<<"L22 dist= "<<slip::L22_dist_vect<slip::Vector2d<double> >()(V10,V11)<<std::endl;
  std::cout<<"L1 dist= "<<slip::L1_dist_vect<slip::Vector2d<double> >()(V10,V11)<<std::endl;

  //serialization
  // create and open a character archive for output
    std::ofstream ofs("vector2d.txt");
    slip::Vector2d<double> p(1.3,2.4);
        
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<p;
    }//to close archive
    std::ifstream ifs("vector2d.txt");
    slip::Vector2d<double> new_p;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_p;
    }//to close archive
    std::cout<<"new_p\n"<<new_p<<std::endl;

    std::ofstream ofsb("vector2d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<p;
    }//to close archive

    std::ifstream ifsb("vector2d.bin");
    slip::Vector2d<double> new_pb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_pb;
    }//to close archive
    std::cout<<"new_pb\n"<<new_pb<<std::endl;
    
 
   return 0;
}
