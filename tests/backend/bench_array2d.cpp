//#include "slip.h"
#include "Array2d.hpp"
#include <iostream>
#include <complex>
#include <algorithm>
#include <numeric>


int main(int argc, char* argv[])
{
  if(argc < 1)
    {
      std::cout << "syntax <./bench_array2d 25> where 25 is the Array2d dimension." << std::endl;
      return 1;
    }

  const std::size_t N = std::size_t(atoi(argv[1]));
  
  for (int i=0;i<100;i++) {
   slip::Array2d<double> M1(N,N,2.2);
   slip::Array2d<double> M2(N,N,1.0);
   std::cout<<std::inner_product(M1.begin(),M1.end(),M2.begin(),0.0)<<std::endl;   
  }

   
   return 0;
}
