#include "Matrix.hpp"
#include "Point2d.hpp"
#include "RegularVector2dField2d.hpp"
#include "Vector2d.hpp"
#include "Box2d.hpp"
#include "iterator2d_box.hpp"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <complex>
#include <algorithm>
#include <numeric>
#include <vector>
#include "dynamic.hpp"
#include "arithmetic_op.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

int main()
{

  //------------------------------
  //constructors
  //------------------------------
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "constructors " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::RegularVector2dField2d<double> M;
  std::cout<<"name = "<<M.name()<<" dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 

  slip::RegularVector2dField2d<double> M2(6,3);
  std::cout<<"dim1 = "<<M2.dim1()<<" dim2 = "<<M2.dim2()<<" size = "<<M2.size()<<" empty = "<<M2.empty()<<std::endl; 
  std::cout<<"rows = "<<M2.rows()<<" cols = "<<M2.cols()<<" columns = "<<M2.columns()<<std::endl;
  std::cout<<"height = "<<M2.height()<<" width = "<<M2.width()<<std::endl; 
  std::cout<<M2<<std::endl; 
 
  slip::RegularVector2dField2d<double> M3(6,3,slip::Vector2d<double>(1.0,2.0));
  std::cout<<"dim1 = "<<M3.dim1()<<" dim2 = "<<M3.dim2()<<" size = "<<M3.size()<<" empty = "<<M3.empty()<<std::endl; 
  std::cout<<"rows = "<<M3.rows()<<" cols = "<<M3.cols()<<" columns = "<<M3.columns()<<std::endl;
  std::cout<<M3<<std::endl;


 double d[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0};
 slip::RegularVector2dField2d<double> M4(2,3,d);
 std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
 std::cout<<M4<<std::endl; 
 

 slip::Matrix<double> Mat1(4,3,12.0);
 slip::Matrix<double> Mat2(4,3,13.0);

 
 slip::RegularVector2dField2d<double> Mcol(4,3,Mat1.begin(),Mat1.end(),Mat2.begin());
 std::cout<<Mcol<<std::endl; 


//   std::vector<int> V(20);
//   for(std::size_t i = 0; i < 20; ++i)
//     V[i] = i;
//   slip::RegularVector2dField2d<int> M5(4,5,V.begin(),V.end());
//   std::cout<<"dim1 = "<<M5.dim1()<<" dim2 = "<<M5.dim2()<<" size = "<<M5.size()<<" empty = "<<M5.empty()<<std::endl; 
//  std::cout<<M5<<std::endl; 


 slip::RegularVector2dField2d<double> M6 = M4;
  std::cout<<"dim1 = "<<M6.dim1()<<" dim2 = "<<M6.dim2()<<" size = "<<M6.size()<<" empty = "<<M6.empty()<<std::endl; 
 std::cout<<M6<<std::endl; 

 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "constructors with init point and grid step" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

 slip::RegularVector2dField2d<double> M10(2,3,slip::Point2d<double>(0.2,0.4),slip::Point2d<double>(1.1,1.2));
 std::cout<<"dim1 = "<<M10.dim1()<<" dim2 = "<<M10.dim2()<<" size = "<<M10.size()<<" empty = "<<M10.empty()<<std::endl; 
 std::cout<<"init point = "<<M10.get_init_point()<<std::endl;
 std::cout<<"grid step = "<<M10.get_grid_step()<<std::endl;
 std::cout<<"M10 = \n"<<M10<<std::endl;


 slip::RegularVector2dField2d<double> M11(2,3,slip::Point2d<double>(0.2,0.4),slip::Point2d<double>(1.1,1.2),3.3);
 std::cout<<"M11 = \n"<<M11<<std::endl;
  
 slip::RegularVector2dField2d<double> M12(2,3,slip::Point2d<double>(0.2,0.4),slip::Point2d<double>(1.1,1.2),slip::Vector2d<double>(2.2,4.4));
 std::cout<<"M12 = \n"<<M12<<std::endl;

 slip::RegularVector2dField2d<double> M13(2,3,slip::Point2d<double>(0.2,0.4),slip::Point2d<double>(1.1,1.2),d);
 std::cout<<"M13 = \n"<<M13<<std::endl;
 
 slip::RegularVector2dField2d<double> M14(2,3,slip::Point2d<double>(0.2,0.4),slip::Point2d<double>(1.1,1.2),d,d+6);
 std::cout<<"M14 = \n"<<M14<<std::endl;
 slip::RegularVector2dField2d<double> M15(4,3,slip::Point2d<double>(0.2,0.4),slip::Point2d<double>(1.1,1.2),Mat1.begin(),Mat1.end(),Mat2.begin());
 std::cout<<"M15 = \n"<<M15<<std::endl;


slip::RegularVector2dField2d<int,float> MMM(4,3,slip::Point2d<float>(0.2f,0.4f), slip::Point2d<float>(1.1f,1.2f),slip::Vector2d<int>(2,4));

 //------------------------------

 //------------------------------
 //resize
 //------------------------------ 
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "resize " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  M11.resize(4,3,slip::Vector2d<double>(2.2,4.4));
  std::cout<<"dim1 = "<<M11.dim1()<<" dim2 = "<<M11.dim2()<<" size = "<<M11.size()<<" empty = "<<M11.empty()<<std::endl; 
  std::cout<<M11<<std::endl; 
  //------------------------------


  //------------------------------
  //Affectation
  //------------------------------
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "assignment " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::RegularVector2dField2d<double> M7;
  M7 = M11;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M7 = M7;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M4 = 12.2;
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl;   

  M4 = slip::Vector2d<double> (128.0,255.0);
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl;   
  //------------------------------

  
  //------------------------------
  //Element access operators
  //------------------------------
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "element access operators " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   
   M2(2,1) = 2.1;
   M2[1][2] = 1.2;
  
  for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2[i][j]<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
   for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2(i,j)<<" ";
	}
      std::cout<<std::endl;
    }

    slip::Matrix<double> Mat3(4,3);
    slip::Matrix<double> Mat4(4,3);
    slip::iota(Mat3.begin(),Mat3.end(),1.0,1.0);
    slip::iota(Mat4.begin(),Mat4.end(),2.0,2.0);
    

   slip::RegularVector2dField2d<double> M16(4,3,slip::Point2d<double>(0.2,0.4),slip::Point2d<double>(1.1,1.2),Mat3.begin(),Mat3.end(),Mat4.begin());
 std::cout<<"M16 = \n"<<M16<<std::endl;
   for(size_t i = 0; i < M16.dim1(); ++i)
    {
      for(size_t j = 0; j < M16.dim2(); ++j)
	{
	  //	  std::cout<<M16.Vx1(i,j)<<" ";
	  std::cout<<M16.u(i,j)<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;

    for(size_t i = 0; i < M16.dim1(); ++i)
    {
      for(size_t j = 0; j < M16.dim2(); ++j)
	{
	  //	  std::cout<<M16.Vx2(i,j)<<" ";
	  std::cout<<M16.v(i,j)<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;

   for(size_t i = 0; i < M16.dim1(); ++i)
    {
      for(size_t j = 0; j < M16.dim2(); ++j)
	{
	  //std::cout<<M16.x1(i,j)<<" ";
	   std::cout<<M16.x(i,j)<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;

    for(size_t i = 0; i < M16.dim1(); ++i)
    {
      for(size_t j = 0; j < M16.dim2(); ++j)
	{
	  //std::cout<<M16.x2(i,j)<<" ";
	  std::cout<<M16.y(i,j)<<" ";
	  
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
  //------------------------------

   //------------------------------
   //swap
   //------------------------------
   M2.swap(M3);
   std::cout<<M2<<std::endl;
   std::cout<<std::endl;
   std::cout<<M3<<std::endl;
   //------------------------------


   //----------------------------
   //iterators
   //----------------------------
   slip::RegularVector2dField2d<double> M8(2,4);
   std::vector<slip::Vector2d<double> > V8(8);
   for(std::size_t i = 0; i < 8; ++i)
     V8[i] = slip::Vector2d<double>(i,i);
    
   M8.fill(V8.begin(),V8.end());   
   std::cout<<"M8 = \n"<<M8<<std::endl;

   std::copy(M8.begin(),M8.end(),std::ostream_iterator<slip::Vector2d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M8.rbegin(),M8.rend(),std::ostream_iterator<slip::Vector2d<double> >(std::cout," "));
   std::cout<<std::endl<<std::endl;
   
   
//    // std::transform(M3.rbegin(),M3.rend(),M8.begin(),M2.begin(),std::plus<double>());
  
//    std::cout<<M2<<std::endl;
   

   std::copy(M8.row_begin(1),M8.row_end(1),std::ostream_iterator<slip::Vector2d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;

   std::copy(M8.col_begin(1),M8.col_end(1),std::ostream_iterator<slip::Vector2d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;
   
   std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_begin(2),std::plus<slip::Vector2d<double> >());
   std::cout<<M2<<std::endl;

   std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_begin(2),std::plus<slip::Vector2d<double> >());
   std::cout<<M2<<std::endl;


   std::copy(M8.row_rbegin(1),M8.row_rend(1),std::ostream_iterator<slip::Vector2d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M8.col_rbegin(1),M8.col_rend(1),std::ostream_iterator<slip::Vector2d<double> >(std::cout," "));
   std::cout<<std::endl;
   

   std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_rbegin(2),std::plus<slip::Vector2d<double> >());
   std::cout<<M2<<std::endl;

   std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_rbegin(2),std::plus<slip::Vector2d<double> >());
   std::cout<<M2<<std::endl;
   
   
     slip::RegularVector2dField2d<int> M100(10,12);
     for(std::size_t i = 0; i < M100.dim1(); ++i)
       {
	 for(std::size_t j = 0; j < M100.dim2(); ++j)
	   {
	     M100[i][j][0] = i + j;
	     M100[i][j][1] = i + j + 1;
	   }
       }
     std::cout<<M100<<std::endl;
   
     slip::Box2d<int> box4(1,1,2,2);
     slip::Box2d<int> box5(0,0,1,1);
//      std::cout<<std::inner_product(M100.upper_left(box4),M100.bottom_right(box4),M100.upper_left(box5),0.0)<<std::endl;
     
     slip::RegularVector2dField2d<int>::iterator2d it  = M100.upper_left(box4);
     slip::RegularVector2dField2d<int>::iterator2d it2 = M100.upper_left(box5);
     
     for(;it != M100.bottom_right(box4); ++it, ++it2)
       {
	 std::cout<<"("<<*it<<" "<<*it2<<") ";
       }
     std::cout<<std::endl;
  

     //first plane
     std::cout<<"first plane "<<std::endl;
     std::copy(M100.begin(0),M100.end(0),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //second plane
     std::cout<<"second plane "<<std::endl;
     std::copy(M100.begin(1),M100.end(1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //third plane
     std::cout<<"third plane "<<std::endl;
     std::copy(M100.begin(2),M100.end(2),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     
          std::cout<<"first plane, row 1 "<<std::endl;
     std::copy(M100.row_begin(0,1),M100.row_end(0,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<"first plane, reverse row 1 "<<std::endl;
     std::copy(M100.row_rbegin(0,1),M100.row_rend(0,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;

   //    std::cout<<"first plane, col 1 "<<std::endl;
//       std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
     
//      std::cout<<"first plane, reverse col 1 "<<std::endl;
//      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));

  std::cout<<"second plane, row 1 "<<std::endl;
     std::copy(M100.row_begin(1,1),M100.row_end(1,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<"second plane, reverse row 1 "<<std::endl;
     std::copy(M100.row_rbegin(1,1),M100.row_rend(1,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;

    //  std::cout<<"first plane, col 1 "<<std::endl;
//      std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
     
//      std::cout<<"first plane, reverse col 1 "<<std::endl;
//      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));


     std::cout<<"third plane, row 1 "<<std::endl;
     std::copy(M100.row_begin(2,1),M100.row_end(2,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<"third plane, reverse row 1 "<<std::endl;
     std::copy(M100.row_rbegin(2,1),M100.row_rend(2,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;

    //  std::cout<<"first plane, col 1 "<<std::endl;
//      std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
     
//      std::cout<<"first plane, reverse col 1 "<<std::endl;
//      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));


  //----------------------------  

   //------------------------------
  //Comparison operators
  //------------------------------
  std::cout<<" M3 == M2 "<<(M3 == M2)<<std::endl;
  std::cout<<" M3 != M2 "<<(M3 != M2)<<std::endl;
//   std::cout<<" M3 > M2 "<<(M3 > M2)<<std::endl;
//   std::cout<<" M3 < M2 "<<(M3 < M2)<<std::endl;
//   std::cout<<" M3 >= M2 "<<(M3 >= M2)<<std::endl;
//   std::cout<<" M3 <= M2 "<<(M3 <= M2)<<std::endl;
  //------------------------------


  //------------------------------
  //fill methods
  //------------------------------
  
  M3.fill(4.0);
  std::cout<<M3<<std::endl;
  M3.fill(slip::Vector2d<double>(2.0,3.0));
  std::cout<<M3<<std::endl;
  double d2[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0};
  M3.fill(d2);
  std::cout<<M3<<std::endl;
  std::cout<<std::endl;
  std::vector<slip::Vector2d<double> > V2(18);
  for(std::size_t i = 0; i < 18; ++i)
    V2[i] = slip::Vector2d<double>(i,i);
    
  M3.fill(V2.begin(),V2.end());
  std::cout<<M3<<std::endl;

  slip::Matrix<double> mat(2,3);

  //------------------------------
  

 


   //------------------------------
   // Artithmetic an mathematical operators
   //------------------------------   
  std::cout<<"Test arithmetic and mathematical operators..."<<std::endl;
  slip::RegularVector2dField2d<float> Mat(4,3,2.0);
       
  slip::RegularVector2dField2d<float> MM(3,2);
  MM = 5.5;
  std::cout<<MM<<std::endl;
   
  slip::RegularVector2dField2d<float> MM2(3,2);
  MM2 = 2.2;
  std::cout<<MM2<<std::endl;
   
  MM += MM2;
  std::cout<<MM<<std::endl;
   
   MM -= MM2;
   std::cout<<MM<<std::endl;
   
   MM *= MM2;
   std::cout<<MM<<std::endl;
   
   MM /= MM2;
   std::cout<<MM<<std::endl;
   
   slip::RegularVector2dField2d<float> MM3(3,2);
   MM3 = MM + MM2;
   std::cout<<MM<<"+"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM - MM2;
   std::cout<<MM<<"-"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM * MM2;
   std::cout<<MM<<"*"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM / MM2;
   std::cout<<MM<<"/"<<MM2<<" = "<<MM3<<std::endl;
  
   std::cout<<"-MM3 = "<<-MM3<<std::endl;
   

 

   std::cout<<"M2 "<<std::endl;
   std::cout<<MM2<<std::endl;
   MM2+=1.1;
   std::cout<<MM2<<std::endl;
   MM2-=1.1;
   std::cout<<MM2<<std::endl;
   MM2*=1.1;
   std::cout<<MM2<<std::endl;
   MM2/=1.1;
   std::cout<<MM2<<std::endl;

   slip::Vector2d<float> vvv(4.4,4.4);
   std::cout<<"M2 "<<std::endl;
   std::cout<<MM2<<std::endl;
   MM2+=vvv;
   std::cout<<MM2<<std::endl;
   MM2-=vvv;
   std::cout<<MM2<<std::endl;
   MM2*=vvv;
   std::cout<<MM2<<std::endl;
   MM2/=vvv;
   std::cout<<MM2<<std::endl;
   
  std::cout<<" mins(M100) = "<<M100.min()<<std::endl;
  std::cout<<" maxs(M100) = "<<M100.max()<<std::endl;

   

   slip::RegularVector2dField2d<double> MM5(3,4);
   slip::Vector2d<double> vv(2.0,2.0);
   MM5 = M4 + M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5+vv<<std::endl;
   std::cout<<(vv+MM5)<<std::endl;


   MM5 = M4 - M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5-vv<<std::endl;
   std::cout<<(vv-MM5)<<std::endl;
 

   MM5 = M4 * M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5*vv<<std::endl;
   std::cout<<(vv*MM5)<<std::endl;
 

   MM5 = M4 / M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5/vv<<std::endl;
    

   
   std::cout<<MM5<<std::endl;
   std::cout<<MM5+2.0<<std::endl;
   std::cout<<(2.0+MM5)<<std::endl;

   std::cout<<MM5<<std::endl;
   std::cout<<MM5-2.0<<std::endl;
   std::cout<<(2.0-MM5)<<std::endl;
   
   std::cout<<MM5<<std::endl;
   std::cout<<MM5*2.0<<std::endl;
   std::cout<<(2.0*MM5)<<std::endl;
     
   std::cout<<MM5<<std::endl;
   std::cout<<MM5/2.0<<std::endl;
   //std::cout<<(2.0/MM5)<<std::endl;
 
   //M4.apply(std::sqrt);
   //std::cout<<M4<<std::endl;

   slip::Matrix<double> Norm(2,3);
   std::transform(MM5.begin(),MM5.end(),Norm.begin(),std::mem_fun_ref(&slip::Vector2d<double>::Euclidean_norm));
   std::cout<<"Norm ;"<<std::endl;
   std::cout<<Norm<<std::endl;

   MM5.norm(Norm);
   std::cout<<"Norm ;"<<std::endl;
   std::cout<<Norm<<std::endl;
   slip::Matrix<double> Angle(2,3);
   MM5.angle(Angle);
   std::cout<<"Angle ;"<<std::endl;
   std::cout<<Angle<<std::endl;
 //------------------------------
   double val[]={0.125,0.125,0.25,0.25,0.5,0.5,0.75,0.75,1.0,1.0,1.125,1.125};
   slip::RegularVector2dField2d<double> Test(2,3,val);   
   Test.write_gnuplot("test.gnu");

   Test.write_tecplot("test.tecplot","titre","zone");

   // Test Matrix of 2d Vectors
  std::size_t ni = 64;
  std::size_t nj = 64;

  slip::Vector<double> X(nj,0.0);
  slip::Vector<double> Y(ni,0.0);
  //  slip::Matrix< slip::Vector<double> > V(X.size(),Y.size());
  slip::RegularVector2dField2d<double> V(X.size(),Y.size());
  //slip::Matrix< slip::Vector<double> > DV(X.size(),Y.size());
  for(std::size_t j=0; j < nj; ++j)
    X(j)=j/double(nj-1)-0.5;

  std::cout << "Vector X:" << std::endl;
  std::cout << "No of Elements: " << X.size() << std::endl;
  std::cout << X << std::endl;

  for(std::size_t i=0; i < ni; ++i)
    Y(i)=i/double(ni-1)-0.5;

  std::cout << "Vector Y:" << std::endl;
  std::cout << "No of Elements: " << Y.size() << std::endl;
  std::cout << Y << std::endl;

  slip::Array2d<double> R(ni,nj,0.0);
  for(std::size_t i=0; i<ni ; ++i)
    for(std::size_t j=0; j<nj ; ++j)
      R(i,j)=sqrt(X(j)*X(j)+Y(i)*Y(i));
 


  std::cout << "Matrix R:" << std::endl;
  std::cout << "No of Lines: " << R.rows() << ", No of Columns:" << R.columns() << std::endl;
  std::cout << R << std::endl;
  
  std::cout << "Matrix V:" << std::endl;
  std::cout << "No of Lines: " << V.rows() << ", No of Columns:" << V.columns() << std::endl;  


  for(std::size_t i=0; i<nj ; ++i)
  {
    for(std::size_t j=0; j<ni; ++j)
    {
      //V(i,j).set_x(-Y(j));
      //V(i,j).set_y(X(i));
      double r = R(i,j);
      V(i,j)[0]=(-Y(j)/r/r)*(1-exp(-r*r*25));
      V(i,j)[1]=(X(i)/r/r)*(1-exp(-r*r*25));
      //V(i,j).set(-Y(j)/R(i,j)/R(i,j)/R(i,j),X(i)/R(i,j)/R(i,j)/R(i,j));
      //V(i,j).set(Y(j),0.0);
    }

  }
  slip::RegularVector2dField2d<double> Vxy(Y.size(),X.size());
  for(std::size_t i=0; i<nj ; ++i)
    {
      for(std::size_t j=0; j<ni; ++j)
	{
	  
	  Vxy[j][i] =  V[i][nj-1-j];
	}
    }
  //V.write_tecplot("V.gnu","toto","titi");
  V.write_gnuplot("V.gnu");
  Vxy.write_gnuplot("Vxy.gnu");
  Vxy.write_tecplot("Vxy.tec","","");

  slip::Array2d<double> Matrice(6,7,0.0);
   for(std::size_t i=0; i < Matrice.rows(); ++i)
     for(std::size_t j=0; j < Matrice.cols(); ++j)
       Matrice(i,j)=double(i+Matrice.rows()*j);
   
   //copy M twice : in the first plane and in the second plane of VFM
   slip::RegularVector2dField2d<double> VFM(6,7,Matrice.begin(),Matrice.end(),Matrice.begin());
   VFM.set_grid_step(slip::Point2d<double>(0.5,0.75));
   slip::Matrix<double> Result(6,7,0.0);
 std::cout<<std::endl;
 std::cout<<"-------------------------------------------"<<std::endl;
 std::cout<<"-- GENERIC DERIVATION + BORDER TREATMENT --"<<std::endl;
 std::cout<<"-------------------------------------------"<<std::endl;
 /**
  ** prototype
  ** void derivative(const TT& Cin, std::size_t der_dir, std::size_t der_ord, std::size_t sch_ord, std::size_t sch_shift, TT& Dout)
  **/
 std::size_t plane = 0;
 std::cout<<"Matrice="<<std::endl;
 std::cout<<Matrice<<std::endl;
 std::cout<<"-- direction X, ordre 1 (der), ordre 1 (sch), shift 1 --"<<std::endl;
 VFM.derivative(plane,slip::X_DIRECTION,1,1,Result);
 std::cout<<"Result="<<std::endl;
 std::cout<<Result<<std::endl;
 std::cout<<"-- direction X, ordre 1 (der), ordre 2 (sch), shift 1 --"<<std::endl;
 VFM.derivative(plane,slip::X_DIRECTION,1,2,Result);
 std::cout<<"Result="<<std::endl;
 std::cout<<Result<<std::endl;
  std::cout<<"-- direction Y, ordre 1 (der), ordre 1 (sch), shift 1 --"<<std::endl;
  VFM.derivative(plane,slip::Y_DIRECTION,1,1,Result);
 std::cout<<"Result="<<std::endl;
 std::cout<<Result<<std::endl;
 std::cout<<"-- direction Y, ordre 1 (der), ordre 2 (sch), shift 1 --"<<std::endl;
 VFM.derivative(plane,slip::Y_DIRECTION,1,2,Result);
 std::cout<<"Result="<<std::endl;
 std::cout<<Result<<std::endl;
 std::cout<<"-- direction Y, ordre 1 (der), ordre 3 (sch), shift 1 --"<<std::endl;
 VFM.derivative(plane,slip::Y_DIRECTION,1,3,Result);
 std::cout<<"Result="<<std::endl;
 std::cout<<Result<<std::endl;

 std::cout<<"-- direction Y, ordre 1 (der), ordre 4 (sch), shift 1 --"<<std::endl;
 VFM.derivative(plane,slip::Y_DIRECTION,1,4,Result);
 std::cout<<"Result="<<std::endl;
 std::cout<<Result<<std::endl;

 std::cout<<std::endl;
 std::cout<<"--------------------------------------------"<<std::endl;
 std::cout<<"--  div2d, vorticity2d & lambda22d using  --"<<std::endl;
 std::cout<<"-- GENERIC DERIVATION  + BORDER TREATMENT --"<<std::endl;
 std::cout<<"--------------------------------------------"<<std::endl;
 std::cout<<std::endl;

 std::cout<<"tested on a synthetic Oseen vortex"<<std::endl<<std::endl;
 slip::Matrix<double> omega(nj,ni);
 slip::Matrix<double> l2(nj,ni);
 slip::Matrix<double> div(nj,ni);
 Vxy.vorticity(4,omega);
 Vxy.lambda2(4,l2);
 Vxy.divergence(4,div);
 // export data files
  std::ofstream omdat("omtest.dat",std::ios::out);
  omdat<<omega;
  omdat.close();
  
  std::ofstream l2dat("l2test.dat",std::ios::out);
  l2dat<<l2;
  l2dat.close();

  std::ofstream divdat("divtest.dat",std::ios::out);
  divdat<<div;
  divdat.close();
//  // export gnuplot command files
//   std::ofstream omgnuplot("omtest.gnuplot",std::ios::out);
//   omgnuplot<<"set term X11 0 enhanced;set mouse; set pm3d at s;unset surface; set zlabel \"{/Symbol W}\";set xlabel \"x\";set ylabel \"y\";splot \"omtest.dat\" matrix;"<<"\n";
//   omgnuplot.close();

//   std::ofstream l2gnuplot("l2test.gnuplot",std::ios::out);
//   l2gnuplot<<"set term X11 0 enhanced;set mouse; set pm3d at s;unset surface; set zlabel \"{/Symbol l}_2\";set xlabel \"x\";set ylabel \"y\";splot \"l2test.dat\" matrix;"<<"\n";
//   l2gnuplot.close();
  
//   std::ofstream divgnuplot("divtest.gnuplot",std::ios::out);
//   divgnuplot<<"set term X11 0 enhanced;set mouse; set pm3d at s;unset surface; set zlabel \"div\";set xlabel \"x\";set ylabel \"y\";splot \"divtest.dat\" matrix;"<<"\n";
//   divgnuplot.close();

//   // call gnuplot
//   std::stringstream cmd;
//   cmd<<"gnuplot -persist omtest.gnuplot && gnuplot -persist
//   l2test.gnuplot && gnuplot -persist divtest.gnuplot"<<std::endl;
//   std::cout<<cmd.str()<<std::endl;
//   system(cmd.str().c_str());

//   //system("gnuplot \"test.gnuplot\"");
//   //system("ls -al");

//   std::cout<<"if nothing happens, try this:"<<std::endl<<std::endl;
//  std::cout<<cmd.str().c_str()<<std::endl<<std::endl;
//   std::cout<<"--------------------------------------------"<<std::endl;

 //
 //serialization
 //
  // create and open a character archive for output
    std::ofstream ofs("rvectorfield2d.txt");
    slip::Point2d<float> inits(0.5f,1.0f);
    slip::Point2d<float> steps(1.0f,2.0f);
    
    slip::RegularVector2dField2d<float,float> b(3,4,inits,steps);
    slip::iota(b.begin(),b.end(),slip::Vector2d<float>(1.0f,2.0f),
	       slip::Vector2d<float>(1.0f,1.0f));
    
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<b;
    }//to close archive
    std::ifstream ifs("rvectorfield2d.txt");
    slip::RegularVector2dField2d<float,float> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("rvectorfield2d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<b;
    }//to close archive

    std::ifstream ifsb("rvectorfield2d.bin");
    slip::RegularVector2dField2d<float,float> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;
  
  return 0;


}
