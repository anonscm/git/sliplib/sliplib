//#include "svg_shapes.hpp"
#include "svg_image_descriptor_types.hpp"
#include "Point2d.hpp"
#include "Color.hpp"

int main()
{

  slip::SVGImageDescriptorType<float> svg_image;
  svg_image.SetDimensions(1024, 768);
  svg_image.AddCircle(slip::Point2d<float>(600.f, 400.f),
		      100.f);
  svg_image.AddLine(slip::Point2d<float>(10.f, 500.f),
		    slip::Point2d<float>(800.f, 40.f),
		    1.0,
		    slip::Color<unsigned char>(255,0,0));
  svg_image.AddEllipse(slip::Point2d<float>(600.f, 400.f),
		       50.0f,
		       30.f,
		       20.0f,
		       slip::Color<unsigned char>(0,255,0));
  svg_image.AddAxisAlignedRectangle(slip::Point2d<float>(10.f, 100.f),
				    slip::Point2d<float>(20.f, 150.f),
				    slip::Color<unsigned char>(0,0,255)); 
  svg_image.Write("test.svg") ;


 return 0;
}
