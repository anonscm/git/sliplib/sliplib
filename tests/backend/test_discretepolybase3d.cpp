#include <iostream>
#include <iomanip>
#include <vector>
#include <map>
#include "DiscretePolyBase3d.hpp"
#include "Polynomial.hpp"
#include "Array3d.hpp"
#include "polynomial_algo.hpp"
#include "arithmetic_op.hpp"
#include "Array3d.hpp"
#include "DenseVector3dField3d.hpp"

#include "Block.hpp"


int main()
{
  
   typedef double T;
   

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "base construction " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::size_t slice_range = 3;
  std::size_t slice_order = 2;
  std::size_t row_range = 4;
  std::size_t row_order = 3;
  std::size_t col_range = 5;
  std::size_t col_order = 4;

  slip::ChebyshevWeight<T> cheb_weight_fun;
  slip::LegendreWeight<T> legendre_weight_fun;
  slip::UniformCollocations<T> uniform_collocation_fun;
  slip::ChebyshevCollocations<T> cheb_collocation_fun;
  slip::DiscretePolyBase3d<T,
                               slip::LegendreWeight<T>,
                               slip::UniformCollocations<T>,
                               slip::LegendreWeight<T>,
                               slip::UniformCollocations<T>,
                               slip::LegendreWeight<T>,
                               slip::UniformCollocations<T> > 
                               base_legendre(slice_range,
  					     row_range,
  					     col_range,
  					     slice_order,
  					     legendre_weight_fun,
  					     uniform_collocation_fun,
  					     row_order,
  					     legendre_weight_fun, 
  					     uniform_collocation_fun,
  					     col_order,
  					     legendre_weight_fun, 
  					     uniform_collocation_fun,
  					     false);




    base_legendre.generate();
    base_legendre.print();


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "inner product" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  std::cout<<"(P(0,0),P(0,0)) = "<<base_legendre.inner_product(0,0)<<std::endl;
  std::cout<<"(P(0,1),P(0,0)) = "<<base_legendre.inner_product(1,0)<<std::endl;
  std::cout<<"(P(0,1),P(0,1)) = "<<base_legendre.inner_product(1,1)<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Gram matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array2d<T> Gram;
  base_legendre.orthogonality_matrix(Gram);
  std::cout<<"Gram = \n"<<Gram<<std::endl;
  std::cout<<"base_legendre.is_orthogonal(1e-13) "<<base_legendre.is_orthogonal(1e-13)<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "projection of data onto the base" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Array3d<T> A(slice_range,row_range,col_range);
  slip::iota(A.begin(),A.end(),1.0,1.0);
  std::cout<<"A = \n"<<A<<std::endl;
  
  slip::Array<T> coef(base_legendre.size());

  base_legendre.project(A.front_upper_left(),A.back_bottom_right(),
  			slice_order,row_order,col_order,
  			coef.begin(),coef.end());
  
   
  
   std::cout<<"coef = \n"<<coef<<std::endl;
 
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "projection of data onto a polynomial" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

for(std::size_t k = 0; k <= slice_order; ++k)
  {
    for(std::size_t i = 0; i <= row_order; ++i)
      {
	for(std::size_t j = 0; j <= col_order; ++j)
	  {
	    
	    std::cout<< base_legendre.project(A.front_upper_left(),A.back_bottom_right(),k,i,j)<<" ";
	  }
	std::cout<<std::endl;
      }
    std::cout<<std::endl;
    std::cout<<std::endl;
  }
 std::cout<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "reconstruct data " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Array3d<T> Ar(slice_range,row_range,col_range);
   
   base_legendre.reconstruct(coef.begin(),coef.end(),
			     coef.size(),
			     Ar.front_upper_left(),
			     Ar.back_bottom_right());
   std::cout<<"Ar = \n"<<Ar<<std::endl;







//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "base construction 2" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   slip::DLegendreBase3d_d  base_legendre2(slice_range,
//  					      row_range,
//  					      col_range,
//  					      slice_order,
//  					      legendre_weight_fun,
//  					      uniform_collocation_fun,
//  					      legendre_weight_fun, 
//  					      uniform_collocation_fun,
//  					      legendre_weight_fun, 
//  					      uniform_collocation_fun,
//  					      true);
//   base_legendre2.generate();
//   base_legendre2.print();


//  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "inner product" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
//   std::cout<<"(P(0,0),P(0,0)) = "<<base_legendre2.inner_product(0,0)<<std::endl;
//   std::cout<<"(P(0,1),P(0,0)) = "<<base_legendre2.inner_product(1,0)<<std::endl;
//   std::cout<<"(P(0,1),P(0,1)) = "<<base_legendre2.inner_product(1,1)<<std::endl;

//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "Gram matrix" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   slip::Array2d<T> Gram;
//   base_legendre2.orthogonality_matrix(Gram);
//   std::cout<<"Gram = \n"<<Gram<<std::endl;
//   std::cout<<"base_legendre2.is_orthogonal(1e-13) "<<base_legendre2.is_orthogonal(1e-13)<<std::endl;


//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "projection of data onto the base" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
//   slip::Array3d<T> A(slice_range,row_range,col_range);
//   slip::iota(A.begin(),A.end(),1.0,1.0);
//   std::cout<<"A = \n"<<A<<std::endl;
  
//   slip::Array<T> coef(base_legendre2.size());

//   base_legendre2.project(A.front_upper_left(),A.back_bottom_right(),
//   			slice_order,slice_order,slice_order,
//   			coef.begin(),coef.end());
  
   
  
//    std::cout<<"coef = \n"<<coef<<std::endl;
 
//    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//    std::cout << "projection of data onto a polynomial" << std::endl;
//    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

// for(std::size_t k = 0; k <= slice_order; ++k)
//   {
//     for(std::size_t i = 0; i <= slice_order; ++i)
//       {
// 	for(std::size_t j = 0; j <= slice_order && ((i+j+k) <= slice_order); ++j)
// 	  {
	    
// 	    std::cout<< base_legendre2.project(A.front_upper_left(),A.back_bottom_right(),k,i,j)<<" ";
// 	  }
// 	std::cout<<std::endl;
//       }
//     std::cout<<std::endl;
//     std::cout<<std::endl;
//   }
//  std::cout<<std::endl;

//    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//    std::cout << "reconstruct data " << std::endl;
//    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//    slip::Array3d<T> Ar(slice_range,row_range,col_range);
   
//    base_legendre2.reconstruct(coef.begin(),coef.end(),
//    			     coef.size(),
//    			     Ar.front_upper_left(),
//    			     Ar.back_bottom_right());
//    std::cout<<"Ar = \n"<<Ar<<std::endl;
 


  //  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "base construction 3" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   slip::DiscretePolyBase3d<T,
//                                slip::LegendreWeight<T>,
//                                slip::UniformCollocations<T>,
//                                slip::LegendreWeight<T>,
//                                slip::UniformCollocations<T>,
//   			       slip::LegendreWeight<T>,
//                                slip::UniformCollocations<T> > 
//                                base_legendre3(slice_range,
//   					      row_range,
//   					      col_range,
//   					      slice_order,
//   					      legendre_weight_fun,
//   					      uniform_collocation_fun,
//   					      true);
//   base_legendre3.generate();
//   base_legendre3.print();






//  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "inner product" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
//   std::cout<<"(P(0,0),P(0,0)) = "<<base_legendre3.inner_product(0,0)<<std::endl;
//   std::cout<<"(P(0,1),P(0,0)) = "<<base_legendre3.inner_product(1,0)<<std::endl;
//   std::cout<<"(P(0,1),P(0,1)) = "<<base_legendre3.inner_product(1,1)<<std::endl;

//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "Gram matrix" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   slip::Array2d<T> Gram;
//   base_legendre3.orthogonality_matrix(Gram);
//   std::cout<<"Gram = \n"<<Gram<<std::endl;
//   std::cout<<"base_legendre3.is_orthogonal(1e-13) "<<base_legendre3.is_orthogonal(1e-13)<<std::endl;


//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "projection of data onto the base" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
//   slip::Array3d<T> A(slice_range,row_range,col_range);
//   slip::iota(A.begin(),A.end(),1.0,1.0);
//   std::cout<<"A = \n"<<A<<std::endl;
  
//   slip::Array<T> coef(base_legendre3.size());

//   base_legendre3.project(A.front_upper_left(),A.back_bottom_right(),
//   			slice_order,slice_order,slice_order,
//   			coef.begin(),coef.end());
  
   
  
//    std::cout<<"coef = \n"<<coef<<std::endl;
 
//    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//    std::cout << "projection of data onto a polynomial" << std::endl;
//    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

// for(std::size_t k = 0; k <= slice_order; ++k)
//   {
//     for(std::size_t i = 0; i <= slice_order; ++i)
//       {
// 	for(std::size_t j = 0; j <= slice_order && ((i+j+k) <= slice_order); ++j)
// 	  {
	    
// 	    std::cout<< base_legendre3.project(A.front_upper_left(),A.back_bottom_right(),k,i,j)<<" ";
// 	  }
// 	std::cout<<std::endl;
//       }
//     std::cout<<std::endl;
//     std::cout<<std::endl;
//   }
//  std::cout<<std::endl;

//    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//    std::cout << "reconstruct data " << std::endl;
//    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//    slip::Array3d<T> Ar(slice_range,row_range,col_range);
   
//    base_legendre3.reconstruct(coef.begin(),coef.end(),
//    			     coef.size(),
//    			     Ar.front_upper_left(),
//    			     Ar.back_bottom_right());
//    std::cout<<"Ar = \n"<<Ar<<std::endl;
 

//    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "base construction 4" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   slip::DiscretePolyBase3d<T,
//                                slip::LegendreWeight<T>,
//                                slip::UniformCollocations<T>,
//                                slip::LegendreWeight<T>,
//                                slip::UniformCollocations<T> > 
//     base_legendre4(base_legendre);
 
// //base_legendre4.print();


//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "base construction 5" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   slip::DiscretePolyBase3d<T,
//                                slip::LegendreWeight<T>,
//                                slip::UniformCollocations<T>,
//                                slip::LegendreWeight<T>,
//                                slip::UniformCollocations<T> > 
//     base_legendre5;
//   base_legendre5 = base_legendre;
// //base_legendre5.print();


 

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "init_canonical" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::DLegendreBase3d_d  base_legendre3;
  base_legendre3.init_canonical(slice_range,
				row_range,
				col_range,
				slice_order,
				legendre_weight_fun,
				uniform_collocation_fun,
				row_order,
				legendre_weight_fun,
				uniform_collocation_fun,
				col_order,
				legendre_weight_fun, 
				uniform_collocation_fun);

  base_legendre3.print();

  base_legendre3.gram_schmidt();
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "gram schmidt" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // base_legendre3.print();
  //base_legendre3.orthogonality_matrix(Gram);
  //std::cout<<"Gram = \n"<<Gram<<std::endl;

  

  return 0;
}
