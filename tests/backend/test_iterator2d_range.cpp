
#include<algorithm>
#include<iostream>
#include<iterator>
#include <numeric>
#include <complex>
#include "Array2d.hpp"

#include "Point2d.hpp"
#include "DPoint2d.hpp"
#include "Range.hpp"
#include "iterator2d_range.hpp"

int main()
{

  slip::Array2d<int> M(9,10);
  int k = 1;
  for(std::size_t i = 0; i < M.dim1(); ++i)
    {
      for(std::size_t j = 0; j < M.dim2(); ++j)
	{
	  M[i][j] = k++;
	}
    }
  
  std::cout<<M<<std::endl;
  
  // slip::Point2d<int> db(0,2);
  slip::Range<int> r1(1,5,2);
  std::cout<<r1<<std::endl;
  slip::Range<int> r2(0,3,1);
  std::cout<<r2<<std::endl;
  slip::DPoint2d<int> de(2,3);



  slip::iterator2d_range<slip::Array2d<int> > beg(&M,r1,r2);
  slip::iterator2d_range<slip::Array2d<int> > e = (beg + de);
  //  e++;
  std::cout<<*e<<std::endl;
  std::cout<<"e - beg = "<<(e-beg)<<std::endl;
  std::cout<<beg[de]<<std::endl<<std::endl;

  std::cout<<" beg == beg "<<(beg == beg)<<std::endl;
  std::cout<<" beg != beg "<<(beg != beg)<<std::endl;
  std::cout<<" beg == e "<<(beg == e)<<std::endl;
  std::cout<<" beg != e "<<(beg != e)<<std::endl;
  std::cout<<" beg < e "<<(beg < e)<<std::endl;
  std::cout<<" beg <= e "<<(beg <= e)<<std::endl;
  std::cout<<" beg > e "<<(beg > e)<<std::endl;
  std::cout<<" beg >= e "<<(beg >= e)<<std::endl;

  slip::iterator2d_range<slip::Array2d<int> > it = beg;
  for(it = beg; it != e; ++it)
    {
      std::cout<<" (i,j) = ("<<it.x1()<<","<<it.x2()<<") ";
    }
  std::cout<<std::endl;

  for(it = beg; it != e; ++it)
    {
      std::cout<<*it<<" ";
    }
  std::cout<<std::endl;
  std::cout<<std::endl;


  slip::Array2d<int> Ex(3,4);
  std::copy(beg,e,Ex.begin());
  std::cout<<Ex<<std::endl;
 

  std::cout<<"const upper_left - bottom_right "<<std::endl;
  std::copy(M.upper_left(r1,r2),M.bottom_right(r1,r2),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"const rupper_left - rbottom_right "<<std::endl;
  std::copy(M.rupper_left(r1,r2),M.rbottom_right(r1,r2),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;

  slip::Range<int> r12(0,4,2);
  std::cout<<r12<<std::endl;
  slip::Range<int> r22(5,8,1);
  std::cout<<r22<<std::endl;
  slip::Array2d<int> M2(9,10,0);
  std::cout<<"upper_left - bottom_right "<<std::endl;
  std::copy(M.upper_left(r1,r2),M.bottom_right(r1,r2),M2.upper_left(r12,r22));
  std::cout<<M2<<std::endl;
  std::cout<<"rupper_left - rbottom_right "<<std::endl;
  std::copy(M.upper_left(r1,r2),M.bottom_right(r1,r2),M2.rupper_left(r12,r22));
 
  std::cout<<M2<<std::endl;


  
 slip::Array2d<int>::iterator2d_range itc = M.upper_left(r1,r2);
  std::cout<<"rows of the range "<<std::endl; 
  for(std::size_t i = 0; i <= r1.iterations();++i)
    {
      std::copy(itc.row_begin(i),itc.row_end(i),std::ostream_iterator<int>(std::cout," "));
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
  std::cout<<"cols of the range "<<std::endl; 
  for(std::size_t j = 0; j <= r2.iterations();++j)
    {
      std::copy(itc.col_begin(j),itc.col_end(j),std::ostream_iterator<int>(std::cout," "));
      std::cout<<std::endl;
    }
  std::cout<<std::endl;


  slip::Range<int> all1(0,M.dim1()-1,3);
  std::cout<<all1<<std::endl;
  std::cout<<all1.iterations()<<std::endl;
  slip::Range<int> all2(0,M.dim2()-1,3);
  std::cout<<all2<<std::endl;
  std::cout<<all2.iterations()<<std::endl;
  std::cout<<M.bottom_right(all1,all2).i()<<" "<<M.bottom_right(all1,all2).j()<<std::endl;
  std::copy(M.upper_left(all1,all2),M.bottom_right(all1,all2),
	    std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;

  //  slip::Array2d<int>::reverse_iterator2d_range itrc = M.rupper_left(all1
  std::copy(M.rupper_left(all1,all2),M.rbottom_right(all1,all2),
	    std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
 
 
  return 0;
}

