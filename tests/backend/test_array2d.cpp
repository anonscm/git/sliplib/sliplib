#include "Array2d.hpp"
#include "Point2d.hpp"
#include "DPoint2d.hpp"
#include "Box2d.hpp"
#include "iterator2d_box.hpp"
#include "arithmetic_op.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <complex>
#include <algorithm>
#include <numeric>
#include <vector>
//#include <boost/mem_fn.hpp>
//#include <cmath>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


float function2(float a, float b)
{
  return a * b;
}

template<typename T>
struct pixelRGB
{
  T red;
  T green;
  T blue;

  T get_red()const {return red;}
  T get_green()const {return green;}
  T get_blue()const {return blue;}
  void set_red(const T& r){ red = r;}
  void set_green(const T& g){ green = g;}
  void set_blue(const T& b){ blue = b;}
};


//Fonction de conversion RGB en Luminance
struct RGBToY
{
  unsigned char operator()(const pixelRGB<double> & rgb) const
  {
    return (unsigned char)(0.3 * rgb.red + 0.59 * rgb.green + 0.11 * rgb.blue);
  }
};

 
class A
{
public :
  A():
  n_(0),data_(0){}
  A(const std::size_t n):
    n_(n),data_(new double[n])
  {
    for(size_t i = 0; i < n_; ++i)
      data_[i] = i;
  }

  A(const A& rhs):
    n_(rhs.n_)
  {
    data_ = new double[rhs.n_];
    std::copy(&(rhs.data_[0]), &(rhs.data_[0]) + rhs.n_,&(data_[0]));
  }
  
  A& operator=(const A& rhs)
  {
    if(this != &rhs)
      {
	if(data_ != 0)
	  {
	    delete[] data_;
	  }
	n_ = rhs.n_;
	data_ = new double[rhs.n_];
	std::copy(&(rhs.data_[0]), &(rhs.data_[0]) + rhs.n_,&(data_[0]));
      }
    return *this;
  }
  ~A()
  {
    if(data_ != 0)
      {
	delete[] data_;
      } 
  }
  
  //Utile ici ?, il vaut mieux le mettre
  void swap(A& x)
  {
    for(size_t n = 0; n < n_; ++n)
      std::swap(data_[n],x.data_[n]);
  }
  void print()
  {
    std::cout<<"[ ";
    for(size_t i = 0; i < n_; ++i)
      std::cout<<data_[i]<<" ";
    std::cout<<"] ";
  }
  private :
  std::size_t n_;
  double* data_;

};



int main()
{

  //------------------------------
  //constructors
  //------------------------------
  slip::Array2d<double> M;
  std::cout<<"name = "<<M.name()<<" dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 

 
  slip::Array2d<double> M2(6,3);
  std::cout<<"dim1 = "<<M2.dim1()<<" dim2 = "<<M2.dim2()<<" size = "<<M2.size()<<" empty = "<<M2.empty()<<std::endl; 
  std::cout<<"rows = "<<M2.rows()<<" cols = "<<M2.cols()<<" columns = "<<M2.columns()<<std::endl;
  std::cout<<M2<<std::endl; 
 
  slip::Array2d<double> M3(6,3,7.1);
  std::cout<<"dim1 = "<<M2.dim1()<<" dim2 = "<<M2.dim2()<<" size = "<<M2.size()<<" empty = "<<M2.empty()<<std::endl; 
  std::cout<<"rows = "<<M3.rows()<<" cols = "<<M3.cols()<<" columns = "<<M3.columns()<<std::endl;
  std::cout<<M3<<std::endl;


 double d[] = {1.2,3.4,5.5,6.6,7.7,8.8};
 slip::Array2d<double> M4(2,3,d);
 std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
 std::cout<<M4<<std::endl; 
 
 

  std::vector<int> V(20);
  for(std::size_t i = 0; i < 20; ++i)
    V[i] = i;
  slip::Array2d<int> M5(4,5,V.begin(),V.end());
  std::cout<<"dim1 = "<<M5.dim1()<<" dim2 = "<<M5.dim2()<<" size = "<<M5.size()<<" empty = "<<M5.empty()<<std::endl; 
 std::cout<<M5<<std::endl; 


 slip::Array2d<double> M6 = M4;
  std::cout<<"dim1 = "<<M6.dim1()<<" dim2 = "<<M6.dim2()<<" size = "<<M6.size()<<" empty = "<<M6.empty()<<std::endl; 
 std::cout<<M6<<std::endl; 


 //------------------------------

 //------------------------------
 //resize
 //------------------------------
  M.resize(4,3,5.6);
  std::cout<<"dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 
//------------------------------


  //------------------------------
  //Affectation
  //------------------------------
  slip::Array2d<double> M7;
  M7 = M;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M7 = M7;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M4 = 12.2;
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl;   
  //------------------------------

  
  //------------------------------
  //Element access operators
  //------------------------------
  M2(2,1) = 2.1;
  M2[1][2] = 1.2;
  
  for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2[i][j]<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
   for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2(i,j)<<" ";
	}
      std::cout<<std::endl;
    }
   std::cout<<std::endl;

   slip::Array2d<double> Arange(10,9);
   double val = 0.0;
    for(size_t i = 0; i < Arange.dim1(); ++i)
    {
      for(size_t j = 0; j < Arange.dim2(); ++j)
	{
	  Arange(slip::Point2d<std::size_t>(i,j)) = val;
	  val = val + 1.0;
	}
    }

    for(size_t i = 0; i < Arange.rows(); ++i)
    {
      for(size_t j = 0; j < Arange.cols(); ++j)
	{
	  std::cout<<Arange(slip::Point2d<std::size_t>(i,j))<<" ";
	}
      std::cout<<std::endl;
    }
    std::cout<<std::endl;
   
    slip::Range<int> range_r(0,Arange.dim2()-1,2);
    slip::Range<int> range_c(0,Arange.dim1()-1,2);
    slip::Array2d<double> Aext = Arange(range_r,range_c);
    std::cout<<Aext<<std::endl;

    
   //------------------------------

   //------------------------------
   //swap
   //------------------------------
   M2.swap(M3);
   std::cout<<M2<<std::endl;
   std::cout<<std::endl;
   std::cout<<M3<<std::endl;
   //------------------------------


   //----------------------------
   //iterators
   //----------------------------
   slip::Array2d<double> M8(6,3,4.4);
   std::copy(M3.begin(),M3.end(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(M3.rbegin(),M3.rend(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl<<std::endl;
   
   
   std::transform(M3.rbegin(),M3.rend(),M8.begin(),M2.begin(),std::plus<double>());
  
   std::cout<<M2<<std::endl;
   
   std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_begin(2),std::plus<double>());
   std::cout<<M2<<std::endl;

   std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_begin(2),std::plus<double>());
   std::cout<<M2<<std::endl;


   std::copy(M2.row_rbegin(1),M2.row_rend(1),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(M2.col_rbegin(1),M2.col_rend(1),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   

    std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_rbegin(2),std::plus<double>());
   std::cout<<M2<<std::endl;

   std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_rbegin(2),std::plus<double>());
   std::cout<<M2<<std::endl;
   
   std::partial_sum(M2.begin(),M2.end(),M2.begin());
   std::cout<<"iterator2d_box test..."<<std::endl;
   std::cout<<M2<<std::endl;
   std::cout<<"bottom_right() and upper_left() test "<<std::endl;
   std::copy(M2.upper_left(),M2.bottom_right(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(M2.rupper_left(),M2.rbottom_right(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;

   std::cout<<"bottom_right(box) and upper_left(box) box(2,1,4,2) test "<<std::endl;
   slip::Box2d<int> box(2,1,4,2);
   std::copy(M2.upper_left(box),M2.bottom_right(box),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(M2.rupper_left(box),M2.rbottom_right(box),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   
   slip::Array2d<double> Mask(3,2,0.0);
   double k = 1.0;
   for(std::size_t i = 0; i < Mask.dim1(); ++i)
     for(std::size_t j = 0; j < Mask.dim2(); ++j)
       {
	 Mask[i][j] = k;
	 k += 1.0;
       }
   std::copy(Mask.upper_left(),Mask.bottom_right(),
	     M2.upper_left(box));
   std::cout<<M2<<std::endl;
  
   std::copy(Mask.upper_left(),Mask.bottom_right(),
	     M2.rupper_left(box));
   std::cout<<M2<<std::endl;
   
   std::transform(M2.upper_left(box),M2.bottom_right(box),Mask.upper_left(),M2.upper_left(box),std::plus<double>());
   std::cout<<M2<<std::endl;
   
   std::transform(M2.upper_left(box),M2.bottom_right(box),Mask.rupper_left(),M2.upper_left(box),std::plus<double>());
   std::cout<<M2<<std::endl;
 
   slip::Array2d<int> Mrange(10,10);
   int ind = 0;
   for(std::size_t i = 0; i < Mrange.dim1(); ++i)
     for(std::size_t j = 0; j < Mrange.dim2(); ++j)
       {
	 Mrange[i][j] = ind;
	 ind += 1;
       }
   std::cout<<Mrange<<std::endl;
   std::cout<<"row_range_iterator ..."<<std::endl;
   //slip::Range<int> range(2,7,2);
   slip::Range<int> range(1,(Mrange.dim2() - 1),2);
   std::copy(Mrange.row_begin(3,range),Mrange.row_end(3,range),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(Mrange.row_rbegin(3,range),Mrange.row_rend(3,range),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;

   std::cout<<"col_range_iterator ..."<<std::endl;
   //slip::Range<int> range2(2,7,2);
   slip::Range<int> range2(0,(Mrange.dim1() - 1),2);
   std::copy(Mrange.col_begin(2,range2),Mrange.col_end(2,range2),std::ostream_iterator<double>(std::cout,"\n"));
   std::cout<<std::endl;
   std::copy(Mrange.col_rbegin(2,range2),Mrange.col_rend(2,range2),std::ostream_iterator<double>(std::cout,"\n"));
   std::cout<<std::endl;

   //std::copy(Mrange.row_begin(4,range),Mrange.row_end(4,range),Mrange.row_begin(8,range));

  std::copy(Mrange.col_begin(4,range2),Mrange.col_end(4,range2),Mrange.col_begin(2,range));

   std::cout<<Mrange<<std::endl;
   
   std::copy(Mrange.upper_left(range,range2),Mrange.bottom_right(range,range2),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
    std::copy(Mrange.rupper_left(range,range2),Mrange.rbottom_right(range,range2),std::ostream_iterator<double>(std::cout," "));
    std::cout<<std::endl;
   std::copy(Mrange.upper_left(range),Mrange.bottom_right(range),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
     std::copy(Mrange.rupper_left(range),Mrange.rbottom_right(range),std::ostream_iterator<double>(std::cout," "));
    std::cout<<std::endl;
   
  //----------------------------  

   //------------------------------
  //Comparison operators
  //------------------------------
  std::cout<<" M3 == M2 "<<(M3 == M2)<<std::endl;
  std::cout<<" M3 != M2 "<<(M3 != M2)<<std::endl;
  std::cout<<" M3 > M2 "<<(M3 > M2)<<std::endl;
  std::cout<<" M3 < M2 "<<(M3 < M2)<<std::endl;
  std::cout<<" M3 >= M2 "<<(M3 >= M2)<<std::endl;
  std::cout<<" M3 <= M2 "<<(M3 <= M2)<<std::endl;
  //------------------------------


  //------------------------------
  //fill methods
  //------------------------------
  M3.fill(4.0);
  std::cout<<M3<<std::endl;
  double d2[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0};
  M3.fill(d2);
  std::cout<<M3<<std::endl;
  std::cout<<std::endl;
  std::vector<int> V2(18);
  for(std::size_t i = 0; i < 18; ++i)
    V2[i] = i;
  M3.fill(V2.begin(),V2.end());
  std::cout<<M3<<std::endl;
  //------------------------------
  
  
  //------------------------------
  // With non primitive data
  //------------------------------
  A S(2);
  S.print();
  std::cout<<std::endl;
  slip::Array2d<A> MA(3,4,S);
 
  std::cout<<"dim1 = "<<MA.dim1()<<" dim2 = "<<MA.dim2()<<" size = "<<MA.size()<<" empty = "<<MA.empty()<<std::endl; 
   for(size_t i = 0; i < MA.dim1(); ++i)
    {
      for(size_t j = 0; j < MA.dim2(); ++j)
	{
	  MA[i][j].print();
	}
      std::cout<<std::endl;
    }
 

   std::complex<double> c(2.3,2.3);
   std::cout<<c<<std::endl;
   slip::Array2d<std::complex<double> > MC(3,2,c);
   std::cout<<MC<<std::endl; 

   std::cout<<MC[1+1][0+1]<<std::endl;
  

  

   
   slip::Array2d<slip::block<double,3> > Mmulti(3,4);

   for(size_t i = 0; i < Mmulti.dim1(); ++i)
     {
       for(size_t j = 0; j < Mmulti.dim2(); ++j)
	 {
	   Mmulti[i][j][0] = i + j ;
	   Mmulti[i][j][1] = 2 * (i + j);
	   Mmulti[i][j][2] = 3 * (i + j);
	 }
     }

   for(size_t i = 0; i < Mmulti.dim1(); ++i)
     {
       for(size_t j = 0; j < Mmulti.dim2(); ++j)
	 {
	   std::cout<<Mmulti[i][j];
	   //std::cout<<Mmulti[i][j][0]<<" ";
	 }
       std::cout<<std::endl;
     }

   std::cout<<"Premier plan ..."<<std::endl;
   for(size_t i = 0; i < Mmulti.dim1(); ++i)
     {
       for(size_t j = 0; j < Mmulti.dim2(); ++j)
	 {
	   std::cout<<Mmulti[i][j][0]<<" ";
	 }
       std::cout<<std::endl;
     }
 
   slip::Array2d<pixelRGB<double> > MRGB(3,4);
   for(size_t i = 0; i < MRGB.dim1(); ++i)
     {
       for(size_t j = 0; j < MRGB.dim2(); ++j)
	 {
	   MRGB[i][j].set_red(i + j) ;
	   MRGB[i][j].set_green(2 * (i + j));
	   MRGB[i][j].set_blue(3 * (i + j));
	 }
     }
   
   std::cout<<"Plan vert..."<<std::endl;
   for(size_t i = 0; i < MRGB.dim1(); ++i)
     {
       for(size_t j = 0; j < MRGB.dim2(); ++j)
	 {
	   //std::cout<<MRGB[i][j].get_green()<<" ";
	   std::cout<<MRGB[i][j].green<<" ";
	 }
       std::cout<<std::endl;
     }
   //------------------------------

   std::copy((double*)(MRGB.begin()),(double*)(MRGB.begin())+36,std::ostream_iterator<double>(std::cout," "));
  
   std::cout<<std::endl;
    std::copy((double*)(Mmulti.begin()),(double*)(Mmulti.begin())+36,std::ostream_iterator<double>(std::cout," "));
    std::cout<<std::endl;
  

    slip::Array2d<double>::iterator2d i1 = M3.upper_left();
      slip::Array2d<double>::iterator2d i2 = M3.bottom_right();

      
      std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
      std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
      std::cout << "Array2d ascii serialization " << std::endl;
      std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
      std::ofstream ofs("array2d.txt");
    slip::Array2d<float> b(3,4);
    slip::iota(b.begin(),b.end(),1.0f);
    
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<b;
    }//to close archive
    std::ifstream ifs("array2d.txt");
    slip::Array2d<float> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "Array2d binary serialization " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("array2d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<b;
    }//to close archive
    std::ifstream ifsb("array2d.bin");
    slip::Array2d<float> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;

    
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "complex Array2d ASCII serialization " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    slip::Array2d<std::complex<float> > bc(3,4);
    slip::iota(bc.begin(),bc.end(),std::complex<float>(1.0f,2.0f),
    	       std::complex<float>(1.0f,1.0f));
    std::ofstream ofsc("array2dc.txt");
     {
       
       boost::archive::text_oarchive oac(ofsc);
       oac<<bc;
     }//to close archive
    std::ifstream ifsc("array2dc.txt");
    slip::Array2d<std::complex<float> > new_pc;
    {
    boost::archive::text_iarchive iac(ifsc);
    iac>>new_pc;
    }//to close archive
    std::cout<<"new_pc = \n"<<new_pc<<std::endl;
    return 0;
}
