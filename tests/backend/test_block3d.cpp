#include "Block3d.hpp"
#include <iostream>
#include <complex>

int main()
{

  slip::block3d<double,4,3,2> M;
  std::cout<<"name = "<<M.name()<<" dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" dim3 = "<<M.dim3()<<" size = "<<M.size()<<" size_max = "<<M.size_max()<<" empty = "<<M.empty()<<std::endl; 
  M.fill(3.7);
  std::cout<<M<<std::endl;


  double d[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0};
  
  slip::block3d<double,4,3,2> M2;
  M2.fill(d);

  slip::block2d<double,3,2> M3=M2[1];


 if (M2<=M2) std::cout <<"infEGAL";
else std::cout << "DIFFERENT";
;

  std::cout<<M2<<std::endl<<std::endl<<M2[1][2][1]<<" "<<M3[2][1]<<" "<<M2(2,1,1)<<std::endl;

  
  //affichage lin�aire des valeurs de M2
  std::copy(M2.begin(),M2.end(),std::ostream_iterator<double>(std::cout,"\n"));
  //a l'envers
  std::copy(M2.rbegin(),M2.rend(),std::ostream_iterator<double>(std::cout,"\n"));

  
  
  M.swap(M2);
  std::cout<<M2<<std::endl;
  std::cout<<M<<std::endl;

  return 0;
}
