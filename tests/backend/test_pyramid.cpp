#include <iostream>
#include <iomanip>
#include "Array.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "Vector2d.hpp"
#include "DenseVector2dField2d.hpp"
#include "arithmetic_op.hpp"
#include "DPoint2d.hpp"

#include "Pyramid.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
int main()
{

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "slip::Pyramid<slip::Array<float> > default constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Pyramid<slip::Array<float> > pyd1d0;
  std::cout<<pyd1d0<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "slip::Pyramid<slip::Array<float> >" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Array<float> init1d(16);
  slip::iota(init1d.begin(),init1d.end(),1.0,1.0);
  std::cout<<"init1d = \n"<<init1d<<std::endl;
  slip::Pyramid<slip::Array<float> > pyd1d(init1d.begin(),
					   init1d.end(),
					   3);
  std::cout<<pyd1d<<std::endl;

   
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "slip::Pyramid<slip::Array2d<float> >" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Array2d<float> init2d(16,16);
  slip::iota(init2d.begin(),init2d.end(),1.0,1.0);
  std::cout<<"init2d = \n"<<init1d<<std::endl;
  slip::Pyramid<slip::Array2d<float> > pyd2d(init2d.upper_left(),
					     init2d.bottom_right(),
					     3);
  std::cout<<pyd2d<<std::endl;
  std::cout<<"size = "<<pyd2d.size()<<std::endl;
  std::cout<<"max_size = "<<pyd2d.max_size()<<std::endl;
  std::cout<<"empty = "<<pyd2d.empty()<<std::endl;
  std::cout<<"levels = "<<pyd2d.levels()<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "slip::Pyramid<slip::Array2d<float> > copy constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Pyramid<slip::Array2d<float> > pyd2d2(pyd2d);
  std::cout<<pyd2d2<<std::endl;

  // slip::Pyramid<slip::Array2d<float> > pyd2d3(pyd2d);
  // std::cout<<pyd2d3<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "slip::Pyramid<slip::Array2d<float> > operator=" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Pyramid<slip::Array2d<float> > pyd2d4;
  pyd2d4 = pyd2d;
  std::cout<<pyd2d4<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "slip::Pyramid<slip::Array2d<float> > self assignment" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;


  pyd2d4 = pyd2d4;
  std::cout<<pyd2d4<<std::endl;

   
  
 //  slip::Pyramid<slip::Array2d<float> > pyd2d5;
//   pyd2d5.swap(pyd2d);
//   std::cout<<pyd2d5<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "slip::Pyramid<slip::Array3d<float> > fill" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Pyramid<slip::Array2d<float> > pyd2d5(pyd2d);
  pyd2d5.fill(2.0);
  std::cout<<pyd2d5<<std::endl;

  

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "slip::Pyramid<slip::Array2d<float> > == !=" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"pyd2d2 == pyd2d4: "<<(pyd2d2 == pyd2d4)<<std::endl;;
  std::cout<<"pyd2d2 != pyd2d4: "<<(pyd2d2 != pyd2d4)<<std::endl;;
  
  std::cout<<"pyd2d2 == pyd2d5: "<<(pyd2d2 == pyd2d5)<<std::endl;;
  std::cout<<"pyd2d2 != pyd2d5: "<<(pyd2d2 != pyd2d5)<<std::endl;;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "slip::Pyramid<slip::Array3d<float> > swap" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<pyd2d<<std::endl;
  std::cout<<pyd2d5<<std::endl;
  pyd2d5.swap(pyd2d);
  std::cout<<pyd2d<<std::endl;
  std::cout<<pyd2d5<<std::endl;


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "slip::Pyramid<slip::DenseVector2dField2d<float>  > " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::DenseVector2dField2d<float> initvf2d(16,16);
  slip::iota(initvf2d.begin(),initvf2d.end(),slip::Vector2d<float>(1.0,1.0));
  std::cout<<"initvf2d = \n"<<initvf2d<<std::endl;
  slip::Pyramid<slip::DenseVector2dField2d<float> > pyd2dvf(initvf2d.upper_left(),
							  initvf2d.bottom_right(),3);
  std::cout<<pyd2dvf<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "slip::Pyramid<slip::Array3d<float> >" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Array3d<float> init3d(16,16,16);
  slip::iota(init3d.begin(),init3d.end(),1.0,1.0);
  std::cout<<"init3d = \n"<<init3d<<std::endl;
  slip::Pyramid<slip::Array3d<float> > pyd3d(init3d.front_upper_left(),
					     init3d.back_bottom_right(),
					     3);
  std::cout<<pyd3d<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "slip::Pyramid<slip::Array3d<float> > copy constructor"<< std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Pyramid<slip::Array3d<float> > pyd3d2(pyd3d);
  std::cout<<pyd3d2<<std::endl;
  
  
  slip::Pyramid<slip::Array<float> > pyd3d6(init1d.begin(),
					    init1d.end(),
					     5);
  std::cout<<pyd3d6<<std::endl;

  for(std::size_t i = 0; i < pyd3d6.levels(); ++i)
    {
      std::cout<<"size level "<<i<<": "<<pyd3d6[i].size()<<std::endl;
    }
  //
  //serialization
  //
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "slip::Pyramid<slip::Array<float> >" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Array<float> inits1d(16);
  slip::iota(inits1d.begin(),inits1d.end(),1.0,1.0);
  std::cout<<"inits1d = \n"<<inits1d<<std::endl;
  slip::Pyramid<slip::Array<float> > pyds1d(inits1d.begin(),
					   inits1d.end(),
					   3);
  slip::Range<int> ranges1(0,15,2);
  std::copy(pyds1d[0].begin(ranges1),
	    pyds1d[0].end(ranges1),
	    pyds1d[1].begin());
  slip::Range<int> ranges2(0,15,4);
  std::copy(pyds1d[0].begin(ranges2),
	    pyds1d[0].end(ranges2),
	    pyds1d[2].begin());
 
 
  std::cout<<pyds1d<<std::endl;

  //  create and open a character archive for output
    std::ofstream ofs("pyds1d.txt");
      
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<pyds1d;
    }//to close archive
    std::ifstream ifs("pyds1d.txt");
    slip::Pyramid<slip::Array<float> > new_pyds1d;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_pyds1d;
    }//to close archive
    std::cout<<"new_pyds1d\n"<<new_pyds1d<<std::endl;

    std::ofstream ofsb("pyds1d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<pyds1d;
    }//to close archive

    std::ifstream ifsb("pyds1d.bin");
    slip::Pyramid<slip::Array<float> > new_pyds1db;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_pyds1db;
    }//to close archive
    std::cout<<"new_pyds1b\n"<<new_pyds1db<<std::endl;
    

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "slip::Pyramid<slip::Array2d<float> >" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    slip::Array2d<float> inits2d(16,16);
    slip::iota(inits2d.begin(),inits2d.end(),1.0,1.0);
    
    slip::Pyramid<slip::Array2d<float> > pyds2d(inits2d.upper_left(),
    					       inits2d.bottom_right(),
    					       3);
    slip::Range<int> ranges2d1(0,15,2);
    std::copy(pyds2d[0].upper_left(ranges2d1,ranges2d1),
    	      pyds2d[0].bottom_right(ranges2d1,ranges2d1),
    	      pyds2d[1].begin());
    slip::Range<int> ranges2d2(0,15,4);
    std::copy(pyds2d[0].upper_left(ranges2d2,ranges2d2),
    	      pyds2d[0].bottom_right(ranges2d2,ranges2d2),
    	      pyds2d[2].begin());
 
    std::cout<<pyds2d<<std::endl;
    

    // create and open a character archive for output
    std::ofstream ofs2("pyds2d.txt");
      
    {
    boost::archive::text_oarchive oa2(ofs2);
    oa2<<pyds2d;
    }//to close archive

    std::ifstream ifs2("pyds2d.txt");
    slip::Pyramid<slip::Array2d<float> > new_pyds2d;
     {
       boost::archive::text_iarchive ia2(ifs2);
       ia2>>new_pyds2d;
     }//to close archive
     std::cout<<"new_pyds2d\n"<<new_pyds2d<<std::endl;
     
     std::ofstream ofs2b("pyds2d.bin");
    {
    boost::archive::binary_oarchive oa2b(ofs2b);
    oa2b<<pyds2d;
    }//to close archive

    std::ifstream ifs2b("pyds2d.bin");
    slip::Pyramid<slip::Array2d<float> > new_pyds2db;
    {
    boost::archive::binary_iarchive ia(ifs2b);
    ia>>new_pyds2db;
    }//to close archive
    std::cout<<"new_pyds2b\n"<<new_pyds2db<<std::endl;
    

     std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "slip::Pyramid<slip::Array3d<float> >" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    slip::Array3d<float> inits3d(16,16,16);
    slip::iota(inits3d.begin(),inits3d.end(),1.0,1.0);
    
    slip::Pyramid<slip::Array3d<float> > pyds3d(inits3d.front_upper_left(),
    					       inits3d.back_bottom_right(),
    					       3);
    slip::Range<int> ranges3d1(0,15,2);
    std::copy(pyds3d[0].front_upper_left(ranges3d1,ranges3d1,ranges3d1),
    	      pyds3d[0].back_bottom_right(ranges3d1,ranges3d1,ranges3d1),
    	      pyds3d[1].begin());
    slip::Range<int> ranges3d2(0,15,4);
    std::copy(pyds3d[0].front_upper_left(ranges3d2,ranges3d2,ranges3d2),
    	      pyds3d[0].back_bottom_right(ranges3d2,ranges3d2,ranges3d2),
    	      pyds3d[2].begin());
 
    std::cout<<pyds3d<<std::endl;
    

    // create and open a character archive for output
    std::ofstream ofs3("pyds3d.txt");
      
    {
    boost::archive::text_oarchive oa3(ofs3);
    oa3<<pyds3d;
    }//to close archive

    std::ifstream ifs3("pyds3d.txt");
    slip::Pyramid<slip::Array3d<float> > new_pyds3d;
     {
       boost::archive::text_iarchive ia3(ifs3);
       ia3>>new_pyds3d;
     }//to close archive
     std::cout<<"new_pyds3d\n"<<new_pyds3d<<std::endl;
     
     std::ofstream ofs3b("pyds3d.bin");
    {
    boost::archive::binary_oarchive oa3b(ofs3b);
    oa3b<<pyds3d;
    }//to close archive

    std::ifstream ifs3b("pyds3d.bin");
    slip::Pyramid<slip::Array3d<float> > new_pyds3db;
    {
    boost::archive::binary_iarchive ia(ifs3b);
    ia>>new_pyds3db;
    }//to close archive
    std::cout<<"new_pyds3b\n"<<new_pyds3db<<std::endl;

  return 0;
}
