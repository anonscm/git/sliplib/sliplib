#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <cmath>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <string>

#include "Vector.hpp"
#include "Array.hpp"
#include "Matrix.hpp"
#include "io_tools.hpp"

#include "SoloffCamera.hpp"
#include "MultivariatePolynomial.hpp"

#include "arithmetic_op.hpp"
// #include <boost/archive/text_oarchive.hpp>
// #include <boost/archive/text_iarchive.hpp>

// #include <boost/archive/binary_oarchive.hpp>
// #include <boost/archive/binary_iarchive.hpp>

//using namespace::std;

// main program
//int main(int argc, char *argv[])
int main()
{
  //typedef float Type_Calcul;
  typedef double Type_Calcul;


  // --------------------------------------------------      
  // ----- constructors          ----------------------
  // --------------------------------------------------   
  std::cout<<"--Default constructor ---"<<std::endl;
  slip::SoloffCamera<Type_Calcul> soloff_camera1;
  std::cout<<soloff_camera1<<std::endl;
  std::cout<<std::endl;
 
  // std::cerr << "reading calibration files" <<std::endl;
  // soloff_camera1.read("soloff.cal");
  // std::cout<<soloff_camera1<<std::endl;
  // std::cout<<std::endl;
 
  // std::cout<<"--Copy constructor ---"<<std::endl;
  // slip::SoloffCamera<Type_Calcul> soloff_camera2(soloff_camera1);
  // std::cout<<soloff_camera2<<std::endl;
  // std::cout<<std::endl;

  // std::cout<<"-- operator= ---"<<std::endl;
  // slip::SoloffCamera<Type_Calcul> soloff_camera3;
  // soloff_camera3 = soloff_camera1;
  // std::cout<<soloff_camera3<<std::endl;
  // std::cout<<std::endl;

  // std::cout<<"-- self assignment ---"<<std::endl;
  // soloff_camera1 = soloff_camera1;
  // std::cout<<soloff_camera1<<std::endl;
  // std::cout<<std::endl;


  // // std::cout<<"-- compute ---"<<std::endl;
  // // slip::Matrix<Type_Calcul> data_soloff(x,x);
  // // slip::SoloffCamera<Type_Calcul> soloff_camera4;
  // // soloff_camera4.compute(P);
  // // std::cout<<soloff_camera4<<std::endl;
  // //std::cout<<std::endl;
  
 //  // --------------------------------------------------      
 //  // ----- Accessors/Mutators    ----------------------
 //  // --------------------------------------------------   
 //  std::cout<<"calibration_polynomial_x = \n"<< soloff_camera3.calibration_polynomial_x()<<std::endl;

 //   std::cout<<"calibration_polynomial_coeff_x = \n"<< soloff_camera3.calibration_polynomial_coeff_x()<<std::endl;

 //  std::cout<<"calibration_polynomial_y = \n"<<soloff_camera3.calibration_polynomial_y()<<std::endl;

 //  std::cout<<"calibration_polynomial_coeff_y = \n"<< soloff_camera3.calibration_polynomial_coeff_y()<<std::endl;

 //  std::cout<<"calibration_polynomial_X = \n"<<soloff_camera3.calibration_polynomial_X()<<std::endl;

 //  std::cout<<"calibration_polynomial_coeff_X = \n"<< soloff_camera3.calibration_polynomial_coeff_X()<<std::endl;


 //  std::cout<<"calibration_polynomial_Y = \n"<<soloff_camera3.calibration_polynomial_Y()<<std::endl;
  
 //  std::cout<<"calibration_polynomial_coeff_Y = \n"<< soloff_camera3.calibration_polynomial_coeff_Y()<<std::endl;

 //  slip::SoloffCamera<Type_Calcul> soloff_camera5;
 //  slip::MultivariatePolynomial<Type_Calcul,3>& px_3 =  soloff_camera3.calibration_polynomial_x();
 //  soloff_camera5.calibration_polynomial_x(px_3);
 //  std::cout<<"soloff_camera5 = \n"<<soloff_camera5<<std::endl;
  
 //  slip::MultivariatePolynomial<Type_Calcul,3>& py_3 =  soloff_camera3.calibration_polynomial_y();
 //  soloff_camera5.calibration_polynomial_y(py_3);
 //  std::cout<<"soloff_camera5 = \n"<<soloff_camera5<<std::endl;
  
 //   slip::MultivariatePolynomial<Type_Calcul,3>& pX_3 =  soloff_camera3.calibration_polynomial_X();
 //  soloff_camera5.calibration_polynomial_X(pX_3);
 //  std::cout<<"soloff_camera5 = \n"<<soloff_camera5<<std::endl;

 //  slip::MultivariatePolynomial<Type_Calcul,3>& pY_3 =  soloff_camera3.calibration_polynomial_Y();
 //  soloff_camera5.calibration_polynomial_Y(pY_3);
 //  std::cout<<"soloff_camera5 = \n"<<soloff_camera5<<std::endl;

 //  slip::SoloffCamera<Type_Calcul> soloff_camera6;
 //  slip::Array<Type_Calcul> px_coeff_3(20);
 //  slip::iota(px_coeff_3.begin(),px_coeff_3.end(),Type_Calcul(1.0),Type_Calcul(1.0));
 //  soloff_camera6.calibration_polynomial_coeff_x(px_coeff_3.begin(),px_coeff_3.end());
 //   std::cout<<"soloff_camera6 = \n"<<soloff_camera6<<std::endl;
  
 //   slip::Array<Type_Calcul> py_coeff_3(20);
 //  slip::iota(py_coeff_3.begin(),py_coeff_3.end(),Type_Calcul(2.0),Type_Calcul(1.0));
 //  soloff_camera6.calibration_polynomial_coeff_y(py_coeff_3.begin(),py_coeff_3.end());
 //   std::cout<<"soloff_camera6 = \n"<<soloff_camera6<<std::endl;
  
 //   slip::Array<Type_Calcul> pX_coeff_3(20);
 //  slip::iota(pX_coeff_3.begin(),pX_coeff_3.end(),Type_Calcul(3.0),Type_Calcul(1.0));
 //  soloff_camera6.calibration_polynomial_coeff_X(pX_coeff_3.begin(),pX_coeff_3.end());
 //   std::cout<<"soloff_camera6 = \n"<<soloff_camera6<<std::endl;
  
 //   slip::Array<Type_Calcul> pY_coeff_3(20);
 //  slip::iota(pY_coeff_3.begin(),pY_coeff_3.end(),Type_Calcul(4.0),Type_Calcul(1.0));
 //  soloff_camera6.calibration_polynomial_coeff_Y(pY_coeff_3.begin(),pY_coeff_3.end());
 //   std::cout<<"soloff_camera6 = \n"<<soloff_camera6<<std::endl;
  

 //  // --------------------------------------------------      
 //  // ----- camera models reading ----------------------
 //  // --------------------------------------------------      
 //  std::cerr << "reading calibration files" <<std::endl;

 //  slip::CameraModel<Type_Calcul>* soloff_camera;

 //  std::cerr << "reading soloff camera model" << std::endl;
 //  soloff_camera = new slip::SoloffCamera<Type_Calcul>();
 //  soloff_camera->read("soloff.cal");

  
 //  //int ni=100000000;
 //  int ni = 10;
 //  std::cerr<<"computes "<<ni<<" projection/backprojection "<<std::endl;
 //  std::cerr<<std::endl;
 //  std::time_t H1,H2;
 //  // computing soloff
 //  time(&H1);
 //  slip::Point2d<Type_Calcul> pixp(Type_Calcul(1),Type_Calcul(1));
 //  Type_Calcul z=Type_Calcul(1);
 //  slip::Point3d<Type_Calcul> voxp;
  
 //  for (int i=0; i<ni; ++i)
 //    {
 //      //pixp=slip::Point2d<Type_Calcul>(Type_Calcul(1),Type_Calcul(1));
 //      pixp=slip::Point2d<Type_Calcul>(Type_Calcul(2),Type_Calcul(3));
      
 //      voxp=(*soloff_camera).backprojection(pixp,z);
 //      //std::cout<<voxp<<std::endl;
 //      pixp=(*soloff_camera).projection(voxp);
 //      //std::cout<<pixp<<std::endl;
 //    }

 //  time(&H2);
 // std::cerr <<std::endl;
 // std::cerr << "####### soloff computation time ###################" <<std::endl;
 //  if ((H2-H1)% 86400 / 3600 !=0 )
 //   std::cerr << ((H2-H1)% 86400 / 3600) << " h ";
 //  if ((H2-H1) % 3600 / 60 !=0)
 //   std::cerr << ((H2-H1) % 3600 / 60) << " min ";
 //  if ((H2-H1) % 60 !=0)
 //   std::cerr << ((H2-H1) % 60) << " s";
 // std::cerr <<std::endl;
 // std::cerr << "#####################################################" <<std::endl;
 // std::cerr <<std::endl;
  

 //  delete soloff_camera;
 //  //
 //  //serialization
 //  //

 //  std::cout<<"--Default constructor ---"<<std::endl;
 //  slip::SoloffCamera<Type_Calcul> soloff_cameras;
 //  // std::cout<<soloff_cameras<<std::endl;
 //  //std::cout<<std::endl;
 
 //  std::cerr << "reading calibration files" <<std::endl;
 //  soloff_cameras.read("soloff.cal");
 //  std::cout<<soloff_cameras<<std::endl;
 //  std::cout<<std::endl;

 

 //  // create and open a character archive for output
 //    std::ofstream ofs("soloffcamera.txt");
      
 //    {
 //    boost::archive::text_oarchive oa(ofs);
 //    oa<<soloff_cameras;
 //    }//to close archive
 //    std::ifstream ifs("soloffcamera.txt");
 //    slip::SoloffCamera<Type_Calcul>  new_soloff;
 //    {
 //    boost::archive::text_iarchive ia(ifs);
 //    ia>>new_soloff;
 //    }//to close archive
 //    std::cout<<"new_soloff\n"<<new_soloff<<std::endl;

 //    std::ofstream ofsb("soloffcamera.bin");
 //    {
 //    boost::archive::binary_oarchive oa(ofsb);
 //    oa<<soloff_cameras;
 //    }//to close archive

 //    std::ifstream ifsb("soloffcamera.bin");
 //    slip::SoloffCamera<Type_Calcul> new_soloffb;
 //    {
 //    boost::archive::binary_iarchive ia(ifsb);
 //    ia>>new_soloffb;
 //    }//to close archive
 //    std::cout<<"new_soloffb\n"<<new_soloffb<<std::endl;

}





