#include<algorithm>
#include<iostream>
#include<iterator>
#include<complex>
#include "stride_iterator.hpp"
#include "Array2d.hpp"
#include "statistics.hpp"
#include "Array.hpp"

int main()
{

  int t[] = {0,1,2,3,4,5,6,7};

  slip::stride_iterator<int*> first(t,2);
  slip::stride_iterator<int*> last(t+8,2);
  std::copy(first,last,std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;

  slip::Array2d<double> M(3,4);
  std::generate(M.begin(),M.end(),std::rand);
  std::cout<<M<<std::endl;
 
  slip::stride_iterator<slip::Array2d<double>::const_iterator> firstM(M.begin(),2);
  slip::stride_iterator<slip::Array2d<double>::const_iterator> lastM(M.end(),2);
  std::copy(firstM,lastM,std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;

  slip::stride_iterator<slip::Array2d<double>::const_iterator> it;
  slip::stride_iterator<slip::Array2d<double>::const_iterator> itb = firstM;
  slip::stride_iterator<slip::Array2d<double>::const_iterator> ite = lastM;
  
  for(it = itb; it != ite; ++it)
    {
      std::cout<<(*it)<<" ";
    }
   std::cout<<std::endl;

   slip::stride_iterator<slip::Array2d<double>::iterator> it2(M.begin(),2);
   for(int i = 0; i < (3*4) /2; ++i)
    {
      std::cout<<it2[i]<<" ";
    }
   std::cout<<std::endl;

    slip::stride_iterator<slip::Array2d<double>::const_iterator> it3(itb);
    std::cout<<*(it3 + 4)<<" "<<std::endl;
   for(int i = 0; i < 4; ++i)
     {
       std::cout<<*it3<<" -";
       it3 = it3 + 2;
       std::cout<<*it3<<" ";
       //it3+=2;
     }
   std::cout<<std::endl;

   slip::Array<int> S1d(10);
   for(size_t i = 0; i < S1d.size(); ++i)
     {
       S1d[i] = i;
     }
   std::cout<<S1d<<std::endl;
   slip::stride_iterator<slip::Array<int>::iterator> it1d(S1d.begin(),2);
   slip::stride_iterator<slip::Array<int>::iterator> it1de(S1d.end(),2);
  
 std::cout<<slip::mean<double>(S1d.begin(),S1d.end())<<std::endl;
 
 std::cout<<slip::mean<float>(S1d.begin(),S1d.end())<<std::endl;
 std::cout<<slip::cardinal<float>(it1d,it1de)<<std::endl;
 std::cout<<slip::mean<float>(it1d,it1de)<<std::endl;
 std::copy(it1d,it1de,std::ostream_iterator<int>(std::cout," "));
 std::cout<<std::endl;
 //std::partial_sum(it1d,it1de,it1d);
 //std::copy(it1d,it1de,std::ostream_iterator<int>(std::cout," "));
 //std::cout<<std::endl;
 std::cout<<std::inner_product(it1d,it1de,it1d,0.0)<<std::endl;
 std::cout<<*it1d<<std::endl;
 for(std::size_t i = 0; i < 5; ++i)
   std::cout<<*(it1d + i)<<" "; 
 std::cout<<std::endl;
 for(std::size_t i = 1; i < 6; ++i)
   std::cout<<*(it1de - i)<<" ";
 std::cout<<std::endl;

 for(std::size_t i = 0; i < 5; ++i)
   std::cout<<it1d[i]<<" "; 
 std::cout<<std::endl;

 for(std::size_t i = 0; i < 5; ++i)
   std::cout<<((it1d + i) - it1d)<<" "; 
 std::cout<<std::endl;

 std::cout<<"nb element = "<<(it1de - it1d)<<std::endl;

for(std::size_t i = 0; i < 5; ++i)
   it1d[i] = i + 4; 

for(std::size_t i = 0; i < 5; ++i)
   std::cout<<it1d[i]<<" "; 
 std::cout<<std::endl;


 slip::Array<std::complex<double> > Mcomp(10);
 slip::stride_iterator<slip::Array2d<std::complex<double> >::iterator> itc(Mcomp.begin(),2);
 for(std::size_t i = 0; i < 5; ++i)
   {
     //itc->real() = 2.0;
     *itc = std::complex<double>(2.0,0);
   }
 std::cout<<std::endl;
  slip::stride_iterator<slip::Array2d<std::complex<double> >::const_iterator> itcc(Mcomp.begin(),2);
 for(std::size_t i = 0; i < 5; ++i)
   {
     std::cout<<itcc->real()<<", ";
   }
 std::cout<<std::endl;
 
 return 0;
}
