#include <iostream>
#include <iomanip>
#include <vector>

#include "PolySupport.hpp"

int main()
{
  typedef double T;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Default constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::PolySupport<T> default_support;
  std::cout<<"default_support = "<<default_support<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::PolySupport<T> support1(static_cast<T>(-1.0),static_cast<T>(3.0));
  std::cout<<"support1 = "<<support1<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Copy constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::PolySupport<T> copy_support(support1);
  std::cout<<"copy_support = "<<copy_support<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "operator=" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::PolySupport<T> equal_support;
  equal_support = support1;
  std::cout<<"equal_support = "<<equal_support<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "accessors/mutators" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"support1.a() = "<<support1.a()<<std::endl;
  std::cout<<"support1.min() = "<<support1.min()<<std::endl;
  std::cout<<"support1.b() = "<<support1.b()<<std::endl;
  std::cout<<"support1.max() = "<<support1.max()<<std::endl;
  std::cout<<"support1.set(4.0,9.0) = "<<std::endl;
  support1.set(4.0,9.0);
  std::cout<<"support1 = "<<support1<<std::endl;
  std::cout<<"support1.a(-4.0)"<<std::endl;
  support1.a(-4.0);
  std::cout<<"support1 = "<<support1<<std::endl;
  std::cout<<"support1.min(-7.0)"<<std::endl;
  support1.min(-7.0);
  std::cout<<"support1 = "<<support1<<std::endl;
  
  std::cout<<"support1.b(4.0)"<<std::endl;
  support1.b(4.0);
  std::cout<<"support1 = "<<support1<<std::endl;
  std::cout<<"support1.max(7.0)"<<std::endl;
  support1.max(7.0);
  std::cout<<"support1 = "<<support1<<std::endl;
  

  

  return 0;
}
