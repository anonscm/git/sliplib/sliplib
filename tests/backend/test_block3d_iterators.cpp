#include<algorithm>
#include<iostream>
#include<iterator>
#include <numeric>
#include <complex>

#include "Block3d.hpp"


template<typename T>
void daccum(T& a){static int init = 0.0; a = init++;}
template<typename T>
void disp(T const& a){std::cout << a <<" ";}

int main()
{
  
  //Array3d
  slip::block3d<double,5,4,3> M;
  std::for_each(M.begin(),M.end(),daccum<double>);
  std::cout<< M;
  std::cout<<std::endl;
  std::cout<<std::endl;

  //global iterator : 
  std::cout<< "global iterator" <<std::endl;
  std::copy(M.begin(),M.end(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rbegin(),M.rend(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //const global iterator : 
  slip::block3d<double,5,4,3> const Mc(M);
  std::cout<< "const global iterator" <<std::endl;
  std::copy(Mc.begin(),Mc.end(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rbegin(),Mc.rend(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  return 0;
}

