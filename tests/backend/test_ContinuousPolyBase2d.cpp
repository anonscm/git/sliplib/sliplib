#include <iostream>
#include <iomanip>
#include <vector>

#include "ContinuousPolyBase2d.hpp"
#include "PolySupport.hpp"

int main()
{
  typedef double T;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Default constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousPolyBase2d<T> default_base2d;
  std::cout<<"default_base2d = \n"<<default_base2d<<std::endl;

  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Constructor 2d" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  const std::size_t base2d1_range_size = 6;
  const std::size_t base2d1_degree = 5;
  const std::size_t base2d1_dim = 1;
  slip::PolySupport<T> base2d1_support(0.0,4.0);
  slip::POLY_BASE_GENERATION_METHOD generation_method = slip::POLY_BASE_GENERATION_FILE;
  //slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_RECTANGULAR;
  //slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_SIMPSON;
  //slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_HOSNY2007;
    slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_EXACT;
 
  // slip::ContinuousLegendreBase2d<T> base2d1(base2d1_degree,false,false,generation_method,base2d1_range_size,base2d1_support,integration_method);
  // std::cout<<"base2d1 = \n"<<base2d1<<std::endl;
  // base2d1.generate();
  // std::cout<<base2d1<<std::endl;

 
 
  
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "Copy constructor" << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // slip::ContinuousLegendreBase1d<T,2> copy_base2d(base2d1);
  // std::cout<<"copy_base2d = \n"<<copy_base2d<<std::endl;


  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "operator=" << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // slip::ContinuousLegendreBase1d<T,2> equal_base2d;
  // equal_base2d = base2d1;
  // std::cout<<"equal_base2d = \n"<<equal_base2d<<std::endl;

  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "project" << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // slip::Array<T> data(base2d1_range_size,T(5.12));
  // // slip::Array<T> data(base2d1_range_size);
  // // slip::iota(data.begin(),data.end(),T(0.0));
  // std::cout<<"data =\n"<<data<<std::endl;
  // for(std::size_t k = 0; k < base2d1.size(); ++k)
  //   {
  //     std::cout<<"(P"<<k<<",data) ="<<base2d1.project(data.begin(),data.end(),k)<<std::endl;
  //   }
  
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "projection coefficients" << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // slip::Array<T> base2d1_coef(base2d1.size());
  // base2d1.project(data.begin(),data.end(),base2d1_coef.begin(),base2d1_coef.end());
  // std::cout<<"base2d1_coef = \n"<<base2d1_coef<<std::endl;


  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "reconstruction" << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // slip::Array<T> base2d1_reconstruct(base2d1.range_size());
  // base2d1.reconstruct(base2d1_coef.begin(),base2d1_coef.end(),base2d1.size(),
  // 		      base2d1_reconstruct.begin(),base2d1_reconstruct.end());
  // std::cout<<"base2d1_reconstruct = \n"<<base2d1_reconstruct<<std::endl;

  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "analytic reconstruction " << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // slip::MultivariatePolynomial<T,2> base2d1_recont_poly =
  //   base2d1.reconstruct(base2d1_coef.begin(),base2d1_coef.end(),base2d1.size());
  // std::cout<<"base2d1_recont_poly =\n"<<base2d1_recont_poly<<std::endl;
 
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "accessors/mutators" << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  

  

  return 0;
}
