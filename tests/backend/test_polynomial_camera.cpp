#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <cmath>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <string>

#include "Vector.hpp"
#include "Array.hpp"
#include "Matrix.hpp"
#include "io_tools.hpp"

#include "PolynomialCamera.hpp"
//#include "MultivariatePolynomial.hpp"

#include "arithmetic_op.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

//using namespace::std;

// main program
//int main(int argc, char *argv[])
int main()
{
  //typedef float Type_Calcul;
  typedef double Type_Calcul;


  // --------------------------------------------------      
  // ----- constructors          ----------------------
  // --------------------------------------------------   
  std::cout<<"--Default constructor ---"<<std::endl;
  slip::PolynomialCamera<Type_Calcul> polynomial_camera1;
  std::cout<<polynomial_camera1<<std::endl;
  std::cout<<std::endl;
 
  // std::cerr << "reading calibration files" <<std::endl;
  // polynomial_camera1.read("soloff.cal");
  // std::cout<<polynomial_camera1<<std::endl;
  // std::cout<<std::endl;
 
  // std::cout<<"--Copy constructor ---"<<std::endl;
  // slip::PolynomialCamera<Type_Calcul> polynomial_camera2(polynomial_camera1);
  // std::cout<<polynomial_camera2<<std::endl;
  // std::cout<<std::endl;

  // std::cout<<"-- operator= ---"<<std::endl;
  // slip::PolynomialCamera<Type_Calcul> polynomial_camera3;
  // polynomial_camera3 = polynomial_camera1;
  // std::cout<<polynomial_camera3<<std::endl;
  // std::cout<<std::endl;

  // std::cout<<"-- self assignment ---"<<std::endl;
  // polynomial_camera1 = polynomial_camera1;
  // std::cout<<polynomial_camera1<<std::endl;
  // std::cout<<std::endl;


  // std::cout<<"-- compute ---"<<std::endl;
  // slip::Matrix<Type_Calcul> data_polynomial(x,x);
  // slip::PolynomialCamera<Type_Calcul> polynomial_camera4;
  // polynomial_camera4.compute(P);
  // std::cout<<polynomial_camera4<<std::endl;
  //std::cout<<std::endl;
  
  // // --------------------------------------------------      
  // // ----- Accessors/Mutators    ----------------------
  // // --------------------------------------------------   
  // std::cout<<"x_coeffs = \n"<<polynomial_camera3.x_coeffs()<<std::endl;
  // std::cout<<"y_coeffs = \n"<<polynomial_camera3.y_coeffs()<<std::endl;


  // std::cout<<"XY_coeffs = \n"<<polynomial_camera3.XY_coeffs()<<std::endl;
  // std::cout<<"YX_coeffs = \n"<<polynomial_camera3.YX_coeffs()<<std::endl;
  // std::cout<<"XZ_coeffs = \n"<<polynomial_camera3.XZ_coeffs()<<std::endl;
  // std::cout<<"ZX_coeffs = \n"<<polynomial_camera3.ZX_coeffs()<<std::endl;
  // std::cout<<"YZ_coeffs = \n"<<polynomial_camera3.YZ_coeffs()<<std::endl;
  // std::cout<<"ZY_coeffs = \n"<<polynomial_camera3.ZY_coeffs()<<std::endl;
  
 
  // slip::PolynomialCamera<Type_Calcul> polynomial_camera6;
  // slip::Array<Type_Calcul> px_coeff_3(20);
  // slip::iota(px_coeff_3.begin(),px_coeff_3.end(),Type_Calcul(1.0),Type_Calcul(1.0));
  // polynomial_camera6.calibration_polynomial_coeff_x(px_coeff_3.begin(),px_coeff_3.end());
  //  std::cout<<"polynomial_camera6 = \n"<<polynomial_camera6<<std::endl;
  
  //  slip::Array<Type_Calcul> py_coeff_3(20);
  // slip::iota(py_coeff_3.begin(),py_coeff_3.end(),Type_Calcul(2.0),Type_Calcul(1.0));
  // polynomial_camera6.calibration_polynomial_coeff_y(py_coeff_3.begin(),py_coeff_3.end());
  //  std::cout<<"polynomial_camera6 = \n"<<polynomial_camera6<<std::endl;
  
  //  slip::Array<Type_Calcul> pX_coeff_3(20);
  // slip::iota(pX_coeff_3.begin(),pX_coeff_3.end(),Type_Calcul(3.0),Type_Calcul(1.0));
  // polynomial_camera6.calibration_polynomial_coeff_X(pX_coeff_3.begin(),pX_coeff_3.end());
  //  std::cout<<"polynomial_camera6 = \n"<<polynomial_camera6<<std::endl;
  
  //  slip::Array<Type_Calcul> pY_coeff_3(20);
  // slip::iota(pY_coeff_3.begin(),pY_coeff_3.end(),Type_Calcul(4.0),Type_Calcul(1.0));
  // polynomial_camera6.calibration_polynomial_coeff_Y(pY_coeff_3.begin(),pY_coeff_3.end());
  //  std::cout<<"polynomial_camera6 = \n"<<polynomial_camera6<<std::endl;
  

 //  // --------------------------------------------------      
 //  // ----- camera models reading ----------------------
 //  // --------------------------------------------------      
 //  std::cerr << "reading calibration files" <<std::endl;

 //  slip::CameraModel<Type_Calcul>* polynomial_camera;

 //  std::cerr << "reading polynomial camera model" << std::endl;
 //  polynomial_camera = new slip::PolynomialCamera<Type_Calcul>();
 //  polynomial_camera->read("soloff.cal");

  
 //  //int ni=100000000;
 //  int ni = 10;
 //  std::cerr<<"computes "<<ni<<" projection/backprojection "<<std::endl;
 //  std::cerr<<std::endl;
 //  std::time_t H1,H2;
 //  // computing polynomial
 //  time(&H1);
 //  slip::Point2d<Type_Calcul> pixp(Type_Calcul(1),Type_Calcul(1));
 //  Type_Calcul z=Type_Calcul(1);
 //  slip::Point3d<Type_Calcul> voxp;
  
 //  for (int i=0; i<ni; ++i)
 //    {
 //      //pixp=slip::Point2d<Type_Calcul>(Type_Calcul(1),Type_Calcul(1));
 //      pixp=slip::Point2d<Type_Calcul>(Type_Calcul(2),Type_Calcul(3));
      
 //      voxp=(*polynomial_camera).backprojection(pixp,z);
 //      //std::cout<<voxp<<std::endl;
 //      pixp=(*polynomial_camera).projection(voxp);
 //      //std::cout<<pixp<<std::endl;
 //    }

 //  time(&H2);
 // std::cerr <<std::endl;
 // std::cerr << "####### polynomial computation time ###################" <<std::endl;
 //  if ((H2-H1)% 86400 / 3600 !=0 )
 //   std::cerr << ((H2-H1)% 86400 / 3600) << " h ";
 //  if ((H2-H1) % 3600 / 60 !=0)
 //   std::cerr << ((H2-H1) % 3600 / 60) << " min ";
 //  if ((H2-H1) % 60 !=0)
 //   std::cerr << ((H2-H1) % 60) << " s";
 // std::cerr <<std::endl;
 // std::cerr << "#####################################################" <<std::endl;
 // std::cerr <<std::endl;
  

 //  delete polynomial_camera;
 //  //
 //  //serialization
 //  //

 //  std::cout<<"--Default constructor ---"<<std::endl;
 //  slip::PolynomialCamera<Type_Calcul> polynomial_cameras;
 //  // std::cout<<polynomial_cameras<<std::endl;
 //  //std::cout<<std::endl;
 
 //  std::cerr << "reading calibration files" <<std::endl;
 //  polynomial_cameras.read("soloff.cal");
 //  std::cout<<polynomial_cameras<<std::endl;
 //  std::cout<<std::endl;

 

  // create and open a character archive for output
    // std::ofstream ofs("soloffcamera.txt");
      
    // {
    // boost::archive::text_oarchive oa(ofs);
    // oa<<polynomial_cameras;
    // }//to close archive
    // std::ifstream ifs("soloffcamera.txt");
    // slip::PolynomialCamera<Type_Calcul>  new_polynomial;
    // {
    // boost::archive::text_iarchive ia(ifs);
    // ia>>new_polynomial;
    // }//to close archive
    // std::cout<<"new_polynomial\n"<<new_polynomial<<std::endl;

    // std::ofstream ofsb("soloffcamera.bin");
    // {
    // boost::archive::binary_oarchive oa(ofsb);
    // oa<<polynomial_cameras;
    // }//to close archive

    // std::ifstream ifsb("soloffcamera.bin");
    // slip::PolynomialCamera<Type_Calcul> new_polynomialb;
    // {
    // boost::archive::binary_iarchive ia(ifsb);
    // ia>>new_polynomialb;
    // }//to close archive
    // std::cout<<"new_polynomialb\n"<<new_polynomialb<<std::endl;

}





