#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

#include "OcTreeBox.hpp"
#include "Point3d.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


int main()
{
  
  typedef double T;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Default constructor " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::OcTreeBox<slip::Point3d<T> > qtb;
  std::cout<<"qtb = "<<qtb<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Constructor with coordinates" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::OcTreeBox<slip::Point3d<T> > qtb2(T(-1.023),T(4.56),T(0.0),T(8.7),T(-2.0),T(3.5));
  std::cout<<"qtb2 = "<<qtb2<<std::endl;

  std::cout<<"qtb2 width  = "<<qtb2.width()<<std::endl;
  std::cout<<"qtb2 height = "<<qtb2.height()<<std::endl;
  std::cout<<"qtb2 depth = "<<qtb2.depth()<<std::endl;
  std::cout<<"qtb2 volume  = "<<qtb2.volume()<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Constructor with points" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  slip::Point3d<T> min_p(T(-1.023),T(4.56),T(-2.0));
  slip::Point3d<T> max_p(T(0.0),T(8.7),T(3.5));
  
  slip::OcTreeBox<slip::Point3d<T> > qtb3(min_p,max_p);
  std::cout<<"qtb3 = "<<qtb3<<std::endl;

  std::cout<<"qtb3 width  = "<<qtb3.width()<<std::endl;
  std::cout<<"qtb3 height = "<<qtb3.height()<<std::endl;
  std::cout<<"qtb2 depth = "<<qtb2.depth()<<std::endl;
  std::cout<<"qtb2 volume  = "<<qtb2.volume()<<std::endl;
   
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Copy constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  slip::OcTreeBox<slip::Point3d<T> > qtb_copy(qtb3);
  std::cout<<"qtb_copy = "<<qtb_copy<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "operator=" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::OcTreeBox<slip::Point3d<T> > qtb_equal;
  qtb_equal = qtb3;
  std::cout<<"qtb_equal = "<<qtb_equal<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Comparison operators == and !=" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"qtb == qtb2: "<<(qtb == qtb2)<<std::endl;
  std::cout<<"qtb != qtb2: "<<(qtb != qtb2)<<std::endl;
  
  std::cout<<"qtb2 == qtb3: "<<(qtb3 == qtb2)<<std::endl;
  std::cout<<"qtb2 != qtb3: "<<(qtb3 != qtb2)<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "contains method" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Point3d<T> p(T(-0.023),T(5.56),T(0.0));
  std::cout<<"p = "<<p<<" inside qtb3 ? "<<qtb3.contains(p)<<std::endl;
  slip::Point3d<T> p2(T(-4.023),T(3.56),T(1.0));
  std::cout<<"p2= "<<p2<<" inside qtb3 ? "<<qtb3.contains(p2)<<std::endl;
  

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "contains with margin method" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  T margin = static_cast<T>(0.1);
  slip::Point3d<T> pm(T(-0.023),T(5.56),T(0.0));
  std::cout<<"pm = "<<pm<<" inside qtb3 ? "<<qtb3.contains(pm,margin)<<std::endl;
  slip::Point3d<T> pm2(T(-4.023),T(3.56),T(1.0));
  std::cout<<"pm2= "<<pm2<<" inside qtb3 ? "<<qtb3.contains(pm2,margin)<<std::endl;
  slip::Point3d<T> pm3(-T(0.5),T(5.56),T(0.0));
  std::cout<<"pm3= "<<pm3<<" inside qtb3 ? "<<qtb3.contains(pm3,margin)<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "is_consistent method" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Point3d<T> min_p2 = max_p;
  slip::Point3d<T> max_p2 = min_p;
  slip::OcTreeBox<slip::Point3d<T> > qtb4(min_p2,max_p2);
  std::cout<<"qtb4 = "<<qtb4<<std::endl;
  std::cout<<"qtb4.is_consistent() = "<<qtb4.is_consistent()<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "make_consistent method" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"make qtb4 consistent "<<std::endl;
  qtb4.make_consistent();
  std::cout<<"qtb4 = "<<qtb4<<std::endl;
  std::cout<<"qtb4.is_consistent() = "<<qtb4.is_consistent()<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "swap method" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  qtb4.swap(qtb);
  std::cout<<"qtb = "<<qtb<<std::endl;
  std::cout<<"qtb4 = "<<qtb4<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "OcTreeBox points" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"qtb3 = "<<qtb3<<std::endl;
  std::cout<<"xmin = "<<qtb3.xmin()<<std::endl;
  std::cout<<"xmax = "<<qtb3.xmax()<<std::endl;
  std::cout<<"xmiddle = "<<qtb3.xmiddle()<<std::endl;
  std::cout<<"ymin = "<<qtb3.ymin()<<std::endl;
  std::cout<<"ymax = "<<qtb3.ymax()<<std::endl;
  std::cout<<"ymiddle = "<<qtb3.ymiddle()<<std::endl;
  std::cout<<"zmin = "<<qtb3.zmin()<<std::endl;
  std::cout<<"zmax = "<<qtb3.zmax()<<std::endl;
  std::cout<<"zmiddle = "<<qtb3.zmiddle()<<std::endl;

  std::cout<<"middle = "<<qtb3.middle()<<std::endl;
  std::cout<<"middle_left = "<<qtb3.middle_left()<<std::endl;
  std::cout<<"middle_right = "<<qtb3.middle_right()<<std::endl;
  std::cout<<"middle_bottom = "<<qtb3.middle_bottom()<<std::endl;
  std::cout<<"middle_upper = "<<qtb3.middle_upper()<<std::endl;
  std::cout<<"middle_front = "<<qtb3.middle_front()<<std::endl;
  std::cout<<"middle_back = "<<qtb3.middle_back()<<std::endl;
  
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "OcTreeBox ascii serialization " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   // create and open a character archive for output
    std::ofstream ofs("qtb.txt");
    {
      boost::archive::text_oarchive oa(ofs);
      oa<<qtb;
    }//to close archive
    std::ifstream ifs("qtb.txt");
    slip::OcTreeBox<slip::Point3d<T> > qtb5;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>qtb5;
    }//to close archive

    std::cout<<"qtb5 = "<<qtb5<<std::endl;

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "OcTreeBox binary serialization " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::ofstream ofsb("qtb.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<qtb;
    }//to close archive

    std::ifstream ifsb("qtb.bin");
    slip::OcTreeBox<slip::Point3d<T> > qtb6;
    {
      boost::archive::binary_iarchive ia(ifsb);
      ia>>qtb6;
    }
    std::cout<<"qtb5 = "<<qtb5<<std::endl;




  return 0;
}
