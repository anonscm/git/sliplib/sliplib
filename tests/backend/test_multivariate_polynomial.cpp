#include "MultivariatePolynomial.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <complex>
#include <algorithm>
#include <numeric>
#include <vector>
#include "Vector.hpp"

#include "arithmetic_op.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


int main()
{

  //***********************************
  //           Monomial
  //----------------------------------
  //          "Constructors"
  //----------------------------------
  slip::Monomial<3> m;
  std::cout<<m<<std::endl;
  std::cout<<"name = "<<m.name()<<std::endl;
  std::cout<<"dim = "<<m.dim()<<std::endl;
  std::cout<<"degree = "<<m.degree()<<std::endl;
  std::cout<<"is_a_constant = "<<m.is_a_constant()<<std::endl;
 
  m.powers[0] = 0;
  m.powers[1] = 0;
  m.powers[2] = 0;
  std::cout<<m<<std::endl;
  std::cout<<"dim = "<<m.dim()<<std::endl;
  std::cout<<"degree = "<<m.degree()<<std::endl;
  std::cout<<"is_a_constant = "<<m.is_a_constant()<<std::endl;

  m.powers[0] = 1;
  m.powers[1] = 2;
  m.powers[2] = 3;
  std::cout<<m<<std::endl;
  std::cout<<"dim = "<<m.dim()<<std::endl;
  std::cout<<"degree = "<<m.degree()<<std::endl;
  std::cout<<"is_a_constant = "<<m.is_a_constant()<<std::endl;

 
  
  slip::Monomial<3> mm;
  mm=m;
  std::cout<<mm<<std::endl;
  std::cout<<"dim = "<<mm.dim()<<std::endl;
  std::cout<<"degree = "<<mm.degree()<<std::endl;
  std::cout<<"is_a_constant = "<<mm.is_a_constant()<<std::endl;
   m.powers[0] = 2;
   m.powers[1] = 3;
   m.powers[2] = 1;

  slip::Monomial<3> mm0;
  mm0.powers[0] = 0;
  mm0.powers[1] = 0;
  mm0.powers[2] = 0;
  std::cout<<mm0<<std::endl;

  slip::Monomial<3> mm1;
  mm1.powers[0] = 1;
  mm1.powers[1] = 0;
  mm1.powers[2] = 0;
  std::cout<<mm1<<std::endl;

  slip::Monomial<3> mm2;
  mm2.powers[0] = 2;
  mm2.powers[1] = 0;
  mm2.powers[2] = 0;
  std::cout<<mm2<<std::endl;
  
  slip::Monomial<3> mm3;
  mm3.powers[0] = 0;
  mm3.powers[1] = 1;
  mm3.powers[2] = 0;
  std::cout<<mm3<<std::endl;

  slip::Monomial<3> mm4;
  mm4.powers[0] = 1;
  mm4.powers[1] = 1;
  mm4.powers[2] = 0;
  std::cout<<mm4<<std::endl;
  
  slip::Monomial<3> mm5;
  mm5.powers[0] = 0;
  mm5.powers[1] = 2;
  mm5.powers[2] = 0;
  std::cout<<mm5<<std::endl;
  

  
  std::cout<<(mm1 < mm2)<<std::endl;
  std::cout<<(mm1 < mm3)<<std::endl;
  std::cout<<(mm1 < mm4)<<std::endl;
  std::cout<<(mm1 < mm5)<<std::endl;
  std::cout<<(mm2 < mm3)<<std::endl;
  std::cout<<(mm2 < mm4)<<std::endl;
  std::cout<<(mm2 < mm5)<<std::endl;
  std::cout<<(mm3 < mm4)<<std::endl;
  std::cout<<(mm3 < mm5)<<std::endl;
  std::cout<<(mm4 < mm5)<<std::endl;

  std::cout<<"Comparisons : "<<std::endl;
  std::cout<<(mm == mm)<<std::endl;
  std::cout<<(mm != mm)<<std::endl;
  std::cout<<(mm == m)<<std::endl;
  std::cout<<(mm != m)<<std::endl;
  
  std::cout<<(mm < mm)<<std::endl;
  std::cout<<(mm > mm)<<std::endl;
  std::cout<<(mm < m)<<std::endl;
  std::cout<<(mm > m)<<std::endl;


  std::cout<<"Multiplies..."<<std::endl;
  slip::Monomial<3> mmm;
  mmm = m*mm;
  std::cout<<mmm<<std::endl;
  std::cout<<"dim = "<<mmm.dim()<<std::endl;
  std::cout<<"degree = "<<mmm.degree()<<std::endl;
  std::cout<<"is_a_constant = "<<mmm.is_a_constant()<<std::endl;
 
 
  //***********************************
  //           MultivariatePolynomial
  //----------------------------------
  //          Constructors
  //----------------------------------
   slip::MultivariatePolynomial<double,3> P0;
 
  std::cout<<P0<<std::endl;
  std::cout<<"name = "<<P0.name()<<std::endl;
  std::cout<<"size = "<<P0.size()<<std::endl;
  std::cout<<"max_size = "<<P0.max_size()<<std::endl;
  std::cout<<"empty = "<<P0.empty()<<std::endl;
  std::cout<<"degree = "<<P0.degree()<<std::endl;
  std::cout<<"order = "<<P0.order()<<std::endl;
  std::cout<<"total_nb_coeff = "<<P0.total_nb_coeff()<<std::endl;
  slip::Vector<double> V0(P0.total_nb_coeff());
  P0.all_coeff(V0.begin(),V0.end());
  std::cout<<V0<<std::endl;

  slip::MultivariatePolynomial<double,3> Pord3;
  Pord3.insert(mm0,1.0);
  Pord3.insert(mm1,2.0);
  Pord3.insert(mm2,3.0);
  Pord3.insert(mm3,4.0);
  Pord3.insert(mm4,5.0);
  Pord3.insert(mm5,6.0);
  std::cout<<"size = "<<Pord3.size()<<std::endl;
  std::cout<<"max_size = "<<Pord3.max_size()<<std::endl;
  std::cout<<"empty = "<<Pord3.empty()<<std::endl;
  std::cout<<"degree = "<<Pord3.degree()<<std::endl;
  std::cout<<"order = "<<Pord3.order()<<std::endl;
  std::cout<<"total_nb_coeff = "<<Pord3.total_nb_coeff()<<std::endl;
  slip::Vector<double> V3(Pord3.total_nb_coeff());
  Pord3.all_coeff(V3.begin(),V3.end());
  std::cout<<V3<<std::endl;
  std::cout<<Pord3<<std::endl;

  slip::MultivariatePolynomial<double,3> Pord(2);
  std::cout<<Pord<<std::endl;
  std::cout<<"size = "<<Pord.size()<<std::endl;
  std::cout<<"max_size = "<<Pord.max_size()<<std::endl;
  std::cout<<"empty = "<<Pord.empty()<<std::endl;
  std::cout<<"degree = "<<Pord.degree()<<std::endl;
  std::cout<<"order = "<<Pord.order()<<std::endl;
  std::cout<<"total_nb_coeff = "<<Pord.total_nb_coeff()<<std::endl;

  slip::Vector<double> A(10);
  slip::iota(A.begin(),A.end(),1.0,1.0);
  slip::MultivariatePolynomial<double,3> Pord2(2,A.begin(),A.end());
  std::cout<<Pord2<<std::endl;
  std::cout<<"size = "<<Pord2.size()<<std::endl;
  std::cout<<"max_size = "<<Pord2.max_size()<<std::endl;
  std::cout<<"empty = "<<Pord2.empty()<<std::endl;
  std::cout<<"degree = "<<Pord2.degree()<<std::endl;
  std::cout<<"order = "<<Pord2.order()<<std::endl;
  std::cout<<"total_nb_coeff = "<<Pord2.total_nb_coeff()<<std::endl;


  slip::MultivariatePolynomial<double,3> P0tmp = P0;
  P0tmp.derivative(1);
  std::cout<<P0tmp<<std::endl;
  std::cout<<"size = "<<P0tmp.size()<<std::endl;
  std::cout<<"max_size = "<<P0tmp.max_size()<<std::endl;
  std::cout<<"empty = "<<P0tmp.empty()<<std::endl;
  std::cout<<"degree = "<<P0tmp.degree()<<std::endl;
  std::cout<<"order = "<<P0tmp.order()<<std::endl;
  std::cout<<"P0.integral(1) "<<std::endl;
  P0.integral(1);
  std::cout<<P0<<std::endl;
  std::cout<<"size = "<<P0.size()<<std::endl;
  std::cout<<"max_size = "<<P0.max_size()<<std::endl;
  std::cout<<"empty = "<<P0.empty()<<std::endl;
  std::cout<<"degree = "<<P0.degree()<<std::endl;
  std::cout<<"order = "<<P0.order()<<std::endl;
  

  slip::MultivariatePolynomial<double,3> P;

  P.insert(m,2.4);
   
  slip::Monomial<3> m2;
  m2.powers[0] = 0;
  m2.powers[1] = 0;
  m2.powers[2] = 0;
  P.insert(m2,2.2);
  

  slip::Monomial<3> m3;
  m3.powers[0] = 1;
  m3.powers[1] = 1;
  m3.powers[2] = 1;
  P.insert(m3,-2.3);
  

  slip::Monomial<3> m4;
  m4.powers[0] = 1;
  m4.powers[1] = 1;
  m4.powers[2] = 1;
  P.insert(m4,-2.6);
  

 
  std::cout<<P<<std::endl;
  std::cout<<"size = "<<P.size()<<std::endl;
  std::cout<<"max_size = "<<P.max_size()<<std::endl;
  std::cout<<"empty = "<<P.empty()<<std::endl;
  std::cout<<"degree = "<<P.degree()<<std::endl;
  std::cout<<"order = "<<P.order()<<std::endl;
  std::cout<<"total_nb_coeff = "<<P.total_nb_coeff()<<std::endl;
  slip::Vector<double> VV(P.total_nb_coeff());
  P.all_coeff(VV.begin(),VV.end());
 
  slip::block<double,3> b = {1.0,2.0,3.0};
  std::cout<<"P("<<b[0]<<","<<b[1]<<","<<b[2]<<") = "<<P.evaluate(b.begin(),b.end())<<std::endl;
  

  //----------------------------------
  //          Arithmetic operators
  //----------------------------------
  std::cout<<"P = \n"<<P<<std::endl;
 
  std::cout<<"arithmetic operators..."<<std::endl;
  P+=4.0;
  std::cout<<"P +=4 \n"<<P<<std::endl;

  P-=4.0;
  std::cout<<"P -=4 \n"<<P<<std::endl;

  P*=2.0;
  std::cout<<"P *=2 \n"<<P<<std::endl;

  P/=2.0;
  std::cout<<"P /=2 \n"<<P<<std::endl;

  std::cout<<"-P = \n"<<-P<<std::endl;


  slip::MultivariatePolynomial<double,3> P2;

  slip::Monomial<3> m21;
  m21.powers[0] = 0;
  m21.powers[1] = 0;
  m21.powers[2] = 0;
  P2.insert(m21,1.0);

  slip::Monomial<3> m22;
  m22.powers[0] = 1;
  m22.powers[1] = 4;
  m22.powers[2] = 3;
  P2.insert(m22,1.0);
 
  slip::Monomial<3> m23;
  m23.powers[0] = 0;
  m23.powers[1] = 1;
  m23.powers[2] = 1;
  P2.insert(m23,1.0);

  std::cout<<"P = \n"<<P<<std::endl;
  std::cout<<"P2 = \n"<<P2<<std::endl;

  P+=P2;
  std::cout<<"P +=P2 \n"<<P<<std::endl;
 
  P-=P2;
  std::cout<<"P -=P2 \n"<<P<<std::endl;


  
  P.derivative(2,1);
  std::cout<<"P.derivative(2,1) "<<std::endl;
  std::cout<<P<<std::endl;

  P.derivative(2);
  std::cout<<"P.derivative(2) "<<std::endl;
  std::cout<<P<<std::endl;
  
  P.integral(2);
  std::cout<<"P.integral(2) "<<std::endl;
  std::cout<<P<<std::endl;
  
  slip::MultivariatePolynomial<double,3> P3;
  slip::Monomial<3> m33;
  m33.powers[0] = 0;
  m33.powers[1] = 0;
  m33.powers[2] = 0;
  P3.insert(m33,2.0);
  std::cout<<"P3 = \n"<<P3<<std::endl;
  P3+=4.0;
  std::cout<<"P3 +=4 \n"<<P3<<std::endl;
  
  P3-=4.0;
  std::cout<<"P3 -=4 \n"<<P3<<std::endl;
 
    
  slip::Monomial<3> m34;
  m34.powers[0] = 2;
  m34.powers[1] = 3;
  m34.powers[2] = 4;
  P3.insert(m34,3.0);
  std::cout<<"P2 = \n"<<P2<<std::endl;
  std::cout<<"P3 = \n"<<P3<<std::endl;

  P3*=P2;
  std::cout<<"P3 *=P2 \n"<<P3<<std::endl;
  P3+=P2;
  std::cout<<"P3 +=P2 \n"<<P3<<std::endl;
  P3-=P2;
  std::cout<<"P3 -=P2 \n"<<P3<<std::endl;
  
  
  slip::MultivariatePolynomial<double,3> P4;
  std::cout<<"P2 = \n"<<P2<<std::endl;
  std::cout<<"P3 = \n"<<P3<<std::endl;
  P4 = P2+P3;
  std::cout<<"P4 = P2 + P3 \n"<<P4<<std::endl;
 
  P4 = P2-P3;
  std::cout<<"P4 = P2 - P3 \n"<<P4<<std::endl;
  
  P4 = P3-P2;
  std::cout<<"P4 = P3 - P2 \n"<<P4<<std::endl;

  P4 = P2*P3;
  std::cout<<"P4 = P2 * P3 \n"<<P4<<std::endl;


  P4 = P2+4.0;
  std::cout<<"P4 = P2 + 4 \n"<<P4<<std::endl;
 
  P4 = 4.0 + P2;
  std::cout<<"P4 = 4 +P2  \n"<<P4<<std::endl;

  P4 = P2-4.0;
  std::cout<<"P4 = P2 - 4 \n"<<P4<<std::endl;
  P4 = 4.0 - P2;
  std::cout<<"P4 = 4  - P2  \n"<<P4<<std::endl;

  P4 = P2*4.0;
  std::cout<<"P4 = P2 * 4 \n"<<P4<<std::endl;
  P4 = 4.0 * P2;
  std::cout<<"P4 = 4  * P2  \n"<<P4<<std::endl;
  
  P4 = P2/4.0;
  std::cout<<"P4 = P2 / 4 \n"<<P4<<std::endl;

  slip::MultivariatePolynomial<double,2> P5;
  slip::Monomial<2> m6;
  m6.powers[0] = 0;
  m6.powers[1] = 0;
  P5.insert(m6,1.0);
  slip::Monomial<2> m7;
  m7.powers[0] = 0;
  m7.powers[1] = 1;
  P5.insert(m7,2.0);
  slip::Monomial<2> m8;
  m8.powers[0] = 0;
  m8.powers[1] = 2;
  P5.insert(m8,3.0);
  slip::Monomial<2> m9;
  m9.powers[0] = 1;
  m9.powers[1] = 2;
  P5.insert(m9,7.0);


  std::cout<<P5<<std::endl;
  std::cout<<P5.total_nb_coeff()<<std::endl;
  slip::Vector<double> V(P5.total_nb_coeff());
  P5.all_coeff(V.begin(),V.end());
  std::cout<<V<<std::endl;

  //***********************************
  //      Complex MultivariatePolynomial
  //----------------------------------
  //          Constructors
  //----------------------------------
  slip::MultivariatePolynomial<std::complex<double>,2> Pc0;
 
  std::cout<<Pc0<<std::endl;
  std::cout<<"size = "<<Pc0.size()<<std::endl;
  std::cout<<"max_size = "<<Pc0.max_size()<<std::endl;
  std::cout<<"empty = "<<Pc0.empty()<<std::endl;
  std::cout<<"degree = "<<Pc0.degree()<<std::endl;
  std::cout<<"order = "<<Pc0.order()<<std::endl;
  std::cout<<"total_nb_coeff = "<<Pc0.total_nb_coeff()<<std::endl;
  slip::Vector<std::complex<double> > Vc0(Pc0.total_nb_coeff());
  Pc0.all_coeff(Vc0.begin(),Vc0.end());
  std::cout<<Vc0<<std::endl;

  slip::Monomial<2> mc1;
  mc1.powers[0] = 0;
  mc1.powers[1] = 0;
  Pc0.insert(mc1,std::complex<double>(1.0,1.0));
  slip::Monomial<2> mc2;
  mc2.powers[0] = 0;
  mc2.powers[1] = 1;
  Pc0.insert(mc2,std::complex<double>(2.0,2.0));
  slip::Monomial<2> mc3;
  mc3.powers[0] = 0;
  mc3.powers[1] = 2;
  Pc0.insert(mc3,std::complex<double>(2.0,3.0));

  std::cout<<Pc0<<std::endl;
  std::cout<<"size = "<<Pc0.size()<<std::endl;
  std::cout<<"max_size = "<<Pc0.max_size()<<std::endl;
  std::cout<<"empty = "<<Pc0.empty()<<std::endl;
  std::cout<<"degree = "<<Pc0.degree()<<std::endl;
  std::cout<<"order = "<<Pc0.order()<<std::endl;
  std::cout<<"total_nb_coeff = "<<Pc0.total_nb_coeff()<<std::endl;
  Vc0.resize(Pc0.total_nb_coeff());
  Pc0.all_coeff(Vc0.begin(),Vc0.end());
  std::cout<<Vc0<<std::endl;
  
  slip::block<std::complex<double>,2> bc;
  bc[0] = std::complex<double>(1.0,1.0);
  bc[1] = std::complex<double>(2.0,2.0);
  std::cout<<bc<<" \nevaluation "<<std::endl;
  std::cout<<"Pc0("<<bc[0]<<","<<bc[1]<<") = "<<Pc0.evaluate(bc.begin(),bc.end())<<std::endl;
  slip::block<std::complex<double>,2> bc2;
  bc2[0] = std::complex<double>(2.0,1.0);
  bc2[1] = std::complex<double>(1.0,0.0);
  std::cout<<"Pc0("<<bc2[0]<<","<<bc2[1]<<") = "<<Pc0.evaluate(bc2.begin(),bc2.end())<<std::endl;
  slip::block<std::complex<double>,2> bc3;
  bc3[0] = std::complex<double>(2.0,1.0);
  bc3[1] = std::complex<double>(0.0,0.0);
  std::cout<<"Pc0("<<bc3[0]<<","<<bc3[1]<<") = "<<Pc0.evaluate(bc3.begin(),bc3.end())<<std::endl;
  slip::block<std::complex<double>,2> bc4;
  bc4[0] = std::complex<double>(2.0,1.0);
  bc4[1] = std::complex<double>(-1.0,0.0);
  std::cout<<"Pc0("<<bc4[0]<<","<<bc4[1]<<") = "<<Pc0.evaluate(bc4.begin(),bc4.end())<<std::endl;

  //----------------------------------
  //          Arithmetic operators
  //----------------------------------
  std::cout<<"arithmetic operators..."<<std::endl;
  std::complex<double> c4(4.0,4.0);
  std::complex<double> c2(2.0,2.0);
  Pc0+=c4;

  std::cout<<Pc0<<std::endl;
  Pc0-=c4;
  std::cout<<Pc0<<std::endl;
  Pc0*=c2;
  std::cout<<Pc0<<std::endl;
  Pc0/=c2;
  std::cout<<Pc0<<std::endl;
  std::cout<<-Pc0<<std::endl;

  slip::MultivariatePolynomial<std::complex<double>,2> Pc3;
  slip::Monomial<2> mc31;
  mc31.powers[0] = 0;
  mc31.powers[1] = 0;
  Pc3.insert(mc31,std::complex<double>(2.0,1.0));
  std::cout<<P3<<std::endl;
     
  slip::Monomial<2> mc32;
  mc32.powers[0] = 2;
  mc32.powers[1] = 3;
  Pc3.insert(mc32,std::complex<double>(3.0,1.0));
  
  std::cout<<Pc3<<std::endl;
  std::cout<<"size = "<<Pc3.size()<<std::endl;
  std::cout<<"max_size = "<<Pc3.max_size()<<std::endl;
  std::cout<<"empty = "<<Pc3.empty()<<std::endl;
  std::cout<<"degree = "<<Pc3.degree()<<std::endl;
  std::cout<<"order = "<<Pc3.order()<<std::endl;
  std::cout<<"total_nb_coeff = "<<Pc3.total_nb_coeff()<<std::endl;
  slip::Vector<std::complex<double> > Vc3(Pc3.total_nb_coeff());
  Pc3.all_coeff(Vc3.begin(),Vc3.end());
  std::cout<<Vc3<<std::endl;


  slip::MultivariatePolynomial<std::complex<double>,2> Pc4;
  Pc4 = Pc0+Pc3;
  std::cout<<Pc4<<std::endl;
  Pc4 = Pc0-Pc3;
  std::cout<<Pc4<<std::endl;
  Pc4 = Pc0*Pc3;
  std::cout<<Pc4<<std::endl;


  slip::MultivariatePolynomial<std::complex<double>,2> Pc5;
  Pc5 = Pc4.partial_evaluate(2,std::complex<double>(3.0,0.0));
  std::cout<<Pc4<<std::endl;
  std::cout<<Pc5<<std::endl;

  
   ////////////////////////////////////////
  //2nd order 3d polynomial fitting
  ////////////////////////////////////////
  
  //construction of the 3d-polynomial data
  std::size_t n = 32;
  slip::Vector<double> x3(n+1);
  slip::iota(x3.begin(),x3.end(),-1.0,1.0/double(n));
  slip::Vector<double> y3(x3);
  slip::Vector<double> z3(x3);
  slip::Vector<slip::Vector<double> > datax3(x3.size()*x3.size()*x3.size());  
  std::size_t k = 0;
  for(std::size_t i = 0; i < y3.size(); ++i)
     {
       for(std::size_t j = 0; j < x3.size(); ++j)
	 {
	   for(std::size_t l = 0; l < z3.size(); ++l)
	     {
	       datax3[k].resize(3);
	       datax3[k][0] = x3[i];
	       datax3[k][1] = y3[j];
	       datax3[k][2] = z3[l];
	       k++;
	     }
	 }
     }
 slip::Vector<double> datay3(datax3.size(),0.0);
 
 for(std::size_t k = 0; k < datax3.size(); ++k)
     {
       double x = datax3[k][0];
       double y = datax3[k][1];
       double z = datax3[k][2];
       datay3[k] =  1.2 + 2.0 * x + 0.5* x*x - 1.0 * y + 0.8 * x *
        y + 0.3 * y * y + 1.1*z*y + 1.32 * z * z;
     }

 std::size_t coeff_size3 = 10;
 slip::Vector<double> coeff3(coeff_size3,1.0);
 
 slip::MultivariatePolynomial<double,3> P3d;
 double err = P3d.fit(datax3,datay3,2);
 std::cout<<P3d<<std::endl;
 std::cout<<"erreur = "<<err<<std::endl;
   
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << " compose " << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 typedef double T;
 std::vector<T> coefQ1(6);
  slip::iota(coefQ1.begin(),coefQ1.end(),1.0);
  slip::MultivariatePolynomial<T,2> Q1(2,coefQ1.begin(),coefQ1.end());
  std::cout<<"Q1 = \n"<<Q1<<std::endl;

  std::vector<T> coefQ2(6);
  slip::iota(coefQ2.begin(),coefQ2.end(),2.0);
  slip::MultivariatePolynomial<T,2> Q2(2,coefQ2.begin(),coefQ2.end());
  std::cout<<"Q2 = \n"<<Q2<<std::endl;

  std::vector<slip::MultivariatePolynomial<T,2> > VP(2);
  VP[0] = Q1;
  VP[1] = Q2;

  std::vector<T> coefS(6);
  slip::iota(coefS.begin(),coefS.end(),3.0);
  slip::MultivariatePolynomial<T,2> S(2,coefS.begin(),coefS.end());
  std::cout<<"S = \n"<<S<<std::endl;
  slip::MultivariatePolynomial<T,2> SrondQ1Q2(2);
  SrondQ1Q2 = S.compose(VP);
  std::cout<<"S(Q1,Q2) = \n"<<SrondQ1Q2<<std::endl;

 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << " Truncate " << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 slip::MultivariatePolynomial<T,2> Pbt(5);
 std::cout<<"Pbt = \n"<<Pbt<<std::endl;
 slip::MultivariatePolynomial<T,2> Pt = Pbt.truncate(2);
 std::cout<<"Pt = \n"<<Pt<<std::endl;
  //
  //serialization
  //
   slip::Monomial<3> smm0;
  smm0.powers[0] = 0;
  smm0.powers[1] = 0;
  smm0.powers[2] = 0;
  std::cout<<smm0<<std::endl;

  slip::Monomial<3> smm1;
  smm1.powers[0] = 1;
  smm1.powers[1] = 0;
  smm1.powers[2] = 0;
  std::cout<<smm1<<std::endl;

  slip::Monomial<3> smm2;
  smm2.powers[0] = 2;
  smm2.powers[1] = 0;
  smm2.powers[2] = 0;
  std::cout<<smm2<<std::endl;
  
  slip::Monomial<3> smm3;
  smm3.powers[0] = 0;
  smm3.powers[1] = 1;
  smm3.powers[2] = 0;
  std::cout<<smm3<<std::endl;

  slip::Monomial<3> smm4;
  smm4.powers[0] = 1;
  smm4.powers[1] = 1;
  smm4.powers[2] = 0;
  std::cout<<smm4<<std::endl;
  
  slip::Monomial<3> smm5;
  smm5.powers[0] = 0;
  smm5.powers[1] = 2;
  smm5.powers[2] = 0;
  std::cout<<smm5<<std::endl;
  slip::MultivariatePolynomial<double,3> Pords3;
  Pords3.insert(smm0,1.0);
  Pords3.insert(smm1,2.0);
  Pords3.insert(smm2,3.0);
  Pords3.insert(smm3,4.0);
  Pords3.insert(smm4,5.0);
  Pords3.insert(smm5,6.0);
  std::cout<<"size = "<<Pords3.size()<<std::endl;
  std::cout<<"max_size = "<<Pords3.max_size()<<std::endl;
  std::cout<<"empty = "<<Pords3.empty()<<std::endl;
  std::cout<<"degree = "<<Pords3.degree()<<std::endl;
  std::cout<<"order = "<<Pords3.order()<<std::endl;
  std::cout<<"total_nb_coeff = "<<Pords3.total_nb_coeff()<<std::endl;
  slip::Vector<double> Vs3(Pords3.total_nb_coeff());
  Pords3.all_coeff(Vs3.begin(),Vs3.end());
  std::cout<<Vs3<<std::endl;
  std::cout<<Pords3<<std::endl;

  // create and open a character archive for output
    std::ofstream ofs("multivariatepoly.txt");
    
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<Pords3;
    }//to close archive
    std::ifstream ifs("multivariatepoly.txt");
    slip::MultivariatePolynomial<double,3> new_p;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_p;
    }//to close archive
    std::cout<<"new_p\n"<<new_p<<std::endl;

    std::ofstream ofsb("multivariatepoly.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<Pords3;
    }//to close archive

    std::ifstream ifsb("multivariatepoly.bin");
     slip::MultivariatePolynomial<double,3> new_pb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_pb;
    }//to close archive
    std::cout<<"new_pb\n"<<new_pb<<std::endl;
    
    return 0;
}
