#include "Matrix.hpp"
#include "MultispectralImage.hpp"
#include "Box2d.hpp"
#include "iterator2d_box.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <complex>
#include <algorithm>
#include <numeric>
#include <vector>
#include "dynamic.hpp"
#include "Block.hpp"

#include "arithmetic_op.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

int main()
{

  //------------------------------
  //constructors
  //------------------------------
  slip::MultispectralImage<double,3> M;
  std::cout<<"name = "<<M.name()<<" dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 

 
  slip::MultispectralImage<double,3> M2(6,3);
  std::cout<<"dim1 = "<<M2.dim1()<<" dim2 = "<<M2.dim2()<<" size = "<<M2.size()<<" empty = "<<M2.empty()<<std::endl; 
  std::cout<<"rows = "<<M2.rows()<<" cols = "<<M2.cols()<<" columns = "<<M2.columns()<<std::endl;
  std::cout<<"height = "<<M2.height()<<" width = "<<M2.width()<<std::endl; 
  std::cout<<M2<<std::endl; 

  slip::block<double,3> a = {1.0,2.0,3.0};
  slip::MultispectralImage<double,3> M3(6,3,a);
  std::cout<<"dim1 = "<<M3.dim1()<<" dim2 = "<<M3.dim2()<<" size = "<<M3.size()<<" empty = "<<M3.empty()<<std::endl; 
  std::cout<<"rows = "<<M3.rows()<<" cols = "<<M3.cols()<<" columns = "<<M3.columns()<<std::endl;
  std::cout<<M3<<std::endl;


 double d[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,
	       14.0,15.0,16.0,17.0,18.0};
 slip::MultispectralImage<double,3> M4(2,3,d);
 std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
 std::cout<<M4<<std::endl; 
 

 slip::Matrix<double> Mat1(4,3,12.0);
 slip::Matrix<double> Mat2(4,3,13.0);
 slip::Matrix<double> Mat3(4,3,14.0);

 std::vector<slip::Matrix<double>::iterator> iterators_list(3);
 iterators_list[0] = Mat1.begin();
 iterators_list[1] = Mat2.begin();
 iterators_list[2] = Mat3.begin();
 
 
slip::MultispectralImage<double,3> Mcol(4,3,iterators_list,Mat1.end());
std::cout<<Mcol<<std::endl; 

// //   std::vector<int> V(20);
// //   for(std::size_t i = 0; i < 20; ++i)
// //     V[i] = i;
// //   slip::MultispectralImage<int> M5(4,5,V.begin(),V.end());
// //   std::cout<<"dim1 = "<<M5.dim1()<<" dim2 = "<<M5.dim2()<<" size = "<<M5.size()<<" empty = "<<M5.empty()<<std::endl; 
// //  std::cout<<M5<<std::endl; 


 slip::MultispectralImage<double,3> M6 = M4;
 std::cout<<"dim1 = "<<M6.dim1()<<" dim2 = "<<M6.dim2()<<" size = "<<M6.size()<<" empty = "<<M6.empty()<<std::endl; 
 std::cout<<M6<<std::endl; 
 //------------------------------

 //------------------------------
 //resize
 //------------------------------
 slip::block<double,3> bm;
 bm.fill(5.6);
 M.resize(4,3,bm);
  std::cout<<"dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 
  //------------------------------


  //------------------------------
  //Affectation
  //------------------------------
  slip::MultispectralImage<double,3> M7;
  M7 = M;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M7 = M7;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M4 = 12.2;
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl;
   
  
  slip::block<double,3> b = {128.0,255.0,127.0};
  M4 = b;
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl;   
  //------------------------------

  
  //------------------------------
  //Element access operators
  //------------------------------
   M2(2,1) = 2.1;
   M2[1][2] = 1.2;
  
  for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2[i][j]<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
   for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2(i,j)<<" ";
	}
      std::cout<<std::endl;
    }
  //------------------------------

   //------------------------------
   //swap
   //------------------------------
   M2.swap(M3);
   std::cout<<M2<<std::endl;
   std::cout<<std::endl;
   std::cout<<M3<<std::endl;
   //------------------------------


   //----------------------------
   //iterators
   //----------------------------
   slip::MultispectralImage<double,3> M8(2,4);
   std::vector<slip::block<double,3> > V8(8);
   for(std::size_t i = 0; i < 8; ++i)
     {
       slip::block<double,3> bb;
       bb.fill(i);
       V8[i] = bb;
     }
   M8.fill(V8.begin(),V8.end());   
   std::cout<<"M8 = \n"<<M8<<std::endl;

   std::copy(M8.begin(),M8.end(),std::ostream_iterator<slip::block<double,3> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M8.rbegin(),M8.rend(),std::ostream_iterator<slip::block<double,3> >(std::cout," "));
   std::cout<<std::endl<<std::endl;
   
   
// //    // std::transform(M3.rbegin(),M3.rend(),M8.begin(),M2.begin(),std::plus<double>());
  
// //    std::cout<<M2<<std::endl;
   

   std::copy(M8.row_begin(1),M8.row_end(1),std::ostream_iterator<slip::block<double,3> >(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;

   std::copy(M8.col_begin(1),M8.col_end(1),std::ostream_iterator<slip::block<double,3> >(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;
   
//    //std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_begin(2),std::plus<double>());
//    //std::cout<<M2<<std::endl;

//    //std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_begin(2),std::plus<double>());
//    //std::cout<<M2<<std::endl;


   std::copy(M8.row_rbegin(1),M8.row_rend(1),std::ostream_iterator<slip::block<double,3> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M8.col_rbegin(1),M8.col_rend(1),std::ostream_iterator<slip::block<double,3> >(std::cout," "));
   std::cout<<std::endl;
   

//    //std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_rbegin(2),std::plus<double>());
//    //std::cout<<M2<<std::endl;

//    //std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_rbegin(2),std::plus<double>());
//    //std::cout<<M2<<std::endl;
   
   
     slip::MultispectralImage<int,3> M100(10,12);
     for(std::size_t i = 0; i < M100.dim1(); ++i)
       {
	 for(std::size_t j = 0; j < M100.dim2(); ++j)
	   {
	     M100[i][j][0] = i + j;
	     M100[i][j][1] = i + j + 1;
	     M100[i][j][2] = i + j + 2;
	       
	     
	   }
       }
     std::cout<<M100<<std::endl;
   
     slip::Box2d<int> box4(1,1,2,2);
     slip::Box2d<int> box5(0,0,1,1);
//      std::cout<<std::inner_product(M100.upper_left(box4),M100.bottom_right(box4),M100.upper_left(box5),0.0)<<std::endl;
     
     slip::MultispectralImage<int,3>::iterator2d it  = M100.upper_left(box4);
     slip::MultispectralImage<int,3>::iterator2d it2 = M100.upper_left(box5);
     
     for(;it != M100.bottom_right(box4); ++it, ++it2)
       {
	 std::cout<<"("<<*it<<" "<<*it2<<") ";
       }
     std::cout<<std::endl;
  

     //first plane
     std::cout<<"first plane "<<std::endl;
     std::copy(M100.begin(0),M100.end(0),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //second plane
     std::cout<<"second plane "<<std::endl;
     std::copy(M100.begin(1),M100.end(1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //third plane
     std::cout<<"third plane "<<std::endl;
     std::copy(M100.begin(2),M100.end(2),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     

//   //----------------------------  

   //------------------------------
  //Comparison operators
  //------------------------------
  std::cout<<" M3 == M2 "<<(M3 == M2)<<std::endl;
  std::cout<<" M3 != M2 "<<(M3 != M2)<<std::endl;
//   std::cout<<" M3 > M2 "<<(M3 > M2)<<std::endl;
//   std::cout<<" M3 < M2 "<<(M3 < M2)<<std::endl;
//   std::cout<<" M3 >= M2 "<<(M3 >= M2)<<std::endl;
//   std::cout<<" M3 <= M2 "<<(M3 <= M2)<<std::endl;
  //------------------------------


  //------------------------------
  //fill methods
  //------------------------------
  
  slip::block<double,3> b3;
  b3.fill(4.0);
  M3.fill(b3);
  std::cout<<M3<<std::endl;
  slip::block<double,3> bbb = {2.0,3.0,6.0};
  M3.fill(bbb);
  std::cout<<M3<<std::endl;
  double d2[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0};
  M3.fill(d2);
  std::cout<<M3<<std::endl;
  std::cout<<std::endl;
  std::vector<slip::block<double,3> > V2(18);
  for(std::size_t i = 0; i < 18; ++i)
    {
      slip::block<double,3> bbbb;
      bbbb.fill(i);
      V2[i] = bbbb;
    }
  M3.fill(V2.begin(),V2.end());
  std::cout<<M3<<std::endl;
  //------------------------------
  
 
   
  std::cout<<" mins(M100) = "<<M100.min()<<std::endl;
  std::cout<<" maxs(M100) = "<<M100.max()<<std::endl;

  //
  //serialization
  //
   // create and open a character archive for output
    std::ofstream ofs("multispectralimage.txt");
    slip::MultispectralImage<float,4> bs(3,4);
    for(std::size_t i = 0; i < bs.rows(); ++i)
      {
	for(std::size_t j = 0; j < bs.cols(); ++j)
	  {
	    bs[i][j][0] = i+j;
	    bs[i][j][1] = i+j+1;
	    bs[i][j][2] = i+j+2;
	    bs[i][j][3] = i+j+3;
	    
	  }
      }
       
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<bs;
    }//to close archive
    std::ifstream ifs("multispectralimage.txt");
     slip::MultispectralImage<float,4>  new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("multispectralimage.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<bs;
    }//to close archive

    std::ifstream ifsb("multispectralimage.bin");
    slip::MultispectralImage<float,4> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;
    

     return 0;
}
