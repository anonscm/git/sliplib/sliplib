#include <iostream>
#include <iomanip>
#include <vector>
#include <complex>
#include <random>
#include "OLearyPolyBase1d.hpp"
#include "Array.hpp"
#include "Vector.hpp"
#include "Polynomial.hpp"
#include "polynomial_algo.hpp"
#include "arithmetic_op.hpp"
#include "compare.hpp"
#include "Vector2d.hpp"
#include "discrete_polynomial_algos.hpp"



int main()
{
  typedef double T;
  
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "default base construction " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::OLearyPolyBase1d<T> base1;
  std::cout<<"base1 = \n"<<base1<<std::endl;

    
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "base construction and generation" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  std::size_t range_size = 10;
 
  //std::size_t order = 9;
  //std::size_t order = 5;

 
  //slip::OLearyPolyBase1d<T> base2(range_size);
  //slip::OLearyPolyBase1d<T> base2(range_size,slip::BASE_REORTHOGONALIZATION_GRAM_SCHMIDT);
  slip::OLearyPolyBase1d<T> base2(range_size,slip::BASE_REORTHOGONALIZATION_NONE);
  
  base2.generate();
  std::cout<<"base2 = \n"<<base2<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Orthogonality test" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Matrix<double> Gram2;
  base2.orthogonality_matrix(Gram2);
  std::cout<<"orthogonality matrix:\n"<<Gram2<<std::endl;
  std::cout<<"orthogonality test (1e-14): "<<base2.is_orthogonal(1e-14)<<std::endl;
  std::cout<<"orthogonality precision: "<<base2.orthogonality_precision()<<std::endl;

 

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "copy constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::OLearyPolyBase1d<T> base3(base2);
   std::cout<<"base3 = \n"<<base3<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "operator=" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::OLearyPolyBase1d<T> base4;
  base4 = base2;
  std::cout<<"base4 = \n"<<base4<<std::endl;



  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "get_base" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<T> base2_matrix;
  base2.get_base(base2_matrix);
  std::cout<<"base2_matrix = \n"<<base2_matrix<<std::endl;

  
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "poly_iterator" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  for(std::size_t i = 0; i < base2.size(); ++i)
    {
      std::copy(base2.poly_begin(i),base2.poly_end(i),std::ostream_iterator<T>(std::cout," "));
      std::cout<<std::endl;
    }
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "reverse poly_iterator" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  for(std::size_t i = 0; i < base2.size(); ++i)
    {
      std::copy(base2.poly_rbegin(i),base2.poly_rend(i),std::ostream_iterator<T>(std::cout," "));
      std::cout<<std::endl;
    }

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Gram condition number " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<base2.gram_cond()<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "condition number " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<base2.cond()<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "polynomial matrix rank " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<base2.rank()<<std::endl;

 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "projection of data onto the basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Array<double> A(range_size);
  slip::iota(A.begin(),A.end(),1.0,1.0);
  std::cout<<"A = \n"<<A<<std::endl;
  
  slip::Array<double> coef(base2.size());

  base2.project(A.begin(),A.end(),
		coef.begin(),coef.end());
  std::cout<<"coef = \n"<<coef<<std::endl;
  
  for(std::size_t k = 0; k < base2.size(); ++k)
    {
      std::cout<<"(A,P"<<k<<") = "<<base2.project(A.begin(),A.end(),k)<<std::endl;
    }
 
 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "reconstruction of the data from the basis" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Array<double> Ar(A.size());
  std::size_t K = A.size();
  base2.reconstruct(coef.begin(),coef.end(),
			    K,
			    Ar.begin(),
			    Ar.end());
  std::cout<<"Ar = \n"<<Ar<<std::endl;

 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "reconstruction tests" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<double> A_cte(range_size,T(5.12));
  std::cout<<"A_cte = \n"<<A_cte<<std::endl;
  
  slip::Array<double> coef_cte(base2.size());
  base2.project(A_cte.begin(),A_cte.end(),
		coef_cte.begin(),coef_cte.end());
  std::cout<<"coef_cte = \n"<<coef_cte<<std::endl;

  slip::Array<double> Ar_cte(A.size());
  std::size_t K_cte = A_cte.size();
  base2.reconstruct(coef_cte.begin(),coef_cte.end(),
		    K_cte,
		    Ar_cte.begin(),
		    Ar_cte.end());
  std::cout<<"Ar_cte = \n"<<Ar_cte<<std::endl;

  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(0.0,1.0);

  slip::Vector<double> A_rnd(range_size);
  for(std::size_t i = 0; i < A_rnd.size(); ++i)
    {
      A_rnd[i] = distribution(generator);
    }
  
  
  std::cout<<"A_rnd = \n"<<A_rnd<<std::endl;
  slip::Array<double> coef_rnd(base2.size());
  base2.project(A_rnd.begin(),A_rnd.end(),
		coef_rnd.begin(),coef_rnd.end());
  std::cout<<"coef_rnd = \n"<<coef_rnd<<std::endl;

  slip::Array<double> Ar_rnd(A.size());
  std::size_t K_rnd = A_rnd.size();
  base2.reconstruct(coef_rnd.begin(),coef_rnd.end(),
		    K_rnd,
		    Ar_rnd.begin(),
		    Ar_rnd.end());
  std::cout<<"Ar_rnd = \n"<<Ar_rnd<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "big base test" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  const std::size_t big_size = 2048;
  std::cout<<"big_size = "<<big_size<<std::endl;
  slip::OLearyPolyBase1d<T> big_base(big_size);
  //slip::OLearyPolyBase1d<T> big_base(big_size,slip::BASE_REORTHOGONALIZATION_GRAM_SCHMIDT);
  //slip::OLearyPolyBase1d<T> big_base(big_size,slip::BASE_REORTHOGONALIZATION_SVD);
  //slip::OLearyPolyBase1d<T> big_base(big_size,slip::BASE_REORTHOGONALIZATION_PROCRUSTES);
  //slip::OLearyPolyBase1d<T> big_base(big_size,slip::BASE_REORTHOGONALIZATION_NONE);
  big_base.generate();
 
  slip::Vector<double> A_bigrnd(big_size);
  for(std::size_t i = 0; i < A_bigrnd.size(); ++i)
    {
      A_bigrnd[i] = distribution(generator);
    }
 
  slip::Array<double> coef_bigrnd(big_base.size());
  big_base.project(A_bigrnd.begin(),A_bigrnd.end(),
  		coef_bigrnd.begin(),coef_bigrnd.end());
  //std::cout<<"coef_bigrnd = \n"<<coef_bigrnd<<std::endl;

  slip::Array<double> Ar_bigrnd(A_bigrnd.size());
  std::size_t K_bigrnd = coef_bigrnd.size();
  big_base.reconstruct(coef_bigrnd.begin(),coef_bigrnd.end(),
  		    K_bigrnd,
  		    Ar_bigrnd.begin(),
  		    Ar_bigrnd.end());
  
  //std::cout<<"A_bigrnd = \n"<<A_bigrnd<<std::endl;
  //std::cout<<"Ar_bigrnd = \n"<<Ar_bigrnd<<std::endl;
  std::cout<<"L2 reconstruction error = "<<slip::mean_square_diff<double>(A_bigrnd.begin(),A_bigrnd.end(),Ar_bigrnd.begin())<<std::endl;
  //  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "Gram condition number " << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout<<big_base.gram_cond()<<std::endl;
  
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "condition number " << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout<<big_base.cond()<<std::endl;

  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "polynomial matrix rank " << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout<<big_base.rank()<<std::endl;
  return 0;
}
