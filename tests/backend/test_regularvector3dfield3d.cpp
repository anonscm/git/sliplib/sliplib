#include "Matrix3d.hpp"
#include "RegularVector3dField3d.hpp"
//#include "Box2d.hpp"
//#include "iterator2d_box.hpp"
#include <iostream>
#include <fstream>
#include <complex>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iomanip>
#include "dynamic.hpp"
#include "Vector3d.hpp"
#include "arithmetic_op.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
int main()
{

  //------------------------------
  //constructors
  //------------------------------
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "constructors " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::RegularVector3dField3d<double> M;
  std::cout<<"name = "<<M.name()<<" dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" dim3 = "<<M.dim3()<<" size = "<<M.size()<<" slice_size = "<<M.slice_size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 

 
  slip::RegularVector3dField3d<double> M2(6,3,4);
  std::cout<<"dim1 = "<<M2.dim1()<<" dim2 = "<<M2.dim2()<<" dim3 = "<<M2.dim3()<<" size = "<<M2.size()<<" slice_size = "<<M2.slice_size()<<" max_size = "<<M2.max_size()<<" empty = "<<M2.empty()<<std::endl;
  std::cout<<"slices = "<<M2.slices()<<" rows = "<<M2.rows()<<" cols = "<<M2.cols()<<" columns = "<<M2.columns()<<std::endl;
  std::cout<<M2<<std::endl; 
 
  slip::RegularVector3dField3d<double> M3(6,3,4,slip::Vector3d<double>(1.0,2.0,3.0));
 std::cout<<"dim1 = "<<M3.dim1()<<" dim2 = "<<M3.dim2()<<" dim3 = "<<M3.dim3()<<" size = "<<M3.size()<<" slice_size = "<<M3.slice_size()<<" max_size = "<<M3.max_size()<<" empty = "<<M3.empty()<<std::endl; 
  std::cout<<M3<<std::endl; 


 double d[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0,19.0,20.0,21.0,22.0,23.0,24.0,25.0,26.0,27.0,28.0,29.0,30.0,31.0,32.0,33.0,34.0,35.0,36.0};
   slip::RegularVector3dField3d<double> M4(2,2,3,d);
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" dim3 = "<<M4.dim3()<<" size = "<<M4.size()<<" slice_size = "<<M4.slice_size()<<" max_size = "<<M4.max_size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl; 
 

 slip::Matrix3d<double> Mat1(2,4,3,12.0);
 slip::Matrix3d<double> Mat2(2,4,3,13.0);
 slip::Matrix3d<double> Mat3(2,4,3,14.0);
 
slip::RegularVector3dField3d<double> Mcol(2,4,3,Mat1.begin(),Mat1.end(),Mat2.begin(),Mat3.begin());
std::cout<<Mcol<<std::endl; 


 slip::RegularVector3dField3d<double> M6 = M4;
 std::cout<<"dim1 = "<<M6.dim1()<<" dim2 = "<<M6.dim2()<<" dim3 = "<<M6.dim3()<<" size = "<<M6.size()<<" slice_size = "<<M6.slice_size()<<" max_size = "<<M6.max_size()<<" empty = "<<M6.empty()<<std::endl; 
  std::cout<<M6<<std::endl; 

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "constructors with init point and grid step" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::RegularVector3dField3d<double> M10(2,4,3,slip::Point3d<double>(0.2,0.4,0.6),slip::Point3d<double>(1.1,1.2,1.3));
 std::cout<<"dim1 = "<<M10.dim1()<<" dim2 = "<<M10.dim2()<<" dim3 = "<<M10.dim3()<<" size = "<<M10.size()<<" empty = "<<M10.empty()<<std::endl; 
 std::cout<<"init point = "<<M10.get_init_point()<<std::endl;
 std::cout<<"grid step = "<<M10.get_grid_step()<<std::endl;
 std::cout<<"M10 = \n"<<M10<<std::endl;


 slip::RegularVector3dField3d<double> M11(2,4,3,slip::Point3d<double>(0.2,0.4,0.6),slip::Point3d<double>(1.1,1.2,1.3),3.3);
 std::cout<<"M11 = \n"<<M11<<std::endl;
  
 slip::RegularVector3dField3d<double> M12(2,4,3,slip::Point3d<double>(0.2,0.4,0.6),slip::Point3d<double>(1.1,1.2,1.3),slip::Vector3d<double>(2.2,4.4,5.5));
 std::cout<<"M12 = \n"<<M12<<std::endl;

 double dp[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0,19.0,20.0,21.0,22.0,23.0,24.0,25.0,26.0,27.0,28.0,29.0,30.0,31.0,32.0,33.0,34.0,35.0,36.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0,19.0,20.0,21.0,22.0,23.0,24.0,25.0,26.0,27.0,28.0,29.0,30.0,31.0,32.0,33.0,34.0,35.0,36.0};
 slip::RegularVector3dField3d<double> M13(2,4,3,slip::Point3d<double>(0.2,0.4,0.6),slip::Point3d<double>(1.1,1.2,1.3),dp);
 std::cout<<"M13 = \n"<<M13<<std::endl;
 
 slip::RegularVector3dField3d<double> M14(2,4,3,slip::Point3d<double>(0.2,0.4,0.6),slip::Point3d<double>(1.1,1.2,1.3),dp,dp+24);
 std::cout<<"M14 = \n"<<M14<<std::endl;
 slip::RegularVector3dField3d<double> M15(2,4,3,slip::Point3d<double>(0.2,0.4,0.6),slip::Point3d<double>(1.1,1.2,1.3),Mat1.begin(),Mat1.end(),Mat2.begin(),Mat3.begin());
 std::cout<<"M15 = \n"<<M15<<std::endl;

slip::RegularVector3dField3d<int,double> MMMMM(2,4,3);
 //------------------------------

 //------------------------------
 //resize
 //------------------------------
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "resize " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

 M.resize(4,3,1,5.6);
  std::cout<<"dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" dim3 = "<<M.dim3()<<" size = "<<M.size()<<" slice_size = "<<M.slice_size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  
  std::cout<<M<<std::endl; 

  //------------------------------


  //------------------------------
  //Affectation
  //------------------------------
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "assignment " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::RegularVector3dField3d<double> M7;
  M7 = M11;
   std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" dim3 = "<<M7.dim3()<<" size = "<<M7.size()<<" slice_size = "<<M7.slice_size()<<" max_size = "<<M7.max_size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl; 

  M7 = M7;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" dim3 = "<<M7.dim3()<<" size = "<<M7.size()<<" slice_size = "<<M7.slice_size()<<" max_size = "<<M7.max_size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl; 


  M4 = 12.2;
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" dim3 = "<<M4.dim3()<<" size = "<<M4.size()<<" slice_size = "<<M4.slice_size()<<" max_size = "<<M4.max_size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl; 
  
  M4 = slip::Vector3d<double> (128.0,255.0,127.0);
    std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" dim3 = "<<M4.dim3()<<" size = "<<M4.size()<<" slice_size = "<<M4.slice_size()<<" max_size = "<<M4.max_size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl; 
  //------------------------------

  
  //------------------------------
  //Element access operators
  //------------------------------
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "element access operators " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

   M2(2,1,0) = 2.1;
   M2[1][2][0] = 1.2;
  
  for(size_t k = 0; k < M2.dim1(); ++k)
    {
      for(size_t i = 0; i < M2.dim2(); ++i)
	{
	  for(size_t j = 0; j < M2.dim3(); ++j)
	    {
	      std::cout<<M2[k][i][j]<<" ";
	    }
	  std::cout<<std::endl;
	}
      std::cout<<"--"<<std::endl;
    }
  std::cout<<std::endl;

  for(size_t k = 0; k < M2.dim1(); ++k)
    {
      for(size_t i = 0; i < M2.dim2(); ++i)
	{
	  for(size_t j = 0; j < M2.dim3(); ++j)
	    {
	      std::cout<<M2(k,i,j)<<" ";
	    }
	  std::cout<<std::endl;
	}
      std::cout<<"--"<<std::endl;
    }
  slip::Matrix3d<double> Mat4(2,4,3);
  slip::Matrix3d<double> Mat5(2,4,3);
  slip::Matrix3d<double> Mat6(2,4,3);
  slip::iota(Mat4.begin(),Mat4.end(),1.0,1.0);
  slip::iota(Mat5.begin(),Mat5.end(),2.0,2.0);
  slip::iota(Mat6.begin(),Mat6.end(),3.0,3.0);
 
  slip::RegularVector3dField3d<double> M16(2,4,3,slip::Point3d<double>(0.2,0.4,0.6),slip::Point3d<double>(1.1,1.2,1.3),Mat4.begin(),Mat4.end(),Mat5.begin(),Mat6.begin());
 std::cout<<"M16 = \n"<<M16<<std::endl;
 for(size_t k = 0; k < M16.dim1(); ++k)
    {
      for(size_t i = 0; i < M16.dim2(); ++i)
	{
	  for(size_t j = 0; j < M16.dim3(); ++j)
	    {
	      //std::cout<<M16.Vx1(k,i,j)<<" ";
	      std::cout<<M16.u(k,i,j)<<" ";
	    }
	  std::cout<<std::endl;
	}
      std::cout<<"--"<<std::endl;
    }
  std::cout<<std::endl;

   for(size_t k = 0; k < M16.dim1(); ++k)
    {
      for(size_t i = 0; i < M16.dim2(); ++i)
	{
	  for(size_t j = 0; j < M16.dim3(); ++j)
	    {
	      //	      std::cout<<M16.Vx2(k,i,j)<<" ";
	      std::cout<<M16.v(k,i,j)<<" ";
	    }
	  std::cout<<std::endl;
	}
      std::cout<<"--"<<std::endl;
    }
  std::cout<<std::endl;

   for(size_t k = 0; k < M16.dim1(); ++k)
    {
      for(size_t i = 0; i < M16.dim2(); ++i)
	{
	  for(size_t j = 0; j < M16.dim3(); ++j)
	    {
	      //std::cout<<M16.Vx3(k,i,j)<<" ";
	      std::cout<<M16.w(k,i,j)<<" ";
	    }
	  std::cout<<std::endl;
	}
      std::cout<<"--"<<std::endl;
    }
  std::cout<<std::endl;

for(size_t k = 0; k < M16.dim1(); ++k)
    {
      for(size_t i = 0; i < M16.dim2(); ++i)
	{
	  for(size_t j = 0; j < M16.dim3(); ++j)
	    {
	      std::cout<<M16.x(k,i,j)<<" ";
	    }
	  std::cout<<std::endl;
	}
      std::cout<<"--"<<std::endl;
    }
  std::cout<<std::endl;

   for(size_t k = 0; k < M16.dim1(); ++k)
    {
      for(size_t i = 0; i < M16.dim2(); ++i)
	{
	  for(size_t j = 0; j < M16.dim3(); ++j)
	    {
	      std::cout<<M16.y(k,i,j)<<" ";
	    }
	  std::cout<<std::endl;
	}
      std::cout<<"--"<<std::endl;
    }
  std::cout<<std::endl;

   for(size_t k = 0; k < M16.dim1(); ++k)
    {
      for(size_t i = 0; i < M16.dim2(); ++i)
	{
	  for(size_t j = 0; j < M16.dim3(); ++j)
	    {
	      std::cout<<M16.z(k,i,j)<<" ";
	    }
	  std::cout<<std::endl;
	}
      std::cout<<"--"<<std::endl;
    }
  std::cout<<std::endl;

  //------------------------------

   //------------------------------
   //swap
   //------------------------------
   M2.swap(M3);
   std::cout<<M2<<std::endl;
   std::cout<<std::endl;
   std::cout<<M3<<std::endl;
   //------------------------------


   //----------------------------
   //iterators
   //----------------------------
   slip::RegularVector3dField3d<double> M8(2,4,2);
   std::vector<slip::Vector3d<double> > V8(16);
   for(std::size_t i = 0; i < 16; ++i)
     V8[i] = slip::Vector3d<double>(i,i,i);
    
   M8.fill(V8.begin(),V8.end());   
   std::cout<<"M8 = \n"<<M8<<std::endl;

   std::copy(M8.begin(),M8.end(),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M8.rbegin(),M8.rend(),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl<<std::endl;
   
   slip::Matrix3d<double> mat(2,4,2,7.7);
   M8.fill(1,mat.begin(),mat.end());
   std::cout<<M8<<std::endl;
//    // std::transform(M3.rbegin(),M3.rend(),M8.begin(),M2.begin(),std::plus<double>());
  
//    std::cout<<M2<<std::endl;
   

//    std::copy(M8.row_begin(1),M8.row_end(1),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
//    std::cout<<std::endl;
//    std::cout<<std::endl;

//    std::copy(M8.col_begin(1),M8.col_end(1),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
//    std::cout<<std::endl;
//    std::cout<<std::endl;
   
//    //std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_begin(2),std::plus<double>());
//    //std::cout<<M2<<std::endl;

//    //std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_begin(2),std::plus<double>());
//    //std::cout<<M2<<std::endl;


 //   std::copy(M8.row_rbegin(1),M8.row_rend(1),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
//    std::cout<<std::endl;
//    std::copy(M8.col_rbegin(1),M8.col_rend(1),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
//    std::cout<<std::endl;
   
   
      

//    //std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_rbegin(2),std::plus<double>());
//    //std::cout<<M2<<std::endl;

//    //std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_rbegin(2),std::plus<double>());
//    //std::cout<<M2<<std::endl;
   
   
     slip::RegularVector3dField3d<int> M100(10,12,2);
     for(std::size_t k = 0; k < M100.dim1(); ++k)
       {
	 for(std::size_t i = 0; i < M100.dim2(); ++i)
	   {
	     for(std::size_t j = 0; j < M100.dim3(); ++j)
	       {
		 
		 M100[k][i][j][0] = k + i + j;
		 M100[k][i][j][1] = k + i + j + 1;
		 M100[k][i][j][2] = k + i + j + 2;
	       }
	     
	   }
       }
     std::cout<<M100<<std::endl;
   
  //    slip::Box2d<int> box4(1,1,2,2);
//      slip::Box2d<int> box5(0,0,1,1);
// //      std::cout<<std::inner_product(M100.upper_left(box4),M100.bottom_right(box4),M100.upper_left(box5),0.0)<<std::endl;
     
//      slip::RegularVector3dField3d<slip::Vector3d<int> >::iterator2d it  = M100.upper_left(box4);
//      slip::RegularVector3dField3d<slip::Vector3d<int> >::iterator2d it2 = M100.upper_left(box5);
     
//      for(;it != M100.bottom_right(box4); ++it, ++it2)
//        {
// 	 std::cout<<"("<<*it<<" "<<*it2<<") ";
//        }
//      std::cout<<std::endl;
  

     //first plane
     std::cout<<"first plane "<<std::endl;
     std::copy(M100.begin(0),M100.end(0),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //second plane
     std::cout<<"second plane "<<std::endl;
     std::copy(M100.begin(1),M100.end(1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //third plane
     std::cout<<"third plane "<<std::endl;
     std::copy(M100.begin(2),M100.end(2),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     

//      std::cout<<"first plane, row 1 "<<std::endl;
//      std::copy(M100.row_begin(0,1),M100.row_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
//      std::cout<<"first plane, reverse row 1 "<<std::endl;
//      std::copy(M100.row_rbegin(0,1),M100.row_rend(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;

//    //    std::cout<<"first plane, col 1 "<<std::endl;
// //       std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
// //      std::cout<<std::endl;
     
// //      std::cout<<"first plane, reverse col 1 "<<std::endl;
// //      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));

//   std::cout<<"second plane, row 1 "<<std::endl;
//      std::copy(M100.row_begin(1,1),M100.row_end(1,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
//      std::cout<<"second plane, reverse row 1 "<<std::endl;
//      std::copy(M100.row_rbegin(1,1),M100.row_rend(1,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;

//     //  std::cout<<"first plane, col 1 "<<std::endl;
// //      std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
// //      std::cout<<std::endl;
     
// //      std::cout<<"first plane, reverse col 1 "<<std::endl;
// //      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));


//        std::cout<<"third plane, row 1 "<<std::endl;
//      std::copy(M100.row_begin(2,1),M100.row_end(2,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
//      std::cout<<"third plane, reverse row 1 "<<std::endl;
//      std::copy(M100.row_rbegin(2,1),M100.row_rend(2,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;

//     //  std::cout<<"first plane, col 1 "<<std::endl;
// //      std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
// //      std::cout<<std::endl;
     
// //      std::cout<<"first plane, reverse col 1 "<<std::endl;
// //      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));

     

//   //----------------------------  

   //------------------------------
  //Comparison operators
  //------------------------------
  std::cout<<" M3 == M2 "<<(M3 == M2)<<std::endl;
  std::cout<<" M3 != M2 "<<(M3 != M2)<<std::endl;
//   std::cout<<" M3 > M2 "<<(M3 > M2)<<std::endl;
//   std::cout<<" M3 < M2 "<<(M3 < M2)<<std::endl;
//   std::cout<<" M3 >= M2 "<<(M3 >= M2)<<std::endl;
//   std::cout<<" M3 <= M2 "<<(M3 <= M2)<<std::endl;
  //------------------------------


  //------------------------------
  //fill methods
  //------------------------------
  
  M.fill(4.0);
  std::cout<<M<<std::endl;
  M.fill(slip::Vector3d<double>(2.0,3.0,6.0));
  std::cout<<M<<std::endl;
  double d2[] =  {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,
	       14.0,15.0,16.0,17.0,18.0,19.0,20.0,21.0,22.0,23.0,24.0,25.0,26.0,27.0,28.0,29.0,30.0,31.0,32.0,33.0,34.0,35.0,36.0};
  M.fill(d2);
  std::cout<<M<<std::endl;
  std::cout<<std::endl;
  std::vector<slip::Vector3d<double> > V2(12);
  for(std::size_t i = 0; i < 12; ++i)
    V2[i] = slip::Vector3d<double>(i,i,i);
    
  M.fill(V2.begin(),V2.end());
  std::cout<<M<<std::endl;
  //------------------------------
  

  //------------------------------
   // Artithmetic an mathematical operators
   //------------------------------   
  std::cout<<"Test arithmetic and mathematical operators..."<<std::endl;
  //  slip::RegularVector3dField3d<float> Mat(4,3,2.0);
       
  slip::RegularVector3dField3d<float> MM(3,2,1);
  MM = 5.5;
  std::cout<<MM<<std::endl;
   
  slip::RegularVector3dField3d<float> MM2(3,2,1);
  MM2 = 2.2;
  std::cout<<MM2<<std::endl;
   
  MM += MM2;
  std::cout<<MM<<std::endl;
   
   MM -= MM2;
   std::cout<<MM<<std::endl;
   
   MM *= MM2;
   std::cout<<MM<<std::endl;
   
   MM /= MM2;
   std::cout<<MM<<std::endl;
   
   slip::RegularVector3dField3d<float> MM3(3,2,1);
   MM3 = MM + MM2;
   std::cout<<MM<<"+"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM - MM2;
   std::cout<<MM<<"-"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM * MM2;
   std::cout<<MM<<"*"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM / MM2;
   std::cout<<MM<<"/"<<MM2<<" = "<<MM3<<std::endl;
  
   std::cout<<"-MM3 = "<<-MM3<<std::endl;
   

 

   std::cout<<"M2 "<<std::endl;
   std::cout<<MM2<<std::endl;
   MM2+=1.1;
   std::cout<<MM2<<std::endl;
   MM2-=1.1;
   std::cout<<MM2<<std::endl;
   MM2*=1.1;
   std::cout<<MM2<<std::endl;
   MM2/=1.1;
   std::cout<<MM2<<std::endl;

   slip::Vector3d<float> vvv(4.4,4.4,4.4);
   std::cout<<"M2 "<<std::endl;
   std::cout<<MM2<<std::endl;
   MM2+=vvv;
   std::cout<<MM2<<std::endl;
   MM2-=vvv;
   std::cout<<MM2<<std::endl;
   MM2*=vvv;
   std::cout<<MM2<<std::endl;
   MM2/=vvv;
   std::cout<<MM2<<std::endl;
   
  std::cout<<" mins(M100) = "<<M100.min()<<std::endl;
  std::cout<<" maxs(M100) = "<<M100.max()<<std::endl;

   

   slip::RegularVector3dField3d<double> MM5(2,2,3);
   slip::Vector3d<double> vv(2.0,2.0,2.0);
   MM5 = M4 + M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5+vv<<std::endl;
   std::cout<<(vv+MM5)<<std::endl;


   MM5 = M4 - M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5-vv<<std::endl;
   std::cout<<(vv-MM5)<<std::endl;
 

   MM5 = M4 * M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5*vv<<std::endl;
   std::cout<<(vv*MM5)<<std::endl;
 

   MM5 = M4 / M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5/vv<<std::endl;
    

   
   std::cout<<MM5<<std::endl;
   std::cout<<MM5+2.0<<std::endl;
   std::cout<<(2.0+MM5)<<std::endl;

   std::cout<<MM5<<std::endl;
   std::cout<<MM5-2.0<<std::endl;
   std::cout<<(2.0-MM5)<<std::endl;
   
   std::cout<<MM5<<std::endl;
   std::cout<<MM5*2.0<<std::endl;
   std::cout<<(2.0*MM5)<<std::endl;
     
   std::cout<<MM5<<std::endl;
   std::cout<<MM5/2.0<<std::endl;
   //std::cout<<(2.0/MM5)<<std::endl;
 
   //M4.apply(std::sqrt);
   //std::cout<<M4<<std::endl;

   slip::Matrix3d<double> Norm(2,2,3);
   std::transform(MM5.begin(),MM5.end(),Norm.begin(),std::mem_fun_ref(&slip::Vector3d<double>::Euclidean_norm));
   std::cout<<"Norm ;"<<std::endl;
   std::cout<<Norm<<std::endl;

   std::cout<<" mins(M100) = "<<M100.min()<<std::endl;
   std::cout<<" maxs(M100) = "<<M100.max()<<std::endl;

    
 //------------------------------

   // M4.write_gnuplot("test3d3d.gnu");

   M4.write_tecplot("test3d3d.tecplot","titre","zone");


   //------------------------------

   slip::Array3d<double> Matrice(3,6,7,0.0);
   slip::iota(Matrice.begin(),Matrice.end(),1.0,1.0);

   
   //copy M three time : in the first plane, in the second plane the third plane of VFM
   slip::RegularVector3dField3d<double> VFM(3,6,7,Matrice.begin(),Matrice.end(),Matrice.begin(),Matrice.begin());
   slip::Matrix3d<double> Result(3,6,7,0.0);
 std::cout<<std::endl;
 std::cout<<"-------------------------------------------"<<std::endl;
 std::cout<<"-- GENERIC DERIVATION + BORDER TREATMENT --"<<std::endl;
 std::cout<<"-------------------------------------------"<<std::endl;

 std::size_t plane = 0;
 std::cout<<"Matrice="<<std::endl;
 std::cout<<Matrice<<std::endl;
 std::cout<<"-- direction X, ordre 1 (der), ordre 1 (sch), shift 1 --"<<std::endl;
 VFM.derivative(plane,slip::X_DIRECTION,1,1,Result);
 std::cout<<"Result="<<std::endl;
 std::cout<<Result<<std::endl;
 std::cout<<"-- direction X, ordre 1 (der), ordre 2 (sch), shift 1 --"<<std::endl;
   
VFM.derivative(plane,slip::X_DIRECTION,1,2,Result);
 std::cout<<"Result="<<std::endl;
 std::cout<<Result<<std::endl;
  std::cout<<"-- direction Y, ordre 1 (der), ordre 1 (sch), shift 1 --"<<std::endl;
  VFM.derivative(plane,slip::Y_DIRECTION,1,1,Result);
 std::cout<<"Result="<<std::endl;
 std::cout<<Result<<std::endl;
 std::cout<<"-- direction Y, ordre 1 (der), ordre 2 (sch), shift 1 --"<<std::endl;
 VFM.derivative(plane,slip::Y_DIRECTION,1,2,Result);
 std::cout<<"Result="<<std::endl;
 std::cout<<Result<<std::endl;
 std::cout<<"-- direction Z, ordre 1 (der), ordre 3 (sch), shift 1 --"<<std::endl;

   VFM.derivative(plane,slip::Z_DIRECTION,1,1,Result);
 std::cout<<"Result="<<std::endl;
 std::cout<<Result<<std::endl;
 std::cout<<"-- direction Z, ordre 1 (der), ordre 2 (sch), shift 1 --"<<std::endl;
 VFM.derivative(plane,slip::Z_DIRECTION,1,2,Result);
 std::cout<<"Result="<<std::endl;
 std::cout<<Result<<std::endl;
 
std::cout<<std::endl;
 std::cout<<"--------------------------------------------"<<std::endl;
 std::cout<<"--  div3d,  vorticity                              --"<<std::endl;
 std::cout<<"-- GENERIC DERIVATION  + BORDER TREATMENT --"<<std::endl;
 std::cout<<"--------------------------------------------"<<std::endl;
 std::cout<<std::endl;

 slip::Matrix3d<double> div(VFM.slices(),VFM.rows(),VFM.cols());
 VFM.divergence(1,div);
 std::cout<<div<<std::endl;
  
 // std::cout<<" vorticity "<<std::endl;
//  slip::RegularVector3dField3d<double> vort(VFM.slices(),VFM.rows(),VFM.cols());
//  VFM.vorticity(1,vort);
//  std::cout<<vort<<std::endl;

std::cout<<" norm "<<std::endl;
 VFM.norm(Result);
 std::cout<<Result<<std::endl;

std::cout<<" * test a 2D Lamb-Oseen vortex in a 3D container * "<<std::endl;
  std::size_t ni=8,nj=8,nk=4;
  slip::Vector<double> Z(nk,0.0);
  slip::Vector<double> X(nj,0.0);
  slip::Vector<double> Y(ni,0.0);
  //  slip::Matrix< slip::Vector<double> > V(X.size(),Y.size());
  slip::RegularVector3dField3d<double> VFM2(Z.size(),Y.size(),X.size());
  //slip::Matrix< slip::Vector<double> > DV(X.size(),Y.size());
  for(std::size_t j=0; j < nj; ++j)
    X(j)=j/double(nj-1)-0.5;
  for(std::size_t i=0; i < ni; ++i)
    Y(i)=i/double(ni-1)-0.5;
  for(std::size_t k=0; k < nk; ++k)
    Z(k)=k/double(nk-1)-0.5;

  slip::Array2d<double> R(ni,nj,0.0);
  for(std::size_t i=0; i<ni ; ++i)
    for(std::size_t j=0; j<nj ; ++j)
      R(ni-1-i,j)=sqrt(X(j)*X(j)+Y(i)*Y(i));// X is indexed by j, Y by i inverted, Z by k inverted

  for(std::size_t k=0; k<nk ; ++k)
  {
   for(std::size_t i=0; i<ni ; ++i)
   {
    for(std::size_t j=0; j<nj; ++j)
    {
      //V(i,j).set_x(-Y(j));
      //V(i,j).set_y(X(i));
      double r = R(ni-1-i,j);
      VFM2(nk-1-k,ni-1-i,j)[0]=(-Y(i)/r/r)*(1-exp(-r*r*25));
      VFM2(nk-1-k,ni-1-i,j)[1]=(X(j)/r/r)*(1-exp(-r*r*25));
      //V(i,j).set(-Y(j)/R(i,j)/R(i,j)/R(i,j),X(i)/R(i,j)/R(i,j)/R(i,j));
      //V(i,j).set(Y(j),0.0);
    }
   }
  }
  
 std::cout<<" div (2nd order, centered) "<<std::endl;
 div.resize(VFM2.slices(),VFM2.rows(),VFM2.cols());
 VFM2.divergence(2,div);
 std::cout<<div<<std::endl;

 std::cout<<" vorticity (2nd order, centered) "<<std::endl;
 slip::RegularVector3dField3d<double> vort(VFM2.slices(),VFM2.rows(),VFM2.cols());
 VFM2.vorticity(2,vort);
 std::cout<<vort<<std::endl;

 std::cout<<" lambda2 (2nd order, centered) "<<std::endl;
 slip::RegularVector3dField3d<double> lambda(VFM2.slices(),VFM2.rows(),VFM2.cols());
 slip::RegularVector3dField3d<double> lambdav1(VFM2.slices(),VFM2.rows(),VFM2.cols());
 slip::RegularVector3dField3d<double> lambdav2(VFM2.slices(),VFM2.rows(),VFM2.cols());
 slip::RegularVector3dField3d<double> lambdav3(VFM2.slices(),VFM2.rows(),VFM2.cols());

 std::cout<<" without eigenvectors : "<<std::endl;
 VFM2.lambda(2,lambda);
 std::cout<<" eigenvalues "<<std::endl;
 std::cout<<lambda<<std::endl;

   //  // tensor to diagonalize A
//     //double v[]={1.0, 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 3.0};
//     double v[]={0.0, 1.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 3.0};
//     slip::Matrix<double> A(3,3,v); 
//     // eigenvalues
//     slip::Vector<double> lmbd(6); // 2*3 components <=> 3 complex eigenvalues
//     // eigenvectors
//     slip::Matrix<double> eigenvectors(6,3);// 3 vectors * 2*3 components
//     // sort eigenvalues ? no <- sorting is based on absolute value, not suitable with real eigens
//     bool sort=false;
//     // normalize eigenvectors ? yes
//     bool normalize=true;
//     // error flag for slip::eigen
//     std::size_t ier;

//     eigen(A,lmbd,ier,sort);
//     std::cout<<" A "<<std::endl;
//     std::cout<<A<<std::endl;
//     std::cout<<" lmbd "<<std::endl;
//     std::cout<<lmbd<<std::endl;

//     eigen(A,lmbd,eigenvectors,ier,sort,normalize);
//     std::cout<<" A "<<std::endl;
//     std::cout<<A<<std::endl;
//     std::cout<<" lmbd "<<std::endl;
//     std::cout<<lmbd<<std::endl;
//     std::cout<<" eigenvectors "<<std::endl;
//     std::cout<<eigenvectors<<std::endl;

//  std::cout<<" with eigenvectors: "<<std::endl;
// //  VFM2.lambda(2,lambda,lambdav1,lambdav2,lambdav3);
// //  std::cout<<" eigenvalues "<<std::endl;
// //  std::cout<<lambda<<std::endl;
// //  std::cout<<" eigenvectors(lambda2) "<<std::endl;
// //  std::cout<<lambdav2<<std::endl;
// //  std::cout<<" eigenvectors(lambda3) "<<std::endl;
// //  std::cout<<lambdav3<<std::endl;

 

 
// //  slip::RegularVector3dField3d<double> VFM3;
// //  VFM3.read_tecplot("./../algorithms/xyzuvw.dat");
// //  std::cout<<VFM3<<std::endl;
//  // VFM3.read_gnuplot("./../algorithms/xyzuvw.dat");
// //  std::cout<<VFM3<<std::endl;

 slip::RegularVector3dField3d<double> VFM4;
 VFM4.read_tecplot_bin("testXYZ_double.plt");
 std::cout<<VFM4<<std::endl;
 VFM4.write_tecplot_bin("VFM4.plt","title","zone");


 //
 //serialization
 //
 // create and open a character archive for output
    std::ofstream ofs("rvectorfield3d.txt");
    //slip::Point3d<float> inits(0.5f,1.0f,2.0f);
    //slip::Point3d<float> steps(1.0f,2.0f,3.0f);
   
    slip::RegularVector3dField3d<float> b(3,3,4);
    slip::iota(b.begin(),b.end(),slip::Vector3d<float>(1.0f,2.0f,3.0f),
	       slip::Vector3d<float>(1.0f,1.0f,1.0f));
    
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<b;
    }//to close archive
    std::ifstream ifs("rvectorfield3d.txt");
    slip::RegularVector3dField3d<float> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("rvectorfield3d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<b;
    }//to close archive

    std::ifstream ifsb("rvectorfield3d.bin");
    slip::RegularVector3dField3d<float> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;

 return 0;
}
