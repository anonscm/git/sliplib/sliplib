#include "Matrix.hpp"
#include "GenericMultiComponent2d.hpp"
#include "Box2d.hpp"
#include "iterator2d_box.hpp"
#include <iostream>
#include <complex>
#include <algorithm>
#include <numeric>
#include <vector>
#include "dynamic.hpp"
#include "Vector3d.hpp"


int main()
{

  //------------------------------
  //constructors
  //------------------------------
  slip::GenericMultiComponent2d<slip::Vector3d<double> > M;
  std::cout<<"dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 

 
  slip::GenericMultiComponent2d<slip::Vector3d<double> > M2(6,3);
  std::cout<<"dim1 = "<<M2.dim1()<<" dim2 = "<<M2.dim2()<<" size = "<<M2.size()<<" empty = "<<M2.empty()<<std::endl; 
  std::cout<<"rows = "<<M2.rows()<<" cols = "<<M2.cols()<<" columns = "<<M2.columns()<<std::endl;
  std::cout<<"height = "<<M2.height()<<" width = "<<M2.width()<<std::endl; 
  std::cout<<M2<<std::endl; 
 
  slip::GenericMultiComponent2d<slip::Vector3d<double> > M3(6,3,slip::Vector3d<double>(1.0,2.0,3.0));
  std::cout<<"dim1 = "<<M3.dim1()<<" dim2 = "<<M3.dim2()<<" size = "<<M3.size()<<" empty = "<<M3.empty()<<std::endl; 
  std::cout<<"rows = "<<M3.rows()<<" cols = "<<M3.cols()<<" columns = "<<M3.columns()<<std::endl;
  std::cout<<M3<<std::endl;


 double d[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,
	       14.0,15.0,16.0,17.0,18.0};
 slip::GenericMultiComponent2d<slip::Vector3d<double> > M4(2,3,d);
 std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
 std::cout<<M4<<std::endl; 
 

 slip::Matrix<double> Mat1(4,3,12.0);
 slip::Matrix<double> Mat2(4,3,13.0);
 slip::Matrix<double> Mat3(4,3,14.0);

 std::vector<slip::Matrix<double>::iterator> iterators_list(3);
 iterators_list[0] = Mat1.begin();
 iterators_list[1] = Mat2.begin();
 iterators_list[2] = Mat3.begin();
 
 
slip::GenericMultiComponent2d<slip::Vector3d<double> > Mcol(4,3,iterators_list,Mat1.end());
std::cout<<Mcol<<std::endl; 

// //   std::vector<int> V(20);
// //   for(std::size_t i = 0; i < 20; ++i)
// //     V[i] = i;
// //   slip::GenericMultiComponent2d<int> M5(4,5,V.begin(),V.end());
// //   std::cout<<"dim1 = "<<M5.dim1()<<" dim2 = "<<M5.dim2()<<" size = "<<M5.size()<<" empty = "<<M5.empty()<<std::endl; 
// //  std::cout<<M5<<std::endl; 


 slip::GenericMultiComponent2d<slip::Vector3d<double> > M6 = M4;
 std::cout<<"dim1 = "<<M6.dim1()<<" dim2 = "<<M6.dim2()<<" size = "<<M6.size()<<" empty = "<<M6.empty()<<std::endl; 
 std::cout<<M6<<std::endl; 
 //------------------------------

 //------------------------------
 //resize
 //------------------------------
  M.resize(4,3,5.6);
  std::cout<<"dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 
  //------------------------------


  //------------------------------
  //Affectation
  //------------------------------
  slip::GenericMultiComponent2d<slip::Vector3d<double> > M7;
  M7 = M;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M7 = M7;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M4 = 12.2;
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl;   

  M4 = slip::Vector3d<double> (128.0,255.0,127.0);
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl;   
  //------------------------------

  
  //------------------------------
  //Element access operators
  //------------------------------
   M2(2,1) = 2.1;
   M2[1][2] = 1.2;
  
  for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2[i][j]<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
   for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2(i,j)<<" ";
	}
      std::cout<<std::endl;
    }

   slip::GenericMultiComponent2d<slip::Vector3d<double> > Arange(10,9);
   double val = 0.0;
    for(size_t i = 0; i < Arange.dim1(); ++i)
    {
      for(size_t j = 0; j < Arange.dim2(); ++j)
	{
	  Arange(slip::Point2d<std::size_t>(i,j)) = val;
	  val = val + 1.0;
	}
    }

    for(size_t i = 0; i < Arange.dim1(); ++i)
    {
      for(size_t j = 0; j < Arange.dim2(); ++j)
	{
	  std::cout<<Arange(slip::Point2d<std::size_t>(i,j))<<" ";
	}
      std::cout<<std::endl;
    }
    std::cout<<std::endl;
    slip::Range<int> range_r(0,Arange.dim2()-1,2);
    slip::Range<int> range_c(0,Arange.dim1()-1,2);
    slip::GenericMultiComponent2d<slip::Vector3d<double> > Aext = Arange(range_r,range_c);
    std::cout<<Aext<<std::endl;

  //------------------------------

   //------------------------------
   //swap
   //------------------------------
   M2.swap(M3);
   std::cout<<M2<<std::endl;
   std::cout<<std::endl;
   std::cout<<M3<<std::endl;
   //------------------------------


   //----------------------------
   //iterators
   //----------------------------
   slip::GenericMultiComponent2d<slip::Vector3d<double> > M8(2,4);
   std::vector<slip::Vector3d<double> > V8(8);
   for(std::size_t i = 0; i < 8; ++i)
     V8[i] = slip::Vector3d<double>(i,i,i);
    
   M8.fill(V8.begin(),V8.end());   
   std::cout<<"M8 = \n"<<M8<<std::endl;

   std::copy(M8.begin(),M8.end(),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M8.rbegin(),M8.rend(),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl<<std::endl;
   
   slip::Matrix<double> mat(2,4,7.7);
   M8.fill(1,mat.begin(),mat.end());
  //  std::transform(M3.rbegin(),M3.rend(),M8.begin(),M2.begin(),std::plus<slip::Vector3d<double> >());
  
//    std::cout<<M2<<std::endl;
   

   std::copy(M8.row_begin(1),M8.row_end(1),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;

   std::copy(M8.col_begin(1),M8.col_end(1),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;
   
   std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_begin(2),std::plus<slip::Vector3d<double> >());
   std::cout<<M2<<std::endl;

   std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_begin(2),std::plus<slip::Vector3d<double> >());
   std::cout<<M2<<std::endl;


   std::copy(M8.row_rbegin(1),M8.row_rend(1),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M8.col_rbegin(1),M8.col_rend(1),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   
   
      

   std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_rbegin(2),std::plus<slip::Vector3d<double> >());
   std::cout<<M2<<std::endl;

   std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_rbegin(2),std::plus<slip::Vector3d<double> >());
   std::cout<<M2<<std::endl;
   
 
   std::cout<<"iterator2d_box test..."<<std::endl;
   std::cout<<M2<<std::endl;
   std::cout<<"bottom_right() and upper_left() test "<<std::endl;
   std::copy(M2.upper_left(),M2.bottom_right(),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M2.rupper_left(),M2.rbottom_right(),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;

    std::cout<<"bottom_right(box) and upper_left(box) box(2,1,4,2) test "<<std::endl;
   slip::Box2d<int> box(2,1,4,2);
   std::copy(M2.upper_left(box),M2.bottom_right(box),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M2.rupper_left(box),M2.rbottom_right(box),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   
   slip::GenericMultiComponent2d<slip::Vector3d<double> > Mask(3,2);
   double k = 1.0;
   for(std::size_t i = 0; i < Mask.dim1(); ++i)
     for(std::size_t j = 0; j < Mask.dim2(); ++j)
       {
	 Mask[i][j] = slip::Vector3d<double>(k,k+1,k+2);
	 k += 1.0;
       }
   std::copy(Mask.upper_left(),Mask.bottom_right(),
	     M2.upper_left(box));
   std::cout<<M2<<std::endl;
  
   std::copy(Mask.upper_left(),Mask.bottom_right(),
	     M2.rupper_left(box));
   std::cout<<M2<<std::endl;
   
   std::transform(M2.upper_left(box),M2.bottom_right(box),Mask.upper_left(),M2.upper_left(box),std::plus<slip::Vector3d<double> >());
   std::cout<<M2<<std::endl;
   
   std::transform(M2.upper_left(box),M2.bottom_right(box),Mask.rupper_left(),M2.upper_left(box),std::plus<slip::Vector3d<double> >());
   std::cout<<M2<<std::endl;



  slip::GenericMultiComponent2d<slip::Vector3d<int> > Mrange(10,10);
   int ind = 0;
   for(std::size_t i = 0; i < Mrange.dim1(); ++i)
     for(std::size_t j = 0; j < Mrange.dim2(); ++j)
       {
	 Mrange[i][j] = slip::Vector3d<int>(ind,ind+2,ind+4);
	 ind += 1;
       }
   std::cout<<Mrange<<std::endl;
   std::cout<<"row_range_iterator ..."<<std::endl;
   //slip::Range<int> range(2,7,2);
   slip::Range<int> range(0,(Mrange.dim2() - 1),2);
   std::copy(Mrange.row_begin(3,range),Mrange.row_end(3,range),std::ostream_iterator<slip::Vector3d<int> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(Mrange.row_rbegin(3,range),Mrange.row_rend(3,range),std::ostream_iterator<slip::Vector3d<int> >(std::cout," "));
   std::cout<<std::endl;

   std::cout<<"col_range_iterator ..."<<std::endl;
   //slip::Range<int> range2(2,7,2);
   slip::Range<int> range2(0,(Mrange.dim1() - 1),2);
   std::copy(Mrange.col_begin(2,range2),Mrange.col_end(2,range2),std::ostream_iterator<slip::Vector3d<int> >(std::cout,"\n"));
   std::cout<<std::endl;
   std::copy(Mrange.col_rbegin(2,range2),Mrange.col_rend(2,range2),std::ostream_iterator<slip::Vector3d<int> >(std::cout,"\n"));
   std::cout<<std::endl;

   std::cout<<"component_row_range_iterator ..."<<std::endl;
   slip::Range<int> range3(2,7,2);
   std::copy(Mrange.row_begin(0,3,range3),Mrange.row_end(0,3,range3),std::ostream_iterator<int>(std::cout," "));
   std::cout<<std::endl;
   std::copy(Mrange.row_rbegin(0,3,range3),Mrange.row_rend(0,3,range3),std::ostream_iterator<int>(std::cout," "));
   std::cout<<std::endl;

   std::cout<<"component_col_range_iterator ..."<<std::endl;
   slip::Range<int> range4(2,7,2);
   std::copy(Mrange.col_begin(1,2,range4),Mrange.col_end(1,2,range4),std::ostream_iterator<int>(std::cout,"\n"));
   std::cout<<std::endl;
   std::copy(Mrange.col_rbegin(1,2,range4),Mrange.col_rend(1,2,range4),std::ostream_iterator<int>(std::cout,"\n"));
   std::cout<<std::endl;

   //std::copy(Mrange.row_begin(4,range),Mrange.row_end(4,range),Mrange.row_begin(8,range));

   //std::copy(Mrange.col_begin(4,range2),Mrange.col_end(4,range2),Mrange.col_begin(2,range));

   // std::cout<<Mrange<<std::endl;
   
   std::copy(Mrange.upper_left(range,range2),Mrange.bottom_right(range,range2),std::ostream_iterator<slip::Vector3d<int> >(std::cout," "));
   std::cout<<std::endl;
   // std::copy(Mrange.rupper_left(range,range2),Mrange.rbottom_right(range,range2),std::ostream_iterator<double>(std::cout," "));
//    std::cout<<std::endl;
   std::copy(Mrange.upper_left(range),Mrange.bottom_right(range),std::ostream_iterator<slip::Vector3d<int> >(std::cout," "));
   std::cout<<std::endl;
   //  std::copy(Mrange.rupper_left(range),Mrange.rbottom_right(range),std::ostream_iterator<double>(std::cout," "));
//    std::cout<<std::endl;
   

   slip::GenericMultiComponent2d<slip::Vector3d<int> > M100(10,12);
   for(std::size_t i = 0; i < M100.dim1(); ++i)
       {
	 for(std::size_t j = 0; j < M100.dim2(); ++j)
	   {
	     M100[i][j][0] = i + j;
	     M100[i][j][1] = i + j + 1;
	     M100[i][j][2] = i + j + 2;
	       
	     
	   }
       }
     std::cout<<M100<<std::endl;
   
     slip::Box2d<int> box4(1,1,2,2);
     slip::Box2d<int> box5(0,0,1,1);
//      std::cout<<std::inner_product(M100.upper_left(box4),M100.bottom_right(box4),M100.upper_left(box5),0.0)<<std::endl;
     
     
     slip::GenericMultiComponent2d<slip::Vector3d<int> >::iterator2d it  = M100.upper_left(box4);
     slip::GenericMultiComponent2d<slip::Vector3d<int> >::iterator2d it2 = M100.upper_left(box5);
     
     for(;it != M100.bottom_right(box4); ++it, ++it2)
       {
	 std::cout<<"("<<*it<<" "<<*it2<<") ";
       }
     std::cout<<std::endl;
  
   //   std::cout<<"M100 : "<<std::endl;
//      std::cout << M100 << std::endl << std::endl;

     //first plane
     std::cout<<"first plane "<<std::endl;
     std::copy(M100.begin(0),M100.end(0),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //second plane
     std::cout<<"second plane "<<std::endl;
     std::copy(M100.begin(1),M100.end(1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //third plane
     std::cout<<"third plane "<<std::endl;
     std::copy(M100.begin(2),M100.end(2),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     

     std::cout<<"first plane, row 1 "<<std::endl;
     std::copy(M100.row_begin(0,1),M100.row_end(0,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<"first plane, reverse row 1 "<<std::endl;
     std::copy(M100.row_rbegin(0,1),M100.row_rend(0,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;

   //    std::cout<<"first plane, col 1 "<<std::endl;
//       std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
     
//      std::cout<<"first plane, reverse col 1 "<<std::endl;
//      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));

     std::cout<<"second plane, row 1 "<<std::endl;
     std::copy(M100.row_begin(1,1),M100.row_end(1,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<"second plane, reverse row 1 "<<std::endl;
     std::copy(M100.row_rbegin(1,1),M100.row_rend(1,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;

     std::cout<<"first plane, col 2 "<<std::endl;
     std::copy(M100.col_begin(0,2),M100.col_end(0,2),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     
     //      std::cout<<"first plane, reverse col 1 "<<std::endl;
     //      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));
     
     
     std::cout<<"third plane, row 1 "<<std::endl;
     std::copy(M100.row_begin(2,1),M100.row_end(2,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<"third plane, reverse row 1 "<<std::endl;
     std::copy(M100.row_rbegin(2,1),M100.row_rend(2,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<std::endl;

     std::cout<<"first plane, component 2d iterator "<<std::endl;
     std::copy(M100.upper_left(0),M100.bottom_right(0),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<std::endl;

     std::cout<<"first plane, reverse component 2d iterator "<<std::endl;
     std::copy(M100.rupper_left(0),M100.rbottom_right(0),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<std::endl;
    
     std::cout<<"first plane, const component 2d iterator "<<std::endl;
     slip::GenericMultiComponent2d<slip::Vector3d<int> > const M100c(M100);
     std::copy(M100c.upper_left(0),M100c.bottom_right(0),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<std::endl;

     std::cout<<"first plane, const reverse component 2d iterator "<<std::endl;
     std::copy(M100c.rupper_left(0),M100c.rbottom_right(0),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<std::endl;

     std::cout<<"second plane, iterator2d box (1,1,2,2)"<<std::endl;
     std::copy(M100.upper_left(1,box4),M100.bottom_right(1,box4),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<std::endl;
  
     std::cout<<"second plane, reverse iterator2d box (1,1,2,2)"<<std::endl;
     std::copy(M100.rupper_left(1,box4),M100.rbottom_right(1,box4),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<std::endl;

     std::cout<<"second plane, const iterator2d box (1,1,2,2)"<<std::endl;
     std::copy(M100c.upper_left(1,box4),M100c.bottom_right(1,box4),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<std::endl;
  
     std::cout<<"second plane, const reverse iterator2d box (1,1,2,2)"<<std::endl;
     std::copy(M100c.rupper_left(1,box4),M100c.rbottom_right(1,box4),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<std::endl;

     slip::Range<int> rr(1,5,2);
     slip::Range<int> rc(2,6,2);
     std::cout<<"second plane, iterator2d range (1,5),(2,6) with step 2"<<std::endl;
     std::copy(M100.upper_left(1,rr,rc),M100.bottom_right(1,rr,rc),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<std::endl;
  
     std::cout<<"second plane, reverse iterator2d range (1,5),(2,6)"<<std::endl;
     std::copy(M100.rupper_left(1,rr,rc),M100.rbottom_right(1,rr,rc),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<std::endl;

     std::cout<<"second plane, const iterator2d range (1,5),(2,6)"<<std::endl;
     std::copy(M100c.upper_left(1,rr,rc),M100c.bottom_right(1,rr,rc),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<std::endl;
  
     std::cout<<"second plane, const reverse iterator2d range (1,5),(2,6)"<<std::endl;
     std::copy(M100c.rupper_left(1,rr,rc),M100c.rbottom_right(1,rr,rc),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<std::endl;

//      std::cout<<"first plane, reverse col 1 "<<std::endl;
     //      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));
     
     
     
     //   //----------------------------  
     
     //------------------------------
     //Comparison operators
     //------------------------------
     std::cout<<" M3 == M2 "<<(M3 == M2)<<std::endl;
     std::cout<<" M3 != M2 "<<(M3 != M2)<<std::endl;
     //   std::cout<<" M3 > M2 "<<(M3 > M2)<<std::endl;
     //   std::cout<<" M3 < M2 "<<(M3 < M2)<<std::endl;
     //   std::cout<<" M3 >= M2 "<<(M3 >= M2)<<std::endl;
     //   std::cout<<" M3 <= M2 "<<(M3 <= M2)<<std::endl;
     //------------------------------
     
     
     //------------------------------
     //fill methods
     //------------------------------
     
     M3.fill(4.0);
     std::cout<<M3<<std::endl;
     M3.fill(slip::Vector3d<double>(2.0,3.0,6.0));
     std::cout<<M3<<std::endl;
     double d2[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0};
     M3.fill(d2);
     std::cout<<M3<<std::endl;
     std::cout<<std::endl;
     std::vector<slip::Vector3d<double> > V2(18);
     for(std::size_t i = 0; i < 18; ++i)
       V2[i] = slip::Vector3d<double>(i,i,i);
     
     M3.fill(V2.begin(),V2.end());
     std::cout<<M3<<std::endl;
     //------------------------------
     
     
     
     
     
     
     
     std::cout<<" mins(M100) = "<<M100.min()<<std::endl;
     std::cout<<" maxs(M100) = "<<M100.max()<<std::endl;
     
     
     
     return 0;
}
