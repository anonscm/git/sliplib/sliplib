#include <iostream>
#include <iomanip>
#include <vector>
#include "Array2d.hpp"

#include "ContinuousLegendreBase2d.hpp"
#include "PolySupport.hpp"

int main()
{
  typedef double T;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Default constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousLegendreBase2d<T> default_base2d;
  std::cout<<"default_base2d = \n"<<default_base2d<<std::endl;

  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Constructor 2d" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  const std::size_t base2d1_range_size_row = 4;
  const std::size_t base2d1_range_size_col = 4;
  const std::size_t base2d1_degree = 3;

  slip::PolySupport<T> base2d1_support_row(0.0,4.0);
  slip::PolySupport<T> base2d1_support_col(0.0,4.0);

  slip::POLY_BASE_GENERATION_METHOD generation_method = slip::POLY_BASE_GENERATION_FILE;
  //slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_RECTANGULAR;
  //slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_SIMPSON;
  //slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_HOSNY2007;
    slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_EXACT;
 
    slip::ContinuousLegendreBase2d<T> base2d1(base2d1_range_size_row,
						  base2d1_range_size_col,
						  base2d1_degree,
						  false,
						  false,
						  generation_method,
						  integration_method,
						  base2d1_support_row,
						  base2d1_support_col);
    // std::cout<<"base2d1 = \n"<<base2d1<<std::endl;
   base2d1.generate();
   std::cout<<"base2d1 = \n"<<base2d1<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Constructor 2d complete base" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  const std::size_t base2d2_range_size_row = 6;
  const std::size_t base2d2_range_size_col = 5;
  const std::size_t base2d2_degree_row = 5;
  const std::size_t base2d2_degree_col = 4;
 

  slip::PolySupport<T> base2d2_support_row(0.0,4.0);
  slip::PolySupport<T> base2d2_support_col(0.0,3.0);
  slip::ContinuousLegendreBase2d<T> base2d2(base2d2_range_size_row,
						base2d2_range_size_col,
						base2d2_degree_row,
						base2d2_degree_col,
						false,
						false,
						generation_method,
						integration_method,
						base2d1_support_row,
						base2d1_support_col);
  std::cout<<"base2d2 = \n"<<base2d2<<std::endl;
  base2d2.generate();
  std::cout<<base2d2<<std::endl;
 
  
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "Copy constructor" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   slip::ContinuousLegendreBase2d<T> copy_base2d(base2d1);
//   std::cout<<"copy_base2d = \n"<<copy_base2d<<std::endl;


//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "operator=" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   slip::ContinuousLegendreBase2d<T> equal_base2d;
//   equal_base2d = base2d1;
//   std::cout<<"equal_base2d = \n"<<equal_base2d<<std::endl;

//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "evaluate" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   slip::Array2d<T> P00(base2d1_range_size_row,
// 		       base2d1_range_size_col);
//   base2d1.evaluate(0,0,P00.upper_left(),P00.bottom_right());
//   std::cout<<"P00 = \n"<<P00<<std::endl;
//   slip::Array2d<T> P01(base2d1_range_size_row,
// 		       base2d1_range_size_col);
//   base2d1.evaluate(0,1,P01.upper_left(),P01.bottom_right());
//   std::cout<<"P01 =  \n"<<P01<<std::endl;

//   slip::Array2d<T> P02(base2d1_range_size_row,
// 		       base2d1_range_size_col);
//   base2d1.evaluate(0,2,P02.upper_left(),P02.bottom_right());
//   std::cout<<"P02 =  \n"<<P02<<std::endl;
//    slip::Array2d<T> P03(base2d1_range_size_row,
// 		       base2d1_range_size_col);
//   base2d1.evaluate(0,3,P03.upper_left(),P03.bottom_right());
//   std::cout<<"P03 =  \n"<<P03<<std::endl;
//  slip::Array2d<T> P10(base2d1_range_size_row,
// 		       base2d1_range_size_col);
//   base2d1.evaluate(1,0,P10.upper_left(),P10.bottom_right());
//   std::cout<<"P10 =  \n"<<P10<<std::endl;
//   slip::Array2d<T> P11(base2d1_range_size_row,
// 		       base2d1_range_size_col);
//   base2d1.evaluate(1,1,P11.upper_left(),P11.bottom_right());
//    std::cout<<"P11 =  \n"<<P11<<std::endl;
//   slip::Array2d<T> P12(base2d1_range_size_row,
// 		       base2d1_range_size_col);
//   base2d1.evaluate(1,2,P12.upper_left(),P12.bottom_right());
//   std::cout<<"P12 =  \n"<<P12<<std::endl;
//  slip::Array2d<T> P20(base2d1_range_size_row,
// 		      base2d1_range_size_col);
//   base2d1.evaluate(2,0,P20.upper_left(),P20.bottom_right());
//   std::cout<<"P20 =  \n"<<P20<<std::endl;
//   slip::Array2d<T> P21(base2d1_range_size_row,
// 		      base2d1_range_size_col);
//   base2d1.evaluate(2,1,P21.upper_left(),P21.bottom_right());
//   std::cout<<"P21 =  \n"<<P21<<std::endl;
//  slip::Array2d<T> P30(base2d1_range_size_row,
// 		      base2d1_range_size_col);
//   base2d1.evaluate(3,0,P30.upper_left(),P30.bottom_right());
//   std::cout<<"P30 =  \n"<<P30<<std::endl;


//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "project" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   slip::Array2d<T> data(base2d1_range_size_row,
// 			base2d1_range_size_col,
// 			T(5.12));
//   // // slip::Array<T> data(base2d1_range_size);
//   // // slip::iota(data.begin(),data.end(),T(0.0));
//   std::cout<<"data =\n"<<data<<std::endl;
//   std::cout<<"(P00,data) ="<<base2d1.project(data.upper_left(),data.bottom_right(),0,0)<<std::endl;

//   slip::Array2d<T> A(4,4);
//   A[0][0] = T(3.0); A[0][1] = T(2.0); A[0][2] = T(1.0); A[0][3] = T(5.0); 
//   A[1][0] = T(6.0); A[1][1] = T(1.0); A[1][2] = T(7.0); A[1][3] = T(3.0); 
//   A[2][0] = T(2.0); A[2][1] = T(8.0); A[2][2] = T(4.0); A[2][3] = T(6.0); 
//   A[3][0] = T(5.0); A[3][1] = T(1.0); A[3][2] = T(4.0); A[3][3] = T(2.0); 
//   std::cout<<"A =\n"<<A<<std::endl;
//   std::cout<<"(P00,A) ="<<base2d1.project(A.upper_left(),A.bottom_right(),0,0)<<std::endl;
//   for(std::size_t i = 0; i < 4; ++i)
//     {
//       for(std::size_t j = 0; j < 4; ++j)
// 	{
// 	  std::cout<<"(P"<<i<<j<<",A) ="<<base2d1.project(A.upper_left(),A.bottom_right(),i,j)<<" ";
// 	}
//       std::cout<<std::endl;
//     }

//   //Yap PAMI 2005 matrix
//    slip::Array2d<T> A2(4,4);
//   A2[0][0] = T(8.0); A2[0][1] = T(4.0); A2[0][2] = T(6.0); A2[0][3] = T(7.0); 
//   A2[1][0] = T(2.0); A2[1][1] = T(9.0); A2[1][2] = T(6.0); A2[1][3] = T(6.0); 
//   A2[2][0] = T(2.0); A2[2][1] = T(5.0); A2[2][2] = T(9.0); A2[2][3] = T(3.0); 
//   A2[3][0] = T(7.0); A2[3][1] = T(4.0); A2[3][2] = T(6.0); A2[3][3] = T(7.0); 
//   std::cout<<"A2 =\n"<<A2<<std::endl;
//   std::cout<<base2d1.project(A2.upper_left(),A2.bottom_right(),0,0)<<std::endl;
// for(std::size_t i = 0; i < 4; ++i)
//     {
//       for(std::size_t j = 0; j < 4; ++j)
// 	{
// 	  std::cout<<base2d1.project(A2.upper_left(),A2.bottom_right(),i,j)<<" ";
// 	}
//       std::cout<<std::endl;
//     }

//  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//  std::cout << "projection coefficients" << std::endl;
//  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//  slip::Array<T> coefA2_deg3(10);
//  base2d1.project(A2.upper_left(),A2.bottom_right(),3,coefA2_deg3.begin(),coefA2_deg3.end());
//  std::cout<<"coefA2_deg3 = \n"<<coefA2_deg3<<std::endl;
//  slip::Array<T> coefA2_deg2(6);
//  base2d1.project(A2.upper_left(),A2.bottom_right(),2,coefA2_deg2.begin(),coefA2_deg2.end());
//  std::cout<<"coefA2_deg2 = \n"<<coefA2_deg2<<std::endl;

 
//   // for(std::size_t k = 0; k < base2d1.size(); ++k)
//   //   {
//   //     std::cout<<"(P"<<k<<",data) ="<<base2d1.project(data.begin(),data.end(),k)<<std::endl;
//   //   }
  
  
//   // slip::Array<T> base2d1_coef(base2d1.size());
//   // base2d1.project(data.begin(),data.end(),base2d1_coef.begin(),base2d1_coef.end());
//   // std::cout<<"base2d1_coef = \n"<<base2d1_coef<<std::endl;


//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "reconstruction" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   slip::Array2d<T> A2r(base2d1_range_size_row,
// 		       base2d1_range_size_col);
//   for(std::size_t k = 1; k <= coefA2_deg3.size(); ++k)
//     {
//       base2d1.reconstruct(coefA2_deg3.begin(),coefA2_deg3.end(),
// 			  k,
// 			  A2r.upper_left(),A2r.bottom_right());
//       std::cout<<"A2r (KA2 = "<<k<<")  = \n"<<A2r<<std::endl;
//     }


//   slip::PolySupport<T> base2d3_support_row(0.0,3.0);
//   slip::PolySupport<T> base2d3_support_col(0.0,4.0);

//     slip::ContinuousLegendreBase2d<T> base2d3(3,
// 						  4,
// 						  3,
// 						  false,
// 						  false,
// 						  generation_method,
// 						  integration_method,
// 						  base2d3_support_row,
// 						  base2d3_support_col);
//     base2d3.generate();
//     std::cout<<"base2d3 = \n"<<base2d3<<std::endl;

//   slip::Array2d<T> A3(3,4);
//   A3[0][0] = T(3.0); A3[0][1] = T(2.0); A3[0][2] = T(1.0); A3[0][3] = T(5.0); 
//   A3[1][0] = T(6.0); A3[1][1] = T(1.0); A3[1][2] = T(7.0); A3[1][3] = T(3.0); 
//   A3[2][0] = T(2.0); A3[2][1] = T(8.0); A3[2][2] = T(4.0); A3[2][3] = T(6.0); 
//   std::cout<<"A3 =\n"<<A3<<std::endl;

//   std::cout<<base2d3.project(A3.upper_left(),A3.bottom_right(),0,0)<<std::endl;
// for(std::size_t dx = 0; dx < 4; ++dx)
//     {
//       for(std::size_t dy = 0; dy < 3; ++dy)
// 	{
// 	  std::cout<<base2d3.project(A3.upper_left(),A3.bottom_right(),dx,dy)<<" ";
// 	}
//       std::cout<<std::endl;
//     }

// slip::Array<T> coefA3_deg3(10);
//  base2d3.project(A3.upper_left(),A3.bottom_right(),3,coefA3_deg3.begin(),coefA3_deg3.end());
//  std::cout<<"coefA3_deg3 = \n"<<coefA3_deg3<<std::endl;
//  slip::Array<T> coefA3_deg2(6);
//  base2d3.project(A3.upper_left(),A3.bottom_right(),2,coefA3_deg2.begin(),coefA3_deg2.end());
//  std::cout<<"coefA3_deg2 = \n"<<coefA3_deg2<<std::endl;
// // slip::Array2d<T> A3r(4,3);
// //   for(std::size_t k = 1; k <= coefA3_deg3.size(); ++k)
// //     {
// //       base2d3.reconstruct(coefA3_deg3.begin(),coefA3_deg3.end(),
// // 			  k,
// // 			  A3r.upper_left(),A3r.bottom_right());
// //       std::cout<<"A3r (KA3 = "<<k<<")  = \n"<<A3r<<std::endl;
// //     }
 
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   // std::cout << "analytic reconstruction " << std::endl;
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   // slip::MultivariatePolynomial<T,2> base2d1_recont_poly =
//   //   base2d1.reconstruct(base2d1_coef.begin(),base2d1_coef.end(),base2d1.size());
//   // std::cout<<"base2d1_recont_poly =\n"<<base2d1_recont_poly<<std::endl;
 
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   // std::cout << "accessors/mutators" << std::endl;
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  

  

  return 0;
}
