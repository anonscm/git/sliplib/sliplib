#include "Matrix.hpp"
#include "DenseVector3dField2d.hpp"
#include "Vector3d.hpp"
#include "Box2d.hpp"
#include "iterator2d_box.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <complex>
#include <algorithm>
#include <numeric>
#include <vector>
#include "dynamic.hpp"

#include "arithmetic_op.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
int main()
{

  //------------------------------
  //constructors
  //------------------------------
  slip::DenseVector3dField2d<double> M;
  std::cout<<"name = "<<M.name()<<" dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 

  slip::DenseVector3dField2d<double> M2(6,3);
  std::cout<<"dim1 = "<<M2.dim1()<<" dim2 = "<<M2.dim2()<<" size = "<<M2.size()<<" empty = "<<M2.empty()<<std::endl; 
  std::cout<<"rows = "<<M2.rows()<<" cols = "<<M2.cols()<<" columns = "<<M2.columns()<<std::endl;
  std::cout<<"height = "<<M2.height()<<" width = "<<M2.width()<<std::endl; 
  std::cout<<M2<<std::endl; 
 
  slip::DenseVector3dField2d<double> M3(6,3,slip::Vector3d<double>(1.0,2.0,3.0));
  std::cout<<"dim1 = "<<M3.dim1()<<" dim2 = "<<M3.dim2()<<" size = "<<M3.size()<<" empty = "<<M3.empty()<<std::endl; 
  std::cout<<"rows = "<<M3.rows()<<" cols = "<<M3.cols()<<" columns = "<<M3.columns()<<std::endl;
  std::cout<<M3<<std::endl;


 double d[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0};
 slip::DenseVector3dField2d<double> M4(2,3,d);
 std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
 std::cout<<M4<<std::endl; 
 

 slip::Matrix<double> Mat1(4,3,12.0);
 slip::Matrix<double> Mat2(4,3,13.0);
 slip::Matrix<double> Mat3(4,3,14.0);

 
 slip::DenseVector3dField2d<double> Mcol(4,3,Mat1.begin(),Mat1.end(),Mat2.begin(),Mat3.begin());
 std::cout<<Mcol<<std::endl; 


//   std::vector<int> V(20);
//   for(std::size_t i = 0; i < 20; ++i)
//     V[i] = i;
//   slip::DenseVector3dField2d<int> M5(4,5,V.begin(),V.end());
//   std::cout<<"dim1 = "<<M5.dim1()<<" dim2 = "<<M5.dim2()<<" size = "<<M5.size()<<" empty = "<<M5.empty()<<std::endl; 
//  std::cout<<M5<<std::endl; 


 slip::DenseVector3dField2d<double> M6 = M4;
  std::cout<<"dim1 = "<<M6.dim1()<<" dim2 = "<<M6.dim2()<<" size = "<<M6.size()<<" empty = "<<M6.empty()<<std::endl; 
 std::cout<<M6<<std::endl; 


 //------------------------------

 //------------------------------
 //resize
 //------------------------------
  M.resize(4,3,5.6);
  std::cout<<"dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 
  //------------------------------


  //------------------------------
  //Affectation
  //------------------------------
  slip::DenseVector3dField2d<double> M7;
  M7 = M;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M7 = M7;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M4 = 12.2;
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl;   

  M4 = slip::Vector3d<double> (128.0,255.0,3.0);
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl;   
  //------------------------------

  
  //------------------------------
  //Element access operators
  //------------------------------
   M2(2,1) = 2.1;
   M2[1][2] = 1.2;
  
  for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2[i][j]<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
   for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2(i,j)<<" ";
	}
      std::cout<<std::endl;
    }

    for(size_t i = 0; i < M4.dim1(); ++i)
    {
      for(size_t j = 0; j < M4.dim2(); ++j)
	{
	  std::cout<<M4.Vx1(i,j)<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;

    for(size_t i = 0; i < M4.dim1(); ++i)
    {
      for(size_t j = 0; j < M4.dim2(); ++j)
	{
	  std::cout<<M4.Vx2(i,j)<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;

  for(size_t i = 0; i < M4.dim1(); ++i)
    {
      for(size_t j = 0; j < M4.dim2(); ++j)
	{
	  std::cout<<M4.Vx3(i,j)<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
  //------------------------------

   //------------------------------
   //swap
   //------------------------------
   M2.swap(M3);
   std::cout<<M2<<std::endl;
   std::cout<<std::endl;
   std::cout<<M3<<std::endl;
   //------------------------------


   //----------------------------
   //iterators
   //----------------------------
   slip::DenseVector3dField2d<double> M8(2,4);
   std::vector<slip::Vector3d<double> > V8(8);
   for(std::size_t i = 0; i < 8; ++i)
     V8[i] = slip::Vector3d<double>(i,i,i);
    
   M8.fill(V8.begin(),V8.end());   
   std::cout<<"M8 = \n"<<M8<<std::endl;

   std::copy(M8.begin(),M8.end(),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M8.rbegin(),M8.rend(),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl<<std::endl;
   
   
//    // std::transform(M3.rbegin(),M3.rend(),M8.begin(),M2.begin(),std::plus<double>());
  
//    std::cout<<M2<<std::endl;
   

   std::copy(M8.row_begin(1),M8.row_end(1),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;

   std::copy(M8.col_begin(1),M8.col_end(1),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;
   
   //std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_begin(2),std::plus<double>());
   //std::cout<<M2<<std::endl;

   //std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_begin(2),std::plus<double>());
   //std::cout<<M2<<std::endl;


   std::copy(M8.row_rbegin(1),M8.row_rend(1),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M8.col_rbegin(1),M8.col_rend(1),std::ostream_iterator<slip::Vector3d<double> >(std::cout," "));
   std::cout<<std::endl;
   

   //std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_rbegin(2),std::plus<double>());
   //std::cout<<M2<<std::endl;

   //std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_rbegin(2),std::plus<double>());
   //std::cout<<M2<<std::endl;
   
   
     slip::DenseVector3dField2d<int> M100(10,12);
     for(std::size_t i = 0; i < M100.dim1(); ++i)
       {
	 for(std::size_t j = 0; j < M100.dim2(); ++j)
	   {
	     M100[i][j][0] = i + j;
	     M100[i][j][1] = i + j + 1;
	     M100[i][j][2] = i + j + 2;
	   }
       }
     std::cout<<M100<<std::endl;
   
     slip::Box2d<int> box4(1,1,2,2);
     slip::Box2d<int> box5(0,0,1,1);
//      std::cout<<std::inner_product(M100.upper_left(box4),M100.bottom_right(box4),M100.upper_left(box5),0.0)<<std::endl;
     
     slip::DenseVector3dField2d<int>::iterator2d it  = M100.upper_left(box4);
     slip::DenseVector3dField2d<int>::iterator2d it2 = M100.upper_left(box5);
     
     for(;it != M100.bottom_right(box4); ++it, ++it2)
       {
	 std::cout<<"("<<*it<<" "<<*it2<<") ";
       }
     std::cout<<std::endl;
  

     //first plane
     std::cout<<"first plane "<<std::endl;
     std::copy(M100.begin(0),M100.end(0),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //second plane
     std::cout<<"second plane "<<std::endl;
     std::copy(M100.begin(1),M100.end(1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //third plane
     std::cout<<"third plane "<<std::endl;
     std::copy(M100.begin(2),M100.end(2),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     
          std::cout<<"first plane, row 1 "<<std::endl;
     std::copy(M100.row_begin(0,1),M100.row_end(0,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<"first plane, reverse row 1 "<<std::endl;
     std::copy(M100.row_rbegin(0,1),M100.row_rend(0,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;

   //    std::cout<<"first plane, col 1 "<<std::endl;
//       std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
     
//      std::cout<<"first plane, reverse col 1 "<<std::endl;
//      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));

  std::cout<<"second plane, row 1 "<<std::endl;
     std::copy(M100.row_begin(1,1),M100.row_end(1,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<"second plane, reverse row 1 "<<std::endl;
     std::copy(M100.row_rbegin(1,1),M100.row_rend(1,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;

    //  std::cout<<"first plane, col 1 "<<std::endl;
//      std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
     
//      std::cout<<"first plane, reverse col 1 "<<std::endl;
//      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));


     std::cout<<"third plane, row 1 "<<std::endl;
     std::copy(M100.row_begin(2,1),M100.row_end(2,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<"third plane, reverse row 1 "<<std::endl;
     std::copy(M100.row_rbegin(2,1),M100.row_rend(2,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;

    //  std::cout<<"first plane, col 1 "<<std::endl;
//      std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
     
//      std::cout<<"first plane, reverse col 1 "<<std::endl;
//      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));


  //----------------------------  

   //------------------------------
  //Comparison operators
  //------------------------------
  std::cout<<" M3 == M2 "<<(M3 == M2)<<std::endl;
  std::cout<<" M3 != M2 "<<(M3 != M2)<<std::endl;
//   std::cout<<" M3 > M2 "<<(M3 > M2)<<std::endl;
//   std::cout<<" M3 < M2 "<<(M3 < M2)<<std::endl;
//   std::cout<<" M3 >= M2 "<<(M3 >= M2)<<std::endl;
//   std::cout<<" M3 <= M2 "<<(M3 <= M2)<<std::endl;
  //------------------------------


  //------------------------------
  //fill methods
  //------------------------------
  
  M3.fill(4.0);
  std::cout<<M3<<std::endl;
  M3.fill(slip::Vector3d<double>(2.0,3.0,4.0));
  std::cout<<M3<<std::endl;
  double d2[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0};
  M3.fill(d2);
  std::cout<<M3<<std::endl;
  std::cout<<std::endl;
  std::vector<slip::Vector3d<double> > V2(18);
  for(std::size_t i = 0; i < 18; ++i)
    V2[i] = slip::Vector3d<double>(i,i,i);
    
  M3.fill(V2.begin(),V2.end());
  std::cout<<M3<<std::endl;

  slip::Matrix<double> mat(2,3);

  //------------------------------
  

 


   //------------------------------
   // Artithmetic an mathematical operators
   //------------------------------   
  std::cout<<"Test arithmetic and mathematical operators..."<<std::endl;
  slip::DenseVector3dField2d<float> Mat(4,3,2.0);
       
  slip::DenseVector3dField2d<float> MM(3,2);
  MM = 5.5;
  std::cout<<MM<<std::endl;
   
  slip::DenseVector3dField2d<float> MM2(3,2);
  MM2 = 2.2;
  std::cout<<MM2<<std::endl;
   
  MM += MM2;
  std::cout<<MM<<std::endl;
   
   MM -= MM2;
   std::cout<<MM<<std::endl;
   
   MM *= MM2;
   std::cout<<MM<<std::endl;
   
   MM /= MM2;
   std::cout<<MM<<std::endl;
   
   slip::DenseVector3dField2d<float> MM3(3,2);
   MM3 = MM + MM2;
   std::cout<<MM<<"+"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM - MM2;
   std::cout<<MM<<"-"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM * MM2;
   std::cout<<MM<<"*"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM / MM2;
   std::cout<<MM<<"/"<<MM2<<" = "<<MM3<<std::endl;
  
   std::cout<<"-MM3 = "<<-MM3<<std::endl;
   

 

   std::cout<<"M2 "<<std::endl;
   std::cout<<MM2<<std::endl;
   MM2+=1.1;
   std::cout<<MM2<<std::endl;
   MM2-=1.1;
   std::cout<<MM2<<std::endl;
   MM2*=1.1;
   std::cout<<MM2<<std::endl;
   MM2/=1.1;
   std::cout<<MM2<<std::endl;

   slip::Vector3d<float> vvv(4.4,4.4,4.4);
   std::cout<<"M2 "<<std::endl;
   std::cout<<MM2<<std::endl;
   MM2+=vvv;
   std::cout<<MM2<<std::endl;
   MM2-=vvv;
   std::cout<<MM2<<std::endl;
   MM2*=vvv;
   std::cout<<MM2<<std::endl;
   MM2/=vvv;
   std::cout<<MM2<<std::endl;
   
  std::cout<<" mins(M100) = "<<M100.min()<<std::endl;
  std::cout<<" maxs(M100) = "<<M100.max()<<std::endl;

   

   slip::DenseVector3dField2d<double> MM5(3,4);
   slip::Vector3d<double> vv(2.0,2.0,2.0);
   MM5 = M4 + M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5+vv<<std::endl;
   std::cout<<(vv+MM5)<<std::endl;


   MM5 = M4 - M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5-vv<<std::endl;
   std::cout<<(vv-MM5)<<std::endl;
 

   MM5 = M4 * M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5*vv<<std::endl;
   std::cout<<(vv*MM5)<<std::endl;
 

   MM5 = M4 / M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5/vv<<std::endl;
    

   
   std::cout<<MM5<<std::endl;
   std::cout<<MM5+2.0<<std::endl;
   std::cout<<(2.0+MM5)<<std::endl;

   std::cout<<MM5<<std::endl;
   std::cout<<MM5-2.0<<std::endl;
   std::cout<<(2.0-MM5)<<std::endl;
   
   std::cout<<MM5<<std::endl;
   std::cout<<MM5*2.0<<std::endl;
   std::cout<<(2.0*MM5)<<std::endl;
     
   std::cout<<MM5<<std::endl;
   std::cout<<MM5/2.0<<std::endl;
   //std::cout<<(2.0/MM5)<<std::endl;
 
   //M4.apply(std::sqrt);
   //std::cout<<M4<<std::endl;

   slip::Matrix<double> Norm(2,3);
   std::transform(MM5.begin(),MM5.end(),Norm.begin(),std::mem_fun_ref(&slip::Vector3d<double>::Euclidean_norm));
   std::cout<<"Norm ;"<<std::endl;
   std::cout<<Norm<<std::endl;
 //------------------------------

   M4.write_gnuplot("test3d2d.gnu");

   M4.write_tecplot("test3d2d.tecplot","titre","zone");

  //  slip::DenseVector3dField2d<unsigned char> I;
//    I.read("lena_rgb.jpg");
   
//    //slip::DenseVector3dField2d<unsigned char> I2(I.dim1(),I.dim2());
   
//   //  for(std::size_t i = 0; i < I.dim1(); ++i)
// //      {
// //        for(std::size_t j = 0; j < I.dim2(); ++j)
// // 	 {
// // 	   std::cout<<"("<<int(I(i,j).get_red())<<","<<int(I(i,j).get_green())<<","<<int(I(i,j).get_blue())<<")"<<" ";
// // 	 }
// //        std::cout<<std::endl;
// //      }

//    // I.write("lena_rgb.gif");

//    I.write("lena2.gif");
   
//   //    slip::DenseVector3dField2d<unsigned char> I3;
// //      I3.read("lena.gif");

// //      slip::DenseVector3dField2d<float> I4(I3.dim1(),I3.dim2());
// //      std::copy(I3.begin(),I3.end(),I4.begin());
// //      slip::change_dynamic(I4.begin(),I4.end(),I4.begin(),slip::range_fun_inter01<float,float>(I4.min(),I4.max()));
// //      I4.write("lena_d.gif");

//serialization
   // create and open a character archive for output
    std::ofstream ofs("vector3dfield2d.txt");
    slip::DenseVector3dField2d<float> b(3,4);
    slip::iota(b.begin(),b.end(),slip::Vector3d<float>(1.0f,2.0f,3.0f),
	       slip::Vector3d<float>(1.0f,1.0f,1.0f));
    
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<b;
    }//to close archive
    std::ifstream ifs("vector3dfield2d.txt");
    slip::DenseVector3dField2d<float> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("vector3dfield2d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<b;
    }//to close archive

    std::ifstream ifsb("vector3dfield2d.bin");
    slip::DenseVector3dField2d<float> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;
    
     return 0;
}
