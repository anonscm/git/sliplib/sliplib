#include <iostream>
#include <iomanip>
#include <vector>

#include "ContinuousPolyBase1d.hpp"

int main()
{
  typedef double T;
  //not usable because ContinuousPolyBase1d is abstract
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Default constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousPolyBase1d<T,2> default_base2d;
  std::cout<<"default_base2d = \n"<<default_base2d<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Default constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousPolyBase1d<T,3> default_base3d;
  std::cout<<"default_base3d \n= "<<default_base3d<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Constructor 2d" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousPolyBase1d<T,2> base2d1(1,5,true,true,"file");
  std::cout<<"base2d1 = \n"<<base2d1<<std::endl;
  //  base2d1.generate();
  //std::cout<<base2d1<<std::endl;

  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "Orthogonality matrix" << std::endl;
  // std::cout <<  std::setfill('_') <<std::setw(60) << '_' << std::endl;
  // slip::Matrix<T> Gram_base2d1;
  // base2d1.orthogonality_matrix(Gram_base2d1);
  // std::cout<<"Gram_base2d1 = \n"<<Gram_base2d1<<std::endl;

  
 

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Copy constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousPolyBase1d<T,2> copy_base2d(base2d1);
  std::cout<<"copy_base2d = \n"<<copy_base2d<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "operator=" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousPolyBase1d<T,2> equal_base2d;
  equal_base2d = base2d1;
  std::cout<<"equal_base2d = \n"<<equal_base2d<<std::endl;

  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "accessors/mutators" << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  

  

  return 0;
}
