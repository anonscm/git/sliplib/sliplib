#include<iostream>
#include<iterator>
#include <fstream>

#include "DPoint1d.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


int main()
{

  slip::DPoint1d<int> p;
  std::cout<<p<<std::endl;
  std::cout<<"name = "<<p.name()<<std::endl;
  
  slip::DPoint1d<float> pf(2.3);
  std::cout<<pf<<std::endl;

  pf.dx1(2.4);
  std::cout<<pf<<std::endl;

  slip::DPoint1d<float> pf2 = pf;
  std::cout<<pf2<<std::endl;
  
  slip::DPoint1d<float> pf3;
  pf3 = pf;
  std::cout<<pf3<<std::endl;
    
  std::cout<<pf3[0]<<std::endl;
  std::cout<<pf3.dx1()<<std::endl;
  
  std::cout<<"dim = "<<pf3.dim()<<std::endl;
  std::cout<<"size = "<<pf3.size()<<std::endl;
  std::cout<<"empty = "<<pf3.empty()<<std::endl;

  slip::DPoint1d<float> pf4(3.0);
  pf4.swap(pf3);
  std::cout<<pf3<<std::endl;
  std::cout<<pf4<<std::endl;
 
  pf4[0] = 6.0;
  std::cout<<pf4<<std::endl;
 
  pf4.dx1() = 8.0;
  std::cout<<pf4<<std::endl;
 
 
  // pf4[3] = 6.0;
  std::cout<<-pf4<<std::endl;
  
  std::cout<<"pf3 == pf4 :"<<(pf3==pf4)<<std::endl;
  std::cout<<"pf3 != pf4 :"<<(pf3!=pf4)<<std::endl;

  std::cout<<(pf3 + pf4)<<std::endl;
  std::cout<<(pf3 - pf4)<<std::endl;
  std::cout<<(pf3 += pf4)<<std::endl;
  std::cout<<(pf3 -= pf4)<<std::endl;
  

  //
  //serialization
  //
    std::ofstream ofs("dpoint1d.txt");
    slip::DPoint1d<double> ps(1.3);
        
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<ps;
    }//to close archive
    std::ifstream ifs("dpoint1d.txt");
    slip::DPoint1d<double> new_ps;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_ps;
    }//to close archive
    std::cout<<"new_ps\n"<<new_ps<<std::endl;

    std::ofstream ofsb("dpoint1d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<ps;
    }//to close archive

    std::ifstream ifsb("dpoint1d.bin");
    slip::DPoint1d<double> new_psb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_psb;
    }//to close archive
    std::cout<<"new_psb\n"<<new_psb<<std::endl;

return 0;
}
