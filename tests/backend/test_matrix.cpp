#include "Matrix.hpp"
#include "Array2d.hpp"
#include "Point2d.hpp"
#include "DPoint2d.hpp"
#include "Box2d.hpp"
#include "iterator2d_box.hpp"
#include <iostream>
#include <fstream>
#include <complex>
#include <algorithm>
#include <numeric>
#include <vector>
#include "arithmetic_op.hpp"
#include "linear_algebra.hpp"

#include "arithmetic_op.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


float function2(float a, float b)
{
  return a * b;
}

template<typename T>
struct pixelRGB
{
  T red;
  T green;
  T blue;

  T get_red()const {return red;}
  T get_green()const {return green;}
  T get_blue()const {return blue;}
  void set_red(const T& r){ red = r;}
  void set_green(const T& g){ green = g;}
  void set_blue(const T& b){ blue = b;}
};

class A
{
public :
  A():
  n_(0),data_(0){}
  A(const std::size_t n):
    n_(n),data_(new double[n])
  {
    for(size_t i = 0; i < n_; ++i)
      data_[i] = i;
  }

  A(const A& rhs):
    n_(rhs.n_)
  {
    data_ = new double[rhs.n_];
    std::copy(&(rhs.data_[0]), &(rhs.data_[0]) + rhs.n_,&(data_[0]));
  }
  
  A& operator=(const A& rhs)
  {
    if(this != &rhs)
      {
	if(data_ != 0)
	  {
	    delete[] data_;
	  }
	n_ = rhs.n_;
	data_ = new double[rhs.n_];
	std::copy(&(rhs.data_[0]), &(rhs.data_[0]) + rhs.n_,&(data_[0]));
      }
    return *this;
  }
  ~A()
  {
    if(data_ != 0)
      {
	delete[] data_;
      } 
  }
  
  //Utile ici ?, il vaut mieux le mettre
  void swap(A& x)
  {
    for(size_t n = 0; n < n_; ++n)
      std::swap(data_[n],x.data_[n]);
  }
  void print()
  {
    std::cout<<"[ ";
    for(size_t i = 0; i < n_; ++i)
      std::cout<<data_[i]<<" ";
    std::cout<<"] ";
  }
  private :
  std::size_t n_;
  double* data_;

};



int main()
{


   //------------------------------
  //constructors
  //------------------------------
  slip::Matrix<double> M;
  std::cout<<"name = "<<M.name()<<" dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 

 
  slip::Matrix<double> M2(6,3);
  std::cout<<"dim1 = "<<M2.dim1()<<" dim2 = "<<M2.dim2()<<" size = "<<M2.size()<<" empty = "<<M2.empty()<<std::endl; 
  std::cout<<"rows = "<<M2.rows()<<" cols = "<<M2.cols()<<" columns = "<<M2.columns()<<std::endl;
  std::cout<<M2<<std::endl; 
 
  slip::Matrix<double> M3(6,3,7.1);
  std::cout<<"dim1 = "<<M2.dim1()<<" dim2 = "<<M2.dim2()<<" size = "<<M2.size()<<" empty = "<<M2.empty()<<std::endl; 
  std::cout<<"rows = "<<M3.rows()<<" cols = "<<M3.cols()<<" columns = "<<M3.columns()<<std::endl;
  std::cout<<M3<<std::endl;


 double d[] = {1.2,3.4,5.5,6.6,7.7,8.8};
 slip::Matrix<double> M4(2,3,d);
 std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
 std::cout<<M4<<std::endl; 
 
 

  std::vector<int> V(20);
  for(std::size_t i = 0; i < 20; ++i)
    V[i] = i;
  slip::Matrix<int> M5(4,5,V.begin(),V.end());
  std::cout<<"dim1 = "<<M5.dim1()<<" dim2 = "<<M5.dim2()<<" size = "<<M5.size()<<" empty = "<<M5.empty()<<std::endl; 
 std::cout<<M5<<std::endl; 


 slip::Matrix<double> M6 = M4;
  std::cout<<"dim1 = "<<M6.dim1()<<" dim2 = "<<M6.dim2()<<" size = "<<M6.size()<<" empty = "<<M6.empty()<<std::endl; 
 std::cout<<M6<<std::endl; 


 //------------------------------

 //------------------------------
 //resize
 //------------------------------
  M.resize(4,3,5.6);
  std::cout<<"dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 
  //------------------------------


  //------------------------------
  //Affectation
  //------------------------------
  slip::Matrix<double> M7;
  M7 = M;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M7 = M7;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M4 = 12.2;
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl;   
  //------------------------------

  
  //------------------------------
  //Element access operators
  //------------------------------
  M2(2,1) = 2.1;
  M2[1][2] = 1.2;
  
  for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2[i][j]<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
   for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2(i,j)<<" ";
	}
      std::cout<<std::endl;
    }

   slip::Matrix<double> Arange(10,9);
   double val = 0.0;
    for(size_t i = 0; i < Arange.dim1(); ++i)
    {
      for(size_t j = 0; j < Arange.dim2(); ++j)
	{
	  Arange(slip::Point2d<std::size_t>(i,j)) = val;
	  val = val + 1.0;
	}
    }

    for(size_t i = 0; i < Arange.dim1(); ++i)
    {
      for(size_t j = 0; j < Arange.dim2(); ++j)
	{
	  std::cout<<Arange(slip::Point2d<std::size_t>(i,j))<<" ";
	}
      std::cout<<std::endl;
    }
 
    std::cout<<std::endl;
    slip::Range<int> range_r(0,Arange.dim2()-1,2);
    slip::Range<int> range_c(0,Arange.dim1()-1,2);
    slip::Matrix<double> Aext = Arange(range_r,range_c);
    std::cout<<Aext<<std::endl;
  //------------------------------

   //------------------------------
   //swap
   //------------------------------
   M2.swap(M3);
   std::cout<<M2<<std::endl;
   std::cout<<std::endl;
   std::cout<<M3<<std::endl;
   //------------------------------


   //----------------------------
   //iterators
   //----------------------------
   slip::Matrix<double> M8(6,3,4.4);
 //   slip::Matrix<double> const Mc8(M8);
//    slip::Matrix<double>::const_iterator2d cit = Mc8.upper_left();
   std::cout << "yop" << std::endl;
   std::cout<<std::endl;
   std::copy(M3.begin(),M3.end(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(M3.rbegin(),M3.rend(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl<<std::endl;
   
   
   std::transform(M3.rbegin(),M3.rend(),M8.begin(),M2.begin(),std::plus<double>());
  
   std::cout<<M2<<std::endl;
   
   std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_begin(2),std::plus<double>());
   std::cout<<M2<<std::endl;

   std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_begin(2),std::plus<double>());
   std::cout<<M2<<std::endl;


   std::copy(M2.row_rbegin(1),M2.row_rend(1),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(M2.col_rbegin(1),M2.col_rend(1),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   

    std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_rbegin(2),std::plus<double>());
   std::cout<<M2<<std::endl;

   std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_rbegin(2),std::plus<double>());
   std::cout<<M2<<std::endl;
   
   std::partial_sum(M2.begin(),M2.end(),M2.begin());
   std::cout<<"iterator2d_box test..."<<std::endl;
   std::cout<<M2<<std::endl;
   std::cout<<"bottom_right() and upper_left() test "<<std::endl;
   std::copy(M2.upper_left(),M2.bottom_right(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(M2.rupper_left(),M2.rbottom_right(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;

   std::cout<<"bottom_right(box) and upper_left(box) box(2,1,4,2) test "<<std::endl;
   slip::Box2d<int> box(2,1,4,2);
   std::copy(M2.upper_left(box),M2.bottom_right(box),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(M2.rupper_left(box),M2.rbottom_right(box),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   
   slip::Matrix<double> Mask(3,2,0.0);
   double k = 1.0;
   for(std::size_t i = 0; i < Mask.dim1(); ++i)
     for(std::size_t j = 0; j < Mask.dim2(); ++j)
       {
	 Mask[i][j] = k;
	 k += 1.0;
       }
   std::copy(Mask.upper_left(),Mask.bottom_right(),
	     M2.upper_left(box));
   std::cout<<M2<<std::endl;
  
   std::copy(Mask.upper_left(),Mask.bottom_right(),
	     M2.rupper_left(box));
   std::cout<<M2<<std::endl;
   
   std::transform(M2.upper_left(box),M2.bottom_right(box),Mask.upper_left(),M2.upper_left(box),std::plus<double>());
   std::cout<<M2<<std::endl;
   
   std::transform(M2.upper_left(box),M2.bottom_right(box),Mask.rupper_left(),M2.upper_left(box),std::plus<double>());
   std::cout<<M2<<std::endl;
 
   slip::Matrix<int> Mrange(10,10);
   int ind = 0;
   for(std::size_t i = 0; i < Mrange.dim1(); ++i)
     for(std::size_t j = 0; j < Mrange.dim2(); ++j)
       {
	 Mrange[i][j] = ind;
	 ind += 1;
       }
   std::cout<<Mrange<<std::endl;
   std::cout<<"row_range_iterator ..."<<std::endl;
   //slip::Range<int> range(2,7,2);
   slip::Range<int> range(0,(Mrange.dim2() - 1),2);
   std::copy(Mrange.row_begin(3,range),Mrange.row_end(3,range),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(Mrange.row_rbegin(3,range),Mrange.row_rend(3,range),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;

   std::cout<<"col_range_iterator ..."<<std::endl;
   //slip::Range<int> range2(2,7,2);
   slip::Range<int> range2(0,(Mrange.dim1() - 1),2);
   std::copy(Mrange.col_begin(2,range2),Mrange.col_end(2,range2),std::ostream_iterator<double>(std::cout,"\n"));
   std::cout<<std::endl;
   std::copy(Mrange.col_rbegin(2,range2),Mrange.col_rend(2,range2),std::ostream_iterator<double>(std::cout,"\n"));
   std::cout<<std::endl;

   //std::copy(Mrange.row_begin(4,range),Mrange.row_end(4,range),Mrange.row_begin(8,range));

  std::copy(Mrange.col_begin(4,range2),Mrange.col_end(4,range2),Mrange.col_begin(2,range));

   std::cout<<Mrange<<std::endl;
   
   std::copy(Mrange.upper_left(range,range2),Mrange.bottom_right(range,range2),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(Mrange.rupper_left(range,range2),Mrange.rbottom_right(range,range2),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(Mrange.upper_left(range),Mrange.bottom_right(range),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
    std::copy(Mrange.rupper_left(range),Mrange.rbottom_right(range),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   
  
  //----------------------------  

   //------------------------------
  //Comparison operators
  //------------------------------
  std::cout<<" M3 == M2 "<<(M3 == M2)<<std::endl;
  std::cout<<" M3 != M2 "<<(M3 != M2)<<std::endl;
  std::cout<<" M3 > M2 "<<(M3 > M2)<<std::endl;
  std::cout<<" M3 < M2 "<<(M3 < M2)<<std::endl;
  std::cout<<" M3 >= M2 "<<(M3 >= M2)<<std::endl;
  std::cout<<" M3 <= M2 "<<(M3 <= M2)<<std::endl;
  //------------------------------


  //------------------------------
  //fill methods
  //------------------------------
  M3.fill(4.0);
  std::cout<<M3<<std::endl;
  double d2[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0};
  M3.fill(d2);
  std::cout<<M3<<std::endl;
  std::cout<<std::endl;
  std::vector<int> V2(18);
  for(std::size_t i = 0; i < 18; ++i)
    V2[i] = i;
  M3.fill(V2.begin(),V2.end());
  std::cout<<M3<<std::endl;
  //------------------------------
  

  //------------------------------
  // With non primitive data
  //------------------------------
  A S(2);
  S.print();
  std::cout<<std::endl;
  slip::Matrix<A> MA(3,4,S);
 
  std::cout<<"dim1 = "<<MA.dim1()<<" dim2 = "<<MA.dim2()<<" size = "<<MA.size()<<" empty = "<<MA.empty()<<std::endl; 
   for(size_t i = 0; i < MA.dim1(); ++i)
    {
      for(size_t j = 0; j < MA.dim2(); ++j)
	{
	  MA[i][j].print();
	}
      std::cout<<std::endl;
    }
 

   std::complex<double> c(2.3,2.3);
   std::cout<<c<<std::endl;
   slip::Matrix<std::complex<double> > MC(3,2,c);
   std::cout<<MC<<std::endl; 

   std::cout<<MC[1+1][0+1]<<std::endl;
  

  

   
   slip::Matrix<slip::block<double,3> > Mmulti(3,4);

   for(size_t i = 0; i < Mmulti.dim1(); ++i)
     {
       for(size_t j = 0; j < Mmulti.dim2(); ++j)
	 {
	   Mmulti[i][j][0] = i + j ;
	   Mmulti[i][j][1] = 2 * (i + j);
	   Mmulti[i][j][2] = 3 * (i + j);
	 }
     }

   for(size_t i = 0; i < Mmulti.dim1(); ++i)
     {
       for(size_t j = 0; j < Mmulti.dim2(); ++j)
	 {
	   std::cout<<Mmulti[i][j];
	   //std::cout<<Mmulti[i][j][0]<<" ";
	 }
       std::cout<<std::endl;
     }

   std::cout<<"Premier plan ..."<<std::endl;
   for(size_t i = 0; i < Mmulti.dim1(); ++i)
     {
       for(size_t j = 0; j < Mmulti.dim2(); ++j)
	 {
	   std::cout<<Mmulti[i][j][0]<<" ";
	 }
       std::cout<<std::endl;
     }
 
   slip::Matrix<pixelRGB<double> > MRGB(3,4);
   for(size_t i = 0; i < MRGB.dim1(); ++i)
     {
       for(size_t j = 0; j < MRGB.dim2(); ++j)
	 {
	   MRGB[i][j].set_red(i + j) ;
	   MRGB[i][j].set_green(2 * (i + j));
	   MRGB[i][j].set_blue(3 * (i + j));
	 }
     }
   
   std::cout<<"Plan vert..."<<std::endl;
   for(size_t i = 0; i < MRGB.dim1(); ++i)
     {
       for(size_t j = 0; j < MRGB.dim2(); ++j)
	 {
	   //std::cout<<MRGB[i][j].get_green()<<" ";
	   std::cout<<MRGB[i][j].green<<" ";
	 }
       std::cout<<std::endl;
     }
   //------------------------------



   //------------------------------
   // Artithmetic an mathematical operators
   //------------------------------   
   slip::Matrix<float> Mat(4,3,2.0);
       
   slip::Matrix<float> MM(3,2);
   MM = 5.5;
   std::cout<<MM<<std::endl;
   
   slip::Matrix<float> MM2(3,2);
   MM2 = 2.2;
   std::cout<<MM2<<std::endl;
   
   MM += MM2;
   std::cout<<MM<<std::endl;
   
   MM -= MM2;
   std::cout<<MM<<std::endl;
   
   MM *= MM2;
   std::cout<<MM<<std::endl;
   
   MM /= MM2;
   std::cout<<MM<<std::endl;
   
   slip::Matrix<float> MM3(3,2);
   MM3 = MM + MM2;
   std::cout<<MM<<"+"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM - MM2;
   std::cout<<MM<<"-"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM * MM2;
   std::cout<<MM<<"*"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM / MM2;
   std::cout<<MM<<"/"<<MM2<<" = "<<MM3<<std::endl;
  
   std::cout<<"-MM3 = "<<-MM3<<std::endl;
   
   std::cout<<"M2 "<<std::endl;
   std::cout<<MM2<<std::endl;
   MM2+=1.1;
   std::cout<<MM2<<std::endl;
   MM2-=1.1;
   std::cout<<MM2<<std::endl;
   MM2*=1.1;
   std::cout<<MM2<<std::endl;
   MM2/=1.1;
   std::cout<<MM2<<std::endl;
   
   std::cout<<M4.min()<<std::endl;
   std::cout<<M4.max()<<std::endl;
   std::cout<<M4.sum()<<std::endl;
   
   std::cout<<min(M4)<<std::endl;
   std::cout<<max(M4)<<std::endl;
   
   slip::Matrix<double> Mn(4,4);
   slip::iota(Mn.begin(),Mn.end(),1.0,1.0);
   std::cout<<Mn<<std::endl;
   std::cout<<"L2   norm : "<<Mn.L2_norm()<<std::endl;
   std::cout<<"L1   norm : "<<Mn.L1_norm()<<std::endl;
   std::cout<<"Linf norm : "<<Mn.infinite_norm()<<std::endl;
   std::cout<<"trace : "<<Mn.trace()<<std::endl;
   std::cout<<"det : "<<Mn.det()<<std::endl;
   double Mn_tab[] = {10.0,7.0,8.0,7.0,7.0,5.0,6.0,5.0,8.0,6.0,10.0,9.0,7.0,5.0,9.0,10.0};
   slip::Matrix<double> Mn2(4,4,Mn_tab);
   std::cout<<Mn2<<std::endl;
   std::cout<<"inverse : "<<std::endl;
   std::cout<<Mn2.inv()<<std::endl;
   std::cout<<"condition number  : "<<Mn2.cond()<<std::endl;
   std::cout<<"rank  : "<<Mn2.rank()<<std::endl;
   
   
   slip::Matrix<double> MM5(3,4);
   MM5 = M4 + M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5+2.0<<std::endl;
   std::cout<<(2.0+MM5)<<std::endl;
   
   std::cout<<MM5<<std::endl;
   std::cout<<MM5-2.0<<std::endl;
   std::cout<<(2.0-MM5)<<std::endl;
   
   std::cout<<MM5<<std::endl;
   std::cout<<MM5*2.0<<std::endl;
   std::cout<<(2.0*MM5)<<std::endl;
     
   std::cout<<MM5<<std::endl;
   std::cout<<MM5/2.0<<std::endl;
   //   std::cout<<(2.0/MM5)<<std::endl;
 
   std::cout<<abs(-M4)<<std::endl;
   std::cout<<sqrt(M4)<<std::endl;
   std::cout<<cos(M4)<<std::endl;
   std::cout<<acos(M4)<<std::endl;
   std::cout<<sin(M4)<<std::endl;
   std::cout<<asin(M4)<<std::endl;
   std::cout<<tan(M4)<<std::endl;
   std::cout<<atan(M4)<<std::endl;
   std::cout<<exp(M4)<<std::endl;
   std::cout<<log(M4)<<std::endl;
   std::cout<<cosh(M4)<<std::endl;
   std::cout<<sinh(M4)<<std::endl;
   std::cout<<tanh(M4)<<std::endl;
   std::cout<<log10(M4)<<std::endl;

   M4.apply(std::sqrt);
   std::cout<<M4<<std::endl;

 //------------------------------

 

//------------------------------
   // Artithmetic and mathematical operators complex
   //------------------------------   
    typedef std::complex<double> TC;
    typedef std::complex<float> TCf;
    slip::Matrix<std::complex<float> > Matc(4,3,2.0);
       
   slip::Matrix<std::complex<float> > MMc(3,2);
   MMc = TCf(5.5);
   std::cout<<MMc<<std::endl;
   
   slip::Matrix<std::complex<float> > MM2c(3,2);
   MM2c = TCf(2.2);
   std::cout<<MM2c<<std::endl;
   
   MMc += MM2c;
   std::cout<<MMc<<std::endl;
   
   MMc -= MM2c;
   std::cout<<MMc<<std::endl;
   
   MMc *= MM2c;
   std::cout<<MMc<<std::endl;
   
   MMc /= MM2c;
   std::cout<<MMc<<std::endl;
   
   slip::Matrix<std::complex<float> > MM3c(3,2);
   MM3c = MMc + MM2c;
   std::cout<<MMc<<"+"<<MM2c<<" = "<<MM3c<<std::endl;
  
   MM3c = MMc - MM2c;
   std::cout<<MMc<<"-"<<MM2c<<" = "<<MM3c<<std::endl;
  
   MM3c = MMc * MM2c;
   std::cout<<MMc<<"*"<<MM2c<<" = "<<MM3c<<std::endl;
  
   MM3c = MMc / MM2c;
   std::cout<<MMc<<"/"<<MM2c<<" = "<<MM3c<<std::endl;
  
   std::cout<<"-MM3c = "<<-MM3c<<std::endl;
   
   std::cout<<"M2c "<<std::endl;
   std::cout<<MM2c<<std::endl;
   MM2c+=TCf(1.1f,0.0f);
   std::cout<<MM2c<<std::endl;
   MM2c-=TCf(1.1f,0.0f);
   std::cout<<MM2c<<std::endl;
   MM2c*=TCf(1.1f,0.0f);
   std::cout<<MM2c<<std::endl;
   MM2c/=TCf(1.1f,0.0f);
   std::cout<<MM2c<<std::endl;
   

   slip::Matrix<std::complex<double> > M4c(4,4);
   slip::iota(M4c.begin(),M4c.end(),std::complex<double>(1.0,1.0),std::complex<double>(1.0,1.0));
   std::cout<<M4.min()<<std::endl;
   std::cout<<M4.max()<<std::endl;
    std::cout<<M4c.sum()<<std::endl;
   
    std::cout<<min(M4)<<std::endl;
    std::cout<<max(M4)<<std::endl;
   
   slip::Matrix<std::complex<double> > Mnc(4,4);
   slip::iota(Mnc.begin(),Mnc.end(),std::complex<double>(1.0,1.0),std::complex<double>(1.0,1.0));
   std::cout<<Mnc<<std::endl;
   std::cout<<"L2   norm : "<<Mnc.L2_norm()<<std::endl;
   std::cout<<"L1   norm : "<<Mnc.L1_norm()<<std::endl;
   std::cout<<"Linf norm : "<<Mnc.infinite_norm()<<std::endl;
   std::cout<<"trace : "<<Mnc.trace()<<std::endl;
   std::cout<<"det : "<<Mnc.det()<<std::endl;
  
   std::complex<double> Mn_tabc[] = {TC(10.0,1.0),TC(7.0,0.2),TC(8.0,0.1),TC(7.0,0.3),
				     TC(7.0,2.0),TC(5.0,1.0),TC(6.0,0.5),TC(5.0,0.4),
				     TC(8.0,1.0),TC(6.0,0.1),TC(10.0,0.2),TC(9.0,0.1),
				     TC(7.0,0.9),TC(5.0,0.5),TC(9.0,0.7),TC(10.0,0.8)};
   slip::Matrix<std::complex<double> > Mn2c(4,4,Mn_tabc);
   std::cout<<Mn2c<<std::endl;
   std::cout<<"inverse : "<<std::endl;
   slip::Matrix<std::complex<double> > Mn2cinv(4,4);
   Mn2cinv = Mn2c.inv();
   std::cout<<Mn2cinv<<std::endl;
   std::cout<<"verification : "<<std::endl;
   slip::Matrix<std::complex<double> > Mn2cMn2cinv(4,4);
   slip::matrix_matrix_multiplies(Mn2c,Mn2cinv,Mn2cMn2cinv);
   std::cout<<Mn2cMn2cinv<<std::endl;
   std::cout<<"condition number  : "<<Mn2c.cond()<<std::endl;
   std::cout<<"rank  : "<<Mn2c.rank()<<std::endl;
   
   
   slip::Matrix<std::complex<double> > MM5c(3,4);
   MM5c = M4c + M4c;
   std::cout<<MM5c<<std::endl;
   std::cout<<MM5c+TC(2.0)<<std::endl;
   std::cout<<(TC(2.0)+MM5c)<<std::endl;
   
   std::cout<<MM5c<<std::endl;
   std::cout<<MM5c-TC(2.0)<<std::endl;
   std::cout<<(TC(2.0)-MM5c)<<std::endl;
   
   std::cout<<MM5c<<std::endl;
   std::cout<<MM5c*TC(2.0)<<std::endl;
   std::cout<<(TC(2.0)*MM5c)<<std::endl;
     
   std::cout<<MM5c<<std::endl;
   std::cout<<MM5c/TC(2.0)<<std::endl;
   //   std::cout<<(2.0/MM5c)<<std::endl;
 
   std::cout<<"Mathematical operators "<<std::endl;
   std::cout<<abs(-M4c)<<std::endl;
   std::cout<<sqrt(M4c)<<std::endl;
   std::cout<<cos(M4c)<<std::endl;
   //std::cout<<acos(M4c)<<std::endl;
   std::cout<<sin(M4c)<<std::endl;
   //std::cout<<asin(M4c)<<std::endl;
   std::cout<<tan(M4c)<<std::endl;
   //std::cout<<atan(M4c)<<std::endl;
   std::cout<<exp(M4c)<<std::endl;
   std::cout<<log(M4c)<<std::endl;
   std::cout<<cosh(M4c)<<std::endl;
   std::cout<<sinh(M4c)<<std::endl;
   std::cout<<tanh(M4c)<<std::endl;
   std::cout<<log10(M4c)<<std::endl;

   M4c.apply(std::sqrt);
   std::cout<<M4c<<std::endl;

   //--------------------------------------
   // serialization
   //--------------------------------------
    // create and open a character archive for output
    std::ofstream ofs("matrix.txt");
    slip::Matrix<float> b(3,4);
    slip::iota(b.begin(),b.end(),1.0f);
    
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<b;
    }//to close archive
    std::ifstream ifs("matrix.txt");
    slip::Matrix<float> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("matrix.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<b;
    }//to close archive

    std::ifstream ifsb("matrix.bin");
    slip::Matrix<float> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;
    

    slip::Matrix<std::complex<float> > bc(3,4);
    slip::iota(bc.begin(),bc.end(),std::complex<float>(1.0f,2.0f),
    	       std::complex<float>(1.0f,1.0f));
    std::ofstream ofsc("matrixc.txt");
     {
       
       boost::archive::text_oarchive oac(ofsc);
       oac<<bc;
     }//to close archive
    std::ifstream ifsc("matrixc.txt");
    slip::Matrix<std::complex<float> > new_pc;
    {
    boost::archive::text_iarchive iac(ifsc);
    iac>>new_pc;
    }//to close archive
    std::cout<<"new_pc = \n"<<new_pc<<std::endl;

   return 0;
}
