#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <cmath>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <string>

#include "Vector.hpp"
#include "Matrix.hpp"
#include "io_tools.hpp"

#include "CameraModel.hpp"
#include "PinholeDLTCamera.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

//using namespace::std;

// main program
//int main(int argc, char *argv[])
int main()
{
  //typedef float Type_Calcul;
  typedef double Type_Calcul;


  // --------------------------------------------------      
  // ----- constructors          ----------------------
  // --------------------------------------------------   
  std::cout<<"--Default constructor ---"<<std::endl;
  slip::PinholeDLTCamera<Type_Calcul> pinholeDLT_camera1;
  std::cout<<pinholeDLT_camera1<<std::endl;
  std::cout<<std::endl;
 
  std::cerr << "reading calibration files" <<std::endl;
  pinholeDLT_camera1.read("./../../../../tests/images/pinhole.cal");
  std::cout<<pinholeDLT_camera1<<std::endl;
  std::cout<<std::endl;
 
  std::cout<<"--Copy constructor ---"<<std::endl;
  slip::PinholeDLTCamera<Type_Calcul> pinholeDLT_camera2(pinholeDLT_camera1);
  std::cout<<pinholeDLT_camera2<<std::endl;
  std::cout<<std::endl;

  std::cout<<"-- operator= ---"<<std::endl;
  slip::PinholeDLTCamera<Type_Calcul> pinholeDLT_camera3;
  pinholeDLT_camera3 = pinholeDLT_camera1;
  std::cout<<pinholeDLT_camera3<<std::endl;
  std::cout<<std::endl;

  std::cout<<"-- self assignment ---"<<std::endl;
  pinholeDLT_camera1 = pinholeDLT_camera1;
  std::cout<<pinholeDLT_camera1<<std::endl;
  std::cout<<std::endl;
  
 // --------------------------------------------------      
  // ----- Accessors/Mutators    ----------------------
  // --------------------------------------------------
  std::cout<<"Calibration matrix = \n"<<pinholeDLT_camera1.calibration_matrix()<<std::endl;

  slip::Matrix<Type_Calcul> Cal1(3,4);
  Cal1[0][0] = Type_Calcul(1.0);
  Cal1[1][1] = Type_Calcul(1.0);
  Cal1[2][3] = Type_Calcul(1.0);
  pinholeDLT_camera3.calibration_matrix(Cal1);
  std::cout<<pinholeDLT_camera3<<std::endl;
  std::cout<<std::endl;
  // std::cout<<"-- compute ---"<<std::endl;
  // slip::Matrix<Type_Calcul> data_pinholeDLT(x,x);
  // slip::PinholeDLTCamera<Type_Calcul> pinholeDLT_camera4;
  // pinholeDLT_camera4.compute(P);
  // std::cout<<pinholeDLT_camera4<<std::endl;
  //std::cout<<std::endl;
  
  std::cout<<"-- decompose RQ ---"<<std::endl;
  slip::Matrix<Type_Calcul> K(3,3);
  slip::Matrix<Type_Calcul> R(3,3);
  slip::Vector3d<Type_Calcul> c;
   
  pinholeDLT_camera1.decompose(K,R,c,slip::RQ);
  std::cout<<"K = \n"<<K<<std::endl;
  std::cout<<"R = \n"<<R<<std::endl;
  std::cout<<"c = \n"<<c<<std::endl;
  std::cout<<std::endl;

  std::cout<<"-- decompose direct ---"<<std::endl;
  K.fill(Type_Calcul(0.0));
  R.fill(Type_Calcul(0.0));
  c.fill(Type_Calcul(0.0));
  pinholeDLT_camera1.decompose(K,R,c,slip::direct);
  std::cout<<"K = \n"<<K<<std::endl;
  std::cout<<"R = \n"<<R<<std::endl;
  std::cout<<"c = \n"<<c<<std::endl;
  std::cout<<std::endl;
  
  // --------------------------------------------------      
  // ----- camera models reading ----------------------
  // --------------------------------------------------      
  std::cerr << "reading calibration files" <<std::endl;

  slip::CameraModel<Type_Calcul>* pinholeDLT_camera;

  std::cerr << "reading pinholeDLT camera model" << std::endl;
  pinholeDLT_camera = new slip::PinholeDLTCamera<Type_Calcul>();
  pinholeDLT_camera->read("./../../../../tests/images/pinhole.cal");

  
  //int ni=100000000;
  int ni = 10;
  std::cerr<<"computes "<<ni<<" projection/backprojection "<<std::endl;
  std::cerr<<std::endl;
  std::time_t H1,H2;
  // computing pinhole
  time(&H1);
  slip::Point2d<Type_Calcul> pixp(Type_Calcul(1),Type_Calcul(1));
  Type_Calcul z=Type_Calcul(1);
  slip::Point3d<Type_Calcul> voxp;
  
  for (int i=0; i<ni; ++i)
    {
      //pixp=slip::Point2d<Type_Calcul>(Type_Calcul(1),Type_Calcul(1));
      pixp=slip::Point2d<Type_Calcul>(Type_Calcul(2),Type_Calcul(3));
      
      voxp=(*pinholeDLT_camera).backprojection(pixp,z);
      //std::cout<<voxp<<std::endl;
      pixp=(*pinholeDLT_camera).projection(voxp);
      //std::cout<<pixp<<std::endl;
    }

  time(&H2);
 std::cerr <<std::endl;
 std::cerr << "####### pinhole computation time ###################" <<std::endl;
  if ((H2-H1)% 86400 / 3600 !=0 )
   std::cerr << ((H2-H1)% 86400 / 3600) << " h ";
  if ((H2-H1) % 3600 / 60 !=0)
   std::cerr << ((H2-H1) % 3600 / 60) << " min ";
  if ((H2-H1) % 60 !=0)
   std::cerr << ((H2-H1) % 60) << " s";
 std::cerr <<std::endl;
 std::cerr << "#####################################################" <<std::endl;
 std::cerr <<std::endl;
  

  delete pinholeDLT_camera;

  //
  //serialization
  //
   std::cout<<"--Default constructor ---"<<std::endl;
  slip::PinholeDLTCamera<Type_Calcul> pinhole_camera_s;
  std::cout<<pinhole_camera_s<<std::endl;
  std::cout<<std::endl;
 
  std::cerr << "reading calibration files" <<std::endl;
  pinhole_camera_s.read("./../../../../tests/images/pinhole.cal");
  std::cout<<pinhole_camera_s<<std::endl;
  std::cout<<std::endl;

  // create and open a character archive for output
    std::ofstream ofs("pinholeDLTcamera.txt");
      
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<pinhole_camera_s;
    }//to close archive
    std::ifstream ifs("pinholeDLTcamera.txt");
    slip::PinholeDLTCamera<Type_Calcul>  new_pinhole;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_pinhole;
    }//to close archive
    std::cout<<"new_pinhole\n"<<new_pinhole<<std::endl;

    std::ofstream ofsb("pinholeDLTcamera.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<pinhole_camera_s;
    }//to close archive

    std::ifstream ifsb("pinholeDLTcamera.bin");
    slip::PinholeDLTCamera<Type_Calcul> new_pinholeb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_pinholeb;
    }//to close archive
    std::cout<<"new_pinholeb\n"<<new_pinholeb<<std::endl;
 
}





