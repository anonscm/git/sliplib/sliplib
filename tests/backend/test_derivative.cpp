#include "DPoint2d.hpp"
#include "Box2d.hpp"
#include "Array2d.hpp"
#include "iterator2d_box.hpp"
#include <iostream>
#include <complex>
#include <algorithm>
#include <numeric>




//first derivative
template <typename OutType,typename RandomAccessIterator>
struct dx : public std::unary_function<RandomAccessIterator,OutType>
{
  OutType operator()(typename std::iterator_traits<RandomAccessIterator>::value_type& it) const
  {
     
    return (OutType)( *(&it + 1) - *(&it - 1)) / 2;
  }
};

template <typename OutType,typename RandomAccessIterator>
struct dx2 : public std::unary_function<RandomAccessIterator,OutType>
{
  OutType operator()(typename std::iterator_traits<RandomAccessIterator>::value_type& it)
  {
    typename RandomAccessIterator::difference_type left(0,-1);
    typename RandomAccessIterator::difference_type right(0,1);
    std::cout<<left<<std::endl;
    return (OutType)( (it[ left] - it[right])) / 2;
   }
};

 
template<typename _InputIterator, typename _OutputIterator,
           typename _UnaryOperation>
    _OutputIterator
    transform2(_InputIterator __first, _InputIterator __last,
	       _OutputIterator __result, _UnaryOperation __unary_op)
    {
    
      for ( ; __first != __last; ++__first, ++__result)
        *__result = __unary_op(__first);
      return __result;
    }


int main()
{

  
  double d[] = {1.2,3.4,5.5,6.6,7.7,8.8,9.9,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0};
  slip::Array2d<double> M(4,4,d);
  std::cout<<M<<std::endl;
  slip::Array2d<double> Result(4,4);
  
  slip::Box2d<int> inside(1,1,2,2);
 //  std::transform(M.upper_left(inside),M.bottom_right(inside),Result.upper_left(inside),dx<double,slip::Array2d<double>::const_iterator2d >());
  
//   slip::Array2d<double>::iterator2d it = M.upper_left(inside);
//   slip::Array2d<double>::iterator2d::difference_type left(1,1);
  
//   std::cout<<it[left]<<std::endl;

  // std::transform(M.upper_left(inside),M.bottom_right(inside),Result.upper_left(inside),dx2<double,slip::Array2d<double>::iterator2d >());
  //transform2(M.upper_left(inside),M.bottom_right(inside),Result.upper_left(inside),dx2<double,slip::Array2d<double>::iterator2d >());
  std::cout<<Result<<std::endl;

}
