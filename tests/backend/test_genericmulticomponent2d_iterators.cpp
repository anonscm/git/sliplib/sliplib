#include<algorithm>
#include<iostream>
#include<iomanip>
#include<iterator>
#include <numeric>
#include <complex>

#include "Vector3d.hpp"
#include "GenericMultiComponent2d.hpp"


template<typename Vect>
void daccum(Vect& a){
  static int init = 0; 
  a[0] = init++;
  a[1] = init++;
  a[2] = init++;
}
template<typename T>
void disp(T const& a){std::cout << a <<" ";}

int main()
{

  typedef slip::Vector3d<double> T; 
  //GenericMultiComponent2d
  std::cout <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "GenericMultiComponent2d for tests and test of begin() end end() iterators" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  slip::GenericMultiComponent2d<T> M(5,4);
  std::for_each(M.begin(),M.end(),daccum<T>);
  std::cout<<M<<std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;

  //ranges
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Ranges for tests" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Range<int> rj(0,M.dim1()-1,2);
  slip::Range<int> rjd(1,M.dim1()-1,2);
  slip::Range<int> ri(0,M.dim2()-1,2);
  slip::Range<int> rid(1,M.dim2()-1,2);
  std::cout << "range for rows : " << ri << std::endl;
  std::cout << "range for rows + 1 : " << rid << std::endl;
  std::cout << "range for cols : " << rj << std::endl;
  std::cout << "range for cols + 1 : " << rjd << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;

  //boxes
  slip::Box2d<int> b2d(1,0,3,3);
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Box for tests" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout << "box used : " << b2d << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;

  //one dimensionnal iterators
  
  //row : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "row iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  std::copy(M.row_begin(2),M.row_end(2),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(2),M.row_rend(2),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row range :
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "row range iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.row_begin(2,ri),M.row_end(2,ri),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(2,ri),M.row_rend(2,ri),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "row range iterator + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.row_begin(2,rid),M.row_end(2,rid),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(2,rid),M.row_rend(2,rid),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "col iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.col_begin(2),M.col_end(2),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(2),M.col_rend(2),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col range :
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "col range iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.col_begin(2,rj),M.col_end(2,rj),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(2,rj),M.col_rend(2,rj),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "col range iterator + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.col_begin(2,rjd),M.col_end(2,rjd),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(2,rjd),M.col_rend(2,rjd),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //Global iterator 2d : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "global iterator2d" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.upper_left(),M.bottom_right(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rupper_left(),M.rbottom_right(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //box iterator 2d : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "box iterator2d" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.upper_left(b2d),M.bottom_right(b2d),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rupper_left(b2d),M.rbottom_right(b2d),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //range iterator 2d : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "range iterator2d" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.upper_left(rj,ri),M.bottom_right(rj,ri),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rupper_left(rj,ri),M.rbottom_right(rj,ri),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "range iterator2d + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.upper_left(rjd,rid),M.bottom_right(rjd,rid),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rupper_left(rjd,rid),M.rbottom_right(rjd,rid),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  //****************************** Const ***********************************//
  
  
  std::cout << std::setfill('#') << std::setw(60) << '#' << std::endl;
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout << "Const Iterator" <<std::endl;
  std::cout <<  std::setfill('#') << std::setw(60) << '#' << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  slip::GenericMultiComponent2d<T> const Mc(M);
  
  std::for_each(Mc.begin(),Mc.end(),disp<T>);
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "const row iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.row_begin(2),Mc.row_end(2),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_rbegin(2),Mc.row_rend(2),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row range :
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "const row range iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.row_begin(2,ri),Mc.row_end(2,ri),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_rbegin(2,ri),Mc.row_rend(2,ri),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "const row range iterator + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.row_begin(2,rid),Mc.row_end(2,rid),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_rbegin(2,rid),Mc.row_rend(2,rid),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "const col iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.col_begin(2),Mc.col_end(2),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_rbegin(2),Mc.col_rend(2),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col range :
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "const col range iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.col_begin(2,rj),Mc.col_end(2,rj),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_rbegin(2,rj),Mc.col_rend(2,rj),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "const col range iterator + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.col_begin(2,rjd),Mc.col_end(2,rjd),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_rbegin(2,rjd),Mc.col_rend(2,rjd),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
 
  //Global iterator 2d : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "const global iterator2d" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.upper_left(),Mc.bottom_right(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rupper_left(),Mc.rbottom_right(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //box iterator 2d : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "const box iterator2d" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.upper_left(b2d),Mc.bottom_right(b2d),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rupper_left(b2d),Mc.rbottom_right(b2d),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //range iterator 2d : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "const range iterator2d" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.upper_left(rj,ri),Mc.bottom_right(rj,ri),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rupper_left(rj,ri),Mc.rbottom_right(rj,ri),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "const range iterator2d + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.upper_left(rjd,rid),Mc.bottom_right(rjd,rid),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rupper_left(rjd,rid),Mc.rbottom_right(rjd,rid),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //****************************** Component Iterators ***********************************//
  
  
  std::cout << std::setfill('#') << std::setw(60) << '#' << std::endl;
  std::cout << std::setfill('#') << std::setw(60) << '#' << std::endl;
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout << "Component Iterator" <<std::endl;
  std::cout <<  std::setfill('#') << std::setw(60) << '#' << std::endl;
  std::cout <<  std::setfill('#') << std::setw(60) << '#' << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;


  //one dimensionnal iterators
  
  //row : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Component row iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  std::copy(M.row_begin(1,2),M.row_end(1,2),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(1,2),M.row_rend(1,2),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row range :
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Component row range iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.row_begin(1,2,ri),M.row_end(1,2,ri),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(1,2,ri),M.row_rend(1,2,ri),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Component row range iterator + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.row_begin(1,2,rid),M.row_end(1,2,rid),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(1,2,rid),M.row_rend(1,2,rid),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Component col iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.col_begin(1,2),M.col_end(1,2),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(1,2),M.col_rend(1,2),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col range :
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Component col range iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.col_begin(1,2,rj),M.col_end(1,2,rj),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(1,2,rj),M.col_rend(1,2,rj),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Component col range iterator + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.col_begin(1,2,rjd),M.col_end(1,2,rjd),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(1,2,rjd),M.col_rend(1,2,rjd),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //Global iterator 2d : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Component global iterator2d" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.upper_left(1),M.bottom_right(1),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rupper_left(1),M.rbottom_right(1),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //box iterator 2d : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Component box iterator2d" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.upper_left(1,b2d),M.bottom_right(1,b2d),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rupper_left(1,b2d),M.rbottom_right(1,b2d),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //range iterator 2d : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Component range iterator2d" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.upper_left(1,rj,ri),M.bottom_right(1,rj,ri),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rupper_left(1,rj,ri),M.rbottom_right(1,rj,ri),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Component range iterator2d + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(M.upper_left(1,rjd,rid),M.bottom_right(1,rjd,rid),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.rupper_left(1,rjd,rid),M.rbottom_right(1,rjd,rid),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //****************************** Const Component Iterators ***********************************//
  
  
  std::cout << std::setfill('#') << std::setw(60) << '#' << std::endl;
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout << "Const Component Iterator" <<std::endl;
  std::cout <<  std::setfill('#') << std::setw(60) << '#' << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;


  //one dimensionnal iterators
  
  //row : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Const component row iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  std::copy(Mc.row_begin(1,2),Mc.row_end(1,2),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_rbegin(1,2),Mc.row_rend(1,2),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row range :
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Const component row range iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.row_begin(1,2,ri),Mc.row_end(1,2,ri),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_rbegin(1,2,ri),Mc.row_rend(1,2,ri),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Const component row range iterator + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.row_begin(1,2,rid),Mc.row_end(1,2,rid),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_rbegin(1,2,rid),Mc.row_rend(1,2,rid),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Const component col iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.col_begin(1,2),Mc.col_end(1,2),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_rbegin(1,2),Mc.col_rend(1,2),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col range :
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Const component col range iterator" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.col_begin(1,2,rj),Mc.col_end(1,2,rj),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_rbegin(1,2,rj),Mc.col_rend(1,2,rj),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Const component col range iterator + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.col_begin(1,2,rjd),Mc.col_end(1,2,rjd),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_rbegin(1,2,rjd),Mc.col_rend(1,2,rjd),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //Global iterator 2d : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Const component global iterator2d" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.upper_left(1),Mc.bottom_right(1),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rupper_left(1),Mc.rbottom_right(1),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //box iterator 2d : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Const component box iterator2d" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.upper_left(1,b2d),Mc.bottom_right(1,b2d),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rupper_left(1,b2d),Mc.rbottom_right(1,b2d),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //range iterator 2d : 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Const component range iterator2d" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.upper_left(1,rj,ri),Mc.bottom_right(1,rj,ri),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rupper_left(1,rj,ri),Mc.rbottom_right(1,rj,ri),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Const component range iterator2d + 1" <<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::copy(Mc.upper_left(1,rjd,rid),Mc.bottom_right(1,rjd,rid),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.rupper_left(1,rjd,rid),Mc.rbottom_right(1,rjd,rid),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 return 0;
}

