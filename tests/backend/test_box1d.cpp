#include<iostream>
#include<iterator>
#include <iomanip>
#include <fstream>

#include "Box1d.hpp"
#include "Point1d.hpp"
#include "DPoint1d.hpp"
#include "arithmetic_op.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

int main()
{
  slip::Point1d<int> p0;
  std::cout<<p0<<std::endl;

  slip::Point1d<int> p1(1);
  std::cout<<p1<<std::endl;
  
  slip::Point1d<int> p2(2);
  std::cout<<p2<<std::endl;

  slip::Box1d<int> box2(p1,p2);
  std::cout<<box2<<std::endl;


  slip::Box1d<int> box3(3,4);
  std::cout<<box3<<std::endl;
  
  
  slip::Box1d<int> box4 = box3;
  std::cout<<box4<<std::endl;

  box4 = box2;
  std::cout<<box4<<std::endl;
 
  
  std::cout<<box2.contains(slip::Point1d<int>(1))<<std::endl;
  std::cout<<box2.contains(slip::Point1d<int>(-4))<<std::endl;
 
  slip::DPoint1d<int> dp(2);
  
  std::cout<<box4<<" + "<<dp<<" = ";
  box4.translate(dp);
  std::cout<<box4<<std::endl;
  
  std::cout<<box4.is_consistent()<<std::endl;
  slip::Box1d<int> box5(3,-6);
  std::cout<<box5.is_consistent()<<std::endl;
  
  box5.swap(box4);
  std::cout<<box4<<std::endl;
  std::cout<<box5<<std::endl;
  std::cout<<(box5==box4)<<std::endl;
  std::cout<<(box5!=box4)<<std::endl;
  
  
  std::cout<<box5<<std::endl;
  std::cout<<"upper_left   = "<<box5.upper_left()<<std::endl;
  std::cout<<"bottom_right = "<<box5.bottom_right()<<std::endl;
  std::cout<<"name = "<<box5.name()<<std::endl;
  std::cout<<"width = "<<box5.width()<<std::endl;
  std::cout<<"length = "<<box5.length()<<std::endl;
  
  
  slip::Box1d<int> box6(10,3);
  std::cout<<box6<<std::endl;
  std::cout<<"upper_left   = "<<box6.upper_left()<<std::endl;
  std::cout<<"bottom_right = "<<box6.bottom_right()<<std::endl;
  std::cout<<"width = "<<box6.width()<<std::endl;
  std::cout<<"length = "<<box6.length()<<std::endl;
  
  
  slip::Point1d<int> p3(10);
  slip::Box1d<int> box7(p3,3);
  std::cout<<box7<<std::endl;
  std::cout<<"upper_left   = "<<box7.upper_left()<<std::endl;
  std::cout<<"bottom_right = "<<box7.bottom_right()<<std::endl;
  std::cout<<"width = "<<box7.width()<<std::endl;
  std::cout<<"length = "<<box7.length()<<std::endl;
  
  box7.set_coord(1,4);
  std::cout<<box7<<std::endl;
  std::cout<<"upper_left   = "<<box7.upper_left()<<std::endl;
  std::cout<<"bottom_right = "<<box7.bottom_right()<<std::endl;
  std::cout<<"width = "<<box7.width()<<std::endl;
  std::cout<<"length = "<<box7.length()<<std::endl;
  
  //Const iterators
  slip::Box1d<int> const box5c(box5);
  std::cout<<box5c<<std::endl;
  std::cout<<"const upper_left   = "<<box5c.upper_left()<<std::endl;
  std::cout<<"const bottom_right = "<<box5c.bottom_right()<<std::endl;
  
  slip::Box1d<int> const box6c(box6);
  std::cout<<box6<<std::endl;
  std::cout<<"const upper_left   = "<<box6c.upper_left()<<std::endl;
  std::cout<<"const bottom_right = "<<box6c.bottom_right()<<std::endl;
  
  slip::Box1d<int> const box7c(p3,3);
  std::cout<<box7c<<std::endl;
  std::cout<<"const upper_left   = "<<box7c.upper_left()<<std::endl;
  std::cout<<"const bottom_right = "<<box7c.bottom_right()<<std::endl;
  
  ////////////// Tests for is_consistent() methods /////////////////////

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "is-consistent() tests" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  slip::Box1d<int> const a(0,3);
  std::cout<< "the box : " << a << std::endl;
  std::cout<< "is consistent? : " << a.is_consistent() << std::endl;
  slip::Box1d<int> const b(3,0);
  std::cout<< "the box : " << b << std::endl;
  std::cout<< "is consistent? : " << b.is_consistent() << std::endl;


  // create and open a character archive for output
    std::ofstream ofs("box1d.txt");
    slip::Point1d<double> p1s(1.3);
    slip::Point1d<double> p2s(4.5);
    
    slip::Box1d<double> bs(p1s,p2s);
        
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<bs;
    }//to close archive
    std::ifstream ifs("box1d.txt");
    slip::Box1d<double> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("box1d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<bs;
    }//to close archive

    std::ifstream ifsb("box1d.bin");
    slip::Box1d<double> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_bb\n"<<new_bb<<std::endl;
  return 0;
}
