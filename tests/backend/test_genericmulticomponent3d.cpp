#include "Matrix3d.hpp"
#include "GenericMultiComponent3d.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <complex>
#include <algorithm>
#include <numeric>
#include <vector>
#include "dynamic.hpp"
#include "Color.hpp"


#include "arithmetic_op.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

int main()
{

  //------------------------------
  //constructors
  //------------------------------
  slip::GenericMultiComponent3d<slip::Color<double> > M;
   std::cout<<"dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" dim3 = "<<M.dim3()<<" size = "<<M.size()<<" slice_size = "<<M.slice_size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 
  
 
  slip::GenericMultiComponent3d<slip::Color<double> > M2(4,3,2);
    std::cout<<"dim1 = "<<M2.dim1()<<" dim2 = "<<M2.dim2()<<" dim3 = "<<M2.dim3()<<" size = "<<M2.size()<<" slice_size = "<<M2.slice_size()<<" max_size = "<<M2.max_size()<<" empty = "<<M2.empty()<<std::endl;
  std::cout<<"slices = "<<M2.slices()<<" rows = "<<M2.rows()<<" cols = "<<M2.cols()<<" columns = "<<M2.columns()<<std::endl;
  std::cout<<M2<<std::endl; 
 
  slip::GenericMultiComponent3d<slip::Color<double> > M3(4,3,2,slip::Color<double>(1.0,2.0,3.0));
  std::cout<<"dim1 = "<<M3.dim1()<<" dim2 = "<<M3.dim2()<<" dim3 = "<<M3.dim3()<<" size = "<<M3.size()<<" slice_size = "<<M3.slice_size()<<" max_size = "<<M3.max_size()<<" empty = "<<M3.empty()<<std::endl; 
  std::cout<<M3<<std::endl; 
 

 double d[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,
	       14.0,15.0,16.0,17.0,18.0,19.0,20.0,21.0,22.0,23.0,24.0,25.0,26.0,27.0,28.0,29.0,30.0,31.0,32.0,33.0,34.0,35.0,36.0};
 slip::GenericMultiComponent3d<slip::Color<double> > M4(2,2,3,d);
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" dim3 = "<<M4.dim3()<<" size = "<<M4.size()<<" slice_size = "<<M4.slice_size()<<" max_size = "<<M4.max_size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl; 

  std::vector<slip::Color<int> > V(24);
  for(std::size_t i = 0; i < 24; ++i)
    V[i] = slip::Color<int>(i,i,i);
  slip::GenericMultiComponent3d<slip::Color<int> > M5(2,3,4,V.begin(),V.end());
  std::cout<<"dim1 = "<<M5.dim1()<<" dim2 = "<<M5.dim2()<<" dim3 = "<<M5.dim3()<<" size = "<<M5.size()<<" slice_size = "<<M5.slice_size()<<" max_size = "<<M5.max_size()<<" empty = "<<M5.empty()<<std::endl; 
  std::cout<<M5<<std::endl;

 slip::Matrix3d<double> Mat1(4,3,2,12.0);
 slip::Matrix3d<double> Mat2(4,3,2,13.0);
 slip::Matrix3d<double> Mat3(4,3,2,14.0);

 std::vector<slip::Matrix<double>::iterator> iterators_list(3);
 iterators_list[0] = Mat1.begin();
 iterators_list[1] = Mat2.begin();
 iterators_list[2] = Mat3.begin();
 
 
slip::GenericMultiComponent3d<slip::Color<double> > Mcol(4,3,2,iterators_list,Mat1.end());
std::cout<<Mcol<<std::endl; 


 slip::GenericMultiComponent3d<slip::Color<double> > M6 = M4;
 std::cout<<"dim1 = "<<M6.dim1()<<" dim2 = "<<M6.dim2()<<" dim3 = "<<M6.dim3()<<" size = "<<M6.size()<<" slice_size = "<<M6.slice_size()<<" max_size = "<<M6.max_size()<<" empty = "<<M6.empty()<<std::endl; 
  std::cout<<M6<<std::endl; 
  //------------------------------

 //------------------------------
 //resize
 //------------------------------
  M.resize(2,3,4,5.6);
   std::cout<<"dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" dim3 = "<<M.dim3()<<" size = "<<M.size()<<" slice_size = "<<M.slice_size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 
  //------------------------------


  //------------------------------
  //Affectation
  //------------------------------
  slip::GenericMultiComponent3d<slip::Color<double> > M7;
  M7 = M;
   std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" dim3 = "<<M7.dim3()<<" size = "<<M7.size()<<" slice_size = "<<M7.slice_size()<<" max_size = "<<M7.max_size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl; 
  M7 = M7;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" dim3 = "<<M7.dim3()<<" size = "<<M7.size()<<" slice_size = "<<M7.slice_size()<<" max_size = "<<M7.max_size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl; 
  
  M4 = 12.2;
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" dim3 = "<<M4.dim3()<<" size = "<<M4.size()<<" slice_size = "<<M4.slice_size()<<" max_size = "<<M4.max_size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl; 

  M4 = slip::Color<double> (128.0,255.0,127.0);
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" dim3 = "<<M4.dim3()<<" size = "<<M4.size()<<" slice_size = "<<M4.slice_size()<<" max_size = "<<M4.max_size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl; 
  //------------------------------

  
  //------------------------------
  //Element access operators
  //------------------------------
   M2(3,2,1) = 2.1;
  M2[1][2][0] = 1.2;
  
  for(size_t k = 0; k < M2.dim1(); ++k)
    {
      for(size_t i = 0; i < M2.dim2(); ++i)
	{
	  for(size_t j = 0; j < M2.dim3(); ++j)
	    {
	      std::cout<<M2[k][i][j]<<" ";
	    }
	  std::cout<<std::endl;
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
  

   for(size_t k = 0; k < M2.dim1(); ++k)
    {
      for(size_t i = 0; i < M2.dim2(); ++i)
	{
	  for(size_t j = 0; j < M2.dim3(); ++j)
	    {
	      std::cout<<M2(k,i,j)<<" ";
	    }
	  std::cout<<std::endl;
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
 
  //------------------------------

   //------------------------------
   //swap
   //------------------------------
   M2.swap(M3);
   std::cout<<M2<<std::endl;
   std::cout<<std::endl;
   std::cout<<M3<<std::endl;
   //------------------------------


   //----------------------------
   //iterators
   //----------------------------
   slip::GenericMultiComponent3d<slip::Color<double> > M8(4,3,2);
   std::vector<slip::Color<double> > V8(24);
   for(std::size_t i = 0; i < 24; ++i)
     V8[i] = slip::Color<double>(i,i,i);
    
   M8.fill(V8.begin(),V8.end());   
   std::cout<<"M8 = \n"<<M8<<std::endl;

   std::copy(M8.begin(),M8.end(),std::ostream_iterator<slip::Color<double> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M8.rbegin(),M8.rend(),std::ostream_iterator<slip::Color<double> >(std::cout," "));
   std::cout<<std::endl<<std::endl;
   
   slip::Matrix3d<double> mat(4,3,2,7.7);
   M8.fill(1,mat.begin(),mat.end());
// //    // std::transform(M3.rbegin(),M3.rend(),M8.begin(),M2.begin(),std::plus<double>());
  
// //    std::cout<<M2<<std::endl;
   

 //   std::copy(M8.row_begin(1),M8.row_end(1),std::ostream_iterator<slip::Color<double> >(std::cout," "));
//    std::cout<<std::endl;
//    std::cout<<std::endl;

//    std::copy(M8.col_begin(1),M8.col_end(1),std::ostream_iterator<slip::Color<double> >(std::cout," "));
//    std::cout<<std::endl;
//    std::cout<<std::endl;
   
//    //std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_begin(2),std::plus<double>());
//    //std::cout<<M2<<std::endl;

//    //std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_begin(2),std::plus<double>());
//    //std::cout<<M2<<std::endl;


 //   std::copy(M8.row_rbegin(1),M8.row_rend(1),std::ostream_iterator<slip::Color<double> >(std::cout," "));
//    std::cout<<std::endl;
//    std::copy(M8.col_rbegin(1),M8.col_rend(1),std::ostream_iterator<slip::Color<double> >(std::cout," "));
//    std::cout<<std::endl;
   
   
//    //std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_rbegin(2),std::plus<double>());
//    //std::cout<<M2<<std::endl;

//    //std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_rbegin(2),std::plus<double>());
//    //std::cout<<M2<<std::endl;
   
   
     slip::GenericMultiComponent3d<slip::Color<int> > M100(2,10,12);
      for(std::size_t k = 0; k < M100.dim1(); ++k)
       {
	 for(std::size_t i = 0; i < M100.dim2(); ++i)
	   {
	     for(std::size_t j = 0; j < M100.dim3(); ++j)
	       {
		 M100[k][i][j][0] = k +i + j;
		 M100[k][i][j][1] = k + i + j + 1;
		 M100[k][i][j][2] = k + i + j + 2;
	       
	       }
	   }
       }
     std::cout<<M100<<std::endl;
 
  

     //first plane
     std::cout<<"first plane "<<std::endl;
     std::copy(M100.begin(0),M100.end(0),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //second plane
     std::cout<<"second plane "<<std::endl;
     std::copy(M100.begin(1),M100.end(1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //third plane
     std::cout<<"third plane "<<std::endl;
     std::copy(M100.begin(2),M100.end(2),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     

    //  std::cout<<"first plane, row 1 "<<std::endl;
//      std::copy(M100.row_begin(0,1),M100.row_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
//      std::cout<<"first plane, reverse row 1 "<<std::endl;
//      std::copy(M100.row_rbegin(0,1),M100.row_rend(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;

   //    std::cout<<"first plane, col 1 "<<std::endl;
//       std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
     
//      std::cout<<"first plane, reverse col 1 "<<std::endl;
//      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));

//   std::cout<<"second plane, row 1 "<<std::endl;
//      std::copy(M100.row_begin(1,1),M100.row_end(1,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
//      std::cout<<"second plane, reverse row 1 "<<std::endl;
//      std::copy(M100.row_rbegin(1,1),M100.row_rend(1,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;

    //  std::cout<<"first plane, col 1 "<<std::endl;
//      std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
     
//      std::cout<<"first plane, reverse col 1 "<<std::endl;
//      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));


  //      std::cout<<"third plane, row 1 "<<std::endl;
//      std::copy(M100.row_begin(2,1),M100.row_end(2,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
//      std::cout<<"third plane, reverse row 1 "<<std::endl;
//      std::copy(M100.row_rbegin(2,1),M100.row_rend(2,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;

    //  std::cout<<"first plane, col 1 "<<std::endl;
//      std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
     
//      std::cout<<"first plane, reverse col 1 "<<std::endl;
//      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));

     

//   //----------------------------  

   //------------------------------
  //Comparison operators
  //------------------------------
  std::cout<<" M3 == M2 "<<(M3 == M2)<<std::endl;
  std::cout<<" M3 != M2 "<<(M3 != M2)<<std::endl;
//   std::cout<<" M3 > M2 "<<(M3 > M2)<<std::endl;
//   std::cout<<" M3 < M2 "<<(M3 < M2)<<std::endl;
//   std::cout<<" M3 >= M2 "<<(M3 >= M2)<<std::endl;
//   std::cout<<" M3 <= M2 "<<(M3 <= M2)<<std::endl;
  //------------------------------


  //------------------------------
  //fill methods
  //------------------------------
  
  M3.fill(4.0);
  std::cout<<M3<<std::endl;
  M3.fill(slip::Color<double>(2.0,3.0,6.0));
  std::cout<<M3<<std::endl;
  double d2[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,
	       14.0,15.0,16.0,17.0,18.0,19.0,20.0,21.0,22.0,23.0,24.0,25.0,26.0,27.0,28.0,29.0,30.0,31.0,32.0,33.0,34.0,35.0,36.0};
  M3.fill(d2);
  std::cout<<M3<<std::endl;
  std::cout<<std::endl;
  std::vector<slip::Color<double> > V2(12);
  for(std::size_t i = 0; i < 12; ++i)
    V2[i] = slip::Color<double>(i,i,i);
    
  M3.fill(V2.begin(),V2.end());
  std::cout<<M3<<std::endl;
  //------------------------------
  
   
  std::cout<<" mins(M100) = "<<M100.min()<<std::endl;
  std::cout<<" maxs(M100) = "<<M100.max()<<std::endl;

    
  M100.resize(4,2,1,slip::Color<int>(2,3,6));
  std::cout<<M100<<std::endl;

  //serialization
   // create and open a character archive for output
    std::ofstream ofs("generic3d.txt");
    slip::GenericMultiComponent3d<slip::Color<float> > b(3,3,4);
    slip::iota(b.begin(),b.end(),slip::Color<float>(1.0f,2.0f,3.0f),
	       slip::Color<float>(1.0f,1.0f,1.0f));
    
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<b;
    }//to close archive
    std::ifstream ifs("generic3d.txt");
    slip::GenericMultiComponent3d<slip::Color<float> > new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("generic3d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<b;
    }//to close archive

    std::ifstream ifsb("generic3d.bin");
    slip::GenericMultiComponent3d<slip::Color<float> > new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;
     return 0;
}
