#include<algorithm>
#include<iostream>
#include<iterator>
#include <numeric>
#include <complex>

//#define ALL_PLANE_ITERATOR3D


#include "Array3d.hpp"
#include "Point3d.hpp"
#include "DPoint3d.hpp"
#include "DPoint2d.hpp"
#include "Box2d.hpp"
#include "Box3d.hpp"
#include "iterator3d_box.hpp"




template<typename T>
void daccum(T& a){static int init = 0.0; a = init++;}
template<typename T>
void disp(T const& a){std::cout << a <<" ";}

int main()
{
  
  //Array3d
  slip::Array3d<double> M(5,4,3);
  //ranges
  slip::Range<int> rk(0,M.dim1()-1,2);
  slip::Range<int> rkd(1,M.dim1()-1,2);
  slip::Range<int> ri(0,M.dim3()-1,2);
  slip::Range<int> rid(1,M.dim3()-1,2);
  slip::Range<int> rj(0,M.dim2()-1,2);
  slip::Range<int> rjd(1,M.dim2()-1,2);
  //boxes
  slip::Box2d<int> b2d(1,0,3,3);
  slip::Box3d<int> b3d(1,0,1,3,2,2);
  
  //Array3d<T>::begin() and Array3d<T>::end()
  std::for_each(M.begin(),M.end(),daccum<double>);
  slip::Array3d<double> Mr(5,4,3);
  //  std::copy(M.rbegin(),M.end()
  std::cout<<M<<std::endl;
  
  //one dimensionnal iterators
  
  //slice : 
  std::cout<< "slice iterator" <<std::endl;
  std::copy(M.slice_begin(2,2),M.slice_end(2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_rbegin(2,2),M.slice_rend(2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //slice range :
  std::cout<< "slice range iterator" <<std::endl;
  std::copy(M.slice_begin(2,2,rk),M.slice_end(2,2,rk),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_rbegin(2,2,rk),M.slice_rend(2,2,rk),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout<< "slice range iterator + 1" <<std::endl;
  std::copy(M.slice_begin(2,2,rkd),M.slice_end(2,2,rkd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_rbegin(2,2,rkd),M.slice_rend(2,2,rkd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row : 
  std::cout<< "row iterator" <<std::endl;
  std::copy(M.row_begin(2,2),M.row_end(2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(2,2),M.row_rend(2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row range :
  std::cout<< "row range iterator" <<std::endl;
  std::copy(M.row_begin(2,2,ri),M.row_end(2,2,ri),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(2,2,ri),M.row_rend(2,2,ri),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  std::cout<< "row range iterator + 1" <<std::endl;
  std::copy(M.row_begin(2,2,rid),M.row_end(2,2,rid),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(2,2,rid),M.row_rend(2,2,rid),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col : 
  std::cout<< "col iterator" <<std::endl;
  std::copy(M.col_begin(2,2),M.col_end(2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(2,2),M.col_rend(2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col range :
  std::cout<< "col range iterator" <<std::endl;
  std::copy(M.col_begin(2,2,rj),M.col_end(2,2,rj),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(2,2,rj),M.col_rend(2,2,rj),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout<< "col range iterator + 1" <<std::endl;
  std::copy(M.col_begin(2,2,rjd),M.col_end(2,2,rjd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(2,2,rjd),M.col_rend(2,2,rjd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //plane iterators

  //global plane 1d
  std::cout<< "global plane iterator" <<std::endl;
  std::copy(M.plane_begin(2),M.plane_end(2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_rbegin(2),M.plane_rend(2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;


#ifdef ALL_PLANE_ITERATOR3D
  //plane_row
  std::cout<< "plane row iterator" <<std::endl;
  std::copy(M.plane_row_begin(slip::KI_PLANE,2,2),M.plane_row_end(slip::KI_PLANE,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_row_rbegin(slip::KI_PLANE,2,2),M.plane_row_rend(slip::KI_PLANE,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //plane_col
  std::cout<< "plane col iterator" <<std::endl;
  std::copy(M.plane_col_begin(slip::KI_PLANE,2,0),M.plane_col_end(slip::KI_PLANE,2,0),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_col_rbegin(slip::KI_PLANE,2,0),M.plane_col_rend(slip::KI_PLANE,2,0),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //plane box row
  std::cout<< "plane box row iterator" <<std::endl;
  std::copy(M.plane_row_begin(slip::KI_PLANE,2,2,b2d),M.plane_row_end(slip::KI_PLANE,2,2,b2d),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_row_rbegin(slip::KI_PLANE,2,2,b2d),M.plane_row_rend(slip::KI_PLANE,2,2,b2d),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //plane_col
  std::cout<< "plane box col iterator" <<std::endl;
  std::copy(M.plane_col_begin(slip::KI_PLANE,2,0,b2d),M.plane_col_end(slip::KI_PLANE,2,0,b2d),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_col_rbegin(slip::KI_PLANE,2,0,b2d),M.plane_col_rend(slip::KI_PLANE,2,0,b2d),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
#endif

 //global plane 2d
  std::cout<< "global plane 2d iterator" <<std::endl;
  std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

   //box plane 2d
   std::cout<< "box plane 2d iterator" <<std::endl;
   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b2d),M.plane_bottom_right(slip::KI_PLANE,2,b2d),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;
   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b2d),M.plane_rbottom_right(slip::KI_PLANE,2,b2d),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;

   //global 3d iterator
   std::cout<< "Global 3d iterator" <<std::endl;
   slip::Array3d<double> C(M.dim1(),M.dim2(),M.dim3());
   std::copy(M.front_upper_left(),M.back_bottom_right(),C.front_upper_left());
   std::cout<< C;
   std::cout<<std::endl;
   std::cout<<std::endl;
   slip::Array3d<double> CBack(M.dim1(),M.dim2(),M.dim3());
   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),CBack.front_upper_left());
   std::cout<< CBack;
   std::cout<<std::endl;
   std::cout<<std::endl;

   //3d box iterator
   std::cout<< "3d box iterator" <<std::endl;
   slip::Array3d<double> Cbox(b3d.depth(),b3d.height(),b3d.width());
   std::copy(M.front_upper_left(b3d),M.back_bottom_right(b3d),Cbox.front_upper_left());
   std::cout<< Cbox;
   std::cout<<std::endl;
   std::cout<<std::endl;
   slip::Array3d<double> CboxBack(b3d.depth(),b3d.height(),b3d.width());
   std::copy(M.rfront_upper_left(b3d),M.rback_bottom_right(b3d),CboxBack.front_upper_left());
   std::cout<< CboxBack;
   std::cout<<std::endl;
   std::cout<<std::endl;

   //****************************** Const ***********************************//


  std::cout << "######################## CONST ####################"<< std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  slip::Array3d<double> const Mc(M);
  
  //Array3d<T>::begin() and Array3d<T>::end()
  std::for_each(Mc.begin(),Mc.end(),disp<double>);
  std::cout<<std::endl;
  std::cout<<std::endl;

  //slice : 
  std::cout<< "Const slice iterator" <<std::endl;
  std::copy(Mc.slice_begin(2,2),Mc.slice_end(2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.slice_rbegin(2,2),Mc.slice_rend(2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //slice range :
  std::cout<< "Const slice range iterator" <<std::endl;
  std::copy(Mc.slice_begin(2,2,rk),Mc.slice_end(2,2,rk),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.slice_rbegin(2,2,rk),Mc.slice_rend(2,2,rk),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout<< "const slice range iterator + 1" <<std::endl;
  std::copy(Mc.slice_begin(2,2,rkd),Mc.slice_end(2,2,rkd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.slice_rbegin(2,2,rkd),Mc.slice_rend(2,2,rkd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //row : 
  std::cout<< "const row iterator" <<std::endl;
  std::copy(Mc.row_begin(2,2),Mc.row_end(2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_rbegin(2,2),Mc.row_rend(2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row range :
  std::cout<< "const row range iterator" <<std::endl;
  std::copy(Mc.row_begin(2,2,ri),Mc.row_end(2,2,ri),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_rbegin(2,2,ri),Mc.row_rend(2,2,ri),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout<< "const row range iterator + 1" <<std::endl;
  std::copy(Mc.row_begin(2,2,rid),Mc.row_end(2,2,rid),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_rbegin(2,2,rid),Mc.row_rend(2,2,rid),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //col : 
  std::cout<< "const col iterator" <<std::endl;
  std::copy(Mc.col_begin(2,2),Mc.col_end(2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_rbegin(2,2),Mc.col_rend(2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col range :
  std::cout<< "const col range iterator" <<std::endl;
  std::copy(Mc.col_begin(2,2,rj),Mc.col_end(2,2,rj),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_rbegin(2,2,rj),Mc.col_rend(2,2,rj),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout<< "const col range iterator + 1" <<std::endl;
  std::copy(Mc.col_begin(2,2,rjd),Mc.col_end(2,2,rjd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_rbegin(2,2,rjd),Mc.col_rend(2,2,rjd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //global plane 1d
  std::cout<< "global const plane iterator" <<std::endl;
  std::copy(Mc.plane_begin(2),Mc.plane_end(2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_rbegin(2),Mc.plane_rend(2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

#ifdef ALL_PLANE_ITERATOR3D
  //plane_row
  std::cout<< "const plane row iterator" <<std::endl;
  std::copy(Mc.plane_row_begin(slip::KI_PLANE,2,2),Mc.plane_row_end(slip::KI_PLANE,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_row_rbegin(slip::KI_PLANE,2,2),Mc.plane_row_rend(slip::KI_PLANE,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //plane_col
  std::cout<< "const plane col iterator" <<std::endl;
  std::copy(Mc.plane_col_begin(slip::KI_PLANE,2,0),Mc.plane_col_end(slip::KI_PLANE,2,0),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_col_rbegin(slip::KI_PLANE,2,0),Mc.plane_col_rend(slip::KI_PLANE,2,0),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //plane box row
  std::cout<< "plane box row iterator" <<std::endl;
  std::copy(Mc.plane_row_begin(slip::KI_PLANE,2,2,b2d),Mc.plane_row_end(slip::KI_PLANE,2,2,b2d),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_row_rbegin(slip::KI_PLANE,2,2,b2d),Mc.plane_row_rend(slip::KI_PLANE,2,2,b2d),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //plane_col
  std::cout<< "plane box col iterator" <<std::endl;
  std::copy(Mc.plane_col_begin(slip::KI_PLANE,2,0,b2d),Mc.plane_col_end(slip::KI_PLANE,2,0,b2d),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_col_rbegin(slip::KI_PLANE,2,0,b2d),Mc.plane_col_rend(slip::KI_PLANE,2,0,b2d),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
#endif

 //global plane 2d
  std::cout<< "global const plane 2d iterator" <<std::endl;
  std::copy(Mc.plane_upper_left(slip::KI_PLANE,2),Mc.plane_bottom_right(slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_rupper_left(slip::KI_PLANE,2),Mc.plane_rbottom_right(slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //box plane 2d
  std::cout<< "box const plane 2d iterator" <<std::endl;
  std::copy(Mc.plane_upper_left(slip::KI_PLANE,2,b2d),Mc.plane_bottom_right(slip::KI_PLANE,2,b2d),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_rupper_left(slip::KI_PLANE,2,b2d),Mc.plane_rbottom_right(slip::KI_PLANE,2,b2d),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //global 3d iterator
  std::cout<< "Global const 3d iterator" <<std::endl;
  slip::Array3d<double> Cc(Mc.dim1(),Mc.dim2(),Mc.dim3());
  std::copy(Mc.front_upper_left(),Mc.back_bottom_right(),Cc.front_upper_left());
  std::cout<< Cc;
  std::cout<<std::endl;
  std::cout<<std::endl;
  slip::Array3d<double> CBackc(Mc.dim1(),Mc.dim2(),Mc.dim3());
  std::copy(Mc.rfront_upper_left(),Mc.rback_bottom_right(),CBackc.front_upper_left());
  std::cout<< CBackc;
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  //3d box iterator
  std::cout<< "3d const box iterator" <<std::endl;
  slip::Array3d<double> Cboxc(b3d.depth(),b3d.height(),b3d.width());
  std::copy(Mc.front_upper_left(b3d),Mc.back_bottom_right(b3d),Cboxc.front_upper_left());
  std::cout<< Cboxc;
  std::cout<<std::endl;
  std::cout<<std::endl;
  slip::Array3d<double> CboxBackc(b3d.depth(),b3d.height(),b3d.width());
  std::copy(Mc.rfront_upper_left(b3d),Mc.rback_bottom_right(b3d),CboxBackc.front_upper_left());
  std::cout<< CboxBackc;
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  return 0;
}

