#include<iostream>
#include<iterator>
#include <iomanip>
#include <fstream>
#include "Box3d.hpp"
#include "arithmetic_op.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
int main()
{

   slip::Box3d<int> box;

   std::cout<<box<<std::endl;
   std::cout<<"name  = "<<box.name()<<std::endl;
   
  slip::Point3d<int> p1(1,1,1);
  std::cout<<p1<<std::endl;
  
  slip::Point3d<int> p2(2,3,4);
  std::cout<<p2<<std::endl;

  slip::Box3d<int> box2(p1,p2);
  std::cout<<box2<<std::endl;

/*  box2.back_bottom_right(p1);
  box2.front_upper_left(p2);
  std::cout<<box2<<std::endl;
*/
  slip::Box3d<int> box3(3,4,5,6,7,8);
  std::cout<<box3<<std::endl;
  
  
  slip::Box3d<int> box4 = box3;
  std::cout<<box4<<std::endl;

  box4 = box2;
  std::cout<<box4<<std::endl;
 
  
  std::cout<<box2.contains(slip::Point3d<int>(1,2,3))<<std::endl;
  std::cout<<box2.contains(slip::Point3d<int>(12,-4,-2))<<std::endl;
 
  slip::DPoint3d<int> dp(1,-1,-1);

 std::cout<<box4<<" + "<<dp<<" = ";
 box4.translate(dp);
 std::cout<<box4<<std::endl;

 std::cout<<box4.is_consistent()<<std::endl;
 slip::Box3d<int> box5(2,3,4,-5,-6,-7);
 std::cout<<box5.is_consistent()<<std::endl;

 box5.swap(box4);
 std::cout<<box4<<std::endl;
 std::cout<<box5<<std::endl;
 std::cout<<(box5==box4)<<std::endl;
 std::cout<<(box5!=box4)<<std::endl;


 std::cout<<box5<<std::endl;
 std::cout<<"front_upper_left   = "<<box5.front_upper_left()<<std::endl;
 std::cout<<"back_bottom_right = "<<box5.back_bottom_right()<<std::endl;
 std::cout<<"width = "<<box5.width()<<std::endl;
 std::cout<<"height = "<<box5.height()<<std::endl;
 std::cout<<"depth = "<<box5.depth()<<std::endl;
 std::cout<<"volume = "<<box5.volume()<<std::endl;
 

 slip::Box3d<int> box6(10,10,10,3);
 std::cout<<"front_upper_left   = "<<box6.front_upper_left()<<std::endl;
 std::cout<<"back_bottom_right = "<<box6.back_bottom_right()<<std::endl;
 std::cout<<"width = "<<box6.width()<<std::endl;
 std::cout<<"height = "<<box6.height()<<std::endl;
 std::cout<<"depth = "<<box6.depth()<<std::endl;
 std::cout<<"volume = "<<box6.volume()<<std::endl;
 
 slip::Point3d<int> p3(10,10,10);
 slip::Box3d<int> box7(p3,3);
 std::cout<<"front_upper_left   = "<<box7.front_upper_left()<<std::endl;
 std::cout<<"back_bottom_right = "<<box7.back_bottom_right()<<std::endl;
 std::cout<<"width = "<<box7.width()<<std::endl;
 std::cout<<"height = "<<box7.height()<<std::endl;
 std::cout<<"depth = "<<box7.depth()<<std::endl;

 std::cout<<"volume = "<<box7.volume()<<std::endl;

 box7.set_coord(1,2,3,4,5,6);
 std::cout<<box7<<std::endl;
 
 //Const iterators
 slip::Box3d<int> const box5c(box5);
 std::cout<<box5c<<std::endl;
 std::cout<<"const upper_left   = "<<box5c.front_upper_left()<<std::endl;
 std::cout<<"const bottom_right = "<<box5c.back_bottom_right()<<std::endl;

 slip::Box3d<int> const box6c(box6);
 std::cout<<box6<<std::endl;
 std::cout<<"const upper_left   = "<<box6c.front_upper_left()<<std::endl;
 std::cout<<"const bottom_right = "<<box6c.back_bottom_right()<<std::endl;

 slip::Box3d<int> const box7c(p3,3);
 std::cout<<box7c<<std::endl;
 std::cout<<"const upper_left   = "<<box7c.front_upper_left()<<std::endl;
 std::cout<<"const bottom_right = "<<box7c.back_bottom_right()<<std::endl;



 ////////////// Tests for is_consistent() methods /////////////////////

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "is-consistent() tests" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  slip::Box3d<int> const a(0,0,0,3,3,3);
  std::cout<< "the box : " << a << std::endl;
  std::cout<< "is consistent? : " << a.is_consistent() << std::endl;
  slip::Box3d<int> const b(3,3,3,0,0,0);
  std::cout<< "the box : " << b << std::endl;
  std::cout<< "is consistent? : " << b.is_consistent() << std::endl;


  // create and open a character archive for output
    std::ofstream ofs("box3d.txt");
    slip::Point3d<double> p1s(1.3,2.4,3.5);
    slip::Point3d<double> p2s(2.3,4.5,6.7);
    
    slip::Box3d<double> bs(p1s,p2s);
        
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<bs;
    }//to close archive
    std::ifstream ifs("box3d.txt");
    slip::Box3d<double> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("box3d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<bs;
    }//to close archive

    std::ifstream ifsb("box3d.bin");
    slip::Box3d<double> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_bb\n"<<new_bb<<std::endl;
  

return 0;
}
