#include<iostream>
#include<iterator>
#include <fstream>
#include "DPoint2d.hpp"
#include "Array2d.hpp"
#include "Box2d.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


int main()
{

  slip::DPoint2d<int> p;
  std::cout<<p<<std::endl;
  std::cout<<"name = "<<p.name()<<std::endl;

  slip::DPoint2d<float> pf(2.3,4.5);
  std::cout<<pf<<std::endl;

  pf.dx1(2.4);
  pf.dx2(4.6);
  std::cout<<pf<<std::endl;
 
  slip::DPoint2d<float> pf2 = pf;
  std::cout<<pf2<<std::endl;
  
  slip::DPoint2d<float> pf3;
  pf3 = pf;
  std::cout<<pf3<<std::endl;
    
  std::cout<<pf3[0]<<" "<<pf3[1]<<std::endl;
  std::cout<<pf3.dx1()<<" "<<pf3.dx2()<<std::endl;
  
  std::cout<<"dim = "<<pf3.dim()<<std::endl;
  std::cout<<"size = "<<pf3.size()<<std::endl;
  std::cout<<"empty = "<<pf3.empty()<<std::endl;

  slip::DPoint2d<float> pf4(3.0,4.0);
  pf4.swap(pf3);
  std::cout<<pf3<<std::endl;
  std::cout<<pf4<<std::endl;
 
  pf4[0] = 6.0;
  pf4[1] = 7.0;
  std::cout<<pf4<<std::endl;
 
  pf4.dx1() = 8.0;
  pf4.dx2() = 9.0;
  std::cout<<pf4<<std::endl;
 
  pf4.set_coord(8.8,9.9);	
  std::cout<<pf4<<std::endl;
  // pf4[3] = 6.0;
  std::cout<<-pf4<<std::endl;
  
  std::cout<<"pf3 == pf4 :"<<(pf3==pf4)<<std::endl;
  std::cout<<"pf3 != pf4 :"<<(pf3!=pf4)<<std::endl;

  float tab[] = {1.0,2.0};
  slip::DPoint2d<float> pf5(tab);
  std::cout<<pf5<<std::endl;
  
  slip::DPoint2d<std::size_t> pf6(4,5);
  std::cout<<-pf6<<std::endl;

  std::cout<<(pf3 + pf4)<<std::endl;
  std::cout<<(pf3 - pf4)<<std::endl;
  std::cout<<(pf3 += pf4)<<std::endl;
  std::cout<<(pf3 -= pf4)<<std::endl;

  //---classical 2d displacements
   slip::Array2d<float> Mf(5,6);
  float valf = 30.0;
  for(std::size_t i = 0; i < Mf.dim1(); ++i)
    {
      for(std::size_t j = 0; j < Mf.dim2(); ++j)
	{
	  Mf(i,j) = valf;
	  valf -= 1.0;
	}
    }
  std::cout<<Mf<<std::endl;
  
  slip::Box2d<int> boxf(1,1,2,3);
  slip::Array2d<float>::iterator2d itf = Mf.upper_left(boxf);
/*
  std::cout<<"max 4c neighborhood : "<<std::endl;
  for(std::size_t i = 0; i < slip::n_4c.size(); ++i)
    {
      std::cout<<slip::n_4c[i]<<" = "<<itf[slip::n_4c[i]]<<std::endl;
    }
  
  std::cout<<"max 8c neighborhood : "<<std::endl;
  for(std::size_t i = 0; i < slip::n_8c.size(); ++i)
    {
      std::cout<<slip::n_8c[i]<<" = "<<itf[slip::n_8c[i]]<<std::endl;
    }
  
   std::cout<<"previous 4c : "<<std::endl;
   for(std::size_t i = 0; i < slip::prev_4c.size(); ++i)
    {
      std::cout<<slip::prev_4c[i]<<" = "<<itf[slip::prev_4c[i]]<<std::endl;
    }
    
   std::cout<<"slip::next 4c : "<<std::endl;
   for(std::size_t i = 0; i < slip::next_4c.size(); ++i)
    {
      std::cout<<slip::next_4c[i]<<" = "<<itf[slip::next_4c[i]]<<std::endl;
    }

     std::cout<<"previous 8c : "<<std::endl;
   for(std::size_t i = 0; i < slip::prev_8c.size(); ++i)
    {
      std::cout<<slip::prev_8c[i]<<" = "<<itf[slip::prev_8c[i]]<<std::endl;
    }
    
   std::cout<<"slip::next 8c : "<<std::endl;
   for(std::size_t i = 0; i < slip::next_8c.size(); ++i)
    {
      std::cout<<slip::next_8c[i]<<" = "<<itf[slip::next_8c[i]]<<std::endl;
    }
*/   

   //
   //serialization
   //

    // create and open a character archive for output
    std::ofstream ofs("dpoint2d.txt");
    slip::DPoint2d<double> ps(1.3,2.4);
        
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<ps;
    }//to close archive
    std::ifstream ifs("dpoint2d.txt");
    slip::DPoint2d<double> new_ps;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_ps;
    }//to close archive
    std::cout<<"new_ps\n"<<new_ps<<std::endl;

    std::ofstream ofsb("dpoint2d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<ps;
    }//to close archive

    std::ifstream ifsb("dpoint2d.bin");
    slip::DPoint2d<double> new_psb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_psb;
    }//to close archive
    std::cout<<"new_psb\n"<<new_psb<<std::endl;

return 0;
}
