#include <iostream>
#include <iomanip>
#include <vector>
#include <complex>
#include <random>
#include "OLearyPolyBase3d.hpp"
#include "Array3d.hpp"
#include "Vector.hpp"
#include "Polynomial.hpp"
#include "polynomial_algo.hpp"
#include "arithmetic_op.hpp"
#include "Vector3d.hpp"
#include "discrete_polynomial_algos.hpp"



int main()
{
  typedef double T;
  
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "default base construction " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::OLearyPolyBase3d<T> base1;
  std::cout<<"base1 = \n"<<base1<<std::endl;

    
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "base construction and generation" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
   std::size_t x_range_size = 6;
  //std::size_t x_range_size = 8;
  std::size_t y_range_size = 6;
  std::size_t z_range_size = 4;


 
  //slip::OLearyPolyBase3d<T> base2(x_range_size,
  //				      y_range_size,
  //				       z_range_size);
  //slip::OLearyPolyBase3d<T> base2(x_range_size,y_range_size,
  //				       z_range_size,slip::BASE_REORTHOGONALIZATION_GRAM_SCHMIDT);
  //slip::OLearyPolyBase3d<T> base2(x_range_size,y_range_size,slip::BASE_REORTHOGONALIZATION_NONE,slip::POLY_BASE_1D_RAM_STORAGE);
   slip::OLearyPolyBase3d<T> base2(x_range_size,
				       y_range_size,
				       z_range_size,
				       slip::BASE_REORTHOGONALIZATION_NONE,
				       slip::POLY_BASE_ND_RAM_STORAGE);

   base2.generate();
   std::cout<<"base2 = \n"<<base2<<std::endl;
//    //OK
//   //  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   //  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   // std::cout << "Orthogonality test" << std::endl;
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

//   // slip::Matrix<double> Gram2;
//   // base2.orthogonality_matrix(Gram2);
//   // std::cout<<"orthogonality matrix:\n"<<Gram2<<std::endl;
//   // std::cout<<"orthogonality test (1e-14): "<<base2.is_orthogonal(1e-14)<<std::endl;
//   // std::cout<<"orthogonality precision: "<<base2.orthogonality_precision()<<std::endl;


 
 
  //OK
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "copy constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::OLearyPolyBase3d<T> base3(base2);
   std::cout<<"base3 = \n"<<base3<<std::endl;

  //OK
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "operator=" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::OLearyPolyBase3d<T> base4;
  base4 = base2;
  std::cout<<"base4 = \n"<<base4<<std::endl;




  

// //   //OK
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   // std::cout << "Gram condition number " << std::endl;
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   // std::cout<<base2.gram_cond()<<std::endl;
  
// // //   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
// // //   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
// // //   std::cout << "condition number " << std::endl;
// // //   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
// // //   std::cout<<base2.cond()<<std::endl;

// // //   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
// // //   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
// // //   std::cout << "polynomial matrix rank " << std::endl;
// // //   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
// // //   std::cout<<base2.rank()<<std::endl;

 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "projection of data onto the basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Array3d<double> Acte(z_range_size,y_range_size,x_range_size,5.12);
  std::cout<<"Acte = \n"<<Acte<<std::endl;
  
  


 
  std::cout<<"base2.project(Acte.front_upper_left(),Acte.back_bottom_right(),0,0,0)  = "<<base2.project(Acte.front_upper_left(),Acte.back_bottom_right(),0,0,0)<<std::endl;
  std::cout<<"base2.project(Acte.front_upper_left(),Acte.back_bottom_right(),0,0,1)  = "<<base2.project(Acte.front_upper_left(),Acte.back_bottom_right(),0,0,1)<<std::endl;
  std::cout<<"base2.project(Acte.front_upper_left(),Acte.back_bottom_right(),0,1,0)  = "<<base2.project(Acte.front_upper_left(),Acte.back_bottom_right(),0,1,0)<<std::endl;
  std::cout<<"base2.project(Acte.front_upper_left(),Acte.back_bottom_right(),1,0,0)  = "<<base2.project(Acte.front_upper_left(),Acte.back_bottom_right(),1,0,0)<<std::endl;


   
   
   
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "projection of data onto the basis " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    const std::size_t x_degree_max2 = x_range_size-1;
    const std::size_t y_degree_max2 = y_range_size-1;
    const std::size_t z_degree_max2 = z_range_size-1;
    
    slip::Array<double> coef_cte((x_degree_max2+1)*(y_degree_max2+1)*(z_degree_max2+1));
    // std::cout<<"coef_cte.size() = "<<coef_cte.size()<<std::endl;
    base2.project(Acte.front_upper_left(),Acte.back_bottom_right(),
		  x_degree_max2,
		  y_degree_max2,
		  z_degree_max2,
		  coef_cte.begin(),coef_cte.end());
    std::cout<<"coef_cte = "<<coef_cte<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "reconstruction of the data from the basis" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Array3d<double> Ar_cte(Acte.slices(),Acte.rows(),Acte.cols());
  //const std::size_t x_degree_rec2 = 1;
  // const std::size_t y_degree_rec2 = 2;
  const std::size_t x_degree_rec2 = x_degree_max2;
  const std::size_t y_degree_rec2 = y_degree_max2;
  const std::size_t z_degree_rec2 = z_degree_max2;
 
  
  base2.reconstruct(coef_cte.begin(),coef_cte.end(),
  		    x_degree_rec2,
  		    y_degree_rec2,
  		    z_degree_rec2,
  		    Ar_cte.front_upper_left(),
  		    Ar_cte.back_bottom_right());
  std::cout<<"Ar_cte = \n"<<Ar_cte<<std::endl;

  slip::Array3d<double> A(z_range_size,y_range_size,x_range_size);
  slip::iota(A.begin(),A.end(),1.0);
  std::cout<<"A = \n"<<A<<std::endl;
  const std::size_t x_degree_max = x_range_size-1;
  const std::size_t y_degree_max = y_range_size-1;
  const std::size_t z_degree_max = z_range_size-1;
    
    slip::Array<double> coef((x_degree_max+1)*(y_degree_max+1)*(z_degree_max+1));
    base2.project(A.front_upper_left(),A.back_bottom_right(),
  		  x_degree_max,
  		  y_degree_max,
    z_degree_max,
  		  coef.begin(),coef.end());
    std::cout<<"coef = "<<coef<<std::endl;
    
    slip::Array3d<double> Ar(A.slices(),A.rows(),A.cols());

    const std::size_t x_degree_rec = x_degree_max;
    const std::size_t y_degree_rec = y_degree_max;
    const std::size_t z_degree_rec = z_degree_max;

 
  
  base2.reconstruct(coef.begin(),coef.end(),
  		    x_degree_rec,
  		    y_degree_rec,
  		    z_degree_rec,
  		    Ar.front_upper_left(),
  		    Ar.back_bottom_right());
  std::cout<<"Ar = \n"<<Ar<<std::endl;
  std::cout<<"mean square reconstruction error = "<<slip::mean_square_diff<double>(A.begin(),A.end(),Ar.begin())<<std::endl;
 
    
  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(0.0,1.0);

  slip::Matrix3d<double> A_rnd(z_range_size,y_range_size,x_range_size);
  for(auto it = A_rnd.begin(); it != A_rnd.end(); ++it)
    {
      *it = distribution(generator);
    }
  
  const std::size_t x_degree_rnd_max = x_range_size-1;
  const std::size_t y_degree_rnd_max = y_range_size-1;
  const std::size_t z_degree_rnd_max = z_range_size-1;

 
  std::cout<<"A_rnd = \n"<<A_rnd<<std::endl;
  slip::Array<double> coef_rnd(base2.size());
  base2.project(A_rnd.front_upper_left(),A_rnd.back_bottom_right(),
  		x_degree_rnd_max,
  		y_degree_rnd_max,
  		z_degree_rnd_max,    
  		coef_rnd.begin(),coef_rnd.end());
  std::cout<<"coef_rnd = \n"<<coef_rnd<<std::endl;

  slip::Matrix3d<double> Ar_rnd(A_rnd.slices(),A_rnd.rows(),A_rnd.cols());
  const std::size_t x_degree_rnd_rec2 = x_degree_rnd_max;
  const std::size_t y_degree_rnd_rec2 = y_degree_rnd_max;
  const std::size_t z_degree_rnd_rec2 = z_degree_rnd_max;
 
  base2.reconstruct(coef_rnd.begin(),coef_rnd.end(),
  		    x_degree_rnd_rec2,
  		    y_degree_rnd_rec2,
   		    z_degree_rnd_rec2,   
  		    Ar_rnd.front_upper_left(),
  		    Ar_rnd.back_bottom_right());
  std::cout<<"Ar_rnd = \n"<<Ar_rnd<<std::endl;
  std::cout<<"mean square reconstruction error = "<<slip::mean_square_diff<double>(A_rnd.begin(),A_rnd.end(),Ar_rnd.begin())<<std::endl;
 

 



 
  return 0;
}
