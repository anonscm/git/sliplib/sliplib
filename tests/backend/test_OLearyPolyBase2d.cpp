#include <iostream>
#include <iomanip>
#include <vector>
#include <complex>
#include <random>
#include "OLearyPolyBase2d.hpp"
#include "Array2d.hpp"
#include "Vector.hpp"
#include "Polynomial.hpp"
#include "polynomial_algo.hpp"
#include "arithmetic_op.hpp"
#include "Vector2d.hpp"
#include "discrete_polynomial_algos.hpp"



int main()
{
  typedef double T;
  
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "default base construction " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::OLearyPolyBase2d<T> base1;
  std::cout<<"base1 = \n"<<base1<<std::endl;

    
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "base construction and generation" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
   std::size_t x_range_size = 10;
  //std::size_t x_range_size = 8;
  std::size_t y_range_size = 8;


 
  //slip::OLearyPolyBase2d<T> base2(x_range_size,
  //				      y_range_size);
  //slip::OLearyPolyBase2d<T> base2(x_range_size,y_range_size,slip::BASE_REORTHOGONALIZATION_GRAM_SCHMIDT);
  //slip::OLearyPolyBase2d<T> base2(x_range_size,y_range_size,slip::BASE_REORTHOGONALIZATION_NONE,slip::POLY_BASE_1D_RAM_STORAGE);
   slip::OLearyPolyBase2d<T> base2(x_range_size,y_range_size,slip::BASE_REORTHOGONALIZATION_NONE,slip::POLY_BASE_ND_RAM_STORAGE);


  

 
  base2.generate();
  std::cout<<"base2 = \n"<<base2<<std::endl;

  //OK
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Orthogonality test" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Matrix<double> Gram2;
  base2.orthogonality_matrix(Gram2);
  std::cout<<"orthogonality matrix:\n"<<Gram2<<std::endl;
  std::cout<<"orthogonality test (1e-14): "<<base2.is_orthogonal(1e-14)<<std::endl;
  std::cout<<"orthogonality precision: "<<base2.orthogonality_precision()<<std::endl;


 
 
  //OK
  //  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "copy constructor" << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // slip::OLearyPolyBase2d<T> base3(base2);
  //  std::cout<<"base3 = \n"<<base3<<std::endl;

  //OK
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  // std::cout << "operator=" << std::endl;
  // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  // slip::OLearyPolyBase2d<T> base4;
  // base4 = base2;
  // std::cout<<"base4 = \n"<<base4<<std::endl;




  

  //OK
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Gram condition number " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<base2.gram_cond()<<std::endl;
  
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "condition number " << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout<<base2.cond()<<std::endl;

//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "polynomial matrix rank " << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout<<base2.rank()<<std::endl;

 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "projection of data onto the basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Array2d<double> Acte(y_range_size,x_range_size,5.12);
  std::cout<<"Acte = \n"<<Acte<<std::endl;
  
  


 
  std::cout<<"base2.project(Acte.upper_left(),Acte.bottom_right(),0,0)  = "<<base2.project(Acte.upper_left(),Acte.bottom_right(),0,0)<<std::endl;
 std::cout<<"base2.project(Acte.upper_left(),Acte.bottom_right(),0,1)  = "<<base2.project(Acte.upper_left(),Acte.bottom_right(),0,1)<<std::endl;
  std::cout<<"base2.project(Acte.upper_left(),Acte.bottom_right(),1,0)  = "<<base2.project(Acte.upper_left(),Acte.bottom_right(),1,0)<<std::endl;
    std::cout<<"base2.project(Acte.upper_left(),Acte.bottom_right(),1,1)  = "<<base2.project(Acte.upper_left(),Acte.bottom_right(),1,1)<<std::endl;


   
   
   
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "projection of data onto the basis " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    const std::size_t x_degree_max2 = x_range_size-1;
    const std::size_t y_degree_max2 = y_range_size-1;
    
    slip::Array<double> coef_cte((x_degree_max2+1)*(y_degree_max2+1));
    base2.project(Acte.upper_left(),Acte.bottom_right(),
		  x_degree_max2,
		  y_degree_max2,
		  coef_cte.begin(),coef_cte.end());
    std::cout<<"coef_cte = "<<coef_cte<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "reconstruction of the data from the basis" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Array2d<double> Ar_cte(Acte.rows(),Acte.cols());
  //const std::size_t x_degree_rec2 = 1;
  // const std::size_t y_degree_rec2 = 2;
  const std::size_t x_degree_rec2 = x_degree_max2;
  const std::size_t y_degree_rec2 = y_degree_max2;

 
  
  base2.reconstruct(coef_cte.begin(),coef_cte.end(),
		    x_degree_rec2,
		    y_degree_rec2,
		    Ar_cte.upper_left(),
		    Ar_cte.bottom_right());
  std::cout<<"Ar_cte = \n"<<Ar_cte<<std::endl;

  slip::Array2d<double> A(y_range_size,x_range_size);
  slip::iota(A.begin(),A.end(),1.0);
  std::cout<<"A = \n"<<A<<std::endl;
  const std::size_t x_degree_max = x_range_size-1;
  const std::size_t y_degree_max = y_range_size-1;
    
    slip::Array<double> coef((x_degree_max+1)*(y_degree_max+1));
    base2.project(A.upper_left(),A.bottom_right(),
		  x_degree_max,
		  y_degree_max,
		  coef.begin(),coef.end());
    std::cout<<"coef = "<<coef<<std::endl;
    
    slip::Array2d<double> Ar(A.rows(),A.cols());

    const std::size_t x_degree_rec = x_degree_max;
    const std::size_t y_degree_rec = y_degree_max;

 
  
  base2.reconstruct(coef.begin(),coef.end(),
		    x_degree_rec,
		    y_degree_rec,
		    Ar.upper_left(),
		    Ar.bottom_right());
  std::cout<<"Ar = \n"<<Ar<<std::endl;
  std::cout<<"mean square reconstruction error = "<<slip::mean_square_diff<double>(A.begin(),A.end(),Ar.begin())<<std::endl;
 
    
  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(0.0,1.0);

  slip::Matrix<double> A_rnd(y_range_size,x_range_size);
  for(auto it = A_rnd.begin(); it != A_rnd.end(); ++it)
    {
      *it = distribution(generator);
    }
  
  const std::size_t x_degree_rnd_max = x_range_size-1;
  const std::size_t y_degree_rnd_max = y_range_size-1;

 
  std::cout<<"A_rnd = \n"<<A_rnd<<std::endl;
  slip::Array<double> coef_rnd(base2.size());
  base2.project(A_rnd.upper_left(),A_rnd.bottom_right(),
  		x_degree_rnd_max,
  		y_degree_rnd_max,
  		coef_rnd.begin(),coef_rnd.end());
  std::cout<<"coef_rnd = \n"<<coef_rnd<<std::endl;

  slip::Matrix<double> Ar_rnd(A_rnd.rows(),A_rnd.cols());
  const std::size_t x_degree_rnd_rec2 = x_degree_rnd_max;
  const std::size_t y_degree_rnd_rec2 = y_degree_rnd_max;
 
  base2.reconstruct(coef_rnd.begin(),coef_rnd.end(),
  		    x_degree_rnd_rec2,
  		    y_degree_rnd_rec2,
  		    Ar_rnd.upper_left(),
  		    Ar_rnd.bottom_right());
  std::cout<<"Ar_rnd = \n"<<Ar_rnd<<std::endl;
  std::cout<<"mean square reconstruction error = "<<slip::mean_square_diff<double>(A_rnd.begin(),A_rnd.end(),Ar_rnd.begin())<<std::endl;
 
//  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "reconstruction tests" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   slip::Array<double> A_cte(range_size,T(5.12));
//   std::cout<<"A_cte = \n"<<A_cte<<std::endl;
  
//   slip::Array<double> coef_cte(base2.size());
//   base2.project(A_cte.begin(),A_cte.end(),
// 		coef_cte.begin(),coef_cte.end());
//   std::cout<<"coef_cte = \n"<<coef_cte<<std::endl;

//   slip::Array<double> Ar_cte(A.size());
//   std::size_t K_cte = A_cte.size();
//   base2.reconstruct(coef_cte.begin(),coef_cte.end(),
// 		    K_cte,
// 		    Ar_cte.begin(),
// 		    Ar_cte.end());
//   std::cout<<"Ar_cte = \n"<<Ar_cte<<std::endl;



//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "big base test" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   slip::OLearyPolyBase1d<T> big_base(1024);
//   //slip::OLearyPolyBase1d<T> big_base(1024,slip::BASE_REORTHOGONALIZATION_GRAM_SCHMIDT);
//   //slip::OLearyPolyBase1d<T> big_base(1024,slip::BASE_REORTHOGONALIZATION_NONE);
//   big_base.generate();

//    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "Gram condition number " << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout<<big_base.gram_cond()<<std::endl;
  
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "condition number " << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout<<big_base.cond()<<std::endl;

//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "polynomial matrix rank " << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout<<big_base.rank()<<std::endl;
  return 0;
}
