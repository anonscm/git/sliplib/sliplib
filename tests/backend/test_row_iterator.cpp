#include<algorithm>
#include<iostream>
#include<iterator>

#include "Array2d.hpp"
#include "Matrix.hpp"
#include "GrayscaleImage.hpp"
#include "Block2d.hpp"
#include "stride_iterator.hpp"
int main()
{

  std::cout<<"Test Array2d..."<<std::endl;
  slip::Array2d<double> M(3,4);
  std::generate(M.begin(),M.end(),std::rand);
  std::cout<<"M = "<<std::endl;
  std::cout<<M<<std::endl;
  std::cout<<std::endl;
  
  std::cout<<"row 2 : "<<std::endl;
  std::copy(M.row_begin(2),M.row_end(2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;

   std::cout<<"col 1 : "<<std::endl;
  std::copy(M.col_begin(1),M.col_end(1),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;

  std::cout<<"col 2 : "<<std::endl;
  slip::Array2d<double>::col_iterator it;
  slip::Array2d<double>::col_iterator itb = M.col_begin(2);
  slip::Array2d<double>::col_iterator ite = M.col_end(2);
  
    for(it = itb; it != ite; ++it)
     {
       std::cout<<(*it)<<" ";
     }
   std::cout<<std::endl;


   std::cout<<"M by row iteration : "<<std::endl;
   for(std::size_t i = 0; i < M.dim1(); ++i)
     { 
       double* itl;
       double* itlb = M.row_begin(i);
       double* itle = M.row_end(i);  
       for(itl = itlb; itl != itle; ++itl)
	 {
	   std::cout<<*itl<<" ";
	 }
       std::cout<<std::endl;
	 
     }
   

    std::cout<<"-----------------------------------"<<std::endl;
    std::cout<<"Test Matrix..."<<std::endl;
    slip::Matrix<double> MM(3,4);
    std::generate(MM.begin(),MM.end(),std::rand);
    std::cout<<"MM = "<<std::endl;
    std::cout<<MM<<std::endl;
    std::cout<<std::endl;

    std::cout<<"row 2 : "<<std::endl;
    std::copy(MM.row_begin(2),MM.row_end(2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;

  std::cout<<"col 1 : "<<std::endl;
  std::copy(MM.col_begin(1),MM.col_end(1),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"col 2 : "<<std::endl;
  slip::Matrix<double>::col_iterator itm;
  slip::Matrix<double>::col_iterator itmb = MM.col_begin(2);
  slip::Matrix<double>::col_iterator itme = MM.col_end(2);
  
    for(itm = itmb; itm != itme; ++itm)
     {
       std::cout<<(*itm)<<" ";
     }
   std::cout<<std::endl;


   std::cout<<"MM by row iteration : "<<std::endl;
   for(std::size_t i = 0; i < MM.dim1(); ++i)
     { 
       double* itl;
       double* itlb = MM.row_begin(i);
       double* itle = MM.row_end(i);  
       for(itl = itlb; itl != itle; ++itl)
	 {
	   std::cout<<*itl<<" ";
	 }
       std::cout<<std::endl;
	 
     }
 std::cout<<"-----------------------------------"<<std::endl;   

   
 std::cout<<"-----------------------------------"<<std::endl;
    std::cout<<"Test GrayscaleImage..."<<std::endl;
    slip::GrayscaleImage<double> I(3,4);
    std::generate(I.begin(),I.end(),std::rand);
    std::cout<<"I = "<<std::endl;
    std::cout<<I<<std::endl;
    std::cout<<std::endl;

    std::cout<<"row 2 : "<<std::endl;
    std::copy(I.row_begin(2),I.row_end(2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;

  std::cout<<"col 1 : "<<std::endl;
  std::copy(I.col_begin(1),I.col_end(1),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"col 2 : "<<std::endl;
  slip::GrayscaleImage<double>::col_iterator iti;
  slip::GrayscaleImage<double>::col_iterator itib = I.col_begin(2);
  slip::GrayscaleImage<double>::col_iterator itie = I.col_end(2);
  
    for(iti = itib; iti != itie; ++iti)
     {
       std::cout<<(*iti)<<" ";
     }
   std::cout<<std::endl;


   std::cout<<"I by row iteration : "<<std::endl;
   for(std::size_t i = 0; i < I.dim1(); ++i)
     { 
       double* itl;
       double* itlb = I.row_begin(i);
       double* itle = I.row_end(i);  
       for(itl = itlb; itl != itle; ++itl)
	 {
	   std::cout<<*itl<<" ";
	 }
       std::cout<<std::endl;
	 
     }
 std::cout<<"-----------------------------------"<<std::endl;   

   


   
 // std::cout<<"-----------------------------------"<<std::endl;
//     std::cout<<"Test block2d..."<<std::endl;
//     slip::block2d<double,3,5> B;
//     std::generate(B.begin(),B.end(),std::rand);
//     std::cout<<"B = "<<std::endl;
//     std::cout<<B<<std::endl;
//     std::cout<<std::endl;

//     std::cout<<"row 2 : "<<std::endl;
//     std::copy(B.row_begin(2),B.row_end(2),std::ostream_iterator<double>(std::cout," "));
//   std::cout<<std::endl;

//   std::cout<<"col 1 : "<<std::endl;
//   std::copy(B.col_begin(1),B.col_end(1),std::ostream_iterator<double>(std::cout," "));
//   std::cout<<std::endl;
//   std::cout<<"col 2 : "<<std::endl;
//   slip::block2d<double>::col_iterator itb;
//   slip::block2d<double>::col_iterator itbb = B.col_begin(2);
//   slip::block2d<double>::col_iterator itbe = B.col_end(2);
  
//     for(itb = itbb; itb != itbe; ++itb)
//      {
//        std::cout<<(*itb)<<" ";
//      }
//    std::cout<<std::endl;


//    std::cout<<"B by row iteration : "<<std::endl;
//    for(std::size_t i = 0; i < B.dim1(); ++i)
//      { 
//        double* itl;
//        double* itlb = B.row_begin(i);
//        double* itle = B.row_end(i);  
//        for(itl = itlb; itl != itle; ++itl)
// 	 {
// 	   std::cout<<*itl<<" ";
// 	 }
//        std::cout<<std::endl;
	 
//      }
//  std::cout<<"-----------------------------------"<<std::endl;   

   
  return 0;
}
