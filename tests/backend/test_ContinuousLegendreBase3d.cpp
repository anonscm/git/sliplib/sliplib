#include <iostream>
#include <iomanip>
#include <vector>
#include "Array3d.hpp"

#include "ContinuousLegendreBase3d.hpp"
#include "PolySupport.hpp"

int main()
{
  typedef double T;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Default constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousLegendreBase3d<T> default_base3d;
  std::cout<<"default_base3d = \n"<<default_base3d<<std::endl;

  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Constructor 3d" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  const std::size_t base3d1_range_size_slice = 4;
  const std::size_t base3d1_range_size_row = 4;
  const std::size_t base3d1_range_size_col = 4;
  const std::size_t base3d1_degree = 3;

  slip::PolySupport<T> base3d1_support_slice(T(),T(base3d1_range_size_slice-1));
  slip::PolySupport<T> base3d1_support_row(T(),T(base3d1_range_size_row-1));
  slip::PolySupport<T> base3d1_support_col(T(),T(base3d1_range_size_col-1));

 slip::POLY_BASE_GENERATION_METHOD generation_method = slip::POLY_BASE_GENERATION_FILE;
  slip::POLY_INTEGRATION_METHOD integration_method = slip::POLY_INTEGRATION_EXACT;
 
  slip::ContinuousLegendreBase3d<T> base3d1(base3d1_range_size_slice,
						base3d1_range_size_row,
						base3d1_range_size_col,
						base3d1_degree,
						false,
						false,
						generation_method,
						integration_method,
						base3d1_support_slice,
						base3d1_support_row,
						base3d1_support_col);
  // std::cout<<"base3d1 = \n"<<base3d1<<std::endl;
   base3d1.generate();
   std::cout<<"base3d1 = \n"<<base3d1<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Constructor 3d complete base" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  const std::size_t base3d2_range_size_slice = 4;
  const std::size_t base3d2_range_size_row = 6;
  const std::size_t base3d2_range_size_col = 5;
  const std::size_t base3d2_degree_slice = 3;
  const std::size_t base3d2_degree_row = 5;
  const std::size_t base3d2_degree_col = 4;
 
  slip::PolySupport<T> base3d2_support_slice(T(),T(base3d2_range_size_slice-1));
  slip::PolySupport<T> base3d2_support_row(T(),T(base3d2_range_size_row-1));
  slip::PolySupport<T> base3d2_support_col(T(),T(base3d2_range_size_col-1));
  slip::ContinuousLegendreBase3d<T> base3d2(base3d2_range_size_slice,
  						base3d2_range_size_row,
  						base3d2_range_size_col,
  						base3d2_degree_slice,
  						base3d2_degree_row,
  						base3d2_degree_col,
  						false,
  						false,
  						generation_method,
  						integration_method,
  						base3d1_support_slice,
  						base3d1_support_row,
  						base3d1_support_col);
  //std::cout<<"base3d2 = \n"<<base3d2<<std::endl;
  base3d2.generate();
  std::cout<<"base3d2 = \n"<<base3d2<<std::endl;
  
 
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Copy constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousLegendreBase3d<T> copy_base3d(base3d1);
  std::cout<<"copy_base3d = \n"<<copy_base3d<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "operator=" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ContinuousLegendreBase3d<T> equal_base3d;
  equal_base3d = base3d1;
  std::cout<<"equal_base3d = \n"<<equal_base3d<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "evaluate" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array3d<T> P000(base3d1_range_size_slice,
			base3d1_range_size_row,
			base3d1_range_size_col);
  base3d1.evaluate(0,0,0,P000.front_upper_left(),P000.back_bottom_right());
  std::cout<<"P000 = \n"<<P000<<std::endl;
  slip::Array3d<T> P001(base3d1_range_size_slice,
			base3d1_range_size_row,
			base3d1_range_size_col);
  base3d1.evaluate(0,0,1,P001.front_upper_left(),P001.back_bottom_right());
  std::cout<<"P001 =  \n"<<P001<<std::endl;
  slip::Array3d<T> P010(base3d1_range_size_slice,
			base3d1_range_size_row,
			base3d1_range_size_col);
  base3d1.evaluate(0,1,0,P010.front_upper_left(),P010.back_bottom_right());
  std::cout<<"P010 =  \n"<<P010<<std::endl;

   slip::Array3d<T> P100(base3d1_range_size_slice,
			 base3d1_range_size_row,
			 base3d1_range_size_col);
  base3d1.evaluate(1,0,0,P100.front_upper_left(),P100.back_bottom_right());
  std::cout<<"P100 =  \n"<<P100<<std::endl;




  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "project" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array3d<T> data(base3d1_range_size_slice,
			base3d1_range_size_row,
			base3d1_range_size_col,
			T(5.12));
  
  std::cout<<"data =\n"<<data<<std::endl;
  std::cout<<"(P000,data) ="<<base3d1.project(data.front_upper_left(),
  					      data.back_bottom_right(),0ul,0ul,0ul)<<std::endl;

  slip::Array3d<T> A(4,4,4);
  A[0][0][0] = T(3.0); A[0][0][1] = T(2.0); A[0][0][2] = T(1.0); A[0][0][3] = T(5.0); 
  A[0][1][0] = T(6.0); A[0][1][1] = T(1.0); A[0][1][2] = T(7.0); A[0][1][3] = T(3.0); 
  A[0][2][0] = T(2.0); A[0][2][1] = T(8.0); A[0][2][2] = T(4.0); A[0][2][3] = T(6.0); 
  A[0][3][0] = T(5.0); A[0][3][1] = T(1.0); A[0][3][2] = T(4.0); A[0][3][3] = T(2.0); 

  A[1][0][0] = T(8.0); A[1][0][1] = T(4.0); A[1][0][2] = T(6.0); A[1][0][3] = T(7.0); 
  A[1][1][0] = T(2.0); A[1][1][1] = T(9.0); A[1][1][2] = T(6.0); A[1][1][3] = T(6.0); 
  A[1][2][0] = T(2.0); A[1][2][1] = T(5.0); A[1][2][2] = T(9.0); A[1][2][3] = T(3.0); 
  A[1][3][0] = T(7.0); A[1][3][1] = T(4.0); A[1][3][2] = T(6.0); A[1][3][3] = T(7.0); 

 A[2][0][0] = T(8.0); A[2][0][1] = T(4.0); A[2][0][2] = T(6.0); A[2][0][3] = T(7.0); 
  A[2][1][0] = T(2.0); A[2][1][1] = T(9.0); A[2][1][2] = T(6.0); A[2][1][3] = T(6.0); 
  A[2][2][0] = T(2.0); A[2][2][1] = T(5.0); A[2][2][2] = T(9.0); A[2][2][3] = T(3.0); 
  A[2][3][0] = T(7.0); A[2][3][1] = T(4.0); A[2][3][2] = T(6.0); A[2][3][3] = T(7.0); 

  A[3][0][0] = T(8.0); A[3][0][1] = T(4.0); A[3][0][2] = T(6.0); A[3][0][3] = T(7.0); 
  A[3][1][0] = T(2.0); A[3][1][1] = T(9.0); A[3][1][2] = T(6.0); A[3][1][3] = T(6.0); 
  A[3][2][0] = T(2.0); A[3][2][1] = T(5.0); A[3][2][2] = T(9.0); A[3][2][3] = T(3.0); 
  A[3][3][0] = T(7.0); A[3][3][1] = T(4.0); A[3][3][2] = T(6.0); A[3][3][3] = T(7.0); 
   std::cout<<"A =\n"<<A<<std::endl;
//   std::cout<<base3d1.project(A.front_upper_left(),A.back_bottom_right(),0,0,0)<<std::endl;
for(std::size_t k = 0; k < 4; ++k)
    {
      for(std::size_t i = 0; i < 4; ++i)
	{
	  for(std::size_t j = 0; j < 4; ++j)
	    {
	      std::cout<<base3d1.project(A.front_upper_left(),A.back_bottom_right(),k,i,j)<<" ";
	    }
	  std::cout<<" ;"<<std::endl;
	}
      std::cout<<std::endl;
      std::cout<<std::endl;
    }

 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "projection coefficients" << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 slip::Array<T> coefdata(base3d1.size());
 base3d1.project(data.front_upper_left(),data.back_bottom_right(),base3d1.size(),coefdata.begin(),coefdata.end());
 std::cout<<"coefdata = \n"<<coefdata<<std::endl;


 slip::Array<T> coefA_deg3(base3d1.size());
 base3d1.project(A.front_upper_left(),A.back_bottom_right(),3,coefA_deg3.begin(),coefA_deg3.end());
 std::cout<<"coefA_deg3 = \n"<<coefA_deg3<<std::endl;
 // slip::Array<T> coefA2_deg2(6);
 // base3d1.project(A2.upper_left(),A2.bottom_right(),2,coefA2_deg2.begin(),coefA2_deg2.end());
 // std::cout<<"coefA2_deg2 = \n"<<coefA2_deg2<<std::endl;

 
//   // for(std::size_t k = 0; k < base3d1.size(); ++k)
//   //   {
//   //     std::cout<<"(P"<<k<<",data) ="<<base3d1.project(data.begin(),data.end(),k)<<std::endl;
//   //   }
  
  
//   // slip::Array<T> base3d1_coef(base3d1.size());
//   // base3d1.project(data.begin(),data.end(),base3d1_coef.begin(),base3d1_coef.end());
//   // std::cout<<"base3d1_coef = \n"<<base3d1_coef<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "reconstruction" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array3d<T> datar(data.slices(),data.rows(),data.cols());
  
   base3d1.reconstruct(coefdata.begin(),coefdata.end(),
		       3,
		       datar.front_upper_left(),datar.back_bottom_right());
   std::cout<<"datar = \n"<<datar<<std::endl;

//   slip::Array3d<T> A2r(base3d1_range_size_row,
// 		       base3d1_range_size_col);
//   for(std::size_t k = 1; k <= coefA2_deg3.size(); ++k)
//     {
//       base3d1.reconstruct(coefA2_deg3.begin(),coefA2_deg3.end(),
// 			  k,
// 			  A2r.upper_left(),A2r.bottom_right());
//       std::cout<<"A2r (KA2 = "<<k<<")  = \n"<<A2r<<std::endl;
//     }


//   slip::PolySupport<T> base3d3_support_row(0.0,3.0);
//   slip::PolySupport<T> base3d3_support_col(0.0,4.0);

//     slip::ContinuousLegendreBase3d<T> base3d3(3,
// 						  4,
// 						  3,
// 						  false,
// 						  false,
// 						  generation_method,
// 						  integration_method,
// 						  base3d3_support_row,
// 						  base3d3_support_col);
//     base3d3.generate();
//     std::cout<<"base3d3 = \n"<<base3d3<<std::endl;

//   slip::Array3d<T> A3(3,4);
//   A3[0][0] = T(3.0); A3[0][1] = T(2.0); A3[0][2] = T(1.0); A3[0][3] = T(5.0); 
//   A3[1][0] = T(6.0); A3[1][1] = T(1.0); A3[1][2] = T(7.0); A3[1][3] = T(3.0); 
//   A3[2][0] = T(2.0); A3[2][1] = T(8.0); A3[2][2] = T(4.0); A3[2][3] = T(6.0); 
//   std::cout<<"A3 =\n"<<A3<<std::endl;

//   std::cout<<base3d3.project(A3.upper_left(),A3.bottom_right(),0,0)<<std::endl;
// for(std::size_t dx = 0; dx < 4; ++dx)
//     {
//       for(std::size_t dy = 0; dy < 3; ++dy)
// 	{
// 	  std::cout<<base3d3.project(A3.upper_left(),A3.bottom_right(),dx,dy)<<" ";
// 	}
//       std::cout<<std::endl;
//     }

// slip::Array<T> coefA3_deg3(10);
//  base3d3.project(A3.upper_left(),A3.bottom_right(),3,coefA3_deg3.begin(),coefA3_deg3.end());
//  std::cout<<"coefA3_deg3 = \n"<<coefA3_deg3<<std::endl;
//  slip::Array<T> coefA3_deg2(6);
//  base3d3.project(A3.upper_left(),A3.bottom_right(),2,coefA3_deg2.begin(),coefA3_deg2.end());
//  std::cout<<"coefA3_deg2 = \n"<<coefA3_deg2<<std::endl;
// // slip::Array3d<T> A3r(4,3);
// //   for(std::size_t k = 1; k <= coefA3_deg3.size(); ++k)
// //     {
// //       base3d3.reconstruct(coefA3_deg3.begin(),coefA3_deg3.end(),
// // 			  k,
// // 			  A3r.upper_left(),A3r.bottom_right());
// //       std::cout<<"A3r (KA3 = "<<k<<")  = \n"<<A3r<<std::endl;
// //     }
 
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   // std::cout << "analytic reconstruction " << std::endl;
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   // slip::MultivariatePolynomial<T,2> base3d1_recont_poly =
//   //   base3d1.reconstruct(base3d1_coef.begin(),base3d1_coef.end(),base3d1.size());
//   // std::cout<<"base3d1_recont_poly =\n"<<base3d1_recont_poly<<std::endl;
 
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   // std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   // std::cout << "accessors/mutators" << std::endl;
//   // std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  

  

  return 0;
}
