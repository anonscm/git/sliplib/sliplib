#include<algorithm>
#include<iostream>
#include<iterator>
#include <numeric>
#include <complex>
#include <vector>
#include "Array3d.hpp"
#include "DPoint2d.hpp"
#include "DPoint3d.hpp"
#include "Box3d.hpp"
#include "Box2d.hpp"
#include "iterator3d_plane.hpp"

template <typename T>
void daccum(T& a){static T init = T(0); a = init++;}

int main()
{
  typedef int T;
  slip::Array3d<T> M(5,4,3);
  std::for_each(M.begin(),M.end(),daccum<T>);
  std::cout<<M<<std::endl;
  slip::Array3d<T> const Mc(M);

  slip::DPoint3d<int> de(2,1,1);
  slip::Box3d<int> box(1,2,1,3,2,2);
  std::cout<<box<<std::endl;
 //  //plane_row
//   std::cout<<"row plane forward"<<std::endl;
//   slip::Array3d<T>::iterator1d beg =  M.plane_row_begin(1,2,3);
//   slip::Array3d<T>::iterator1d end =  M.plane_row_end(1,2,3);
//   std::copy(beg,end, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   beg = M.plane_row_begin(0,3,2);
//   end = M.plane_row_end(0,3,2);
//   std::copy(beg,end, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   beg = M.plane_row_begin(2,1,3);
//   end = M.plane_row_end(2,1,3);
//   std::copy(beg,end, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   std::cout<<"const row plane forward"<<std::endl;
//   slip::Array3d<T>::const_iterator1d cbeg =  Mc.plane_row_begin(1,2,3);
//   slip::Array3d<T>::const_iterator1d cend =  Mc.plane_row_end(1,2,3);
//   std::copy(cbeg,cend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   cbeg = Mc.plane_row_begin(0,3,2);
//   cend = Mc.plane_row_end(0,3,2);
//   std::copy(cbeg,cend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;
  
//   cbeg = Mc.plane_row_begin(2,1,3);
//   cend = Mc.plane_row_end(2,1,3);
//   std::copy(cbeg,cend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;
  
//   std::cout<<"row plane backward"<<std::endl;
//   slip::Array3d<T>::reverse_iterator1d rbeg =  M.plane_row_rbegin(1,2,3);
//   slip::Array3d<T>::reverse_iterator1d rend =  M.plane_row_rend(1,2,3);
//   std::copy(rbeg,rend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   rbeg = M.plane_row_rbegin(0,3,2);
//   rend = M.plane_row_rend(0,3,2);
//   std::copy(rbeg,rend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   rbeg = M.plane_row_rbegin(2,1,3);
//   rend = M.plane_row_rend(2,1,3);
//   std::copy(rbeg,rend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   std::cout<<"const row plane backward"<<std::endl;
//   slip::Array3d<T>::const_reverse_iterator1d rcbeg =  Mc.plane_row_rbegin(1,2,3);
//   slip::Array3d<T>::const_reverse_iterator1d rcend =  Mc.plane_row_rend(1,2,3);
//   std::copy(rcbeg,rcend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   rcbeg = Mc.plane_row_rbegin(0,3,2);
//   rcend = Mc.plane_row_rend(0,3,2);
//   std::copy(rcbeg,rcend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   rcbeg = Mc.plane_row_rbegin(2,1,3);
//   rcend = Mc.plane_row_rend(2,1,3);
//   std::copy(rcbeg,rcend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   //plane_col
//   std::cout<<"plane_col forward"<<std::endl;
//   slip::Array3d<T>::iterator1d colbeg =  M.plane_col_begin(1,2,1);
//   slip::Array3d<T>::iterator1d colend =  M.plane_col_end(1,2,1);
//   std::copy(colbeg,colend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;
 
//   colbeg = M.plane_col_begin(0,3,2);
//   colend = M.plane_col_end(0,3,2);
//   std::copy(colbeg,colend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   colbeg = M.plane_col_begin(2,1,3);
//   colend = M.plane_col_end(2,1,3);
//   std::copy(colbeg,colend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   std::cout<<"Const plane_col forward"<<std::endl;
//   slip::Array3d<T>::const_iterator1d ccolbeg =  Mc.plane_col_begin(1,2,1);
//   slip::Array3d<T>::const_iterator1d ccolend =  Mc.plane_col_end(1,2,1);
//   std::copy(ccolbeg,ccolend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;
 
//   ccolbeg = Mc.plane_col_begin(0,3,2);
//   ccolend = Mc.plane_col_end(0,3,2);
//   std::copy(ccolbeg,ccolend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   ccolbeg = Mc.plane_col_begin(2,1,3);
//   ccolend = Mc.plane_col_end(2,1,3);
//   std::copy(ccolbeg,ccolend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   std::cout<<"plane_col backward"<<std::endl;
//   slip::Array3d<T>::reverse_iterator1d rcolbeg =  M.plane_col_rbegin(1,2,1);
//   slip::Array3d<T>::reverse_iterator1d rcolend =  M.plane_col_rend(1,2,1);

//   std::copy(rcolbeg,rcolend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;
 
//   rcolbeg = M.plane_col_rbegin(0,3,2);
//   rcolend = M.plane_col_rend(0,3,2);
//   std::copy(rcolbeg,rcolend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   rcolbeg = M.plane_col_rbegin(2,1,3);
//   rcolend = M.plane_col_rend(2,1,3);
//   std::copy(rcolbeg,rcolend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   std::cout<<"Const plane_col backward"<<std::endl;
//   slip::Array3d<T>::const_reverse_iterator1d crcolbeg =  Mc.plane_col_rbegin(1,2,1);
//   slip::Array3d<T>::const_reverse_iterator1d crcolend =  Mc.plane_col_rend(1,2,1);

//   std::copy(crcolbeg,crcolend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;
 
//   crcolbeg = Mc.plane_col_rbegin(0,3,2);
//   crcolend = Mc.plane_col_rend(0,3,2);
//   std::copy(crcolbeg,crcolend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

//   crcolbeg = Mc.plane_col_rbegin(2,1,3);
//   crcolend = Mc.plane_col_rend(2,1,3);
//   std::copy(crcolbeg,crcolend, std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;

 //  //iterator3d_plane
//   slip::iterator3d_plane<slip::Array3d<T> > it(&M,box);
//   std::cout<<"iterator3d_plane in box"<<std::endl;
//  //first line of the plane
//   std::copy(it.row_begin(1),it.row_end(1), std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;
//   //first column of the plane
//   std::copy(it.col_begin(1),it.col_end(1), std::ostream_iterator<int>(std::cout," "));
//   std::cout<<std::endl;

//  //const_iterator3d_plane
//   slip::const_iterator3d_plane<const slip::Array3d<T> > cit(&Mc,box);
//   std::cout<<"const_iterator3d_plane in box"<<std::endl;
//  //first line of the plane
//   std::copy(cit.row_begin(1),cit.row_end(1), std::ostream_iterator<int>(std::cout," ")); 
//   std::cout<<std::endl;
//   //first column of the plane
//   std::copy(cit.col_begin(1),cit.col_end(1), std::ostream_iterator<int>(std::cout," "));
//   std::cout<<std::endl;

  //iterator 2d
  std::cout<<"plane"<<std::endl;
  slip::Array3d<T>::iterator2d up = M.plane_upper_left(slip::KJ_PLANE,2);
  slip::Array3d<T>::iterator2d bot = M.plane_bottom_right(slip::KJ_PLANE,2);

  std::copy(up,bot, std::ostream_iterator<int>(std::cout," ")); 
  std::cout<<std::endl;
  
  std::cout<<"plane backward"<<std::endl;
  slip::Array3d<T>::reverse_iterator2d rup = M.plane_rupper_left(slip::KJ_PLANE,2);
  slip::Array3d<T>::reverse_iterator2d rbot = M.plane_rbottom_right(slip::KJ_PLANE,2);

  std::copy(rup,rbot, std::ostream_iterator<int>(std::cout," ")); 
  std::cout<<std::endl;

  //const iterator 2d
  std::cout<<"const plane"<<std::endl;
  slip::Array3d<T>::const_iterator2d cup = Mc.plane_upper_left(slip::KJ_PLANE,2);
  slip::Array3d<T>::const_iterator2d cbot = Mc.plane_bottom_right(slip::KJ_PLANE,2);

  std::copy(cup,cbot, std::ostream_iterator<int>(std::cout," ")); 
  std::cout<<std::endl;
  
  std::cout<<"const plane backward"<<std::endl;
  slip::Array3d<T>::const_reverse_iterator2d crup = Mc.plane_rupper_left(slip::KJ_PLANE,2);
  slip::Array3d<T>::const_reverse_iterator2d crbot = Mc.plane_rbottom_right(slip::KJ_PLANE,2);

  std::copy(crup,crbot, std::ostream_iterator<int>(std::cout," ")); 
  std::cout<<std::endl;
  return 0;
}
