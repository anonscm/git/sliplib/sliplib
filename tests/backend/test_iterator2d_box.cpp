#include<algorithm>
#include<iostream>
#include<iterator>
#include <numeric>
#include <complex>
#include "Array2d.hpp"

#include "Point2d.hpp"
#include "DPoint2d.hpp"
#include "Box2d.hpp"
#include "iterator2d_box.hpp"
#include "Range.hpp"

int main()
{

  slip::Array2d<double> M(5,4);
  std::generate(M.begin(),M.end(),std::rand);
  std::cout<<M<<std::endl;
  
  // slip::Point2d<int> db(0,2);
  slip::DPoint2d<int> de(3,2);
  slip::Box2d<int> box(1,1,3,2);
  std::cout<<box<<std::endl;
  


  slip::iterator2d_box<slip::Array2d<double> > beg(&M,box);
  slip::iterator2d_box<slip::Array2d<double> > e = (beg + de);
  std::cout<<*e<<std::endl;
  std::cout<<"e - beg = "<<(e-beg)<<std::endl;
  //e++;
  std::cout<<beg[de]<<std::endl<<std::endl;
  slip::iterator2d_box<slip::Array2d<double> > it;

  std::cout<<" beg == beg "<<(beg == beg)<<std::endl;
  std::cout<<" beg != beg "<<(beg != beg)<<std::endl;
  std::cout<<" beg == e "<<(beg == e)<<std::endl;
  std::cout<<" beg != e "<<(beg != e)<<std::endl;
  std::cout<<" beg < e "<<(beg < e)<<std::endl;
  std::cout<<" beg <= e "<<(beg <= e)<<std::endl;
  std::cout<<" beg > e "<<(beg > e)<<std::endl;
  std::cout<<" beg >= e "<<(beg >= e)<<std::endl;

  
  std::cout<<"ie = "<<e.x1()<<" je = "<<e.x2()<<"*e = "<<*e<<std::endl;
  for(it = beg; it != e; ++it)
    {
      std::cout<<"i = "<<it.x1()<<" j = "<<it.x2()<<"*it = "<<*it<<" ";
    }
  std::cout<<std::endl;
  std::cout<<std::endl;
 
 for(it = (e -slip::DPoint2d<int>(1,1)) ;it != (beg - slip::DPoint2d<int>(1,1)) ; --it)
    {
      std::cout<<"i = "<<it.x1()<<" j = "<<it.x2()<<"*it = "<<*it<<" ";
    }
  std::cout<<std::endl;
  std::cout<<std::endl;
 
  

  slip::iterator2d_box<slip::Array2d<double> > it2;
  
  for(it2 = M.upper_left(); it2 != M.bottom_right(); ++it2)
    {
      std::cout<<*it2<<" ";
    }
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  slip::Array2d<double>::reverse_iterator2d itr2;
  for(itr2 = M.rupper_left(); itr2 != M.rbottom_right(); ++itr2)
    {
      std::cout<<*itr2<<" ";
    }
  std::cout<<std::endl;
  std::cout<<std::endl;

 slip::iterator2d_box<slip::Array2d<double> > it3;
 slip::Box2d<int> box3(1,1,2,3);
  
  for(it3 = M.upper_left(box3); it3 != M.bottom_right(box3); ++it3)
    {
      std::cout<<*it3<<" ";
    }
  std::cout<<std::endl;
  slip::Array2d<double>::reverse_iterator2d itr3;
  for(itr3 = M.rupper_left(box3); itr3 != M.rbottom_right(box3); ++itr3)
    {
      std::cout<<*itr3<<" ";
    }
 std::cout<<std::endl;
  
 double d[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0};
 slip::Array2d<double> Md(5,4,d);
std::cout<<Md<<std::endl;
  

 
 double masque[] = {2.0, 2.0, 2.0, 2.0};
 slip::Array2d<double> Mm(2,2,masque);
 std::cout<<Mm<<std::endl;
 

 slip::Box2d<int> box4(1,1,2,2);
 std::cout<<std::inner_product(Md.upper_left(box4),Md.bottom_right(box4),Mm.upper_left(),0.0)<<std::endl;

 slip::Array2d<double> Mm2(2,2);
 std::copy(Md.upper_left(box4),Md.bottom_right(box4),Mm2.upper_left());
 std::cout<<Mm2<<std::endl;


 slip::Array2d<std::complex<double> > Mcomp(8,7);
 slip::iterator2d_box<slip::Array2d<std::complex<double> > > itc = Mcomp.upper_left();
 for(; itc != Mcomp.bottom_right(); ++itc)
    {
      *itc = *itc + 2.0;
      std::cout<<*itc<<" ";
    }
  std::cout<<std::endl;
  std::cout<<std::endl;
  itc =  Mcomp.upper_left();
  double val = 0.0;
  for(; itc != Mcomp.bottom_right(); ++itc)
    {
      // itc->imag() = val;
      *itc = std::complex<double>(itc->real(),val);
       val += 1.0;
      
      std::cout<<" ("<<itc->real()<<","<<itc->imag()<<"), ";
    } 
  std::cout<<std::endl;
  std::cout<<std::endl;

  box4.set_coord(1,1,5,6);
  
 //  itc =  Mcomp.upper_left();
//   for(std::size_t i = 0; i < Mcomp.dim1(); ++i)
//     {
//       std::copy(itc[i],itc[i] + Mcomp.dim2(),std::ostream_iterator<std::complex<double> >(std::cout," "));
//       std::cout<<std::endl;
//     }
  
  std::cout<<"Mcomp = "<<std::endl;
  std::cout<<Mcomp<<std::endl;
  std::cout<<"box 4 "<<box4<<std::endl;
  slip::iterator2d_box<slip::Array2d<std::complex<double> > > itc2 =  Mcomp.upper_left(box4);
  std::cout<<"rows of the box "<<std::endl; 
  for(std::size_t i = 0; i < (std::size_t)box4.width();++i)
    {
      std::copy(itc2.row_begin(i),itc2.row_end(i),std::ostream_iterator<std::complex<double> >(std::cout," "));
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
  std::cout<<"cols of the box "<<std::endl; 
  for(std::size_t j = 0; j < (std::size_t)box4.height();++j)
    {
      std::copy(itc2.col_begin(j),itc2.col_end(j),std::ostream_iterator<std::complex<double> >(std::cout," "));
      std::cout<<std::endl;
    }
  std::cout<<std::endl;

  slip::Range<int> r_row(1,box4.height()-2,2);
  slip::Range<int> r_col(1,box4.width()-2,2);
  std::cout<<"rows range of the box "<<std::endl; 
  std::cout<< r_col << std::endl;
  for(std::size_t i = 0; i < (std::size_t)box4.width();++i)
    {
      std::copy(itc2.row_begin(i,r_col),itc2.row_end(i,r_col),std::ostream_iterator<std::complex<double> >(std::cout," "));
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
  std::cout<<"cols range of the box "<<std::endl; 
  std::cout<< r_row << std::endl;
  for(std::size_t j = 0; j < (std::size_t)box4.height();++j)
    {
      std::copy(itc2.col_begin(j,r_row),itc2.col_end(j,r_row),std::ostream_iterator<std::complex<double> >(std::cout," "));
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
  //  std::transform(itc2.col_begin(0),itc2.col_end(0),itc2.col_begin(1),itc2.col_begin(2),std::plus<std::complex<double> >());
  std::transform(itc2.col_begin(0),itc2.col_end(0),itc2.col_begin(1),std::bind2nd(std::multiplies<std::complex<double> >(),2.0));
		 //std::compose2(std::plus<double>(),std::ptr_fun(void),bind2nd(multipy<double>(), 2.0)));
  std::cout<<"Mcomp = "<<std::endl;
  std::cout<<Mcomp<<std::endl;


  slip::Array2d<float> Mf(5,6);
  float valf = 30.0;
  for(std::size_t i = 0; i < Mf.dim1(); ++i)
    {
      for(std::size_t j = 0; j < Mf.dim2(); ++j)
	{
	  Mf(i,j) = valf;
	  valf -= 1.0;
	}
    }
  slip::Box2d<int> boxf(1,1,2,3);
  std::cout<<Mf<<std::endl;
  slip::Array2d<float>::iterator2d itf = std::max_element(Mf.upper_left(boxf),Mf.bottom_right(boxf));
  std::cout<<"max element in the box (1,1,2,3) = "<<*itf<<std::endl;
  std::cout<<" i = "<<itf.i()<<" j = "<<itf.j()<<std::endl;
  // std::sort(Mf.upper_left(boxf),Mf.bottom_right(boxf));
  std::cout<<Mf<<std::endl; 

  slip::Array2d<float> tes(2,3);
  std::copy(Mf.upper_left(boxf),Mf.bottom_right(boxf),tes.upper_left());
  std::cout<<tes<<std::endl;
  return 0;
}
