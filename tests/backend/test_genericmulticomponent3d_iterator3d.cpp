#include <algorithm>
#include <functional>
#include <iostream>
#include <iomanip>
#include <iterator>
#include <numeric>
#include <complex>

//#define ALL_PLANE_ITERATOR3D


#include "GenericMultiComponent3d.hpp"
#include "Point3d.hpp"
#include "DPoint3d.hpp"
#include "DPoint2d.hpp"
#include "Box2d.hpp"
#include "Box3d.hpp"
#include "iterator3d_box.hpp"
#include "Color.hpp"


template<typename T>
void daccum(T& a){static T init = 0.0; a = init++;}

template<typename T>
struct inc : public std::binary_function <T,T,T>
{
  T operator() (T & a, const T & c) const {return (a*=c);}
};
  
template<typename T>
void disp(T const& a){std::cout << a << std::endl;}

int main()
{
  //GenericMultiComponent3d
  slip::Matrix3d<double> Mat(5,4,3);
  std::for_each(Mat.begin(),Mat.end(),daccum<double>);
  std::vector<slip::Matrix<double>::iterator> iterators_list(3);
  iterators_list[0] = Mat.begin();
  iterators_list[1] = Mat.begin();
  iterators_list[2] = Mat.begin();
  slip::GenericMultiComponent3d<slip::Color<double> > M(5,4,3,iterators_list,Mat.end());
  //ranges
  slip::Range<int> rk(0,M.dim1()-1,2);
  slip::Range<int> rkd(1,M.dim1()-1,2);
  slip::Range<int> ri(0,M.dim3()-1,2);
  slip::Range<int> rid(1,M.dim3()-1,2);
  slip::Range<int> rj(0,M.dim2()-1,2);
  slip::Range<int> rjd(1,M.dim2()-1,2);
  //boxes
  slip::Box2d<int> b2d(1,0,3,3);
  slip::Box3d<int> b3d(1,0,1,3,2,2);
  
  std::cout<<M<<std::endl;
  
  //one dimensionnal iterators
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Simple Iterator" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  //slice : 
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "slice iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_begin(2,2),M.slice_end(2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_rbegin(2,2),M.slice_rend(2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //slice range :
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "slice range iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_begin(2,2,rk),M.slice_end(2,2,rk),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_rbegin(2,2,rk),M.slice_rend(2,2,rk),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
 
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "slice range iterator + 1" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_begin(2,2,rkd),M.slice_end(2,2,rkd),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_rbegin(2,2,rkd),M.slice_rend(2,2,rkd),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row : 
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "row iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_begin(2,2),M.row_end(2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(2,2),M.row_rend(2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row range :
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "row range iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_begin(2,2,ri),M.row_end(2,2,ri),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(2,2,ri),M.row_rend(2,2,ri),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "row range iterator + 1" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_begin(2,2,rid),M.row_end(2,2,rid),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(2,2,rid),M.row_rend(2,2,rid),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col : 
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "col iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_begin(2,2),M.col_end(2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(2,2),M.col_rend(2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col range :
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "col range iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_begin(2,2,rj),M.col_end(2,2,rj),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(2,2,rj),M.col_rend(2,2,rj),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "col range iterator + 1" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_begin(2,2,rjd),M.col_end(2,2,rjd),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(2,2,rjd),M.col_rend(2,2,rjd),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //plane iterators

  //global plane 1d
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "global plane iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_begin(2),M.plane_end(2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_rbegin(2),M.plane_rend(2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;


#ifdef ALL_PLANE_ITERATOR3D
  //plane_row
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "plane row iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_row_begin(slip::KI_PLANE,2,2),M.plane_row_end(slip::KI_PLANE,2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_row_rbegin(slip::KI_PLANE,2,2),M.plane_row_rend(slip::KI_PLANE,2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //plane_col
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "plane col iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_col_begin(slip::KI_PLANE,2,0),M.plane_col_end(slip::KI_PLANE,2,0),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_col_rbegin(slip::KI_PLANE,2,0),M.plane_col_rend(slip::KI_PLANE,2,0),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //plane box row
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "plane box row iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_row_begin(slip::KI_PLANE,2,2,b2d),M.plane_row_end(slip::KI_PLANE,2,2,b2d),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_row_rbegin(slip::KI_PLANE,2,2,b2d),M.plane_row_rend(slip::KI_PLANE,2,2,b2d),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //plane_col
  std::cout<< "plane box col iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_col_begin(slip::KI_PLANE,2,0,b2d),M.plane_col_end(slip::KI_PLANE,2,0,b2d),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_col_rbegin(slip::KI_PLANE,2,0,b2d),M.plane_col_rend(slip::KI_PLANE,2,0,b2d),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
#endif

 //global plane 2d
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "global plane 2d iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(slip::KI_PLANE,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(slip::KI_PLANE,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

   //box plane 2d
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout<< "box plane 2d iterator" <<std::endl;
  std::cout<<std::endl;
   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b2d),M.plane_bottom_right(slip::KI_PLANE,2,b2d),std::ostream_iterator<slip::Color<double> >(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;
   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b2d),M.plane_rbottom_right(slip::KI_PLANE,2,b2d),std::ostream_iterator<slip::Color<double> >(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;

   //global 3d iterator
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout<< "Global 3d iterator" <<std::endl;
  std::cout<<std::endl;
   slip::GenericMultiComponent3d<slip::Color<double> > C(M.dim1(),M.dim2(),M.dim3());
   std::copy(M.front_upper_left(),M.back_bottom_right(),C.front_upper_left());
   std::cout<< C;
   std::cout<<std::endl;
   std::cout<<std::endl;
   slip::GenericMultiComponent3d<slip::Color<double> > CBack(M.dim1(),M.dim2(),M.dim3());
   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),CBack.front_upper_left());
   std::cout<< CBack;
   std::cout<<std::endl;
   std::cout<<std::endl;

   //3d box iterator
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout<< "3d box iterator" <<std::endl;
  std::cout<<std::endl;
   slip::GenericMultiComponent3d<slip::Color<double> > Cbox(b3d.depth(),b3d.height(),b3d.width());
   std::copy(M.front_upper_left(b3d),M.back_bottom_right(b3d),Cbox.front_upper_left());
   std::cout<< Cbox;
   std::cout<<std::endl;
   std::cout<<std::endl;
   slip::GenericMultiComponent3d<slip::Color<double> > CboxBack(b3d.depth(),b3d.height(),b3d.width());
   std::copy(M.rfront_upper_left(b3d),M.rback_bottom_right(b3d),CboxBack.front_upper_left());
   std::cout<< CboxBack;
   std::cout<<std::endl;
   std::cout<<std::endl;

   //****************************** Const ***********************************//
   std::cout << std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
   std::cout << "Const Iterator" <<std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   
   slip::GenericMultiComponent3d<slip::Color<double> > const Mc(M);
  
  //Matrix3d<T>::begin() and Matrix3d<T>::end()
  std::for_each(Mc.begin(),Mc.end(),disp<slip::Color<double> >);
  std::cout<<std::endl;
  std::cout<<std::endl;

  //slice : 
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "Const slice iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.slice_begin(2,2),Mc.slice_end(2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.slice_rbegin(2,2),Mc.slice_rend(2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //slice range :
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "Const slice range iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.slice_begin(2,2,rk),Mc.slice_end(2,2,rk),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.slice_rbegin(2,2,rk),Mc.slice_rend(2,2,rk),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
 
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "const slice range iterator + 1" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.slice_begin(2,2,rkd),Mc.slice_end(2,2,rkd),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.slice_rbegin(2,2,rkd),Mc.slice_rend(2,2,rkd),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //row : 
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "const row iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_begin(2,2),Mc.row_end(2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_rbegin(2,2),Mc.row_rend(2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row range :
  std::cout<< "const row range iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_begin(2,2,ri),Mc.row_end(2,2,ri),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_rbegin(2,2,ri),Mc.row_rend(2,2,ri),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "const row range iterator + 1" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_begin(2,2,rid),Mc.row_end(2,2,rid),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.row_rbegin(2,2,rid),Mc.row_rend(2,2,rid),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //col : 
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "const col iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_begin(2,2),Mc.col_end(2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_rbegin(2,2),Mc.col_rend(2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col range :
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "const col range iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_begin(2,2,rj),Mc.col_end(2,2,rj),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_rbegin(2,2,rj),Mc.col_rend(2,2,rj),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "const col range iterator + 1" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_begin(2,2,rjd),Mc.col_end(2,2,rjd),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.col_rbegin(2,2,rjd),Mc.col_rend(2,2,rjd),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //global plane 1d
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "global const plane iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_begin(2),Mc.plane_end(2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_rbegin(2),Mc.plane_rend(2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

#ifdef ALL_PLANE_ITERATOR3D
  //plane_row
  std::cout<< "const plane row iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_row_begin(slip::KI_PLANE,2,2),Mc.plane_row_end(slip::KI_PLANE,2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_row_rbegin(slip::KI_PLANE,2,2),Mc.plane_row_rend(slip::KI_PLANE,2,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //plane_col
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "const plane col iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_col_begin(slip::KI_PLANE,2,0),Mc.plane_col_end(slip::KI_PLANE,2,0),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_col_rbegin(slip::KI_PLANE,2,0),Mc.plane_col_rend(slip::KI_PLANE,2,0),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //plane box row
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "plane box row iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_row_begin(slip::KI_PLANE,2,2,b2d),Mc.plane_row_end(slip::KI_PLANE,2,2,b2d),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_row_rbegin(slip::KI_PLANE,2,2,b2d),Mc.plane_row_rend(slip::KI_PLANE,2,2,b2d),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //plane_col
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "plane box col iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_col_begin(slip::KI_PLANE,2,0,b2d),Mc.plane_col_end(slip::KI_PLANE,2,0,b2d),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_col_rbegin(slip::KI_PLANE,2,0,b2d),Mc.plane_col_rend(slip::KI_PLANE,2,0,b2d),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
#endif

 //global plane 2d
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "global const plane 2d iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_upper_left(slip::KI_PLANE,2),Mc.plane_bottom_right(slip::KI_PLANE,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_rupper_left(slip::KI_PLANE,2),Mc.plane_rbottom_right(slip::KI_PLANE,2),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //box plane 2d
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "box const plane 2d iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_upper_left(slip::KI_PLANE,2,b2d),Mc.plane_bottom_right(slip::KI_PLANE,2,b2d),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mc.plane_rupper_left(slip::KI_PLANE,2,b2d),Mc.plane_rbottom_right(slip::KI_PLANE,2,b2d),std::ostream_iterator<slip::Color<double> >(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //global 3d iterator
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "Global const 3d iterator" <<std::endl;
  std::cout<<std::endl;
  slip::GenericMultiComponent3d<slip::Color<double> > Cc(Mc.dim1(),Mc.dim2(),Mc.dim3());
  std::copy(Mc.front_upper_left(),Mc.back_bottom_right(),Cc.front_upper_left());
  std::cout<< Cc;
  std::cout<<std::endl;
  std::cout<<std::endl;
  slip::GenericMultiComponent3d<slip::Color<double> > CBackc(Mc.dim1(),Mc.dim2(),Mc.dim3());
  std::copy(Mc.rfront_upper_left(),Mc.rback_bottom_right(),CBackc.front_upper_left());
  std::cout<< CBackc;
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  //3d box iterator
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "3d const box iterator" <<std::endl;
  std::cout<<std::endl;
  slip::GenericMultiComponent3d<slip::Color<double> > Cboxc(b3d.depth(),b3d.height(),b3d.width());
  std::copy(Mc.front_upper_left(b3d),Mc.back_bottom_right(b3d),Cboxc.front_upper_left());
  std::cout<< Cboxc;
  std::cout<<std::endl;
  std::cout<<std::endl;
  slip::GenericMultiComponent3d<slip::Color<double> > CboxBackc(b3d.depth(),b3d.height(),b3d.width());
  std::copy(Mc.rfront_upper_left(b3d),Mc.rback_bottom_right(b3d),CboxBackc.front_upper_left());
  std::cout<< CboxBackc;
  std::cout<<std::endl;
  std::cout<<std::endl;


  //****************************** Component ***********************************//
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Component Simple Iterator" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  slip::Matrix3d<double> Mat2(5,4,3);
  for(int i=0; i<3; i++)
    {
      std::transform(Mat.begin(),Mat.end(),Mat2.begin(),std::bind2nd(inc<double>(),i+1));
      M.fill(i,Mat2.begin(),Mat2.end());
    }

  std::cout<<M;
  std::cout<<std::endl;
  std::cout<<std::endl;

  //Global :
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Global Component iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.begin(1),M.end(1),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  //slice : 
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "slice Component iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_begin(2,2,2),M.slice_end(2,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_rbegin(2,2,2),M.slice_rend(2,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  //slice range :
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "slice component range iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_begin(2,2,2,rk),M.slice_end(2,2,2,rk),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_rbegin(2,2,2,rk),M.slice_rend(2,2,2,rk),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
 
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "slice component range iterator + 1" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_begin(2,2,2,rkd),M.slice_end(2,2,2,rkd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.slice_rbegin(2,2,2,rkd),M.slice_rend(2,2,2,rkd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row : 
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "component row iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_begin(0,2,2),M.row_end(0,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(0,2,2),M.row_rend(0,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row range :
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "component row range iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_begin(0,2,2,ri),M.row_end(0,2,2,ri),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(0,2,2,ri),M.row_rend(0,2,2,ri),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "component row range iterator + 1" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_begin(0,2,2,rid),M.row_end(0,2,2,rid),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.row_rbegin(0,2,2,rid),M.row_rend(0,2,2,rid),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //col : 
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "component col iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_begin(1,2,2),M.col_end(1,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(1,2,2),M.col_rend(1,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col range :
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "component col range iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_begin(1,2,2,rj),M.col_end(1,2,2,rj),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(1,2,2,rj),M.col_rend(1,2,2,rj),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "component col range iterator + 1" <<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_begin(1,2,2,rjd),M.col_end(1,2,2,rjd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(M.col_rbegin(1,2,2,rjd),M.col_rend(1,2,2,rjd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  // plane iterators

  //global plane 1d
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout<< "component global plane iterator" <<std::endl;
   std::cout<<std::endl;
   std::copy(M.plane_begin(1,2),M.plane_end(1,2),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;
   std::copy(M.plane_rbegin(1,2),M.plane_rend(1,2),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;

 //   //global plane 2d
//    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//    std::cout<< "component global plane iterator 2d" <<std::endl;
//    std::cout<<std::endl;
//    std::copy(M.plane_upper_left(1,slip::KI_PLANE,2),M.plane_bottom_right(1,slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
//    std::cout<<std::endl;
//    std::cout<<std::endl;
//    std::copy(M.plane_rupper_left(),M.plane_rbottom_right(),std::ostream_iterator<double>(std::cout," "));
//    std::cout<<std::endl;
//    std::cout<<std::endl;

//****************************** Const Component ***********************************//

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Const Component Simple Iterator" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  slip::GenericMultiComponent3d<slip::Color<double> > const Mcc(M);
//Global :
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "Global const Component iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.begin(1),Mcc.end(1),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //slice : 
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "slice const Component iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.slice_begin(2,2,2),Mcc.slice_end(2,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.slice_rbegin(2,2,2),Mcc.slice_rend(2,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  //slice range :
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "slice const component range iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.slice_begin(2,2,2,rk),Mcc.slice_end(2,2,2,rk),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.slice_rbegin(2,2,2,rk),Mcc.slice_rend(2,2,2,rk),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
 
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "slice const component range iterator + 1" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.slice_begin(2,2,2,rkd),Mcc.slice_end(2,2,2,rkd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.slice_rbegin(2,2,2,rkd),Mcc.slice_rend(2,2,2,rkd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row : 
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "const component row iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.row_begin(0,2,2),Mcc.row_end(0,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.row_rbegin(0,2,2),Mcc.row_rend(0,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //row range :
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "const component row range iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.row_begin(0,2,2,ri),Mcc.row_end(0,2,2,ri),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.row_rbegin(0,2,2,ri),Mcc.row_rend(0,2,2,ri),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "const component row range iterator + 1" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.row_begin(0,2,2,rid),Mcc.row_end(0,2,2,rid),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.row_rbegin(0,2,2,rid),Mcc.row_rend(0,2,2,rid),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

 //col : 
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "const component col iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.col_begin(1,2,2),Mcc.col_end(1,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.col_rbegin(1,2,2),Mcc.col_rend(1,2,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //col range :
  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "const component col range iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.col_begin(1,2,2,rj),Mcc.col_end(1,2,2,rj),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.col_rbegin(1,2,2,rj),Mcc.col_rend(1,2,2,rj),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout << std::setfill(' ') << std::setw(20) << ' ' ;
  std::cout<< "const component col range iterator + 1" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.col_begin(1,2,2,rjd),Mcc.col_end(1,2,2,rjd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.col_rbegin(1,2,2,rjd),Mcc.col_rend(1,2,2,rjd),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //plane iterators

  //global plane 1d
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<< "const component global plane iterator" <<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.plane_begin(1,2),Mcc.plane_end(1,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::copy(Mcc.plane_rbegin(1,2),Mcc.plane_rend(1,2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;


  return 0;
}

