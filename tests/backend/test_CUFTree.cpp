

#include <iostream>
#include <iomanip>
#include <vector>

#include "CUFTree.hpp"
#include "Array2d.hpp"
#include "arithmetic_op.hpp"
#include "DPoint2d.hpp"
#include "neighborhood.hpp"

int main()
{

  slip::Array2d<int> A(5,4);
  A[0][0] = 1; A[0][1] = 1; A[0][2] = 20; A[0][3] = 20;
  A[1][0] = 1; A[1][1] = 1; A[1][2] = 20; A[1][3] = 20;
  A[2][0] = 2; A[2][1] = 2; A[2][2] = 2; A[2][3] = 1;
  A[3][0] = 2; A[3][1] = 15; A[3][2] = 10; A[3][3] = 10;
  A[4][0] = 5; A[4][1] = 15; A[4][2] = 10; A[4][3] = 10;
  
  std::cout<<"A = \n"<<A<<std::endl;
  
  typedef slip::Array2d<int>::iterator2d iterator2d;
 
  slip::Array2d<slip::CUFTree<int> > TreeTab(A.rows(),A.cols());

  std::size_t tags = 0;
  //tags the first point
  (TreeTab[0][0]).set_Element(tags++);
  slip::DPoint2d<int> d(0,0);
  std::vector<iterator2d> neighborsPrev(5);
  typedef std::vector<iterator2d>::iterator it_prev;
  slip::Array2d<slip::CUFTree<int> >::iterator2d it_tab = TreeTab.upper_left();
  slip::Array2d<slip::CUFTree<int> >::iterator2d tab_up = TreeTab.upper_left();
  iterator2d in_upper_left = A.upper_left();
  iterator2d in_bottom_right = A.bottom_right();
  
  slip::Prev4C Prev;

  for(; in_upper_left != in_bottom_right; ++in_upper_left, ++it_tab)
    {
      bool ccfound = false;
      //get previous points
      Prev(in_upper_left, neighborsPrev);
      it_prev prev_first = neighborsPrev.begin();
      it_prev prev_last  = neighborsPrev.end();
      //search for a connected components
      //if a connected component is found merge the union find trees
      for(; prev_first != prev_last; ++prev_first)
	{
	  if(*in_upper_left == **prev_first)
	    {
	      d[0] = (*prev_first).i();
	      d[1] = (*prev_first).j();
	      (*it_tab).merge(&(tab_up[d]));
	      ccfound = true;
	    }
	}
      //if no connected component have been found
      //add a new tag to the current union find tree
      if (!ccfound)
	{
	  (*it_tab).set_Element(tags++);
	}
    } 
 
  //write the tags image
  slip::Array2d<int> ConnectedComponents(A.rows(),A.cols());
  iterator2d out_upper_left = ConnectedComponents.upper_left();
  iterator2d out_bottom_right = ConnectedComponents.bottom_right();
  
  slip::Array2d<slip::CUFTree<int> >::iterator tab_first = TreeTab.begin();
  for(; out_upper_left != out_bottom_right; ++out_upper_left, ++tab_first)
    {
      *out_upper_left = static_cast<int>((*tab_first).get_Element());
    } 
  

  std::cout<<" ConnectedComponents =\n"<< ConnectedComponents<<std::endl;
    return 0;
}
