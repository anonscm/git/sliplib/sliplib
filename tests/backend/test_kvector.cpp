#include "KVector.hpp"
#include "Range.hpp"
#include "arithmetic_op.hpp"

#include <algorithm>
#include <iterator>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <numeric>
#include <cmath>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

//foncion unaire qui affiche une valeur
template<class T> struct print : public std::unary_function<T,void>
{
  print(std::ostream& out):os(out),count(0){}
  void operator() (T x){os<<x<<" ";++count;}
  std::ostream& os;
  int count;
};

double C()
{return -4.0;}

template<class T> struct test_func : public std::unary_function<T,T>
{
  test_func(){}
  T operator() (T x){return x + 2.2;}
};

int main(){

  //----------------------------
  //fill
  //----------------------------
  slip::kvector<int,8> B = {8,7,6,5,3,3,2,1};
  std::cout<<B<<std::endl;
  std::cout<<"name = "<<B.name()<<std::endl;
  
  slip::kvector<int,8> B2;
  B2.fill(4);
  std::cout<<B2<<std::endl;
  int tabi[] = {1,2,3,4,5,6};
  slip::kvector<int,6> B3;
  B3.fill(tabi);
  std::cout<<B3<<std::endl;
  
  slip::kvector<float,6> B4;
  B4.fill(tabi, tabi+6);
  std::cout<<B4<<std::endl;
   
 
  slip::kvector<int,5> B5;
  B5.fill(tabi, tabi+5);
  std::cout<<B5<<std::endl;

  slip::kvector<float,6> MM;
  MM.fill(5.5);
  slip::kvector<float,6> MM2;
  MM2.fill(2.2);
  
  
  //----------------------------
  
  
  //----------------------------
  //Accessors
  //----------------------------
  std::cout<<"empty : "<<B.empty()<<" size = "<<B.size()<<" max_size = "<<B.max_size()<<std::endl;
  //----------------------------

  //----------------------------
  //Swap
  //----------------------------
  B.swap(B2);
  std::cout<<B<<std::endl;
  std::cout<<B2<<std::endl;
  //----------------------------  

  //----------------------------
  //iterators
  //----------------------------
 //  std::cout<<std::endl;
//   std::cout<<std::endl;
//   std::cout << "Iterators" << std::endl;
//   std::cout << "B2 : " << std::endl;
//   std::copy(B2.begin(),B2.end(),std::ostream_iterator<int>(std::cout," "));
//   std::cout<<std::endl;
//   std::copy(B2.rbegin(),B2.rend(),std::ostream_iterator<int>(std::cout," "));
//   std::cout<<std::endl;

//   std::transform(B2.rbegin(),B2.rend(),B2.begin(),B.begin(),std::plus<int>());
  
//   std::cout<<B<<std::endl;

//   std::cout<<B3<<std::endl;
//   slip::Range<int> range(1,5,2);
//   std::copy(B3.begin(range),B3.end(range),std::ostream_iterator<double>(std::cout," "));
//   std::cout<<std::endl;
//   std::copy(B3.rbegin(range),B3.rend(range),std::ostream_iterator<double>(std::cout," "));
//   std::cout<<std::endl;
  
//   slip::Range<int> range2(0,5,3);
//   std::copy(B3.begin(range2),B3.end(range2),std::ostream_iterator<double>(std::cout," "));
//   std::cout<<std::endl;
//   std::copy(B3.rbegin(range2),B3.rend(range2),std::ostream_iterator<double>(std::cout," "));
//   std::cout<<std::endl;
//   //----------------------------  
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Global Iterators : " << std::endl;
  std::copy(B2.begin(),B2.end(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::copy(B2.rbegin(),B2.rend(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;

  slip::kvector<int,8> const B2c(B2);
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Global Const Iterators : " << std::endl;
  std::copy(B2c.begin(),B2c.end(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::copy(B2c.rbegin(),B2c.rend(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Transform : B = B2 + Reverse(B2)" << std::endl;
  std::transform(B2.rbegin(),B2.rend(),B2.begin(),B.begin(),std::plus<int>());
  
  std::cout<<B<<std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "B3 : " << std::endl;
  std::cout<<B3<<std::endl;
  std::cout<<std::endl;
  std::cout << "Range1  :" << std::endl;
  slip::Range<int> range(1,5,2);
  std::cout<<range << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Range Iterators : " << std::endl;
  std::copy(B3.begin(range),B3.end(range),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(B3.rbegin(range),B3.rend(range),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  slip::kvector<int,6> const B3c(B3);
  std::cout << "Range Const Iterators : " << std::endl;
  std::copy(B3c.begin(range),B3c.end(range),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(B3c.rbegin(range),B3c.rend(range),std::ostream_iterator<double>(std::cout," "));

  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Range2  :" << std::endl;
  slip::Range<int> range2(0,5,3);
  std::cout<<range2 << std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Range Iterators : " << std::endl;
  std::copy(B3.begin(range2),B3.end(range2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(B3.rbegin(range2),B3.rend(range2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout << "Range Const Iterators : " << std::endl;
  std::copy(B3c.begin(range2),B3c.end(range2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::copy(B3c.rbegin(range2),B3c.rend(range2),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  //---------------------------- 

  //----------------------------
  //comparison operators
  //----------------------------
  std::cout<<" B == B2 "<<(B == B2)<<std::endl;
  std::cout<<" B != B2 "<<(B != B2)<<std::endl;
  std::cout<<" B < B2  "<<(B < B2)<<std::endl;
  std::cout<<" B > B2  "<<(B > B2)<<std::endl;
  std::cout<<" B <= B2 "<<(B <= B2)<<std::endl;
  std::cout<<" B >= B2 "<<(B >= B2)<<std::endl;
  //----------------------------  


  //----------------------------
  //elements access operators
  //----------------------------
  B[2] = 3;
  B[3] = 7;
  std::cout<<B<<std::endl;
  B(2) = 7;
  B(3) = 3;
  std::cout<<B<<std::endl;

  for(std::size_t i = 0; i < B.size(); ++i)
    {
      std::cout<<B[i]<<" ";
    }
  std::cout<<std::endl;
  //----------------------------  


  //------------------------------
   //Artithmetic an mathematical operators
   //------------------------------
  std::cout<<"MM  = "<<MM<<std::endl;
  std::cout<<"MM2 = "<<MM2<<std::endl;
 
  MM += MM2;
  std::cout<<MM<<std::endl;
    
  MM -= MM2;
    std::cout<<MM<<std::endl;

    MM *= MM2;
    std::cout<<MM<<std::endl;

    MM /= MM2;
    std::cout<<MM<<std::endl;

    slip::kvector<float,6> MM3;
    MM3 = MM + MM2;
    std::cout<<MM<<"+"<<MM2<<" = "<<MM3<<std::endl;

    MM3 = MM - MM2;
    std::cout<<MM<<"-"<<MM2<<" = "<<MM3<<std::endl;

    MM3 = MM * MM2;
    std::cout<<MM<<"*"<<MM2<<" = "<<MM3<<std::endl;

    MM3 = MM / MM2;
    std::cout<<MM<<"/"<<MM2<<" = "<<MM3<<std::endl;

    std::cout<<"- MM3 = "<<-MM3<<std::endl;


    std::cout<<"MM2 = "<<std::endl;
    std::cout<<MM2<<std::endl;
    MM2+=1.1;
    std::cout<<MM2<<std::endl;
    MM2-=1.1;
    std::cout<<MM2<<std::endl;
    MM2*=1.1;
    std::cout<<MM2<<std::endl;
    MM2/=1.1;
    std::cout<<MM2<<std::endl;
     
    std::cout<<B4.min()<<std::endl;
    std::cout<<B4.max()<<std::endl;
    std::cout<<B4.sum()<<std::endl;
    
    std::cout<<min(B4)<<std::endl;
    std::cout<<max(B4)<<std::endl;


    std::cout<<B4.Euclidean_norm()<<std::endl;
    std::cout<<B4.L2_norm()<<std::endl;
    std::cout<<B4.L1_norm()<<std::endl;
    std::cout<<B4.L22_norm()<<std::endl;
    std::cout<<B4.infinite_norm()<<std::endl;
    
    slip::kvector<float,6> MM5;
    MM5 = B4 + B4;
    std::cout<<MM5<<std::endl;
    std::cout<<MM5+2.0f<<std::endl;
    std::cout<<(2.0f+MM5)<<std::endl;
    
    std::cout<<MM5<<std::endl;
    std::cout<<MM5-2.0f<<std::endl;
    std::cout<<(2.0f-MM5)<<std::endl;
     
    std::cout<<MM5<<std::endl;
    std::cout<<MM5*2.0f<<std::endl;
    std::cout<<(2.0f*MM5)<<std::endl;
    
    std::cout<<MM5<<std::endl;
    std::cout<<MM5/2.0f<<std::endl;
    //std::cout<<(2.0/MM5)<<std::endl;
    


    std::cout<<abs(-B4)<<std::endl;
    std::cout<<sqrt(B4)<<std::endl;
    std::cout<<cos(B4)<<std::endl;
    std::cout<<acos(B4)<<std::endl;
    std::cout<<sin(B4)<<std::endl;
    std::cout<<asin(B4)<<std::endl;
    std::cout<<tan(B4)<<std::endl;
    std::cout<<atan(B4)<<std::endl;
    std::cout<<exp(B4)<<std::endl;
    std::cout<<log(B4)<<std::endl;
    std::cout<<cosh(B4)<<std::endl;
    std::cout<<sinh(B4)<<std::endl;
    std::cout<<tanh(B4)<<std::endl;
    std::cout<<log10(B4)<<std::endl;
    
    B4.apply(std::sqrt);
    std::cout<<B4<<std::endl;
    //----------------------------------

    std::cout<<MM5<<std::endl;
    std::cout<<B4<<std::endl;
    std::cout<<"dot = "<<MM5.dot<float>(B4)<<std::endl;
     
    //----------------------------------
    // complex test
    //---------------------------------
    slip::kvector<std::complex<float>,6> MMc;
    MMc = std::complex<float>(5.5f,4.4f);
    std::cout<<MMc<<std::endl;
  
    slip::kvector<std::complex<float>,6> MM2c;
    MM2c = std::complex<float>(2.2f,3.3f);
    std::cout<<MM2c<<std::endl;
    

    //------------------------------
   //Artithmetic an mathematical operators
   //------------------------------
    MMc += MM2c;
    std::cout<<MMc<<std::endl;
    
    MMc -= MM2c;
    std::cout<<MMc<<std::endl;

    MMc *= MM2c;
    std::cout<<MMc<<std::endl;

    MMc /= MM2c;
    std::cout<<MMc<<std::endl;

    slip::kvector<std::complex<float>,6 > MM3c;
    MM3c = MMc + MM2c;
    std::cout<<MMc<<"+"<<MM2c<<" = "<<MM3c<<std::endl;

    MM3c = MMc - MM2c;
    std::cout<<MMc<<"-"<<MM2c<<" = "<<MM3c<<std::endl;

    MM3c = MMc * MM2c;
    std::cout<<MMc<<"*"<<MM2c<<" = "<<MM3c<<std::endl;

    MM3c = MMc / MM2c;
    std::cout<<MMc<<"/"<<MM2c<<" = "<<MM3c<<std::endl;

    std::cout<<"- MM3c = "<<-MM3c<<std::endl;


    std::cout<<"MM2c = "<<std::endl;
    std::cout<<MM2c<<std::endl;
    MM2c+=std::complex<float>(1.1f,2.2f);
    std::cout<<MM2c<<std::endl;
    MM2c-=std::complex<float>(1.1f,2.2f);
    std::cout<<MM2c<<std::endl;
    MM2c*=std::complex<float>(1.1f,2.2f);
    std::cout<<MM2c<<std::endl;
    MM2c/=std::complex<float>(1.1f,2.2f);
    std::cout<<MM2c<<std::endl;

    slip::kvector<std::complex<float>,5 > V4c;
    slip::iota(V4c.begin(),V4c.end(),
	       std::complex<float>(1.0f,1.0f),
	       std::complex<float>(1.0f,2.0f));
     
    std::cout<<V4c.min()<<std::endl;
    std::cout<<V4c.max()<<std::endl;
    std::cout<<V4c.sum()<<std::endl;
    
    std::cout<<min(V4c)<<std::endl;
    std::cout<<max(V4c)<<std::endl;

     std::cout<<V4c.Euclidean_norm()<<std::endl;
     std::cout<<V4c.L2_norm()<<std::endl;
     std::cout<<V4c.L1_norm()<<std::endl;
     std::cout<<V4c.L22_norm()<<std::endl;
     std::cout<<V4c.infinite_norm()<<std::endl;
    
    
     slip::kvector<std::complex<float>,5 > MM5c;
    MM5c = V4c + V4c;
    std::cout<<MM5c<<std::endl;
    std::cout<<MM5c+std::complex<float>(2.0f,1.0f)<<std::endl;
    std::cout<<(std::complex<float>(2.0f,1.0f)+MM5c)<<std::endl;
    
    std::cout<<MM5c<<std::endl;
    std::cout<<MM5c-std::complex<float>(2.0f,1.0f)<<std::endl;
    std::cout<<(std::complex<float>(2.0f,1.0f)-MM5c)<<std::endl;
     
    std::cout<<MM5c<<std::endl;
    std::cout<<MM5c*std::complex<float>(2.0f,1.0f)<<std::endl;
    std::cout<<(std::complex<float>(2.0f,1.0f)*MM5c)<<std::endl;
    
    std::cout<<MM5c<<std::endl;
    std::cout<<MM5c/std::complex<float>(2.0f,1.0f)<<std::endl;
    //std::cout<<(std::complex<float>(2.0f,1.0f)/MM5c)<<std::endl;
    


    std::cout<<abs(-V4c)<<std::endl;
    std::cout<<sqrt(V4c)<<std::endl;
    std::cout<<cos(V4c)<<std::endl;
    //std::cout<<acos(V4c)<<std::endl;
    std::cout<<sin(V4c)<<std::endl;
    //std::cout<<asin(V4c)<<std::endl;
    std::cout<<tan(V4c)<<std::endl;
    //std::cout<<atan(V4c)<<std::endl;
    std::cout<<exp(V4c)<<std::endl;
    std::cout<<log(V4c)<<std::endl;
    std::cout<<cosh(V4c)<<std::endl;
    std::cout<<sinh(V4c)<<std::endl;
    std::cout<<tanh(V4c)<<std::endl;
    std::cout<<log10(V4c)<<std::endl;
    
    V4c.apply(std::sqrt);
    std::cout<<V4c<<std::endl;

    //serialization
     // create and open a character archive for output
    std::ofstream ofs("kvector.txt");
    slip::kvector<float,10> b;
    for(std::size_t i = 0; i < b.size(); ++i)
      {
	b[i] = static_cast<float>(i);
      }
    
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<b;
    }//to close archive
    std::ifstream ifs("kvector.txt");
    slip::kvector<float,10> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("kvector.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<b;
    }//to close archive

    std::ifstream ifsb("kvector.bin");
    slip::kvector<float,10> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;
    

    slip::kvector<std::complex<float>,10> bc;
    slip::iota(bc.begin(),bc.end(),std::complex<float>(1.0f,2.0f),
	       std::complex<float>(1.0f,1.0f));
    std::ofstream ofsc("kvectorc.txt");
     {
       
       boost::archive::text_oarchive oac(ofsc);
       oac<<bc;
     }//to close archive
    std::ifstream ifsc("kvectorc.txt");
    slip::kvector<std::complex<float>,10> new_pc;
    {
    boost::archive::text_iarchive iac(ifsc);
    iac>>new_pc;
    }//to close archive
   


   return EXIT_SUCCESS;

}
