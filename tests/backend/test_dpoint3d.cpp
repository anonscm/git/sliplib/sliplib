#include<iostream>
#include <fstream>
#include<iterator>

#include "DPoint3d.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


int main()
{

  slip::DPoint3d<int> p;
  std::cout<<p<<std::endl;
   std::cout<<"name = "<<p.name()<<std::endl;
 
  
  slip::DPoint3d<float> pf(2.3,4.5,6.7);
  std::cout<<pf<<std::endl;
 
  pf.dx1(2.4);
  pf.dx2(4.6);
  pf.dx3(6.8);
  std::cout<<pf<<std::endl;

  slip::DPoint3d<float> pf2 = pf;
  std::cout<<pf2<<std::endl;
  
  slip::DPoint3d<float> pf3;
  pf3 = pf;
  std::cout<<pf3<<std::endl;
    
  std::cout<<pf3[0]<<" "<<pf3[1]<<" "<<pf3[2]<<std::endl;
  std::cout<<pf3.dx1()<<" "<<pf3.dx2()<<" "<<pf3.dx3()<<std::endl;
  
  std::cout<<"dim = "<<pf3.dim()<<std::endl;
  std::cout<<"size = "<<pf3.size()<<std::endl;
  std::cout<<"empty = "<<pf3.empty()<<std::endl;

  slip::DPoint3d<float> pf4(3.0,4.0,5.0);
  pf4.swap(pf3);
  std::cout<<pf3<<std::endl;
  std::cout<<pf4<<std::endl;
 
  pf4[0] = 6.0;
  pf4[1] = 7.0;
  pf4[2] = 8.0;
  
  std::cout<<pf4<<std::endl;
 
  pf4.dx1() = 8.0;
  pf4.dx2() = 9.0;
  pf4.dx3() = 10.0;
  
  std::cout<<pf4<<std::endl;
 
 
  // pf4[3] = 6.0;
  std::cout<<-pf4<<std::endl;
  
  std::cout<<"pf3 == pf4 :"<<(pf3==pf4)<<std::endl;
  std::cout<<"pf3 != pf4 :"<<(pf3!=pf4)<<std::endl;

  float tab[] = {1.0,2.0,3.0};
  slip::DPoint3d<float> pf5(tab);
  std::cout<<pf5<<std::endl;

  std::cout<<(pf3 + pf4)<<std::endl;
  std::cout<<(pf3 - pf4)<<std::endl;
  std::cout<<(pf3 += pf4)<<std::endl;
  std::cout<<(pf3 -= pf4)<<std::endl;

  pf5.set_coord(5.0,6.0,7.0);
  std::cout<<pf5<<std::endl;

  //
  //serialization
  //

// create and open a character archive for output
    std::ofstream ofs("dpoint3d.txt");
    slip::DPoint3d<double> ps(1.3,2.4,4.5);
        
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<ps;
    }//to close archive
    std::ifstream ifs("dpoint3d.txt");
    slip::DPoint3d<double> new_ps;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_ps;
    }//to close archive
    std::cout<<"new_ps\n"<<new_ps<<std::endl;

    std::ofstream ofsb("dpoint3d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<ps;
    }//to close archive

    std::ifstream ifsb("dpoint3d.bin");
    slip::DPoint3d<double> new_psb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_psb;
    }//to close archive
    std::cout<<"new_psb\n"<<new_psb<<std::endl;

return 0;
}
