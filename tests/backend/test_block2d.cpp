#include "Block2d.hpp"
#include "arithmetic_op.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <complex>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cmath>
#include <vector>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

int main()
{

  //----------------------------
  //fill
  //----------------------------

  slip::block2d<double,4,3> M;
  std::cout<<"name = "<<M.name()<<" dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<"rows = "<<M.rows()<<" cols = "<<M.cols()<<" columns = "<<M.columns()<<std::endl;
  M.fill(3.7);
  std::cout<<M<<std::endl;


  double d[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0};
  
  slip::block2d<double,4,3> M2;
  M2.fill(d);
  std::cout<<M2<<std::endl;
  std::cout<<std::endl;
  std::vector<int> V(12);
  for(std::size_t i = 0; i < 12; ++i)
    {
      V[i] = i;
    }
  slip::block2d<double,4,3> M3;
  M3.fill(V.begin(),V.end());
  std::cout<<M3<<std::endl;

 
  slip::block2d<int,2,3> M4 = {1,2,3,4,5,6};
  std::cout<<M4<<std::endl;

  slip::block2d<int,2,2> M5;
  M5 = 5;
  std::cout<<M5<<std::endl;
  //----------------------------
   //Swap
   //----------------------------
   M2.swap(M3);
  
   std::cout<<M2<<std::endl;
   std::cout<<M3<<std::endl;
   //----------------------------  
  
  
  //----------------------------
  //iterators
  //----------------------------
   slip::block2d<double,4,3> M8;
   M8 = 4.4;

   std::copy(M3.begin(),M3.end(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(M3.rbegin(),M3.rend(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl<<std::endl;
   
   std::copy(M3.row_begin(1),M3.row_end(1),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;

    std::copy(M3.col_begin(1),M3.col_end(1),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;

   std::transform(M3.rbegin(),M3.rend(),M8.begin(),M2.begin(),std::plus<double>());
  
   std::cout<<M2<<std::endl;
   
   std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_begin(2),std::plus<double>());
   std::cout<<M2<<std::endl;
   
   std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_begin(2),std::plus<double>());
   std::cout<<M2<<std::endl;

   std::cout<<"col, row, reverse "<<std::endl;
   std::copy(M2.row_rbegin(1),M2.row_rend(1),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;
   std::copy(M2.col_rbegin(1),M2.col_rend(1),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;

    std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_rbegin(2),std::plus<double>());
    std::cout<<M2<<std::endl;

   std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_rbegin(2),std::plus<double>());
   std::cout<<M2<<std::endl;

   
   std::generate(M2.upper_left(),M2.bottom_right(),std::rand);
   std::cout<<M2<<std::endl;

   slip::iterator2d_box<slip::block2d<double,4,3> > it;
   slip::Box2d<int> box1(1,1,2,2);
   
   for(it = M2.upper_left(box1); it != M2.bottom_right(box1); ++it)
     {
       std::cout<<*it<<" ";
     }
   std::cout<<std::endl;
  
   std::fill(M2.upper_left(box1),M2.bottom_right(box1),2.0);
   std::cout<<M2<<std::endl;

    std::cout<<"iterator2d_box test..."<<std::endl;
   std::cout<<M2<<std::endl;
   std::cout<<"bottom_right() and upper_left() test "<<std::endl;
   std::copy(M2.upper_left(),M2.bottom_right(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(M2.rupper_left(),M2.rbottom_right(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;

   std::cout<<"bottom_right(box) and upper_left(box) box(2,1,4,2) test "<<std::endl;
   slip::Box2d<int> box(2,1,4,2);
   std::copy(M2.upper_left(box),M2.bottom_right(box),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(M2.rupper_left(box),M2.rbottom_right(box),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   
   slip::block2d<double,3,2> Mask;
   double k = 1.0;
   for(std::size_t i = 0; i < Mask.dim1(); ++i)
     for(std::size_t j = 0; j < Mask.dim2(); ++j)
       {
	 Mask[i][j] = k;
	 k += 1.0;
       }
   std::copy(Mask.upper_left(),Mask.bottom_right(),
	     M2.upper_left(box));
   std::cout<<M2<<std::endl;
  
   std::copy(Mask.upper_left(),Mask.bottom_right(),
	     M2.rupper_left(box));
   std::cout<<M2<<std::endl;
   
   std::transform(M2.upper_left(box),M2.bottom_right(box),Mask.upper_left(),M2.upper_left(box),std::plus<double>());
   std::cout<<M2<<std::endl;
   
   std::transform(M2.upper_left(box),M2.bottom_right(box),Mask.rupper_left(),M2.upper_left(box),std::plus<double>());
   std::cout<<M2<<std::endl;
 
   slip::block2d<int,10,10> Mrange;
   int ind = 0;
   for(std::size_t i = 0; i < Mrange.dim1(); ++i)
     for(std::size_t j = 0; j < Mrange.dim2(); ++j)
       {
	 Mrange[i][j] = ind;
	 ind += 1;
       }
   std::cout<<Mrange<<std::endl;
   std::cout<<"row_range_iterator ..."<<std::endl;
   //slip::Range<int> range(2,7,2);
   slip::Range<int> range(0,(Mrange.dim2() - 1),2);
   std::copy(Mrange.row_begin(3,range),Mrange.row_end(3,range),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(Mrange.row_rbegin(3,range),Mrange.row_rend(3,range),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;

   std::cout<<"col_range_iterator ..."<<std::endl;
   //slip::Range<int> range2(2,7,2);
   slip::Range<int> range2(0,(Mrange.dim1() - 1),2);
   std::copy(Mrange.col_begin(2,range2),Mrange.col_end(2,range2),std::ostream_iterator<double>(std::cout,"\n"));
   std::cout<<std::endl;
   std::copy(Mrange.col_rbegin(2,range2),Mrange.col_rend(2,range2),std::ostream_iterator<double>(std::cout,"\n"));
   std::cout<<std::endl;

   //std::copy(Mrange.row_begin(4,range),Mrange.row_end(4,range),Mrange.row_begin(8,range));

  std::copy(Mrange.col_begin(4,range2),Mrange.col_end(4,range2),Mrange.col_begin(2,range));

   std::cout<<Mrange<<std::endl;
   
   std::copy(Mrange.upper_left(range,range2),Mrange.bottom_right(range,range2),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   // std::copy(Mrange.rupper_left(range,range2),Mrange.rbottom_right(range,range2),std::ostream_iterator<double>(std::cout," "));
//    std::cout<<std::endl;
   std::copy(Mrange.upper_left(range),Mrange.bottom_right(range),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   //  std::copy(Mrange.rupper_left(range),Mrange.rbottom_right(range),std::ostream_iterator<double>(std::cout," "));
//    std::cout<<std::endl;
   //----------------------------  

   //----------------------------
  //comparison operators
  //----------------------------
  std::cout<<" M3 == M2 "<<(M3 == M2)<<std::endl;
  std::cout<<" M3 != M2 "<<(M3 != M2)<<std::endl;
  std::cout<<" M3 < M2  "<<(M3 < M2)<<std::endl;
  std::cout<<" M3 > M2  "<<(M3 > M2)<<std::endl;
  std::cout<<" M3 <= M2 "<<(M3 <= M2)<<std::endl;
  std::cout<<" M3 >= M2 "<<(M3 >= M2)<<std::endl;
  //----------------------------  

  //----------------------------
  //elements access operators
  //----------------------------
  M2(1,2) = 134;
  M2[2][1] = 154;
  std::cout<<M2<<std::endl;

  for(std::size_t i = 0; i < M2.dim1(); ++i)
    {
      for(std::size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2[i][j]<<" ";  
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;

   for(std::size_t i = 0; i < M2.dim1(); ++i)
    {
      for(std::size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2(i,j)<<" ";  
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
  slip::block2d<double,10,9> Arange;
   double val = 0.0;
    for(size_t i = 0; i < Arange.dim1(); ++i)
    {
      for(size_t j = 0; j < Arange.dim2(); ++j)
	{
	  Arange(slip::Point2d<std::size_t>(i,j)) = val;
	  val = val + 1.0;
	}
    }

    for(size_t i = 0; i < Arange.rows(); ++i)
    {
      for(size_t j = 0; j < Arange.cols(); ++j)
	{
	  std::cout<<Arange(slip::Point2d<std::size_t>(i,j))<<" ";
	}
      std::cout<<std::endl;
    }
    std::cout<<std::endl;
  //----------------------------  

// create and open a character archive for output
    std::ofstream ofs("block2d.txt");
    slip::block2d<float,3,4> b;
    slip::iota(b.begin(),b.end(),1.0f);
    
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<b;
    }//to close archive
    std::ifstream ifs("block2d.txt");
    slip::block2d<float,3,4> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("block2d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<b;
    }//to close archive

    std::ifstream ifsb("block2d.bin");
    slip::block2d<float,3,4> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;
    

    slip::block2d<std::complex<float>,3,4 > bc;
    slip::iota(bc.begin(),bc.end(),std::complex<float>(1.0f,2.0f),
    	       std::complex<float>(1.0f,1.0f));
    std::ofstream ofsc("block2dc.txt");
     {
       
       boost::archive::text_oarchive oac(ofsc);
       oac<<bc;
     }//to close archive
    std::ifstream ifsc("block2dc.txt");
    slip::block2d<std::complex<float>,3,4> new_pc;
    {
    boost::archive::text_iarchive iac(ifsc);
    iac>>new_pc;
    }//to close archive
    std::cout<<"new_pc = \n"<<new_pc<<std::endl;

 return 0;
}
