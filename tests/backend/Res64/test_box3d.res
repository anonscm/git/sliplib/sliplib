[ (0,0,0),(0,0,0) ]
name  = Box3d
(1,1,1)
(2,3,4)
[ (1,1,1),(2,3,4) ]
[ (3,4,5),(6,7,8) ]
[ (3,4,5),(6,7,8) ]
[ (1,1,1),(2,3,4) ]
1
0
[ (1,1,1),(2,3,4) ] + (1,-1,-1) = [ (2,0,0),(3,2,3) ]
1
0
[ (2,3,4),(-5,-6,-7) ]
[ (2,0,0),(3,2,3) ]
0
1
[ (2,0,0),(3,2,3) ]
front_upper_left   = (2,0,0)
back_bottom_right = (3,2,3)
width = 4
height = 3
depth = 2
volume = 24
front_upper_left   = (7,7,7)
back_bottom_right = (13,13,13)
width = 7
height = 7
depth = 7
volume = 343
front_upper_left   = (7,7,7)
back_bottom_right = (13,13,13)
width = 7
height = 7
depth = 7
volume = 343
[ (1,2,3),(4,5,6) ]
[ (2,0,0),(3,2,3) ]
const upper_left   = (2,0,0)
const bottom_right = (3,2,3)
[ (7,7,7),(13,13,13) ]
const upper_left   = (7,7,7)
const bottom_right = (13,13,13)
[ (7,7,7),(13,13,13) ]
const upper_left   = (7,7,7)
const bottom_right = (13,13,13)
____________________________________________________________
                    is-consistent() tests
____________________________________________________________


the box : [ (0,0,0),(3,3,3) ]
is consistent? : 1
the box : [ (3,3,3),(0,0,0) ]
is consistent? : 0
