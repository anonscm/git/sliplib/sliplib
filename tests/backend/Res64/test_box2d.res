[ (0,0),(0,0) ]
name = Box2d
(1,1)
(2,3)
[ (1,1),(2,3) ]
[ (3,4),(5,6) ]
[ (3,4),(5,6) ]
[ (1,1),(2,3) ]
1
0
[ (1,1),(2,3) ] + (1,-1) = [ (2,0),(3,2) ]
1
0
[ (3,4),(-5,-6) ]
[ (2,0),(3,2) ]
0
1
[ (2,0),(3,2) ]
upper_left   = (2,0)
bottom_right = (3,2)
width = 3
height = 2
area = 6
upper_left   = (7,7)
bottom_right = (13,13)
width = 7
height = 7
area = 49
upper_left   = (7,7)
bottom_right = (13,13)
width = 7
height = 7
area = 49
upper_left   = (1,2)
bottom_right = (4,5)
width = 4
height = 4
area = 16
[ (2,0),(3,2) ]
const upper_left   = (2,0)
const bottom_right = (3,2)
[ (7,7),(13,13) ]
const upper_left   = (7,7)
const bottom_right = (13,13)
[ (7,7),(13,13) ]
const upper_left   = (7,7)
const bottom_right = (13,13)
____________________________________________________________
                    is-consistent() tests
____________________________________________________________


the box : [ (0,0),(3,3) ]
is consistent? : 1
the box : [ (0,3),(3,2) ]
is consistent? : 0
