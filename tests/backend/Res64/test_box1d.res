(0)
(1)
(2)
[ (1),(2) ]
[ (3),(4) ]
[ (3),(4) ]
[ (1),(2) ]
1
0
[ (1),(2) ] + (2) = [ (3),(4) ]
1
0
[ (3),(-6) ]
[ (3),(4) ]
0
1
[ (3),(4) ]
upper_left   = (3)
bottom_right = (4)
name = Box1d
width = 2
length = 2
[ (10),(3) ]
upper_left   = (10)
bottom_right = (3)
width = -6
length = -6
[ (7),(13) ]
upper_left   = (7)
bottom_right = (13)
width = 7
length = 7
[ (1),(4) ]
upper_left   = (1)
bottom_right = (4)
width = 4
length = 4
[ (3),(4) ]
const upper_left   = (3)
const bottom_right = (4)
[ (10),(3) ]
const upper_left   = (10)
const bottom_right = (3)
[ (7),(13) ]
const upper_left   = (7)
const bottom_right = (13)
____________________________________________________________
                    is-consistent() tests
____________________________________________________________


the box : [ (0),(3) ]
is consistent? : 1
the box : [ (3),(0) ]
is consistent? : 0
