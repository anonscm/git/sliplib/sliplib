#include <iostream>
#include <iomanip>
#include <fstream>

#include "Array2d.hpp"
#include "Color.hpp"
#include "Colormap.hpp"

#include "arithmetic_op.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

int main()
{
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Default constructor " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Colormap<int> C0;
   std::cout<<"C0 =\n "<<C0<<std::endl;
   std::cout<<"C0.name() = "<<C0.name()<<std::endl;
  std::cout<<"C0.size() = "<<C0.size()<<std::endl;
  std::cout<<"C0.empty() = "<<C0.empty()<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Constructor with n elements" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Colormap<int> C(4);
   std::cout<<"C(4) = \n"<<C<<std::endl;
   std::cout<<"C.size() = "<<C.size()<<std::endl;

 
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Constructor with n elements and an inital value" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Color<int> cl(20,60,128);
   std::cout<<"initial value = "<<cl<<std::endl;
   slip::Colormap<int> Ci(5,cl);
   std::cout<<"Ci = \n"<<Ci<<std::endl;
   std::cout<<"Ci.size() = "<<Ci.size()<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Random Colormap constructor" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Colormap<int> Crand(8,1,128);
  std::cout<<"Crand = \n"<<Crand<<std::endl;
  std::cout<<"Crand.name() = "<<Crand.name()<<std::endl;
  std::cout<<"Crand.size() = "<<Crand.size()<<std::endl;
  std::cout<<"Crand.empty() = "<<Crand.empty()<<std::endl;
 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Colormap range constructor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Colormap<int> Crange(8,Crand.begin(),Crand.end());
  std::cout<<"Crange = \n"<<Crange<<std::endl;
  

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Colormap copy constructor" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Colormap<int> Ccopy(Crand);
   std::cout<<"Ccopy = \n"<<Ccopy<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Colormap operator=val" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Colormap<int> Cequalval(8);
   Cequalval = slip::Color<int>(4,5,6);
   std::cout<<"Cequalval = \n"<<Cequalval<<std::endl;
  
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Colormap operator=" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Colormap<int> Cequal;
   Cequal = Crand;
   std::cout<<"Cequal = \n"<<Cequal<<std::endl;
    
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "self assignment" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   Cequal = Cequal;
   std::cout<<"Cequal = \n"<<Cequal<<std::endl;

   

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Colormap resize" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   Ccopy.resize(6);
   std::cout<<"Ccopy = \n"<<Ccopy<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Fill with a value" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Colormap<int> Cfillval(7);
   std::cout<<"value = "<<slip::Color<int>(1,2,3)<<std::endl;
   Cfillval.fill(slip::Color<int>(1,2,3));
   std::cout<<"Cfillval = \n"<<Cfillval<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Fill with a range" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Colormap<int> Cfillrange(8);
   Cfillrange.fill(Crand.begin(),Crand.end());
   std::cout<<"Cfillrange = \n"<<Cfillrange<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "convert colormap into a 2d array" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Array2d<int> Carray;
   Crand.to_array2d(Carray);
   std::cout<<"Crand = \n"<<Crand<<std::endl;
   std::cout<<"Carray = \n"<<Carray<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "copy a array2d into a colormap" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Colormap<int> CfromA(8);
   CfromA.from_array2d(Carray);
   std::cout<<"CfromA = \n"<<CfromA<<std::endl;
   
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "write colormap into an ASCII file" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   Crand.write("Crand.txt");

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "read colormap from an ASCII file" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Colormap<int> Crand2;
   Crand2.read("Crand.txt");
    std::cout<<"Crand2 = \n"<<Crand2<<std::endl;


    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "randomize colormap" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout<<"Ci = \n "<<Ci<<std::endl;
    Ci.randomize(1,64);
    std::cout<<"Ci randomize = \n "<<Ci<<std::endl;

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "Element access read" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    for(std::size_t i = 0; i < Crand.size(); ++i)
      {
	std::cout<<Crand[i]<<std::endl;
	std::cout<<Crand(i)<<std::endl;
      }
    std::cout<<std::endl;

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "Element access write" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout<<"Crand = \n"<<Crand<<std::endl;
    Crand[2] = slip::Color<int>(2,2,2);
    Crand(3) = slip::Color<int>(3,3,3);
    std::cout<<"Crand = \n"<<Crand<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "iterators" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::copy(Crand.begin(),Crand.end(),std::ostream_iterator<slip::Color<int> >(std::cout," "));
   std::cout<<std::endl;
 
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "reverse iterators" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::copy(Crand.rbegin(),Crand.rend(),std::ostream_iterator<slip::Color<int> >(std::cout," "));
   std::cout<<std::endl;


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "operators == and !=" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Crand = \n"<<Crand<<std::endl;
   std::cout<<"Ccopy = \n"<<Ccopy<<std::endl;
   std::cout << "Crand == Ccopy: "<<(Crand == Ccopy)<<std::endl;
   std::cout << "Crand != Ccopy: "<<(Crand != Ccopy)<<std::endl;

   slip::Colormap<int> Ccopy2(Ccopy);
   std::cout<<"Ccopy = \n"<<Ccopy<<std::endl;
   std::cout<<"Ccopy2 = \n"<<Ccopy2<<std::endl;
   std::cout << "Ccopy == Ccopy2: "<<(Ccopy == Ccopy2)<<std::endl;
   std::cout << "Ccopy != Ccopy2: "<<(Ccopy != Ccopy2)<<std::endl;
   
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "swap" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Crand = \n"<<Crand<<std::endl;
   std::cout<<"Ccopy = \n"<<Ccopy<<std::endl;
   Crand.swap(Ccopy);
   std::cout<<"Crand = \n"<<Crand<<std::endl;
   std::cout<<"Ccopy = \n"<<Ccopy<<std::endl;


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "text serialization" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   // create and open a character archive for output
   std::ofstream ofs("colormap.txt");
   slip::Colormap<double> cmap(8);
   cmap.randomize(0.0,1.0);
   
        
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<cmap;
    }//to close archive
    std::ifstream ifs("colormap.txt");
    slip::Colormap<double> new_cmap;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_cmap;
    }//to close archive
    std::cout<<"new_cmap\n"<<new_cmap<<std::endl;

    std::ofstream ofsb("colormap.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<cmap;
    }//to close archive

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "binary serialization" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::ifstream ifsb("colormap.bin");
    slip::Colormap<double> new_cmapb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_cmapb;
    }//to close archive
    std::cout<<"new_cmapb\n"<<new_cmapb<<std::endl;

  return 0;
}
