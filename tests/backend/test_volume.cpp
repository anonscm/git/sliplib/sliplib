#include "Volume.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <complex>
#include <vector>
#include "arithmetic_op.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
int main()
{
  //------------------------------
  //constructors
  //------------------------------

  slip::Volume<double> M;
  std::cout<<"name = "<<M.name()<<" dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" dim3 = "<<M.dim3()<<" size = "<<M.size()<<" slice_size = "<<M.slice_size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 
  

  

  slip::Volume<double> M2(4,3,2);
  std::cout<<"dim1 = "<<M2.dim1()<<" dim2 = "<<M2.dim2()<<" dim3 = "<<M2.dim3()<<" size = "<<M2.size()<<" slice_size = "<<M2.slice_size()<<" max_size = "<<M2.max_size()<<" empty = "<<M2.empty()<<std::endl;
  std::cout<<"slices = "<<M2.slices()<<" rows = "<<M2.rows()<<" cols = "<<M2.cols()<<" columns = "<<M2.columns()<<std::endl;
  std::cout<<M2<<std::endl; 
  

  slip::Volume<double> M3(4,3,2,4.4);
  std::cout<<"dim1 = "<<M3.dim1()<<" dim2 = "<<M3.dim2()<<" dim3 = "<<M3.dim3()<<" size = "<<M3.size()<<" slice_size = "<<M3.slice_size()<<" max_size = "<<M3.max_size()<<" empty = "<<M3.empty()<<std::endl; 
  std::cout<<M3<<std::endl; 
  

  double d[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0};
  slip::Volume<double> M4(2,2,3,d);
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" dim3 = "<<M4.dim3()<<" size = "<<M4.size()<<" slice_size = "<<M4.slice_size()<<" max_size = "<<M4.max_size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl; 


  std::vector<int> V(24);
  for(std::size_t i = 0; i < 24; ++i)
    V[i] = i;
  slip::Volume<int> M5(2,3,4,V.begin(),V.end());
  std::cout<<"dim1 = "<<M5.dim1()<<" dim2 = "<<M5.dim2()<<" dim3 = "<<M5.dim3()<<" size = "<<M5.size()<<" slice_size = "<<M5.slice_size()<<" max_size = "<<M5.max_size()<<" empty = "<<M5.empty()<<std::endl; 
  std::cout<<M5<<std::endl; 
  
  slip::Volume<double> M6 = M4;
  std::cout<<"dim1 = "<<M6.dim1()<<" dim2 = "<<M6.dim2()<<" dim3 = "<<M6.dim3()<<" size = "<<M6.size()<<" slice_size = "<<M6.slice_size()<<" max_size = "<<M6.max_size()<<" empty = "<<M6.empty()<<std::endl; 
  std::cout<<M6<<std::endl; 
  
  
 //------------------------------

 //------------------------------
 //resize
 //------------------------------

  M.resize(2,3,4);
  std::cout<<"dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" dim3 = "<<M.dim3()<<" size = "<<M.size()<<" slice_size = "<<M.slice_size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 
  
  //------------------------------


  //------------------------------
  //Affectation
  //------------------------------
  slip::Volume<double> M7;
  M7 = M;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" dim3 = "<<M7.dim3()<<" size = "<<M7.size()<<" slice_size = "<<M7.slice_size()<<" max_size = "<<M7.max_size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl; 
  M7 = M7;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" dim3 = "<<M7.dim3()<<" size = "<<M7.size()<<" slice_size = "<<M7.slice_size()<<" max_size = "<<M7.max_size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl; 
  
  M4 = 12.2;
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" dim3 = "<<M4.dim3()<<" size = "<<M4.size()<<" slice_size = "<<M4.slice_size()<<" max_size = "<<M4.max_size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl; 
  //------------------------------

  //------------------------------
  //Element access operators
  //------------------------------
  M2(3,2,1) = 2.1;
  M2[1][2][0] = 1.2;
  
  for(size_t k = 0; k < M2.dim1(); ++k)
    {
      for(size_t i = 0; i < M2.dim2(); ++i)
	{
	  for(size_t j = 0; j < M2.dim3(); ++j)
	    {
	      std::cout<<M2[k][i][j]<<" ";
	    }
	  std::cout<<std::endl;
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
  

   for(size_t k = 0; k < M2.dim1(); ++k)
    {
      for(size_t i = 0; i < M2.dim2(); ++i)
	{
	  for(size_t j = 0; j < M2.dim3(); ++j)
	    {
	      std::cout<<M2(k,i,j)<<" ";
	    }
	  std::cout<<std::endl;
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
  
  //------------------------------


  //------------------------------
  //swap
  //------------------------------
  M2.swap(M3);
  std::cout<<M2<<std::endl;
  std::cout<<std::endl;
  std::cout<<M3<<std::endl;
  //------------------------------


  //----------------------------
  //iterators
  //----------------------------
   slip::Volume<double> M8(4,3,2,2.2);
   std::copy(M3.begin(),M3.end(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::copy(M3.rbegin(),M3.rend(),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl<<std::endl;
   
   
   std::transform(M3.rbegin(),M3.rend(),M8.begin(),M2.begin(),std::plus<double>());
  
   std::cout<<M2<<std::endl;
   
   std::cout<<"slice iterator"<<std::endl;
   std::copy(M5.slice_begin(1,2),M5.slice_end(1,2),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<"reverse slice iterator"<<std::endl;
   std::copy(M5.slice_rbegin(1,2),M5.slice_rend(1,2),std::ostream_iterator<double>(std::cout," "));   
   std::cout<<std::endl;
   std::cout<<"row iterator"<<std::endl;
   std::copy(M5.row_begin(1,2),M5.row_end(1,2),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<"reverse row iterator"<<std::endl;
   std::copy(M5.row_rbegin(1,2),M5.row_rend(1,2),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<"col iterator"<<std::endl;
   std::copy(M5.col_begin(1,2),M5.col_end(1,2),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;

   std::cout<<"revers col iterator"<<std::endl;
   std::copy(M5.col_rbegin(1,2),M5.col_rend(1,2),std::ostream_iterator<double>(std::cout," "));
   std::cout<<std::endl;
   //----------------------------  

   //------------------------------
   //Comparison operators
   //------------------------------
   std::cout<<" M3 == M2 "<<(M3 == M2)<<std::endl;
   std::cout<<" M3 != M2 "<<(M3 != M2)<<std::endl;
   std::cout<<" M3 > M2 "<<(M3 > M2)<<std::endl;
   std::cout<<" M3 < M2 "<<(M3 < M2)<<std::endl;
   std::cout<<" M3 >= M2 "<<(M3 >= M2)<<std::endl;
   std::cout<<" M3 <= M2 "<<(M3 <= M2)<<std::endl;
   //------------------------------

   //------------------------------
   //fill methods
   //------------------------------
   M3.fill(4.0);
   std::cout<<M3<<std::endl;
   double d2[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0,19.0,20.0,21.0,22.0,23.0,24.0};
   M3.fill(d2);
   std::cout<<M3<<std::endl;
   std::cout<<std::endl;
   std::vector<int> V2(24);
   for(std::size_t i = 0; i < 24; ++i)
     V2[i] = i;
   M3.fill(V2.begin(),V2.end());
   std::cout<<M3<<std::endl;
  //------------------------------

   //------------------------------
   // With non primitive data
   //------------------------------
   std::complex<double> c(2.3,2.3);
   std::cout<<c<<std::endl;
   slip::Volume<std::complex<double> > MC(2,1,3,c);
   std::cout<<MC<<std::endl; 
   std::cout<<MC[1][0][2]<<std::endl;
  

    //------------------------------
   // Artithmetic an mathematical operators
   //------------------------------   
   slip::Volume<float> MM(3,2,4);
   MM = 5.5;
   std::cout<<MM<<std::endl;
   
   slip::Volume<float> MM2(3,2,4);
   MM2 = 2.2;
   std::cout<<MM2<<std::endl;
   
   MM += MM2;
   std::cout<<MM<<std::endl;
   
   MM -= MM2;
   std::cout<<MM<<std::endl;
   
   MM *= MM2;
   std::cout<<MM<<std::endl;
   
   MM /= MM2;
   std::cout<<MM<<std::endl;
   
   slip::Volume<float> MM3(3,2,4);
   MM3 = MM + MM2;
   std::cout<<MM<<"+"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM - MM2;
   std::cout<<MM<<"-"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM * MM2;
   std::cout<<MM<<"*"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM / MM2;
   std::cout<<MM<<"/"<<MM2<<" = "<<MM3<<std::endl;
  
   std::cout<<"-MM3 = "<<-MM3<<std::endl;
   
   std::cout<<"M2 "<<std::endl;
   std::cout<<MM2<<std::endl;
   MM2+=1.1;
   std::cout<<MM2<<std::endl;
   MM2-=1.1;
   std::cout<<MM2<<std::endl;
   MM2*=1.1;
   std::cout<<MM2<<std::endl;
   MM2/=1.1;
   std::cout<<MM2<<std::endl;
   
   std::cout<<M4.min()<<std::endl;
   std::cout<<M4.max()<<std::endl;
   std::cout<<M4.sum()<<std::endl;
   
   std::cout<<min(M4)<<std::endl;
   std::cout<<max(M4)<<std::endl;
   

   slip::Volume<double> MM5(2,2,3);
   MM5 = M4 + M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5+2.0<<std::endl;
   std::cout<<(2.0+MM5)<<std::endl;
   
   std::cout<<MM5<<std::endl;
   std::cout<<MM5-2.0<<std::endl;
   std::cout<<(2.0-MM5)<<std::endl;
   
   std::cout<<MM5<<std::endl;
   std::cout<<MM5*2.0<<std::endl;
   std::cout<<(2.0*MM5)<<std::endl;
     
   std::cout<<MM5<<std::endl;
   std::cout<<MM5/2.0<<std::endl;
   //   std::cout<<(2.0/MM5)<<std::endl;
 
   std::cout<<abs(-M4)<<std::endl;
   std::cout<<sqrt(M4)<<std::endl;
   std::cout<<cos(M4)<<std::endl;
   std::cout<<acos(M4)<<std::endl;
   std::cout<<sin(M4)<<std::endl;
   std::cout<<asin(M4)<<std::endl;
   std::cout<<tan(M4)<<std::endl;
   std::cout<<atan(M4)<<std::endl;
   std::cout<<exp(M4)<<std::endl;
   std::cout<<log(M4)<<std::endl;
   std::cout<<cosh(M4)<<std::endl;
   std::cout<<sinh(M4)<<std::endl;
   std::cout<<tanh(M4)<<std::endl;
   std::cout<<log10(M4)<<std::endl;

     M4.apply(std::sqrt);
     std::cout<<M4<<std::endl;

 //------------------------------

   //------------------------------
   // i/o methods
   //------------------------------
   M3.write_raw("volume.raw");
   M3.read_raw("volume.raw",4,3,2);
   std::cout<<M3<<std::endl;

   
  //  slip::Volume<double> Vol;
//    std::string file_base_name;
//    std::cout<<"file base name ? (example : toto%3d.png)"<<std::endl;
//    std::cin>>file_base_name;
//    std::cout<<"first image number ?"<<std::endl;
//    std::size_t img_from = 1;
//    std::cin>>img_from;
//    std::cout<<"last image number ?"<<std::endl;
//    std::size_t img_to = 1;
//    std::cin>>img_to;
//    Vol.read_from_images(file_base_name,img_from,img_to);
//    std::cout<<"dim1 = "<<Vol.dim1()<<" dim2 = "<<Vol.dim2()<<" dim3 = "<<Vol.dim3()<<" size = "<<Vol.size()<<" slice_size = "<<Vol.slice_size()<<" max_size = "<<Vol.max_size()<<" empty = "<<Vol.empty()<<std::endl; 
//    //std::cout<<Vol<<std::endl; 

  
//    Vol.write_to_images("toto-%4d.bmp",1,img_to - img_from);

   //-----------------------
   //Serialization
   //-----------------------
    // create and open a character archive for output
    std::ofstream ofs("volume.txt");
    slip::Volume<float> b(3,3,4);
    slip::iota(b.begin(),b.end(),1.0f);
    
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<b;
    }//to close archive
    std::ifstream ifs("volume.txt");
    slip::Volume<float> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("volume.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<b;
    }//to close archive

    std::ifstream ifsb("volume.bin");
    slip::Volume<float> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;
  return 0;
}
