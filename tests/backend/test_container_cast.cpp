#include <iostream>
#include <iomanip>
#include "Array.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "arithmetic_op.hpp"
#include "container_cast.hpp"

int main()
{
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "1d container cast...  " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Array<double> V(5);
  slip::iota(V.begin(),V.end(),1.0,1.0);
  std::cout<<"V = \n"<<V<<std::endl;
  slip::Array<double>::default_iterator firstV;
  slip::Array<double>::default_iterator lastV;
  slip::container_cast(V,firstV,lastV);
  
  for(; firstV != lastV; ++firstV)
    {
      std::cout<<*firstV<<" ";
    }
  std::cout<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "1d reverse container cast...  " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<double>::reverse_iterator revfirstV;
  slip::Array<double>::reverse_iterator revlastV;
  slip::reverse_container_cast(V,revfirstV,revlastV);
 
  for(; revfirstV != revlastV; ++revfirstV)
    {
      std::cout<<*revfirstV<<" ";
    }
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "2d container cast...  " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Array2d<double> A(5,6);
  slip::iota(A.begin(),A.end(),1.0,1.0);
  std::cout<<"A = \n"<<A<<std::endl;
  slip::Array2d<double>::default_iterator firstA;
  slip::Array2d<double>::default_iterator lastA;
  slip::container_cast(A,firstA,lastA);
  
  for(; firstA != lastA; ++firstA)
    {
      std::cout<<"("<<firstA.i()<<","<<firstA.j()<<") ";
    }
  std::cout<<std::endl;
   
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "2d reverse container cast...  " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array2d<double>::iterator2d revfirstA;
  slip::Array2d<double>::iterator2d revlastA;
  slip::reverse_container_cast(A,revfirstA,revlastA);
  
  for(; revfirstA != revlastA; --revfirstA)
    {
      std::cout<<"("<<revfirstA.i()<<","<<revfirstA.j()<<") ";
    }
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "3d container cast...  " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Array3d<double> Vol(2,5,6);
  slip::iota(Vol.begin(),Vol.end(),1.0,1.0);
  std::cout<<"Vol = \n"<<Vol<<std::endl;
  slip::Array3d<double>::default_iterator firstVol;
  slip::Array3d<double>::default_iterator lastVol;
  slip::container_cast(Vol,firstVol,lastVol);
  
  for(; firstVol != lastVol; ++firstVol)
    {
      std::cout<<"("<<firstVol.k()<<","<<firstVol.i()<<","<<firstVol.j()<<") ";
    }
  std::cout<<std::endl;
  

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "3d reverse container cast...  " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array3d<double>::iterator3d revfirstVol;
  slip::Array3d<double>::iterator3d revlastVol;
  slip::reverse_container_cast(Vol,revfirstVol,revlastVol);
  
  for(; revfirstVol != revlastVol; --revfirstVol)
    {
      std::cout<<"("<<revfirstVol.k()<<","<<revfirstVol.i()<<","<<revfirstVol.j()<<") ";
    }
  std::cout<<std::endl;
  


 return 0;
}
