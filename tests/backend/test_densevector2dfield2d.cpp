#include "Matrix.hpp"
#include "DenseVector2dField2d.hpp"
#include "Vector2d.hpp"
#include "Box2d.hpp"
#include "iterator2d_box.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>

#include <complex>
#include <algorithm>
#include <numeric>
#include <vector>
#include "dynamic.hpp"
#include "compare.hpp"
#include "statistics.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


int main()
{

  //------------------------------
  //constructors
  //------------------------------
  slip::DenseVector2dField2d<double> M;
  std::cout<<"name = "<<M.name()<<" dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" max_size = "<<M.max_size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 

  slip::DenseVector2dField2d<double> M2(6,3);
  std::cout<<"dim1 = "<<M2.dim1()<<" dim2 = "<<M2.dim2()<<" size = "<<M2.size()<<" empty = "<<M2.empty()<<std::endl; 
  std::cout<<"rows = "<<M2.rows()<<" cols = "<<M2.cols()<<" columns = "<<M2.columns()<<std::endl;
  std::cout<<"height = "<<M2.height()<<" width = "<<M2.width()<<std::endl; 
  std::cout<<M2<<std::endl; 
 
  slip::DenseVector2dField2d<double> M3(6,3,slip::Vector2d<double>(1.0,2.0));
  std::cout<<"dim1 = "<<M3.dim1()<<" dim2 = "<<M3.dim2()<<" size = "<<M3.size()<<" empty = "<<M3.empty()<<std::endl; 
  std::cout<<"rows = "<<M3.rows()<<" cols = "<<M3.cols()<<" columns = "<<M3.columns()<<std::endl;
  std::cout<<M3<<std::endl;


 double d[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0};
 slip::DenseVector2dField2d<double> M4(2,3,d);
 std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
 std::cout<<M4<<std::endl; 
 

 slip::Matrix<double> Mat1(4,3,12.0);
 slip::Matrix<double> Mat2(4,3,13.0);

 
 slip::DenseVector2dField2d<double> Mcol(4,3,Mat1.begin(),Mat1.end(),Mat2.begin());
 std::cout<<Mcol<<std::endl; 


//   std::vector<int> V(20);
//   for(std::size_t i = 0; i < 20; ++i)
//     V[i] = i;
//   slip::DenseVector2dField2d<int> M5(4,5,V.begin(),V.end());
//   std::cout<<"dim1 = "<<M5.dim1()<<" dim2 = "<<M5.dim2()<<" size = "<<M5.size()<<" empty = "<<M5.empty()<<std::endl; 
//  std::cout<<M5<<std::endl; 


 slip::DenseVector2dField2d<double> M6 = M4;
  std::cout<<"dim1 = "<<M6.dim1()<<" dim2 = "<<M6.dim2()<<" size = "<<M6.size()<<" empty = "<<M6.empty()<<std::endl; 
 std::cout<<M6<<std::endl; 


 //------------------------------

 //------------------------------
 //resize
 //------------------------------
  M.resize(4,3,5.6);
  std::cout<<"dim1 = "<<M.dim1()<<" dim2 = "<<M.dim2()<<" size = "<<M.size()<<" empty = "<<M.empty()<<std::endl; 
  std::cout<<M<<std::endl; 
  //------------------------------


  //------------------------------
  //Affectation
  //------------------------------
  slip::DenseVector2dField2d<double> M7;
  M7 = M;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M7 = M7;
  std::cout<<"dim1 = "<<M7.dim1()<<" dim2 = "<<M7.dim2()<<" size = "<<M7.size()<<" empty = "<<M7.empty()<<std::endl; 
  std::cout<<M7<<std::endl;   

  M4 = 12.2;
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl;   

  M4 = slip::Vector2d<double> (128.0,255.0);
  std::cout<<"dim1 = "<<M4.dim1()<<" dim2 = "<<M4.dim2()<<" size = "<<M4.size()<<" empty = "<<M4.empty()<<std::endl; 
  std::cout<<M4<<std::endl;   
  //------------------------------

  
  //------------------------------
  //Element access operators
  //------------------------------
   M2(2,1) = 2.1;
   M2[1][2] = 1.2;
  
  for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2[i][j]<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
   for(size_t i = 0; i < M2.dim1(); ++i)
    {
      for(size_t j = 0; j < M2.dim2(); ++j)
	{
	  std::cout<<M2(i,j)<<" ";
	}
      std::cout<<std::endl;
    }

    for(size_t i = 0; i < M4.dim1(); ++i)
    {
      for(size_t j = 0; j < M4.dim2(); ++j)
	{
	  std::cout<<M4.Vx1(i,j)<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;

    for(size_t i = 0; i < M4.dim1(); ++i)
    {
      for(size_t j = 0; j < M4.dim2(); ++j)
	{
	  std::cout<<M4.Vx2(i,j)<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;


   for(size_t i = 0; i < M4.dim1(); ++i)
    {
      for(size_t j = 0; j < M4.dim2(); ++j)
	{
	  std::cout<<M4.norm(i,j)<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;
 for(size_t i = 0; i < M4.dim1(); ++i)
    {
      for(size_t j = 0; j < M4.dim2(); ++j)
	{
	  std::cout<<M4.angle(i,j)<<" ";
	}
      std::cout<<std::endl;
    }
  std::cout<<std::endl;

  //------------------------------

   //------------------------------
   //swap
   //------------------------------
   M2.swap(M3);
   std::cout<<M2<<std::endl;
   std::cout<<std::endl;
   std::cout<<M3<<std::endl;
   //------------------------------


   //----------------------------
   //iterators
   //----------------------------
   slip::DenseVector2dField2d<double> M8(2,4);
   std::vector<slip::Vector2d<double> > V8(8);
   for(std::size_t i = 0; i < 8; ++i)
     V8[i] = slip::Vector2d<double>(i,i);
    
   M8.fill(V8.begin(),V8.end());   
   std::cout<<"M8 = \n"<<M8<<std::endl;

   std::copy(M8.begin(),M8.end(),std::ostream_iterator<slip::Vector2d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M8.rbegin(),M8.rend(),std::ostream_iterator<slip::Vector2d<double> >(std::cout," "));
   std::cout<<std::endl<<std::endl;
   
   
//    // std::transform(M3.rbegin(),M3.rend(),M8.begin(),M2.begin(),std::plus<double>());
  
//    std::cout<<M2<<std::endl;
   

   std::copy(M8.row_begin(1),M8.row_end(1),std::ostream_iterator<slip::Vector2d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;

   std::copy(M8.col_begin(1),M8.col_end(1),std::ostream_iterator<slip::Vector2d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;
   
   std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_begin(2),std::plus<slip::Vector2d<double> >());
   std::cout<<M2<<std::endl;

   std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_begin(2),std::plus<slip::Vector2d<double> >());
   std::cout<<M2<<std::endl;


   std::copy(M8.row_rbegin(1),M8.row_rend(1),std::ostream_iterator<slip::Vector2d<double> >(std::cout," "));
   std::cout<<std::endl;
   std::copy(M8.col_rbegin(1),M8.col_rend(1),std::ostream_iterator<slip::Vector2d<double> >(std::cout," "));
   std::cout<<std::endl;
   

   std::transform(M2.row_begin(1),M2.row_end(1),M2.row_begin(1),M2.row_rbegin(2),std::plus<slip::Vector2d<double> >());
   std::cout<<M2<<std::endl;

   std::transform(M2.col_begin(1),M2.col_end(1),M2.col_begin(1),M2.col_rbegin(2),std::plus<slip::Vector2d<double> >());
   std::cout<<M2<<std::endl;
   
   
     slip::DenseVector2dField2d<int> M100(10,12);
     for(std::size_t i = 0; i < M100.dim1(); ++i)
       {
	 for(std::size_t j = 0; j < M100.dim2(); ++j)
	   {
	     M100[i][j][0] = i + j;
	     M100[i][j][1] = i + j + 1;
	   }
       }
     std::cout<<M100<<std::endl;
   
     slip::Box2d<int> box4(1,1,2,2);
     slip::Box2d<int> box5(0,0,1,1);
//      std::cout<<std::inner_product(M100.upper_left(box4),M100.bottom_right(box4),M100.upper_left(box5),0.0)<<std::endl;
     
     slip::DenseVector2dField2d<int>::iterator2d it  = M100.upper_left(box4);
     slip::DenseVector2dField2d<int>::iterator2d it2 = M100.upper_left(box5);
     
     for(;it != M100.bottom_right(box4); ++it, ++it2)
       {
	 std::cout<<"("<<*it<<" "<<*it2<<") ";
       }
     std::cout<<std::endl;
  

     //first plane
     std::cout<<"first plane "<<std::endl;
     std::copy(M100.begin(0),M100.end(0),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //second plane
     std::cout<<"second plane "<<std::endl;
     std::copy(M100.begin(1),M100.end(1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     //third plane
     std::cout<<"third plane "<<std::endl;
     std::copy(M100.begin(2),M100.end(2),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     
          std::cout<<"first plane, row 1 "<<std::endl;
     std::copy(M100.row_begin(0,1),M100.row_end(0,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<"first plane, reverse row 1 "<<std::endl;
     std::copy(M100.row_rbegin(0,1),M100.row_rend(0,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;

   //    std::cout<<"first plane, col 1 "<<std::endl;
//       std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
     
//      std::cout<<"first plane, reverse col 1 "<<std::endl;
//      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));

  std::cout<<"second plane, row 1 "<<std::endl;
     std::copy(M100.row_begin(1,1),M100.row_end(1,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<"second plane, reverse row 1 "<<std::endl;
     std::copy(M100.row_rbegin(1,1),M100.row_rend(1,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;

    //  std::cout<<"first plane, col 1 "<<std::endl;
//      std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
     
//      std::cout<<"first plane, reverse col 1 "<<std::endl;
//      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));


     std::cout<<"third plane, row 1 "<<std::endl;
     std::copy(M100.row_begin(2,1),M100.row_end(2,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;
     std::cout<<"third plane, reverse row 1 "<<std::endl;
     std::copy(M100.row_rbegin(2,1),M100.row_rend(2,1),std::ostream_iterator<int>(std::cout," "));
     std::cout<<std::endl;

    //  std::cout<<"first plane, col 1 "<<std::endl;
//      std::copy(M100.col_begin(0,1),M100.col_end(0,1),std::ostream_iterator<int>(std::cout," "));
//      std::cout<<std::endl;
     
//      std::cout<<"first plane, reverse col 1 "<<std::endl;
//      std::copy(M100.col_rbegin(0,1),M100.col_rend(0,1),std::ostream_iterator<int>(std::cout," "));


  //----------------------------  

   //------------------------------
  //Comparison operators
  //------------------------------
  std::cout<<" M3 == M2 "<<(M3 == M2)<<std::endl;
  std::cout<<" M3 != M2 "<<(M3 != M2)<<std::endl;
//   std::cout<<" M3 > M2 "<<(M3 > M2)<<std::endl;
//   std::cout<<" M3 < M2 "<<(M3 < M2)<<std::endl;
//   std::cout<<" M3 >= M2 "<<(M3 >= M2)<<std::endl;
//   std::cout<<" M3 <= M2 "<<(M3 <= M2)<<std::endl;
  //------------------------------


  //------------------------------
  //fill methods
  //------------------------------
  
  M3.fill(4.0);
  std::cout<<M3<<std::endl;
  M3.fill(slip::Vector2d<double>(2.0,3.0));
  std::cout<<M3<<std::endl;
  double d2[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0};
  M3.fill(d2);
  std::cout<<M3<<std::endl;
  std::cout<<std::endl;
  std::vector<slip::Vector2d<double> > V2(18);
  for(std::size_t i = 0; i < 18; ++i)
    V2[i] = slip::Vector2d<double>(i,i);
    
  M3.fill(V2.begin(),V2.end());
  std::cout<<M3<<std::endl;

  slip::Matrix<double> mat(2,3);

  //------------------------------
  

 


   //------------------------------
   // Artithmetic an mathematical operators
   //------------------------------   
  std::cout<<"Test arithmetic and mathematical operators..."<<std::endl;
  slip::DenseVector2dField2d<float> Mat(4,3,2.0);
       
  slip::DenseVector2dField2d<float> MM(3,2);
  MM = 5.5;
  std::cout<<MM<<std::endl;
   
  slip::DenseVector2dField2d<float> MM2(3,2);
  MM2 = 2.2;
  std::cout<<MM2<<std::endl;
   
  MM += MM2;
  std::cout<<MM<<std::endl;
   
   MM -= MM2;
   std::cout<<MM<<std::endl;
   
   MM *= MM2;
   std::cout<<MM<<std::endl;
   
   MM /= MM2;
   std::cout<<MM<<std::endl;
   
   slip::DenseVector2dField2d<float> MM3(3,2);
   MM3 = MM + MM2;
   std::cout<<MM<<"+"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM - MM2;
   std::cout<<MM<<"-"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM * MM2;
   std::cout<<MM<<"*"<<MM2<<" = "<<MM3<<std::endl;
  
   MM3 = MM / MM2;
   std::cout<<MM<<"/"<<MM2<<" = "<<MM3<<std::endl;
  
   std::cout<<"-MM3 = "<<-MM3<<std::endl;
   

 

   std::cout<<"M2 "<<std::endl;
   std::cout<<MM2<<std::endl;
   MM2+=1.1;
   std::cout<<MM2<<std::endl;
   MM2-=1.1;
   std::cout<<MM2<<std::endl;
   MM2*=1.1;
   std::cout<<MM2<<std::endl;
   MM2/=1.1;
   std::cout<<MM2<<std::endl;

   slip::Vector2d<float> vvv(4.4,4.4);
   std::cout<<"M2 "<<std::endl;
   std::cout<<MM2<<std::endl;
   MM2+=vvv;
   std::cout<<MM2<<std::endl;
   MM2-=vvv;
   std::cout<<MM2<<std::endl;
   MM2*=vvv;
   std::cout<<MM2<<std::endl;
   MM2/=vvv;
   std::cout<<MM2<<std::endl;
   
  std::cout<<" mins(M100) = "<<M100.min()<<std::endl;
  std::cout<<" maxs(M100) = "<<M100.max()<<std::endl;

   

   slip::DenseVector2dField2d<double> MM5(3,4);
   slip::Vector2d<double> vv(2.0,2.0);
   MM5 = M4 + M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5+vv<<std::endl;
   std::cout<<(vv+MM5)<<std::endl;


   MM5 = M4 - M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5-vv<<std::endl;
   std::cout<<(vv-MM5)<<std::endl;
 

   MM5 = M4 * M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5*vv<<std::endl;
   std::cout<<(vv*MM5)<<std::endl;
 

   MM5 = M4 / M4;
   std::cout<<MM5<<std::endl;
   std::cout<<MM5/vv<<std::endl;
    

   
   std::cout<<MM5<<std::endl;
   std::cout<<MM5+2.0<<std::endl;
   std::cout<<(2.0+MM5)<<std::endl;

   std::cout<<MM5<<std::endl;
   std::cout<<MM5-2.0<<std::endl;
   std::cout<<(2.0-MM5)<<std::endl;
   
   std::cout<<MM5<<std::endl;
   std::cout<<MM5*2.0<<std::endl;
   std::cout<<(2.0*MM5)<<std::endl;
     
   std::cout<<MM5<<std::endl;
   std::cout<<MM5/2.0<<std::endl;
   //std::cout<<(2.0/MM5)<<std::endl;
 
   //M4.apply(std::sqrt);
   //std::cout<<M4<<std::endl;

   slip::Matrix<double> Norm(2,3);
   std::transform(MM5.begin(),MM5.end(),Norm.begin(),std::mem_fun_ref(&slip::Vector2d<double>::Euclidean_norm));
   std::cout<<"Norm ;"<<std::endl;
   std::cout<<Norm<<std::endl;

   MM5.norm(Norm);
   std::cout<<"Norm ;"<<std::endl;
   std::cout<<Norm<<std::endl;
   slip::Matrix<double> Angle(2,3);
   MM5.angle(Angle);
   std::cout<<"Angle ;"<<std::endl;
   std::cout<<Angle<<std::endl;
 //------------------------------
   double val[]={0.125,0.125,0.25,0.25,0.5,0.5,0.75,0.75,1.0,1.0,1.125,1.125};
   slip::DenseVector2dField2d<double> Test(2,3,val);   
   Test.write_gnuplot("test.gnu");

   Test.write_tecplot("test.tecplot","titre","zone");

   // Test Matrix of 2d Vectors
  std::size_t ni = 64;
  std::size_t nj = 64;

  slip::Vector<double> X(nj,0.0);
  slip::Vector<double> Y(ni,0.0);
  //  slip::Matrix< slip::Vector<double> > V(X.size(),Y.size());
  slip::DenseVector2dField2d<double> V(X.size(),Y.size());
  //slip::Matrix< slip::Vector<double> > DV(X.size(),Y.size());
  for(std::size_t j=0; j < nj; ++j)
    X(j)=j/double(nj-1)-0.5;

  std::cout << "Vector X:" << std::endl;
  std::cout << "No of Elements: " << X.size() << std::endl;
  std::cout << X << std::endl;

  for(std::size_t i=0; i < ni; ++i)
    Y(i)=i/double(ni-1)-0.5;

  std::cout << "Vector Y:" << std::endl;
  std::cout << "No of Elements: " << Y.size() << std::endl;
  std::cout << Y << std::endl;

  slip::Array2d<double> R(ni,nj,0.0);
  for(std::size_t i=0; i<ni ; ++i)
    for(std::size_t j=0; j<nj ; ++j)
      R(i,j)=sqrt(X(j)*X(j)+Y(i)*Y(i));
 


  std::cout << "Matrix R:" << std::endl;
  std::cout << "No of Lines: " << R.rows() << ", No of Columns:" << R.columns() << std::endl;
  std::cout << R << std::endl;
  
  std::cout << "Matrix V:" << std::endl;
  std::cout << "No of Lines: " << V.rows() << ", No of Columns:" << V.columns() << std::endl;  


  for(std::size_t i=0; i<nj ; ++i)
  {
    for(std::size_t j=0; j<ni; ++j)
    {
      //V(i,j).set_x(-Y(j));
      //V(i,j).set_y(X(i));
      double r = R(i,j);
      V(i,j)[0]=(-Y(j)/r/r)*(1-exp(-r*r*25));
      V(i,j)[1]=(X(i)/r/r)*(1-exp(-r*r*25));
      //V(i,j).set(-Y(j)/R(i,j)/R(i,j)/R(i,j),X(i)/R(i,j)/R(i,j)/R(i,j));
      //V(i,j).set(Y(j),0.0);
    }

  }
  slip::DenseVector2dField2d<double> Vxy(Y.size(),X.size());
  for(std::size_t i=0; i<nj ; ++i)
    {
      for(std::size_t j=0; j<ni; ++j)
	{
	  
	  Vxy[j][i] =  V[i][nj-1-j];
	}
    }
  //V.write_tecplot("V.gnu","toto","titi");
  V.write_gnuplot("V.gnu");
  Vxy.write_gnuplot("Vxy.gnu");
  Vxy.write_tecplot("Vxy.tec","","");

  slip::Array2d<double> Matrice(6,7,0.0);
   for(std::size_t i=0; i < Matrice.rows(); ++i)
     for(std::size_t j=0; j < Matrice.cols(); ++j)
       Matrice(i,j)=double(i+Matrice.rows()*j);
   
   //copy M twice : in the first plane and in the second plane of VFM
   slip::DenseVector2dField2d<double> VFM(6,7,Matrice.begin(),Matrice.end(),Matrice.begin());
   slip::Matrix<double> Result(6,7,0.0);

  slip::DenseVector2dField2d<float> VF3(4,3);
  slip::iota(VF3.begin(),VF3.end(),slip::Vector2d<float>(1.0,1.0),slip::Vector2d<float>(1.0,1.0));
  std::cout<<"VF3 = \n"<<VF3<<std::endl;
  slip::DenseVector2dField2d<float> VF4(4,3);
  slip::iota(VF4.begin(),VF4.end(),slip::Vector2d<float>(1.0,0.5),slip::Vector2d<float>(0.5,1.0));
  std::cout<<"VF4 = \n"<<VF4<<std::endl;
  slip::Matrix<float> RVF(4,3);
  slip::distance(VF3.begin(),VF3.end(),VF4.begin(),RVF.begin(),slip::L2_dist_vect<slip::Vector2d<float> >());
  
  std::cout<<"RVF L2 = \n"<<RVF<<std::endl;
  slip::distance(VF3.begin(),VF3.end(),VF4.begin(),RVF.begin(),slip::L22_dist_vect<slip::Vector2d<float> >());
  
  std::cout<<"RVF L22= \n"<<RVF<<std::endl;
  slip::distance(VF3.begin(),VF3.end(),VF4.begin(),RVF.begin(),slip::L1_dist_vect<slip::Vector2d<float> >());
  
  std::cout<<"RVF L1= \n"<<RVF<<std::endl;

  slip::distance(VF3.begin(),VF3.end(),VF4.begin(),RVF.begin(),slip::AE_deg<float>());
  
  std::cout<<"RVF AE= \n"<<RVF<<std::endl;
  float aae = slip::mean<float>(RVF.begin(),RVF.end());
  std::cout<<"RVF AAE = "<<aae<<std::endl;
  std::cout<<"RVF AE STD = "<<slip::std_dev<float>(RVF.begin(),RVF.end(),aae)<<std::endl;
  slip::Matrix<float> RVFU(4,3);
  slip::Matrix<float> RVFV(4,3);
  slip::distance(VF3.begin(0),VF3.end(0),VF4.begin(0),RVFU.begin(),slip::L2_dist<float,float>());
std::cout<<"RVFU L2 = \n"<<RVFU<<std::endl;
float mean_u = slip::mean<float>(RVFU.begin(),RVFU.end());
float rms_u = std::sqrt(mean_u);
float std_u = slip::std_dev<float>(RVFU.begin(),RVFU.end(),mean_u);
std::cout<<"mean_u = "<<mean_u<<std::endl;
std::cout<<"rms_u = "<<rms_u<<std::endl;
std::cout<<"std dev u = "<<std_u<<std::endl;

slip::distance(VF3.begin(1),VF3.end(1),VF4.begin(1),RVFV.begin(),slip::L2_dist<float,float>());

std::cout<<"RVFV L2 = \n"<<RVFV<<std::endl;
float mean_v = slip::mean<float>(RVFV.begin(),RVFV.end());
float rms_v = std::sqrt(mean_v);
float std_v = slip::std_dev<float>(RVFV.begin(),RVFV.end(),mean_v);
std::cout<<"mean_v = "<<mean_v<<std::endl;
std::cout<<"rms_v = "<<rms_v<<std::endl;
std::cout<<"std dev v = "<<std_v<<std::endl;

 float ee = slip::distance<float>(VF3.begin(),VF3.end(),VF4.begin(),slip::L2_dist_vect<slip::Vector2d<float> >());
 std::cout<<"Euclidan Error = "<<ee<<std::endl;
 std::cout<<"Average Euclidan Error = "<<ee/float(VF3.size())<<std::endl;
 

 slip::Matrix<int> RVF_mask(4,3);
 RVF_mask(0,0) = 2;
 RVF_mask(0,1) = 2;
 RVF_mask(1,1) = 2;
 RVF_mask(3,2) = 2;
 RVF.fill(0.0f);
 std::cout<<"RVF_mask = \n"<<RVF_mask<<std::endl;
 slip::distance_mask(VF3.begin(1),VF3.end(1),RVF_mask.begin(),VF4.begin(1),RVFV.begin(),slip::L2_dist<float,float>(),2);

std::cout<<"RVFV L2 mask = \n"<<RVFV<<std::endl;
mean_v = slip::mean_mask<float>(RVFV.begin(),RVFV.end(),RVF_mask.begin(),2);
rms_v = std::sqrt(mean_v);
std_v = slip::std_dev_mask<float>(RVFV.begin(),RVFV.end(),RVF_mask.begin(),mean_v,2);
std::cout<<"mean_v = "<<mean_v<<std::endl;
std::cout<<"rms_v = "<<rms_v<<std::endl;
std::cout<<"std dev v = "<<std_v<<std::endl;


//serialization
 // create and open a character archive for output
    std::ofstream ofs("vectorfield2dtxt");
    slip::DenseVector2dField2d<float> b(3,4);
    slip::iota(b.begin(),b.end(),slip::Vector2d<float>(1.0f,2.0f),
	       slip::Vector2d<float>(1.0f,1.0f));
    
    {
    boost::archive::text_oarchive oa(ofs);
    oa<<b;
    }//to close archive
    std::ifstream ifs("vectorfield2d.txt");
    slip::DenseVector2dField2d<float> new_b;
    {
    boost::archive::text_iarchive ia(ifs);
    ia>>new_b;
    }//to close archive
    std::cout<<"new_b\n"<<new_b<<std::endl;

    std::ofstream ofsb("vectorfield2d.bin");
    {
    boost::archive::binary_oarchive oa(ofsb);
    oa<<b;
    }//to close archive

    std::ifstream ifsb("vectorfield2d.bin");
    slip::DenseVector2dField2d<float> new_bb;
    {
    boost::archive::binary_iarchive ia(ifsb);
    ia>>new_bb;
    }//to close archive
    std::cout<<"new_rangeb\n"<<new_bb<<std::endl;
  return 0;


}
