
#include "Array.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "arithmetic_op.hpp"
#include "bspline_interpolation.hpp"
#include <iomanip>
#include <limits>

int main()
{

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "1d bspline interpolation " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<double> A(10);
  slip::iota(A.begin(),A.end(),1.0,1.0);
  std::cout<<"A = \n"<<A<<std::endl;

  slip::Array<double> Poles;
  slipalgo::BSplinePoles(3,Poles);
  std::cout<<"Poles = \n"<<Poles<<std::endl;
  slip::Array<double> Coef(A.size());
  slipalgo::BSplineComputeInterpolationCoefficients(A.begin(),A.end(),
						    Coef.begin(),
						    Poles,
						    std::numeric_limits<double>::epsilon());

  std::cout<<"Coef = \n"<<Coef<<std::endl;

  slipalgo::BSplineComputeInterpolationCoefficients(A.begin(),A.end(),
						    Coef.begin(),
						    Poles.begin(),Poles.end(),
						    std::numeric_limits<double>::epsilon());

  std::cout<<"Coef = \n"<<Coef<<std::endl;

  
  //  std::cout<<"interpolate at x = 0.5, degree 3 : "<<slipalgo::BSplineInterpolatedValue(Coef.begin(),Coef.end(),1.5,3)<<std::endl;

  for(std::size_t i = 0; i < A.size(); ++i)
    {
       std::cout<<"interpolate at x = "<<float(i-0.5)<<", degree 3 : "<<slipalgo::BSplineInterpolatedValue(Coef.begin(),Coef.end(),float(i-0.5),3)<<std::endl;
       std::cout<<"interpolate at x = "<<float(i)<<", degree 3 : "<<slipalgo::BSplineInterpolatedValue(Coef.begin(),Coef.end(),float(i),3)<<std::endl;
    } 

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "1d resampling " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<double> Aup2(A.size()*3);
  slipalgo::bspline_resampling_1d(A.begin(),A.end(),
				  3,
				  Aup2.begin(),Aup2.end());
  std::cout<<"A = \n"<<A<<std::endl;
  std::cout<<"Aup 3 = \n"<<Aup2<<std::endl;

  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "2d resampling " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array2d<double> Min(8,7);
  slip::iota(Min.begin(),Min.end(),1.0,1.0);
  std::cout<<"Min = \n"<<Min<<std::endl;
  slip::Array2d<double> Mout(Min.rows()*2,
			     Min.cols()*3);

  slipalgo::bspline_resampling_2d(Min.upper_left(),Min.bottom_right(),
				  3,
				  Mout.upper_left(),Mout.bottom_right());

  std::cout<<"Mout = \n"<<Mout<<std::endl;



  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "3d resampling " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array3d<double> Min3d(5,7,6);
  slip::iota(Min3d.begin(),Min3d.end(),1.0,1.0);
  std::cout<<"Min3d = \n"<<Min3d<<std::endl;
  slip::Array3d<double> Mout3d(Min3d.slices()*2,
  			       Min3d.rows()*2,
  			       Min3d.cols()*2);

  slipalgo::bspline_resampling_3d(Min3d.front_upper_left(),
				  Min3d.back_bottom_right(),
				  3,
				  Mout3d.front_upper_left(),
				  Mout3d.back_bottom_right());

  std::cout<<"Mout3d = \n"<<Mout3d<<std::endl;

  return 0;
}
