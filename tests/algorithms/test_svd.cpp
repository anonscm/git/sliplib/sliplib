#include <iostream>
#include <algorithm>
#include <iomanip>
#include <numeric>
#include <ctime>
#include <vector>
#include <complex>
#include "linear_algebra.hpp"
#include "linear_algebra_svd.hpp"
#include "linear_algebra_eigen.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "Array2d.hpp"
#include "Block2d.hpp"

template< typename Vector, typename Matrix>
void fill(Vector &d, Vector &e, Matrix &B)
{
 typedef typename Matrix::value_type value_type;
   
 B.fill(value_type(0));
 for( std::size_t i=0;i<B.cols()-1;i++)
   {	
     B[i][i] = d[i];
     B[i][i+1] = e[i];
   }
			
  B[B.cols()-1][B.cols()-1]=d[B.cols()-1];


}

int main()
{
  /////////////////////////////////////////////////////////////
  //  Bireduce
  /////////////////////////////////////////////////////////////
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "BiReduce real data" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_'<< std::endl;
  slip::Matrix<double> X(5,4);
  slip::iota(X.begin(),X.end(),1.0,1.0);
  std::cout<<"X = \n"<<X<<std::endl;
  
  slip::Vector<double>d(X.cols());
  slip::Vector<double>e(X.cols()-1);
  slip::Matrix<double> Ub(X.rows(),X.cols());
  slip::Matrix<double> Vb(X.cols(),X.cols());
  slip::Matrix<double> B(X.cols(),X.cols());
  slip::BiReduce(X,d,e,Ub,Vb);
  fill(d,e,B);
  std::cout<<"d"<<d<<std::endl;
  std::cout<<"e"<<e<<std::endl;
  std::cout<<"Ub="<<std::endl<<Ub<<std::endl;
  std::cout<<"Vb="<<std::endl<<Vb<<std::endl;
  std::cout<<"B="<<std::endl<<B<<std::endl;
  ///verify if X=UBV'
  slip::Matrix<double> VbT(X.cols(),X.cols());
  slip::transpose(Vb,VbT);

  slip::Matrix<double> UbB(Ub.rows(),B.cols());
  slip::matrix_matrix_multiplies(Ub,B,UbB);
   
  slip::Matrix<double> UbBVbT(Ub.rows(),Vb.cols());
  slip::matrix_matrix_multiplies(UbB,VbT,UbBVbT);
   
  std::cout<<"UbBVbT="<<std::endl<<UbBVbT<<std::endl;
  
  slip::Matrix<double> VbTVb(X.cols(),X.cols());
  slip::matrix_matrix_multiplies(VbT,Vb,VbTVb);
  std::cout<<"VbTVb = "<<std::endl<<VbTVb<<std::endl;
  slip::Matrix<double> VbVbT(X.cols(),X.cols());
  slip::matrix_matrix_multiplies(Vb,VbT,VbVbT);
  std::cout<<"VbVbT = "<<std::endl<<VbVbT<<std::endl;
  
  slip::Matrix<double> UbT(Ub.cols(),Ub.rows());
  slip::transpose(Ub,UbT);
  slip::Matrix<double> UbUbT(Ub.rows(),Ub.rows());
  slip::matrix_matrix_multiplies(Ub,UbT,UbUbT);
  std::cout<<"UbUbT = "<<std::endl<<UbUbT<<std::endl;
  slip::Matrix<double> UbTUb(Ub.cols(),Ub.cols());
  slip::matrix_matrix_multiplies(UbT,Ub,UbTUb);
  std::cout<<"UbTUb = "<<std::endl<<UbTUb<<std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "BiReduce commplex data" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_'<< std::endl;
// // BiReduce works on this example
 //  std::complex<double> c(1.0,1.0);
//   std::complex<double> c2(1.0,1.0);
//   slip::Matrix<std::complex<double> > M(5,4);//,f1);
//   slip::iota(M.begin(),M.end(),c,c2);

// BiReduce doesn't work on this example
   std::complex<double> c(1.0,2.0);
   std::complex<double> c2(1.0,1.0);
   slip::Matrix<std::complex<double> > M(5,4);//,f1);
   slip::iota(M.begin(),M.end(),c,c2);  
     

// // BiReduce doesn't work on this example
// /*
// std::complex<double>c0(0.0,0.0);
// std::complex<double>c1(1.0,1.0);
// std::complex<double>c2(2.0,1.0);
// std::complex<double>c3(1.0,2.0);
// std::complex<double>c4(8.0,7.0);
// std::complex<double>c5(2.0,2.0);
// std::complex<double>c6(1.0,5.0);
// std::complex<double>c7(2.0,9.0);
// std::complex<double>c8(3.0,4.0);
// std::complex<double>c9(3.0,0.0);
// std::complex<double>c10(7.0,3.0);
// std::complex<double>c11(4.0,7.0);
// std::complex<double>c12(3.0,9.0);
// std::complex<double>c13(3.0,5.0);
// std::complex<double>c14(2.0,3.0);
// std::complex<double>c15(4.0,1.0);
// std::complex<double>f[]={c0,c1, c2,c3, c4,c5, c6,c7, c8,c9,c10,c11,c12,c13,c14,c15,c15};

//  slip::Matrix<std::complex<double> > M(4,4,f);

  std::cout<<"M= \n"<<M<<std::endl;
  slip::Vector<std::complex<double> >  dc(M.cols());
  slip::Vector<std::complex<double> >  ec(M.cols()-1);
  slip::Matrix<std::complex<double> >  Uc(M.rows(),M.cols());
  slip::Matrix<std::complex<double> >  Vc(M.cols(),M.cols());

  slip::Matrix<std::complex<double> >  Sc(M.cols(),M.cols());

  slip::BiReduce(M,dc,ec,Uc,Vc);

  fill(dc,ec,Sc);
  std::cout<<"d"<<dc<<std::endl;
  std::cout<<"e"<<ec<<std::endl;
  std::cout<<"U="<<std::endl<<Uc<<std::endl;
  std::cout<<"V="<<std::endl<<Vc<<std::endl;
  std::cout<<"B="<<std::endl<<Sc<<std::endl;
//verify if X=UScVT
  slip::Matrix<std::complex<double> >  VcT(M.cols(),M.cols());
  slip::hermitian_transpose(Vc,VcT);//V'=V
  std::cout<<"VcT="<<std::endl<<VcT<<std::endl;
  slip::Matrix<std::complex<double> >  UcT(M.cols(),M.rows());
  slip::hermitian_transpose(Uc,UcT);//V'=V
  std::cout<<"UcT="<<std::endl<<UcT<<std::endl;
  slip::Matrix<std::complex<double> >  UcSc(Uc.rows(),Sc.cols());
  //slip::Matrix<std::complex<double> >  UcSc(M.cols(),M.cols());
  
  //slip::Box2d<int> box(0,0,M.cols()-1,M.cols()-1);
  slip::matrix_matrix_multiplies(Uc,Sc,UcSc);//U*S
 //  std::cout<<(Uc.bottom_right(box) - Uc.upper_left(box))<<std::endl;
//   std::cout<<(Sc.bottom_right() - Sc.upper_left())<<std::endl;
//   std::cout<<(UcSc.bottom_right() - UcSc.upper_left())<<std::endl;

//   slip::matrix_matrix_multiplies(Uc.upper_left(box),Uc.bottom_right(box),
// 				 Sc.upper_left(),Sc.bottom_right(),
// 				 UcSc.upper_left(),UcSc.bottom_right());

  std::cout<<"UcSc="<<std::endl<<UcSc<<std::endl;
          
  slip::Matrix<std::complex<double> > UcScVcT(Uc.rows(),VcT.cols());
  //slip::Matrix<std::complex<double> > UcScVcT(Uc.cols(),VcT.cols());
  slip::matrix_matrix_multiplies(UcSc,VcT,UcScVcT);
  
  std::cout<<"UBVT="<<std::endl<<UcScVcT<<std::endl;//U*S*V'

  slip::Matrix<std::complex<double> >  VcTVc(M.cols(),M.cols());
  slip::matrix_matrix_multiplies(VcT,Vc,VcTVc);
  std::cout<<"VcTVc="<<std::endl<<VcTVc<<std::endl;
 
  slip::Matrix<std::complex<double> >  VcVcT(M.cols(),M.cols());
  slip::matrix_matrix_multiplies(Vc,VcT,VcVcT);
  std::cout<<"VcVcT="<<std::endl<<VcVcT<<std::endl;
 
  slip::Matrix<std::complex<double> >  UcUcT(M.rows(),M.rows());
  slip::matrix_matrix_multiplies(Uc,UcT,UcUcT);
  std::cout<<"UcUcT="<<std::endl<<UcUcT<<std::endl;
  
   slip::Matrix<std::complex<double> >  UcTUc(M.cols(),M.cols());
   slip::matrix_matrix_multiplies(UcT,Uc,UcTUc);
   std::cout<<"UcTUc="<<std::endl<<UcTUc<<std::endl;

  /////////////////////////////////////////////////////////////
  //  SVD decomposition
  /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "SVD decomposition" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  double f[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0, 16.0 , 18.0, 5.0};
  slip::Matrix<double> SVDin(4,3,f);
  std::cout <<SVDin<<std::endl;
  slip::Matrix<double> U(4,3,0.0);
  slip::Matrix<double> W(3,3,0.0);
  slip::Matrix<double> V(3,3,0.0);
  //svd_decomp(SVDin,4,3,U,4,3,W,3,3,V,3,3);
  slip::svd(SVDin,U,W,V);
  std::cout <<"SVDin = \n"<<SVDin<<std::endl<<std::endl;
  std::cout <<"U = \n"<<U<<std::endl<<std::endl;
  std::cout <<"W = \n"<<W<<std::endl<<std::endl;
  std::cout <<"V = \n"<<V<<std::endl<<std::endl;
  slip::Matrix<double> SVDin2(4,3);
  slip::svd_approx(U,W,V,U.cols(),SVDin2);
  std::cout <<"SVDin2 = \n"<<SVDin2<<std::endl<<std::endl;
	
  //std::copy(e,e+12,SVDin.begin());
  std::cout <<"SVDin = \n"<<SVDin<<std::endl<<std::endl;
  slip::Vector<double> Wvect(3,0.0);
  slip::svd(SVDin,U,Wvect.begin(),Wvect.end(),V);
  std::cout <<"U = \n"<<U<<std::endl<<std::endl;
  std::cout <<"Wvect = \n"<<Wvect<<std::endl<<std::endl;
  std::cout <<"V = \n"<<V<<std::endl<<std::endl;
 
  slip::svd_approx(U,Wvect.begin(),Wvect.end(),V,U.cols(),SVDin2);
  std::cout <<"SVDin2 = \n"<<SVDin2<<std::endl<<std::endl;

  slip::Matrix<double> SVDinH(4,4);
  slip::hilbert(SVDinH);
  std::cout <<"SVDinH = \n"<<SVDinH<<std::endl<<std::endl;
  slip::Matrix<double> UH(4,4,0.0);
  slip::Matrix<double> WH(4,4,0.0);
  slip::Matrix<double> VH(4,4,0.0);
  slip::svd(SVDinH,UH,WH,VH);
  std::cout <<"Uh = \n"<<UH<<std::endl<<std::endl;
  std::cout <<"Wh = \n"<<WH<<std::endl<<std::endl;
  std::cout <<"Vh = \n"<<VH<<std::endl<<std::endl;
  slip::Matrix<double> SVDinH2(4,4);
  slip::svd_approx(UH,WH,VH,UH.cols(),SVDinH2);
  std::cout <<"SVDinH2 = \n"<<SVDinH2<<std::endl<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "complex SVD decomposition" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
   slip::iota(M.begin(),M.end(),c,c2);  
   slip::svd(M,Uc,Sc,Vc);
   std::cout<<"U="<<std::endl<<Uc<<std::endl;
   std::cout<<"V="<<std::endl<<Vc<<std::endl;
   std::cout<<"S="<<std::endl<<Sc<<std::endl;
   
   slip::svd_approx(Uc,Sc,Vc,Uc.cols(),UcScVcT);
   std::cout<<"USVT="<<std::endl<<UcScVcT<<std::endl;//U*S*V'
  

   /////////////////////////////////////////////////////////////
   //  SVD solve
   /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "SVD solve " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   double x[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0, 16.0 , 18.0, 5.0};
   double y[] ={7.0, 14.0, 11.0, 21.0, 15.0, 17.0, 31.0, 23.0, 25.0, 33.0 , 37.0, 11.0};
   slip::Matrix<double> Msol(12,2);
   slip::Vector<double> Bsol(12);
   std::fill(Msol.col_begin(0),Msol.col_end(0),1.0);
   std::copy(x,x+12,Msol.col_begin(1));
   std::copy(y,y+12,Bsol.begin());
   std::cout<<"Msol = \n"<<Msol<<std::endl;
   std::cout<<"Bsol = \n"<<Bsol<<std::endl;
   slip::Vector<double> Xsol(2);
   slip::svd_solve(Msol,Xsol,Bsol);
   std::cout<<"Xsol = \n"<<Xsol<<std::endl;
   slip::Vector<double> Bsol2(12);
   slip::matrix_vector_multiplies(Msol,Xsol,Bsol2);
   std::cout<<"Bsol2 = \n"<<Bsol2<<std::endl;

   /////////////////////////////////////////////////////////////
   //  SVD inverse
   /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "SVD inverse " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Matrix<double> Msolinv(2,12);
   slip::svd_inv(Msol,Msolinv);
   //slip::pinv(Msol,Msolinv);
   std::cout<<"Msolinv = \n"<<Msolinv<<std::endl;
   slip::matrix_vector_multiplies(Msolinv,Bsol,Xsol);
   std::cout<<"Xsol = \n"<<Xsol<<std::endl;

   /////////////////////////////////////////////////////////////
   //  Eigen values of an hermitian semi-definite matrix
   /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Eigen values of an hermitian semi-definite matrix " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Matrix<double> Meig(4,4);
   slip::hilbert(Meig);
   std::cout<<"Meig = \n"<<Meig<<std::endl;
   slip::Vector<double> MEigVal(4);
   slip::Matrix<double> MEigVect;
   slip::hermitian_eigen(Meig,MEigVal,MEigVect);
   std::cout<<"MEigVal = \n"<<MEigVal<<std::endl;
   std::cout<<"MEigVect = \n"<<MEigVect<<std::endl;


   slip::Matrix<std::complex<double> > Meigc(4,4);
   slip::hilbert(Meigc);
   std::cout<<"Meigc = \n"<<Meigc<<std::endl;
   slip::Vector<std::complex<double> > MEigValc(4);
   slip::Matrix<std::complex<double> > MEigVectc;
   slip::hermitian_eigen(Meigc,MEigValc,MEigVectc);
   std::cout<<"MEigValc  = \n"<<MEigValc<<std::endl;
   std::cout<<"MEigVectc = \n"<<MEigVectc<<std::endl;

   /////////////////////////////////////////////////////////////
   //  Normalized SVD solve
   /////////////////////////////////////////////////////////////
   double gnsvd[] ={26.0, 32.0, 39.0, 32.0, 40.0, 50.0, 39.0, 50.0, 65.0};
   double xnsvd[] = {1.0, 2.0, 3.0};
   double bnsvd[] = {207.0, 262.0, 334.0};
   slip::Matrix<double> Gnsvd(3,3, gnsvd, gnsvd+9);
   slip::Vector<double> Xnsvd(xnsvd, xnsvd+3);
   slip::Vector<double> Bnsvd(bnsvd, bnsvd+3);
   std::cout<<"Gnsvd = \n"<<Gnsvd<<std::endl;
   std::cout<<"Xnsvd = \n"<<Xnsvd<<std::endl;
   std::cout<<"Bnsvd = \n"<<Bnsvd<<std::endl;
   slip::Vector<double> Xsolnsvd(3);
   slip::normalized_svd_solve(Gnsvd,Xsolnsvd,Bnsvd, 1e-10);
   std::cout<<"Xsolnsvd = \n"<<Xsolnsvd<<std::endl;
   slip::Vector<double> Bnsvd2(3);
   slip::matrix_vector_multiplies(Gnsvd,Xsolnsvd,Bnsvd2);
   std::cout<<"Bnsvd2 = \n"<<Bnsvd2<<std::endl;
   
  return 0;  
}

