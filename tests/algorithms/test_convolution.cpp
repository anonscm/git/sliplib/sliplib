#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include "convolution.hpp"
#include "Block.hpp"
#include "border_treatment.hpp"
#include <vector>
#include <complex>
#include "Volume.hpp"
#include "GrayscaleImage.hpp"
#include "Array.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "dynamic.hpp"
#include "Range.hpp"
#include "arithmetic_op.hpp"
#include "FFT.hpp"
#include "correlation.hpp"
int main()
{
  
  
  std::vector<float> v(16);
  for(int i = 0; i < 16; ++i)
    v[i] = i + 1;
  
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  slip::block<float,7> kernel = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0};

  std::vector<float> result(10);
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "valid convolution " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::valid_convolution(v.begin(),v.end(),
			  kernel.begin(),kernel.end(),
			  4,2,result.begin());
  std::cout<<" signal  ="<<std::endl;
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<" kernel  ="<<std::endl;
  std::copy(kernel.begin(),kernel.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;

  std::cout<<" valid convolution (signal,kernel)  ="<<std::endl;
  std::copy(result.begin(),result.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "full convolution " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::vector<float> result2(22);
  slip::full_convolution(v.begin(),v.end(),kernel.begin(),kernel.end(),result2.begin());
  std::cout<<" signal  ="<<std::endl;
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<" kernel  ="<<std::endl;
  std::copy(kernel.begin(),kernel.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<" full convolution (signal,kernel)  ="<<std::endl;
  std::copy(result2.begin(),result2.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "fft convolution " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  std::vector<float> result_fft2(22);
  slip::fft_convolution(v.begin(),v.end(),
			kernel.begin(),kernel.end(),
			result_fft2.begin(),
			result_fft2.end());
   std::cout<<" signal  =\n";
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<" kernel  =\n";
  std::copy(kernel.begin(),kernel.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<" fft convolution (signal,kernel)  with iterator ="<<std::endl;
   std::copy(result2.begin(),result2.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<" fft convolution (signal,kernel)  ="<<std::endl;
   slip::fft_convolution(v,kernel,result_fft2);
   std::copy(result2.begin(),result2.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
  
   std::cout<<" fft crosscorrelation (signal,fliplr(kernel))  ="<<std::endl;
 
   slip::fft_crosscorrelation(v.begin(),v.end(),
			      kernel.rbegin(),kernel.rend(),
			      result_fft2.begin(),
			      result_fft2.end());
   
   std::copy(result2.begin(),result2.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
  
  
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "same convolution zero-padded (4,2)" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::vector<float> result4(16);
   slip::same_convolution(v.begin(),v.end(),kernel.begin(),kernel.end(),4,2,result4.begin(),slip::BORDER_TREATMENT_ZERO_PADDED);
   std::cout<<" signal  =\n";
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<" kernel  =\n";
  std::copy(kernel.begin(),kernel.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<" same convolution zero-padded (signal,kernel)="<<std::endl;
   std::copy(result4.begin(),result4.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
 
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "same convolution neumann (4,2)" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::same_convolution(v.begin(),v.end(),kernel.begin(),kernel.end(),4,2,result4.begin(),slip::BORDER_TREATMENT_NEUMANN);
   std::cout<<" signal  =\n";
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<" kernel  =\n";
  std::copy(kernel.begin(),kernel.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<" same convolution neuman (4,2) (signal,kernel)="<<std::endl;
   std::copy(result4.begin(),result4.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
 
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "same convolution circular (4,2)" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::same_convolution(v.begin(),v.end(),kernel.begin(),kernel.end(),4,2,result4.begin(),slip::BORDER_TREATMENT_CIRCULAR);
   std::cout<<" signal  =\n";
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<" kernel  =\n";
  std::copy(kernel.begin(),kernel.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<" same convolution circular (4,2) (signal,kernel)="<<std::endl;
   std::copy(result4.begin(),result4.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
   

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "same convolution avoid (4,2)" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::same_convolution(v.begin(),v.end(),kernel.begin(),kernel.end(),4,2,result4.begin(),slip::BORDER_TREATMENT_AVOID);
   std::cout<<" signal  =\n";
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<" kernel  =\n";
  std::copy(kernel.begin(),kernel.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<" same convolution avoid (4,2) (signal,kernel)="<<std::endl;
   std::copy(result4.begin(),result4.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "circular convolution" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Array<double> signal1(16);
   slip::iota(signal1.begin(),signal1.end(),1.0);
   slip::Array<double> signal2(16);
   slip::iota(signal2.begin(),signal2.end(),3.0);
   slip::Array<double> resultc(16);
   std::cout<<"signal1 = \n"<<signal1<<std::endl;
   std::cout<<"signal2 = \n"<<signal2<<std::endl;
   slip::same_convolution(signal1.begin(),signal1.end(),
			  signal2.begin(),signal2.end(),8,7,
			  resultc.begin(),
			  slip::BORDER_TREATMENT_CIRCULAR);
   std::cout<<"circular convolution (signal1,signal2) = \n"<<resultc<<std::endl;
   slip::Array<std::complex<double> > fft1(16);
   slip::Array<std::complex<double> > fft2(16);
   slip::real_fft(signal1.begin(),signal1.end(),fft1.begin());
   slip::real_fft(signal2.begin(),signal2.end(),fft2.begin());
   slip::multiplies(fft1.begin(),fft1.end(),
		    fft2.begin(),
		    fft1.begin());
   slip::ifft(fft1.begin(),fft1.end(),fft2.begin());
   slip::fftshift(fft2.begin(),fft2.end());
   std::transform(fft2.begin(),fft2.end(),resultc.begin(),slip::un_real<std::complex<double>,double>());  
   std::cout<<"circular fft convolution (signal1,signal2) = \n"<<resultc<<std::endl;
  
  
 slip::fft_circular_convolution(signal1.begin(),signal1.end(),
				signal2.begin(),signal2.end(),
				resultc.begin(),resultc.end());
 std::cout<<"circular fft convolution (signal1,signal2) = \n"<<resultc<<std::endl;
 
 
 
 

 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "same convolution zero-padded odd kernel" << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 float ker1[] = {1.0, 2.0, 3.0, 4.0,5.0};
 slip::Array<float> Ker1(5,ker1);
 slip::same_convolution(v.begin(),v.end(),
			  Ker1.begin(),Ker1.end(),
			  result4.begin(),
			  slip::BORDER_TREATMENT_ZERO_PADDED);
   std::cout<<"signal = "<<std::endl;
   std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<"kernel1 = \n"<<Ker1<<std::endl;
   std::copy(result4.begin(),result4.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
   slip::same_convolution(v,Ker1,result4,slip::BORDER_TREATMENT_ZERO_PADDED);

   std::cout<<"signal = "<<std::endl;
   std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<"kernel1 = \n"<<Ker1<<std::endl;
   std::copy(result4.begin(),result4.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "same convolution zero-padded even kernel" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   float ker2[] = {1.0, 2.0, 3.0, 4.0};
   slip::Array<float> Ker2(4,ker2);
   slip::same_convolution(v.begin(),v.end(),
			  Ker2.begin(),Ker2.end(),
			  result4.begin(),
			  slip::BORDER_TREATMENT_ZERO_PADDED);
   std::cout<<"signal = "<<std::endl;
   std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<"kernel2 = \n"<<Ker2<<std::endl;
   std::copy(result4.begin(),result4.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;
   slip::same_convolution(v,Ker2,result4,slip::BORDER_TREATMENT_ZERO_PADDED);

   std::cout<<"signal = "<<std::endl;
   std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<"kernel2 = \n"<<Ker2<<std::endl;
   std::copy(result4.begin(),result4.end(),std::ostream_iterator<float>(std::cout," "));
   std::cout<<std::endl;
   std::cout<<std::endl;
 
 
 

  //  slip::GrayscaleImage<float> I;
//    I.read("./../backend/lena.gif");
   
//    slip::GrayscaleImage<float> I2(I.dim1(),I.dim2());
//    slip::block<float,3> m = {0.5,0.0,-0.5};
//    for(size_t i = 0; i < I.dim1(); ++i)
//      {
//        slip::same_convolution(I.row_begin(i),I.row_end(i),
// 			      m.begin(),m.end(),1,1,
// 			      I2.row_begin(i),
// 			      slip::BORDER_TREATMENT_NEUMANN);
   
//      }
//    I2.write("I2.gif");
//    slip::block<float,3> m2 = {1.0,2.0,1.0};
//    slip::GrayscaleImage<float> I3(I.dim1(),I.dim2());
//    for(size_t j = 0; j < I2.dim2(); ++j)
//      {
//        slip::same_convolution(I2.col_begin(j),I2.col_end(j),
// 			      m2.begin(),m2.end(),1,1,
//  			      I3.col_begin(j),
//  			      slip::BORDER_TREATMENT_CIRCULAR);
//      }

   
   
//    //slip::range_fun_inter01<float,float> fun01(I3.min(),I3.max());
//    //slip::change_dynamic(I3.begin(),I3.end(),I3.begin(),fun01);
//    I3.write("lena_mean.gif");

//    slip::Array2d<float> Ibis(I.dim1(),I.dim2(),I.begin(),I.end());
//    slip::Array2d<float> I4(Ibis.dim1(),Ibis.dim2()/2);
//    slip::block<float,3> m3 = {1.0, 2.0, 1.0};
//    slip::Range<int> rangei(0,(Ibis.dim2() - 1), 2);
   
   
//    for(size_t i = 0; i < Ibis.dim1(); ++i)
//      {
//        slip::same_convolution(Ibis.row_begin(i,rangei),Ibis.row_end(i,rangei),
// 			      m3.begin(),m3.end(),1,1,
// 			      I4.row_begin(i),
// 			      slip::BORDER_TREATMENT_NEUMANN);
   
//      }
//    //I2.write("I2.gif");
//    //slip::block<float,3> m2 = {1.0,2.0,1.0};
//    slip::Array2d<float> I5(Ibis.dim1()/2,Ibis.dim2()/2);
//    slip::Range<int> rangej(0,(Ibis.dim1() - 1), 2);
//    for(size_t j = 0; j < I4.dim2(); ++j)
//      {
//        slip::same_convolution(I4.col_begin(j,rangej),I4.col_end(j,rangej),
// 			      m3.begin(),m3.end(),1,1,
//  			      I5.col_begin(j),
//  			      slip::BORDER_TREATMENT_CIRCULAR);
//      }

//    slip::GrayscaleImage<float> I5bis(I5.dim1(),I5.dim2(),I5.begin(),I5.end());
  
//    slip::range_fun_inter01<float,float> fun01(I5bis.min(),I5bis.max());
//    slip::change_dynamic(I5bis.begin(),I5bis.end(),I5bis.begin(),fun01);
//     I5bis.write("lena_decimate.gif");
   
//     slip::Vector<float> Signal(16);
//     slip::Vector<float> result5(16);
//     slip::iota(Signal.begin(),Signal.end(),1.0,1.0);
//     slip::same_convolution(Signal.begin(),Signal.end(),
// 			   kernel.begin(),kernel.end(),
// 			   result5.begin(),
// 			   slip::BORDER_TREATMENT_ZERO_PADDED);    
//     std::cout<<"result5 = \n"<<result5<<std::endl;
//     slip::same_convolution(Signal,kernel,result5,slip::BORDER_TREATMENT_ZERO_PADDED);    
//     std::cout<<"result5 = \n"<<result5<<std::endl;
//     slip::same_convolution(Signal.begin(),Signal.end(),
// 			   kernel.begin(),kernel.end(),
// 			   result5.begin(),
// 			   slip::BORDER_TREATMENT_NEUMANN);    
//     std::cout<<"result5 = \n"<<result5<<std::endl;
//     slip::same_convolution(Signal,kernel,result5,slip::BORDER_TREATMENT_NEUMANN);    
//     std::cout<<"result5 = \n"<<result5<<std::endl;
//      slip::same_convolution(Signal.begin(),Signal.end(),
// 			   kernel.begin(),kernel.end(),
// 			    result5.begin(),
// 			   slip::BORDER_TREATMENT_AVOID);    
//     std::cout<<"result5 = \n"<<result5<<std::endl;
//     slip::same_convolution(Signal,kernel,result5,slip::BORDER_TREATMENT_AVOID);    
//     std::cout<<"result5 = \n"<<result5<<std::endl;
//      slip::same_convolution(Signal.begin(),Signal.end(),
// 			   kernel.begin(),kernel.end(),
// 			   result5.begin(),
// 			    slip::BORDER_TREATMENT_CIRCULAR);    
//     std::cout<<"result5 = \n"<<result5<<std::endl;
//     slip::same_convolution(Signal,kernel,result5,slip::BORDER_TREATMENT_CIRCULAR);
//     std::cout<<"result5 = \n"<<result5<<std::endl;
    //---------------------------------------------------------
    //convolution2d 
    //---------------------------------------------------------
  
//   slip::GrayscaleImage<float> Result(I.rows(),I.cols());
//  slip::block<float,3> k1 = {1.0, 0.0, -1.0};
//  slip::block<float,3> k2 = {1.0,2.0,1.0};
//   slip::block<float,5> k1 = {1.0/16.0,4.0/16.0,6.0/16.0,4.0/16,1.0/16};
//  slip::block<float,5> k2 = k1;
 //  slip::block<float,3> k1 = {1.0/4.0, 2.0/4.0, 1.0/4.0};
//   slip::block<float,3> k2 = {1.0/4.0,2.0/4.0,1.0/4.0};
 //std::size_t size = k1.size()/2;
//  slip::separable_convolution2d(I.upper_left(),I.bottom_right(),
// 				   k1.begin(),k1.end(),
// 				   size,size,
// 				   slip::BORDER_TREATMENT_NEUMANN,
// 				   k2.begin(),k2.end(),
// 				   size,size,
// 				   slip::BORDER_TREATMENT_NEUMANN,
// 				   Result.upper_left());

//  slip::separable_convolution2d(I.upper_left(),I.bottom_right(),
// 				   k1.begin(),k1.end(),
// 				   k2.begin(),k2.end(),
// 				   Result.upper_left(),
//                              slip::BORDER_TREATMENT_NEUMANN,);

//  slip::separable_convolution2d(I,
// 				   k1,
// 				   k2,
// 				   Result,
// 				   slip::BORDER_TREATMENT_NEUMANN);


 // slip::separable_fft_convolution2d(I.upper_left(),I.bottom_right(),
// 				   k1.begin(),k1.end(),
// 				   k2.begin(),k2.end(),
// 				   Result.upper_left());
//  slip::separable_fft_convolution2d(I,
// 				   k1,k2,
// 				   Result);

 // Result.write("Result.png");

 slip::Volume<float> V3(16,16,16);
 slip::Volume<float> V3r(16,16,16);
 slip::iota(V3.begin(),V3.end(),1.0f);
 slip::block<float,3> ke1 = {0.25f, 0.5f, 0.25f};
 slip::block<float,3> ke2 = {0.25f, 0.5f, 0.25f};
 slip::block<float,3> ke3 = {0.5f, 0.0f, -0.5f};
 slip::separable_convolution3d(V3.front_upper_left(),
			       V3.back_bottom_right(),
			       ke1.begin(),ke1.end(),1,1,
			       slip::BORDER_TREATMENT_ZERO_PADDED,
			       ke2.begin(),ke2.end(),1,1,
			       slip::BORDER_TREATMENT_ZERO_PADDED,
			       ke3.begin(),ke3.end(),1,1,
			       slip::BORDER_TREATMENT_ZERO_PADDED,
			       V3r.front_upper_left());
 
 std::cout<<V3r<<std::endl;

 slip::separable_convolution3d(V3.front_upper_left(),
			       V3.back_bottom_right(),
			       ke1.begin(),ke1.end(),
			       ke2.begin(),ke2.end(),
			       ke3.begin(),ke3.end(),
			       V3r.front_upper_left(),
			       slip::BORDER_TREATMENT_ZERO_PADDED);
 
 std::cout<<V3r<<std::endl;

 slip::separable_convolution3d(V3,
			       ke1,ke2,ke3,
			       V3r,
			       slip::BORDER_TREATMENT_ZERO_PADDED);
 
 std::cout<<V3r<<std::endl;

  slip::separable_fft_convolution3d(V3.front_upper_left(),
				    V3.back_bottom_right(),
				    ke1.begin(),ke1.end(),
				    ke2.begin(),ke2.end(),
				    ke3.begin(),ke3.end(),
				    V3r.front_upper_left());
 
 std::cout<<V3r<<std::endl;

 slip::separable_fft_convolution3d(V3,
				   ke1,
				   ke2,
				   ke3,
				   V3r);
 
 std::cout<<V3r<<std::endl;


 slip::Array2d<float> A(8,10);
 slip::iota(A.begin(),A.end(),1.0);
 std::cout<<"A = \n"<<A<<std::endl;
 slip::Array2d<float> Mask2d(4,4);
 slip::iota(Mask2d.begin(),Mask2d.end(),10.0,1.0);
 std::cout<<"Mask = \n"<<Mask2d<<std::endl;

  std::size_t convA_rows = A.rows()+ Mask2d.rows()-static_cast<std::size_t>(1);
  std::size_t convA_cols = A.cols()+ Mask2d.cols()-static_cast<std::size_t>(1);
 
 slip::Array2d<float> ConvA(convA_rows,convA_cols);
 slip::fft_convolution2d(A.upper_left(),A.bottom_right(),
			 Mask2d.upper_left(),Mask2d.bottom_right(),
			 ConvA.upper_left(),ConvA.bottom_right());
 std::cout<<"ConvA = \n"<<ConvA<<std::endl;
 slip::fft_convolution2d(A,Mask2d,ConvA);
 std::cout<<"ConvA = \n"<<ConvA<<std::endl;
 
 std::cout<<ConvA.rows()<<","<<ConvA.cols()<<std::endl;

 std::cout<<"valid convolution2d "<<std::endl;
 slip::Array2d<float> ConvAv(A.rows()-Mask2d.rows()+1,A.cols() - Mask2d.cols()+1);
 std::cout<<ConvAv.rows()<<"x"<<ConvAv.cols()<<std::endl;

 slip::valid_convolution2d(A.upper_left(),A.bottom_right(),
			   Mask2d.upper_left(),Mask2d.bottom_right(),
			   2,1,
			   1,2,
			   ConvAv.upper_left(),ConvAv.bottom_right());
 std::cout<<"ConvAv = \n"<<ConvAv<<std::endl;

  std::cout<<"same convolution2d "<<std::endl;
  slip::Array2d<float> ConvAs(A.rows(),A.cols());
//  //std::cout<<ConvAs.rows()<<"x"<<ConvAs.cols()<<std::endl;

 slip::same_convolution2d(A.upper_left(),A.bottom_right(),
			  Mask2d.upper_left(),Mask2d.bottom_right(),
			  2,1,
			  1,2,
			  ConvAs.upper_left(),ConvAs.bottom_right(),
			   slip::BORDER_TREATMENT_ZERO_PADDED);
 std::cout<<"ConvAs ZERO_PADDED = \n"<<ConvAs<<std::endl;


 slip::same_convolution2d(A.upper_left(),A.bottom_right(),
			  Mask2d.upper_left(),Mask2d.bottom_right(),
			  ConvAs.upper_left(),ConvAs.bottom_right(),
			  slip::BORDER_TREATMENT_ZERO_PADDED);
 std::cout<<"ConvAs ZERO_PADDED without ksize_left... = \n"<<ConvAs<<std::endl;

 ConvAs.fill(0.0f);
 slip::same_convolution2d(A.upper_left(),A.bottom_right(),
			  Mask2d.upper_left(),Mask2d.bottom_right(),
			  1,2,
			  2,1,
			  ConvAs.upper_left(),ConvAs.bottom_right(),
			  slip::BORDER_TREATMENT_NEUMANN);
 std::cout<<"ConvAs NEUMANN 1,2, 2,1 = \n"<<ConvAs<<std::endl;

 ConvAs.fill(0.0f);
 slip::same_convolution2d(A.upper_left(),A.bottom_right(),
			  Mask2d.upper_left(),Mask2d.bottom_right(),
			  2,1,
			  1,2,
			  ConvAs.upper_left(),ConvAs.bottom_right(),
			  slip::BORDER_TREATMENT_NEUMANN);
 std::cout<<"ConvAs NEUMANN 2,1, 1,2 = \n"<<ConvAs<<std::endl;

 ConvAs.fill(0.0f);
 slip::same_convolution2d(A.upper_left(),A.bottom_right(),
			  Mask2d.upper_left(),Mask2d.bottom_right(),
			  2,1,
			  1,2,
			  ConvAs.upper_left(),ConvAs.bottom_right(),
			  slip::BORDER_TREATMENT_CIRCULAR);
 std::cout<<"ConvAs CIRCULAR= \n"<<ConvAs<<std::endl;

 slip::Array2d<float> Mask2d2(3,3);
 slip::iota(Mask2d2.begin(),Mask2d2.end(),10.0,1.0);
 std::cout<<"Mask2d2 = \n"<<Mask2d2<<std::endl;

 ConvAs.fill(0.0f);
 slip::same_convolution2d(A.upper_left(),A.bottom_right(),
			  Mask2d2.upper_left(),Mask2d2.bottom_right(),
			  ConvAs.upper_left(),ConvAs.bottom_right(),
			  slip::BORDER_TREATMENT_ZERO_PADDED);
 std::cout<<"ConvAs ZERO_PADDED symmetric mask = \n"<<ConvAs<<std::endl;

 ConvAs.fill(0.0f);
 slip::same_convolution2d(A.upper_left(),A.bottom_right(),
			  Mask2d2.upper_left(),Mask2d2.bottom_right(),
			  ConvAs.upper_left(),ConvAs.bottom_right(),
			  slip::BORDER_TREATMENT_NEUMANN);
 std::cout<<"ConvAs NEUMANN symmetric mask = \n"<<ConvAs<<std::endl;

 ConvAs.fill(0.0f);
 slip::same_convolution2d(A.upper_left(),A.bottom_right(),
			  Mask2d2.upper_left(),Mask2d2.bottom_right(),
			  ConvAs.upper_left(),ConvAs.bottom_right(),
			  slip::BORDER_TREATMENT_CIRCULAR);
 std::cout<<"ConvAs CIRCULAR symmetric mask = \n"<<ConvAs<<std::endl;


 ConvAs.fill(0.0f);
 slip::same_convolution2d(A,Mask2d2,ConvAs,
			  slip::BORDER_TREATMENT_ZERO_PADDED);
 std::cout<<"ConvAs ZERO_PADDED symmetric mask = \n"<<ConvAs<<std::endl;

 ConvAs.fill(0.0f);
 slip::same_convolution2d(A,Mask2d2,ConvAs,
			  slip::BORDER_TREATMENT_NEUMANN);
 std::cout<<"ConvAs NEUMANN symmetric mask = \n"<<ConvAs<<std::endl;

 ConvAs.fill(0.0f);
 slip::same_convolution2d(A,Mask2d2,ConvAs,
			  slip::BORDER_TREATMENT_CIRCULAR);
 std::cout<<"ConvAs CIRCULAR symmetric mask = \n"<<ConvAs<<std::endl;


 slip::Array2d<float> ConvAfull(A.rows()+Mask2d.rows()-1,
				A.cols()+Mask2d.cols()-1);
 slip::full_convolution2d(A.upper_left(),A.bottom_right(),
			  Mask2d.upper_left(),Mask2d.bottom_right(),
			  ConvAfull.upper_left(),ConvAfull.bottom_right());
 std::cout<<"ConvAfull = \n"<<ConvAfull<<std::endl;

 
 
 slip::Array3d<float> A3(4,8,10);
 slip::iota(A3.begin(),A3.end(),1.0);
 std::cout<<"A3 = \n"<<A3<<std::endl;
 slip::Array3d<float> Mask3d(2,3,4);
 slip::iota(Mask3d.begin(),Mask3d.end(),10.0,1.0);
 std::cout<<"Mask3d = \n"<<Mask3d<<std::endl;
 std::size_t convA3_slices = A3.slices()+ Mask3d.slices()-static_cast<std::size_t>(1);
 std::size_t convA3_rows = A3.rows()+ Mask3d.rows()-static_cast<std::size_t>(1);
 std::size_t convA3_cols = A3.cols()+ Mask3d.cols()-static_cast<std::size_t>(1);								
 std::cout<<"slices = "<<convA3_slices<<" "<<convA3_rows<<" "<<convA3_cols<<std::endl;
 slip::Array3d<float> ConvA3(convA3_slices,convA3_rows,convA3_cols);
 slip::fft_convolution3d(A3,Mask3d,ConvA3);
 
 std::cout<<"ConvA3 = \n"<<ConvA3<<std::endl;
 


 std::cout<<"valid convolution3d "<<std::endl;
 slip::Array3d<float> ConvAv3(A3.slices() - Mask3d.slices() + 1,
			     A3.rows()   - Mask3d.rows()   + 1,
			     A3.cols()   - Mask3d.cols()   + 1);
 std::cout<<ConvAv3.slices()<<"x"<<ConvAv3.rows()<<"x"<<ConvAv3.cols()<<std::endl;

 slip::valid_convolution3d(A3.front_upper_left(),A3.back_bottom_right(),
			   Mask3d.front_upper_left(),Mask3d.back_bottom_right(),
			   1,0,
			   2,1,
			   0,2,
			   ConvAv3.front_upper_left(),ConvAv3.back_bottom_right());
 std::cout<<"ConvAv3 = \n"<<ConvAv3<<std::endl;
 
 
}
