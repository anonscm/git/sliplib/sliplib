#include <iostream>
#include <complex>
#include <limits>
#include "linear_algebra_traits.hpp"




int main()
{

  std::complex<float> c(2.0,1.0);
  float f = 4.0;
  std::cout<<"__is_complex<float> : "<<slip::__is_complex<float>::__val<<std::endl;
  std::cout<<"__is_complex<double> : "<<slip::__is_complex<double>::__val<<std::endl;
  std::cout<<"__is_complex<std::complex<float> > : "<<slip::__is_complex<std::complex<float> >::__val<<std::endl;
  std::cout<<"__is_complex<std::complex<double> > : "<<slip::__is_complex<std::complex<double> >::__val<<std::endl;
  std::cout<<"__is_complex<std::complex<long double> > : "<<slip::__is_complex<std::complex<long double> >::__val<<std::endl;


  std::cout<<"epsilon = "<<slip::epsilon<std::complex<float> >()<<std::endl;
  std::cout<<"epsilon = "<<slip::epsilon<double>()<<std::endl;
  

  std::cout<<"is_complex = "<<slip::is_complex(std::complex<float>())<<std::endl;
  std::cout<<"is_complex = "<<slip::is_complex(std::complex<double>())<<std::endl;
 std::cout<<"is_complex = "<<slip::is_complex(std::complex<long double>())<<std::endl;
  std::cout<<"is_complex = "<<slip::is_complex(float())<<std::endl;
  std::cout<<"is_complex = "<<slip::is_complex(double())<<std::endl;
  
  
  std::cout<<"is_real = "<<slip::is_real(std::complex<float>())<<std::endl;
  std::cout<<"is_real = "<<slip::is_real(std::complex<double>())<<std::endl;
  std::cout<<"is_real = "<<slip::is_real(std::complex<long double>())<<std::endl;
  std::cout<<"is_real = "<<slip::is_real(float())<<std::endl;
  std::cout<<"is_real = "<<slip::is_real(double())<<std::endl;
  //std::cout<<"is_real = "<<slip::is_real((long double)())<<std::endl;
  std::cout<<"is_real("<<c<<") = "<<slip::is_real(c)<<std::endl;
  std::cout<<"is_real("<<f<<") = "<<slip::is_real(f)<<std::endl;
  std::cout<<"is_complex("<<c<<") = "<<slip::is_complex(c)<<std::endl;
  std::cout<<"is_complex("<<f<<") = "<<slip::is_complex(f)<<std::endl;
  return 0;
}
