#include <iostream>
#include <iomanip>
#include "Array.hpp"
#include "poly_weight_functors.hpp"
#include "polynomial_algo.hpp"

int main()
{
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "legendre weight " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<double> legendre_w(10);
  slip::Array<double> x(10);
  slip::uniform_collocations(x.begin(),x.end(),-1.0,1.0);
  std::transform(x.begin(),x.end(),
		 legendre_w.begin(),
		 slip::LegendreWeight<double>());
  std::cout<<"x = \n"<<x<<std::endl;
  std::cout<<"legendre_w = \n"<<legendre_w<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Gram weight " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<double> gram_w(10);
  slip::GramWeight<double> GramW(x.size());
  slip::gram_uniform_collocations(x.begin(),x.end(),-1.0,1.0);
  std::transform(x.begin(),x.end(),
		 gram_w.begin(),
		 GramW);
   std::cout<<"gram_w = \n"<<gram_w<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "chebyshev (first kind) weight " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<double> chebyshev_w(10);
  std::transform(x.begin(),x.end(),
		 chebyshev_w.begin(),
		 slip::ChebyshevWeight<double>());
  std::cout<<"x = \n"<<x<<std::endl;
  std::cout<<"chebyshev_w = \n"<<chebyshev_w<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "chebyshev (second kind) weight " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<double> chebyshevII_w(10);
  std::transform(x.begin(),x.end(),
		 chebyshevII_w.begin(),
		 slip::ChebyshevIIWeight<double>());
  std::cout<<"x = \n"<<x<<std::endl;
  std::cout<<"chebyshevII_w = \n"<<chebyshevII_w<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "laguerre weight " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<double> laguerre_w(10);
  std::transform(x.begin(),x.end(),
		 laguerre_w.begin(),
		 slip::LaguerreWeight<double>());
  std::cout<<"x = \n"<<x<<std::endl;
  std::cout<<"laguerre_w = \n"<<laguerre_w<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "hermite weight " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<double> hermite_w(10);
  std::transform(x.begin(),x.end(),
		 hermite_w.begin(),
		 slip::HermiteWeight<double>());
  std::cout<<"x = \n"<<x<<std::endl;
  std::cout<<"hermite_w = \n"<<hermite_w<<std::endl;
  return 0;
}

