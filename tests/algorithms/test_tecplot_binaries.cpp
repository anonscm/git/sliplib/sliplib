#include <iostream>
#include "RegularVector3dField3d.hpp"
#include "RegularVector2dField2d.hpp"
#include "tecplot_binaries.hpp"
#include <typeinfo>



int main()
//int main(int argc, char* argv[])
{
  slip::RegularVector3dField3d<double> reg,reg2;
  std::string name="testXYZ_double.plt", title, zone;
  std::vector<std::string> varnames;
  std::cout<<"Read plt"<<std::endl;
  slip::plt_to_RegularVector3dField3d(name, reg, title, zone, varnames);
  reg.write_tecplot("test_read2.dat","titre","zone");
	
  std::cout<<"Write plt => "<<title<<"\t"<<zone<<std::endl;
  std::string outname="test_write_plt.plt";
	slip::RegularVector3dField3d_to_plt(outname, reg, title, zone);
	
	std::cout<<"Read Array"<<std::endl;
	slip::Array2d<double> data;
	slip::plt_to_Array2d(name,data);
	size_t dim1=data.dim1(), dim2=data.dim2();//, dim3=1;
	
	//		std::size_t size =  data.rows();
	//	std::size_t size_xy = std::count(data.col_begin(2),data.col_end(2),data[0][2]);
		//		std::size_t size_z = size / size_xy;
		std::size_t d=0, du=0;
		const size_t nb_points=dim1, nb_vars=dim2;
		std::cout<<"nb_vars="<<nb_vars<<"\tnb_points="<<nb_points<<std::endl;
		if (nb_vars==4 || ( nb_vars==5 && d==1 ) ) du=1;
		else if (nb_vars!=4 && nb_vars!=5) du=1;
		std::vector<slip::Point3d<double> > datax(nb_points), datay(nb_points);
		for (size_t i=0; i<nb_points; ++i)
		{
			datax[i][0]=data(i,0);
			datax[i][1]=data(i,1);
			if (d==0) datax[i][2]=data(i,2);
			else datax[i][2]=0.;
			datay[i][0]=data(i,3-d);
			datay[i][1]=data(i,4-d);
			if (du==0) datay[i][2]=data(i,5-d);
			else datay[i][2]=0.;
		}
		double xmin, xmax, ymin, ymax, zmin, zmax;
		xmin=(*min_element(datax.begin(),datax.end(),slip::GenericComparator<slip::Point3d<double> >(0)))[0];
		xmax=(*max_element(datax.begin(),datax.end(),slip::GenericComparator<slip::Point3d<double> >(0)))[0];
		ymin=(*min_element(datax.begin(),datax.end(),slip::GenericComparator<slip::Point3d<double> >(1)))[1];
		ymax=(*max_element(datax.begin(),datax.end(),slip::GenericComparator<slip::Point3d<double> >(1)))[1];
		zmin=(*min_element(datax.begin(),datax.end(),slip::GenericComparator<slip::Point3d<double> >(2)))[2];
		zmax=(*max_element(datax.begin(),datax.end(),slip::GenericComparator<slip::Point3d<double> >(2)))[2];
		
		std::cout<<xmin<<"\t"<<xmax<<"\t"<<ymin<<"\t"<<ymax<<"\t"<<zmin<<"\t"<<zmax<<std::endl;
		
		std::cout<<std::endl<<"**************************************"<<std::endl;
		std::cout<<"ok"<<std::endl;
}
