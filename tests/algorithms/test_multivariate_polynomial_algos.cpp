#include <iostream>
#include <iomanip>
#include <cmath>

#include "Block.hpp"
#include "Matrix.hpp"
#include "multivariate_polynomial_algos.hpp"
#include "PolySupport.hpp"
#include "MultivariatePolynomial.hpp"
#include "polynomial_algo.hpp"

template <typename RandomAccessIterator,
	  typename Matrix,
	  class InnerProduct>
void orthogonality_matrix(RandomAccessIterator first,
			  RandomAccessIterator last,
			  Matrix& Gram,
			  InnerProduct& inner_product)
  {
   
    const std::size_t size_base = static_cast<std::size_t>(last-first);
    Gram.resize(size_base,size_base);
    std::size_t i = 0;
    for(auto it1 = first; it1 != last; ++it1, ++i)
      {
	std::size_t j = i;
	for(auto it2 = it1; it2 != last; ++it2, ++j)
  	  {
	    std::cout<<"(it1,it2) = "<<*it1<<","<<*it2<<std::endl;
  	    Gram[i][j] =  inner_product(*it1,*it2);
  	    Gram[j][i] = Gram[i][j];
  	  }
      }
  }

int main()
{

  

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Generate x1 2d Legendre basis " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::MultivariatePolynomial<double,2> pl0;
   slip::Monomial<2> ml00;
   ml00.powers[0] = 0;
   ml00.powers[1] = 0;
   //pl0.insert(ml00,1.0/std::sqrt(2.0));
   pl0.insert(ml00,1.0);
   slip::MultivariatePolynomial<double,2> pl1;
   slip::Monomial<2> ml10;
   ml10.powers[0] = 1;
   ml10.powers[1] = 0;
   //pl1.insert(ml10,std::sqrt(3.0)/2.0);
   pl1.insert(ml10,1.0);
   std::vector<slip::MultivariatePolynomial<double,2> > legendre_x1_basis_2d;
   legendre_x1_basis_2d.push_back(pl0);
   legendre_x1_basis_2d.push_back(pl1);
    for(unsigned l = 1; l < 3; ++l)
      {
	legendre_x1_basis_2d.push_back(slip::legendre_nd_next<slip::MultivariatePolynomial<double,2> >(l, 
													    pl1, 
													    legendre_x1_basis_2d[l], 
													    legendre_x1_basis_2d[l-1]));
      }
    
    for(unsigned l = 0; l < legendre_x1_basis_2d.size(); ++l)
      {
	std::cout<<legendre_x1_basis_2d[l]<<std::endl;
      }

    slip::MultivariatePolynomial<double,2> P20 =  legendre_x1_basis_2d[2]* legendre_x1_basis_2d[0];

    std::cout<<"P20 = "<<P20<<std::endl;
    slip::block<slip::PolySupport<double>,2 > lstIntervals;
    lstIntervals[0] = slip::PolySupport<double>(-1.0,1.0);
    lstIntervals[1] = lstIntervals[0];
    std::cout<<"slip::integral(P20) "<<slip::integral(P20,lstIntervals)<<std::endl;
    slip::MultivariatePolynomial<double,2> P11 =  legendre_x1_basis_2d[1]* legendre_x1_basis_2d[1];
    std::cout<<"P11 = "<<P11<<std::endl;
     std::cout<<"slip::integral(P11) "<<slip::integral(P11,lstIntervals)<<std::endl;


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "LegendreInnerProduct " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::LegendreInnerProduct<double,2> inner_prod;

   std::cout<<"inner_prod(P00,P01) = "<<inner_prod(legendre_x1_basis_2d[0],legendre_x1_basis_2d[1])<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "OrthogonalityMatrix " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Matrix<double> Gram;
   
   orthogonality_matrix(legendre_x1_basis_2d.begin(),
			legendre_x1_basis_2d.end(),
			Gram,
			inner_prod);

   std::cout<<"Gram = \n"<<Gram<<std::endl;
   
   return 0;
}
