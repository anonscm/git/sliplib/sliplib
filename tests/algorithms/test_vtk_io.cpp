#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include "Vector.hpp"
#include "Vector2d.hpp"
#include "Vector3d.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "DenseVector3dField3d.hpp"
#include "DenseVector2dField2d.hpp"
#include "RegularVector3dField3d.hpp"
#include "RegularVector2dField2d.hpp"
#include "GrayscaleImage.hpp"
#include "vtk_io.hpp"
//#include "./../slipalgo/netcdf_io.hpp"


int main()
{
 

   ////////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "read/write 3d containers using vtk file format " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   
   slip::Array3d<double> A3d(2,5,6);
   slip::iota(A3d.begin(),A3d.end(),2.0,1.0);
   std::cout<<A3d<<std::endl;
  
   slip::write_vtk_3d_binary(A3d,"A3d.vtk");
   slip::write_vtk_3d(A3d,"A3d_ascii.vtk");
   slip::write_raw(A3d.begin(),A3d.end(),"A3d.raw");

 
   slip::Array3d<float> A3f(2,5,6);
   slip::iota(A3f.begin(),A3f.end(),2.0f,1.0f);
   std::cout<<A3f<<std::endl;
   slip::write_vtk_3d_binary(A3f,"A3f.vtk");
   slip::write_vtk_3d(A3f,"A3f_ascii.vtk");

   slip::Array3d<int> A3i(2,5,6);
   slip::iota(A3i.begin(),A3i.end(),2,1);
   std::cout<<A3i<<std::endl;

   slip::write_vtk_3d_binary(A3i,"A3i.vtk");
   slip::write_vtk_3d(A3i,"A3i_ascii.vtk");

   slip::Array3d<char> A3c(2,5,6);
   slip::iota(A3c.begin(),A3c.end(),2,1);
   std::cout<<A3c<<std::endl;

   slip::write_vtk_3d_binary(A3c,"A3c.vtk");
   slip::write_vtk_3d(A3c,"A3c_ascii.vtk");

   slip::Array3d<unsigned char> A3uc(2,5,6);
   slip::iota(A3uc.begin(),A3uc.end(),2,1);
   std::cout<<A3uc<<std::endl;

  slip::write_vtk_3d_binary(A3uc,"A3uc.vtk");
   slip::write_vtk_3d(A3uc,"A3uc_ascii.vtk");

   slip::Array3d<long> A3l(2,5,6);
   slip::iota(A3l.begin(),A3l.end(),2,1);
   std::cout<<A3l<<std::endl;

    slip::write_vtk_3d_binary(A3l,"A3l.vtk");
   slip::write_vtk_3d(A3l,"A3l_ascii.vtk");

   slip::Array3d<unsigned long> A3ul(2,5,6);
   slip::iota(A3ul.begin(),A3ul.end(),2,1);
   std::cout<<A3ul<<std::endl;

    slip::write_vtk_3d_binary(A3ul,"A3ul.vtk");
   slip::write_vtk_3d(A3ul,"A3ul_ascii.vtk");

   slip::Array3d<short> A3s(2,5,6);
   slip::iota(A3s.begin(),A3s.end(),2,1);
   std::cout<<A3s<<std::endl;

   slip::write_vtk_3d_binary(A3s,"A3s.vtk");
   slip::write_vtk_3d(A3s,"A3s_ascii.vtk");

    slip::Array3d<unsigned short> A3us(2,5,6);
   slip::iota(A3us.begin(),A3us.end(),2,1);
   std::cout<<A3us<<std::endl;

    slip::write_vtk_3d_binary(A3us,"A3us.vtk");
   slip::write_vtk_3d(A3us,"A3us_ascii.vtk");


   slip::Array3d<bool> A3b(2,5,6,false);
   A3b[0][1][1] = true;
   A3b[0][2][2] = true;
   A3b[0][3][3] = true;
   A3b[0][4][4] = true;
   A3b[1][1][2] = true;
   A3b[1][2][3] = true;
   A3b[1][3][4] = true;
   A3b[1][4][5] = true;
   std::cout<<A3b<<std::endl;

   slip::write_vtk_3d_binary(A3b,"A3b.vtk");
   slip::write_vtk_3d(A3b,"A3b_ascii.vtk");

   std::cout<<"is_big_endian = "<<slip::is_big_endian()<<std::endl;
   std::cout<<"is_little_endian = "<<slip::is_little_endian()<<std::endl;
   std::cout<<"sizeof(long) = "<<(sizeof(long))<<std::endl;
   std::cout<<"sizeof(double) = "<<(sizeof(double))<<std::endl;
    std::cout<<"sizeof(float) = "<<(sizeof(float))<<std::endl;
   std::cout<<"sizeof(int) = "<<(sizeof(int))<<std::endl;


   ////////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "read/write 3d vector fields using vtk file format " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   
   slip::DenseVector3dField3d<double> DVF3d(2,5,6);
   slip::iota(DVF3d.begin(),DVF3d.end(),
	      slip::Vector3d<double>(1.0,2.0,3.0),
	      slip::Vector3d<double>(1.0,1.0,1.0));
   std::cout<<"DVF3d = \n"<<DVF3d<<std::endl;
   slip::write_vtk_vect3d(DVF3d,"DVF3d_ascii.vtk");
  slip::write_vtk_vect3d_binary(DVF3d,"DVF3d.vtk");

  slip::Point3d<double> init_point3d(1.5,2.5,3.0);
  slip::Point3d<double> step3d(0.5,1.0,2.0);
  
  slip::RegularVector3dField3d<double> RVF3d(2,5,6,init_point3d,step3d);
  slip::iota(RVF3d.begin(),RVF3d.end(),
	      slip::Vector3d<double>(1.0,2.0,3.0),
	      slip::Vector3d<double>(1.0,1.0,1.0));
   std::cout<<"RVF3d = \n"<<RVF3d<<std::endl;
   slip::write_vtk_vect3d_grid(RVF3d,"RVF3d_ascii.vtk");
   slip::write_vtk_vect3d_grid_binary(RVF3d,"RVF3d.vtk");
   //slip::write_netcdf_multi_3d(RVF3d,"RVF3d.nc");
    ////////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "read/write 2d vector fields using vtk file format " << std::endl;  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   
   slip::DenseVector2dField2d<double> DVF2d(5,6);
   slip::iota(DVF2d.begin(),DVF2d.end(),
	      slip::Vector2d<double>(1.0,2.0),
	      slip::Vector2d<double>(1.0,1.0));
   std::cout<<"DVF2d = \n"<<DVF2d<<std::endl;
   slip::write_vtk_vect2d(DVF2d,"DVF2d_ascii.vtk");
   slip::write_vtk_vect2d_binary(DVF2d,"DVF2d.vtk");

   ////////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "read/write 2d vector fields using vtk file format " << std::endl;  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   
   slip::Point2d<double> init_point2d(1.5,2.5);
   slip::Point2d<double> step2d(0.5,1.0);
   
   slip::RegularVector2dField2d<double> RVF2d(5,6,init_point2d,step2d);
   slip::iota(RVF2d.begin(),RVF2d.end(),
	      slip::Vector2d<double>(1.0,2.0),
	      slip::Vector2d<double>(1.0,1.0));
   std::cout<<"RVF2d = \n"<<RVF2d<<std::endl;
   slip::write_vtk_vect2d_grid(RVF2d,"RVF2d_ascii.vtk");
   slip::write_vtk_vect2d_grid_binary(RVF2d,"RVF2d.vtk");
   //slip::write_netcdf_multi_2d(RVF2d,"RVF2d.nc");
    ////////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "read/write 2d container using vtk file format " << std::endl;  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   
   slip::GrayscaleImage<double> I2d(5,6);
   slip::iota(I2d.begin(),I2d.end(),1.0,1.0);
   std::cout<<"I2d = \n"<<I2d<<std::endl;
   slip::write_vtk_2d(I2d,"I2d_ascii.vtk");
   slip::write_vtk_2d_binary(I2d,"I2d.vtk");
   slip::write_image_surface_vtk(I2d,"I2dsurf.vtk");
  
   
   slip::GrayscaleImage<double> Lena;
   Lena.read("./../../../../tests/images/lena_gray.gif");
   slip::write_image_surface_vtk(Lena,"Lenasurf.vtk");
   slip::write_vtk_2d_binary(Lena,"Lena.vtk");
   // slip::GrayscaleImage<unsigned char> Lena;
   // Lena.read("lena.pgm");
   // slip::write_image_surface_vtk(Lena,"Lenasurf.vtk");
  
   

  return 0;
}
