#include "FFT.hpp"
#include "Array.hpp"
#include <complex>
#include <iostream>
#include <vector>

#include "GrayscaleImage.hpp"
#include "dynamic.hpp"

int main()
{
  slip::GrayscaleImage<double> I;
  I.read("../images/lena_gray.gif");
  
  //fft2d
  slip::Matrix<std::complex<double> > IFFT(I.dim1(),I.dim2());
  slip::Matrix<std::complex<double> > IOut(I.dim1(),I.dim2());
  slip::real_split_radix_fft2d(I,IFFT);
  slip::GrayscaleImage<double> INorm(IFFT.dim1(),IFFT.dim2());
  //log(magnitude)
  for(size_t i = 0; i < IFFT.dim1(); ++i)
    {
      for(size_t j = 0; j < IFFT.dim2(); ++j)
	{
	  INorm[i][j] = std::log(1.0+std::abs(IFFT[i][j]));
	}
    }
  slip::fftshift2d(INorm.upper_left(),INorm.bottom_right());
  slip::change_dynamic_01(INorm.begin(),INorm.end(),INorm.begin(),slip::AFFINE_FUNCTION);
  INorm.write("lena_split_radix_fft.gif");
  
  //ifft2d
  slip::split_radix_ifft2d(IFFT,IOut);
  
  for(size_t i = 0; i < IOut.dim1(); ++i)
    {
      for(size_t j = 0; j < IOut.dim2(); ++j)
	{
	  INorm[i][j] = std::abs(IOut[i][j]);
	}
    }
  
  slip::change_dynamic_01(INorm.begin(),INorm.end(),INorm.begin(),slip::AFFINE_FUNCTION);
  INorm.write("lena_split_radix_fft_rec.gif");
  
  //std::transform(I2.begin(),I2.end(),Norm.begin(),std::ptr_fun(std::norm));
  return 0;
}

