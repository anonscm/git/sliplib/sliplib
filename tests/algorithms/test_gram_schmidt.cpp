#include <iostream>
#include <vector>
#include "Vector.hpp"
#include "Matrix.hpp"
#include "MultivariatePolynomial.hpp"
#include "Polynomial.hpp"
#include "gram_schmidt.hpp"
#include "linear_algebra.hpp"

template<class T> struct inner_prod : public std::binary_function<slip::Vector<T>, slip::Vector<T>,T>
{
  inner_prod(){}
  T operator() (const slip::Vector<T>& x, const slip::Vector<T>& y)
  {
    slip::Vector<T> z = x * y;
    return z.sum();
  }
};

template<class T>
struct inner_prod_poly2 : public std::binary_function<slip::Polynomial<T>,
						     slip::Polynomial<T>,T>
{
  inner_prod_poly2(){}
  T operator() (const slip::Polynomial<T>& x, const slip::Polynomial<T>& y)
  {
    slip::Polynomial<T> z = x * y;
    z.integral();
    return z.evaluate(T(1)) - z.evaluate(T(-1));
  }
};

template<class T>
struct inner_prod_poly : public std::binary_function<slip::MultivariatePolynomial<T,2>,
						     slip::MultivariatePolynomial<T,2>,T>
{
  inner_prod_poly(){}
  T operator() (const slip::MultivariatePolynomial<T,2>& x, const slip::MultivariatePolynomial<T,2>& y)
  {
    slip::MultivariatePolynomial<T,2> z = x * y;
    z.integral(1);
    slip::MultivariatePolynomial<T,2> zint;
    zint =  (z.partial_evaluate(1,1) - z.partial_evaluate(1,-1));
    zint.integral(2);
    slip::block<T,2> b1;
    b1[0] = T(0);
    b1[1] = T(1);
    slip::block<T,2> b2;
    b2[0] = T(0);
    b2[1] = T(-1);
    return zint.evaluate(b1.begin(),b1.end()) - zint.evaluate(b2.begin(),b2.end());
  }
};

int main()
{

  //////////////////////////////////////////////////////////////////////
  //  Gram-Schmidt on a Vectorial Base                                //
  //////////////////////////////////////////////////////////////////////

  slip::Vector<double> e1(3,0.0f);
  slip::Vector<double> e2(3,0.0f);
  slip::Vector<double> e3(3,0.0f);

  e1[0] =  1.0; e1[1] =  1.0; e1[2] =   1.0; 
  e2[0] =  1.0; e2[1] = -1.0; e2[2] =   1.0; 
  e3[0] =  1.0; e3[1] =  1.0; e3[2] =  -1.0; 
  std::vector<slip::Vector<double> > init_base(3);
  init_base[0] = e1;
  init_base[1] = e2;
  init_base[2] = e3;
  
  std::cout<<"Initial Base  "<<std::endl;
  for(std::size_t i = 0; i < init_base.size();++i)
    {
      std::cout<<init_base[i]<<std::endl;
    }

  
  std::vector<slip::Vector<double> > ortho_base(3);
  slip::gram_schmidt_normalization(init_base.begin(),
				   init_base.end(),
				   ortho_base.begin(),
				   ortho_base.end(),
				   inner_prod<double>());
  
  std::cout<<"Orthonormalized Base  "<<std::endl;
  for(std::size_t i = 0; i < ortho_base.size();++i)
    {
      std::cout<<ortho_base[i]<<std::endl;
    }
  
  std::cout<<"Base verification"<<std::endl;
  slip::Matrix<double> Mortho(ortho_base.size(),ortho_base.size());
  slip::gram_matrix(ortho_base.begin(),ortho_base.end(),
		    Mortho.upper_left(),Mortho.bottom_right(),
		    inner_prod<double>());
  std::cout<<Mortho<<std::endl;

  slip::modified_gram_schmidt_normalization(init_base.begin(),
					    init_base.end(),
					    ortho_base.begin(),
					    ortho_base.end(),
					    inner_prod<double>());
  
  std::cout<<"Modified Orthonormalized Base  "<<std::endl;
  for(std::size_t i = 0; i < ortho_base.size();++i)
    {
      std::cout<<ortho_base[i]<<std::endl;
    }
  
  std::cout<<"Base verification"<<std::endl;
  slip::gram_matrix(ortho_base.begin(),ortho_base.end(),
		    Mortho.upper_left(),Mortho.bottom_right(),
		    inner_prod<double>());
  std::cout<<Mortho<<std::endl;


  slip::gram_schmidt_orthogonalization(init_base.begin(),
				       init_base.end(),
				       ortho_base.begin(),
				       ortho_base.end(),
				       inner_prod<double>());
  std::cout<<"Orthogonalized Base  "<<std::endl;
  for(std::size_t i = 0; i < ortho_base.size();++i)
    {
      std::cout<<ortho_base[i]<<std::endl;
    }
  
  std::cout<<"Base verification"<<std::endl;
  slip::gram_matrix(ortho_base.begin(),ortho_base.end(),
		    Mortho.upper_left(),Mortho.bottom_right(),
		    inner_prod<double>());  
  std::cout<<Mortho<<std::endl;

  slip::modified_gram_schmidt_orthogonalization(init_base.begin(),
						init_base.end(),
						ortho_base.begin(),
						ortho_base.end(),
						inner_prod<double>());
  std::cout<<"Modified Orthogonalized Base  "<<std::endl;
  for(std::size_t i = 0; i < ortho_base.size();++i)
    {
      std::cout<<ortho_base[i]<<std::endl;
    }
  
  std::cout<<"Base verification"<<std::endl;
  slip::gram_matrix(ortho_base.begin(),ortho_base.end(),
		    Mortho.upper_left(),Mortho.bottom_right(),
		    inner_prod<double>());  
  std::cout<<Mortho<<std::endl;


  //////////////////////////////////////////////////////////////////////
  //  Gram-Schmidt on a Polynomial Base                               //
  //////////////////////////////////////////////////////////////////////

  double d1[] = {1.0,0.0,0.0};
  slip::Polynomial<double> p01(2,d1);
  double d2[] = {0.0,1.0,0.0};
  slip::Polynomial<double> p02(2,d2);
  double d3[] = {0.0,0.0,1.0};
  slip::Polynomial<double> p03(2,d3);
  std::vector<slip::Polynomial<double> > init_base_poly2(3);
  init_base_poly2[0] = p01;
  init_base_poly2[1] = p02;
  init_base_poly2[2] = p03;
  std::cout<<"Initial Base  "<<std::endl;
  for(std::size_t i = 0; i < init_base_poly2.size();++i)
    {
      std::cout<<init_base_poly2[i]<<std::endl;
    }
  std::vector<slip::Polynomial<double> > ortho_base_poly2(3);
  
  gram_schmidt_normalization(init_base_poly2.begin(),
			     init_base_poly2.end(),
			     ortho_base_poly2.begin(),
			     ortho_base_poly2.end(),
			     inner_prod_poly2<double>());

  std::cout<<"Orthonormalized Base  "<<std::endl;
  for(std::size_t i = 0; i < ortho_base_poly2.size();++i)
    {
      std::cout<<ortho_base_poly2[i]<<std::endl;
    }
  
  std::cout<<"Base verification"<<std::endl;
 slip::Matrix<double> Mortho_poly2(ortho_base_poly2.size(),ortho_base_poly2.size());
 slip::gram_matrix(ortho_base_poly2.begin(),ortho_base_poly2.end(),
		  Mortho_poly2.upper_left(),Mortho_poly2.bottom_right(),
		    inner_prod_poly2<double>());
 std::cout<<Mortho_poly2<<std::endl;

 slip::modified_gram_schmidt_normalization(init_base_poly2.begin(),
					   init_base_poly2.end(),
					   ortho_base_poly2.begin(),
					   ortho_base_poly2.end(),
					   inner_prod_poly2<double>());

  std::cout<<"Orthonormalized Base  "<<std::endl;
  for(std::size_t i = 0; i < ortho_base_poly2.size();++i)
    {
      std::cout<<ortho_base_poly2[i]<<std::endl;
    }
  
std::cout<<"Base verification"<<std::endl;
slip::gram_matrix(ortho_base_poly2.begin(),ortho_base_poly2.end(),
		    Mortho_poly2.upper_left(),Mortho_poly2.bottom_right(),
		    inner_prod_poly2<double>());
std::cout<<Mortho_poly2<<std::endl; 

slip::gram_schmidt_orthogonalization(init_base_poly2.begin(),
				     init_base_poly2.end(),
				     ortho_base_poly2.begin(),
				     ortho_base_poly2.end(),
				     inner_prod_poly2<double>());

  std::cout<<"Orthogonalization Base  "<<std::endl;
  for(std::size_t i = 0; i < ortho_base_poly2.size();++i)
    {
      std::cout<<ortho_base_poly2[i]<<std::endl;
    }
  
  std::cout<<"Base verification"<<std::endl;
  slip::gram_matrix(ortho_base_poly2.begin(),ortho_base_poly2.end(),
		    Mortho_poly2.upper_left(),Mortho_poly2.bottom_right(),
		    inner_prod_poly2<double>());
  std::cout<<Mortho_poly2<<std::endl; 

  slip::modified_gram_schmidt_orthogonalization(init_base_poly2.begin(),
					       init_base_poly2.end(),
					       ortho_base_poly2.begin(),
					       ortho_base_poly2.end(),
					       inner_prod_poly2<double>());
  
  std::cout<<"Orthogonalization Base  "<<std::endl;
  for(std::size_t i = 0; i < ortho_base_poly2.size();++i)
    {
      std::cout<<ortho_base_poly2[i]<<std::endl;
    }
  
  std::cout<<"Base verification"<<std::endl;
  slip::gram_matrix(ortho_base_poly2.begin(),ortho_base_poly2.end(),
		    Mortho_poly2.upper_left(),Mortho_poly2.bottom_right(),
		    inner_prod_poly2<double>());
  std::cout<<Mortho_poly2<<std::endl; 

  //////////////////////////////////////////////////////////////////////
  //  Gram-Schmidt on a MultivariatePolynomial Base                   //
  //////////////////////////////////////////////////////////////////////
  
  slip::MultivariatePolynomial<double,2> p1;
  slip::Monomial<2> m11;
  m11.powers[0] = 0;
  m11.powers[1] = 0;
  p1.insert(m11,1.0);

  slip::MultivariatePolynomial<double,2> p2;
  slip::Monomial<2> m21;
  m21.powers[0] = 1;
  m21.powers[1] = 0;
  p2.insert(m21,1.0);

  slip::MultivariatePolynomial<double,2> p3;
  slip::Monomial<2> m31;
  m31.powers[0] = 0;
  m31.powers[1] = 1;
  p3.insert(m31,1.0);

  
  std::vector<slip::MultivariatePolynomial<double,2> > init_base_poly(3);
  init_base_poly[0] = p1;
  init_base_poly[1] = p2;
  init_base_poly[2] = p3;
  std::cout<<"Initial Base  "<<std::endl;
  for(std::size_t i = 0; i < init_base_poly.size();++i)
    {
      std::cout<<init_base_poly[i]<<std::endl;
    }

   for(std::size_t i = 0; i < init_base_poly.size();++i)
    {
      std::cout<<inner_prod_poly<double>()(init_base_poly[i],init_base_poly[i])<<std::endl;
    }
  


  std::vector<slip::MultivariatePolynomial<double,2> > ortho_base_poly(3);
  

   slip::gram_schmidt_normalization(init_base_poly.begin(),
				    init_base_poly.end(),
				    ortho_base_poly.begin(),
				    ortho_base_poly.end(),
				    inner_prod_poly<double>());

  std::cout<<"Orthonormalized Base  "<<std::endl;
  for(std::size_t i = 0; i < ortho_base_poly.size();++i)
    {
      std::cout<<ortho_base_poly[i]<<std::endl;
    }
  
  std::cout<<"Base verification"<<std::endl;
  slip::Matrix<double> Mortho_poly(ortho_base_poly.size(),ortho_base_poly.size());
  slip::gram_matrix(ortho_base_poly.begin(),ortho_base_poly.end(),
		    Mortho_poly.upper_left(),Mortho_poly.bottom_right(),
		    inner_prod_poly<double>());
  std::cout<<Mortho_poly<<std::endl; 


   slip::modified_gram_schmidt_normalization(init_base_poly.begin(),
					     init_base_poly.end(),
					     ortho_base_poly.begin(),
					     ortho_base_poly.end(),
					     inner_prod_poly<double>());
   
   std::cout<<"Modified Orthonormalized Base  "<<std::endl;
   for(std::size_t i = 0; i < ortho_base_poly.size();++i)
     {
      std::cout<<ortho_base_poly[i]<<std::endl;
     }
  
   std::cout<<"Base verification"<<std::endl;
   slip::gram_matrix(ortho_base_poly.begin(),ortho_base_poly.end(),
		    Mortho_poly.upper_left(),Mortho_poly.bottom_right(),
		    inner_prod_poly<double>());
  std::cout<<Mortho_poly<<std::endl; 

  

  slip::gram_schmidt_orthogonalization(init_base_poly.begin(),
				       init_base_poly.end(),
				       ortho_base_poly.begin(),
				       ortho_base_poly.end(),
				       inner_prod_poly<double>());

  std::cout<<"Orthogonalization Base  "<<std::endl;
  for(std::size_t i = 0; i < ortho_base_poly.size();++i)
    {
      std::cout<<ortho_base_poly[i]<<std::endl;
    }
  
  std::cout<<"Base verification"<<std::endl;
  slip::gram_matrix(ortho_base_poly.begin(),ortho_base_poly.end(),
		    Mortho_poly.upper_left(),Mortho_poly.bottom_right(),
		    inner_prod_poly<double>());
  std::cout<<Mortho_poly<<std::endl; 


  slip::modified_gram_schmidt_orthogonalization(init_base_poly.begin(),
						init_base_poly.end(),
						ortho_base_poly.begin(),
						ortho_base_poly.end(),
						inner_prod_poly<double>());

  std::cout<<"Modified Orthogonalization Base  "<<std::endl;
  for(std::size_t i = 0; i < ortho_base_poly.size();++i)
    {
      std::cout<<ortho_base_poly[i]<<std::endl;
    }
  
  std::cout<<"Base verification"<<std::endl;
  slip::gram_matrix(ortho_base_poly.begin(),ortho_base_poly.end(),
		    Mortho_poly.upper_left(),Mortho_poly.bottom_right(),
		    inner_prod_poly<double>());
  std::cout<<Mortho_poly<<std::endl; 


  slip::Matrix<double> A(4,4);
  slip::Matrix<double> Q(4,4);
  slip::Matrix<double> R(4,4);
  for(std::size_t i = 0; i < A.rows(); ++i)
    {
      for(std::size_t j = 0; j < A.cols(); ++j)
	{
	  A[i][j] = 4 + (i-j);
	}
    }
  std::cout<<"A = \n"<<A<<std::endl;
  slip::modified_gram_schmidt(A.upper_left(),A.bottom_right(),
			      Q.upper_left(),Q.bottom_right(),
			      R.upper_left(),R.bottom_right());
  std::cout<<"Q = \n"<<Q<<std::endl;
  std::cout<<"R = \n"<<R<<std::endl;
  slip::Matrix<double> QT(4,4);
  slip::transpose(Q,QT);
  slip::Matrix<double> QTQ(4,4);
  slip::matrix_matrix_multiplies(QT,Q,QTQ);
  std::cout<<"QTQ = \n"<<QTQ<<std::endl;
  slip::Matrix<double> QR(4,4);
  slip::matrix_matrix_multiplies(Q,R,QR);
   std::cout<<"QR = \n"<<QR<<std::endl;


   slip::Vector<double> ea1(A.col_begin(0),A.col_end(0));
   slip::Vector<double> ea2(A.col_begin(1),A.col_end(1));
   slip::Vector<double> ea3(A.col_begin(2),A.col_end(2));
   slip::Vector<double> ea4(A.col_begin(3),A.col_end(3));
   

 
  std::vector<slip::Vector<double> > init_basea(4);
  init_basea[0] = ea1;
  init_basea[1] = ea2;
  init_basea[2] = ea3;
  init_basea[3] = ea4;
  std::cout<<"Initial Base  "<<std::endl;
  for(std::size_t i = 0; i < init_base.size();++i)
    {
      std::cout<<init_basea[i]<<std::endl;
    }

  
  std::vector<slip::Vector<double> > ortho_basea(4);
  slip::modified_gram_schmidt_normalization(init_basea.begin(),
					    init_basea.end(),
					    ortho_basea.begin(),
					    ortho_basea.end(),
					    inner_prod<double>());
  std::cout<<"Modified Orthonormalized Base  "<<std::endl;
  for(std::size_t i = 0; i < ortho_basea.size();++i)
    {
      std::cout<<ortho_basea[i]<<std::endl;
    }

  std::cout<<"Base verification"<<std::endl;
  
  slip::Matrix<double> Morthoa(4,4);
  slip::gram_matrix(ortho_basea.begin(),ortho_basea.end(),
		    Morthoa.upper_left(),Morthoa.bottom_right(),
		    inner_prod<double>());
  std::cout<<Morthoa<<std::endl;


  slip::Matrix<double> Ar(4,3);
  slip::Matrix<double> Qr(4,3);
  slip::Matrix<double> Rr(3,3);
  for(std::size_t i = 0; i < Ar.rows(); ++i)
    {
      for(std::size_t j = 0; j < Ar.cols(); ++j)
	{
	  Ar[i][j] = 4 + (i-j);
	}
    }
  std::cout<<"Ar = \n"<<Ar<<std::endl;
  slip::modified_gram_schmidt(Ar.upper_left(),Ar.bottom_right(),
			      Qr.upper_left(),Qr.bottom_right(),
			      Rr.upper_left(),Rr.bottom_right());
  std::cout<<"Qr = \n"<<Qr<<std::endl;
  std::cout<<"Rr = \n"<<Rr<<std::endl;
  slip::Matrix<double> QrT(3,4);
  slip::transpose(Qr,QrT);
  slip::Matrix<double> QrTQr(3,3);
  slip::matrix_matrix_multiplies(QrT,Qr,QrTQr);
  std::cout<<"QrTQr = \n"<<QrTQr<<std::endl;
  slip::Matrix<double> QrRr(4,3);
  slip::matrix_matrix_multiplies(Qr,Rr,QrRr);
  std::cout<<"QrRr = \n"<<QrRr<<std::endl;


  slip::Matrix<std::complex<double> > Arc(4,3);
  slip::Matrix<std::complex<double> > Qrc(4,3);
  slip::Matrix<std::complex<double> > Rrc(3,3);
  for(int i = 0; i < static_cast<int>(Arc.rows()); ++i)
    {
      for(int j = 0; j < static_cast<int>(Arc.cols()); ++j)
	{
	  Arc[i][j] = std::complex<double>(4 + (i-j),(i-j));
	}
    }
  std::cout<<"Arc = \n"<<Arc<<std::endl;
  slip::modified_gram_schmidt(Arc.upper_left(),Arc.bottom_right(),
			      Qrc.upper_left(),Qrc.bottom_right(),
			      Rrc.upper_left(),Rrc.bottom_right());
  std::cout<<"Qrc = \n"<<Qrc<<std::endl;
  std::cout<<"Rrc = \n"<<Rrc<<std::endl;
  slip::Matrix<std::complex<double> > QrcT(3,4);
  slip::hermitian_transpose(Qrc,QrcT);
  slip::Matrix<std::complex<double> > QrcTQrc(3,3);
  slip::matrix_matrix_multiplies(QrcT,Qrc,QrcTQrc);
  std::cout<<"QrcTQrc = \n"<<QrcTQrc<<std::endl;
  slip::Matrix<std::complex<double> > QrcRrc(4,3);
  slip::matrix_matrix_multiplies(Qrc,Rrc,QrcRrc);
  std::cout<<"QrcRrc = \n"<<QrcRrc<<std::endl;



  

  return 0;
}
