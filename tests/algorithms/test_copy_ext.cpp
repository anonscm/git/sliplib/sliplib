#include <iostream>
#include <ctime>
#include "Array.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "arithmetic_op.hpp"
#include "copy_ext.hpp"

int main()
{

  slip::Array<double> A(10);
  slip::Array<double> B(10);
  slip::iota(A.begin(),A.end(),1.0);
  std::cout<<"A = \n"<<A<<std::endl;
  slip::copy(A.begin(),A.end(),B.begin());
  std::cout<<"B = \n"<<B<<std::endl;

  slip::Array2d<double> A2(3,4);
  slip::Array2d<double> B2(3,4);
  slip::iota(A2.begin(),A2.end(),1.0);
  std::cout<<"A2 = \n"<<A2<<std::endl;
  slip::copy(A2.upper_left(),A2.bottom_right(),
	    B2.upper_left());
  std::cout<<"B2 = \n"<<B2<<std::endl;
  slip::Array3d<double> A3(2,3,4);
  slip::Array3d<double> B3(2,3,4);
  slip::iota(A3.begin(),A3.end(),1.0);
  std::cout<<"A3 = \n"<<A3<<std::endl;
  slip::copy(A3.front_upper_left(),A3.back_bottom_right(),
	     B3.front_upper_left());
  std::cout<<"B3 = \n"<<B3<<std::endl;

  std::cout<<std::endl;
  std::cout<<"------------benchmark -----------"<<std::endl;
  std::cout<<std::endl;
  

  size_t size = 10000;
  slip::Array2d<double> A22(size,size);
  slip::Array2d<double> B22(size,size);
  slip::iota(A22.begin(),A22.end(),1.0);
  unsigned int t1 = static_cast<unsigned int>(clock());
  slip::copy(A22.upper_left(),A22.bottom_right(),
	     B22.upper_left());
  unsigned int t2 = static_cast<unsigned int>(clock());
  std::cout<<"slip::copy time = "<<(t2 - t1)<<std::endl;
  unsigned int t11 = static_cast<unsigned int>(clock());
  std::copy(A22.upper_left(),A22.bottom_right(),
	    B22.upper_left());
  unsigned int t22 = static_cast<unsigned int>(clock());
  std::cout<<"std::copy time = "<<(t22 - t11)<<std::endl;
  std::cout<<"std::copy time / slip::copy time "<<static_cast<double>(t22-t11)/
    static_cast<double>(t2-t1)<<std::endl;
  return 0;
}
