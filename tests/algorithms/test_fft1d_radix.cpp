#include <complex>
#include <iostream>
#include <iomanip>
#include <numeric>
#include <vector>
#include <ctime>

#include "FFT.hpp"
#include "Array.hpp"
#include "GrayscaleImage.hpp"
#include "dynamic.hpp"

template <typename T>
T aleat10(void)
{
  return 10.0 * (rand()/static_cast<T>(RAND_MAX));
}

int main()
{
  time_t start_real_split, end_real_split;
  time_t start_radix2, end_radix2;
  time_t start_cp_split, end_cp_split;
  int time_diff;
  std::srand(std::clock());
 
  const int N(9);
  bool disp("yes");
  typedef double real;
  typedef std::complex<real> cx;
  slip::Array<cx> b(N,cx(0));
  slip::Array<cx> bb(N,cx(0));
  slip::Array<cx> bc(N,cx(0));
  slip::Array<cx> b2(N,cx(0));
  slip::Array<cx> b2b(N,cx(0));
  slip::Array<real> I(N,0.0);
  std::generate(I.begin(),I.end(),aleat10<real>);
  slip::Array<cx> Ic(N,cx(0));
  std::copy(I.begin(),I.end(),Ic.begin());
  
  std::cout <<  std::setfill('*') << std::setw(80) << '*' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "FFT1D Split-Radix vs Cooley-Tukey" << std::endl;
  std::cout <<  std::setfill('*') << std::setw(80) << '*' << std::endl;
  if(disp)
    {
      std::cout << std::endl;  
      std::cout<< "Real Signal" <<std::endl;
      std::cout << I << std::endl;  
      std::cout << std::endl;  
      std::cout<< "Complex Signal" <<std::endl;
      std::cout << Ic << std::endl; 
    }
  
  //fft
  std::cout<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Forward FFT" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  
  time(&start_real_split);
  slip::real_fft(I.begin(),I.end(),b.begin());
  time(&end_real_split);
  time(&start_cp_split);
  slip::complex_fft(Ic.begin(),Ic.end(),bc.begin());
  time(&end_cp_split);
  time(&start_radix2);
  slip::radix2_fft(I.begin(),bb.begin(),slip::log2(N));
  time(&end_radix2);
  
  if(disp)
    {
      std::cout << std::endl;  
      std::cout<< "Real-Split-Radix" <<std::endl;
      std::copy(b.begin(), b.end(),std::ostream_iterator<cx>(std::cout," "));
      std::cout<<std::endl;
      std::cout<<std::endl;
      std::cout<< "Real-Radix2" <<std::endl;
      std::copy(bb.begin(), bb.end(),std::ostream_iterator<cx>(std::cout," "));
      std::cout<<std::endl;
      std::cout<<std::endl;
      std::cout<< "Complex-Split-Radix" <<std::endl;
      std::copy(bc.begin(), bc.end(),std::ostream_iterator<cx>(std::cout," "));
      std::cout<<std::endl;
      std::cout<<std::endl;
    }
  
  time_diff = end_real_split - start_real_split;
  std::cout << "Real Split FFT computation time : " << time_diff << "sec.\n";
  time_diff = end_radix2 - start_radix2;
  std::cout << "Radix 2 FFT computation time : " << time_diff << "sec.\n";
  time_diff = end_cp_split - start_cp_split;
  std::cout << "Complex Split FFT computation time : " << time_diff << "sec.\n";
  
  //fft backward
  std::cout<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Backward FFT" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  
  time(&start_real_split);
  slip::ifft(b.begin(),b.end(),b2.begin());
  time(&end_real_split);
  time(&start_radix2);
  slip::radix2_ifft(bb.begin(),b2b.begin(),slip::log2(N));
  time(&end_radix2);
  
  if(disp)
    { 
      std::cout << std::endl; 
      std::cout<< "Split-Radix" <<std::endl;
      std::copy(b2.begin(), b2.end(),std::ostream_iterator<cx>(std::cout," "));
      std::cout<<std::endl;
      std::cout<<std::endl;
      std::cout<< "Radix2" <<std::endl;
      std::copy(b2b.begin(), b2b.end(),std::ostream_iterator<cx>(std::cout," "));
      std::cout<<std::endl;
      std::cout<<std::endl;
    }
  
  time_diff = end_real_split - start_real_split;
  std::cout << "Split iFFT computation time : " << time_diff << "sec.\n";
  time_diff = end_radix2 - start_radix2;
  std::cout << "Radix 2 iFFT computation time : " << time_diff << "sec.\n";
}
