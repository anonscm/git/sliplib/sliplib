#include <ios>
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include "GrayscaleImage.hpp"
#include "Volume.hpp"
#include "Array.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "dynamic.hpp"
#include "FFT.hpp"
#include "Box2d.hpp"
#include "statistics.hpp"
#include "macros.hpp"
#include "dct.hpp"



int main()
{
  typedef double tt;
  tt a[] = { tt(0), tt(1), tt(3), tt(4), tt(4), tt(3),tt(1),tt(0)};
  tt b[8];
  slip::dct(a,a+8,b);
   for(int i = 0; i < 8; ++i)
     {
       std::cout<<b[i]<<" ";
     }
  
   std::cout<<std::endl;

   std::cout<<"dct shift..."<<std::endl;
   slip::fftshift(b,b+8);
   for(int i = 0; i < 8; ++i)
     {
       std::cout<<b[i]<<" ";
     }
   std::cout<<std::endl;

   slip::Array<tt> ac(8,a);
   slip::dct(ac.begin(),ac.end(),b);
   std::cout<<ac<<std::endl<<std::endl;
    for(int i = 0; i < 8; ++i)
     {
       std::cout<<b[i]<<" ";
     }
    std::cout<<std::endl<<std::endl;
    tt b2[8];
    slip::idct(b,b+8,b2);
    for(int i = 0; i < 8; ++i)
     {
       std::cout<<b2[i]<<" ";
     }
    std::cout<<std::endl<<std::endl;

   double tabd[] = {0.0,1.0,3.0,4.0,4.0,3.0,1.0,0.0};
   slip::dct(tabd,tabd + 8,b);
   for(int i = 0; i < 8; ++i)
     {
       std::cout<<b[i]<<" ";
     }

  std::cout<<std::endl;
  std::cout<<std::endl;


  


  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dct_8 cosinus table" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Vector<double> costab(7);
  costab[0] = 0.5 * std::cos(slip::constants<double>::pi()/16.0);
  costab[1] = 0.5 * std::cos((2.0*slip::constants<double>::pi())/16.0);
  costab[2] = 0.5 * std::cos((3.0*slip::constants<double>::pi())/16.0);
  costab[3] = 0.5 * std::cos((4.0*slip::constants<double>::pi())/16.0);
  costab[4] = 0.5 * std::cos((5.0*slip::constants<double>::pi())/16.0);
  costab[5] = 0.5 * std::cos((6.0*slip::constants<double>::pi())/16.0);
  costab[6] = 0.5 * std::cos((7.0*slip::constants<double>::pi())/16.0);
  int default_precision = std::cout.precision();
  std::cout.setf(std::ios::fixed);
  std::cout.precision(34);
  for(std::size_t i = 0; i < costab.size(); ++i)
    {
      std::cout<<costab[i]<<std::endl;
    }
  
  std::cout<<std::endl;
  std::cout.precision(default_precision);
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dct_8 " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Vector<double> A(8);
  double val1[] = {8,16,24,32,40,48,56,64};
 
  slip::dct_8(val1,val1+8,A.begin());
  std::cout<<"A = \n"<<A<<std::endl;
  slip::Vector<double> IdctA(8);
  slip::idct_8(A.begin(),A.end(),IdctA.begin());
  std::cout<<"IdctA = \n"<<IdctA<<std::endl;

  std::cout<<"------------------"<<std::endl;
  A.fill(0.0);
  slip::dct(val1,val1+8,A.begin());
  std::cout<<"dct = \n"<<A<<std::endl;
  slip::idct(A.begin(),A.end(),IdctA.begin());
  std::cout<<"IdctA = \n"<<IdctA<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dct1d " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  A.fill(0.0);
  slip::dct(val1,val1+8,A.begin());
  std::cout<<"A = \n"<<A<<std::endl;
  IdctA.fill(0.0);
  slip::idct(A.begin(),A.end(),IdctA.begin());
  std::cout<<"IdctA = \n"<<IdctA<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dctI_8 matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> DCTI_8_norm;
  slip::dctI_coeff<double> dct_coeffI_norm(8,true);
  dct_matrix(dct_coeffI_norm,DCTI_8_norm);
  std::cout<<"DCTI_8_norm = \n"<<DCTI_8_norm<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dctII_8 matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> DCTII_8;
  slip::dctII_coeff<double> dct_coeffII(8,false);
  dct_matrix(dct_coeffII,DCTII_8);
  std::cout<<"DCTII_8 = \n"<<DCTII_8<<std::endl;
  A.fill(0.0);
  slip::matrix_vector_multiplies(DCTII_8.upper_left(),DCTII_8.bottom_right(),

				 val1,val1+8, A.begin(),A.end());
  std::cout<<"A = \n"<<A<<std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dctIII_8 matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> DCTIII_8;
  slip::dctIII_coeff<double> dct_coeffIII(8,false);
  dct_matrix(dct_coeffIII,DCTIII_8);

  std::cout<<"DCTIII_8 = \n"<<DCTIII_8<<std::endl;
  IdctA.fill(0.0);
  slip::matrix_vector_multiplies(DCTIII_8,A,IdctA);
  std::cout<<"IdctA = \n"<<IdctA<<std::endl;
  
  slip::Matrix<double> DCTII_8_norm;
  dct_coeffII.normalized_=true;
  dct_matrix(dct_coeffII,DCTII_8_norm);

  std::cout<<"DCTII_8_norm = \n"<<DCTII_8_norm<<std::endl;
  A.fill(0.0);
  slip::matrix_vector_multiplies(DCTII_8_norm.upper_left(),
				 DCTII_8_norm.bottom_right(),
				 val1,val1+8, A.begin(),A.end());
  std::cout<<"A = \n"<<A<<std::endl;
  
  slip::Matrix<double> DCTIII_8_norm;
  dct_coeffIII.normalized_=true;
  dct_matrix(dct_coeffIII,DCTIII_8_norm);

  std::cout<<"DCTIII_8_norm = \n"<<DCTIII_8_norm<<std::endl;
  IdctA.fill(0.0);
  slip::matrix_vector_multiplies(DCTIII_8_norm.upper_left(),
				 DCTIII_8_norm.bottom_right(),
				 A.begin(),A.end(),
				 IdctA.begin(),IdctA.end());
  std::cout<<"IdctA = \n"<<IdctA<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dctIV_8 matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> DCTIV_8;
  slip::dctIV_coeff<double> dct_coeffIV(8);
  dct_matrix(dct_coeffIV,DCTIV_8);
  std::cout<<"DCTIV_8 = \n"<<DCTIV_8<<std::endl;
  
  
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "split_radix_dct " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   A.fill(0.0);
   slip::split_radix_dct(val1,val1+8,A.begin());
   std::cout<<"A = \n"<<A<<std::endl;



  

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dct2d 8x8" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
		  63, 59, 66, 90, 109, 85, 69, 72,
		  62, 59, 68, 113, 144, 104, 66, 73,
		  63, 58, 71, 122, 154, 106, 70, 69,
		  67, 61, 68, 104, 126,  88, 68, 70,
		  79, 65, 60,  70,  77,  68, 58, 75,
		  85, 71, 64,  59,  55,  61, 65, 83,
		  87, 79, 69,  68,  65,  76, 78, 94};
  slip::GrayscaleImage<double> I(8,8,val);
  std::cout<<"I = \n"<<I<<std::endl;
  
  I-=128;
  std::cout<<"I-128 = \n"<<I<<std::endl;
  std::cout<<"mean(I-128) = "<<slip::mean<double>(I.begin(),I.end())<<std::endl;

  slip::GrayscaleImage<double> DCT(I.rows(),I.cols());
  slip::dct_8x8(I.upper_left(),I.bottom_right(),DCT.upper_left());
  std::cout<<"DCT(I) = \n"<<DCT<<std::endl;
  slip::GrayscaleImage<double> IDCT(I.rows(),I.cols());
  slip::idct_8x8(DCT.upper_left(),DCT.bottom_right(),
		     IDCT.upper_left());
  std::cout<<"IDCT(DCT(I)) = \n"<<IDCT<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dct2d " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"I-128 = \n"<<I<<std::endl;
  DCT.fill(0.0);
  slip::dct2d(I.upper_left(),I.bottom_right(),DCT.upper_left());
  std::cout<<"DCT(I) = \n"<<DCT<<std::endl;
  IDCT.fill(0.0);
  slip::idct2d(DCT.upper_left(),DCT.bottom_right(),
 		   IDCT.upper_left());
  std::cout<<"IDCT(DCT(I)) = \n"<<IDCT<<std::endl;

  std::cout<<"-----------------"<<std::endl;
  DCT.fill(0.0);
  slip::dct2d(I,DCT);
  std::cout<<"DCT(I) = \n"<<DCT<<std::endl;
  slip::idct2d(DCT,IDCT);
  std::cout<<"IDCT(DCT) = \n"<<IDCT<<std::endl;
 
  // IdctA.fill(0.0);
  // slip::Array<double> dctA(8);
  // dctA.fill(0.0);
  // slip::dct(val1,val1+8,dctA.begin());
  // std::cout<<"dctA = \n"<<dctA<<std::endl;
  // slip::idct(dctA.begin(),dctA.end(),IdctA.begin());
  // std::cout<<"IdctA = \n"<<IdctA<<std::endl;

  // dctA[0] /= std::sqrt(2.0);
  // std::cout<<"dctA morm = \n"<<dctA<<std::endl;
  // IdctA.fill(0.0);
  // slip::idct(dctA.begin(),dctA.end(),IdctA.begin());
  // std::cout<<"IdctA = \n"<<IdctA<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "dct3d " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Volume<double> Vol(8,8,8);
   slip::iota(Vol.begin(),Vol.end(),0.0,1.0);
   //std::cout<<"Vol = \n"<<Vol<<std::endl;
   slip::Volume<double> DCTVol(8,8,8);
   slip::dct3d(Vol.front_upper_left(),
		   Vol.back_bottom_right(),
		   DCTVol.front_upper_left());
   std::cout<<"DCTVol = \n"<<DCTVol<<std::endl;
   slip::Volume<double> IDCTVol(8,8,8);
   slip::idct3d(DCTVol.front_upper_left(),
		   DCTVol.back_bottom_right(),
		   IDCTVol.front_upper_left());
   std::cout<<"IDCTVol = \n"<<IDCTVol<<std::endl;
   std::cout<<"-----------------"<<std::endl;
   DCTVol.fill(0.0);
   slip::dct3d(Vol,DCTVol);
   std::cout<<"DCT(Vol) = \n"<<DCTVol<<std::endl;
   slip::idct3d(DCTVol,IDCTVol);
   std::cout<<"IDCTVol = \n"<<IDCTVol<<std::endl;
  return 0;
}


