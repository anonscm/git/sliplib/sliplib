#include <iostream>
#include <algorithm>
#include <iomanip>
#include <numeric>
#include "norms.hpp"
#include "compare.hpp"
#include "Matrix.hpp"
#include <vector>

template<typename T>
bool lt5Predicate (const T& val)
{
  return (val < T(5));
}

template<typename T>
bool lt2Predicate (const T& val)
{
  return (val <= T(2));
}

int main()
{
  

  float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
  float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};

  slip::Matrix<float> M1(2,3,f1);
  slip::Matrix<float> M2(2,3,f2);
  
  std::cout<<"mean_square_diff"<<std::endl;
  std::cout<<slip::mean_square_diff<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
  std::cout<<slip::mean_square_diff<float>(M1.begin(),M1.end(),M1.begin())<<std::endl;

  std::cout<<"mean_abs_diff"<<std::endl;
  std::cout<<slip::mean_abs_diff<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
  std::cout<<slip::mean_abs_diff<float>(M1.begin(),M1.end(),M1.begin())<<std::endl;

  std::cout<<"snr"<<std::endl;
  std::cout<<slip::snr<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
  std::cout<<slip::snr<float>(M1.begin(),M1.end(),M1.begin())<<std::endl;

  std::cout<<"snr_db"<<std::endl;
  std::cout<<slip::snr_dB<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
  std::cout<<slip::snr_dB<float>(M1.begin(),M1.end(),M1.begin())<<std::endl;

  std::cout<<"L1"<<std::endl;
  std::cout<<slip::L1_distance<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
  std::cout<<slip::L1_distance<float>(M1.begin(),M1.end(),M1.begin())<<std::endl;

  std::cout<<"L2"<<std::endl;
  std::cout<<slip::L2_distance<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
  std::cout<<slip::L2_distance<float>(M1.begin(),M1.end(),M1.begin())<<std::endl;

  std::cout<<"L22"<<std::endl;
  std::cout<<slip::L22_distance<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
  std::cout<<slip::L22_distance<float>(M1.begin(),M1.end(),M1.begin())<<std::endl;

  std::cout<<"Linf"<<std::endl;
  std::cout<<slip::Linf_distance<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
  std::cout<<slip::Linf_distance<float>(M1.begin(),M1.end(),M1.begin())<<std::endl;

  std::cout<<"Kullback"<<std::endl;
  std::cout<<slip::KullbackLeibler_distance<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
  std::cout<<slip::KullbackLeibler_distance<float>(M1.begin(),M1.end(),M1.begin())<<std::endl;

  std::cout<<"Chi2"<<std::endl;
  std::cout<<slip::Chi2_distance<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
  std::cout<<slip::Chi2_distance<float>(M1.begin(),M1.end(),M1.begin())<<std::endl;

  std::cout<<"Minkowski3"<<std::endl;
  std::cout<<slip::Minkowski_distance<float>(3,M1.begin(),M1.end(),M2.begin())<<std::endl;
  std::cout<<slip::Minkowski_distance<float>(3,M1.begin(),M1.end(),M1.begin())<<std::endl;


 std::cout<<"PSNR"<<std::endl;
 std::cout<<slip::psnr<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
 std::cout<<slip::psnr<float>(M1.begin(),M1.end(),M1.begin())<<std::endl;

 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"SSIM"<<std::endl;
 std::cout<<slip::SSIM<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
 std::cout<<slip::SSIM<float>(M1.begin(),M1.end(),M1.begin())<<std::endl;


/*
  std::cout<<"Mahalanobis"<<std::endl;
  std::cout<<slip::Mahalanobis_distance<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
  std::cout<<slip::Mahalanobis_distance<float>(M1.begin(),M1.end(),M1.begin())<<std::endl;
*/
  
int f3[] = {1,1,1,0,0,1};
slip::Matrix<int> Mask(2,3,f3);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"mean_square_diff_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
 std::cout<<slip::mean_square_diff_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"mean_square_diff_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::mean_square_diff_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"mean_abs_diff_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
 std::cout<<slip::mean_abs_diff_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"mean_abs_diff_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::mean_abs_diff_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"snr_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
std::cout<<slip::snr_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"snr_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::snr_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"snr_dB_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
 std::cout<<slip::snr_dB_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"snr_dB_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::snr_dB_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"L22_distance_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
 std::cout<<slip::L22_distance_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"L22_distance_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::L22_distance_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"L2_distance_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
 std::cout<<slip::L2_distance_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"L2_distance_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::L2_distance_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"L1_distance_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
 std::cout<<slip::L1_distance_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"L1_distance_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::L1_distance_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"Minkowski_distance_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
 std::cout<<slip::Minkowski_distance_mask<float>(3,M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"Minkowski_distance_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::Minkowski_distance_if<float>(3,M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"Linf_distance_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
std::cout<<slip::Linf_distance_mask<float>(M1.begin(),M1.end(),M2.begin(),Mask.begin(),1)<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"Linf_distance_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::Linf_distance_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"Kullback_Leibler_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
std::cout<<slip::KullbackLeibler_distance_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"KullbackLeibler_distance_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::KullbackLeibler_distance_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"Chi2_distance_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
 std::cout<<slip::Chi2_distance_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"Chi2_distance_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::Chi2_distance_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"psnr_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
 std::cout<<slip::psnr_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"psnr_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::psnr_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"SSIM_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
 std::cout<<slip::SSIM_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"SSIM_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::SSIM_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;

 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout<<"distance"<<std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 slip::Matrix<float> Md1(2,3);
 slip::Matrix<float> Md2(2,3);
 slip::Matrix<float> Resultd(2,3);
 slip::iota(Md1.begin(),Md1.end(),1.0f,1.0f);
 slip::iota(Md2.begin(),Md2.end(),6.0f,-1.0f);
 std::cout<<"Md1 = \n"<<Md1<<std::endl;
 std::cout<<"Md2 = \n"<<Md2<<std::endl;
 std::cout<<"L1_distance(Md1,Md2) = "<<slip::distance<float>(Md1.begin(),Md1.end(),Md2.begin(),slip::L1_dist<float,float>())<<std::endl;
 std::cout<<"L2_distance(Md1,Md2) = "<<slip::distance<float>(Md1.begin(),Md1.end(),Md2.begin(),slip::L2_dist<float,float>())<<std::endl;
   std::cout<<"L22_distance(Md1,Md2) = "<<slip::distance<float>(Md1.begin(),Md1.end(),Md2.begin(),slip::L22_dist<float,float>())<<std::endl;
   std::cout<<"relative_error_L1(Md1,Md2) = "<<slip::distance<float>(Md1.begin(),Md1.end(),Md2.begin(),slip::Relative_error_L1<float,float>())<<std::endl;
   std::cout<<"relative_error_L2(Md1,Md2) = "<<slip::distance<float>(Md1.begin(),Md1.end(),Md2.begin(),slip::Relative_error_L2<float,float>())<<std::endl;
   std::cout<<"relative_error_L22(Md1,Md2) = "<<slip::distance<float>(Md1.begin(),Md1.end(),Md2.begin(),slip::Relative_error_L22<float,float>())<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"distance mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
int maskd[] = {2,0,0,2,0,2};
slip::Matrix<int> Maskd(2,3,maskd);
std::cout<<"Md1 = \n"<<Md1<<std::endl;
std::cout<<"Md2 = \n"<<Md2<<std::endl; 
std::cout<<"Maskd = \n"<<Maskd<<std::endl;
std::cout<<"L1_distance(Md1,Md2) = "<<slip::distance_mask<float>(Md1.begin(),Md1.end(),Maskd.begin(),Md2.begin(),slip::L1_dist<float,float>(),2)<<std::endl;
 std::cout<<"L2_distance(Md1,Md2) = "<<slip::distance_mask<float>(Md1.begin(),Md1.end(),Maskd.begin(),Md2.begin(),slip::L2_dist<float,float>(),2)<<std::endl;
   std::cout<<"L22_distance(Md1,Md2) = "<<slip::distance_mask<float>(Md1.begin(),Md1.end(),Maskd.begin(),Md2.begin(),slip::L22_dist<float,float>(),2)<<std::endl;
   std::cout<<"relative_error_L1(Md1,Md2) = "<<slip::distance_mask<float>(Md1.begin(),Md1.end(),Maskd.begin(),Md2.begin(),slip::Relative_error_L1<float,float>(),2)<<std::endl;
   std::cout<<"relative_error_L2(Md1,Md2) = "<<slip::distance_mask<float>(Md1.begin(),Md1.end(),Maskd.begin(),Md2.begin(),slip::Relative_error_L2<float,float>(),2)<<std::endl;
   std::cout<<"relative_error_L22(Md1,Md2) = "<<slip::distance_mask<float>(Md1.begin(),Md1.end(),Maskd.begin(),Md2.begin(),slip::Relative_error_L22<float,float>(),2)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"distance if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"Md1 = \n"<<Md1<<std::endl;
std::cout<<"Md2 = \n"<<Md2<<std::endl; 
 std::cout<<"L1_distance(Md1,Md2) = "<<slip::distance_if<float>(Md1.begin(),Md1.end(),Md2.begin(),slip::L1_dist<float,float>(),lt2Predicate<float>)<<std::endl;
std::cout<<"L2_distance(Md1,Md2) = "<<slip::distance_if<float>(Md1.begin(),Md1.end(),Md2.begin(),slip::L2_dist<float,float>(),lt2Predicate<float>)<<std::endl;
std::cout<<"L22_distance(Md1,Md2) = "<<slip::distance_if<float>(Md1.begin(),Md1.end(),Md2.begin(),slip::L22_dist<float,float>(),lt2Predicate<float>)<<std::endl;
 std::cout<<"relative_error_L1(Md1,Md2) = "<<slip::distance_if<float>(Md1.begin(),Md1.end(),Md2.begin(),slip::Relative_error_L1<float,float>(),lt2Predicate<float>)<<std::endl;
   std::cout<<"relative_error_L2(Md1,Md2) = "<<slip::distance_if<float>(Md1.begin(),Md1.end(),Md2.begin(),slip::Relative_error_L2<float,float>(),lt2Predicate<float>)<<std::endl;
   std::cout<<"relative_error_L22(Md1,Md2) = "<<slip::distance_if<float>(Md1.begin(),Md1.end(),Md2.begin(),slip::Relative_error_L22<float,float>(),lt2Predicate<float>)<<std::endl;


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout<<"container distance"<<std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Md1 = \n"<<Md1<<std::endl;
   std::cout<<"Md2 = \n"<<Md2<<std::endl; 
   slip::distance(Md1.begin(),Md1.end(),Md2.begin(),Resultd.begin(),slip::L1_dist<float,float>());
   std::cout<<"Resultd L1 = \n"<<Resultd<<std::endl; 
   slip::distance(Md1.begin(),Md1.end(),Md2.begin(),Resultd.begin(),slip::L2_dist<float,float>());
   std::cout<<"Resultd L2 = \n"<<Resultd<<std::endl;
   slip::distance(Md1.begin(),Md1.end(),Md2.begin(),Resultd.begin(),slip::L22_dist<float,float>());
   std::cout<<"Resultd L22 = \n"<<Resultd<<std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout<<"container distance mask"<<std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Md1 = \n"<<Md1<<std::endl;
   std::cout<<"Md2 = \n"<<Md2<<std::endl;
   std::cout<<"Maskd = \n"<<Maskd<<std::endl;
   Resultd.fill(0.0);
   slip::distance_mask(Md1.begin(),Md1.end(),Maskd.begin(),Md2.begin(),Resultd.begin(),slip::L1_dist<float,float>(),2);
   std::cout<<"Resultd L1 = \n"<<Resultd<<std::endl;
   Resultd.fill(0.0);
   slip::distance_mask(Md1.begin(),Md1.end(),Maskd.begin(),Md2.begin(),Resultd.begin(),slip::L2_dist<float,float>(),2);
   std::cout<<"Resultd L2 = \n"<<Resultd<<std::endl;
   Resultd.fill(0.0);
   slip::distance_mask(Md1.begin(),Md1.end(),Maskd.begin(),Md2.begin(),Resultd.begin(),slip::L22_dist<float,float>(),2);
   std::cout<<"Resultd L22 = \n"<<Resultd<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout<<"container distance if"<<std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Md1 = \n"<<Md1<<std::endl;
   std::cout<<"Md2 = \n"<<Md2<<std::endl;
   Resultd.fill(0.0);
   slip::distance_if(Md1.begin(),Md1.end(),Md2.begin(),Resultd.begin(),slip::L1_dist<float,float>(),lt2Predicate<float>);
   std::cout<<"Resultd L1 = \n"<<Resultd<<std::endl;
    Resultd.fill(0.0);
   slip::distance_if(Md1.begin(),Md1.end(),Md2.begin(),Resultd.begin(),slip::L2_dist<float,float>(),lt2Predicate<float>);
   std::cout<<"Resultd L2 = \n"<<Resultd<<std::endl;
    Resultd.fill(0.0);
   slip::distance_if(Md1.begin(),Md1.end(),Md2.begin(),Resultd.begin(),slip::L22_dist<float,float>(),lt2Predicate<float>);
   std::cout<<"Resultd L22 = \n"<<Resultd<<std::endl;



}
