#include "FFT.hpp"
#include "Array.hpp"
#include <complex>
#include <iostream>
#include <vector>

#include "GrayscaleImage.hpp"
#include "dynamic.hpp"

int main()
{
#ifdef HAVE_FFTW
  typedef double tt;
  tt a[] = { tt(0), tt(1), tt(3), tt(4), tt(4), tt(3),tt(1),tt(0)};
  tt b[8];
  tt bfftw[8];
  slip::split_radix_dct(a,a+8,b);
  slip::fftw_dct(a,a+8,bfftw);
  std::cout<<"split_radix_dct..."<<std::endl;
  for(int i = 0; i < 8; ++i)
    {
      std::cout<<b[i]<<" ";
    }
  std::cout<<std::endl;
//   std::cout<<"dct shift..."<<std::endl;
//   slip::fftshift(b,b+8);
//   for(int i = 0; i < 8; ++i)
//     {
//       std::cout<<b[i]<<" ";
//     }
//   std::cout<<std::endl;
  std::cout<<"fftw_dct..."<<std::endl;
  for(int i = 0; i < 8; ++i)
    {
      std::cout<<bfftw[i]<<" ";
    }
  std::cout<<std::endl;
//   std::cout<<"fftw_dct shift..."<<std::endl;
//   slip::fftshift(bfftw,bfftw+8);
//   for(int i = 0; i < 8; ++i)
//     {
//       std::cout<<bfftw[i]<<" ";
//     }
//   std::cout<<std::endl;
  std::cout<<"Ratio fftw_dct / dct ..."<<std::endl;
  for(int i = 0; i < 8; ++i)
    {
      std::cout<<bfftw[i]/b[i]<<" ";
    }
  std::cout<<std::endl;
  std::cout<<std::endl;

  std::cout<<"split_radix_dct & idct..."<<std::endl;
   slip::Array<tt> ac(8,a);
   slip::split_radix_dct(ac.begin(),ac.end(),b);
   std::cout<<ac<<std::endl<<std::endl;
    for(int i = 0; i < 8; ++i)
     {
       std::cout<<b[i]<<" ";
     }
    std::cout<<std::endl<<std::endl;
    tt b2[8];
    slip::split_radix_idct(b,b+8,b2);
    for(int i = 0; i < 8; ++i)
     {
       std::cout<<b2[i]<<" ";
     }
    std::cout<<std::endl<<std::endl;

   double tabd[] = {0.0,1.0,3.0,4.0,4.0,3.0,1.0,0.0};
   slip::split_radix_dct(tabd,tabd + 8,b);
   for(int i = 0; i < 8; ++i)
     {
       std::cout<<b[i]<<" ";
     }
   std::cout<<std::endl<<std::endl;
   std::cout<<"fftw_dct & idct ..."<<std::endl;
   slip::Array<tt> acfftw(8,a);
   slip::fftw_dct(acfftw.begin(),acfftw.end(),bfftw);
   std::cout<<acfftw<<std::endl<<std::endl;
    for(int i = 0; i < 8; ++i)
     {
       std::cout<<bfftw[i]<<" ";
     }
    std::cout<<std::endl<<std::endl;
    tt b2fftw[8];
    slip::fftw_idct(bfftw,bfftw+8,b2fftw);
    for(int i = 0; i < 8; ++i)
     {
       std::cout<<b2fftw[i]<<" ";
     }
    std::cout<<std::endl<<std::endl;

   double tabdfftw[] = {0.0,1.0,3.0,4.0,4.0,3.0,1.0,0.0};
   slip::fftw_dct(tabdfftw,tabdfftw + 8,bfftw);
   for(int i = 0; i < 8; ++i)
     {
       std::cout<<bfftw[i]<<" ";
     }
   
   std::cout<<std::endl;
   std::cout<<std::endl;
#else
   std::cout << "fftw not defined for SLIP use.\n";
#endif

 return 0;

}
