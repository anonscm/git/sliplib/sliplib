#include <iostream>
#include <iomanip>
#include <algorithm>
#include "Array.hpp"
#include "uniform_random_sampling.hpp"

int main()

{

  
  typedef  double T;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "uniform random sampling  " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  const std::size_t N = 10000;
  slip::Rand1<T> rnd(static_cast<T>(-2.0),static_cast<T>(2.0));
  for(std::size_t i = 0; i < N; ++i)
    {
      std::cout<<rnd()<<std::endl;
    }

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "double uniform random sampling  " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Rand1<T> rnd1(static_cast<T>(-2.0),static_cast<T>(2.0));
  slip::Rand1<T> rnd2(static_cast<T>(-2.0),static_cast<T>(2.0));
  for(std::size_t i = 0; i < N; ++i)
    {
      std::cout<<rnd1()<<" "<<rnd2()<<std::endl;
      
    }

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "generate values between -2.0 and 2.0 " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<T> A(10);
  std::generate(A.begin(),A.end(),rnd1);
  std::cout<<"A =\n"<<A<<std::endl;

  return 0;
}
