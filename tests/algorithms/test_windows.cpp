#include "Array.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "Matrix.hpp"
#include "Matrix3d.hpp"

#include "interpolation.hpp"
#include "window.hpp"


int main()
{
  
  slip::Array<double> hamming(10);
  //slip::Hamming<double> H(hamming.size());
  slip::Hamming<double> hamming_fun(10);
  slip::window1d(hamming.begin(),hamming.end(),hamming_fun);
  std::cout<<"hamming(10) = \n"<<hamming<<std::endl;
  hamming.fill(0.0);
  slip::hamming(hamming.begin(),hamming.end());
  std::cout<<"hamming(10) = \n"<<hamming<<std::endl;
  slip::Array<double> hanning(10);
  //slip::Hanning<double> Ha(hanning.size());
  slip::Hamming<double> hanning_fun(hanning.size());
  slip::window1d(hanning.begin(),hanning.end(),hanning_fun);
  std::cout<<"hanning(10) = \n"<<hanning<<std::endl;
  slip::hanning(hanning.begin(),hanning.end());
 std::cout<<"hanning(10) = \n"<<hanning<<std::endl;


 slip::Array<double> bartlett(10);
 slip::Bartlett<double> bartlett_fun(bartlett.size());
 slip::window1d(bartlett.begin(),bartlett.end(),
		    bartlett_fun);
  std::cout<<"bartlett(10) = \n"<<bartlett<<std::endl;
 slip::bartlett(bartlett.begin(),bartlett.end());
 std::cout<<"bartlett(10) = \n"<<bartlett<<std::endl;


 slip::Array<double> blackman(10);
 slip::Blackman<double> blackman_fun(blackman.size());
 slip::window1d(blackman.begin(),blackman.end(),blackman_fun);
 std::cout<<"blackman(10) = \n"<<blackman<<std::endl;
 slip::blackman(blackman.begin(),blackman.end());
 std::cout<<"blackman(10) = \n"<<blackman<<std::endl;

 slip::Array<double> sincc(7);
 slip::Array<double> x(7);
 slip::iota(x.begin(),x.end(),-static_cast<double>(x.size())/static_cast<double>(2),static_cast<double>(x.size())/static_cast<double>(x.size()-1));
 std::cout<<"x = \n"<<x<<std::endl;
 std::transform(x.begin(),x.end(),sincc.begin(),slip::sinc<double>());
  std::cout<<"sinc_window = \n"<<sincc<<std::endl;
  slip::sinc_window(sincc.begin(),sincc.end());
 std::cout<<"sinc_window = \n"<<sincc<<std::endl;

 slip::Array<double> lanczos(7);
 std::cout<<"x = \n"<<x<<std::endl;
 std::transform(x.begin(),x.end(),lanczos.begin(),slip::Lanczos<double>());
 std::cout<<"lanczos = \n"<<lanczos<<std::endl;
 lanczos.fill(0.0);
 std::transform(x.begin(),x.end(),lanczos.begin(),slip::Lanczos<double>(3));
 std::cout<<"lanczos = \n"<<lanczos<<std::endl;

 lanczos.fill(0.0);
 std::transform(x.begin(),x.end(),lanczos.begin(),slip::Lanczos<double>(5));
 std::cout<<"lanczos = \n"<<lanczos<<std::endl;

 slip::Array<double> nearest_neigh(7);
 slip::Array<double> x2(7);
 slip::iota(x2.begin(),x2.end(),-1.0,2.0/static_cast<double>(x2.size()-1));
 std::cout<<"x2 = \n"<<x2<<std::endl;
 std::transform(x2.begin(),x2.end(),nearest_neigh.begin(),slip::NearestNeighbor<double>());
 std::cout<<"nearest_neigh = \n"<<nearest_neigh<<std::endl;


 slip::Array2d<double> hamming2d(10,10);
 slip::window2d(hamming2d.upper_left(),hamming2d.bottom_right(),
		    hamming_fun);
 std::cout<<"hamming2d = \n"<<hamming2d<<std::endl;

 
 slip::Array3d<double> hamming3d(6,6,6);
 slip::window3d(hamming3d.front_upper_left(),hamming3d.back_bottom_right(),
		    hamming_fun);
 std::cout<<"hamming3d = \n"<<hamming3d<<std::endl;


 hamming.fill(0.0);
 slip::window(hamming.begin(), hamming.end(),hamming_fun);
 std::cout<<"hamming = \n"<<hamming<<std::endl;
 
hamming2d.fill(0.0);
slip::window(hamming2d.upper_left(), hamming2d.bottom_right(),hamming_fun);
std::cout<<"hamming2d = \n"<<hamming2d<<std::endl;

hamming3d.fill(0.0);
slip::window(hamming3d.front_upper_left(), hamming3d.back_bottom_right(),hamming_fun);
std::cout<<"hamming3d = \n"<<hamming3d<<std::endl;

 return 0;
}

