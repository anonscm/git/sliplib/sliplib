#include <complex>
#include <iostream>
#include <vector>

#include "correlation.hpp"
#include "Array.hpp"
#include "GrayscaleImage.hpp"
#include "dynamic.hpp"
#include "error.hpp"

int main()
{
  slip::GrayscaleImage<double> I1;
  I1.read("../images/lena_gray.gif");
  slip::GrayscaleImage<double> I2;
  I2.read("../images/lena_eyes.gif");
  
  slip::DPoint2d<int> size1 = I1.bottom_right() - I1.upper_left();
  slip::DPoint2d<int> size2 = I2.bottom_right() - I2.upper_left();
  slip::DPoint2d<int> size(size1[0] + size2[0] - 1, size1[1] + size2[1] - 1);
  std::cout<<"size of the image = "<<size1[0]<<" x "<<size1[1]<<std::endl;
  std::cout<<"size of the mask  = "<<size2[0]<<" x "<<size2[1]<<std::endl;

  slip::Array2d<double> A1(size[0],size[1],0.0);
  slip::Array2d<double> A2(size[0],size[1],0.0);
  slip::Box2d<int> box1(size2[0]/2,size2[1]/2,
			size2[0]/2 + size1[0]-1,size2[1]/2 + size1[1]-1);
  slip::Box2d<int> box2(size1[0]/2,size1[1]/2,
			size1[0]/2 + size2[0]-1,size1[1]/2 + size2[1]-1);
  std::copy(I1.upper_left(),I1.bottom_right(),A1.upper_left(box1));
  std::copy(I2.upper_left(),I2.bottom_right(),A2.upper_left(box2));
  slip::GrayscaleImage<double> OutSTD(I1.dim1(),I1.dim2(),1.0);
  slip::GrayscaleImage<double> OutFFT(size[0],size[1],1.0);
  slip::GrayscaleImage<double> OutFFT2(I1.dim1(),I1.dim2(),1.0);
  
  // FFT
  std::cout << "FFT circular crosscorrelation starting...";
  slip::fft_circular_crosscorrelation2d(A1.upper_left(),A1.bottom_right(),
					A2.upper_left(),A2.bottom_right(),
					OutFFT.upper_left(),OutFFT.bottom_right());
  slip::GrayscaleImage<double>::iterator2d maxfft = std::max_element(OutFFT.upper_left(),OutFFT.bottom_right());
  std::cout<<"max circular fft = ("<<maxfft.i()<<","<<maxfft.j()<<") = "<<*maxfft<<std::endl;
  slip::change_dynamic_01(OutFFT.upper_left(),OutFFT.bottom_right(),OutFFT.upper_left(),slip::AFFINE_FUNCTION);
  std::copy(OutFFT.upper_left(box1),OutFFT.bottom_right(box1),OutFFT2.upper_left());
  OutFFT2.write("lena_circ_xcorr_fft.gif");

  std::cout << "FFT crosscorrelation starting...";
  slip::fft_crosscorrelation2d_same(I1.upper_left(),I1.bottom_right(),
				    I2.upper_left(),I2.bottom_right(),
				    OutFFT2.upper_left(),OutFFT2.bottom_right());
  maxfft = std::max_element(OutFFT2.upper_left(),OutFFT2.bottom_right());
  std::cout<<"max fft = ("<<maxfft.i()<<","<<maxfft.j()<<") = "<<*maxfft<<std::endl;
  slip::change_dynamic_01(OutFFT2.upper_left(),OutFFT2.bottom_right(),OutFFT2.upper_left(),slip::AFFINE_FUNCTION);
  OutFFT.write("lena_xcorr_fft.gif");

  //STD
  
  std::cout << "STD crosscorrelation starting..."<<std::endl;
  
  // CC
  std::cout << "calculating CC algorithm..."<<std::endl;
#ifndef SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
  slip::crosscorrelation2d<double>(I1.upper_left(),I1.bottom_right(),I2.upper_left(),I2.bottom_right(),OutSTD.upper_left(),OutSTD.bottom_right(),slip::CC);
#else
  slip::crosscorrelation2d<double>(I1.upper_left(),I1.bottom_right(),I2.upper_left(),I2.bottom_right(),OutSTD.upper_left(),OutSTD.bottom_right(),slip::CROSSCORRELATION_TYPE::SLIP_CC);
#endif
  slip::GrayscaleImage<double>::iterator2d maxstd = 
    std::max_element(OutSTD.upper_left(),OutSTD.bottom_right());
  std::cout<<"max cc = ("<<maxstd.i()<<","<<maxstd.j()<<") = "<<*maxstd<<std::endl;
  slip::change_dynamic_01(OutSTD.upper_left(),OutSTD.bottom_right(),OutSTD.upper_left(),slip::AFFINE_FUNCTION);
  OutSTD.write("lena_crcorr_cc.gif");

  // PNCC
  std::cout << "calculating PNCC algorithm..."<<std::endl;
#ifndef SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
  slip::crosscorrelation2d<double>(I1.upper_left(),I1.bottom_right(),I2.upper_left(),I2.bottom_right(),OutSTD.upper_left(),OutSTD.bottom_right(),slip::PNCC);
#else
  slip::crosscorrelation2d<double>(I1.upper_left(),I1.bottom_right(),I2.upper_left(),I2.bottom_right(),OutSTD.upper_left(),OutSTD.bottom_right(),slip::CROSSCORRELATION_TYPE::SLIP_PNCC);
#endif
  maxstd = std::max_element(OutSTD.upper_left(),OutSTD.bottom_right());
  std::cout<<"max PNCC = ("<<maxstd.i()<<","<<maxstd.j()<<") = "<<*maxstd<<std::endl;
  slip::change_dynamic_01(OutSTD.upper_left(),OutSTD.bottom_right(),OutSTD.upper_left(),slip::AFFINE_FUNCTION);
  OutSTD.write("lena_crcorr_pncc.gif");

  // CCC
  std::cout << "calculating CCC algorithm..."<<std::endl;
#ifndef SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
  slip::crosscorrelation2d<double>(I1.upper_left(),I1.bottom_right(),I2.upper_left(),I2.bottom_right(),OutSTD.upper_left(),OutSTD.bottom_right(),slip::CCC);
#else
  slip::crosscorrelation2d<double>(I1.upper_left(),I1.bottom_right(),I2.upper_left(),I2.bottom_right(),OutSTD.upper_left(),OutSTD.bottom_right(),slip::CROSSCORRELATION_TYPE::SLIP_CCC);
#endif
  maxstd = std::max_element(OutSTD.upper_left(),OutSTD.bottom_right());
  std::cout<<"max CCC = ("<<maxstd.i()<<","<<maxstd.j()<<") = "<<*maxstd<<std::endl;
  slip::change_dynamic_01(OutSTD.upper_left(),OutSTD.bottom_right(),OutSTD.upper_left(),slip::AFFINE_FUNCTION);
  OutSTD.write("lena_crcorr_ccc.gif");

 // NCC
  std::cout << "calculating NCC algorithm..."<<std::endl;
#ifndef SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
  slip::crosscorrelation2d<double>(I1.upper_left(),I1.bottom_right(),I2.upper_left(),I2.bottom_right(),OutSTD.upper_left(),OutSTD.bottom_right(),slip::NCC);
#else
  slip::crosscorrelation2d<double>(I1.upper_left(),I1.bottom_right(),I2.upper_left(),I2.bottom_right(),OutSTD.upper_left(),OutSTD.bottom_right(),slip::CROSSCORRELATION_TYPE::SLIP_NCC);
#endif
  maxstd = std::max_element(OutSTD.upper_left(),OutSTD.bottom_right());
  std::cout<<"max NCC = ("<<maxstd.i()<<","<<maxstd.j()<<") = "<<*maxstd<<std::endl;
  slip::change_dynamic_01(OutSTD.upper_left(),OutSTD.bottom_right(),OutSTD.upper_left(),slip::AFFINE_FUNCTION);
  OutSTD.write("lena_crcorr_ncc.gif");

  std::cout << "...stop" << std::endl;	
}

