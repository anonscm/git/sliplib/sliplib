#include <iostream>
#include <algorithm>
#include <numeric>
#include "interpolation.hpp"
#include "Vector2d.hpp"
#include "Vector.hpp"
#include "Array.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "Matrix.hpp"
#include "Matrix3d.hpp"
#include "GrayscaleImage.hpp"
#include "linear_algebra.hpp"
#include "Polynomial.hpp"

int main()
{

  typedef double T;
  
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Linear interpolation " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  T ya = T(3.2);
  T yb = T(5.1);
  T mu = T(0.0);
  T y = slip::linear_interpolation(ya,yb,mu);
  std::cout<<"ya = "<<ya<<" yb = "<<yb<<" mu = "<<mu<<" y = "<<y<<std::endl;
  
  mu = T(1.0);
  y = slip::linear_interpolation(ya,yb,mu);
  std::cout<<"ya = "<<ya<<" yb = "<<yb<<" mu = "<<mu<<" y = "<<y<<std::endl;

  mu = T(0.5);
  y = slip::linear_interpolation(ya,yb,mu);
  std::cout<<"ya = "<<ya<<" yb = "<<yb<<" mu = "<<mu<<" y = "<<y<<std::endl;

  mu = T(0.2);
  y = slip::linear_interpolation(ya,yb,mu);
  std::cout<<"ya = "<<ya<<" yb = "<<yb<<" mu = "<<mu<<" y = "<<y<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Newton Polynomial interpolation " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<T> XN(4);
  XN[0] = T(0);
  XN[1] = T(2);
  XN[2] = T(4);
  XN[3] = T(6);
 
  slip::Array<T> YN(4);
  YN[0] = T(0);
  YN[1] = T(4);
  YN[2] = T(0);
  YN[3] = T(4);

  slip::Array<T> Ncoeff(4);
  slip::newton_polynomial_coeff(XN.begin(),XN.end(),
				YN.begin(),YN.end(),
				Ncoeff.begin(),Ncoeff.end(),3);
  std::cout<<"XN =\n"<<XN<<std::endl;
  std::cout<<"YN =\n"<<YN<<std::endl;
  std::cout<<"Ncoeff =\n"<<Ncoeff<<std::endl;
  T valN = T(2.2);

  
  std::cout<<"Evaluate P("<<valN<<") = "<<slip::newton_interpolation(Ncoeff.begin(),Ncoeff.end(),XN.begin(),XN.end(),valN)<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Polynomial interpolation - order 1" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<T> X(2);
  slip::Array<T> Y(2);
  
  X[0] = T(0.0);
  X[1] = T(1.0);
  Y[0] = ya;
  Y[1] = yb;
  y = slip::polynomial_interpolation(X.begin(),X.end(),
				     Y.begin(),Y.end(),
				     1,
				     mu);
  std::cout<<"X = "<<X<<std::endl;
  std::cout<<"Y = "<<Y<<std::endl;
  std::cout<<"mu = "<<mu<<" y = "<<y<<std::endl;
  y = slip::polynomial_interpolation(Y.begin(),Y.end(),
				     1,
				     mu);
  std::cout<<"X = "<<X<<std::endl;
  std::cout<<"Y = "<<Y<<std::endl;
  std::cout<<"mu = "<<mu<<" y = "<<y<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Linear interpolation of slip::Vector2d" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Vector2d<T> p1(T(1.2),T(1.3));
  slip::Vector2d<T> p2(T(2.2),T(3.3));
  slip::Vector2d<T> yp;
  mu = T(0.0);
  yp = slip::linear_interpolation(p1,p2,mu);
  std::cout<<"p1 = "<<p1<<" p2 = "<<p2<<" mu = "<<mu<<" yp = "<<yp<<std::endl;

  mu = T(1.0);
  yp = slip::linear_interpolation(p1,p2,mu);
  std::cout<<"p1 = "<<p1<<" p2 = "<<p2<<" mu = "<<mu<<" yp = "<<yp<<std::endl;

  mu = T(0.5);
  yp = slip::linear_interpolation(p1,p2,mu);
  std::cout<<"p1 = "<<p1<<" p2 = "<<p2<<" mu = "<<mu<<" yp = "<<yp<<std::endl;

  mu = T(0.3);
  yp = slip::linear_interpolation(p1,p2,mu);
  std::cout<<"p1 = "<<p1<<" p2 = "<<p2<<" mu = "<<mu<<" yp = "<<yp<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Polynomial interpolation of slip::Vector2d - order 1" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<slip::Vector2d<T> > YP(2);
  YP[0] = p1;
  YP[1] = p2;
  yp = slip::polynomial_interpolation(X.begin(),X.end(),
				      YP.begin(),YP.end(),
				      1,
				      mu);

  std::cout<<"X = "<<X<<std::endl;
  std::cout<<"YP = "<<YP<<std::endl;
  std::cout<<"mu = "<<mu<<" yp = "<<yp<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Bilinear interpolation " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  T p00 = T(3.2);
  T p01 = T(5.1);
  T p10 = T(1.1);
  T p11 = T(4.1);
  T mui = T(0.0);  
  T muj = T(0.0);  
  T y2 = slip::bilinear_interpolation(p00,p01,p10,p11,mui,muj);
  std::cout<<"p00 = "<<p00<<" p01 = "<<p01<<" p10 = "<<p10<<" p11 = "<<p11<<" mui = "<<mui<<" muj = "<<muj<<" y2 = "<<y2<<std::endl;


  mui = T(0.0);  
  muj = T(1.0);  
  y2 = slip::bilinear_interpolation(p00,p01,p10,p11,mui,muj);
  std::cout<<"p00 = "<<p00<<" p01 = "<<p01<<" p10 = "<<p10<<" p11 = "<<p11<<" mui = "<<mui<<" muj = "<<muj<<" y2 = "<<y2<<std::endl;

  mui = T(1.0);  
  muj = T(0.0);  
  y2 = slip::bilinear_interpolation(p00,p01,p10,p11,mui,muj);
  std::cout<<"p00 = "<<p00<<" p01 = "<<p01<<" p10 = "<<p10<<" p11 = "<<p11<<" mui = "<<mui<<" muj = "<<muj<<" y2 = "<<y2<<std::endl;


  mui = T(1.0);  
  muj = T(1.0);  
  y2 = slip::bilinear_interpolation(p00,p01,p10,p11,mui,muj);
  std::cout<<"p00 = "<<p00<<" p01 = "<<p01<<" p10 = "<<p10<<" p11 = "<<p11<<" mui = "<<mui<<" muj = "<<muj<<" y2 = "<<y2<<std::endl;


  mui = T(0.2);  
  muj = T(0.3);  
  y2 = slip::bilinear_interpolation(p00,p01,p10,p11,mui,muj);
  std::cout<<"p00 = "<<p00<<" p01 = "<<p01<<" p10 = "<<p10<<" p11 = "<<p11<<" mui = "<<mui<<" muj = "<<muj<<" y2 = "<<y2<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "2d polynomial interpolation -order 1" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  slip::Array2d<T> PP(2,2);
  PP[0][0] = p00;
  PP[0][1] = p01;
  PP[1][0] = p10;
  PP[1][1] = p11;
  slip::Array<T> Mu(2);
  Mu[0] = mui;
  Mu[1] = muj;
  y2 = slip::polynomial_interpolation2d(PP.upper_left(),
					PP.bottom_right()
					,1,Mu);
  std::cout<<"PP = \n"<<PP<<std::endl;
  std::cout<<"Mu = \n"<<Mu<<std::endl;
  std::cout<<"y2 = "<<y2<<std::endl;



  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Trilinear interpolation " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  T p000 = T(3.2);
  T p001 = T(5.1);
  T p010 = T(1.1);
  T p011 = T(4.1);
  T p100 = T(1.2);
  T p101 = T(2.3);
  T p110 = T(3.4);
  T p111 = T(2.7);
  T mukk = T(0.0);  
  T muii = T(0.0);  
  T mujj = T(0.0);  
  T y3 = slip::trilinear_interpolation(p000,p001,p010,p011,
				       p100,p101,p110,p111,
				       mukk,muii,mujj);
  std::cout<<"p000 = "<<p000<<" p001 = "<<p001<<" p010 = "<<p010<<" p011 = "<<p011<<std::endl;
  std::cout<<"p100 = "<<p100<<" p101 = "<<p101<<" p110 = "<<p110<<" p111 = "<<p111<<std::endl;
  std::cout<<" mukk = "<<mukk<<" muii = "<<muii<<" mujj = "<<mujj<<" y3 = "<<y3<<std::endl;
 
  mukk = T(0.0);  
  muii = T(1.0);  
  mujj = T(0.0);  
  y3 = slip::trilinear_interpolation(p000,p001,p010,p011,
				       p100,p101,p110,p111,
				       mukk,muii,mujj);
  std::cout<<"p000 = "<<p000<<" p001 = "<<p001<<" p010 = "<<p010<<" p011 = "<<p011<<std::endl;
  std::cout<<"p100 = "<<p100<<" p101 = "<<p101<<" p110 = "<<p110<<" p111 = "<<p111<<std::endl;
  std::cout<<" mukk = "<<mukk<<" muii = "<<muii<<" mujj = "<<mujj<<" y3 = "<<y3<<std::endl;
 
  mukk = T(0.0);  
  muii = T(0.0);  
  mujj = T(1.0);  
  y3 = slip::trilinear_interpolation(p000,p001,p010,p011,
				       p100,p101,p110,p111,
				       mukk,muii,mujj);
  std::cout<<"p000 = "<<p000<<" p001 = "<<p001<<" p010 = "<<p010<<" p011 = "<<p011<<std::endl;
  std::cout<<"p100 = "<<p100<<" p101 = "<<p101<<" p110 = "<<p110<<" p111 = "<<p111<<std::endl;
  std::cout<<" mukk = "<<mukk<<" muii = "<<muii<<" mujj = "<<mujj<<" y3 = "<<y3<<std::endl;
 

  mukk = T(0.0);  
  muii = T(1.0);  
  mujj = T(1.0);  
  y3 = slip::trilinear_interpolation(p000,p001,p010,p011,
				     p100,p101,p110,p111,
				     mukk,muii,mujj);
  std::cout<<"p000 = "<<p000<<" p001 = "<<p001<<" p010 = "<<p010<<" p011 = "<<p011<<std::endl;
  std::cout<<"p100 = "<<p100<<" p101 = "<<p101<<" p110 = "<<p110<<" p111 = "<<p111<<std::endl;
  std::cout<<" mukk = "<<mukk<<" muii = "<<muii<<" mujj = "<<mujj<<" y3 = "<<y3<<std::endl;
 

  mukk = T(1.0);  
  muii = T(0.0);  
  mujj = T(0.0);  
  y3 = slip::trilinear_interpolation(p000,p001,p010,p011,
				     p100,p101,p110,p111,
				     mukk,muii,mujj);
  std::cout<<"p000 = "<<p000<<" p001 = "<<p001<<" p010 = "<<p010<<" p011 = "<<p011<<std::endl;
  std::cout<<"p100 = "<<p100<<" p101 = "<<p101<<" p110 = "<<p110<<" p111 = "<<p111<<std::endl;
  std::cout<<" mukk = "<<mukk<<" muii = "<<muii<<" mujj = "<<mujj<<" y3 = "<<y3<<std::endl;

  mukk = T(1.0);  
  muii = T(1.0);  
  mujj = T(0.0);  
  y3 = slip::trilinear_interpolation(p000,p001,p010,p011,
				     p100,p101,p110,p111,
				     mukk,muii,mujj);
  std::cout<<"p000 = "<<p000<<" p001 = "<<p001<<" p010 = "<<p010<<" p011 = "<<p011<<std::endl;
  std::cout<<"p100 = "<<p100<<" p101 = "<<p101<<" p110 = "<<p110<<" p111 = "<<p111<<std::endl;
  std::cout<<" mukk = "<<mukk<<" muii = "<<muii<<" mujj = "<<mujj<<" y3 = "<<y3<<std::endl;
 

  mukk = T(1.0);  
  muii = T(1.0);  
  mujj = T(1.0);  
  y3 = slip::trilinear_interpolation(p000,p001,p010,p011,
				     p100,p101,p110,p111,
				     mukk,muii,mujj);
  std::cout<<"p000 = "<<p000<<" p001 = "<<p001<<" p010 = "<<p010<<" p011 = "<<p011<<std::endl;
  std::cout<<"p100 = "<<p100<<" p101 = "<<p101<<" p110 = "<<p110<<" p111 = "<<p111<<std::endl;
  std::cout<<" mukk = "<<mukk<<" muii = "<<muii<<" mujj = "<<mujj<<" y3 = "<<y3<<std::endl;
 
  mukk = T(0.2);  
  muii = T(0.3);  
  mujj = T(0.4);  
  y3 = slip::trilinear_interpolation(p000,p001,p010,p011,
				     p100,p101,p110,p111,
				     mukk,muii,mujj);
  std::cout<<"p000 = "<<p000<<" p001 = "<<p001<<" p010 = "<<p010<<" p011 = "<<p011<<std::endl;
  std::cout<<"p100 = "<<p100<<" p101 = "<<p101<<" p110 = "<<p110<<" p111 = "<<p111<<std::endl;
  std::cout<<" mukk = "<<mukk<<" muii = "<<muii<<" mujj = "<<mujj<<" y3 = "<<y3<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "3d polynomial interpolation - order 1" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Array3d<T> PPP(2,2,2);
  PPP[0][0][0] = p000;
  PPP[0][0][1] = p001;
  PPP[0][1][0] = p010;
  PPP[0][1][1] = p011;
  PPP[1][0][0] = p100;
  PPP[1][0][1] = p101;
  PPP[1][1][0] = p110;
  PPP[1][1][1] = p111;

  slip::Array<T> Mu3(3);
  
  Mu3[0] = mukk;
  Mu3[1] = muii;
  Mu3[2] = mujj;
  y3 = slip::polynomial_interpolation3d(PPP.front_upper_left(),
					PPP.back_bottom_right(),
					1,Mu3);
  std::cout<<"PPP = \n"<<PPP<<std::endl;
  std::cout<<"Mu3 = \n"<<Mu3<<std::endl;
  std::cout<<"y3 = "<<y3<<std::endl;


 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "cubic spline " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<double> xsp(6);
  xsp[0] = 1.0;
  xsp[1] = 2.0;
  xsp[2] = 4.0;
  xsp[3] = 5.0;
  xsp[4] = 8.0;
  xsp[5] = 10.0;
  slip::Array<double> ysp(xsp.size());
  ysp[0] = 2.0;
  ysp[1] = 1.5;
  ysp[2] = 1.25;
  ysp[3] = 1.2;
  ysp[4] = 1.125;
  ysp[5] = 1.1;
 
  slip::Array<double> diagsp(xsp.size());
  slip::Array<double> up_diagsp(xsp.size()-1);
  slip::Array<double> low_diagsp(xsp.size()-1);
  
  slip::Array<double> bsp(xsp.size());

  slip::cubic_spline_system_main(xsp.begin(),xsp.end(),
			    ysp.begin(),ysp.end(),
			    diagsp.begin(),diagsp.end(),
			    up_diagsp.begin(),up_diagsp.end(),
			    low_diagsp.begin(),low_diagsp.end(),
			    bsp.begin(),bsp.end());
  std::cout<<"xsp = \n"<<xsp<<std::endl;
  std::cout<<"ysp = \n"<<ysp<<std::endl;
  std::cout<<"diagsp = \n"<<diagsp<<std::endl;
  std::cout<<"up_diagsp = \n"<<up_diagsp<<std::endl;
  std::cout<<"low_diagsp = \n"<<low_diagsp<<std::endl;
  std::cout<<"bsp = \n"<<bsp<<std::endl;
  
  std::cout<<"natural spline boundary condition "<<std::endl;
  slip::cubic_spline_system_boundary_natural(xsp.begin(),xsp.end(),
					     ysp.begin(),ysp.end(),
					     diagsp.begin(),diagsp.end(),
					     up_diagsp.begin(),up_diagsp.end(),
					     low_diagsp.begin(),low_diagsp.end(),
					     bsp.begin(),bsp.end());
    
  std::cout<<"diagsp = \n"<<diagsp<<std::endl;
  std::cout<<"up_diagsp = \n"<<up_diagsp<<std::endl;
  std::cout<<"low_diagsp = \n"<<low_diagsp<<std::endl;
  std::cout<<"bsp = \n"<<bsp<<std::endl;
  slip::Array<double> coeffsp(xsp.size());
  slip::thomas_solve( diagsp.begin(),diagsp.end(),
		      up_diagsp.begin(),up_diagsp.end(),
		      low_diagsp.begin(),low_diagsp.end(),
		      coeffsp.begin(),coeffsp.end(),
		      bsp.begin(),bsp.end());
  std::cout<<"coeffsp = \n"<<coeffsp<<std::endl;

  slip::cubic_spline_coefficients(xsp.begin(),xsp.end(),
				  ysp.begin(),ysp.end(),
				  coeffsp.begin(),coeffsp.end());
  std::cout<<"coeffsp = \n"<<coeffsp<<std::endl;

  for(std::size_t i = 0; i < xsp.size(); ++i)
    {
      std::cout<<"eval("<<xsp[i]<<") = "<<slip::cubic_spline_eval(xsp.begin(),xsp.end(),ysp.begin(),ysp.end(),coeffsp.begin(),coeffsp.end(),xsp[i])<<std::endl;
    }
  std::cout<<"eval("<<3.0<<") = "<<slip::cubic_spline_eval(xsp.begin(),xsp.end(),ysp.begin(),ysp.end(),coeffsp.begin(),coeffsp.end(),3.0)<<std::endl;
  std::cout<<"eval("<<4.5<<") = "<<slip::cubic_spline_eval(xsp.begin(),xsp.end(),ysp.begin(),ysp.end(),coeffsp.begin(),coeffsp.end(),4.5)<<std::endl;
  std::cout<<"eval("<<5.5<<") = "<<slip::cubic_spline_eval(xsp.begin(),xsp.end(),ysp.begin(),ysp.end(),coeffsp.begin(),coeffsp.end(),5.5)<<std::endl;
  std::cout<<"eval("<<6.0<<") = "<<slip::cubic_spline_eval(xsp.begin(),xsp.end(),ysp.begin(),ysp.end(),coeffsp.begin(),coeffsp.end(),6.0)<<std::endl;
  std::cout<<"eval("<<7.0<<") = "<<slip::cubic_spline_eval(xsp.begin(),xsp.end(),ysp.begin(),ysp.end(),coeffsp.begin(),coeffsp.end(),7.0)<<std::endl;
  std::cout<<"eval("<<9.0<<") = "<<slip::cubic_spline_eval(xsp.begin(),xsp.end(),ysp.begin(),ysp.end(),coeffsp.begin(),coeffsp.end(),9.0)<<std::endl;
  std::cout<<"eval("<<9.5<<") = "<<slip::cubic_spline_eval(xsp.begin(),xsp.end(),ysp.begin(),ysp.end(),coeffsp.begin(),coeffsp.end(),9.5)<<std::endl;

  slip::Array<T> xxsp(19);
  slip::iota(xxsp.begin(),xxsp.end(),xsp[0],0.5);
  std::cout<<"xxsp = \n"<<xxsp<<std::endl;
  slip::Array<T> yysp(19);
  slip::cubic_spline(xsp.begin(),xsp.end(),
		     ysp.begin(),ysp.end(),
		     xxsp.begin(),xxsp.end(),
		     yysp.begin(),yysp.end());
 
  std::cout<<"yysp = \n"<<yysp<<std::endl;
  
  // slip::Array<double> xsp2(6);
//   slip::iota(xsp2.begin(),xsp2.end(),1.0,2.0);
//   slip::cubic_spline_system(xsp2.begin(),xsp2.end(),
// 			    ysp.begin(),ysp.end(),
// 			    diagsp.begin(),diagsp.end(),
// 			    up_diagsp.begin(),up_diagsp.end(),
// 			    low_diagsp.begin(),low_diagsp.end(),
// 			    bsp.begin(),bsp.end());
//   std::cout<<"diagsp = \n"<<diagsp<<std::endl;
//   std::cout<<"up_diagsp = \n"<<up_diagsp<<std::endl;
//   std::cout<<"low_diagsp = \n"<<low_diagsp<<std::endl;
//   std::cout<<"bsp = \n"<<bsp<<std::endl;

//   slip::Array<double> diag2sp(3);
//   slip::regular_cubic_spline_system(2.0,
// 				    ysp.begin(),ysp.end(),
// 				    diagsp.begin(),diagsp.end(),
// 				    diag2sp.begin(),diag2sp.end(),
// 				    bsp.begin(),bsp.end());
//   std::cout<<"diagsp = \n"<<diagsp<<std::endl;
//   std::cout<<"diag2sp = \n"<<diag2sp<<std::endl;
//   std::cout<<"bsp = \n"<<bsp<<std::endl;

slip::Vector<double> xsp3(8);
  // xsp3[0] = 0.0;
  // xsp3[1] = 1.0;
  // xsp3[2] = 2.5;
  // xsp3[3] = 3.6;
  // xsp3[4] = 5.0;
  // xsp3[5] = 7.0;
  // xsp3[6] = 8.1;
  // xsp3[7] = 10.0;
  xsp3[0] = 0.0;
  xsp3[1] = 1.0;
  xsp3[2] = 2.0;
  xsp3[3] = 3.0;
  xsp3[4] = 4.0;
  xsp3[5] = 5.0;
  xsp3[6] = 6.0;
  xsp3[7] = 7.0;
  slip::Vector<double> ysp3 = slip::sin(xsp3);
  // ysp3[0] = std::sin(sp);
  // ysp3[1] = 0.84147;
  // ysp3[2] = 0.59847;
  // ysp3[3] = -0.44252;
  // ysp3[4] = -0.95892;
  // ysp3[5] = 0.65699;
  // ysp3[6] = 0.96989;
  // ysp3[7] = -0.54402;
  
  slip::Array<double> diagsp3(xsp3.size());
  slip::Array<double> up_diagsp3(xsp3.size()-1);
  slip::Array<double> low_diagsp3(xsp3.size()-1);
  
  slip::Array<double> bsp3(xsp3.size());
  slip::cubic_spline_system_main(xsp3.begin(),xsp3.end(),
				 ysp3.begin(),ysp3.end(),
				 diagsp3.begin(),diagsp3.end(),
				 up_diagsp3.begin(),up_diagsp3.end(),
				 low_diagsp3.begin(),low_diagsp3.end(),
				 bsp3.begin(),bsp3.end());
  slip::cubic_spline_system_boundary_natural(xsp3.begin(),xsp3.end(),
					     ysp3.begin(),ysp3.end(),
					     diagsp3.begin(),diagsp3.end(),
					     up_diagsp3.begin(),up_diagsp3.end(),
					     low_diagsp3.begin(),low_diagsp3.end(),
					     bsp3.begin(),bsp3.end());
  std::cout<<"xsp3 = \n"<<xsp3<<std::endl;
  std::cout<<"ysp3 = \n"<<ysp3<<std::endl;
  std::cout<<"diagsp3 = \n"<<diagsp3<<std::endl;
  std::cout<<"up_diagsp3 = \n"<<up_diagsp3<<std::endl;
  std::cout<<"low_diagsp3 = \n"<<low_diagsp3<<std::endl;
  std::cout<<"bsp3 = \n"<<bsp3<<std::endl;

  slip::Matrix<double> Msp3(diagsp3.size(),diagsp3.size());
  slip::set_diagonal(diagsp3.begin(),diagsp3.end(),Msp3);
  slip::set_diagonal(up_diagsp3.begin(),up_diagsp3.end(),Msp3,1);
  slip::set_diagonal(low_diagsp3.begin(),low_diagsp3.end(),Msp3,-1);
  std::cout<<"Msp3 = \n"<<Msp3<<std::endl;
  
  
  slip::Array<double> coeffsp3(xsp3.size());
  slip::thomas_solve( diagsp3.begin(),diagsp3.end(),
		      up_diagsp3.begin(),up_diagsp3.end(),
		      low_diagsp3.begin(),low_diagsp3.end(),
		      coeffsp3.begin(),coeffsp3.end(),
		      bsp3.begin(),bsp3.end());
  std::cout<<"coeffsp3 = \n"<<coeffsp3<<std::endl;

  slip::cubic_spline_coefficients(xsp3.begin(),xsp3.end(),
				  ysp3.begin(),ysp3.end(),
				  coeffsp3.begin(),coeffsp3.end());
  std::cout<<"coeffsp3 = \n"<<coeffsp3<<std::endl;

  //slip::Array<T> xxsp3(41)
  slip::Array<T> xxsp3(29);
  slip::iota(xxsp3.begin(),xxsp3.end(),xsp3[0],0.25);
  std::cout<<"xxsp3 = \n"<<xxsp3<<std::endl;
  slip::Array<T> yysp3(xxsp3.size());
  slip::cubic_spline(xsp3.begin(),xsp3.end(),
		     ysp3.begin(),ysp3.end(),
		     xxsp3.begin(),xxsp3.end(),
		     yysp3.begin(),yysp3.end());
 
  std::cout<<"yysp3 = \n"<<yysp3<<std::endl;
 
  slip::Vector<double> xsp4(3);
  xsp4[0] = -1.0;
  xsp4[1] = 0.0;
  xsp4[2] = 3.0;
  slip::Vector<double> ysp4(xsp4.size());
  ysp4[0] = 0.5;
  ysp4[1] = 0.0;
  ysp4[2] = 3.0;
  std::cout<<"xsp4 = \n"<<xsp4<<std::endl;
  std::cout<<"ysp4 = \n"<<ysp4<<std::endl;

  slip::Array<double> diagsp4(xsp4.size());
  slip::Array<double> up_diagsp4(xsp4.size()-1);
  slip::Array<double> low_diagsp4(xsp4.size()-1);
  
  slip::Array<double> bsp4(xsp4.size());
  slip::cubic_spline_system_main(xsp4.begin(),xsp4.end(),
			    ysp4.begin(),ysp4.end(),
			    diagsp4.begin(),diagsp4.end(),
			    up_diagsp4.begin(),up_diagsp4.end(),
			    low_diagsp4.begin(),low_diagsp4.end(),
			    bsp4.begin(),bsp4.end());
  slip::cubic_spline_system_boundary_natural(xsp4.begin(),xsp4.end(),
					     ysp4.begin(),ysp4.end(),
					     diagsp4.begin(),diagsp4.end(),
					     up_diagsp4.begin(),up_diagsp4.end(),
					     low_diagsp4.begin(),low_diagsp4.end(),
					     bsp4.begin(),bsp4.end());
  
  slip::Matrix<double> Msp4(diagsp4.size(),diagsp4.size());
  slip::set_diagonal(diagsp4.begin(),diagsp4.end(),Msp4);
  slip::set_diagonal(up_diagsp4.begin(),up_diagsp4.end(),Msp4,1);
  slip::set_diagonal(low_diagsp4.begin(),low_diagsp4.end(),Msp4,-1);
  std::cout<<"Msp4 = \n"<<Msp4<<std::endl;
  slip::Vector<double> Vbsp4(bsp4.begin(),bsp4.end());
  std::cout<<"Vbsp4 = \n"<<Vbsp4<<std::endl;

 
  slip::Vector<double> coeffsp4(xsp4.size());
  slip::cubic_spline_coefficients(xsp4.begin(),xsp4.end(),
				  ysp4.begin(),ysp4.end(),
				  coeffsp4.begin(),coeffsp4.end());
  std::cout<<"coeffsp4 = \n"<<coeffsp4<<std::endl;

 
  
  slip::Array<T> xxsp4(9);
  slip::iota(xxsp4.begin(),xxsp4.end(),xsp4[0],0.5);
  std::cout<<"xxsp4 = \n"<<xxsp4<<std::endl;
  slip::Array<T> yysp4(xxsp4.size());
  slip::cubic_spline(xsp4.begin(),xsp4.end(),
		     ysp4.begin(),ysp4.end(),
		     xxsp4.begin(),xxsp4.end(),
		     yysp4.begin(),yysp4.end());
 
  std::cout<<"yysp4 = \n"<<yysp4<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Bspline " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
// Datas From abs = 0->8
double datas[]={1.0,5.0,6.0,8.0,7.0,4.0,3.0,2.0,1.0,5.0};
slip::Array2d<double> coeff(10,1);

slip::Array<double> Poles(4);

 slipalgo::BSplinePoles(3,Poles);
std::cout<<Poles<<std::endl;
 std::cout << "start\n";
std::cout << slipalgo::BSplineInitialCausalCoefficient(datas,datas+10,Poles[0],std::numeric_limits<double>::epsilon())<<std::endl;
std::cout << "start2\n";
slipalgo::BSplineComputeInterpolationCoefficients(datas,datas+10,coeff.begin(), Poles, std::numeric_limits<double>::epsilon() );

 std::cout<< coeff<<"----->"<<slipalgo::BSplineInterpolatedValue (coeff,0.0,2.5,3)<<std::endl;
std::cout << "end\n";

std::cout << coeff<<std::endl;

double d[] = {1.2,3.4,5.5,6.6,7.7,8.8};
slip::Matrix<double> Min(2,3,d);
slip::Matrix<double> Mout(2,3);


slipalgo::BSplineSamplesToCoefficients(3,Min,Mout);
std::cout<<Min<<std::endl;
std::cout<<Mout<<std::endl;
std::cout<< "----->"<<slipalgo::BSplineInterpolatedValue (Mout,0.5,0.5,9)<<std::endl;


// slip::GrayscaleImage<double> ImageIn;
// ImageIn.read("lena.jpg");

// slip::GrayscaleImage<double> ImageOut(ImageIn.rows(),ImageIn.cols());
// slipalgo::BSplineGeometricTransformation(ImageIn,ImageOut,3,0,0,15,0,0,true);
// ImageOut.write("interpol.png");

}

