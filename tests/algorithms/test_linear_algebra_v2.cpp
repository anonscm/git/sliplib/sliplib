#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <complex>
#include "linear_algebra.hpp"
#include "linear_algebra_qr.hpp"
#include "linear_algebra_eigen.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "Array2d.hpp"
#include "Block2d.hpp"
#include "threshold.hpp"

template<typename T>
void accum(T& a){static T init = T(0); a = (init+=T(1));}

int main()
{
  std::size_t Nr = 4;
  //std::size_t Nc = 4;
  typedef std::complex<double> TC;
  //  typedef std::complex<double> T;
//   T d[] = {T(1,1),T(0,1),T(0,1),T(0,1),
// 	   T(1,0),T(1,1),T(0,1),T(0,1),
// 	   T(1,0),T(1,0),T(1,1),T(0,1),
// 	   T(1,0),T(1,0),T(1,0),T(1,1)};
  //   T(1,2),T(1,2),T(1,2),T(1,2)};
   typedef double T;
  // T d[] = {T(1),T(0),T(0),T(2),
  // 	   T(4),T(1),T(0),T(0),
  // 	   T(1),T(9),T(1),T(0),
  // 	   T(1),T(1),T(1),T(1)};

//   T d[] = {T(1),T(0),T(0),T(2),
// 	   T(1),T(1),T(0),T(0),
// 	   T(1),T(1),T(1),T(0),
// 	   T(1),T(1),T(1),T(1)};
  //	   T(2),T(1),T(1),T(1)};


  //******************************************************************//
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Gram-Schmidt QR decomposition" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<T> Ags(4,4);
  slip::Matrix<T> Qgs(4,4);
  slip::Matrix<T> Rgs(4,4);
  for(std::size_t i = 0; i < Ags.rows(); ++i)
    {
      for(std::size_t j = 0; j < Ags.cols(); ++j)
	{
	  Ags[i][j] = 4 + (i-j);
	}
    }
  std::cout<<"Ags = \n"<<Ags<<std::endl;
  slip::gram_schmidt_qr(Ags.upper_left(),Ags.bottom_right(),
			Qgs.upper_left(),Qgs.bottom_right(),
			Rgs.upper_left(),Rgs.bottom_right(),
			1e-06);
  std::cout<<"Qgs = \n"<<Qgs<<std::endl;
  std::cout<<"Rgs = \n"<<Rgs<<std::endl;
  slip::Matrix<T> Tgs(4,4);
  slip::matrix_matrix_multiplies(Rgs,Qgs,Tgs);
  std::cout<<"Tgs = \n"<<Tgs<<std::endl;
  Qgs.fill(0.0);
  Rgs.fill(0.0);
  slip::gram_schmidt_qr(Ags,Qgs,Rgs,1e-06);
  std::cout<<"Qgs = \n"<<Qgs<<std::endl;
  std::cout<<"Rgs = \n"<<Rgs<<std::endl;
  slip::matrix_matrix_multiplies(Rgs,Qgs,Tgs);
  std::cout<<"Tgs = \n"<<Tgs<<std::endl;
   
 //******************************************************************//
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "QR algorithm" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<T> Mc(4,4);
  slip::hilbert(Mc);
  std::cout<<" Mc before \n"<<std::endl;
  std::cout<< Mc <<std::endl;
  slip::Matrix<T> Qc(Mc.rows(),Mc.rows());
  slip::Matrix<T> Rc(Mc.rows(),Mc.cols());
  //slip::QR_Transform(Mc,Qc,Rc);
  slip::householder_qr(Mc,Qc,Rc);
  std::cout<<" Qc \n"<<std::endl;
  std::cout<< Qc <<std::endl;
  std::cout<<" Rc \n"<<std::endl;
  std::cout<< Rc <<std::endl;
  slip::matrix_matrix_multiplies(Qc,Rc,Mc);
  std::cout<<" Mc after \n"<<std::endl;
  std::cout<< Mc <<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Complex Hessenberg QR" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<TC > Hc(4,4);
  slip::hilbert(Hc);
  std::cout<<"Hc = \n"<<Hc<<std::endl;
  slip::Vector<TC > V0c(Hc.rows());
  slip::householder_qr(Hc,V0c);
  std::cout<<"Hc = \n"<<Hc<<std::endl;
  std::cout<<"V0c = \n"<<V0c<<std::endl;
  
  
  slip::hilbert(Hc);
  std::cout<<"Hc = \n"<<Hc<<std::endl;
  slip::Matrix<TC > Qcc(4,4);
  slip::Matrix<TC > Rcc(4,4);
  slip::householder_qr(Hc,Qcc,Rcc);
  std::cout<<"Hc = \n"<<Hc<<std::endl;
  std::cout<<"Qcc = \n"<<Qcc<<std::endl;
  std::cout<<"Rcc = \n"<<Rcc<<std::endl;
  slip::Matrix<TC > QccRcc(Qcc.rows(),Qcc.cols());
  slip::matrix_matrix_multiplies(Qcc,Rcc,QccRcc);
  std::cout<<"QccRcc = \n"<<QccRcc<<std::endl;
 

 
 //******************************************************************//
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "2_by_2 shur decomposition" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  for(int i=0; i<=2; i++)
    {
      // T d2_2[] = {1.0,2.0,
      // 		       -T(i)/4.0,i};
      T elem3 = -T(i)/4.0;
      T elem4 = T(i);
      T d2_2[] = {1.0,2.0,elem3,elem4};
      slip::Matrix<T> A2_2(2,2,d2_2);
      slip::Matrix<T> Z2_2(slip::identity<slip::Matrix<T> >(2,2));
      slip::Matrix<T> D2_2(2,2,0.0);
      std::cout<< "A2_2 before : " <<std::endl;
      std::cout<< A2_2  <<std::endl;
      std::cout<< "Z2_2 before : " <<std::endl;
      std::cout<< Z2_2  <<std::endl;
      std::cout<< "D2_2 before : " <<std::endl;
      std::cout<< D2_2  <<std::endl;
      slip::Schur_factorization_2by2(A2_2.upper_left(),A2_2.bottom_right(),Z2_2.upper_left(),Z2_2.bottom_right(),D2_2.upper_left(),D2_2.bottom_right());
      std::cout<< "A2_2 after : " <<std::endl;
      std::cout<< A2_2  <<std::endl;
      std::cout<< "Z2_2 after : " <<std::endl;
      std::cout<< Z2_2  <<std::endl;
      std::cout<< "D2_2 after : " <<std::endl;
      std::cout<< D2_2  <<std::endl;
     
      slip::Matrix<T> Z2_2t(2,2,0.0);
      slip::Matrix<T> T2_2(2,2,0.0);
      std::cout<< "A2_2 = Z2_2 * D2_2 * Z2_2* : " <<std::endl;
      slip::matrix_matrix_multiplies(Z2_2,D2_2,T2_2);
      slip::hermitian_transpose(Z2_2,Z2_2t);
      slip::matrix_matrix_multiplies(T2_2,Z2_2t,A2_2);
      std::cout<< A2_2  <<std::endl;
    }
  //******************************************************************//
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Householder Matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  std::cout<< "M : " <<std::endl;
  slip::Matrix<T> M(Nr,Nr,0.0);
  std::for_each(M.begin(),M.end(),accum<T>);
  std::cout<< M <<std::endl;

  std::cout<< "V : " <<std::endl;
  slip::Vector<T> Vh(Nr,0.0);
  slip::VectorHouseholder(Vh,M.upper_left(),M.bottom_right());
  std::cout<< Vh <<std::endl;

  std::cout<< "H : " <<std::endl;
  slip::Matrix<T> H(Nr,Nr,0.0);
  slip::MatrixHouseholder(H,Vh.begin(),Vh.end());
  std::cout<< H <<std::endl;
 
  std::cout<< "H*M : " <<std::endl;
  slip::Matrix<T> HM(Nr,Nr,0.0);
  slip::matrix_matrix_multiplies(H,M,HM);
  std::cout<< HM <<std::endl;

 
 
 //******************************************************************//
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Schur Transform" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<" Mc before \n"<<std::endl;
  slip::Matrix<T> Tc(Mc.rows(),Mc.rows());
  slip::Matrix<T> Tctr(Mc.rows(),Mc.rows());
  slip::Matrix<T> Wc(Mc.rows(),Mc.cols());
  slip::Matrix<T> Temp(Mc.rows(),Mc.cols());
  slip::db_threshold(Mc.begin(),Mc.end(),Mc.begin(),-10E-5,10E-5,T(0));
  std::cout<< Mc <<std::endl;
  //slip::Hessenberg_Transform(Mc,Tc,Wc);
  slip::householder_hessenberg(Mc,Wc,Tc);
  slip::Vector<std::complex<T> > Eig(Mc.rows(),std::complex<T>(0));
  slip::Box2d<int> box(0,0,Mc.rows()-1,Mc.rows()-1);
  slip::Francis_Schur_decomp(Wc,Eig,Tc,box,10E-10,true);
  std::cout<<" Tc \n"<<std::endl;
  std::cout<< Tc <<std::endl;
  std::cout<<" Wc \n"<<std::endl;
  std::cout<< Wc <<std::endl;
  slip::matrix_matrix_multiplies(Tc,Wc,Temp);
  slip::hermitian_transpose(Tc,Tctr);
  slip::matrix_matrix_multiplies(Temp,Tctr,Mc);
  slip::db_threshold(Mc.begin(),Mc.end(),Mc.begin(),-10E-5,10E-5,T(0));
  std::cout<<" Mc after recalculation and threshold\n"<<std::endl;
  std::cout<< Mc <<std::endl;
  std::cout<<" Eigenvalues\n"<<std::endl;
  std::cout<< Eig <<std::endl;


  T db[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0};
  slip::Matrix<T> Mb(3,3,db);
  slip::Matrix<T> Hb(3,3);
  slip::Matrix<T> Zb(3,3);
  slip::Matrix<T> Zbtr(3,3);
  slip::Matrix<T> Tempb(3,3);
  slip::Vector<TC > Eigb(3);
  slip::Box2d<int> boxb(0,0,2,2);
 
  slip::householder_hessenberg(Mb,Hb,Zb);
  slip::Francis_Schur_decomp(Hb,Eigb,Zb,boxb,10E-10,true);
   
  std::cout<<" Mb \n"<<std::endl;
  std::cout<< Mb <<std::endl;
  std::cout<<" Zb \n"<<std::endl;
  std::cout<< Zb <<std::endl;
  std::cout<<" Hb \n"<<std::endl;
  std::cout<< Hb <<std::endl;
  slip::matrix_matrix_multiplies(Zb,Hb,Tempb);
  slip::hermitian_transpose(Zb,Zbtr);
  slip::matrix_matrix_multiplies(Tempb,Zbtr,Mb);
  std::cout<<" Mb after recalculation\n"<<std::endl;
  std::cout<< Mb <<std::endl;
  std::cout<<" Eigenvalues\n"<<std::endl;
  std::cout<< Eigb <<std::endl;

  Mb.fill(db,db+9);
  std::cout<<" Mb \n"<<std::endl;
  std::cout<< Mb <<std::endl;
  slip::schur(Mb,Hb,Zb,Eigb,true);
  std::cout<<" Mb \n"<<std::endl;
  std::cout<< Mb <<std::endl;
  std::cout<<" Hb \n"<<std::endl;
  std::cout<< Hb <<std::endl;
  std::cout<<" Zb \n"<<std::endl;
  std::cout<< Zb <<std::endl;
  std::cout<<" Eigenvalues\n"<<std::endl;
  std::cout<< Eigb <<std::endl;
  slip::matrix_matrix_multiplies(Zb,Hb,Tempb);
  slip::hermitian_transpose(Zb,Zbtr);
  slip::matrix_matrix_multiplies(Tempb,Zbtr,Mb);
  std::cout<<" Mb after recalculation\n"<<std::endl;
  std::cout<< Mb <<std::endl;

 //  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "Complex Schur Transform" << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   TC dbc[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0};
//   slip::Matrix<TC> Mbc(3,3,dbc);
//   slip::Matrix<TC> Hbc(3,3);
//   slip::Matrix<TC> Zbc(3,3);
//   slip::Matrix<TC> Zbtrc(3,3);
//   slip::Matrix<TC> Tempbc(3,3);
//   slip::Vector<TC > Eigbc(3);
//   slip::schur(Mbc,Hbc,Zbc,Eigbc,true);
//   std::cout<<" Mbc \n"<<std::endl;
//   std::cout<< Mbc <<std::endl;
//   std::cout<<" Hbc \n"<<std::endl;
//   std::cout<< Hbc <<std::endl;
//   std::cout<<" Zbc \n"<<std::endl;
//   std::cout<< Zbc <<std::endl;
//   std::cout<<" Eigenvalues\n"<<std::endl;
//   std::cout<< Eigbc <<std::endl;
//   slip::matrix_matrix_multiplies(Zbc,Hbc,Tempbc);
//   slip::hermitian_transpose(Zbc,Zbtrc);
//   slip::matrix_matrix_multiplies(Tempbc,Zbtrc,Mbc);
//   std::cout<<" Mbc after recalculation\n"<<std::endl;
//   std::cout<< Mbc <<std::endl;


  //******************************************************************//
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Balancing Matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<" Mc before \n"<<std::endl;
  slip::db_threshold(Mc.begin(),Mc.end(),Mc.begin(),-10E-5,10E-5,T(0));
  std::cout<< Mc <<std::endl;
  slip::Matrix<T> Bal(Mc.rows(),Mc.rows());
  slip::balance(Mc,Bal);
  std::cout<<" Mc balanced \n"<<std::endl;
  std::cout<< Mc <<std::endl;
  std::cout<<" Diagonal scaling Matrix\n"<<std::endl;
  std::cout<< Bal <<std::endl;
  slip::householder_hessenberg(Mc,Wc,Tc);
  slip::Francis_Schur_decomp(Wc,Eig,Tc,box,10E-10,true);
  std::cout<<" Tc \n"<<std::endl;
  std::cout<< Tc <<std::endl;
  std::cout<<" Wc \n"<<std::endl;
  std::cout<< Wc <<std::endl;
  slip::matrix_matrix_multiplies(Tc,Wc,Temp);
  slip::hermitian_transpose(Tc,Tctr);
  slip::matrix_matrix_multiplies(Temp,Tctr,Mc);
  slip::Matrix<T> Balinv(Mc.rows(),Mc.rows());
  slip::inverse(Bal,Balinv);
  slip::matrix_matrix_multiplies(Bal,Mc,Temp);
  slip::matrix_matrix_multiplies(Temp,Balinv,Mc);
  slip::db_threshold(Mc.begin(),Mc.end(),Mc.begin(),-10E-5,10E-5,T(0));
  std::cout<<" Mc after recalculation and threshold\n"<<std::endl;
  std::cout<< Mc <<std::endl;
  std::cout<<" Eigenvalues\n"<<std::endl;
  std::cout<< Eig <<std::endl;

 

 

 //******************************************************************//
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Sylvester_solve" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  T ds[] = {T(1),T(5),T(3),T(2),
	    T(4),T(2),T(9),T(0),
	    T(0),T(0),T(3),T(0),
	    T(0),T(0),T(1),T(4)};
  slip::Matrix<T> Ss(4,4,ds);
  std::cout << "Ss : " << std::endl;
  std::cout << Ss << std::endl;

  for(std::size_t p = 1 ; p<3 ; p++)
    for(std::size_t q = 1 ; q<3 ; q++)
      {
	slip::Matrix<T> Xs(q,p,T(0));
	slip::Box2d<int> b11s(0,0,p-1,p-1);
	slip::Box2d<int> b22s(p,p,p+q-1,p+q-1);
	T g = 1.0;
 	if(slip::Sylvester_solve(Ss,b11s,b22s,g,Xs))
 	  std::cout << "Sylvester ok!\n";
	
	std::cout<< "b11s : " << b11s << std::endl;
	std::cout<< "b22s : " << b22s << std::endl;
	std::cout<< "p : " << p << std::endl;
	std::cout<< "q : " << q << std::endl;
	std::cout << "Xs : " << std::endl;
	std::cout << Xs << std::endl;
      }


 //  slip::Matrix<T> X111(1,1);
//   X111 = 4.2;
//   slip::Matrix<T> X222(2,2);
//   X222(0,0) = 1.1; X222(0,1) = 3.3;
//   X222(1,0) = 2.2; X222(1,1) = 4.4;
//   if(slip::Sylvester_solve(Ss,b11s,b22s,g,Xs))
//  	  std::cout << "Sylvester ok!\n";
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Eigen values" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   Mb.fill(db,db+9);
  std::cout<<" Mb \n"<<std::endl;
  std::cout<< Mb <<std::endl;
  slip::eigen(Mb,Eigb,false);
  std::cout<<" Mb \n"<<std::endl;
  std::cout<< Mb <<std::endl;
  std::cout<<" Eigenvalues\n"<<std::endl;
  std::cout<< Eigb <<std::endl;
  slip::eigen(Mb,Eigb,true);
  std::cout<<" Mb \n"<<std::endl;
  std::cout<< Mb <<std::endl;
  std::cout<<" Eigenvalues\n"<<std::endl;
  std::cout<< Eigb <<std::endl;

  

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "sin cos from symetric 2x2 matrix " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<T> Mat2(2,2);
  Mat2[0][0] = 3.0; Mat2[0][1] = 2.0;
  Mat2[1][0] = 2.0; Mat2[1][1] = 1.0;

  T s = T();
  T c = T();
  slip::computes_sin_cos(Mat2[0][0],Mat2[0][1],Mat2[1][1],s,c);
  std::cout<<"Mat2 = \n"<<Mat2<<std::endl;
  std::cout<<"cos(theta) = "<<c<<std::endl;
  std::cout<<"sin(theta) = "<<s<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "eigen values and vectors from symmetric 2x2 matrix " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Vector<T> eigenvalues(2);
  slip::Matrix<T> eigenvectors(2,2);
  
  

  slip::eigen_symmetric_2x2(Mat2[0][0],Mat2[0][1],Mat2[1][1],
			   eigenvalues,
			   eigenvectors);
  std::cout<<"eigenvalues  = \n"<<eigenvalues<<std::endl;
  std::cout<<"eigenvectors = \n"<<eigenvectors<<std::endl;


  slip::Matrix<T> eigenvectorsT(2,2);
  slip::transpose(eigenvectors,eigenvectorsT);
  slip::Matrix<T> tmp(2,2);
  slip::Matrix<T> Mat3(2,2);
  slip::Matrix<T> Diag(2,2,T());
   Diag[0][0] = eigenvalues[0];
   Diag[1][1] = eigenvalues[1];
   
  slip::matrix_matrix_multiplies(eigenvectors,Diag,tmp);
  slip::matrix_matrix_multiplies(tmp,eigenvectorsT,Mat3);
  std::cout<<"Mat3 = \n"<<Mat3<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "sorted eigen values and vectors from symmetric 2x2 matrix " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
slip::eigen_symmetric_2x2(Mat2[0][0],Mat2[0][1],Mat2[1][1],
			  eigenvalues,
			  eigenvectors,
			  true);
  std::cout<<"eigenvalues  = \n"<<eigenvalues<<std::endl;
  std::cout<<"eigenvectors = \n"<<eigenvectors<<std::endl;

   Diag[0][0] = eigenvalues[0];
   Diag[1][1] = eigenvalues[1];
  std::cout<<"Diag = \n"<<Diag<<std::endl;
  slip::matrix_matrix_multiplies(eigenvectors,Diag,tmp);
  slip::transpose(eigenvectors,eigenvectorsT);
  slip::matrix_matrix_multiplies(tmp,eigenvectorsT,Mat3);
  std::cout<<"Mat3 = \n"<<Mat3<<std::endl;

  slip::eigen_symmetric_2x2(Mat2,
			    eigenvalues,
			    eigenvectors,
			    true);
  std::cout<<"eigenvalues  = \n"<<eigenvalues<<std::endl;
  std::cout<<"eigenvectors = \n"<<eigenvectors<<std::endl;

  

  return 0;  
}

