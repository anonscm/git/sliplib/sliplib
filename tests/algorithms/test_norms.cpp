#include <iostream>
#include <algorithm>
#include <iomanip>
#include <numeric>
#include "norms.hpp"
#include "Matrix.hpp"
#include <vector>

template<typename T>
bool lt5Predicate (const T& val)
{
  return (val < T(5));
}

int main()
{
  
  slip::Array2d<float> M(4,5);
  
  float k = 0.0;
  for(std::size_t i = 0; i < M.dim1(); ++i)
    {
      for(std::size_t j = 0; j < M.dim2(); ++j)
	{
	  M[i][j] = k;
	  k+=1.0;
	}
    }
  
  std::cout<<M<<std::endl;
  std::cout<<slip::L2_norm<float>(M.begin(),M.end())<<std::endl;
  std::cout<<slip::Euclidean_norm<float>(M.begin(),M.end())<<std::endl;
  std::cout<<slip::L22_norm<float>(M.begin(),M.end())<<std::endl;
  std::cout<<slip::L1_norm<float>(M.begin(),M.end())<<std::endl;
  std::cout<<slip::infinite_norm<float>(M.begin(),M.end())<<std::endl;
  

  float f[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
  slip::Array2d<float> M2(2,3,f);
  std::cout<<slip::L2_norm<float>(M2.begin(),M2.end())<<std::endl;
  std::cout<<slip::Euclidean_norm<float>(M2.begin(),M2.end())<<std::endl;
  std::cout<<slip::L22_norm<float>(M2.begin(),M2.end())<<std::endl;
  std::cout<<slip::L1_norm<float>(M2.begin(),M2.end())<<std::endl;
  std::cout<<slip::infinite_norm<float>(M2.begin(),M2.end())<<std::endl;
  


  std::vector<float> v(5,-2.0);
  std::cout<<slip::L2_norm<float>(v.begin(),v.end())<<std::endl;
  std::cout<<slip::Euclidean_norm<float>(v.begin(),v.end())<<std::endl;
  std::cout<<slip::L22_norm<float>(v.begin(),v.end())<<std::endl;
  std::cout<<slip::L1_norm<float>(v.begin(),v.end())<<std::endl;
  std::cout<<slip::infinite_norm<float>(v.begin(),v.end())<<std::endl;
 
  slip::Array2d<float> M3(2,3);
  slip::iota(M3.begin(),M3.end(),1.0,1.0);
  float d[] = {1,0,1,0,1,0};
  slip::Array2d<float> Mask(2,3,d);

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"L1_norm_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M3="<<std::endl;
std::cout<<M3<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
std::cout<<slip::L1_norm_mask<float>(M3.begin(),M3.end(),Mask.begin(),1)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"L1_norm_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M3="<<std::endl;
std::cout<<M3<<std::endl;
std::cout<<slip::L1_norm_if<float>(M3.begin(),M3.end(),lt5Predicate<float>)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"L22_norm_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M3="<<std::endl;
std::cout<<M3<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
std::cout<<slip::L22_norm_mask<float>(M3.begin(),M3.end(),Mask.begin(),1)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"L22_norm_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M3="<<std::endl;
std::cout<<M3<<std::endl;
std::cout<<slip::L22_norm_if<float>(M3.begin(),M3.end(),lt5Predicate<float>)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"L2_norm_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M3="<<std::endl;
std::cout<<M3<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
std::cout<<slip::L2_norm_mask<float>(M3.begin(),M3.end(),Mask.begin(),1)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"L2_norm_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M3="<<std::endl;
std::cout<<M3<<std::endl;
std::cout<<slip::L2_norm_if<float>(M3.begin(),M3.end(),lt5Predicate<float>)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"infinite_norm_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M3="<<std::endl;
std::cout<<M3<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
std::cout<<slip::infinite_norm_mask<float>(M3.begin(),M3.end(),Mask.begin(),1)<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"infinite_norm_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M3="<<std::endl;
std::cout<<M3<<std::endl;
std::cout<<slip::infinite_norm_if<float>(M3.begin(),M3.end(),lt5Predicate<float>)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"Euclidean_norm_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M3="<<std::endl;
std::cout<<M3<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
std::cout<<slip::Euclidean_norm_mask<float>(M3.begin(),M3.end(),Mask.begin(),1)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"Euclidean_norm_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M3="<<std::endl;
std::cout<<M3<<std::endl;
std::cout<<slip::Euclidean_norm_if<float>(M3.begin(),M3.end(),lt5Predicate<float>)<<std::endl;



   
}
