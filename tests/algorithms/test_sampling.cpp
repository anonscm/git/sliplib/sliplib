#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include "GrayscaleImage.hpp"
#include "Volume.hpp"
#include "Array.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "arithmetic_op.hpp"

#include "sampling.hpp"

int main()
{
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "downsample 2 " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<float> I(21);
  slip::iota(I.begin(),I.end(),1.0f);
  std::cout<<"I = \n"<<I<<std::endl;
  slip::Array<float> res(I.size()/2);
  
  slip::downsample1d(I.begin(), I.end(), res.begin());
  std::cout<<"downsample_2(I) = \n"<<res<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "downsample M " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::size_t M = 3;
  std::cout<<"I = \n"<<I<<std::endl;
  slip::Array<float> res_3(I.size()/M);
  
  res_3.fill(0.0f);
  slip::downsample1d(I.begin(), I.end(), res_3.begin(),M);
  std::cout<<"downsample_"<<M<<"(I) = \n"<<res_3<<std::endl;

  res_3.fill(0.0f);
  slip::downsample(I.begin(),I.end(),res_3.begin(),res_3.end(),M);
  std::cout<<"downsample_"<<M<<"(I) = \n"<<res_3<<std::endl;
  
  
  res_3.fill(0.0f);
  slip::downsample(I,res_3,M);
  std::cout<<"downsample_"<<M<<"(I) = \n"<<res_3<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "upsample 2 " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<float> res_up2(I.size()*2);
  std::cout<<"I = \n"<<I<<std::endl;
  slip::upsample1d(I.begin(), I.end(), res_up2.begin(),res_up2.end());
  std::cout<<"upsample_2(I) = \n"<<res_up2<<std::endl;
  slip::upsample1d(I.begin(), I.end(), res_up2.begin(),res_up2.end(),2,false);
  std::cout<<"upsample_2(I) = \n"<<res_up2<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "upsample M " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<float> res_upM(I.size()*M);
  std::cout<<"I = \n"<<I<<std::endl;
  slip::upsample1d(I.begin(), I.end(), res_upM.begin(),res_upM.end(),M);
  std::cout<<"upsample_"<<M<<"(I) = \n"<<res_upM<<std::endl;
  slip::upsample1d(I.begin(), I.end(), res_upM.begin(),res_upM.end(),M,false);
  std::cout<<"upsample_"<<M<<"(I) = \n"<<res_upM<<std::endl;

 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "downsample2d 2 " << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 M = 2;
 slip::Array2d<float> I2d(12, 21);
 slip::iota(I2d.begin(),I2d.end(),1.0f);
 std::size_t new_rows = std::ceil(double(I2d.rows())/double(M));
 std::size_t new_cols = std::ceil(double(I2d.cols())/double(M));
 slip::Array2d<float> res2d_2(new_rows, new_cols);
 std::cout<<"row x cols = "<<new_rows<<" x "<<new_cols<<std::endl;
 slip::downsample2d(I2d.upper_left(), I2d.bottom_right(), 
  			res2d_2.upper_left(),M);
 std::cout<<"I2d = \n"<<I2d<<std::endl;
 std::cout<<"downsample_"<<M<<"(I2d) = \n"<<res2d_2<<std::endl;

 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "downsample2d M " << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 M = 3;
 
 new_rows = std::ceil(double(I2d.rows())/double(M));
 new_cols = std::ceil(double(I2d.cols())/double(M));
 std::cout<<"row x cols = "<<new_rows<<" x "<<new_cols<<std::endl;
 slip::Array2d<float> res2d_3(new_rows,new_cols);
 slip::downsample2d(I2d.upper_left(), I2d.bottom_right(), 
			res2d_3.upper_left(),M);
 std::cout<<"I2d = \n"<<I2d<<std::endl;
 std::cout<<"downsample_"<<M<<"(I2d) = \n"<<res2d_3<<std::endl;

 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "upsample2d 2 " << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 slip::Array2d<float> I2d_up2(I2d.rows()*2,I2d.cols()*2);
 slip::upsample2d(I2d.upper_left(),I2d.bottom_right(),
		      I2d_up2.upper_left(),I2d_up2.bottom_right());
 std::cout<<"I2d  = \n"<<I2d<<std::endl;
 std::cout<<"I2d_up = \n"<<I2d_up2<<std::endl;
 M = 2;
 std::cout<<"rows x cols = "<<I2d_up2.rows()<<" x "<<I2d_up2.cols()<<std::endl;
 I2d_up2.fill(0.0f);
 slip::upsample2d(I2d.upper_left(),I2d.bottom_right(),
 		      I2d_up2.upper_left(),I2d_up2.bottom_right(),
 		      M,
 		      false);
 std::cout<<"I2d_up = \n"<<I2d_up2<<std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "upsample2d M " << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 M = 3;
 slip::Array2d<float> I2d_upM(I2d.rows()*M,I2d.cols()*M);
 slip::upsample2d(I2d.upper_left(),I2d.bottom_right(),
		      I2d_upM.upper_left(),I2d_upM.bottom_right(),
		      M,
		      true);
 std::cout<<"I2d_up_"<<M<<" = \n"<<I2d_upM<<std::endl;
 slip::upsample2d(I2d.upper_left(),I2d.bottom_right(),
		      I2d_upM.upper_left(),I2d_upM.bottom_right(),
		      M,
		      false);
 std::cout<<"I2d_up_"<<M<<" = \n"<<I2d_upM<<std::endl;


 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "downsample3d 2 " << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 M = 2;
 slip::Array3d<float> I3d(6,5,4);
 slip::iota(I3d.begin(),I3d.end(),1.0f);
 std::size_t new_slices = std::ceil(double(I3d.slices())/double(M));
 new_rows = std::ceil(double(I3d.rows())/double(M));
 new_cols = std::ceil(double(I3d.cols())/double(M));
 slip::Array3d<float> res3d_2(new_slices,new_rows,new_cols);
 slip::downsample3d(I3d.front_upper_left(), I3d.back_bottom_right(), 
			res3d_2.front_upper_left());
 std::cout<<"I3d = \n"<<I3d<<std::endl;
 std::cout<<"downsample_"<<M<<"(I3d) = \n"<<res3d_2<<std::endl;
 
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "downsample3d M " << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 M = 3;
 new_slices = std::ceil(double(I3d.slices())/double(M));
 new_rows = std::ceil(double(I3d.rows())/double(M));
 new_cols = std::ceil(double(I3d.cols())/double(M));
 slip::Array3d<float> res3d_M(new_slices,new_rows,new_cols);
 slip::downsample3d(I3d.front_upper_left(), I3d.back_bottom_right(), 
			res3d_M.front_upper_left(),M);
 std::cout<<"I3d = \n"<<I3d<<std::endl;
 std::cout<<"downsample_"<<M<<"(I3d) = \n"<<res3d_M<<std::endl;
 res3d_M.fill(0.0f);
 slip::downsample(I3d.front_upper_left(), I3d.back_bottom_right(), 
		      res3d_M.front_upper_left(),res3d_M.back_bottom_right(),M);
 std::cout<<"I3d = \n"<<I3d<<std::endl;
 std::cout<<"downsample_"<<M<<"(I3d) = \n"<<res3d_M<<std::endl;
 
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "upsample3d 2 " << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 slip::Array3d<float> I3d_up2(I3d.slices()*2,I3d.rows()*2,I3d.cols()*2);
 slip::upsample3d(I3d.front_upper_left(),I3d.back_bottom_right(),
		      I3d_up2.front_upper_left(),I3d_up2.back_bottom_right());
 std::cout<<"I3d  = \n"<<I3d<<std::endl;
 std::cout<<"I3d_up2 = \n"<<I3d_up2<<std::endl;
 M = 2;
 I3d_up2.fill(0.0f);
 slip::upsample3d(I3d.front_upper_left(),I3d.back_bottom_right(),
		      I3d_up2.front_upper_left(),I3d_up2.back_bottom_right(),M,
		      false);
 std::cout<<"I3d  = \n"<<I3d<<std::endl;
 std::cout<<"I3d_up2 = \n"<<I3d_up2<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "upsample3d M " << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 M = 3;
 slip::Array3d<float> I3d_upM(I3d.slices()*M,I3d.rows()*M,I3d.cols()*M);
 slip::upsample3d(I3d.front_upper_left(),I3d.back_bottom_right(),
		      I3d_upM.front_upper_left(),I3d_upM.back_bottom_right(),
		      M);
 std::cout<<"I3d  = \n"<<I3d<<std::endl;
 std::cout<<"I3d_upM = \n"<<I3d_upM<<std::endl;
 I3d_up2.fill(0.0f);
 slip::upsample3d(I3d.front_upper_left(),I3d.back_bottom_right(),
		      I3d_upM.front_upper_left(),I3d_upM.back_bottom_right(),M,
		      false);
 std::cout<<"I3d  = \n"<<I3d<<std::endl;
 std::cout<<"I3d_up"<<M<<" = \n"<<I3d_upM<<std::endl;
 return 0;
}
