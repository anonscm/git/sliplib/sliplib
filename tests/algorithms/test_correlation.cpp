#include <iostream>
#include <algorithm>
#include <numeric>
#include <iomanip>
#include "correlation.hpp"
#include "Matrix.hpp"
#include "Matrix3d.hpp"
#include "Array.hpp"
#include <vector>
#include "iterator2d_box.hpp"
#include "Box2d.hpp"
#include "arithmetic_op.hpp"
#include "statistics.hpp"
#include "convolution.hpp"

template<typename T>
bool lt5Predicate (const T& val)
{
  return (val < T(5));
}

int main()
{
  
  
  float f[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
  slip::Matrix<float> M(2,3,f);

  float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
  slip::Matrix<float> M2(2,3,f2);

  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<<"std_crosscorrelation"<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"M  = \n"<<M<<std::endl;
  std::cout<<"M2 = \n"<<M2<<std::endl;
  std::cout<<"std_crosscorrelation = "<<slip::cc<float>(M.begin(),M.end(),M2.begin())<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<<"fft_crosscorrelation"<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<float> Rfftc(11,0.0f);
  std::cout<<"f = "<<std::endl;
  std::copy(f,f+6,std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
   std::cout<<"f2 = "<<std::endl;
  std::copy(f2,f2+6,std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  slip::fft_crosscorrelation(f,f+6,f2,f2+6,Rfftc.begin(),Rfftc.end());
  std::cout<<"fft_crosscorrelation"<<std::endl;
  std::cout<<Rfftc<<std::endl;
   

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<<"fft_crosscorrelation_same"<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"f = "<<std::endl;
  std::copy(f,f+6,std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
   std::cout<<"f2 = "<<std::endl;
  std::copy(f2,f2+6,std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;
  slip::Array<float> Rfftc2(6,0.0f);
  slip::fft_crosscorrelation_same(f,f+6,f2,f2+6,Rfftc2.begin(),Rfftc2.end());
  std::cout<<"fft_crosscorrelation_same"<<std::endl;
  std::cout<<Rfftc2<<std::endl;
   
 

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<<"fft_complex_crosscorrelation"<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl; 
  typedef std::complex<double> TC;
  std::complex<double> dxc[] = {TC(1.0,3.0), TC(2.0,0.0), TC(3.0,-1.0), 
				TC(4.0,1.0)};
  std::complex<double> dyc[] = {TC(-1.0,1.0), TC(2.0,-1.0), TC(1.0,0.0),
				TC(-1.0,-1.0)};
  
  
  slip::Array<TC> xc(4,dxc);
  slip::Array<TC> yc(4,dyc);
  slip::Array<TC> convxyc(xc.size()+yc.size() - 1);

  std::cout<<"xc = "<<xc<<std::endl;
  std::cout<<"yc = "<<yc<<std::endl;
  
  slip::complex_fft_crosscorrelation(xc.begin(),xc.end(),
				     yc.begin(),yc.end(),
				     convxyc.begin(),convxyc.end());
  std::cout<<"fft xcorr(xc,yc) = \n"<<convxyc<<std::endl;
  slip::complex_fft_crosscorrelation(xc.begin(),xc.end(),
				     xc.begin(),xc.end(),
				     convxyc.begin(),convxyc.end());
  std::cout<<"fft xcorr(xc,xc) = \n"<<convxyc<<std::endl;
  slip::complex_fft_crosscorrelation(yc.begin(),yc.end(),
				     xc.begin(),xc.end(),
				     convxyc.begin(),convxyc.end());
  std::cout<<"fft xcorr(yc,xc) = \n"<<convxyc<<std::endl;
    


  float mean1 = slip::mean<float>(M.begin(),M.end());
  float mean2 = slip::mean<float>(M2.begin(),M2.end());
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<<"centered_crosscorrelation with mean = 0"<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<slip::centered_crosscorrelation<float>(M.begin(),M.end(),M2.begin())<<std::endl;
  std::cout<<slip::ccc<float>(M.begin(),M.end(),M2.begin())<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<<"centered_crosscorrelation"<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<slip::centered_crosscorrelation<float>(M.begin(),M.end(),M2.begin(),mean1,mean2)<<std::endl;
  std::cout<<slip::ccc<float>(M.begin(),M.end(),M2.begin(),mean1,mean2)<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<<"normalized_crosscorrelation"<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  std::cout<<slip::normalized_crosscorrelation<float>(M.begin(),M.end(),M2.begin(),mean1,mean2)<<std::endl;
std::cout<<slip::normalized_crosscorrelation<float>(M.begin(),M.end(),M2.begin(),mean1,mean2)<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<<"pseudo_normalized_crosscorrelation"<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

   std::cout<<slip::pseudo_normalized_crosscorrelation<float>(M.begin(),M.end(),M2.begin(),mean1,mean2)<<std::endl;
   std::cout<<slip::pseudo_normalized_crosscorrelation<float>(M.begin(),M.end(),M2.begin(),mean1,mean2)<<std::endl;
   std::cout<<slip::pncc<float>(M.begin(),M.end(),M2.begin(),mean1,mean2)<<std::endl;
   std::cout<<slip::pncc<float>(M.begin(),M.end(),M2.begin(),mean1,mean2)<<std::endl;

   //   int f3[] = {1,0,1,0,0,0};
   int f3[] = {1,1,1,1,1,1};
   slip::Matrix<int> Mask(2,3,f3);
   slip::Matrix<float> M1 = M;
   float mean1_m = slip::mean_mask<float>(M1.begin(),M1.end(),Mask.begin(),1);
   float mean2_m = slip::mean_mask<float>(M2.begin(),M2.end(),Mask.begin(),1);
   
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"std_crosscorrelation_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;;
std::cout<<slip::cc_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"std_crosscorrelation_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
  std::cout<<slip::cc_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl; 

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"centered_crosscorrelation_mask with mean = 0"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;;
 std::cout<<slip::ccc_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin())<<std::endl;


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"centered_crosscorrelation_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;;
std::cout<<slip::ccc_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),mean1_m,mean2_m)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"centered_crosscorrelation_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
float mean_if1 = slip::mean_if<float>(M1.begin(),M1.end(),lt5Predicate<float>);
float mean_if2 = slip::mean_if<float>(M2.begin(),M2.end(),lt5Predicate<float>);

std::cout<<slip::ccc_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>,mean_if1,mean_if2)<<std::endl; 


std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"pseudo_normalized_crosscorrelation_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;
 
std::cout<<slip::pncc_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),mean1_m,mean2_m)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"pseudo_normalized_crosscorrelation_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::pncc_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>,mean1_m,mean2_m)<<std::endl; 

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"normalized_crosscorrelation_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<"Mask="<<std::endl;
std::cout<<Mask<<std::endl;;
std::cout<<slip::ncc_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),mean1_m,mean2_m)<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"normalized_crosscorrelation_if"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"M1="<<std::endl;
std::cout<<M1<<std::endl;
std::cout<<"M2="<<std::endl;
std::cout<<M2<<std::endl;
std::cout<<slip::ncc_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>,mean1_m,mean2_m)<<std::endl; 


  slip::Matrix<float> Motif(8,8,0.0);
  float k1 = 8.0;
  for(std::size_t i = 0; i < Motif.dim1(); ++i)
    {
      for(std::size_t j = 0; j < Motif.dim2(); ++j)
	{
	  k1 += 1.0;
	  Motif[i][j] = k1;
	}
    }

  std::cout<<Motif<<std::endl;
  slip::Matrix<float> I(128,128,0.0);
  float k = 0.0;
  for(std::size_t i = 0; i < I.dim1(); ++i)
    {
      for(std::size_t j = 0; j < I.dim2(); ++j)
	{
	  k += 1.0;
	  I[i][j] = k;
	}
    }

  //std::cout<<I<<std::endl;

  slip::Matrix<float> Result(16,16,0.0);
  //
  for(std::size_t i = 0; i < I.dim1(); i+=8)
    {
      for(std::size_t j = 0; j < I.dim2(); j+=8)
	{
	  slip::Box2d<int> box(i,j,i+7,j+7);
	  Result[i/8][j/8] = slip::std_crosscorrelation<float>(I.upper_left(box),I.bottom_right(box),Motif.begin());
	}
      //std::cout<<std::endl;
    }
  
  //std::cout<<Result<<std::endl;


  

  slip::Array<float> V(5);
  slip::iota(V.begin(),V.end(),1.0f,1.0f);
  std::cout<<"V = \n"<<V<<std::endl;
  slip::Array<float> AutoFull(2*V.size()-1);
  std::cout<<"full autocorelation "<<std::endl;
  slip::autocorrelation_full(V.begin(),V.end(),AutoFull.begin(),AutoFull.end());
  std::cout<<AutoFull<<std::endl;
  slip::Array<float> Auto(V.size());
  std::cout<<"autocorelation "<<std::endl;
  slip::autocorrelation(V.begin(),V.end(),Auto.begin(),Auto.end());
  std::cout<<Auto<<std::endl;
  std::cout<<"biased autocorelation "<<std::endl;
  slip::biased_autocorrelation(V.begin(),V.end(),Auto.begin(),Auto.end());
  std::cout<<Auto<<std::endl;
  std::cout<<"unbiased autocorrelation "<<std::endl;
  slip::unbiased_autocorrelation(V.begin(),V.end(),Auto.begin(),Auto.end());
  std::cout<<Auto<<std::endl;

  std::cout<<"autocovariance "<<std::endl;
  slip::autocovariance(V.begin(),V.end(),Auto.begin(),Auto.end());
  std::cout<<Auto<<std::endl;
  std::cout<<"biased autocovariance "<<std::endl;
  slip::biased_autocovariance(V.begin(),V.end(),Auto.begin(),Auto.end());
  std::cout<<Auto<<std::endl;
  std::cout<<"unbiased autocovariance "<<std::endl;
  slip::unbiased_autocovariance(V.begin(),V.end(),Auto.begin(),Auto.end());
  std::cout<<Auto<<std::endl;

   slip::Array<float> Vtoep(5);
   slip::iota(Vtoep.begin(),Vtoep.end(),1.0);
   std::cout<<"Vtoeplitz = "<<std::endl;
   std::cout<<Vtoep<<std::endl;
   slip::Matrix<float> Atoep(Vtoep.size(),Vtoep.size());
   slip::biased_autocorrelation_matrix(Vtoep.begin(),Vtoep.end(),Atoep);
   std::cout<<"Biased autocorrelation matrix = "<<std::endl;
   std::cout<<Atoep<<std::endl;
   slip::biased_autocovariance_matrix(Vtoep.begin(),Vtoep.end(),Atoep);
   std::cout<<"Biased autocovariance matrix = "<<std::endl;
   std::cout<<Atoep<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<<"fft crosscorrelation2d"<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Matrix<float> Axcorr(8,10);
  slip::iota(Axcorr.begin(),Axcorr.end(),1.0f);
  slip::Matrix<float> Bxcorr(4,4);
  slip::iota(Bxcorr.begin(),Bxcorr.end(),10.0f);
  slip::Matrix<float> Bxcorr_rot(4,4);
  for(std::size_t i = 0; i < Bxcorr.rows(); ++i)
    {
       std::copy(Bxcorr.row_rbegin(i),Bxcorr.row_rend(i),
		 Bxcorr_rot.row_begin(Bxcorr.rows()-1-i));
     }
   
   std::cout<<"Axcorr = \n"<<Axcorr<<std::endl;
   std::cout<<"Bxcorr = \n"<<Bxcorr<<std::endl;
   std::cout<<"Bxcorr_rot = \n"<<Bxcorr_rot<<std::endl;
   slip::Matrix<float> Cxcorr(Axcorr.rows()+Bxcorr.rows()-1,
			      Axcorr.cols()+Bxcorr.cols()-1);
   slip::fft_crosscorrelation2d(Axcorr.upper_left(),
				Axcorr.bottom_right(),
				Bxcorr.upper_left(),
				Bxcorr.bottom_right(),
				Cxcorr.upper_left(),
				Cxcorr.bottom_right());
   std::cout<<"Cxcorr = slip::fft_crosscorrelation2d(Axcorr,Bxcorr,Cxcorr) = \n"<<Cxcorr<<std::endl;
  
   

   
  
   slip::fft_convolution2d(Axcorr,
			   Bxcorr_rot,
			   Cxcorr);
   std::cout<<"Cxcorr slip::fft_convolution2d(Axcorr,Bxcorr_rot,Cxcorr); = \n"<<Cxcorr<<std::endl;

   slip::full_convolution2d(Axcorr.upper_left(),Axcorr.bottom_right(),
			    Bxcorr_rot.upper_left(),Bxcorr_rot.bottom_right(),
			    Cxcorr.upper_left(),Cxcorr.bottom_right());
   std::cout<<"Cxcorr slip::full_convolution2d(Axcorr,Bxcorr_rot,Cxcorr); = \n"<<Cxcorr<<std::endl;

   
   slip::fft_crosscorrelation2d(Bxcorr.upper_left(),
				Bxcorr.bottom_right(),
				Axcorr.upper_left(),
				Axcorr.bottom_right(),
				Cxcorr.upper_left(),
				Cxcorr.bottom_right());
   std::cout<<"Cxcorr = slip::fft_crosscorrelation2d(Bxcorr,Axcorr,Cxcorr) = \n"<<Cxcorr<<std::endl;

  

   slip::Matrix<double> Axcorr2(8,8);
   slip::iota(Axcorr2.begin(),Axcorr2.end(),1.0);
   slip::Matrix<double> Bxcorr2(8,8);
   slip::iota(Bxcorr2.begin(),Bxcorr2.end(),10.0);
   slip::Matrix<double> Cxcorr2(Axcorr2.rows(),Axcorr2.cols());
   slip::fft_circular_crosscorrelation2d(Axcorr2.upper_left(),
					 Axcorr2.bottom_right(),
					 Bxcorr2.upper_left(),
					 Bxcorr2.bottom_right(),
					 Cxcorr2.upper_left(),
					 Cxcorr2.bottom_right());
   std::cout<<"fft_circular_crosscorrelation2d(Axcorr2,Bxcorr2,Cxcorr2) = \n "<<Cxcorr2<<std::endl;

  

   
   slip::Matrix<double> Bxcorr2_rot(8,8);
   for(std::size_t i = 0; i < Bxcorr2.rows(); ++i)
     {
       std::copy(Bxcorr2.row_rbegin(i),Bxcorr2.row_rend(i),
		 Bxcorr2_rot.row_begin(Bxcorr2.rows()-1-i));
     }

   //   std::cout<<"Bxcorr2_rot = \n "<<Bxcorr2_rot<<std::endl;
  
  //   slip::fft_circular_convolution2d(Axcorr2.upper_left(),
// 				    Axcorr2.bottom_right(),
// 				    Bxcorr2_rot.upper_left(),
// 				    Bxcorr2_rot.bottom_right(),
// 				    Cxcorr2.upper_left(),
// 				    Cxcorr2.bottom_right());
//    std::cout<<"fft_circular_convolution2d(Axcorr2,Bxcorr2,Cxcorr2) = \n "<<Cxcorr2<<std::endl;

   slip::same_convolution2d(Axcorr2.upper_left(),Axcorr2.bottom_right(),
			    Bxcorr2_rot.upper_left(),Bxcorr2_rot.bottom_right(),
			    Cxcorr2.upper_left(),Cxcorr2.bottom_right(),
			    slip::BORDER_TREATMENT_CIRCULAR);
   std::cout<<"same_convolution2d(Axcorr2,Bxcorr2_rot,Cxcorr2) = \n "<<Cxcorr2<<std::endl;

   slip::fft_circular_crosscorrelation2d(Bxcorr2.upper_left(),
					 Bxcorr2.bottom_right(),
					 Axcorr2.upper_left(),
					 Axcorr2.bottom_right(),
					 Cxcorr2.upper_left(),
					 Cxcorr2.bottom_right());
   std::cout<<"fft_circular_crosscorrelation2d(Bxcorr2,Axcorr2,Cxcorr2) = \n "<<Cxcorr2<<std::endl;

   slip::Matrix<double> Axcorr2_rot(8,8);
   for(std::size_t i = 0; i < Axcorr2.rows(); ++i)
     {
       std::copy(Axcorr2.row_rbegin(i),Axcorr2.row_rend(i),
		 Axcorr2_rot.row_begin(Axcorr2.rows()-1-i));
     }
    slip::same_convolution2d(Bxcorr2.upper_left(),Bxcorr2.bottom_right(),
			     Axcorr2_rot.upper_left(),Axcorr2_rot.bottom_right(),
			    Cxcorr2.upper_left(),Cxcorr2.bottom_right(),
			    slip::BORDER_TREATMENT_CIRCULAR);
   std::cout<<"same_convolution2d(Bxcorr2,Axcorr2_rot,Cxcorr2) = \n "<<Cxcorr2<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout<<"fft crosscorrelation2d same"<<std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

   slip::Matrix<double> Cxcorr_s(Axcorr.rows(),Axcorr.cols());
   slip::fft_crosscorrelation2d_same(Axcorr.upper_left(),
				     Axcorr.bottom_right(),
				     Bxcorr.upper_left(),
				     Bxcorr.bottom_right(),
				     Cxcorr_s.upper_left(),
				     Cxcorr_s.bottom_right());
   std::cout<<"Cxcorr_s = slip::fft_crosscorrelation2d_same(Axcorr,Bxcorr,Cxcorr_s) = \n"<<Cxcorr_s<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout<<"fft crosscorrelation3d"<<std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

   slip::Matrix3d<double> Axcorr3d(4,8,10);
   slip::iota(Axcorr3d.begin(),Axcorr3d.end(),1.0f);
   slip::Matrix3d<double> Bxcorr3d(2,2,2);
   slip::iota(Bxcorr3d.begin(),Bxcorr3d.end(),10.0f);
   std::cout<<"Axcorr3d = \n"<<Axcorr3d<<std::endl;
   std::cout<<"Bxcorr3d = \n"<<Bxcorr3d<<std::endl;
   slip::Matrix3d<double> Cxcorr3d(Axcorr3d.slices()+Bxcorr3d.slices()-1,
				Axcorr3d.rows()+Bxcorr3d.rows()-1,
				Axcorr3d.cols()+Bxcorr3d.cols()-1);
   slip::fft_crosscorrelation3d(Axcorr3d.front_upper_left(),
				Axcorr3d.back_bottom_right(),
				Bxcorr3d.front_upper_left(),
				Bxcorr3d.back_bottom_right(),
				Cxcorr3d.front_upper_left(),
				Cxcorr3d.back_bottom_right());
   std::cout<<"Cxcorr3d = slip::fft_crosscorrelation3d(Axcorr3d,Bxcorr3d,Cxcorr3d) = \n"<<Cxcorr3d<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout<<"fft crosscorrelation3d same"<<std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

   slip::Matrix3d<double> Cxcorr3d_s(Axcorr3d.slices(),
				     Axcorr3d.rows(),
				     Axcorr3d.cols());
   slip::fft_crosscorrelation3d_same(Axcorr3d.front_upper_left(),
				     Axcorr3d.back_bottom_right(),
				     Bxcorr3d.front_upper_left(),
				     Bxcorr3d.back_bottom_right(),
				     Cxcorr3d_s.front_upper_left(),
				     Cxcorr3d_s.back_bottom_right());
   std::cout<<"Cxcorr3d_s = slip::fft_crosscorrelation3d_same(Axcorr3d,Bxcorr3d,Cxcorr3d_s) = \n"<<Cxcorr3d<<std::endl;
   

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout<<"fft circular crosscorrelation3d"<<std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

   slip::Matrix3d<double> Axcorr3(4,4,4);
   slip::iota(Axcorr3.begin(),Axcorr3.end(),1.0);
   slip::Matrix3d<double> Bxcorr3(4,4,4);
   slip::iota(Bxcorr3.begin(),Bxcorr3.end(),10.0);
   slip::Matrix3d<double> Cxcorr3(Axcorr3.slices(),Axcorr3.rows(),Axcorr3.cols());
   slip::fft_circular_crosscorrelation3d(Axcorr3.front_upper_left(),
					 Axcorr3.back_bottom_right(),
					 Bxcorr3.front_upper_left(),
					 Bxcorr3.back_bottom_right(),
					 Cxcorr3.front_upper_left(),
					 Cxcorr3.back_bottom_right());
   std::cout<<"fft_circular_crosscorrelation3d(Axcorr3,Bxcorr3,Cxcorr3) = \n "<<Cxcorr3<<std::endl;
}
