F = 
(1.165,0.6268,0.0751,0.3516,-0.6965)
kurtosis = 2.16589
unbiased_kurtosis = 3.66354
skewness = -0.293223
unbiased_skewness = -0.437111
____________________________________________________________
                    Descriptive statistics 
____________________________________________________________
M = 
20 19 18 17 16 
15 14 13 12 11 
10 9 8 7 6 
5 4 3 2 1 

cardinal = 20
mean = 10.5
variance = 33.25
unbiased_variance = 35
standard deviation = 5.76628
unbiased standard deviation = 5.91608
kurtosis = 1.79398
unbiased_kurtosis = 1.8
skewness = 0
unbiased_skewness = 0
covariance(M,M) = 33.25
unbiased covariance(M,M) = 35
RMS(M) = 11.9791
median = 11
first_quartile = 6
third_quartile = 15
statistics 
(1,6,11,15,20,10.5,5.76628,0,1.79398,20)
unbiased_statistics 
(1,6,11,15,20,10.5,5.91608,0,1.8,20)
____________________________________________________________
                    Cardinal with slip structures 
____________________________________________________________
cardinal MV = 40
cardinal MV = 40
____________________________________________________________
                    Median statistics 
____________________________________________________________
M2 = 
 2 4.2 -1.2 
4.1 2.2 0 
4.3 5.9 -3.2 

M2 sorted= 
 -3.2 -1.2 0 
2 2.2 4.1 
4.2 4.3 5.9 

median = 2.2
median_n = 2.2
first_quartile = 0
third_quartile = 4.2
M3 = 
 2 4.2 
-1.2 4.1 
2.2 0 
4.3 5.9 

M3 sorted= 
 -1.2 0 
2 2.2 
4.1 4.2 
4.3 5.9 

median = 4.1
median = 4.1
median_n = 4.1
first_quartile = 2
third_quartile = 4.2
M4 = 
 2 
4.2 
-1.2 
4.1 
2.2 
0 
4.3 
5.9 
-3.2 
5.5 
7.7 

M4 sorted= 
 -3.2 
-1.2 
0 
2 
2.2 
4.1 
4.2 
4.3 
5.5 
5.9 
7.7 

median = 4.1
median = 4.1
median_n = 4.1
first_quartile = 0
third_quartile = 5.5
M5 = 
 2 4.2 
-1.2 4.1 
2.2 0 
4.3 5.9 
2.3 1.2 

M5 sorted= 
 -1.2 0 
1.2 2 
2.2 2.3 
4.1 4.2 
4.3 5.9 

median = 2.3
median = 2.3
median_n = 2.3
first_quartile = 1.2
third_quartile = 4.2
11
11
11
11
11
11
11
____________________________________________________________
                    weighted mean 
____________________________________________________________
weight = 
1 2 1 
values = 
5 4 12 
weighted mean = 6.25
____________________________________________________________
                    center 
____________________________________________________________
Mc = 
1 2 3 
4 5 6 
7 8 9 

center(Mc) = 
Mc2 = 
-4 -3 -2 
-1 0 1 
2 3 4 

____________________________________________________________
                    studentize 
____________________________________________________________
Mc = 
1 2 3 
4 5 6 
7 8 9 

studentize(Mc) = 
Mc2 = 
-1.54919 -1.1619 -0.774597 
-0.387298 0 0.387298 
0.774597 1.1619 1.54919 

____________________________________________________________
                    n_max_elements 
____________________________________________________________
12 6 6 9 10 1 5 0 -1 
---------------------
max[0] = 12 
---------------------
---------------------
max[0] = 12 max[1] = 10 
---------------------
max[0] = 12 max[1] = 10 max[2] = 9 
---------------------
---------------------
max[0] = 12 max[1] = 10 max[2] = 9 max[3] = 6 max[4] = 6 max[5] = 5 max[6] = 1 max[7] = 0 max[8] = -1 
30 29 28 27 26 25 
24 23 22 21 20 19 
18 17 16 15 14 13 
12 11 10 9 8 7 
6 5 4 3 2 1 

max element in the box (1,1,2,3) = 23
 i = 1 j = 1
number of elements within the box = 6
maxf[0] = 23 maxf[0] = (1 1); maxf[1] = 22 maxf[1] = (1 2); maxf[2] = 21 maxf[2] = (1 3); 
maxf[0][DPoint2d<int>(1,2)]  = 15
____________________________________________________________
                    n_min_elements 
____________________________________________________________
minf[0] = 15 minf[0] = (2 3); minf[1] = 16 minf[1] = (2 2); minf[2] = 17 minf[2] = (2 1); 
minf[0][DPoint2d<int>(1,2)]  = 7
____________________________________________________________
                    Descriptive statistics mask 
____________________________________________________________
Data : 
20 19 18 17 16 
15 14 13 12 11 
10 9 8 7 6 
5 4 3 2 1 

Mask : 
0 0 0 0 0 
0 1 1 1 1 
0 1 1 1 1 
0 1 1 1 1 

ValuedMask : 
0 0 0 0 0 
0 2 2 2 2 
0 2 2 2 2 
0 2 2 2 2 

cardinal = 12
cardinal value = 2 12
mean = 7.5
mean value = 2 = 7.5
variance = 17.9167
unbiased variance = 19.5455
variance value = 2 = 17.9167
unbiased variance value = 2 = 19.5455
standard deviation = 4.23281
unbiased standard deviation = 4.42102
standard deviation value = 2 = 4.23281
unbiased standard deviation value = 2 = 4.42102
kurtosis = 1.69538
unbiased kurtosis = 1.66044
kurtosis value = 2 = 1.69538
unbiased kurtosis value = 2 = 1.66044
skewness = 0
unbiased skewness = 0
skewness value = 2 = 0
unbiased skewness value = 2 = 0
median = 8
median value = 2 = 8
first quartile = 4
first quartile value = 2 = 4
third quartile = 11
third quartile value = 2 = 11
covariance(M,M) = 17.9167
unbiased_covariance(M,M) = 19.5455
RMS(M) = 8.61201
covariance(M,M) value = 2 = 17.9167
unbiased_covariance(M,M) value = 2 = 19.5455
RMS(M) value = 2 = 8.61201
statistics mask
(1,4,8,11,14,7.5,4.23281,0,1.69538,12)
unbiased_statistics mask
(1,4,8,11,14,7.5,4.42102,0,1.66044,12)
statistics mask value = 2
(1,4,8,11,14,7.5,4.23281,0,1.69538,12)
unbiased_statistics mask value = 2
(1,4,8,11,14,7.5,4.42102,0,1.66044,12)
____________________________________________________________
                    center mask 
____________________________________________________________
Mcmask = 
1 2 3 4 5 
6 7 8 9 10 
11 12 13 14 15 
16 17 18 19 20 

Mcmask2 = 
0 0 0 0 0 
0 -6.5 -5.5 -4.5 -3.5 
0 -1.5 -0.5 0.5 1.5 
0 3.5 4.5 5.5 6.5 

____________________________________________________________
                    studentize mask 
____________________________________________________________
Mcmask2 = 
0 0 0 0 0 
0 -1.53562 -1.29937 -1.06312 -0.826874 
0 -0.354375 -0.118125 0.118125 0.354375 
0 0.826874 1.06312 1.29937 1.53562 

____________________________________________________________
                    Descriptive statistics using a predicate less than 10
____________________________________________________________
Data : 
20 19 18 17 16 
15 14 13 12 11 
10 9 8 7 6 
5 4 3 2 1 

cardinal = 20
mean = 5
variance = 6.66667
unbiased variance = 7.5
standard deviation = 2.58199
unbiased standard deviation = 2.73861
kurtosis = 1.77
unbiased kurtosis = 1.8
skewness = 0
unbiased skewness = 0
median = 5
first quartile = 3
third quartile = 7
covariance(M,M) = 6.66667
unbiased covariance(M,M) = 7.5
RMS(M) = 5.62731
statistics using a predicate
(1,3,5,7,9,5,2.58199,0,1.77,9)
unbiased_statistics using a predicate
(1,3,5,7,9,5,2.73861,0,1.8,9)
____________________________________________________________
                    center using a predicate 
____________________________________________________________
M = 
20 19 18 17 16 
15 14 13 12 11 
10 9 8 7 6 
5 4 3 2 1 

Mcif2 = 
0 0 0 0 0 
0 0 0 0 0 
0 4 3 2 1 
0 -1 -2 -3 -4 

____________________________________________________________
                    studentize using a predicate 
____________________________________________________________
M = 
20 19 18 17 16 
15 14 13 12 11 
10 9 8 7 6 
5 4 3 2 1 

Mcif2 = 
0 0 0 0 0 
0 0 0 0 0 
0 1.54919 1.1619 0.774597 0.387298 
0 -0.387298 -0.774597 -1.1619 -1.54919 

____________________________________________________________
                    recursive mean...  
____________________________________________________________
x = 
(1,2,3,4,5,6,7,8)
mu = 1
mu = 1.5
mu = 2
mu = 2.5
mu = 3
mu = 3.5
mu = 4
mu = 4.5
slip::mean = 4.5
____________________________________________________________
                    recursive variance...  
____________________________________________________________
x = 
(1,2,3,4,5,6,7,8)
mu = 1 var = 0
mu = 1.5 var = 0.25
mu = 2 var = 0.666667
mu = 2.5 var = 1.25
mu = 3 var = 2
mu = 3.5 var = 2.91667
mu = 4 var = 4
mu = 4.5 var = 5.25
slip::mean = 4.5
slip::variance = 5.25
