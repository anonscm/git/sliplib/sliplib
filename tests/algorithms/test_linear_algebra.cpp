#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <complex>
#include "linear_algebra.hpp"
//#include "linear_algebra_eigen.hpp"
//#include "linear_algebra_svd.hpp"
#include "Matrix.hpp"
#include "Matrix3d.hpp"
#include "Vector.hpp"
#include "Array2d.hpp"
#include "Block2d.hpp"
#include "arithmetic_op.hpp"
#include "noise.hpp"
int main()
{

   ////////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Matrix transpose " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Matrix<float> M(3,4);
   slip::iota(M.begin(),M.end(),0.0);
   std::cout<<"M = "<<std::endl;
   std::cout<<M<<std::endl;
   slip::Array2d<float> MT(4,3);
   slip::transpose(M,MT);
   std::cout<<"M^T = "<<std::endl;
   std::cout<<MT<<std::endl;
   typedef std::complex<double> TC;
   TC dc[] = {TC(1,1),TC(0,1),TC(0,1),TC(0,1),
	     TC(1,0),TC(1,1),TC(0,1),TC(0,1),
	     TC(1,2),TC(1,2),TC(1,2),TC(1,2)};
   slip::Matrix<TC> Mcc(3,4,dc);
   std::cout<<"Mcc = "<<std::endl;
   std::cout<<Mcc<<std::endl;
   slip::Array2d<TC> MccT(4,3);
   slip::hermitian_transpose(Mcc,MccT);
   std::cout<<"Mcc^T = "<<std::endl;
   std::cout<<MccT<<std::endl;
   slip::transpose(Mcc,MccT);
   std::cout<<"Mcc^T = "<<std::endl;
   std::cout<<MccT<<std::endl;
   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Matrix-Matrix multiplies " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"M = "<<std::endl;
   std::cout<<M<<std::endl;
   slip::Matrix<float> M2(4,2);
   slip::iota(M2.begin(),M2.end(),0.0);
   std::cout<<"M2 = "<<std::endl;
   std::cout<<M2<<std::endl;
   slip::Matrix<float> MxM2(3,2);
 //   slip::multiplies(M,M.rows(),M.cols(),
// 		    M2,M2.rows(),M2.cols(),
// 		    MxM2,MxM2.rows(),MxM2.cols());
//    std::cout<<"M x M2 = "<<std::endl;
//    std::cout<<MxM2<<std::endl;
  //  MxM2.fill(0.0);
//    slip::multiplies(M,M2,MxM2);
//    std::cout<<"M x M2 = "<<std::endl;
//    std::cout<<MxM2<<std::endl;
   MxM2.fill(0.0);
   slip::matrix_matrix_multiplies(M,M2,MxM2);
   std::cout<<"M x M2 = "<<std::endl;
   std::cout<<MxM2<<std::endl;

   MxM2.fill(0.0f);
   slip::matrix_matrix_multiplies(M.upper_left(),M.bottom_right(),
				  M2.upper_left(),M2.bottom_right(),
				  MxM2.upper_left(),MxM2.bottom_right());
   std::cout<<"M x M2 = "<<std::endl;
   std::cout<<MxM2<<std::endl;
   TC dc2[] = {TC(1,2),TC(-1,1),
	       TC(1,0),TC(1,1),
               TC(1,-1),TC(2,1),
               TC(1,0),TC(1,1)};
   slip::Matrix<TC> Mcc2(4,2,dc2);
   slip::Matrix<TC> MccxMcc2(3,2,TC());
   std::cout<<"Mcc = "<<std::endl;
   std::cout<<Mcc<<std::endl;
   std::cout<<"Mcc2 = "<<std::endl;
   std::cout<<Mcc2<<std::endl;
   slip::matrix_matrix_multiplies(Mcc.upper_left(),Mcc.bottom_right(),
				  Mcc2.upper_left(),Mcc2.bottom_right(),
				  MccxMcc2.upper_left(),MccxMcc2.bottom_right());
   std::cout<<"Mcc x Mcc2 = "<<std::endl;
   std::cout<<MccxMcc2<<std::endl;
   slip::matrix_matrix_multiplies(Mcc,Mcc2,MccxMcc2);
   std::cout<<"Mcc x Mcc2 = "<<std::endl;
   std::cout<<MccxMcc2<<std::endl;
   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Matrix-Vector multiplies " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"M = "<<std::endl;
   std::cout<<M<<std::endl;
   slip::Array<float> V1(4);
   slip::iota(V1.begin(),V1.end(),1.0);
   std::cout<<"V = "<<std::endl;
   std::cout<<V1<<std::endl;
   slip::Array<float> MxV(3);
   slip::matrix_vector_multiplies(M,V1,MxV);
   std::cout<<"MxV = "<<std::endl;
   std::cout<<MxV<<std::endl;
 //   MxV.fill(0.0);
//    slip::multiplies(M,M.rows(),M.cols(),V1,V1.size(),MxV,MxV.size());
//    std::cout<<"MxV = "<<std::endl;
//    std::cout<<MxV<<std::endl;
   MxV.fill(0.0);
   slip::matrix_vector_multiplies(M.upper_left(),M.bottom_right(),
				   V1.begin(),V1.end(),
				   MxV.begin(),MxV.end());
   std::cout<<"MxV = "<<std::endl;
   std::cout<<MxV<<std::endl;


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Matrix-Vector multiplies complex " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Mcc = "<<std::endl;
   std::cout<<Mcc<<std::endl;
   slip::Array<std::complex<double> > V1c(4);
   slip::iota(V1c.begin(),V1c.end(),std::complex<double>(1.0,1.0));
   std::cout<<"V1c = "<<std::endl;
   std::cout<<V1c<<std::endl;
   slip::Array<std::complex<double> > MxVc(3);
   slip::matrix_vector_multiplies(Mcc,V1c,MxVc);
   std::cout<<"MxVc = "<<std::endl;
   std::cout<<MxVc<<std::endl;
  //  MxVc.fill(std::complex<double>());
//    slip::multiplies(Mcc,Mcc.rows(),Mcc.cols(),V1c,V1c.size(),MxVc,MxVc.size());
//    std::cout<<"MxVc = "<<std::endl;
//    std::cout<<MxVc<<std::endl;
   MxVc.fill(std::complex<double>());
   slip::matrix_vector_multiplies(Mcc.upper_left(),Mcc.bottom_right(),
				   V1c.begin(),V1c.end(),
				   MxVc.begin(),MxVc.end());
   std::cout<<"MxVc = "<<std::endl;
   std::cout<<MxVc<<std::endl;

  

   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Vector-scalar multiplies " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"V = "<<std::endl;
   std::cout<<V1<<std::endl;
   float scal = 3.3;
   slip::Array<float> Vscal1(4);
   slip::vector_scalar_multiplies(V1.begin(),V1.end(),scal,Vscal1.begin());
   std::cout<<"Vscal1 = V * "<<scal<<std::endl;
   std::cout<<Vscal1<<std::endl;
    slip::vector_scalar_multiplies(V1,scal,Vscal1);
   std::cout<<"Vscal1 = V * "<<scal<<std::endl;
   std::cout<<Vscal1<<std::endl;
   std::complex<float> scalc(2.2,2.2);
   slip::Array<std::complex<float> > Vscalc1(4);
   slip::vector_scalar_multiplies(V1.begin(),V1.end(),scalc,Vscalc1.begin());
   std::cout<<"Vscalc1 = V * "<<scalc<<std::endl;
   std::cout<<Vscalc1<<std::endl;
   slip::Array<std::complex<float> > Vscalc2(4);
   slip::iota(Vscalc2.begin(),Vscalc2.end(),std::complex<float>(1.0,1.0),std::complex<float>(1.0,1.0));
   std::cout<<"Vscalc2 = "<<std::endl;
   std::cout<<Vscalc2<<std::endl;
   
   slip::vector_scalar_multiplies(Vscalc2.begin(),Vscalc2.end(),
				  scalc,Vscalc1.begin());
   std::cout<<"Vscalc1 = Vscalc2 * "<<scalc<<std::endl;
   std::cout<<Vscalc1<<std::endl;
   slip::vector_scalar_multiplies(Vscalc2,scalc,Vscalc1);
   std::cout<<"Vscalc1 = Vscalc2 * "<<scalc<<std::endl;
   std::cout<<Vscalc1<<std::endl;

   std::cout<<"Vscalc2 = "<<std::endl;
   std::cout<<Vscalc2<<std::endl;
   
   slip::vector_scalar_multiplies(Vscalc2.begin(),Vscalc2.end(),
				  scal,Vscalc1.begin());
   std::cout<<"Vscalc1 = Vscalc2 * "<<scal<<std::endl;
   std::cout<<Vscalc1<<std::endl;

   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "conj Vector-scalar multiplies " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"V = "<<std::endl;
   std::cout<<V1<<std::endl;
   slip::conj_vector_scalar_multiplies(V1.begin(),V1.end(),scal,Vscal1.begin());
   std::cout<<"Vscal1 = V^* * "<<scal<<std::endl;
   std::cout<<Vscal1<<std::endl;
   slip::conj_vector_scalar_multiplies(V1,scal,Vscal1);
   std::cout<<"Vscal1 = V^* * "<<scal<<std::endl;
   std::cout<<Vscal1<<std::endl;
   slip::conj_vector_scalar_multiplies(V1.begin(),V1.end(),scalc,Vscalc1.begin());
   std::cout<<"Vscalc1 = V^* * "<<scalc<<std::endl;
   std::cout<<Vscalc1<<std::endl;

   std::cout<<"Vscalc2 = "<<std::endl;
   std::cout<<Vscalc2<<std::endl;
   
   slip::conj_vector_scalar_multiplies(Vscalc2.begin(),Vscalc2.end(),
				  scalc,Vscalc1.begin());
   std::cout<<"Vscalc1 = Vscalc2^* * "<<scalc<<std::endl;
   std::cout<<Vscalc1<<std::endl;
   slip::conj_vector_scalar_multiplies(Vscalc2,scalc,Vscalc1);
   std::cout<<"Vscalc1 = Vscalc2^* * "<<scalc<<std::endl;
   std::cout<<Vscalc1<<std::endl;



   /////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Matrix-scalar multiplies " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"M = "<<std::endl;
   std::cout<<M<<std::endl;
   slip::Matrix<float> Mscal1(3,4);
   slip::matrix_scalar_multiplies(M.upper_left(),M.bottom_right(),
				  scal,
				  Mscal1.upper_left(), Mscal1.bottom_right());
   std::cout<<"Mscal1 = M * "<<scal<<std::endl;
   std::cout<<Mscal1<<std::endl;
   slip::matrix_scalar_multiplies(M,scal,Mscal1);
   std::cout<<"Mscal1 = M * "<<scal<<std::endl;
   std::cout<<Mscal1<<std::endl;

   std::cout<<"Mcc = "<<std::endl;
   std::cout<<Mcc<<std::endl;
   slip::Matrix<std::complex<double> > Mscal1c(3,4);
   std::complex<double> scalcd(2.2,2.2);
   slip::matrix_scalar_multiplies(Mcc.upper_left(),Mcc.bottom_right(),
				  scalcd,
				  Mscal1c.upper_left(), Mscal1c.bottom_right());
   std::cout<<"Mscal1c = Mcc * "<<scalcd<<std::endl;
   std::cout<<Mscal1c<<std::endl;
   slip::matrix_scalar_multiplies(Mcc,scalcd,Mscal1c);
   std::cout<<"Mscal1c = Mcc * "<<scalcd<<std::endl;
   std::cout<<Mscal1c<<std::endl;

   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "hermitian vector-Matrix multiplies " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Mcc = "<<std::endl;
   std::cout<<Mcc<<std::endl;
   
   slip::Vector<std::complex<double> > Vcc(3);
   slip::iota(Vcc.begin(),Vcc.end(),std::complex<double>(1.0,1.0));
   std::cout<<"Vcc = "<<std::endl;
   std::cout<<Vcc<<std::endl;
   slip::Vector<std::complex<double> > Vrcc(4);
   slip::hvector_matrix_multiplies(Vcc.begin(),Vcc.end(),
				   Mcc.upper_left(),Mcc.bottom_right(),
				   Vrcc.begin(),Vrcc.end());
   std::cout<<"Vrcc = "<<std::endl;
   std::cout<<Vrcc<<std::endl;
   slip::hvector_matrix_multiplies(Vcc,Mcc,Vrcc);
   std::cout<<"Vrcc = "<<std::endl;
   std::cout<<Vrcc<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "transpose vector-Matrix multiplies " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"M = "<<std::endl;
   std::cout<<M<<std::endl;
   
   slip::Vector<float> Vtt(3);
   slip::iota(Vtt.begin(),Vtt.end(),1.0);
   std::cout<<"Vtt = "<<std::endl;
   std::cout<<Vtt<<std::endl;
   slip::Vector<float> Vttr(4);
   slip::tvector_matrix_multiplies(Vtt.begin(),Vtt.end(),
				   M.upper_left(),M.bottom_right(),
				   Vttr.begin(),Vttr.end());
   std::cout<<"Vttr = "<<std::endl;
   std::cout<<Vttr<<std::endl;
   slip::tvector_matrix_multiplies(Vtt,M,Vttr);
   std::cout<<"Vttr = "<<std::endl;
   std::cout<<Vttr<<std::endl;


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "hermitian vector-Matrix multiplies with real data" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"M = "<<std::endl;
   std::cout<<M<<std::endl;
   std::cout<<"Vtt = "<<std::endl;
   std::cout<<Vtt<<std::endl;
   slip::hvector_matrix_multiplies(Vtt.begin(),Vtt.end(),
				   M.upper_left(),M.bottom_right(),
				   Vttr.begin(),Vttr.end());
   std::cout<<"Vttr = "<<std::endl;
   std::cout<<Vttr<<std::endl;
   slip::hvector_matrix_multiplies(Vcc.begin(),Vcc.end(),
				   M.upper_left(),M.bottom_right(),
				   Vrcc.begin(),Vrcc.end());
   std::cout<<"Vrcc = "<<std::endl;
   std::cout<<Vrcc<<std::endl;

  
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "hermitian matrix-Matrix multiplies with real data" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   
   slip::Matrix<double> M1r(2,3);
   slip::iota(M1r.begin(),M1r.end(),1.0);
   slip::Matrix<double> M2r(2,4);
   slip::iota(M2r.begin(),M2r.end(),1.0);
   slip::Matrix<double> M3r(3,4);

  slip::hmatrix_matrix_multiplies(M1r.upper_left(),M1r.bottom_right(),
				  M2r.upper_left(),M2r.bottom_right(),
				  M3r.upper_left(),M3r.bottom_right());
  std::cout<<"M1r = \n"<<M1r<<std::endl;
  std::cout<<"M2r = \n"<<M2r<<std::endl;
  std::cout<<"M3r = \n"<<M3r<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "hermitian matrix-Matrix multiplies with complex data" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  //typedef std::complex<double> TC;
  slip::Matrix<std::complex<double> > M1c(2,3);
  slip::iota(M1c.begin(),M1c.end(),
	     std::complex<double>(1.0,1.0),
	     std::complex<double>(1.0,1.0));
  slip::Matrix<std::complex<double> > M2c(2,4);
  slip::iota(M2c.begin(),M2c.end(),
	     std::complex<double>(1.0,1.0),
	     std::complex<double>(1.0,1.0));
  slip::Matrix<std::complex<double> > M3c(3,4);

  slip::hmatrix_matrix_multiplies(M1c.upper_left(),M1c.bottom_right(),
				  M2c.upper_left(),M2c.bottom_right(),
				  M3c.upper_left(),M3c.bottom_right());
  std::cout<<"M1c = \n"<<M1c<<std::endl;
  std::cout<<"M2c = \n"<<M2c<<std::endl;
  std::cout<<"M3c = \n"<<M3c<<std::endl;
  slip::hmatrix_matrix_multiplies(M1c,M2c,M3c);
  std::cout<<"M1c = \n"<<M1c<<std::endl;
  std::cout<<"M2c = \n"<<M2c<<std::endl;
  std::cout<<"M3c = \n"<<M3c<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "hermitian matrix-vector multiplies with real data" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Vector<double> V1r(M1r.rows());
  slip::iota(V1r.begin(),V1r.end(),1.0);
  slip::Vector<double> V2r(M1r.cols());
  slip::hmatrix_vector_multiplies(M1r,V1r,V2r);
  std::cout<<"M1r = \n"<<M1r<<std::endl;
  std::cout<<"V1r = \n"<<V1r<<std::endl;
  std::cout<<"V2r = M^TV1r\n"<<V2r<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "hermitian matrix-vector multiplies with complex data" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Vector<std::complex<double> > V1cc(M1c.rows());
  slip::iota(V1cc.begin(),V1cc.end(),
	     std::complex<double>(1.0,1.0),
	     std::complex<double>(1.0,1.0));
  slip::Vector<std::complex<double> > V2cc(M1c.cols());
  slip::hmatrix_vector_multiplies(M1c,V1cc,V2cc);
  std::cout<<"M1c = \n"<<M1c<<std::endl;
  std::cout<<"V1cc = \n"<<V1cc<<std::endl;
  std::cout<<"V2cc = M1c^HV1c\n"<<V2cc<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "matrix_hmatrix_multiplies " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    typedef std::complex<double> TC;
  slip::Matrix<TC> M1c2(2,3);
  slip::iota(M1c2.begin(),M1c2.end(),TC(1.0,1.0),TC(1.0,1.0));
  slip::Matrix<TC> M2c2(4,3);
  slip::iota(M2c2.begin(),M2c2.end(),TC(1.0,1.0),TC(1.0,1.0));
  slip::Matrix<TC> M3c2(2,4);
  slip::matrix_hmatrix_multiplies(M1c2,M2c2,M3c2);
  std::cout<<"M1c = \n"<<M1c2<<std::endl;
  std::cout<<"M2c = \n"<<M2c2<<std::endl;
  std::cout<<"M3c = \n"<<M3c2<<std::endl;
  
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Matrix Matrix transpose " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   const std::size_t rows = 5;
   const std::size_t cols = 6;
   slip::Matrix<double> A(rows,cols);
   slip::iota(A.begin(),A.end(),1.0);
   std::cout<<"A = \n"<<A<<std::endl;
   slip::Matrix<double> AAT(rows,rows);
   slip::matrix_matrixt_multiplies(A,AAT);
   std::cout<<"AAT = \n"<<AAT<<std::endl;
   std::cout<<std::endl;
   AAT = 0.0;
   slip::matrix_matrixt_multiplies(A.upper_left(),
				   A.bottom_right(),
				   AAT.upper_left(),
				   AAT.bottom_right());
   std::cout<<"AAT = \n"<<AAT<<std::endl;
   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Y = aX + Y " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"M = "<<std::endl;
   std::cout<<M<<std::endl;
   slip::Matrix<float> M3(3,4);
   slip::iota(M3.begin(),M3.end(),2.0);
   std::cout<<"M3 = "<<std::endl;
   std::cout<<M3<<std::endl;
   float a = 2.0;
   slip::aXpY(a,M3.begin(),M3.end(),M.begin());
   std::cout<<"M = M + 2 x M3"<<std::endl;
   std::cout<<M<<std::endl;
 
   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Y = MV + Y " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"M = "<<std::endl;
   std::cout<<M<<std::endl;
   std::cout<<"V = "<<std::endl;
   std::cout<<V1<<std::endl;
   slip::Array<float> Y(3);
   slip::iota(Y.begin(),Y.end(),2.0);
   std::cout<<"Y = "<<std::endl;
   std::cout<<Y<<std::endl;
   slip::gen_matrix_vector_multiplies(M,V1,Y);
   std::cout << "Y = MV + Y " <<std::endl;
   std::cout<<Y<<std::endl;
   slip::iota(Y.begin(),Y.end(),2.0);
   slip::gen_matrix_vector_multiplies(M.upper_left(),M.bottom_right(),
				      V1.begin(),V1.end(),
				      Y.begin(),Y.end());
   std::cout << "Y = MV + Y " <<std::endl;
   std::cout<<Y<<std::endl;

   std::cout<<"Mcc = "<<std::endl;
   std::cout<<Mcc<<std::endl;
   std::cout<<"V1c = "<<std::endl;
   std::cout<<V1c<<std::endl;
   slip::Array<std::complex<double> > Yc(3);
   slip::iota(Yc.begin(),Yc.end(),std::complex<double>(2.0,1.0));
   std::cout<<"Yc = "<<std::endl;
   std::cout<<Yc<<std::endl;
   slip::gen_matrix_vector_multiplies(Mcc,V1c,Yc);
   std::cout << "Yc = MccVc + Yc " <<std::endl;
   std::cout<<Yc<<std::endl;
   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "C = AB + C " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"A = "<<std::endl;
   std::cout<<M<<std::endl;
   std::cout<<"B = "<<std::endl;
   std::cout<<M2<<std::endl;
   slip::Matrix<float> C(3,2);
   slip::iota(C.begin(),C.end(),2.0);
   std::cout<<"C = "<<std::endl;
   std::cout<<C<<std::endl;
   std::cout << "C = AB + C " << std::endl;
   slip::gen_matrix_matrix_multiplies(M,M2,C);
   std::cout<<C<<std::endl;
   slip::iota(C.begin(),C.end(),2.0);
   std::cout<<"C = "<<std::endl;
   std::cout<<C<<std::endl;
   std::cout << "C = AB + C " << std::endl;
   slip::gen_matrix_matrix_multiplies(M.upper_left(),M.bottom_right(),
				      M2.upper_left(),M2.bottom_right(),
				      C.upper_left(),C.bottom_right());
   std::cout<<C<<std::endl;

   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "outer product " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Vector<std::complex<double> > Vout1(4);
   slip::iota(Vout1.begin(),Vout1.end(),
	      std::complex<double>(1.0,1.0),
	      std::complex<double>(1.0,1.0));
   std::cout<<"Vout1 = "<<std::endl;
   std::cout<<Vout1<<std::endl;
   slip::Vector<std::complex<double> > Vout2(3);
   slip::iota(Vout2.begin(),Vout2.end(),
	      std::complex<double>(2.0,1.0),
	      std::complex<double>(1.0,1.0));
   std::cout<<"Vout2 = "<<std::endl;
   std::cout<<Vout2<<std::endl;
   slip::Matrix<std::complex<double> > Aout(Vout1.size(),Vout2.size());
   slip::outer_product(Vout1.begin(),Vout1.end(),
		       Vout2.begin(),Vout2.end(),
		       Aout.upper_left(),Aout.bottom_right());
   std::cout<<"Aout = "<<std::endl;
   std::cout<<Aout<<std::endl;
   

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "rank1_rank1_tensorial_product " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Vector<double> Vbase1(4);
   slip::iota(Vbase1.begin(),Vbase1.end(),1.0);
   std::cout<<"Vbase1 = \n"<<Vbase1<<std::endl;
   slip::Vector<double> Vbase2(5);
   slip::iota(Vbase2.begin(),Vbase2.end(),2.0);
   std::cout<<"Vbase2 = \n"<<Vbase2<<std::endl;

   slip::Matrix<double> Tpbase1base2(Vbase1.size(),Vbase2.size());
   slip::rank1_tensorial_product(Vbase1.begin(),Vbase1.end(),
				 Vbase2.begin(),Vbase2.end(),
				 Tpbase1base2.upper_left(),Tpbase1base2.bottom_right());
   std::cout<<"Tpbase1base2 = \n"<<Tpbase1base2<<std::endl;
   

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "rank1_rank1_rank1_tensorial_product " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Vector<double> Vbase12(4);
   slip::iota(Vbase12.begin(),Vbase12.end(),1.0);
   std::cout<<"Vbase12 = \n"<<Vbase12<<std::endl;
   slip::Vector<double> Vbase22(5);
   slip::iota(Vbase22.begin(),Vbase22.end(),2.0);
   std::cout<<"Vbase22 = \n"<<Vbase22<<std::endl;
   slip::Vector<double> Vbase32(3);
   slip::iota(Vbase32.begin(),Vbase32.end(),3.0);
   std::cout<<"Vbase32 = \n"<<Vbase32<<std::endl;

   slip::Matrix3d<double> Tpbase1base2base3(Vbase12.size(),Vbase22.size(),Vbase32.size());
   slip::rank1_tensorial_product(Vbase12.begin(),Vbase12.end(),
				 Vbase22.begin(),Vbase22.end(),
				 Vbase32.begin(),Vbase32.end(),
				 
				 Tpbase1base2base3.front_upper_left(),Tpbase1base2base3.back_bottom_right());
   std::cout<<"Tpbase1base2base3 = \n"<<Tpbase1base2base3<<std::endl;
   
   
   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Rank-one update " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Matrix<TC> Mrank1(4,3,dc);
   std::cout<<"Mrank1 = "<<std::endl;
   std::cout<<Mrank1<<std::endl;
   std::cout<<"Vout1 = "<<std::endl;
   std::cout<<Vout1<<std::endl;
   std::cout<<"Vout2 = "<<std::endl;
   std::cout<<Vout2<<std::endl;
   slip::rank1_update(Mrank1.upper_left(),Mrank1.bottom_right(),
		      3.0,
		      Vout1.begin(),Vout1.end(),
		      Vout2.begin(),Vout2.end());
   std::cout<<"Mrank1 = Mrank1 + 3xXxY^*"<<std::endl;
   std::cout<<Mrank1<<std::endl;
   std::copy(dc,dc+12,Mrank1.begin());
   slip::rank1_update(Mrank1,3.0,Vout1,Vout2);
   std::cout<<"Mrank1 = Mrank1 + 3xXxY^*"<<std::endl;
   std::cout<<Mrank1<<std::endl;
  

   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Generalized inner-product " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Array2d<float> Ai(3,3);
   slip::iota(Ai.begin(),Ai.end(),1.0f);
   std::cout<<"Ai = "<<std::endl;
   std::cout<<Ai<<std::endl;
   slip::Array<float> Xi(Ai.cols());
   slip::iota(Xi.begin(),Xi.end(),2.0f);
   std::cout<<"Xi = "<<std::endl;
   std::cout<<Xi<<std::endl;
   slip::Array<float> Yi(Ai.rows());
   slip::iota(Yi.begin(),Yi.end(),3.0f);
   std::cout<<"Yi = "<<std::endl;
   std::cout<<Yi<<std::endl;
   std::cout<<"Xi^T Ai Yi = "<<std::endl;
   std::cout<<slip::gen_inner_product(Xi.begin(),Xi.end(),
				      Ai.upper_left(),
				      Ai.bottom_right(),
				      Yi.begin(),Yi.end())<<std::endl;
   std::cout<<"Xi^T Ai Yi = "<<std::endl;
   std::cout<<slip::gen_inner_product(Xi,Ai,Yi)<<std::endl;
  
  

   TC dci[] = {TC(1,1),TC(0,1),TC(0,1),
	       TC(1,0),TC(1,1),TC(0,1),
	       TC(1,2),TC(1,2),TC(1,2)};
   slip::Array2d<TC> Mic(3,3,dci);
   std::cout<<"Mic = "<<std::endl;
   std::cout<<Mic<<std::endl;
   TC xic[] = {TC(1,1),TC(2,1),TC(2,3)};
   slip::Array<TC> Xic(Mic.cols(),xic); 
   std::cout<<"Xic = "<<std::endl;
   std::cout<<Xic<<std::endl;
   TC yic[] = {TC(2,1),TC(1,3),TC(2,4)};
   slip::Array<TC> Yic(Mic.cols(),yic);
  
   
   std::cout<<"Yic = "<<std::endl;
   std::cout<<Yic<<std::endl;
   std::cout<<"Xic^H Mic Yic = "<<std::endl;
   std::cout<<slip::gen_inner_product(Xic.begin(),Xic.end(),
				      Mic.upper_left(),
				      Mic.bottom_right(),
				      Yic.begin(),Yic.end())<<std::endl;
 std::cout<<"Xic^H Mic Yic = "<<std::endl;
 std::cout<<slip::gen_inner_product(Xic,Mic,Yic)<<std::endl;



     ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "inner_product " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Xi = "<<std::endl;
   std::cout<<Xi<<std::endl;
   std::cout<<"Yi = "<<std::endl;
   std::cout<<Yi<<std::endl;
   std::cout<<"Xi^T Yi = "<<std::endl;
   std::cout<<slip::inner_product(Xi.begin(),Xi.end(),
				  Yi.begin(),0.0)<<std::endl;
   std::cout<<"Xi^T Yi = "<<std::endl;
   std::cout<<slip::inner_product(Xi.begin(),Xi.end(),
				  Yi.begin())<<std::endl;
   std::cout<<"Xi^T Yi = "<<std::endl;
   std::cout<<slip::inner_product(Xi,Yi)<<std::endl;
  

   std::cout<<"Xic = "<<std::endl;
   std::cout<<Xic<<std::endl;
   std::cout<<"Yic = "<<std::endl;
   std::cout<<Yic<<std::endl;
   std::cout<<"Xic^H Yic = "<<std::endl;
   std::cout<<slip::hermitian_inner_product(Xic.begin(),Xic.end(),
					    Yic.begin(),TC(0,0))<<std::endl;
   std::cout<<"Xic^H Yic = "<<std::endl;
   std::cout<<slip::inner_product(Xic.begin(),Xic.end(),
				  Yic.begin(),TC(0,0))<<std::endl;
   std::cout<<"Xic^H Yic = "<<std::endl;
   std::cout<<slip::inner_product(Xic.begin(),Xic.end(),
				  Yic.begin())<<std::endl;
 
   std::cout<<"Xic^H Yic = "<<std::endl;
   std::cout<<slip::inner_product(Xic,Yic)<<std::endl;

   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Rayleigh coefficient " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Ai = "<<std::endl;
   std::cout<<Ai<<std::endl;
   std::cout<<"Xi = "<<std::endl;
   std::cout<<Xi<<std::endl;
   std::cout<<"rayleigh(Ai,Xi) = "<<std::endl;
   std::cout<<slip::rayleigh(Xi.begin(),Xi.end(),
			     Ai.upper_left(),Ai.bottom_right())<<std::endl;
   std::cout<<"rayleigh(Ai,Xi) = "<<std::endl;
   std::cout<<slip::rayleigh(Xi,Ai)<<std::endl;

   std::cout<<"Xic = "<<std::endl;
   std::cout<<Xic<<std::endl;
   std::cout<<"Mic = "<<std::endl;
   std::cout<<Mic<<std::endl;
   std::cout<<"rayleigh(Mic,Xic) = "<<std::endl;
   std::cout<<slip::rayleigh(Xic.begin(),Xic.end(),
			     Mic.upper_left(),Mic.bottom_right())<<std::endl;
   std::cout<<"rayleigh(Mic,Xic) = "<<std::endl;
   std::cout<<slip::rayleigh(Xic,Mic)<<std::endl;

   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Matrix norms" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"M = "<<std::endl;
   std::cout<<M<<std::endl;
   std::cout<<"row norm of M = "<<slip::row_norm(M)<<std::endl;
   std::cout<<"row norm of M = "<<slip::row_norm(M.upper_left(),M.bottom_right())<<std::endl;
   std::cout<<"col norm of M = "<<slip::col_norm(M)<<std::endl;
   std::cout<<"col norm of M = "<<slip::col_norm(M.upper_left(),M.bottom_right())<<std::endl;
   std::cout<<"frobenius norm of M = "<<slip::frobenius_norm(M)<<std::endl;
   std::cout<<"frobenius norm of M = "<<slip::frobenius_norm(M.upper_left(),M.bottom_right())<<std::endl;

   std::cout<<"L22 complex norm of M = "<<slip::L22_norm_cplx(M.upper_left(),M.bottom_right())<<std::endl;
   std::cout<<"Mcc = "<<std::endl;
   std::cout<<Mcc<<std::endl;
   std::cout<<"row norm of Mcc = "<<slip::row_norm(Mcc)<<std::endl;
   std::cout<<"row norm of Mcc = "<<slip::row_norm(Mcc.upper_left(),Mcc.bottom_right())<<std::endl;
   std::cout<<"col norm of Mcc = "<<slip::col_norm(Mcc)<<std::endl;
   std::cout<<"col norm of Mcc = "<<slip::col_norm(Mcc.upper_left(),Mcc.bottom_right())<<std::endl;
   std::cout<<"L22 complex norm of Mcc = "<<slip::L22_norm_cplx(Mcc.upper_left(),Mcc.bottom_right())<<std::endl;
   std::cout<<"frobenius norm of Mcc = "<<slip::frobenius_norm(Mcc)<<std::endl;
   std::cout<<"frobenius norm of Mcc = "<<slip::frobenius_norm(Mcc.upper_left(),Mcc.bottom_right())<<std::endl;
    ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Matrix shift" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Matrix<double> Mshift(4,4);
   slip::iota(Mshift.begin(),Mshift.end(),1.0);
   std::cout<<"Mshift = "<<std::endl;
   std::cout<<Mshift<<std::endl;
   
   slip::Matrix<double> Mshift2(4,4);
   slip::matrix_shift(Mshift.upper_left(),Mshift.bottom_right(),
		      2.2,
		      Mshift2.upper_left(),Mshift2.bottom_right());
   std::cout<<"Mshift - 2.2 I4= "<<std::endl;
   std::cout<<Mshift2<<std::endl;

   slip::matrix_shift(Mshift.upper_left(),Mshift.bottom_right(),2.2);
   std::cout<<"Mshift - 2.2 I4= "<<std::endl;
   std::cout<<Mshift<<std::endl;
   slip::matrix_shift(Mshift,2.2);
   std::cout<<"Mshift - 2.2 I4= "<<std::endl;
   std::cout<<Mshift<<std::endl;
   slip::matrix_shift(Mshift,2.2,Mshift2);
   std::cout<<"Mshift - 2.2 I4= "<<std::endl;
   std::cout<<Mshift<<std::endl;


   slip::Matrix<std::complex<double> > Mshiftc(4,4);
   slip::iota(Mshiftc.begin(),Mshiftc.end(),std::complex<double>(1.0,1.0));
   std::cout<<"Mshiftc = "<<std::endl;
   std::cout<<Mshiftc<<std::endl;
   
   slip::Matrix<std::complex<double> > Mshift2c(4,4);
   slip::matrix_shift(Mshiftc.upper_left(),Mshiftc.bottom_right(),
		      std::complex<double>(2.2,2.2),
		      Mshift2c.upper_left(),Mshift2c.bottom_right());
   std::cout<<"Mshiftc - (2.2+2.2i)* I4= "<<std::endl;
   std::cout<<Mshift2c<<std::endl;

   slip::matrix_shift(Mshiftc.upper_left(),Mshiftc.bottom_right(),std::complex<double>(2.2,2.2));
   std::cout<<"Mshiftc - (2.2+2.2i)I4= "<<std::endl;
   std::cout<<Mshiftc<<std::endl;
   slip::matrix_shift(Mshiftc,std::complex<double>(2.2,2.2));
   std::cout<<"Mshiftc - (2.2+2.2i)I4= "<<std::endl;
   std::cout<<Mshiftc<<std::endl;
   slip::matrix_shift(Mshiftc,std::complex<double>(2.2,2.2),Mshift2c);
   std::cout<<"Mshiftc - (2.2+2.2i)I4= "<<std::endl;
   std::cout<<Mshift2c<<std::endl;

   /////////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Special Matrices" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Matrix<double> H(4,5);
   slip::hilbert(H);
   std::cout<<"Hilbert matrix = "<<std::endl;
   std::cout<<H<<std::endl;

   slip::Array<float> Vtoep(5);
   slip::iota(Vtoep.begin(),Vtoep.end(),1.0);
   std::cout<<"Vtoeplitz = \n"<<Vtoep<<std::endl;
   slip::Matrix<float> Atoep(Vtoep.size(),Vtoep.size());
   slip::toeplitz(Vtoep.begin(),Vtoep.end(),Atoep);
   std::cout<<"Atoeplitz = \n"<<Atoep<<std::endl;
  
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Matrix tests" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Hilbert matrix = "<<std::endl;
   std::cout<<H<<std::endl;
   if(is_squared(H))
     {
       std::cout<<"H is squared"<<std::endl;
     }
   else
     {
        std::cout<<"H is not squared"<<std::endl;
     }
   slip::Box2d<int> box(1,1,3,3);
   slip::Box2d<int> box2(1,0,3,3);
   if(is_squared(H.upper_left(box),H.bottom_right(box)))
     {
       std::cout<<"H"<<box<<" is squared"<<std::endl;
     }
   else
     {
        std::cout<<"H"<<box<<" is not squared"<<std::endl;
     }
 if(is_squared(H.upper_left(box2),H.bottom_right(box2)))
     {
       std::cout<<"H"<<box2<<" is squared"<<std::endl;
     }
   else
     {
        std::cout<<"H"<<box2<<" is not squared"<<std::endl;
     }
 
   slip::Matrix<double> S(5,5);
   std::cout<<"S = "<<std::endl;
   std::cout<<S<<std::endl;
   if(is_identity(S))
     {
       std::cout<<"S is identity"<<std::endl;
     }
   else
     {
        std::cout<<"S is not identity"<<std::endl;
     }
   if(is_diagonal(S))
     {
       std::cout<<"S is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"S is not diagonal"<<std::endl;
     }
   if(is_diagonal(S.upper_left(),S.bottom_right()))
     {
       std::cout<<"S is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"S is not diagonal"<<std::endl;
     }
   if(is_hermitian(S.upper_left(),S.bottom_right()))
     {
       std::cout<<"S is hermitian"<<std::endl;
     }
   else
     {
        std::cout<<"S is not hermitian"<<std::endl;
     }
   fill_diagonal(S,1.0);
   std::cout<<"S = "<<std::endl;
    std::cout<<S<<std::endl;
    if(is_identity(S))
     {
       std::cout<<"S is identity"<<std::endl;
     }
   else
     {
        std::cout<<"S is not identity"<<std::endl;
     }
      if(is_diagonal(S))
     {
       std::cout<<"S is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"S is not diagonal"<<std::endl;
     }
      if(is_diagonal(S.upper_left(),S.bottom_right()))
     {
       std::cout<<"S is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"S is not diagonal"<<std::endl;
     }
      if(is_hermitian(S.upper_left(),S.bottom_right()))
     {
       std::cout<<"S is hermitian"<<std::endl;
     }
   else
     {
        std::cout<<"S is not hermitian"<<std::endl;
     }
    fill_diagonal(S,1.1);
    std::cout<<"S = "<<std::endl;
    std::cout<<S<<std::endl;
    if(is_identity(S))
     {
       std::cout<<"S is identity"<<std::endl;
     }
   else
     {
        std::cout<<"S is not identity"<<std::endl;
     }
    if(is_diagonal(S))
     {
       std::cout<<"S is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"S is not diagonal"<<std::endl;
     }
       if(is_diagonal(S.upper_left(),S.bottom_right()))
     {
       std::cout<<"S is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"S is not diagonal"<<std::endl;
     }
        if(is_hermitian(S.upper_left(),S.bottom_right()))
     {
       std::cout<<"S is hermitian"<<std::endl;
     }
   else
     {
        std::cout<<"S is not hermitian"<<std::endl;
     }
    S(1,3) = 2.0;
     std::cout<<"S = "<<std::endl;
    std::cout<<S<<std::endl;
    if(is_identity(S))
     {
       std::cout<<"S is identity"<<std::endl;
     }
   else
     {
        std::cout<<"S is not identity"<<std::endl;
     }
    if(is_diagonal(S))
     {
       std::cout<<"S is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"S is not diagonal"<<std::endl;
     }
       if(is_diagonal(S.upper_left(),S.bottom_right()))
     {
       std::cout<<"S is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"S is not diagonal"<<std::endl;
     }
        if(is_hermitian(S.upper_left(),S.bottom_right()))
     {
       std::cout<<"S is hermitian"<<std::endl;
     }
   else
     {
        std::cout<<"S is not hermitian"<<std::endl;
     }
    //
    slip::Matrix<std::complex<double> > Sc(5,5);
   std::cout<<"Sc = "<<std::endl;
   std::cout<<Sc<<std::endl;
   if(is_identity(Sc))
     {
       std::cout<<"Sc is identity"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not identity"<<std::endl;
     }
   if(is_diagonal(Sc))
     {
       std::cout<<"Sc is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not diagonal"<<std::endl;
     }
       if(is_diagonal(Sc.upper_left(),Sc.bottom_right()))
     {
       std::cout<<"Sc is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not diagonal"<<std::endl;
     }
        if(is_hermitian(Sc.upper_left(),Sc.bottom_right()))
     {
       std::cout<<"Sc is hermitian"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not hermitian"<<std::endl;
     }
   fill_diagonal(Sc,1.0);
   std::cout<<"Sc = "<<std::endl;
    std::cout<<Sc<<std::endl;
    if(is_identity(Sc))
     {
       std::cout<<"Sc is identity"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not identity"<<std::endl;
     }
      if(is_diagonal(Sc))
     {
       std::cout<<"Sc is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not diagonal"<<std::endl;
     }
          if(is_diagonal(Sc.upper_left(),Sc.bottom_right()))
     {
       std::cout<<"Sc is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not diagonal"<<std::endl;
     }
	  if(is_hermitian(Sc.upper_left(),Sc.bottom_right()))
     {
       std::cout<<"Sc is hermitian"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not hermitian"<<std::endl;
     }
      fill_diagonal(Sc,std::complex<double>(1.1,3.3));
    std::cout<<"Sc = "<<std::endl;
    std::cout<<Sc<<std::endl;
    if(is_identity(Sc))
     {
       std::cout<<"Sc is identity"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not identity"<<std::endl;
     }
    if(is_diagonal(Sc))
     {
       std::cout<<"Sc is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not diagonal"<<std::endl;
     }
    if(is_diagonal(Sc.upper_left(),Sc.bottom_right()))
     {
       std::cout<<"Sc is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not diagonal"<<std::endl;
     }
          if(is_hermitian(Sc.upper_left(),Sc.bottom_right()))
     {
       std::cout<<"Sc is hermitian"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not hermitian"<<std::endl;
     }
    Sc(1,3) = 2.0;
     std::cout<<"Sc = "<<std::endl;
    std::cout<<Sc<<std::endl;
    if(is_identity(Sc))
     {
       std::cout<<"Sc is identity"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not identity"<<std::endl;
     }
    if(is_diagonal(Sc))
     {
       std::cout<<"Sc is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not diagonal"<<std::endl;
     }
    if(is_diagonal(Sc.upper_left(),Sc.bottom_right()))
     {
       std::cout<<"Sc is diagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not diagonal"<<std::endl;
     }
          if(is_hermitian(Sc.upper_left(),Sc.bottom_right()))
     {
       std::cout<<"Sc is hermitian"<<std::endl;
     }
   else
     {
        std::cout<<"Sc is not hermitian"<<std::endl;
     }

     slip::Matrix<std::complex<double> > Shc(3,3);
     Shc(0,0) = std::complex<double>(1.0,0.0);
     Shc(0,1) = std::complex<double>(0.5,-0.5);
     Shc(0,2) = std::complex<double>(1.0,-1.2);
     Shc(1,0) = std::complex<double>(0.5,0.5);  
     Shc(1,1) = std::complex<double>(2.0,0.0);
     Shc(1,2) = std::complex<double>(0.2,-0.3);
     Shc(2,0) = std::complex<double>(1.0,1.2);  
     Shc(2,1) = std::complex<double>(0.2,0.3);
     Shc(2,2) = std::complex<double>(3.0,0.0);
     slip::Matrix<double> Sh(3,3);
     Sh(0,0) = 1.0;
     Sh(0,1) = 0.5;
     Sh(0,2) = -1.2;
     Sh(1,0) = 0.5;
     Sh(1,1) = 2.0;
     Sh(1,2) = -0.3;
     Sh(2,0) = -1.2;
     Sh(2,1) = -0.3;
     Sh(2,2) = 3.0;
     std::cout<<"Shc = \n"<<Shc<<std::endl;
     if(is_hermitian(Shc.upper_left(),Shc.bottom_right()))
     {
       std::cout<<"Shc is hermitian"<<std::endl;
     }
   else
     {
        std::cout<<"Shc is not hermitian"<<std::endl;
     }
     if(is_symmetric(Shc.upper_left(),Shc.bottom_right()))
     {
       std::cout<<"Shc is symmetric"<<std::endl;
     }
   else
     {
        std::cout<<"Shc is not symmetric"<<std::endl;
     }
     std::cout<<"Sh = \n"<<Sh<<std::endl;
     if(is_hermitian(Sh.upper_left(),Sh.bottom_right()))
     {
       std::cout<<"Sh is hermitian"<<std::endl;
     }
   else
     {
        std::cout<<"Sh is not hermitian"<<std::endl;
     }
      if(is_symmetric(Sh.upper_left(),Sh.bottom_right()))
     {
       std::cout<<"Sh is symmetric"<<std::endl;
     }
   else
     {
        std::cout<<"Sh is not symmetric"<<std::endl;
     }
      slip::Matrix<std::complex<double> > Sshc(3,3);
     Sshc(0,0) = std::complex<double>(0.0,1.0);
     Sshc(0,1) = std::complex<double>(0.5,-0.5);
     Sshc(0,2) = std::complex<double>(1.0,-1.2);
     Sshc(1,0) = std::complex<double>(-0.5,-0.5);  
     Sshc(1,1) = std::complex<double>(0.0,2.0);
     Sshc(1,2) = std::complex<double>(0.2,-0.3);
     Sshc(2,0) = std::complex<double>(-1.0,-1.2);  
     Sshc(2,1) = std::complex<double>(-0.2,-0.3);
     Sshc(2,2) = std::complex<double>(0.0,3.0);
     std::cout<<"Sshc = \n"<<Sshc<<std::endl;
     if(is_hermitian(Sshc.upper_left(),Sshc.bottom_right()))
     {
       std::cout<<"Sshc is hermitian"<<std::endl;
     }
   else
     {
        std::cout<<"Sshc is not hermitian"<<std::endl;
     } 
     if(is_skew_hermitian(Sshc.upper_left(),Sshc.bottom_right()))
     {
       std::cout<<"Sshc is skew hermitian"<<std::endl;
     }
   else
     {
        std::cout<<"Sshc is not skew hermitian"<<std::endl;
     }
      slip::Matrix<std::complex<double> > Sudc(3,3);
     Sudc(0,0) = std::complex<double>(1.0,0.0);
     Sudc(0,1) = std::complex<double>(0.5,-0.5);
     Sudc(0,2) = std::complex<double>(1.0,-1.2);
     Sudc(1,0) = std::complex<double>(0.0,0.0);  
     Sudc(1,1) = std::complex<double>(2.0,0.0);
     Sudc(1,2) = std::complex<double>(0.2,-0.3);
     Sudc(2,0) = std::complex<double>(0.0,0.0);  
     Sudc(2,1) = std::complex<double>(0.0,0.0);
     Sudc(2,2) = std::complex<double>(3.0,0.0);
     slip::Matrix<double> Sud(3,3);
     Sud(0,0) = 1.0;
     Sud(0,1) = 0.5;
     Sud(0,2) = -1.2;
     Sud(1,0) = 0.0;
     Sud(1,1) = 2.0;
     Sud(1,2) = -0.3;
     Sud(2,0) = 0.0;
     Sud(2,1) = 0.0;
     Sud(2,2) = 3.0;
     std::cout<<"Sudc = \n"<<Sudc<<std::endl;
     if(is_upper_triangular(Sudc.upper_left(),Sudc.bottom_right()))
     {
       std::cout<<"Sudc is upper triangular"<<std::endl;
     }
   else
     {
        std::cout<<"Sudc is not upper triangular"<<std::endl;
     }
 if(is_lower_triangular(Sudc.upper_left(),Sudc.bottom_right()))
     {
       std::cout<<"Sudc is lower triangular"<<std::endl;
     }
   else
     {
        std::cout<<"Sudc is not lower triangular"<<std::endl;
     }
     std::cout<<"Sud = \n"<<Sud<<std::endl;
 if(is_upper_triangular(Sud.upper_left(),Sud.bottom_right()))
     {
       std::cout<<"Sud is upper triangular"<<std::endl;
     }
   else
     {
        std::cout<<"Sud is not upper triangular"<<std::endl;
     }
  if(is_lower_triangular(Sud.upper_left(),Sud.bottom_right()))
     {
       std::cout<<"Sud is lower triangular"<<std::endl;
     }
   else
     {
        std::cout<<"Sud is not lower triangular"<<std::endl;
     }
     slip::Matrix<std::complex<double> > Sldc(3,3);
     Sldc(0,0) = std::complex<double>(1.0,0.0);
     Sldc(0,1) = std::complex<double>(0.0,0.0);
     Sldc(0,2) = std::complex<double>(0.0,0.0);
     Sldc(1,0) = std::complex<double>(0.5,-0.5);  
     Sldc(1,1) = std::complex<double>(2.0,0.0);
     Sldc(1,2) = std::complex<double>(0.0,0.0);
     Sldc(2,0) = std::complex<double>(0.0,0.0);  
     Sldc(2,1) = std::complex<double>(1.0,-1.2);
     Sldc(2,2) = std::complex<double>(3.0,0.0);
     slip::Matrix<double> Sld(3,3);
     Sld(0,0) = 1.0;
     Sld(0,1) = 0.0;
     Sld(0,2) = 0.0;
     Sld(1,0) = 2.0;
     Sld(1,1) = 3.0;
     Sld(1,2) = 0.0;
     Sld(2,0) = 1.0;
     Sld(2,1) = 2.0;
     Sld(2,2) = 3.0;
     std::cout<<"Sldc = \n"<<Sldc<<std::endl;
     if(is_upper_triangular(Sldc.upper_left(),Sldc.bottom_right()))
     {
       std::cout<<"Sldc is upper triangular"<<std::endl;
     }
   else
     {
        std::cout<<"Sldc is not upper triangular"<<std::endl;
     }
     if(is_lower_triangular(Sldc.upper_left(),Sldc.bottom_right()))
     {
       std::cout<<"Sldc is lower triangular"<<std::endl;
     }
   else
     {
        std::cout<<"Sldc is not lower triangular"<<std::endl;
     }
     std::cout<<"Sld = \n"<<Sld<<std::endl;
   if(is_upper_triangular(Sld.upper_left(),Sld.bottom_right()))
     {
       std::cout<<"Sld is upper triangular"<<std::endl;
     }
   else
     {
        std::cout<<"Sld is not upper triangular"<<std::endl;
     }
     if(is_lower_triangular(Sld.upper_left(),Sld.bottom_right()))
     {
       std::cout<<"Sld is lower triangular"<<std::endl;
     }
   else
     {
        std::cout<<"Sld is not lower triangular"<<std::endl;
     }
     slip::Matrix<std::complex<double> > Sb11c(5,5);
     slip::fill_diagonal(Sb11c,std::complex<double>(1.0,1.0));
     std::cout<<"Sb11cld = \n"<<Sb11c<<std::endl;
       if(is_band_matrix(Sb11c.upper_left(),Sb11c.bottom_right(),0,0))
     {
       std::cout<<"Sb11c is band matrix 0 0"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not band matrix 0 0"<<std::endl;
     }
    if(is_upper_bidiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is upper bidiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not upper bidiagonal"<<std::endl;
     }
    if(is_lower_bidiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is lower bidiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not lower bidiagonal"<<std::endl;
     }
     if(is_tridiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is tridiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not tridiagonal"<<std::endl;
     }
     if(is_upper_hessenberg(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is upper hessenberg"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not upper hessenberg"<<std::endl;
     }
    if(is_lower_hessenberg(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is lower hessenberg"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not lower hessenberg"<<std::endl;
     }
     slip::fill_diagonal(Sb11c,std::complex<double>(2.0,2.0),1);
     slip::fill_diagonal(Sb11c,std::complex<double>(-2.0,-2.0),-1);
     std::cout<<"Sb11cld = \n"<<Sb11c<<std::endl;
     if(is_band_matrix(Sb11c.upper_left(),Sb11c.bottom_right(),1,1))
     {
       std::cout<<"Sb11c is band matrix 1 1"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not band matrix 1 1"<<std::endl;
     }
     if(is_upper_bidiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is upper bidiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not upper bidiagonal"<<std::endl;
     }
    if(is_lower_bidiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is lower bidiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not lower bidiagonal"<<std::endl;
     }
     if(is_tridiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is tridiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not tridiagonal"<<std::endl;
     }
     if(is_upper_hessenberg(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is upper hessenberg"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not upper hessenberg"<<std::endl;
     }
    if(is_lower_hessenberg(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is lower hessenberg"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not lower hessenberg"<<std::endl;
     }
     slip::fill_diagonal(Sb11c,std::complex<double>(3.0,3.0),2);
     //  slip::fill_diagonal(Sb11c,std::complex<double>(-3.0,-3.0),-2);
      std::cout<<"Sb11cld = \n"<<Sb11c<<std::endl;
      if(is_band_matrix(Sb11c.upper_left(),Sb11c.bottom_right(),1,2))
     {
       std::cout<<"Sb11c is band matrix 1 2"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not band matrix 1 2"<<std::endl;
     }
       if(is_upper_bidiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is upper bidiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not upper bidiagonal"<<std::endl;
     }
    if(is_lower_bidiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is lower bidiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not lower bidiagonal"<<std::endl;
     }
     if(is_tridiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is tridiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not tridiagonal"<<std::endl;
     }
     if(is_upper_hessenberg(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is upper hessenberg"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not upper hessenberg"<<std::endl;
     }
    if(is_lower_hessenberg(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is lower hessenberg"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not lower hessenberg"<<std::endl;
     }
      slip::fill_diagonal(Sb11c,std::complex<double>(0.0,0.0),2);
      std::cout<<"Sb11cld = \n"<<Sb11c<<std::endl;
    

      slip::fill_diagonal(Sb11c,std::complex<double>(-3.0,-3.0),-2);
      std::cout<<"Sb11cld = \n"<<Sb11c<<std::endl;
      if(is_band_matrix(Sb11c.upper_left(),Sb11c.bottom_right(),2,1))
     {
       std::cout<<"Sb11c is band matrix 2 1"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not band matrix 2 1"<<std::endl;
     }
 if(is_upper_bidiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is upper bidiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not upper bidiagonal"<<std::endl;
     }
    if(is_lower_bidiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is lower bidiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not lower bidiagonal"<<std::endl;
     }
     if(is_tridiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is tridiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not tridiagonal"<<std::endl;
     }
     if(is_upper_hessenberg(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is upper hessenberg"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not upper hessenberg"<<std::endl;
     }
    if(is_lower_hessenberg(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is lower hessenberg"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not lower hessenberg"<<std::endl;
     }
       slip::fill_diagonal(Sb11c,std::complex<double>(-4.0,-4.0),-3);
      std::cout<<"Sb11cld = \n"<<Sb11c<<std::endl;
      if(is_band_matrix(Sb11c.upper_left(),Sb11c.bottom_right(),3,1))
     {
       std::cout<<"Sb11c is band matrix 3 1"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not band matrix 3 1"<<std::endl;
     }
      slip::fill_diagonal(Sb11c,std::complex<double>(3.0,3.0),2);
      std::cout<<"Sb11cld = \n"<<Sb11c<<std::endl;
      if(is_band_matrix(Sb11c.upper_left(),Sb11c.bottom_right(),3,2))
     {
       std::cout<<"Sb11c is band matrix 3 2"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not band matrix 3 2"<<std::endl;
     }
 if(is_upper_bidiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is upper bidiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not upper bidiagonal"<<std::endl;
     }
    if(is_lower_bidiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is lower bidiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not lower bidiagonal"<<std::endl;
     }
     if(is_tridiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is tridiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not tridiagonal"<<std::endl;
     }
     if(is_upper_hessenberg(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is upper hessenberg"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not upper hessenberg"<<std::endl;
     }
    if(is_lower_hessenberg(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is lower hessenberg"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not lower hessenberg"<<std::endl;
     }
      slip::fill_diagonal(Sb11c,std::complex<double>(4.0,4.0),3);
      slip::fill_diagonal(Sb11c,std::complex<double>(5.0,5.0),4);
      slip::fill_diagonal(Sb11c,std::complex<double>(-5.0,-5.0),-4);
     
      std::cout<<"Sb11cld = \n"<<Sb11c<<std::endl;
      if(is_band_matrix(Sb11c.upper_left(),Sb11c.bottom_right(),4,4))
     {
       std::cout<<"Sb11c is band matrix 4 4"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not band matrix 4 4"<<std::endl;
     }
       if(is_upper_bidiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is upper bidiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not upper bidiagonal"<<std::endl;
     }
    if(is_lower_bidiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is lower bidiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not lower bidiagonal"<<std::endl;
     }
     if(is_tridiagonal(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is tridiagonal"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not tridiagonal"<<std::endl;
     }
     if(is_upper_hessenberg(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is upper hessenberg"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not upper hessenberg"<<std::endl;
     }
    if(is_lower_hessenberg(Sb11c.upper_left(),Sb11c.bottom_right()))
     {
       std::cout<<"Sb11c is lower hessenberg"<<std::endl;
     }
   else
     {
        std::cout<<"Sb11c is not lower hessenberg"<<std::endl;
     }

    /////////////////////////////////////////////////////////////////////////
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "is_null_diagonal " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    slip::Array2d<double> Anul(6,6);
    slip::fill_diagonal(Anul,1.0,0);
    std::cout<<"Anul = \n"<<Anul<<std::endl;
    std::cout<<"i = "<<0<<" "<<slip::is_null_diagonal(Anul.upper_left(),Anul.bottom_right(),0)<<std::endl;
    for(int i = 1; i < int(Anul.rows());++i)
      {

	std::cout<<"i = "<<i<<" "<<slip::is_null_diagonal(Anul.upper_left(),Anul.bottom_right(),i)<<std::endl;
	std::cout<<"i = "<<-i<<" "<<slip::is_null_diagonal(Anul.upper_left(),Anul.bottom_right(),-i)<<std::endl;
	
      }
    slip::fill_diagonal(Anul,-2.0,-1);
    std::cout<<"Anul = \n"<<Anul<<std::endl;
    std::cout<<"i = "<<0<<" "<<slip::is_null_diagonal(Anul.upper_left(),Anul.bottom_right(),0)<<std::endl;
    for(int i = 1; i < int(Anul.rows());++i)
      {

	std::cout<<"i = "<<i<<" "<<slip::is_null_diagonal(Anul.upper_left(),Anul.bottom_right(),i)<<std::endl;
	std::cout<<"i = "<<-i<<" "<<slip::is_null_diagonal(Anul.upper_left(),Anul.bottom_right(),-i)<<std::endl;
	
      }

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "analyse_matrix" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<slip::analyse_matrix(Anul.upper_left(),Anul.bottom_right())<<std::endl;

    /////////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "get_diagonal " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array2d<float> AA(5,5);
  slip::iota(AA.begin(),AA.end(),1.0);
  std::cout<<"AA ="<<std::endl;
  std::cout<<AA<<std::endl;
  slip::Array<float> diag0(5);
  slip::get_diagonal(AA,diag0.begin(),diag0.end());
  std::cout<<"main diagonal:\n"<<diag0<<std::endl;
 slip::Array<float> diag1(4);
 slip::get_diagonal(AA,diag1.begin(),diag1.end(),1);
 std::cout<<"first upper diagonal:\n"<<diag1<<std::endl;
  slip::Array<float> diagm1(4);
  slip::get_diagonal(AA,diagm1.begin(),diagm1.end(),-1);
  std::cout<<"first lower diagonal:\n"<<diagm1<<std::endl;
  slip::Array<float> diag2(3);
  slip::get_diagonal(AA,diag2.begin(),diag2.end(),2);
  std::cout<<"second upper diagonal:\n"<<diag2<<std::endl;
  slip::Array<float> diagm2(3);
  slip::get_diagonal(AA,diagm2.begin(),diagm2.end(),-2);
  std::cout<<"second lower diagonal:\n"<<diagm2<<std::endl;
  
  /////////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "set_diagonal " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  diag0.fill(0.0);
  diag1.fill(1.0);
  diagm1.fill(-1.0);
  diag2.fill(2.0);
  diagm2.fill(-2.0);
  std::cout<<"AA ="<<std::endl;
  std::cout<<AA<<std::endl;
  std::cout<<"set main diagonal with : "<<diag0<<std::endl;
  slip::set_diagonal(diag0.begin(),diag0.end(),AA);
  std::cout<<"set first upper diagonal with : "<<diag1<<std::endl;
  slip::set_diagonal(diag1.begin(),diag1.end(),AA,1);
  std::cout<<"set first lower diagonal with : "<<diagm1<<std::endl;
  slip::set_diagonal(diagm1.begin(),diagm1.end(),AA,-1);
  std::cout<<"set second upper diagonal with : "<<diag2<<std::endl;
  slip::set_diagonal(diag2.begin(),diag2.end(),AA,2);
  std::cout<<"set second lower diagonal with : "<<diagm2<<std::endl;
  slip::set_diagonal(diagm2.begin(),diagm2.end(),AA,-2);
  std::cout<<"new AA ="<<std::endl;
  std::cout<<AA<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "fill_diagonal " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"AA ="<<std::endl;
   std::cout<<AA<<std::endl;

   std::cout<<"fill main diagonal with 1.0 :"<<std::endl;
   slip::fill_diagonal(AA,1.0);
  std::cout<<"fill first upper diagonal with 2.0 :"<<std::endl;
  slip::fill_diagonal(AA,2.0,1);
  std::cout<<"fill first lower diagonal with -2.0 :"<<std::endl;
  slip::fill_diagonal(AA,-2.0,-1);
  std::cout<<"fill second upper diagonal with 3.0 :"<<std::endl;
  slip::fill_diagonal(AA,3.0,2);
  std::cout<<"fill second lower diagonal with -3.0 :"<<std::endl;
  slip::fill_diagonal(AA,-3.0,-2);
  std::cout<<"new AA ="<<std::endl;
  std::cout<<AA<<std::endl;
  slip::fill_diagonal(AA.upper_left(),AA.bottom_right(),1.5,-2);
  std::cout<<"new AA ="<<std::endl;
  std::cout<<AA<<std::endl;


  /////////////////////////////////////////////////////////////
  //  test tridiagonal LU
  /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "tridiagonal LU" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   double dtlu_diag[]     = {2.0, 1.0, 5.0, -0.2, 7.0, 9.0, 8.0};
   double dtlu_up_diag[]  = {1.0, 1.0, 1.0, 1.2, 1.0, 1.0};
   double dtlu_low_diag[] = {1.0, 1.0, 1.0, 1.2, 1.0, 1.0};
   slip::Array<double> Dtlu_diag(7,dtlu_diag);
   slip::Array<double> Dtlu_up_diag(6,dtlu_up_diag);
   slip::Array<double> Dtlu_low_diag(6,dtlu_low_diag);
   std::cout<<"Dtlu_diag = "<<Dtlu_diag<<std::endl;
   std::cout<<"Dtlu_up_diag = "<<Dtlu_up_diag<<std::endl;
   std::cout<<"Dtlu_low_diag = "<<Dtlu_low_diag<<std::endl;

   slip::Array<double> dtlu_l(6);
   slip::Array<double> dtlu_u(7);
   slip::tridiagonal_lu(dtlu_diag,dtlu_diag+7,
			dtlu_up_diag,dtlu_up_diag+6,
			dtlu_low_diag,dtlu_low_diag+6,
			dtlu_l.begin(),dtlu_l.end(),
			dtlu_u.begin(),dtlu_u.end());
   slip::Array2d<double> dtlu_L(7,7);
   slip::set_diagonal(dtlu_l.begin(),dtlu_l.end(),dtlu_L,-1);
   slip::fill_diagonal(dtlu_L,1.0);
   slip::Array2d<double> dtlu_U(7,7);
   slip::set_diagonal(dtlu_u.begin(),dtlu_u.end(),dtlu_U,0);
   slip::set_diagonal(Dtlu_up_diag.begin(),Dtlu_up_diag.end(),dtlu_U,1);
   slip::Array2d<double> dtlu_Lxdtlu_U(7,7);
   slip::matrix_matrix_multiplies(dtlu_L,dtlu_U,dtlu_Lxdtlu_U);
   std::cout<<"dtlu_l = \n"<<dtlu_l<<std::endl;
   std::cout<<"dtlu_u = \n"<<dtlu_u<<std::endl;
   std::cout<<"dtlu_L = \n"<<dtlu_L<<std::endl;
   std::cout<<"dtlu_U = \n"<<dtlu_U<<std::endl;
  
   std::cout<<"dtlu_Lxdtlu_U = \n"<<dtlu_Lxdtlu_U<<std::endl;


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "complex tridiagonal LU" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
   std::complex<double> dtlu_diagc[] = {TC(2.0,2.0), TC(1.0,0.5), TC(5.0,6.0),
					TC(-0.2,-0.2),TC(7.0,6.0),TC(9.0,2.0), 
					TC(8.0,2.0)};
   std::complex<double> dtlu_up_diagc[]  = {TC(1.0,1.0), TC(1.0,1.0), TC(2.0,2.0),
			      TC(-0.2,-0.2),TC(1.0,1.0),TC(1.0,3.0)};
   std::complex<double> dtlu_low_diagc[] = {TC(1.0,1.0), TC(1.0,1.0), TC(2.0,2.0),
			      TC(-0.2,-0.2),TC(1.0,1.0),TC(1.0,3.0)};
   slip::Array<std::complex<double> > Dtlu_diagc(7,dtlu_diagc);
   slip::Array<std::complex<double> > Dtlu_up_diagc(6,dtlu_up_diagc);
   slip::Array<std::complex<double> > Dtlu_low_diagc(6,dtlu_low_diagc);
   std::cout<<"Dtlu_diagc = "<<Dtlu_diagc<<std::endl;
   std::cout<<"Dtlu_up_diagc = "<<Dtlu_up_diagc<<std::endl;
   std::cout<<"Dtlu_low_diagc = "<<Dtlu_low_diagc<<std::endl;

   slip::Array<std::complex<double> > dtlu_lc(6);
   slip::Array<std::complex<double> > dtlu_uc(7);
   slip::tridiagonal_lu(dtlu_diagc,dtlu_diagc+7,
			dtlu_up_diagc,dtlu_up_diagc+6,
			dtlu_low_diagc,dtlu_low_diagc+6,
			dtlu_lc.begin(),dtlu_lc.end(),
			dtlu_uc.begin(),dtlu_uc.end());
   slip::Array2d<std::complex<double> > dtlu_Lc(7,7);
   slip::set_diagonal(dtlu_lc.begin(),dtlu_lc.end(),dtlu_Lc,-1);
   slip::fill_diagonal(dtlu_Lc,1.0);
   slip::Array2d<std::complex<double> > dtlu_Uc(7,7);
   slip::set_diagonal(dtlu_uc.begin(),dtlu_uc.end(),dtlu_Uc,0);
   slip::set_diagonal(Dtlu_up_diagc.begin(),Dtlu_up_diagc.end(),dtlu_Uc,1);
   slip::Array2d<std::complex<double> > dtlu_Lxdtlu_Uc(7,7);
   slip::matrix_matrix_multiplies(dtlu_Lc,dtlu_Uc,dtlu_Lxdtlu_Uc);
   std::cout<<"dtlu_lc = \n"<<dtlu_lc<<std::endl;
   std::cout<<"dtlu_uc = \n"<<dtlu_uc<<std::endl;
   std::cout<<"dtlu_Lc = \n"<<dtlu_Lc<<std::endl;
   std::cout<<"dtlu_Uc = \n"<<dtlu_Uc<<std::endl;
  
   std::cout<<"dtlu_Lxdtlu_Uc = \n"<<dtlu_Lxdtlu_Uc<<std::endl;
   
   

   /////////////////////////////////////////////////////////////
  //  test Lower triangular solve algorithm
  /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Lower triangular solve" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   double dlow[] = {2.0, 0.0, 0.0, 1.0, 5.0, 0.0, 7.0, 9.0, 8.0};
   slip::Array2d<double> Alow(3,3,dlow);
   std::cout<<"Alow ="<<std::endl;
   std::cout<<Alow<<std::endl;
   slip::Array<double> Xlow(3);
   double blow[] = {6.0,2.0,5.0};
   slip::Array<double> Blow(3,blow);
   std::cout<<"Blow ="<<std::endl;
   std::cout<<Blow<<std::endl;
   slip::lower_triangular_solve(Alow,Xlow,Blow,10E-6);
   std::cout<<"Xlow = "<<Xlow<<std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::Array<float> Blow2(3);
   slip::matrix_vector_multiplies(Alow,Xlow,Blow2);
   std::cout<<"Alow * Xlow = "<<std::endl;
   std::cout<<Blow2<<std::endl;

   slip::lower_triangular_solve(Alow.upper_left(),Alow.bottom_right(),
				Xlow.begin(),Xlow.end(),
				Blow.begin(),Blow.end(),10E-6);
   std::cout<<"Xlow = "<<Xlow<<std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::matrix_vector_multiplies(Alow,Xlow,Blow2);
   std::cout<<"Alow * Xlow = "<<std::endl;
   std::cout<<Blow2<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Lower triangular inverse" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Array2d<double> invAlow(3,3);
   std::cout<<"Alow \n="<<Alow<<std::endl;
   slip::lower_triangular_inv(Alow.upper_left(),Alow.bottom_right(),
			      invAlow.upper_left(),invAlow.bottom_right(),
			      1.0e-6);
   std::cout<<"invAlow \n="<<invAlow<<std::endl;
   slip::Array2d<double> IAlow(3,3);
   slip::matrix_matrix_multiplies(Alow,invAlow,IAlow);
   std::cout<<"Alow invAlow = \n"<<IAlow<<std::endl;
			      

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Complex Lower triangular solve" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::complex<double> dlowc[] = {TC(2.0,0.2), TC(0.0), TC(0.0), 
		     TC(1.0,1.3), TC(5.0,2.1), TC(0.0), 
		     TC(7.0,1.4), TC(9.0,-1.5), TC(8.0,1.4)};
   slip::Array2d<std::complex<double> > Alowc(3,3,dlowc);
   std::cout<<"Alowc ="<<std::endl;
   std::cout<<Alowc<<std::endl;
   slip::Array<std::complex<double> > Xlowc(3);
   std::complex<double> blowc[] = {TC(6.0,1.0),TC(2.0,1.5),TC(5.0,2.3)};
   slip::Array<std::complex<double> > Blowc(3,blowc);
   std::cout<<"Blowc ="<<std::endl;
   std::cout<<Blowc<<std::endl;
   slip::lower_triangular_solve(Alowc,Xlowc,Blowc,10E-6);
   std::cout<<"Xlowc = "<<Xlowc<<std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::Array<std::complex<double> > Blow2c(3);
   slip::matrix_vector_multiplies(Alowc,Xlowc,Blow2c);
   std::cout<<"Alowc * Xlowc = "<<std::endl;
   std::cout<<Blow2c<<std::endl;

   slip::lower_triangular_solve(Alowc.upper_left(),Alowc.bottom_right(),
				Xlowc.begin(),Xlowc.end(),
				Blowc.begin(),Blowc.end(),10E-6);
   std::cout<<"Xlowc = "<<Xlowc<<std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::matrix_vector_multiplies(Alowc,Xlowc,Blow2c);
   std::cout<<"Alowc * Xlowc = "<<std::endl;
   std::cout<<Blow2c<<std::endl;

  /////////////////////////////////////////////////////////////
  //  test Upper triangular solve algorithm
  /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Upper triangular solve" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   double dup2[] = {2.0, 3.0, 4.5, 0.0, 1.0, 3.0, 0.0, 0.0, 2.0};
   slip::Array2d<double> Aup(3,3,dup2);
   std::cout<<"Aup ="<<std::endl;
   std::cout<<Aup<<std::endl;
   slip::Array<double> Xup(3);
   double bup[] = {6.0,2.0,5.0};
   slip::Array<double> Bup(3,bup);
   std::cout<<"Bup ="<<std::endl;
   std::cout<<Bup<<std::endl;
   slip::upper_triangular_solve(Aup,Xup,Bup,10E-6);
   std::cout<<"Xup = "<<Xup<<std::endl;

   std::cout<<"Check:"<<std::endl;
   slip::Array<float> Bup2(3);
   slip::matrix_vector_multiplies(Aup,Xup,Bup2);
   std::cout<<"Aup * Xup = "<<std::endl;
   std::cout<<Bup2<<std::endl;
   
  
   slip::upper_triangular_solve(Aup.upper_left(),Aup.bottom_right(),
				Xup.begin(),Xup.end(),
				Bup.begin(),Bup.end(),10E-6);
   std::cout<<"Xup = "<<Xup<<std::endl;
   
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Upper triangular inverse " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Aup =\n "<<Aup<<std::endl;
   slip::Array2d<double> invAup(3,3);
   slip::upper_triangular_inv(Aup.upper_left(),Aup.bottom_right(),
			      invAup.upper_left(),invAup.bottom_right(),
			      1.0e-8);
    std::cout<<"invAup = \n"<<invAup<<std::endl;
    std::cout<<"Check:"<<std::endl;
   slip::Array2d<float> IAup(3,3);
   slip::matrix_matrix_multiplies(Aup,invAup,IAup);
   std::cout<<"Aup * invAup = \n"<<IAup<<std::endl;
  

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Complex Upper triangular solve" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::complex<double> dup2c[] = {TC(2.0,0.5), TC(1.0,0.3), TC(5.0,1.2),
                                   TC(0.0,0.0), TC(1.0,2.1), TC(3.0,-0.1),
		                   TC(0.0,0.0), TC(0.0,0.0), TC(2.0,4.1)};
   slip::Array2d<std::complex<double> > Aupc(3,3,dup2c);
   std::cout<<"Aupc ="<<std::endl;
   std::cout<<Aupc<<std::endl;
   slip::Array<std::complex<double> > Xupc(3);
   std::complex<double> bupc[] = {TC(6.0,0.5),TC(2.0,1.1),TC(5.0,4.7)};
   slip::Array<std::complex<double> > Bupc(3,bupc);
   std::cout<<"Bupc ="<<std::endl;
   std::cout<<Bupc<<std::endl;
   slip::upper_triangular_solve(Aupc,Xupc,Bupc,10E-6);
   std::cout<<"Xupc = "<<Xupc<<std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::Array<std::complex<double> > Bup2c(3);
   slip::matrix_vector_multiplies(Aupc,Xupc,Bup2c);
   std::cout<<"Aupc * Xupc = "<<std::endl;
   std::cout<<Bup2c<<std::endl;
   slip::upper_triangular_solve(Aupc.upper_left(),Aupc.bottom_right(),
				Xupc.begin(),Xupc.end(),
				Bupc.begin(),Bupc.end(),10E-6);
   std::cout<<"Xupc = "<<Xupc<<std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::matrix_vector_multiplies(Aupc,Xupc,Bup2c);
   std::cout<<"Aupc * Xupc = "<<std::endl;
   std::cout<<Bup2c<<std::endl;

   /////////////////////////////////////////////////////////////
  //  test unit Lower triangular solve algorithm
  /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "unit lower triangular solve" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   double dllow[] = {1.0, 0.0, 0.0, 2.0, 1.0, 0.0, 3.0, 4.0, 1.0};
   slip::Array2d<double> Allow(3,3,dllow);
   std::cout<<"Allow ="<<std::endl;
   std::cout<<Allow<<std::endl;
   slip::Array<double> Xllow(3);
   double bllow[] = {6.0,2.0,5.0};
   slip::Array<double> Bllow(3,bllow);
   std::cout<<"Bllow ="<<std::endl;
   std::cout<<Bllow<<std::endl;
   slip::unit_lower_triangular_solve(Allow,Xllow,Bllow);
   std::cout<<"Xllow = "<<Xllow<<std::endl;
   slip::unit_lower_triangular_solve(Allow.upper_left(),Allow.bottom_right(),
				     Xllow.begin(),Xllow.end(),
				     Bllow.begin(),Bllow.end());
   std::cout<<"Xllow = "<<Xllow<<std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::Array<float> Bllow2(3);
   slip::matrix_vector_multiplies(Allow,Xllow,Bllow2);
   std::cout<<"Allow * Xllow = "<<std::endl;
   std::cout<<Bllow2<<std::endl;

   /////////////////////////////////////////////////////////////
   //  test unit Upper triangular solve algorithm
   /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "unit upper triangular solve" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   double dulow[] = {1.0, 2.0, 3.0, 0.0, 1.0, 4.0, 0.0, 0.0, 1.0};
   slip::Array2d<double> Aulow(3,3,dulow);
   std::cout<<"Aulow ="<<std::endl;
   std::cout<<Aulow<<std::endl;
   slip::Array<double> Xulow(3);
   double bulow[] = {6.0,2.0,5.0};
   slip::Array<double> Bulow(3,bulow);
   std::cout<<"Bulow ="<<std::endl;
   std::cout<<Bulow<<std::endl;
   slip::unit_upper_triangular_solve(Aulow,Xulow,Bulow);
   std::cout<<"Xulow = "<<Xulow<<std::endl;
   slip::unit_upper_triangular_solve(Aulow.upper_left(),Aulow.bottom_right(),
				     Xulow.begin(),Xulow.end(),
				     Bulow.begin(),Bulow.end());
   std::cout<<"Xulow = "<<Xulow<<std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::Array<float> Bulow2(3);
   slip::matrix_vector_multiplies(Aulow,Xulow,Bulow2);
   std::cout<<"Aulow * Xulow = "<<std::endl;
   std::cout<<Bulow2<<std::endl;

   /////////////////////////////////////////////////////////////
   //  test unit diagonal solve algorithm
   /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "diagonal solve" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   double diagonal[] = {1.0,2.0,4.0};
   slip::Array<double> Ddiag(3,diagonal);
   std::cout<<"Ddiag ="<<std::endl;
   std::cout<<Ddiag<<std::endl;
   slip::Array<double> Xdiag(3);
   double bdiag[] = {6.0,2.0,5.0};
   slip::Array<double> Bdiag(3,bdiag);
   std::cout<<"Bdiag ="<<std::endl;
   std::cout<<Bdiag<<std::endl;
   slip::diagonal_solve(Ddiag.begin(),Ddiag.end(),
			Xdiag.begin(),Xdiag.end(),
			Bdiag.begin(),Bdiag.end(),1.0E-6);
   std::cout<<"Xdiag = "<<Xdiag<<std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::Array<float> Bdiag2(3);
   slip::Array2d<float> Adiag(3,3);
   slip::set_diagonal(Ddiag.begin(),Ddiag.end(),Adiag);
   std::cout<<"Adiag ="<<std::endl;
   std::cout<<Adiag<<std::endl;
   slip::matrix_vector_multiplies(Adiag,Xdiag,Bdiag2);
   std::cout<<"Adiag * Xdiag = "<<std::endl;
   std::cout<<Bdiag2<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "diagonal solve complex " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::complex<double> diagonalc[] = {TC(1.0),TC(2.0),TC(4.0)};
   slip::Array<std::complex<double> > Ddiagc(3,diagonalc);
   std::cout<<"Ddiagc ="<<std::endl;
   std::cout<<Ddiagc<<std::endl;
   slip::Array<std::complex<double> > Xdiagc(3);
   std::complex<double> bdiagc[] = {TC(6.0),TC(2.0),TC(5.0)};
   slip::Array<std::complex<double> > Bdiagc(3,bdiagc);
   std::cout<<"Bdiagc ="<<std::endl;
   std::cout<<Bdiagc<<std::endl;
   slip::diagonal_solve(Ddiagc.begin(),Ddiagc.end(),
			Xdiagc.begin(),Xdiagc.end(),
			Bdiagc.begin(),Bdiagc.end(),1.0E-6);
   std::cout<<"Xdiagc = "<<Xdiagc<<std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::Array<std::complex<double> > Bdiag2c(3);
   slip::Array2d<std::complex<double> > Adiagc(3,3);
   slip::set_diagonal(Ddiagc.begin(),Ddiagc.end(),Adiagc);
   std::cout<<"Adiagc ="<<std::endl;
   std::cout<<Adiagc<<std::endl;
   slip::matrix_vector_multiplies(Adiagc,Xdiagc,Bdiag2c);
   std::cout<<"Adiagc * Xdiagc = "<<std::endl;
   std::cout<<Bdiag2c<<std::endl;


  /////////////////////////////////////////////////////////////
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "LDL^T decomposition" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  double dsym[] = {10.0, 20.0, 30.0, 20.0, 45.0, 80.0, 30.0, 80.0, 171.0};
  slip::Array2d<double> Asym(3,3,dsym);
  std::cout<<"Asym ="<<std::endl;
  std::cout<<Asym<<std::endl;
  slip::Array2d<double> Lsym(Asym.dim1(),Asym.dim2());
  slip::Array2d<double> LTsym(Asym.dim2(),Asym.dim1());
  slip::Array<double> Dsym(Asym.rows());
  LDLT_decomposition(Asym,Lsym,Dsym,LTsym);
  std::cout<<"Lsym ="<<std::endl;
  std::cout<<Lsym<<std::endl;
  std::cout<<"Dsym ="<<std::endl;
  std::cout<<Dsym<<std::endl;
  std::cout<<"LTsym ="<<std::endl;
  std::cout<<LTsym<<std::endl;
  slip::Array2d<double> Asym2(3,3);
  slip::Array2d<double> DiagTmp(3,3);
  slip::Array2d<double> Tmp(3,3);
  slip::set_diagonal(Dsym.begin(),Dsym.end(),DiagTmp);
  slip::matrix_matrix_multiplies(Lsym,DiagTmp,Tmp);
  slip::matrix_matrix_multiplies(Tmp,LTsym,Asym2);
  std::cout << "LDL^T = " << std::endl;
  std::cout<<Asym2<<std::endl;
  //
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "LDL^T solve" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"Asym ="<<std::endl;
  std::cout<<Asym<<std::endl;
  float bldlt[] = {1.0,2.0,3.0};
  slip::Array<float> Bldlt(3,bldlt);
  std::cout<<"Bldlt = "<<std::endl;
  std::cout<<Bldlt<<std::endl;
  slip::Array<float> Xldlt(3);
  slip::LDLT_solve(Asym,Xldlt,Bldlt);
  std::cout<<"Xldlt = "<<std::endl;
  std::cout<<Xldlt<<std::endl;
  slip::Array<float> Bldlt2(3);
  slip::matrix_vector_multiplies(Asym,Xldlt,Bldlt2);
  std::cout<<"Asym Xldlt = "<<std::endl;
  std::cout<<Bldlt2<<std::endl;
  
  //
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "LDL^T  inplace decomposition" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::LDLT_decomposition(Asym);
  std::cout<<"Asym = LDLT = "<<std::endl;
  std::cout<<Asym<<std::endl;




   ///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "cholesky decomposition " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   // float dchol[] = {100.0,20.0,30.0,40.0,
// 		    20.0,50.0,45.0,80.0,
// 		    30.0,45.0,171.0,12.0,
//                     40.0,80.0,12.0,56.0};
   //slip::Array2d<float> Chol(4,4,dchol);
   slip::Array2d<float> Chol(4,4);
   slip::hilbert(Chol);
   std::cout<<"Chol = "<<std::endl;
   std::cout<<Chol<<std::endl;
   slip::Array2d<float> LChol(4,4);
   slip::Array2d<float> LCholT(4,4);
   slip::cholesky_real(Chol,LChol,LCholT);
   std::cout<<"LChol = "<<std::endl;
   std::cout<<LChol<<std::endl;
   std::cout<<"LChol^T = "<<std::endl;
   std::cout<<LCholT<<std::endl;
   slip::Array2d<float> LCholxLCholT(4,4);
   slip::matrix_matrix_multiplies(LChol,LCholT,LCholxLCholT);
   std::cout<<"LCholxLChol^T  = "<<std::endl;
   std::cout<<LCholxLCholT<<std::endl;

  
  
   slip::cholesky_real(Chol.upper_left(),Chol.bottom_right(),
				     LChol.upper_left(),LChol.bottom_right(),
				     LCholT.upper_left(),LCholT.bottom_right());

   std::cout<<"LChol = "<<std::endl;
   std::cout<<LChol<<std::endl;
   std::cout<<"LChol^T = "<<std::endl;
   std::cout<<LCholT<<std::endl;
   slip::matrix_matrix_multiplies(LChol,LCholT,LCholxLCholT);
   std::cout<<"LCholxLChol^T  = "<<std::endl;
   std::cout<<LCholxLCholT<<std::endl;

   slip::cholesky_real(Chol.upper_left(),Chol.bottom_right());

   std::cout<<"Chol = "<<std::endl;
   std::cout<<Chol<<std::endl;


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "cholesky solve " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::hilbert(Chol);
   std::cout<<"Chol = "<<std::endl;
   std::cout<<Chol<<std::endl;
   float bchol[] = {1.0,2.0,3.0,4.0};
   slip::Array<float> BChol(4,bchol);
   std::cout<<"BChol = "<<std::endl;
   std::cout<<BChol<<std::endl;
   slip::Array<float> XChol(4);
   slip::cholesky_solve(Chol,XChol,BChol);
   std::cout<<"XChol = "<<std::endl;
   std::cout<<XChol<<std::endl;
   slip::Array<float> BChol2(4);
   slip::matrix_vector_multiplies(Chol,XChol,BChol2);
   std::cout<<"Chol XChol = "<<std::endl;
   std::cout<<BChol2<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "cholesky inverse " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::hilbert(Chol);
   std::cout<<"Chol = \n"<<Chol<<std::endl;
   slip::Array2d<float> invChol(4,4);
   slip::cholesky_inv(Chol.upper_left(),Chol.bottom_right(),
		      invChol.upper_left(),invChol.bottom_right(),
		      1.0e-10);
   std::cout<<"invChol = \n"<<Chol<<std::endl;
   slip::Array2d<float> IChol(4,4);
   std::cout<<"Chol invChol = "<<std::endl;
   slip::matrix_matrix_multiplies(Chol,invChol,IChol);
   std::cout<<IChol<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "cholesky decomposition complex " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::complex<double> dcholc[] = {TC(2.0,0.0),TC(1.0,1.0),TC(0.0,2.0),
				   TC(1.0,-1.0),TC(5.0,0.0),TC(-3.0,0.0),
				   TC(0.0,-2.0),TC(-3.0,0.0),TC(10.0,0.0)};
   
   slip::Array2d<std::complex<double> > Cholc(3,3,dcholc);
   std::cout<<"Cholc = "<<std::endl;
   std::cout<<Cholc<<std::endl;
   slip::Array2d<std::complex<double> > LCholc(3,3);
   slip::Array2d<std::complex<double> > LCholTc(3,3);
   slip::cholesky_cplx(Cholc,LCholc,LCholTc);
   std::cout<<"LCholc = "<<std::endl;
   std::cout<<LCholc<<std::endl;
   std::cout<<"LChol^Tc = "<<std::endl;
   std::cout<<LCholTc<<std::endl;
   slip::Array2d<std::complex<double> > LCholxLCholTc(3,3);
   slip::matrix_matrix_multiplies(LCholc,LCholTc,LCholxLCholTc);
   std::cout<<"LCholxLChol^Tc  = "<<std::endl;
   std::cout<<LCholxLCholTc<<std::endl;
   LCholc.fill(0.0);
   LCholTc.fill(0.0);
   std::cout<<"Cholc = "<<std::endl;
   std::cout<<Cholc<<std::endl;
   slip::cholesky_cplx(Cholc.upper_left(),Cholc.bottom_right(),
				     LCholc.upper_left(),LCholc.bottom_right(),
				     LCholTc.upper_left(),LCholTc.bottom_right());
   
   std::cout<<"LCholc = "<<std::endl;
   std::cout<<LCholc<<std::endl;
   std::cout<<"LChol^Tc = "<<std::endl;
   std::cout<<LCholTc<<std::endl;
   slip::matrix_matrix_multiplies(LCholc,LCholTc,LCholxLCholTc);
   
   std::cout<<"LCholxLChol^Tc  = "<<std::endl;
   std::cout<<LCholxLCholTc<<std::endl;
 
   slip::cholesky_cplx(Cholc.upper_left(),Cholc.bottom_right());
   std::cout<<"Cholc = "<<std::endl;
   std::cout<<Cholc<<std::endl;
   
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "cholesky solve complex" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   Cholc.fill(dcholc,dcholc+Cholc.size());
   std::cout<<"Cholc = "<<std::endl;
   std::cout<<Cholc<<std::endl;
   std::complex<double> bcholc[] = {TC(1.0,0.2),TC(2.0,0.5),TC(3.0,0.5)};
   slip::Array<std::complex<double> > BCholc(3,bcholc);
   std::cout<<"BCholc = "<<std::endl;
   std::cout<<BCholc<<std::endl;
   slip::Array<std::complex<double> > XCholc(3);
   //   slip::cholesky_solve(Cholc,XCholc,BCholc);
   slip::cholesky_solve(Cholc.upper_left(),Cholc.bottom_right(),
			XCholc.begin(),XCholc.end(),
			BCholc.begin(),BCholc.end());
   std::cout<<"XCholc = "<<std::endl;
   std::cout<<XCholc<<std::endl;
   slip::Array<std::complex<double> > BChol2c(3);
   slip::matrix_vector_multiplies(Cholc,XCholc,BChol2c);
   std::cout<<"Cholc XCholc = "<<std::endl;
   std::cout<<BChol2c<<std::endl;

///////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "tridiagonal cholesky decomposition " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
   slip::Array2d<float> TriChol(4,4);
   slip::fill_diagonal(TriChol,2.0);
   slip::fill_diagonal(TriChol,-1.0,-1);
   slip::fill_diagonal(TriChol,-1.0,1);
   std::cout<<"TriChol = "<<std::endl;
   std::cout<<TriChol<<std::endl;
   slip::Array<float> TriChol_diag(4);
   slip::get_diagonal(TriChol,TriChol_diag.begin(),TriChol_diag.end());
   slip::Array<float> TriChol_low_diag(3);
   slip::get_diagonal(TriChol,TriChol_low_diag.begin(),TriChol_low_diag.end(),1);

    std::cout<<"TriChol_diag = "<<std::endl;
   std::cout<<TriChol_diag <<std::endl;
   std::cout<<"TriChol_low_diag = "<<std::endl;
   std::cout<<TriChol_low_diag<<std::endl;

   slip::Array<float> R_TriChol_diag(4);
   slip::Array<float> R_TriChol_low_diag(3);
 
   
   slip::tridiagonal_cholesky(TriChol_diag.begin(),TriChol_diag.end(),
			      TriChol_low_diag.begin(),TriChol_low_diag.end(),
			      R_TriChol_diag.begin(),R_TriChol_diag.end(),
			      R_TriChol_low_diag.begin(),R_TriChol_low_diag.end());
   std::cout<<"R_TriChol_diag = "<<std::endl;
   std::cout<<R_TriChol_diag <<std::endl;
   std::cout<<"R_TriChol_low_diag = "<<std::endl;
   std::cout<<R_TriChol_low_diag<<std::endl;

   slip::Array2d<float> LTriChol(4,4);
   slip::Array2d<float> LTriCholT(4,4);
   slip::set_diagonal(R_TriChol_diag.begin(),R_TriChol_diag.end(),LTriChol);
   slip::set_diagonal(R_TriChol_low_diag.begin(),R_TriChol_low_diag.end(),LTriChol,-1);
   slip::transpose(LTriChol,LTriCholT);
   std::cout<<"LTriChol = "<<std::endl;
   std::cout<<LTriChol <<std::endl;
   std::cout<<"LTriCholT = "<<std::endl;
   std::cout<<LTriCholT<<std::endl;

   slip::Array2d<float> LTriCholxLTriCholT(4,4);
   slip::matrix_matrix_multiplies(LTriChol,LTriCholT,LTriCholxLTriCholT);
   std::cout<<"LTriCholxLTriChol^T  = "<<std::endl;
   std::cout<<LTriCholxLTriCholT<<std::endl;

   
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "tridiagonal cholesky solve " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"TriChol = "<<std::endl;
   std::cout<<TriChol<<std::endl;
   float btricol[] = {1.0,2.0,3.0,4.0};
   slip::Array<float> BTriChol(4,btricol);
   std::cout<<"BTriChol = \n"<<BTriChol<<std::endl;
   slip::Array<float> XTriChol(4);
   slip::tridiagonal_cholesky_solve(TriChol_diag.begin(),TriChol_diag.end(),
				    TriChol_low_diag.begin(),TriChol_low_diag.end(),
				    XTriChol.begin(),XTriChol.end(),
				    BTriChol.begin(),BTriChol.end());
   std::cout<<"XTriChol = \n"<<XTriChol<<std::endl;
   std::cout<<"Check: "<<std::endl;
   slip::Array<float> BTriChol2(4);
   slip::matrix_vector_multiplies(TriChol,XTriChol,BTriChol2);
    std::cout<<"BTriChol2 = \n"<<BTriChol2<<std::endl;
    slip::tridiagonal_cholesky_solve(TriChol,XTriChol,BTriChol);
   std::cout<<"XTriChol = \n"<<XTriChol<<std::endl;
   std::cout<<"Check: "<<std::endl;
   slip::matrix_vector_multiplies(TriChol,XTriChol,BTriChol2);
    std::cout<<"BTriChol2 = \n"<<BTriChol2<<std::endl;
   ///////////////

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "tridiagonal cholesky decomposition complex " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
   slip::Array2d<std::complex<float> > TriCholc(4,4);
   slip::fill_diagonal(TriCholc,std::complex<float>(2.0,0.0));
   slip::fill_diagonal(TriCholc,std::complex<float>(-1.0,0.5),-1);
   slip::fill_diagonal(TriCholc,std::complex<float>(-1.0,-0.5),1);
    std::cout<<"TriCholc = "<<std::endl;
   std::cout<<TriCholc<<std::endl;
   slip::Array<std::complex<float> > TriChol_diagc(4);
   slip::get_diagonal(TriCholc,TriChol_diagc.begin(),TriChol_diagc.end());
   slip::Array<std::complex<float> > TriChol_low_diagc(3);
   slip::get_diagonal(TriCholc,TriChol_low_diagc.begin(),TriChol_low_diagc.end(),1);

    std::cout<<"TriChol_diagc = "<<std::endl;
   std::cout<<TriChol_diagc <<std::endl;
   std::cout<<"TriChol_low_diagc = "<<std::endl;
   std::cout<<TriChol_low_diagc<<std::endl;

   slip::Array<std::complex<float> > R_TriChol_diagc(4);
   slip::Array<std::complex<float> > R_TriChol_low_diagc(3);
 
   
   slip::tridiagonal_cholesky(TriChol_diagc.begin(),TriChol_diagc.end(),
			      TriChol_low_diagc.begin(),TriChol_low_diagc.end(),
			      R_TriChol_diagc.begin(),R_TriChol_diagc.end(),
			      R_TriChol_low_diagc.begin(),R_TriChol_low_diagc.end());
   std::cout<<"R_TriChol_diagc = "<<std::endl;
   std::cout<<R_TriChol_diagc <<std::endl;
   std::cout<<"R_TriChol_low_diagc = "<<std::endl;
   std::cout<<R_TriChol_low_diagc<<std::endl;

   slip::Array2d<std::complex<float> > LTriCholc(4,4);
   slip::Array2d<std::complex<float> > LTriCholTc(4,4);
   slip::set_diagonal(R_TriChol_diagc.begin(),R_TriChol_diagc.end(),LTriCholc);
   slip::set_diagonal(R_TriChol_low_diagc.begin(),R_TriChol_low_diagc.end(),LTriCholc,-1);
   slip::hermitian_transpose(LTriCholc,LTriCholTc);
   std::cout<<"LTriCholc = "<<std::endl;
   std::cout<<LTriCholc <<std::endl;
   std::cout<<"LTriCholTc = "<<std::endl;
   std::cout<<LTriCholTc<<std::endl;

   slip::Array2d<std::complex<float> > LTriCholxLTriCholTc(4,4);
   slip::matrix_matrix_multiplies(LTriCholc,LTriCholTc,LTriCholxLTriCholTc);
   std::cout<<"LTriCholxLTriChol^Tc  = "<<std::endl;
   std::cout<<LTriCholxLTriCholTc<<std::endl;


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "tridiagonal cholesky solve complex " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"TriCholc = "<<std::endl;
   std::cout<<TriCholc<<std::endl;
   std::complex<float> btricolc[] = {std::complex<float>(1.0),std::complex<float>(2.0),std::complex<float>(3.0),std::complex<float>(4.0)};
   //std::complex<float> btricolc[] = {std::complex<float>(1.0),std::complex<float>(0.0),std::complex<float>(0.0),std::complex<float>(0.0)};
   slip::Array<std::complex<float> > BTriCholc(4,btricolc);
   std::cout<<"BTriCholc = \n"<<BTriCholc<<std::endl;
   slip::Array<std::complex<float> > XTriCholc(4);
   slip::tridiagonal_cholesky_solve(TriChol_diagc.begin(),TriChol_diagc.end(),
				    TriChol_low_diagc.begin(),TriChol_low_diagc.end(),
				    XTriCholc.begin(),XTriCholc.end(),
				    BTriCholc.begin(),BTriCholc.end());
   std::cout<<"XTriCholc = \n"<<XTriCholc<<std::endl;
   std::cout<<"Check: "<<std::endl;
   slip::Array<std::complex<float> > BTriChol2c(4);
   slip::matrix_vector_multiplies(TriCholc,XTriCholc,BTriChol2c);
    std::cout<<"BTriChol2c = \n"<<BTriChol2c<<std::endl;
    slip::tridiagonal_cholesky_solve(TriCholc,XTriCholc,BTriCholc);
   std::cout<<"XTriCholc = \n"<<XTriCholc<<std::endl;
   std::cout<<"Check: "<<std::endl;
   slip::matrix_vector_multiplies(TriCholc,XTriCholc,BTriChol2c);
    std::cout<<"BTriChol2c = \n"<<BTriChol2c<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "tridiagonal cholesky invert " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"TriChol  = \n"<<TriChol<<std::endl;
   slip::Matrix<float> TriCholinv (TriChol.rows(),TriChol.cols());
   slip::tridiagonal_cholesky_inv(TriChol_diag.begin(),TriChol_diag.end(),
				  TriChol_low_diag.begin(),TriChol_low_diag.end(),TriCholinv.upper_left(),TriCholinv.bottom_right());
   std::cout<<"TriCholinv  = \n"<<TriCholinv<<std::endl;
   slip::Matrix<float> TriCholTriCholinv(TriChol.rows(),TriChol.cols());
   std::cout<<"Check "<<std::endl;
   slip::matrix_matrix_multiplies(TriChol,TriCholinv,TriCholTriCholinv);
   std::cout<<"TriCholTriCholinv  = \n"<<TriCholTriCholinv<<std::endl;
   slip::tridiagonal_cholesky_inv(TriChol,TriCholinv);
   std::cout<<"TriCholinv  = \n"<<TriCholinv<<std::endl;
   std::cout<<"Check "<<std::endl;
   slip::matrix_matrix_multiplies(TriChol,TriCholinv,TriCholTriCholinv);
   std::cout<<"TriCholTriCholinv  = \n"<<TriCholTriCholinv<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "tridiagonal cholesky invert complex" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"TriCholc  = \n"<<TriCholc<<std::endl;
   slip::Matrix<std::complex<float> > TriCholinvc (TriCholc.rows(),TriCholc.cols());
   slip::tridiagonal_cholesky_inv(TriChol_diagc.begin(),TriChol_diagc.end(),
				  TriChol_low_diagc.begin(),TriChol_low_diagc.end(),TriCholinvc.upper_left(),TriCholinvc.bottom_right());
   std::cout<<"TriCholinvc  = \n"<<TriCholinvc<<std::endl;
   slip::Matrix<std::complex<float> > TriCholcTriCholinvc(TriCholc.rows(),TriCholc.cols());
   std::cout<<"Check "<<std::endl;
   slip::matrix_matrix_multiplies(TriCholc,TriCholinvc,TriCholcTriCholinvc);
   std::cout<<"TriCholcTriCholinvc  = \n"<<TriCholcTriCholinvc<<std::endl;
   slip::tridiagonal_cholesky_inv(TriCholc,TriCholinvc);
   std::cout<<"TriCholinv  = \n"<<TriCholinvc<<std::endl;
   std::cout<<"Check "<<std::endl;
   slip::matrix_matrix_multiplies(TriCholc,TriCholinvc,TriCholcTriCholinvc);
   std::cout<<"TriCholcTriCholinvc  = \n"<<TriCholcTriCholinvc<<std::endl;

  /////////////////////////////////////////////////////////////
  //  test Thomas algorithm
  /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Thomas algorithm" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   double d[]= {1.0,2.0,3.0,4.0};
   slip::Array<double> diag(4,d);
   double dup[]= {3.0,1.0,1.0};
   slip::Array<double> diag_up(3,dup); 
   double ddown[]= {2.0,2.0,2.0};
   slip::Array<double> diag_down(3,ddown);
  
  slip::Array<double> X(4);
  double b[]= {4.0,2.0,3.0,1.0};
  slip::Array<double> B(4,b);
 //  std::cout<<B<<std::endl;
//   slip::thomas_solve(diag.begin(),diag.end(),
// 		     diag_up.begin(),diag_up.end(),
// 		     diag_down.begin(),diag_down.end(),
// 		     X.begin(),X.end(),
// 		     B.begin(),B.end());
 
//   std::cout<<X<<std::endl;

  
  slip::Array2d<double> D3(4,4,0.0);
  slip::set_diagonal(diag.begin(),diag.end(),D3);
  slip::set_diagonal(diag_up.begin(),diag_up.end(),D3,1);
  slip::set_diagonal(diag_down.begin(),diag_down.end(),D3,-1);
  std::cout<<"D3 = "<<std::endl;
  std::cout<<D3<<std::endl;
  std::cout<<"B = "<<std::endl;
  std::cout<<B<<std::endl;
  std::cout<<std::endl;
  slip::thomas_solve(D3,X,B);
  std::cout<<"X = "<<std::endl;
  std::cout<<X<<std::endl;

  std::cout<<std::endl;
  std::cout<<"Check:"<<std::endl;
  slip::Array<double> B2(4);
  slip::matrix_vector_multiplies(D3,X,B2);
  std::cout<<"D3 * X = "<<std::endl;
  std::cout<<B2<<std::endl;

 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "thomas algorithm for complex" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

   slip::Array2d<std::complex<double> > D3c(7,7,std::complex<double>(0.0,0.0));
  slip::set_diagonal(Dtlu_diagc.begin(),Dtlu_diagc.end(),D3c);
  slip::set_diagonal(Dtlu_up_diagc.begin(),Dtlu_up_diagc.end(),D3c,1);
  slip::set_diagonal(Dtlu_low_diagc.begin(),Dtlu_low_diagc.end(),D3c,-1);
  slip::Array<std::complex<double> > Xc(7);
  std::complex<double> bc[]= {TC(4.0,4.0),TC(2.0,2.0),TC(3.0,1.0),TC(1.0,2.0),
  TC(2.0,4.0),TC(1.0,2.0),TC(3.5,1.0)};
  slip::Array<std::complex<double> > Bc(7,bc);

  std::cout<<"D3c = "<<std::endl;
  std::cout<<D3c<<std::endl;
  std::cout<<"Bc = "<<std::endl;
  std::cout<<Bc<<std::endl;
  std::cout<<std::endl;
  slip::thomas_solve(D3c,Xc,Bc);
  std::cout<<"Xc = "<<std::endl;
  std::cout<<Xc<<std::endl;

  std::cout<<std::endl;
  std::cout<<"Check:"<<std::endl;
  slip::Array<std::complex<double> > B2c(7);
  slip::matrix_vector_multiplies(D3c,Xc,B2c);
  std::cout<<"D3c * Xc = "<<std::endl;
  std::cout<<B2c<<std::endl;


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Thomas invert" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Matrix<double> D3inv(D3.rows(),D3.cols());
   slip::thomas_inv(diag.begin(),diag.end(),
		    diag_up.begin(),diag_up.end(),
		    diag_down.begin(),diag_down.end(),
		    D3inv.upper_left(),D3inv.bottom_right());
   std::cout<<"D3inv = \n "<<D3inv<< std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::Matrix<double> D3D3inv(D3.rows(),D3.cols());
   slip::matrix_matrix_multiplies(D3,D3inv,D3D3inv);
   std::cout<<"D3D3inv = \n "<<D3D3inv<< std::endl;
   D3inv.fill(0.0);
   slip::thomas_inv(D3,D3inv);
   std::cout<<"D3inv = \n "<<D3inv<< std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::matrix_matrix_multiplies(D3,D3inv,D3D3inv);
   std::cout<<"D3D3inv = \n "<<D3D3inv<< std::endl;


    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Thomas invert complex " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Matrix<std::complex<double> > D3cinv(D3c.rows(),D3c.cols());
   slip::thomas_inv(Dtlu_diagc.begin(),Dtlu_diagc.end(),
		    Dtlu_up_diagc.begin(),Dtlu_up_diagc.end(),
		    Dtlu_low_diagc.begin(),Dtlu_low_diagc.end(),
		    D3cinv.upper_left(),D3cinv.bottom_right());
   std::cout<<"D3cinv = \n "<<D3cinv<< std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::Matrix<std::complex<double> > D3cD3cinv(D3c.rows(),D3c.cols());
   slip::matrix_matrix_multiplies(D3c,D3cinv,D3cD3cinv);
   std::cout<<"D3cD3cinv = \n "<<D3cD3cinv<< std::endl;

   slip::thomas_inv(D3c,D3cinv);
   std::cout<<"D3cinv = \n "<<D3cinv<< std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::matrix_matrix_multiplies(D3c,D3cinv,D3cD3cinv);
   std::cout<<"D3cD3cinv = \n "<<D3cD3cinv<< std::endl;
  ///////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////
  //  LU
  /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "LU decomposition " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    std::size_t size=5;  


  slip::Matrix<double> MM(size,size);
  slip::Matrix<double> L(size,size);
  slip::Matrix<double> U2(size,size);
  slip::Matrix<double> D(size,size);
  slip::Matrix<double> LU(size,size);
  slip::Matrix<double> DLU(size,size);
  slip::Matrix<double> L1(size,size);
  slip::Matrix<double> U1(size,size);
  slip::Matrix<double> D1(size,size);
  
  slip::Matrix<double> tmp1(size,size);
  slip::Matrix<double> tmp2(size,size);
  slip::Matrix<double> tmp3(size,size);

  slip::Matrix<double> MMI(size,size);
  slip::Matrix<double> testinv(size,size);

  std::generate(MM.begin(),MM.end(),std::rand);

  slip::lu(MM,L,U2,D);	

  std::cout<<std::endl;
  std::cout << MM<<std::endl;

  std::cout<<std::endl;
  std::cout <<"LLLL"<<std::endl<< L<<std::endl;

  std::cout<<std::endl;
  std::cout <<"UUU"<<std::endl<< U2<<std::endl;

  std::cout<<std::endl;
  std::cout <<"DDDD"<<std::endl<< D<<std::endl;
  slip::matrix_matrix_multiplies(L,U2,LU);
  slip::matrix_matrix_multiplies(D,LU,DLU);

  std::cout<<std::endl;
  std::cout <<"DLUDLU"<<std::endl<< DLU<<std::endl;




  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "LU inverse " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
// Inversion using LU method
  slip::inverse(L,tmp1);	
  slip::inverse(U2,tmp2);	
  slip::matrix_matrix_multiplies(tmp2,tmp1,tmp3);

  slip::inverse(D,tmp1);	
  slip::matrix_matrix_multiplies(tmp3,tmp1,MMI);
  slip::matrix_matrix_multiplies(MM,MMI,testinv);
  
 std::cout<<std::endl;
  std::cout <<"INV"<<std::endl<< testinv<<std::endl;



   // LU inversion 
  slip::Matrix<double> MM2(size,size);
  slip::Matrix<double> IMM2(size,size);
  slip::Matrix<double> LU2(size,size);
  slip::Vector<int> permut(size);
  slip::Vector<double> Bu(size);
  slip::Vector<double> Xu(size);

  slip::Matrix<double> testinv2(size,size);

  std::generate(MM2.begin(),MM2.end(),std::rand);
  std::cout<<"MM2 = "<<std::endl;
  std::cout<<MM2<<std::endl;
  slip::lu_inv(MM2,IMM2);	

  std::cout<<"IMM2 = "<<std::endl;
  std::cout<<IMM2<<std::endl;
  slip::matrix_matrix_multiplies(MM2,IMM2,testinv2);


  std::cout<<std::endl;
  std::cout <<"MM2 x IMM2 = "<<std::endl<< testinv2<<std::endl;


  slip::lu(MM2,LU2,permut);

   for (std::size_t j=0;j<size;++j)
     {
       for (std::size_t i=0;i<size;++i)
	 {
		Bu[i]=0.0;
	 }
       Bu[j] = 1.0;
       slip::lu_solve(LU2,size,size,permut,size,Bu,size,Xu,size);
       for (std::size_t i = 0; i < size; ++i) 
	 { 
	   IMM2[i][j] = Xu[i];
	 }
  }
  // Speed Test :
  // size=100 -> 0.1
  // size=300 -> 3.02
  // size=500 -> 14.24

   
   slip::matrix_matrix_multiplies(MM2,IMM2,testinv2);

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "LU determinant " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   double ddet[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0};
   slip::Matrix<double> Mdet(3,3,ddet);
   std::cout<<"Mdet"<<std::endl;
   std::cout<<Mdet<<std::endl;
   std::cout <<"lu_det(Mdet) = "<<lu_det(Mdet)<<std::endl;

  
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "LU solve " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Mdet"<<std::endl;
   std::cout<<Mdet<<std::endl;
   double bdet[] ={1.0, 2.0, 3.0};
   slip::Vector<double> Bdet(3,bdet);
   slip::Vector<double> Xdet(3);
   std::cout<<"Bdet"<<std::endl;
   std::cout<<Bdet<<std::endl;
   slip::lu_solve(Mdet,Xdet,Bdet);
   std::cout<<"Xdet"<<std::endl;
   std::cout<<Xdet<<std::endl;
   std::cout<<"Check: "<<std::endl;
   slip::Vector<double> Bdet2(3);
   slip::matrix_vector_multiplies(Mdet,Xdet,Bdet2);
   std::cout<<"Bdet2"<<std::endl;
   std::cout<<Bdet2<<std::endl;
  /////////////////////////////////////////////////////////////
  //  LU complex
  /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "LU complex decomposition " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    std::size_t sizec=5;  


    slip::Matrix<std::complex<double> > MMc(sizec,sizec);
    slip::Matrix<std::complex<double> > Lc(sizec,sizec);
    slip::Matrix<std::complex<double> > U2c(sizec,sizec);
    slip::Matrix<std::complex<double> > Dc(sizec,sizec);
    slip::Matrix<std::complex<double> > LUc(sizec,sizec);
    slip::Matrix<std::complex<double> > DLUc(sizec,sizec);
    slip::Matrix<std::complex<double> > L1c(sizec,sizec);
    slip::Matrix<std::complex<double> > U1c(sizec,sizec);
    slip::Matrix<std::complex<double> > D1c(sizec,sizec);
  
    slip::Matrix<std::complex<double> > tmp1c(sizec,sizec);
    slip::Matrix<std::complex<double> > tmp2c(sizec,sizec);
    slip::Matrix<std::complex<double> > tmp3c(sizec,sizec);

    slip::Matrix<std::complex<double> > MMIc(sizec,sizec);
    slip::Matrix<std::complex<double> > testinvc(sizec,sizec);

    //  std::generate(MMc.begin(),MMc.end(),std::rand);
    slip::iota(MMc.begin(),MMc.end(),std::complex<double>(1.0,1.0),
	       std::complex<double>(0.2,0.1));
    slip::lu(MMc,Lc,U2c,Dc);	

  std::cout<<std::endl;
  std::cout << MMc<<std::endl;

  std::cout<<std::endl;
  std::cout <<"LLLL"<<std::endl<< Lc<<std::endl;

  std::cout<<std::endl;
  std::cout <<"UUU"<<std::endl<< U2c<<std::endl;

  std::cout<<std::endl;
  std::cout <<"DDDD"<<std::endl<< Dc<<std::endl;
  slip::matrix_matrix_multiplies(Lc,U2c,LUc);
  slip::matrix_matrix_multiplies(Dc,LUc,DLUc);

  std::cout<<std::endl;
  std::cout <<"DLUDLU"<<std::endl<< DLUc<<std::endl;




 //  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//   std::cout << "LU inverse " << std::endl;
//   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
// // Inversion using LU method
//   slip::inverse(L,tmp1);	
//   slip::inverse(U2,tmp2);	
//   slip::matrix_matrix_multiplies(tmp2,tmp1,tmp3);

//   slip::inverse(D,tmp1);	
//   slip::matrix_matrix_multiplies(tmp3,tmp1,MMI);
//   slip::matrix_matrix_multiplies(MM,MMI,testinv);
  
//  std::cout<<std::endl;
//   std::cout <<"INV"<<std::endl<< testinv<<std::endl;



//    // LU inversion 
//   slip::Matrix<double> MM2(sizec,sizec);
//   slip::Matrix<double> IMM2(sizec,sizec);
//   slip::Matrix<double> LU2(sizec,sizec);
//   slip::Vector<int> permut(sizec);
//   slip::Vector<double> Bu(sizec);
//   slip::Vector<double> Xu(sizec);

//   slip::Matrix<double> testinv2(sizec,sizec);

//   std::generate(MM2.begin(),MM2.end(),std::rand);
//   std::cout<<"MM2 = "<<std::endl;
//   std::cout<<MM2<<std::endl;
//   slip::lu_inv(MM2,IMM2);	

//   std::cout<<"IMM2 = "<<std::endl;
//   std::cout<<IMM2<<std::endl;
//   slip::matrix_matrix_multiplies(MM2,IMM2,testinv2);


//   std::cout<<std::endl;
//   std::cout <<"MM2 x IMM2 = "<<std::endl<< testinv2<<std::endl;


//   slip::lu(MM2,LU2,permut);

//    for (std::sizec_t j=0;j<sizec;++j)
//      {
//        for (std::sizec_t i=0;i<sizec;++i)
// 	 {
// 		Bu[i]=0.0;
// 	 }
//        Bu[j] = 1.0;
//        slip::lu_solve(LU2,sizec,sizec,permut,sizec,Bu,sizec,Xu,sizec);
//        for (std::sizec_t i = 0; i < sizec; ++i) 
// 	 { 
// 	   IMM2[i][j] = Xu[i];
// 	 }
//   }
//   // Speed Test :
//   // sizec=100 -> 0.1
//   // sizec=300 -> 3.02
//   // sizec=500 -> 14.24

   
//    slip::matrix_matrix_multiplies(MM2,IMM2,testinv2);

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "LU determinant complex " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   TC ddetc[] ={TC(3.0), TC(7.0), TC(5.0),
	       TC(10.0), TC(7.0), TC(8.0), 
	       TC(15.0), TC(11.0), TC(12.0)};
   slip::Matrix<TC> Mdetc(3,3,ddetc);
   std::cout<<"Mdetc"<<std::endl;
   std::cout<<Mdetc<<std::endl;
   std::cout <<"lu_det(Mdetc) = "<<lu_det(Mdetc)<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "LU solve complex " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Mdetc"<<std::endl;
   std::cout<<Mdetc<<std::endl;
   TC bdetc[] ={TC(1.0), TC(2.0), TC(3.0)};
   slip::Vector<TC> Bdetc(3,bdetc);
   slip::Vector<TC> Xdetc(3);
   std::cout<<"Bdetc"<<std::endl;
   std::cout<<Bdetc<<std::endl;
   slip::lu_solve(Mdetc,Xdetc,Bdetc);
   std::cout<<"Xdetc"<<std::endl;
   std::cout<<Xdetc<<std::endl;
   std::cout<<"Check: "<<std::endl;
   slip::Vector<TC> Bdet2c(3);
   slip::matrix_vector_multiplies(Mdetc,Xdetc,Bdet2c);
   std::cout<<"Bdet2c"<<std::endl;
   std::cout<<Bdet2c<<std::endl;
   /////////////////////////////////////////////////////////////
  //  inverse complex
  /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "inverse complex" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   
   std::cout<<"MMC = \n"<<std::endl;
   std::cout << MMc<<std::endl;
   slip::Matrix<std::complex<double> > invMMc(MMc.rows(),MMc.cols());
   slip::inverse(MMc,invMMc);
   std::cout<<"inverse MMC = \n"<<std::endl;
   std::cout << invMMc<<std::endl;
   std::cout<<"verify inv(MMC)xMMC = \n"<<std::endl;
   slip::Matrix<std::complex<double> > invMMcxMMC(MMc.rows(),MMc.cols());
   slip::matrix_matrix_multiplies(invMMc,MMc,invMMcxMMC);
   std::cout<<invMMcxMMC<<std::endl;


   //******************************************************************//
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Gauss Solve" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Matrix<double> G(4,4);
  slip::hilbert(G);
  //  G(0,0) = 0.0;
  double bg[] = {22.0,7.0,4.5,1.0};
  slip::Vector<double> Bg(4,bg);
  slip::Vector<double> Xg(4);
  std::cout<< "G : " <<std::endl;
  std::cout<< G <<std::endl;
  std::cout<< "B : " <<std::endl;
  std::cout<< Bg <<std::endl;
  if(slip::gauss_solve(G,Xg,Bg,10E-6))
    std::cout << "Gauss ok!\n";
  else
    {
      std::cout << "Gauss not ok!\n";
    }
 
  std::cout<< "X : " <<std::endl;
  std::cout<< Xg <<std::endl;
  std::cout<< "Check : "<<std::endl;
  slip::Vector<double> Bg2(4);
  slip::matrix_vector_multiplies(G,Xg,Bg2);
  std::cout<< "Bg2  = G Xg: " <<std::endl;
  std::cout<< Bg2 <<std::endl;

 if(slip::gauss_solve_with_filling(G,Xg,Bg,10E-6))
    std::cout << "Gauss with filling ok!\n";
 else
    {
      std::cout << "Gauss with filling not ok!\n";
    }
  std::cout<< "G : " <<std::endl;
  std::cout<< G <<std::endl;
  std::cout<< "B : " <<std::endl;
  std::cout<< Bg <<std::endl;
  std::cout<< "X : " <<std::endl;
  std::cout<< Xg <<std::endl;
  std::cout<< "Check : "<<std::endl;
  slip::matrix_vector_multiplies(G,Xg,Bg2);
  std::cout<< "Bg2  = G Xg: " <<std::endl;
  std::cout<< Bg2 <<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Gauss Solve complex" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Matrix<std::complex<double> > Gc(4,4);
  slip::hilbert(Gc);
  //Gc(0,0) = std::complex<double>(0.0,0.0);
  std::complex<double> bgc[] = {TC(22.0),TC(7.0),TC(4.5),TC(1.0)};
  slip::Vector<std::complex<double> > Bgc(4,bgc);
  slip::Vector<std::complex<double> > Xgc(4);
  if(slip::gauss_solve(Gc,Xgc,Bgc,10E-6))
    std::cout << "Gauss ok!\n";
  else
    {
      std::cout << "Gauss not ok!\n";
    }
  std::cout<< "Gc : " <<std::endl;
  std::cout<< Gc <<std::endl;
  std::cout<< "Bc : " <<std::endl;
  std::cout<< Bgc <<std::endl;
  std::cout<< "Xc : " <<std::endl;
  std::cout<< Xgc <<std::endl;
  std::cout<< "Check : "<<std::endl;
  slip::Vector<std::complex<double> > Bg2c(4);
  slip::matrix_vector_multiplies(Gc,Xgc,Bg2c);
  std::cout<< "Bg2c  = Gc Xgc: " <<std::endl;
  std::cout<< Bg2c <<std::endl;

  //Gc(0,0) = std::complex<double>(0.0,0.0);
  if(slip::gauss_solve_with_filling(Gc,Xgc,Bgc,10E-6))
    std::cout << "Gauss with filling ok!\n";
    else
    {
      std::cout << "Gauss with filling not ok!\n";
    }
 
  std::cout<< "Gc : " <<std::endl;
  std::cout<< Gc <<std::endl;
  std::cout<< "Bc : " <<std::endl;
  std::cout<< Bgc <<std::endl;
  std::cout<< "Xc : " <<std::endl;
  std::cout<< Xgc <<std::endl;
  std::cout<< "Check : "<<std::endl;
  slip::matrix_vector_multiplies(Gc,Xgc,Bg2c);
  std::cout<< "Bg2c  = Gc Xgc: " <<std::endl;
  std::cout<< Bg2c <<std::endl;


  //********************************************************************//

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "upper_band solve " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> Guband(5,5);
  slip::hilbert(Guband);
  slip::fill_diagonal(Guband,0.0,-1);
  slip::fill_diagonal(Guband,0.0,-2);
  slip::fill_diagonal(Guband,0.0,-3);
  slip::fill_diagonal(Guband,0.0,-4);
  slip::fill_diagonal(Guband,0.0,4);
  slip::fill_diagonal(Guband,0.0,3);

  double buband[] = {22.0,7.0,4.5,1.0,2.0};
  slip::Vector<double> Buband(5,buband);
  slip::Vector<double> Xuband(5);
  std::cout<< "Guband : " <<std::endl;
  std::cout<< Guband <<std::endl;
  std::cout<< "Buband : " <<std::endl;
  std::cout<< Buband <<std::endl;
  slip::upper_band_solve(Guband.upper_left(),Guband.bottom_right(),
			 2,
			 Xuband.begin(),Xuband.end(),
			 Buband.begin(),Buband.end(),1.0e-06);
  std::cout<< "Xuband : \n" <<Xuband<<std::endl;
  slip::Vector<double> Buband2(5);
  std::cout<< "Check: "<<std::endl;
  slip::matrix_vector_multiplies(Guband,Xuband,Buband2);
  std::cout<< "Buband2 : \n" <<Buband2<<std::endl;


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "unit upper_band solve " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> Guuband(5,5);
  slip::hilbert(Guuband);
  slip::fill_diagonal(Guuband,0.0,-1);
  slip::fill_diagonal(Guuband,0.0,-2);
  slip::fill_diagonal(Guuband,0.0,-3);
  slip::fill_diagonal(Guuband,0.0,-4);
  slip::fill_diagonal(Guuband,0.0,4);
  slip::fill_diagonal(Guuband,0.0,3);
  slip::fill_diagonal(Guuband,1.0);

  double buuband[] = {22.0,7.0,4.5,1.0,2.0};
  slip::Vector<double> Buuband(5,buuband);
  slip::Vector<double> Xuuband(5);
  std::cout<< "Guuband : " <<std::endl;
  std::cout<< Guuband <<std::endl;
  std::cout<< "Buuband : " <<std::endl;
  std::cout<< Buuband <<std::endl;
  slip::upper_band_solve(Guuband.upper_left(),Guuband.bottom_right(),
			 2,
			 Xuuband.begin(),Xuuband.end(),
			 Buuband.begin(),Buuband.end(),1.0e-06);
  std::cout<< "Xuuband : \n" <<Xuuband<<std::endl;
  slip::Vector<double> Buuband2(5);
  std::cout<< "Check: "<<std::endl;
  slip::matrix_vector_multiplies(Guuband,Xuuband,Buuband2);
  std::cout<< "Buuband2 : \n" <<Buuband2<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "lower_band solve " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> Glband(5,5);
  slip::hilbert(Glband);
  slip::fill_diagonal(Glband,0.0,1);
  slip::fill_diagonal(Glband,0.0,2);
  slip::fill_diagonal(Glband,0.0,3);
  slip::fill_diagonal(Glband,0.0,4);
  slip::fill_diagonal(Glband,0.0,-4);
  slip::fill_diagonal(Glband,0.0,-3);

  double blband[] = {22.0,7.0,4.5,1.0,2.0};
  slip::Vector<double> Blband(5,blband);
  slip::Vector<double> Xlband(5);
  std::cout<< "Glband : " <<std::endl;
  std::cout<< Glband <<std::endl;
  std::cout<< "Blband : " <<std::endl;
  std::cout<< Blband <<std::endl;
  slip::lower_band_solve(Glband.upper_left(),Glband.bottom_right(),
			 2,
			 Xlband.begin(),Xlband.end(),
			 Blband.begin(),Blband.end(),1.0e-06);
  std::cout<< "Xlband : \n" <<Xlband<<std::endl;
  slip::Vector<double> Blband2(5);
  std::cout<< "Check: "<<std::endl;
  slip::matrix_vector_multiplies(Glband,Xlband,Blband2);
  std::cout<< "Blband2 : \n" <<Blband2<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "unit lower_band solve " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> Gulband(5,5);
  slip::hilbert(Gulband);
  slip::fill_diagonal(Gulband,0.0,1);
  slip::fill_diagonal(Gulband,0.0,2);
  slip::fill_diagonal(Gulband,0.0,3);
  slip::fill_diagonal(Gulband,0.0,4);
  slip::fill_diagonal(Gulband,0.0,-4);
  slip::fill_diagonal(Gulband,0.0,-3);
  slip::fill_diagonal(Gulband,1.0);
  double bulband[] = {22.0,7.0,4.5,1.0,2.0};
  slip::Vector<double> Bulband(5,bulband);
  slip::Vector<double> Xulband(5);
  std::cout<< "Gulband : " <<std::endl;
  std::cout<< Gulband <<std::endl;
  std::cout<< "Bulband : " <<std::endl;
  std::cout<< Bulband <<std::endl;
  slip::unit_lower_band_solve(Gulband.upper_left(),Gulband.bottom_right(),
			 2,
			 Xulband.begin(),Xulband.end(),
			 Bulband.begin(),Bulband.end());
  std::cout<< "Xulband : \n" <<Xulband<<std::endl;
  slip::Vector<double> Bulband2(5);
  std::cout<< "Check: "<<std::endl;
  slip::matrix_vector_multiplies(Gulband,Xulband,Bulband2);
  std::cout<< "Bulband2 : \n" <<Bulband2<<std::endl;

/////////////////////////////////////////////////////////////
   //  test band LU
   /////////////////////////////////////////////////////////////
   std::cout << std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Band LU" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Array2d<double> Aband(5,5);
   slip::fill_diagonal(Aband,1.0,0);
   slip::fill_diagonal(Aband,2.0,1);
   slip::fill_diagonal(Aband,-2.0,-1);
   slip::fill_diagonal(Aband,3.0,2);
   slip::fill_diagonal(Aband,-3.0,-2);
   slip::fill_diagonal(Aband,-4.0,-3);
   
   std::cout<<"Aband = \n"<<Aband<<std::endl;
   slip::Array2d<double> AbandL(5,5);
   slip::Array2d<double> AbandU(5,5);
   slip::Array2d<double> Pband(5,5);
   slip::Array<int> Indxband(5);
   slip::band_lu(Aband,
		 3,2,
		 AbandL,
		 AbandU,
		 Pband,
		 1.0E-6);
   std::cout<<"Pband = \n"<<Pband<<std::endl;
   std::cout<<"AbandL = \n"<<AbandL<<std::endl;
   std::cout<<"AbandU = \n"<<AbandU<<std::endl;
   slip::Array2d<double> PxAbandL(5,5);
   slip::matrix_matrix_multiplies(Pband,AbandL,PxAbandL);
   slip::Array2d<double> PxAbandLxAbandU(5,5);
   slip::matrix_matrix_multiplies(PxAbandL,AbandU, PxAbandLxAbandU);
    std::cout<<"PxAbandLxAbandU = \n"<<PxAbandLxAbandU<<std::endl;
   
   //  std::cout << std::setfill('_') << std::setw(60) << '_' << std::endl;
//    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//    std::cout << "Band LU solve" << std::endl;
//    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//    double bband[] = {1.0,2.0,3.0,4.0,5.0};
//    slip::Array<double> Bband(5,bband);
//    std::cout<<"Aband = \n"<<Aband<<std::endl;
//    std::cout<<"Bband = \n"<<Bband<<std::endl;
//    slip::Array<double> Xband(5,bband);
//    slip::band_lu_solve(Aband.upper_left(),Aband.bottom_right(),
// 		       3,2,
// 		       Xband.begin(),Xband.end(),
// 		       Bband.begin(),Bband.end());
//     std::cout<<"Xband = \n"<<Xband<<std::endl;

//     std::cout<<"Check = \n"<<std::endl;
//     slip::Array<double> Bband2(5);
//     slip::matrix_vector_multiplies(Aband,Xband,Bband2);
//     std::cout<<"Bband2 = \n"<<Bband2<<std::endl;

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "lower bidiagonal invert" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    double lb_diag[] = {1.0,2.0,3.0,4.0};
    double lb_low[]  = {2.0,2.0,1.0};
    
    slip::Vector<double> LB_diag(4,lb_diag);
    slip::Vector<double> LB_low(3,lb_low);
    slip::Matrix<double> LB(LB_diag.size(),LB_diag.size());
    slip::set_diagonal(LB_diag.begin(),LB_diag.end(),LB);
    slip::set_diagonal(LB_low.begin(),LB_low.end(),LB,-1);
    std::cout<<"LB = \n"<<LB<<std::endl;
    slip::Matrix<double> LBinv(LB.rows(),LB.cols());
    slip::lower_bidiagonal_inv(LB_diag.begin(),LB_diag.end(),
				LB_low.begin(),LB_low.end(),
				LBinv.upper_left(),LBinv.bottom_right());
    std::cout<<"LBinv = \n"<<LBinv<< std::endl;
    std::cout<<"Check:"<<std::endl;
    slip::Matrix<double> LBLBinv(LB.rows(),LB.cols());
    slip::matrix_matrix_multiplies(LB,LBinv,LBLBinv);
    std::cout<<"LBLBinv = \n"<<LBLBinv<< std::endl;
   LBinv.fill(0.0);
   slip::lower_bidiagonal_inv(LB,LBinv);
   std::cout<<"LBinv = \n"<<LBinv<< std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::matrix_matrix_multiplies(LB,LBinv,LBLBinv);
   std::cout<<"LBLBinv = \n"<<LBLBinv<< std::endl;


    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "complex lower bidiagonal invert" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::complex<double> lb_diagc[] = {TC(1.0),TC(2.0),TC(3.0),TC(4.0)};
    std::complex<double> lb_lowc[]  = {TC(2.0),TC(2.0),TC(1.0)};
    
    slip::Vector<std::complex<double> > LB_diagc(4,lb_diagc);
    slip::Vector<std::complex<double> > LB_lowc(3,lb_lowc);
    slip::Matrix<std::complex<double> > LBc(LB_diagc.size(),LB_diagc.size());
    slip::set_diagonal(LB_diagc.begin(),LB_diagc.end(),LBc);
    slip::set_diagonal(LB_lowc.begin(),LB_lowc.end(),LBc,-1);
    std::cout<<"LBc = \n"<<LBc<<std::endl;
    slip::Matrix<std::complex<double> > LBinvc(LBc.rows(),LBc.cols());
    slip::lower_bidiagonal_inv(LB_diagc.begin(),LB_diagc.end(),
				LB_lowc.begin(),LB_lowc.end(),
				LBinvc.upper_left(),LBinvc.bottom_right());
    std::cout<<"LBinvc = \n"<<LBinvc<< std::endl;
    std::cout<<"Check:"<<std::endl;
    slip::Matrix<std::complex<double> > LBLBinvc(LBc.rows(),LBc.cols());
    slip::matrix_matrix_multiplies(LBc,LBinvc,LBLBinvc);
    std::cout<<"LBLBinvc = \n"<<LBLBinvc<< std::endl;
   LBinvc.fill(0.0);
   slip::lower_bidiagonal_inv(LBc,LBinvc);
   std::cout<<"LBinvc = \n"<<LBinvc<< std::endl;
   std::cout<<"Check:"<<std::endl;
   slip::matrix_matrix_multiplies(LBc,LBinvc,LBLBinvc);
   std::cout<<"LBLBinvc = \n"<<LBLBinvc<< std::endl;


 


    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "upper bidiagonal invert" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    double ub_diag[] = {1.0,2.0,3.0,4.0};
    double ub_up[]  = {2.0,2.0,1.0};
    
    slip::Vector<double> UB_diag(4,ub_diag);
    slip::Vector<double> UB_up(3,ub_up);
    slip::Matrix<double> UB(UB_diag.size(),UB_diag.size());
    slip::set_diagonal(UB_diag.begin(),UB_diag.end(),UB);
    slip::set_diagonal(UB_up.begin(),UB_up.end(),UB,1);
    std::cout<<"UB = \n"<<UB<<std::endl;
    slip::Matrix<double> UBinv(UB.rows(),UB.cols());
    slip::upper_bidiagonal_inv(UB_diag.begin(),UB_diag.end(),
				UB_up.begin(),UB_up.end(),
				UBinv.upper_left(),UBinv.bottom_right());
    std::cout<<"UBinv = \n"<<UBinv<< std::endl;
    std::cout<<"Check:"<<std::endl;
    slip::Matrix<double> UBUBinv(UB.rows(),UB.cols());
    slip::matrix_matrix_multiplies(UB,UBinv,UBUBinv);
    std::cout<<"UBUBinv = \n"<<UBUBinv<< std::endl;
    UBinv.fill(0.0);
    slip::upper_bidiagonal_inv(UB,UBinv);
    std::cout<<"UBinv = \n"<<UBinv<< std::endl;
    std::cout<<"Check:"<<std::endl;
    slip::matrix_matrix_multiplies(UB,UBinv,UBUBinv);
    std::cout<<"UBUBinv = \n"<<UBUBinv<< std::endl;

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "complex upper bidiagonal invert" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::complex<double> ub_diagc[] = {TC(1.0),TC(2.0),TC(3.0),TC(4.0)};
    std::complex<double> ub_upc[]  = {TC(2.0),TC(2.0),TC(1.0)};
    
    slip::Vector<std::complex<double> > UB_diagc(4,ub_diagc);
    slip::Vector<std::complex<double> > UB_upc(3,ub_upc);
    slip::Matrix<std::complex<double> > UBc(UB_diagc.size(),UB_diagc.size());
    slip::set_diagonal(UB_diagc.begin(),UB_diagc.end(),UBc);
    slip::set_diagonal(UB_upc.begin(),UB_upc.end(),UBc,1);
    std::cout<<"UBc = \n"<<UBc<<std::endl;
    slip::Matrix<std::complex<double> > UBinvc(UBc.rows(),UBc.cols());
    slip::upper_bidiagonal_inv(UB_diagc.begin(),UB_diagc.end(),
				UB_upc.begin(),UB_upc.end(),
				UBinvc.upper_left(),UBinvc.bottom_right());
    std::cout<<"UBinvc = \n"<<UBinvc<< std::endl;
    std::cout<<"Check:"<<std::endl;
    slip::Matrix<std::complex<double> > UBUBinvc(UBc.rows(),UBc.cols());
    slip::matrix_matrix_multiplies(UBc,UBinvc,UBUBinvc);
    std::cout<<"UBUBinvc = \n"<<UBUBinvc<< std::endl;
    UBinvc.fill(0.0);
    slip::upper_bidiagonal_inv(UBc,UBinvc);
    std::cout<<"UBinvc = \n"<<UBinvc<< std::endl;
    std::cout<<"Check:"<<std::endl;
    slip::matrix_matrix_multiplies(UBc,UBinvc,UBUBinvc);
    std::cout<<"UBUBinvc = \n"<<UBUBinvc<< std::endl;
  /////////////////////////////////////////////////////////////
  //  eigen values
   /////////////////////////////////////////////////////////////
//    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
//    std::cout << "Power method " << std::endl;
//    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
//    double h2[] ={15.0, -2.0, 1.0,
// 		1.0, 10.0, -3.0,
// 		-2.0, 1.0, 0.0};
//    slip::Matrix<double> Meigval1(3,3,h2);
//    std::cout<<"Meigval1"<<std::endl;
//    std::cout<<Meigval1<<std::endl;
//    slip::Vector<double> x0(Meigval1.rows());
//    slip::add_uniform_noise(x0.begin(),x0.end(),
// 			   x0.begin(),-1.0,1.0);
//    std::cout<<"x0 = "<<std::endl;
//    std::cout<<x0<<std::endl;
   
//    slip::Vector<double> eigvec(Meigval1.rows());
//    double eigval = 0.0;
//    double erreig = slip::eigen_power(Meigval1,x0,1e-06,50,eigvec,eigval);
//    std::cout<<" max eigen vector = "<<std::endl;
//    std::cout<<eigvec<<std::endl;
//    std::cout<<" max eigen value = "<<eigval<<std::endl;
//    std::cout<<" approximation error = "<<erreig<<std::endl;

//    erreig = slip::eigen_power(Meigval1,eigvec,eigval,1e-06);
//    std::cout<<" max eigen vector = "<<std::endl;
//    std::cout<<eigvec<<std::endl;
//    std::cout<<" max eigen value = "<<eigval<<std::endl;
//    std::cout<<" approximation error = "<<erreig<<std::endl;

//    slip::Matrix<std::complex<double> > Meigvalc(3,3);
//    Meigvalc[0][0] = std::complex<double>(15.0,0.0); 
//    Meigvalc[0][1] = std::complex<double>(-2.0,0.0); 
//    Meigvalc[0][2] = std::complex<double>(1.0,0.0); 
//    Meigvalc[1][0] = std::complex<double>(1.0,0.0); 
//    Meigvalc[1][1] = std::complex<double>(10.0,0.0); 
//    Meigvalc[1][2] = std::complex<double>(-3.0,0.0); 
//    Meigvalc[2][0] = std::complex<double>(-2.0,0.0); 
//    Meigvalc[2][1] = std::complex<double>(1.0,0.0); 
//    Meigvalc[2][2] = std::complex<double>(0.0,0.0); 
//    slip::Vector<std::complex<double> > eigvecc(Meigvalc.rows());
//    std::cout<<"Meigvalc"<<std::endl;
//    std::cout<<Meigvalc<<std::endl;
//    slip::Vector<double> x1(2*Meigvalc.rows());
//    slip::add_uniform_noise(x1.begin(),x1.end(),
// 			   x1.begin(),-1.0,1.0);
//    slip::Vector<std::complex<double> > xc(Meigvalc.rows());
//    for(std::size_t i = 0; i < x1.size(); i+=2)
//      {
//        xc[i/2].real() = x1[i];
//        xc[i/2].imag() = x1[i+1];
//      }
//    std::cout<<"xc = "<<std::endl;
//    std::cout<<xc<<std::endl;
//    std::complex<double> eigvalc;
//    double erreig = slip::eigen_power(Meigvalc,xc,1e-06,50,eigvecc,eigvalc);
//    std::cout<<" max eigen vector = "<<std::endl;
//    std::cout<<eigvecc<<std::endl;
//    std::cout<<" max eigen value = "<<eigvalc<<std::endl;
//    std::cout<<" approximation error = "<<erreig<<std::endl;

 
 

  /////////////////////////////////////////////////////////////
  //  Givens
  /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Givens sinus/cosinus " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   float xi = 3.1;
   float xk = 4.2;
   float s = 0.0;
   float c = 0.0;
   slip::givens_sin_cos(xi,xk,s,c);
   std::cout<<"Givens sinus   of ("<<xi<<","<<xk<<") = "<<s<<std::endl;
   std::cout<<"Givens cosinus of ("<<xi<<","<<xk<<") = "<<c<<std::endl;
  
   
   slip::Matrix<float> MGiv(5,5);
   slip::iota(MGiv.begin(),MGiv.end(),1.0);
   std::cout<<"MGiv = "<<std::endl;
   std::cout<<MGiv<<std::endl;
   slip::Matrix<float> MGiv_tmp(MGiv);
   slip::left_givens(MGiv_tmp,1,4,s,c,2,4);
   std::cout<<"Giv(1,4,atan(4.2/3.1))^T MGiv = "<<std::endl;
   std::cout<<MGiv_tmp<<std::endl;
   slip::right_givens(MGiv,1,4,s,c,2,4);
   std::cout<<"MGiv Giv(1,4,atan(4.2/3.1)) = "<<std::endl;
   std::cout<<MGiv<<std::endl;



   std::complex<float> comp;
   std::cout<<slip::complex_sign<std::complex<float> >()(comp)<<std::endl;
   std::complex<float> comp1(2,3);
   std::cout<<slip::complex_sign<std::complex<float> >()(comp1)<<std::endl;
   
   std::complex<float> comp2(1,2);
   std::cout<<slip::complex_sign<std::complex<float> >()(comp2)<<std::endl;
   std::complex<float> sc;
   float cc;
   slip::complex_givens_sin_cos(comp1,comp2,sc,cc);
   std::cout<<"Complex Givens sinus   of ("<<comp1<<","<<comp2<<") = "<<sc<<std::endl;
   std::cout<<"Complex Givens cosinus of ("<<comp1<<","<<comp2<<") = "<<cc<<std::endl;
   slip::Matrix<std::complex<float> > Mc(2,2);
   Mc[0][0] = cc; Mc[0][1] = -std::conj(sc);
   Mc[1][0] = sc; Mc[1][1] = cc;
   slip::Matrix<std::complex<float> > McH(2,2);
   slip::hermitian_transpose(Mc,McH);
   std::cout<<"Mc = "<<std::endl;
   std::cout<<Mc<<std::endl;
   std::cout<<"McH = "<<std::endl;
   std::cout<<McH<<std::endl;
   slip::Matrix<std::complex<float> > McHMc(2,2);
   slip::matrix_matrix_multiplies(McH,Mc,McHMc);
   std::cout<<"McH Mc = "<<std::endl;
   std::cout<<McHMc<<std::endl;

   std::complex<float> r1(3.1,0);
   std::complex<float> r2(4.2,0);
   std::complex<float> rsc;
   float rcc;
   slip::complex_givens_sin_cos(r1,r2,rsc,rcc);
   std::cout<<"Complex Givens sinus   of ("<<r1<<","<<r2<<") = "<<rsc<<std::endl;
   std::cout<<"Complex Givens cosinus of ("<<r1<<","<<r2<<") = "<<rcc<<std::endl;
   /////////////////////////////////////////////////////////////
  //  Householder
  /////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Householder vector" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Vector<double> Xh(5);
  slip::iota(Xh.begin(),Xh.end(),5.0,-1.0);
  std::cout<<"Xh = \n"<<Xh<<std::endl;
  slip::Vector<double> Vh(Xh.size());
  double nu  = 0.0;
  slip::housegen(Xh.begin(),Xh.end(),Vh.begin(),Vh.end(),nu);
  std::cout<<"Vh = \n"<<Vh<<std::endl;
  std::cout<<"nu = \n"<<nu<<std::endl;

  


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Complex Householder vector" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Vector<std::complex<double> > Xhc(5);
  slip::iota(Xhc.begin(),Xhc.end(),
	     std::complex<double>(5.0,5.0),
	     std::complex<double>(-1.0,-0.5));
  std::cout<<"Xhc = \n"<<Xhc<<std::endl;
  slip::Vector<std::complex<double> > Vhc(Xhc.size());
  std::complex<double> nuc  = 0.0;
  slip::housegen(Xhc.begin(),Xhc.end(),Vhc.begin(),Vhc.end(),nuc);
  std::cout<<"Vc = \n"<<Vhc<<std::endl;
  std::cout<<"nuc = \n"<<nuc<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "left Householder update " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> Mh(5,4);
  slip::iota(Mh.begin(),Mh.end(),1.0);
  std::cout<<"Mh = \n"<<Mh<<std::endl;
  slip::Vector<double> Vh2(5);
  slip::housegen(Mh.col_begin(0),Mh.col_end(0),Vh2.begin(),Vh2.end(),nu);
  std::cout<<"Vh2 = \n"<<Vh2<<std::endl;
  std::cout<<"nu = "<<nu<<std::endl;
  slip::left_householder_update(Vh2.begin(),Vh2.end(),
				Mh.upper_left(),Mh.bottom_right());
  std::cout<<"Mh = \n"<<Mh<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "complex left Householder update" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<std::complex<double> > Mhc(5,4);
  slip::iota(Mhc.begin(),Mhc.end(),
	     std::complex<double>(1.0,1.0),
	     std::complex<double>(1.0,1.0));
  std::cout<<"Mhc = \n"<<Mhc<<std::endl;
  slip::housegen(Mhc.col_begin(0),Mhc.col_end(0),Vhc.begin(),Vhc.end(),nuc);
  std::cout<<"Vhc = \n"<<Vhc<<std::endl;
  std::cout<<"nuc = "<<nuc<<std::endl;
  slip::left_householder_update(Vhc.begin(),Vhc.end(),
				Mhc.upper_left(),Mhc.bottom_right());

  slip::Matrix<std::complex<double> > Mhc2(4,4);
  slip::hilbert(Mhc2);
  std::cout<<"Mhc2 = \n"<<Mhc2<<std::endl;
  slip::Vector<std::complex<double> > Vhc2(4);
  slip::housegen(Mhc2.col_begin(0),Mhc2.col_end(0),Vhc2.begin(),Vhc2.end(),nuc);
  std::cout<<"Vhc2 = \n"<<Vhc2<<std::endl;
  std::cout<<"nuc = "<<nuc<<std::endl;
  slip::left_householder_update(Vhc2.begin(),Vhc2.end(),
				Mhc2.upper_left(),Mhc2.bottom_right());

  std::cout<<"Mhc = \n"<<Mhc<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "right Householder update" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::iota(Mh.begin(),Mh.end(),1.0);
  std::cout<<"Mh = \n"<<Mh<<std::endl;
  slip::Vector<double> Vh3(4);
  slip::housegen(Mh.row_begin(0),Mh.row_end(0),Vh3.begin(),Vh3.end(),nu);
  std::cout<<"Vh3 = \n"<<Vh3<<std::endl;
  std::cout<<"nu = "<<nu<<std::endl;
  slip::right_householder_update(Vh3.begin(),Vh3.end(),
				 Mh.upper_left(),Mh.bottom_right());
  std::cout<<"Mh = \n"<<Mh<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "complex right Householder update Stewart" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<std::complex<double> > Mh2c(4,4);
  slip::iota(Mh2c.begin(),Mh2c.end(),
	     std::complex<double>(1.0,1.0),
	     std::complex<double>(1.0,1.0));
  std::cout<<"Mh2c = \n"<<Mh2c<<std::endl;
  slip::Vector<std::complex<double> > Vh3c(Mh2c.cols());
  slip::housegen(Mh2c.row_begin(0),Mh2c.row_end(0),
		 Vh3c.begin(),Vh3c.end(),nuc);
  std::cout<<"Vh3c = \n"<<Vh3c<<std::endl;
  std::cout<<"nuc = "<<nuc<<std::endl;
  slip::right_householder_update(Vh3c.begin(),Vh3c.end(),
				 Mh2c.upper_left(),Mh2c.bottom_right());
  std::cout<<"Mh2c = \n"<<Mh2c<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Householder Hessenberg" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> HH(4,4);
  slip::hilbert(HH);
  std::cout<<"HH = \n"<<HH<<std::endl;
  slip::householder_hessenberg(HH);
  std::cout<<"HH = \n"<<HH<<std::endl;
  double ah[] = {1.0,5.0,7.0,3.0,0.0,6.0,4.0,3.0,1.0};
  slip::Matrix<double> Ah(3,3,ah);
  std::cout<<"Ah = \n"<<Ah<<std::endl;
  slip::householder_hessenberg(Ah);
  std::cout<<"Ah = \n"<<Ah<<std::endl;
  Ah.fill(ah,ah+9);
  std::cout<<"Ah = \n"<<Ah<<std::endl;
  slip::Matrix<double> Hh(3,3);
  slip::householder_hessenberg(Ah,Hh);
  std::cout<<"Hh = \n"<<Hh<<std::endl;
  
  Ah.fill(ah,ah+9);
  std::cout<<"Ah = \n"<<Ah<<std::endl;
  slip::Matrix<double> Ph(3,3);
  slip::householder_hessenberg(Ah,Hh,Ph);
  std::cout<<"Hh = \n"<<Hh<<std::endl;
  std::cout<<"Ph = \n"<<Ph<<std::endl;
  slip::Matrix<double> PhT(3,3);
  slip::transpose(Ph,PhT);
  slip::Matrix<double> PhHh(3,3);
  slip::matrix_matrix_multiplies(Ph,Hh,PhHh);
  slip::Matrix<double> PhHhPhT(3,3);
  slip::matrix_matrix_multiplies(PhHh,PhT,PhHhPhT);
  std::cout<<"Ph Hh PhT = \n"<<PhHhPhT<<std::endl;
  
  slip::hilbert(HH);
  std::cout<<"HH = \n"<<HH<<std::endl;
  slip::Matrix<double> Hhh(4,4);
  slip::Matrix<double> Phh(4,4);
  slip::householder_hessenberg(HH,Hhh,Phh);
  std::cout<<"Hhh = \n"<<Hhh<<std::endl;
  std::cout<<"Phh = \n"<<Phh<<std::endl;
  slip::Matrix<double> PhhT(4,4);
  slip::transpose(Phh,PhhT);
  slip::Matrix<double> PhhHhh(4,4);
  slip::matrix_matrix_multiplies(Phh,Hhh,PhhHhh);
  slip::Matrix<double> PhhHhhPhhT(4,4);
  slip::matrix_matrix_multiplies(PhhHhh,PhhT,PhhHhhPhhT);
  std::cout<<"Phh Hhh PhhT = \n"<<PhhHhhPhhT<<std::endl;
 


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Householder Hessenberg complex" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<std::complex<double> > Hc1(4,4);
  slip::hilbert(Hc1);
  std::cout<<"Hc1 = \n"<<Hc1<<std::endl;
  slip::householder_hessenberg(Hc1);
  std::cout<<"Hc1 = \n"<<Hc1<<std::endl;

  slip::hilbert(Hc1);
  slip::Matrix<std::complex<double> > Hhc1(Hc1.rows(),Hc1.cols());
  slip::householder_hessenberg(Hc1,Hhc1);
  std::cout<<"Hc1 = \n"<<Hc1<<std::endl;
  std::cout<<"Hhc1 = \n"<<Hhc1<<std::endl;
  slip::hilbert(Hc1);
  std::cout<<"Hc1 = \n"<<Hc1<<std::endl;
  slip::Matrix<std::complex<double> > Phc1(Hc1.rows(),Hc1.cols());
  slip::householder_hessenberg(Hc1,Hhc1,Phc1);
  std::cout<<"Hhc1 = \n"<<Hhc1<<std::endl;
  std::cout<<"Phc1 = \n"<<Phc1<<std::endl;
  slip::Matrix<std::complex<double> > Phc1Hhc1Phc1T(Hc1.rows(),Hc1.cols());
  slip::Matrix<std::complex<double> > Phc1Hhc1(Hc1.rows(),Hc1.cols());
  slip::Matrix<std::complex<double> > Phc1T(Hc1.rows(),Hc1.cols());
  slip::hermitian_transpose(Phc1,Phc1T);
  slip::matrix_matrix_multiplies(Phc1,Hhc1,Phc1Hhc1);
  slip::matrix_matrix_multiplies(Phc1Hhc1,Phc1T,Phc1Hhc1Phc1T);
  std::cout<<"Phc1 Hhc1 Phc1T = \n"<<Phc1Hhc1Phc1T<<std::endl;
  

  // slip::Matrix<double> Imag(Hc1.rows(),Hc1.cols());
//   slip::imag(Hc1,Imag);
//   std::cout<<Hc1<<std::endl;
  


   return 0;  
}

