#include <iostream>
#include <iomanip>
#include <vector>
#include <complex>
#include <cmath>
#include "integration.hpp"
#include "Array.hpp"




double fun(const double& x)
{
  //return (1 + x*x - x*x*x)*std::exp(x/5);
  //return x;
  return (1.0 + x*x - x*x*x);
  //return std::sin(x);
}

 template<typename T>
  struct Cos:public std::unary_function<T,T>
  {

    T operator()(const T& x)
    {
      return std::cos(x);
    }
    
  };

template<typename T>
  struct Sin:public std::unary_function<T,T>
  {

    T operator()(const T& x)
    {
      return std::sin(x);
    }
    
  };

template<typename T>
  struct Fun:public std::unary_function<T,T>
  {

    T operator()(const T& x)
    {
      return fun(x);
    }
    
  };

template<typename T>
  struct Fun2:public std::unary_function<T,T>
  {

    T operator()(const T& x)
    {
      return (x*x)*std::exp(-static_cast<T>(2.0)*x);
    }
    
  };

int main()
{
  typedef double T;
 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "newton cotes" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  T a = -1.0;
  T b = 3.0;
 std::size_t N = 200;
 //std::size_t N = 7;

 T good_value_fun2 = static_cast<T>(1.83177182362850);
 std::cout<<"Integration of x^2exp(-2x) on [-1.0,3.0]:"<<std::endl;
 std::cout<<"Exact result = "<<good_value_fun2<<std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"rectangle        = "<<slip::newton_cotes(a,b,N,0,Fun2<T>())<<std::endl;
  std::cout<<"trapeze          = "<<slip::newton_cotes(a,b,N,1,Fun2<T>())<<std::endl;
  std::cout<<"Simpson          = "<<slip::newton_cotes(a,b,N,2,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 3   = "<<slip::newton_cotes(a,b,N,3,Fun2<T>())<<std::endl;
  std::cout<<"Boole-Villarceau = "<<slip::newton_cotes(a,b,N,4,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 5   = "<<slip::newton_cotes(a,b,N,5,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 6   = "<<slip::newton_cotes(a,b,N,6,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 7   = "<<slip::newton_cotes(a,b,N,7,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 8   = "<<slip::newton_cotes(a,b,N,8,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 9   = "<<slip::newton_cotes(a,b,N,9,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 10  = "<<slip::newton_cotes(a,b,N,10,Fun2<T>())<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "newton cotes functors" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Rectangular<T> rect;
  slip::Trapezium<T> trapezium;
  slip::Simpson<T> simpson;
  slip::NewtonCotes3<T> newtoncotes3;
  slip::BooleVillarceau<T> boolevillarceau;
  slip::NewtonCotes5<T> newtoncotes5;
  slip::NewtonCotes6<T> newtoncotes6;
  slip::NewtonCotes7<T> newtoncotes7;
  slip::NewtonCotes8<T> newtoncotes8;
  slip::NewtonCotes9<T> newtoncotes9;
  slip::NewtonCotes10<T> newtoncotes10;
  
  
  slip::Weddle<T> weddle;
  slip::Shovelton<T> shovelton;
  
 

 
  std::cout<<"rectangular      = "<<slip::newton_cotes(a,b,N,rect,Fun2<T>())<<std::endl;
  std::cout<<"trapeze          = "<<slip::newton_cotes(a,b,N,trapezium,Fun2<T>())<<std::endl;
  std::cout<<"Simpson          = "<<slip::newton_cotes(a,b,N,simpson,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 3   = "<<slip::newton_cotes(a,b,N,newtoncotes3,Fun2<T>())<<std::endl;
  std::cout<<"Boole-Villarceau = "<<slip::newton_cotes(a,b,N,boolevillarceau,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 5   = "<<slip::newton_cotes(a,b,N,newtoncotes5,Fun2<T>())<<std::endl;
  std::cout<<"Weddle           = "<<slip::newton_cotes(a,b,N,weddle,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 6   = "<<slip::newton_cotes(a,b,N,newtoncotes6,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 7   = "<<slip::newton_cotes(a,b,N,newtoncotes7,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 8   = "<<slip::newton_cotes(a,b,N,newtoncotes8,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 9   = "<<slip::newton_cotes(a,b,N,newtoncotes9,Fun2<T>())<<std::endl;
  std::cout<<"Newton-Cotes 10  = "<<slip::newton_cotes(a,b,N,newtoncotes10,Fun2<T>())<<std::endl;
  std::cout<<"Shovelton        = "<<slip::newton_cotes(a,b,N,shovelton,Fun2<T>())<<std::endl;

  std::cout<<"rectangular support ="<<std::endl;
  std::copy(rect.cbegin(),rect.cend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"trapezium support = "<<std::endl;
  std::copy(trapezium.cbegin(),trapezium.cend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"simpson support = "<<std::endl;
  std::copy(simpson.cbegin(),simpson.cend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"newtoncotes3 support ="<<std::endl;
  std::copy(newtoncotes3.cbegin(),newtoncotes3.cend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"boolevillarceau support = "<<std::endl;
 std::copy(boolevillarceau.cbegin(),boolevillarceau.cend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"newtoncotes5 support = "<<std::endl;
  std::copy(newtoncotes5.cbegin(),newtoncotes5.cend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
 std::cout<<"weddle support = "<<std::endl;
 std::copy(weddle.cbegin(),weddle.cend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"newtoncotes6 support = "<<std::endl;
  std::copy(newtoncotes6.cbegin(),newtoncotes6.cend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"newtoncotes7 support ="<<std::endl;
  std::copy(newtoncotes7.cbegin(),newtoncotes7.cend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
 std::cout<<"newtoncotes8 support = "<<std::endl;
  std::copy(newtoncotes8.cbegin(),newtoncotes8.cend(),std::ostream_iterator<T>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"newtoncotes9 support = "<<std::endl;
 std::copy(newtoncotes9.cbegin(),newtoncotes9.cend(),std::ostream_iterator<T>(std::cout," "));
 std::cout<<std::endl;
 std::cout<<"newtoncotes10 support = "<<std::endl;
 std::copy(newtoncotes10.cbegin(),newtoncotes10.cend(),std::ostream_iterator<T>(std::cout," "));
 std::cout<<std::endl;
 std::cout<<"shovelton support = "<<std::endl;
 std::copy(shovelton.cbegin(),shovelton.cend(),std::ostream_iterator<T>(std::cout," "));
 std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "newton cotes functors" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  T h = (b-a)/static_cast<T>(N);
  slip::Array<T> xi(N+1);
  slip::iota(xi.begin(),xi.end(),a,h);
  //std::cout<<xi<<std::endl;
  slip::Array<T> yi(N+1);
  std::transform(xi.begin(),xi.end(),yi.begin(),Fun2<T>());
  slip::NewtonCotesMethod<T>* ptrnc6 = &newtoncotes6;
  std::cout<<"rectangular      = "<<slip::newton_cotes(a,b,rect,yi.begin(),yi.end())<<std::endl;
  std::cout<<"trapeze          = "<<slip::newton_cotes(a,b,trapezium,yi.begin(),yi.end())<<std::endl;
  std::cout<<"Simpson          = "<<slip::newton_cotes(a,b,simpson,yi.begin(),yi.end())<<std::endl;
  std::cout<<"Newton-Cotes 3   = "<<slip::newton_cotes(a,b,newtoncotes3,yi.begin(),yi.end())<<std::endl;
  std::cout<<"Boole-Villarceau = "<<slip::newton_cotes(a,b,boolevillarceau,yi.begin(),yi.end())<<std::endl;
  std::cout<<"Newton-Cotes 5   = "<<slip::newton_cotes(a,b,newtoncotes5,yi.begin(),yi.end())<<std::endl;
  std::cout<<"Newton-Cotes 6   = "<<slip::newton_cotes(a,b,*ptrnc6,yi.begin(),yi.end())<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Alternative Extended Simpson's rule" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::AlternativeExtendedSimpsonsRule<T> aesr8;
  std::cout<<"AESR 8   = "<<slip::alternative_extended_simpsons_rule(a,b,aesr8,yi.begin(),yi.end())<<std::endl;

  for(std::size_t n = 8; n <= N; ++n)
    {
      slip::AlternativeExtendedSimpsonsRule<T> aesr_Np1(n+1);
      std::cout<<"AESR "<<(n+1)<<"  = "<<slip::alternative_extended_simpsons_rule(a,b,aesr_Np1,yi.begin(),yi.end())<<std::endl;
    }


  
  return 0;
}
