#include <complex>
#include <iostream>
#include <iomanip>
#include <vector>

#include "FFT.hpp"
#include "Array.hpp"
#include "GrayscaleImage.hpp"
#include "Volume.hpp"
#include "dynamic.hpp"
#include "arithmetic_op.hpp"

int main()
{
  typedef std::complex<double> cx;
  cx a[] = { cx(0,0), cx(1,1), cx(3,3), cx(4,4), cx(4,4), cx(3,3),cx(1,1),cx(0,0)};
  cx b[8];
  //Test fft1d
  slip::radix2_fft(a,b,3);
  for(int i = 0; i < 8; ++i)
    {
      std::cout<<b[i]<<" ";
    }
  
  std::cout<<std::endl;
  // fft shift
  std::cout<<"fft shift..."<<std::endl;
  slip::fftshift(b,b+8);
  for(int i = 0; i < 8; ++i)
    {
      std::cout<<b[i]<<" ";
    }
  std::cout<<std::endl;
  //fft sur une array 
  slip::Array<std::complex<double> > ac(8,a);
  slip::radix2_fft(ac,b,3);
  std::cout<<ac<<std::endl<<std::endl;
  for(int i = 0; i < 8; ++i)
    {
      std::cout<<b[i]<<" ";
    }
  std::cout<<std::endl<<std::endl;
  cx b2[8];
  //fft inverse
  slip::radix2_ifft(b,b2,3);
  for(int i = 0; i < 8; ++i)
    {
      std::cout<<b2[i]<<" ";
    }
  std::cout<<std::endl<<std::endl;
  
  //fft real
  double tabd[] = {0.0,1.0,3.0,4.0,4.0,3.0,1.0,0.0};
  slip::real_radix2_fft(tabd,b,3);
  for(int i = 0; i < 8; ++i)
    {
      std::cout<<b[i]<<" ";
    }
  
  std::cout<<std::endl;
  std::cout<<std::endl;
  
  //  double tabd2[8];
  //  slip::real_radix2_ifft(b,tabd2,3);
  //   for(int i = 0; i < 8; ++i)
  //     {
  //       std::cout<<tabd2[i]<<" ";
  //     }
  
  //  std::cout<<std::endl;
  //  std::cout<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "ifftshift"<< std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  const std::size_t N_odd = 7;
  const std::size_t N_even = 8;
  slip::Array<double> Aodd(N_odd);
  slip::iota(Aodd.begin(),Aodd.end(),1.0);
  slip::Array<double> Aeven(N_even);
  slip::iota(Aeven.begin(),Aeven.end(),1.0);
  std::cout<<"Aodd = \n"<<Aodd<<std::endl;
  std::cout<<"Aeven = \n"<<Aeven<<std::endl;
  slip::fftshift(Aodd.begin(),Aodd.end());
  slip::fftshift(Aeven.begin(),Aeven.end());
  std::cout<<"Aodd = \n"<<Aodd<<std::endl;
  std::cout<<"Aeven = \n"<<Aeven<<std::endl;
  slip::iota(Aodd.begin(),Aodd.end(),1.0);
  slip::iota(Aeven.begin(),Aeven.end(),1.0);
  slip::ifftshift(Aodd.begin(),Aodd.end());
  slip::ifftshift(Aeven.begin(),Aeven.end());
  std::cout<<"Aodd = \n"<<Aodd<<std::endl;
  std::cout<<"Aeven = \n"<<Aeven<<std::endl;

  
  slip::GrayscaleImage<double> I;
  I.read("./../../../../tests/images/lena_gray.gif");
  
  slip::GrayscaleImage<double> Norm(I.dim1(),I.dim2());
  int nblig = I.dim1();
  int nbcol = I.dim2();
  std::complex<double>* I2 = new std::complex<double>[nblig * nbcol];
  std::complex<double>* I3 = new std::complex<double>[nblig*nbcol];
  
  slip::GrayscaleImage<double>::row_iterator it_row_I_begin = I.row_begin(0);
  //fft1d2d
  slip::radix2_fft1d2d(it_row_I_begin,I2,nblig,nbcol);
  
  //log(magnitude)
  for(size_t i = 0; i < I.dim1(); ++i)
    {
      for(size_t j = 0; j < I.dim2(); ++j)
	{
	  Norm[i][j] = std::log(1.0+std::abs(I2[nbcol * i + j]));
	}
    }
  slip::fftshift2d(Norm.upper_left(),Norm.bottom_right());
  slip::change_dynamic_01(Norm.begin(),Norm.end(),Norm.begin(),slip::AFFINE_FUNCTION);
  Norm.write("lena_fft1d2d.gif");
  
  //ifft1d2d
  slip::radix2_ifft1d2d(I2,I3,nblig,nbcol);
  
  double max=0;
  for(size_t i = 0; i < I.dim1(); ++i)
    {
      for(size_t j = 0; j < I.dim2(); ++j)
	{
	  if (max<std::abs(I[i][j]-std::abs(I3[nbcol * i + j]))){
	    max=std::abs(I[i][j]-std::abs(I3[nbcol * i + j]));
	  }
	}
    }
  std::cout<<"Erreur max de reconstruction : "<<max<<std::endl;
  
  for(size_t i = 0; i < I.dim1(); ++i)
    {
      for(size_t j = 0; j < I.dim2(); ++j)
	{
	  Norm[i][j] = std::abs(I3[nbcol * i + j]);
	}
    }
  
  slip::change_dynamic_01(Norm.begin(),Norm.end(),Norm.begin(),slip::AFFINE_FUNCTION);
  Norm.write("lena_fft1d2d_rec.gif");
  //std::transform(I2.begin(),I2.end(),Norm.begin(),std::ptr_fun(std::norm));

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "ifftshift2"<< std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::GrayscaleImage<double> M_odd(N_odd,N_odd);
  slip::iota(M_odd.begin(),M_odd.end(),0.0);
  std::cout<<"M_odd = \n"<<M_odd<<std::endl;
  slip::ifftshift2d(M_odd.upper_left(),M_odd.bottom_right());
  std::cout<<"shifted M_odd = \n"<<M_odd<<std::endl;
  slip::GrayscaleImage<double> M_even(N_even,N_even);
  slip::iota(M_even.begin(),M_even.end(),0.0);
  std::cout<<"M_even = \n"<<M_even<<std::endl;
  slip::ifftshift2d(M_even.upper_left(),M_even.bottom_right());
  std::cout<<"shifted M_even = \n"<<M_even<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "ifftshift3d"<< std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Volume<double> M3_odd(N_odd,N_odd,N_odd);
  slip::iota(M3_odd.begin(),M3_odd.end(),0.0);
  std::cout<<"M3_odd = \n"<<M3_odd<<std::endl;
  slip::ifftshift3d(M3_odd.front_upper_left(),M3_odd.back_bottom_right());
  std::cout<<"shifted M3_odd = \n"<<M3_odd<<std::endl;
  slip::Volume<double> M3_even(N_even,N_even,N_even);
  slip::iota(M3_even.begin(),M3_even.end(),0.0);
  std::cout<<"M3_even = \n"<<M3_even<<std::endl;
  slip::ifftshift3d(M3_even.front_upper_left(),M3_even.back_bottom_right());
  std::cout<<"shifted M3_even = \n"<<M3_even<<std::endl;
  
}
