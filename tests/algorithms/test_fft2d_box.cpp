#include <complex>
#include <iostream>
#include <vector>

#include "FFT.hpp"
#include "Array.hpp"
#include "GrayscaleImage.hpp"
#include "dynamic.hpp"

int main()
{
  slip::GrayscaleImage<double> I;
  I.read("../images/lena_gray.gif");
  
  //fft2d sur une box
  slip::Box2d<int> box(100,100,163,163);
  slip::Matrix<std::complex<double> > IBoxFFT(64,64);
  slip::Matrix<std::complex<double> > IBox(64,64);
  slip::real_fft2d(I.upper_left(box),I.bottom_right(box),IBoxFFT.upper_left());
  slip::GrayscaleImage<double> IBoxNorm(IBoxFFT.dim1(),IBoxFFT.dim2());
  //log(magnitude)
  for(size_t i = 0; i < IBoxFFT.dim1(); ++i)
    {
      for(size_t j = 0; j < IBoxFFT.dim2(); ++j)
	{
	  IBoxNorm[i][j] = std::log(1.0+std::abs(IBoxFFT[i][j]));
	}
    }
  slip::fftshift2d(IBoxNorm.upper_left(),IBoxNorm.bottom_right());
  slip::change_dynamic_01(IBoxNorm.begin(),IBoxNorm.end(),IBoxNorm.begin(),slip::AFFINE_FUNCTION);
  IBoxNorm.write("lena_fft2d_box.gif");
  
  //ifft2d sur une box
  slip::ifft2d(IBoxFFT.upper_left(),IBoxFFT.bottom_right(),IBox.upper_left());
  
  for(size_t i = 0; i < IBox.dim1(); ++i)
    {
      for(size_t j = 0; j < IBox.dim2(); ++j)
	{
	  IBoxNorm[i][j] = std::abs(IBox[i][j]);
	}
    }
  
  slip::change_dynamic_01(IBoxNorm.begin(),IBoxNorm.end(),IBoxNorm.begin(),slip::AFFINE_FUNCTION);
  IBoxNorm.write("lena_fft2d_box_rec.gif");
      
  //std::transform(I2.begin(),I2.end(),Norm.begin(),std::ptr_fun(std::norm));
}
