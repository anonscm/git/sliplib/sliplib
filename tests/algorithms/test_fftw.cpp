#include <complex>
#include <iostream>
#include <iomanip>
#include <numeric>
#include <vector>
#include <ctime>

#include "FFT.hpp"
#include "Array.hpp"
#include "GrayscaleImage.hpp"
#include "dynamic.hpp"

template <typename T>
T aleat10(void)
{
  //return 10.0 * (rand()/static_cast<T>(RAND_MAX));
  static T a = 1;
  return a++;
}

int main()
{
   time_t start_real_split, end_real_split;
   time_t start_cp_split, end_cp_split;
   time_t start_fftwr, end_fftwr;
   time_t start_fftwc, end_fftwc;
   int time_diff;
  std::srand(std::clock());
 
#ifdef HAVE_FFTW 
  const int N(9);
  bool disp("yes");
  typedef double real;
  typedef std::complex<real> cx;
  
  slip::Array<cx> b(N,cx(0));
  slip::Array<cx> bc(N,cx(0));
  slip::Array<cx> bfftwr(N,cx(0));
  slip::Array<cx> bfftwc(N,cx(0));
  
  slip::Array<cx> b2(N,cx(0));
  slip::Array<cx> bfftw2(N,cx(0));
  
  slip::Array<real> I(N,real(0));
  std::generate(I.begin(),I.end(),aleat10<real>);
  slip::Array<cx> Ic(N,cx(0));
  std::copy(I.begin(),I.end(),Ic.begin());
  
  std::cout <<  std::setfill('*') << std::setw(80) << '*' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "FFT1D Split-Radix vs fftw" << std::endl;
  std::cout <<  std::setfill('*') << std::setw(80) << '*' << std::endl;
  if(disp)
    {
      std::cout << std::endl;  
      std::cout<< "Real Signal" <<std::endl;
      std::cout << I << std::endl;  
      std::cout << std::endl;  
      std::cout<< "Complex Signal" <<std::endl;
      std::cout << Ic << std::endl; 
    }
  
  //fft
  std::cout<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Forward FFT" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  
  time(&start_real_split);
  slip::real_split_radix_fft(I.begin(),I.end(),b.begin());
  time(&end_real_split);
  time(&start_cp_split);
  
  slip::complex_split_radix_fft(Ic.begin(),Ic.end(),bc.begin());
  time(&end_cp_split);
  
  time(&start_fftwr);
  slip::real_fftw(I.begin(),I.end(),bfftwr.begin());
  time(&end_fftwr);
  
  time(&start_fftwc);
  slip::complex_fftw(Ic.begin(),Ic.end(),bfftwc.begin());
  time(&end_fftwc);
  
  if(disp)
    {
      std::cout << std::endl;  
      std::cout<< "Real-Split-Radix" <<std::endl;
      std::copy(b.begin(), b.end(),std::ostream_iterator<cx>(std::cout," "));
      std::cout<<std::endl;
      std::cout<<std::endl;
      std::cout<< "Complex-Split-Radix" <<std::endl;
      std::copy(bc.begin(), bc.end(),std::ostream_iterator<cx>(std::cout," "));
      std::cout<<std::endl;
      std::cout<<std::endl;
      std::cout<< "Real-fftw" <<std::endl;
      std::copy(bfftwr.begin(), bfftwr.end(),std::ostream_iterator<cx>(std::cout," "));
      
      std::cout<<std::endl;
      std::cout<<std::endl;
      std::cout<< "Complex-fftw" <<std::endl;
      std::copy(bfftwc.begin(), bfftwc.end(),std::ostream_iterator<cx>(std::cout," "));
      
      std::cout<<std::endl;
      std::cout<<std::endl;
    }
  
  time_diff = end_real_split - start_real_split;
  std::cout << "Real Split FFT computation time : " << time_diff << "sec.\n";
  time_diff = end_cp_split - start_cp_split;
  std::cout << "Complex Split FFT computation time : " << time_diff << "sec.\n";
  time_diff = end_fftwr - start_fftwr;
  std::cout << "Real FFTW computation time : " << time_diff << "sec.\n";
  
  time_diff = end_fftwc - start_fftwc;
  std::cout << "Complex FFTW computation time : " << time_diff << "sec.\n";
  
  //fft backward
  std::cout<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Backward FFT" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(80) << '_' << std::endl;
  
  time(&start_real_split);
  slip::split_radix_ifft(b.begin(),b.end(),b2.begin());
  time(&end_real_split);
  
  time(&start_fftwr);
  slip::ifftw(bfftwc.begin(),bfftwc.end(),bfftw2.begin());
  time(&end_fftwr);
  
  if(disp)
    { 
      std::cout << std::endl; 
      std::cout<< "Split-Radix" <<std::endl;
      std::copy(b2.begin(), b2.end(),std::ostream_iterator<cx>(std::cout," "));
      std::cout<<std::endl;
      std::cout<<std::endl;
      std::cout<< "fftw" <<std::endl;
      std::copy(bfftw2.begin(), bfftw2.end(),std::ostream_iterator<cx>(std::cout," "));
      std::cout<<std::endl;
      std::cout<<std::endl;
    }
  
  time_diff = end_real_split - start_real_split;
  std::cout << "Split iFFT computation time : " << time_diff << "sec.\n";
  time_diff = end_fftwr - start_fftwr;
  std::cout << "iFFTW computation time : " << time_diff << "sec.\n";
#else
  std::cout << "fftw not defined for SLIP use.\n";
#endif    
  return 0;
}

 
