#include <iostream>
#include <algorithm>
#include <numeric>
#include "threshold.hpp"
#include "Matrix.hpp"
#include "GrayscaleImage.hpp"

int main()
{
  
  slip::Matrix<float> M(4,5);
  
  float k = 0.0;
  for(std::size_t i = 0; i < M.dim1(); ++i)
    {
      for(std::size_t j = 0; j < M.dim2(); ++j)
	{
	  M[i][j] = k;
	  k+=1.0;
	}
    }
  
  std::cout<<M<<std::endl;
  slip::Matrix<int> Result(4,5);
 
  slip::binarize(M.begin(),M.end(),Result.begin(),8.0f,2,255);
  std::cout<<Result<<std::endl;

  slip::threshold(M.begin(),M.end(),Result.begin(),8.0f,0);
  std::cout<<Result<<std::endl;


  slip::GrayscaleImage<unsigned char> I1;
  I1.read("./../backend/lena.gif");
  slip::GrayscaleImage<unsigned char> I2(I1.dim1(),I1.dim2());
  //slip::binarize(I1.begin(),I1.end(),I2.begin(),120,0,255);
  slip::threshold(I1.begin(),I1.end(),I2.begin(),120,0);
  I2.write("lena_threshold1.gif");
  slip::db_threshold(I1.begin(),I1.end(),I2.begin(),40,120,0);
  I2.write("lena_threshold2.gif");


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Multi-threshold " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  slip::Matrix<double> Mat(4,5);
  slip::iota(Mat.begin(),Mat.end(),1.0,1.0);
  Mat[0][4] = 18.0;
  Mat[3][3] = 2.0;
  Mat[3][0] = 5.0;
  Mat[1][2] = 20.0;
  std::cout<<"Mat=\n"<<Mat<<std::endl;
  int f1[]={5,10,15,17};
  int f2[]={1,2,3,4};
  slip::Array<int> threshold(4,f1);
  slip::Array<int> level(4,f2);
  std::cout<<"threshold = \n"<<threshold<<std::endl;
  std::cout<<"level = \n"<<level<<std::endl;
  slip::Matrix<double> Mat_res(Mat.rows(),Mat.cols());
  slip::multi_threshold(Mat.begin(),Mat.end(),
                        threshold.begin(),threshold.end(),
			level.begin(),level.end(),
			Mat_res.begin(),Mat_res.end());
  std::cout<<"Mat_res=\n"<<Mat_res<<std::endl;

}
