#include "pca.hpp"

int main()
{

	float d[] = {13.92,-1.92,-1.0,		-21.08,-19.92,-20.0,	7.92,10.08,6.0,	  	27.92,-6.92,-11.0,
		      -2.08,-4.92,-3.0,		-23.08,-9.92,-4.0,	28.92,17.08,21.0,	8.92,14.08,12.0,
		      2.92,-5.92,-7.0,		12.92,1.08,5.,		2.92,-2.92,-3.0,	-14.08,-17.92,-9.0,
		      -28.08,-19.92,-16.0,	5.92,35.08,37.0,	7.92,15.08,20.0,	0.92,-5.92,-8.0,
		      26.92,21.08,20.0,		-16.08,17.08,20.0,	42.92,-8.92,-7.0,	-19.08,3.08,11.0,
		      -6.08,-5.92,-11.0,	-17.08,9.08,7.0,	-15.08,-15.92,-24.0,	-16.08,-10.92,-16.0,
		      -13.08,-4.92,-19.0};
	std::size_t size=3;
	std::size_t nbvector=25;


/*	double d[] = { 1.0,1.5,1.0,0.5,-1.0,-1.5,-1.0,-0.5 };
	std::size_t size=2;
	std::size_t nbvector=4;
*/

//	double d[] = { 0.0,3.0,1.0, 0.2,2.0,2.0, 0.5,1.0,3.0};
//	std::size_t size=3;
//	std::size_t nbvector=3;

	slip::Matrix<float> data(nbvector,size,d);
	std::cout<<"DATA"<<std::endl<<data<<std::endl;
	slip::Matrix<double> acp(nbvector,size,0.0);
	std::cout<<"ACP INIT"<<std::endl<<acp<<std::endl;
	slip::Vector<double> eigenval(size,0.0);
	slip::Matrix<double> metric(size,size,0.0);
	slip::pca_uniform_metric(metric);
	std::cout<<"METRIC"<<std::endl<<metric<<std::endl;
	slip::pca_computation(data,acp,metric,eigenval);
	std::cout<<"ACP"<<std::endl<<acp<<std::endl;
	std::cout<<"EIGEN VALUES"<<std::endl<<eigenval<<std::endl;

	slip::pca_adaptative_metric(data,metric);
	std::cout<<"METRIC"<<std::endl<<metric<<std::endl;
	slip::pca_computation(data,acp, metric, eigenval);
	std::cout<<"ACP"<<std::endl<<acp<<std::endl;
	std::cout<<"EIGEN VALUES"<<std::endl<<eigenval<<std::endl;


	slip::Matrix<double> reduc_data(nbvector,size-1);
	slip::pca_data_reduction(data, metric, reduc_data,reduc_data.cols());
	std::cout<<"REDUC DATA"<<std::endl<<reduc_data<<std::endl;
	slip::pca_data_reduction(data, metric, reduc_data);
	std::cout<<"REDUC DATA2"<<std::endl<<reduc_data<<std::endl;


}

