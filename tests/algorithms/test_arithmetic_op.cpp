#include <iostream>
#include <algorithm>
#include <numeric>
#include <iomanip>
#include "arithmetic_op.hpp"
#include "Array.hpp"
#include "Array2d.hpp"
#include "Matrix.hpp"
#include "macros.hpp"
#include <vector>
#include <complex>

template<typename T>
bool lt5Predicate (const T& val)
  {
     return (val < T(5));
  }

template<typename T>
bool gt5Predicate (const T& val)
  {
     return (val > T(5));
  }

template<typename T>
bool lt16Predicate (const T& val)
  {
     return (val < T(16));
  }
  
int main()
{
  
  slip::Array2d<int> M(4,5);
  
  int k = 0;
  for(std::size_t i = 0; i < M.dim1(); ++i)
    {
      for(std::size_t j = 0; j < M.dim2(); ++j)
	{
	  M[i][j] = ++k; 
	}
    }
  
  slip::Array2d<int> M2(4,5,2);
  slip::Array2d<int> M3(4,5);
  slip::plus(M.begin(),M.end(),M2.begin(),M3.begin());
  std::cout<<M<<std::endl;
  std::cout<<M2<<std::endl;
  std::cout<<M3<<std::endl;

  slip::minus(M.begin(),M.end(),M2.begin(),M3.begin());
  std::cout<<M3<<std::endl;

  slip::multiplies(M.begin(),M.end(),M2.begin(),M3.begin());
  std::cout<<M3<<std::endl;

  slip::divides(M.begin(),M.end(),M2.begin(),M3.begin());
  std::cout<<M3<<std::endl;
 
  slip::negate(M.begin(),M.end(),M3.begin());
  std::cout<<M3<<std::endl;

  slip::minimum(M.begin(),M.end(),M2.begin(),M3.begin());
  std::cout<<M3<<std::endl;

  slip::maximum(M.begin(),M.end(),M2.begin(),M3.begin());
  std::cout<<M3<<std::endl;

  std::transform(M.begin(),M.end(),M3.begin(),std::bind1st(std::plus<int>(),4)); std::cout<<M3<<std::endl;
 

  std::vector<float> v(5,2.0);
  std::vector<float> v2(5,3.0);
  std::vector<float> v3(5);

  slip::plus(v.begin(),v.end(),v2.begin(),v3.begin());
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout,"\n"));
  std::copy(v2.begin(),v2.end(),std::ostream_iterator<float>(std::cout,"\n"));
  std::copy(v3.begin(),v3.end(),std::ostream_iterator<float>(std::cout,"\n"));
 
 
  

  std::complex<float> c1(1.0,2.0);
  slip::Array2d<std::complex<float> > MM(1,2,c1);
  std::cout<<MM<<std::endl;
  std::complex<float> c2(2.0,1.0);
  slip::Array2d<std::complex<float> > MM2(1,2,c2);
  std::cout<<MM2<<std::endl;
  slip::Array2d<std::complex<float> > MM3(1,2);
  
  slip::plus(MM.begin(),MM.end(),MM2.begin(),MM3.begin());
  std::cout<<MM3<<std::endl;

  slip::Matrix<float> Mf(2,2,3.0);
  slip::Array2d<slip::Matrix<float> > MM4(2,2,Mf);
  std::cout<<MM4<<std::endl;

  slip::Matrix<double> Miota(4,5,0.0);
  slip::iota(Miota.begin(),Miota.end(),0.0,0.05);
  std::cout<<Miota<<std::endl;

  float a = -2.3f;
  float b = 15.3f;
  float c = 0.0f;
 
  slip::Sign<float,int> S; 
  std::cout<<"sign("<<a<<") = "<<S(a)<<std::endl;
  std::cout<<"sign("<<b<<") = "<<S(b)<<std::endl;
  std::cout<<"sign("<<c<<") = "<<S(c)<<std::endl;


   ////////////////////////////////////////////////////////////////////////
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "plus mask " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Array<int> V1(10);
   slip::iota(V1.begin(),V1.end(),1,1);
   std::cout<<"V1 = "<<V1<<std::endl;
   slip::Array<int> V2(V1);
   std::cout<<"V2 = "<<V2<<std::endl;
   slip::Array<bool> mask(V1.size());
   mask[2] = 1;
   mask[4] = 1;
   mask[9] = 1;
   slip::Array<int> maskint(V1.size());
   std::cout<<"mask = "<<mask<<std::endl;
   maskint[2] = 2;
   maskint[4] = 2;
   maskint[9] = 2;
   slip::Array<int> V3(V1.size());  
	
   slip::plus_mask(V1.begin(),V1.end(),
                   mask.begin(),
                   V2.begin(),
                   V3.begin());
   std::cout<<"V3 = "<<V3<<std::endl;
   std::cout<<"maskint = "<<maskint<<std::endl;
  
   slip::plus_mask(V1.begin(),V1.end(),
                   maskint.begin(),
                   V2.begin(),
                   V3.begin(),
                   int(2));
  std::cout<<"V3 = "<<V3<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "plus if " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  V3 = 0;
  slip::plus_if(V1.begin(),V1.end(),
                V2.begin(),
                V3.begin(),
                lt5Predicate<int>);
  std::cout<<"V3 = "<<V3<<std::endl;
  ////////////////////////////////////////////////////////////////////////
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "minus mask " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"V1 = "<<V1<<std::endl;
  slip::iota(V2.begin(),V2.end(),10,-1);
  std::cout<<"V2 = "<<V2<<std::endl;
  V3 = 0;
  slip::minus_mask(V1.begin(),V1.end(),
                   maskint.begin(),
                   V2.begin(),
                   V3.begin(),
                   int(2));
  std::cout<<"V3 = "<<V3<<std::endl;
  V3 = 0;
  slip::minus_mask(V1.begin(),V1.end(),
                   mask.begin(),
                   V2.begin(),
                   V3.begin());
  std::cout<<"V3 = "<<V3<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "minus if " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  V3 = 0;
  slip::minus_if(V1.begin(),V1.end(),
                V2.begin(),
                V3.begin(),
                lt5Predicate<int>);
  std::cout<<"V3 = "<<V3<<std::endl;
////////////////////////////////////////////////////////////////////////
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "multiplies mask " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"V1 = "<<V1<<std::endl;
  slip::iota(V2.begin(),V2.end(),10,-1);
  std::cout<<"V2 = "<<V2<<std::endl;
  V3 = 0;
  slip::multiplies_mask(V1.begin(),V1.end(),
                   maskint.begin(),
                   V2.begin(),
                   V3.begin(),
                   int(2));
  std::cout<<"V3 = "<<V3<<std::endl;
  V3 = 0;
  slip::multiplies_mask(V1.begin(),V1.end(),
                   mask.begin(),
                   V2.begin(),
                   V3.begin());
  std::cout<<"V3 = "<<V3<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "multiplies if " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  V3 = 0;
  slip::multiplies_if(V1.begin(),V1.end(),
                V2.begin(),
                V3.begin(),
                lt5Predicate<int>);
  std::cout<<"V3 = "<<V3<<std::endl;
////////////////////////////////////////////////////////////////////////
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "divides mask " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::iota(V1.begin(),V1.end(),10,2);
  std::cout<<"V1 = "<<V1<<std::endl;
  slip::iota(V2.begin(),V2.end(),1,1);
  std::cout<<"V2 = "<<V2<<std::endl;
  V3 = 0;
  slip::divides_mask(V1.begin(),V1.end(),
                   maskint.begin(),
                   V2.begin(),
                   V3.begin(),
                   int(2));
  std::cout<<"V3 = "<<V3<<std::endl;
  V3 = 0;
  slip::divides_mask(V1.begin(),V1.end(),
                   mask.begin(),
                   V2.begin(),
                   V3.begin());
  std::cout<<"V3 = "<<V3<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "divides if " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  V3 = 0;
  slip::divides_if(V1.begin(),V1.end(),
                V2.begin(),
                V3.begin(),
                lt16Predicate<int>);
  std::cout<<"V3 = "<<V3<<std::endl;

////////////////////////////////////////////////////////////////////////
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "negate mask " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::iota(V1.begin(),V1.end(),10,2);
  std::cout<<"V1 = "<<V1<<std::endl;
  V3 = 0;
  slip::negate_mask(V1.begin(),V1.end(),
                   mask.begin(),
                   V3.begin());
  std::cout<<"V3 = "<<V3<<std::endl;
  V3 = 0;
  slip::negate_mask(V1.begin(),V1.end(),
                   maskint.begin(),
                   V3.begin(),
                   int(2));
  std::cout<<"V3 = "<<V3<<std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
 std::cout << "negate if " << std::endl;
 std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 V3 = 0;
 std::cout<<"V1 = "<<V1<<std::endl;
 slip::negate_if(V1.begin(),V1.end(),
                V3.begin(),
                lt16Predicate<int>);
 std::cout<<"V3 = "<<V3<<std::endl;

////////////////////////////////////////////////////////////////////////
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "minimum mask " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::iota(V1.begin(),V1.end(),10,2);
  std::cout<<"V1 = "<<V1<<std::endl;
  slip::iota(V2.begin(),V2.end(),1,1);
  std::cout<<"V2 = "<<V2<<std::endl;
  V3 = 0;
  slip::minimum_mask(V1.begin(),V1.end(),
                   maskint.begin(),
                   V2.begin(),
                   V3.begin(),
                   int(2));
  std::cout<<"V3 = "<<V3<<std::endl;
  V3 = 0;
  slip::minimum_mask(V1.begin(),V1.end(),
                   mask.begin(),
                   V2.begin(),
                   V3.begin());
  std::cout<<"V3 = "<<V3<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "minimum if " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  V3 = 0;
  slip::minimum_if(V1.begin(),V1.end(),
                V2.begin(),
                V3.begin(),
                lt16Predicate<int>);
  std::cout<<"V3 = "<<V3<<std::endl;

////////////////////////////////////////////////////////////////////////
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "maximum mask " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::iota(V1.begin(),V1.end(),10,2);
  std::cout<<"V1 = "<<V1<<std::endl;
  slip::iota(V2.begin(),V2.end(),1,1);
  std::cout<<"V2 = "<<V2<<std::endl;
  V3 = 0;
  slip::maximum_mask(V1.begin(),V1.end(),
                   maskint.begin(),
                   V2.begin(),
                   V3.begin(),
                   int(2));
  std::cout<<"V3 = "<<V3<<std::endl;
  V3 = 0;
  slip::maximum_mask(V1.begin(),V1.end(),
                   mask.begin(),
                   V2.begin(),
                   V3.begin());
  std::cout<<"V3 = "<<V3<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "maximum if " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  V3 = 0;
  slip::maximum_if(V1.begin(),V1.end(),
                V2.begin(),
                V3.begin(),
                lt16Predicate<int>);
  std::cout<<"V3 = "<<V3<<std::endl;
////////////////////////////////////////////////////////////////////////
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "iota mask " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"mask = "<<mask<<std::endl;
  V1 = 0;
  slip::iota_mask(V1.begin(),V1.end(),mask.begin(),1,2);
  std::cout<<"V1 = "<<V1<<std::endl;
  std::cout<<"maskint = "<<maskint<<std::endl;
  V1 = 0;
  slip::iota_mask(V1.begin(),V1.end(),maskint.begin(),10,2,int(2));
  std::cout<<"V1 = "<<V1<<std::endl;
 

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "plus scalar " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"M = \n"<<M<<std::endl;
   slip::plus_scalar(M.begin(),M.end(),5,M3.begin());
   std::cout<<"M + 5 = \n"<<M3<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "minus scalar " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"M = \n"<<M<<std::endl;
   slip::minus_scalar(M.begin(),M.end(),5,M3.begin());
   std::cout<<"M - 5 = \n"<<M3<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "multiplies by a  scalar " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"M = \n"<<M<<std::endl;
   slip::multiplies_scalar(M.begin(),M.end(),5,M3.begin());
   std::cout<<"M * 5 = \n"<<M3<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "divides by a scalar " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"Miota = \n"<<Miota<<std::endl;
   slip::Array2d<float> M3f(4,5);
   slip::divides_scalar(Miota.begin(),Miota.end(),5,M3f.begin());
   std::cout<<"Miota / 5.0 = \n"<<M3f<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "plus scalar mask " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"V1 = \n"<<V1<<std::endl;
   std::cout<<"mask = \n"<<mask<<std::endl;
   V3.fill(0);
   slip::plus_scalar_mask(V1.begin(),V1.end(),
			  mask.begin(),
			  5,
			  V3.begin());
   std::cout<<"V1 + 5 = \n"<<V3<<std::endl;
   V3.fill(0);
   std::cout<<"maskint = \n"<<maskint<<std::endl;
  
   slip::plus_scalar_mask(V1.begin(),V1.end(),
                   maskint.begin(),
                   5,
                   V3.begin(),
                   int(2));
   std::cout<<"V1 + 5 = \n"<<V3<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "minus scalar mask " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"V1 = \n"<<V1<<std::endl;
   std::cout<<"mask = \n"<<mask<<std::endl;
   V3.fill(0);
   slip::minus_scalar_mask(V1.begin(),V1.end(),
			  mask.begin(),
			  5,
			  V3.begin());
   std::cout<<"V1 - 5 = \n"<<V3<<std::endl;
   V3.fill(0);
   std::cout<<"maskint = \n"<<maskint<<std::endl;
  
   slip::minus_scalar_mask(V1.begin(),V1.end(),
                   maskint.begin(),
                   5,
                   V3.begin(),
                   int(2));
   std::cout<<"V1 - 5 = \n"<<V3<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "multiplies scalar mask " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"V1 = \n"<<V1<<std::endl;
   std::cout<<"mask = \n"<<mask<<std::endl;
   V3.fill(0);
   slip::multiplies_scalar_mask(V1.begin(),V1.end(),
			  mask.begin(),
			  5,
			  V3.begin());
   std::cout<<"V1 * 5 = \n"<<V3<<std::endl;
   V3.fill(0);
   std::cout<<"maskint = \n"<<maskint<<std::endl;
  
   slip::multiplies_scalar_mask(V1.begin(),V1.end(),
                   maskint.begin(),
                   5,
                   V3.begin(),
                   int(2));
   std::cout<<"V1 * 5 = \n"<<V3<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "divides scalar mask " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"V1 = \n"<<V1<<std::endl;
   std::cout<<"mask = \n"<<mask<<std::endl;
   V3.fill(0);
   slip::divides_scalar_mask(V1.begin(),V1.end(),
			  mask.begin(),
			  2,
			  V3.begin());
   std::cout<<"V1 / 2 = \n"<<V3<<std::endl;
   V3.fill(0);
   std::cout<<"maskint = \n"<<maskint<<std::endl;
  
   slip::divides_scalar_mask(V1.begin(),V1.end(),
                   maskint.begin(),
                   2,
                   V3.begin(),
                   int(2));
   std::cout<<"V1 / 2 = \n"<<V3<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "plus scalar if " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   V3 = 0;
   std::cout<<"V1 = \n "<<V1<<std::endl;
   slip::plus_scalar_if(V1.begin(),V1.end(),
			5,
			V3.begin(),
			lt5Predicate<int>);
   std::cout<<"V1 + 5 if V1 < 5= \n"<<V3<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "minus scalar if " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   V3 = 0;
   std::cout<<"V1 = \n "<<V1<<std::endl;
   slip::minus_scalar_if(V1.begin(),V1.end(),
			5,
			V3.begin(),
			lt5Predicate<int>);
   std::cout<<"V1 - 5 if V1 < 5 = \n"<<V3<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "multiplies scalar if " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   V3 = 0;
   std::cout<<"V1 = \n "<<V1<<std::endl;
   slip::multiplies_scalar_if(V1.begin(),V1.end(),
			      int(5),
			V3.begin(),
			gt5Predicate<int>);
   std::cout<<"V1 * 5 if V1 > 5 = \n"<<V3<<std::endl;
 
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "divides scalar if " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   V3 = 0;
   std::cout<<"V1 = \n "<<V1<<std::endl;
   slip::divides_scalar_if(V1.begin(),V1.end(),
			   int(2),
			V3.begin(),
			gt5Predicate<int>);
   std::cout<<"V1 / 2 if V1 > 5= \n"<<V3<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "linspace " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   typedef double T;
   slip::Array<T> A(7);
   slip::linspace(-5.0,5.0,7,A.begin(),A.end());
   std::cout<<"slip::linspace(-5.0,5.0,"<<A.size()<<") = \n"<<A<<std::endl;
   slip::linspace(-5.0,5.0,7,A.begin(),A.end(),false);
   std::cout<<"slip::linspace(-5.0,5.0,"<<A.size()<<",false) = \n"<<A<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "logspace " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::logspace(-5.0,5.0,7,A.begin(),A.end());
   std::cout<<"slip::logspace(-5.0,5.0,"<<A.size()<<") = \n"<<A<<std::endl;
   slip::logspace(-5.0,5.0,7,A.begin(),A.end(),false);
   std::cout<<"slip::logspace(-5.0,5.0,"<<A.size()<<",false) = \n"<<A<<std::endl;
   slip::logspace(-5.0,5.0,7,A.begin(),A.end(),true,slip::constants<double>::euler());
   std::cout<<"slip::logspace(-5.0,5.0,"<<A.size()<<",true,e) = \n"<<A<<std::endl;
   slip::logspace(-5.0,5.0,7,A.begin(),A.end(),false,slip::constants<double>::euler());
   std::cout<<"slip::logspace(-5.0,5.0,"<<A.size()<<",false,e) = \n"<<A<<std::endl;
   slip::logspace(1.0,7.0,7,A.begin(),A.end(),true,2.0);
   std::cout<<"slip::logspace(1.0,7.0,"<<A.size()<<",true,2) = \n"<<A<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "geomspace " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::geomspace(1.0,1000.0,7,A.begin(),A.end());
   std::cout<<"slip::geomspace(1.0,1000.0,"<<A.size()<<") = \n"<<A<<std::endl;
   slip::geomspace(2.0,1000.0,7,A.begin(),A.end());
   std::cout<<"slip::geomspace(2.0,1000.0,"<<A.size()<<") = \n"<<A<<std::endl;
   slip::geomspace(1.0,1000.0,7,A.begin(),A.end(),false);
   std::cout<<"slip::geomspace(1.0,1000.0,"<<A.size()<<",false) = \n"<<A<<std::endl;
   slip::geomspace(2.0,1000.0,7,A.begin(),A.end(),false);
   std::cout<<"slip::geomspace(2.0,1000.0,"<<A.size()<<",false) = \n"<<A<<std::endl;
}
