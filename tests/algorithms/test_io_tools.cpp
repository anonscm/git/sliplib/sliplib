#include <iostream>
#include <cstdlib>
#include "io_tools.hpp"
#include "Array2d.hpp"
#include "Vector2d.hpp"
#include "Vector3d.hpp"
#include "GrayscaleImage.hpp"
#include "ColorImage.hpp"
#include "Matrix.hpp"
#include "Volume.hpp"
#include "Range.hpp"
#include "Array.hpp"
#include "DenseVector2dField2d.hpp"
#include "DenseVector3dField3d.hpp"
#include "arithmetic_op.hpp"
//read/write image with ImageMagick
#include <wand/magick_wand.h> 
#include <magick/api.h>

struct RndDoubleGen
{
  RndDoubleGen(double low, double high):low_(low),high_(high)
  {}
  slip::Vector2d<double> operator()() const
  {
    return slip::Vector2d<double>(low_ + (((double)rand() / RAND_MAX) * (high_ - low_)),low_ + (((double)rand() / RAND_MAX) * (high_ - low_)));
  }

  double low_;
  double high_;
};

struct RndDoubleGen3
{
 RndDoubleGen3(double low, double high):low_(low),high_(high)
  {}
  slip::Vector3d<double> operator()() const
  {
    return slip::Vector3d<double>(low_ + (((double)rand() / RAND_MAX) * (high_ - low_)),low_ + (((double)rand() / RAND_MAX) * (high_ - low_)),low_ + (((double)rand() / RAND_MAX) * (high_ - low_)));
  }

  double low_;
  double high_;
};

int main()
{


  
  std::string ext;
  std::string file_path_cut;
  std::size_t pos = slip::split_extension("/toto/titi/tutu000_a.tiff",file_path_cut,ext);
  std::cout<<"file_path_cut = "<<file_path_cut<<std::endl;
  std::cout<<"ext = "<<ext<<std::endl;
  std::cout<<"pos = "<<pos<<std::endl;
 
  std::cout<<slip::compose_file_name("/toto/titi/tutu%4d.tiff",4)<<std::endl;
  std::cout<<slip::compose_file_name("/toto/titi/tutu%4d_a.tiff",4)<<std::endl;
  
//   for(std::size_t img = 1; img < 5; ++img)
//     {
//       std::cout<<slip::compose_file_name("toto%3d.bmp",img)<<std::endl;
//     }


//   for(std::size_t img = 1; img < 5; ++img)
//     {
//       std::cout<<slip::compose_file_name("titi%5d.png",img)<<std::endl;
//     }

   double d[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,
		 15.0,16.0,17.0,18.0};

   slip::Array2d<double> A(3,6,d);
   slip::print(A);
   slip::print(A,"A");
   

   slip::write_ascii_2d<slip::Array2d<double>::iterator2d,double>(A.upper_left(),A.bottom_right(),"A.dat");

   slip::Range<int> r1(0,2,1);
   slip::Range<int> r2(1,5,2);
   slip::write_ascii_2d<slip::Array2d<double>::iterator2d_range,double>(A.upper_left(r1,r2),A.bottom_right(r1,r2),"Arange.dat");

   slip::Array2d<double> AAA;
   slip::read_ascii_2d(AAA,"Arange.dat");
   std::cout<<"read ascii2d "<<std::endl;
   std::cout<<AAA<<std::endl;

   slip::Array2d<double> AAAA;
   slip::read_ascii_2d(AAAA,"Arange_head.dat");
   std::cout<<"read ascii2d  with comments"<<std::endl;
   std::cout<<AAAA<<std::endl;

  

 //  slip::GrayscaleImage<unsigned char> I;
//   I.read("lena.jpg");
// //   slip::write_magick_gray_char(I.begin(),I.width(),I.height(),"A.png");
  
// //    slip::GrayscaleImage<short> I;
// //    I.read("lena.jpg");
// //    slip::write_magick_gray_short(I.begin(),I.width(),I.height(),"A.png");
   
// //    slip::GrayscaleImage<int> I;
// //    I.read("lena.jpg");
// //    slip::write_magick_gray_int(I.begin(),I.width(),I.height(),"A.png");

// //    slip::GrayscaleImage<long> I;
// //    I.read("lena.jpg");
// //    slip::write_magick_gray_long(I.begin(),I.width(),I.height(),"A.png");
// //    slip::GrayscaleImage<float> I;
// //    I.read("lena.jpg");
// //    slip::write_magick_gray_float(I.begin(),I.width(),I.height(),"A.png");
// //     slip::GrayscaleImage<double> I;
// //     I.read("lena.jpg");
// //     slip::write_magick_gray_double(I.begin(),I.width(),I.height(),"A.png");
//  //   slip::GrayscaleImage<unsigned char> I2;
// //    slip::read_magick_gray_char("lena.jpg",I2);

// //    slip::GrayscaleImage<short> I2;
// //    slip::read_magick_gray_short("lena.jpg",I2);

// //    slip::GrayscaleImage<int> I2;
// //    slip::read_magick_gray_int("lena.jpg",I2);

// //    slip::GrayscaleImage<long> I2;
// //    slip::read_magick_gray_long("lena.jpg",I2);
   
//  //   slip::GrayscaleImage<float> I2;
// //    slip::read_magick_gray_float("lena.jpg",I2);
   
//   //  slip::GrayscaleImage<double> I2;
// //    slip::read_magick_gray_double("lena.jpg",I2);

//    slip::Range<int> ra1(0,256,1);
//    slip::Range<int> ra2(0,256,1);
//    // slip::write_magick_gray_char(I2.upper_left(ra1,ra2),I2.bottom_right(ra1,ra2),"Abox.png");
//  //   slip::write_magick_gray_short(I2.upper_left(ra1,ra2),I2.bottom_right(ra1,ra2),"Abox.png");
// //    slip::write_magick_gray_int(I2.upper_left(ra1,ra2),I2.bottom_right(ra1,ra2),"Abox.png");
// //    slip::write_magick_gray_long(I2.upper_left(ra1,ra2),I2.bottom_right(ra1,ra2),"Abox.png");
// //    slip::write_magick_gray_float(I2.upper_left(ra1,ra2),I2.bottom_right(ra1,ra2),"Abox.png");
//  //   slip::write_magick_gray_double(I2.upper_left(ra1,ra2),I2.bottom_right(ra1,ra2),"Abox.png");

//    std::cout<<" read/write ascii 1d"<<std::endl;
//    float tab[] = {1.0,2.0,3.0,4.0,5.0,6.0};
//    slip::Array<float> Tab(6,tab);
//    slip::write_ascii_1d<slip::Array<float>::iterator,float>(Tab.begin(),Tab.end(),"test.dat");

//    slip::Array<float> Tab2;
//    slip::read_ascii_1d("test.dat",Tab2);
//    std::cout<<Tab2.size()<<std::endl;
//    std::cout<<Tab2<<std::endl;
   
   slip::GrayscaleImage<double> I2(5,6);
   slip::iota(I2.begin(),I2.end(),1.0,std::sqrt(2));
   slip::write_tecplot_2d<slip::GrayscaleImage<double>::iterator2d,double>(I2.upper_left(),I2.bottom_right(),"Abox.tec", "Abox.tec","image",12);

//  //   slip::ColorImage<double> I2c;
// //    I2c.read("lena.jpg");
// //    slip::write_magick_color_double(I2c.begin(),I2c.width(),I2c.height(),"lena_col.png");

// //    slip::write_magick_color(I2c.upper_left(ra1,ra2),I2c.bottom_right(ra1,ra2),DoublePixel,"lena_col_box.png");


// //    slip::ColorImage<float> Ic;
// //    slip::read_magick_color("lena.jpg",Ic,FloatPixel);
// //    slip::write_magick_color_float(Ic.begin(),Ic.width(),Ic.height(),"lena_col.png");

//    int tabi[] = {1,2,3,4,5,6};
//    slip::Matrix<int> Mi(2,3,tabi);
//    std::cout<<Mi<<std::endl;
//    slip::write_raw(Mi.begin(),Mi.end(),"Mi.raw");

//    slip::GrayscaleImage<int> Mi2(2,3);
//    slip::read_raw("Mi.raw",Mi2.begin(),Mi2.end());
//    std::cout<<Mi2<<std::endl;
//    slip::Array<int> AA(6);
//    slip::read_raw("Mi.raw",AA.begin(),AA.end());
//    std::cout<<AA<<std::endl;


//    slip::DenseVector2dField2d<double> XY;
//    slip::DenseVector2dField2d<double> field;
//    slip::read_tecplot_vect2d("xyuv.dat",field);
//    slip::read_tecplot_XY("xyuv.dat",XY);
//    std::cout<<"read_tecplot_vect2d xyuv.dat"<<std::endl;
//    std::cout<<"XY : "<<std::endl;
//    std::cout<<XY<<std::endl;
//    std::cout<<"Field : "<<std::endl;
//    std::cout<<field<<std::endl;

//    slip::read_gnuplot_vect2d("xyuv.dat",field);
//    slip::read_gnuplot_XY("xyuv.dat",XY);
//    std::cout<<"read_tecplot_vect2d xyuv.dat"<<std::endl;
//    std::cout<<"XY : "<<std::endl;
//    std::cout<<XY<<std::endl;
//    std::cout<<"Field : "<<std::endl;
//    std::cout<<field<<std::endl;



//    slip::DenseVector3dField3d<double> XYZ;
//    slip::DenseVector3dField3d<double> field3d;
//    slip::read_tecplot_vect3d("xyzuvw.dat",field3d);
//    slip::read_tecplot_XYZ("xyzuvw.dat",XYZ);
//    std::cout<<"read_tecplot_vect3d xyzuvw.dat"<<std::endl;
//    std::cout<<"XYZ : "<<std::endl;
//    std::cout<<XYZ<<std::endl;
//    std::cout<<"Field : "<<std::endl;
//    std::cout<<field3d<<std::endl;

//    slip::DenseVector3dField3d<double> field3d2;
//    slip::read_tecplot_vect3d("xyzuvw.tec",field3d2);
//    std::cout<<"Field : "<<std::endl;
//    std::cout<<field3d2<<std::endl;

//   //  slip::Volume<double> Vol;
// //    std::string file_base_name;
// //    std::cout<<"file base name ? (example : toto%3d.png)"<<std::endl;
// //    std::cin>>file_base_name;
// //    std::cout<<"first image number ?"<<std::endl;
// //    std::size_t img_from = 1;
// //    std::cin>>img_from;
// //    std::cout<<"last image number ?"<<std::endl;
// //    std::size_t img_to = 1;
// //    std::cin>>img_to;
// //    //Vol.read_from_images(file_base_name,img_from,img_to);
// //    slip::read_from_images(file_base_name,img_from,img_to,Vol,DoublePixel);
// //    std::cout<<"dim1 = "<<Vol.dim1()<<" dim2 = "<<Vol.dim2()<<" dim3 = "<<Vol.dim3()<<" size = "<<Vol.size()<<" slice_size = "<<Vol.slice_size()<<" max_size = "<<Vol.max_size()<<" empty = "<<Vol.empty()<<std::endl; 
// //    //std::cout<<Vol<<std::endl; 

  
// //    Vol.write_to_images("toto-%4d.bmp",1,img_to-img_from);
// //  //   std::vector<slip::Volume<double>::const_iterator> planes(img_to-1);
// // //   for(std::size_t i = 1; i <= img_to-1; ++i)
// // //     {
// // //       planes[i-1] = Vol.plane_begin(i);
// // //     }
// // //    slip::write_to_images_double("toto-%4d.bmp",1,img_to-1,Vol.cols(),Vol.rows(),planes);
    
 //    Vol.write_tecplot("vol.tec","vol title");
//     Vol.write_to_images_tecplot("vol2.tec","vol title",1,4);
   
    slip::Point2d<double> init_point2(0.5,0.1);
    slip::Point2d<double> step2(0.1,0.2);
    slip::DenseVector2dField2d<double> FR2(4,5);
    srand(static_cast<unsigned int>(clock()));
    std::generate(FR2.begin(),FR2.end(),RndDoubleGen(-1.0f,1.0f));
    std::cout<<"FR2 = \n"<<FR2<<std::endl;
    slip::write_tecplot_vect2d(FR2,init_point2,step2,"test_tec2.txt","title","zone");
    slip::write_gnuplot_vect2d(FR2,init_point2,step2,"test_gnu2.txt");
    slip::write_tecplot_vect2d(FR2,"test_tec2_0.txt","title","zone");
    slip::write_gnuplot_vect2d(FR2,"test_gnu2_0.txt");


    slip::Point2d<double> init_point22;
    slip::Point2d<double> step22;
    slip::DenseVector2dField2d<double> FR22(4,5);
//     slip::DenseVector2dField2d<double> XY22(4,5);
//     slip::read_tecplot_vect2d("test_tec2.txt",FR22,init_point22,step22);
//     std::cout<<"init_point22 = "<<init_point22<<std::endl;
//     std::cout<<"step22 = "<<step22<<std::endl;
//     std::cout<<FR22<<std::endl;
//     slip::read_tecplot_XY("test_tec2.txt",XY22);
//     std::cout<<"XY22 = \n"<<XY22<<std::endl;
  //   slip::Array<double> X2;
//     slip::Array<double> Y2;
//     slip::read_tecplot_XY("test_tec2.txt",X2,Y2);
//     std::cout<<"X2 = \n"<<X2<<std::endl;
//     std::cout<<"Y2 = \n"<<Y2<<std::endl;
    slip::read_gnuplot_vect2d("test_gnu2.txt",FR22,init_point22,step22);
    std::cout<<"init_point22 = "<<init_point22<<std::endl;
    std::cout<<"step22 = "<<step22<<std::endl;
    std::cout<<FR22<<std::endl;
    slip::read_gnuplot_vect2d("test_gnu2.txt",FR22);
    std::cout<<FR22<<std::endl;

   
    slip::read_tecplot_vect2d("test_tec2_0.txt",FR22);
    std::cout<<FR22<<std::endl;
    slip::read_tecplot_vect2d("test_tec2.txt",FR22,init_point22,step22);
    std::cout<<"init_point22 = "<<init_point22<<std::endl;
    std::cout<<"step22 = "<<step22<<std::endl;
    std::cout<<FR22<<std::endl;

  slip::Point3d<double> init_point(0.5,0.4,0.1);
    slip::Point3d<double> step(0.1,0.2,0.3);
    slip::DenseVector3dField3d<double> FR(2,4,3);
    slip::DenseVector3dField3d<double> XYZ3(2,4,3);
    (static_cast<unsigned int>(clock()));
    std::generate(FR.begin(),FR.end(),RndDoubleGen3(-1.0f,1.0f));
    std::cout<<"FR = \n"<<FR<<std::endl;
    slip::write_tecplot_vect3d(FR,init_point,step,"test_tec.txt","title","zone");
    slip::write_tecplot_vect3d(FR,"test_tec0.txt","title","zone");
//     slip::write_tecplot_vect3d(FR,init_point,step,"test_tec3.txt","title","zone");
    slip::read_tecplot_vect3d("test_tec0.txt",FR);
    std::cout<<FR<<std::endl;
    
    slip::Point3d<double> init_point3;
    slip::Point3d<double> step3;
    slip::DenseVector3dField3d<double> FR3;
    slip::read_tecplot_vect3d("test_tec.txt",FR3,init_point3,step3);
    std::cout<<"init_point3 = "<<init_point3<<std::endl;
    std::cout<<"step3 = "<<step3<<std::endl;
    std::cout<<FR3<<std::endl;
//     slip::read_tecplot_XYZ("test_tec.txt",XYZ3);
//     std::cout<<"XYZ3 = \n"<<XYZ3<<std::endl;

//     slip::Array<double> X3;
//    slip::Array<double> Y3;
//    slip::Array<double> Z3;
//    slip::read_tecplot_XYZ("test_tec.txt",X3,Y3,Z3);
//    std::cout<<"X : \n"<<X3<<std::endl;
//    std::cout<<"Y : \n"<<Y3<<std::endl;
//    std::cout<<"Z : \n"<<Z3<<std::endl;
  
     slip::Point2d<double> init_point222;
     slip::Point2d<double> step222;
     slip::DenseVector2dField2d<double> FR222;
     slip::read_tecplot_vect2d("BB0001.dat",FR222,init_point222,step222);
     std::cout<<"init_point222 = "<<init_point222<<std::endl;
     std::cout<<"step222 = "<<step222<<std::endl;
     std::cout<<FR222.rows()<<"x"<<FR222.cols()<<std::endl;
     //std::cout<<FR222<<std::endl;

     
     slip::write_tecplot_vect2d(FR222,init_point222,step222,"BB0001_2.dat","title","zone");
     //slip::write_tecplot_vect2d(FR222,"BB0001_3.dat","title","zone");
     slip::read_tecplot_vect2d("BB0001_2.dat",FR222,init_point222,step222);
     std::cout<<"init_point222 = "<<init_point222<<std::endl;
     std::cout<<"step222 = "<<step222<<std::endl;
     // std::cout<<FR222<<std::endl;
}
    
