#include <iostream>
#include <complex>
#include <vector>
#include "Matrix3d.hpp"
#include "Volume.hpp"
#include "FFT.hpp"
#include "dynamic.hpp"

/* 
 ** Test where realized with aal.img and aal.hdr from MRIcro
 ** http://www.sph.sc.edu/comd/rorden/mricro.html
 */

static void usage(const char * );

int main(int argc, char **argv)
{
  std::vector<std::string> args;
  for(int i = 0; i < argc; i++)
    {
      args.push_back(argv[i]);
    }
  
  if((args.size() == 1) || (args[1] == "-h") || (args[1] == "--help"))
    {
      usage(args[0].c_str());
      exit(0);
    }  
  
  if(args.size() == 7)
    {
      slip::Volume<double> M;
      M.read_raw(args[2],181,217,181);
      slip::change_dynamic_01(M.begin(),M.end(),M.begin(),slip::AFFINE_FUNCTION);
      slip::Volume<double> Out(181,217,181,1);
      
      //FFT3D
      std::complex<double> Czero(0,0);
      slip::Matrix3d<std::complex<double> > OFFT(M.dim1(),M.dim2(),M.dim3(),Czero);
      slip::real_split_radix_fft3d(M,OFFT);
      slip::Volume<double> NFFT(OFFT.dim1(),OFFT.dim2(),OFFT.dim3(),M.front_upper_left(),M.back_bottom_right());
      //  log(magnitude)
      for(size_t z = 0; z < OFFT.dim1(); ++z)
	for(size_t i = 0; i < OFFT.dim2(); ++i)
	  for(size_t j = 0; j < OFFT.dim3(); ++j)
	    {
	      NFFT[z][i][j] = std::log(1.0+std::abs(OFFT[z][i][j]));
	    }
      
      slip::fftshift3d(NFFT.front_upper_left(),NFFT.back_bottom_right());
      slip::change_dynamic_01(NFFT.begin(),NFFT.end(),NFFT.begin(),slip::AFFINE_FUNCTION);
      NFFT.write_raw(args[4]);
      
      // IFFT3D
      slip::Matrix3d<std::complex<double> > OC(M.dim1(),M.dim2(),M.dim3(),Czero);
      slip::split_radix_ifft3d(OFFT,OC);
      for(size_t z = 0; z < OFFT.dim1(); ++z)
	for(size_t i = 0; i < OFFT.dim2(); ++i)
	  for(size_t j = 0; j < OFFT.dim3(); ++j)
	    {
	      Out[z][i][j] = std::abs(OC[z][i][j]);
	    }
      slip::change_dynamic_01(Out.begin(),Out.end(),Out.begin(),slip::AFFINE_FUNCTION);
      Out.write_raw(args[6]);
    }
  else 
    {
      usage(args[0].c_str());
      exit(0);
    }
  return 0;
}

static void usage(const char* cmd)
{
  std::cout<<std::endl;
  std::cout<<"NAME:"<<std::endl;
  std::cout<<"\t"<<cmd<<" - Computes the log-magnitude of the 3d split-radix fft of a volume and inverse it"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"SYNOPSIS:"<<std::endl;
  std::cout<<"\t"<<cmd<<" -i file -f file -o file"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"DESCRIPTION: - Computes the log-magnitude of the 3d spit-radix fft of a volume and inverse it :\n"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"\t -i  input volume"<<std::endl;
  std::cout<<"\t -f  fft volume"<<std::endl;
  std::cout<<"\t -o  output volume"<<std::endl;
  std::cout<<"\t -h, --help\t display this help and exit"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"AUTHOR:"<<std::endl;
  std::cout<<"\t Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"REPORTING BUGS:"<<std::endl;
  std::cout<<"\t Report bugs to slip trac serveur"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"COPYRIGHT:"<<std::endl;
  std::cout<<"\t This is free software; see the source for copying conditions."<<std::endl;
  std::cout<<std::endl;
  std::cout<<"SEE ALSO:"<<std::endl;
  std::cout<<std::endl;
}


