#include <complex>
#include <iostream>
#include <vector>

#include "correlation.hpp"
#include "Array3d.hpp"
#include "Volume.hpp"
#include "dynamic.hpp"
#include "error.hpp"

static void usage(const char * );

int main(int argc, char **argv)
{
  //get the argmuments
  std::vector<std::string> args;
  for(int i = 0; i < argc; i++)
    {
      args.push_back(argv[i]);
    }
  
  if((args.size() == 1) || (args[1] == "-h") || (args[1] == "--help"))
    {
      usage(args[0].c_str());
      exit(0);
    }  
  
  if(args.size() == 7)
    {
      slip::Volume<double> I1;
      I1.read_raw(args[2],51,217,181);
      slip::Volume<double> I2;
      I2.read_raw(args[4],51,64-17+1,110-70+1);
      
      slip::DPoint3d<int> size1 = I1.back_bottom_right() - I1.front_upper_left();
      slip::DPoint3d<int> size2 = I2.back_bottom_right() - I2.front_upper_left();
      slip::DPoint3d<int> size(size1[0] + size2[0] - 1, size1[1] + size2[1] - 1, size1[2] + size2[2] - 1);

      slip::Array3d<double> A1(size[0],size[1],size[2],0.0);
      slip::Array3d<double> A2(size[0],size[1],size[2],0.0);
      slip::Box3d<int> box1(size2[0]/2,size2[1]/2,size2[2]/2,size2[0]/2 + size1[0]-1,size2[1]/2 + size1[1]-1,size2[2]/2 + size1[2]-1);
      slip::Box3d<int> box2(size1[0]/2,size1[1]/2,size1[2]/2,size1[0]/2 + size2[0]-1,size1[1]/2 + size2[1]-1,size1[2]/2 + size2[2]-1);
      std::copy(I1.front_upper_left(),I1.back_bottom_right(),A1.front_upper_left(box1));
      std::copy(I2.front_upper_left(),I2.back_bottom_right(),A2.front_upper_left(box2));
      slip::Volume<double> OutFFT(size[0],size[1],size[2],1.0);
      slip::Volume<double> OutFFT2(I1.dim1(),I1.dim2(),I1.dim3(),1.0);
      
      std::cout << "FFT crosscorrelation starting...";
      slip::fft_circular_crosscorrelation3d(A1.front_upper_left(),
					    A1.back_bottom_right(),
					    A2.front_upper_left(),
					    A2.back_bottom_right(),
					    OutFFT.front_upper_left(),
					    OutFFT.back_bottom_right());
      std::cout << "...stop" << std::endl;
      
      slip::Volume<double>::iterator maxfft = std::max_element(OutFFT.begin(),OutFFT.end());
      std::cout << "...stop" << std::endl;	
      std::cout << "max fft : " <<  (size_t)(maxfft - OutFFT.begin()) / I1.dim2() << " , " << (size_t)(maxfft - OutFFT.begin()) % I1.dim2() << " = " << *maxfft << std::endl;
      slip::change_dynamic_01(OutFFT.front_upper_left(),OutFFT.back_bottom_right(),OutFFT.front_upper_left(),slip::AFFINE_FUNCTION);
      std::copy(OutFFT.front_upper_left(box1),OutFFT.back_bottom_right(box1),OutFFT2.front_upper_left());
      OutFFT2.write_to_images("./images/aal_double_fftcorr-%4d.bmp",0,50);
    }
  else 
    {
      usage(args[0].c_str());
      exit(0);
    }
  return 0;
}

static void usage(const char* cmd)
{
  std::cout<<std::endl;
  std::cout<<"NAME:"<<std::endl;
  std::cout<<"\t"<<cmd<<" - Computes the fft cross correlation of two volumes"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"SYNOPSIS:"<<std::endl;
  std::cout<<"\t"<<cmd<<" -i file -m file -o file"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"DESCRIPTION: - Computes  the cross correlation of two volumes :\n"<<std::endl;
  std::cout<<"\t "<<std::endl;
  std::cout<<std::endl;
  std::cout<<"\t -i  input volume"<<std::endl;
  std::cout<<"\t -m  mask volume"<<std::endl;
  std::cout<<"\t -o  output volume"<<std::endl;
  std::cout<<"\t -h, --help\t display this help and exit"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"AUTHOR:"<<std::endl;
  std::cout<<"\t Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"REPORTING BUGS:"<<std::endl;
  std::cout<<"\t Report bugs to slip trac serveur"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"COPYRIGHT:"<<std::endl;
  std::cout<<"\t This is free software; see the source for copying conditions."<<std::endl;
  std::cout<<std::endl;
  std::cout<<"SEE ALSO:"<<std::endl;
  std::cout<<std::endl;
}
