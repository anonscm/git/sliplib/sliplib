#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include "linear_least_squares.hpp"
#include "polynomial_algo.hpp"
#include "arithmetic_op.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "Vector2d.hpp"
#include "Vector3d.hpp"


int main()
{

  double x[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0, 16.0 , 18.0, 5.0};
  double y[] ={7.0, 14.0, 11.0, 21.0, 15.0, 17.0, 31.0, 23.0, 25.0, 33.0 , 37.0, 11.0};


std::size_t data_size=12;
std::size_t coeff_size=3;
  slip::Vector<double> datax(data_size,x);
  slip::Vector<double> datay(data_size,y);
  slip::Vector<double> datasig(data_size,1.0);
  slip::Vector<double> coeff(coeff_size,1.0);
 //  slip::Matrix<double> U(data_size,coeff_size,0.0);
//   slip::Vector<double> W(coeff_size,0.0);
//   slip::Matrix<double> V(coeff_size,coeff_size,0.0);
  double chisq;

  //  typedef slip::Vector<double>::iterator iterator;
  slip::EvalPowerBasis<double,slip::Vector<double>::iterator> power_basis;
  chisq = 
    slip::svd_least_square(datax.begin(),
			   datax.end(),
			   datay.begin(),
			   datasig.begin(), 
			   coeff.begin(),
			   coeff.end(),
  			   power_basis);
  std::cout<< coeff<<std::endl;
  std::cout<< chisq<<std::endl;
  
  chisq = 
    slip::svd_least_square(datax,
			   datay,
			   datasig, 
			   coeff,
			   power_basis);
  std::cout<< coeff<<std::endl;
  std::cout<< chisq<<std::endl;
 
  
  chisq = 
    slip::svd_least_square(datax.begin(),
			   datax.end(),
			   datay.begin(),
			   coeff.begin(),
			   coeff.end(),
  			   power_basis);
  std::cout<< coeff<<std::endl;
  std::cout<< chisq<<std::endl;

  chisq = 
    slip::svd_least_square(datax,
			   datay,
			   coeff,
			   power_basis);
  std::cout<< coeff<<std::endl;
  std::cout<< chisq<<std::endl;

  ////////////////////////////////////////
  //2d polynomial fitting
  ////////////////////////////////////////
  
  //construction of the 2d-polynomial data
  std::size_t n = 32;
  slip::Vector<double> x2(n+1);
  slip::iota(x2.begin(),x2.end(),-1.0,1.0/double(n));
  slip::Vector<double> y2(x2);
  slip::Vector<slip::Vector2d<double> > datax2(x2.size()*x2.size());  
  std::size_t k = 0;
  for(std::size_t i = 0; i < y2.size(); ++i)
     {
       for(std::size_t j = 0; j < x2.size(); ++j)
	 {
	   datax2[k][0] = x2[i];
	   datax2[k][1] = y2[j];
	   k++;
	 }
     }
 slip::Vector<double> datay2(datax2.size(),0.0);
 
 for(std::size_t k = 0; k < datax2.size(); ++k)
     {
       double x = datax2[k][0];
       double y = datax2[k][1];
       datay2[k] =  1.2 + 2.0 * x + 0.5* x*x - 1.0 * y + 0.8 * x *
        y + 0.3 * y * y;
       //datay2[k] =  1.2 + 0.5* x*x + 2.2 * y * y;
     }

 //polynomial fitting
   slip::Vector<double> datasig2(datax2.size(),1.0);
   std::size_t coeff_size2 = 6;
   slip::Vector<double> coeff2(coeff_size2,1.0);
   
    slip::EvalPower2dBasis<slip::Vector2d<double>,slip::Vector<double>::iterator> power2d_basis;
   slip::Vector<double> power_2d(6);
    power2d_basis(datax2[3],2,power_2d.begin(),power_2d.end());
    std::cout<<power_2d<<std::endl;
    chisq = 
    slip::svd_least_square_nd(datax2.begin(),
			      datax2.end(),
			      datay2.begin(),
			      datasig2.begin(),
			      coeff2.begin(),
			      coeff2.end(),
			      slip::Vector2d<double>::SIZE,
			      power2d_basis);
     std::cout<< coeff2<<std::endl;
     std::cout<< chisq<<std::endl;

      chisq = 
    slip::svd_least_square_nd(datax2.begin(),
			      datax2.end(),
			      datay2.begin(),
			      coeff2.begin(),
			      coeff2.end(),
			      slip::Vector2d<double>::SIZE,
			      power2d_basis);
     std::cout<< coeff2<<std::endl;
     std::cout<< chisq<<std::endl;

  chisq = 
    slip::svd_least_square_nd(datax2,
			      datay2,
			      coeff2,
			      slip::Vector2d<double>::SIZE,
			      power2d_basis);
     std::cout<< coeff2<<std::endl;
     std::cout<< chisq<<std::endl;


   ////////////////////////////////////////
  //2nd order 3d polynomial fitting
  ////////////////////////////////////////
  
  //construction of the 3d-polynomial data
  n = 32;
  slip::Vector<double> x3(n+1);
  slip::iota(x3.begin(),x3.end(),-1.0,1.0/double(n));
  slip::Vector<double> y3(x3);
  slip::Vector<double> z3(x3);
  slip::Vector<slip::Vector<double> > datax3(x3.size()*x3.size()*x3.size());  
  k = 0;
  for(std::size_t i = 0; i < y3.size(); ++i)
     {
       for(std::size_t j = 0; j < x3.size(); ++j)
	 {
	   for(std::size_t l = 0; l < z3.size(); ++l)
	     {
	       datax3[k].resize(3);
	       datax3[k][0] = x3[i];
	       datax3[k][1] = y3[j];
	       datax3[k][2] = z3[l];
	       k++;
	     }
	 }
     }
 slip::Vector<double> datay3(datax3.size(),0.0);
 
 for(std::size_t k = 0; k < datax3.size(); ++k)
     {
       double x = datax3[k][0];
       double y = datax3[k][1];
       double z = datax3[k][2];
       datay3[k] =  1.2 + 2.0 * x + 0.5* x*x - 1.0 * y + 0.8 * x *
        y + 0.3 * y * y + 1.1*z*y + 1.5 * z * z;
     }

 //2nd order 3d-polynomial fitting
   slip::Vector<double> datasig3(datax3.size(),1.0);
   std::size_t coeff_size3 = 10;
   slip::Vector<double> coeff3(coeff_size3,1.0);
   
   slip::EvalPowerNdBasis<slip::Vector<double>,slip::Vector<double>::iterator> power3d_basis;
   
  
   chisq = 
     slip::svd_least_square_nd(datax3.begin(),
			    datax3.end(),
			    datay3.begin(),
			    datasig3.begin(),
			    coeff3.begin(),
			    coeff3.end(),
			       2,
			    power3d_basis);
   std::cout<< coeff3<<std::endl;
   std::cout<< chisq<<std::endl;

   chisq = 
     slip::svd_least_square_nd(datax3.begin(),
			    datax3.end(),
			    datay3.begin(),
			    coeff3.begin(),
			    coeff3.end(),
			    2,
			    power3d_basis);
     std::cout<< coeff3<<std::endl;
     std::cout<< chisq<<std::endl;
     
     chisq = 
       slip::svd_least_square_nd(datax3,
			      datay3,
			      coeff3,
			      2,
			      power3d_basis);
     std::cout<< coeff3<<std::endl;
     std::cout<< chisq<<std::endl;

     std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "pseudo inverse " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> M1(2,5);
  slip::iota(M1.begin(),M1.end(),1.0,2.0);
  slip::Matrix<double> M2(2,5);
  slip::iota(M2.begin(),M2.end(),1.0,1.0);
  slip::Matrix<double> A;
  
  slip::pseudo_inverse(M1,M2,A);
  std::cout<<"M1 = \n"<<M1<<std::endl;
  std::cout<<"M2 = \n"<<M2<<std::endl;
  std::cout<<"A = \n"<<A<<std::endl;
}

