#include <iostream>
#include <iomanip>
#include "optimization.hpp"

template<typename Real>
struct cubic_func : public std::unary_function<Real,Real>
{

  Real operator()(const Real& x)
  {
    return x*x*x-static_cast<Real>(2.0)*x-static_cast<Real>(5.0);
  }
};

int main()
{
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Newton-Raphson " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::size_t iterations = 200;
  double h = 0.0001;
  double x0 = 2.0;
  double tolerance = 1E-06;
  double error = 0.0;
  cubic_func<double> cubic_f;
  double x = slip::newton_raphson(cubic_f,x0,h,iterations,error,tolerance);
  std::cout<<"Solution of x^3-2x-5 = 0:"<<std::endl;
  std::cout<<"x = "<<x<<" iterations = "<<iterations<<" error = "<<error<<std::endl;

return 0;
}
