#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <vector>
#include "statistics.hpp"
#include "Array2d.hpp"
#include "Matrix.hpp"
#include "Array3d.hpp"
#include "arithmetic_op.hpp"

bool myPredicate (const double& val)
{
  return (val<10);
  //return (val> 0);
  //return (val< 0);
    
}


int main()
{

  float f[] = {1.1650,0.6268,0.0751,0.3516,-0.6965};
  slip::Array<float> F(5,f);
  std::cout<<"F = \n"<<F<<std::endl;
  float meanF = slip::mean<float>(F.begin(),F.end());
  std::cout<<"kurtosis = "<<slip::kurtosis(F.begin(),F.end(),meanF)<<std::endl;
    
  std::cout<<"unbiased_kurtosis = "<<slip::unbiased_kurtosis(F.begin(),F.end(),meanF)<<std::endl;
  std::cout<<"skewness = "<<slip::skewness(F.begin(),F.end(),meanF)<<std::endl;
  std::cout<<"unbiased_skewness = "<<slip::unbiased_skewness(F.begin(),F.end(),meanF)<<std::endl;

  

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Descriptive statistics " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array2d<int> M(4,5);
  slip::iota(M.begin(),M.end(),20,-1);
  
  std::cout<<"M = \n"<<M<<std::endl;
  std::cout<<"cardinal = "<<slip::cardinal<int>(M.begin(),M.end())<<std::endl;
  double mean = slip::mean<double>(M.begin(),M.end());
  std::cout<<"mean = "<<mean<<std::endl;
  std::cout<<"variance = "<<slip::variance(M.begin(),M.end(),mean)<<std::endl;
  std::cout<<"unbiased_variance = "<<slip::unbiased_variance(M.begin(),M.end(),mean)<<std::endl;
  std::cout<<"standard deviation = "<<slip::std_dev(M.begin(),M.end(),mean)<<std::endl;
  std::cout<<"unbiased standard deviation = "<<slip::unbiased_std_dev(M.begin(),M.end(),mean)<<std::endl;
  std::cout<<"kurtosis = "<<slip::kurtosis(M.begin(),M.end(),mean)<<std::endl;
  std::cout<<"unbiased_kurtosis = "<<slip::unbiased_kurtosis(M.begin(),M.end(),mean)<<std::endl;
   
  std::cout<<"skewness = "<<slip::skewness(M.begin(),M.end(),mean)<<std::endl;
  std::cout<<"unbiased_skewness = "<<slip::unbiased_skewness(M.begin(),M.end(),mean)<<std::endl;
  std::cout<<"covariance(M,M) = "<<slip::covariance(M.begin(),M.end(),M.begin(),mean,mean)<<std::endl;
  std::cout<<"unbiased covariance(M,M) = "<<slip::unbiased_covariance(M.begin(),M.end(),M.begin(),mean,mean)<<std::endl;
  std::cout<<"RMS(M) = "<<slip::rms<double>(M.begin(),M.end())<<std::endl;
  std::cout<<"median = "<<slip::median(M.begin(),M.end())<<std::endl;
  std::cout<<"first_quartile = "<<slip::first_quartile(M.begin(),M.end())<<std::endl;
  std::cout<<"third_quartile = "<<slip::third_quartile(M.begin(),M.end())<<std::endl;
  
  slip::Statistics<double> S;
  slip::statistics(M.begin(),M.end(),S);
  std::cout<<"statistics "<<std::endl;
  std::cout<<S.all()<<std::endl;
  slip::unbiased_statistics(M.begin(),M.end(),S);
  std::cout<<"unbiased_statistics "<<std::endl;
  std::cout<<S.all()<<std::endl;
   
 
 
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Cardinal with slip structures " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array3d<int> MV(2,4,5);
  std::cout<<"cardinal MV = "<<slip::cardinal<int>(MV.begin(),MV.end())<<std::endl;
  std::cout<<"cardinal MV = "<<slip::cardinal<int>(MV.front_upper_left(),
						   MV.back_bottom_right())<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Median statistics " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  float f_odd[] = {2.0,4.2,-1.2,
		   4.1,2.2,0.0,
		   4.3,5.9,-3.2};
  slip::Array2d<float> M2(3,3,f_odd);
  slip::Array2d<float> M2_sorted(M2);
  std::sort(M2_sorted.begin(),M2_sorted.end());
  std::cout<<"M2 = \n "<<M2<<std::endl;
  std::cout<<"M2 sorted= \n "<<M2_sorted<<std::endl;
  std::cout<<"median = "<<slip::median(M2.begin(),M2.end())<<std::endl;
  std::cout<<"median_n = "<<slip::median_n(M2.begin(),M2.end(),M2.size())<<std::endl;
  std::cout<<"first_quartile = "<<slip::first_quartile(M2.begin(),M2.end())<<std::endl;
  std::cout<<"third_quartile = "<<slip::third_quartile(M2.begin(),M2.end())<<std::endl;
  

  float f_even[] = {2.0,4.2,-1.2,
		    4.1,2.2,0.0,
		    4.3,5.9};
  slip::Array2d<float> M3(4,2,f_even);
  std::cout<<"M3 = \n "<<M3<<std::endl;
  slip::Array2d<float> M3_sorted(M3);
  std::sort(M3_sorted.begin(),M3_sorted.end());
  std::cout<<"M3 sorted= \n "<<M3_sorted<<std::endl;
  std::cout<<"median = "<<slip::median_from_data_n(M3.begin(),M3.end(),M3.size())<<std::endl;
  std::cout<<"median = "<<slip::median(M3.begin(),M3.end())<<std::endl;
  std::cout<<"median_n = "<<slip::median_n(M3.begin(),M3.end(),M3.size())<<std::endl;
  std::cout<<"first_quartile = "<<slip::first_quartile(M3.begin(),M3.end())<<std::endl;
  std::cout<<"third_quartile = "<<slip::third_quartile(M3.begin(),M3.end())<<std::endl;


  float f_odd2[] = {2.0,4.2,-1.2,
		   4.1,2.2,0.0,
		    4.3,5.9,-3.2,5.5,7.7};
  slip::Array2d<float> M4(11,1,f_odd2);
  slip::Array2d<float> M4_sorted(M4);
  std::sort(M4_sorted.begin(),M4_sorted.end());
  std::cout<<"M4 = \n "<<M4<<std::endl;
  std::cout<<"M4 sorted= \n "<<M4_sorted<<std::endl;
 
  std::cout<<"median = "<<slip::median_from_data_n(M4.begin(),M4.end(),M4.size())<<std::endl;
  std::cout<<"median = "<<slip::median(M4.begin(),M4.end())<<std::endl;
  std::cout<<"median_n = "<<slip::median_n(M4.begin(),M4.end(),M4.size())<<std::endl;
  std::cout<<"first_quartile = "<<slip::first_quartile(M4.begin(),M4.end())<<std::endl;
  std::cout<<"third_quartile = "<<slip::third_quartile(M4.begin(),M4.end())<<std::endl;
  
  float f_even2[] = {2.0,4.2,-1.2,
		    4.1,2.2,0.0,
		     4.3,5.9,2.3,1.2};
  slip::Array2d<float> M5(5,2,f_even2);
  std::cout<<"M5 = \n "<<M5<<std::endl;
  slip::Array2d<float> M5_sorted(M5);
  std::sort(M5_sorted.begin(),M5_sorted.end());
  std::cout<<"M5 sorted= \n "<<M5_sorted<<std::endl;
  std::cout<<"median = "<<slip::median_from_data_n(M5.begin(),M5.end(),M5.size())<<std::endl;
  std::cout<<"median = "<<slip::median(M5.begin(),M5.end())<<std::endl;
  std::cout<<"median_n = "<<slip::median_n(M5.begin(),M5.end(),M5.size())<<std::endl;
  std::cout<<"first_quartile = "<<slip::first_quartile(M5.begin(),M5.end())<<std::endl;
  std::cout<<"third_quartile = "<<slip::third_quartile(M5.begin(),M5.end())<<std::endl;


  std::cout<<slip::median(M.begin(),M.end())<<std::endl;
  std::cout<<slip::median(M.upper_left(),M.bottom_right())<<std::endl;
  std::cout<<slip::median_n(M.begin(),M.end(),M.rows()*M.cols())<<std::endl;
  std::cout<<slip::median_n(M.upper_left(),M.bottom_right(),M.rows()*M.cols())<<std::endl;
  std::cout<<slip::median(M.begin(),M.end(),std::less<float>())<<std::endl;
  std::cout<<slip::median_n(M.begin(),M.end(),M.rows()*M.cols(),std::less<float>())<<std::endl;
  
   std::cout<<slip::median_from_data_n(M.begin(),M.end(),M.rows()*M.cols(),std::less<float>())<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "weighted mean " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  float weight[] = {1.0,2.0,1.0};
  std::cout<<"weight = "<<std::endl;
  std::copy(weight,weight+3,std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  float values[] = {5.0,4.0,12.0};
  std::cout<<"values = "<<std::endl;
  std::copy(values,values+3,std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"weighted mean = "<<slip::weighted_mean<float>(values,values+3,weight,weight+3)<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "center " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array2d<float> Mc(3,3);
  slip::iota(Mc.begin(),Mc.end(),1.0);
  std::cout<<"Mc = \n"<<Mc<<std::endl;
  slip::Array2d<float> Mc2(3,3);
  std::cout<<"center(Mc) = "<<std::endl;
  slip::center(Mc.begin(),Mc.end(),Mc2.begin());
  std::cout<<"Mc2 = \n"<<Mc2<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "studentize " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  std::cout<<"Mc = \n"<<Mc<<std::endl;
  std::cout<<"studentize(Mc) = "<<std::endl;
  slip::studentize(Mc.begin(),Mc.end(),Mc2.begin());
  std::cout<<"Mc2 = \n"<<Mc2<<std::endl;

  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "n_max_elements " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  int tab[] = {12,6,6,9,10,1,5,0,-1};
  std::vector<int*> max;
  
  std::copy(tab,tab+9,std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  max.resize(1);
  slip::n_max_elements(tab,tab+9,max);
  std::cout<<"---------------------"<<std::endl;
  for(std::size_t i = 0; i < max.size();++i)
	 {
	   std::cout<<"max["<<i<<"] = "<<*(max[i])<<" ";
	 }
  std::cout<<std::endl;
  std::cout<<"---------------------"<<std::endl;
  max.resize(2);
  slip::n_max_elements(tab,tab+9,max,2);
  std::cout<<"---------------------"<<std::endl;
  for(std::size_t i = 0; i < max.size();++i)
    {
      std::cout<<"max["<<i<<"] = "<<*(max[i])<<" ";
    }
  std::cout<<std::endl;
  std::cout<<"---------------------"<<std::endl;
   max.resize(3);
  slip::n_max_elements(tab,tab+9,max,3);
  for(std::size_t i = 0; i < max.size();++i)
    {
      std::cout<<"max["<<i<<"] = "<<*(max[i])<<" ";
    }
  std::cout<<std::endl;
 std::cout<<"---------------------"<<std::endl;
 max.resize(9);
 slip::n_max_elements(tab,tab+9,max,9);
 std::cout<<"---------------------"<<std::endl;
 for(std::size_t i = 0; i < max.size();++i)
   {
     std::cout<<"max["<<i<<"] = "<<*(max[i])<<" ";
   }
 std::cout<<std::endl;
 


slip::Array2d<float> Mf(5,6);
  float valf = 30.0;
  for(std::size_t i = 0; i < Mf.dim1(); ++i)
    {
      for(std::size_t j = 0; j < Mf.dim2(); ++j)
	{
	  Mf(i,j) = valf;
	  valf -= 1.0;
	}
    }

  slip::Box2d<int> boxf(1,1,2,3);
  std::cout<<Mf<<std::endl;
  slip::Array2d<float>::iterator2d itf = std::max_element(Mf.upper_left(boxf),Mf.bottom_right(boxf));
  std::cout<<"max element in the box (1,1,2,3) = "<<*itf<<std::endl;
  std::cout<<" i = "<<itf.i()<<" j = "<<itf.j()<<std::endl;
  
  std::cout<<"number of elements within the box = "<<boxf.area()<<std::endl;
  std::vector<slip::Array2d<float>::iterator2d> maxf(3);
  slip::n_max_elements(Mf.upper_left(boxf),Mf.bottom_right(boxf),maxf,3);
  for(std::size_t i = 0; i < maxf.size();++i)
    {
      std::cout<<"maxf["<<i<<"] = "<<*maxf[i]<<" ";
      std::cout<<"maxf["<<i<<"] = ("<<maxf[i].i()<<" "<<maxf[i].j()<<")"<<"; ";
    }
  std::cout<<std::endl;

  std::cout<<"maxf[0][DPoint2d<int>(1,2)]  = "<<maxf[0][slip::DPoint2d<int>(1,2)]<<std::endl; 
 
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "n_min_elements " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::vector<slip::Array2d<float>::iterator2d> minf(3);
  slip::n_min_elements(Mf.upper_left(boxf),Mf.bottom_right(boxf),minf,3);
  for(std::size_t i = 0; i < minf.size();++i)
    {
      std::cout<<"minf["<<i<<"] = "<<*minf[i]<<" ";
      std::cout<<"minf["<<i<<"] = ("<<minf[i].i()<<" "<<minf[i].j()<<")"<<"; ";
    }
  std::cout<<std::endl;

  std::cout<<"minf[0][DPoint2d<int>(1,2)]  = "<<minf[0][slip::DPoint2d<int>(1,2)]<<std::endl; 



//--------------  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Descriptive statistics mask " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  //slip::Array2d<bool> Mask(4,5,false);
  slip::Array2d<bool> Mask(4,5,true);
  for(std::size_t i = 0; i < M.dim1(); ++i)
    {
	Mask[i][0]=false;
    }  
  for(std::size_t j = 0; j < M.dim2(); ++j)
    {
	Mask[0][j]=false;
    }  

  slip::Array2d<int> ValuedMask(4,5,2);
  for(std::size_t i = 0; i < M.dim1(); ++i)
    {
	ValuedMask[i][0]= 0;
    }  
  for(std::size_t j = 0; j < M.dim2(); ++j)
    {
	ValuedMask[0][j]=0;
    }  
  std::cout << "Data : "<<std::endl<<M<<std::endl;
  std::cout << "Mask : "<< std::endl<<Mask<<std::endl;	
  std::cout << "ValuedMask : "<< std::endl<<ValuedMask<<std::endl;	
 
  std::cout<<"cardinal = "<<slip::cardinal_mask<int>(Mask.begin(),Mask.end())<<std::endl;
  std::cout<<"cardinal value = 2 "<<slip::cardinal_mask<int>(ValuedMask.begin(),ValuedMask.end(),2)<<std::endl;
  double mean_mask=slip::mean_mask<double>(M.begin(),M.end(),Mask.begin());
  std::cout<<"mean = "<<mean_mask<<std::endl;
  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
  std::cout<<"mean value = 2 = "<<mean_mask2<<std::endl;
  std::cout<<"variance = "<<slip::variance_mask(M.begin(),M.end(),Mask.begin(),mean_mask)<<std::endl;
  std::cout<<"unbiased variance = "<<slip::unbiased_variance_mask(M.begin(),M.end(),Mask.begin(),mean_mask)<<std::endl;
 
  
  std::cout<<"variance value = 2 = "<<slip::variance_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
   std::cout<<"unbiased variance value = 2 = "<<slip::unbiased_variance_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
  std::cout<<"standard deviation = "<<slip::std_dev_mask(M.begin(),M.end(),Mask.begin(),mean_mask)<<std::endl;
   std::cout<<"unbiased standard deviation = "<<slip::unbiased_std_dev_mask(M.begin(),M.end(),Mask.begin(),mean_mask)<<std::endl;
  std::cout<<"standard deviation value = 2 = "<<slip::std_dev_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
  std::cout<<"unbiased standard deviation value = 2 = "<<slip::unbiased_std_dev_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
  std::cout<<"kurtosis = "<<slip::kurtosis_mask(M.begin(),M.end(),Mask.begin(),mean_mask)<<std::endl;
  std::cout<<"unbiased kurtosis = "<<slip::unbiased_kurtosis_mask(M.begin(),M.end(),Mask.begin(),mean_mask)<<std::endl;
  std::cout<<"kurtosis value = 2 = "<<slip::kurtosis_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
  std::cout<<"unbiased kurtosis value = 2 = "<<slip::unbiased_kurtosis_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
  std::cout<<"skewness = "<<slip::skewness_mask(M.begin(),M.end(),Mask.begin(),mean_mask)<<std::endl;
  std::cout<<"unbiased skewness = "<<slip::unbiased_skewness_mask(M.begin(),M.end(),Mask.begin(),mean_mask)<<std::endl;
  std::cout<<"skewness value = 2 = "<<slip::skewness_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
  std::cout<<"unbiased skewness value = 2 = "<<slip::unbiased_skewness_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
  std::cout<<"median = "<<slip::median_mask(M.begin(),M.end(),Mask.begin())<<std::endl;
  std::cout<<"median value = 2 = "<<slip::median_mask(M.begin(),M.end(),ValuedMask.begin(),2)<<std::endl;
   std::cout<<"first quartile = "<<slip::first_quartile_mask(M.begin(),M.end(),Mask.begin())<<std::endl;
   std::cout<<"first quartile value = 2 = "<<slip::first_quartile_mask(M.begin(),M.end(),ValuedMask.begin(),2)<<std::endl;
   std::cout<<"third quartile = "<<slip::third_quartile_mask(M.begin(),M.end(),Mask.begin())<<std::endl;
   std::cout<<"third quartile value = 2 = "<<slip::third_quartile_mask(M.begin(),M.end(),ValuedMask.begin(),2)<<std::endl;
  std::cout<<"covariance(M,M) = "<<slip::covariance_mask(M.begin(),M.end(),M.begin(),Mask.begin(),mean_mask,mean_mask)<<std::endl;
  std::cout<<"unbiased_covariance(M,M) = "<<slip::unbiased_covariance_mask(M.begin(),M.end(),M.begin(),Mask.begin(),mean_mask,mean_mask)<<std::endl;
  std::cout<<"RMS(M) = "<<slip::rms_mask<double>(M.begin(),M.end(),Mask.begin())<<std::endl;

  std::cout<<"covariance(M,M) value = 2 = "<<slip::covariance_mask(M.begin(),M.end(),M.begin(),ValuedMask.begin(),mean_mask2,mean_mask2,2)<<std::endl;
  std::cout<<"unbiased_covariance(M,M) value = 2 = "<<slip::unbiased_covariance_mask(M.begin(),M.end(),M.begin(),ValuedMask.begin(),mean_mask2,mean_mask2,2)<<std::endl;
  std::cout<<"RMS(M) value = 2 = "<<slip::rms_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2)<<std::endl;



  slip::statistics_mask(M.begin(),M.end(),Mask.begin(),S);
  std::cout<<"statistics mask"<<std::endl;
  std::cout<<S.all()<<std::endl;
  slip::unbiased_statistics_mask(M.begin(),M.end(),Mask.begin(),S);
  std::cout<<"unbiased_statistics mask"<<std::endl;
  std::cout<<S.all()<<std::endl;

  slip::statistics_mask(M.begin(),M.end(),ValuedMask.begin(),S,2);
  std::cout<<"statistics mask value = 2"<<std::endl;
  std::cout<<S.all()<<std::endl;
  slip::unbiased_statistics_mask(M.begin(),M.end(),ValuedMask.begin(),S,2);
  std::cout<<"unbiased_statistics mask value = 2"<<std::endl;
  std::cout<<S.all()<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "center mask " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array2d<float> Mcmask(4,5);
  slip::iota(Mcmask.begin(),Mcmask.end(),1.0);
  std::cout<<"Mcmask = \n"<<Mcmask<<std::endl;
  slip::Array2d<float> Mcmask2(4,5);
  slip::center_mask(Mcmask.begin(),Mcmask.end(),
		    ValuedMask.begin(),
		    Mcmask2.begin(),2);
  std::cout<<"Mcmask2 = \n"<<Mcmask2<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "studentize mask " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::studentize_mask(Mcmask.begin(),Mcmask.end(),
			ValuedMask.begin(),
			Mcmask2.begin(),2);
  std::cout<<"Mcmask2 = \n"<<Mcmask2<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Descriptive statistics using a predicate less than 10" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;


  std::cout << "Data : "<<std::endl<<M<<std::endl;
  double mean_if=slip::mean_if<double>(M.begin(),M.end(),myPredicate);
  std::cout<<"cardinal = "<<slip::cardinal_if<int>(Mask.begin(),Mask.end(),myPredicate)<<std::endl;
  std::cout<<"mean = "<<mean_if<<std::endl;
  std::cout<<"variance = "<<slip::variance_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
  std::cout<<"unbiased variance = "<<slip::unbiased_variance_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
  std::cout<<"standard deviation = "<<slip::std_dev_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
  std::cout<<"unbiased standard deviation = "<<slip::unbiased_std_dev_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
  std::cout<<"kurtosis = "<<slip::kurtosis_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
  std::cout<<"unbiased kurtosis = "<<slip::unbiased_kurtosis_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
  std::cout<<"skewness = "<<slip::skewness_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
  std::cout<<"unbiased skewness = "<<slip::unbiased_skewness_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
  

  std::cout<<"median = "<<slip::median_if(M.begin(),M.end(),myPredicate)<<std::endl;
  std::cout<<"first quartile = "<<slip::first_quartile_if(M.begin(),M.end(),myPredicate)<<std::endl;
  std::cout<<"third quartile = "<<slip::third_quartile_if(M.begin(),M.end(),myPredicate)<<std::endl;


 
  std::cout<<"covariance(M,M) = "<<slip::covariance_if(M.begin(),M.end(),M.begin(),mean_if,mean_if,myPredicate)<<std::endl;
  std::cout<<"unbiased covariance(M,M) = "<<slip::unbiased_covariance_if(M.begin(),M.end(),M.begin(),mean_if,mean_if,myPredicate)<<std::endl;
  std::cout<<"RMS(M) = "<<slip::rms_if<double>(M.begin(),M.end(),myPredicate)<<std::endl;



  slip::statistics_if(M.begin(),M.end(),S,myPredicate);
  std::cout<<"statistics using a predicate"<<std::endl;
  std::cout<<S.all()<<std::endl;
  slip::unbiased_statistics_if(M.begin(),M.end(),S,myPredicate);
  std::cout<<"unbiased_statistics using a predicate"<<std::endl;
  std::cout<<S.all()<<std::endl;

  

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "center using a predicate " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"M = \n"<<M<<std::endl;
  slip::Array2d<float> Mcif2(4,5);
  slip::center_if(M.begin(),M.end(),myPredicate,Mcif2.begin());
  std::cout<<"Mcif2 = \n"<<Mcif2<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "studentize using a predicate " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"M = \n"<<M<<std::endl;
  slip::studentize_if(M.begin(),M.end(),myPredicate,Mcif2.begin());
  std::cout<<"Mcif2 = \n"<<Mcif2<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "covariance matrix...  " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  double data[] = {1,    7,    3,    4,
                   5,    4,    7,    8,
		   9,   20,    1,   12};
  slip::Matrix<double> A(3,4,data,data+12);
  std::cout<<"A = \n"<<A<<std::endl;
  slip::Matrix<double> CovA(A.cols(),A.cols());
  slip::covariance_matrix(A,CovA);
  std::cout<<"CovA = \n"<<CovA<<std::endl;
  slip::Matrix<double> UnCovA(A.cols(),A.cols());
  slip::unbiased_covariance_matrix(A,UnCovA);
  std::cout<<"unbiased CovA = \n"<<UnCovA<<std::endl;
  
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "recursive mean...  " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  slip::Array<int> x(8);
  slip::iota(x.begin(),x.end(),1,1); 
  std::cout<<"x = \n"<<x<<std::endl;
  double mu = static_cast<double>(x[0]);
  std::cout<<"mu = "<<mu<<std::endl;
  for(int i = 1; i < 8; ++i)
    {
      mu = slip::mean_next(i,mu,x[i]);
      std::cout<<"mu = "<<mu<<std::endl;
    }
  std::cout<<"slip::mean = "<<slip::mean<double>(x.begin(),x.end())<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "recursive variance...  " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout<<"x = \n"<<x<<std::endl;
  mu = static_cast<double>(x[0]);
  double var = 0.0;
  std::cout<<"mu = "<<mu<<" var = "<<var<<std::endl;
  for(int i = 1; i < 8; ++i)
    {
      slip::var_next(i,mu,var,x[i],mu,var);
      std::cout<<"mu = "<<mu<<" var = "<<var<<std::endl;
    }
  std::cout<<"slip::mean = "<<slip::mean<double>(x.begin(),x.end())<<std::endl;
  std::cout<<"slip::variance = "<<slip::variance(x.begin(),x.end(),slip::mean<double>(x.begin(),x.end()))<<std::endl;
  
  

}
