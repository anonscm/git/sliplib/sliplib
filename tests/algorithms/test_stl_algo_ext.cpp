#include <iostream>
#include "Array.hpp"
#include "Array2d.hpp"
#include "arithmetic_op.hpp"
#include "stl_algo_ext.hpp"


template<typename T>
bool lt5Predicate (const T& val)
{
  return (val < T(5));
}

template<typename T>
bool lt1Predicate (const T& val)
{
  return (val < T(0.1));
}

template<typename T>
bool neverPredicate (const T& val)
{
  return (val> T(1000));
}

template<typename T>
 bool compare(const T val1, const T val2)
 {
   return (std::abs(val1) < std::abs(val2)); 
 }
int main()
{  
  
  slip::Array<int> v1(10);
  slip::iota(v1.begin(),v1.end(),1,1);
  std::cout<<"v1 = "<<v1<<std::endl;
  slip::Array<int> v2(10);
  slip::Array<bool> mask(v1.size());
  mask[2] = 1;
  mask[4] = 1;
  mask[8] = 1;
  slip::Array<int> maskint(v1.size());
  maskint[2] = 2;
  maskint[4] = 2;
  maskint[8] = 2;
  std::cout<<"mask = "<<mask<<std::endl;
  slip::transform_mask_un(v1.begin(),v1.end(),
		       mask.begin(),
		       v2.begin(),
		       std::negate<int>());
  std::cout<<"v2 = "<<v2<<std::endl;
  slip::transform_mask_un(v1.begin(),v1.end(),
		       maskint.begin(),
		       v2.begin(),
		       std::negate<int>(),int(2));
  std::cout<<"v2 = "<<v2<<std::endl;

  v2= 0;
  std::cout<<"v1 = "<<v1<<std::endl;
  slip::transform_if(v1.begin(),v1.end(),
		      v2.begin(),
		      std::negate<int>(),
		      lt5Predicate<int>);
  std::cout<<"v2 = "<<v2<<std::endl;

  v2.fill(0);
  std::cout<<"v2 = v1 = "<<v2<<std::endl;
 
  slip::Array<int> v3(v1.size());
  slip::transform_mask_bin(v1.begin(),v1.end(),
		       mask.begin(),
		       v2.begin(),
		       v3.begin(),
		       std::plus<int>());
   std::cout<<"v3 = "<<v3<<std::endl;
  slip::transform_mask_bin(v1.begin(),v1.end(),
		       maskint.begin(),
		       v2.begin(),
		       v3.begin(),
			   std::plus<int>(),int(2));
   std::cout<<"v3 = "<<v3<<std::endl;
   v3.fill(0);
   v2=v1;
   slip::transform_if(v1.begin(),v1.end(),
		      v2.begin(),
		      v3.begin(),
		      std::plus<int>(),
		      lt5Predicate<int>);
   std::cout<<"v3 = "<<v3<<std::endl;

   
   float f[] = {5.0,2.0,1.0,7.0,3.0,0.5};
   slip::Array2d<float> M(2,3,f);
   int d[] = {1,1,0,0,1,1};
   slip::Array2d<int> MaskM(2,3,d);
   int nullm[] = {0,0,0,0,0,0};
   slip::Array2d<int> NullMask(2,3,nullm);
   std::cout<<M<<std::endl;
   std::cout<<MaskM<<std::endl;
   slip::Array2d<float>::iterator it = slip::max_element_mask(M.begin(),M.end(),MaskM.begin(),1);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"nul mask ";
     }
   it = slip::max_element_mask(M.begin(),M.end(),MaskM.begin());
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"nul mask"<<std::endl;
     }
   it = slip::max_element_mask(M.begin(),M.end(),NullMask.begin(),1);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"nul mask "<<std::endl;
     }

  it = slip::min_element_mask(M.begin(),M.end(),MaskM.begin(),1);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"nul mask ";
     }
   it = slip::min_element_mask(M.begin(),M.end(),MaskM.begin());
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"nul mask"<<std::endl;
     }
   it = slip::min_element_mask(M.begin(),M.end(),NullMask.begin(),1);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"nul mask "<<std::endl;
     }
   std::cout<<M<<std::endl;
   it = slip::max_element_if(M.begin(),M.end(),lt5Predicate<float>);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
    else
     {
       std::cout<<"predicate never verified "<<std::endl;
     }
   it = slip::max_element_if(M.begin(),M.end(),neverPredicate<float>);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
    else
     {
       std::cout<<"predicate never verified "<<std::endl;
     }
   it = slip::min_element_if(M.begin(),M.end(),lt5Predicate<float>);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
    else
     {
       std::cout<<"predicate never verified "<<std::endl;
     }
   it = slip::min_element_if(M.begin(),M.end(),neverPredicate<float>);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
    else
     {
       std::cout<<"predicate never verified "<<std::endl;
     }

  
  

   slip::iota(M.begin(),M.end(),0.6,-0.5);
   M(0,0) = 0.0;
   M(1,2) = 0.25;
   std::cout<<M<<std::endl;
   std::cout<<MaskM<<std::endl;
   it = slip::max_element_mask_compare(M.begin(),M.end(),MaskM.begin(),compare<float>,1);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"nul mask"<<std::endl;
     }
   it = slip::max_element_mask_compare(M.begin(),M.end(),MaskM.begin(),compare<float>,2);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"nul mask"<<std::endl;
     }
   it = slip::max_element_mask_compare(M.begin(),M.end(),MaskM.begin(),compare<float>);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"nul mask "<<std::endl;
     }

    it = slip::min_element_mask_compare(M.begin(),M.end(),MaskM.begin(),compare<float>,1);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"nul mask"<<std::endl;
     }
   it = slip::min_element_mask_compare(M.begin(),M.end(),MaskM.begin(),compare<float>,2);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"nul mask"<<std::endl;
     }
   it = slip::min_element_mask_compare(M.begin(),M.end(),MaskM.begin(),compare<float>);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"nul mask "<<std::endl;
     }
   it =slip::max_element_if_compare(M.begin(),M.end(),lt1Predicate<float>,compare<float>);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"predicate never verified "<<std::endl;
     }
   it =slip::max_element_if_compare(M.begin(),M.end(),neverPredicate<float>,compare<float>);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"predicate never verified "<<std::endl;
     }
     it =slip::min_element_if_compare(M.begin(),M.end(),lt1Predicate<float>,compare<float>);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"predicate never verified "<<std::endl;
     }
   it =slip::min_element_if_compare(M.begin(),M.end(),neverPredicate<float>,compare<float>);
   if(it != M.end())
     {
       std::cout<<*it<<std::endl;
     }
   else
     {
       std::cout<<"predicate never verified "<<std::endl;
     }
  

  

  return 0;
}
