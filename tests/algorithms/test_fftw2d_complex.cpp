#include "FFT.hpp"
#include "Array.hpp"
#include <complex>
#include <iostream>
#include <vector>
#include "GrayscaleImage.hpp"
#include "dynamic.hpp"

int main()
{
#ifdef HAVE_FFTW 
  slip::GrayscaleImage<double> I;
  I.read("../images/lena_gray.gif");
      
  //fft2d
  slip::Matrix<std::complex<double> > Icp(I.dim1(),I.dim2(),(std::complex<double>)(0));
  std::copy(I.begin(),I.end(),Icp.begin());
  slip::Matrix<std::complex<double> > IFFT(I.dim1(),I.dim2());
  slip::Matrix<std::complex<double> > IOut(I.dim1(),I.dim2());
  slip::complex_fftw2d(Icp.upper_left(),Icp.bottom_right(),IFFT.upper_left());
  slip::GrayscaleImage<double> INorm(IFFT.dim1(),IFFT.dim2());
  //log(magnitude)
  for(size_t i = 0; i < IFFT.dim1(); ++i)
    {
      for(size_t j = 0; j < IFFT.dim2(); ++j)
	{
	  INorm[i][j] = std::log(1.0+std::abs(IFFT[i][j]));
	}
    }
  slip::fftshift2d(INorm.upper_left(),INorm.bottom_right());
  slip::change_dynamic_01(INorm.begin(),INorm.end(),INorm.begin(),slip::AFFINE_FUNCTION);
  INorm.write("lena_fftw_cp.gif");
  
  //ifft2d
  slip::ifftw2d(IFFT,IOut);
  
  for(size_t i = 0; i < IOut.dim1(); ++i)
    {
      for(size_t j = 0; j < IOut.dim2(); ++j)
	{
	  INorm[i][j] = std::abs(IOut[i][j]);
	}
    }
  
  slip::change_dynamic_01(INorm.begin(),INorm.end(),INorm.begin(),slip::AFFINE_FUNCTION);
  INorm.write("lena_fftw_cp_rec.gif");
      
  //std::transform(I2.begin(),I2.end(),Norm.begin(),std::ptr_fun(std::norm));
#else
  std::cout << "fftw not defined for SLIP use.\n";
#endif
}
