#include <complex>
#include <iostream>
#include <vector>

#include "FFT.hpp"
#include "Array.hpp"
#include "GrayscaleImage.hpp"
#include "dynamic.hpp"

int main()
{
  slip::GrayscaleImage<double> I;
  I.read("../images/lena_gray.gif");

  //range variables definition
  int pas = 2;
  int RanXmin = (int)((double)I.dim1()/4.0) + 3;
  int RanXmax = (int)((double)I.dim1()/2.0);
  int RanYmin = (int)((double)I.dim2()/4.0) + 5;
  int RanYmax = (int)((double)I.dim2()/2.0);
  int dimimoutX = (RanXmax-RanXmin)/pas+1;
  int dimimoutY = (RanYmax-RanYmin)/pas+1;
  std::cout << RanXmin << " , ";
  std::cout << RanXmax << " , ";
  std::cout << RanYmin << " , ";
  std::cout << RanYmax << std::endl;
  std::cout << dimimoutX << " , ";
  std::cout << dimimoutY << std::endl;
  
  //real_split_radix_fft2d on a range
  slip::Range<int> range_line(RanXmin,RanXmax,pas);
  slip::Range<int> range_col(RanYmin,RanYmax,pas);
  slip::Matrix<std::complex<double> > IRangeFFT(dimimoutX,dimimoutY);
  slip::Matrix<std::complex<double> > IRange(dimimoutX,dimimoutY);
  
  slip::real_split_radix_fft2d(I.upper_left(range_line,range_col),I.bottom_right(range_line,range_col),IRangeFFT.upper_left());
  
  slip::GrayscaleImage<double> IRangeNorm(IRange.dim1(),IRange.dim2());
  
  //log(magnitude)
  for(size_t i = 0; i < IRangeFFT.dim1(); ++i)
    {
      for(size_t j = 0; j < IRangeFFT.dim2(); ++j)
	{
	  IRangeNorm[i][j] = std::log(1.0+std::abs(IRangeFFT[i][j]));
	}
    }
  slip::fftshift2d(IRangeNorm.upper_left(),IRangeNorm.bottom_right());
  slip::change_dynamic_01(IRangeNorm.begin(),IRangeNorm.end(),IRangeNorm.begin(),slip::AFFINE_FUNCTION);
  IRangeNorm.write("lena_split_radix_fft_range.gif");

  //split_radix_ifft2d on the fft image 
  slip::split_radix_ifft2d(IRangeFFT.upper_left(),IRangeFFT.bottom_right(),IRange.upper_left());
  
  for(size_t i = 0; i < IRange.dim1(); ++i)
    {
      for(size_t j = 0; j < IRange.dim2(); ++j)
	{
	  IRangeNorm[i][j] = std::abs(IRange[i][j]);
	}
    }
  
  slip::change_dynamic_01(IRangeNorm.begin(),IRangeNorm.end(),IRangeNorm.begin(),slip::AFFINE_FUNCTION);
  IRangeNorm.write("lena_split_radix_fft_range_rec.gif");
  
  //std::transform(I2.begin(),I2.end(),Norm.begin(),std::ptr_fun(std::norm));
}
