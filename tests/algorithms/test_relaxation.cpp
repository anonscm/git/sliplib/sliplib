#include <iostream>
#include <iomanip>
#include "Matrix.hpp"
#include "Vector.hpp"
#include "linear_algebra.hpp"
#include "relaxation.hpp"

int main()
{

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "Gauss-Seidel " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  double taba[] = {10.0,-1.0,2.0,0.0,-1.0,11.0,-1.0,3.0,2.0,-1.0,10.0,-1.0,0.0,3.0,-1.0,8.0};
  slip::Matrix<double> A(4,4,taba);
  double tabb[] = {6.0,25.0,-11.0,15.0};
  slip::Vector<double> B(4,tabb); 
  slip::Vector<double> X0(B.size()); 
  slip::Vector<double> X(B.size());
  slip::Vector<double> R(B.size());
  double tol = 1e-6;

  slip::gauss_seidel(A,B,X0,X,tol,100);
  std::cout<<"A = \n"<<A<<std::endl;
  std::cout<<"B = \n"<<B<<std::endl;
  std::cout<<"X0 = \n"<<X0<<std::endl;
  std::cout<<"X = \n"<<X<<std::endl;
  
  slip::matrix_vector_multiplies(A,X,R);
  std::cout<<"R = AX\n"<<R<<std::endl;

  X.fill(0.0);
  slip::gauss_seidel(A,B,X0,X);
  std::cout<<"A = \n"<<A<<std::endl;
  std::cout<<"B = \n"<<B<<std::endl;
  std::cout<<"X0 = \n"<<X0<<std::endl;
  std::cout<<"X = \n"<<X<<std::endl;
  
  slip::matrix_vector_multiplies(A,X,R);
  std::cout<<"R = AX\n"<<R<<std::endl;

  X.fill(0.0);
  
  //std::cout<<"X0 = \n"<<X0<<std::endl;
  slip::gauss_seidel(A.upper_left(),
		     A.bottom_right(),
		     B.begin(),B.end(),
		     X0.begin(),X0.end(),
		     X.begin(),X.end(),
		     tol,
		     100);
  std::cout<<"X = \n"<<X<<std::endl;
  
  slip::matrix_vector_multiplies(A,X,R);
  
  std::cout<<"R = AX\n"<<R<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "SOR " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  //double w = 1.995;
  double w = 1.0;
  std::cout<<"A = \n"<<A<<std::endl;
  std::cout<<"B = \n"<<B<<std::endl;
  std::cout<<"X0 = \n"<<X0<<std::endl;
  std::cout<<"w = "<<w<<std::endl;
  X.fill(0.0);

  slip::SOR(A.upper_left(),A.bottom_right(),
	    B.begin(),B.end(),
	    X0.begin(),X0.end(),
	    w,
	    X.begin(),X.end(),
	    tol,
	    100);
  std::cout<<"X = \n"<<X<<std::endl;
  slip::matrix_vector_multiplies(A,X,R);
  std::cout<<"R = AX\n"<<R<<std::endl;

  X.fill(0.0);

  slip::SOR(A.upper_left(),A.bottom_right(),
	    B.begin(),B.end(),
	    X0.begin(),X0.end(),
	    w,
	    X.begin(),X.end());
  std::cout<<"X = \n"<<X<<std::endl;
  slip::matrix_vector_multiplies(A,X,R);
  std::cout<<"R = AX\n"<<R<<std::endl;

  X.fill(0.0);
  slip::SOR(A,B,X0,w,X,tol,100);
  std::cout<<"X = \n"<<X<<std::endl;
  slip::matrix_vector_multiplies(A,X,R);
  std::cout<<"R = AX\n"<<R<<std::endl;
   
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Jacobi " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"A = \n"<<A<<std::endl;
  std::cout<<"B = \n"<<B<<std::endl;
  std::cout<<"X0 = \n"<<X0<<std::endl;
 
  X.fill(0.0);
  slip::jacobi(A.upper_left(),A.bottom_right(),
	       B.begin(),B.end(),
	       X0.begin(),X0.end(),
	       X.begin(),X.end(),
	       tol,100);
  std::cout<<"X = \n"<<X<<std::endl;
  slip::matrix_vector_multiplies(A,X,R);
  std::cout<<"R = AX\n"<<R<<std::endl;
  
  X.fill(0.0);
  slip::jacobi(A.upper_left(),A.bottom_right(),
	       B.begin(),B.end(),
	       X0.begin(),X0.end(),
	       X.begin(),X.end());
  std::cout<<"X = \n"<<X<<std::endl;
  slip::matrix_vector_multiplies(A,X,R);
  std::cout<<"R = AX\n"<<R<<std::endl;
  X.fill(0.0);
  slip::jacobi(A,B,X0,X,tol,100);
  std::cout<<"X = \n"<<X<<std::endl;
  slip::matrix_vector_multiplies(A,X,R);
  std::cout<<"R = AX\n"<<R<<std::endl;
  
  return 0;
}
