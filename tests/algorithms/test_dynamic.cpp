#include <iostream>
#include <algorithm>
#include <iomanip>
#include <numeric>
#include "dynamic.hpp"
#include "Matrix.hpp"
#include "statistics.hpp"

template<typename T>
bool lt5Predicate (const T& val)
{
  return (val < T(250));
}

int main()
{
  
  slip::Matrix<float> M(10,10);
  
  float k = 200.0;
  for(std::size_t i = 0; i < M.dim1(); ++i)
    {
      for(std::size_t j = 0; j < M.dim2(); ++j)
	{
	  M[i][j] = k;
	  k+=1.0;
	}
    }
  
  std::cout<<M<<std::endl;
  std::cout<<"min = "<<M.min()<<" max = "<<M.max()<<std::endl;
  slip::Matrix<int> Result(10,10);
 
  
  slip::range_fun_inter0255<float,int> fun(M.min(),M.max());
  slip::change_dynamic(M.begin(),M.end(),Result.begin(),fun);
  std::cout<<Result<<std::endl;

  slip::Matrix<double> Result2(10,10);
  slip::range_fun_inter01<int,double> fun01(Result.min(),Result.max());
  slip::change_dynamic(Result.begin(),Result.end(),Result2.begin(),fun01);
  std::cout<<Result2<<std::endl;

  slip::Matrix<int> Result3(10,10);
  slip::range_fun_interab<double,int> funab(Result2.min(),Result2.max(),0,50);
  slip::change_dynamic(Result2.begin(),Result2.end(),Result3.begin(),funab);
  std::cout<<Result3<<std::endl;

  slip::Matrix<double> Result4(10,10);
  slip::range_fun_inter01<int,double> fun012(Result3.min(),Result3.max());
  slip::change_dynamic(Result3.begin(),Result3.end(),Result4.begin(),fun012);
  std::cout<<Result4<<std::endl;

  slip::Matrix<int> Result5(10,10);
  float mu = slip::mean<float>(Result4.begin(),Result4.end());
  float std = slip::std_dev<float>(Result4.begin(),Result4.end(),mu);
  slip::range_fun_sigmoideb<double,float,int> funsigb(mu,std,255);
  slip::change_dynamic(Result4.begin(),Result4.end(),Result5.begin(),funsigb);
  std::cout<<Result5<<std::endl;

  slip::Matrix<double> Result6(10,10);
  double mu5 = slip::mean<double>(Result5.begin(),Result5.end());
  double std5 = slip::std_dev<double>(Result5.begin(),Result5.end(),mu5);
  slip::range_fun_sigmoide<int,double> funsig(mu5,std5);
  slip::change_dynamic(Result5.begin(),Result5.end(),Result6.begin(),funsig);
  std::cout<<Result6<<std::endl;

  slip::Matrix<int> Result7(10,10);
  slip::range_fun_inter0b<double,int> fun0b(Result6.min(),Result6.max(),255);
  slip::change_dynamic(Result6.begin(),Result6.end(),Result7.begin(),fun0b);
  std::cout<<Result7<<std::endl;

  slip::Matrix<double> Result8(10,10);
  double mu7 = slip::mean<double>(Result7.begin(),Result7.end());
  double std7 = slip::std_dev<double>(Result7.begin(),Result7.end(),mu7);
  slip::range_fun_normal<int,double,double> funnorm(mu7,std7);
  slip::change_dynamic(Result7.begin(),Result7.end(),Result8.begin(),funnorm);
  std::cout<<Result8<<std::endl;
 
  slip::Matrix<int> Result9(10,10);
  slip::range_fun_interab<double,int> funab2(Result8.min(),Result8.max(),0,300);
  slip::change_dynamic(Result8.begin(),Result8.end(),Result9.begin(),funab2);
  std::cout<<Result9<<std::endl;

  slip::Matrix<float> Result10(10,10);
  slip::change_dynamic_01(Result9.begin(),Result9.end(),Result10.begin(),slip::AFFINE_FUNCTION);
  std::cout<<Result10<<std::endl;

  slip::change_dynamic_01(Result9.begin(),Result9.end(),Result10.begin(),slip::SIGMOID_FUNCTION);
  std::cout<<Result10<<std::endl;
  std::cout<<"min = "<<Result10.min()<<" max = "<<Result10.max()<<" mean = "<<slip::mean<double>(Result10.begin(),Result10.end())<<" std = "<<slip::std_dev<double>(Result10.begin(),Result10.end(),slip::mean<double>(Result10.begin(),Result10.end()))<<std::endl;

  slip::Matrix<int> Mask(10,10,2);
  Mask(0,0) = 0;
  Mask(1,1) = 0;
  Mask(2,2) = 0;
  Mask(3,3) = 0;
  Mask(4,4) = 0;
  
 

  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<<"change_dynamic_mask "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::change_dynamic_mask(M.begin(),M.end(),Mask.begin(),Result.begin(),fun,2);
  std::cout<<M<<std::endl;
  std::cout<<Mask<<std::endl;
  std::cout<<Result<<std::endl;
 
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout<<"change_dynamic_if "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::change_dynamic_if(M.begin(),M.end(),Result.begin(),lt5Predicate<float>,fun);
  std::cout<<Result<<std::endl;
 
}
