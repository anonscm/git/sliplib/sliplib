#include "FFT.hpp"
#include "Array.hpp"
#include <complex>
#include <iostream>

#include "GrayscaleImage.hpp"
#include "dynamic.hpp"

int main()
{
  typedef std::complex<double> cx;
  cx a[] = { cx(0,0), cx(1,1), cx(3,3), cx(4,4), cx(4,4), cx(3,3),cx(1,1),cx(0,0)};
  cx b[8];
  slip::complex_fft(a,a+8,b);
  for(int i = 0; i < 8; ++i)
    {
      std::cout<<b[i]<<" ";
    }

  std::cout<<std::endl;

  std::cout<<"fft shift..."<<std::endl;
  slip::fftshift(b,b+8);
  for(int i = 0; i < 8; ++i)
    {
      std::cout<<b[i]<<" ";
    }
  std::cout<<std::endl;

  slip::Array<std::complex<double> > ac(8,a);
  slip::complex_fft(ac.begin(),ac.end(),b);
  std::cout<<ac<<std::endl<<std::endl;
   for(int i = 0; i < 8; ++i)
    {
      std::cout<<b[i]<<" ";
    }
   std::cout<<std::endl<<std::endl;
   cx b2[8];
   slip::ifft(b,b+8,b2);
   for(int i = 0; i < 8; ++i)
    {
      std::cout<<b2[i]<<" ";
    }
   std::cout<<std::endl<<std::endl;

  double tabd[] = {0.0,1.0,3.0,4.0,4.0,3.0,1.0,0.0};
  slip::real_fft(tabd,tabd+8,b);
  for(int i = 0; i < 8; ++i)
    {
      std::cout<<b[i]<<" ";
    }

 std::cout<<std::endl;
 std::cout<<std::endl;

 slip::GrayscaleImage<double> I;
 I.read("./../backend/lena.gif");


 slip::GrayscaleImage<double> Norm(I.dim1(),I.dim2());
 slip::Matrix<std::complex<double> > I2(I.dim1(),I.dim2());
 slip::Matrix<std::complex<double> > I3(I.dim1(),I.dim2());
 
 slip::real_fft2d(I,I2);

 //log(magnitude)
 for(size_t i = 0; i < I2.dim1(); ++i)
   {
   for(size_t j = 0; j < I2.dim2(); ++j)
     {
       Norm[i][j] = std::log(1.0+std::abs(I2[i][j]));
     }
   }
 
 slip::change_dynamic_01(Norm.begin(),Norm.end(),Norm.begin(),slip::AFFINE_FUNCTION);
 
 Norm.write("fft.gif");

 slip::ifft2d(I2,I3);


 double max=0;
 for(size_t i = 0; i < I2.dim1(); ++i)
   {
   for(size_t j = 0; j < I2.dim2(); ++j)
     {
	if (max<std::abs(I[i][j]-std::abs(I3[i][j]))){
		max=std::abs(I[i][j]-std::abs(I3[i][j]));
	}
	}
 }
std::cout<<"Erreur max de reconstruction : "<<max<<std::endl;

 for(size_t i = 0; i < I2.dim1(); ++i)
   {
   for(size_t j = 0; j < I2.dim2(); ++j)
     {
       Norm[i][j] = std::abs(I3[i][j]);
     }
   }

 slip::change_dynamic_01(Norm.begin(),Norm.end(),Norm.begin(),slip::AFFINE_FUNCTION);
 Norm.write("ifft.gif");

 //std::transform(I2.begin(),I2.end(),Norm.begin(),std::ptr_fun(std::norm));
 


}
