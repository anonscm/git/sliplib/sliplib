#include <iostream>
#include "Array.hpp"
#include "Array2d.hpp"
#include "arithmetic_op.hpp"
#include "stl_numeric_ext.hpp"


template<typename T>
bool lt5Predicate (const T& val)
{
  return (val < T(5));
}

int main()
{  
  
  slip::Array<int> v1(10);
  slip::iota(v1.begin(),v1.end(),1,1);
  std::cout<<"v1 = "<<v1<<std::endl;
  slip::Array<int> v2(v1);
  std::cout<<"v2 = "<<v2<<std::endl;
  slip::Array<bool> mask(v1.size());
  mask[2] = 1;
  mask[4] = 1;
  mask[8] = 1;
  std::cout<<"mask = "<<mask<<std::endl;
  slip::Array<int> maskint(v1.size());
  maskint[2] = 2;
  maskint[4] = 2;
  maskint[8] = 2;
  std::cout<<"maskint = "<<maskint<<std::endl;
  std::cout<<slip::inner_product_mask(v1.begin(),v1.end(),
				      mask.begin(),
				      v2.begin(),int(1))<<std::endl;
  
  std::cout<<slip::inner_product_mask(v1.begin(),v1.end(),
				      maskint.begin(),
				      v2.begin(),int(1),int(2))<<std::endl;
  
  std::cout<<slip::inner_product_mask(v1.begin(),v1.end(),
				      mask.begin(),
				      v2.begin(),
				      int(1),
				      std::plus<int>(),
				      std::multiplies<int>())<<std::endl;
  std::cout<<slip::inner_product_mask(v1.begin(),v1.end(),
				      maskint.begin(),
				      v2.begin(),
				      int(1),
				      std::plus<int>(),
				      std::multiplies<int>(),
				      int(2))<<std::endl;
  std::cout<<slip::inner_product_if(v1.begin(),v1.end(),
				    v2.begin(),
				    int(1),
				    lt5Predicate<int>)<<std::endl;
  
  std::cout<<slip::inner_product_if(v1.begin(),v1.end(),
				    v2.begin(),
				    int(1),
				    std::plus<int>(),
				    std::multiplies<int>(),
				    lt5Predicate<int>)<<std::endl;
  return 0;
}
