// #ifdef HAVE_CONFIG_H
// #include "config.h"
// #endif
#include<iostream>
#include <iomanip>
#include "noise.hpp"
#include "Matrix.hpp"
#include "arithmetic_op.hpp"
template<typename T>
bool lt15Predicate (const T& val)
{
  return (val < T(15));
}
int main()
{
 slip::Matrix<double> tab(5,5,100.0);
 slip::Matrix<double> noisy(5,5);
 std::cout <<tab<<std::endl;
slip::add_gaussian_noise(tab.begin(),tab.end(),noisy.begin(),1.0,0.02);
std::cout <<"Gaussian Noise"<<std::endl<<noisy<<std::endl; 
slip::add_uniform_noise(tab.begin(),tab.end(),noisy.begin(),-1.0,1.0);
std::cout <<"Uniform Noise"<<std::endl<<noisy<<std::endl; 
slip::add_salt_and_pepper_noise(tab.begin(),tab.end(),noisy.begin(),0.20,0.0,255.0);
std::cout <<"Salt and pepper Noise"<<std::endl<<noisy<<std::endl; 
slip::add_speckle_noise(tab.begin(),tab.end(),noisy.begin(),0.04);
std::cout <<"Speckle Noise"<<std::endl<<noisy<<std::endl; 
  
slip::Matrix<int> mask(5,5,0);
mask(0,0)=1;mask(1,1)=1; mask(2,2)=1; mask(3,3)=1; mask(4,4)=1;  

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_gaussian_noise_mask "<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
std::cout<<"mask = "<<std::endl<<mask<<std::endl;
slip::add_gaussian_noise_mask(tab.begin(),tab.end(),mask.begin(),noisy.begin(),1.0,0.02,1);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl;

slip::iota(tab.begin(),tab.end(),1.0,1.0);
slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_gaussian_noise_if "<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
slip::add_gaussian_noise_if(tab.begin(),tab.end(),noisy.begin(),lt15Predicate<double>,1.0,0.02);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl;

slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_speckle_noise_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
std::cout<<"mask = "<<std::endl<<mask<<std::endl;
slip::add_speckle_noise_mask(tab.begin(),tab.end(),mask.begin(),noisy.begin(),0.04,1);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl;

slip::iota(tab.begin(),tab.end(),1.0,1.0);
slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_speckle_noise_if "<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
slip::add_speckle_noise_if(tab.begin(),tab.end(),noisy.begin(),lt15Predicate<double>,0.04);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl;

slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_uniform_noise_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
std::cout<<"mask = "<<std::endl<<mask<<std::endl;
 slip::add_uniform_noise_mask(tab.begin(),tab.end(),mask.begin(),noisy.begin(),-1.0,1.0,1);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl;

slip::iota(tab.begin(),tab.end(),1.0,1.0);
slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_uniform_noise_if "<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
slip::add_uniform_noise_if(tab.begin(),tab.end(),noisy.begin(),lt15Predicate<double>,-1.0,1.0);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl;

slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_salt_and_pepper_noise_mask"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
std::cout<<"mask = "<<std::endl<<mask<<std::endl;
slip::add_salt_and_pepper_noise_mask(tab.begin(),tab.end(),mask.begin(),noisy.begin(),0.20,0.0,255.0,1);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl;

slip::iota(tab.begin(),tab.end(),1.0,1.0);
slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_salt_and_pepper_noise_if "<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
slip::add_salt_and_pepper_noise_if(tab.begin(),tab.end(),noisy.begin(),lt15Predicate<double>,0.20,0.0,255.0);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl;

slip::iota(tab.begin(),tab.end(),1.0,1.0);
slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_exponential_noise "<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
slip::add_exponential_noise(tab.begin(),tab.end(),noisy.begin(),0.3);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl;
 
slip::iota(tab.begin(),tab.end(),1.0,1.0);
slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_exponential_noise_mask "<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
std::cout<<"mask = "<<std::endl<<mask<<std::endl;
slip::add_exponential_noise_mask(tab.begin(),tab.end(),mask.begin(),noisy.begin(),0.3, 1);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl;

slip::iota(tab.begin(),tab.end(),1.0,1.0);
slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_exponential_noise_if "<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
 slip::add_exponential_noise_if(tab.begin(),tab.end(),noisy.begin(),lt15Predicate<double>,0.3);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl; 


slip::iota(tab.begin(),tab.end(),1.0,1.0);
slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_gamma_noise "<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
slip::add_gamma_noise(tab.begin(),tab.end(),noisy.begin(),2.0,0.5);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl;


slip::iota(tab.begin(),tab.end(),1.0,1.0);
slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_gamma_noise_mask "<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
std::cout<<"mask = "<<std::endl<<mask<<std::endl;
 slip::add_gamma_noise_mask(tab.begin(),tab.end(),mask.begin(),noisy.begin(),2.0, 0.5, 1);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl;


slip::iota(tab.begin(),tab.end(),1.0,1.0);
slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"add_gamma_noise_if "<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
 slip::add_gamma_noise_if(tab.begin(),tab.end(),noisy.begin(),lt15Predicate<double>,2.0,0.5);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl; 


slip::iota(tab.begin(),tab.end(),1.0,1.0);
slip::iota(noisy.begin(),noisy.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"apply_NLF_noise "<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"tab = "<<std::endl<<tab<<std::endl;
 slip::apply_NLF_noise(tab.begin(),tab.end(),noisy.begin(),0.1,0.001,0.1);
std::cout<<"noisy= "<<std::endl<<noisy<<std::endl;
 
return 0;
}

