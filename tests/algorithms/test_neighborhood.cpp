#include <iostream>
#include "Array.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "Box2d.hpp"
#include "Box3d.hpp"
#include "arithmetic_op.hpp"
#include "DenseVector2dField2d.hpp"
#include "Vector2d.hpp"
#include "neighborhood.hpp"

int main()
{
 
  slip::Array<double> A(6);
  slip::iota(A.begin(),A.end(),1.0);
  typedef slip::Array<double>::iterator iterator; 
  iterator it1 = A.begin();

  slip::Array2d<double> A2(5,4,0.0);
  slip::iota(A2.begin(),A2.end(),1.0);
  typedef slip::Array2d<double>::iterator2d iterator2d;
  iterator2d it2 = A2.upper_left();
  slip::Box2d<int> box2(1,1,A2.rows()-2,A2.cols()-2);

  slip::Array3d<double> A3(3,5,4,0.0);
  slip::iota(A3.begin(),A3.end(),1.0);
  typedef slip::Array3d<double>::iterator3d iterator3d;
  iterator3d it3 = A3.front_upper_left();
  slip::Box3d<int> box3(1,1,1,A3.slices() -2,A3.rows()-2,A3.cols()-2);

 
  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t N2c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::N2C N2c;
  std::vector<iterator> v(26);
  std::vector<double> vv(26);
  std::cout<<"A = \n"<<A<<std::endl;
  it1 = A.begin() + 1;
  for(std::size_t i = 1; i < (A.size() - 1); ++i)
    {
      N2c(it1,v);
      std::cout<<"neighbors of "<<*it1<<" : ";
      for(std::size_t k = 0; k < v.size(); ++k)
  	{
  	  std::cout<<*v[k]<<" ";
  	}
      std::cout<<std::endl;
      it1++;
    }
  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t N2c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it1 = A.begin() + 1;
  for(std::size_t i = 1; i < (A.size() - 1); ++i)
    {
      N2c(it1,vv);
      std::cout<<"neighbors of "<<*it1<<" : ";
      for(std::size_t k = 0; k < vv.size(); ++k)
  	{
  	  std::cout<<vv[k]<<" ";
  	}
      std::cout<<std::endl;
      it1++;
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t N4c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  
  slip::N4C N4c;
 
 std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left(box2);
  std::vector<iterator2d> v2(26);
  std::vector<double> vv2(26);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  N4c(it2,v2);
	  std::cout<<"neighbors of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t N4c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;

  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  N4c(it2,vv2);
	  std::cout<<"neighbors of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t N8c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::N8C N8c;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  N8c(it2,v2);
	  std::cout<<"neighbors of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t N8c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  N8c(it2,vv2);
	  std::cout<<"neighbors of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t PseudoHexagonal"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::PseudoHexagonal PSH;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  PSH(it2,v2);
	  std::cout<<"neighbors of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t PseudoHexagonal value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  PSH(it2,vv2);
	  std::cout<<"neighbors of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 


  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t N6c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::N6C N6c;
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left(box3);
  std::vector<iterator3d> v3(26);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      N6c(it3,v3);
	      std::cout<<"neighbors of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t N6c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it3 = A3.front_upper_left(box3);
  std::vector<double> vv3(26);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      N6c(it3,vv3);
	      std::cout<<"neighbors of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t N18c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::N18C N18c;
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      N18c(it3,v3);
	      std::cout<<"neighbors of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t N18c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      N18c(it3,vv3);
	      std::cout<<"neighbors of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t N26c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::N26C N26c;
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      N26c(it3,v3);
	      std::cout<<"neighbors of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t N26c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      N26c(it3,vv3);
	      std::cout<<"neighbors of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }
 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe N2c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  
  slip::SafeN2C<slip::Array<double> > SN2c(A);
  
  std::cout<<"A = \n"<<A<<std::endl;
  it1 = A.begin();
  for(std::size_t i = 0; i < A.size(); ++i)
    {
      SN2c(it1,v);
      std::cout<<"neighbors of "<<*it1<<" : ";
      for(std::size_t k = 0; k < v.size(); ++k)
  	{
  	  std::cout<<*v[k]<<" ";
  	}
      std::cout<<std::endl;
      it1++;
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe N2c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
 
  it1 = A.begin();
  for(std::size_t i = 0; i < A.size(); ++i)
    {
      SN2c(it1,vv);
      std::cout<<"neighbors of "<<*it1<<" : ";
      for(std::size_t k = 0; k < vv.size(); ++k)
  	{
  	  std::cout<<vv[k]<<" ";
  	}
      std::cout<<std::endl;
      it1++;
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe N4c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;


  slip::SafeN4C<slip::Array2d<double> > SN4c(A2);
  std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SN4c(it2,v2);
	  std::cout<<"neighbors of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe N4c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SN4c(it2,vv2);
	  std::cout<<"neighbors of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

 std::cout<<"----------------------------------------------------"<<std::endl;
 std::cout<<" \t Safe N8c"<<std::endl;
 std::cout<<"----------------------------------------------------"<<std::endl;
 
 slip::SafeN8C<slip::Array2d<double> > SN8c(A2);
 std::cout<<"A2 = \n"<<A2<<std::endl;
 it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SN8c(it2,v2);
	  std::cout<<"neighbors of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe N8c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
 it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SN8c(it2,vv2);
	  std::cout<<"neighbors of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 


   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe PseudoHexagonal"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
 
   slip::SafePseudoHexagonal<slip::Array2d<double> > SPH(A2);
 std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SPH(it2,v2);
	  std::cout<<"neighbors of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe PseudoHexagonal value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SPH(it2,vv2);
	  std::cout<<"neighbors of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  


 std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe N6c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  slip::SafeN6C<slip::Array3d<double> > SN6c(A3);
std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SN6c(it3,v3);
	      std::cout<<"neighbors of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe N6c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SN6c(it3,vv3);
	      std::cout<<"neighbors of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }


   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe N18c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  slip::SafeN18C<slip::Array3d<double> > SN18c(A3);
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SN18c(it3,v3);
	      std::cout<<"neighbors of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe N18c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SN18c(it3,vv3);
	      std::cout<<"neighbors of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

    

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe N26c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  slip::SafeN26C<slip::Array3d<double> > SN26c(A3);
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SN26c(it3,v3);
	      std::cout<<"neighbors of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe N26c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SN26c(it3,vv3);
	      std::cout<<"neighbors of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }



  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Prev2c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::Prev2C Prev2c;
  std::cout<<"A = \n"<<A<<std::endl;
  it1 = A.begin() + 1;
  for(std::size_t i = 1; i < (A.size() - 1); ++i)
    {
      Prev2c(it1,v);
      std::cout<<"previous of "<<*it1<<" : ";
      for(std::size_t k = 0; k < v.size(); ++k)
  	{
  	  std::cout<<*v[k]<<" ";
  	}
      std::cout<<std::endl;
      it1++;
    }
  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Prev2c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;

  it1 = A.begin() + 1;
  for(std::size_t i = 1; i < (A.size() - 1); ++i)
    {
      Prev2c(it1,vv);
      std::cout<<"previous of "<<*it1<<" : ";
      for(std::size_t k = 0; k < vv.size(); ++k)
  	{
  	  std::cout<<vv[k]<<" ";
  	}
      std::cout<<std::endl;
      it1++;
    }

   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Next2c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::Next2C Next2c;
  std::cout<<"A = \n"<<A<<std::endl;
  it1 = A.begin() + 1;
  for(std::size_t i = 1; i < (A.size() - 1); ++i)
    {
      Next2c(it1,v);
      std::cout<<"next of "<<*it1<<" : ";
      for(std::size_t k = 0; k < v.size(); ++k)
  	{
  	  std::cout<<*v[k]<<" ";
  	}
      std::cout<<std::endl;
      it1++;
    }
  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Next2c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  
   it1 = A.begin() + 1;
  for(std::size_t i = 1; i < (A.size() - 1); ++i)
    {
      Next2c(it1,vv);
      std::cout<<"next of "<<*it1<<" : ";
      for(std::size_t k = 0; k < vv.size(); ++k)
  	{
  	  std::cout<<vv[k]<<" ";
  	}
      std::cout<<std::endl;
      it1++;
    }


  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Prev4c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  
  slip::Prev4C Prev4c;
  
  std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  Prev4c(it2,v2);
	  std::cout<<"previous of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Prev4c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  Prev4c(it2,vv2);
	  std::cout<<"previous of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 


  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Next4c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  
  slip::Next4C Next4c;
  
  std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  Next4c(it2,v2);
	  std::cout<<"next of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Next4c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  Next4c(it2,vv2);
	  std::cout<<"next of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 


   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Prev8c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  
  slip::Prev8C Prev8c;
  
  std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  Prev8c(it2,v2);
	  std::cout<<"previous of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Prev8c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  Prev8c(it2,vv2);
	  std::cout<<"previous of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 


  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Next8c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  
  slip::Next8C Next8c;
  
  std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  Next8c(it2,v2);
	  std::cout<<"next of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Next8c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  Next8c(it2,vv2);
	  std::cout<<"next of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 


 std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t PrevPseudoHexagonal"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  
  slip::PrevPseudoHexagonal PrevPseudoHexagonal;
  
  std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  PrevPseudoHexagonal(it2,v2);
	  std::cout<<"previous of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t PrevPseudoHexagonal value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  PrevPseudoHexagonal(it2,vv2);
	  std::cout<<"previous of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 


  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t NextPseudoHexagonal"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  
  slip::NextPseudoHexagonal NextPseudoHexagonal;
  
  std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  NextPseudoHexagonal(it2,v2);
	  std::cout<<"next of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t NextPseudoHexagonal value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left(box2);
  for(std::size_t i = 1; i < (A2.rows() - 1); ++i)
    {
      for(std::size_t j = 1; j < (A2.cols() - 1); ++j)
	{
	  NextPseudoHexagonal(it2,vv2);
	  std::cout<<"next of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Prev6c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::Prev6C Prev6c;
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      Prev6c(it3,v3);
	      std::cout<<"previous of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Prev6c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      Prev6c(it3,vv3);
	      std::cout<<"previous of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }


  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Next6c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::Next6C Next6c;
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      Next6c(it3,v3);
	      std::cout<<"next of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Next6c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      Next6c(it3,vv3);
	      std::cout<<"next of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }


   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Prev18c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::Prev18C Prev18c;
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      Prev18c(it3,v3);
	      std::cout<<"previous of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Prev18c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      Prev18c(it3,vv3);
	      std::cout<<"previous of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }


  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Next18c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::Next18C Next18c;
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      Next18c(it3,v3);
	      std::cout<<"next of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Next18c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      Next18c(it3,vv3);
	      std::cout<<"next of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Prev26c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::Prev26C Prev26c;
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      Prev26c(it3,v3);
	      std::cout<<"previous of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }


  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Prev26c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      Prev26c(it3,vv3);
	      std::cout<<"previous of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }


  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Next26c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::Next26C Next26c;
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      Next26c(it3,v3);
	      std::cout<<"next of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }


  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Next26c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it3 = A3.front_upper_left(box3);
  for(std::size_t k = 1; k < (A3.slices() - 1); ++k)
    {
      for(std::size_t i = 1; i < (A3.rows() - 1); ++i)
	{
	  for(std::size_t j = 1; j < (A3.cols() - 1); ++j)
	    {
	      Next26c(it3,vv3);
	      std::cout<<"next of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

 std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Prev2c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  
  slip::SafePrev2C<slip::Array<double> > SPrev2c(A);
  
  std::cout<<"A = \n"<<A<<std::endl;
  it1 = A.begin();
  for(std::size_t i = 0; i < A.size(); ++i)
    {
      SPrev2c(it1,v);
      std::cout<<"previous of "<<*it1<<" : ";
      for(std::size_t k = 0; k < v.size(); ++k)
  	{
  	  std::cout<<*v[k]<<" ";
  	}
      std::cout<<std::endl;
      it1++;
    }
  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Prev2c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
 
  it1 = A.begin();
  for(std::size_t i = 0; i < A.size(); ++i)
    {
      SPrev2c(it1,vv);
      std::cout<<"previous of "<<*it1<<" : ";
      for(std::size_t k = 0; k < vv.size(); ++k)
  	{
  	  std::cout<<vv[k]<<" ";
  	}
      std::cout<<std::endl;
      it1++;
    }

   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Next2c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  
  slip::SafeNext2C<slip::Array<double> > SNext2c(A);
  
  std::cout<<"A = \n"<<A<<std::endl;
  it1 = A.begin();
  for(std::size_t i = 0; i < A.size(); ++i)
    {
      SNext2c(it1,v);
      std::cout<<"previous of "<<*it1<<" : ";
      for(std::size_t k = 0; k < v.size(); ++k)
  	{
  	  std::cout<<*v[k]<<" ";
  	}
      std::cout<<std::endl;
      it1++;
    }
 std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Next2c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
 
   it1 = A.begin();
  for(std::size_t i = 0; i < A.size(); ++i)
    {
      SNext2c(it1,vv);
      std::cout<<"previous of "<<*it1<<" : ";
      for(std::size_t k = 0; k < vv.size(); ++k)
  	{
  	  std::cout<<vv[k]<<" ";
  	}
      std::cout<<std::endl;
      it1++;
    }


  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Prev4c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;


  slip::SafePrev4C<slip::Array2d<double> > SPrev4c(A2);
  std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SPrev4c(it2,v2);
	  std::cout<<"previous of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 


   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Prev4c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SPrev4c(it2,vv2);
	  std::cout<<"previous of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Next4c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;


  slip::SafeNext4C<slip::Array2d<double> > SNext4c(A2);
  std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SNext4c(it2,v2);
	  std::cout<<"next of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Next4c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SNext4c(it2,vv2);
	  std::cout<<"next of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 


   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Prev8c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;


  slip::SafePrev8C<slip::Array2d<double> > SPrev8c(A2);
  std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SPrev8c(it2,v2);
	  std::cout<<"previous of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Prev8c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SPrev8c(it2,vv2);
	  std::cout<<"previous of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Next8c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;


  slip::SafeNext8C<slip::Array2d<double> > SNext8c(A2);
  std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SNext8c(it2,v2);
	  std::cout<<"next of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Next8c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SNext8c(it2,vv2);
	  std::cout<<"next of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 


  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe PrevPseudoHexagonal"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;


  slip::SafePrevPseudoHexagonal<slip::Array2d<double> > SPrevPH(A2);
  std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SPrevPH(it2,v2);
	  std::cout<<"previous of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe PrevPseudoHexagonal value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SPrevPH(it2,vv2);
	  std::cout<<"previous of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 

   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe NextPseudoHexagonal"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;

  slip::SafeNextPseudoHexagonal<slip::Array2d<double> > SNextPH(A2);
  std::cout<<"A2 = \n"<<A2<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SNextPH(it2,v2);
	  std::cout<<"next of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < v2.size(); ++k)
	    {
	      std::cout<<*v2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 
  
  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe NextPseudoHexagonal value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  it2 = A2.upper_left();
  for(std::size_t i = 0; i < A2.rows(); ++i)
    {
      for(std::size_t j = 0; j < A2.cols(); ++j)
	{
	  SNextPH(it2,vv2);
	  std::cout<<"next of "<<*it2<<" : ";
	  for(std::size_t k = 0; k < vv2.size(); ++k)
	    {
	      std::cout<<vv2[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2++;
	}
    } 



  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Prev6c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  slip::SafePrev6C<slip::Array3d<double> > SPrev6c(A3);
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SPrev6c(it3,v3);
	      std::cout<<"previous of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Prev6c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SPrev6c(it3,vv3);
	      std::cout<<"previous of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Next6c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  slip::SafeNext6C<slip::Array3d<double> > SNext6c(A3);
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SNext6c(it3,v3);
	      std::cout<<"next of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Next6c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 

  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SNext6c(it3,vv3);
	      std::cout<<"next of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }


   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Prev18c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  slip::SafePrev18C<slip::Array3d<double> > SPrev18c(A3);
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SPrev18c(it3,v3);
	      std::cout<<"previous of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Prev18c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SPrev18c(it3,vv3);
	      std::cout<<"previous of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

    std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Next18c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  slip::SafeNext18C<slip::Array3d<double> > SNext18c(A3);
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SNext18c(it3,v3);
	      std::cout<<"next of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Next18c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SNext18c(it3,vv3);
	      std::cout<<"next of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }


   std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Prev26c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  slip::SafePrev26C<slip::Array3d<double> > SPrev26c(A3);
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SPrev26c(it3,v3);
	      std::cout<<"previous of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Prev26c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SPrev26c(it3,vv3);
	      std::cout<<"previous of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

    std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Next26c"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  slip::SafeNext26C<slip::Array3d<double> > SNext26c(A3);
  std::cout<<"A3 = \n"<<A3<<std::endl;
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SNext26c(it3,v3);
	      std::cout<<"next of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < v3.size(); ++kk)
		{
		  std::cout<<*v3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe Next26c value"<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl; 
  it3 = A3.front_upper_left();
  for(std::size_t k = 0; k < A3.slices(); ++k)
    {
      for(std::size_t i = 0; i < A3.rows(); ++i)
	{
	  for(std::size_t j = 0; j < A3.cols(); ++j)
	    {
	      SNext26c(it3,vv3);
	      std::cout<<"next of "<<*it3<<" : ";
	      for(std::size_t kk = 0; kk < vv3.size(); ++kk)
		{
		  std::cout<<vv3[kk]<<" ";
		}
	      std::cout<<std::endl;
	      it3++;
	    }
	} 
    }

  std::cout<<"----------------------------------------------------"<<std::endl;
  std::cout<<" \t Safe N8c slip::DenseVector2dField2d<double>     "<<std::endl;
  std::cout<<"----------------------------------------------------"<<std::endl;
  slip::DenseVector2dField2d<double> DVF2(5,4);
  slip::iota(DVF2.begin(),DVF2.end(),
	     slip::Vector2d<double>(1.0,1.0),
	     slip::Vector2d<double>(1.0,1.0));
  typedef slip::DenseVector2dField2d<double>::iterator2d iterator2d_vect;
  iterator2d_vect it2_vect = DVF2.upper_left();
  slip::Box2d<int> box2_vect(1,1,DVF2.rows()-2,DVF2.cols()-2);
  
  slip::SafeN8C< slip::DenseVector2dField2d<double> > SN8c_vect(DVF2);
  std::cout<<"DVF2 = \n"<<DVF2<<std::endl;
  std::vector<iterator2d_vect> v2_vect(8);
  for(std::size_t i = 0; i < DVF2.rows(); ++i)
    {
      for(std::size_t j = 0; j < DVF2.cols(); ++j)
	{
	  SN8c(it2_vect,v2_vect);
	  std::cout<<"neighbors of "<<*it2_vect<<" : ";
	  for(std::size_t k = 0; k < v2_vect.size(); ++k)
	    {
	      std::cout<<*v2_vect[k]<<" ";
	    }
	  std::cout<<std::endl;
	  it2_vect++;
	}
    } 

  return 0;
}
