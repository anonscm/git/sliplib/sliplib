#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include "histo.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "GrayscaleImage.hpp"
#include "arithmetic_op.hpp"


bool myPredicate (const double& val)
{
  return (val > 3);
}


int main()
{
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Histogram " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::Matrix<double> M(5,5);
  slip::iota(M.begin(),M.end(),0.0);
  std::cout<<"M =\n"<<M<<std::endl;
  std::size_t size = 10;
  slip::Vector<int> Result(size,2);
  slip::histogram(M.begin(),M.end(),Result.begin(),Result.end(),0,30,3);
  std::cout<<"Histogram(M,0,30,3) = \n"<<Result<<std::endl;
  slip::Vector<int> Resultc(Result.size());
  slip::cumulative_histogram(Result.begin(),Result.end(),
			     Resultc.begin(),Resultc.end());
  std::cout<<"Cumulative Histogram(M,0,30,3) = \n"<<Resultc<<std::endl;
  std::cout<<std::endl;

  slip::Vector<int> V(13);
  V[0] = V[8] = V[9] = 1;
  V[1] = V[4] = V[5] = V[7] = V[10] = 3;
  V[2] = V[3] = V[6] = V[11] = V[12] = 2;
  std::cout<<"V = \n"<<V<<std::endl;
  slip::Vector<int> Casier(3);
  slip::histogram(V.begin(),V.end(),Casier.begin(),Casier.end(),1,3,1);
  std::cout<<"Histogram(V,1,3,1) = \n"<<Casier<<std::endl;
 
  std::cout << "Histogram on a random matrix" << std::endl;
  std::generate(M.begin(),M.end(),std::rand);
  std::cout<<"M =\n"<<M<<std::endl;
  slip::Vector<int> Result2(10);
  double min = M.min();
  double max = M.max();
  double step = (max-min)/double(Result2.size());
  std::cout<<"min(M) = "<<min<<std::endl;
  std::cout<<"max(M) = "<<max<<std::endl;
  std::cout<<"step(M) = "<<step<<std::endl;
  slip::histogram(M.begin(),M.end(),Result2.begin(),Result2.end(),min,max,step);
  std::cout << "Histogram("<<min<<","<<max<<","<<step<<")\n"<<Result2<<std::endl;
  slip::Vector<int> Result2c(Result2.size());
  slip::cumulative_histogram(Result2.begin(),Result2.end(),
			     Result2c.begin(),Result2c.end());
  std::cout<<"Cumulative Histogram("<<min<<","<<max<<","<<step<<")\n"<<Result2c<<std::endl;
  

  std::cout<<std::endl;
  for(std::size_t i = 0; i < M.dim1(); ++i)
    {
      for(std::size_t j = 0; j < M.dim2(); ++j)
	{
	  M[i][j] = i * j;
	}
    }
  std::cout<<"M = \n"<<M<<std::endl;
  
  slip::Vector<int> Result4(17);
  min = M.min();
  max = M.max();
  std::cout<<"min = "<<min<<std::endl;
  std::cout<<"max = "<<max<<std::endl;
  slip::histogram(M.begin(),M.end(),Result4.begin(),Result4.end(),min,max);
  std::cout << "Histogram("<<min<<","<<max<<","<<1<<")\n"<<Result4<<std::endl;
  
  slip::Vector<int> Result4c(Result4.size());
  slip::cumulative_histogram(Result4.begin(),Result4.end(),
			    Result4c.begin(),Result4c.end());
  std::cout << "Cumultative Histogram("<<min<<","<<max<<","<<1<<")\n"<<Result4c<<std::endl;



  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Real Histogram " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::iota(M.begin(),M.end(),0.0);
  std::cout<<"M =\n"<<M<<std::endl;
  double rsteps[] = {0.0,2.3,4.5,5.7,7.0,30.0};
  std::cout<<"rsteps = "<<std::endl;
  std::copy(rsteps,rsteps+6,std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  slip::Vector<int> Result3(5);
  slip::real_histogram(M.begin(),M.end(),rsteps,rsteps+6,Result3.begin());
  std::cout<<"Real Histogram(M,0,30,3) = \n"<<Result3<<std::endl;
  slip::Vector<int> Result3c(Result3.size());
  slip::cumulative_histogram(Result3.begin(),Result3.end(),
			     Result3c.begin(),Result3c.end());
  std::cout<<"Cumulative Histogram(M,0,30,3) = \n"<<Result3c<<std::endl;



  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Histogram mask" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::iota(M.begin(),M.begin()+5,2.0,0.0);
  slip::iota(M.begin()+5,M.begin()+10,3.0,0.0);
  slip::iota(M.begin()+10,M.begin()+18,4.0,0.0);
  std::cout<<"M = \n"<<M<<std::endl;
  slip::Matrix<int> MaskValue(5,5);
  std::fill(MaskValue.begin(),MaskValue.begin()+12,1);
  std::fill(MaskValue.begin()+12,MaskValue.begin()+21,2);
  std::cout<<"MaskValue=\n"<<MaskValue<<std::endl;
  slip::Vector<int> Histo_mask(25);
  slip::histogram_mask(M.begin(),M.end(),
		       MaskValue.begin(),
		       Histo_mask.begin(),Histo_mask.end(),0,24,1);
  std::cout<<"Histo_mask(0,24,1) mask_value = 1 : \n"<<Histo_mask<<std::endl;
  slip::histogram_mask(M.begin(),M.end(),
		       MaskValue.begin(),
		       Histo_mask.begin(),Histo_mask.end(),0,24,1,2);
  std::cout<<"Histo_mask(0,24,1) mask_value = 2 : \n"<<Histo_mask<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Histogram predicate" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  std::cout<<"M = \n"<<M<<std::endl;
  slip::Vector<int> Histo_if(25);
  slip::histogram_if(M.begin(),M.end(),
		     Histo_if.begin(),Histo_if.end(),
		     myPredicate,
		     0,24);
  std::cout<<"Histo_if(0,24,mypredicate : val > 3) = \n"<<Histo_if<<std::endl;



   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "RealHistogram...  " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  slip::RealHistogram<double> Histo(-1.0,3.0,0.3);
  Histo.print();

  slip::RealHistogram<double> Histo2(-1.0,3.0,static_cast<std::size_t>(13));
  Histo2.print();
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "Histo3.1 "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  
  slip::Array<double> steps(14);
  slip::iota(steps.begin(),steps.end(),-1.0,0.3);
  slip::RealHistogram<double> Histo3(steps.begin(),steps.end());
  Histo3.print();
  std::cout<<"find_bin 0.1 = "<<Histo3.find_bin(0.1)<<std::endl;
  std::cout<<"add 0.1"<<std::endl;
  Histo3.add(0.1);
  Histo3.print();
  std::cout<<"find_bin -0.5 = "<<Histo3.find_bin(-0.5)<<std::endl;
  std::cout<<"add -0.5"<<std::endl;
  Histo3.add(-0.5);
  Histo3.print();
  std::cout<<"find_bin 0.15 = "<<Histo3.find_bin(0.15)<<std::endl;
  std::cout<<"add 0.15"<<std::endl;
  Histo3.add(0.15);
  Histo3.print();
  std::cout<<"remove 0.15"<<std::endl;
  Histo3.remove(0.15);
  Histo3.print();

  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "Histo3.2 "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  
  Histo3.init();
  Histo3.print();
  double data[] = {0.1,0.2,0.8,-0.6,-0.4};
  Histo3.compute(data,data+5);
  Histo3.print();

  Histo3.cumulate();
  Histo3.print();

  std::cout<<"median = "<<Histo3.median(5)<<std::endl<<std::endl;

  std::cout<<"values = "<<Histo3.values()<<std::endl;
  std::cout<<"min =    "<<Histo3.min()<<std::endl;
  std::cout<<"max=     "<<Histo3.max()<<std::endl;
  std::cout<<"mean=     "<<Histo3.mean(5)<<std::endl;
  std::cout<<"energy=     "<<Histo3.energy(5)<<std::endl;
  std::cout<<"entropy=     "<<Histo3.entropy(5)<<std::endl;
  std::cout<<"skewness=     "<<Histo3.skewness(5)<<std::endl;
  std::cout<<"kurtosis=     "<<Histo3.kurtosis(5)<<std::endl;
 
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "Histo_d "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  slip::Array<double> steps2(9);
  slip::iota(steps2.begin(),steps2.end(),0.0,1.0/double(8));
  slip::RealHistogram<double> Histo_d(steps2.begin(),steps2.end());
  Histo_d.print();
  double xxd[] = {1.0,2.0,3.0,1.0,2.0,3.0,4.0,5.0,6.0,1.0,1.0,4.0,5.0,6.0,7.0,0.0,0.0};
  slip::Array<double> xd(17,xxd,xxd+17);
  slip::divides_scalar(xd.begin(),xd.end(),8.0,xd.begin());
  //  slip::iota(xd.begin(),xd.end(),0.0,1.0/double(33-1));
  for(std::size_t i = 0; i < xd.size(); ++i)
    {
      std::cout<<"value = "<<xd[i]<<" bins = "<<Histo_d.find_bin(xd[i])<<std::endl;
    }
  std::cout<<"Histo_d = \n";
  Histo_d.print();
  Histo_d.compute(xd.begin(),xd.end());
  Histo_d.cumulate();
  Histo_d.print();
  std::cout<<"median = "<<Histo_d.median(17)<<" median * 8 = "<<Histo_d.median(17)*8.0<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "Histo_dd "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  double xxdd[] = {1.0,2.0,2.0,3.0,1.0,2.0,3.0,4.0,5.0,6.0,1.0,1.0,4.0,5.0,6.0,7.0,0.0,0.0};
  slip::Array<double> xdd(18,xxdd,xxdd+18);
  slip::divides_scalar(xdd.begin(),xdd.end(),8.0,xdd.begin());
  Histo_d.init();
  Histo_d.compute(xdd.begin(),xdd.end());
  Histo_d.cumulate();
  Histo_d.print();
  std::cout<<"median = "<<Histo_d.median(18)<<" median * 8 = "<<Histo_d.median(18)*8.0<<std::endl;
   std::cout<<"values = "<<Histo_d.values()<<std::endl;
  std::cout<<"min =    "<<Histo_d.min()<<std::endl;
  std::cout<<"max=     "<<Histo_d.max()<<std::endl;
   std::cout<<"mean=    "<<Histo_d.mean(18)<<std::endl;
  std::cout<<"var=     "<<Histo_d.var(18)<<std::endl;
  std::cout<<"unbiased var=     "<<Histo_d.unbiased_var(18)<<std::endl;
  std::cout<<"std=     "<<Histo_d.std(18)<<std::endl;
  std::cout<<"unbiased std=     "<<Histo_d.unbiased_std(18)<<std::endl;
  std::cout<<"entropy=     "<<Histo_d.entropy(18)<<std::endl;
 

  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "Histo_ddd "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  double xxddd[] = {1.0,2.0,3.0,1.0,2.0,3.0,4.0,5.0,6.0,1.0};

  slip::Array<double> xddd(10,xxddd,xxddd+10);
  slip::divides_scalar(xddd.begin(),xddd.end(),8.0,xddd.begin());
  Histo_d.init();
  Histo_d.compute(xddd.begin(),xddd.end());
  Histo_d.cumulate();
  Histo_d.print();
  std::cout<<"median = "<<Histo_d.median(10)<<" median * 8 = "<<Histo_d.median(10)*8.0<<std::endl;
   std::cout<<"values = "<<Histo_d.values()<<std::endl;
  std::cout<<"min =    "<<Histo_d.min()<<std::endl;
  std::cout<<"max=     "<<Histo_d.max()<<std::endl;
   std::cout<<"mean=    "<<Histo_d.mean(10)<<std::endl;
  std::cout<<"var=     "<<Histo_d.var(10)<<std::endl;
  std::cout<<"unbiased var=     "<<Histo_d.unbiased_var(10)<<std::endl;
  std::cout<<"std=     "<<Histo_d.std(10)<<std::endl;
  std::cout<<"unbiased std=     "<<Histo_d.unbiased_std(10)<<std::endl;
 
  

   std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "Histo_dddd "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  double xxdddd[] = {1.0,2.0,3.0,2.0,1.0,2.0,3.0,4.0,5.0,6.0,1.0};

  slip::Array<double> xdddd(11,xxdddd,xxdddd+11);
  slip::divides_scalar(xdddd.begin(),xdddd.end(),8.0,xdddd.begin());
  Histo_d.init();
  Histo_d.compute(xdddd.begin(),xdddd.end());
  Histo_d.cumulate();
  Histo_d.print();
  std::cout<<"median = "<<Histo_d.median(11)<<" median * 8 = "<<Histo_d.median(11)*8.0<<std::endl;
   std::cout<<"values = "<<Histo_d.values()<<std::endl;
  std::cout<<"min =    "<<Histo_d.min()<<std::endl;
  std::cout<<"max=     "<<Histo_d.max()<<std::endl;
   std::cout<<"mean=    "<<Histo_d.mean(11)<<std::endl;
  std::cout<<"var=     "<<Histo_d.var(11)<<std::endl;
  std::cout<<"unbiased var=     "<<Histo_d.unbiased_var(11)<<std::endl;
  std::cout<<"std=     "<<Histo_d.std(11)<<std::endl;
  std::cout<<"unbiased std=     "<<Histo_d.unbiased_std(11)<<std::endl;
 
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "RealHistogram copy constructor "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  slip::RealHistogram<double> Histo_copy(Histo_d);
  std::cout<<"Histo_d =\n";
  Histo_d.print();
  std::cout<<"Histo_copy =\n";
  Histo_copy.print();
  

  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "RealHistogram operator= "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  slip::RealHistogram<double> Histo_equal;
  Histo_equal = Histo_d;
  std::cout<<"Histo_d =\n";
  Histo_d.print();
  std::cout<<"Histo_equal =\n";
  Histo_equal.print();

   std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "Histo4 "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
 
  slip::Histogram<int> Histo4(1,9);

  int datai[] = {1,2,3,1,2,3,4,5,6,1};
  Histo4.print();

  Histo4.compute(datai,datai+10);
  Histo4.print();
  Histo4.cumulate();
  Histo4.print();

  

  std::cout<<"values = "<<Histo4.values()<<std::endl;
  std::cout<<"min =    "<<Histo4.min()<<std::endl;
  std::cout<<"max=     "<<Histo4.max()<<std::endl;
  std::cout<<"median = "<<Histo4.median(10)<<std::endl;
  std::cout<<"mean=    "<<Histo4.mean(10)<<std::endl;
  std::cout<<"var=     "<<Histo4.var(10)<<std::endl;
  std::cout<<"unbiased var=     "<<Histo4.unbiased_var(10)<<std::endl;
  std::cout<<"std=     "<<Histo4.std(10)<<std::endl;
  std::cout<<"unbiased std=     "<<Histo4.unbiased_std(10)<<std::endl;
 

  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "Histo5 "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
 
  slip::Histogram<int> Histo5(0,8);
  int datai2[] = {1,2,3,1,2,3,4,5,6,1,1,4,5,6,7,0,0};
   Histo5.print();

  Histo5.compute(datai2,datai2+17);
  Histo5.print();
  Histo5.cumulate();
  Histo5.print();

  

  std::cout<<"values = "<<Histo5.values()<<std::endl;
  std::cout<<"min =    "<<Histo5.min()<<std::endl;
  std::cout<<"max=     "<<Histo5.max()<<std::endl;
  std::cout<<"median = "<<Histo5.median(17)<<std::endl;
  std::cout<<"slip::median = "<<slip::median(datai2,datai2+17)<<std::endl;
  std::cout<<"mean=    "<<Histo5.mean(17)<<std::endl;
  std::cout<<"var=     "<<Histo5.var(17)<<std::endl;
  std::cout<<"unbiased var=     "<<Histo5.unbiased_var(17)<<std::endl;
  std::cout<<"std=     "<<Histo5.std(17)<<std::endl;
  std::cout<<"unbiased std=     "<<Histo5.unbiased_std(17)<<std::endl;
 
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "Histo6 "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;


  slip::Histogram<int> Histo6(0,8);
  int datai3[] = {1,2,2,3,1,2,3,4,5,6,1,1,4,5,6,7,0,0};
   Histo6.print();

  Histo6.compute(datai3,datai3+18);
  Histo6.print();
  Histo6.cumulate();
  Histo6.print();

  

  std::cout<<"values = "<<Histo6.values()<<std::endl;
  std::cout<<"min =    "<<Histo6.min()<<std::endl;
  std::cout<<"max=     "<<Histo6.max()<<std::endl;
  std::cout<<"median = "<<Histo6.median(18)<<std::endl;
  std::cout<<"slip::median = "<<slip::median(datai3,datai3+18)<<std::endl;
   
  std::cout<<"mean=    "<<Histo6.mean(18)<<std::endl;
  std::cout<<"var=     "<<Histo6.var(18)<<std::endl;
  std::cout<<"unbiased var=     "<<Histo6.unbiased_var(18)<<std::endl;
  std::cout<<"std=     "<<Histo6.std(18)<<std::endl;
  std::cout<<"unbiased std=     "<<Histo6.unbiased_std(18)<<std::endl;
 
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "Histogram copy constructor "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  slip::Histogram<int> Histo4_copy(Histo4);
  std::cout<<"Histo4 =\n";
  Histo4.print();
  std::cout<<"Histo4_copy =\n";
  Histo4_copy.print();
  

  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "Histogram operator= "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  slip::Histogram<int> Histo4_equal;
  Histo4_equal = Histo4;
  std::cout<<"Histo4 =\n";
  Histo4.print();
  std::cout<<"Histo4_equal =\n";
  Histo4_equal.print();

   std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "iterator "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::copy(Histo4.begin(),Histo4.end(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;

  std::copy(Histo_d.begin(),Histo_d.end(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "cumul_iterator "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::copy(Histo4.cumul_begin(),Histo4.cumul_end(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;

 std::copy(Histo_d.cumul_begin(),Histo_d.cumul_end(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "chi2, intersection "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout<<"Histo5 = \n";
  Histo5.print();
  std::cout<<"Histo6 = \n";
   Histo6.print();
  std::cout<<"chi2(Histo5,Histo6) = "<<slip::chi2_histo(Histo5,Histo6)<<std::endl;
  std::cout<<"inter(Histo5,Histo6) = "<<slip::inter_histo(Histo5,Histo6)<<std::endl;

  // std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  // std::cout << "add "<<std::endl;
  // std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  // slip::Histogram<int> Histo7 =  slip::add_histo(Histo5,Histo6);
  // std::cout<<"Histo7 = Histo6 + Histo5\n";
  // Histo7.print();



  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "operator+= "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout<<"Histo5 = \n";
  Histo5.print();
  std::cout<<"Histo6 = \n";
  Histo6.print();
  std::cout<<"---------------"<<std::endl;
  std::cout<<"Histo5+=Histo6 \n";
  Histo5+=Histo6;
  Histo5.print();
  
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "operator-= "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout<<"Histo5 = \n";
  Histo5.print();
  std::cout<<"Histo6 = \n";
  Histo6.print();
  std::cout<<"---------------"<<std::endl;
  std::cout<<"Histo5-=Histo6 \n";
  Histo5-=Histo6;
  Histo5.print();

  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "operator+ "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  slip::Histogram<int> Histo8 = Histo5 + Histo6;
  std::cout<<"Histo5 = \n";
  Histo5.print();
  std::cout<<"Histo6 = \n";
  Histo6.print();
  std::cout<<"---------------"<<std::endl;
  std::cout<<"Histo5+Histo6 \n";
  Histo8.print();
  

   std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  std::cout << "operator+ "<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(20) << '_' << std::endl;
  slip::Histogram<int> Histo9 = Histo6 - Histo5;
  std::cout<<"Histo5 = \n";
  Histo5.print();
  std::cout<<"Histo6 = \n";
  Histo6.print();
  std::cout<<"---------------"<<std::endl;
  std::cout<<"Histo6-Histo5 \n";
  Histo9.print();

  
}

