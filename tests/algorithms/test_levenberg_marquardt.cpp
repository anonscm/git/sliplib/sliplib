#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <string>

#include "Vector.hpp"
#include "Matrix.hpp"
#include "io_tools.hpp"
#include "noise.hpp"
#include "linear_algebra.hpp"
#include "arithmetic_op.hpp"
#include "levenberg_marquardt.hpp"
#include "DistortionCamera.hpp"

template <typename Type>
struct funLM_ex1 : public std::unary_function <slip::Vector<Type>,slip::Vector<Type> >
{
  typedef funLM_ex1<Type> self;
  funLM_ex1(const slip::Vector<Type>& x, const slip::Vector<Type>& y) : 
    x_(x),
    r_(slip::Vector<Type>(x.size())),
    y_(y)
 {
   assert(x.size() == y.size());
 }
 slip::Vector<Type>& operator() (const slip::Vector<Type>& pt) // define the function
 {
   for(std::size_t i = 0; i < x_.size(); ++i)
     {
       r_[i]= y_[i] - (pt[0]*exp(-pt[1]*x_[i]) + pt[2]);
     }
   return r_;
}
 
 
  slip::Vector<Type> x_;//x
  slip::Vector<Type> r_; //residue ||y - f(x,pk)||
  slip::Vector<Type> y_; //y = f(x,p)
};



using namespace::std;
// main program
//int main(int argc, char *argv[])
int main()
{
  typedef double Type;
  std::size_t n = 40;
  slip::Vector<Type> x(n);
  slip::iota(x.begin(),x.end(),Type(0.0));
  slip::Vector<Type> y(n);
  for(std::size_t i = 0; i < n; ++i)
    {
      //p[0] = 5.0, p[1] = 0.1, p[2] = 1.0
      y[i]=(5.0*exp(-0.1*x[i]) + 1.0);
    }
  
  std::cout<<"y = \n"<<y<<std::endl;
  slip::add_gaussian_noise(y.begin(),y.end(),y.begin(),0.0,0.01);

  std::cout<<"y with noise = \n"<<y<<std::endl;
  funLM_ex1<Type> fun(x,y);
  //parameters vector
  slip::Vector<Type> p(3);
  p[0] = 1.0;
  p[1] = 0.0;
  p[2] = 0.0;
  //residue vector
  slip::Vector<Type> r(n);
  
  
  slip::LMDerivFunctor<funLM_ex1<Type>, Type> df(fun,n,p.size());
  Type chi2 = 0.0;
  slip::marquardt(fun,df,p,r,chi2);
  std::cout<<"estimated p = \n"<<p<<std::endl;
  std::cout<<"r = \n"<<r<<std::endl;
  std::cout<<"chi2 = "<<chi2<<std::endl;
  
 
  slip::funLM_bp<Type> bp(y);
  slip::funLM_bp<Type> bp2 = bp;
  slip::LMDerivFunctor<slip::funLM_bp<Type>, Type> dh(bp2,y.size(),p.size());
  slip::LMDerivFunctor<slip::funLM_bp<Type>, Type> dh2 = dh;
  slip::funLM_DLT<Type> dlt(y);
  slip::funLM_DLT<Type> dlt2 = dlt;
 
  

}





