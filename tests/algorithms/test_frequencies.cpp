#include <iostream>
#include <iomanip>

#include "Signal.hpp"
#include "GrayscaleImage.hpp"
#include "Volume.hpp"
#include "frequencies.hpp"

int main()
{
  const std::size_t N_odd = 7;
  const std::size_t N_even = 8;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "normalized frequencies"<< std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Array<double> Freqodd(N_odd);
  slip::Array<double> Freqeven(N_even);
  slip::normalized_frequencies(Freqodd.begin(),Freqodd.end());
  slip::normalized_frequencies(Freqeven.begin(),Freqeven.end());
  std::cout<<"Freqodd = \n"<<Freqodd<<std::endl;
  std::cout<<"Freqeven = \n"<<Freqeven<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "shifted normalized frequencies "<< std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  Freqodd = 0.0;
  Freqeven = 0.0;
  slip::normalized_frequencies(Freqodd.begin(),Freqodd.end(),true);
  slip::normalized_frequencies(Freqeven.begin(),Freqeven.end(),true);
  std::cout<<"Freqodd = \n"<<Freqodd<<std::endl;
  std::cout<<"Freqeven = \n"<<Freqeven<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "angular frequencies"<< std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  Freqodd = 0.0;
  Freqeven = 0.0;
  slip::angular_frequencies(Freqodd.begin(),Freqodd.end());
  slip::angular_frequencies(Freqeven.begin(),Freqeven.end());
  std::cout<<"Freqodd = \n"<<Freqodd<<std::endl;
  std::cout<<"Freqeven = \n"<<Freqeven<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "shifted angular frequencies "<< std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  Freqodd = 0.0;
  Freqeven = 0.0;
  slip::angular_frequencies(Freqodd.begin(),Freqodd.end(),true);
  slip::angular_frequencies(Freqeven.begin(),Freqeven.end(),true);
  std::cout<<"Freqodd = \n"<<Freqodd<<std::endl;
  std::cout<<"Freqeven = \n"<<Freqeven<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "frequencies"<< std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  Freqodd = 0.0;
  Freqeven = 0.0;
  double Fs = 4000.0;
  slip::frequencies(Freqodd.begin(),Freqodd.end(),Fs);
  slip::frequencies(Freqeven.begin(),Freqeven.end(),Fs);
  std::cout<<"Freqodd = \n"<<Freqodd<<std::endl;
  std::cout<<"Freqeven = \n"<<Freqeven<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "shifted frequencies "<< std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  Freqodd = 0.0;
  Freqeven = 0.0;
  slip::frequencies(Freqodd.begin(),Freqodd.end(),Fs,true);
  slip::frequencies(Freqeven.begin(),Freqeven.end(),Fs,true);
  std::cout<<"Freqodd = \n"<<Freqodd<<std::endl;
  std::cout<<"Freqeven = \n"<<Freqeven<<std::endl;
  
}

