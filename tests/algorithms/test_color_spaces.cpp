#include <iostream>
#include <algorithm>
#include <numeric>
#include "color_spaces.hpp"
#include "Color.hpp"
#include "Array2d.hpp"

int main()
{

  slip::Array2d<slip::Color<int> > M(4,5);
  
  int k = 0;
  for(std::size_t i = 0; i < M.dim1(); ++i)
    {
      for(std::size_t j = 0; j < M.dim2(); ++j)
	{
	  M[i][j][0] = ++k;
	  M[i][j][1] = ++k;
	  M[i][j][2] = ++k;
	}
    }

  std::cout<<M<<std::endl;

  slip::Array2d<slip::Color<float> > M2(4,5);

  std::transform(M.begin(),M.end(),M2.begin(),slip::RGBtoXYZ<int,float>());

  std::cout<<M2<<std::endl;

  slip::Array2d<int> M3(4,5);
  std::transform(M.begin(),M.end(),M3.begin(),slip::RGBtoL<int,int>());
  std::cout<<M3<<std::endl;

  //slip::Color<double> RGB0(0.255*255.0,0.104*255.0,0.918*255.0);
  slip::Color<double> RGB0(0.255,0.104,0.918);
  std::cout<<"RGB0 = "<<RGB0<<std::endl;
  slip::RGBtoHSI_01<double,double> rgbtohsi01;
  slip::Color<double> HSI = rgbtohsi01(RGB0);
  std::cout<<"HSI = "<<HSI<<std::endl;
  slip::HSItoRGB_01<double,double> hsitorgb01;

  slip::Color<double> RGB1 = hsitorgb01(HSI);
  std::cout<<"RGB1 = "<<RGB1<<std::endl;

  slip::Color<double> RGB2(0.255*255.0,0.104*255.0,0.918*255.0);
  std::cout<<"RGB2 = "<<RGB2<<std::endl;
  slip::RGBtoHSI<double,double> rgbtohsi;
  slip::Color<double> HSI2 = rgbtohsi(RGB2);
  std::cout<<"HSI2 = "<<HSI2<<std::endl;
  slip::HSItoRGB<double,double> hsitorgb;

  slip::Color<double> RGB3 = hsitorgb(HSI2);
  std::cout<<"RGB3 = "<<RGB3<<std::endl;

}
