#include "Polynomial.hpp"
#include "MultivariatePolynomial.hpp"
#include "polynomial_algo.hpp"
#include "gram_schmidt.hpp"
#include "Array2d.hpp"
#include "linear_algebra.hpp"
#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>
#include <iterator>
#include <iomanip>


 template<class T>
  struct inner_prod_legendre : public std::binary_function<slip::Polynomial<T>,
							 slip::Polynomial<T>,T>
  {
    inner_prod_legendre(){}
    T operator() (const slip::Polynomial<T>& x, const slip::Polynomial<T>& y)
    {
      slip::Polynomial<T> z = x * y;
      z.integral();
      return z.evaluate(T(1)) - z.evaluate(T(-1));
    }
  };

template<class T>
struct inner_prod_legendre_2d : public std::binary_function<slip::MultivariatePolynomial<T,2>,
						     slip::MultivariatePolynomial<T,2>,T>
{
  inner_prod_legendre_2d(){}
  T operator() (const slip::MultivariatePolynomial<T,2>& x, const slip::MultivariatePolynomial<T,2>& y)
  {
    slip::MultivariatePolynomial<T,2> z = x * y;
    z.integral(1);
    slip::MultivariatePolynomial<T,2> zint;
    zint =  (z.partial_evaluate(1,1) - z.partial_evaluate(1,-1));
    zint.integral(2);
    slip::block<T,2> b1;
    b1[0] = T(0);
    b1[1] = T(1);
    slip::block<T,2> b2;
    b2[0] = T(0);
    b2[1] = T(-1);
    return zint.evaluate(b1.begin(),b1.end()) - zint.evaluate(b2.begin(),b2.end());
  }
};

int main()
{
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Eval power basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
  float p3[] = {1.0, 2.0, 3.0};
  slip::Polynomial<float> P3(2,p3);
  std::cout<<"P3 = "<<P3<<std::endl;
  std::cout<<"P3("<<2.3<<") = "<<P3.evaluate(2.3f)<<std::endl;
  std::cout<<"P3("<<2.3<<") = "<<slip::eval_horner(P3.begin(),P3.end(),2.3f)<<std::endl;
  std::cout<<"P3("<<0.0<<") = "<<slip::eval_horner(P3.begin(),P3.end(),0.0f)<<std::endl;
  std::cout<<"P3("<<1.0<<") = "<<slip::eval_horner(P3.begin(),P3.end(),1.0f)<<std::endl;
  std::cout<<"P3("<<-1.0<<") = "<<slip::eval_horner(P3.begin(),P3.end(),-1.0f)<<std::endl;
  slip::Vector<float> power(6);
  slip::eval_power_basis(2.0f,power.size()-1,power.begin(),power.end());
  std::cout<<power<<std::endl;
  slip::eval_power_basis(0.0f,power.size()-1,power.begin(),power.end());
  std::cout<<power<<std::endl;
  slip::eval_power_basis(1.0f,power.size()-1,power.begin(),power.end());
  std::cout<<power<<std::endl;
  slip::eval_power_basis(-1.0f,power.size()-1,power.begin(),power.end());
  std::cout<<power<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Eval Legendre basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  power.fill(0.0f);
  slip::eval_legendre_basis(2.0f,power.size()-1,power.begin(),power.end());
  std::cout<<power<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Eval Chebychev basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  power.fill(0.0f);
  slip::eval_chebyshev_basis(2.0f,power.size()-1,power.begin(),power.end());
  std::cout<<power<<std::endl;


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Eval Chebychev II basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  power.fill(0.0f);
  slip::eval_chebyshevII_basis(2.0f,power.size()-1,power.begin(),power.end());
  std::cout<<power<<std::endl;
  

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Eval Chebychev II basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  power.fill(0.0f);
  slip::eval_discrete_chebyshev_basis(2.0f,power.size()-1,power.begin(),power.end());
  std::cout<<power<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Eval Hermite basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  power.fill(0.0f);
  slip::eval_hermite_basis(2.0f,power.size()-1,power.begin(),power.end());
  std::cout<<power<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Eval Laguerre basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  power.fill(0.0f);
  slip::eval_laguerre_basis(2.0f,power.size()-1,power.begin(),power.end());
  std::cout<<power<<std::endl;


    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "Eval power basis functor" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    slip::EvalPowerBasis<float,slip::Vector<float>::iterator> power_basis;
    power_basis(2.0f,power.size()-1,power.begin(),power.end());
    std::cout<<power<<std::endl;

     std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "Eval Legendre basis functor" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    slip::EvalLegendreBasis<float,slip::Vector<float>::iterator> leg_basis;
    leg_basis(2.0f,power.size()-1,power.begin(),power.end());
    std::cout<<power<<std::endl;

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "Eval Chebyshev basis functor" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    slip::EvalChebyshevBasis<float,slip::Vector<float>::iterator> tcheb_basis;
    tcheb_basis(2.0f,power.size()-1,power.begin(),power.end());
    std::cout<<power<<std::endl;


    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "Eval power2d basis" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    slip::Vector<float> X(2); 
    X[0] = 2.0f;
    X[1] = 3.0f;

    slip::Vector<float> power_2d(21);
    slip::eval_power_2d_basis(X,power.size()-1,power_2d.begin(),power_2d.end());
    std::cout<<power_2d<<std::endl;

    slip::EvalPower2dBasis<slip::Vector<float>,slip::Vector<float>::iterator> power2d_basis;
    power2d_basis(X,power.size()-1,power_2d.begin(),power_2d.end());
    std::cout<<power_2d<<std::endl;

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "Eval power nd basis" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    slip::Vector<float> X3(3); 
    X3[0] = 2.0f;
    X3[1] = 3.0f;
    X3[2] = 4.0f;
    slip::Vector<float> power_nd(10);
    slip::eval_power_nd_basis(X3,2,power_nd.begin(),power_nd.end());
    std::cout<<power_nd<<std::endl;
    slip::EvalPowerNdBasis<slip::Vector<float>,slip::Vector<float>::iterator> powernd_basis;
    powernd_basis(X3,2,power_nd.begin(),power_nd.end());
    std::cout<<power_nd<<std::endl;

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "Generate Legendre basis " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  
    slip::Polynomial<double> P0(0,1.0);
    std::cout<<P0<<std::endl;
    slip::Polynomial<double> P1(1,1.0);
    P1[0] = 0.0;
    std::cout<<P1<<std::endl;
 

    std::vector<slip::Polynomial<double> > basis;
  
    basis.push_back(P0);
    basis.push_back(P1);
    
    for(unsigned l = 1; l < 10; ++l)
      basis.push_back(slip::legendre_next<slip::Polynomial<double> >(l, P1, basis[l], basis[l-1]));
    
    for(unsigned l = 0; l < basis.size(); ++l)
      std::cout<<basis[l]<<std::endl;
    
    for(unsigned l = 0; l < basis.size(); ++l)
      basis[l]*=std::sqrt(double(2*l+1)/double(2));
    
    for(unsigned l = 0; l < basis.size(); ++l)
      std::cout<<basis[l]<<std::endl;
    
    slip::Matrix<double> Gram(basis.size(),basis.size());
    slip::gram_matrix(basis.begin(),basis.end(),
		      Gram.upper_left(),Gram.bottom_right(),
		      inner_prod_legendre<double>());
    
    std::cout<<Gram<<std::endl;

     std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Generate chebyshev I basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::vector<slip::Polynomial<double> > chebyshev_basis;
  //creation of Chebyshev polynomial basis
  chebyshev_basis.push_back(P0);
  chebyshev_basis.push_back(P1);
  for(unsigned k = 1; k < 10; ++k)
    {
      chebyshev_basis.push_back(slip::chebyshev_next<slip::Polynomial<double> >(k, P1, chebyshev_basis[k], chebyshev_basis[k-1]));
    }
  for(std::size_t k = 0; k < chebyshev_basis.size(); ++k)
    {
      std::cout<<chebyshev_basis[k]<<std::endl;
    }

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Generate chebyshev II basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::vector<slip::Polynomial<double> > chebyshevII_basis;
  //creation of ChebyshevII polynomial basis
  slip::Polynomial<double> P02(0,1.0);
  slip::Polynomial<double> P12(1,2.0);
  P12[0] = 0.0;
  chebyshevII_basis.push_back(P02);
  chebyshevII_basis.push_back(P12);
  for(unsigned k = 1; k < 10; ++k)
    {
      chebyshevII_basis.push_back(slip::chebyshev_next<slip::Polynomial<double> >(k, P1, chebyshevII_basis[k], chebyshevII_basis[k-1]));
    }
  for(std::size_t k = 0; k < chebyshevII_basis.size(); ++k)
    {
      std::cout<<chebyshevII_basis[k]<<std::endl;
    }
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Generate Hermite basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

  std::vector<slip::Polynomial<double> > hermite_basis;
  //creation of hermite polynomial basis
  hermite_basis.push_back(P02);
  hermite_basis.push_back(P12);
  for(unsigned k = 1; k < 10; ++k)
    {
      hermite_basis.push_back(slip::hermite_next<slip::Polynomial<double> >(k, P1, hermite_basis[k], hermite_basis[k-1]));
    }
  for(std::size_t k = 0; k < hermite_basis.size(); ++k)
    {
      std::cout<<hermite_basis[k]<<std::endl;
    }

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Generate Laguerre basis " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

   std::vector<slip::Polynomial<double> > laguerre_basis;
   //creation of laguerre polynomial basis
   slip::Polynomial<double> P03(0,1.0);
   slip::Polynomial<double> P13(1,-1.0);
   P13[0] = 1.0;
   laguerre_basis.push_back(P03);
   laguerre_basis.push_back(P13);
   for(unsigned k = 1; k < 10; ++k)
    {
    laguerre_basis.push_back(slip::laguerre_next<slip::Polynomial<double> >(k, P1, laguerre_basis[k], laguerre_basis[k-1]));
   }
   for(std::size_t k = 0; k < laguerre_basis.size(); ++k)
    {
      std::cout<<laguerre_basis[k]<<std::endl;
    }

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Generate x1 2d Legendre basis " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    // //Multivariate versions
    // slip::MultivariatePolynomial<double,2> p0;
    // slip::Monomial<2> m01;
    // m01.powers[0] = 0;
    // m01.powers[1] = 0;
    // p0.insert(m01,1.0);


    // slip::MultivariatePolynomial<double,2> p1;
    // slip::Monomial<2> m11;
    // m11.powers[0] = 1;
    // m11.powers[1] = 0;
    // p1.insert(m11,1.0);

    // std::vector<slip::MultivariatePolynomial<double,2> > basis_2d;
    // basis_2d.push_back(p0);
    // basis_2d.push_back(p1);
    // for(unsigned l = 1; l < 10; ++l)
    //   basis_2d.push_back(slip::legendre_nd_next<slip::MultivariatePolynomial<double,2> >(l, p1, basis_2d[l], basis_2d[l-1]));

    // for(unsigned l = 0; l < basis_2d.size(); ++l)
    //   std::cout<<basis_2d[l]<<std::endl;
      slip::MultivariatePolynomial<double,2> pl0;
   slip::Monomial<2> ml00;
   ml00.powers[0] = 0;
   ml00.powers[1] = 0;
   //pl0.insert(ml00,1.0/std::sqrt(2.0));
   pl0.insert(ml00,1.0);
   slip::MultivariatePolynomial<double,2> pl1;
   slip::Monomial<2> ml10;
   ml10.powers[0] = 1;
   ml10.powers[1] = 0;
   //pl1.insert(ml10,std::sqrt(3.0)/2.0);
   pl1.insert(ml10,1.0);
   std::vector<slip::MultivariatePolynomial<double,2> > legendre_x1_basis_2d;
   legendre_x1_basis_2d.push_back(pl0);
   legendre_x1_basis_2d.push_back(pl1);
    for(unsigned l = 1; l < 3; ++l)
      {
	legendre_x1_basis_2d.push_back(slip::legendre_nd_next<slip::MultivariatePolynomial<double,2> >(l, 
													    pl1, 
													    legendre_x1_basis_2d[l], 
													    legendre_x1_basis_2d[l-1]));
      }
    
    for(unsigned l = 0; l < legendre_x1_basis_2d.size(); ++l)
      {
	std::cout<<legendre_x1_basis_2d[l]<<std::endl;
      }

    slip::Matrix<double> Gram_2d(legendre_x1_basis_2d.size(),legendre_x1_basis_2d.size());
    slip::gram_matrix(legendre_x1_basis_2d.begin(),legendre_x1_basis_2d.end(),
		      Gram_2d.upper_left(),Gram_2d.bottom_right(),
		      inner_prod_legendre_2d<double>());

    std::cout<<Gram_2d<<std::endl;


    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Generate 2d x1 Chebyshev basis " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    slip::MultivariatePolynomial<double,2> p0x1;
    slip::Monomial<2> m01x1;
    m01x1.powers[0] = 0;
    m01x1.powers[1] = 0;
    p0x1.insert(m01x1,1.0);


    slip::MultivariatePolynomial<double,2> p1x1;
    slip::Monomial<2> m11x1;
    m11x1.powers[0] = 1;
    m11x1.powers[1] = 0;
    p1x1.insert(m11x1,1.0);

    std::vector<slip::MultivariatePolynomial<double,2> > chebyshev_basis_2d_x1;
    chebyshev_basis_2d_x1.push_back(p0x1);
    chebyshev_basis_2d_x1.push_back(p1x1);
    for(unsigned l = 1; l < 3; ++l)
      {
	chebyshev_basis_2d_x1.push_back(slip::chebyshev_nd_next<slip::MultivariatePolynomial<double,2> >(l, p1x1, chebyshev_basis_2d_x1[l], chebyshev_basis_2d_x1[l-1]));
      }

    for(unsigned l = 0; l < chebyshev_basis_2d_x1.size(); ++l)
      {
	std::cout<<chebyshev_basis_2d_x1[l]<<std::endl;
      }

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Generate 2d x2 Chebyshev basis " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::MultivariatePolynomial<double,2> p0x2;
    slip::Monomial<2> m01x2;
    m01x2.powers[0] = 0;
    m01x2.powers[1] = 0;
    p0x2.insert(m01x2,1.0);


    slip::MultivariatePolynomial<double,2> p1x2;
    slip::Monomial<2> m11x2;
    m11x2.powers[0] = 0;
    m11x2.powers[1] = 1;
    p1x2.insert(m11x2,1.0);
    std::vector<slip::MultivariatePolynomial<double,2> > chebyshev_basis_2d_x2;
    chebyshev_basis_2d_x2.push_back(p0x2);
    chebyshev_basis_2d_x2.push_back(p1x2);
    for(unsigned l = 1; l < 3; ++l)
      {
	chebyshev_basis_2d_x2.push_back(slip::chebyshev_nd_next<slip::MultivariatePolynomial<double,2> >(l, p1x2, chebyshev_basis_2d_x2[l], chebyshev_basis_2d_x2[l-1]));
      }

    for(unsigned l = 0; l < chebyshev_basis_2d_x2.size(); ++l)
      {
	std::cout<<chebyshev_basis_2d_x2[l]<<std::endl;
      }

    
    slip::Matrix<slip::MultivariatePolynomial<double,2> > Chebyshev2dBasis(chebyshev_basis_2d_x1.size(),chebyshev_basis_2d_x1.size());
    
  
    slip::rank1_tensorial_product(chebyshev_basis_2d_x2.begin(),
				  chebyshev_basis_2d_x2.end(),
				  chebyshev_basis_2d_x1.begin(),
				  chebyshev_basis_2d_x1.end(),
				  Chebyshev2dBasis.upper_left(),
				  Chebyshev2dBasis.bottom_right());

    std::cout<<"Chebyshev2dBasis \n"<<Chebyshev2dBasis<<std::endl;


     std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Generate x1 2d Hermite basis " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::MultivariatePolynomial<double,2> ph0;
   slip::Monomial<2> mh00;
   mh00.powers[0] = 0;
   mh00.powers[1] = 0;
   //pl0.insert(ml00,1.0/std::sqrt(2.0));
   ph0.insert(mh00,1.0);
   slip::MultivariatePolynomial<double,2> ph1;
   slip::Monomial<2> mh10;
   mh10.powers[0] = 1;
   mh10.powers[1] = 0;
   //pl1.insert(ml10,std::sqrt(3.0)/2.0);
   pl1.insert(mh10,1.0);
   //std::cout<<"ph1 = "<<ph1<<std::endl;
   std::vector<slip::MultivariatePolynomial<double,2> > hermite_x1_basis_2d;
   hermite_x1_basis_2d.push_back(ph0);
   hermite_x1_basis_2d.push_back(ph1);
    for(unsigned l = 1; l < 10; ++l)
      {
	hermite_x1_basis_2d.push_back(slip::hermite_nd_next<slip::MultivariatePolynomial<double,2> >(l, 
													    ph1, 
													    hermite_x1_basis_2d[l], 
													    hermite_x1_basis_2d[l-1]));
      }
    
    for(unsigned l = 0; l < hermite_x1_basis_2d.size(); ++l)
      {
	std::cout<<hermite_x1_basis_2d[l]<<std::endl;
      }


   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "Generate x1 2d Laguerre basis " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::MultivariatePolynomial<double,2> pla0;
   slip::Monomial<2> mla00;
   mla00.powers[0] = 0;
   mla00.powers[1] = 0;
   pla0.insert(mla00,1.0);
   slip::MultivariatePolynomial<double,2> pla1;
   pla1.insert(mla00,1.0);
   slip::Monomial<2> mla10;
   mla10.powers[0] = 1;
   mla10.powers[1] = 0;
   pla1.insert(mla10,-1.0);

   slip::MultivariatePolynomial<double,2> plax;
   plax.insert(mla10,1.0);
   std::cout<<"plax = "<<plax<<std::endl;
   std::vector<slip::MultivariatePolynomial<double,2> > laguerre_x1_basis_2d;
   laguerre_x1_basis_2d.push_back(pla0);
   laguerre_x1_basis_2d.push_back(pla1);
    for(unsigned l = 1; l < 10; ++l)
      {
	laguerre_x1_basis_2d.push_back(slip::laguerre_nd_next<slip::MultivariatePolynomial<double,2> >(l, 
													    plax, 
													    laguerre_x1_basis_2d[l], 
													    laguerre_x1_basis_2d[l-1]));
      }
    
    for(unsigned l = 0; l < laguerre_x1_basis_2d.size(); ++l)
      {
	std::cout<<laguerre_x1_basis_2d[l]<<std::endl;
      }



    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << " eval_multi_poly_power_basis " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    typedef double T;
     std::vector<T> coefP(6);
  slip::iota(coefP.begin(),coefP.end(),1.0);
  slip::MultivariatePolynomial<T,2> P(2,coefP.begin(),coefP.end());
  std::cout<<"P = \n"<<P<<std::endl;

  std::vector<T> coefQ(6);
  slip::iota(coefQ.begin(),coefQ.end(),2.0);
  slip::MultivariatePolynomial<T,2> Q(2,coefQ.begin(),coefQ.end());
  std::cout<<"Q = \n"<<Q<<std::endl;

  std::vector<slip::MultivariatePolynomial<T,2> > VP(2);
  VP[0] = P;
  VP[1] = Q;

  std::vector<T> coefS(6);
  slip::iota(coefS.begin(),coefS.end(),3.0);
  slip::MultivariatePolynomial<T,2> S(2,coefS.begin(),coefS.end());
  std::cout<<"S = \n"<<S<<std::endl;

  slip::Array2d<slip::MultivariatePolynomial<T,2> > A(VP.size(),S.degree()+1);
   for(std::size_t i = 0; i < A.rows(); ++i)
      {
   	slip::eval_multi_poly_power_basis(VP[i],S.degree(),A.row_begin(i),A.row_end(i));
      }

   
   std::cout<<"P^0 = \n"<<A[0][0]<<std::endl;
   std::cout<<"P^1 = \n"<<A[0][1]<<std::endl;
   std::cout<<"P^2 = \n"<<A[0][2]<<std::endl;
   std::cout<<"Q^0 = \n"<<A[1][0]<<std::endl;
   std::cout<<"Q^1  = \n"<<A[1][1]<<std::endl;
   std::cout<<"Q^2 = \n"<<A[1][2]<<std::endl;
   std::cout<<"P*Q = \n"<<A[0][1]*A[1][1]<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << " eval_multi_poly_power_nd_basis " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::vector<std::size_t> V(2+S.degree());
    slip::iota(V.begin(),V.end(),1,1);
    std::size_t result_size = (std::accumulate(V.begin()+S.degree(),V.end(),1,std::multiplies<int>()))/(std::accumulate(V.begin(),V.begin()+2,1,std::multiplies<int>()));
  
    std::vector<slip::MultivariatePolynomial<T,2> > PolyPowers(result_size);
    slip::eval_multi_poly_power_nd_basis(VP,S.degree(),PolyPowers.begin(),PolyPowers.end());
     for(std::size_t k = 0; k < PolyPowers.size(); ++k)
    {
      std::cout<<"PolyPowers["<<k<<"] = \n"<<PolyPowers[k]<<std::endl;
    }


  slip::Vector<double> Vc(21);
  slip::Vector<double> Vc2(20);
   
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "chebyshev collocations " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::chebyshev_collocations(Vc.begin(),Vc.end(),-1.0,1.0);
  std::cout<<Vc<<std::endl;
  slip::chebyshev_collocations(Vc2.begin(),Vc2.end(),-1.0,1.0);
  std::cout<<Vc2<<std::endl;
  slip::chebyshev_collocations(Vc.begin(),Vc.end(),0.0,256.0);
  std::cout<<Vc<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "chebyshev collocations functor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::ChebyshevCollocations<double> CC(-1.0,1.0);
  CC(Vc.begin(),Vc.end());
  std::cout<<Vc<<std::endl;
  CC(Vc2.begin(),Vc2.end());
  std::cout<<Vc2<<std::endl;
  slip::ChebyshevCollocations<double> CC2(0.0,256.0);
  CC(Vc.begin(),Vc.end());
  std::cout<<Vc<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "fejer weights " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::fejer_weights(Vc.begin(),Vc.end());
  std::cout<<Vc<<std::endl;
  slip::fejer_weights(Vc2.begin(),Vc2.end());
  std::cout<<Vc2<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "fejer collocations " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::fejer_collocations(Vc.begin(),Vc.end());
  std::cout<<Vc<<std::endl;
  slip::fejer_collocations(Vc2.begin(),Vc2.end());
  std::cout<<Vc2<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "uniform collocations " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::uniform_collocations(Vc.begin(),Vc.end(),-1.0,1.0);
  std::cout<<Vc<<std::endl;
  slip::uniform_collocations(Vc2.begin(),Vc2.end(),-1.0,1.0);
  std::cout<<Vc2<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "uniform collocations functor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::UniformCollocations<double> UC(-1.0,1.0);
  UC(Vc.begin(),Vc.end());
  std::cout<<Vc<<std::endl;
  UC(Vc2.begin(),Vc2.end());
  std::cout<<Vc2<<std::endl;


  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Gram uniform collocations " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::gram_uniform_collocations(Vc.begin(),Vc.end(),-1.0,1.0);
  std::cout<<Vc<<std::endl;
  slip::gram_uniform_collocations(Vc2.begin(),Vc2.end(),-1.0,1.0);
  std::cout<<Vc2<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "Gram uniform collocations functor" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::GramUniformCollocations<double> GUC(-1.0,1.0);
  GUC(Vc.begin(),Vc.end());
  std::cout<<Vc<<std::endl;
  GUC(Vc2.begin(),Vc2.end());
  std::cout<<Vc2<<std::endl;



  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "base_poly_nb_lt_degree " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    
  std::cout<<"number of polynomial less than degree "<<4<<" for multivariate polynomials of dimension "<<2<<" = "<<slip::base_poly_nb_lt_degree(2,4)<<std::endl;
  
  std::cout<<"number of polynomial less than degree "<<4<<" for multivariate polynomials of dimension "<<3<<" = "<<slip::base_poly_nb_lt_degree(3,4)<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "lexicographic powers matrix " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  const std::size_t dim_powers = 3;
  const std::size_t degree_powers = 4;
  slip::Matrix<std::size_t> powers_matrix;
  slip::lexicographic_powers_matrix(dim_powers,degree_powers,powers_matrix);
  std::cout<<"powers_matrix = \n"<<powers_matrix<<std::endl;

  const std::size_t degree_powers2 = 3;
  slip::Matrix<std::size_t> powers_matrix2;
  slip::lexicographic_powers_matrix(dim_powers,degree_powers2,powers_matrix2);
  std::cout<<"powers_matrix2 = \n"<<powers_matrix2<<std::endl;
  
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "lexicographic powers matrix less than degree" << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Matrix<std::size_t> powers_lt_degree_matrix;
   slip::lexicographic_powers_matrix_lt_degree(dim_powers,degree_powers,powers_lt_degree_matrix);
   std::cout<<"powers_lt_degree_matrix = \n"<<powers_lt_degree_matrix<<std::endl;
   slip::Matrix<std::size_t> powers_lt_degree_matrix2;
   slip::lexicographic_powers_matrix_lt_degree(dim_powers,degree_powers2,powers_lt_degree_matrix2);
   std::cout<<"powers_lt_degree_matrix2 = \n"<<powers_lt_degree_matrix2<<std::endl;

      std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "lexicographic powers matrix with independant degrees" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    slip::Matrix<std::size_t> powers_degree_matrix_ind;
    slip::Vector<std::size_t> orders(3);
    orders[0] = 3;
    orders[1] = 3;
    orders[2] = 2;
    std::cout<<"orders = "<<orders<<std::endl;
    slip::lexicographic_powers_matrix(dim_powers, orders.begin(), orders.end(),powers_degree_matrix_ind);
    std::cout<<"powers_degree_matrix_ind = \n"<<powers_degree_matrix_ind<<std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "lexicographic powers matrix with independant degrees" << std::endl;
    std::cout <<"less than a maximal order "<<std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    slip::Matrix<std::size_t> powers_degree_matrix_ind_lt;
    std::cout<<"orders = "<<orders<<std::endl;
    std::size_t max_order = 3;
    std::cout<<"max order = "<<max_order<<std::endl;
    slip::lexicographic_powers_matrix(dim_powers, orders.begin(), orders.end(),
				      max_order,
				      powers_degree_matrix_ind_lt);
    std::cout<<"powers_degree_matrix_ind_lt = \n"<<powers_degree_matrix_ind_lt<<std::endl;

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "Vandermonde matrix" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    typedef double T;
    slip::Vector<T> X4(3);
    X4[0] = 2.0;
    X4[1] = 3.0;
    X4[2] = 4.0;
    const std::size_t order = 3;
    std::cout<<"X4 = \n"<<X4<<std::endl;
    std::cout<<"order = \n"<<order<<std::endl;
    slip::Matrix<T> Vander3;
    slip::vandermonde(X4.begin(), X4.end(), order, Vander3);
    std::cout<<"Vander3 = \n" <<Vander3<<std::endl;
    
    slip::Matrix<T> Vander4;
    slip::vandermonde(X4.begin(), X4.end(), order, Vander4, true);
    std::cout<<"Vander4 = \n" <<Vander4<<std::endl;

  
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(10) << ' ';
    std::cout << "nd Vandermonde matrix with independant degrees" << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(10) << ' ';
    std::cout << "and a maximal order" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    const std::size_t dim = 3;
    slip::Matrix<T> Vander5;
    slip::Matrix<T> Data(3, 3);
    Data[0][0] = 2.0;  Data[0][1] = 3.0; Data[0][2] = 4.0;
    Data[1][0] = 3.0;  Data[1][1] = 4.0; Data[1][2] = 5.0;
    Data[2][0] = 4.0;  Data[2][1] = 5.0; Data[2][2] = 6.0;
    
    std::cout<<"Data = \n"<<Data<<std::endl;
    std::cout<<"dim = "<<dim<<std::endl;
    std::cout<<"orders = "<<orders<<std::endl;
    std::cout<<"order = "<<order<<std::endl;
    slip::vandermonde_nd(Data,dim,orders.begin(),orders.end(),order,Vander5);
    std::cout<<"Vander5 = \n" <<Vander5<<std::endl;
    slip::Matrix<T> Vander6;
    slip::vandermonde_nd(Data,dim,orders.begin(),orders.end(),order,Vander6, true);
    std::cout<<"Vander6 = \n" <<Vander6<<std::endl;

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(10) << ' ';
    std::cout << "nd Vandermonde matrix with independant degrees" << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout<<"Data = \n"<<Data<<std::endl;
    std::cout<<"dim = "<<dim<<std::endl;
    std::cout<<"order = "<<order<<std::endl;
    //evaluate 1 x x^2 x^3 y xy x^2y y^2 xy^2 y^3 z xz x^2z yz xyz y^2z z^2 xz^2 z^3 
    //for each row of Data : x_i y_i z_i
    slip::Matrix<T> Vander;
    slip::vandermonde_nd(Data,dim,order,Vander);
    std::cout<<"Vander = \n" <<Vander<<std::endl;
    slip::Matrix<T> Vander2;
    slip::vandermonde_nd(Data,dim,order,Vander2, true);
    std::cout<<"Vander2 = \n" <<Vander2<<std::endl;

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "stieltjes " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    std::size_t fejer_size = 6;
    slip::Array<double> fejer_coll(fejer_size);
    slip::Array<double> fejer_weights(fejer_size);
    
    slip::fejer_collocations(fejer_coll.begin(),fejer_coll.end());
    slip::fejer_weights(fejer_weights.begin(),fejer_weights.end());
    std::cout<<"fejer_collocations = \n"<<fejer_coll<<std::endl;
    std::cout<<"fejer_weights      = \n"<<fejer_weights<<std::endl;

    std::size_t fejer_order = 5;
    slip::Array<double> alpha(fejer_order+1);
    slip::Array<double> beta(fejer_order+1);
   
    slip::Array2d<double> poly(fejer_order+1,fejer_coll.size());
    slip::Array2d<double> poly_w(fejer_order+1,fejer_coll.size());
   
  

    slip::Array<double> pk_pk(fejer_order+1);
    slip::stieltjes(fejer_coll.begin(),fejer_coll.end(),
		    fejer_weights.begin(),fejer_weights.end(),
		    fejer_order,
		    alpha.begin(),alpha.end(),
		    beta.begin(),beta.end(),
 	            pk_pk.begin(),pk_pk.end(),
		    poly,
		    poly_w);
    std::cout<<"alpha   = \n"<<alpha<<std::endl;
    std::cout<<"beta    = \n"<<beta<<std::endl;
    std::cout<<"poly    = \n"<<poly<<std::endl;
    std::cout<<"poly_w  = \n"<<poly_w<<std::endl;
    std::cout<<"pk_pk   = \n"<<pk_pk<<std::endl;
    

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "orthogonality test " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;

    slip::Array2d<double> Grams(fejer_order+1,fejer_order+1);
    for(std::size_t i = 0; i < fejer_order+1; ++i)
      {
	for(std::size_t j = 0; j <= i; ++j)
	  {
	    Grams[i][j] =  slip::discrete_poly_inner_product(poly.row_begin(i),
							    poly.row_end(i),
							    poly.row_begin(j),
							    poly.row_end(j),
							    fejer_weights.begin(),fejer_weights.end(),0.0);
	    Grams[j][i] = Grams[i][j];
	  }
      }
    double epsilon = 1e-15;
    std::cout<<"Gram matrix = \n"<<Grams<<std::endl;
    std::cout<<"slip:is_diagonal(Gram) = "<<slip::is_diagonal(Grams,epsilon)<<std::endl;


    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "legendre jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::size_t legendre_jacobi_order = 5;
    slip::Array<double> alpha_legendre_jacobi(legendre_jacobi_order+1);
    slip::Array<double> beta_legendre_jacobi(legendre_jacobi_order+1);
    slip::legendre_jacobi_coefficients(legendre_jacobi_order,
					   alpha_legendre_jacobi.begin(),
					   alpha_legendre_jacobi.end(),
					   beta_legendre_jacobi.begin(),
					   beta_legendre_jacobi.end());
    std::cout<<"alpha_legendre_jacobi = \n"<<alpha_legendre_jacobi<<std::endl;
    std::cout<<"beta_legendre_jacobi  = \n"<<beta_legendre_jacobi<<std::endl;
					   
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "legendre base with jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::vector<slip::MultivariatePolynomial<double,2> > legendre_x1_basis_2d2;
    slip::stieltjes(alpha_legendre_jacobi.begin(),
		    alpha_legendre_jacobi.end(),
		    beta_legendre_jacobi.begin(),
		    beta_legendre_jacobi.end(),
		    pl1,
		    legendre_x1_basis_2d2);
    for(std::size_t k = 0; k < legendre_x1_basis_2d2.size(); ++k)
      {
	std::cout<<legendre_x1_basis_2d2[k]<<std::endl;
      }

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "legendre normalized base with jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    // std::vector<slip::MultivariatePolynomial<double,2> > legendre_x1_basis_2d2;
    slip::stieltjes_normalized(alpha_legendre_jacobi.begin(),
		    alpha_legendre_jacobi.end(),
		    beta_legendre_jacobi.begin(),
		    beta_legendre_jacobi.end(),
		    pl1,
		    legendre_x1_basis_2d2);
    for(std::size_t k = 0; k < legendre_x1_basis_2d2.size(); ++k)
      {
	std::cout<<legendre_x1_basis_2d2[k]<<std::endl;
      }

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "chebyshev jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::size_t chebyshev_jacobi_order = 5;
    slip::Array<double> alpha_chebyshev_jacobi(chebyshev_jacobi_order+1);
    slip::Array<double> beta_chebyshev_jacobi(chebyshev_jacobi_order+1);
    slip::chebyshev_jacobi_coefficients(chebyshev_jacobi_order,
					   alpha_chebyshev_jacobi.begin(),
					   alpha_chebyshev_jacobi.end(),
					   beta_chebyshev_jacobi.begin(),
					   beta_chebyshev_jacobi.end());
    std::cout<<"alpha_chebyshev_jacobi = \n"<<alpha_chebyshev_jacobi<<std::endl;
    std::cout<<"beta_chebyshev_jacobi  = \n"<<beta_chebyshev_jacobi<<std::endl;
					   
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "chebyshev base with jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::vector<slip::MultivariatePolynomial<double,2> > chebyshev_x1_basis_2d2;
    slip::stieltjes(alpha_chebyshev_jacobi.begin(),
		    alpha_chebyshev_jacobi.end(),
		    beta_chebyshev_jacobi.begin(),
		    beta_chebyshev_jacobi.end(),
		    pl1,
		    chebyshev_x1_basis_2d2);
    for(std::size_t k = 0; k < chebyshev_x1_basis_2d2.size(); ++k)
      {
	std::cout<<chebyshev_x1_basis_2d2[k]<<std::endl;
      }

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "chebyshev normalized base with jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    slip::stieltjes_normalized(alpha_chebyshev_jacobi.begin(),
			       alpha_chebyshev_jacobi.end(),
			       beta_chebyshev_jacobi.begin(),
			       beta_chebyshev_jacobi.end(),
			       pl1,
			       chebyshev_x1_basis_2d2);
    for(std::size_t k = 0; k < chebyshev_x1_basis_2d2.size(); ++k)
      {
	std::cout<<chebyshev_x1_basis_2d2[k]<<std::endl;
      }

    

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "laguerre jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::size_t laguerre_jacobi_order = 5;
    slip::Array<double> alpha_laguerre_jacobi(laguerre_jacobi_order+1);
    slip::Array<double> beta_laguerre_jacobi(laguerre_jacobi_order+1);
    slip::laguerre_jacobi_coefficients(laguerre_jacobi_order,
					   alpha_laguerre_jacobi.begin(),
					   alpha_laguerre_jacobi.end(),
					   beta_laguerre_jacobi.begin(),
					   beta_laguerre_jacobi.end());
    std::cout<<"alpha_laguerre_jacobi = \n"<<alpha_laguerre_jacobi<<std::endl;
    std::cout<<"beta_laguerre_jacobi  = \n"<<beta_laguerre_jacobi<<std::endl;
					   
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "laguerre base with jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::vector<slip::MultivariatePolynomial<double,2> > laguerre_x1_basis_2d2;
    slip::stieltjes(alpha_laguerre_jacobi.begin(),
		    alpha_laguerre_jacobi.end(),
		    beta_laguerre_jacobi.begin(),
		    beta_laguerre_jacobi.end(),
		    pl1,
		    laguerre_x1_basis_2d2);
    for(std::size_t k = 0; k < laguerre_x1_basis_2d2.size(); ++k)
      {
	std::cout<<laguerre_x1_basis_2d2[k]<<std::endl;
      }

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "laguerre normalized base with jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    slip::stieltjes_normalized(alpha_laguerre_jacobi.begin(),
			       alpha_laguerre_jacobi.end(),
			       beta_laguerre_jacobi.begin(),
			       beta_laguerre_jacobi.end(),
			       pl1,
			       laguerre_x1_basis_2d2);
    for(std::size_t k = 0; k < laguerre_x1_basis_2d2.size(); ++k)
      {
	std::cout<<laguerre_x1_basis_2d2[k]<<std::endl;
      }

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "hermite jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::size_t hermite_jacobi_order = 5;
    slip::Array<double> alpha_hermite_jacobi(hermite_jacobi_order+1);
    slip::Array<double> beta_hermite_jacobi(hermite_jacobi_order+1);
    slip::hermite_jacobi_coefficients(hermite_jacobi_order,
					   alpha_hermite_jacobi.begin(),
					   alpha_hermite_jacobi.end(),
					   beta_hermite_jacobi.begin(),
					   beta_hermite_jacobi.end());
    std::cout<<"alpha_hermite_jacobi = \n"<<alpha_hermite_jacobi<<std::endl;
    std::cout<<"beta_hermite_jacobi  = \n"<<beta_hermite_jacobi<<std::endl;
					   
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "hermite base with jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::vector<slip::MultivariatePolynomial<double,2> > hermite_x1_basis_2d2;
    slip::stieltjes(alpha_hermite_jacobi.begin(),
		    alpha_hermite_jacobi.end(),
		    beta_hermite_jacobi.begin(),
		    beta_hermite_jacobi.end(),
		    pl1,
		    hermite_x1_basis_2d2);
    for(std::size_t k = 0; k < hermite_x1_basis_2d2.size(); ++k)
      {
	std::cout<<hermite_x1_basis_2d2[k]<<std::endl;
      }

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "hermite normalized base with jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    slip::stieltjes_normalized(alpha_hermite_jacobi.begin(),
			       alpha_hermite_jacobi.end(),
			       beta_hermite_jacobi.begin(),
			       beta_hermite_jacobi.end(),
			       pl1,
			       hermite_x1_basis_2d2);
    for(std::size_t k = 0; k < hermite_x1_basis_2d2.size(); ++k)
      {
	std::cout<<hermite_x1_basis_2d2[k]<<std::endl;
      }

    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "discrete chebychev jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::size_t discrete_chebychev_jacobi_order = 5;
    std::size_t N_discrete_chebychev = 10;
    slip::Array<double> alpha_discrete_chebychev_jacobi(discrete_chebychev_jacobi_order+1);
    slip::Array<double> beta_discrete_chebychev_jacobi(discrete_chebychev_jacobi_order+1);
    slip::discrete_chebychev_jacobi_coefficients(discrete_chebychev_jacobi_order,
						     N_discrete_chebychev,					     
					   alpha_discrete_chebychev_jacobi.begin(),
					   alpha_discrete_chebychev_jacobi.end(),
					   beta_discrete_chebychev_jacobi.begin(),
					   beta_discrete_chebychev_jacobi.end());
    std::cout<<"alpha_discrete_chebychev_jacobi = \n"<<alpha_discrete_chebychev_jacobi<<std::endl;
    std::cout<<"beta_discrete_chebychev_jacobi  = \n"<<beta_discrete_chebychev_jacobi<<std::endl;

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
    std::cout << "discrete krawtchouk jacobi coefficients " << std::endl;
    std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
    double p_krawtchouk = 0.5;
    std::size_t discrete_krawtchouk_jacobi_order = 5;
    std::size_t N_discrete_krawtchouk = 10;
    slip::Array<double> alpha_discrete_krawtchouk_jacobi(discrete_krawtchouk_jacobi_order+1);
    slip::Array<double> beta_discrete_krawtchouk_jacobi(discrete_krawtchouk_jacobi_order+1);
    slip::discrete_krawtchouk_jacobi_coefficients(discrete_krawtchouk_jacobi_order,
						     N_discrete_krawtchouk,					     
						      p_krawtchouk,
					   alpha_discrete_krawtchouk_jacobi.begin(),
					   alpha_discrete_krawtchouk_jacobi.end(),
					   beta_discrete_krawtchouk_jacobi.begin(),
					   beta_discrete_krawtchouk_jacobi.end());
    std::cout<<"alpha_discrete_krawtchouk_jacobi = \n"<<alpha_discrete_krawtchouk_jacobi<<std::endl;
    std::cout<<"beta_discrete_krawtchouk_jacobi  = \n"<<beta_discrete_krawtchouk_jacobi<<std::endl;
    return 0;
}
