#include <ios>
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include "GrayscaleImage.hpp"
#include "Volume.hpp"
#include "Array.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "dynamic.hpp"
#include "Box2d.hpp"
#include "statistics.hpp"
#include "macros.hpp"
#include "dst.hpp"



int main(int argc, char **argv)
{
 
  
  
  //std::cout.precision(default_precision);
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dst_8 " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Vector<double> A(8);
  double val1[] = {8,16,24,32,40,48,56,64};

  slip::dst_8(val1,val1+8,A.begin());
  std::cout<<"A = \n"<<A<<std::endl;
  slip::Vector<double> IdstA(8);
  slip::idst_8(A.begin(),A.end(),IdstA.begin());
  std::cout<<"IdstA = \n"<<IdstA<<std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "split_radix_dst " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  A.fill(0.0);

  IdstA.fill(0.0);
  slip::split_radix_dst(val1,val1+8,A.begin());
  std::cout<<"A = \n"<<A<<std::endl;
  slip::split_radix_idst(A.begin(),A.end(),IdstA.begin());
  std::cout<<"IdstA = \n"<<IdstA<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "fftw_dst " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  A.fill(0.0);
  IdstA.fill(0.0);
  slip::fftw_dst(val1,val1+8,A.begin());
  std::cout<<"A = \n"<<A<<std::endl;
  slip::fftw_idst(A.begin(),A.end(),IdstA.begin());
  std::cout<<"IdstA = \n"<<IdstA<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dstI_8 matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> DSTI_8_norm;
  slip::dstI_coeff<double> dst_coeffI_norm(8);
  dst_matrix(dst_coeffI_norm,DSTI_8_norm);
  std::cout<<"DSTI_8_norm = \n"<<DSTI_8_norm<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dstII_8 matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> DSTII_8;
  slip::dstII_coeff<double> dst_coeffII(8,false);
  dst_matrix(dst_coeffII,DSTII_8);
  std::cout<<"DSTII_8 = \n"<<DSTII_8<<std::endl;
  A.fill(0.0);
  slip::matrix_vector_multiplies(DSTII_8.upper_left(),DSTII_8.bottom_right(),
				 val1,val1+8, A.begin(),A.end());
  std::cout<<"A = \n"<<A<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dstIII_8 matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> DSTIII_8;
  slip::dstIII_coeff<double> dst_coeffIII(8,false);
  dst_matrix(dst_coeffIII,DSTIII_8);
  std::cout<<"DSTIII_8 = \n"<<DSTIII_8<<std::endl;
  IdstA .fill(0.0);
  slip::matrix_vector_multiplies(DSTIII_8,A,IdstA);
  std::cout<<"IdstA = \n"<<IdstA<<std::endl;
  

  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dstII_8_norm matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> DSTII_8_norm;
  slip::dstII_coeff<double> dst_coeffII_norm(8,true);
  dst_matrix(dst_coeffII_norm,DSTII_8_norm);
  std::cout<<"DSTII_8_norm = \n"<<DSTII_8_norm<<std::endl;
  A.fill(0.0);
  slip::matrix_vector_multiplies(DSTII_8_norm.upper_left(),
				 DSTII_8_norm.bottom_right(),
				 val1,val1+8, A.begin(),A.end());
  std::cout<<"A = \n"<<A<<std::endl;



  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dstIII_8_norm matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> DSTIII_8_norm;
  slip::dstIII_coeff<double> dst_coeffIII_norm(8,true);
  dst_matrix(dst_coeffIII_norm,DSTIII_8_norm);
  std::cout<<"DSTIII_8_norm = \n"<<DSTIII_8_norm<<std::endl;
  IdstA.fill(0.0);
  slip::matrix_vector_multiplies(DSTIII_8_norm,A,IdstA);
  std::cout<<"IdstA = \n"<<IdstA<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dstIV_8 matrix" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  slip::Matrix<double> DSTIV_8;
  slip::dctIV_coeff<double> dst_coeffIV(8);
  dst_matrix(dst_coeffIV,DSTIV_8);
  std::cout<<"DSTIV_8 = \n"<<DSTIV_8<<std::endl;
  
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dst1d " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  A.fill(0.0);
  slip::dst(val1,val1+8,A.begin());
  std::cout<<"A = \n"<<A<<std::endl;
  IdstA.fill(0.0);
  slip::idst(A.begin(),A.end(),IdstA.begin());
  std::cout<<"IdstA = \n"<<IdstA<<std::endl;

  std::cout<<"--------------"<<std::endl;
  A.fill(0.0);
  slip::Array<double> VAL1(8,val1,val1+8);
  slip::dst(VAL1,A);
  std::cout<<"A = \n"<<A<<std::endl;
  IdstA.fill(0.0);
  slip::idst(A,IdstA);
  std::cout<<"IdstA = \n"<<IdstA<<std::endl;
  
 
  

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dst2d 8x8" << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 
  double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
		  63, 59, 66, 90, 109, 85, 69, 72,
		  62, 59, 68, 113, 144, 104, 66, 73,
		  63, 58, 71, 122, 154, 106, 70, 69,
		  67, 61, 68, 104, 126,  88, 68, 70,
		  79, 65, 60,  70,  77,  68, 58, 75,
		  85, 71, 64,  59,  55,  61, 65, 83,
		  87, 79, 69,  68,  65,  76, 78, 94};
  slip::GrayscaleImage<double> I(8,8,val);
  std::cout<<"I = \n"<<I<<std::endl;
  
  I-=128;
  std::cout<<"I-128 = \n"<<I<<std::endl;
  std::cout<<"mean(I-128) = "<<slip::mean<double>(I.begin(),I.end())<<std::endl;

  slip::GrayscaleImage<double> DST(I.rows(),I.cols());
  slip::dst_8x8(I.upper_left(),I.bottom_right(),DST.upper_left());
  std::cout<<"DST(I) = \n"<<DST<<std::endl;
  slip::GrayscaleImage<double> IDST(I.rows(),I.cols());
  slip::idst_8x8(DST.upper_left(),DST.bottom_right(),
		     IDST.upper_left());
  std::cout<<"IDST(DST(I)) = \n"<<IDST<<std::endl;

  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
  std::cout << "dst2d " << std::endl;
  std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
  std::cout<<"I-128 = \n"<<I<<std::endl;
  DST.fill(0.0);
  slip::dst2d(I.upper_left(),I.bottom_right(),DST.upper_left());
  std::cout<<"DST(I) = \n"<<DST<<std::endl;
  IDST.fill(0.0);
  slip::idst2d(DST.upper_left(),DST.bottom_right(),
	       IDST.upper_left());
   std::cout<<"IDST(DST(I)) = \n"<<IDST<<std::endl;

   std::cout<<"-------------"<<std::endl;
   DST.fill(0.0);
   slip::dst2d(I,DST);
   std::cout<<"DST(I) = \n"<<DST<<std::endl;
   IDST.fill(0.0);
   slip::idst2d(DST,IDST);
   std::cout<<"IDST(DST(I)) = \n"<<IDST<<std::endl;

   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
   std::cout << "dst3d " << std::endl;
   std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
   slip::Volume<double> Vol(8,8,8);
   slip::iota(Vol.begin(),Vol.end(),0.0,1.0);
   //std::cout<<"Vol = \n"<<Vol<<std::endl;
   slip::Volume<double> DSTVol(8,8,8);
   slip::dst3d(Vol.front_upper_left(),
	       Vol.back_bottom_right(),
	       DSTVol.front_upper_left());
   std::cout<<"DSTVol = \n"<<DSTVol<<std::endl;
   slip::Volume<double> IDSTVol(8,8,8);
   slip::idst3d(DSTVol.front_upper_left(),
		DSTVol.back_bottom_right(),
		IDSTVol.front_upper_left());
   std::cout<<"IDSTVol = \n"<<IDSTVol<<std::endl;

   std::cout<<"-------------"<<std::endl;
   DSTVol.fill(0.0);
   slip::dst3d(Vol,DSTVol);
   std::cout<<"DSTVol = \n"<<DSTVol<<std::endl;
   IDSTVol.fill(0.0);
   slip::idst3d(DSTVol, IDSTVol);
   std::cout<<"IDSTVol = \n"<<IDSTVol<<std::endl;
   
  return 0;
}


