#include <iostream>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <vector>
#include <iomanip>
#include "apply.hpp"
#include "Array2d.hpp"
#include "arithmetic_op.hpp"

float sqr_c(const float& x)
{
  return x * x;
}

float sqr(float x)
{
  return x * x;
}


float atimesb_c(const float& a, const float& b)
{
  return a * b;
}

float atimesb(float a, float b)
{
  return a * b;
}

template<typename T>
      bool lt5Predicate (const T& val)
      {
        return (val < T(5));
      }

int main()
{
  
  slip::Array2d<float> M(4,5);
  
  float k = 0.0;
  for(std::size_t i = 0; i < M.dim1(); ++i)
    {
      for(std::size_t j = 0; j < M.dim2(); ++j)
	{
	  M[i][j] = k;
	  k+=1.0;
	}
    }
  
  std::cout<<M<<std::endl;
  slip::apply(M.begin(),M.end(),M.begin(),std::sqrt);
  std::cout<<M<<std::endl;

  std::vector<float> v(5,-2.0);
  slip::apply(v.begin(),v.end(),v.begin(),sqr);
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<std::endl;

  slip::Array2d<float> M2(M);
  slip::Array2d<float> M3(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),M2.begin(),M3.begin(),atimesb_c);
  std::cout<<M2<<std::endl;
  std::cout<<M3<<std::endl;

  slip::apply(M3.row_begin(2),M3.row_end(2),M3.row_begin(1),M3.row_begin(0),atimesb_c);
  std::cout<<M3<<std::endl;
  slip::apply(M3.col_begin(2),M3.col_end(2),M3.col_begin(1),M3.col_begin(0),atimesb_c);
  std::cout<<M3<<std::endl;


  slip::Array2d<float> MM(3,3);
  slip::iota(MM.begin(),MM.end(),1.0,1.0);
  slip::Array2d<float> M4(MM);
  slip::Array2d<float> M5(MM.dim1(),MM.dim2());
  int f[]={1,1,1,0,1,1,0,1,0};
  slip::Array2d<int>Mask(3,3,f);

std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"apply_mask unary"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"MM = "<<std::endl<<MM<<std::endl;
std::cout<<"Mask = "<<std::endl<<Mask<<std::endl;
slip::apply_mask(MM.begin(),MM.end(),Mask.begin(),MM.begin(),sqr,1);
std::cout<<"MM = "<<std::endl<<MM<<std::endl;

slip::iota(MM.begin(),MM.end(),1.0,1.0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"apply_mask binary"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"MM = "<<std::endl<<MM<<std::endl;
std::cout<<"M4 = "<<std::endl<<M4<<std::endl;
std::cout<<"Mask = "<<std::endl<<Mask<<std::endl;
 slip::apply_mask(MM.begin(),MM.end(),Mask.begin(),M4.begin(),M5.begin(),atimesb,1);
std::cout<<"M5 = "<<std::endl<<M5<<std::endl;

slip::iota(M5.begin(),M5.end(),0,0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"apply_if unary"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout<<"MM = "<<std::endl<<MM<<std::endl;
std::cout<<"Mask = "<<std::endl<<Mask<<std::endl;
slip::apply_if(MM.begin(),MM.end(),MM.begin(),lt5Predicate<float>,sqr);
std::cout<<"MM = "<<std::endl<<MM<<std::endl;

slip::iota(M5.begin(),M5.end(),0,0);
slip::iota(MM.begin(),MM.end(),1.0,1.0);
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
std::cout <<  std::setfill(' ') << std::setw(20) << ' ';
std::cout<<"apply_if binary"<<std::endl;
std::cout <<  std::setfill('_') << std::setw(60) << '_' << std::endl;
 std::cout<<"MM = "<<std::endl<<MM<<std::endl;
std::cout<<"M4 = "<<std::endl<<M4<<std::endl;
std::cout<<"Mask = "<<std::endl<<Mask<<std::endl;
slip::apply_if(MM.begin(),MM.end(),M4.begin(),M5.begin(),lt5Predicate<float>,atimesb_c);
std::cout<<"M5 = "<<std::endl<<M5<<std::endl;


 }
