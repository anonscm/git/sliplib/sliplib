#include <iostream>
#include "derivatives.hpp"
#include "Matrix.hpp"
#include "arithmetic_op.hpp"
#include "Point2d.hpp"
#include "Array3d.hpp"

int main()
{

  slip::Matrix<double> Dinv;
  std::size_t sch_order = 2;
  std::size_t sch_shift = sch_order/2;
  slip::finite_diff_coef(sch_order,sch_shift,Dinv);
  std::cout<<Dinv<<std::endl;
  double h = 0.1;
  std::cout<<"with h = "<<h<<std::endl;
  slip::finite_diff_coef(sch_order,sch_shift,h,Dinv);
  std::cout<<Dinv<<std::endl;
 
  double data[]={1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,11.0,14.0};

 
  std::cout<<"Data to derivate "<<std::endl;
  std::copy(data,data+10,std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  slip::Vector<double> result(10);
  slip::derivative(data,data+ 10, 1,sch_order,sch_shift,Dinv,result.begin());
  std::cout<<"First derivative, order = "<<sch_order<<" : "<<std::endl;
  std::copy(result.begin(),result.end(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"-------------------"<<std::endl;
  sch_order = 4;
  sch_shift = sch_order/2;
  slip::finite_diff_coef(sch_order,sch_shift,Dinv);
  std::cout<<Dinv<<std::endl;
  slip::derivative(data,data+ 10, 1,sch_order,sch_shift,Dinv,result.begin());
  std::cout<<"First derivative, order = "<<sch_order<<" : "<<std::endl;
  std::copy(result.begin(),result.end(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  
  std::cout<<"-------------------"<<std::endl;
  sch_order = 5;
  sch_shift = sch_order/2;
  slip::finite_diff_coef(sch_order,sch_shift,Dinv);
  std::cout<<Dinv<<std::endl;
  slip::derivative(data,data+ 10, 1,sch_order,sch_shift,Dinv,result.begin());
  std::cout<<"First derivative, order = "<<sch_order<<" : "<<std::endl;
  std::copy(result.begin(),result.end(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;
  

  std::cout<<"compute finite derivatives kernels "<<std::endl;
  std::size_t der_order = 1;
  sch_order = 5;
  sch_shift = sch_order/2;
  
  //computes all kernels
  slip::Matrix<double> kernels(sch_order + 1, sch_order + 1);
  std::vector<slip::Matrix<double>::iterator> kernels_iterators(sch_order + 1);   
  for(std::size_t i = 0; i < (sch_order + 1); ++i)
    {
      kernels_iterators[i] = kernels.row_begin(i);
    }
  slip::finite_diff_kernels(der_order,sch_order,sch_shift,
			    kernels_iterators);
  for(std::size_t i = 0; i < (sch_order + 1); ++i)
    {
      std::cout<<"kernel  "<<i<<std::endl;
      std::copy(kernels_iterators[i],kernels_iterators[i]+(sch_order + 1),std::ostream_iterator<double>(std::cout," "));
      std::cout<<std::endl;
		
		
    }
  std::cout<<"-----------------------------------"<<std::endl;
  std::cout<<"compute finite derivatives kernels "<<std::endl;
  std::cout<<"with h = "<<h<<std::endl;
  std::cout<<"-----------------------------------"<<std::endl;
  for(std::size_t i = 0; i < (sch_order + 1); ++i)
    {
      kernels_iterators[i] = kernels.row_begin(i);
    }
  slip::finite_diff_kernels(der_order,sch_order,sch_shift,h,
			    kernels_iterators);
  for(std::size_t i = 0; i < (sch_order + 1); ++i)
    {
      std::cout<<"kernel  "<<i<<std::endl;
      std::copy(kernels_iterators[i],kernels_iterators[i]+(sch_order + 1),std::ostream_iterator<double>(std::cout," "));
      std::cout<<std::endl;
		
		
    }

  std::cout<<"pre-computed kernels derivative ...."<<std::endl;
  slip::derivative(data,data+ 10, 1,sch_order,sch_shift,kernels_iterators,result.begin());
  std::cout<<"First derivative, order = "<<sch_order<<" : "<<std::endl;
  std::copy(result.begin(),result.end(),std::ostream_iterator<double>(std::cout," "));
  std::cout<<std::endl;

  

  slip::Matrix<double> M2d(5,6);
  slip::Matrix<double> M2d_res(5,6);
  const std::size_t der_order2 = 1;
  const std::size_t sch_order2 = 2;
  std::cout<<"M2d = \n"<<M2d<<std::endl;
  slip::iota(M2d.begin(),M2d.end(),1.0,1.0);
  slip::derivative_2d(M2d.upper_left(),M2d.bottom_right(),
		      slip::Point2d<double>(1.0,0.5),
		      slip::X_DIRECTION,
		      der_order2,
		      sch_order2,
		      M2d_res.upper_left(),
		      M2d_res.bottom_right());
  std::cout<<"M2d_res = \n"<<M2d_res<<std::endl;


  slip::Dx<double> toto;

  slip::Array3d<double> M3d(8,5,6);
  slip::Array3d<double> M3d_res(8,5,6);
  slip::iota(M3d.begin(),M3d.end(),1.0);
  std::cout<<"M3d = \n"<<M3d<<std::endl;
  slip::derivative_3d(M3d.front_upper_left(),M3d.back_bottom_right(),
		      slip::Point3d<double>(1.0,0.5,0.5),
		      slip::Z_DIRECTION,
		      der_order2,
		      sch_order2,
		      M3d_res.front_upper_left(),
		      M3d_res.back_bottom_right());
  std::cout<<"M3d_res = \n"<<M3d_res<<std::endl;

  return 0;

}
