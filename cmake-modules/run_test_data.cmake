# test_cmd is the command to run with all its arguments
if( NOT test_cmd )
   message( FATAL_ERROR "Variable test_cmd not defined" )
endif( NOT test_cmd )
# output_ref contains the name of the reference output file with the desired standard output.
if( NOT std_output_ref )
   message( FATAL_ERROR "Variable std_output_ref not defined" )
endif( NOT std_output_ref )
# output_test contains the name of the output file with the standard output the test_cmd will produce
if( NOT std_output_test )
   message( FATAL_ERROR "Variable std_output_test not defined" )
endif( NOT std_output_test )

if( input_data )
separate_arguments(input_data)
LIST(LENGTH input_data li )
math(EXPR li2 "${li} - 1")
foreach(val RANGE ${li2})
list(GET input_data ${val} ifname)
set(test_arg "${test_arg} ${input_data_path}${ifname}")
endforeach()
endif( input_data )

if( output_data )
separate_arguments(output_data)
LIST(LENGTH output_data lo )
math(EXPR lo2 "${lo} - 1")
foreach(val RANGE ${lo2})
list(GET output_data ${val} ofname)
set(test_arg "${test_arg} ${output_data_path}${ofname}")
endforeach()
endif( output_data )

separate_arguments(test_arg)

#message("COMMAND : " ${test_cmd})
#message("ARGUMENTS : " ${test_arg})

execute_process(
   COMMAND ${test_cmd} ${test_arg}
   OUTPUT_FILE ${std_output_test}
)

execute_process( COMMAND ${CMAKE_COMMAND} -E compare_files ${std_output_ref} ${std_output_test}
RESULT_VARIABLE test_not_successful
OUTPUT_QUIET
ERROR_QUIET
)
if( test_not_successful )
   message( SEND_ERROR "${std_output_test} does not match ${std_output_ref}!" )
endif( test_not_successful )


if ( output_data )
foreach(val RANGE ${lo2})
list(GET output_data ${val} fname)
execute_process( COMMAND ${CMAKE_COMMAND} -E compare_files ${output_data_ref_path}${fname} ${output_data_path}${fname}
RESULT_VARIABLE test_not_successful
OUTPUT_QUIET
ERROR_QUIET )
if( test_not_successful )
   message( SEND_ERROR "${output_data_ref_path}${fname} does not match ${output_data_path}${fname}!" )
endif( test_not_successful )
endforeach()
endif( output_data )
