if(NOT CCFITS_FOUND)

SET(TRIAL_PATHS
 $ENV{CCFITS_ROOT_DIR}
 $ENV{CCFITS_ROOT_DIR}/CCfits
 $ENV{CCFITS_ROOT_DIR}/include/CCfits
 /usr/include/CCfits
 /usr/local/include/CCfits
 /opt/local/include/CCfits
 /sw/include/CCfits
 $ENV{CCFITS_ROOT_DIR}/lib
 /usr/lib 
 /usr/local/lib
 /opt/local/lib
 /sw/lib
 )

find_path(CCFITS_INCLUDE_DIR NAMES CCfits.h HINTS ${TRIAL_PATHS} DOC "Include for CCFITS")
find_library(CCFITS_LIBRARY NAMES libCCfits.so libCCfits.a libCCfits.dylib libCCfits.la HINTS 
${TRIAL_PATHS} DOC "CCFITS library")

find_package(CFITSIO)
if (NOT CFITSIO_FOUND)
message("CFITSIO not found!")
else(NOT CFITSIO_FOUND)
message("CFITSIO found")
mark_as_advanced(CCFITS_INCLUDE_DIR CCFITS_LIBRARY)
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CCFITS DEFAULT_MSG CCFITS_LIBRARY CCFITS_INCLUDE_DIR)
set(CCFITS_INCLUDE_DIRS ${CCFITS_INCLUDE_DIR} ${CFITSIO_INCLUDE_DIR})
set(CCFITS_LIBRARIES ${CCFITS_LIBRARY} ${CFITSIO_LIBRARY})
message("CCFITS_INCLUDE_DIR = ${CCFITS_INCLUDE_DIR}")
message("CCFITS_LIBRARIES = ${CCFITS_LIBRARIES}")
endif(NOT CFITSIO_FOUND)
endif(NOT CCFITS_FOUND)