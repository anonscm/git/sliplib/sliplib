#  LIBAV_FOUND - system has Libav libraries
#  LIBAV_INCLUDE_DIR - the Libav include directory
#  LIBAV_LIBRARIES - link these to use Libav

IF(WIN32)
    FIND_PATH( LIBAV_DIR include/libavformat/avformat.h PATH_SUFFIXES .. )
    GET_FILENAME_COMPONENT(LIBAV_DIR ${LIBAV_DIR} ABSOLUTE )
    SET(LIBAV_DIR ${LIBAV_DIR} CACHE FILEPATH "" FORCE)

    FIND_PATH( LIBAV_INCLUDE_DIR libavformat/avformat.h HINTS ${LIBAV_DIR}/include )

    FIND_LIBRARY( LIBAV_avutil_LIBRARY avutil HINTS ${LIBAV_DIR}/lib )
    FIND_LIBRARY( LIBAV_avcodec_LIBRARY avcodec HINTS ${LIBAV_DIR}/lib )
    FIND_LIBRARY( LIBAV_avformat_LIBRARY avformat HINTS ${LIBAV_DIR}/lib )
    FIND_LIBRARY( LIBAV_swscale_LIBRARY swscale HINTS ${LIBAV_DIR}/lib )

    FIND_PACKAGE_HANDLE_STANDARD_ARGS(LIBAV DEFAULT_MSG 
        LIBAV_INCLUDE_DIR
        LIBAV_avutil_LIBRARY
        LIBAV_avcodec_LIBRARY
        LIBAV_avformat_LIBRARY
        LIBAV_swscale_LIBRARY )

    IF(LIBAV_FOUND)
        SET(LIBAV_LIBRARIES
            ${LIBAV_avcodec_LIBRARY}
            ${LIBAV_avformat_LIBRARY}
            ${LIBAV_avutil_LIBRARY}
            ${LIBAV_swscale_LIBRARY} )
    ENDIF()
ELSE()
    INCLUDE(FindPkgConfig)
    SET(_PACKAGE_ARGS libavutil>=51.22 libavcodec>=53.35 libavformat>=53.21 libswscale>=2.1)
    PKG_CHECK_MODULES( LIBAV_PKGCONF ${_PACKAGE_ARGS} )
    FIND_PATH(LIBAV_INCLUDE_DIR libavformat/avformat.h HINTS ${LIBAV_PKGCONG_INCLUDE_DIRS})
    
    FIND_LIBRARY( LIBAV_avutil_LIBRARY avutil HINTS ${LIBAV_PKGCONF_LIBRARY_DIRS} )
    FIND_LIBRARY( LIBAV_avcodec_LIBRARY avcodec HINTS ${LIBAV_PKGCONF_LIBRARY_DIRS} )
    FIND_LIBRARY( LIBAV_avformat_LIBRARY avformat HINTS ${LIBAV_PKGCONF_LIBRARY_DIRS} )
    FIND_LIBRARY( LIBAV_swscale_LIBRARY swscale HINTS ${LIBAV_PKGCONF_LIBRARY_DIRS} )

    FIND_PACKAGE_HANDLE_STANDARD_ARGS(LIBAV DEFAULT_MSG 
        LIBAV_INCLUDE_DIR
        LIBAV_avcodec_LIBRARY
        LIBAV_avformat_LIBRARY
        LIBAV_avutil_LIBRARY
        LIBAV_swscale_LIBRARY )

    IF(LIBAV_FOUND)
        SET(LIBAV_LIBRARIES
            ${LIBAV_avcodec_LIBRARY}
            ${LIBAV_avformat_LIBRARY}
            ${LIBAV_avutil_LIBRARY}
            ${LIBAV_swscale_LIBRARY}
            ${LIBAV_PKGCONF_LDFLAGS_OTHER} )            
        ENDIF()
ENDIF()

# Determine library's version

FIND_PATH (LIBAV_ROOT_DIR
  NAMES include/libavcodec/avcodec.h
        include/libavdevice/avdevice.h
        include/libavfilter/avfilter.h
        include/libavutil/avutil.h
        include/libswscale/swscale.h
  PATHS ENV LIBAVROOT
  DOC "libav root directory")


FIND_PROGRAM (LIBAV_AVCONV_EXECUTABLE NAMES avconv
  HINTS ${LIBAV_ROOT_DIR}
  PATH_SUFFIXES bin
  DOC "avconv executable")

IF (LIBAV_AVCONV_EXECUTABLE)
  EXECUTE_PROCESS (COMMAND ${LIBAV_AVCONV_EXECUTABLE} -version
    OUTPUT_VARIABLE _LIBAV_AVCONV_OUTPUT ERROR_QUIET)

  STRING (REGEX REPLACE ".*avconv[ \t]+([0-9]+\\.[0-9]+\\.[0-9]+).*" "\\1"
    LIBAV_VERSION "${_LIBAV_AVCONV_OUTPUT}")
  STRING (REGEX REPLACE "([0-9]+)\\.([0-9]+)\\.([0-9]+)" "\\1"
    LIBAV_VERSION_MAJOR "${LIBAV_VERSION}")
  STRING (REGEX REPLACE "([0-9]+)\\.([0-9]+)\\.([0-9]+)" "\\2"
    LIBAV_VERSION_MINOR "${LIBAV_VERSION}")
  STRING (REGEX REPLACE "([0-9]+)\\.([0-9]+)\\.([0-9]+)" "\\3"
    LIBAV_VERSION_PATCH "${LIBAV_VERSION}")

  SET (LIBAV_VERSION_COMPONENTS 3)
ENDIF (LIBAV_AVCONV_EXECUTABLE)

