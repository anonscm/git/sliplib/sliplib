#include <iostream>
#include <cmath>
#include <algorithm> //std::min, std::max
int main()
{

  double PI = 3.1415;
  //Trigonometric functions:
  std::cout<<std::cos(PI/4.0)<<std::endl;
  std::cout<<std::sin(PI/4.0)<<std::endl;
  std::cout<<std::tan(PI/4.0)<<std::endl;
  std::cout<<std::acos(0.35)<<std::endl;
  std::cout<<std::asin(1.53)<<std::endl;
  std::cout<<std::atan(1.0)<<std::endl;
  std::cout<<std::atan2(0.25,0.5)<<std::endl;
 
  //Hyperbolic functions
  std::cout<<std::cosh(1.0)<<std::endl;
  std::cout<<std::sinh(1.0)<<std::endl;
  std::cout<<std::tanh(1.0)<<std::endl;
  
  //Exponential and logarithmic functions:
  std::cout<<std::exp(1.0)<<std::endl;
  std::cout<<std::log(1.24)<<std::endl;
  std::cout<<std::log10(100)<<std::endl;
  //     double frexp (      double x, int * exp );
  //    float frexp (       float x, int * exp );
  // long double frexp ( long double x, int * exp );
  //Breaks the floating point number x into its binary significand 
  //(a floating point value between 0.5 and 1.0) and an integral
  //exponent for 2, such that:
  //x = significand * 2 exponent
  //The exponent is stored in the location pointed by exp, and the significand is the value returned by the function.
  //If x is zero, both parts (significand and exponent) are zero.
  int n;
  double param = 15.2;
  double result = std::frexp (param , &n);
  std::cout<<result<<" "<<param<<" "<<n<<std::endl;
  // double ldexp (      double x, int exp );
  // float ldexp (       float x, int exp );
  // long double ldexp ( long double x, int exp );
  //Generate number from significand and exponent
  //Returns the resulting floating point value from multiplying x 
  //(the significand) by 2 raised to the power of exp (the exponent).
  n = 4;
  param = 0.95;
  result = std::ldexp (param , n);
  std::cout<<param<<" "<<n<<" "<<result<<std::endl;
  //       double modf ( long double x, long double * intpart );
  //long double modf ( long double x, long double * intpart );
  //    float modf (       float x,       float * intpart );
  //Break into fractional and integral parts
  //Breaks x into two parts: the integer part (stored in the object 
  //pointed by intpart) and the fractional part (returned by the function).
  //Each part has the same sign as x.
  param = 3.14159265;
  double intpart;
  double fractpart = std::modf (param , &intpart);
  std::cout<<param<<" "<<intpart<<" "<<fractpart<<std::endl;

  //Power functions
  std::cout<<std::sqrt(2.0)<<std::endl;
  std::cout<<std::pow(2.21,3.45)<<std::endl;
  std::cout<<std::pow(2.21,4)<<std::endl;
  
  //Rounding, absolute value and remainder functions:
  std::cout<<std::floor(1.53)<<std::endl;
  std::cout<<std::ceil(1.53)<<std::endl;
  std::cout<<std::fabs(-2.35)<<std::endl;
  std::cout<<std::abs(-4.2)<<std::endl;
  std::cout<<std::abs(-4.2f)<<std::endl;
  std::cout<<std::fmod(5.3,2)<<std::endl;
  std::cout<<std::fmod(2.3,4.5)<<std::endl;

  //min, max
  std::cout<<std::min(4,6)<<std::endl;
  std::cout<<std::max(4,6)<<std::endl;
  std::cout<<std::min(4.5,6.3)<<std::endl;
  std::cout<<std::max(4.5,6.3)<<std::endl;
  std::cout<<std::min('a','z')<<std::endl;
  std::cout<<std::max('a','z')<<std::endl;

  
  return 0;
}
