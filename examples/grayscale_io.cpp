#include <iostream>
#include <slip/GrayscaleImage.hpp>
#include <slip/io_tools.hpp>
#include <slip/dynamic.hpp>

int main()
{
  slip::GrayscaleImage<double> I1(4,5,1.1);
 //print image to terminal using std::cout
 std::cout<<"I1 = \n"<<I1<<std::endl;
 //print image to terminal using print
 slip::print(I1,"I1");
 
 //read an image from file
 //For integer valued images, ImageMagick impose values of
 //the image to be in the range [min(type),max(type)]
 //where type is the integer type
 slip::GrayscaleImage<unsigned char> I2;
 I2.read("lena.gif");	
 //get image dimensions
 const std::size_t rows = I2.rows();
 const std::size_t cols = I2.cols();
 slip::GrayscaleImage<float> I3(rows,cols);
 //change dynamic to [0.0,1.0]
 //ImageMagick impose float or double image to have values in [0.0,1.0]
 slip::change_dynamic_01(I2.begin(),I2.end(),I3.begin(),slip::AFFINE_FUNCTION);
 //write I3 to file
 I2.write("lena_f.png");
}
