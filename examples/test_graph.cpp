#include <iostream>
#include <slip/GrayscaleImage.hpp>
#include <slip/dynamic.hpp>
#include <vector>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/property_map/property_map.hpp>

using namespace boost;

 //definition of the node of the graph
  struct Node
  {
    double value;
  };
 //definition of the edge of the graph
  struct Edge
  {
    double poids;
  };

int main(int argc, char **argv)
{
  //---------------------------------
  //Create a 5x5 GrayscaleImage
  //---------------------------------
  slip::GrayscaleImage<double> I(4,5);
  slip::iota(I.begin(),I.end(),0.0,2.0);
  const int rows = static_cast<int>(I.rows());
  const int cols = static_cast<int>(I.cols());
  
 
 
  //print the image
  std::cout<<"I = \n"<<I<<std::endl;

  //typedef for edges indices pair
  typedef std::pair<int,int> Pair;
  //array to record the pairs of edges
  const int edge_pairs_number = (cols - 1) * rows + (rows - 1) * cols;
  std::vector<Pair> edge_array(edge_pairs_number);
  std::cout<<"edge pairs number = "<<edge_pairs_number<<std::endl;
  
  //---------------------------------
  //4-connexity edges construction
  //---------------------------------
  std::size_t kc = 0;
  for(int i = 0; i < rows; ++i)
    {
      
      for(int j = 0; j < cols; ++j)
	{
	  
	  if((j-1) >= 0)
	    {
	      edge_array[kc] = Pair(i * cols+j,i* cols+(j-1));
	      kc++;
	    }
	  if((i-1) >= 0)
	    {
	      edge_array[kc] = Pair(i * cols+j,(i-1)* cols+j);
	      kc++;
	    }
	}
    }
  
  std::cout<<"kc = "<<kc<<std::endl;
 
  //print the edge_array elements
  std::cout<<"Edges:\n"<<std::endl;
  for(std::size_t l = 0; l < edge_array.size(); ++l)
    {
      std::cout<<"("<<edge_array[l].first<<","<<edge_array[l].second<<")"<<std::endl;
    }
 


  //----------------------------
  //undirected graph definition
  //----------------------------
  typedef adjacency_list<mapS,vecS,undirectedS,Node,Edge> UGraph;
  //  typedef adjacency_list<mapS, vecS, undirectedS, 
  //                          VertexProperty, EdgeProperty> UGraph;
  // typedef boost::adjacency_matrix<boost::undirectedS> UGraph;
 
  //graph construction from an iterator to the edge_array elements
  UGraph ug(edge_array.begin(), edge_array.begin() + edge_array.size(), rows*cols);
 
   //   
  // previous ligne is equivalent to the following one but is more efficient
  //    UGraph ug(I.rows()*I.cols());
  //    for(int kc = 0; kc < edge_array.size();++kc)
  //     add_edge(edge_array[kc].first, edge_array[kc].second, ug);
 

  // get the property map for vertex indices
  // and print the vertices indices
  typedef property_map<UGraph, vertex_index_t>::type IndexMap;
  IndexMap index = get(vertex_index, ug);
  graph_traits<UGraph>::vertex_iterator vi1 = vertices(ug).first;
  graph_traits<UGraph>::vertex_iterator vi1_end = vertices(ug).second;
  std::cout<<"Vertices indices:"<<std::endl;
  for (; vi1 != vi1_end; ++vi1)
    {
      std::cout<<index[*vi1]<<" "; 
    }
  std::cout<<std::endl;
  
  //get the first and last elements of the graph ug
  vi1 = vertices(ug).first;
  vi1_end = vertices(ug).second;
  
  //copy the GrayscaleImage value in the Nodes of the graph
  slip::GrayscaleImage<double>::const_iterator itI = I.begin();
  for (; vi1 != vi1_end; ++vi1, ++itI)
    {
      ug[*vi1].value = *itI;
      std::cout<<ug[*vi1].value<<" "; 
    }
  std::cout<<std::endl;

  
  std::cout << "vertex set: ";
  graph_traits<UGraph>::vertex_iterator vi,vi_end;
    for (tie(vi,vi_end) = vertices(ug); vi != vi_end; ++vi)
      std::cout <<*vi<< " ";
    std::cout << std::endl;
 
    std::cout << "incident edges: " << std::endl;
    graph_traits<UGraph>::vertex_iterator ui,ui_end;
    for (tie(ui,ui_end) = vertices(ug); ui != ui_end; ++ui) {
      std::cout << *ui << " <--> ";
    graph_traits<UGraph>
        ::out_edge_iterator ei, ei_end;
      for(tie(ei,ei_end) = out_edges(*ui,ug); ei != ei_end; ++ei)
        std::cout << target(*ei,ug) << " ";
      std::cout << std::endl;
    }
  return 0;
}
