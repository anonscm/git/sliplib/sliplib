#include <iostream>
#include <complex>

int main()
{
  std::complex<double> c1;
  std::cout<<c1<<std::endl;
  std::complex<double> c2(4.0);
  std::cout<<c2<<std::endl;
  std::complex<double> c3(4.0,3.0);
  std::cout<<c3<<std::endl;
  std::complex<double> c4 = 2.0;
  std::cout<<c4<<std::endl;
  std::complex<double> c5 = c3;
  std::cout<<c5<<std::endl;

  std::cout<<"real part = "<<c3.real()<<std::endl;
  std::cout<<"imag part = "<<c3.imag()<<std::endl;
  std::cout<<"real part = "<<real(c3)<<std::endl;
  std::cout<<"imag part = "<<imag(c3)<<std::endl;
  std::cout<<"conjugate = "<<conj(c3)<<std::endl;
  std::complex<double> c6 = std::polar(abs(c3),arg(c3));
  std::cout<<c6<<std::endl;
  std::cout<<"abs = "<<abs(c3)<<std::endl;
  std::cout<<"arg = "<<arg(c3)<<std::endl;
  std::cout<<"norm = abs^2 = "<<norm(c3)<<std::endl;
  std::cout<<"sin = "<<sin(c3)<<std::endl;
  std::cout<<"sinh = "<<sinh(c3)<<std::endl;
  std::cout<<"cos = "<<cos(c3)<<std::endl;
  std::cout<<"cosh = "<<cosh(c3)<<std::endl;
  std::cout<<"tan = "<<tan(c3)<<std::endl;
  std::cout<<"tanh = "<<tanh(c3)<<std::endl;
  std::cout<<"sqrt = "<<sqrt(c3)<<std::endl;
  
  std::cout<<"exp = "<<exp(c3)<<std::endl;
  std::cout<<"log = "<<log(c3)<<std::endl;
  std::cout<<"log10 = "<<log10(c3)<<std::endl;
 
  std::cout<<"pow(c3,2) = "<<pow(c3,2)<<std::endl;
  std::cout<<"pow(c3,3.2) = "<<pow(c3,3.2)<<std::endl;
  std::cout<<"pow(c3,c3) = "<<pow(c3,c3)<<std::endl;
  std::cout<<"pow(3.2,c3) = "<<pow(3.2,c3)<<std::endl;
 
  std::complex<double> c(1.0,2.0);
  double d = 4.0;
  std::cout<<c3<<"+"<<d<<" = "<<(c3+d)<<std::endl;
  std::cout<<c3<<"-"<<d<<" = "<<(c3-d)<<std::endl;
  std::cout<<c3<<"*"<<d<<" = "<<(c3*d)<<std::endl;
  std::cout<<c3<<"/"<<d<<" = "<<(c3/d)<<std::endl;

  std::cout<<d<<"+"<<c3<<" = "<<(d+c3)<<std::endl;
  std::cout<<d<<"-"<<c3<<" = "<<(d-c3)<<std::endl;
  std::cout<<d<<"*"<<c3<<" = "<<(d*c3)<<std::endl;
  std::cout<<d<<"/"<<c3<<" = "<<(d/c3)<<std::endl;

  std::cout<<c3<<"+"<<c<<" = "<<(c3+c)<<std::endl;
  std::cout<<c3<<"-"<<c<<" = "<<(c3-c)<<std::endl;
  std::cout<<c3<<"*"<<c<<" = "<<(c3*c)<<std::endl;
  std::cout<<c3<<"/"<<c<<" = "<<(c3/c)<<std::endl;
 

  std::cout<<(c3+=d)<<std::endl;
  std::cout<<(c3-=d)<<std::endl;
  std::cout<<(c3*=d)<<std::endl;
  std::cout<<(c3/=d)<<std::endl;

  std::cout<<(c3+=c)<<std::endl;
  std::cout<<(c3-=c)<<std::endl;
  std::cout<<(c3*=c)<<std::endl;
  std::cout<<(c3/=c)<<std::endl;

  std::cout<<"c3 == c : "<<(c3 == c)<<std::endl;
  std::cout<<"c3 != c : "<<(c3 != c)<<std::endl;
  
  return 0;
}
