/*!
 ** \file netcdf_file_io.cpp
 ** \brief netcdf and png files io examples
 ** \version Fluex 1.0
 ** \date 2013/10/31
 ** \author  Denis Arrivault <denis.arrivault_AT_inria.fr>
 */
//stl
#include <iostream>
//slip
#include "GrayscaleImage.hpp"
#include "io_tools.hpp"
#include "NetCDFReader.hpp"
#include "NetCDFWriter.hpp"
#include "PngReader.hpp"
#include "PngWriter.hpp"

/*!
 ** \brief usage :
 */
void usage(const char *name) {
	std::cout << "usage :\n\n";
	std::cout << "\t " << name << " input_nc_file input_nvg_png_file";
	std::cout << std::endl;
}


int main(int argc, char* argv[])
{
	if (argc < 3) {
		usage(*argv);
		return 0;
	}
	std::string test_name(*argv);
	std::string netcdf_filename(argv[1]);
	std::string netcdf_ext;
	std::string netcdf_output_base;
	slip::split_extension(netcdf_filename,netcdf_output_base,netcdf_ext);
	std::string png_filename(argv[2]);
	std::string png_ext;
	std::string png_output_base;
	slip::split_extension(png_filename,png_output_base,png_ext);

	//Read the netcdf file as a grayscale image

	std::vector<std::string> var_names;
	//keep var_names empty if you don't know the name of the variables
	slip::NetCDFReader<slip::GrayscaleImage<double>,double,1,1,2> netcdfread;
	netcdfread.set_data_filename(netcdf_filename);
	try{
		netcdfread.initialize(var_names);
	}catch(std::exception & exc){
		std::cerr << exc.what() << '\n';
	}
	slip::GrayscaleImage<double> netcdf_image;
	try{
		netcdfread.read(netcdf_image);
	}catch(std::exception & exc){
		std::cerr << exc.what() << '\n';
	}

	//Write the image in a png file

	slip::PngWriter<slip::GrayscaleImage<double>,double,1> ImageWriter(netcdf_output_base + "_netcdf_nvg"
			+ png_ext, netcdf_image.width(),netcdf_image.height());
	ImageWriter.write(netcdf_image);

	std::cout << netcdf_output_base + "_netcdf_nvg" + png_ext << " written.\n";

	//Read the png input image

	slip::PngReader<slip::GrayscaleImage<double>,double,1,1> ImageReader(png_filename);
	slip::GrayscaleImage<double> png_image;
	ImageReader.read(png_image);


	//Write the png image in a netcdf file with default variables name

	std::vector<std::size_t> sizes;
	sizes.push_back(png_image.width());
	sizes.push_back(png_image.height());
	slip::NetCDFWriter<slip::GrayscaleImage<double>,double,1,2> netcdfwriter(
			png_output_base + "_default_var" + netcdf_ext,sizes,std::vector<std::string>());
	netcdfwriter.write(png_image);
	std::cout << png_output_base + "_default_var" + netcdf_ext << " written.\n";

	std::cout << test_name << " successful executed.\n";
	return 0;
}


