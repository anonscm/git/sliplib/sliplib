#include <iostream>
#include <vector>
#include <slip/FFT.hpp>
#include <slip/GrayscaleImage.hpp>
#include <slip/Matrix.hpp>
#include <slip/Vector.hpp>
#include <slip/dynamic.hpp>

static void usage(const char * );

double my_abs(std::complex<double> c)
{
  return std::abs(c);
}

int main(int argc, char **argv)
{
  //get the argmuments
  std::vector<std::string> args;
  for(int i = 0; i < argc; i++)
    {
      args.push_back(argv[i]);
    }
  
  if((args.size() == 1) || (args[1] == "-h") || (args[1] == "--help"))
    {
      usage(args[0].c_str());
      exit(0);
    }  
  
  if(args.size() == 7)
    {
      typedef double Real;
      typedef std::complex<Real> Complex;

      slip::GrayscaleImage<Real> I;
      I.read(args[2]);
      
      const std::size_t rows = I.rows();
      const std::size_t cols = I.cols();
 
      slip::Matrix<Complex> IFFT(rows,cols);

      //fft2d
      slip::real_fft2d(I,IFFT);
      
      //log(magnitude)
      slip::GrayscaleImage<Real> Norm(rows,cols);
      for(size_t i = 0; i < rows; ++i)
	{
	  for(size_t j = 0; j < cols; ++j)
	    {
	      Norm[i][j] = std::log(static_cast<Real>(1.0)+ std::abs(IFFT[i][j]));
	    }
	}
     
      //center, change dynamic and save fft image
     slip::fftshift2d(Norm.upper_left(),Norm.bottom_right());
     slip::change_dynamic_01(Norm.begin(),Norm.end(),Norm.begin(),slip::AFFINE_FUNCTION);
     Norm.write(args[4]);

      //ifft2d
     slip::Matrix<std::complex<Real> > IOut(rows,cols);
     slip::ifft2d(IFFT,IOut);

     //save out image
     slip::GrayscaleImage<Real> Out(rows,cols);
     std::transform(IOut.begin(),IOut.end(),Out.begin(),my_abs);
     Out.write(args[6]);

    }
  else
    {
      usage(args[0].c_str());
      exit(1);
    }
  return 0;
}


static void usage(const char* cmd)
{
  std::cout<<std::endl;
  std::cout<<"NAME:"<<std::endl;
  std::cout<<"\t"<<cmd<<" - Computes the log-magnitude of the 2d-fft of an image and its inverse image"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"SYNOPSIS:"<<std::endl;
  std::cout<<"\t"<<cmd<<" -i file -o file"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"DESCRIPTION: Computes the log-magnitude of the 2d-fft of an image and its inverse image:\n"<<std::endl;
  std::cout<<"\t "<<std::endl;
  std::cout<<std::endl;
  std::cout<<"\t -i  input image"<<std::endl;
  std::cout<<"\t -f  fft image"<<std::endl;
  std::cout<<"\t -o  inverse image"<<std::endl;
  std::cout<<"\t -h, --help\t display this help and exit"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"AUTHOR:"<<std::endl;
  std::cout<<"\t Benoit Tremblais <tremblais@sic.univ-poitiers.fr>"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"REPORTING BUGS:"<<std::endl;
  std::cout<<"\t Report bugs to slip trac serveur"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"COPYRIGHT:"<<std::endl;
  std::cout<<"\t This is free software; see the source for copying conditions."<<std::endl;
  std::cout<<std::endl;
  std::cout<<"SEE ALSO:"<<std::endl;
  std::cout<<std::endl;
}
