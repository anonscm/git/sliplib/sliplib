/*!
 ** \file fits_file_io.cpp
 ** \brief fits and av file io examples
 ** \version Fluex 1.0
 ** \date 2013/10/31
 ** \author  Denis Arrivault <denis.arrivault_AT_inria.fr>
 */
//stl
#include <iostream>
//slip
#include "Volume.hpp"
#include "io_tools.hpp"
#include "dynamic.hpp"
#include "FITSReader.hpp"
#include "FITSWriter.hpp"
#include "AvReader.hpp"
#include "AvWriter.hpp"

/*!
 ** \brief usage :
 */
void usage(const char *name) {
	std::cout << "usage :\n\n";
	std::cout << "\t " << name << " input_nvg_video_fits_file input_nvg_av_file";
	std::cout << std::endl;
}


int main(int argc, char* argv[])
{
	if (argc < 3) {
		usage(*argv);
		return 0;
	}
	std::string test_name(*argv);
	std::string fits_filename(argv[1]);
	std::string fits_ext;
	std::string fits_output_base;
	slip::split_extension(fits_filename,fits_output_base,fits_ext);
	std::string av_filename(argv[2]);
	std::string av_ext;
	std::string av_output_base;
	slip::split_extension(av_filename,av_output_base,av_ext);

	//Read the fits file as a grayscale video

	slip::FITSReader<slip::Volume<double>,double,1,1,3> fitsread;
	fitsread.set_data_filename(fits_filename);
	try{
		fitsread.initialize();
	}catch(std::exception & exc){
		std::cerr << exc.what() << '\n';
	}
	slip::Volume<double> fits_video;
	try{
		fitsread.read(fits_video);
	}catch(std::exception & exc){
		std::cerr << exc.what() << '\n';
	}

	//Write the video in a mp4 file

	//Change the dynamic to have video data between 0 and 255
	slip::range_fun_interab<double,double> funf(fits_video.min(),fits_video.max(),0,255);
	slip::change_dynamic(fits_video.begin(),fits_video.end(),fits_video.begin(),funf);
	//Initialize the writer with a frame per second time base and a desired bit rate
	AVRational t_base;
	t_base.num = 1;
	t_base.den = 25;
	int64_t bit_rate = 40000;
	int nbframes = fits_video.slices();
	int height = fits_video.rows();
	int width = fits_video.cols();
	slip::AvWriter<slip::Volume<double>,double,1> videowriter(fits_output_base + "_fits.mp4",	width,height,nbframes,bit_rate,t_base,0,CODEC_ID_MPEG4);
	//Write the video
	videowriter.write(fits_video);

	std::cout << fits_output_base + "_fits.mp4" << " written.\n";

	//Read the png input video

	slip::AvReader<slip::Volume<double>,double,1,1> videoreader(av_filename);
	slip::Volume<double> av_video;
	videoreader.read(av_video);


	//Write the png video in a fits file

	std::vector<std::size_t> sizes;
	sizes.push_back(av_video.dim1());
	sizes.push_back(av_video.dim2());
	sizes.push_back(av_video.dim3());
	slip::FITSWriter<slip::Volume<double>,double,1,3> fitswriter(	av_output_base + fits_ext,sizes);
	fitswriter.write(av_video);
	std::cout << av_output_base + fits_ext << " written.\n";

	std::cout << test_name << " successful executed.\n";
	return 0;
}


