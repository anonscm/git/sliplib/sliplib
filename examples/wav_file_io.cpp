/*!
 ** \file wav_file_io.cpp
 ** \brief wav files io examples
 ** \version Fluex 1.0
 ** \date 2013/10/30
 ** \author  Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

//stl
#include <iostream>
//slip
#include "Vector.hpp"
#include "io_tools.hpp"
#include "WavReader.hpp"
#include "WavWriter.hpp"


/*!
 ** \brief usage :
 */
void usage(const char *name) {
	std::cout << "usage :\n\n";
	std::cout << "\t " << name << " input_wav_signal ";
	std::cout << std::endl;
}


int main(int argc, char* argv[])
{
	if (argc < 2) {
		usage(*argv);
		return 0;
	}
	std::string test_name(*argv);
	std::string filename(argv[1]);
	std::string ext;
	std::string output_base;
	slip::split_extension(filename,output_base,ext);

	//Read the audio signal
	slip::WavReader<slip::Vector<double>,double,1> reader(filename);
	slip::Vector<double> audio;
	reader.read(audio);
	WAVE_HEADER head = reader.get_header();

	//Duplicate the signal four times
	std::vector<slip::Vector<double> > audio_list(4,audio);

	//Write all the four audio signals in one output file
	std::size_t list_size = audio_list.size();
	std::size_t size = audio_list.begin()->size()  * list_size;
	slip::WavWriter<slip::Vector<double>,double> writer(output_base + "_duplicate4times" + ext,head,size);
	std::vector<slip::Vector<double> >::const_iterator it_l = audio_list.begin();
	while(writer.write(*it_l)){it_l++;};

	std::cout << output_base + "_duplicate4times" + ext << " written.\n";
	std::cout << test_name << " successful executed.\n";
	return 0;
}


