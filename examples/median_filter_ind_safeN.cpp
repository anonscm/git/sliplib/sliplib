#include <iostream>
#include <vector>
#include <slip/border_treatment.hpp>
#include <slip/neighborhood.hpp>
#include <slip/statistics.hpp>
#include <slip/GrayscaleImage.hpp>
#include <slip/dynamic.hpp>
#include <boost/iterator/indirect_iterator.hpp>
static void usage(const char * );
/*!
 * \brief Computes the median filter using iterators and neighborhood functors.
 * \param in_first iterator to the begining of the input image.
 * \param in_last iterator one-past-the-end of the input image.
 * \param out_first iterator to the begining of the output image.
 * \param out_last iterator one-past-the-end of the output image.
 * \param N Neighborhood functor
 */
template<typename Iterator1,
	   typename Iterator2,
	   typename NeighborFunc>
  void median_filter_indirect(Iterator1 in_first, Iterator1 in_last, 
			      Iterator2 out_first, Iterator2 out_last,
			      NeighborFunc N)
  {
    typedef typename std::iterator_traits<Iterator1>::value_type value_type;

    std::vector<Iterator1> neighbors(26);
    for(; in_first != in_last; ++in_first, ++out_first)
      {
	//get neighborhoods of the current pixel: in_first
	N(in_first,neighbors);
	//add current pixel
	neighbors.push_back(in_first);
	//definition of indirect_iterator to the neighbors
	boost::indirect_iterator<typename std::vector<Iterator1>::iterator,
	                         typename std::vector<value_type>::value_type>
	  indirect_first(neighbors.begin()), 
	  indirect_last(neighbors.end());
	  assert(indirect_first != indirect_last);

	  //get the median 
	  *out_first = slip::median(indirect_first,indirect_last);
      }
  }




int main(int argc, char **argv)
{

  //get the argmuments
  std::vector<std::string> args;
  for(int i = 0; i < argc; i++)
    {
      args.push_back(argv[i]);
    }

 
  if((args.size() == 1) || (args[1] == "-h") || (args[1] == "--help"))
    {
      usage(args[0].c_str());
      exit(0);
    }  

  if(args.size() == 5)
    {
      typedef double T;
      //---------------------------
      // Read the input image
      //---------------------------
      slip::GrayscaleImage<T> I;
      I.read(args[2]);
      
      const std::size_t rows = I.rows();
      const std::size_t cols = I.cols();
      //define the safe 8-connexity neighborhood functor
      //associated with I
      slip::SafeN8C<slip::GrayscaleImage<T> > Neigh(I);

      //----------------------------
      // Computes the median filter
      //----------------------------
      slip::GrayscaleImage<T> Median(rows,cols,0.0);
      median_filter_indirect(I.upper_left(),I.bottom_right(),
      			   Median.upper_left(),Median.bottom_right(),
      			   Neigh);
      Median.write(args[4]);
      
    }
  else
    {
      usage(args[0].c_str());
      exit(1);
    }
  return 0;
}


static void usage(const char* cmd)
{
  std::cout<<std::endl;
  std::cout<<"NAME:"<<std::endl;
  std::cout<<"\t"<<cmd<<" - Computes the median filter of size 3x3 of a grayscale image"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"SYNOPSIS:"<<std::endl;
  std::cout<<"\t"<<cmd<<" -i file -o file"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"DESCRIPTION: Computes the median filter of size (2*N+1)x(2*N+1) of a grayscale image:\n"<<std::endl;
  std::cout<<"\t "<<std::endl;
  std::cout<<std::endl;
  std::cout<<"\t -i  input image"<<std::endl;
  std::cout<<"\t -o  output image"<<std::endl;
  std::cout<<"\t -h, --help\t display this help and exit"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"AUTHOR:"<<std::endl;
  std::cout<<"\t Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"REPORTING BUGS:"<<std::endl;
  std::cout<<"\t Report bugs to slip trac serveur"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"COPYRIGHT:"<<std::endl;
  std::cout<<"\t This is free software; see the source for copying conditions."<<std::endl;
  std::cout<<std::endl;
  std::cout<<"SEE ALSO:"<<std::endl;
  std::cout<<std::endl;
}
