#include <iostream>
#include <slip/Vector.hpp>
#include <slip/Matrix.hpp>
#include <slip/linear_algebra.hpp>

#define N 4

template<typename T>
void myrand(T& a)
{
  a = T((double)std::rand()/((double)RAND_MAX+1)*N);
}

int main()
{
  typedef double T;
  //init A with random values
  slip::Matrix<T> A(N,N);
  std::for_each(A.begin(),A.end(),myrand<T>); 
  //init B with random values
  slip::Vector<T> B(N);
  std::for_each(B.begin(),B.end(),myrand<T>); 
  //solve the linear system using lu algorithm
  slip::Vector<T> X(N);
  slip::lu_solve(A,X,B);
  std::cout<<"A = \n"<<A<<std::endl;
  std::cout<<"B = \n"<<B<<std::endl;
  std::cout<<"X = \n"<<X<<std::endl;
  slip::Vector<T> B2(N);
  std::cout<<"check result "<<std::endl;
  slip::matrix_vector_multiplies(A,X,B2);
  std::cout<<"AX = \n"<<B2<<std::endl;
  
  

 return 0;
}
