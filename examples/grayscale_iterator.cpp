#include <iostream>
#include <algorithm>
#include <slip/GrayscaleImage.hpp>
#include <slip/dynamic.hpp>

struct divide_by_2
{
  float operator()(float val)
  {
    return float(val / 2);
  }
};

int main()
{ 
  slip::GrayscaleImage<float> I1;
  I1.read("lena.gif");
  const std::size_t height = I1.height();
  const std::size_t width  = I1.width();
  
  //apply the function divide_by_2 from I1.begin() to I1.end() 
  //and put the result from Result.begin() to Result.begin()+I1.size()
  slip::GrayscaleImage<float> Result(height,width);
  std::transform(I1.begin(),I1.end(),Result.begin(),divide_by_2());
  Result.write("lena_d2.gif");
  //in the reverse order
  //apply the function divide_by_2 from (I1.end()-1) to I1.begin() 
  //and put the result from Result2.begin() to Result2.begin()+I1.size()
  slip::GrayscaleImage<float> Result2(height,width);
  std::transform(I1.rbegin(),I1.rend(),Result2.begin(),divide_by_2());
  Result2.write("lena_inv.gif");

  return 0;
}
