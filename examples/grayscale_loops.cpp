#include <iostream>
#include <slip/GrayscaleImage.hpp>
#include <slip/dynamic.hpp>

int main()
{ 
  //construct an unsigned char GrayscaleImage of 
  // heightxwidth = 256x256 which all elements are 
  //initialized to 128
  slip::GrayscaleImage<unsigned char> I(256,256,128);
 //get image dimensions
 const std::size_t height = I.height();
 const std::size_t width  = I.width();
 //construct a grayscale image with same dimensions as I
 slip::GrayscaleImage<float> Result(height,width);
 //double loop to iterate values 
  for(std::size_t i = 0; i < height; ++i)
    {
      for(std::size_t j = 0; j < width; ++j)
	{
	  //element access using double brackets
	  Result[i][j] = float(I[i][j] / 2);
	  // or using parenthesis
	  Result(i,j)  = float(I(i,j) * 2);
	}
    }

  return 0;
}
