#include <iostream>
#include <vector>
#include <slip/convolution.hpp>
#include <slip/Block.hpp>
#include <slip/border_treatment.hpp>
#include <slip/GrayscaleImage.hpp>
#include <slip/dynamic.hpp>

//#include <ctime>

static void usage(const char * );


int main(int argc, char **argv)
{

  //get the argmuments
  std::vector<std::string> args;
  for(int i = 0; i < argc; i++)
    {
      args.push_back(argv[i]);
    }

  if((args.size() == 1) || (args[1] == "-h") || (args[1] == "--help"))
    {
      usage(args[0].c_str());
      exit(0);
    }  

  if(args.size() == 5)
    {
      //---------------------------
      //edges detection by a Sobel filtering
      //---------------------------
      //create convolution kernels
      const slip::block<float,3> derivative_kernel = {1.0f,0.0f,-1.0f};
      const slip::block<float,3> smooth_kernel     = {1.0f,2.0f,1.0f};
      
      //---------------------------
      //read input image
      //---------------------------
      slip::GrayscaleImage<float> I;
      I.read(args[2]);
      const std::size_t rows = I.rows();
      const std::size_t cols = I.cols();
      
     
      //---------------------------
      //x gradient computation
      //---------------------------
      //create temporary image same size as I
      slip::GrayscaleImage<float> I2(rows,cols);
      
      for(size_t i = 0; i < rows; ++i)
	{
	  //convolution of the row i with derivative kernel
	  //with NEUMANN border conditions
	  //result is put in row i of I2
	  slip::same_convolution(I.row_begin(i),I.row_end(i),
				 derivative_kernel.begin(),
				 derivative_kernel.end(),
				 I2.row_begin(i),
				 slip::BORDER_TREATMENT_NEUMANN);
      
	}
      //create x gradient image    
      slip::GrayscaleImage<float> Gx(rows,cols);
      for(size_t j = 0; j < cols; ++j)
	{
	  //convolution of the column j with smooth kernel
	  //with NEUMANN border conditions
	  //result is put in column i of I2
	  slip::same_convolution(I2.col_begin(j),I2.col_end(j),
				 smooth_kernel.begin(),smooth_kernel.end(),
				 Gx.col_begin(j),
				 slip::BORDER_TREATMENT_NEUMANN);
	}
      
      //---------------------------
      //y gradient computation
      //---------------------------
      for(size_t i = 0; i < rows; ++i)
	{
	  slip::same_convolution(I.row_begin(i),I.row_end(i),
				 smooth_kernel.begin(),smooth_kernel.end(),
				 I2.row_begin(i),
				 slip::BORDER_TREATMENT_NEUMANN);
	  
	}
  
      //create y gradient image
      slip::GrayscaleImage<float> Gy(rows,cols);
      for(size_t j = 0; j < cols; ++j)
	{
	  slip::same_convolution(I2.col_begin(j),I2.col_end(j),
				 derivative_kernel.begin(),derivative_kernel.end(),
				 Gy.col_begin(j),
				 slip::BORDER_TREATMENT_NEUMANN);
	}
      
      //---------------------------
      //compute the gradient norm
      //---------------------------
      //create L1 gradient norm image
      slip::GrayscaleImage<float> G(rows,cols);
      for(size_t i = 0; i < rows; ++i)
	{
	  for(size_t j = 0; j < cols; ++j)
	    {
	      G[i][j] = std::abs(Gx[i][j]) + std::abs(Gy[i][j]); 
	    }
	}

      //---------------------------
      //change dynamic to put values of G in the range [0.0,1.0]
      //---------------------------
      slip::change_dynamic_01(G.begin(),G.end(),G.begin(),slip::AFFINE_FUNCTION);
      
      //write the gradient norm image
      G.write(args[4]);
    }
  else
    {
      usage(args[0].c_str());
      exit(1);
    }
  return 0;
}


static void usage(const char* cmd)
{
  std::cout<<std::endl;
  std::cout<<"NAME:"<<std::endl;
  std::cout<<"\t"<<cmd<<" - Computes the gradient norm using the Sobel masks"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"SYNOPSIS:"<<std::endl;
  std::cout<<"\t"<<cmd<<" -i file -o file"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"DESCRIPTION: Computes the gradient norm using the Sobel masks :\n"<<std::endl;
  std::cout<<"\thx = [-1 0 1; and hy = [-1 -2 -1;  "<<std::endl;
  std::cout<<"\t      -2 0 2;            0  0  0;"<<std::endl;
  std::cout<<"\t      -1 0 1]            1  2  1]"<<std::endl;
  std::cout<<"\t "<<std::endl;
  std::cout<<std::endl;
  std::cout<<"\t -i  input image"<<std::endl;
  std::cout<<"\t -o  output image"<<std::endl;
  std::cout<<"\t -h, --help\t display this help and exit"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"AUTHOR:"<<std::endl;
  std::cout<<"\t Benoit Tremblais <tremblais@sic.univ-poitiers.fr>"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"REPORTING BUGS:"<<std::endl;
  std::cout<<"\t Report bugs to slip trac serveur"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"COPYRIGHT:"<<std::endl;
  std::cout<<"\t This is free software; see the source for copying conditions."<<std::endl;
  std::cout<<std::endl;
  std::cout<<"SEE ALSO:"<<std::endl;
  std::cout<<std::endl;
}
