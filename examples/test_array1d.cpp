#include "Array.hpp"
#include <iostream>

int main()
{
  double d[] = {1.2,3.4,5.5,6.6,7.7};
  slip::Array<double> V(5,3.4);

  std::cout<<V<<std::endl;
 
  
  slip::Array<double> V2(5,d);
  std::cout<<V2<<std::endl;
 
  slip::Array<double> V3 = V2;
  std::cout<<V3<<std::endl;
 
  
  slip::Array<double> V4;
  V = V;
  std::cout<<V<<std::endl;

  for(size_t i = 0; i < V.size(); ++i)
    {
      //std::cout<<V[i]<<" ";
      V[i] = 4.4;
    }
  std::cout<<V<<std::endl;

  slip::Array<double> V5(5,d+5);
  std::cout<<V5<<std::endl;

  slip::Array<float> V6(6);
  V6 = 6.0;
    std::cout<<V6<<std::endl;
    
  return 0;
}
