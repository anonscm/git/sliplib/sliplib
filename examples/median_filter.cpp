#include <iostream>
#include <vector>
#include <slip/border_treatment.hpp>
#include <slip/statistics.hpp>
#include <slip/GrayscaleImage.hpp>
#include <slip/Box2d.hpp>

static void usage(const char * );


int main(int argc, char **argv)
{

  //get the argmuments
  std::vector<std::string> args;
  for(int i = 0; i < argc; i++)
    {
      args.push_back(argv[i]);
    }

 
  if((args.size() == 1) || (args[1] == "-h") || (args[1] == "--help"))
    {
      usage(args[0].c_str());
      exit(0);
    }  

  if(args.size() == 7)
    {
      typedef double T;
      
      slip::GrayscaleImage<T> I;
      I.read(args[2]);
      
      const std::size_t rows = I.rows();
      const std::size_t cols = I.cols();
      
          
      slip::GrayscaleImage<T> Median(rows,cols,0.0);
      //size of the median filter (2*N+1)x(2*N+1)
      std::size_t N = static_cast<std::size_t>(atoi(argv[4]));
     
      //enlarge border of I in I2
      const std::size_t b_size = N;
      slip::GrayscaleImage<T> I2;
      slip::add_border2d(I,slip::BORDER_TREATMENT_COPY,I2,b_size);
      
      const std::size_t _2_N = 2 * N;
      slip::Box2d<int> box(0,0,_2_N,_2_N);
      for(std::size_t i = 0 ; i < rows; ++i)
      	 {
      	   for(std::size_t j = 0; j < cols; ++j)
      	     {
      	       //update box coordinate
      	       box.set_coord(i,j,i+_2_N,j+_2_N);
      	       //compute median of the box
      	       Median[i][j] = slip::median(I2.upper_left(box),I2.bottom_right(box));
      	     }
      	 }

     
       
       Median.write(args[6]);
      
    }
  else
    {
      usage(args[0].c_str());
      exit(1);
    }
  return 0;
}


static void usage(const char* cmd)
{
  std::cout<<std::endl;
  std::cout<<"NAME:"<<std::endl;
  std::cout<<"\t"<<cmd<<" - Computes the median filter of size NxN of a grayscale image"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"SYNOPSIS:"<<std::endl;
  std::cout<<"\t"<<cmd<<" -i file -n N -o file"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"DESCRIPTION: Computes the median filter of size (2*N+1)x(2*N+1) of a grayscale image:\n"<<std::endl;
  std::cout<<"\t "<<std::endl;
  std::cout<<std::endl;
  std::cout<<"\t -i  input image"<<std::endl;
  std::cout<<"\t -n  N size of the NxN median mask"<<std::endl;
  std::cout<<"\t -o  output image"<<std::endl;
  std::cout<<"\t -h, --help\t display this help and exit"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"AUTHOR:"<<std::endl;
  std::cout<<"\t Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"REPORTING BUGS:"<<std::endl;
  std::cout<<"\t Report bugs to slip trac serveur"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"COPYRIGHT:"<<std::endl;
  std::cout<<"\t This is free software; see the source for copying conditions."<<std::endl;
  std::cout<<std::endl;
  std::cout<<"SEE ALSO:"<<std::endl;
  std::cout<<std::endl;
}
