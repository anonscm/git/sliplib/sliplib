#include<algorithm>
#include<iostream>
#include<iterator>
#include <numeric>
#include <complex>
#include <slip/Array3d.hpp>

#include <slip/Point3d.hpp>
#include <slip/DPoint3d.hpp>
#include <slip/DPoint2d.hpp>
#include <slip/Box3d.hpp>
#include <slip/iterator3d_box.hpp>

int main()
{

  slip::Array3d<double> M(5,4,3);
  std::generate(M.begin(),M.end(),std::rand);
  std::cout<<M<<std::endl;
  
  // slip::Point3d<int> db(0,2,2);
  slip::DPoint3d<int> de(2,1,1);
  slip::Box3d<int> box(1,1,1,3,2,2);
  std::cout<<box<<std::endl;
  


  slip::iterator3d_box<slip::Array3d<double> > beg(&M,box);
  slip::iterator3d_box<slip::Array3d<double> > e = (beg + de);
  e++;
  std::cout<<beg[de]<<std::endl<<std::endl;
  slip::iterator3d_box<slip::Array3d<double> > it;

  
  
   for(it = beg; it != e; ++it)
     {
       std::cout<<*it<<" ";
     }
   std::cout<<std::endl;
   std::cout<<std::endl;
 
 
  

   slip::iterator3d_box<slip::Array3d<double> > it2;
  
   for(it2 = M.upper_left(); it2 != M.bottom_right(); ++it2)
     {
       std::cout<<*it2<<" ";
     }
   std::cout<<std::endl;
   std::cout<<std::endl;

 slip::iterator3d_box<slip::Array3d<double> > it3;
 slip::Box3d<int> box3(1,1,1,2,3,2);
  std::cout<<box3<<std::endl;
  std::cout<<*M.bottom_right(box3)<<std::endl;
  for(it3 = M.upper_left(box3); it3 != M.bottom_right(box3); ++it3)
    {
      std::cout<<*it3<<" ";
    }
 std::cout<<std::endl;
  
 double d[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0};
 slip::Array3d<double> Md(5,2,2,d);
 std::cout<<Md<<std::endl;
 std::cout<<std::endl;
 std::cout<<std::endl;
 
 double masque[] = {2.0, 2.0, 2.0, 2.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0};
 slip::Array3d<double> Mm(3,2,2,masque);
 std::cout<<Mm<<std::endl;
 std::cout<<std::endl;
 std::cout<<std::endl;

  slip::Box3d<int> box4(1,0,0,3,1,1);
 std::cout<<std::inner_product(Md.upper_left(box4),Md.bottom_right(box4),Mm.upper_left(),0.0)<<std::endl;
 std::cout<<std::endl;
 std::cout<<std::endl;

 slip::Array3d<double> Mm2(3,2,2);
 std::copy(Md.upper_left(box4),Md.bottom_right(box4),Mm2.upper_left());
 std::cout<<Mm2<<std::endl;
 std::cout<<std::endl;
 std::cout<<std::endl;

  slip::Array3d<std::complex<double> > Mcomp(3,5,3);
 slip::iterator3d_box<slip::Array3d<std::complex<double> > > itc = Mcomp.upper_left();
 for(; itc != Mcomp.bottom_right(); ++itc)
    {
      *itc = *itc + 2.0;
      std::cout<<*itc<<" ";
    }
  std::cout<<std::endl;
  std::cout<<std::endl;
  itc =  Mcomp.upper_left();
  double val = 0.0;
  for(; itc != Mcomp.bottom_right(); ++itc)
    {
      itc->imag() = val;
      val += 1.0;
      // std::cout<<" ("<<itc->real()<<","<<itc->imag()<<"), ";
    } 
  //  std::cout<<std::endl;
  // std::cout<<std::endl;

  itc =  Mcomp.upper_left();
  for(std::size_t k = 0; k < Mcomp.dim1(); ++k)
    {
      std::cout<<std::endl << "Slice " << k << std::endl << std::endl;
      for(std::size_t i = 0; i < Mcomp.dim2(); ++i)
	{
	  slip::DPoint2d<int> d(k,i);
	  std::copy(itc[d],itc[d] + Mcomp.dim3(),std::ostream_iterator<std::complex<double> >(std::cout," "));
	  std::cout<< std::endl;
	}
    }
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout<<"Mcomp = "<<std::endl;
  std::cout<<Mcomp<<std::endl;
  
  slip::Box3d<int> box5(1,1,0,2,2,2);
  std::cout<<"box 5 "<<box5<<std::endl;
  slip::iterator3d_box<slip::Array3d<std::complex<double> > > itc3 =  Mcomp.upper_left(box5);
  std::cout<<"rows of the box "<<std::endl; 
  for(std::size_t k = 0; k < box5.depth(); ++k)
    { 
      std::cout<<std::endl << "Slice " << k << std::endl << std::endl;
      for(std::size_t i = 0; i < (std::size_t)box5.height();++i)
	{
	  // std::copy(itc3.row_begin(k,i),itc3.row_begin(k,i)+box5.width(),std::ostream_iterator<std::complex<double> >(std::cout," "));
	  std::copy(itc3.row_begin(k,i),itc3.row_end(k,i),std::ostream_iterator<std::complex<double> >(std::cout," "));
	  std::cout<<std::endl;
	}
    }
   std::cout<<std::endl;
   std::cout<<"cols of the box "<<std::endl; 
   for(std::size_t k = 0; k < box5.depth(); ++k)
    { 
      std::cout<<std::endl << "Slice " << k << std::endl << std::endl;
      for(std::size_t j = 0; j < (std::size_t)box5.height();++j)
	{
	  //   std::copy(itc3.col_begin(k,j),itc3.col_begin(k,j)+box5.height(),std::ostream_iterator<std::complex<double> >(std::cout," "));
	  std::copy(itc3.col_begin(k,j),itc3.col_end(k,j),std::ostream_iterator<std::complex<double> >(std::cout," "));
	  std::cout<<std::endl;
	}
    }
   std::cout<<std::endl;

   std::cout<<"slices of the box "<<std::endl; 
   for(std::size_t k = 0;  k < box5.depth(); ++k)
     {
       std::copy(itc3.begin(k),itc3.end(k),std::ostream_iterator<std::complex<double> >(std::cout," "));
       std::cout<<std::endl;
     }

 return 0;
}
