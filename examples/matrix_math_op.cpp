#include <cmath>
#include <iostream>
#include <slip/Matrix.hpp>

int main()
{

slip::Matrix<double> M4(4,4);
slip::iota(M4.begin(),M4.end(),1.0,1.0);
std::cout<<"M4.min() = "<<M4.min()<<std::endl;
std::cout<<"M4.max() = "<<M4.max()<<std::endl;
std::cout<<"M4.sum() = "<<M4.sum()<<std::endl;
std::cout<<"M4.trace() = "<<M4.trace()<<std::endl;
std::cout<<"M4.det() = "<<M4.det()<<std::endl;
std::cout<<"M4.cond() = "<<M4.cond()<<std::endl;
std::cout<<"M4.rank() = "<<M4.rank()<<std::endl;
std::cout<<"M4.inv() = \n"<<M4.inv()<<std::endl;
std::cout<<"M4.L1_norm() = "<<M4.L1_norm()<<std::endl;
std::cout<<"M4.L2_norm() = "<<M4.L2_norm()<<std::endl;
std::cout<<"M4.infinite_norm() = "<<M4.infinite_norm()<<std::endl;
std::cout<<"M4.frobenius_norm() = "<<M4.frobenius_norm()<<std::endl;
std::cout<<"min(M4) = "<<min(M4)<<std::endl;
std::cout<<"max(M4) = "<<max(M4)<<std::endl;
M4.apply(std::sqrt);
std::cout<<"M4.apply(std::sqrt):\n"<<M4;
std::cout<<"abs(-M4) = \n"<<abs(-M4)<<std::endl;
std::cout<<"sqrt(M4) = \n"<<sqrt(M4)<<std::endl;
std::cout<<"cos(M4) = \n"<<cos(M4)<<std::endl;
std::cout<<"sin(M4) = \n"<<sin(M4)<<std::endl;
std::cout<<"tan(M4) = \n"<<tan(M4)<<std::endl;
std::cout<<"atan(M4) = \n"<<atan(M4)<<std::endl;
std::cout<<"exp(M4) = \n"<<exp(M4)<<std::endl;
std::cout<<"log(M4) = \n"<<log(M4)<<std::endl;
std::cout<<"cosh(M4) = \n"<<cosh(M4)<<std::endl;
std::cout<<"sinh(M4) = \n"<<sinh(M4)<<std::endl;
std::cout<<"tanh(M4) = \n"<<tanh(M4)<<std::endl;
std::cout<<"log10(M4) = \n"<<log10(M4)<<std::endl;


}
