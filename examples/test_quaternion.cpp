#include <iostream>
#include <complex>
#include <boost/math/quaternion.hpp>

int main()
{

  //constructions
  boost::math::quaternion<float>        q0;

  std::cout<<q0<<std::endl;

  //array of quaternions
  boost::math::quaternion<float>        qa[4];
  for(int i = 0; i < 4; ++i)
    std::cout<<qa[i]<<" ";
  std::cout<<std::endl;

  // using constructor "H seen as R^4"
  boost::math::quaternion<double>       q1(1,2,3,4);
  std::cout<<q1<<std::endl;
   
  std::complex<float>                   c0(5,6);
    
  // using constructor "H seen as C^2"
  boost::math::quaternion<float>        q2(c0);
  std::cout<<q2<<std::endl;
 
  std::complex<float>                   c00(7,8);
  boost::math::quaternion<float>        q22(c0,c00);
  std::cout<<q22<<std::endl;
  // using UNtemplated copy constructor
  boost::math::quaternion<float>        q3(q2);
  std::cout<<q3<<std::endl;

  // using templated copy constructor
  boost::math::quaternion<long double>  q4(q3);
  std::cout<<q4<<std::endl;

  // using UNtemplated assignment operator
  q3 = q0;
  std::cout<<q3<<std::endl;
  qa[0] = q0;
  for(int i = 0; i < 4; ++i)
    std::cout<<qa[i]<<" ";
  std::cout<<std::endl;

  // using templated assignment operator
  q4 = q0;
  std::cout<<q4<<std::endl;
  qa[1] = q1;
  for(int i = 0; i < 4; ++i)
    std::cout<<qa[i]<<" ";
  std::cout<<std::endl;  
  float f0(7);
  
  // using converting assignment operator
  q2 = f0;
  std::cout<<q2<<std::endl;

  // using converting assignment operator
  q3 = c0;
  std::cout<<q3<<std::endl;

  // using += (const T &)
  q2 += f0;
  std::cout<<q2<<std::endl;

  // using += (const ::std::complex<T> &)
  q2 += c0;
  std::cout<<q2<<std::endl;

  // using += (const quaternion<X> &)
  q2 += q3;
  std::cout<<q2<<std::endl;

  // using -= (const T &)
  q3 -= f0;
   std::cout<<q3<<std::endl;

  // using -= (const ::std::complex<T> &)
  q3 -= c0;
  std::cout<<q3<<std::endl;

  // using -= (const quaternion<X> &)
  q3 -= q2;
  std::cout<<q3<<std::endl;

   double                                  d0(8);
   std::complex<double>                  c1(9,10);
    
    // using *= (const T &)
    q1 *= d0;
    std::cout<<q1<<std::endl;
    // using *= (const ::std::complex<T> &)
    q1 *= c1;
    std::cout<<q1<<std::endl;
    // using *= (const quaternion<X> &)
    q1 *= q1;
    std::cout<<q1<<std::endl;
    long double                             l0(11);
    std::complex<long double>             c2(12,13);
    
    // using /= (const T &)
    q4 /= l0;
    std::cout<<q4<<std::endl;

    // using /= (const ::std::complex<T> &)
    q4 /= c2;
    std::cout<<q4<<std::endl;

    // using /= (const quaternion<X> &)
    q4 /= q1;
    std::cout<<q4<<std::endl;

    // using + (const T &, const quaternion<T> &)
    boost::math::quaternion<float>        q5 = f0+q2;
    std::cout<<q5<<std::endl;

    // using + (const quaternion<T> &, const T &)
    boost::math::quaternion<float>        q6 = q2+f0;
    std::cout<<q6<<std::endl;

    // using + (const ::std::complex<T> &, const quaternion<T> &)
    boost::math::quaternion<float>        q7 = c0+q2;
    std::cout<<q7<<std::endl;

    // using + (const quaternion<T> &, const ::std::complex<T> &)
    boost::math::quaternion<float>        q8 = q2+c0;
    std::cout<<q8<<std::endl;
    
    // using + (const quaternion<T> &,const quaternion<T> &)
    boost::math::quaternion<float>        q9 = q2+q3;
    std::cout<<q9<<std::endl;

    // using - (const T &, const quaternion<T> &)
    q5 = f0-q2;
    std::cout<<q5<<std::endl;

    // using - (const quaternion<T> &, const T &)
    q6 = q2-f0;
    std::cout<<q6<<std::endl;

    // using - (const ::std::complex<T> &, const quaternion<T> &)
    q7 = c0-q2;
    std::cout<<q7<<std::endl;

    // using - (const quaternion<T> &, const ::std::complex<T> &)
    q8 = q2-c0;
    std::cout<<q8<<std::endl;

    // using - (const quaternion<T> &,const quaternion<T> &)
    q9 = q2-q3;
    std::cout<<q9<<std::endl;
    
    // using * (const T &, const quaternion<T> &)
    q5 = f0*q2;
    std::cout<<q5<<std::endl;
    
    // using * (const quaternion<T> &, const T &)
    q6 = q2*f0;
    std::cout<<q6<<std::endl;

    // using * (const ::std::complex<T> &, const quaternion<T> &)
    q7 = c0*q2;
    std::cout<<q7<<std::endl;

    // using * (const quaternion<T> &, const ::std::complex<T> &)
    q8 = q2*c0;
    std::cout<<q8<<std::endl;

    // using * (const quaternion<T> &,const quaternion<T> &)
    q9 = q2*q3;
    std::cout<<q9<<std::endl;

    // using / (const T &, const quaternion<T> &)
    q5 = f0/q2;
    std::cout<<q5<<std::endl;

    // using / (const quaternion<T> &, const T &)
    q6 = q2/f0;
    std::cout<<q6<<std::endl;

    // using / (const ::std::complex<T> &, const quaternion<T> &)
    q7 = c0/q2;
    std::cout<<q7<<std::endl;

    // using / (const quaternion<T> &, const ::std::complex<T> &)
    q8 = q2/c0;
    std::cout<<q8<<std::endl;

    // using / (const quaternion<T> &,const quaternion<T> &)
    q9 = q2/q3;
    std::cout<<q9<<std::endl;

    // using + (const quaternion<T> &)
    q2 = +q0;
    std::cout<<q2<<std::endl;

    // using - (const quaternion<T> &)
    q2 = -q3;
    std::cout<<q2<<std::endl;

    // using == (const T &, const quaternion<T> &)
    f0 == q2;


    // using == (const quaternion<T> &, const T &)
    q2 == f0;

    
    // using == (const ::std::complex<T> &, const quaternion<T> &)
    c0 == q2;


    // using == (const quaternion<T> &, const ::std::complex<T> &)
    q2 == c0;


    // using == (const quaternion<T> &,const quaternion<T> &)
    q2 == q3;
    

    // using != (const T &, const quaternion<T> &)
    f0 != q2;
    
    // using != (const quaternion<T> &, const T &)
    q2 != f0;
    
    // using != (const ::std::complex<T> &, const quaternion<T> &)
    c0 != q2;
    
    // using != (const quaternion<T> &, const ::std::complex<T> &)
    q2 != c0;
    
    // using != (const quaternion<T> &,const quaternion<T> &)
    q2 != q3;
    q0 = boost::math::quaternion<float>(1,2,3,4); 
    std::cout<<q0<<std::endl;
    std::cout<<q0.R_component_1()<<" "<<q0.R_component_2()<<" "<<q0.R_component_3()<<" "<<q0.R_component_4()<<std::endl;
    std::cout<<q0.C_component_1()<<" "<<q0.C_component_2()<<std::endl;
    
    std::cout<<"the value of the real part is "<< real(q0)<<std::endl;
    std::cout<<"the value of the real part is "<< q0.real()<<std::endl;
    std::cout<<"the value of the unreal part is "<< unreal(q0)<<std::endl;
    std::cout<<"the value of the unreal part is "<< q0.unreal()<<std::endl;
     
    std::cout<<"the value of the sup norm is "<< sup(q0)<<std::endl;
    std::cout<<"the value of the l1 norm is "<< l1(q0)<<std::endl;
    std::cout<<"the value of the magnitude is "<< abs(q0)<<std::endl;
    std::cout<<"the value of the Cayley norm is "<< norm(q0)<<std::endl;
    std::cout<<"the value of the conjugate is "<< conj(q0)<<std::endl;
    std::cout<<"the value of the exponential is "<< exp(q0)<<std::endl;
    std::cout<<"the value of the cube is "<< pow(q0,3)<<std::endl;
    std::cout<<"the value of the cosinus is "<< cos(q0)<<std::endl;
    std::cout<<"the value of the sinus is "<< sin(q0)<<std::endl;
    std::cout<<"the value of the tangent is "<< tan(q0)<<std::endl;
    std::cout<<"the value of the hyperbolic cosinus is "<< cosh(q0)<<std::endl;
    std::cout<<"the value of the hyperbolic sinus is "<< sinh(q0)<<std::endl;
    std::cout<<"the value of the hyperbolic tangent is "<< tanh(q0)<<std::endl;
    std::cout<<"the value of the Sinus Cardinal (of index pi) is "<< sinc_pi(q0)<<std::endl;
     

    
    float rho = std::sqrt(8.0f);
    float theta = std::atan(1.0f);
    float phi1 = std::atan(1.0f);
    float phi2 = std::atan(1.0f);
    
    
    std::cout<<"The value of the quaternion represented in spherical form by\n"
	     << "rho = " << rho << " , theta = " << theta
	     << " , phi1 = " << phi1 << " , phi2 = " << phi2
	     << " is \n"
	     <<boost::math::spherical(rho, theta, phi1, phi2)<<std::endl;
    float  alpha = std::atan(1.0f);
    std::cout<<"The value of the quaternion represented in semipolar form by\n"
	     << "rho = " << rho << " , alpha = " << alpha
	     << " , phi1 = " << phi1 << " , phi2 = " << phi2
	     << " is \n"
	     <<boost::math::semipolar(rho, alpha, phi1, phi2)<<std::endl;

    float rho1 = 1;
    float rho2 = 2;
    float theta1 = 0;
    float theta2 = std::atan(1.0f)*2;
    
    std::cout<<"The value of the quaternion represented in multipolar form by\n"
	     << "rho1 = " << rho1 << " , theta1 = " << theta1
	     << " , rho2 = " << rho2 << " , tetha2 = " << theta2
	     << " is \n"
	     <<boost::math::multipolar(rho1, theta1, rho2, theta2)<<std::endl;


     
     

  return 0;
}
