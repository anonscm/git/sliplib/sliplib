#include <iostream>
#include <cmath>
#include <string>
#include <vector>
#include <cmath>
#include <algorithm>

#include <slip/Array.hpp>
#include <slip/Array2d.hpp>
#include <slip/arithmetic_op.hpp>
#include <slip/ColorImage.hpp>
#include <slip/dynamic.hpp>
#include <slip/convolution.hpp>

//command line options
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

namespace po = boost::program_options;



/**
   ** \brief Computes the Pascal's triangle : triangular array of the binomial coefficients in a triangle.
   ** Pascal's triangle determines the coefficients which arise in binomial expansions 
   ** \param order order of the binomial expansions.
   ** \param triangle Container2d containing the binomial coefficients.
   ** \remarks May be used to compute binomial coefficients.
   ** \par Example:
   ** \code
   ** //generate
   ** //1 0 0 0 0 
   ** //1 1 0 0 0 
   ** //1 2 1 0 0 
   ** //1 3 3 1 0 
   ** //1 4 6 4 1
   ** std::size_t order = 4;
   ** slip::Array2d<int> triangle;
   ** slipalgo::pascal_triangle(order,triangle);
   ** \endcode
   */
template<class Container2d>
void pascal_triangle(const std::size_t order,
		     Container2d& triangle)
{
  triangle.resize(order+1,order+1);
  for (std::size_t i = 0; i < order+1; ++i)
    {
      triangle[i][0] = 1; // left-most entry
		
      for (std::size_t j = 1; j < i; ++j)
	{
	  triangle[i][j] = triangle[i-1][j-1] + triangle[i-1][j];
	}
      triangle[i][i] = 1; // right-most entry
    }
}
   /**
   ** \brief Computes real binomial filters of standard deviation egal to sqrt(((last -first-1)/4)
    ** \param first RandomAccessIterator to the filter range.
   ** \param last RandomAccessIterator to the filter range.
   ** \par Example:
   ** \code
   ** slip::Vector<double> binomial5(5);
   ** slipalgo::binomial_filter(binomial5.begin(),binomial5.end());
   ** std::cout<<"binomial filter of size 5\n "<<binomial5<<std::endl;
   ** std::cout<<std::endl;
   ** \endcode
   */
template <class RandomAccessIterator>
inline
void binomial_filter(RandomAccessIterator first,
		     RandomAccessIterator last)
{
  typedef typename std::iterator_traits<RandomAccessIterator>::value_type T; 
  const int filter_size = static_cast<int>(last -first);
  const int order = filter_size - 1;
  slip::Array2d<T> triangle;
  pascal_triangle(order,triangle);
  T sum = std::accumulate(triangle.row_begin(order),triangle.row_end(order),T());
  slip::divides_scalar(triangle.row_begin(order),triangle.row_end(order),sum,first);
}


//---------------------------------------------------------------------------
// Computes a the Gaussian Filter of an image
//
//--------------------------------------------------------------------------
int main(int ac, char** av)
{
  //typedef to setup computation type
  typedef float T;
  
  //--------------------------------------------------------
  //  Get the options from the command line
  //--------------------------------------------------------
  T sigma = 1.0;
  // Declare the supported options.
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help", "Binomial filtering of variance sigma")
    //two possible options --input-file or -i
    ("input-file,i", po::value<std::string>(), "input image file")
    ("sigma,s", po::value<T>(&sigma)->default_value(1),"standard deviation of the gaussian filter")
    ("output-file,o", po::value<std::string>(), "output image file")
    ;


  //get the command line parameters
  po::variables_map vm;
  po::store(po::parse_command_line(ac, av, desc), vm);
  po::notify(vm);    

  if (vm.count("help")) 
    {
      std::cout << desc << "\n";
      return 1;
    }
  //get the input file
  std::string vs;
  if (vm.count("input-file"))
    {
      vs = vm["input-file"].as<std::string>();
      std::cout << "Input file is: ";
      std::cout<< vs;
    }
  std::cout<<std::endl;
  //get the 
  if (vm.count("sigma")) 
    {
      std::cout << "The sigma of the gaussian is " 
		<< vm["sigma"].as<T>() << ".\n";
    } 
  else
    {
      std::cout << "The sigma of the Gaussian is set to"<<sigma<<std::endl; 
    }
  
  if (vm.count("output-file")) 
    {
      std::cout << "output-file was set to " 
		<< vm["output-file"].as<std::string>() << ".\n";
    } 
  //--------------------------------------------------------

 //-----------------------------------------------
 // Read the input images
 //-----------------------------------------------
 slip::ColorImage<T> I1;
 I1.read(vs);

 const std::size_t rows = I1.rows();
 const std::size_t cols = I1.cols();
 slip::ColorImage<T> I2(rows,cols);
 
 //-----------------------------------------------
 // Computes the binomial filter mask
 //-----------------------------------------------
 const std::size_t filter_size = (static_cast<std::size_t>(static_cast<T>(4.0)*sigma*sigma)+1);
 slip::Array<T> filter(filter_size);
 binomial_filter(filter.begin(),filter.end());

 //-----------------------------------------------
 // Separable computation of the binomial filter
 //-----------------------------------------------
 //red component
 slip::separable_convolution2d(I1.upper_left(0),I1.bottom_right(0),
 			       filter.begin(),filter.end(),
 			       filter.begin(),filter.end(),
 			       I2.upper_left(0));
 //green component
 slip::separable_convolution2d(I1.upper_left(1),I1.bottom_right(1),
 			       filter.begin(),filter.end(),
 			       filter.begin(),filter.end(),
 			       I2.upper_left(1));
 //blue component
 slip::separable_convolution2d(I1.upper_left(2),I1.bottom_right(2),
 			       filter.begin(),filter.end(),
 			       filter.begin(),filter.end(),
 			       I2.upper_left(2));
 // //or equivalently and shortly
 // slip::separable_convolution2d(I1,filter,filter,I2);
 
 //-----------------------------------------------
 // Write the output images
 //-----------------------------------------------
 I2.write(vm["output-file"].as<std::string>());

 return 0;
}
