#include <iostream>
#include <vector>
#include <slip/linear_algebra_svd.hpp>
#include <slip/GrayscaleImage.hpp>
#include <slip/Matrix.hpp>
#include <slip/Vector.hpp>
#include <slip/dynamic.hpp>

//#include <ctime>

static void usage(const char * );


int main(int argc, char **argv)
{

  //get the argmuments
  std::vector<std::string> args;
  for(int i = 0; i < argc; i++)
    {
      args.push_back(argv[i]);
    }

 
  if((args.size() == 1) || (args[1] == "-h") || (args[1] == "--help"))
    {
      usage(args[0].c_str());
      exit(0);
    }  

  if(args.size() == 7)
    {

      //---------------------------
      //read input image
      //---------------------------
      slip::GrayscaleImage<double> I;
      I.read(args[2]);
      
      const std::size_t rows = I.rows();
      const std::size_t cols = I.cols();
      
      //---------------------------
      //compute SVD decomposition
      //---------------------------
       slip::GrayscaleImage<double> U(rows,cols);
       slip::GrayscaleImage<double> V(cols,cols);
       slip::Vector<double> W(cols);
       slip::svd(I,U,W.begin(),W.end(),V);
        
       //----------------------------------------
       //compute K first SVD modes approximation
       //----------------------------------------
       slip::GrayscaleImage<double> Idecomp(rows,cols);
       const std::size_t K = (unsigned int)atoi(argv[4]);
       slip::svd_approx(U,W.begin(),W.end(),V,K,Idecomp);

      //---------------------------
      //write approximated image
      //---------------------------
       Idecomp.write(args[6]);
      
    }
  else
    {
      usage(args[0].c_str());
      exit(1);
    }
  return 0;
}


static void usage(const char* cmd)
{
  std::cout<<std::endl;
  std::cout<<"NAME:"<<std::endl;
  std::cout<<"\t"<<cmd<<" - Compute the Singular Value Decomposition of lenght K of a grayscale"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"SYNOPSIS:"<<std::endl;
  std::cout<<"\t"<<cmd<<" -i file -l K -o file"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"DESCRIPTION: Compute the Singular Value Decomposition of lenght K of a grayscale:\n"<<std::endl;
  std::cout<<"\t K must be less or egal than to the number of columns of the image"<<std::endl;
  std::cout<<"\t "<<std::endl;
  std::cout<<std::endl;
  std::cout<<"\t -i  input image"<<std::endl;
  std::cout<<"\t -l  K lenght of the decomposition (integer)"<<std::endl;
  std::cout<<"\t -o  output image"<<std::endl;
  std::cout<<"\t -h, --help\t display this help and exit"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"AUTHOR:"<<std::endl;
  std::cout<<"\t Benoit Tremblais <tremblais@sic.univ-poitiers.fr>"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"REPORTING BUGS:"<<std::endl;
  std::cout<<"\t Report bugs to slip trac serveur"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"COPYRIGHT:"<<std::endl;
  std::cout<<"\t This is free software; see the source for copying conditions."<<std::endl;
  std::cout<<std::endl;
  std::cout<<"SEE ALSO:"<<std::endl;
  std::cout<<std::endl;
}
