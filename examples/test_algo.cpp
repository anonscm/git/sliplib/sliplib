#include <iostream>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <vector>

#include <cstdlib>
#include <ctime>

struct Float
{
  float twice(const float& x)
  {
    
    return 2.0f * x;
  }

  void print()
  {
    std::cout<<f<<" ";
  }
  float f;
};


float c_fun(const float x)
{
  return 2.0f * x;
}

float c_fun2(const float x, const float y)
{
  return 2.0f * (x + y);
}


template<class T> struct un_fun : public std::unary_function<T,T>
{
  un_fun(){}
  T operator() (const T& x){return x * T(2);}
};

template<class T> struct bin_fun : public std::binary_function<T,T,T>
{
  bin_fun(){}
  T operator() (const T& x, const T& y){return x * x + y * y;}
};


struct RndIntGen
{
  RndIntGen(int low, int high):low_(low),high_(high)
  {}
  int operator()() const
  {
    return low_ + (rand() % ((high_ - low_) + 1));
  }

  int low_;
  int high_;
};

int main()
{
  std::vector<float> v(10);
  for(size_t i = 0; i < v.size(); ++i)
    {
      v[i] = float(i);
    }
  
  std::transform(v.begin(),v.end(),v.begin(),un_fun<float>());
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  //acceptable
  std::transform(v.begin(),v.end(),v.begin(),c_fun);
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  //preferable
  std::transform(v.begin(),v.end(),v.begin(),std::ptr_fun(&c_fun));
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  //acceptable
  std::transform(v.begin(),v.end(),v.begin(),v.begin(),c_fun2);
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  //preferable
  std::transform(v.begin(),v.end(),v.begin(),v.begin(),std::ptr_fun(c_fun2));
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  
  std::transform(v.begin(),v.end(),v.begin(),v.begin(),c_fun2);
  std::copy(v.begin(),v.end(),std::ostream_iterator<float>(std::cout," "));
  std::cout<<std::endl;
  
    std::vector<Float> v2(10);
  //std::transform(v2.begin(),v2.end(),v.begin(),std::mem_fun_ref(&Float::twice));
    std::for_each(v2.begin(),v2.end(),std::mem_fun_ref(&Float::print));
  std::cout<<std::endl;

  std::vector<Float*> v3(10);
  for(size_t i = 0; i < v3.size(); ++i)
    {
      v3[i] = new Float;
    }
  std::for_each(v3.begin(),v3.end(),std::mem_fun(&Float::print));
  std::cout<<std::endl;


  std::vector<int> vi(10);
  srand(static_cast<unsigned int>(clock()));
  //srand(0);
  std::generate(vi.begin(),vi.end(),RndIntGen(0,9));
  std::copy(vi.begin(),vi.end(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;
  std::fill_n(vi.begin(),10,0);
  std::generate_n(vi.begin(),4,RndIntGen(0,9));
  std::copy(vi.begin(),vi.end(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl;


  std::sort(vi.begin(),vi.end());
  std::copy(vi.begin(),vi.end(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl; 
  
  std::vector<int> vi2(10);
  for(size_t i = 0; i < vi2.size(); ++i)
    {
      vi2[i] = i;
    }

  std::cout<<std::inner_product(vi.begin(),vi.end(),vi2.begin(),0)<<std::endl;
  std::partial_sum(vi2.begin(),vi2.end(),vi2.begin());
  std::cout<<std::accumulate(vi2.begin(),vi2.end(),0);
  std::copy(vi2.begin(),vi2.end(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl; 
  std::adjacent_difference(vi2.begin(),vi2.end(),vi2.begin());
  std::copy(vi2.begin(),vi2.end(),std::ostream_iterator<int>(std::cout," "));
  std::cout<<std::endl; 

  std::cout<<*std::find(vi2.begin(),vi2.end(),5)<<std::endl;
  std::cout<<*min_element(vi2.begin(),vi2.end())<<std::endl;
  std::cout<<*max_element(vi2.begin(),vi2.end())<<std::endl;
  return 0;
}
