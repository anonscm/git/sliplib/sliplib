#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <slip/convolution.hpp>
#include <slip/Block.hpp>
#include <slip/Color.hpp>
#include <slip/border_treatment.hpp>
#include <slip/GrayscaleImage.hpp>
#include <slip/ColorImage.hpp>
#include <slip/dynamic.hpp>
#include <slip/convolution.hpp>



static void usage(const char * );


//-------------------------------------
// Functor to compute di-zenzo gradient
// from marginal derivatives
//-------------------------------------
struct Di_zenzo_from_derivative : 
public std::binary_function<slip::Color<float>,
			    slip::Color<float>,
			    float>
{
  Di_zenzo_from_derivative(){}
  float operator()(const slip::Color<float>& c1, 
		   const slip::Color<float>& c2)
  {
    slip::Color<float> I12;
    std::transform(c1.begin(),c1.end(),c1.begin(),I12.begin(),std::multiplies<float>());
    slip::Color<float> I22;
    std::transform(c2.begin(),c2.end(),c2.begin(),I22.begin(),std::multiplies<float>());
    
    float a11 = std::accumulate(I12.begin(),I12.end(),0.0f);
    float a22 = std::accumulate(I22.begin(),I22.end(),0.0f);
    float a12 = std::inner_product(c1.begin(),c1.end(),c2.begin(),0.0f);
    
    float trace = a11 + a22;
    float delta = (a11 * a22) - (a12 * a12);
    return 0.5f * (trace +  std::sqrt((trace * trace) - 4.0f * delta));
  }
};

int main(int argc, char **argv)
{

  //get the argmuments
  std::vector<std::string> args;
  for(int i = 0; i < argc; i++)
    {
      args.push_back(argv[i]);
    }

  if((args.size() == 1) || (args[1] == "-h") || (args[1] == "--help"))
    {
      usage(args[0].c_str());
      exit(0);
    }  

  if(args.size() == 5)
    {

      //---------------------------
      //edges detection by a Sobel filtering
      //---------------------------
      //create convolution kernels
      const slip::block<float,3> derivative_kernel = {1.0f,0.0f,-1.0f};
      const slip::block<float,3> smooth_kernel     = {1.0f,2.0f,1.0f};
      
       //---------------------------
      //read input image
      //---------------------------
      slip::ColorImage<float> I;
      I.read(args[2]);
      const std::size_t rows = I.rows();
      const std::size_t cols = I.cols();
      
      //---------------------------
      //x gradient computation
      //---------------------------
      //create x gradient image   
      slip::ColorImage<float> Gx(rows,cols);
      slip::separable_convolution2d(I,
				    derivative_kernel,
				    smooth_kernel,
				    Gx,
				    slip::BORDER_TREATMENT_NEUMANN);
 
      //---------------------------
      //y gradient computation
      //---------------------------
       slip::ColorImage<float> Gy(rows,cols);
       slip::separable_convolution2d(I,
				    smooth_kernel,
				    derivative_kernel,
				    Gy,
				    slip::BORDER_TREATMENT_NEUMANN);
    
      //----------------------------------
      //compute the Di-zenzo gradient norm
      //----------------------------------
      slip::GrayscaleImage<float> G(rows,cols);
      std::transform(Gx.begin(),Gx.end(),
		     Gy.begin(),
		     G.begin(),
		     xDi_zenzo_from_derivative());
      
      //---------------------------
      //change dynamic to put values of G in the range [0.0,1.0]
      //---------------------------
      slip::change_dynamic_01(G.begin(),G.end(),G.begin(),slip::AFFINE_FUNCTION);
      //---------------------------
      //write the gradient norm image*
      //---------------------------
      G.write(args[4]);
    }
  else
    {
      usage(args[0].c_str());
      exit(1);
    }
  return 0;
}


static void usage(const char* cmd)
{
  std::cout<<std::endl;
  std::cout<<"NAME:"<<std::endl;
  std::cout<<"\t"<<cmd<<" - Computes the gradient norm using the Sobel masks and the Dizenzo gradient norm"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"SYNOPSIS:"<<std::endl;
  std::cout<<"\t"<<cmd<<" -i file -o file"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"DESCRIPTION: Computes the gradient norm using the Sobel masks :\n"<<std::endl;
  std::cout<<"\thx = [-1 0 1; and hy = [-1 -2 -1;  "<<std::endl;
  std::cout<<"\t      -2 0 2;            0  0  0;"<<std::endl;
  std::cout<<"\t      -1 0 1]            1  2  1]"<<std::endl;
  std::cout<<"\t "<<std::endl;
  std::cout<<std::endl;
  std::cout<<"\t -i  input image"<<std::endl;
  std::cout<<"\t -o  output image"<<std::endl;
  std::cout<<"\t -h, --help\t display this help and exit"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"AUTHOR:"<<std::endl;
  std::cout<<"\t Benoit Tremblais <tremblais@sic.univ-poitiers.fr>"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"REPORTING BUGS:"<<std::endl;
  std::cout<<"\t Report bugs to slip trac serveur"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"COPYRIGHT:"<<std::endl;
  std::cout<<"\t This is free software; see the source for copying conditions."<<std::endl;
  std::cout<<std::endl;
  std::cout<<"SEE ALSO:"<<std::endl;
  std::cout<<std::endl;
}
