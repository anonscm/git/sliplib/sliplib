#include <iostream>
#include <iterator>
#include <string>

int main()
{
  //construction
  std::string s0;
  std::cout<<s0<<std::endl;
  std::string s1 = "chaine";
  std::cout<<s1<<std::endl;
  std::string s2("chaine2");
  std::cout<<s2<<std::endl;
  std::string s3("chaine3",4);
  std::cout<<s3<<std::endl;
  std::string s4(6,'c');
  std::cout<<s4<<std::endl;
  const char* chaine = "les chaines de caracteres";
  std::string s5(chaine, chaine+25);
  std::cout<<s5<<std::endl;
  std::string s6(s2.begin(),s2.end());
  std::cout<<s6<<std::endl;
  std::string s7 = s1;
  std::cout<<s7<<std::endl;
  
  //string informations
  std::cout<<"size of the string = "<<s2.size()<<std::endl;
  std::cout<<"max size of the string = "<<s2.max_size()<<std::endl;
  std::cout<<"length of the string = "<<s2.length()<<std::endl;
  std::cout<<"number of elements for which the memory have been alocated = "<<s2.capacity()<<std::endl;
  std::cout<<"string empty ? "<<s2.empty()<<std::endl;

  //elements access
  std::cout<<s2[2]<<std::endl;
  s2[3] = 'r';
  std::cout<<s2<<std::endl;

  //string assignement
  s7 = s1;
  std::cout<<s7<<std::endl;
  s7.assign("toto");
  std::cout<<s7<<std::endl;

  
  //string concatenation
  s7.append("titi");
  std::cout<<s7<<std::endl;
  s7+="tutu";
  std::cout<<s7<<std::endl;
  std::cout<<s7+"turlututu"<<std::endl;
  
  //string comparizon
  std::string s8 = "albert";
  std::string s9 = "alberto";
  
  std::cout<<" == "<<(s8 == s9)<<std::endl;
  std::cout<<" != "<<(s8 != s9)<<std::endl;
  std::cout<<" < "<<(s8 < s9)<<std::endl;
  std::cout<<" <= "<<(s8 <= s9)<<std::endl;
  std::cout<<" > "<<(s8 > s9)<<std::endl;
  std::cout<<" >= "<<(s8 >= s9)<<std::endl;
  std::cout<<s8.compare(s9)<<std::endl;
  std::cout<<s8.compare(s8)<<std::endl;
  std::cout<<s9.compare(s8)<<std::endl;

  //iteration
  std::copy(s9.begin(),s9.end(),std::ostream_iterator<char>(std::cout," "));
  std::cout<<std::endl;
  std::copy(s9.rbegin(),s9.rend(),std::ostream_iterator<char>(std::cout," "));
  std::cout<<std::endl;
  
  //C interface
  const char* tab = s7.c_str();
  for(int i = 0; i < (s7.size() + 1); ++i)
    {
      std::cout<<tab[i];
    }
  std::cout<<std::endl;
  

  //find
  std::cout<<s7.find("tu")<<std::endl;
  std::cout<<s7.rfind("tu")<<std::endl;
  std::cout<<s7.find_first_of("tu")<<std::endl;
  std::cout<<s7.find_last_of("tu")<<std::endl;
  
  //other operations
  std::string s10 = "fichier.dat";
  std::cout<<s10.substr(s10.rfind("."),4)<<std::endl;
  s10.insert(7,"-001");
  std::cout<<s10<<std::endl;
  s10.replace(s10.rfind("."),4,".gz");
  std::cout<<s10<<std::endl;
  s10.erase(s10.rfind("."),4);
  std::cout<<s10<<std::endl;

  s10.resize(7);
  std::cout<<s10<<std::endl;

  //input string
  std::string s11;
  getline(std::cin,s11);
  std::cout<<s11<<std::endl;
  std::string s12;
  std::cin>>s12;
  std::cout<<s12<<std::endl;

 //Unicode string
  std::wstring s = L"e accute : \u00e9";
  std::wcout<<s<<std::endl;
  const wchar_t* w_tab = s.c_str();
  for(int i = 0; i < (s.size() + 1); ++i)
    {
      std::wcout<<w_tab[i];
    }
  std::cout<<std::endl;

  return 0;
}
