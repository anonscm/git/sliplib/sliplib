#include <iostream>

#include <slip/ColorImage.hpp>
#include <slip/statistics.hpp>
#include <slip/Block.hpp>

#define N 10

template<typename T>
void myrand(T& a)
{
  a = T((double)std::rand()/((double)RAND_MAX+1)*N);
}


int main()
{

  typedef double T;
  //------------------------------------
  //init ColorImage with random values
  //------------------------------------
  slip::ColorImage<T> I(N,N);
  std::for_each(I.begin(0),I.end(0),myrand<T>); 
  std::for_each(I.begin(1),I.end(1),myrand<T>); 
  std::for_each(I.begin(2),I.end(2),myrand<T>); 

  //------------------------------------
  //Computes statistics on the red plane
  //------------------------------------
  slip::Statistics<double> Sred;
  slip::statistics(I.begin(0),I.end(0),Sred);
  std::cout<<"----------------------"<<std::endl;
  std::cout<<"red plane statistics: "<<std::endl;
  std::cout<<"----------------------"<<std::endl;
  std::cout<<"min                 = "<<Sred.min()<<std::endl;
  std::cout<<"first_quartile      = "<<Sred.first_quartile()<<std::endl;
  std::cout<<"median              = "<<Sred.median()<<std::endl;
  std::cout<<"third_quartile      = "<<Sred.third_quartile()<<std::endl;
  std::cout<<"max                 = "<<Sred.max()<<std::endl;
  std::cout<<"mean                = "<<Sred.mean()<<std::endl;
  std::cout<<"standard deviation  = "<<Sred.std_dev()<<std::endl;
  std::cout<<"skewness            = "<<Sred.skewness()<<std::endl;
  std::cout<<"kurtosis            = "<<Sred.kurtosis()<<std::endl;
  std::cout<<"cardinal            = "<<Sred.cardinal()<<std::endl;
  slip::block<double,10> all_red_stat = Sred.all();

  slip::Statistics<double> Sgreen;
  slip::statistics(I.begin(1),I.end(1),Sgreen);
  std::cout<<"----------------------"<<std::endl;
  std::cout<<"green plane statistics: "<<std::endl;
  std::cout<<"----------------------"<<std::endl;
  std::cout<<"min                 = "<<Sgreen.min()<<std::endl;
  std::cout<<"first_quartile      = "<<Sgreen.first_quartile()<<std::endl;
  std::cout<<"median              = "<<Sgreen.median()<<std::endl;
  std::cout<<"third_quartile      = "<<Sgreen.third_quartile()<<std::endl;
  std::cout<<"max                 = "<<Sgreen.max()<<std::endl;
  std::cout<<"mean                = "<<Sgreen.mean()<<std::endl;
  std::cout<<"standard deviation  = "<<Sgreen.std_dev()<<std::endl;
  std::cout<<"skewness            = "<<Sgreen.skewness()<<std::endl;
  std::cout<<"kurtosis            = "<<Sgreen.kurtosis()<<std::endl;
  std::cout<<"cardinal            = "<<Sgreen.cardinal()<<std::endl;
  slip::block<double,10> all_green_stat = Sgreen.all();

  slip::Statistics<double> Sblue;
  slip::statistics(I.begin(2),I.end(2),Sblue);
  std::cout<<"----------------------"<<std::endl;
  std::cout<<"blue plane statistics: "<<std::endl;
  std::cout<<"----------------------"<<std::endl;
  std::cout<<"min                 = "<<Sblue.min()<<std::endl;
  std::cout<<"first_quartile      = "<<Sblue.first_quartile()<<std::endl;
  std::cout<<"median              = "<<Sblue.median()<<std::endl;
  std::cout<<"third_quartile      = "<<Sblue.third_quartile()<<std::endl;
  std::cout<<"max                 = "<<Sblue.max()<<std::endl;
  std::cout<<"mean                = "<<Sblue.mean()<<std::endl;
  std::cout<<"standard deviation  = "<<Sblue.std_dev()<<std::endl;
  std::cout<<"skewness            = "<<Sblue.skewness()<<std::endl;
  std::cout<<"kurtosis            = "<<Sblue.kurtosis()<<std::endl;
  std::cout<<"cardinal            = "<<Sblue.cardinal()<<std::endl;
  slip::block<double,10> all_blue_stat = Sblue.all();


  return 0;
}

  
