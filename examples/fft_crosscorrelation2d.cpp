#include <complex>
#include <iostream>
#include <vector>

#include <slip/dynamic.hpp>
#include <slip/correlation.hpp>
#include <slip/GrayscaleImage.hpp>

static void usage(const char * );

int main(int argc, char **argv)
{
  //get the command line argmuments
  std::vector<std::string> args;
  for(int i = 0; i < argc; i++)
    {
      args.push_back(argv[i]);
    }
  
  if((args.size() == 1) || (args[1] == "-h") || (args[1] == "--help"))
    {
      usage(args[0].c_str());
      exit(0);
    }  

  if(args.size() == 7)
    {
      slip::GrayscaleImage<double> I1;
      I1.read(args[2]);
      slip::GrayscaleImage<double> I2;
      I2.read(args[4]);
      const std::size_t rows = I1.rows();
      const std::size_t cols = I1.cols();
      
      slip::GrayscaleImage<double> Out(rows,cols,0.0);
      slip::fft_crosscorrelation2d_same(I1.upper_left(),I1.bottom_right(),
					I2.upper_left(),I2.bottom_right(),
					Out.upper_left(),Out.bottom_right());
      slip::change_dynamic_01(Out.upper_left(),Out.bottom_right(),Out.upper_left(),slip::AFFINE_FUNCTION); 
      Out.write(args[6]);
    }
  else 
    {
      usage(args[0].c_str());
      exit(0);
    }
  return 0;
}

static void usage(const char* cmd)
{
  std::cout<<std::endl;
  std::cout<<"NAME:"<<std::endl;
  std::cout<<"\t"<<cmd<<" - Computes the fft cross correlation of two images"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"SYNOPSIS:"<<std::endl;
  std::cout<<"\t"<<cmd<<" -i file -m file -o file"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"DESCRIPTION: - Computes  the cross correlation of two images :\n"<<std::endl;
  std::cout<<"\t "<<std::endl;
  std::cout<<std::endl;
  std::cout<<"\t -i  input image"<<std::endl;
  std::cout<<"\t -m  mask image"<<std::endl;
  std::cout<<"\t -o  output image"<<std::endl;
  std::cout<<"\t -h, --help\t display this help and exit"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"AUTHOR:"<<std::endl;
  std::cout<<"\t Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"REPORTING BUGS:"<<std::endl;
  std::cout<<"\t Report bugs to slip trac serveur"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"COPYRIGHT:"<<std::endl;
  std::cout<<"\t This is free software; see the source for copying conditions."<<std::endl;
  std::cout<<std::endl;
  std::cout<<"SEE ALSO:"<<std::endl;
  std::cout<<std::endl;
}
