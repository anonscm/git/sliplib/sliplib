/*!
 ** \file av_file_io.cpp
 ** \brief video files io examples
 ** \version Fluex 1.0
 ** \date 2013/10/30
 ** \author  Denis Arrivault <denis.arrivault_AT_inria.fr>
 */
//stl
#include <iostream>
//slip
#include "ColorVolume.hpp"
#include "io_tools.hpp"
#include "AvReader.hpp"
#include "AvWriter.hpp"


/*!
 ** \brief usage :
 */
void usage(const char *name)
{
  std::cout << "usage :\n\n";
  std::cout << "\t " << name << " input_av_color_video ";
  std::cout << std::endl;
}


int main(int argc, char* argv[])
{
  if (argc < 2)
    {
      usage(*argv);
      return 0;
    }
  std::string test_name(*argv);
  std::string filename(argv[1]);
  std::string ext;
  std::string output_base;
  slip::split_extension(filename,output_base,ext);

  //Initialize the reader and the writer for the color video

  slip::AvReader<slip::ColorVolume<double>,double,3,10> coul_avread;
  coul_avread.set_data_filename(filename);
  try
    {
    coul_avread.initialize();
  }
  catch(std::exception & exc)
    {
      std::cerr << exc.what() << '\n';
    }
  AVRational t_base = coul_avread.get_time_base();
  int64_t bit_rate = coul_avread.get_bit_rate();
  int nbframes = coul_avread.get_nb_frames();
  int height = coul_avread.get_frame_height();
  int width = coul_avread.get_frame_width();
  std::cout << "Video size = " << nbframes << " frames, "
	    << width << " x " << height << "\n";
  std::cout << "frame per sec = " <<  double(t_base.den) / double(t_base.num) << " , bit rate = "
	    << bit_rate << "\n";
  //initialize the writer
  slip::AvWriter<slip::ColorVolume<double>,double,3> VideoCoulWriter(output_base + "_out.mp4",
								     width,height,nbframes,bit_rate,t_base,0,AV_CODEC_ID_MPEG4);

  //read and write the video sequentially
  for(int i=0; i<10; ++i)
    {
      slip::ColorVolume<double> coul_video;
      coul_avread.read(coul_video);
      std::cout << "Read frames = " << coul_avread.get_read_frames();
      std::cout << " total nb of frames = " << coul_avread.get_nb_frames() << "\n";
      VideoCoulWriter.write(coul_video);
    }

  std::cout << output_base + "_out" + ext << " written.\n";
  std::cout << test_name << " successful executed.\n";
  return 0;
}


