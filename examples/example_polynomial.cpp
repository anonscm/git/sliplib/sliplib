#include <iostream>
#include <slip/Polynomial.hpp>

int main()
{
  
//creates two polynomials from 
float p1[] = {1.0f, 2.0f, 3.0f, 4.0f, 5.0f};
slip::Polynomial<float> P1(4,p1);
float p2[] = {2.0f, 3.0f, 4.0f};
slip::Polynomial<float> P2(2,p2);
//multiplies the two polynomials
std::cout<<P1*P2<<std::endl;
//derivates and integrates P2
P2.derivative();
P2.integral();
//evaluate P1 at 2.3f
std::cout<<P1.evaluate(2.3f)<<std::endl;
//fit data with a polynomial
slip::Polynomial<float> P6;
slip::Vector<float> xi(4);
xi[0] = 0.0; xi[1] = 1.0; xi[2] = 4.0; xi[3] = 5.0;
slip::Vector<float> yi(4);
yi[0] = 2.0; yi[1] = 7.0; yi[2] = 22.0; yi[3] = 27.0;
P6.fit(xi.begin(),xi.end(),yi.begin(),2);
std::cout<<P6<<std::endl;

 return 0;
}
