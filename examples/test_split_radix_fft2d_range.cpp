#include <slip/FFT.hpp>
#include <slip/Array.hpp>
#include <complex>
#include <iostream>
#include <vector>

#include <slip/GrayscaleImage.hpp>
#include <slip/dynamic.hpp>

static void usage(const char * );

int main(int argc, char **argv)
{
  //get the argmuments
  std::vector<std::string> args;
  for(int i = 0; i < argc; i++)
    {
      args.push_back(argv[i]);
    }
  
  if((args.size() == 1) || (args[1] == "-h") || (args[1] == "--help"))
    {
      usage(args[0].c_str());
      exit(0);
    }  

  if(args.size() == 7)
    {
      slip::GrayscaleImage<double> I;
      I.read(args[2]);

      //range variables definition
      int pas = 2;
      int RanXmin = (int)((double)I.dim1()/4.0) + 3;
      int RanXmax = (int)((double)I.dim1()/2.0);
      int RanYmin = (int)((double)I.dim2()/4.0) + 5;
      int RanYmax = (int)((double)I.dim2()/2.0);
      int dimimoutX = (RanXmax-RanXmin)/pas+1;
      int dimimoutY = (RanYmax-RanYmin)/pas+1;
      std::cout << RanXmin << " , ";
      std::cout << RanXmax << " , ";
      std::cout << RanYmin << " , ";
      std::cout << RanYmax << std::endl;
      std::cout << dimimoutX << " , ";
      std::cout << dimimoutY << std::endl;

  //real_split_radix_fft2d on a range
      slip::Range<int> range_line(RanXmin,RanXmax,pas);
      slip::Range<int> range_col(RanYmin,RanYmax,pas);
      slip::Matrix<std::complex<double> > IRangeFFT(dimimoutX,dimimoutY);
      slip::Matrix<std::complex<double> > IRange(dimimoutX,dimimoutY);
      
      slip::real_split_radix_fft2d(I.upper_left(range_line,range_col),I.bottom_right(range_line,range_col),IRangeFFT.upper_left());
      
      slip::GrayscaleImage<double> IRangeNorm(IRange.dim1(),IRange.dim2());

      //log(magnitude)
      for(size_t i = 0; i < IRangeFFT.dim1(); ++i)
 	{
 	  for(size_t j = 0; j < IRangeFFT.dim2(); ++j)
 	    {
 	      IRangeNorm[i][j] = std::log(1.0+std::abs(IRangeFFT[i][j]));
 	    }
 	}
      slip::fftshift2d(IRangeNorm.upper_left(),IRangeNorm.bottom_right());
       slip::change_dynamic_01(IRangeNorm.begin(),IRangeNorm.end(),IRangeNorm.begin(),slip::AFFINE_FUNCTION);
       IRangeNorm.write(args[4]);

      //split_radix_ifft2d on the fft image 
       slip::split_radix_ifft2d(IRangeFFT.upper_left(),IRangeFFT.bottom_right(),IRange.upper_left());

      
       for(size_t i = 0; i < IRange.dim1(); ++i)
	{
	  for(size_t j = 0; j < IRange.dim2(); ++j)
	    {
	      IRangeNorm[i][j] = std::abs(IRange[i][j]);
	    }
	}
      
      slip::change_dynamic_01(IRangeNorm.begin(),IRangeNorm.end(),IRangeNorm.begin(),slip::AFFINE_FUNCTION);
      IRangeNorm.write(args[6]);
      
      //std::transform(I2.begin(),I2.end(),Norm.begin(),std::ptr_fun(std::norm));
      }
  else 
    {
      usage(args[0].c_str());
      exit(0);
    }
  return 0;
}

static void usage(const char* cmd)
{
  std::cout<<std::endl;
  std::cout<<"NAME:"<<std::endl;
  std::cout<<"\t"<<cmd<<" - Computes the log-magnitude of the split-radix-fft of a range of an image and inverse split-radix-fft it "<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"SYNOPSIS:"<<std::endl;
  std::cout<<"\t"<<cmd<<" -i file -f file -o file"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"DESCRIPTION: - Computes the log-magnitude of the split-radix-fft of a range of an image and inverse split-radix-fft :\n"<<std::endl;
  std::cout<<"\t The numbers of rows and columns must be power of 2 !"<<std::endl;
  std::cout<<"\t "<<std::endl;
  std::cout<<std::endl;
  std::cout<<"\t -i  input image"<<std::endl;
  std::cout<<"\t -f  fft range image"<<std::endl;
  std::cout<<"\t -o  output range image"<<std::endl;
  std::cout<<"\t -h, --help\t display this help and exit"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"AUTHOR:"<<std::endl;
  std::cout<<"\t Denis Arrivault <arrivault@sic.univ-poitiers.fr>"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"REPORTING BUGS:"<<std::endl;
  std::cout<<"\t Report bugs to slip trac serveur"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"COPYRIGHT:"<<std::endl;
  std::cout<<"\t This is free software; see the source for copying conditions."<<std::endl;
  std::cout<<std::endl;
  std::cout<<"SEE ALSO:"<<std::endl;
  std::cout<<std::endl;
}
