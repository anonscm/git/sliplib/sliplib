#include <iostream>
#include <vector>
#include <slip/GrayscaleImage.hpp>

int main()
{
  //create a 256x256 GrayscaleImage
  slip::GrayscaleImage<double> I(256,256);
  //assign 10.0 to each element of the GrayscaleImage
  I = 10.0;
  //assign 12.0  to each element of the GrayscaleImage
  I.fill(12.0);
  //create a 3x4 GrayscaleImage
  unsigned char tab[] = {1,2,3,4,5,6,7,8,9,10,11,12};
  slip::GrayscaleImage<unsigned char> I2(3,4);
  //fill a Grayscale Image with an array 
  I2.fill(tab);
  //fill a GrayscaleImage with an other container
  std::vector<int> v(256*256);
  I.fill(v.begin(),v.end());
  
 
  return 0;
}
