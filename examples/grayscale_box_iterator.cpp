#include <iostream>
#include <algorithm>
#include <slip/GrayscaleImage.hpp>
#include <slip/Box2d.hpp>

int main()
{ 
  slip::GrayscaleImage<float> I1;
  I1.read("lena.gif");
  const std::size_t height = I1.height();
  const std::size_t width  = I1.width();
  //define the image first quarter box 
  slip::Box2d<int> box(0,0,height/2-1,width/2-1);
  //construct the box image
  slip::GrayscaleImage<float> I1_box(height/2,width/2);
  //copy element in the box to the box image  
  std::copy(I1.upper_left(box),I1.bottom_right(box),
	    I1_box.upper_left());
  //write box image on disk
  I1_box.write("lena_box.png");
  
  return 0;
}
