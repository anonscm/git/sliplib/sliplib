#include <iostream>
#include <limits>

int main()
{

  std::cout<<"\nnumeric_limits<bool> : "<<std::endl;
  std::cout<<"digits     = "<<std::numeric_limits<bool>::digits<<std::endl;
  std::cout<<"is_signed  = "<<std::numeric_limits<bool>::is_signed<<std::endl;
  std::cout<<"is_integer = "<<std::numeric_limits<bool>::is_integer<<std::endl;
  std::cout<<"min = "<<std::numeric_limits<bool>::min()<<std::endl;
  std::cout<<"max = "<<std::numeric_limits<bool>::max()<<std::endl;

  std::cout<<"\nnumeric_limits<char> : "<<std::endl;
  std::cout<<"digits     = "<<std::numeric_limits<char>::digits<<std::endl;
  std::cout<<"is_signed  = "<<std::numeric_limits<char>::is_signed<<std::endl;
  std::cout<<"is_integer = "<<std::numeric_limits<char>::is_integer<<std::endl;
  std::cout<<"min = "<<int(std::numeric_limits<char>::min())<<std::endl;
  std::cout<<"max = "<<int(std::numeric_limits<char>::max())<<std::endl;

  std::cout<<"\nnumeric_limits<unsigned char> : "<<std::endl;
  std::cout<<"digits     = "<<std::numeric_limits<unsigned char>::digits<<std::endl;
  std::cout<<"is_signed  = "<<std::numeric_limits<unsigned char>::is_signed<<std::endl;
  std::cout<<"is_integer = "<<std::numeric_limits<unsigned char>::is_integer<<std::endl;
  std::cout<<"min = "<<int(std::numeric_limits<unsigned char>::min())<<std::endl;
  std::cout<<"max = "<<int(std::numeric_limits<unsigned char>::max())<<std::endl;

  std::cout<<"\nnumeric_limits<int> : "<<std::endl;
  std::cout<<"digits     = "<<std::numeric_limits<int>::digits<<std::endl;
  std::cout<<"is_signed  = "<<std::numeric_limits<int>::is_signed<<std::endl;
  std::cout<<"is_integer = "<<std::numeric_limits<int>::is_integer<<std::endl;
  std::cout<<"min = "<<std::numeric_limits<int>::min()<<std::endl;
  std::cout<<"max = "<<std::numeric_limits<int>::max()<<std::endl;

  std::cout<<"\nnumeric_limits<short> : "<<std::endl;
  std::cout<<"digits     = "<<std::numeric_limits<short>::digits<<std::endl;
  std::cout<<"is_signed  = "<<std::numeric_limits<short>::is_signed<<std::endl;
  std::cout<<"is_integer = "<<std::numeric_limits<short>::is_integer<<std::endl;
  std::cout<<"min = "<<std::numeric_limits<short>::min()<<std::endl;
  std::cout<<"max = "<<std::numeric_limits<short>::max()<<std::endl;


  std::cout<<"\nnumeric_limits<long> : "<<std::endl;
  std::cout<<"digits     = "<<std::numeric_limits<long>::digits<<std::endl;
  std::cout<<"is_signed  = "<<std::numeric_limits<long>::is_signed<<std::endl;
  std::cout<<"is_integer = "<<std::numeric_limits<long>::is_integer<<std::endl;
  std::cout<<"min = "<<std::numeric_limits<long>::min()<<std::endl;
  std::cout<<"max = "<<std::numeric_limits<long>::max()<<std::endl;


  std::cout<<"\nnumeric_limits<float> : "<<std::endl;
  std::cout<<"radix    = "<<std::numeric_limits<float>::radix<<std::endl;
  std::cout<<"digits   = "<<std::numeric_limits<float>::digits<<std::endl;
  std::cout<<"digits10 = "<<std::numeric_limits<float>::digits10<<std::endl;
  
  std::cout<<"is_signed   = "<<std::numeric_limits<float>::is_signed<<std::endl;
  std::cout<<"is_intteger = "<<std::numeric_limits<float>::is_integer<<std::endl;
  std::cout<<"is_exact    = "<<std::numeric_limits<float>::is_exact<<std::endl;
  std::cout<<"min = "<<std::numeric_limits<float>::min()<<std::endl;
  std::cout<<"max = "<<std::numeric_limits<float>::max()<<std::endl;
  std::cout<<"epsilon     = "<<std::numeric_limits<float>::epsilon()<<std::endl;
  std::cout<<"round_error = "<<std::numeric_limits<float>::round_error()<<std::endl;
  std::cout<<"infinity = "<<std::numeric_limits<float>::infinity()<<std::endl;
  std::cout<<"min_exponent   = "<<std::numeric_limits<float>::min_exponent<<std::endl;
  std::cout<<"min_exponent10 = "<<std::numeric_limits<float>::min_exponent10<<std::endl;
  std::cout<<"max_exponent   = "<<std::numeric_limits<float>::max_exponent<<std::endl;
  std::cout<<"max_exponent10 = "<<std::numeric_limits<float>::max_exponent10<<std::endl;

std::cout<<"\nnumeric_limits<double> : "<<std::endl;
  std::cout<<"radix    = "<<std::numeric_limits<double>::radix<<std::endl;
  std::cout<<"digits   = "<<std::numeric_limits<double>::digits<<std::endl;
  std::cout<<"digits10 = "<<std::numeric_limits<double>::digits10<<std::endl;
  
  std::cout<<"is_signed   = "<<std::numeric_limits<double>::is_signed<<std::endl;
  std::cout<<"is_intteger = "<<std::numeric_limits<double>::is_integer<<std::endl;
  std::cout<<"is_exact    = "<<std::numeric_limits<double>::is_exact<<std::endl;
  std::cout<<"min = "<<std::numeric_limits<double>::min()<<std::endl;
  std::cout<<"max = "<<std::numeric_limits<double>::max()<<std::endl;
  std::cout<<"epsilon     = "<<std::numeric_limits<double>::epsilon()<<std::endl;
  std::cout<<"round_error = "<<std::numeric_limits<double>::round_error()<<std::endl;
  std::cout<<"infinity = "<<std::numeric_limits<double>::infinity()<<std::endl;
  std::cout<<"min_exponent = "<<std::numeric_limits<double>::min_exponent<<std::endl;
  std::cout<<"min_exponent10 = "<<std::numeric_limits<double>::min_exponent10<<std::endl;
  std::cout<<"max_exponent   = "<<std::numeric_limits<double>::max_exponent<<std::endl;
  std::cout<<"max_exponent10 = "<<std::numeric_limits<double>::max_exponent10<<std::endl;

  
  
  
  return 0;
}
