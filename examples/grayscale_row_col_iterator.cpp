#include <iostream>
#include <algorithm>
#include <slip/GrayscaleImage.hpp>
#include <slip/dynamic.hpp>


int main()
{ 
  slip::GrayscaleImage<float> I1;
  I1.read("lena.gif");
  const std::size_t height = I1.height();
  const std::size_t width  = I1.width();
  
//computes the horizontal flip of I1
  slip::GrayscaleImage<float> Hflip(height,width);
  for(std::size_t i = 0; i < height; ++i)
    { 
      std::copy(I1.row_begin(i),I1.row_end(i),Hflip.row_rbegin(i));
    }
  Hflip.write("Hflip.png");
  //computes the vertical flip of I1
  slip::GrayscaleImage<float> Vflip(height,width);
  for(std::size_t j = 0; j < width; ++j)
    { 
      std::copy(I1.col_begin(j),I1.col_end(j),Vflip.col_rbegin(j));
    }
  Vflip.write("Vflip.png");

  return 0;
}
