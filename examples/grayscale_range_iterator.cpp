#include <iostream>
#include <algorithm>
#include <slip/GrayscaleImage.hpp>
#include <slip/Range.hpp>
#include <slip/arithmetic_op.hpp>


int main()
{ 
  slip::GrayscaleImage<float> I1;
  I1.read("lena.gif");
  const std::size_t height = I1.height();
  const std::size_t width  = I1.width();
  //define the row range: 1 element over 2  for image height
  slip::Range<int> row_range(0,height-1,2);
  const std::size_t row_iterations = row_range.iterations();
  //define the column range: 1 element over 2 for image width
  slip::Range<int> col_range(0,width-1,2);
  const std::size_t col_iterations = col_range.iterations();
 
  //construct the range image
  slip::GrayscaleImage<float> I1_range(row_iterations+1,col_iterations+1);
  std::copy(I1.upper_left(row_range,col_range),
  	    I1.bottom_right(row_range,col_range),
  	    I1_range.upper_left());
  //write box image on disk
  I1_range.write("lena_range.png");
  
  return 0;
}
