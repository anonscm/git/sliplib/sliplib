#include <iostream>
#include <slip/GrayscaleImage.hpp>

float divide_by_2(float val)
{
  return val /2.0f;
}

int main()
{ 
  slip::GrayscaleImage<float> MM(3,2);
  MM = 5.5;
  slip::GrayscaleImage<float> MM2(3,2);
  MM2 = 2.2;
  
  //pointwise += operator equivalent to MM = MM + MM2
  MM += MM2;
  //pointwise -= operator equivalent to MM = MM - MM2
  MM -= MM2;
  //pointwise *= operator equivalent to MM = MM * MM2
  MM *= MM2;
  //pointwise /= operator equivalent to MM = MM / MM2
  MM /= MM2;
  
  slip::GrayscaleImage<float> MM3(3,2);
  //pointwise addition of two GrayscaleImage
  MM3 = MM + MM2;
  //right scalar addition
  MM3 = MM + 2.0f;
  //left scalar addition
  MM3 = 2.0f + MM;

  //pointwise subtraction of two GrayscaleImage
  MM3 = MM - MM2;
   //right scalar subtraction
  MM3 = MM - 2.0f;
  //left scalar subtraction
  MM3 = 2.0f - MM;

  //pointwise multiplies of two GrayscaleImage
  MM3 = MM * MM2;
  //right scalar multiplication
  MM3 = MM * 2.0f;
  //left scalar multiplication
  MM3 = 2.0f * MM;

  //pointwise division of two GrayscaleImage
  MM3 = MM / MM2;
  //right scalar division
  MM3 = MM / 2.0f;

  //negate a GrayscaleImage
  MM3 = -MM2;

  //pointwise += operator equivalent to MM2 = MM2 + 2.0f
  MM2+=2.0f;
  //pointwise -= operator equivalent to MM2 = MM2 - 2.0f
  MM2-=2.0f;
  //pointwise *= operator equivalent to MM2 = MM2 * 2.0f
  MM2*=2.0f;
  //pointwise /= operator equivalent to MM2 = MM2 / 2.0f
  MM2/=2.0f;

  //Compute the min value of MM2
  std::cout<<MM2.min()<<std::endl;
  //Compute the max value of MM2
  std::cout<<MM2.max()<<std::endl;
  
  //apply std::sqrt to all elements of MM2
  MM2.apply(std::sqrt);
  //apply a C-like function divide_by_2 to all elements of MM2
  MM2.apply(divide_by_2);
  return 0;
}
