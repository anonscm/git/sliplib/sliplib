#include <slip/FFT.hpp>
#include <slip/Array.hpp>
#include <complex>
#include <iostream>
#include <vector>

#include <slip/GrayscaleImage.hpp>
#include <slip/dynamic.hpp>

static void usage(const char * );

int main(int argc, char **argv)
{
  //get the argmuments
  std::vector<std::string> args;
  for(int i = 0; i < argc; i++)
    {
      args.push_back(argv[i]);
    }
  
  if((args.size() == 1) || (args[1] == "-h") || (args[1] == "--help"))
    {
      usage(args[0].c_str());
      exit(0);
    }  

  if(args.size() == 7)
    {
      slip::GrayscaleImage<double> I;
      I.read(args[2]);
     
      //fft2d sur un range
      slip::Range<int> range_line(100,163,2);
      slip::Range<int> range_col(100,163,2);
      //    slip::GrayscaleImage<double> Out(I.dim1()/2,I.dim2()/2);
      //  std::copy(I.upper_left(range_line,range_col),I.bottom_right(range_line,range_col),Out.upper_left());
      //   Out.write(args[4]);

      slip::Matrix<std::complex<double> > IRangeFFT(32,32);
      slip::Matrix<std::complex<double> > IRange(32,32);
      slip::radix2_fft2d(I.upper_left(range_line,range_col),I.bottom_right(range_line,range_col),IRangeFFT.upper_left());
      slip::GrayscaleImage<double> IRangeNorm(IRange.dim1(),IRange.dim2());

      //log(magnitude)
      for(size_t i = 0; i < IRangeFFT.dim1(); ++i)
 	{
 	  for(size_t j = 0; j < IRangeFFT.dim2(); ++j)
 	    {
 	      IRangeNorm[i][j] = std::log(1.0+std::abs(IRangeFFT[i][j]));
 	    }
 	}
      slip::fftshift2d(IRangeNorm.upper_left(),IRangeNorm.bottom_right());
       slip::change_dynamic_01(IRangeNorm.begin(),IRangeNorm.end(),IRangeNorm.begin(),slip::AFFINE_FUNCTION);
       IRangeNorm.write(args[4]);

      //ifft2d sur une box
       slip::radix2_ifft2d(IRangeFFT.upper_left(),IRangeFFT.bottom_right(),IRange.upper_left());

      
       for(size_t i = 0; i < IRange.dim1(); ++i)
	{
	  for(size_t j = 0; j < IRange.dim2(); ++j)
	    {
	      IRangeNorm[i][j] = std::abs(IRange[i][j]);
	    }
	}
      
      slip::change_dynamic_01(IRangeNorm.begin(),IRangeNorm.end(),IRangeNorm.begin(),slip::AFFINE_FUNCTION);
      IRangeNorm.write(args[6]);
      
      //std::transform(I2.begin(),I2.end(),Norm.begin(),std::ptr_fun(std::norm));
      }
  else 
    {
      usage(args[0].c_str());
      exit(0);
    }
  return 0;
}

static void usage(const char* cmd)
{
  std::cout<<std::endl;
  std::cout<<"NAME:"<<std::endl;
  std::cout<<"\t"<<cmd<<" - Computes the log-magnitude of the 2d-fft of a range of an image and inverse 2d-fft it "<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"SYNOPSIS:"<<std::endl;
  std::cout<<"\t"<<cmd<<" -i file -f file -o file"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"DESCRIPTION: - Computes the log-magnitude of the 2d-fft of a range of an image and inverse 2d-fft :\n"<<std::endl;
  std::cout<<"\t The numbers of rows and columns must be power of 2 !"<<std::endl;
  std::cout<<"\t "<<std::endl;
  std::cout<<std::endl;
  std::cout<<"\t -i  input image"<<std::endl;
  std::cout<<"\t -f  fft range image"<<std::endl;
  std::cout<<"\t -o  output range image"<<std::endl;
  std::cout<<"\t -h, --help\t display this help and exit"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"AUTHOR:"<<std::endl;
  std::cout<<"\t Denis Arrivault <arrivault@sic.univ-poitiers.fr>"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"REPORTING BUGS:"<<std::endl;
  std::cout<<"\t Report bugs to slip trac serveur"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"COPYRIGHT:"<<std::endl;
  std::cout<<"\t This is free software; see the source for copying conditions."<<std::endl;
  std::cout<<std::endl;
  std::cout<<"SEE ALSO:"<<std::endl;
  std::cout<<std::endl;
}
