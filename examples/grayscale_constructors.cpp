#include <iostream>
#include <vector>
#include <slip/GrayscaleImage.hpp>

int main()
{
  //create a GrayscaleImage without allocation
  slip::GrayscaleImage<unsigned char> I1;
  //create a GrayscaleImage with 50 rows and 60 columns
  slip::GrayscaleImage<float> I2(50,60);
  //create a 256x256 GrayscaleImage initialized with 10.0
  slip::GrayscaleImage<double> I3(256,256,10.0);
  //create a 3x4 GrayscaleImage initialized by an array
  unsigned char tab[] = {1,2,3,4,5,6,7,8,9,10,11,12};
  slip::GrayscaleImage<unsigned char> I4(3,4,tab);
  //create a GrayscaleImage initialized by an other container
  std::vector<int> v(1200);
  slip::GrayscaleImage<float> I5(30,40,v.begin(),v.end());
  //create a GrayscaleImage by an other one
  slip::GrayscaleImage<float> I6(I2);
  //use alias to create a float grayscale image
  slip::GrayscaleImage_f I7(10,20,0.0f);
 
  return 0;
}
