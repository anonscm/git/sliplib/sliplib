#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <slip/arithmetic_op.hpp>
#include <slip/histo.hpp>
#include <slip/GrayscaleImage.hpp>
//#include <slip/border_treatment.hpp>
#include <slip/RegularVector2dField2d.hpp>
#include <slip/dynamic.hpp>
#include <slip/statistics.hpp>

#include <cstdlib>
#include <ctime>


static void usage(const char * );


int main(int argc, char **argv)
{
  //-----------------------------------------------------------------
  //get the command line argmuments
  //-----------------------------------------------------------------
  std::vector<std::string> args;
  for(int i = 0; i < argc; i++)
    {
      args.push_back(argv[i]);
    }

  if((args.size() == 1) || (args[1] == "-h") || (args[1] == "--help"))
    {
      usage(args[0].c_str());
      exit(0);
    }
  //order of finite difference derivative schemes
  const std::size_t order = static_cast<std::size_t>(atoi(argv[4]));
     
  //------------------------------------------------------------------


  if(args.size() == 15)
    {

      //-----------------------------------------------------------------
      //read the vector field
      //-----------------------------------------------------------------
      slip::RegularVector2dField2d<double> VF;
      VF.read_gnuplot(args[2]);
      //store the number of rows and columns of the vector field
      const std::size_t rows = VF.rows();
      const std::size_t cols = VF.cols();

      //-----------------------------------------------------------------
      //computes the (Euclidean) norm map of the vector field
      //and get some statistics from it
      //-----------------------------------------------------------------
      slip::GrayscaleImage<double> Norm(rows,cols);
      VF.norm(Norm);
      double sum_n  = Norm.sum();
      double energy = sum_n * sum_n;
      double mean_n = slip::mean<double>(Norm.begin(),Norm.end());
      double std_n  = slip::std_dev<double>(Norm.begin(),Norm.end(),mean_n);
      double min_n  = *std::min_element(Norm.begin(),Norm.end());
      double median_n = slip::median_from_data_n(Norm.begin(),Norm.end(),Norm.size());
      double max_n  = *std::max_element(Norm.begin(),Norm.end());
      double rms_n  = slip::rms<double>(Norm.begin(),Norm.end());
      //-----------------------------------------------------------------
      //computes a real histogram of norm
      //-----------------------------------------------------------------
      //create the steps array [min_n, min_n + step_n,...,max_n]
      slip::Array<double> histo_step_n(9);
      double step_n = (max_n-min_n)/double(histo_step_n.size()-1);
      slip::iota(histo_step_n.begin(),histo_step_n.end(),min_n,step_n);
      //computes the histogram
      slip::Array<int> histo_n(histo_step_n.size()-1);
      slip::real_histogram(Norm.begin(),Norm.end(),
			   histo_step_n.begin(),
			   histo_step_n.end(),histo_n.begin());

      std::cout<<"Vector field energy = "<<energy<<std::endl;
      std::cout<<"mean_n        = "<<mean_n<<std::endl;
      std::cout<<"std dev n     = "<<std_n<<std::endl;
      std::cout<<"[min_n,median_n,max_n] = "<<"["<<min_n<<","<<median_n<<","<<max_n<<"]"<<std::endl;
      std::cout<<"rms_n         = "<<rms_n<<std::endl;
      std::cout<<"histo_n ="<<std::endl;
      std::cout<<histo_n<<std::endl;

      

      //-----------------------------------------------------------------
      //computes the angle map of the vector field
      //and get some statistics from it
      //-----------------------------------------------------------------
      slip::GrayscaleImage<double> Angle(rows,cols);
      VF.angle(Angle);
      double mean_a = slip::mean<double>(Angle.begin(),Angle.end());
      double std_a  = slip::std_dev<double>(Angle.begin(),Angle.end(),mean_a);
      double min_a  = *std::min_element(Angle.begin(),Angle.end());
      double median_a = slip::median_from_data_n(Angle.begin(),Angle.end(),Angle.size());
      double max_a  = *std::max_element(Angle.begin(),Angle.end());
      double rms_a  = slip::rms<double>(Angle.begin(),Angle.end());
      //create the steps array [min_a, min_a + step_a,...,max_a]
      slip::Array<double> histo_step_a(9);
      double step_a = (max_a-min_a)/double(histo_step_a.size()-1);
      slip::iota(histo_step_a.begin(),histo_step_a.end(),min_a,step_a);
      //computes the histogram
      slip::Array<int> histo_a(histo_step_a.size()-1);
      slip::real_histogram(Angle.begin(),Angle.end(),
			   histo_step_a.begin(),
			   histo_step_a.end(),histo_a.begin());

      std::cout<<"mean_a        = "<<mean_a<<std::endl;
      std::cout<<"std dev n     = "<<std_a<<std::endl;
      std::cout<<"[min_a,median_a,max_a] = "<<"["<<min_a<<","<<median_a<<","<<max_a<<"]"<<std::endl;
      std::cout<<"rms_a         = "<<rms_a<<std::endl;
      std::cout<<"histo_a ="<<std::endl;
      std::cout<<histo_a<<std::endl;

      //-----------------------------------------------------------------
      //computes some statistical informations on the first component u
      //of the vector field
      //-----------------------------------------------------------------
      double mean_u = slip::mean<double>(VF.begin(0),VF.end(0));
      double std_u  = slip::std_dev<double>(VF.begin(0),VF.end(0),mean_u);
      double min_u  = *std::min_element(VF.begin(0),VF.end(0));
      double median_u = slip::median_from_data_n(VF.begin(0),VF.end(0),VF.size());
      double max_u  = *std::max_element(VF.begin(0),VF.end(0));
      double rms_u  = slip::rms<double>(VF.begin(0),VF.end(0));
      //create the steps array [min_u, min_u + step_u,...,max_u]
      slip::Array<double> histo_step_u(9);
      double step_u = (max_u-min_u)/double(histo_step_u.size()-1);
      slip::iota(histo_step_u.begin(),histo_step_u.end(),min_u,step_u);
      //computes the histogram
      slip::Array<int> histo_u(histo_step_u.size()-1);
      slip::real_histogram(VF.begin(0),VF.end(0),
			   histo_step_u.begin(),
			   histo_step_u.end(),histo_u.begin());

      std::cout<<"mean_u        = "<<mean_u<<std::endl;
      //std::cout<<"rms_u = "<<rms_u<<std::endl;
      std::cout<<"std dev u     = "<<std_u<<std::endl;
      std::cout<<"[min_u,median_u,max_u] = "<<"["<<min_u<<","<<median_u<<","<<max_u<<"]"<<std::endl;
      std::cout<<"rms_u         = "<<rms_u<<std::endl;
      std::cout<<"histo_u ="<<std::endl;
      std::cout<<histo_u<<std::endl;
      //-----------------------------------------------------------------
      //computes some statistical informations on the second component v
      //of the vector field
      //-----------------------------------------------------------------
      double mean_v = slip::mean<double>(VF.begin(1),VF.end(1));
      double std_v = slip::std_dev<double>(VF.begin(1),VF.end(1),mean_v);
      double min_v = *std::min_element(VF.begin(1),VF.end(1));
      double median_v = slip::median_from_data_n(VF.begin(1),VF.end(1),VF.size());
      double max_v = *std::max_element(VF.begin(1),VF.end(1));
      double rms_v  = slip::rms<double>(VF.begin(1),VF.end(1));
      //create the steps array [min_v, min_v + step_v,...,max_v]
      slip::Array<double> histo_step_v(9);
      double step_v = (max_v-min_v)/double(histo_step_v.size()-1);
      slip::iota(histo_step_v.begin(),histo_step_v.end(),min_v,step_v);
      //computes the histogram
      slip::Array<int> histo_v(histo_step_v.size()-1);
      slip::real_histogram(VF.begin(1),VF.end(1),
			   histo_step_v.begin(),
			   histo_step_v.end(),histo_v.begin());

      std::cout<<"mean_v =        "<<mean_v<<std::endl;
      std::cout<<"std dev u =     "<<std_v<<std::endl;
      std::cout<<"[min_v,median_v,max_v] = "<<"["<<min_v<<","<<median_v<<","<<max_v<<"]"<<std::endl;
      std::cout<<"rms_v         = "<<rms_v<<std::endl;
      std::cout<<"histo_v ="<<std::endl;
      std::cout<<histo_v<<std::endl;
      
      //-----------------------------------------------------------------
      //compute the divergence with a scheme of order order
      //-----------------------------------------------------------------
      slip::GrayscaleImage<double> Div(rows,cols);
      VF.divergence(order,Div);
      
      //-----------------------------------------------------------------
      //compute the vorticity with a scheme of order order
      //-----------------------------------------------------------------
      slip::GrayscaleImage<double> Vort(rows,cols);
      VF.vorticity(order,Vort);

      //-----------------------------------------------------------------
      //compute the vorticity with a scheme of order order
      //-----------------------------------------------------------------
      slip::GrayscaleImage<double> Lambda2(rows,cols);
      VF.lambda2(order,Lambda2);
  

      //-----------------------------------------------------------------
      //Put the images between [0,1]
      //-----------------------------------------------------------------
      slip::change_dynamic_01(Norm.begin(),Norm.end(),Norm.begin(),slip::AFFINE_FUNCTION);
      slip::change_dynamic_01(Angle.begin(),Angle.end(),Angle.begin(),slip::AFFINE_FUNCTION);
      slip::change_dynamic_01(Div.begin(),Div.end(),Div.begin(),slip::SIGMOID_FUNCTION);
      slip::change_dynamic_01(Vort.begin(),Vort.end(),Vort.begin(),slip::SIGMOID_FUNCTION);
      slip::change_dynamic_01(Lambda2.begin(),Lambda2.end(),Lambda2.begin(),slip::SIGMOID_FUNCTION);

      //-----------------------------------------------------------------
      //Write the images 
      //-----------------------------------------------------------------
      Norm.write(args[6]);
      Angle.write(args[8]);
      Div.write(args[10]);
      Vort.write(args[12]);
      Lambda2.write(args[14]);
      
    }
  else
    {
      usage(args[0].c_str());
      exit(1);
    }
  return 0;
}


static void usage(const char* cmd)
{
  std::cout<<std::endl;
  std::cout<<"NAME:"<<std::endl;
  std::cout<<"\t"<<cmd<<" - Computes some statistics and differential operators on a regular vector field"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"SYNOPSIS:"<<std::endl;
  std::cout<<"\t"<<cmd<<" -i input_gnuplot_file -n derivative_scheme_order -o norm_image_file -o angle_image_file -o divergence_image_file -o vorticity_image_file -o lambda2_image_file"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"DESCRIPTION: Computes some statistics and differential operators on a regular vector field"<<std::endl;
  std::cout<<"\tThe finite difference derivatives scheme order is specify by the -n order option"<<std::endl;
  std::cout<<"\t "<<std::endl;
  std::cout<<std::endl;
  std::cout<<"\t -i input gnuplot_file"<<std::endl;
  std::cout<<"\t -n finite difference derivative scheme order of the divergence and the vorticity"<<std::endl;
  std::cout<<"\t -o norm output image file"<<std::endl;
  std::cout<<"\t -o angle output image file"<<std::endl;
  std::cout<<"\t -o divergence output image file"<<std::endl;
  std::cout<<"\t -o vorticity output image file"<<std::endl;
  std::cout<<"\t -o lambda2 output image file"<<std::endl;
  std::cout<<"\t -h, --help\t display this help and exit"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"AUTHOR:"<<std::endl;
  std::cout<<"\t Benoit Tremblais <tremblais@sic.univ-poitiers.fr>"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"REPORTING BUGS:"<<std::endl;
  std::cout<<"\t Report bugs to slip trac serveur"<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"COPYRIGHT:"<<std::endl;
  std::cout<<"\t This is free software; see the source for copying conditions."<<std::endl;
  std::cout<<std::endl; 
  std::cout<<"EXAMPLE: "<<std::endl;
  std::cout<<"./regularvector2dfield2d_op -i cylCarre_0150.gnu -n 2 -o norm.png -o angle.png -o div.png -o vort.png -o lambda2.png"<<std::endl;
  std::cout<<"\t "<<std::endl;
  std::cout<<std::endl;
  std::cout<<"SEE ALSO: "<<std::endl;
  std::cout<<std::endl;
  
}
