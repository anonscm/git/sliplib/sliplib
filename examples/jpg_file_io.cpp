/*!
 ** \file jpg_file_io.cpp
 ** \brief jpeg files io examples
 ** \version Fluex 1.0
 ** \date 2013/10/30
 ** \author  Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

//stl
#include <iostream>
//slip
#include "ColorImage.hpp"
#include "io_tools.hpp"
#include "JpegReader.hpp"
#include "JpegWriter.hpp"


/*!
 ** \brief usage :
 */
void usage(const char *name) {
	std::cout << "usage :\n\n";
	std::cout << "\t " << name << " input_jpg_color_image ";
	std::cout << std::endl;
}


int main(int argc, char* argv[])
{
	if (argc < 2) {
		usage(*argv);
		return 0;
	}
	std::string test_name(*argv);
	std::string filename(argv[1]);
	std::string ext;
	std::string output_base;
	slip::split_extension(filename,output_base,ext);

	//Read the color image
	slip::JpegReader<slip::ColorImage<double>,double,3,1> coul_jpgread(filename);
	slip::ColorImage<double> coul_image;
	coul_jpgread.read(coul_image);

	//Copy the first component to the others
	std::copy(coul_image.begin(0),coul_image.end(0),coul_image.begin(1));
	std::copy(coul_image.begin(0),coul_image.end(0),coul_image.begin(2));

	//Write the output image
	slip::JpegWriter<slip::ColorImage<double>,double,3> ImageCoulWriter(output_base + "_nvg" + ext,
			coul_image.width(),coul_image.height());
	ImageCoulWriter.write(coul_image);

	std::cout << output_base + "_nvg" + ext << " written.\n";
	std::cout << test_name << " successful executed.\n";
	return 0;
}


