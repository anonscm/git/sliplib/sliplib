
/** 
 * \file slip.hpp
 * 
 * \brief Include slip headers
 * \date 2014/03/15
 * \since 1.4.0
 */
#ifndef SLIP_SLIP_HPP
#define SLIP_SLIP_HPP
#include <slip/Range.hpp>
#include <slip/Array.hpp>
#include <slip/Array2d.hpp>
#include <slip/Array3d.hpp>
#include <slip/Array4d.hpp>
#include <slip/Block.hpp>
#include <slip/Block2d.hpp>
#include <slip/Block3d.hpp>
#include <slip/Block4d.hpp>
#include <slip/Point.hpp>
#include <slip/Point1d.hpp>
#include <slip/Point2d.hpp>
#include <slip/Point3d.hpp>
#include <slip/Point4d.hpp>
#include <slip/Box.hpp>
#include <slip/Box1d.hpp>
#include <slip/Box2d.hpp>
#include <slip/Box3d.hpp>
#include <slip/Box4d.hpp>
#include <slip/Color.hpp>
#include <slip/DPoint.hpp>
#include <slip/DPoint1d.hpp>
#include <slip/DPoint2d.hpp>
#include <slip/DPoint3d.hpp>
#include <slip/DPoint4d.hpp>
#include <slip/Vector.hpp>
#include <slip/Matrix.hpp>
#include <slip/Matrix3d.hpp>
#include <slip/Matrix4d.hpp>
#include <slip/KVector.hpp>
#include <slip/Vector2d.hpp>
#include <slip/Vector3d.hpp>
#include <slip/Vector4d.hpp>
#include <slip/CUFTree.hpp>
#include <slip/Signal.hpp>
#include <slip/GrayscaleImage.hpp>
#include <slip/Volume.hpp>
#include <slip/HyperVolume.hpp>
#include <slip/GenericMultiComponent2d.hpp>
#include <slip/GenericMultiComponent3d.hpp>
#include <slip/GenericMultiComponent4d.hpp>
#include <slip/DenseVector2dField2d.hpp>
#include <slip/DenseVector3dField2d.hpp>
#include <slip/DenseVector3dField3d.hpp>
#include <slip/RegularVector2dField2d.hpp>
#include <slip/RegularVector3dField3d.hpp>
#include <slip/ColorImage.hpp>
#include <slip/ColorVolume.hpp>
#include <slip/ColorHyperVolume.hpp>
#include <slip/MultispectralImage.hpp>
#include <slip/Polynomial.hpp>
#include <slip/MultivariatePolynomial.hpp>
#include <slip/CameraModel.hpp>
#include <slip/PinholeCamera.hpp>
#include <slip/PinholeDLTCamera.hpp>
#include <slip/PinholeFaugerasCamera.hpp>
#include <slip/DistortionCamera.hpp>
#include <slip/SoloffCamera.hpp>
#include <slip/Colormap.hpp>
#include <slip/Pyramid.hpp>
#include <slip/Statistics.hpp>

#endif //SLIP_SLIP_HPP
