/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



#ifndef LINEAR_ALGEBRA_TRAITS_HPP
#define LINEAR_ALGEBRA_TRAITS_HPP

#include <complex>
#include <iterator>
#include <limits>
#include "utils_compilation.hpp"
namespace slip
{

  struct __true_type { };
  struct __false_type { };

  // Real types
  //
  template<typename _Tp>
    struct __is_real
    {
      enum { __val = 0 };
      typedef __false_type __type;
    };

 template<>
 struct __is_real<float>
    {
      enum { __val = 1 };
      typedef __true_type __type;
    };

  template<>
 struct __is_real<double>
    {
      enum { __val = 1 };
      typedef __true_type __type;
    };

  template<>
  struct __is_real<long double>
    {
      enum { __val = 1 };
      typedef __true_type __type;
    };

   /**
   ** \brief Test if an element is real.
   ** \author Benoit Tremblais <tremblais@sic.univ-poitiers.fr> : conceptor
   ** \date 2009/02/21
   ** \since 1.0.0
   ** \param val. Value to evaluate.
   ** \return true if val is real, false else.
   ** \par Example:
   ** \code
   **  std::complex<float> c(2.0,1.0);
   **  float f = 4.0;
   **  std::cout<<"is_real("<<c<<") = "<<slip::is_real(c)<<std::endl;
   **  std::cout<<"is_real("<<f<<") = "<<slip::is_real(f)<<std::endl;
   ** \endcode
   */
  template<typename T>
  bool is_real(const T& UNUSED(val))
  {
    return slip::__is_real<T>::__val;
  }

  // Complex types
  //
  template<typename _Tp>
    struct __is_complex
    {
      enum { __val = 0 };
      typedef __false_type __type;
    };

 template<>
 struct __is_complex<std::complex<float> >
    {
      enum { __val = 1 };
      typedef __true_type __type;
    };

  template<>
    struct __is_complex<std::complex<double> >
    {
      enum { __val = 1 };
      typedef __true_type __type;
    };
 
  template<>
    struct __is_complex<std::complex<long double> >
    {
      enum { __val = 1 };
      typedef __true_type __type;
    };

  /**
   ** \brief Test if an element is complex.
   ** \author Benoit Tremblais <tremblais@sic.univ-poitiers.fr> : conceptor
   ** \date 2009/02/21
   ** \since 1.0.0
   ** \param val. Value to evaluate.
   ** \return true if val is complex. false else
   ** \par Example:
   ** \code
   **  std::complex<float> c(2.0,1.0);
   **  float f = 4.0;
   **  std::cout<<"is_complex("<<c<<") = "<<slip::is_complex(c)<<std::endl;
   **  std::cout<<"is_complex("<<f<<") = "<<slip::is_complex(f)<<std::endl;
   ** \endcode
   */
  template<typename T>
  bool is_complex(const T&  UNUSED(val))
  {
    return slip::__is_complex<T>::__val;
  }


 template<typename _Tp>
  struct lin_alg_traits
    {
      typedef _Tp value_type;
    };

 template<typename _Tp>
 struct lin_alg_traits<std::complex<_Tp> >
    {
      typedef typename std::complex<_Tp>::value_type value_type;
    };

  /**
   ** \brief Returns the epsilon value of a real or a complex.
   ** \author Benoit Tremblais <tremblais@sic.univ-poitiers.fr> : conceptor
   ** \date 2009/02/21
   ** \since 1.0.0
   ** \return The corresponding epsilon value.
   ** \par Example:
   ** \code
   **  std::cout<<"epsilon = "<<slip::epsilon<std::complex<float> >()<<std::endl;
   ** std::cout<<"epsilon = "<<slip::epsilon<double>()<<std::endl;
   ** \endcode
   ** 
   */
  template<typename T>
  typename slip::lin_alg_traits<T>::value_type epsilon()
  {
    return std::numeric_limits<typename slip::lin_alg_traits<T>::value_type>::epsilon();
  }


}//slip::

#endif //LINEAR_ALGEBRA_TRAITS_HPP
