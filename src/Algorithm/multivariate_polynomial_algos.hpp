#include <iostream>
#include <algorithm>
#include "MultivariatePolynomial.hpp"
#include "PolySupport.hpp"

#ifndef SLIP_MULTIVARIATE_POLYNOMIAL_ALGO_HPP
#define SLIP_MULTIVARIATE_POLYNOMIAL_ALGO_HPP

namespace slip
{

  /**
   **  \brief Computes the integral of a %slip::MultivariatePolynomial on the support \a lstIntervals.
   ** \param func Polynomial function.
   ** \param lstIntervals Supports of the polynomial function to integrate.
   ** \return the result of the integral.
   ** \author Martin Druon
   ** \todo to integrate in the class MultivariatePolynomial
   **/
template<class T, size_t DIM>
inline
T integral(const slip::MultivariatePolynomial<T, DIM>& func,
	   const slip::block<slip::PolySupport<T>, DIM>& lstIntervals)
{
  slip::MultivariatePolynomial<T, DIM> newPoly(func);

  for(size_t iDim = 0; iDim < DIM; ++iDim)
    {
      newPoly.integral(iDim + 1);
      newPoly = (newPoly.partial_evaluate(iDim+1, lstIntervals[iDim].max())
		 - newPoly.partial_evaluate(iDim+1, lstIntervals[iDim].min()));
    }


  static slip::Monomial<DIM> constant;
  std::fill(constant.powers.begin(),constant.powers.end(),0);
  
  return (newPoly.find(constant))->second;
}

template <typename T, std::size_t DIM>  
struct LegendreInnerProduct
{
  T operator()(const slip::MultivariatePolynomial<T, DIM>& P1,
	       const slip::MultivariatePolynomial<T, DIM>& P2)
  {
    slip::block<slip::PolySupport<T>, DIM> lstIntervals;
    std::fill(lstIntervals.begin(),lstIntervals.end(),slip::PolySupport<T>(static_cast<T>(-1.0),static_cast<T>(1.0)));
  
    return slip::integral(P1*P2,lstIntervals);
  }

};

template <typename T, std::size_t DIM>  
struct LegendreInnerProduct1d
{
  T operator()(const slip::MultivariatePolynomial<T, DIM>& P1,
	       const slip::MultivariatePolynomial<T, DIM>& P2)
  {
    slip::block<slip::PolySupport<T>, DIM> lstIntervals;
    std::fill(lstIntervals.begin(),lstIntervals.end(),slip::PolySupport<T>(static_cast<T>(-1.0),static_cast<T>(1.0)));
  
    return slip::LegendreInnerProduct<T,DIM>()(P1,P2) / (static_cast<T>(2.0)*static_cast<T>(DIM-1));
  }

};

}//::slip

#endif //SLIP_MULTIVARIATE_POLYNOMIAL_ALGO_HPP
