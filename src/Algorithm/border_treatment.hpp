/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file border_treatment.hpp
 * 
 * \brief Provides some algorithms to handle the border conditions of ranges.
 * 
 */
#ifndef SLIP_BORDER_TREATMENT_HPP
#define SLIP_BORDER_TREATMENT_HPP


#include "iterator_types.hpp"
#include "Box1d.hpp"
#include "Box2d.hpp"
#include "Box3d.hpp"
#include "Box4d.hpp"

#include <algorithm>
#include <iterator>
namespace slip
{

  /** 
    \enum BORDER_TREATMENT
    \brief Choose between different border treatment modes for convolution 
    algorithms 
    \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
    \date 2006/09/27
    \since 1.0.0
    \code
    enum BORDER_TREATMENT 
    {
    // do not operate on a point where the kernel does 
    // not fit in the signal
    BORDER_TREATMENT_AVOID, 

    // zero-padded 
    BORDER_TREATMENT_ZERO_PADDED,

    // repeat the nearest valid pixel
    BORDER_TREATMENT_NEUMANN,

    // periodize signal at last row/column 
    BORDER_TREATMENT_CIRCULAR,

    //reflect signal at last row/column
    BORDER_TREATMENT_REFLECT,

    //copy signal at last row/column
    BORDER_TREATMENT_COPY
    };
    \endcode
*/


enum BORDER_TREATMENT
{
   BORDER_TREATMENT_AVOID, 
   BORDER_TREATMENT_ZERO_PADDED, 
   BORDER_TREATMENT_NEUMANN,
   BORDER_TREATMENT_CIRCULAR,
   BORDER_TREATMENT_REFLECT,
   BORDER_TREATMENT_COPY
};


  //
  //fill border
  //
  //
template<typename>
struct __fill_border
{
  template <typename _II>
  static void
  fill_border(_II first, _II last, 
	      const typename std::iterator_traits<_II>::value_type& value, 
	      const std::size_t size)
  {}
};

template<>
struct __fill_border<std::random_access_iterator_tag>
{
  template <typename _II>
   static void
  fill_border(_II first, _II last,
	      const typename std::iterator_traits<_II>::value_type&  value, 
	      const std::size_t size)
  {
    std::fill_n(first,size,value);
    std::fill_n(std::reverse_iterator<_II>(last),
		size,value);
  }
};




template<>
struct __fill_border<std::random_access_iterator2d_tag>
{
  template <typename _II>
   static  void
  fill_border(_II first, _II last, 
	      const typename std::iterator_traits<_II>::value_type& value, 
	      const std::size_t size)
  {
    typedef typename  std::iterator_traits<_II>::difference_type _Difference;

  const _Difference size_nd = last - first;
  const std::size_t rows  = std::size_t(size_nd[0]);
  const std::size_t cols  = std::size_t(size_nd[1]);
  
  for(std::size_t i = 0; i < rows;++i)
    {
      __fill_border<std::random_access_iterator_tag>::fill_border(first.row_begin(i),first.row_end(i),value,size);
    }
 
  for(std::size_t j = 0; j < cols;++j)
    {
      __fill_border<std::random_access_iterator_tag>::fill_border(first.col_begin(j),first.col_end(j),value,size);
    }

   for(std::size_t i = 0; i < size;++i)
    {
      __fill_border<std::random_access_iterator_tag>::fill_border(first.row_begin(i),first.row_end(i),value,size);
      __fill_border<std::random_access_iterator_tag>::fill_border(first.row_begin(rows - size + i),first.row_end(rows - size + i),value,size);
    }
  }
};
template<>
struct __fill_border<std::random_access_iterator3d_tag>
{
  template <typename _II>
   static  void
  fill_border(_II first, _II last, 
	      const typename std::iterator_traits<_II>::value_type& value,
	      const std::size_t size)
  {
    typedef typename  std::iterator_traits<_II>::difference_type _Difference;

  const _Difference size_nd = last - first;
  const std::size_t slices  = std::size_t(size_nd[0]);
  const std::size_t rows    = std::size_t(size_nd[1]);
  const std::size_t cols    = std::size_t(size_nd[2]);
  
  for(std::size_t k = 0; k < slices;++k)
    {
      for(std::size_t i = 0; i < rows;++i)
	{
	  __fill_border<std::random_access_iterator_tag>::fill_border(first.row_begin(k,i),first.row_end(k,i),value,size);
	}
    }

  for(std::size_t k = 0; k < slices; ++k)
    {
      for(std::size_t j = 0; j < cols;++j)
	{
	  __fill_border<std::random_access_iterator_tag>::fill_border(first.col_begin(k,j),first.col_end(k,j),value,size);
	}
    }

  for(std::size_t i = 0; i < rows;++i)
    {
      for(std::size_t j = 0; j < cols;++j)
	{
	  __fill_border<std::random_access_iterator_tag>::fill_border(first.slice_begin(i,j),first.slice_end(i,j),value,size);
	}
    }
  
   for(std::size_t k = 0; k < size; ++k)
    {
      for(std::size_t i = 0; i < rows;++i)
	 {
	   __fill_border<std::random_access_iterator_tag>::fill_border(first.row_begin(k,i),first.row_end(k,i),value,size);
	   __fill_border<std::random_access_iterator_tag>::fill_border(first.row_begin(k+slices-size,i),first.row_end(k+slices-size,i),value,size);
	 }
       for(std::size_t j = 0; j < cols;++j)
	{
	  __fill_border<std::random_access_iterator_tag>::fill_border(first.col_begin(k,j),first.col_end(k,j),value,size);
	  __fill_border<std::random_access_iterator_tag>::fill_border(first.col_begin(k+slices-size,j),first.col_end(k+slices-size,j),value,size);

	}
     
    }
 
     
   for(std::size_t k = 0; k < slices; ++k)
      {
	for(std::size_t i = 0; i < size;++i)
	  {
	    __fill_border<std::random_access_iterator_tag>::fill_border(first.row_begin(k,i),first.row_end(k,i),value,size);
	    __fill_border<std::random_access_iterator_tag>::fill_border(first.row_begin(k, i + rows - size),first.row_end(k,i + rows - size),value,size);
	 }

      }
  }
};



/*!
 ** \brief fill border
 ** \version Fluex 1.0
 ** \date 2013/10/11
 ** \since 1.4.0 
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

template<>
struct __fill_border<std::random_access_iterator4d_tag>
{
	template <typename _II>
	static  void
	fill_border(_II first, _II last,
			const typename std::iterator_traits<_II>::value_type& value,
			const std::size_t size)
	{
	  typedef typename  std::iterator_traits<_II>::difference_type _Difference;
	  
	  const _Difference size_nd = last - first;
	  const std::size_t slabs  = std::size_t(size_nd[0]);
	  const std::size_t slices  = std::size_t(size_nd[1]);
	  const std::size_t rows    = std::size_t(size_nd[2]);
	  const std::size_t cols    = std::size_t(size_nd[3]);

		for(std::size_t t = 0; t < slabs;++t)
		{
			for(std::size_t k = 0; k < slices;++k)
			{
				for(std::size_t i = 0; i < rows;++i)
				{
					__fill_border<std::random_access_iterator_tag>::fill_border(first.row_begin(t,k,i),
							first.row_end(t,k,i),value,size);
				}
			}
		}

		for(std::size_t t = 0; t < slabs;++t)
		{
			for(std::size_t k = 0; k < slices; ++k)
			{
				for(std::size_t j = 0; j < cols;++j)
				{
					__fill_border<std::random_access_iterator_tag>::fill_border(first.col_begin(t,k,j),
							first.col_end(t,k,j),value,size);
				}
			}
		}

		for(std::size_t t = 0; t < slabs;++t)
		{
			for(std::size_t i = 0; i < rows;++i)
			{
				for(std::size_t j = 0; j < cols;++j)
				{
					__fill_border<std::random_access_iterator_tag>::fill_border(first.slice_begin(t,i,j),
							first.slice_end(t,i,j),value,size);
				}
			}
		}

		for(std::size_t k = 0; k < slices;++k)
		{
			for(std::size_t i = 0; i < rows;++i)
			{
				for(std::size_t j = 0; j < cols;++j)
				{
					__fill_border<std::random_access_iterator_tag>::fill_border(first.slab_begin(k,i,j),
							first.slab_end(k,i,j),value,size);
				}
			}
		}
	}
};


 /**
   ** \struct FillBorder
   ** \brief Generic fill border functor: fill border of 1d, 2d or
   ** 3d iterator ranges.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \version 0.0.1
   ** \since 1.0.0
   ** \par Example:
   ** \code
   ** //read and image
   ** slip::GrayscaleImage<double> I;
   ** I.read("lena.png");
   ** std::size_t nb_row = I.rows();
   ** std::size_t nb_col = I.cols();
   ** std::size_t b_size = 1;
   ** //Initialize AddBorder parameters
   ** AddBorder b(slip::BORDER_TREATMENT_NEUMANN,b_size);
   ** //Initialize FillBorder parameters
   ** FillBorder fill(slip::BORDER_TREATMENT_NEUMANN,b_size);
   ** //create an enhanced image
   ** slip::GrayscaleImage<double> tmp(nb_row+(2*b_size),nb_col+(2*b_size));
   ** //Adds border  
   ** b(I.upper_left(),I.bottom_right(),tmp.upper_left());
   ** for(int n = 0; n < 10; ++n)
   ** {
   **   fill(tmp.upper_left(),tmp.bottom_right());
   ** }
   ** \endcode
   */
struct FillBorder 
{
  /** 
   ** \brief Initializes the paramaters of the FillBorder functor.
   ** The size of border size_ is egal to 1
   */ 
  FillBorder():
    size_(1)
  {}
  /** \brief Initializes the paramaters of the FillBorder functor.
   ** \param size Size of the border.
   */  
  FillBorder(const std::size_t size):
    size_(size)
  {}
   
  /** \brief Fills the border of the range [first,last) according to the parameters of the functor.
   ** \param first RandomAccessIterator or RandomAccessIterator2d or RandomAccessIterator3d.
   ** \param last RandomAccessIterator or RandomAccessIterator2d or
   ** RandomAccessIterator3d.
   ** \param value The value to fill the range.
   ** \pre first, last must be of the same iterator_category.
   */
  template<typename _II>
  void operator() (_II first, _II last, 
		   const typename std::iterator_traits<_II>::value_type& value =
		   typename std::iterator_traits<_II>::value_type())
  {
    typedef typename std::iterator_traits<_II>::iterator_category _Category;
    __fill_border<_Category>::fill_border(first,last,value,size_);
  }
  
  std::size_t size_;

};

 /** \brief Fills the border of the range [first,last) with value.
   ** \since 1.0.0
   ** \param first RandomAccessIterator or RandomAccessIterator2d or RandomAccessIterator3d.
   ** \param last RandomAccessIterator or RandomAccessIterator2d or
   ** RandomAccessIterator3d.
   ** \param b_size Size of the border (1 by default).
   ** \param value The value to fill the range (0 by default).
   ** \pre first, last must be of the same iterator_category.
   ** \pre (last-first) >= 2 *size
   ** \code
   ** //The input grayscale image
   ** slip::GrayscaleImage<double> I2(4,5);
   ** slip::iota(I2.begin(),I2.end(),1.0);
   ** //The enlarged grayscale image
   ** slip::GrayscaleImage<double> tmp2(I2.rows()+(2*b_size),
   **					I2.cols()+(2*b_size));
   ** //add border to I2 in tmp2 with slip::BORDER_TREATMENT_NEUMANN conditions
   ** //and a border size of 2
   ** std::size_t b_size = 2;
   ** slip::add_border(I2.upper_left(),I2.bottom_right(),
   **		       tmp2.upper_left(),tmp2.bottom_right(),
   **	               slip::BORDER_TREATMENT_NEUMANN,b_size);
   ** //fill border of tmp2 with 4
   **  slip::fill_border(tmp2.upper_left(),tmp2.bottom_right(),
   **                    b_size,double(4));
   ** //update border with slip::BORDER_TREATMENT_NEUMANN condition
   ** slip::update_border(tmp.begin(),tmp.end(),
   **      		  slip::BORDER_TREATMENT_NEUMANN,b_size);
   ** \endcode
   */
  template <typename _II>
  void fill_border(_II first,_II last, 
		   std::size_t b_size = 1,
		   const typename std::iterator_traits<_II>::value_type& value =
		   typename std::iterator_traits<_II>::value_type())
  {
    assert((last-first) >= 2*b_size);
    typedef typename std::iterator_traits<_II>::iterator_category _Category;
    __fill_border<_Category>::fill_border(first,last,value,b_size);
  }

//
// update border
//
// 
template<typename>
struct __update_border
{
  template <typename _II>
  static void
  update_border(_II first, _II last, 
		const slip::BORDER_TREATMENT border, 
		const std::size_t size = 1)
  {}
};

template<>
struct __update_border<std::random_access_iterator_tag>
{
  template <typename _II>
   static void
  update_border(_II first, _II last,
		const slip::BORDER_TREATMENT border, 
		const std::size_t size = 1)
  {
    typedef typename std::iterator_traits<_II>::value_type value_type;
   
    switch(border)
    {
    case slip::BORDER_TREATMENT_AVOID: break;
    case slip::BORDER_TREATMENT_ZERO_PADDED:
      {
    	std::fill_n(first,size,value_type());
    	std::fill_n(std::reverse_iterator<_II>(last),
    		    size,value_type());
      }
      break;
    case slip::BORDER_TREATMENT_NEUMANN:
      {
    	std::copy(first+size,first+(size+size),std::reverse_iterator<_II>(first+size));
	std::copy(last-(size+size),
    		  last - size,
		  std::reverse_iterator<_II>(last));
      }
      break;
    case slip::BORDER_TREATMENT_CIRCULAR:
      {
    	std::copy(first+size,first+(size+size),last - size);
    	std::copy(std::reverse_iterator<_II>(last-size),
    		  std::reverse_iterator<_II>(last-(size + size)),
    		  std::reverse_iterator<_II>(first+size));
		  
      }
      break;
       case slip::BORDER_TREATMENT_REFLECT:
      {
    	std::copy(first+size,first+(size+size),std::reverse_iterator<_II>(first+size));
	std::copy(last-(size+size),
    		  last - size,
		  std::reverse_iterator<_II>(last));
      }
      break;
    case slip::BORDER_TREATMENT_COPY:
      {
	value_type left  = value_type(*(first + size));
    	value_type right = value_type(*(last - size - 1));
    	std::fill_n(first,size,left);
    	std::fill_n(std::reverse_iterator<_II>(last),
    		    size,right);
      }
      break;
    default:
      {
    	std::fill_n(first,size,value_type());
    	std::fill_n(std::reverse_iterator<_II>(last-size),
    		    size,value_type());
      }
    };
  }
};




template<>
struct __update_border<std::random_access_iterator2d_tag>
{
  template <typename _II>
   static  void
  update_border(_II first, _II last, 
		const slip::BORDER_TREATMENT border, 
		const std::size_t size = 1)
  {

    typedef typename  std::iterator_traits<_II>::difference_type _Difference;

    _Difference size_nd = last - first;
  const  std::size_t rows  = std::size_t(size_nd[0]);
  const std::size_t cols  = std::size_t(size_nd[1]);
  
  for(std::size_t i = 0; i < rows;++i)
    {
      __update_border<std::random_access_iterator_tag>::update_border(first.row_begin(i),first.row_end(i),border,size);
    }
 
  for(std::size_t j = 0; j < cols;++j)
    {
      __update_border<std::random_access_iterator_tag>::update_border(first.col_begin(j),first.col_end(j),border,size);
    }

   for(std::size_t i = 0; i < size;++i)
    {
      __update_border<std::random_access_iterator_tag>::update_border(first.row_begin(i),first.row_end(i),border,size);
      __update_border<std::random_access_iterator_tag>::update_border(first.row_begin(rows - size + i),first.row_end(rows - size + i),border,size);
    }
  }
};
template<>
struct __update_border<std::random_access_iterator3d_tag>
{
  template <typename _II>
   static  void
  update_border(_II first, _II last, 
		const slip::BORDER_TREATMENT border, 
		const std::size_t size = 1)
  {

    typedef typename  std::iterator_traits<_II>::difference_type _Difference;

  const _Difference size_nd = last - first;
  const std::size_t slices  = std::size_t(size_nd[0]);
  const std::size_t rows    = std::size_t(size_nd[1]);
  const std::size_t cols    = std::size_t(size_nd[2]);
  
  for(std::size_t k = 0; k < slices;++k)
    {
      for(std::size_t i = 0; i < rows;++i)
	{
	  __update_border<std::random_access_iterator_tag>::update_border(first.row_begin(k,i),first.row_end(k,i),border,size);
	}
    }

  for(std::size_t k = 0; k < slices; ++k)
    {
      for(std::size_t j = 0; j < cols;++j)
	{
	  __update_border<std::random_access_iterator_tag>::update_border(first.col_begin(k,j),first.col_end(k,j),border,size);
	}
    }

  for(std::size_t i = 0; i < rows;++i)
    {
      for(std::size_t j = 0; j < cols;++j)
	{
	  __update_border<std::random_access_iterator_tag>::update_border(first.slice_begin(i,j),first.slice_end(i,j),border,size);
	}
    }
  
   for(std::size_t k = 0; k < size; ++k)
    {
      for(std::size_t i = 0; i < rows;++i)
	 {
	   __update_border<std::random_access_iterator_tag>::update_border(first.row_begin(k,i),first.row_end(k,i),border,size);
	   __update_border<std::random_access_iterator_tag>::update_border(first.row_begin(k+slices-size,i),first.row_end(k+slices-size,i),border,size);
	 }
       for(std::size_t j = 0; j < cols;++j)
	{
	  __update_border<std::random_access_iterator_tag>::update_border(first.col_begin(k,j),first.col_end(k,j),border,size);
	  __update_border<std::random_access_iterator_tag>::update_border(first.col_begin(k+slices-size,j),first.col_end(k+slices-size,j),border,size);

	}
     
    }
 
     
   for(std::size_t k = 0; k < slices; ++k)
      {
	for(std::size_t i = 0; i < size;++i)
	  {
	    __update_border<std::random_access_iterator_tag>::update_border(first.row_begin(k,i),first.row_end(k,i),border,size);
	    __update_border<std::random_access_iterator_tag>::update_border(first.row_begin(k, i + rows - size),first.row_end(k,i + rows - size),border,size);
	 }

      }
  }
};

/*!
 ** \brief update border
 ** \version Fluex 1.0
 ** \date 2013/10/11
 ** \since 1.4.0
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

template<>
struct __update_border<std::random_access_iterator4d_tag>
{
	template <typename _II>
	static  void
	update_border(_II first, _II last,
			const slip::BORDER_TREATMENT border,
			const std::size_t size = 1)
	{
	  typedef typename  std::iterator_traits<_II>::difference_type _Difference;

	  const _Difference size_nd = last - first;
	  const std::size_t slabs  = std::size_t(size_nd[0]);
	  const std::size_t slices  = std::size_t(size_nd[1]);
	  const std::size_t rows    = std::size_t(size_nd[2]);
	  const std::size_t cols    = std::size_t(size_nd[3]);

		for(std::size_t t = 0; t < slabs;++t)
		{
			for(std::size_t k = 0; k < slices;++k)
			{
				for(std::size_t i = 0; i < rows;++i)
				{
					__update_border<std::random_access_iterator_tag>::update_border(first.row_begin(t,k,i),
							first.row_end(t,k,i),border,size);
				}
			}
		}

		for(std::size_t t = 0; t < slabs;++t)
		{
			for(std::size_t k = 0; k < slices; ++k)
			{
				for(std::size_t j = 0; j < cols;++j)
				{
					__update_border<std::random_access_iterator_tag>::update_border(first.col_begin(t,k,j),
							first.col_end(t,k,j),border,size);
				}
			}
		}

		for(std::size_t t = 0; t < slabs;++t)
		{
			for(std::size_t i = 0; i < rows;++i)
			{
				for(std::size_t j = 0; j < cols;++j)
				{
					__update_border<std::random_access_iterator_tag>::update_border(first.slice_begin(t,i,j),
							first.slice_end(t,i,j),border,size);
				}
			}
		}

		for(std::size_t k = 0; k < slices;++k)
		{
			for(std::size_t i = 0; i < rows;++i)
			{
				for(std::size_t j = 0; j < cols;++j)
				{
					__update_border<std::random_access_iterator_tag>::update_border(first.slab_begin(k,i,j),
							first.slab_end(k,i,j),border,size);
				}
			}
		}
	}
};

  /**
   ** \struct UpdateBorder
   ** \brief Generic update border functor: update border of 1d, 2d or
   ** 3d iterator ranges.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \version 0.0.1
    ** \since 1.0.0
   ** \par Example:
   ** \code
   ** //read and image
   ** slip::GrayscaleImage<double> I;
   ** I.read("lena.png");
   ** std::size_t nb_row = I.rows();
   ** std::size_t nb_col = I.cols();
   ** std::size_t b_size = 1;
   ** //Initialize AddBorder parameters
   ** AddBorder b(slip::BORDER_TREATMENT_NEUMANN,b_size);
   ** //Initialize UpdateBorder parameters
   ** UpdateBorder update(slip::BORDER_TREATMENT_NEUMANN,b_size);
   ** //create an enhanced image
   ** slip::GrayscaleImage<double> tmp(nb_row+(2*b_size),nb_col+(2*b_size));
   ** //Adds border  
   ** b(I.upper_left(),I.bottom_right(),tmp.upper_left());
   ** for(int n = 0; n < 10; ++n)
   ** {
   **   update(tmp.upper_left(),tmp.bottom_right());
   ** }
   ** \endcode
   */
struct UpdateBorder 
{
  /** \brief Initializes the paramaters of the UpdateBorder functor.
   ** \param border border condition.
   ** \param size Size of the border (1 by default).
   */  UpdateBorder(const slip::BORDER_TREATMENT border,const std::size_t size = 1):
    border_(border),size_(size)
  {}
   
  /** \brief Updates the border of the range [first,last) according to the parameters of the functor.
   ** \since 1.0.0
   ** \param first RandomAccessIterator or RandomAccessIterator2d or RandomAccessIterator3d.
   ** \param last RandomAccessIterator or RandomAccessIterator2d or
   ** RandomAccessIterator3d.
   ** \pre first, last must be of the same iterator_category.
   */
  template<typename _II>
  void operator() (_II first, _II last)
  {
    typedef typename std::iterator_traits<_II>::iterator_category _Category;
    return __update_border<_Category>::update_border(first,last,border_,size_);
  }
  
  slip::BORDER_TREATMENT border_;
  std::size_t size_;

};


 /** \brief Updates the border of the range [first,last) according to b_size and
border condition: border.
   ** \since 1.0.0
   ** \param first RandomAccessIterator or RandomAccessIterator2d or RandomAccessIterator3d.
   ** \param last RandomAccessIterator or RandomAccessIterator2d or
   ** RandomAccessIterator3d.
   ** \param border slip::BORDER_TREATMENT border condition.
   ** \param b_size Size of the border (1 by default).
   ** \pre first, last must be of the same iterator_category.
   ** \par Example:
   ** \code
   ** //The input grayscale image
   ** slip::GrayscaleImage<double> I2(4,5);
   ** slip::iota(I2.begin(),I2.end(),1.0);
   ** //The enlarged grayscale image
   ** slip::GrayscaleImage<double> tmp2(I2.rows()+(2*b_size),
   **					I2.cols()+(2*b_size));
   ** //add border to I2 in tmp2 with slip::BORDER_TREATMENT_NEUMANN conditions
   ** //and a border size of 2
   ** std::size_t b_size = 2;
   ** slip::add_border(I2.upper_left(),I2.bottom_right(),
   **		       tmp2.upper_left(),tmp2.bottom_right(),
   **	               slip::BORDER_TREATMENT_NEUMANN,b_size);
   ** //fill border of tmp2 with 4
   **  slip::fill_border(tmp2.upper_left(),tmp2.bottom_right(),
   **                    b_size,double(4));
   ** //update border with slip::BORDER_TREATMENT_NEUMANN condition
   ** slip::update_border(tmp.begin(),tmp.end(),
   **      		  slip::BORDER_TREATMENT_NEUMANN,b_size);
   ** \endcode
   */
template <typename _II>
void update_border(_II first, _II last, 
		   const slip::BORDER_TREATMENT border,
		   const std::size_t b_size = 1)
{
   typedef typename std::iterator_traits<_II>::iterator_category _Category;
   return __update_border<_Category>::update_border(first,last,border,b_size);
}



template<typename>
struct __add_border
{
  template <typename _II, typename _OI>
  static void
  add_border(_II first, _II last, 
	     const slip::BORDER_TREATMENT border, 
	     _OI out_first, const std::size_t size = 1)
  {}
};

template<>
struct __add_border<std::random_access_iterator_tag>
{
  template <typename _II, typename _OI>
   static void
  add_border(_II first, _II last, 
	     const slip::BORDER_TREATMENT border, 
	     _OI out_first, const std::size_t size = 1)
  {
    typedef typename std::iterator_traits<_II>::iterator_category _Category;
   
    std::copy(first,last, out_first + size);
    
    __update_border<_Category>::update_border(out_first,
    					      out_first+(last-first)+(2*size),
    					      border,
    					      size);
  }
};

template<>
struct __add_border<std::random_access_iterator2d_tag>
{
  template <typename _II, typename _OI>
   static  void
  add_border(_II first, _II last, 
	     const slip::BORDER_TREATMENT border, 
	     _OI out_first, const std::size_t size = 1)
  {
    typedef typename  std::iterator_traits<_II>::difference_type _Difference;

  const _Difference size_nd = last - first;
  const std::size_t rows  = std::size_t(size_nd[0]);
  const std::size_t cols  = std::size_t(size_nd[1]);
  
  for(std::size_t i = 0; i < rows;++i)
    {
      std::copy(first.row_begin(i),first.row_end(i),out_first.row_begin(i+size) + size);
      __add_border<std::random_access_iterator_tag>::add_border(first.row_begin(i),first.row_end(i),border,out_first.row_begin(i+size),size);
    }
 
  for(std::size_t j = 0; j < cols;++j)
    {
      __add_border<std::random_access_iterator_tag>::add_border(first.col_begin(j),first.col_end(j),border,out_first.col_begin(j+size),size);
    }

   for(std::size_t i = 0; i < size;++i)
    {
      __add_border<std::random_access_iterator_tag>::add_border(out_first.row_begin(i)+size,out_first.row_end(i)-size,border,out_first.row_begin(i),size);
      __add_border<std::random_access_iterator_tag>::add_border(out_first.row_begin(i + (rows + size))+size,out_first.row_end(i + (rows + size))-size,border,out_first.row_begin(i + (rows + size)),size);
    }
  }
};

template<>
struct __add_border<std::random_access_iterator3d_tag>
{
  template <typename _II, typename _OI>
   static  void
  add_border(_II first, _II last, 
	     const slip::BORDER_TREATMENT border, 
	     _OI out_first, const std::size_t size = 1)
  {
   
  typedef typename  std::iterator_traits<_II>::difference_type _Difference;

  const _Difference size_nd = last - first;
  const std::size_t slices  = std::size_t(size_nd[0]);
  const std::size_t rows    = std::size_t(size_nd[1]);
  const std::size_t cols    = std::size_t(size_nd[2]);
  
  for(std::size_t k = 0; k < slices;++k)
    {
      for(std::size_t i = 0; i < rows;++i)
	{
      
	  std::copy(first.row_begin(k,i),first.row_end(k,i),out_first.row_begin(k+size,i+size) + size);
	  __add_border<std::random_access_iterator_tag>::add_border(first.row_begin(k,i),first.row_end(k,i),border,out_first.row_begin(k+size,i+size),size);
	}
    }

  for(std::size_t k = 0; k < slices; ++k)
    {
      for(std::size_t j = 0; j < cols;++j)
	{
	  __add_border<std::random_access_iterator_tag>::add_border(first.col_begin(k,j),first.col_end(k,j),border,out_first.col_begin(k+size,j+size),size);
	}
    }

  for(std::size_t i = 0; i < rows;++i)
    {
      for(std::size_t j = 0; j < cols;++j)
	{
	  __add_border<std::random_access_iterator_tag>::add_border(first.slice_begin(i,j),first.slice_end(i,j),border,out_first.slice_begin(i+size,j+size),size);
	}
    }
  
   for(std::size_t k = 0; k < size; ++k)
    {
      for(std::size_t i = 0; i < rows;++i)
	 {
	   __add_border<std::random_access_iterator_tag>::add_border(out_first.row_begin(k,i+size)+size,out_first.row_end(k,i+size)-size,border,out_first.row_begin(k,i+size),size);
	   __add_border<std::random_access_iterator_tag>::add_border(out_first.row_begin(k+(slices+size),i+size)+size,out_first.row_end(k+(slices+size),i+size)-size,border,out_first.row_begin(k+(slices+size),i+size),size);
	 }
       for(std::size_t j = 0; j < cols;++j)
	{
	  __add_border<std::random_access_iterator_tag>::add_border(out_first.col_begin(k,j+size)+size,out_first.col_end(k,j+size)-size,border,out_first.col_begin(k,j+size),size);
	  __add_border<std::random_access_iterator_tag>::add_border(out_first.col_begin(k+(slices+size),j+size)+size,out_first.col_end(k+(slices+size),j+size)-size,border,out_first.col_begin(k+(slices+size),j+size),size);

	}
     
    }
 
     
   for(std::size_t k = 0; k < (slices + 2*size); ++k)
      {
	for(std::size_t i = 0; i < size;++i)
	  {
	    __add_border<std::random_access_iterator_tag>::add_border(out_first.row_begin(k,i)+size,out_first.row_end(k,i)-size,border,out_first.row_begin(k,i),size);
	    __add_border<std::random_access_iterator_tag>::add_border(out_first.row_begin(k, i + (rows + size))+size,out_first.row_end(k,i + (rows + size))-size,border,out_first.row_begin(k,i + (rows + size)),size);
	 }

      }
  }
};


/*!
 ** \brief add border
 ** \version Fluex 1.0
 ** \date 2013/10/11
 ** \since 1.4.0
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */
template<>
struct __add_border<std::random_access_iterator4d_tag>
{
	template <typename _II, typename _OI>
	static  void
	add_border(_II first, _II last,
			const slip::BORDER_TREATMENT border,
			_OI out_first, const std::size_t size = 1)
	{
	
		typedef typename  std::iterator_traits<_II>::difference_type _Difference;

		const _Difference size_nd = last - first;
		const std::size_t slabs  = std::size_t(size_nd[0]);
		const std::size_t slices  = std::size_t(size_nd[1]);
		const std::size_t rows    = std::size_t(size_nd[2]);
		const std::size_t cols    = std::size_t(size_nd[3]);

		for(std::size_t t = 0; t < slabs; ++t)
		{
			for(std::size_t k = 0; k < slices;++k)
			{
				for(std::size_t i = 0; i < rows;++i)
				{

					std::copy(first.row_begin(t,k,i),first.row_end(t,k,i),out_first.row_begin(t+size,k+size,i+size) + size);
					__add_border<std::random_access_iterator_tag>::add_border(first.row_begin(t,k,i),first.row_end(t,k,i),
							border,out_first.row_begin(t+size,k+size,i+size),size);
				}
			}
		}

		for(std::size_t t = 0; t < slabs; ++t)
		{
			for(std::size_t k = 0; k < slices; ++k)
			{
				for(std::size_t j = 0; j < cols;++j)
				{
					__add_border<std::random_access_iterator_tag>::add_border(first.col_begin(t,k,j),
							first.col_end(t,k,j),border,out_first.col_begin(t+size,k+size,j+size),size);
				}
			}
		}

		for(std::size_t t = 0; t < slabs; ++t)
		{
			for(std::size_t i = 0; i < rows;++i)
			{
				for(std::size_t j = 0; j < cols;++j)
				{
					__add_border<std::random_access_iterator_tag>::add_border(first.slice_begin(t,i,j),
							first.slice_end(t,i,j),border,out_first.slice_begin(t+size,i+size,j+size),size);
				}
			}
		}

		for(std::size_t k = 0; k < slices; ++k)
		{
			for(std::size_t i = 0; i < rows;++i)
			{
				for(std::size_t j = 0; j < cols;++j)
				{
					__add_border<std::random_access_iterator_tag>::add_border(first.slab_begin(k,i,j),
							first.slab_end(k,i,j),border,out_first.slab_begin(k+size,i+size,j+size),size);
				}
			}
		}

		for(std::size_t t = 0; t < size; ++t)
		{
			for(std::size_t k = 0; k < slices; ++k)
			{
				for(std::size_t i = 0; i < rows;++i)
				{
					__add_border<std::random_access_iterator_tag>::add_border(out_first.row_begin(t,k+size,i+size)+size,
							out_first.row_end(t,k+size,i+size)-size,border,out_first.row_begin(t,k+size,i+size),size);
					__add_border<std::random_access_iterator_tag>::add_border(
							out_first.row_begin(t+(slabs+size),k+size,i+size)+size,
							out_first.row_end(t+(slabs+size),k+size,i+size)-size,border,
							out_first.row_begin(t+(slabs+size),k+size,i+size),size);
				}
			}
			for(std::size_t k = 0; k < slices; ++k)
			{
				for(std::size_t j = 0; j < cols;++j)
				{
					__add_border<std::random_access_iterator_tag>::add_border(out_first.col_begin(t,k+size,j+size)+size,
							out_first.col_end(t,k+size,j+size)-size,border,out_first.col_begin(t,k+size,j+size),size);
					__add_border<std::random_access_iterator_tag>::add_border(
							out_first.col_begin(t+(slabs+size),k+size,j+size)+size,
							out_first.col_end(t+(slabs+size),k+size,j+size)-size,border,
							out_first.col_begin(t+(slabs+size),k+size,j+size),size);

				}
			}
			for(std::size_t i = 0; i < rows;++i)
			{
				for(std::size_t j = 0; j < cols;++j)
				{
					__add_border<std::random_access_iterator_tag>::add_border(out_first.slice_begin(t,i+size,j+size)+size,
							out_first.slice_end(t,i+size,j+size)-size,border,out_first.slice_begin(t,i+size,j+size),size);
					__add_border<std::random_access_iterator_tag>::add_border(
							out_first.slice_begin(t+(slabs+size),i+size,j+size)+size,
							out_first.slice_end(t+(slabs+size),i+size,j+size)-size,border,
							out_first.slice_begin(t+(slabs+size),i+size,j+size),size);

				}
			}
		}
		for(std::size_t t = 0; t < (slabs + 2*size); ++t)
		{
			for(std::size_t k = 0; k < size; ++k)
			{
				for(std::size_t i = 0; i < rows;++i)
				{
					__add_border<std::random_access_iterator_tag>::add_border(out_first.row_begin(t,k,i+size)+size,
							out_first.row_end(t,k,i+size)-size,border,out_first.row_begin(t,k,i+size),size);
					__add_border<std::random_access_iterator_tag>::add_border(out_first.row_begin(t,k+(slices+size),i+size)+size,
							out_first.row_end(t,k+(slices+size),i+size)-size,border,out_first.row_begin(t,k+(slices+size),
									i+size),size);
				}
				for(std::size_t j = 0; j < cols;++j)
				{
					__add_border<std::random_access_iterator_tag>::add_border(out_first.col_begin(t,k,j+size)+size,
							out_first.col_end(t,k,j+size)-size,border,out_first.col_begin(t,k,j+size),size);
					__add_border<std::random_access_iterator_tag>::add_border(out_first.col_begin(t,k+(slices+size),j+size)+size,
							out_first.col_end(t,k+(slices+size),j+size)-size,border,out_first.col_begin(t,k+(slices+size),
									j+size),size);
				}
			}

			for(std::size_t k = 0; k < (slices + 2*size); ++k)
			{
				for(std::size_t i = 0; i < size;++i)
				{
					__add_border<std::random_access_iterator_tag>::add_border(out_first.row_begin(t,k,i)+size,
							out_first.row_end(t,k,i)-size,border,out_first.row_begin(t,k,i),size);
					__add_border<std::random_access_iterator_tag>::add_border(
							out_first.row_begin(t,k, i + (rows + size))+size,
							out_first.row_end(t,k,i + (rows + size))-size,border,
							out_first.row_begin(t,k,i + (rows + size)),size);
				}
			}
		}
	}
};


/**
 ** \struct AddBorder
 ** \brief Generic add border functor: adds border of 1d, 2d or
 ** 3d iterator ranges.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.1
 ** \since 1.0.0
 ** \todo Compact code using update border
 ** \par Example:
 ** \code
 ** //read and image
 ** slip::GrayscaleImage<double> I;
 ** I.read("lena.png");
 ** std::size_t nb_row = I.rows();
 ** std::size_t nb_col = I.cols();
 ** std::size_t b_size = 1;
 ** //Initialize AddBorder parameters
 ** AddBorder b(slip::BORDER_TREATMENT_NEUMANN,b_size);
 ** //create an enhanced image
 ** slip::GrayscaleImage<double> tmp(nb_row+(2*b_size),nb_col+(2*b_size));
 ** //Adds border  
 ** b(I.upper_left(),I.bottom_right(),tmp.upper_left());
 **
 ** \endcode
 */
struct AddBorder 
{
  /** \brief Initializes the paramaters of the AddBorder functor.
   ** \param border Border condition.
   ** \param size Size of the border (1 by default).
   */
  AddBorder(const slip::BORDER_TREATMENT border,const std::size_t size = 1):
    border_(border),size_(size)
  {}
  /** \brief Adds the border of the range [first,last) according to the parameters of the functor.
   ** \since 1.0.0
   ** \param first RandomAccessIterator or RandomAccessIterator2d or RandomAccessIterator3d.
   ** \param last RandomAccessIterator or RandomAccessIterator2d or
   ** RandomAccessIterator3d.
   ** \param out_first RandomAccessIterator or RandomAccessIterator2d or
   ** RandomAccessIterator3d.
   ** \pre first, last and out_first must be of the same iterator_category.
   */
  template<typename _II, typename _OI>
  void operator() (_II first, _II last,_OI out_first)
  {
    typedef typename std::iterator_traits<_II>::iterator_category _Category;
    return __add_border<_Category>::add_border(first,last,border_,out_first,size_);
  }
  
  slip::BORDER_TREATMENT border_;
  std::size_t size_;

};
 
/**
 ** \brief Adds the border of the range [first,last) according to the border condtion border and the border size b_size.
 ** \since 1.0.0
  ** \param first RandomAccessIterator or RandomAccessIterator2d or RandomAccessIterator3d.
   ** \param last RandomAccessIterator or RandomAccessIterator2d or
   ** RandomAccessIterator3d.
   ** \param out_first RandomAccessIterator or RandomAccessIterator2d or
   ** RandomAccessIterator3d.
   ** \param border slip::BORDER_TREATMENT border condition.
   ** \param b_size Size of the border (1 by default).
   ** \pre first, last and out_first must be of the same iterator_category.
   ** \pre (out_last-out_first) >= (in_last-in_first)+2*(b_size)
   ** \par Example:
   ** \code
   ** //The input grayscale image
   ** slip::GrayscaleImage<double> I2(4,5);
   ** slip::iota(I2.begin(),I2.end(),1.0);
   ** //The enlarged grayscale image
   ** slip::GrayscaleImage<double> tmp2(I2.rows()+(2*b_size),
   **					I2.cols()+(2*b_size));
   ** //add border to I2 in tmp2 with slip::BORDER_TREATMENT_NEUMANN conditions
   ** //and a border size of 2
   ** std::size_t b_size = 2;
   ** slip::add_border(I2.upper_left(),I2.bottom_right(),
   **		       tmp2.upper_left(),tmp2.bottom_right(),
   **	               slip::BORDER_TREATMENT_NEUMANN,b_size);
   ** \endcode
   */
template <typename _II, typename _OI>
void add_border(_II first, _II last,_OI out_first, _OI out_last,
		const slip::BORDER_TREATMENT border,
		const std::size_t b_size = 1)
{
  
  typedef typename std::iterator_traits<_II>::iterator_category _Category;
    return __add_border<_Category>::add_border(first,last,border,
					       out_first,b_size);
}

/**
 ** \brief Adds the border at a 1d container according to the border condition border and the border size b_size.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.1
 ** \date 2013/11/30
 ** \since 1.4.0
 ** \param in input container.
 ** \param border_type slip::BORDER_TREATMENT border condition.
 ** \param out output container.
 ** \param b_size Size of the border (1 by default).
 ** \post out.size() == (in.size() + 2 * border_size)
 ** \par Example:
 ** \code
 ** slip::Array<double> in1d(10);
 ** slip::iota(in1d.begin(),in1d.end(),1.0);
 ** slip::Array<double> out1d;
 ** const std::size_t border_size1 = 2;
 ** slip::BORDER_TREATMENT border_type = slip::BORDER_TREATMENT_REFLECT;
 ** slip::add_border1d(in1d,
 **		       border_type,
 **		       out1d,
 **		       border_size1);
 **	
 ** std::cout<<"in1d  = \n"<<in1d<<std::endl;
 ** std::cout<<"out1d = \n"<<out1d<<std::endl;
 ** \endcode
 */
template<typename Container1,
	 typename Container2>
void add_border1d(const Container1 & in,
		  const slip::BORDER_TREATMENT border_type,
		  Container2& out,
		  const std::size_t border_size = 1)
{
   const std::size_t size = in.size();
   slip::AddBorder b(border_type,border_size);
   out.resize(size+(2u*border_size));
   b(in.begin(),in.end(),out.begin());
}
/**
 ** \brief Adds the border at a 2d container according to the border condition border and the border size b_size.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.1
 ** \date 2013/11/30
 ** \since 1.4.0
 ** \param in input container.
 ** \param border_type slip::BORDER_TREATMENT border condition.
 ** \param out output container.
 ** \param b_size Size of the border (1 by default).
 ** \post out.rows() == (in.rows() + 2 * border_size)
 ** \post out.cols() == (in.cols() + 2 * border_size)
 ** \par Example:
 ** \code
 ** slip::Array2d<double> in2d(4,5);
 ** slip::iota(in2d.begin(),in2d.end(),1.0);
 ** slip::Array2d<double> out2d;
 ** const std::size_t border_size2 = 2;
 ** slip::BORDER_TREATMENT border_type = slip::BORDER_TREATMENT_REFLECT;
 ** slip::add_border2d(in2d,
 **		       border_type,
 **		       out2d,
 **		       border_size2);
 **  
 ** std::cout<<"in2d  = \n"<<in2d<<std::endl;
 ** std::cout<<"out2d = \n"<<out2d<<std::endl;
 ** \endcode
 */
template<typename Container2d1,
	 typename Container2d2>
void add_border2d(const Container2d1 & in,
		  const slip::BORDER_TREATMENT border_type,
		  Container2d2& out,
		  const std::size_t border_size =  1)
{
   const std::size_t rows = in.rows();
   const std::size_t cols = in.cols();
   slip::AddBorder b(border_type,border_size);
   out.resize(rows+(2u*border_size),cols+(2u*border_size));
   b(in.upper_left(),in.bottom_right(),out.upper_left());
}

/**
 ** \brief Adds the border at a 3d container according to the border condition border and the border size b_size.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.1
 ** \date 2013/11/30
 ** \since 1.4.0
 ** \param in input container.
 ** \param border_type slip::BORDER_TREATMENT border condition.
 ** \param out output container.
 ** \param b_size Size of the border (1 by default).
 ** \post out.slices() == (in.slices() + 2 * border_size)
 ** \post out.rows() == (in.rows() + 2 * border_size)
 ** \post out.cols() == (in.cols() + 2 * border_size)
 ** \par Example:
 ** \code
 ** slip::Array3d<double> in3d(2,4,5);
 ** slip::iota(in3d.begin(),in3d.end(),1.0);
 ** slip::Array3d<double> out3d;
 ** const std::size_t border_size3 = 2;
 ** slip::BORDER_TREATMENT border_type = slip::BORDER_TREATMENT_REFLECT;
 ** slip::add_border3d(in3d,
 **		       border_type,
 **		       out3d,
 **		       border_size3);
 ** 
 ** std::cout<<"in3d  = \n"<<in3d<<std::endl;
 ** std::cout<<"out3d = \n"<<out3d<<std::endl;
 **\endcode
 */
template<typename Container3d1,
	 typename Container3d2>
void add_border3d(const Container3d1 & in,
		  const slip::BORDER_TREATMENT border_type,
		  Container3d2& out,
		  const  std::size_t border_size = 1)
{
  const std::size_t slices = in.slices();
  const std::size_t rows   = in.rows();
  const std::size_t cols   = in.cols();
   slip::AddBorder b(border_type,border_size);
   out.resize(slices+ (2u*border_size),
	      rows  + (2u*border_size),
	      cols  + (2u*border_size));
   b(in.front_upper_left(),in.back_bottom_right(),out.front_upper_left());
}

/**
 ** \brief Adds the border at a 4d container according to the border condition border and the border size b_size.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.1
 ** \date 2013/11/30
 ** \since 1.4.0
 ** \param in input container.
 ** \param border_type slip::BORDER_TREATMENT border condition.
 ** \param out output container.
 ** \param b_size Size of the border (1 by default).
 ** \post out.slabs() == (in.slabs() + 2 * border_size)
 ** \post out.slices() == (in.slices() + 2 * border_size)
 ** \post out.rows() == (in.rows() + 2 * border_size)
 ** \post out.cols() == (in.cols() + 2 * border_size)
 ** \par Example:
 ** \code
 ** slip::Array4d<double> in4d(2,2,4,5);
 ** slip::iota(in4d.begin(),in4d.end(),1.0);
 ** slip::Array4d<double> out4d;
 ** const std::size_t border_size4 = 2;
 ** slip::BORDER_TREATMENT border_type = slip::BORDER_TREATMENT_REFLECT;
 ** slip::add_border4d(in4d,
 **		       border_type,
 **		       out4d,
 **		       border_size4);
 ** 
 ** std::cout<<"in4d  = \n"<<in4d<<std::endl;
 ** std::cout<<"out4d = \n"<<out4d<<std::endl;
 **\endcode
 */
template<typename Container4d1,
	 typename Container4d2>
void add_border4d(const Container4d1 & in,
		  const slip::BORDER_TREATMENT border_type,
		  Container4d2& out,
		  const std::size_t border_size = 1)
{
  const std::size_t slabs  = in.slabs();
  const std::size_t slices = in.slices();
  const std::size_t rows   = in.rows();
  const std::size_t cols   = in.cols();
   slip::AddBorder b(border_type,border_size);
   out.resize(slabs + (2u*border_size),
	      slices+ (2u*border_size),
	      rows  + (2u*border_size),
	      cols  + (2u*border_size));
   b(in.first_front_upper_left(),in.last_back_bottom_right(),out.first_front_upper_left());
}

/**
 ** \brief Utility function which returns the Box1d corresponding to the inside of the container \a cont when border size is \a border_size.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.1
 ** \date 2013/11/30
 ** \since 1.4.0
 ** \param cont 1d container.
 ** \param border_size Size of the border around the box.
 ** \par Example:
 ** \code
 ** slip::Array<double> V(10);
 ** std::cout<<slip::inside_box1d(V,1)<<std::endl;
 ** std::cout<<slip::inside_box1d(V,2)<<std::endl;
 ** \endcode
 */
template <typename Container1d>
slip::Box1d<int> inside_box1d(const Container1d& cont,
			      const int border_size)
{
  return slip::Box1d<int>(border_size,
			  (int(cont.size())-1)-border_size);
}

/**
 ** \brief Utility function which returns the Box2d corresponding to the inside of the container \a cont when border size is \a border_size.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.1
 ** \date 2013/11/30
 ** \since 1.4.0
 ** \param cont 2d container.
 ** \param border_size Size of the border around the box.
 ** \par Example:
 ** \code
 ** slip::Array2d<double> A(10,8);
 ** std::cout<<slip::inside_box2d(A,1)<<std::endl;
 ** std::cout<<slip::inside_box2d(A,2)<<std::endl;
 ** \endcode
 */
template <typename Container2d>
slip::Box2d<int> inside_box2d(const Container2d& cont,
			      const int border_size)
{
  return slip::Box2d<int>(border_size,
			  border_size,
			  (int(cont.rows())-1)-border_size,
			  (int(cont.cols())-1)-border_size);
}

/**
 ** \brief Utility function which returns the Box3d corresponding to the inside of the container \a cont when border size is \a border_size.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.1
 ** \date 2013/11/30
 ** \since 1.4.0
 ** \param cont 3d container.
 ** \param border_size Size of the border around the box.
 ** \par Example:
 ** \code
 ** slip::Array3d<double> Vol(6,10,8);
 ** std::cout<<slip::inside_box3d(Vol,1)<<std::endl;
 ** std::cout<<slip::inside_box3d(Vol,2)<<std::endl;
 ** \endcode
 */
template <typename Container3d>
slip::Box3d<int> inside_box3d(const Container3d& cont,
			      const int border_size)
{
  return slip::Box3d<int>(border_size,
			  border_size,
			  border_size,
			  (int(cont.slices())-1)-border_size,
			  (int(cont.rows())-1)-border_size,
			  (int(cont.cols())-1)-border_size);
}

/**
 ** \brief Utility function which returns the Box4d corresponding to the inside of the container \a cont when border size is \a border_size.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.1
 ** \date 2013/11/30
 ** \since 1.4.0
 ** \param cont 4d container.
 ** \param border_size Size of the border around the box.
 ** \par Example:
 ** \code
 ** slip::Array4d<double> Vol4(6,10,8,5);
 ** std::cout<<slip::inside_box4d(Vol4,1)<<std::endl;
 ** std::cout<<slip::inside_box4d(Vol4,2)<<std::endl;
 ** \endcode
 */
template <typename Container4d>
slip::Box4d<int> inside_box4d(const Container4d& cont,
			      const int border_size)
{
  return slip::Box4d<int>(border_size,
			  border_size,
			  border_size,
			  border_size,
			  (int(cont.slabs())-1)-border_size,
			  (int(cont.slices())-1)-border_size,
			  (int(cont.rows())-1)-border_size,
			  (int(cont.cols())-1)-border_size);
}


}//slip::


#endif //SLIP_BORDER_TREATMENT_HPP
