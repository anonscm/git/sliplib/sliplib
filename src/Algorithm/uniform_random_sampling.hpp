/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file uniform_random_sampling.hpp
 * 
 * \brief Provides some algorithm to sample random data according to an uniform random low.
 * 
 */
#ifndef SLIP_UNIFORM_RANDOM_SAMPLING_HPP
#define SLIP_UNIFORM_RANDOM_SAMPLING_HPP
#include <iostream>
#include <iterator>
#include <cassert>
#include <cstddef>
#include <ctime>
#include <boost/random.hpp>

namespace slip
{
/** \name Mathematics algorithms */
  /* @{ */

  
  //to avoid similar random values if several generators are created 
  //in the same second
static unsigned int germ(0);
template <class T>
struct Rand1
{
  /**
   ** \brief Construct an uniform sampling random generator with the boost C++ Mersenne Twister pseudo-random generator of 32-bit numbers with a state size of 19937 bits.
   ** \author Benoit Tremblais
   ** \date 2023/04/17
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param min Minimal value of the uniform range.
   ** \param max Maximal value of the uniform range.
   ** \par Example:
   ** \code
   **  typedef  double T;
   ** const std::size_t N = 10000;
   ** //generate double values in the range [-2.0,2.0)
   ** slip::Rand1<T> rnd(static_cast<T>(-2.0),static_cast<T>(2.0));
   ** for(std::size_t i = 0; i < N; ++i)
   ** {
   **   std::cout<<rnd()<<std::endl;
   ** }
   ** \endcode
   */
  Rand1(const T& min,
	const T& max):
    generator_(boost::mt19937(static_cast<unsigned int>(std::time(0)+germ))),
    distrib_(boost::uniform_real<>(min,max)),
    deg_(boost::variate_generator<boost::mt19937&, boost::uniform_real<> >(generator_, distrib_))
  {
    germ++;
  }
  /**
   ** \brief sample a random value
   ** \return the sampled random value.
   */
  T  operator()()
  {
    return deg_();
  }

  boost::mt19937 generator_;
  boost::uniform_real<> distrib_;
  boost::variate_generator<boost::mt19937&, boost::uniform_real<> > deg_;
  
};
  /* @} */

}//::slip
#endif //SLIP_UNIFORM_RANDOM_SAMPLING
