/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file convolution.hpp
 * 
 * \brief Provides some convolution algorithms.
 * \since 1.0.0
 */
#ifndef SLIP_CONVOLUTION_HPP
#define SLIP_CONVOLUTION_HPP


#include <algorithm>
#include <numeric>
#include <cassert>
#include <iterator>
#include <vector>
#include "arithmetic_op.hpp"
#include "border_treatment.hpp"
#include "Array.hpp"
#include "Array3d.hpp"
#include "Box2d.hpp"
#include "Vector.hpp"
#include "FFT.hpp"
#include "apply.hpp"

namespace slip
{

  /** \name 1d convolutions */
  /* @{ */
  /**
   ** \brief Computes the valid convolution of signal by a 1d-kernel.
  
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/09/12
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator.
   ** \param kernel_first A RandomAccessIterator.
   ** \param kernel_last A RandomAccessIterator.
   ** \param ksize_left kernel size left to the "center" of the kernel.
   ** \param ksize_right kernel size right to the "center" of the kernel.
   ** \param result A RandomAccessIterator.
   ** \pre size(result) = size(last - first - ksize_left - ksize_right)
   ** \image html asymetric_kernel1d.png "asymmetric 1d kernel support"
   ** \image latex asymetric_kernel1d.eps "asymmetric 1d kernel support" width=5cm
   ** \par Description:
   ** Only non zero-padded points (kernel is fully inside) are computed:
   **  if the signal size is N, and the kernel size is K = ksize_left + ksize_right + 1, 
   **  the size of the result will be N - (ksize_left + ksize_right).
   ** \par Example:
   ** \code
   ** std::vector<float> v(16);
   ** for(int i = 0; i < 16; ++i)
   **   v[i] = i + 1;
   ** slip::block<float,7> kernel = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0,
   ** 7.0};
   ** std::vector<float> result(10);
   ** slip::valid_convolution(v.begin(),v.end(),
   **                         kernel.begin(),kernel.end(),4,2,
   **                         result.begin());
   ** \endcode
   */
  template <typename SrcIter, typename KernelIter, typename ResIter>
  inline
  void valid_convolution(SrcIter first,
			 SrcIter last,
			 KernelIter kernel_first,
			 KernelIter kernel_last,
			 std::size_t ksize_left,
			 std::size_t ksize_right,
			 ResIter result)
  {
    assert(static_cast<std::size_t>(kernel_last - kernel_first) == (ksize_left + ksize_right + 1));
    SrcIter first_offset = first + ksize_left;
    SrcIter last_offset  = last  - ksize_right;
    std::size_t K = ksize_left + ksize_right + 1;
    for(;first_offset < last_offset; ++first_offset, ++result)
      {
	SrcIter first_offset_tmp = first_offset - ksize_left;
	SrcIter last_offset_tmp  = first_offset_tmp + K;
	std::reverse_iterator<KernelIter> kernel_last_tmp = 
	  std::reverse_iterator<KernelIter>(kernel_last);

	*result = std::inner_product(first_offset_tmp,last_offset_tmp,
				     kernel_last_tmp,
				     typename std::iterator_traits<ResIter>::value_type());
	
      }
  }


  /**
   ** \brief Computes the full convolution of signal by a 1d-kernel
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/26
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator.
   ** \param kernel_first A RandomAccessIterator.
   ** \param kernel_last A RandomAccessIterator.
   ** \param result A RandomAccessIterator.
   ** \pre size(result) = size(last - first + ksize_left + ksize_right - 1)
   ** \par  Description:
   ** If the signal size is N, and the kernel size is K = ksize_left + ksize_right + 1, 
   **  the size of the result will be N + (ksize_left + ksize_right + 1) - 1
   **
    ** \par Example:
   ** \code
   ** std::vector<float> v(16);
   ** for(int i = 0; i < 16; ++i)
   **   v[i] = i + 1;
   ** slip::block<float,7> kernel = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0,
   ** 7.0};
   ** std::vector<float> result2(22);
   ** slip::full_convolution(v.begin(),v.end(),
   **                        kernel.begin(),kernel.end(),
   **                        result2.begin());
   ** \endcode
   */
  template <typename SrcIter, typename KernelIter, typename ResIter>
  inline
  void full_convolution(SrcIter first,
			SrcIter last,
			KernelIter kernel_first,
			KernelIter kernel_last,
			ResIter result)
  {

    std::size_t K = static_cast<std::size_t>(kernel_last - kernel_first - 1);
   
    //valid convolution
    slip::valid_convolution(first,last,
			    kernel_first,kernel_last,
			    K,0,
			    result + K);
   
    //zero-padded treatment
    //left
    std::reverse_iterator<KernelIter> rkernel_last_tmp = 
       std::reverse_iterator<KernelIter>(kernel_last);

      for(size_t i = 0; i < K; ++i)
      {
	*(result + i ) =  std::inner_product(first,first + i + 1,rkernel_last_tmp + (K - i),typename std::iterator_traits<ResIter>::value_type());
      }

      //right
      SrcIter first_tmp = last - K;
      ResIter result_tmp = result + (last - first);
      rkernel_last_tmp = std::reverse_iterator<KernelIter>(kernel_last);

      for(size_t i = 0; i < K; ++i)
	{
	 *(result_tmp + i) = std::inner_product(first_tmp + i,last,rkernel_last_tmp,typename std::iterator_traits<ResIter>::value_type());
	}
  }

  /**
   ** \brief Computes the same convolution of signal by a 1d-kernel
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/26
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator.
   ** \param kernel_first A RandomAccessIterator.
   ** \param kernel_last A RandomAccessIterator.
   ** \param ksize_left kernel size left to the "center" of the kernel
   ** \param ksize_right kernel size right to the "center" of the kernel
   ** \param result A RandomAccessIterator. slip::BORDER_TREATMENT_ZERO_PADDED by default.
   ** \param bt slip::BORDER_TREATMENT
   **  
   ** \pre size(result) = size(last - first)
   ** \image html asymetric_kernel1d.png "asymmetric 1d kernel support"
   ** \image latex asymetric_kernel1d.eps "asymmetric 1d kernel support" width=5cm
   ** \par  Description:
   **  If the signal size is N, the size of the result will be N too.
   **  Different BORDER_TREATMENT can be done:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 (default)
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   **  
   ** \par Example:
   ** \code
   ** std::vector<float> v(16);
   ** for(int i = 0; i < 16; ++i)
   **   v[i] = i + 1;
   ** slip::block<float,7> kernel = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0,
   ** 7.0};
   ** std::vector<float> result4(16);
   ** slip::same_convolution(v.begin(),v.end(),kernel.begin(),kernel.end(),4,2,result4.begin(),slip::BORDER_TREATMENT_ZERO_PADDED);
   ** slip::same_convolution(v.begin(),v.end(),kernel.begin(),kernel.end(),4,2,result4.begin(),slip::BORDER_TREATMENT_NEUMANN);
    **
   ** slip::same_convolution(v.begin(),v.end(),kernel.begin(),kernel.end(),4,2,result4.begin(),slip::BORDER_TREATMENT_AVOID);
 ** slip::same_convolution(v.begin(),v.end(),kernel.begin(),kernel.end(),4,2,result4.begin(),slip::BORDER_TREATMENT_CIRCULAR);
   ** 
   ** \endcode
   ** \par Example2:
   ** \code
   ** //sobel filtering
   ** slip::GrayscaleImage<float> I;
   ** I.read("lena.gif");
   ** slip::GrayscaleImage<float> I2(I.dim1(),I.dim2());
   ** slip::block<float,3> m = {0.5,0.0,-0.5};
   ** for(size_t i = 0; i < I.dim1(); ++i)
   ** {
   **     slip::same_convolution(I.row_begin(i),I.row_end(i)
   **			      ,m.begin(),m.end(),1,1,
   **			      I2.row_begin(i),
   **			      slip::BORDER_TREATMENT_NEUMANN);
   **
   ** }
   ** slip::block<float,3> m2 = {1.0,2.0,1.0};
   ** slip::GrayscaleImage<float> I3(I.dim1(),I.dim2());
   ** for(size_t j = 0; j < I2.dim2(); ++j)
   **  {
   **    slip::same_convolution(I2.col_begin(j),I2.col_end(j)
   **		      ,m2.begin(),m2.end(),1,1,
   **		      I3.col_begin(j),
   **		      slip::BORDER_TREATMENT_NEUMANN);
   **  }
   ** I3.write("lena_mean.gif");
   ** \endcode
   **
   */
  template <typename SrcIter, typename KernelIter, typename ResIter>
  inline
  void same_convolution(SrcIter first,
			SrcIter last,
			KernelIter kernel_first,
			KernelIter kernel_last,
			std::size_t ksize_left,
			std::size_t ksize_right,
			ResIter result,
			slip::BORDER_TREATMENT bt = slip::BORDER_TREATMENT_ZERO_PADDED)
  {
    //valid convolution
    slip::valid_convolution(first,last,
			    kernel_first,kernel_last,
			    ksize_left,ksize_right,
			    result + ksize_right);

    //border_treatments
    if(bt == slip::BORDER_TREATMENT_ZERO_PADDED)
      {
	std::size_t K = ksize_left + ksize_right;
	//left
	for(size_t i = 0; i < ksize_right; ++i)
	  {
	    *(result + (ksize_right - i - 1)) =  std::inner_product(first,first + (K - i),std::reverse_iterator<KernelIter>(kernel_last - i - 1),typename std::iterator_traits<ResIter>::value_type());

	  }
	//right
	SrcIter first_tmp = last - K;
	ResIter result_tmp = result + ((last - first) - ksize_left);
	for(size_t i = 0; i < ksize_left; ++i)
	  {
	    *(result_tmp + i) =  std::inner_product(first_tmp + i,last,std::reverse_iterator<KernelIter>(kernel_last),typename std::iterator_traits<ResIter>::value_type());

	  }
	 

      }
    else if(bt == slip::BORDER_TREATMENT_NEUMANN)
      {
	std::size_t K = ksize_left + ksize_right;
	typedef typename std::iterator_traits<ResIter>::value_type res_value_type;
	typedef typename std::iterator_traits<SrcIter>::value_type src_value_type;
	typedef typename std::iterator_traits<KernelIter>::value_type k_value_type;


	//left
	//compute the left accumulate sum of the elements of the reverse
	//kernel and store each partial result in accu_vec
	//	k_value_type accum = k_value_type(0);
	std::reverse_iterator<KernelIter> rkernel = 
	  std::reverse_iterator<KernelIter>(kernel_last);

	std::vector<k_value_type> accu_vec(ksize_right);
	std::partial_sum(rkernel,rkernel + ksize_right,accu_vec.begin());
	
	res_value_type left_bvalue  = src_value_type(*first);
	for(size_t i = 0; i < ksize_right; ++i)
	  {
	    
	    *(result + (ksize_right - i - 1)) =  
	      std::inner_product(first,
				 first + (K - i),
				 std::reverse_iterator<KernelIter>(kernel_last - i - 1),
				 res_value_type(accu_vec[i]) * left_bvalue);
	  }

	//right
	//compute the right accumulate sum of the elements of the reverse
	//kernel and store each partial result in accu_vec2
	//accum = k_value_type(0);
	KernelIter rkernel2 = kernel_last - K - 1;

	std::vector<k_value_type> accu_vec2(ksize_left);
	std::partial_sum(rkernel2,rkernel2 + ksize_left,accu_vec2.begin());
	res_value_type right_bvalue = src_value_type(*(last - 1));
	SrcIter first_tmp = last - K;
	ResIter result_tmp = result + ((last - first) - ksize_left);
	for(size_t i = 0; i < ksize_left; ++i)
	  {
	    *(result_tmp + i) =  
	      std::inner_product(first_tmp + i,
				 last,
				 std::reverse_iterator<KernelIter>(kernel_last),
				 res_value_type(accu_vec2[i]) * right_bvalue);
	  }

      }
    else if(bt == slip::BORDER_TREATMENT_AVOID)
      {
	//left
	for(size_t i = 0; i < ksize_right; ++i)
	  {
	    *(result + (ksize_right - i - 1)) =  
	      typename std::iterator_traits<ResIter>::value_type();
	  }
	//right
	ResIter result_tmp = result + ((last - first) - ksize_left);
	for(size_t i = 0; i < ksize_left; ++i)
	  {
	    *(result_tmp + i) = 
	      typename std::iterator_traits<ResIter>::value_type();
	  }
      }

    else if(bt == slip::BORDER_TREATMENT_CIRCULAR)
      {
	typedef typename std::iterator_traits<SrcIter>::value_type src_value_type;

	std::size_t kernel_size = ksize_right + ksize_left + 1;
	std::size_t tmp_size = ksize_right + ksize_right+ksize_left;
	slip::Array<src_value_type> tmp_data(tmp_size);

	// Copy data :
	for(size_t i = 0; i < tmp_size-ksize_right; ++i)
 	  {
	  	tmp_data[i+ksize_right]=*(first+i);
 	  }
	for(size_t i = 0; i < ksize_right; ++i)
 	  {
	  	tmp_data[ksize_right-i-1]=*(last-i-1);
 	  }

	//left
 	for(size_t i = 0; i < ksize_right; ++i)
 	  {
	  *(result + i)=std::inner_product(tmp_data.begin() + i,tmp_data.begin() + i + kernel_size,std::reverse_iterator<KernelIter>(kernel_last),typename std::iterator_traits<ResIter>::value_type());
 	  }

	std::size_t tmp_size2 = ksize_left + ksize_left+ksize_right;
	slip::Array<src_value_type> tmp_data2(tmp_size2);
	// Copy data :
	for(size_t i = 0; i < tmp_size2-ksize_left; ++i)
 	  {
	  	tmp_data2[ksize_right+ksize_left-i-1]=*(last-i-1);
 	  }
	for(size_t i = 0; i < ksize_left; ++i)
 	  {
	  	tmp_data2[ksize_right+ksize_left+i]=*(first+i);
 	  }
	//right
 	for(size_t i = 0; i < ksize_left; ++i)
 	  {
	   *(result + ((last - first) - ksize_left) + i)= std::inner_product(tmp_data2.begin() + i,tmp_data2.begin() + i + kernel_size,std::reverse_iterator<KernelIter>(kernel_last),typename std::iterator_traits<ResIter>::value_type());
 	  }
      }
    else
      {
	std::size_t K = ksize_left + ksize_right;
	//left
	for(size_t i = 0; i < ksize_right; ++i)
	  {
	    *(result + (ksize_right - i - 1)) =  std::inner_product(first,first + (K - i),std::reverse_iterator<KernelIter>(kernel_last - i - 1),typename std::iterator_traits<ResIter>::value_type());

	  }
	//right
	SrcIter first_tmp = last - K;
	ResIter result_tmp = result + ((last - first) - ksize_left);
	for(size_t i = 0; i < ksize_left; ++i)
	  {
	    *(result_tmp + i) =  std::inner_product(first_tmp + i,last,std::reverse_iterator<KernelIter>(kernel_last),typename std::iterator_traits<ResIter>::value_type());

	  }
	 
      }
  }
/**
   ** \brief Computes the same convolution of signal by a symmetric 1d-kernel support.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator.
   ** \param kernel_first A RandomAccessIterator.
   ** \param kernel_last A RandomAccessIterator.
   ** \param result A RandomAccessIterator.
   ** \param bt slip::BORDER_TREATMENT. slip::BORDER_TREATMENT_ZERO_PADDED by default.
   ** \pre size(result) = size(last - first)
   ** \image html symmetric_kernel1d.png "symmetric 1d kernel support"
   ** \image latex symmetric_kernel1d.eps "symmetric 1d kernel support" width=5cm

   ** \par Description:
   **  If the signal size is N, the size of the result will be N too.
   **  Different BORDER_TREATMENT can be done:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 (default)
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \par Example:
   ** \code
   ** slip::Vector<float> Signal(16);
   ** slip::Vector<float> result5(16);
   ** slip::iota(Signal.begin(),Signal.end(),1.0,1.0);
   ** slip::same_convolution(Signal.begin(),Signal.end(),
   **		   kernel.begin(),kernel.end(),
   **		   result5.begin(),
   **		   slip::BORDER_TREATMENT_ZERO_PADDED);    
   ** std::cout<<"result5 = \n"<<result5<<std::endl;
   ** slip::same_convolution(Signal.begin(),Signal.end(),
   **		   kernel.begin(),kernel.end(),
   **		   result5.begin(),
   **		   slip::BORDER_TREATMENT_NEUMANN);    
   ** std::cout<<"result5 = \n"<<result5<<std::endl;
   ** slip::same_convolution(Signal.begin(),Signal.end(),
   **                        kernel.begin(),kernel.end(),
   **                        result5.begin(),
   **                        slip::BORDER_TREATMENT_AVOID);    
   ** std::cout<<"result5 = \n"<<result5<<std::endl;
   ** slip::same_convolution(Signal.begin(),Signal.end(),
   **		   kernel.begin(),kernel.end(),
   **		   result5.begin(),
   **		    slip::BORDER_TREATMENT_CIRCULAR);    
   ** std::cout<<"result5 = \n"<<result5<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** //sobel filtering
   ** slip::GrayscaleImage<float> I;
   ** I.read("lena.gif");
   ** slip::GrayscaleImage<float> I2(I.dim1(),I.dim2());
   ** slip::block<float,3> m = {0.5,0.0,-0.5};
   ** for(size_t i = 0; i < I.dim1(); ++i)
   ** {
   **     slip::same_convolution(I.row_begin(i),I.row_end(i),
   **			      m.begin(),m.end(),
   **			      I2.row_begin(i),
   **			      slip::BORDER_TREATMENT_NEUMANN);
   **
   ** }
   ** slip::block<float,3> m2 = {1.0,2.0,1.0};
   ** slip::GrayscaleImage<float> I3(I.dim1(),I.dim2());
   ** for(size_t j = 0; j < I2.dim2(); ++j)
   **  {
   **    slip::same_convolution(I2.col_begin(j),I2.col_end(j),
   **		      m2.begin(),m2.end(),
   **		      I3.col_begin(j),
   **		      slip::BORDER_TREATMENT_NEUMANN);
   **  }
   ** I3.write("lena_mean.gif");
   ** \endcode
   **
   */

template <typename SrcIter, typename KernelIter, typename ResIter>
  inline
  void same_convolution(SrcIter first,
			SrcIter last,
			KernelIter kernel_first,
                        KernelIter kernel_last,
			ResIter result,
			slip::BORDER_TREATMENT bt= slip::BORDER_TREATMENT_ZERO_PADDED)
  {
    
     std::size_t ksize_left = (kernel_last - kernel_first)/ 2;
     std::size_t ksize_right = (kernel_last - kernel_first - 1) - ksize_left;

     slip::same_convolution(first,
			    last,
			    kernel_first,
			    kernel_last,
			    ksize_left,
			    ksize_right,
			    result,
			    bt);
  }

  /**
   ** \brief Computes the same convolution of signal by a symmetric 1d-kernel.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param Signal  1D Container of the input Signal.
   ** \param Kernel  1D Container of the kernel.
   ** \param Result  1D Container of the convolution result.
   ** \param bt slip::BORDER_TREATMENT
   ** \pre Result.size() == Signal.size()
   ** \image html symmetric_kernel1d.png "symmetric 1d kernel support"
   ** \image latex symmetric_kernel1d.eps "symmetric 1d kernel support" width=5cm
   ** \par Description:
   **  If the signal size is N, the size of the result will be N too.
   **  Different BORDER_TREATMENT can be done:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 (default)
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \par Example:
   ** \code
   ** slip::Vector<float> Signal(16);
   ** slip::Vector<float> result5(16);
   ** slip::iota(Signal.begin(),Signal.end(),1.0,1.0);
   ** slip::same_convolution(Signal,kernel,result5,slip::BORDER_TREATMENT_ZERO_PADDED);    
   ** std::cout<<"result5 = \n"<<result5<<std::endl;
   ** slip::same_convolution(Signal,kernel,result5,slip::BORDER_TREATMENT_NEUMANN);    
   ** std::cout<<"result5 = \n"<<result5<<std::endl;
   ** slip::same_convolution(Signal,kernel,result5,slip::BORDER_TREATMENT_AVOID);    
   ** std::cout<<"result5 = \n"<<result5<<std::endl;
   ** slip::same_convolution(Signal,kernel,result5,slip::BORDER_TREATMENT_CIRCULAR);
   ** std::cout<<"result5 = \n"<<result5<<std::endl;
   ** \endcode
   */

template <typename Vector1, typename Vector2, typename Vector3> 

inline void same_convolution(const Vector1& Signal,
                             const Vector2& Kernel, 
                             Vector3& Result, 
                             slip::BORDER_TREATMENT bt= slip::BORDER_TREATMENT_ZERO_PADDED)


  {

    slip::same_convolution(Signal.begin(),Signal.end(),Kernel.begin(),Kernel.end(),Result.begin(),bt);
  
  }

   /**
   ** \brief Computes the FFT circular convolution.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2014/03/13
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first    A RandomAccessIterator iterator to the data.
   ** \param last     A RandomAccessIterator iterator to the data.
   ** \param first2   A RandomAccessIterator iterator to the filter.
   ** \param last2    A RandomAccessIterator iterator to the filter.
   ** \param conv_first   A RandomAccessIterator iterator to the convolution.
   ** \param conv_last    A RandomAccessIterator iterator to the convolution.
   ** \pre data value type must be the same for the 3 ranges
   ** \pre [first,last) must be valid.
   ** \pre [first,last) and [first2,last2) are assumed to contain data of the same type
   ** \pre [first,last) and [first2,last2) are supposed to be (last-first) periodic
   ** \pre (conv_last-conv_first) == (last-first)
   ** \pre (last2-first2) == (last-first)
   ** \par Complexity: 3*K*2*N*log_2(2*N) + N = 6KNlog_2(N) + (4K + 2)N with N =  (last-first)
   ** \par Example:
   ** \code
   ** slip::Array<double> signal1(16);
   ** slip::iota(signal1.begin(),signal1.end(),1.0);
   ** slip::Array<double> signal2(16);
   ** slip::iota(signal2.begin(),signal2.end(),3.0);
   ** slip::Array<double> resultc(16);
   ** std::cout<<"signal1 = \n"<<signal1<<std::endl;
   ** std::cout<<"signal2 = \n"<<signal2<<std::endl;
   ** slip::fft_circular_convolution(signal1.begin(),signal1.end(),
   **				signal2.begin(),signal2.end(),
   **			resultc.begin(),resultc.end());
   ** std::cout<<"circular fft convoultion (signal1,signal2) = \n"<<resultc<<std::endl;
   ** \endcode
   */
   template<typename RandomAccessIterator1, 
	    typename RandomAccessIterator2, 
	    typename RandomAccessIterator3>
  inline
  void fft_circular_convolution(RandomAccessIterator1 first , RandomAccessIterator1 last, 
				RandomAccessIterator2 first2, RandomAccessIterator2 last2,
				RandomAccessIterator3 conv_first, RandomAccessIterator3 conv_last )
  {
    assert((last - first) == (last2 - first2));
    assert((last - first) == (conv_last - conv_first));
    assert((last - first) == (last2 - first2));

    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type;
    typedef typename slip::lin_alg_traits<value_type>::value_type T;

   
    std::size_t N = static_cast<std::size_t>(last-first);
    slip::Array<std::complex<T> > fft1(N);
    slip::Array<std::complex<T> > fft2(N);
    slip::real_fft(first,last,fft1.begin());
    slip::real_fft(first2,last2,fft2.begin());
    slip::multiplies(fft1.begin(),fft1.end(),
		     fft2.begin(),
		     fft1.begin());
    slip::ifft(fft1.begin(),fft1.end(),fft2.begin());
    std::transform(fft2.begin(),fft2.end(),conv_first,
		   slip::un_real<std::complex<T>,T>());  
    
    
  }


 /**
   ** \brief Computes the FFT convolution.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> (2008/05/24)
   ** \date 2008/05/24
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    A RandomAccessIterator iterator to the data.
   ** \param last     A RandomAccessIterator iterator to the data.
   ** \param first2   A RandomAccessIterator iterator to the filter.
   ** \param last2    A RandomAccessIterator iterator to the filter.
   ** \param conv_first   A RandomAccessIterator iterator to the convolution.
   ** \param conv_last    A RandomAccessIterator iterator to the convolution.
   ** \pre [first,last) must be valid.
   ** \pre [first,last) and [first2,last2) are assumed to contain data of the same type
   ** \pre (conv_last-conv_first) >= (last-first) + (last2-first2) - 1
   ** \par Complexity: 3*K*2*N*log_2(2*N) + N = 6KNlog_2(N) + (4K + 2)N with N =  (last-first) + (last2-first2) + 1
   ** \par Example:
   ** \code
   ** std::vector<float> v(16);
   ** slip::iota(v.begin(),v.end(),0.0f,1.0f);
   ** slip::block<float,7> kernel = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0};
   ** std::vector<float> result_fft2(22);
   ** slip::fft_convolution(v.begin(),v.end(),
   **			    kernel.begin(),kernel.end(),
   **		            result_fft2.begin(),
   **		            result_fft2.end());
   ** \endcode
   */
  template<typename RandomAccessIterator1, typename RandomAccessIterator2, typename RandomAccessIterator3>
  inline
  void fft_convolution(RandomAccessIterator1 first , RandomAccessIterator1 last, 
		       RandomAccessIterator2 first2, RandomAccessIterator2 last2,
		       RandomAccessIterator3 conv_first, RandomAccessIterator3 conv_last )
  {

    assert((last - first) != 0);
    assert((last2 - first2) != 0);
    assert((conv_last-conv_first) >= (last - first) + (last2 - first2) -1);
    typedef typename std::iterator_traits<RandomAccessIterator1>::difference_type _Difference1;
    _Difference1 size1 = last - first;
    typedef typename std::iterator_traits<RandomAccessIterator2>::difference_type _Difference2;
    _Difference2 size2 = last2 - first2;

    _Difference1 size = size1 + _Difference1(size2) - 1;

 
    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type;
    typedef  typename slip::lin_alg_traits<value_type>::value_type Real;
 
  //zero padding of the input data
    slip::Vector<value_type> data1(size,value_type());
    std::copy(first,last,data1.begin());
    slip::Vector<value_type> data2(size,value_type());
    std::copy(first2,last2,data2.begin());
    
    //compute the two FFT
    slip::Vector<std::complex<value_type> > fft1(size,std::complex<value_type>());
    slip::Vector<std::complex<value_type> > fft2(size,std::complex<value_type>());
    slip::real_fft(data1.begin(), data1.end(), fft1.begin());
    slip::real_fft(data2.begin(), data2.end(), fft2.begin());
    //multiplies the two FFT
    slip::multiplies(fft1.begin(),fft1.end(),
		     fft2.begin(),
		     fft1.begin());
    //invert the product of the two FFT
    slip::ifft(fft1.begin(),fft1.end(),fft2.begin());
    //get the real part of the inverse FFT
    std::transform(fft2.begin(),fft2.end(),conv_first,slip::un_real<std::complex<Real>,Real>());  
   }

/**
   ** \brief Computes the fft convolution of signal by a symmetric 1d-kernel.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param Signal  1D Container of the input Signal.
   ** \param Kernel  1D Container of the kernel.
   ** \param Result  1D Container of the convolution result.
   ** \pre Result.size() == Signal.size() + Kernel.size() - 1
   ** \pre Signal and Kernel are assumed to contain data of the same type
   ** \par Complexity: 3*K*2*N*log_2(2*N) + N = 6KNlog_2(N) + (4K + 2)N with N =  (last-first) + (last2-first2) + 1
   ** \par Example:
   ** \code
   ** slip::Vector<float> Signal(16);
   ** slip::Vector<float> result5(22);
   ** slip::iota(Signal.begin(),Signal.end(),1.0,1.0);
   ** slip::fft_convolution(Signal,kernel,result5);
   ** std::cout<<result5<<std::endl;
   ** \endcode
   */

  template <typename Vector1, typename Vector2, typename Vector3> 
  inline void fft_convolution(const Vector1& Signal,
			      const Vector2& Kernel, 
			      Vector3& Result)


  {
    slip::fft_convolution(Signal.begin(),Signal.end(),
			  Kernel.begin(),Kernel.end(),
			  Result.begin(),Result.end());
  }


  /* @} */


  /** \name 2d convolutions */
  /* @{ */
  /**
   ** \brief Computes the separable convolution of a 2d signal by two asymmetric 1d-kernels support.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2012/10/21
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param I_up RandomAccessIterator2d on the upper_left element of the 2d signal range.
   ** \param I_bot RandomAccessIterator2d. on one past-the-end element of the 2d signal range.
   ** \param k1_first A RandomAccessIterator on the first element of the horizontal kernel.
   ** \param k1_last A RandomAccessIterator on one past-the-end element of the horizontal kernel.
   ** \param ksize_left1 kernel size left to the "center" of the horizontal kernel
   ** \param ksize_right1 kernel size right to the "center" of the horizontal kernel
   ** \param bt1 slip::BORDER_TREATMENT associated with the horizontal kernel:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \param k2_first A RandomAccessIterator on the first element of the vertical kernel.
   ** \param k2_last A RandomAccessIterator on one past-the-end element of the vertical kernel.
   ** \param ksize_left2 kernel size left to the "center" of the vertical kernel
   ** \param ksize_right2 kernel size right to the "center" of the vertical kernel
   ** \param bt2 slip::BORDER_TREATMENT associated with the vertical kernel:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \param R_up RandomAccessIterator2d on the upper_left element of the 2d convolved signal range.
   ** \pre [I_up,I_bot) must be valid.
   ** \pre [R_up,R_up + (I_bot-I_up)) must be valid.
   ** \par Example:
   ** \code
   ** slip::GrayscaleImage<float> I;
   ** I.read("lena.gif");
   ** slip::GrayscaleImage<float> Result(I.rows(),I.cols());
   ** slip::block<float,3> k1 = {1.0, 0.0, -1.0};
   ** slip::block<float,3> k2 = {1.0,2.0,1.0};
   ** std::size_t size = k1.size()/2;
   ** slip::separable_convolution2d(I.upper_left(),I.bottom_right(),
   ** 				   k1.begin(),k1.end(),
   ** 				   size,size,
   ** 				   slip::BORDER_TREATMENT_NEUMANN,
   ** 				   k2.begin(),k2.end(),
   ** 				   size,size,
   ** 				   slip::BORDER_TREATMENT_NEUMANN,
   ** 				   Result.upper_left());
   ** \endcode
   */
  template <typename RandomAccessIterator2d1, 
	  typename KernelIter1,
	  typename KernelIter2,
	  typename RandomAccessIterator2d2>
inline
void separable_convolution2d(RandomAccessIterator2d1 I_up,
			     RandomAccessIterator2d1 I_bot,
			     KernelIter1 k1_first, 
			     KernelIter1 k1_last,
			     std::size_t ksize_left1,
			     std::size_t ksize_right1,
			     slip::BORDER_TREATMENT bt1,
			     KernelIter2 k2_first, 
			     KernelIter2 k2_last,
			     std::size_t ksize_left2,
			     std::size_t ksize_right2,
			     slip::BORDER_TREATMENT bt2,
			     RandomAccessIterator2d2 R_up)
{

  typename RandomAccessIterator2d1::difference_type size2d = 
    I_bot - I_up;
  typedef typename RandomAccessIterator2d1::size_type size_type;
  typedef typename RandomAccessIterator2d1::value_type value_type;
  size_type nb_rows = size2d[0];
  size_type nb_cols = size2d[1];

 
  for(size_type i = 0; i < nb_rows; ++i)
    {
      slip::same_convolution(I_up.row_begin(i),I_up.row_end(i),
			     k1_first,k1_last,
			     ksize_left1,
			     ksize_right1,
			     R_up.row_begin(i),
			     bt1);
    }
  slip::Array<value_type> col_tmp(nb_rows);
  for(size_type j = 0; j < nb_cols; ++j)
    {
      std::copy(R_up.col_begin(j),R_up.col_end(j),col_tmp.begin());
      slip::same_convolution(col_tmp.begin(),col_tmp.end(),
			     k2_first,k2_last,
			     ksize_left2,
			     ksize_right2,
			     R_up.col_begin(j),
			     bt2);
    }
  
}



 /**
   ** \brief  Computes the separable convolution of a 2d signal by two symmetric 1d-kernels support.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param I_up RandomAccessIterator2d on the upper_left element of the 2d signal range.
   ** \param I_bot RandomAccessIterator2d. on one past-the-end element of the 2d signal range.
   ** \param k1_first A RandomAccessIterator on the first element of the horizontal kernel.
   ** \param k1_last A RandomAccessIterator on one past-the-end element of the horizontal kernel.
   ** \param k2_first A RandomAccessIterator on the first element of the vertical kernel.
   ** \param k2_last A RandomAccessIterator on one past-the-end element of the vertical kernel.
  
   ** \param R_up RandomAccessIterator2d on the upper_left element of the 2d convolved signal range.
   ** \param bt slip::BORDER_TREATMENT associated with the two kernels:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 (default)
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \pre [I_up,I_bot) must be valid.
   ** \pre [R_up,R_up + (I_bot-I_up)) must be valid.
   ** \par Example:
   ** \code
   ** slip::GrayscaleImage<float> I;
   ** I.read("lena.gif");
   ** slip::GrayscaleImage<float> Result(I.rows(),I.cols());
   ** slip::block<float,3> k1 = {1.0, 0.0, -1.0};
   ** slip::block<float,3> k2 = {1.0,2.0,1.0};
   ** std::size_t size = k1.size()/2;
   ** slip::separable_convolution2d(I.upper_left(),I.bottom_right(),
   **  				   k1.begin(),k1.end(),
   ** 				   k2.begin(),k2.end(),
   ** 				   Result.upper_left(),
   ** 				   slip::BORDER_TREATMENT_NEUMANN);
   ** \endcode
   */
template <typename RandomAccessIterator2d1, 
	  typename KernelIter1,
	  typename KernelIter2,
	  typename RandomAccessIterator2d2>
inline
void separable_convolution2d(RandomAccessIterator2d1 I_up,
			     RandomAccessIterator2d1 I_bot,
			     KernelIter1 k1_first, 
			     KernelIter1 k1_last,
			     KernelIter2 k2_first, 
			     KernelIter2 k2_last,
			     RandomAccessIterator2d2 R_up,
			     slip::BORDER_TREATMENT bt = slip::BORDER_TREATMENT_ZERO_PADDED)
{
  std::size_t size1 = (k1_last-k1_first)/2;
  std::size_t size1_bis = (k1_last-k1_first) - 1 - size1;
  std::size_t size2 = (k2_last-k2_first)/2;
  std::size_t size2_bis = (k2_last-k2_first) - 1 - size2;
 
  slip::separable_convolution2d(I_up,I_bot,
				    k1_first,
				    k1_last,
				    size1,
				    size1_bis,
				    bt,
				    k2_first,
				    k2_last,
				    size2,
				    size2_bis,
				    bt,
				    R_up);
}

/**
   ** \brief Computes the separable convolution of 2d signal by 
   ** 2 symmetric 1d-kernel support.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param Signal  2D Container of the input Signal
   ** \param kernel1  1D Container of the horizontal kernel
   ** \param kernel2  1D Container of the vertical kernel
   ** \param Result  2D Container of the convolution result
   ** \param bt slip::BORDER_TREATMENT associated with the two kernels:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 (default)
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \pre Result.rows() >= Signal.rows()
   ** \pre Result.cols() >= Signal.cols()
   ** \par Example:
   ** \code
   ** slip::GrayscaleImage<float> I;
   ** I.read("lena.png");
   ** slip::GrayscaleImage<float> Result(I.rows(),I.cols());
   ** slip::block<float,3> k1 = {1.0/4.0,2.0/4.0,1.0/4.0};
   ** slip::block<float,3> k2 = {1.0/4.0,2.0/4.0,1.0/4.0};
   ** slip::separable_convolution2d(Signal,
   **                               k1,k2,
   **                               Result,
   **                               slip::BORDER_TREATMENT_NEUMANN);
   ** \endcode
   */

  template <typename Container1, 
	    typename Kernel1, 
	    typename Kernel2,
	    typename Container2> 
  inline void separable_convolution2d(const Container1& Signal,
				      const Kernel1& kernel1, 
				      const Kernel2& kernel2,
				      Container2& Result,
				      slip::BORDER_TREATMENT bt = slip::BORDER_TREATMENT_ZERO_PADDED)


  {
    slip::separable_convolution2d(Signal.upper_left(),
				  Signal.bottom_right(),
				  kernel1.begin(),kernel1.end(),
				  kernel2.begin(),kernel2.end(),
				  Result.upper_left());
  }


/**
   ** \brief Computes the in-place separable convolution of a 2d signal by two asymmetric 1d-kernels support.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2012/10/22
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param I_up RandomAccessIterator2d on the upper_left element of the 2d signal range.
   ** \param I_bot RandomAccessIterator2d. on one past-the-end element of the 2d signal range.
   ** \param k1_first A RandomAccessIterator on the first element of the horizontal kernel.
   ** \param k1_last A RandomAccessIterator on one past-the-end element of the horizontal kernel.
   ** \param ksize_left1 kernel size left to the "center" of the horizontal kernel
   ** \param ksize_right1 kernel size right to the "center" of the horizontal kernel
   ** \param bt1 slip::BORDER_TREATMENT associated with the horizontal kernel:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \param k2_first A RandomAccessIterator on the first element of the vertical kernel.
   ** \param k2_last A RandomAccessIterator on one past-the-end element of the vertical kernel.
   ** \param ksize_left2 kernel size left to the "center" of the vertical kernel
   ** \param ksize_right2 kernel size right to the "center" of the vertical kernel
   ** \param bt2 slip::BORDER_TREATMENT associated with the vertical kernel:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \pre [I_up,I_bot) must be valid.
   ** \par Example:
   ** \code
   ** slip::GrayscaleImage<float> I;
   ** I.read("lena.gif");
   ** slip::GrayscaleImage<float> Result(I.rows(),I.cols());
   ** slip::block<float,3> k1 = {1.0, 0.0, -1.0};
   ** slip::block<float,3> k2 = {1.0,2.0,1.0};
   ** std::size_t size = k1.size()/2;
   ** slip::separable_convolution2d(I.upper_left(),I.bottom_right(),
   ** 				   k1.begin(),k1.end(),
   ** 				   size,size,
   ** 				   slip::BORDER_TREATMENT_NEUMANN,
   ** 				   k2.begin(),k2.end(),
   ** 				   size,size,
   ** 				   slip::BORDER_TREATMENT_NEUMANN);
   ** \endcode
   */
template <typename RandomAccessIterator2d1, 
	  typename KernelIter1,
	  typename KernelIter2>
inline
void separable_convolution2d(RandomAccessIterator2d1 I_up,
			     RandomAccessIterator2d1 I_bot,
			     KernelIter1 k1_first, 
			     KernelIter1 k1_last,
			     std::size_t ksize_left1,
			     std::size_t ksize_right1,
			     slip::BORDER_TREATMENT bt1,
			     KernelIter2 k2_first, 
			     KernelIter2 k2_last,
			     std::size_t ksize_left2,
			     std::size_t ksize_right2,
			     slip::BORDER_TREATMENT bt2)
{

  typename RandomAccessIterator2d1::difference_type size2d = 
    I_bot - I_up;
  typedef typename RandomAccessIterator2d1::size_type size_type;
  typedef typename RandomAccessIterator2d1::value_type value_type;
  const size_type nb_rows = size2d[0];
  const size_type nb_cols = size2d[1];

  //slip::Array2d<value_type> Atmp(nb_rows,nb_cols);
  slip::Array<value_type> row_tmp(nb_cols);
  for(size_type i = 0; i < nb_rows; ++i)
    {
      std::copy(I_up.row_begin(i),I_up.row_end(i),row_tmp.begin());
      slip::same_convolution(row_tmp.begin(),row_tmp.end(),
			     k1_first,k1_last,
			     ksize_left1,
			     ksize_right1,
			     I_up.row_begin(i),
			     bt1);
    }
  slip::Array<value_type> col_tmp(nb_rows);
  for(size_type j = 0; j < nb_cols; ++j)
    {
      std::copy(I_up.col_begin(j),I_up.col_end(j),col_tmp.begin());
      slip::same_convolution(col_tmp.begin(),col_tmp.end(),
			     k2_first,k2_last,
			     ksize_left2,
			     ksize_right2,
			     I_up.col_begin(j),
			     bt2);
    }
  
}


 /**
   ** \brief Computes the separable 2d convolution of signal using the fft algorithm.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param I_up RandomAccessIterator2d on the upper_left element of the 2d signal range.
   ** \param I_bot RandomAccessIterator2d. on one past-the-end element of the 2d signal range.
   ** \param k1_first A RandomAccessIterator on the first element of the horizontal kernel.
   ** \param k1_last A RandomAccessIterator on one past-the-end element of the horizontal kernel.

   ** \param k2_first A RandomAccessIterator on the first element of the vertical kernel.
   ** \param k2_last A RandomAccessIterator on one past-the-end element of the vertical kernel.
   ** \param R_up RandomAccessIterator2d on the upper_left element of the 2d convolved signal range.
   ** 
   ** \pre Size of the two range must be the same.
   ** \par Example:
   ** \code
   ** slip::GrayscaleImage<float> I;
   ** I.read("lena.gif");
   ** slip::GrayscaleImage<float> Result(I.rows(),I.cols());
   ** slip::block<float,3> k1 = {1.0, 0.0, -1.0};
   ** slip::block<float,3> k2 = {1.0,2.0,1.0};
   ** std::size_t size = k1.size()/2;
   ** slip::separable_fft_convolution2d(I.upper_left(),I.bottom_right(),
   **			       k1.begin(),k1.end(),
   **			       k2.begin(),k2.end(),
   **			       Result.upper_left());
   ** \endcode
   */
template <typename RandomAccessIterator2d1, 
	  typename KernelIter1,
	  typename KernelIter2,
	  typename RandomAccessIterator2d2>
inline
void separable_fft_convolution2d(RandomAccessIterator2d1 I_up,
				 RandomAccessIterator2d1 I_bot,
				 KernelIter1 k1_first, 
				 KernelIter1 k1_last,
				 KernelIter2 k2_first, 
				 KernelIter2 k2_last,
				 RandomAccessIterator2d2 R_up)
{
  typename RandomAccessIterator2d1::difference_type size2d = 
    I_bot - I_up;
  typedef typename RandomAccessIterator2d1::size_type size_type;
  typedef typename RandomAccessIterator2d1::value_type value_type;
  size_type nb_rows = size2d[0];
  size_type nb_cols = size2d[1];
  size_type size1 = (k1_last - k1_first);
  size_type size2 = (k2_last - k2_first);
  size_type size2o2 = size2/2;
  slip::Array2d<value_type> Atmp(nb_rows+size2-1,nb_cols+size1-1);
  slip::Array<value_type> Vtmp(Atmp.cols());

  for(size_t i = 0; i < nb_rows; ++i)
    {
      slip::fft_convolution(I_up.row_begin(i),I_up.row_end(i),
			    k1_first,k1_last,
			    Atmp.row_begin(i),Atmp.row_end(i));
    }
  for(size_t j = 0; j < nb_cols; ++j)
    {
      slip::fft_convolution(Atmp.col_begin(j)+size2o2,Atmp.col_end(j)-size2o2,
			    k2_first,k2_last,
			    Vtmp.begin(),Vtmp.end());
      std::copy(Vtmp.begin()+size2o2,Vtmp.end()-size2o2,R_up.col_begin(j));
    }
}
  /**
   ** \brief Computes the fft convolution of a 2d signal.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param Signal  2D Container of the input Signal
   ** \param kernel1  1D Container of the horizontal kernel
   ** \param kernel2  1D Container of the vertical kernel
   ** \param Result  2D Container of the convolution result
   ** \pre Result.rows() == Signal.rows()
   ** \pre Result.cols() == Signal.cols()
   
   ** \par Example:
   ** \code
   ** slip::GrayscaleImage<float> I;
   ** I.read("lena.png");
   ** slip::GrayscaleImage<float> Result(I.rows(),I.cols());
   ** slip::block<float,3> k1 = {1.0/4.0,2.0/4.0,1.0/4.0};
   ** slip::block<float,3> k2 = {1.0/4.0,2.0/4.0,1.0/4.0};
   ** slip::separable_fft_convolution2d(Signal,
   **                                   k1,k2,
   **                                   Result);
   ** \endcode
   */

  template <typename Container1, 
	    typename Kernel1, 
	    typename Kernel2,
	    typename Container2> 
  inline void separable_fft_convolution2d(const Container1& Signal,
					  const Kernel1& kernel1, 
					  const Kernel2& kernel2,
					  Container2& Result)


  {
    slip::separable_fft_convolution2d(Signal.upper_left(),
				      Signal.bottom_right(),
				      kernel1.begin(),kernel1.end(),
				      kernel2.begin(),kernel2.end(),
				      Result.upper_left());
  }


   /**
   ** \brief Computes the valid convolution of 2d signal by a 2d-kernel.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param S_up A RandomAccessIterator2d.
   ** \param S_bot  A RandomAccessIterator2d.
   ** \param kernel_up A RandomAccessIterator2d.
   ** \param kernel_bot A RandomAccessIterator2d.
   ** \param ksize_left Number of elements left to the central point of the mask.
   ** \param ksize_right Number of elements right to the central point of the mask.
   ** \param ksize_up Number of elements up to the central point of the mask.
   ** \param ksize_bot Number of elements bottom to the central point of the mask
   ** \param R_up A RandomAccessIterator2d.
   ** \param R_bot A RandomAccessIterator2d.
   ** \pre (R_bot - R_up)[0] == ((S_bot - S_up)[0] - (kernel_bot - kernel_up)[0] + 1)
   ** \pre (R_bot - R_up)[1] == ((S_bot - S_up)[1] - (kernel_bot - kernel_up)[1] + 1)
   ** \pre (kernel_bot - kernel_up)[1] == ksize_left + ksize_right + 1
   ** \pre (kernel_bot - kernel_up)[0] == ksize_top + ksize_up  + 1
   ** \image html asymmetric_kernel2d.png "asymmetric 2d kernel support"
   ** \image latex asymmetric_kernel2d.eps "asymmetric 2d kernel support" width=5cm
   **  \par Description:
   **  Only non zero-padded points (kernel is fully inside) are computed
   **  if the signal size is NxP, 
   **  and the kernel size is 
   **  KxL = (ksize_left + ksize_right + 1) x (ksize_top + ksize_up  + 1), 
   **  the size of the result will be 
   **  (N - (ksize_left + ksize_right)) x (P - (ksize_top + ksize_up))
   **
   ** \par Example:
   ** \code
   ** slip::Array2d<float> A(8,10);
   ** slip::iota(A.begin(),A.end(),1.0);
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Array2d<float> Mask2d(3,4);
   ** slip::iota(Mask2d.begin(),Mask2d.end(),10.0,1.0);
   ** std::cout<<"Mask = \n"<<Mask2d<<std::endl;
   ** slip::Array2d<float> ConvAv(A.rows()-Mask2d.rows()+1,A.cols() - Mask2d.cols()+1);
   ** std::cout<<ConvAv.rows()<<"x"<<ConvAv.cols()<<std::endl;
   ** slip::valid_convolution2d(A.upper_left(),A.bottom_right(),
   **		                Mask2d.upper_left(),Mask2d.bottom_right(),
   **		                2,1,
   **		                0,2,
   **		                ConvAv.upper_left(),ConvAv.bottom_right());
   ** std::cout<<"ConvAv = \n"<<ConvAv<<std::endl;
   ** \endcode
   */
  template <typename SrcIter2d, 
	    typename KernelIter2d, 
	    typename ResIter2d>
  inline
  void valid_convolution2d(SrcIter2d S_up,
			   SrcIter2d S_bot,
			   KernelIter2d kernel_up,
			   KernelIter2d kernel_bot,
			   std::size_t ksize_left,
			   std::size_t ksize_right,
			   std::size_t ksize_up,
			   std::size_t ksize_bot,
			   ResIter2d R_up,
			   ResIter2d R_bot)
  {

    assert((R_bot - R_up)[0] == ((S_bot - S_up)[0] - (kernel_bot - kernel_up)[0] + 1));
    assert((R_bot - R_up)[1] == ((S_bot - S_up)[1] - (kernel_bot - kernel_up)[1] + 1));
    assert(static_cast<int>((kernel_bot - kernel_up)[1]) == static_cast<int>(ksize_left + ksize_right + 1));
    assert(static_cast<int>((kernel_bot - kernel_up)[0]) == static_cast<int>(ksize_up + ksize_bot + 1));
     std::size_t k_rows = static_cast<std::size_t>((kernel_bot - kernel_up)[0]);
    std::size_t k_cols = static_cast<std::size_t>((kernel_bot - kernel_up)[1]);
    std::size_t r_rows = static_cast<std::size_t>((R_bot - R_up)[0]);
    std::size_t r_cols = static_cast<std::size_t>((R_bot - R_up)[1]);

  
    typedef typename SrcIter2d::value_type value_type;
    typedef typename KernelIter2d::value_type k_value_type;
   
    //temporary array used to compute the reverse kernel
    slip::DPoint2d<int> dp(1,0);
    std::reverse_iterator<KernelIter2d> k_rupper_left = std::reverse_iterator<KernelIter2d>(kernel_bot-dp);
    std::reverse_iterator<KernelIter2d> k_rbottom_right = std::reverse_iterator<KernelIter2d>(kernel_up);
    
    slip::Array2d<k_value_type> reverse_kernel(k_rows,k_cols);
    std::copy(k_rupper_left,k_rbottom_right,reverse_kernel.upper_left());

    // apply the convolution on a point where the kernel does 
    // fit in the signal
     for(std::size_t i = 0, ii = ksize_bot; i < r_rows; ++i, ++ii)
     {
       for(std::size_t j = 0, jj = ksize_right; j < r_cols; ++j, ++jj)
       {
	 for(std::size_t k = 0, kk = (ii - ksize_bot); k < k_rows; ++k, ++kk)
           {
	     *(R_up.row_begin(i)+j)+=std::inner_product(S_up.row_begin(kk)+j,
 							S_up.row_begin(kk)+j+k_cols,
							reverse_kernel.row_begin(k),value_type(0));
           }
       }
     }
  }


  /**
   ** \brief Computes the same convolution of 2d signal by a 2d-kernel
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param S_up A RandomAccessIterator2d.
   ** \param S_bot  A RandomAccessIterator2d.
   ** \param kernel_up A RandomAccessIterator2d.
   ** \param kernel_bot A RandomAccessIterator2d.
   ** \param ksize_left Number of elements left to the central point of the mask.
   ** \param ksize_right Number of elements right to the central point of the mask.
   ** \param ksize_up Number of elements up to the central point of the mask.
   ** \param ksize_bot Number of elements bottom to the central point of the mask
   ** \param R_up A RandomAccessIterator2d.
   ** \param R_bot A RandomAccessIterator2d.
   ** \param bt slip::BORDER_TREATMENT:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 (default)
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \image html asymmetric_kernel2d.png "asymmetric 2d kernel support"
   ** \image latex asymmetric_kernel2d.eps "asymmetric 2d kernel support" width=5cm
   ** \pre (R_bot - R_up)[0] == (S_bot - S_up)[0]
   ** \pre (R_bot - R_up)[1] == (S_bot - S_up)[1]
   ** \pre (kernel_bot - kernel_up)[1] == ksize_left + ksize_right + 1
   ** \pre (kernel_bot - kernel_up)[0] == ksize_top + ksize_up  + 1
   ** \par Example:
   ** \code
   ** slip::Array2d<float> A(8,10);
   ** slip::iota(A.begin(),A.end(),1.0);
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Array2d<float> Mask2d(4,4);
   ** slip::iota(Mask2d.begin(),Mask2d.end(),10.0,1.0);
   ** std::cout<<"Mask = \n"<<Mask2d<<std::endl;
   ** slip::Array2d<float> ConvAs(A.rows(),A.cols());
   ** slip::same_convolution2d(A.upper_left(),A.bottom_right(),
   **		               Mask2d.upper_left(),Mask2d.bottom_right(),
   **		               1,2,
   **		               2,1,
   **		               ConvAs.upper_left(),ConvAs.bottom_right(),
   **		               slip::BORDER_TREATMENT_NEUMANN);
   ** std::cout<<"ConvAs NEUMANN 1,2, 2,1 = \n"<<ConvAs<<std::endl;
   ** \endcode
   */
  template <typename SrcIter2d, 
	    typename KernelIter2d, 
	    typename ResIter2d>
  inline
  void same_convolution2d(SrcIter2d S_up,
			  SrcIter2d S_bot,
			  KernelIter2d kernel_up,
			  KernelIter2d kernel_bot,
			  int ksize_left,
			  int ksize_right,
			  int ksize_up,
			  int ksize_bot,
			  ResIter2d R_up,
			  ResIter2d R_bot,
			  slip::BORDER_TREATMENT bt = slip::BORDER_TREATMENT_ZERO_PADDED)
  {

    assert((R_bot - R_up)[0] == ((S_bot - S_up)[0]));
    assert((R_bot - R_up)[1] == ((S_bot - S_up)[1]));
    assert(static_cast<int>((kernel_bot - kernel_up)[1]) == static_cast<int>(ksize_left + ksize_right + 1));
    assert(static_cast<int>((kernel_bot - kernel_up)[0]) == static_cast<int>(ksize_up + ksize_bot + 1));
    int s_rows = static_cast<int>((S_bot - S_up)[0]);
    int s_cols = static_cast<int>((S_bot - S_up)[1]);
    int k_rows = static_cast<int>((kernel_bot - kernel_up)[0]);
    int k_cols = static_cast<int>((kernel_bot - kernel_up)[1]);
    int r_rows = static_cast<int>((R_bot - R_up)[0]);
    int r_cols = static_cast<int>((R_bot - R_up)[1]);

  
    typedef typename SrcIter2d::value_type value_type;
    typedef typename KernelIter2d::value_type k_value_type;
   
    //temporary array used to compute the reverse kernel
    slip::DPoint2d<int> dp(1,0);
    std::reverse_iterator<KernelIter2d> k_rupper_left = std::reverse_iterator<KernelIter2d>(kernel_bot-dp);
    std::reverse_iterator<KernelIter2d> k_rbottom_right = std::reverse_iterator<KernelIter2d>(kernel_up);
    
    slip::Array2d<k_value_type> reverse_kernel(k_rows,k_cols);
    std::copy(k_rupper_left,k_rbottom_right,reverse_kernel.upper_left());

    // apply the convolution on a point where the kernel does 
    // fit in the signal
    for(int ii = ksize_bot; ii < (r_rows - ksize_up); ++ii)
     {
       for(int j = 0, jj = ksize_right; jj < (r_cols - ksize_left); ++j, ++jj)
       {
	 value_type tmp = value_type();
	 for(int k = 0, kk = (ii - ksize_bot); k < k_rows; ++k, ++kk)
           {
	     tmp += std::inner_product(S_up.row_begin(kk)+j,
				       S_up.row_begin(kk)+j+k_cols,
				       reverse_kernel.row_begin(k),
				       value_type());
           }
	 *(R_up.row_begin(ii)+jj) = tmp;
       }
     }
  

    //border treatment
    switch(bt)
      {
      case slip::BORDER_TREATMENT_AVOID: break;
      case slip::BORDER_TREATMENT_NEUMANN:
	{
	//up
	for(int ii = 0; ii < ksize_bot; ++ii)
	  {
	    for(int j = 0,jj = ksize_right; jj < (r_cols - ksize_left); ++j, ++jj)
	      {
		value_type tmp = value_type();
		for(int k = 0; k < (ksize_bot-ii); ++k)
		  {
		    tmp += std::inner_product(S_up.row_begin(0)+j,
					      S_up.row_begin(0)+j+k_cols,
					      reverse_kernel.row_begin(k),
					      value_type());
		  }
	
		for(int k = (ksize_bot-ii), kk = 0; k < k_rows; ++k, ++kk)
		  {
		  
		    tmp += std::inner_product(S_up.row_begin(kk)+j,
					      S_up.row_begin(kk)+j+k_cols,
					      reverse_kernel.row_begin(k),
					      value_type());
		  }
		
		*(R_up.row_begin(ii)+jj) = tmp;
		
	      }
	  }
	
	//bottom
	for(int i = 0, ii = (r_rows - ksize_up); ii < r_rows; ++i, ++ii)
	  {
	    for(int j = 0, jj = ksize_right; jj < (r_cols - ksize_left); ++j, ++jj)
	      {
		value_type tmp = value_type();
		for(int kk = (ii - ksize_bot), k = 0; kk < r_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(S_up.row_begin(kk)+j,
					      S_up.row_begin(kk)+j+k_cols,
					      reverse_kernel.row_begin(k),
					      value_type());
		  }
		for(int k = k_rows-1; k > (k_rows - 2 - i); --k)

		  {
		    tmp += std::inner_product(S_up.row_begin(r_rows-1)+j,
					      S_up.row_begin(r_rows-1)+j+k_cols,
					      reverse_kernel.row_begin(k),
					      value_type());
		    
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
		
	      }
	  }
	//right
	for(int ii = ksize_bot; ii < (r_rows - ksize_up); ++ii)
	  {
	    for(int j = (r_cols - ksize_left - ksize_right), jj = (r_cols - ksize_left),cpt=0; jj < r_cols; ++j, ++jj,++cpt)
	      {
		value_type tmp = value_type();
		for(int k = 0, kk = (ii - ksize_bot); k < k_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(S_up.row_begin(kk)+j,
					      S_up.row_end(kk),
					      reverse_kernel.row_begin(k),
					      value_type());
		    
		  }
		for(int c = (k_cols - 1); c >= (k_cols - 1-cpt); --c)
		  {
		     tmp += std::inner_product(reverse_kernel.col_begin(c),
					       reverse_kernel.col_end(c),
					       S_up.col_begin(s_cols-1)+(ii-ksize_bot),
					       value_type());
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
	//left
	for(int ii = ksize_bot; ii < (r_rows - ksize_up); ++ii)
	  {
	    for(int j = ksize_right, jj = 0; jj < ksize_right; --j, ++jj)
	      {
		value_type tmp = value_type();
		 for(int k = 0, kk = (ii - ksize_bot); k < k_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(reverse_kernel.row_begin(k) + j,
					      reverse_kernel.row_end(k),
					      S_up.row_begin(kk),
					      value_type());
		  }
		 for(int c = (j-1); c >= 0; --c)
		  {
		     tmp += std::inner_product(reverse_kernel.col_begin(c),
					       reverse_kernel.col_end(c),
					       S_up.col_begin(0)+(ii-ksize_bot),
					       value_type());
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
	//upper_left
	//temporary array to extend S
	slip::Array2d<value_type> Stmp(k_rows+ksize_bot-1,k_cols+ksize_right-1);
	int stmp_rows = static_cast<int>(Stmp.rows());
	int stmp_cols = static_cast<int>(Stmp.cols());
	for(int i = ksize_bot, ii = 0 ; i < stmp_rows; ++i, ++ii)
	  {
	    std::copy(S_up.row_begin(ii),
		      S_up.row_begin(ii) + (k_cols - 1),
		      Stmp.row_begin(i) + ksize_right);
	  }
	for(int i = 0; i < ksize_bot; ++i)
	  {
	    std::copy(S_up.row_begin(0),
		      S_up.row_begin(0) + (k_cols - 1),
		      Stmp.row_begin(i) + ksize_right);
	  }
	for(int j = 0; j < ksize_right; ++j)
	  {
	    std::copy(Stmp.col_begin(ksize_right),
		      Stmp.col_end(ksize_right),
		      Stmp.col_begin(j));
	  }
	//compute the convolution
	for(int ii = 0; ii < ksize_bot; ++ii)
	  {
	    for(int j = 0,jj = 0; jj < ksize_right; ++jj, ++j)
	      {
		value_type tmp = value_type();
		for(int k = 0, kk = ii; k < k_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(reverse_kernel.row_begin(k),
					      reverse_kernel.row_end(k),
					      Stmp.row_begin(kk)+j,
					      value_type());
		  }
				
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
	
	//bottom_right
	//temporary array to extend S
	Stmp.resize(ksize_up+k_rows-1,ksize_left+k_cols-1);
	stmp_rows = static_cast<int>(Stmp.rows());
	stmp_cols = static_cast<int>(Stmp.cols());
	int offsetj = (s_cols - k_cols + 1);

	for(int i = 0, ii = ((r_rows - 1) - (ksize_bot + ksize_up - 1)) ; i < (ksize_up + 1); ++i, ++ii)
	  {
	      std::copy(S_up.row_begin(ii) + offsetj,
			S_up.row_end(ii),
			Stmp.row_begin(i));
	  }
	for(int i = (ksize_up + 1); i < stmp_rows; ++i)
	  {
	    std::copy(S_up.row_begin(s_rows - 1) + offsetj,
		      S_up.row_end(s_rows - 1),
		      Stmp.row_begin(i));
	  }
	
	for(int j = stmp_cols-ksize_left; j < stmp_cols ; ++j)
	  {
	    std::copy(Stmp.col_begin(stmp_cols-1-ksize_left),
		      Stmp.col_end(stmp_cols-1-ksize_left),
		      Stmp.col_begin(j));
	  }

	//computes the convolution
	for(int ii = (r_rows - ksize_up), i = 0; ii < r_rows; ++ii,++i)
	  {
	    for(int j = 0, jj = (r_cols - ksize_left); jj < r_cols; ++j, ++jj)
	      {
		value_type tmp = value_type();
		for(int kk = i, k = 0; k < k_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(reverse_kernel.row_begin(k),
					      reverse_kernel.row_end(k),
					      Stmp.row_begin(kk)+j,
					      value_type());
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
	
	//upper_right
	//temporary array to extend S
	Stmp.resize(k_rows + ksize_bot - 1,k_cols + ksize_left - 1);
	stmp_rows = static_cast<int>(Stmp.rows());
	stmp_cols = static_cast<int>(Stmp.cols());
	for(int i = ksize_bot, ii = 0 ; i < stmp_rows; ++i, ++ii)
	  {
	    std::copy(S_up.row_begin(ii)+ offsetj,
		      S_up.row_end(ii),
		      Stmp.row_begin(i));
	  }

	for(int i = 0; i < ksize_bot; ++i)
	  {
	    std::copy(S_up.row_begin(0) + offsetj,
		      S_up.row_end(0),
		      Stmp.row_begin(i));
	  }
	for(int j = (k_cols - 1); j < stmp_cols; ++j)
	  {
	    std::copy(Stmp.col_begin(k_cols - 2),
		      Stmp.col_end(k_cols - 2),
		      Stmp.col_begin(j));
	  }


	//computes the convolution
	for(int ii = 0; ii < ksize_bot; ++ii)
	  {
	    for(int j = 0, jj = (r_cols - ksize_left); jj < r_cols;++j,++jj)
	      {
		value_type tmp = value_type();
		for(int k = 0, kk = ii; k < k_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(reverse_kernel.row_begin(k),
					      reverse_kernel.row_end(k),
					      Stmp.row_begin(kk)+j,
					      value_type());
		   
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }

	//bottom_left
	//temporary array to extend S
	Stmp.resize(k_rows + ksize_up - 1,k_cols + ksize_right - 1);
	stmp_rows = static_cast<int>(Stmp.rows());
	stmp_cols = static_cast<int>(Stmp.cols());
	offsetj = (s_cols - 1 - ksize_left);
	for(int i = 0, ii = ((r_rows - 1) - (ksize_bot + ksize_up - 1)) ; i < (ksize_up + 1); ++i, ++ii)
	  {
	    std::copy(S_up.row_begin(ii),
		      S_up.row_begin(ii) + (k_cols - 1),
		      Stmp.row_begin(i) + ksize_right);
	  }
	for(int i = (ksize_up + 1); i < stmp_rows; ++i)
	  {
	    std::copy(S_up.row_begin(r_rows - 1),
		      S_up.row_begin(r_rows - 1)+ (k_cols - 1),
		      Stmp.row_begin(i)+ ksize_right);
	  }
	for(int j = 0; j < ksize_right; ++j)
	  {
	    std::copy(Stmp.col_begin(ksize_right),
		      Stmp.col_end(ksize_right),
		      Stmp.col_begin(j));
	  }
 	//computes the convolution
	for(int ii = (r_rows - ksize_up), i = 0; ii < r_rows; ++ii, ++i)
	  {
	    for(int j = 0, jj = 0; jj < ksize_right; ++j, ++jj)
	      {
		value_type tmp = value_type();
		for(int kk = i, k = 0; k < k_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(reverse_kernel.row_begin(k),
					      reverse_kernel.row_end(k),
					      Stmp.row_begin(kk)+j,
					      value_type());
		 
		  
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
	
	}
	break;
      case slip::BORDER_TREATMENT_ZERO_PADDED: 
	//up
	for(int ii = 0; ii < ksize_bot; ++ii)
	  {
	    for(int j = 0, jj = ksize_right; jj < (r_cols - ksize_left); ++j, ++jj)
	      {
		value_type tmp = value_type();
		for(int k = (ksize_bot - ii), kk = 0; k < k_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(S_up.row_begin(kk)+j,
					      S_up.row_begin(kk)+j+k_cols,
					      reverse_kernel.row_begin(k),
					      value_type());
		  }
		
		*(R_up.row_begin(ii)+jj) = tmp;
		
	      }
	  }
 	//bottom
	for(int ii = (r_rows - ksize_up); ii < r_rows; ++ii)
	  {
	    for(int j = 0, jj = ksize_right; jj < (r_cols - ksize_left); ++j, ++jj)
	      {
		
		value_type tmp = value_type();
		for(int kk = (ii - ksize_bot), k = 0; kk < r_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(S_up.row_begin(kk)+j,
					      S_up.row_begin(kk)+j+k_cols,
					      reverse_kernel.row_begin(k),
					      value_type());
		  }
		
		*(R_up.row_begin(ii)+jj) = tmp;
		
	      }
	  }

	//right
	for(int ii = ksize_bot; ii < (r_rows - ksize_up); ++ii)
	  {
	    for(int j = (r_cols - ksize_left - ksize_right), jj = (r_cols - ksize_left); jj < r_cols; ++j, ++jj)
	      {
		value_type tmp = value_type();
		for(int k = 0, kk = (ii - ksize_bot); k < k_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(S_up.row_begin(kk)+j,
					      S_up.row_end(kk),
					      reverse_kernel.row_begin(k),
					      value_type());
		    
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }

	//left
	for(int ii = ksize_bot; ii < (r_rows - ksize_up); ++ii)
	  {
	    for(int j = ksize_right, jj = 0; jj < ksize_right; --j, ++jj)
	      {
		 value_type tmp = value_type();
		 for(int k = 0, kk = (ii - ksize_bot); k < k_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(reverse_kernel.row_begin(k) + j,
					      reverse_kernel.row_end(k),
					      S_up.row_begin(kk),
					      value_type());
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
	//upper_left
	for(int ii = 0; ii < ksize_bot; ++ii)
	  {
	    for(int j = ksize_right, jj = 0; jj < ksize_right; --j, ++jj)
	      {
		value_type tmp = value_type();
		for(int k = (ksize_bot - ii), kk = 0; k < k_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(reverse_kernel.row_begin(k) + j,
					      reverse_kernel.row_end(k),
					      S_up.row_begin(kk),
					      value_type());
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
  	
	//upper_right
	for(int ii = 0; ii < ksize_bot; ++ii)
	  {
	    for(int j = (r_cols - ksize_left - ksize_right), jj = (r_cols - ksize_left); jj < r_cols; ++j, ++jj)
	      {
		//std::cout<<"ii = "<<ii<<" jj = "<<jj<<std::endl;
		value_type tmp = value_type();
		for(int k = (ksize_bot - ii), kk = 0; k < k_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(S_up.row_begin(kk)+j,
					      S_up.row_end(kk),
					      reverse_kernel.row_begin(k),
					      value_type());
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }

	//bottom_right
	for(int ii = (r_rows - ksize_up); ii < r_rows; ++ii)
	  {
	    for(int j = (r_cols - ksize_left - ksize_right), jj = (r_cols - ksize_left); jj < r_cols; ++j, ++jj)
	      {
	
		value_type tmp = value_type();
		for(int kk = (ii - ksize_bot), k = 0; kk < r_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(S_up.row_begin(kk)+j,
					      S_up.row_end(kk),
					      reverse_kernel.row_begin(k),
					      value_type());
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
	//	bottom_left
	for(int ii = (r_rows - ksize_up); ii < r_rows; ++ii)
	  {
	    for(int j = ksize_right, jj = 0; jj < ksize_right; --j, ++jj)
	      {
		value_type tmp = value_type();
		for(int kk = (ii - ksize_bot), k = 0; kk < r_rows; ++k, ++kk)
		  {
		    tmp += std::inner_product(reverse_kernel.row_begin(k) + j,
					      reverse_kernel.row_end(k),
					      S_up.row_begin(kk),
					      value_type());
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
	break;
      case slip::BORDER_TREATMENT_CIRCULAR:
	{

	  //up
	  for(unsigned int ii = 0; ii < static_cast<unsigned int>(ksize_bot); ++ii)
	    {
	      for(unsigned int j = 0,jj = static_cast<unsigned int>(ksize_right); jj < static_cast<unsigned int>(r_cols - ksize_left); ++j, ++jj)
		{
		  value_type tmp = value_type();
		  unsigned int jjj = j%s_cols;
		  for(unsigned int k = 0, kk = (ii - static_cast<unsigned int>(ksize_bot)); k < static_cast<unsigned int>(k_rows); ++k, ++kk)
		    {
		      unsigned int kkk = kk%s_rows;
		      tmp += std::inner_product(S_up.row_begin(kkk)+jjj,
						S_up.row_begin(kkk)+jjj+k_cols,
						reverse_kernel.row_begin(k),
						value_type());
		    } 
		  *(R_up.row_begin(ii)+jj) = tmp;
		}
	    }
	  //bottom
	for(unsigned int ii = static_cast<unsigned int>(r_rows - ksize_up); ii < static_cast<unsigned int>(r_rows); ++ii)
	  {
	    for(unsigned int j = 0, jj = static_cast<unsigned int>(ksize_right); jj < static_cast<unsigned int>(r_cols - ksize_left); ++j, ++jj)
	      {
		value_type tmp = value_type();
		unsigned int jjj = j%s_cols;
		for(unsigned int k = 0, kk = (ii - static_cast<unsigned int>(ksize_bot)); k < static_cast<unsigned int>(k_rows); ++k, ++kk)
		    {
		      unsigned int kkk = kk%s_rows;
		      tmp += std::inner_product(S_up.row_begin(kkk)+jjj,
						S_up.row_begin(kkk)+jjj+k_cols,
						reverse_kernel.row_begin(k),
						value_type());
		    } 
		  *(R_up.row_begin(ii)+jj) = tmp;
	      }
	  } 
	//right
for(unsigned int ii =  static_cast<unsigned int>(ksize_bot); ii < static_cast<unsigned int>(r_rows - ksize_up); ++ii)
	  {
	    for(unsigned int j = static_cast<unsigned int>(r_cols - ksize_left - ksize_right), jj = static_cast<unsigned int>(r_cols - ksize_left); jj < static_cast<unsigned int>(r_cols); ++j, ++jj)
	      {
		value_type tmp = value_type();
		slip::DPoint2d<int> d;
		for(unsigned int k = 0, kk = (ii - static_cast<unsigned int>(ksize_bot)); k < static_cast<unsigned int>(k_rows); ++k, ++kk)
		  {
		    d[0] = static_cast<int>(kk);
		    for(unsigned int l = 0, ll = (jj-static_cast<unsigned int>(ksize_right)); l < static_cast<unsigned int>(k_cols); ++l,++ll)
		      {
			d[1] = static_cast<int>(ll%s_cols);
			tmp += S_up[d] * reverse_kernel[k][l];
		      }
		  } 
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
	//left
for(unsigned int ii = static_cast<unsigned int>(ksize_bot); ii < static_cast<unsigned int>(r_rows - ksize_up); ++ii)
	  {
	    for(unsigned int j = static_cast<unsigned int>(ksize_right), jj = 0; jj < static_cast<unsigned int>(ksize_right); --j, ++jj)
	      {
		 value_type tmp = value_type();
		 slip::DPoint2d<int> d;
		 for(unsigned int k = 0, kk = (ii - static_cast<unsigned int>(ksize_bot)); k < static_cast<unsigned int>(k_rows); ++k, ++kk)
		  {
		    d[0] = static_cast<int>(kk);
		    for(unsigned int l = 0, ll = (s_cols - ksize_right+jj); l < static_cast<unsigned int>(k_cols); ++l,++ll)
		      {
			d[1] = static_cast<int>(ll%s_cols);
			tmp += S_up[d] * reverse_kernel[k][l];
		      }
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
	//upper_left
for(int ii = 0; ii < ksize_bot; ++ii)
  {
	    for(int j = ksize_right, jj = 0; jj < ksize_right; --j, ++jj)
	      {
		value_type tmp = value_type();
		slip::DPoint2d<int> d;
		for(unsigned int k = 0, kk = static_cast<unsigned int>(s_rows-ksize_bot+ii); k < static_cast<unsigned int>(k_rows); ++k, ++kk)
		  {
		    d[0] = static_cast<int>(kk%s_rows);
		    for(unsigned int l = 0, ll = static_cast<unsigned int>(s_cols - ksize_right+jj); l < static_cast<unsigned int>(k_cols); ++l,++ll)
		      {
			d[1] = static_cast<int>(ll%s_cols);
			tmp += S_up[d] * reverse_kernel[k][l];
		      }
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }

	//upper_right
	for(int ii = 0; ii < ksize_bot; ++ii)
	  {
	    for(int j = (r_cols - ksize_left - ksize_right), jj = (r_cols - ksize_left); jj < r_cols; ++j, ++jj)
	      {
		value_type tmp = value_type();
		slip::DPoint2d<int> d;
		for(int k = 0, kk = (s_rows-ksize_bot+ii); k < k_rows; ++k, ++kk)
		  {
		   d[0] = static_cast<int>(kk%s_rows);
		   for(unsigned int l = 0, ll = static_cast<unsigned int>(jj-ksize_right); l < static_cast<unsigned int>(k_cols); ++l,++ll)
		      {
			d[1] = static_cast<int>(ll%s_cols);
			tmp += S_up[d] * reverse_kernel[k][l];

		      }
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
	//bottom_right
	for(int ii = (r_rows - ksize_up); ii < r_rows; ++ii)
	  {
	    for(int j = (r_cols - ksize_left - ksize_right), jj = (r_cols - ksize_left); jj < r_cols; ++j, ++jj)
	      {
	
		value_type tmp = value_type();
		slip::DPoint2d<int> d;
		for(unsigned int kk = static_cast<unsigned int>(s_rows-ksize_bot+ii), k = 0; k < static_cast<unsigned int>(k_rows); ++k, ++kk)
		  {
		    d[0] = static_cast<int>(kk%s_rows);
		    for(unsigned int l = 0, ll = static_cast<unsigned int>(jj-ksize_right); l < static_cast<unsigned int>(k_cols); ++l,++ll)
		      {
			d[1] = static_cast<int>(ll%s_cols);
			tmp += S_up[d] * reverse_kernel[k][l];

		      }
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
	//	bottom_left
	for(int ii = (r_rows - ksize_up); ii < r_rows; ++ii)
	  {
	    for(int j = ksize_right, jj = 0; jj < ksize_right; --j, ++jj)
	      {
		value_type tmp = value_type();
		slip::DPoint2d<int> d;
		for(unsigned int kk = static_cast<unsigned int>(s_rows-ksize_bot+ii), k = 0; k < static_cast<unsigned int>(k_rows); ++k, ++kk)
		  {
		   d[0] = static_cast<int>(kk%s_rows);
		   for(unsigned int l = 0, ll = static_cast<unsigned int>(s_cols - ksize_right+jj); l < static_cast<unsigned int>(k_cols); ++l,++ll)
		      {
			d[1] = static_cast<int>(ll%s_cols);
			tmp += S_up[d] * reverse_kernel[k][l];

		      }
		  }
		*(R_up.row_begin(ii)+jj) = tmp;
	      }
	  }
	}
	break;
      default: std::cout<<"default"<<std::endl; 
      };
  }
 
 
  /**
   ** \brief Computes the same convolution of 2d signal by a symmetric 2d-kernel support.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param S_up A RandomAccessIterator2d.
   ** \param S_bot  A RandomAccessIterator2d.
   ** \param kernel_up A RandomAccessIterator2d.
   ** \param kernel_bot A RandomAccessIterator2d.
   ** \param R_up A RandomAccessIterator2d.
   ** \param R_bot A RandomAccessIterator2d.
   ** \param bt slip::BORDER_TREATMENT:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 (default)
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \image html symmetric_kernel2d.png "symmetric 2d kernel support"
   ** \image latex symmetric_kernel2d.eps "symmetric 2d kernel support" width=5cm
   ** \pre (R_bot - R_up)[0] == ((S_bot - S_up)[0] - (kernel_bot - kernel_up)[0] + 1)
   ** \pre (R_bot - R_up)[1] == ((S_bot - S_up)[1] - (kernel_bot - kernel_up)[1] + 1)
   ** \par Example:
   ** \code
   ** slip::Array2d<float> A(8,10);
   ** slip::iota(A.begin(),A.end(),1.0);
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Array2d<float> Mask2d(3,3);
   ** slip::iota(Mask2d.begin(),Mask2d.end(),10.0,1.0);
   ** std::cout<<"Mask = \n"<<Mask2d<<std::endl;
   ** slip::Array2d<float> ConvAs(A.rows(),A.cols());
   ** slip::same_convolution2d(A.upper_left(),A.bottom_right(),
   **		               Mask2d.upper_left(),Mask2d.bottom_right(),
   **		               ConvAs.upper_left(),ConvAs.bottom_right(),
   **		               slip::BORDER_TREATMENT_NEUMANN);
   ** std::cout<<"ConvAs NEUMANN = \n"<<ConvAs<<std::endl;
   ** \endcode
   */
  template <typename SrcIter2d, 
	    typename KernelIter2d, 
	    typename ResIter2d>
  inline
  void same_convolution2d(SrcIter2d S_up,
			  SrcIter2d S_bot,
			  KernelIter2d kernel_up,
			  KernelIter2d kernel_bot,
			  ResIter2d R_up,
			  ResIter2d R_bot,
			  slip::BORDER_TREATMENT bt = slip::BORDER_TREATMENT_ZERO_PADDED)
  {
    int k_rows = static_cast<int>((kernel_bot - kernel_up)[0] - 1);
    int k_cols = static_cast<int>((kernel_bot - kernel_up)[1] - 1);
    int k_rows_2 = k_rows / 2;
    int k_cols_2 = k_cols / 2;
    slip::same_convolution2d(S_up,S_bot,
			     kernel_up,kernel_bot,
			     k_cols - k_cols_2,k_cols_2,
			     k_rows - k_rows_2,k_rows_2,
			     R_up,R_bot,
			     bt);
  }

  /**
   ** \brief Computes the same convolution of 2d signal by a symmetric 2d-kernel.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/08/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param Signal  2D Container of the input Signal.
   ** \param Kernel  2D Container of the kernel.
   ** \param Result  2D Container of the convolution result.
   ** \param bt slip::BORDER_TREATMENT:
   ** \li slip::BORDER_TREATMENT_AVOID
   ** \li slip::BORDER_TREATMENT_ZERO_PADDED
   ** \li slip::BORDER_TREATMENT_NEUMANN
   ** \li slip::BORDER_TREATMENT_CIRCULAR
   ** \pre Result.size() == Signal.size()
   ** \image html symmetric_kernel2d.png "symmetric 2d kernel support"
   ** \image latex symmetric_kernel2d.eps "symmetric 2d kernel support" width=5cm
   ** \par Example:
   ** \code
   ** slip::Array2d<float> A(8,10);
   ** slip::iota(A.begin(),A.end(),1.0);
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Array2d<float> Mask2d(3,3);
   ** slip::iota(Mask2d.begin(),Mask2d.end(),10.0,1.0);
   ** std::cout<<"Mask = \n"<<Mask2d<<std::endl;
   ** slip::Array2d<float> ConvAs(A.rows(),A.cols());
   ** slip::same_convolution2d(A,Mask2d2,ConvAs,
   **		  slip::BORDER_TREATMENT_ZERO_PADDED);
   ** std::cout<<"ConvAs ZERO_PADDED symmetric mask = \n"<<ConvAs<<std::endl;
   ** ConvAs.fill(0.0f);
   ** slip::same_convolution2d(A,Mask2d2,ConvAs,
   **		  slip::BORDER_TREATMENT_NEUMANN);
   ** std::cout<<"ConvAs NEUMANN symmetric mask = \n"<<ConvAs<<std::endl;
   ** ConvAs.fill(0.0f);
   ** slip::same_convolution2d(A,Mask2d2,ConvAs,
   **		  slip::BORDER_TREATMENT_CIRCULAR);
   ** std::cout<<"ConvAs CIRCULAR symmetric mask = \n"<<ConvAs<<std::endl;
   **
   ** \endcode
   */
 template <typename Container2d1, 
	  typename Kernel2d, 
	  typename Container2d2>
  inline
  void same_convolution2d(const Container2d1& Signal,
			  const Kernel2d& Kernel,
			  Container2d2& Result,
			  slip::BORDER_TREATMENT bt)
  {
    slip::same_convolution2d(Signal.upper_left(),Signal.bottom_right(),
			     Kernel.upper_left(),Kernel.bottom_right(),
			     Result.upper_left(),Result.bottom_right(),
			     bt);
  }
  /**
   ** \brief Computes the full convolution of 2d signal by a 2d-kernel
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param S_up A RandomAccessIterator2d.
   ** \param S_bot  A RandomAccessIterator2d.
   ** \param kernel_up A RandomAccessIterator2d.
   ** \param kernel_bot A RandomAccessIterator2d.
   ** \param R_up A RandomAccessIterator2d.
   ** \param R_bot A RandomAccessIterator2d.
   ** \pre (R_bot-R_up)[0] = ((S_bot-S_up)[0] + (kernel_bot-kernel_up)[0] - 1)
   ** \pre (R_bot-R_up)[1] = ((S_bot-S_up)[1] + (kernel_bot-kernel_up)[1] - 1)
   **
   ** \par Example:
   ** \code
   ** slip::Array2d<float> A(8,10);
   ** slip::iota(A.begin(),A.end(),1.0);
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Array2d<float> Mask2d(3,4);
   ** slip::iota(Mask2d.begin(),Mask2d.end(),10.0,1.0);
   ** std::cout<<"Mask = \n"<<Mask2d<<std::endl;
   ** slip::Array2d<float> ConvAfull(A.rows()+Mask2d.rows()-1,
   **			A.cols()+Mask2d.cols()-1);
   ** slip::full_convolution2d(A.upper_left(),A.bottom_right(),
   **		  Mask2d.upper_left(),Mask2d.bottom_right(),
   **		  ConvAfull.upper_left(),ConvAfull.bottom_right());
   ** std::cout<<"ConvAfull = \n"<<ConvAfull<<std::endl;
   ** \endcode
   */
  template <typename SrcIter2d, 
	    typename KernelIter2d, 
	    typename ResIter2d>
  inline
  void full_convolution2d(SrcIter2d S_up,
			  SrcIter2d S_bot,
			  KernelIter2d kernel_up,
			  KernelIter2d kernel_bot,
			  ResIter2d R_up,
			  ResIter2d R_bot)
  {
    assert( (R_bot - R_up)[0] == ((S_bot - S_up)[0] + (kernel_bot - kernel_up)[0] - 1));
    assert( (R_bot - R_up)[1] == ((S_bot - S_up)[1] + (kernel_bot - kernel_up)[1] - 1));

    int s_rows = static_cast<int>((S_bot - S_up)[0]);
    int s_cols = static_cast<int>((S_bot - S_up)[1]);
    int k_rows = static_cast<int>((kernel_bot - kernel_up)[0]);
    int k_cols = static_cast<int>((kernel_bot - kernel_up)[1]);
  
    typedef typename SrcIter2d::value_type value_type;
 
    //temporary array used to compute the reverse kernel
    slip::DPoint2d<int> dp(1,0);
    std::reverse_iterator<KernelIter2d> k_rupper_left = std::reverse_iterator<KernelIter2d>(kernel_bot-dp);
    std::reverse_iterator<KernelIter2d> k_rbottom_right = std::reverse_iterator<KernelIter2d>(kernel_up);
    
    slip::Array2d<typename KernelIter2d::value_type> 
      reverse_kernel(k_rows,k_cols);
    std::copy(k_rupper_left,k_rbottom_right,reverse_kernel.upper_left()); 

   
    slip::DPoint2d<int> d(0,0);
    //up
    int imax = (k_rows - 1);
    int jmax = (s_cols - k_cols + 1);
    for(int i = 0, ii = 0;  i < imax; ++i,++ii)
      {
	d[0] = ii;
	for(int j = 0, jj =(k_cols-1); j < jmax; ++j, ++jj) 
	  {
	    d[1] = jj;
	    value_type tmp = value_type();
	    for(int k = (k_rows-1-i),kk = 0; k < k_rows; ++k,++kk)
	      {
		tmp += std::inner_product(reverse_kernel.row_begin(k),
					  reverse_kernel.row_end(k),
					  S_up.row_begin(kk)+j,
					  value_type(0));
	      }
	    R_up[d] = tmp;
	  }

      }
    

    //center
    imax = (s_rows - k_rows+1);
    jmax = (s_cols - k_cols + 1);
    for(int i = 0, ii = (k_rows -1);  i < imax; ++i,++ii)
      {
	d[0] = ii;
	for(int j = 0, jj =(k_cols-1); j < jmax; ++j, ++jj) 
	  {
	    d[1] = jj;
	    value_type tmp = value_type();
	    for(int k = 0; k < k_rows; ++k)
	      {
		tmp += std::inner_product(reverse_kernel.row_begin(k),
					  reverse_kernel.row_end(k),
					  S_up.row_begin(i+k)+j,
					  value_type(0));
	      }
	    R_up[d] = tmp;
	  }

      }

     //bottom
    jmax = (s_cols - k_cols + 1);
    for(int i = (s_rows - k_rows+1), ii = s_rows;  i < s_rows; ++i,++ii)
      {
	d[0] = ii;
	for(int j = 0, jj =(k_cols-1); j < jmax; ++j, ++jj) 
	  {
	    d[1] = jj;
	    value_type tmp = value_type();
	    for(int k = 0, kk = i; k < (s_rows-i); ++k,++kk)
	      {
		tmp += std::inner_product(reverse_kernel.row_begin(k),
					      reverse_kernel.row_end(k),
					      S_up.row_begin(kk)+j,
					      value_type(0));
	      }
	    R_up[d] = tmp;
	  }

      }
    
    //left
    imax = (s_rows - k_rows + 1);
    jmax =  k_cols - 1;
    for(int i = 0, ii = (k_rows -1);  i < imax; ++i,++ii)
      {
	d[0] = ii;
	for(int j = 0, jj =0; j < jmax; ++j, ++jj) 
	  {
	    d[1] = jj;
	    value_type tmp = value_type();
	    for(int k = 0; k < k_rows; ++k)
	      {
		tmp += std::inner_product(reverse_kernel.row_begin(k)
					  + (jmax - j),
					  reverse_kernel.row_end(k),
					  S_up.row_begin(i+k),
					  value_type(0));
	      }
	    R_up[d] = tmp;
	  }

      }

    //right
    imax = (s_rows - k_rows + 1);
    int jtmp = s_cols - k_cols;
    for(int i = 0, ii = (k_rows -1);  i < imax; ++i,++ii)
      {
	d[0] = ii;
	for(int j = (s_cols - k_cols + 1), jj =s_cols; j < s_cols;++j, ++jj) 
	  {
	    d[1] = jj;
	    value_type tmp = value_type();
	    for(int k = 0; k < k_rows; ++k)
	      {
		tmp += std::inner_product(reverse_kernel.row_begin(k),
					  reverse_kernel.row_end(k) - (j - jtmp),
					  S_up.row_begin(i+k)+j,
					  value_type(0));
	      }
	    R_up[d] = tmp;
	  }

      }

    //upper_left
    imax = k_rows - 1;
    jmax = k_cols - 1;
    for(int i = 0, ii = 0;  i < imax; ++i,++ii)
      {
	d[0] = ii;
	for(int j = 0, jj = 0; j < jmax; ++j, ++jj) 
	  {
	    d[1] = jj;
	    value_type tmp = value_type();
	    for(int k = (k_rows-1-i),kk = 0; k < k_rows; ++k,++kk)
	      {
		tmp += std::inner_product(reverse_kernel.row_begin(k)
					  + (jmax - j),
					  reverse_kernel.row_end(k),
					  S_up.row_begin(kk),
					  value_type(0));
	
	      }
	    R_up[d] = tmp;
	  }

      }

    //upper_right
    imax = k_rows - 1;
    for(int i = 0, ii = 0;  i < imax; ++i,++ii)
      {
	d[0] = ii;
	for(int j = (s_cols - k_cols + 1), jj = s_cols; j < s_cols; ++j, ++jj) 
	  {
	    d[1] = jj;
	    value_type tmp = value_type();
	    for(int k = (k_rows-1-i),kk = 0; k < k_rows; ++k,++kk)
	      {
		tmp += std::inner_product(reverse_kernel.row_begin(k),
					  reverse_kernel.row_end(k) 
					  - (j - jtmp),
					  S_up.row_begin(kk)+j,
					  value_type(0));
	      }
	    R_up[d] = tmp;
	  }

      }

    //bottom_right
    imax = (s_rows - k_rows + 1);
    jmax = (s_cols - k_cols + 1);
    for(int i = (s_rows - k_rows + 1), ii = s_rows;  i < s_rows; ++i,++ii)
      {
	d[0] = ii;
	for(int j = (s_cols - k_cols + 1), jj = s_cols; j < s_cols; ++j, ++jj) 
	  {
	    d[1] = jj;
	    value_type tmp = value_type();
	      for(int k = 0, kk = i; k < (s_rows-i); ++k,++kk)
	      {
		tmp += std::inner_product(reverse_kernel.row_begin(k),
					  reverse_kernel.row_end(k)- (j - jtmp),
					  S_up.row_begin(kk)+j,
					  value_type(0));
	      }
	    R_up[d] = tmp;
	  }

      }

    //bottom_left
    imax = (s_rows - k_rows+1);
    jmax =  k_cols - 1;
    for(int i = (s_rows - k_rows + 1), ii = s_rows;  i < s_rows; ++i,++ii)
      {
	d[0] = ii;
	for(int j = 0, jj =0; j < jmax; ++j, ++jj) 
	  {
	    d[1] = jj;
	    value_type tmp = value_type();
	    for(int k = 0, kk = i; k < (s_rows-i); ++k,++kk)
	      {
		tmp += std::inner_product(reverse_kernel.row_begin(k)
					  + (jmax - j),
					  reverse_kernel.row_end(k),
					  S_up.row_begin(kk),
					  value_type(0));
	      }
	    R_up[d] = tmp;
	  }

      }
    

  }

/**
   ** \brief Computes the standard crosscorrelation between two 2D sequences using fft2d.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/09/17
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in1_upper_left : A 2d input iterator (image).
   ** \param in1_bottom_right : A 2d input iterator (image).
   ** \param in2_upper_left : A 2d input iterator (mask).
   ** \param in2_bottom_right : A 2d input iterator (mask).
   ** \param out_upper_left : A 2d output iterator (result).
   ** \param out_bottom_right : A 2d output iterator (result).
   **
   ** \pre the value_type of the input sequences have to be real (not complex)
   ** \pre (in1_bottom_right - in1_upper_left) + (in2_bottom_right - in2_upper_left) - (1,1) = (out_bottom_right - out_upper_left)
   ** \par Example:
   ** \code
   ** slip::Array2d<float> A(8,10);
   ** slip::iota(A.begin(),A.end(),1.0);
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Array2d<float> Mask2d(4,4);
   ** slip::iota(Mask2d.begin(),Mask2d.end(),10.0,1.0);
   ** std::cout<<"Mask = \n"<<Mask2d<<std::endl;
   **  std::size_t convA_rows = A.rows()+ Mask2d.rows()-static_cast<std::size_t>(1);
   ** std::size_t convA_cols = A.cols()+ Mask2d.cols()-static_cast<std::size_t>(1);
   ** slip::Array2d<float> ConvA(convA_rows,convA_cols);
   ** slip::fft_convolution2d(A.upper_left(),A.bottom_right(),
   **                         Mask2d.upper_left(),Mask2d.bottom_right(),
   **                         ConvA.upper_left(),ConvA.bottom_right());
   ** std::cout<<"ConvA = \n"<<ConvA<<std::endl;
   **  \endcode
   */   
  template<typename InputIterator2d1, typename InputIterator2d2, typename OutputIterator2d>
  inline
  void fft_convolution2d(InputIterator2d1 in1_upper_left, 
			 InputIterator2d1 in1_bottom_right, 
			 InputIterator2d2 in2_upper_left, 
			 InputIterator2d2 in2_bottom_right, 
			 OutputIterator2d out_upper_left, 
			 OutputIterator2d out_bottom_right)
  {
    assert(((in1_bottom_right - in1_upper_left)[0]+(in2_bottom_right - in2_upper_left)[0] - 1) == (out_bottom_right - out_upper_left)[0]);
    assert(((in1_bottom_right - in1_upper_left)[1]+(in2_bottom_right - in2_upper_left)[1] - 1) == (out_bottom_right - out_upper_left)[1]);

    typename InputIterator2d1::difference_type size2din1 = in1_bottom_right - in1_upper_left;
    typename InputIterator2d2::difference_type size2din2 = in2_bottom_right - in2_upper_left;
    typename OutputIterator2d::difference_type size2dout = out_bottom_right - out_upper_left;
    
    typedef typename std::iterator_traits<InputIterator2d1>::value_type value_type;
    typedef  typename slip::lin_alg_traits<value_type>::value_type Real;
     
    std::size_t rows =  size2din1[0] + size2din2[0] - 1; 
    std::size_t cols =  size2din1[1] + size2din2[1] - 1; 
    //zero-padd Signal and Kernel data
    slip::Array2d<Real> Signal_zero_padded(rows,cols,Real());
    slip::Array2d<Real> Kernel_zero_padded(rows,cols,Real());
    slip::Box2d<int> signal_box(0,0,
				static_cast<int>(size2din1[0]-1),
				static_cast<int>(size2din1[1]-1));
    slip::Box2d<int> kernel_box(0,0,
				static_cast<int>(size2din2[0]-1),
				static_cast<int>(size2din2[1]-1));
    std::copy(in1_upper_left,in1_bottom_right,Signal_zero_padded.upper_left(signal_box));

    std::copy(in2_upper_left,in2_bottom_right,
	      Kernel_zero_padded.upper_left(kernel_box));

    slip::Array2d<std::complex<Real> > fft1(rows,cols,std::complex<Real>());
    slip::Array2d<std::complex<Real> > fft2(rows,cols,std::complex<Real>());
   
    //computes the two fft
    slip::real_fft2d(Signal_zero_padded,fft1);
    slip::real_fft2d(Kernel_zero_padded,fft2);
    //computes fft1*fft2
    slip::multiplies(fft1.begin(),fft1.end(),fft2.begin(),fft1.begin());
    //ifft2d
    slip::ifft2d(fft1.upper_left(), fft1.bottom_right(), fft2.upper_left());
    
    //the result should be real
     std::transform(fft2.begin(),fft2.end(),out_upper_left,slip::un_real<std::complex<Real>,Real>());  
        
  } 

  /**
   ** \brief Computes the convolution of a 2d signal using the fft2d.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/08/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param Signal  2D Container of the input Signal.
   ** \param Kernel  2D Container of the kernel.
   ** \param Result  2D Container of the convolution result.
   ** \pre Result.rows() == Signal.rows() + Kernel.rows() - 1
   ** \pre Result.cols() == Signal.cols() + Kernel.cols() - 1
   ** \pre Signal and Kernel are assumed to contain data of the same type.
   ** \par Example:
   ** \code
   ** //create an array2d and a kernel mask
   ** slip::Array2d<float> A(8,10);
   ** slip::iota(A.begin(),A.end(),1.0);
   ** slip::Array2d<float> Mask2d(4,4);
   ** slip::iota(Mask2d.begin(),Mask2d.end(),10.0,1.0);
   ** //print the array2d and the kernel mask
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** std::cout<<"Mask = \n"<<Mask2d<<std::endl;
   ** //computes the fft convolution 2d
   ** std::size_t convA_rows = A.rows()+ Mask2d.rows()-static_cast<std::size_t>(1);
   ** std::size_t convA_cols = A.cols()+ Mask2d.cols()-static_cast<std::size_t>(1);
 
   ** slip::Array2d<float> ConvA(convA_rows,convA_cols);
   ** slip::fft_convolution2d(A,Mask2d,ConvA);
   ** std::cout<<"ConvA = \n"<<ConvA<<std::endl;
   ** \endcode
   */
template <typename Container2d1, 
	  typename Kernel2d, 
	  typename Container2d2> 
inline void fft_convolution2d(const Container2d1& Signal,
			      const Kernel2d& Kernel, 
			      Container2d2& Result)


  {
    slip::fft_convolution2d(Signal.upper_left(),Signal.bottom_right(),
			    Kernel.upper_left(),Kernel.bottom_right(),
			    Result.upper_left(),Result.bottom_right());
  }

 /* @} */


  /** \name 3d convolutions */
  /* @{ */

  /**
   ** \brief Computes the separable convolution of a 3d signal by 3 asymmetric 1d-kernels.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2012/10/21
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param I_up RandomAccessIterator3d on the upper_left element of the 3d signal range.
   ** \param I_bot RandomAccessIterator3d. on one past-the-end element of the 3d signal range.
   ** \param k1_first A RandomAccessIterator on the first element of the horizontal kernel.
   ** \param k1_last A RandomAccessIterator on one past-the-end element of the horizontal kernel.
   ** \param ksize_left1 kernel size left to the "center" of the horizontal kernel
   ** \param ksize_right1 kernel size right to the "center" of the horizontal kernel
   ** \param bt1 slip::BORDER_TREATMENT associated with the horizontal kernel:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \param k2_first A RandomAccessIterator on the first element of the vertical kernel.
   ** \param k2_last A RandomAccessIterator on one past-the-end element of the vertical kernel.
   ** \param ksize_left2 kernel size left to the "center" of the vertical kernel
   ** \param ksize_right2 kernel size right to the "center" of the vertical kernel
   ** \param bt2 slip::BORDER_TREATMENT associated with the vertical kernel:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \param k3_first A RandomAccessIterator on the first element of the depth kernel.
   ** \param k3_last A RandomAccessIterator on one past-the-end element of the depth kernel.
   ** \param ksize_left3 kernel size left to the "center" of the depth kernel
   ** \param ksize_right3 kernel size right to the "center" of the depth kernel
   ** \param bt3 slip::BORDER_TREATMENT associated with the depth kernel:
     **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \param R_up RandomAccessIterator3d on the upper_left element of the 3d convolved signal range.
   ** 
   ** \pre Size of the two range must be the same.
   ** \par Example:
   ** \code
   ** slip::Array3d<float> Signal3d(3,5,5);
   ** slip::Array3d<float> Result3d(3,5,5);
   ** slip::iota(A.front_upper_left(),A.back_bottom_right(),1.0,1.0);
   ** slip::block<float,7> k1 = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0,7.0};
   ** slip::block<float,7> k2 = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0,7.0};
   ** slip::block<float,7> k3 = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0,7.0};
   ** slip::separable_convolution3d(Signal3d.front_upper_left(),
   **                       Signal3d.back_bottom_right(),
   **	              k1.begin(),k1.end(),4,2,
   **                         slip::BORDER_TREATMENT_ZERO_PADDED,
   **		      k2.begin(),k2.end(),4,2,
   **                         slip::BORDER_TREATMENT_ZERO_PADDED,
   **                  k3.begin(),k3.end(),4,2,
   **                         slip::BORDER_TREATMENT_ZERO_PADDED,
   **		      Result3d.front_upper_left());
   ** \endcode
   */
template <typename RandomAccessIterator3d1, 
	  typename KernelIter1,
	  typename KernelIter2,
	  typename KernelIter3,
          typename RandomAccessIterator3d2>
inline
void separable_convolution3d(RandomAccessIterator3d1 I_up,
			     RandomAccessIterator3d1 I_bot,
			     KernelIter1 k1_first, 
			     KernelIter1 k1_last,
			     std::size_t ksize_left1,
			     std::size_t ksize_right1,
			     slip::BORDER_TREATMENT bt1,
			     KernelIter2 k2_first, 
			     KernelIter2 k2_last,
			     std::size_t ksize_left2,
			     std::size_t ksize_right2,
			     slip::BORDER_TREATMENT bt2,
			     KernelIter3 k3_first, 
			     KernelIter3 k3_last,
			     std::size_t ksize_left3,
			     std::size_t ksize_right3,
			     slip::BORDER_TREATMENT bt3,
                             RandomAccessIterator3d2 R_up)
{

  typename RandomAccessIterator3d1::difference_type size3d =I_bot - I_up;
  typedef typename RandomAccessIterator3d1::size_type size_type;
  typedef typename RandomAccessIterator3d1::value_type value_type;

  size_type nb_slice = size3d[0];
  size_type nb_rows  = size3d[1];
  size_type nb_cols  = size3d[2];
 
 
  slip::Array<value_type> col_tmp(nb_cols);
   for(size_type k = 0; k < nb_slice; ++k)
     {
      for(size_type i = 0; i < nb_rows; ++i)
	{
	  slip::same_convolution(I_up.row_begin(k,i),
				 I_up.row_end(k,i),
				 k1_first,
				 k1_last,
				 ksize_left1,
				 ksize_right1,
				 col_tmp.begin(),
				 bt1);
	  std::copy(col_tmp.begin(),col_tmp.end(),
		    R_up.row_begin(k,i));
	}
     }
   slip::Array<value_type> row_tmp(nb_rows);
  for(size_type k = 0; k < nb_slice; ++k)
    {
      for(size_type j = 0; j < nb_cols; ++j)
	{
	  slip::same_convolution(R_up.col_begin(k,j),
				 R_up.col_end(k,j),
				 k2_first,	 
				 k2_last,
				 ksize_left2,
				 ksize_right2,
				 row_tmp.begin(),
				 bt2);
	  std::copy(row_tmp.begin(),row_tmp.end(),
		    R_up.col_begin(k,j));
	}
    }

  slip::Array<value_type> slice_tmp(nb_slice);
   for(size_type i = 0; i < nb_rows; ++i)
     {
       for(size_type j = 0; j < nb_cols; ++j)
	 {
	   slip::same_convolution(R_up.slice_begin(i,j),
				  R_up.slice_end(i,j),
				  k3_first,
				  k3_last,
				  ksize_left3,
				  ksize_right3,
				  slice_tmp.begin(),
				  bt3);
	   std::copy(slice_tmp.begin(),slice_tmp.end(),
		     R_up.slice_begin(i,j));
      }
     }
    

}


/**
   ** \brief Computes the separable convolution of a 3d signal by 3 symmetric 1d-kernels.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/12
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param I_up RandomAccessIterator3d on the upper_left element of the 3d signal range.
   ** \param I_bot RandomAccessIterator3d. on one past-the-end element of the 3d signal range.
   ** \param k1_first A RandomAccessIterator on the first element of the horizontal kernel.
   ** \param k1_last A RandomAccessIterator on one past-the-end element of the horizontal kernel.
   ** \param k2_first A RandomAccessIterator on the first element of the vertical kernel.
   ** \param k2_last A RandomAccessIterator on one past-the-end element of the vertical kernel.
   ** \param k3_first A RandomAccessIterator on the first element of the depth kernel.
   ** \param k3_last A RandomAccessIterator on one past-the-end element of the depth kernel.
   ** \param R_up RandomAccessIterator3d on the upper_left element of the 3d convolved signal range.
   ** \param bt slip::BORDER_TREATMENT associated with the kernels:
    **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 (default)
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
 
   ** 
   ** \pre Size of the two range must be the same.
   ** \par Example:
   ** \code
   ** slip::Array3d<float> Signal3d(3,5,5);
   ** slip::Array3d<float> Result3d(3,5,5);
   ** slip::iota(A.front_upper_left(),A.back_bottom_right(),1.0,1.0);
   ** slip::block<float,7> k1 = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0,7.0};
   ** slip::block<float,7> k2 = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0,7.0};
   ** slip::block<float,7> k3 = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0,7.0};
   ** slip::separable_convolution3d(Signal3d.front_upper_left(),
   **                               Signal3d.back_bottom_right(),
   **	                            k1.begin(),k1.end(),
   **		                    k2.begin(),k2.end(),
   **                               k3.begin(),k3.end(),
   **                               Result3d.front_upper_left(),
   **		                    slip::BORDER_TREATMENT_ZERO_PADDED);
   ** \endcode
   */
template <typename RandomAccessIterator3d1, 
	  typename KernelIter1,
	  typename KernelIter2,
	  typename KernelIter3,
          typename RandomAccessIterator3d2>
inline
void separable_convolution3d(RandomAccessIterator3d1 I_up,
			     RandomAccessIterator3d1 I_bot,
			     KernelIter1 k1_first, 
			     KernelIter1 k1_last,
			     KernelIter2 k2_first, 
			     KernelIter2 k2_last,
			     KernelIter3 k3_first, 
			     KernelIter3 k3_last,
			     RandomAccessIterator3d2 R_up,
                             slip::BORDER_TREATMENT bt = slip::BORDER_TREATMENT_ZERO_PADDED)
{

  std::size_t size1 = (k1_last-k1_first)/2;
  std::size_t size1_bis = (k1_last-k1_first) - 1 - size1;
  std::size_t size2 = (k2_last-k2_first)/2;
  std::size_t size2_bis = (k2_last-k2_first) - 1 - size2;
  std::size_t size3 = (k3_last-k3_first)/2;
  std::size_t size3_bis = (k3_last-k3_first) - 1 - size3;

  
  slip::separable_convolution3d(I_up,I_bot,
				k1_first,
				k1_last,
				size1,
				size1_bis,
				bt,
				k2_first,
				k2_last,
				size2,
				size2_bis,
				bt,
				k3_first,
				k3_last,
				size3,
				size3_bis,
				bt,
				R_up);

}


/**
   ** \brief Computes the separable convolution of 3d signal by a 3 1d-kernel.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param Signal  3D Container of the input Signal.
   ** \param kernel1  1D Container of the horizontal kernel.
   ** \param kernel2  1D Container of the vertical kernel.
   ** \param kernel3  1D Container of the depth kernel.
   ** \param Result  3D Container of the convolution result.
   ** \param bt slip::BORDER_TREATMENT associated with the kernels:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 (default)
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \pre Result.slices() == Signal.slices()
   ** \pre Result.rows() == Signal.rows()
   ** \pre Result.cols() == Signal.cols()
   ** \par Example:
   ** \code
   ** slip::Volume<float> I;
   ** I.read_from_images("lena%3d.png");
   ** slip::Volume<float> Result(I.slices(),I.rows(),I.cols());
   ** slip::block<float,3> k1 = {1.0/4.0,2.0/4.0,1.0/4.0};
   ** slip::block<float,3> k2 = {1.0/4.0,2.0/4.0,1.0/4.0};
   ** slip::block<float,3> k3 = {1.0/4.0,2.0/4.0,1.0/4.0};
   ** slip::separable_convolution(Signal,
   **                             k1,k2,k3,
   **                             Result,
   **                             slip::BORDER_TREATMENT_NEUMANN);
   ** \endcode
   */

  template <typename Container1, 
	    typename Kernel1, 
	    typename Kernel2,
	    typename Kernel3,
	    typename Container2> 
  inline void separable_convolution3d(const Container1& Signal,
				      const Kernel1& kernel1, 
				      const Kernel2& kernel2,
				      const Kernel3& kernel3,
				      Container2& Result,
				      slip::BORDER_TREATMENT bt = slip::BORDER_TREATMENT_ZERO_PADDED)


  {
    slip::separable_convolution3d(Signal.front_upper_left(),
				  Signal.back_bottom_right(),
				  kernel1.begin(),kernel1.end(),
				  kernel2.begin(),kernel2.end(),
				  kernel3.begin(),kernel3.end(),
				  Result.front_upper_left(),
				  bt);
  }


/**
   ** \brief Computes the in-place separable convolution of a 3d signal by 3 asymmetric 1d-kernels.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2012/10/22
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param I_up RandomAccessIterator3d on the upper_left element of the 3d signal range.
   ** \param I_bot RandomAccessIterator3d. on one past-the-end element of the 3d signal range.
   ** \param k1_first A RandomAccessIterator on the first element of the horizontal kernel.
   ** \param k1_last A RandomAccessIterator on one past-the-end element of the horizontal kernel.
   ** \param ksize_left1 kernel size left to the "center" of the horizontal kernel
   ** \param ksize_right1 kernel size right to the "center" of the horizontal kernel
   ** \param bt1 slip::BORDER_TREATMENT associated with the horizontal kernel:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \param k2_first A RandomAccessIterator on the first element of the vertical kernel.
   ** \param k2_last A RandomAccessIterator on one past-the-end element of the vertical kernel.
   ** \param ksize_left2 kernel size left to the "center" of the vertical kernel
   ** \param ksize_right2 kernel size right to the "center" of the vertical kernel
   ** \param bt2 slip::BORDER_TREATMENT associated with the vertical kernel:
   **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   ** \param k3_first A RandomAccessIterator on the first element of the depth kernel.
   ** \param k3_last A RandomAccessIterator on one past-the-end element of the depth kernel.
   ** \param ksize_left3 kernel size left to the "center" of the depth kernel
   ** \param ksize_right3 kernel size right to the "center" of the depth kernel
   ** \param bt3 slip::BORDER_TREATMENT associated with the depth kernel:
     **   \li slip::BORDER_TREATMENT_AVOID: only parts which do not include zero-padded edges are processed
   **   \li slip::BORDER_TREATMENT_ZERO_PADDED: borders are put to 0 
   **   \li slip::BORDER_TREATMENT_NEUMANN: borders are repeated the nearest valid pixel
   **   \li slip::BORDER_TREATMENT_CIRCULAR: the range is supposed to be circular.
   
   ** \par Example:
   ** \code
   ** slip::Array3d<float> Signal3d(3,5,5);
   ** slip::Array3d<float> Result3d(3,5,5);
   ** slip::iota(A.front_upper_left(),A.back_bottom_right(),1.0,1.0);
   ** slip::block<float,7> k1 = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0,7.0};
   ** slip::block<float,7> k2 = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0,7.0};
   ** slip::block<float,7> k3 = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0,7.0};
   ** slip::separable_convolution3d(Signal3d.front_upper_left(),
   **                       Signal3d.back_bottom_right(),
   **	              k1.begin(),k1.end(),4,2,
   **                         slip::BORDER_TREATMENT_ZERO_PADDED,
   **		      k2.begin(),k2.end(),4,2,
   **                         slip::BORDER_TREATMENT_ZERO_PADDED,
   **                  k3.begin(),k3.end(),4,2,
   **                         slip::BORDER_TREATMENT_ZERO_PADDED);
   ** \endcode
   */
template <typename RandomAccessIterator3d1, 
	  typename KernelIter1,
	  typename KernelIter2,
	  typename KernelIter3>
inline
void separable_convolution3d(RandomAccessIterator3d1 I_up,
			     RandomAccessIterator3d1 I_bot,
			     KernelIter1 k1_first, 
			     KernelIter1 k1_last,
			     std::size_t ksize_left1,
			     std::size_t ksize_right1,
			     slip::BORDER_TREATMENT bt1,
			     KernelIter2 k2_first, 
			     KernelIter2 k2_last,
			     std::size_t ksize_left2,
			     std::size_t ksize_right2,
			     slip::BORDER_TREATMENT bt2,
			     KernelIter3 k3_first, 
			     KernelIter3 k3_last,
			     std::size_t ksize_left3,
			     std::size_t ksize_right3,
			     slip::BORDER_TREATMENT bt3)
{

  typename RandomAccessIterator3d1::difference_type size3d =I_bot - I_up;
  typedef typename RandomAccessIterator3d1::size_type size_type;
  typedef typename RandomAccessIterator3d1::value_type value_type;

  const size_type nb_slice = size3d[0];
  const size_type nb_rows  = size3d[1];
  const size_type nb_cols  = size3d[2];
 
 
  slip::Array<value_type> col_tmp(nb_cols);
   for(size_type k = 0; k < nb_slice; ++k)
     {
      for(size_type i = 0; i < nb_rows; ++i)
	{
	  slip::same_convolution(I_up.row_begin(k,i),
				 I_up.row_end(k,i),
				 k1_first,
				 k1_last,
				 ksize_left1,
				 ksize_right1,
				 col_tmp.begin(),
				 bt1);
	  std::copy(col_tmp.begin(),col_tmp.end(),
		    I_up.row_begin(k,i));
	}
     }
   slip::Array<value_type> row_tmp(nb_rows);
  for(size_type k = 0; k < nb_slice; ++k)
    {
      for(size_type j = 0; j < nb_cols; ++j)
	{
	  slip::same_convolution(I_up.col_begin(k,j),
				 I_up.col_end(k,j),
				 k2_first,	 
				 k2_last,
				 ksize_left2,
				 ksize_right2,
				 row_tmp.begin(),
				 bt2);
	  std::copy(row_tmp.begin(),row_tmp.end(),
		    I_up.col_begin(k,j));
	}
    }

  slip::Array<value_type> slice_tmp(nb_slice);
   for(size_type i = 0; i < nb_rows; ++i)
     {
       for(size_type j = 0; j < nb_cols; ++j)
	 {
	   slip::same_convolution(I_up.slice_begin(i,j),
				  I_up.slice_end(i,j),
				  k3_first,
				  k3_last,
				  ksize_left3,
				  ksize_right3,
				  slice_tmp.begin(),
				  bt3);
	   std::copy(slice_tmp.begin(),slice_tmp.end(),
		     I_up.slice_begin(i,j));
      }
     }
    

}

/**
   ** \brief Computes the separable 3d convolution of signal using the fft algorithm.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param I_up RandomAccessIterator3d on the upper_left element of the 3d signal range.
   ** \param I_bot RandomAccessIterator3d. on one past-the-end element of the 3d signal range.
   ** \param k1_first A RandomAccessIterator on the first element of the horizontal kernel.
   ** \param k1_last A RandomAccessIterator on one past-the-end element of the horizontal kernel.

   ** \param k2_first A RandomAccessIterator on the first element of the vertical kernel.
   ** \param k2_last A RandomAccessIterator on one past-the-end element of the vertical kernel.
   ** \param k3_first A RandomAccessIterator on the first element of the depth kernel.
   ** \param k3_last A RandomAccessIterator on one past-the-end element of the depth kernel.
   ** \param R_up RandomAccessIterator3d on the upper_left element of the 3d convolved signal range.
   ** 
   ** \pre Size of the two range must be the same.
   ** \par Example:
   ** \code
   ** slip::Volume<float> V3(16,16,16);
   ** slip::Volume<float> V3r(16,16,16);
   ** slip::iota(V3.begin(),V3.end(),1.0f);
   ** slip::block<float,3> ke1 = {0.25f, 0.5f, 0.25f};
   ** slip::block<float,3> ke2 = {0.25f, 0.5f, 0.25f};
   ** slip::block<float,3> ke3 = {0.5f, 0.0f, -0.5f};
   ** slip::separable_fft_convolution3d(V3.front_upper_left(),
   **			    V3.back_bottom_right(),
   **			    ke1.begin(),ke1.end(),
   **			    ke2.begin(),ke2.end(),
   **			    ke3.begin(),ke3.end(),
   **			    V3r.front_upper_left());
   **
   ** std::cout<<V3r<<std::endl;
   ** \endcode
   */
template <typename RandomAccessIterator3d1, 
	  typename KernelIter1,
	  typename KernelIter2,
	  typename KernelIter3,
	  typename RandomAccessIterator3d2>
inline
void separable_fft_convolution3d(RandomAccessIterator3d1 I_up,
				 RandomAccessIterator3d1 I_bot,
				 KernelIter1 k1_first, 
				 KernelIter1 k1_last,
				 KernelIter2 k2_first, 
				 KernelIter2 k2_last,
				 KernelIter3 k3_first, 
				 KernelIter3 k3_last,
				 RandomAccessIterator3d2 R_up)
{
  typename RandomAccessIterator3d1::difference_type size3d = 
    I_bot - I_up;
  typedef typename RandomAccessIterator3d1::size_type size_type;
  typedef typename RandomAccessIterator3d1::value_type value_type;
  size_type nb_slices = size3d[0];
  size_type nb_rows   = size3d[1];
  size_type nb_cols   = size3d[2];
  
  size_type size1 = (k1_last - k1_first);
  size_type size2 = (k2_last - k2_first);
  size_type size3 = (k3_last - k3_first);
 
  size_type size2o2 = size2/2;
  size_type size1o2 = size1/2;

  slip::Array3d<value_type> Atmp1(nb_slices + size3 - 1,
				  nb_rows   + size1 - 1,
				  nb_cols   + size2 - 1);
  slip::Array3d<value_type> Atmp2(nb_slices + size3 - 1,
				  nb_rows   + size1 - 1,
				  nb_cols   + size2 - 1);
  slip::Array<value_type> Vtmp(Atmp2.slices());


   for(size_t k = 0; k < nb_slices; ++k)
     {
      for(size_t i = 0; i < nb_rows; ++i)
	{
	  slip::fft_convolution(I_up.row_begin(k,i),
				I_up.row_end(k,i),
				k1_first,k1_last,
				Atmp1.row_begin(k,i),
				Atmp1.row_end(k,i));
	}
     }
  for(size_t k = 0; k < nb_slices; ++k)
    {
      for(size_t j = 0; j < nb_cols; ++j)
	{
	  slip::fft_convolution(Atmp1.col_begin(k,j) + size2o2,
				Atmp1.col_end(k,j) - size2o2,
				k2_first,k2_last,
				Atmp2.col_begin(k,j),
				Atmp2.col_end(k,j));
	}
    }
  Atmp1.resize(0,0,0);
   for(size_t i = 0; i < nb_rows; ++i)
     {
       for(size_t j = 0; j < nb_cols; ++j)
	 {
	   slip::fft_convolution(Atmp2.slice_begin(i,j) + size1o2,
				 Atmp2.slice_end(i,j) - size1o2,
				 k3_first,k3_last,
				 Vtmp.begin(),Vtmp.end());
	   std::copy(Vtmp.begin()+size2o2,Vtmp.end()-size2o2,R_up.col_begin(i,j));
      }
     }


}
/**
   ** \brief Computes the separable fft convolution of 3d signal by 3 1d-kernel.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param Signal  3D Container of the input Signal.
   ** \param kernel1  1D Container of the horizontal kernel.
   ** \param kernel2  1D Container of the vertical kernel.
   ** \param kernel3  1D Container of the depth kernel.
   ** \param Result  3D Container of the convolution result.
   ** \pre Result.slices() == Signal.slices()
   ** \pre Result.rows() == Signal.rows()
   ** \pre Result.cols() == Signal.cols()
   ** \par Example:
   ** \code
   ** slip::Volume<float> V3(16,16,16);
   ** slip::Volume<float> V3r(16,16,16);
   ** slip::iota(V3.begin(),V3.end(),1.0f);
   ** slip::block<float,3> ke1 = {0.25f, 0.5f, 0.25f};
   ** slip::block<float,3> ke2 = {0.25f, 0.5f, 0.25f};
   ** slip::block<float,3> ke3 = {0.5f, 0.0f, -0.5f};
   ** slip::separable_fft_convolution3d(V3,
   **			   ke1,
   **			   ke2,
   **			   ke3,
   **			   V3r);
   ** std::cout<<V3r<<std::endl;
   ** \endcode
   */

  template <typename Container1, 
	    typename Kernel1, 
	    typename Kernel2,
	    typename Kernel3,
	    typename Container2> 
  inline void separable_fft_convolution3d(const Container1& Signal,
					  const Kernel1& kernel1, 
					  const Kernel2& kernel2,
					  const Kernel3& kernel3,
					  Container2& Result)


  {
    slip::separable_fft_convolution3d(Signal.front_upper_left(),
				      Signal.back_bottom_right(),
				      kernel1.begin(),kernel1.end(),
				      kernel2.begin(),kernel2.end(),
				      kernel3.begin(),kernel3.end(),
				      Result.front_upper_left());
  }


/**
 ** \brief Computes the convolution of two 3D sequences using fft3d. 
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2009/09/17
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param in1_front_upper_left : A 3d input iterator (volume).
 ** \param in1_back_bottom_right : A 3d input iterator (volume).
 ** \param in2_front_upper_left : A 3d input iterator (mask).
 ** \param in2_back_bottom_right : A 3d input iterator (mask).
 ** \param out_front_upper_left : A 3d output iterator (result).
 ** \param out_back_bottom_right : A32d output iterator (result).
 **
 ** \pre the value_type of the input sequences have to be real (not complex)
 ** \pre ((in1_back_bottom_right - in1_front_upper_left)[0]+(in2_back_bottom_right - in2_front_upper_left)[0] - 1) == (out_back_bottom_right - out_front_upper_left)[0]
 ** \pre ((in1_back_bottom_right - in1_front_upper_left)[1]+(in2_back_bottom_right - in2_front_upper_left)[1] - 1) == (out_back_bottom_right - out_front_upper_left)[1]
 ** \pre ((in1_back_bottom_right - in1_front_upper_left)[2]+(in2_back_bottom_right - in2_front_upper_left)[2] - 1) == (out_back_bottom_right - out_front_upper_left)[2]
 ** \par Example:
 ** \code
 ** slip::Array3d<float> A3(4,8,10);
 ** slip::iota(A3.begin(),A3.end(),1.0);
 ** std::cout<<"A3 = \n"<<A3<<std::endl;
 ** slip::Array3d<float> Mask3d(2,3,4);
 ** slip::iota(Mask3d.begin(),Mask3d.end(),10.0,1.0);
 ** std::cout<<"Mask3d = \n"<<Mask3d<<std::endl;
 **  std::size_t convA3_slices = A3.slices()+ Mask3d.slices()-static_cast<std::size_t>(1);
 **  std::size_t convA3_rows = A3.rows()+Mask3d.rows()-static_cast<std::size_t>(1);
 ** std::size_t convA3_cols = A3.cols()+Mask3d.cols()-static_cast<std::size_t>(1));								
 ** slip::Array3d<float> ConvA3(convA3_slices,convA3_rows,convA3_cols);
 ** slip::fft_convolution3d(A3.front_upper_left(),A3.back_bottom_right(),
 **                         Mask3d.front_upper_left(),Mask3d.back_bottom_right(),
 **                         ConvA3.front_upper_left(),ConvA3.back_bottom_right()); 
 ** \endcode
 */   
template<typename InputIterator3d1, 
	 typename InputIterator3d2, 
	 typename OutputIterator3d>
inline
void 
fft_convolution3d(InputIterator3d1 in1_front_upper_left, 
		  InputIterator3d1 in1_back_bottom_right, 
		  InputIterator3d2 in2_front_upper_left, 
		  InputIterator3d2 in2_back_bottom_right, 
		  OutputIterator3d out_front_upper_left, 
		  OutputIterator3d out_back_bottom_right)
  {
     assert(((in1_back_bottom_right - in1_front_upper_left)[0]+(in2_back_bottom_right - in2_front_upper_left)[0] - 1) == (out_back_bottom_right - out_front_upper_left)[0]);
    assert(((in1_back_bottom_right - in1_front_upper_left)[1]+(in2_back_bottom_right - in2_front_upper_left)[1] - 1) == (out_back_bottom_right - out_front_upper_left)[1]);
assert(((in1_back_bottom_right - in1_front_upper_left)[2]+(in2_back_bottom_right - in2_front_upper_left)[2] - 1) == (out_back_bottom_right - out_front_upper_left)[2]);
 
    typename InputIterator3d1::difference_type size3din1 = in1_back_bottom_right - in1_front_upper_left;
    typename InputIterator3d2::difference_type size3din2 = in2_back_bottom_right - in2_front_upper_left;
    typename OutputIterator3d::difference_type size3dout = out_back_bottom_right - out_front_upper_left;

     typedef  typename slip::lin_alg_traits<typename std::iterator_traits<InputIterator3d1>::value_type>::value_type Real;
 
    std::size_t slices =  size3din1[0] + size3din2[0] - 1; 
    std::size_t rows   =  size3din1[1] + size3din2[1] - 1; 
    std::size_t cols   =  size3din1[2] + size3din2[2] - 1; 

     //zero-padd Signal and Kernel data
    slip::Array3d<Real> Signal_zero_padded(slices,rows,cols,Real());
    slip::Array3d<Real> Kernel_zero_padded(slices,rows,cols,Real());
    slip::Box3d<int> signal_box(0,0,0,
				static_cast<int>(size3din1[0]-1),
				static_cast<int>(size3din1[1]-1),
				static_cast<int>(size3din1[2]-1));
    slip::Box3d<int> kernel_box(0,0,0,
				static_cast<int>(size3din2[0]-1),
				static_cast<int>(size3din2[1]-1),
				static_cast<int>(size3din2[2]-1));
    std::copy(in1_front_upper_left,
	      in1_back_bottom_right,
	      Signal_zero_padded.front_upper_left(signal_box));
    std::copy(in2_front_upper_left,in2_back_bottom_right,
	      Kernel_zero_padded.front_upper_left(kernel_box));


    slip::Array3d<std::complex<Real> > fft1(slices,rows,cols,std::complex<Real>());
    slip::Array3d<std::complex<Real> > fft2(slices,rows,cols,std::complex<Real>());
    //computes the two fft
    slip::real_fft3d(Signal_zero_padded.front_upper_left(),
		     Signal_zero_padded.back_bottom_right(),
		     fft1.front_upper_left());
    slip::real_fft3d(Kernel_zero_padded.front_upper_left(),
		     Kernel_zero_padded.back_bottom_right(),
		     fft2.front_upper_left());
    //computes fft1*fft2
    slip::multiplies(fft1.begin(),fft1.end(),fft2.begin(),fft1.begin());
   
    //ifft3d
    slip::ifft3d(fft1.front_upper_left(), fft1.back_bottom_right(), 
		 fft2.front_upper_left());
    //the result should be real
    std::transform(fft2.front_upper_left(),fft2.back_bottom_right(),
		   out_front_upper_left,
		   slip::un_real<std::complex<Real>,Real>()); 
  }

/**
   ** \brief Computes the convolution of a 3d signal using the fft3d.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/08/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param Signal  3D Container of the input Signal.
   ** \param Kernel  3D Container of the kernel.
   ** \param Result  3D Container of the convolution result.
   ** \pre Result.slices() == Signal.slices() + Kernel.slices() - 1
   ** \pre Result.rows() == Signal.rows() + Kernel.rows() - 1
   ** \pre Result.cols() == Signal.cols() + Kernel.cols() - 1
   ** \pre Signal and Kernel are assumed to contain data of the same type.
   ** \par Example:
   ** \code
   ** slip::Array3d<float> A3(4,8,10);
   ** slip::iota(A3.begin(),A3.end(),1.0);
   ** std::cout<<"A3 = \n"<<A3<<std::endl;
   ** slip::Array3d<float> Mask3d(2,3,4);
   ** slip::iota(Mask3d.begin(),Mask3d.end(),10.0,1.0);
   ** std::cout<<"Mask3d = \n"<<Mask3d<<std::endl;
   **  std::size_t convA3_slices = A3.slices()+static_cast<std::size_t>(2)*(Mask3d.slices()-static_cast<std::size_t>(1));
   **  std::size_t convA3_rows = A3.rows()+static_cast<std::size_t>(2)*(Mask3d.rows()-static_cast<std::size_t>(1));
   ** std::size_t convA3_cols = A3.cols()+static_cast<std::size_t>(2)*(Mask3d.cols()-static_cast<std::size_t>(1));								
   ** slip::Array3d<float> ConvA3(convA3_slices,convA3_rows,convA3_cols);
   ** slip::fft_convolution3d(A3,Mask3d,ConvA3);
   ** \endcode
   */
 template <typename Container3d1, 
	   typename Kernel3d, 
	   typename Container3d2> 
  inline void fft_convolution3d(const Container3d1& Signal,
				const Kernel3d& Kernel, 
				Container3d2& Result)


  {
    slip::fft_convolution3d(Signal.front_upper_left(),
			    Signal.back_bottom_right(),
			    Kernel.front_upper_left(),
			    Kernel.back_bottom_right(),
			    Result.front_upper_left(),
			    Result.back_bottom_right());
  }


/**
 ** \brief Computes the convolution between two 3D sequences using fft3d. 
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2014/03/13
 ** \since 1.0.0
 ** \version 0.0.2
 ** \param in1_front_upper_left : A 3d input iterator (volume).
 ** \param in1_back_bottom_right : A 3d input iterator (volume).
 ** \param in2_front_upper_left : A 3d input iterator (mask).
 ** \param in2_back_bottom_right : A 3d input iterator (mask).
 ** \param out_front_upper_left : A 3d output iterator (result).
 ** \param out_back_bottom_right : A32d output iterator (result).
 **
 ** \pre the value_type of the input sequences have to be real (not complex)
 ** \pre (in1_back_bottom_right - in1_front_upper_left) == (out_back_bottom_right - out_front_upper_left)
 ** \par Example:
 ** \code
 **  
 ** \endcode
 */   
 template<typename InputIterator3d1, 
	  typename InputIterator3d2, 
	  typename OutputIterator3d>
  inline
  void 
  fft_convolution3d_same(InputIterator3d1 in1_front_upper_left, 
			 InputIterator3d1 in1_back_bottom_right, 
			 InputIterator3d2 in2_front_upper_left, 
			 InputIterator3d2 in2_back_bottom_right, 
			 OutputIterator3d out_front_upper_left, 
			 OutputIterator3d out_back_bottom_right)
  {
    assert(   (in1_back_bottom_right - in1_front_upper_left) 
	   == (out_back_bottom_right - out_front_upper_left));

   typename InputIterator3d1::difference_type size3din1 = in1_back_bottom_right - in1_front_upper_left;
    typename InputIterator3d2::difference_type size3din2 = in2_back_bottom_right - in2_front_upper_left;
    typename OutputIterator3d::difference_type size3dout = out_back_bottom_right - out_front_upper_left;

     typedef  typename slip::lin_alg_traits<typename std::iterator_traits<InputIterator3d1>::value_type>::value_type Real;
 
    std::size_t slices =  size3din1[0] + size3din2[0] - 1; 
    std::size_t rows   =  size3din1[1] + size3din2[1] - 1; 
    std::size_t cols   =  size3din1[2] + size3din2[2] - 1; 

     //zero-padd Signal and Kernel data
    slip::Array3d<Real> Signal_zero_padded(slices,rows,cols,Real());
    slip::Array3d<Real> Kernel_zero_padded(slices,rows,cols,Real());
    slip::Box3d<int> signal_box(0,0,0,
				static_cast<int>(size3din1[0]-1),
				static_cast<int>(size3din1[1]-1),
				static_cast<int>(size3din1[2]-1));
    slip::Box3d<int> kernel_box(0,0,0,
				static_cast<int>(size3din2[0]-1),
				static_cast<int>(size3din2[1]-1),
				static_cast<int>(size3din2[2]-1));
    std::copy(in1_front_upper_left,
	      in1_back_bottom_right,
	      Signal_zero_padded.front_upper_left(signal_box));
    std::copy(in2_front_upper_left,in2_back_bottom_right,
	      Kernel_zero_padded.front_upper_left(kernel_box));


    slip::Array3d<std::complex<Real> > fft1(slices,rows,cols,std::complex<Real>());
    slip::Array3d<std::complex<Real> > fft2(slices,rows,cols,std::complex<Real>());
    //computes the two fft
    slip::real_fft3d(Signal_zero_padded.front_upper_left(), 
		     Signal_zero_padded.back_bottom_right(), 
		     fft1.front_upper_left());
    slip::real_fft3d(Kernel_zero_padded.front_upper_left(), 
		     Kernel_zero_padded.back_bottom_right(), 
		     fft2.front_upper_left());
    //computes fft1*fft2
    slip::multiplies(fft1.begin(),fft1.end(),fft2.begin(),fft1.begin());
   
    //ifft3d
    slip::ifft3d(fft1.front_upper_left(), fft1.back_bottom_right(), 
		 fft2.front_upper_left());
    //fftshift3d
    //slip::fftshift3d(fft2.front_upper_left(),fft2.back_bottom_right());
    //the result should be real
    slip::Box3d<int> out_box(size3din2[0]/2,size3din2[1]/2,size3din2[2]/2,
			     static_cast<int>(size3din2[0]/2 + size3din1[0] - 1),
			     static_cast<int>(size3din2[1]/2 + size3din1[1] - 1),
			     static_cast<int>(size3din2[2]/2 + size3din1[2] - 1));
    
    std::transform(fft2.front_upper_left(out_box),
		   fft2.back_bottom_right(out_box),
		   out_front_upper_left,slip::un_real<std::complex<Real>,Real>());  
        
  }

 /**
   ** \brief Computes the valid convolution of 3d signal by a 3d-kernel
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param S_up A RandomAccessIterator3d.
   ** \param S_bot  A RandomAccessIterator3d.
   ** \param ksize_front Number of elements front to the central point of the mask.
   ** \param ksize_back Number of elements back to the central point of the mask
   ** \param kernel_up A RandomAccessIterator3d.
   ** \param kernel_bot A RandomAccessIterator3d.
   ** \param ksize_left Number of elements left to the central point of the mask.
   ** \param ksize_right Number of elements right to the central point of the mask.
   ** \param ksize_up Number of elements up to the central point of the mask.
   ** \param ksize_bot Number of elements bottom to the central point of the mask.
   ** \param R_up A RandomAccessIterator3d.
   ** \param R_bot A RandomAccessIterator3d.
   ** \pre (R_bot - R_up)[0] == ((S_bot - S_up)[0] - (kernel_bot - kernel_up)[0] + 1)
   ** \pre (R_bot - R_up)[1] == ((S_bot - S_up)[1] - (kernel_bot - kernel_up)[1] + 1)
   ** \pre (R_bot - R_up)[2] == ((S_bot - S_up)[2] - (kernel_bot - kernel_up)[2] + 1)
   ** \pre (kernel_bot - kernel_up)[0] == ksize_front + ksize_back  + 1
   ** \pre (kernel_bot - kernel_up)[1] == ksize_left + ksize_right + 1
   ** \pre (kernel_bot - kernel_up)[2] == ksize_top + ksize_up  + 1
    
   **  \par Description:
   **  Only non zero-padded points (kernel is fully inside) are computed
   **  if the signal size is NxPxQ, 
   **  and the kernel size is 
   **  KxLxM = (ksize_front + ksize_back + 1) x (ksize_left + ksize_right + 1) x (ksize_top + ksize_up  + 1), 
   **  the size of the result will be 
   **  (K - (ksize_front + ksize_back)) x (L - (ksize_left + ksize_right)) x (M - (ksize_top + ksize_up))
   **
   ** \par Example:
   ** \code
   ** slip::Array3d<float> A(8,10);
   ** slip::iota(A.begin(),A.end(),1.0);
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Array3d<float> Mask3d(3,4);
   ** slip::iota(Mask3d.begin(),Mask3d.end(),10.0,1.0);
   ** std::cout<<"Mask = \n"<<Mask3d<<std::endl;
   ** std::cout<<"valid convolution3d "<<std::endl;
   ** slip::Array3d<float> ConvAv3(A3.slices() - Mask3d.slices() + 1,
   **		     A3.rows()   - Mask3d.rows()   + 1,
   **		     A3.cols()   - Mask3d.cols()   + 1);
   ** std::cout<<ConvAv3.slices()<<"x"<<ConvAv3.rows()<<"x"<<ConvAv3.cols()<<std::endl;
   ** slip::valid_convolution3d(A3.front_upper_left(),A3.back_bottom_right(),
   **		   Mask3d.front_upper_left(),Mask3d.back_bottom_right(),
   **		   1,0,
   **		   2,1,
   **		   0,2,
   **		   ConvAv3.front_upper_left(),ConvAv3.back_bottom_right());
   ** std::cout<<"ConvAv3 = \n"<<ConvAv3<<std::endl;
   **
   ** \endcode
   */
  template <typename SrcIter3d, 
	    typename KernelIter3d, 
	    typename ResIter3d>
  inline
  void valid_convolution3d(SrcIter3d S_up,
			   SrcIter3d S_bot,
			   KernelIter3d kernel_up,
			   KernelIter3d kernel_bot,
			   std::size_t ksize_front,
			   std::size_t ksize_back,
			   std::size_t ksize_left,
			   std::size_t ksize_right,
			   std::size_t ksize_up,
			   std::size_t ksize_bot,
			   ResIter3d R_up,
			   ResIter3d R_bot)
  {

    assert((R_bot - R_up)[0] == ((S_bot - S_up)[0] - (kernel_bot - kernel_up)[0] + 1));
    assert((R_bot - R_up)[1] == ((S_bot - S_up)[1] - (kernel_bot - kernel_up)[1] + 1));
    assert((R_bot - R_up)[2] == ((S_bot - S_up)[2] - (kernel_bot - kernel_up)[2] + 1));
  
    assert(static_cast<int>((kernel_bot - kernel_up)[0]) == static_cast<int>(ksize_front + ksize_back + 1));
    assert(static_cast<int>((kernel_bot - kernel_up)[1]) == static_cast<int>(ksize_up + ksize_bot + 1));
    assert(static_cast<int>((kernel_bot - kernel_up)[2]) == static_cast<int>(ksize_left + ksize_right + 1));

  
     std::size_t k_slices = static_cast<std::size_t>((kernel_bot - kernel_up)[0]);
    std::size_t k_rows = static_cast<std::size_t>((kernel_bot - kernel_up)[1]);
    std::size_t k_cols = static_cast<std::size_t>((kernel_bot - kernel_up)[2]);
    std::size_t r_slices = static_cast<std::size_t>((R_bot - R_up)[0]);
    std::size_t r_rows = static_cast<std::size_t>((R_bot - R_up)[1]);
    std::size_t r_cols = static_cast<std::size_t>((R_bot - R_up)[2]);

  
    typedef typename SrcIter3d::value_type value_type;
    typedef typename KernelIter3d::value_type k_value_type;
   
    //temporary array used to compute the reverse kernel
    slip::DPoint3d<int> dp(1,1,0);
    std::reverse_iterator<KernelIter3d> k_rupper_left = std::reverse_iterator<KernelIter3d>(kernel_bot-dp);
    std::reverse_iterator<KernelIter3d> k_rbottom_right = std::reverse_iterator<KernelIter3d>(kernel_up);
    
    slip::Array3d<k_value_type> reverse_kernel(k_slices,k_rows,k_cols);
    std::copy(k_rupper_left,k_rbottom_right,reverse_kernel.front_upper_left());

    // apply the convolution on a point where the kernel does 
    // fit in the signal
    for(std::size_t k = 0, kk = ksize_back; k < r_slices; ++k, ++kk)
      {
	for(std::size_t i = 0, ii = ksize_bot; i < r_rows; ++i, ++ii)
	  {
	    for(std::size_t j = 0, jj = ksize_right; j < r_cols; ++j, ++jj)
	      {
		for(std::size_t s = 0, ss = (kk - ksize_back); s < k_slices; ++s, ++ss)
		  {
		    for(std::size_t l = 0, ll = (ii - ksize_bot); l < k_rows; ++l, ++ll)
		      {
			*(R_up.row_begin(k,i)+j)+=
			  std::inner_product(S_up.row_begin(ss,ll)+j,
					     S_up.row_begin(ss,ll)+j+k_cols,
					     reverse_kernel.row_begin(s,l),
					     value_type(0));
		      }
		  }
	      }
	  }
      }
  }

 /* @} */
}//slip::



#endif //SLIP_CONVOLUTION_HPP
