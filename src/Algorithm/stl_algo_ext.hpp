/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file stl_algo_ext.hpp
 * 
 * \brief Provides some extension to STL algorithms.
 * 
 */
#ifndef SLIP_STL_ALGO_EXT_HPP
#define SLIP_STL_ALGO_EXT_HPP
#include <iostream>
#include <cassert>

namespace slip
{

  /** \name Transform algorithms */
  /* @{ */



  /**
   **  \brief Perform an operation on a sequence according to a mask sequence. 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2008/09/27
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  mask_first An input iterator. 
   **  \param  result    An output iterator.
   **  \param  unary_op  A unary operator.
   **  \param  value true value of the mask range. Default is 1.
   **  \return An output iterator equal to result+(last-first).
   **  \par Description:
   **  Applies the operator to each element in the input range 
   **  according to the input mask range and assigns
   **  the results to successive elements of the output sequence.
   **  Evaluates *(result+N)=unary_op(*(first+N)) for each N in the
   **  range [0,last-first) verifying *mask_first == value
   **  \pre [first,last) must be valid.
   **  \pre [mask_first,mask_first + (last-first)) must be valid
   **  \pre [result,result + (last-first)) must be valid
   **  \pre unary_op must not alter its argument.
   **  \par Example1:
   **  \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2(10);
   **  slip::Array<bool> mask(v1.size());
   **  mask[2] = 1;
   **  mask[4] = 1;
   **  mask[9] = 1;
   **  std::cout<<"mask = "<<mask<<std::endl;
   **  slip::transform_mask_un(v1.begin(),v1.end(),
   **		            mask.begin(),
   ** 	                    v2.begin(),
   **		            std::negate<int>());
   **    std::cout<<"v2 = "<<v2<<std::endl;
   **  \endcode
   **
   **  \par Example2:
   **  \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2(10);
   **  slip::Array<bool> mask(v1.size());
   **  maskint[2] = 2;
   **  maskint[4] = 2;
   **  maskint[9] = 2;
   **  std::cout<<"mask = "<<mask<<std::endl;
   **  slip::transform_mask_un(v1.begin(),v1.end(),
   **		               mask.begin(),
   ** 	                       v2.begin(),
   **		               std::negate<int>(),int(2));
   **  std::cout<<"v2 = "<<v2<<std::endl;
   **  \endcode
   **
   */
  template<typename _InputIterator, 
	   typename _MaskIterator,
	   typename _OutputIterator,
	   typename _UnaryOperation>
    _OutputIterator
    transform_mask_un(_InputIterator first, _InputIterator last,
		   _MaskIterator mask_first,
		   _OutputIterator result, 
		   _UnaryOperation unary_op,
		   typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
    {
       assert(first != last); 
       for ( ; first != last; ++first, ++result, ++mask_first)
	 {
	   if(*mask_first == value)
	     { 
	       *result = unary_op(*first);
	     }
	 }
       return result;
    }

  /**
   **  \brief Perform an operation on corresponding elements of two sequences
   **  according to a mask sequence.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2008/09/27
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  mask_first An input iterator.
   **  \param  first2     An input iterator.
   **  \param  result     An output iterator.
   **  \param  binary_op  A binary operator.
   **  \param  value true value of the mask range. Default is 1.
   **  \return An output iterator equal to  result+(last-first).
   **  \par Description:
   **  Applies the operator to the corresponding elements in the two
   **  input ranges  according to the input mask range and assigns 
   **  the results to successive elements of the output sequence.
   **  Evaluates *(result+N)=binary_op(*(first1+N),*(first2+N)) for each
   **   N in the range  [0,last1-first1) verifying *mask_first == value.
   **  \pre [first1,last1) must be valid.
   **  \pre [first2,first2 + (last1-first1)) must be valid
   **  \pre [mask_first,mask_first + (last1-first1)) must be valid
   **  \pre [result,result + (last-first)) must be valid
   **  \pre binary_op must not alter either of its arguments.
   **  \par Example1:
   **  \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2(v1);
   **  std::cout<<"v2 = "<<v2<<std::endl;
   **  slip::Array<bool> mask(v1.size());
   **  mask[2] = 1;
   **  mask[4] = 1;
   **  mask[9] = 1;
   **  slip::Array<int> v3(v1.size());
   **  slip::transform_mask_bin(v1.begin(),v1.end(),
   ** 		       mask.begin(),
   **		       v2.begin(),
   **		       v3.begin(),
   **		       std::plus<int>());
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   ** \par Example1:
   ** \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2(v1);
   **  std::cout<<"v2 = "<<v2<<std::endl;
   **  slip::Array<bool> maskint(v1.size());
   **  mask[2] = 2;
   **  mask[4] = 2;
   **  mask[9] = 2;
   **  slip::Array<int> v3(v1.size());
   **  slip::transform_mask_bin(v1.begin(),v1.end(),
   ** 		       mask.begin(),
   **		       v2.begin(),
   **		       v3.begin(),
   **		       std::plus<int>(),int(2));
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   */
  template<typename _InputIterator1, 
	   typename _InputIterator2,
	   typename _MaskIterator,
	   typename _OutputIterator, 
	   typename _BinaryOperation>
    _OutputIterator
  transform_mask_bin(_InputIterator1 first1, _InputIterator1 last1,
	       _MaskIterator mask_first,
	       _InputIterator2 first2, 
	       _OutputIterator result,
	       _BinaryOperation binary_op,
	       typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
    {
      assert(first1 != last1); 
       for ( ; first1 != last1; ++first1, ++first2,++result, ++mask_first)
	 {
	   if(*mask_first == value)
	     { 
	       *result = binary_op(*first1, *first2);
	     }
	 }
       return result;
     
    }


 /**
   **  \brief Perform an operation on a sequence according to a Predicate.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2008/09/27
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  result    An output iterator.
   **  \param  unary_op  A unary operator.
   **  \param  pred A predicate.
   **  \return An output iterator equal to result+(last-first).
   **  \par Description:
   **  Applies the operator to each element in the input range 
   **  according to the predicate pred and assigns
   **  the results to successive elements of the output sequence.
   **  Evaluates *(result+N)=unary_op(*(first+N)) for each N in the
   **  range [0,last-first) verifying pred(*first);
   **  \pre [first,last) must be valid.
   **  \pre [result,result + (last-first)) must be valid
   **  \pre unary_op must not alter its argument.
   **  \par Example1:
   **  \code
   **   //definition of a predicate
   **   template<typename T>
   **   bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   }
   ** \endcode
   ** \code
   **   slip::Array<int> v1(10);
   **   slip::iota(v1.begin(),v1.end(),1,1);
   **   std::cout<<"v1 = "<<v1<<std::endl;
   **   slip::Array<int> v2(10);
   **   slip::transform_if(v1.begin(),v1.end(),
   **                      v2.begin(),
   **	                   std::negate<int>(),
   **	                   lt5Predicate<int>);
   **  std::cout<<"v2 = "<<v2<<std::endl;
   **  \endcode
   **
   */
  template<typename _InputIterator, 
	   typename _OutputIterator,
	   typename _UnaryOperation,
	   typename _Predicate>
    _OutputIterator
    transform_if(_InputIterator first, _InputIterator last,
		 _OutputIterator result, 
		 _UnaryOperation unary_op,
		 _Predicate pred)
    {
       assert(first != last); 
       for ( ; first != last; ++first, ++result)
	 {
	   if(pred(*first))
	     { 
	       *result = unary_op(*first);
	     }
	 }
       return result;
    }


  /**
   **  \brief Perform an operation on corresponding elements of two sequences
   **  according to a Predicate.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2008/09/27
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  first2     An input iterator.
   **  \param  result     An output iterator.
   **  \param  binary_op  A binary operator.
   **  \param  pred A predicate.
   **  \return An output iterator equal to  result+(last-first).
   **  \par Description:
   **  Applies the operator to the corresponding elements in the two
   **  input ranges  according to the predicate pred and assigns 
   **  the results to successive elements of the output sequence.
   **  Evaluates *(result+N)=binary_op(*(first1+N),*(first2+N)) for each
   **   N in the range  [0,last1-first1) verifying pred(*first1).
   **  \pre [first1,last1) must be valid.
   **  \pre [first2,first2 + (last1-first1)) must be valid
   **  \pre [result,result + (last1-first1)) must be valid
   **  \pre binary_op must not alter either of its arguments.
   **  \par Example1:
   **  \code
   **   //definition of a predicate
   **   template<typename T>
   **   bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   }
   ** \endcode
   ** \code
   **   slip::Array<int> v1(10);
   **   slip::iota(v1.begin(),v1.end(),1,1);
   **   std::cout<<"v1 = "<<v1<<std::endl;
   **   slip::Array<int> v1 = v2;
   **   slip::Array<int> v3(v1.size());
   **   slip::transform_if(v1.begin(),v1.end(),
   **		           v2.begin(),
   **		           v3.begin(),
   **	                   std::plus<int>(),
   **		           lt5Predicate<int>);
   **   std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   */
  template<typename _InputIterator1, 
	   typename _InputIterator2,
	   typename _OutputIterator, 
	   typename _BinaryOperation,
	   typename _Predicate>
    _OutputIterator
  transform_if(_InputIterator1 first1, _InputIterator1 last1,
	       _InputIterator2 first2, 
	       _OutputIterator result,
	       _BinaryOperation binary_op,
	       _Predicate pred)
    {
      assert(first1 != last1); 
       for ( ; first1 != last1; ++first1, ++first2,++result)
	 {
	    if(pred(*first1))
	     { 
	       *result = binary_op(*first1, *first2);
	     }
	 }
       return result;
     
    }

 /* @} */

 /** \name max_element algorithms */
  /* @{ */

/**
   **  \brief Return the maximum element in a range according to a mask sequence or return last if no value verify the mask.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2009/01/13
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  mask_first An input iterator.
   **  \param  value true value of the mask range. Default is 1.
   **  \return Iterator referencing the first instance of the largest value
   **  \pre [first,last) must be valid.
   **  \pre [mask_first,mask_first2 + (last-first)) must be valid
   **  \code
   ** float f[] = {5.0,2.0,1.0,7.0,3.0,2.0};
   ** slip::Array2d<float> M(2,3,f);
   ** int d[] = {1,0,1,1,1,0};
   ** slip::Array2d<int> Mask(2,3,d);
   ** std::cout<<M<<std::endl;
   ** std::cout<<Mask<<std::endl;
   ** std::cout<<*(slip::max_element_mask(M.begin(),M.end(),Mask.begin(),1))<<std::endl;
   ** \endcode
   
  */

 template<typename _ForwardIterator,typename _MaskIterator>

  _ForwardIterator max_element_mask( _ForwardIterator first, 
				     _ForwardIterator last,
                                     _MaskIterator mask_first,
                                     typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
    {
      if (first == last) return first;
      bool nul_mask = true;
      _ForwardIterator result = first; 
       ++mask_first;
   
        while (++first != last)
        {  
	   if(*mask_first == value)
	     {
	       if(*result < *first)
		 { 
		   result = first;
		 }
	       nul_mask = false;
	     }
	   ++mask_first;
	}
       return (nul_mask ?  last : result);
     
    }

/**
   **  \brief Return the maximum element in a range according to a predicate or last if the predicate is never verified.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2009/01/13
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  pred       Predicate.
   **  \return Iterator referencing the first instance of the largest value
   **
   **  \pre [first,last) must be valid.
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f[] = {5.0,2.0,1.0,7.0,3.0,2.0};
   ** slip::Array2d<float> M(2,3,f);
   ** std::cout<<M<<std::endl;
   ** std::cout<<*(slip::max_element_if(M.begin(),M.end(),lt5Predicate<float>))<<std::endl;
   ** \endcode
   
  */

 template<typename _ForwardIterator,typename Predicate>

  _ForwardIterator max_element_if( _ForwardIterator first, _ForwardIterator last,
                                     Predicate pred)
    {
      if (first == last) return first;
      //find the first value verifying the predicate
      while(!pred(*first++)&& first != last)
	{}

      if(first == last) return last;

      _ForwardIterator result = first; 
      while (first != last)
        {  
	  if(pred(*first) && (*first > *result))
	    {
	      result = first;
	    }
	  ++first;
	}
	
      return result;
     
    }
/**
   **  \brief Return the maximum element in a range using comparison functor
   **  according to a mask sequence or return last if no value verify the mask.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2009/01/13
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  mask_first An input iterator.
   **  \param  comp Comparison functor.
   **  \param  value true value of the mask range. Default is 1.
   **  \return Iterator referencing the first instance of the largest value
   **
   **  \pre [first,last) must be valid.
   **  \pre [mask_first,mask_first + (last-first)) must be valid.
   **  \code
   ** template<typename T>
   ** bool compare(const T val1, const T val2)
   **  {
   **     return (val1<val2); 
   **  }
   ** float f[] = {5.0,2.0,1.0,7.0,3.0,2.0};
   ** slip::Array2d<float> M(2,3,f);
   ** int d[] = {1,0,1,1,1,0};
   ** slip::Array2d<int> Mask(2,3,d);
   ** std::cout<<M<<std::endl;
   ** std::cout<<Mask<<std::endl;
   ** std::cout<<*(slip::max_element_mask_compare(M.begin(),M.end(),Mask.begin(),compare<float>,1))<<std::endl;
   ** \endcode
   
  */

 template<typename _ForwardIterator,typename _MaskIterator,typename _Compare>

  _ForwardIterator max_element_mask_compare( _ForwardIterator first, _ForwardIterator last,
                                     _MaskIterator mask_first,_Compare comp,
                                     typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
    {
         if (first == last) return first;
      bool nul_mask = true;
      _ForwardIterator result = first; 
       ++mask_first;
   
        while (++first != last)
        {  
	   if(*mask_first == value)
	     {
	       if(comp(*result,*first))
		 { 
		   result = first;
		 }
	       nul_mask = false;
	     }
	   ++mask_first;
	}
       return (nul_mask ?  last : result);
      if (first == last) return first;
     
     
    }

/**
   **  \brief Return the maximum element in a range using comparison functor
   **  according to a predicate  or return last if the predicate is never verified.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2009/01/13
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  pred       Predicate.
   **  \param  comp Comparison functor.
   **  \return Iterator referencing the first instance of the largest value
   **  \pre [first,last) must be valid.
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** bool compare(const T val1, const T val2)
   **  {
   **     return (val1<val2); 
   **  }
   ** \endcode
   ** \code
   ** float f[] = {5.0,2.0,1.0,7.0,3.0,2.0};
   ** slip::Array2d<float> M(2,3,f);
   ** int d[] = {1,0,1,1,1,0};
   ** std::cout<<M<<std::endl;
   ** std::cout<<*(slip::max_element_if_compare(M.begin(),M.end(),lt5Predicate<float>,compare<float>))<<std::endl;
   ** \endcode
   
  */

 template<typename _ForwardIterator,typename _Compare,typename Predicate>

  _ForwardIterator max_element_if_compare( _ForwardIterator first, _ForwardIterator last,
                                     Predicate pred,_Compare comp)
    {
       if (first == last) return first;
      //find the first value verifying the predicate
      while(!pred(*first++)&& first != last)
	{}

      if(first == last) return last;

      _ForwardIterator result = first; 
      while (first != last)
        {  
	  if(pred(*first) && comp(*result,*first))
	    {
	      result = first;
	    }
	  ++first;
	}
	
      return result;
    }

/* @} */

 /** \name min_element algorithms */
  /* @{ */
/**
   **  \brief Return the minimum element in a range according to a mask sequence or return last if no value verify the mask.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2009/01/13
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  mask_first An input iterator.
   **  \param  value true value of the mask range. Default is 1.
   **  \return Iterator referencing the first instance of the smallest value
   **
   **  \pre [first,last) must be valid.
   **  \pre [mask_first,mask_first + (last-first)) must be valid.
   **  \code
   ** float f[] = {5.0,2.0,1.0,7.0,3.0,2.0};
   ** slip::Array2d<float> M(2,3,f);
   ** int d[] = {1,0,1,1,1,0};
   ** slip::Array2d<int> Mask(2,3,d);
   ** std::cout<<M<<std::endl;
   ** std::cout<<Mask<<std::endl;
   ** std::cout<<*(slip::min_element_mask(M.begin(),M.end(),Mask.begin(),1))<<std::endl;
   ** \endcode
   
  */

 template<typename _ForwardIterator,typename _MaskIterator>

  _ForwardIterator min_element_mask( _ForwardIterator first, _ForwardIterator last,
                                     _MaskIterator mask_first,
                                     typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
    {
        if (first == last) return first;
      bool nul_mask = true;
      _ForwardIterator result = first; 
       ++mask_first;
   
        while (++first != last)
        {  
	   if(*mask_first == value)
	     {
	       if(*result > *first)
		 { 
		   result = first;
		 }
	       nul_mask = false;
	     }
	   ++mask_first;
	}
       return (nul_mask ?  last : result);
     
     
    }
/**
   **  \brief Return the minimum element in a range using comparison functor
   **  according to a mask sequence or return last if no value verify the mask.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2009/01/13
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  mask_first An input iterator.
   **  \param  comp Comparison functor.
   **  \param  value true value of the mask range. Default is 1.
   **  \return Iterator referencing the first instance of the smallest value
   **
   **  \pre [first,last) must be valid.
   **  \pre [mask_first,mask_first + (last-first)) must be valid.
   **  \code
   ** bool compare(const T val1, const T val2)
   **  {
   **     return (val1>val2); 
   **  }
   ** float f[] = {5.0,2.0,1.0,7.0,3.0,2.0};
   ** slip::Array2d<float> M(2,3,f);
   ** int d[] = {1,0,1,1,1,0};
   ** slip::Array2d<int> Mask(2,3,d);
   ** std::cout<<M<<std::endl;
   ** std::cout<<Mask<<std::endl;
   ** std::cout<<*(slip::min_element_mask_compare(M.begin(),M.end(),Mask.begin(),compare<float>,1))<<std::endl;
   ** \endcode
   
  */

 template<typename _ForwardIterator,typename _MaskIterator,typename _Compare>

  _ForwardIterator min_element_mask_compare( _ForwardIterator first, _ForwardIterator last,
                                     _MaskIterator mask_first,_Compare comp,
                                     typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
    {
  
      return slip::max_element_mask_compare(first,last,mask_first,comp,value);
     
    }

/**
   **  \brief Return the minimum element in a range according to a predicate or return last if the predicate is never verified.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2009/01/13
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  pred       Predicate.
   **  \return Iterator referencing the first instance of the smallest value
   **  \pre [first,last] must be valid.
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f[] = {5.0,2.0,1.0,7.0,3.0,2.0};
   ** slip::Array2d<float> M(2,3,f);
   ** std::cout<<M<<std::endl;
   ** std::cout<<*(slip::min_element_if(M.begin(),M.end(),lt5Predicate<float>))<<std::endl;
   ** \endcode
   
  */

 template<typename _ForwardIterator,typename Predicate>

  _ForwardIterator min_element_if( _ForwardIterator first, _ForwardIterator last,
                                     Predicate pred)
    { 
      if (first == last) return first;
      //find the first value verifying the predicate
      while(!pred(*first++)&& first != last)
	{}

      if(first == last) return last;

      _ForwardIterator result = first; 
      while (first != last)
        {  
	  if(pred(*first) && (*first < *result))
	    {
	      result = first;
	    }
	  ++first;
	}
	
      return result;
      
     
    }

/**
   **  \brief Return the minimum element in a range using comparison functor
   **  according to a predicate or return last if the predicate is never verified.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2009/01/13
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  pred       Predicate.
   **  \param  comp Comparison functor.
   **  \return Iterator referencing the first instance of the smallest value
   **  \pre [first,last) must be valid.
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** bool compare(const T val1, const T val2)
   **  {
   **     return (val1>val2); 
   **  }
   ** \endcode
   ** \code
   ** float f[] = {5.0,2.0,1.0,7.0,3.0,2.0};
   ** slip::Array2d<float> M(2,3,f);
   ** int d[] = {1,0,1,1,1,0};
   ** std::cout<<M<<std::endl;
   ** std::cout<<*(slip::min_element_if_compare(M.begin(),M.end(),lt5Predicate<float>,compare<float>))<<std::endl;
   ** \endcode
   
  */

 template<typename _ForwardIterator,typename _Compare,typename Predicate>

  _ForwardIterator min_element_if_compare( _ForwardIterator first, _ForwardIterator last,
                                     Predicate pred,_Compare comp)
    {
      return slip::max_element_if_compare(first,last,pred,comp);
    }

/* @} */
}//slip::

#endif //SLIP_STL_ALGO_EXT_HPP

