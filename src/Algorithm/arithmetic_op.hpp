/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file arithmetic_op.hpp
 * 
 * \brief Provides some algorithms to computes arithmetical operations on ranges.
 * 
 */
#ifndef SLIP_ARITHMETIC_OP_HPP
#define SLIP_ARITHMETIC_OP_HPP

#include <iostream>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <cassert>
#include <cmath>
#include <functional>
#include "stl_algo_ext.hpp"
#include "macros.hpp"
namespace slip
{

 
 
  /** \name Operations between two ranges*/
  /* @{ */

  /**
   ** \brief Computes the addition of two ranges 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param __first1 An InputIterator.
   ** \param __last1   An InputIterator.
   ** \param __first2 An InputIterator.
   ** \param __result An OutputIterator.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   ** \par Example:
   ** \code
   ** //additon of two Array2d
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> M2(4,5,2);
   ** slip::Array2d<int> M3(4,5);
   ** slip::plus(M.begin(),M.end(),M2.begin(),M3.begin());
   ** \endcode
   */
  template<typename InputIterator1, 
	   typename InputIterator2,
	   typename OutputIterator>
  inline
  void plus(InputIterator1 __first1,
	    InputIterator1 __last1,
	    InputIterator2 __first2,
	    OutputIterator __result)
  {
    std::transform(__first1,__last1,
		   __first2,
		   __result,
		   std::plus<typename std::iterator_traits<OutputIterator>::value_type>());
  }


  /**
   **  \brief Computes the addition of two ranges 
   **  according to a mask sequence.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/04
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __mask_first An input iterator.
   **  \param  __first2     An input iterator.
   **  \param  __result     An output iterator.
   **  \param  value      true value of the mask range. Default is 1.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__mask_first,__mask_first + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   **  \par Example1:
   **  \code
   **  // addtion of two Array2d
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2(v1);
   **  std::cout<<"v2 = "<<v2<<std::endl;
   **  slip::Array<bool> mask(v1.size());
   **  mask[2] = 1;
   **  mask[4] = 1;
   **  mask[9] = 1;
   **  slip::Array<int> v3(v1.size());
   **  slip::plus_mask(v1.begin(),v1.end(),
   **                        mask.begin(),
   **                       v2.begin(),
   **                       v3.begin());
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** slip::Array<int> v1(10);
   ** slip::iota(v1.begin(),v1.end(),1,1);
   ** std::cout<<"v1 = "<<v1<<std::endl;
   ** slip::Array<int> v2(v1);
   ** std::cout<<"v2 = "<<v2<<std::endl;
   ** slip::Array<int> maskint(v1.size());
   ** maskint[2] = 2;
   ** maskint[4] = 2;
   ** maskint[9] = 2;
   ** slip::Array<int> v3(v1.size());
   ** slip::plus_mask(v1.begin(),v1.end(),
   maskint.begin(),
   v2.begin(),
   v3.begin(),
   int(2));
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   */
  template<typename _InputIterator1, 
           typename _InputIterator2,
           typename _MaskIterator,
           typename _OutputIterator>
  inline
  void plus_mask(_InputIterator1 __first1, _InputIterator1 __last1,
		 _MaskIterator __mask_first,
		 _InputIterator2 __first2, 
		 _OutputIterator __result,
		 typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
  {
    slip::transform_mask_bin(
			     __first1, 
			     __last1,
			     __mask_first,
			     __first2, 
			     __result,
			     std::plus<typename std::iterator_traits<_InputIterator1>::value_type>(),
			     value);

     
  }

  /** 
   **  \brief Computes the addition of two ranges 
   **  according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/04
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __first2     An input iterator.
   **  \param  __result     An output iterator.
   **  \param  __pred       A predicate.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   **  \par Example:
   **  \code
   **  // addtion of two Array2d according to a Predicate.
   ** template <typename T>
   ** bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2 = v1;
   **  slip::Array<int> v3(v1.size());
   **  slip::plus_if(v1.begin(),v1.end(),
   **              v2.begin(),
   **              v3.begin(),
   **              lt5Predicate<int>);
   **  std::cout<<"v2 = "<<v2<<std::endl; 
   **  std::cout<<"v3 = "<<v3<<std::endl; 
   ** \endcode
   */
  template<typename _InputIterator1, 
           typename _InputIterator2,
	   typename _OutputIterator,
           typename _Predicate>
  inline
  void plus_if(_InputIterator1 __first1, _InputIterator1 __last1,
               _InputIterator2 __first2, 
               _OutputIterator __result,
	       _Predicate __pred)
  {
    slip::transform_if(
		       __first1, 
		       __last1,
		       __first2, 
		       __result,
		       std::plus<typename std::iterator_traits<_InputIterator1>::value_type>(),
		       __pred);

     
  }


  /**
   ** \brief Computes the difference of two ranges.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param __first1 An InputIterator.
   ** \param __last1   An InputIterator.
   ** \param __first2 An InputIterator.
   ** \param __result An OutputIterator.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   ** \par Example:
   ** \code
   ** //substraction of two Array2d
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> M2(4,5,2);
   ** slip::Array2d<int> M3(4,5);
   ** slip::minus(M.begin(),M.end(),M2.begin(),M3.begin());
   ** \endcode
   */
  template<typename InputIterator1, 
	   typename InputIterator2,
	   typename OutputIterator>
  inline
  void minus(InputIterator1 __first1,
	     InputIterator1 __last1,
	     InputIterator2 __first2,
	     OutputIterator __result)
  {
    std::transform(__first1,__last1,
		   __first2,
		   __result,
		   std::minus<typename std::iterator_traits<OutputIterator>::value_type>());
  }

  /** 
   **  \brief Computes the difference of two ranges 
   **  according to a mask sequence.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/04
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __mask_first An input iterator.
   **  \param  __first2     An input iterator.
   **  \param  __result     An output iterator.
   **  \param  value      true value of the mask range. Default is 1.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__mask_first,__mask_first + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   **  \par Example1:
   **  \code
   **  // difference of two Array2d according to a mask
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2(v1);
   **  std::cout<<"v2 = "<<v2<<std::endl;
   **  slip::Array<bool> mask(v1.size());
   **  mask[2] = 1;
   **  mask[4] = 1;
   **  mask[9] = 1;
   **  slip::Array<int> v3(v1.size());
   **  slip::minus_mask(v1.begin(),v1.end(),
   **                        mask.begin(),
   **                       v2.begin(),
   **                       v3.begin());
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** slip::Array<int> v1(10);
   ** slip::iota(v1.begin(),v1.end(),1,1);
   ** std::cout<<"v1 = "<<v1<<std::endl;
   ** slip::Array<int> v2(v1);
   ** std::cout<<"v2 = "<<v2<<std::endl;
   ** slip::Array<int> maskint(v1.size());
   ** maskint[2] = 2;
   ** maskint[4] = 2;
   ** maskint[9] = 2;
   ** slip::Array<int> v3(v1.size());
   ** slip::minus_mask(v1.begin(),v1.end(),
   maskint.begin(),
   v2.begin(),
   v3.begin(),
   int(2));
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   */

  template<typename _InputIterator1, 
           typename _InputIterator2,
           typename _MaskIterator,
           typename _OutputIterator>
  inline
  void minus_mask(_InputIterator1 __first1, _InputIterator1 __last1,
		  _MaskIterator __mask_first,
		  _InputIterator2 __first2, 
		  _OutputIterator __result,
		  typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
  {
    slip::transform_mask_bin(
			     __first1, 
			     __last1,
			     __mask_first,
			     __first2, 
			     __result,
			     std::minus<typename std::iterator_traits<_InputIterator1>::value_type>(),
			     value);

     
  }

  /** 
   **  \brief Computes the difference of two ranges 
   **  according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/04
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __first2     An input iterator.
   **  \param  __result     An output iterator.
   **  \param  __pred       A predicate.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   **  \par Example:
   **  \code
   **  // minus of two Array2d according to a Predicate.
   ** template <typename T>
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };  
   ** \endcode
   ** \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2 = v1;
   **  slip::Array<int> v3(v1.size());
   **  slip::minus_if(v1.begin(),v1.end(),
   **              v2.begin(),
   **              v3.begin(),
   **              lt5Predicate<int>);
   **  std::cout<<"v2 = "<<v2<<std::endl; 
   **  std::cout<<"v3 = "<<v3<<std::endl; 
   ** \endcode
   
   */
  template<typename _InputIterator1, 
           typename _InputIterator2,
	   typename _OutputIterator,
           typename _Predicate>
  inline
  void minus_if(_InputIterator1 __first1, _InputIterator1 __last1,
		_InputIterator2 __first2, 
		_OutputIterator __result,
                _Predicate __pred)
  {
    slip::transform_if(
		       __first1, 
		       __last1,
		       __first2, 
		       __result,
		       std::minus<typename std::iterator_traits<_InputIterator1>::value_type>(),
		       __pred);

     
  }

  /**
   ** \brief Computes the pointwise product of two ranges.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param __first1 An InputIterator.
   ** \param __last1   An InputIterator.
   ** \param __first2 An InputIterator.
   ** \param __result An OutputIterator.
   **
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   ** \par Example:
   ** \code
   ** //bitwise multiplication of two Array2d
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> M2(4,5,2);
   ** slip::Array2d<int> M3(4,5);
   ** slip::multiplies(M.begin(),M.end(),M2.begin(),M3.begin());
   ** \endcode
 
   **
   */
  template<typename InputIterator1, 
	   typename InputIterator2,
	   typename OutputIterator>
  inline
  void multiplies(InputIterator1 __first1,
		  InputIterator1 __last1,
		  InputIterator2 __first2,
		  OutputIterator __result)
  {
    std::transform(__first1,__last1,
		   __first2,
		   __result,
		   std::multiplies<typename std::iterator_traits<OutputIterator>::value_type>());
  }


  /** 
   **  \brief Computes the pointwise product of two ranges 
   **  according to a mask sequence.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/04
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __mask_first An input iterator.
   **  \param  __first2     An input iterator.
   **  \param  __result     An output iterator.
   **  \param  value      true value of the mask range. Default is 1.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__mask_first,__mask_first + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   **  \par Example1:
   **  \code
   **  // Computes the pointwise product of two ranges of two Array2d according to a mask
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2(v1);
   **  std::cout<<"v2 = "<<v2<<std::endl;
   **  slip::Array<bool> mask(v1.size());
   **  mask[2] = 1;
   **  mask[4] = 1;
   **  mask[9] = 1;
   **  slip::Array<int> v3(v1.size());
   **  slip::multiplies_mask(v1.begin(),v1.end(),
   **                        mask.begin(),
   **                       v2.begin(),
   **                       v3.begin());
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** slip::Array<int> v1(10);
   ** slip::iota(v1.begin(),v1.end(),1,1);
   ** std::cout<<"v1 = "<<v1<<std::endl;
   ** slip::Array<int> v2(v1);
   ** std::cout<<"v2 = "<<v2<<std::endl;
   ** slip::Array<int> maskint(v1.size());
   ** maskint[2] = 2;
   ** maskint[4] = 2;
   ** maskint[9] = 2;
   ** slip::Array<int> v3(v1.size());
   ** slip::multiplies_mask(v1.begin(),v1.end(),
   maskint.begin(),
   v2.begin(),
   v3.begin(),
   int(2));
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   */
  template<typename _InputIterator1, 
           typename _InputIterator2,
           typename _MaskIterator,
           typename _OutputIterator>
  inline
  void multiplies_mask(_InputIterator1 __first1, _InputIterator1 __last1,
		       _MaskIterator __mask_first,
		       _InputIterator2 __first2, 
		       _OutputIterator __result,
		       typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
  {
    slip::transform_mask_bin(
			     __first1, 
			     __last1,
			     __mask_first,
			     __first2, 
			     __result,
			     std::multiplies<typename std::iterator_traits<_InputIterator1>::value_type>(),
			     value);

     
  }




  /** 
   **  \brief Computes the pointwise product of two ranges 
   **  according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/04
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __first2     An input iterator.
   **  \param  __result     An output iterator.
   **  \param  __pred       A predicate.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   **  \par Example:
   **  \code
   ** template <typename T>
   ** bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   **  \endcode
   **  \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2 = v1;
   **  slip::Array<int> v3(v1.size());
   **  slip::multiplies_if(v1.begin(),v1.end(),
   **              v2.begin(),
   **              v3.begin(),
   **              lt5Predicate<int>);
   **  std::cout<<"v2 = "<<v2<<std::endl; 
   **  std::cout<<"v3 = "<<v3<<std::endl; 
   ** \endcode
   */
  template<typename _InputIterator1, 
           typename _InputIterator2,
	   typename _OutputIterator,
           typename _Predicate>
  inline
  void multiplies_if(_InputIterator1 __first1, _InputIterator1 __last1,
		     _InputIterator2 __first2, 
		     _OutputIterator __result,
		     _Predicate __pred)
  {
    slip::transform_if(
		       __first1, 
		       __last1,
		       __first2, 
		       __result,
		       std::multiplies<typename std::iterator_traits<_InputIterator1>::value_type>(),
		       __pred);

     
  }


  /**
   ** \brief Computes the pointwise division of two ranges.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param __first1 An InputIterator.
   ** \param __last1   An InputIterator.
   ** \param __first2 An InputIterator.
   ** \param __result An OutputIterator.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   ** \par Example:
   ** \code
   ** //bitwise division of two Array2d
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> M2(4,5,2);
   ** slip::Array2d<int> M3(4,5);
   ** slip::divides(M.begin(),M.end(),M2.begin(),M3.begin());
   ** \endcode
   **
   */
  template<typename InputIterator1, 
	   typename InputIterator2,
	   typename OutputIterator>
  inline
  void divides(InputIterator1 __first1,
	       InputIterator1 __last1,
	       InputIterator2 __first2,
	       OutputIterator __result)
  {
    std::transform(__first1,__last1,
		   __first2,
		   __result,
		   std::divides<typename std::iterator_traits<OutputIterator>::value_type>());
  }

  /** 
   **  \brief Computes the pointwise division of two ranges 
   **  according to a mask sequence.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/04
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __mask_first An input iterator.
   **  \param  __first2     An input iterator.
   **  \param  __result     An output iterator.
   **  \param  value      true value of the mask range. Default is 1.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__mask_first,__mask_first + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   **  \par Example1:
   **  \code
   **  // Computes the pointwise division of two ranges of two Array2d according to a mask
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2(v1);
   **  std::cout<<"v2 = "<<v2<<std::endl;
   **  slip::Array<bool> mask(v1.size());
   **  mask[2] = 1;
   **  mask[4] = 1;
   **  mask[9] = 1;
   **  slip::Array<int> v3(v1.size());
   **  slip::divides_mask(v1.begin(),v1.end(),
   **                        mask.begin(),
   **                       v2.begin(),
   **                       v3.begin());
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** slip::Array<int> v1(10);
   ** slip::iota(v1.begin(),v1.end(),1,1);
   ** std::cout<<"v1 = "<<v1<<std::endl;
   ** slip::Array<int> v2(v1);
   ** std::cout<<"v2 = "<<v2<<std::endl;
   ** slip::Array<int> maskint(v1.size());
   ** maskint[2] = 2;
   ** maskint[4] = 2;
   ** maskint[9] = 2;
   ** slip::Array<int> v3(v1.size());
   ** slip::divides_mask(v1.begin(),v1.end(),
   maskint.begin(),
   v2.begin(),
   v3.begin(),
   int(2));
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   */
  template<typename _InputIterator1, 
           typename _InputIterator2,
           typename _MaskIterator,
           typename _OutputIterator>
  inline
  void divides_mask(_InputIterator1 __first1, _InputIterator1 __last1,
		    _MaskIterator __mask_first,
		    _InputIterator2 __first2, 
		    _OutputIterator __result,
		    typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
  {
    slip::transform_mask_bin(
			     __first1, 
			     __last1,
			     __mask_first,
			     __first2, 
			     __result,
			     std::divides<typename std::iterator_traits<_InputIterator1>::value_type>(),
			     value);

     
  }



  /** 
   **  \brief Computes the pointwise division of two ranges. 
   **  according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/04
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __first2     An input iterator.
   **  \param  __result     An output iterator.
   **  \param  __pred       A predicate.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   **  \par Example:
   **  \code
   ** template <typename T>
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   **  \endcode
   **  \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2 = v1;
   **  slip::Array<int> v3(v1.size());
   **  slip::divides_if(v1.begin(),v1.end(),
   **              v2.begin(),
   **              v3.begin(),
   **              lt5Predicate<int>);
   **  std::cout<<"v2 = "<<v2<<std::endl; 
   **  std::cout<<"v3 = "<<v3<<std::endl; 
   ** \endcode
   */
  template<typename _InputIterator1, 
           typename _InputIterator2,
	   typename _OutputIterator,
           typename _Predicate>
  inline
  void divides_if(_InputIterator1 __first1, _InputIterator1 __last1,
		  _InputIterator2 __first2, 
		  _OutputIterator __result,
		  _Predicate __pred)
  {
    slip::transform_if(
		       __first1, 
		       __last1,
		       __first2, 
		       __result,
		       std::divides<typename std::iterator_traits<_InputIterator1>::value_type>(),
		       __pred);

     
  }

  /**
   ** \brief Computes the minimum of two ranges.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param __first1 An InputIterator.
   ** \param __last1   An InputIterator.
   ** \param __first2 An InputIterator.
   ** \param __result An OutputIterator.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   ** \par Example:
   ** \code
   ** //computes the bitwise minimum of two Array2d
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> M2(4,5,2);
   ** slip::Array2d<int> M3(4,5);
   ** slip::minimun(M.begin(),M.end(),M2.begin(),M3.begin());
   ** \endcode
   **
   */
  template<typename InputIterator1, 
	   typename InputIterator2,
	   typename OutputIterator>
  inline
  void minimum(InputIterator1 __first1,
	       InputIterator1 __last1,
	       InputIterator2 __first2,
	       OutputIterator __result)
  {
    std::transform(__first1,__last1,
		   __first2,
		   __result,
		   slip::mini<typename std::iterator_traits<OutputIterator>::value_type>());
  }

  /**
   **  \brief Computes the minimum of two ranges 
   **  according to a mask sequence.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/04
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __mask_first An input iterator.
   **  \param  __first2     An input iterator.
   **  \param  __result     An output iterator.
   **  \param  value      true value of the mask range. Default is 1.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__mask_first,__mask_first + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   **  \par Example1:
   **  \code
   **  // minimum of two Array2d
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2(v1);
   **  std::cout<<"v2 = "<<v2<<std::endl;
   **  slip::Array<bool> mask(v1.size());
   **  mask[2] = 1;
   **  mask[4] = 1;
   **  mask[9] = 1;
   **  slip::Array<int> v3(v1.size());
   **  slip::minimum_mask(v1.begin(),v1.end(),
   **                        mask.begin(),
   **                       v2.begin(),
   **                       v3.begin());
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** slip::Array<int> v1(10);
   ** slip::iota(v1.begin(),v1.end(),1,1);
   ** std::cout<<"v1 = "<<v1<<std::endl;
   ** slip::Array<int> v2(v1);
   ** std::cout<<"v2 = "<<v2<<std::endl;
   ** slip::Array<int> maskint(v1.size());
   ** maskint[2] = 2;
   ** maskint[4] = 2;
   ** maskint[9] = 2;
   ** slip::Array<int> v3(v1.size());
   ** slip::minimum_mask(v1.begin(),v1.end(),
   maskint.begin(),
   v2.begin(),
   v3.begin(),
   int(2));
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   */
  template<typename _InputIterator1, 
           typename _InputIterator2,
           typename _MaskIterator,
           typename _OutputIterator>
  inline
  void minimum_mask(_InputIterator1 __first1, _InputIterator1 __last1,
		    _MaskIterator __mask_first,
		    _InputIterator2 __first2, 
		    _OutputIterator __result,
		    typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
  {
    slip::transform_mask_bin(
			     __first1, 
			     __last1,
			     __mask_first,
			     __first2, 
			     __result,
			     slip::mini<typename std::iterator_traits<_InputIterator1>::value_type>(),
			     value);

     
  }

  /** 
   **  \brief Computes the minimum of two ranges 
   **  according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/04
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __first2     An input iterator.
   **  \param  __result     An output iterator.
   **  \param  __pred       A predicate.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   **  \par Example:
   **  \code
   **  // minimum two Array2d according to a Predicate.
   ** template <typename T>
   ** bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2 = v1;
   **  slip::Array<int> v3(v1.size());
   **  slip::minimum_if(v1.begin(),v1.end(),
   **              v2.begin(),
   **              v3.begin(),
   **              lt5Predicate<int>);
   **  std::cout<<"v2 = "<<v2<<std::endl; 
   **  std::cout<<"v3 = "<<v3<<std::endl; 
   ** \endcode
   */
  template<typename _InputIterator1, 
           typename _InputIterator2,
	   typename _OutputIterator,
           typename _Predicate>
  inline
  void minimum_if(_InputIterator1 __first1, _InputIterator1 __last1,
		  _InputIterator2 __first2, 
		  _OutputIterator __result,
		  _Predicate __pred)
  {
    slip::transform_if(
		       __first1, 
		       __last1,
		       __first2, 
		       __result,
		       slip::mini<typename std::iterator_traits<_InputIterator1>::value_type>(),
		       __pred);

     
  }


  /**
   ** \brief Computes the maximum of two ranges.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param __first1 An InputIterator.
   ** \param __last1   An InputIterator.
   ** \param __first2 An InputIterator.
   ** \param __result An OutputIterator.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   ** \par Example:
   ** \code
   ** //computes the bitwise maximum of two Array2d
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> M2(4,5,2);
   ** slip::Array2d<int> M3(4,5);
   ** slip::maximum(M.begin(),M.end(),M2.begin(),M3.begin());
   ** \endcode
   **
   */
  template<typename InputIterator1, 
	   typename InputIterator2,
	   typename OutputIterator>
  inline
  void maximum(InputIterator1 __first1,
	       InputIterator1 __last1,
	       InputIterator2 __first2,
	       OutputIterator __result)
  {
    std::transform(__first1,__last1,
		   __first2,
		   __result,
		   slip::maxi<typename std::iterator_traits<OutputIterator>::value_type>());
  }

  /**
   **  \brief Computes the maximum of two ranges 
   **  according to a mask sequence.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/04
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __mask_first An input iterator.
   **  \param  __first2     An input iterator.
   **  \param  __result     An output iterator.
   **  \param  value      true value of the mask range. Default is 1.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__mask_first,__mask_first + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   **  \par Example1:
   **  \code
   **  // addtion of two Array2d
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2(v1);
   **  std::cout<<"v2 = "<<v2<<std::endl;
   **  slip::Array<bool> mask(v1.size());
   **  mask[2] = 1;
   **  mask[4] = 1;
   **  mask[9] = 1;
   **  slip::Array<int> v3(v1.size());
   **  slip::maximum_mask(v1.begin(),v1.end(),
   **                        mask.begin(),
   **                       v2.begin(),
   **                       v3.begin());
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** slip::Array<int> v1(10);
   ** slip::iota(v1.begin(),v1.end(),1,1);
   ** std::cout<<"v1 = "<<v1<<std::endl;
   ** slip::Array<int> v2(v1);
   ** std::cout<<"v2 = "<<v2<<std::endl;
   ** slip::Array<int> maskint(v1.size());
   ** maskint[2] = 2;
   ** maskint[4] = 2;
   ** maskint[9] = 2;
   ** slip::Array<int> v3(v1.size());
   ** slip::maximum_mask(v1.begin(),v1.end(),
   **                    maskint.begin(),
   **                    v2.begin(),
   **                    v3.begin(),
   **                    int(2));
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   */
  template<typename _InputIterator1, 
           typename _InputIterator2,
           typename _MaskIterator,
           typename _OutputIterator>
  inline
  void maximum_mask(_InputIterator1 __first1, _InputIterator1 __last1,
		    _MaskIterator __mask_first,
		    _InputIterator2 __first2, 
		    _OutputIterator __result,
		    typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
  {
    slip::transform_mask_bin(
			     __first1, 
			     __last1,
			     __mask_first,
			     __first2, 
			     __result,
			     slip::maxi<typename std::iterator_traits<_InputIterator1>::value_type>(),
			     value);

     
  }

  /** 
   **  \brief Computes the maximum of two ranges 
   **  according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/04
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __first2     An input iterator.
   **  \param  __result     An output iterator.
   **  \param  __pred       A predicate.
   ** \pre  The two ranges and the result range must have the same
   **       value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__first2,__first2 + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) or
   **      ]__first2,__first2 + (__last1 -  __first1)).
   **  \par Example:
   **  \code
   **  // maximum of two Array2d according to a Predicate.
   ** template <typename T>
   ** bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2 = v1;
   **  slip::Array<int> v3(v1.size());
   **  slip::maximum_if(v1.begin(),v1.end(),
   **              v2.begin(),
   **              v3.begin(),
   **              lt5Predicate<int>);
   **  std::cout<<"v2 = "<<v2<<std::endl; 
   **  std::cout<<"v3 = "<<v3<<std::endl; 
   ** \endcode
   */
  template<typename _InputIterator1, 
           typename _InputIterator2,
	   typename _OutputIterator,
           typename _Predicate>
  inline
  void maximum_if(_InputIterator1 __first1, _InputIterator1 __last1,
		  _InputIterator2 __first2, 
		  _OutputIterator __result,
		  _Predicate __pred)
  {
    slip::transform_if(
		       __first1, 
		       __last1,
		       __first2, 
		       __result,
		       slip::maxi<typename std::iterator_traits<_InputIterator1>::value_type>(),
		       __pred);

     
  }


  /* @} */


  /** \name Operations between a range and a scalar*/
  /* @{ */
  /**
   ** \brief Adds a scalar to a range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/08/23
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param __first1 An InputIterator.
   ** \param __last1   An InputIterator.
   ** \param scalar A scalar value.
   ** \param __result An OutputIterator.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1)
   ** \par Example:
   ** \code
   ** //adds 5 to M
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> M3(4,5);
   ** slip::plus_scalar(M.begin(),M.end(),5,M3.begin());
   ** \endcode
   */
  template<typename InputIterator1, 
	   typename OutputIterator>
  inline
  void plus_scalar(InputIterator1 __first1,
		   InputIterator1 __last1,
		   const typename std::iterator_traits<InputIterator1>::value_type& scalar,
		   OutputIterator __result)
  {
    std::transform(__first1,__last1,
		   __result,
		   std::bind2nd(std::plus<typename std::iterator_traits<InputIterator1>::value_type>(),scalar));
  }


   /**
   **  \brief Adds a scalar to a range according to a mask sequence.
   ** \author Tremblais Benoit <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/08/23
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __mask_first An input iterator.
   **  \param scalar A scalar value.
   **  \param  __result     An output iterator.
   **  \param  value      true value of the mask range. Default is 1.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__mask_first,__mask_first + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1)
   **  \par Example1:
   **  \code
   **  // adds 5 to v1
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<bool> mask(v1.size());
   **  mask[2] = 1;
   **  mask[4] = 1;
   **  mask[9] = 1;
   **  slip::Array<int> v3(v1.size());
   **  slip::plus_scalar_mask(v1.begin(),v1.end(),
   **                        mask.begin(),
   **                        5,  
   **                       v3.begin());
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** slip::Array<int> v1(10);
   ** slip::iota(v1.begin(),v1.end(),1,1);
   ** std::cout<<"v1 = "<<v1<<std::endl;
   ** slip::Array<int> maskint(v1.size());
   ** maskint[2] = 2;
   ** maskint[4] = 2;
   ** maskint[9] = 2;
   ** slip::Array<int> v3(v1.size());
   ** slip::plus_scalar_mask(v1.begin(),v1.end(),
   **                        maskint.begin(),
   **                        5,
   **                        v3.begin(),
   **                        int(2));
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   */
  template<typename _InputIterator1, 
	   typename _MaskIterator,
           typename _OutputIterator>
  inline
  void plus_scalar_mask(_InputIterator1 __first1, _InputIterator1 __last1,
			_MaskIterator __mask_first,
			const typename std::iterator_traits<_InputIterator1>::value_type& scalar,
			_OutputIterator __result,
		 typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
  {
    slip::transform_mask_un(
			     __first1, 
			     __last1,
			     __mask_first,
			     __result,
			     std::bind2nd(std::plus<typename std::iterator_traits<_InputIterator1>::value_type>(),scalar),
			     value);

     
  }


   /** 
   **  \brief Adds a scalar to a range according to a Predicate.
   ** \author Tremblais Benoit <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/08/23
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param scalar A scalar value.
   **  \param  __result     An output iterator.
   **  \param  __pred       A predicate.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1)
   **  \par Example:
   **  \code
   **  // adds 5 to v1 according to the Predicate lt5Predicate.
   ** template <typename T>
   ** bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2 = v1;
   **  slip::Array<int> v3(v1.size());
   **  slip::plus_scalar_if(v1.begin(),v1.end(),
   **              5,
   **              v3.begin(),
   **              lt5Predicate<int>);
   **  std::cout<<"v3 = "<<v3<<std::endl; 
   ** \endcode
   */
  template<typename _InputIterator1, 
    	   typename _OutputIterator,
           typename _Predicate>
  inline
  void plus_scalar_if(_InputIterator1 __first1, _InputIterator1 __last1,
		      const typename std::iterator_traits<_InputIterator1>::value_type& scalar,
               _OutputIterator __result,
	       _Predicate __pred)
  {
    slip::transform_if(
		       __first1, 
		       __last1,
		       __result,
		       std::bind2nd(std::plus<typename std::iterator_traits<_InputIterator1>::value_type>(),scalar),
		       __pred);

     
  }

   /**
   ** \brief Substracts a scalar to a range. 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/08/23
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param __first1 An InputIterator.
   ** \param __last1   An InputIterator.
   ** \param scalar A scalar value.
   ** \param __result An OutputIterator.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1)
   ** \par Example:
   ** \code
   ** //substracts 5 to M
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> M3(4,5);
   ** slip::minus_scalar(M.begin(),M.end(),5,M3.begin());
   ** \endcode
   */
  template<typename InputIterator1, 
	   typename OutputIterator>
  inline
  void minus_scalar(InputIterator1 __first1,
		   InputIterator1 __last1,
		   const typename std::iterator_traits<InputIterator1>::value_type& scalar,
		   OutputIterator __result)
  {
    std::transform(__first1,__last1,
		   __result,
		   std::bind2nd(std::minus<typename std::iterator_traits<InputIterator1>::value_type>(),scalar));
  }


   /**
   **  \brief Substracts a scalar to a range according to a mask sequence.
   ** \author Tremblais Benoit <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/08/23
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __mask_first An input iterator.
   **  \param scalar A scalar value.
   **  \param  __result     An output iterator.
   **  \param  value      true value of the mask range. Default is 1.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__mask_first,__mask_first + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) 
   **  \par Example1:
   **  \code
   **  // substracts 5 to v1
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<bool> mask(v1.size());
   **  mask[2] = 1;
   **  mask[4] = 1;
   **  mask[9] = 1;
   **  slip::Array<int> v3(v1.size());
   **  slip::minus_scalar_mask(v1.begin(),v1.end(),
   **                        mask.begin(),
   **                        5,  
   **                       v3.begin());
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** slip::Array<int> v1(10);
   ** slip::iota(v1.begin(),v1.end(),1,1);
   ** std::cout<<"v1 = "<<v1<<std::endl;
   ** slip::Array<int> maskint(v1.size());
   ** maskint[2] = 2;
   ** maskint[4] = 2;
   ** maskint[9] = 2;
   ** slip::Array<int> v3(v1.size());
   ** slip::minus_scalar_mask(v1.begin(),v1.end(),
   **                        maskint.begin(),
   **                        5,
   **                        v3.begin(),
   **                        int(2));
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   */
  template<typename _InputIterator1, 
	   typename _MaskIterator,
           typename _OutputIterator>
  inline
  void minus_scalar_mask(_InputIterator1 __first1, _InputIterator1 __last1,
			_MaskIterator __mask_first,
			const typename std::iterator_traits<_InputIterator1>::value_type& scalar,
			_OutputIterator __result,
		 typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
  {
    slip::transform_mask_un(
			     __first1, 
			     __last1,
			     __mask_first,
			     __result,
			     std::bind2nd(std::minus<typename std::iterator_traits<_InputIterator1>::value_type>(),scalar),
			     value);

     
  }

  /** 
   **  \brief Substracts a scalar to a range according to a Predicate.
   ** \author Tremblais Benoit <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/08/23
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param scalar A scalar value.
   **  \param  __result     An output iterator.
   **  \param  __pred       A predicate.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1)
   **  \par Example:
   **  \code
   **  // substracts 5 to v1 according to the Predicate lt5Predicate.
   ** template <typename T>
   ** bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2 = v1;
   **  slip::Array<int> v3(v1.size());
   **  slip::minus_scalar_if(v1.begin(),v1.end(),
   **              5,
   **              v3.begin(),
   **              lt5Predicate<int>);
   **  std::cout<<"v3 = "<<v3<<std::endl; 
   ** \endcode
   */
  template<typename _InputIterator1, 
    	   typename _OutputIterator,
           typename _Predicate>
  inline
  void minus_scalar_if(_InputIterator1 __first1, _InputIterator1 __last1,
		      const typename std::iterator_traits<_InputIterator1>::value_type& scalar,
               _OutputIterator __result,
	       _Predicate __pred)
  {
    slip::transform_if(
		       __first1, 
		       __last1,
		       __result,
		       std::bind2nd(std::minus<typename std::iterator_traits<_InputIterator1>::value_type>(),scalar),
		       __pred);

     
  }


   /**
   ** \brief Multiplies a range by a scalar.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/08/23
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param __first1 An InputIterator.
   ** \param __last1   An InputIterator.
   ** \param scalar A scalar value.
   ** \param __result An OutputIterator.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1)
   ** \par Example:
   ** \code
   ** //multiplies  M by 5
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> M3(4,5);
   ** slip::multiplies_scalar(M.begin(),M.end(),5,M3.begin());
   ** \endcode
   */
  template<typename InputIterator1, 
	   typename OutputIterator>
  inline
  void multiplies_scalar(InputIterator1 __first1,
		   InputIterator1 __last1,
		   const typename std::iterator_traits<InputIterator1>::value_type& scalar,
		   OutputIterator __result)
  {
    std::transform(__first1,__last1,
		   __result,
		   std::bind2nd(std::multiplies<typename std::iterator_traits<InputIterator1>::value_type>(),scalar));
  }


  /**
   **  \brief Multiplies a range by a scalar according to a mask sequence.
   ** \author Tremblais Benoit <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/08/23
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __mask_first An input iterator.
   **  \param scalar A scalar value.
   **  \param  __result     An output iterator.
   **  \param  value      true value of the mask range. Default is 1.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__mask_first,__mask_first + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) 
   **  \par Example1:
   **  \code
   **  // multiplies v1 by 5
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<bool> mask(v1.size());
   **  mask[2] = 1;
   **  mask[4] = 1;
   **  mask[9] = 1;
   **  slip::Array<int> v3(v1.size());
   **  slip::multiplies_scalar_mask(v1.begin(),v1.end(),
   **                        mask.begin(),
   **                        5,  
   **                       v3.begin());
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** slip::Array<int> v1(10);
   ** slip::iota(v1.begin(),v1.end(),1,1);
   ** std::cout<<"v1 = "<<v1<<std::endl;
   ** slip::Array<int> maskint(v1.size());
   ** maskint[2] = 2;
   ** maskint[4] = 2;
   ** maskint[9] = 2;
   ** slip::Array<int> v3(v1.size());
   ** slip::multiplies_scalar_mask(v1.begin(),v1.end(),
   **                        maskint.begin(),
   **                        5,
   **                        v3.begin(),
   **                        int(2));
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   */
  template<typename _InputIterator1, 
	   typename _MaskIterator,
           typename _OutputIterator>
  inline
  void multiplies_scalar_mask(_InputIterator1 __first1, _InputIterator1 __last1,
			_MaskIterator __mask_first,
			const typename std::iterator_traits<_InputIterator1>::value_type& scalar,
			_OutputIterator __result,
		 typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
  {
    slip::transform_mask_un(
			     __first1, 
			     __last1,
			     __mask_first,
			     __result,
			     std::bind2nd(std::multiplies<typename std::iterator_traits<_InputIterator1>::value_type>(),scalar),
			     value);

     
  }


  /** 
   **  \brief Multiplies a range by a scalar according to a Predicate.
   ** \author Tremblais Benoit <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/08/23
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param scalar A scalar value.
   **  \param  __result     An output iterator.
   **  \param  __pred       A predicate.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1)
   **  \par Example:
   **  \code
   **  // multiplies v1 by 5 according to the Predicate lt5Predicate.
   ** template <typename T>
   ** bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2 = v1;
   **  slip::Array<int> v3(v1.size());
   **  slip::multiplies_scalar_if(v1.begin(),v1.end(),
   **              5,
   **              v3.begin(),
   **              lt5Predicate<int>);
   **  std::cout<<"v3 = "<<v3<<std::endl; 
   ** \endcode
   */
   template<typename _InputIterator1, 
    	   typename _OutputIterator,
           typename _Predicate>
  inline
  void multiplies_scalar_if(_InputIterator1 __first1, _InputIterator1 __last1,
		      const typename std::iterator_traits<_InputIterator1>::value_type&  scalar,
               _OutputIterator __result,
	       _Predicate __pred)
  {
    slip::transform_if(
		       __first1, 
		       __last1,
		       __result,
		       std::bind2nd(std::multiplies<typename std::iterator_traits<_InputIterator1>::value_type>(),scalar),
		       __pred);

     
  }


  /**
   ** \brief Divides a range by a scalar.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/08/23
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param __first1 An InputIterator.
   ** \param __last1   An InputIterator.
   ** \param scalar A scalar value.
   ** \param __result An OutputIterator.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1)
    ** \par Example:
   ** \code
   ** //divides  M by 5
   ** slip::Array2d<float> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<float> M3(4,5);
   ** slip::divides_scalar(M.begin(),M.end(),5.0f,M3.begin());
   ** \endcode
   */
  template<typename InputIterator1, 
	   typename OutputIterator>
  inline
  void divides_scalar(InputIterator1 __first1,
		   InputIterator1 __last1,
		   const typename std::iterator_traits<InputIterator1>::value_type& scalar,
		   OutputIterator __result)
  {
    std::transform(__first1,__last1,
		   __result,
		   std::bind2nd(std::divides<typename std::iterator_traits<InputIterator1>::value_type>(),scalar));
  }

/**
   **  \brief Divides a range by a scalar according to a mask sequence.
   ** \author Tremblais Benoit <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/08/23
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __mask_first An input iterator.
   **  \param scalar A scalar value.
   **  \param  __result     An output iterator.
   **  \param  value      true value of the mask range. Default is 1.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__mask_first,__mask_first + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1) 
   **  \par Example1:
   **  \code
   **  //divides  v1 by 2
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<bool> mask(v1.size());
   **  mask[2] = 1;
   **  mask[4] = 1;
   **  mask[9] = 1;
   **  slip::Array<int> v3(v1.size());
   **  slip::divides_scalar_mask(v1.begin(),v1.end(),
   **                        mask.begin(),
   **                        2,  
   **                       v3.begin());
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** slip::Array<int> v1(10);
   ** slip::iota(v1.begin(),v1.end(),1,1);
   ** std::cout<<"v1 = "<<v1<<std::endl;
   ** slip::Array<int> maskint(v1.size());
   ** maskint[2] = 2;
   ** maskint[4] = 2;
   ** maskint[9] = 2;
   ** slip::Array<int> v3(v1.size());
   ** slip::divides_scalar_mask(v1.begin(),v1.end(),
   **                        maskint.begin(),
   **                        2,
   **                        v3.begin(),
   **                        int(2));
   ** std::cout<<"v3 = "<<v3<<std::endl;
   ** \endcode
   */
  template<typename _InputIterator1, 
	   typename _MaskIterator,
           typename _OutputIterator>
  inline
  void divides_scalar_mask(_InputIterator1 __first1, _InputIterator1 __last1,
			_MaskIterator __mask_first,
			const typename std::iterator_traits<_InputIterator1>::value_type& scalar,
			_OutputIterator __result,
		 typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
  {
    slip::transform_mask_un(
			     __first1, 
			     __last1,
			     __mask_first,
			     __result,
			     std::bind2nd(std::divides<typename std::iterator_traits<_InputIterator1>::value_type>(),scalar),
			     value);

     
  }


  /** 
   **  \brief Divides a range by a scalar according to a Predicate.
   ** \author Tremblais Benoit <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/08/23
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param scalar A scalar value.
   **  \param  __result     An output iterator.
   **  \param  __pred       A predicate.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range ]__first1,__last1)
   **  \par Example:
   **  \code
   **  // divides v1 by 5 according to the Predicate lt5Predicate.
   ** template <typename T>
   ** bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2 = v1;
   **  slip::Array<int> v3(v1.size());
   **  slip::divides_scalar_if(v1.begin(),v1.end(),
   **              5,
   **              v3.begin(),
   **              lt5Predicate<int>);
   **  std::cout<<"v3 = "<<v3<<std::endl; 
   ** \endcode
   */
  template<typename _InputIterator1, 
    	   typename _OutputIterator,
           typename _Predicate>
  inline
  void divides_scalar_if(_InputIterator1 __first1, _InputIterator1 __last1,
		      const typename std::iterator_traits<_InputIterator1>::value_type& scalar,
               _OutputIterator __result,
	       _Predicate __pred)
  {
    slip::transform_if(
		       __first1, 
		       __last1,
		       __result,
		       std::bind2nd(std::divides<typename std::iterator_traits<_InputIterator1>::value_type>(),scalar),
		       __pred);

     
  }

  /* @} */

  
  /** \name Operations on a range*/
  /* @{ */

  /**
   ** \brief Computes the negation of a range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param __first1 An InputIterator.
   ** \param __last1   An InputIterator.
   ** \param __result An OutputIterator.
   ** \pre  The input and result ranges must have the same value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \par Example:
   ** \code
   ** //negation of a Array2
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> M2(4,5);
   ** slip::plus(M.begin(),M.end(),M2.begin());
   ** \endcode
   **
   */
  template<typename InputIterator, typename OutputIterator>
  inline
  void negate(InputIterator __first1,
	      InputIterator __last1,
	      OutputIterator __result)
  
  {
    std::transform(__first1,__last1,
		   __result,
		   std::negate<typename std::iterator_traits<InputIterator>::value_type>());
  }

  /**
   ** \brief Computes the negation of a range according to a mask range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/12
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param __first1 An InputIterator.
   ** \param __last1   An InputIterator.
   ** \param __mask_first An InputIterator.
   ** \param __result An OutputIterator.
   **  \param  value      true value of the mask range. Default is 1.
   ** \pre  The input and result ranges must have the same value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__mask_first,__mask_first + (__last1 -  __first1)) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \par Example1:
   ** \code
   **  slip::Array<int> V1(10);
   ** slip::iota(V1.begin(),V1.end(),10,2);
   ** slip::Array<int> V3(10);
   **  std::cout<<"V1 = "<<V1<<std::endl;
   ** slip::Array<bool> mask(V1.size());
   ** maskint[2] = 1;
   ** maskint[4] = 1;
   ** maskint[9] = 1;
   ** slip::negate_mask(V1.begin(),V1.end(),
   **                   mask.begin(),
   **                   V3.begin());
   ** std::cout<<"V3 = "<<V3<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   **  slip::Array<int> V1(10);
   ** slip::iota(V1.begin(),V1.end(),10,2);
   ** slip::Array<int> V3(10);
   **  std::cout<<"V1 = "<<V1<<std::endl;
   ** slip::Array<int> maskint(V1.size());
   ** maskint[2] = 2;
   ** maskint[4] = 2;
   ** maskint[9] = 2;
   ** slip::negate_mask(V1.begin(),V1.end(),
   **                   maskint.begin(),
   **                   V3.begin(),
   **                   int(2));
   ** std::cout<<"V3 = "<<V3<<std::endl;
   ** \endcode
   **
   */
  template<typename InputIterator, 
           typename MaskIterator, 
           typename OutputIterator>
  inline
  void negate_mask(InputIterator __first1,
		   InputIterator __last1,
		   MaskIterator __mask_first,
		   OutputIterator __result,
		   typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  
  {
    slip::transform_mask_un(__first1,__last1,
			    __mask_first,
			    __result,
			    std::negate<typename std::iterator_traits<InputIterator>::value_type>(),value);
  }
  /**
   **  \brief Computes the pointwise negate of a ranges according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/04
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  __first1     An input iterator.
   **  \param  __last1      An input iterator.
   **  \param  __result     An output iterator.
   **  \param  __pred       A predicate.
   ** \pre  The input and result range must have the same value type.
   ** \pre [__first1,__last1) is a valid range.
   ** \pre [__result,__result + (__last1 -  __first1)) is a valid range.
   ** \pre __result is not an iterator within the range [__first1,__last1) or
   **  \par Example:
   **  \code
   ** template <typename T>
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2 = v1;
   **  slip::Array<int> v3(v1.size());
   **  slip::negate_if(v1.begin(),v1.end(),
   **                  v3.begin(),
   **                  lt5Predicate<int>);
   **  std::cout<<"v2 = "<<v2<<std::endl; 
   **  std::cout<<"v3 = "<<v3<<std::endl; 
   ** \endcode
   */
  template<typename _InputIterator1, 
	   typename _OutputIterator,
           typename _Predicate>
  inline
  void negate_if(_InputIterator1 __first1, _InputIterator1 __last1,
              
		 _OutputIterator __result,
		 _Predicate __pred)
  {
    slip::transform_if(
		       __first1, 
		       __last1,
		       __result,
		       std::negate<typename std::iterator_traits<_InputIterator1>::value_type>(),
		       __pred);

     
  }
 
  /**
   ** \brief Iota assigns sequential increasing values based on a
   **  predefined step to a range. 
   **  That is, it assigns value to *first, value + step to *(first + 1) 
   **  and so on. In general, each iterator i in the range [first,last) 
   **  is assigned value + step * (i - first).
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/04/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last   An InputIterator.
   ** \param value Init value.
   ** \param step Step between two values. Default step is 1.
   **
   ** \par Example:
   ** \code
   ** //fill a matrix with 0.0 0.05 0.10...
   ** slip::Matrix<double> Miota(4,5,0.0);
   ** slip::iota(Miota.begin(),Miota.end(),0.0,0.05);
   ** std::cout<<Miota<<std::endl;
   ** \endcode
   ** \pre [first,last) is a valid range.
   ** \remarks Inspired from the iota prototype of the Standard
   ** Template Library.
   **
   */
  template <class ForwardIterator, class T>
  inline
  void iota(ForwardIterator first, ForwardIterator last, T value, T step = T(1))
  {
    for (;first != last; ++first)
      {
	*first = value;
	value = value + step;
      }
    
  }

  /** 
   ** \brief Iota_mask sequential increasing values based on a
   **  predefined step to a range according to a mask sequence. 
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/04/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last   An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param value Init value.
   ** \param step Step between two values. Default step is 1.
   ** \param value_mask "True" value of the mask. Default is 1.
   ** 
   ** \par Example:
   ** \code
   ** slip::Array<double> Miota(9); 
   ** slip::Array<bool> mask(9);
   ** mask[2] = 1;
   ** mask[4] = 1;
   ** mask[8] = 1;
   ** std::cout<<Miota<<std::endl; 
   ** slip::iota_mask(Miota.begin(),Miota.end(),
   **             mask.begin(),
   **             1.0,0.05,
   **             int(1));
   ** std::cout<<Miota<<std::endl;
   ** \endcode
   ** \pre [first,last) is a valid range.
   **
   */

  template <class ForwardIterator, typename MaskIterator, class T>
  inline
  void iota_mask(ForwardIterator first, 
		 ForwardIterator last,
		 MaskIterator mask_first,  
		 T value, T step = T(1),
		 typename std::iterator_traits<MaskIterator>::value_type value_mask=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    for (;first != last; ++first,++mask_first)
      {
	if(*mask_first == value_mask)
	  {             
	    *first = value;
	    value = value + step;
	  }
        
      }
  }

 /*!
   ** \brief Generates n points lineary spaced. The spacing step between the points is (max-min)/(n-1) if endpoint is true or (max-min)/n if endpoint is false.
   ** \date 2024/03/27 add endpoint parameter.
   ** \param min minimal value of the range.
   ** \param max maximal value of the range.
   ** \param n number of value of the range.
   ** \param first RandomAccessIterator to the range of the vector.
   ** \param last RandomAccessIterator to the range of the vector.
   ** \param endpoint If true (default) the end point is included: step between point is (max-min)/(n-1) else end point is not included: step is equal to (max-min)/n.
   ** \pre (last-first) >= n
   ** \pre n > 1
   ** \pre value_type of 
   ** \par Example:
   ** \code
   ** typedef double T;
   ** slip::Array<T> A(7);
   ** slip::linspace(-5.0,5.0,A.size(),A.begin(),A.end());
   ** std::cout<<"slip::linspace(-5.0,5.0,7) = \n"<<A<<std::endl;
   ** slip::linspace(-5.0,5.0,7,A.begin(),A.end(),false);
   ** std::cout<<"slip::linspace(-5.0,5.0,"<<A.size()<<",false) = \n"<<A<<std::endl;
   ** \endcode
  */ 
  template<typename T,
	   typename RandomAccessIterator>
  void linspace(const T& min,
		const T& max,
		const std::size_t& n,
		RandomAccessIterator first,
		RandomAccessIterator last,
		const bool endpoint=true)
  {
    assert(static_cast<std::size_t>(last-first) >= n);
    assert(n > 1);
    T step = T();
    if(endpoint)
      {
	step = (max-min)/static_cast<T>(n-1);
      }
    else
      {
	step = (max-min)/static_cast<T>(n);
      }
    slip::iota(first,last,min,step);
  }
  
  /*!
   ** \brief Generates n points spaced evenly on a logarithmic scale.
   ** \date 2024/03/27
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param min minimal value of the range.
   ** \param max maximal value of the range.
   ** \param n number of value of the range.
   ** \param first RandomAccessIterator to the range of the vector.
   ** \param last RandomAccessIterator to the range of the vector.
   ** \param endpoint If true (default) endpoint is included else not.
   ** \param base base of the logarithm (10.0 by default).
   ** \pre min < max
   ** \pre (last-first) >= n
   ** \pre n > 1
   ** \pre value_type of 
   ** \par Example:
   ** \code
   ** typedef double T;
   ** slip::Array<T> A(7);
   ** slip::logspace(-5.0,5.0,7,A.begin(),A.end());
   ** std::cout<<"slip::logspace(-5.0,5.0,"<<A.size()<<") = \n"<<A<<std::endl;
   ** slip::logspace(-5.0,5.0,7,A.begin(),A.end(),false);
   ** std::cout<<"slip::logspace(-5.0,5.0,"<<A.size()<<",false) = \n"<<A<<std::endl;
   ** slip::logspace(-5.0,5.0,7,A.begin(),A.end(),true,slip::constants<double>::euler());
   ** std::cout<<"slip::logspace(-5.0,5.0,"<<A.size()<<",true,e) = \n"<<A<<std::endl;
   ** slip::logspace(-5.0,5.0,7,A.begin(),A.end(),false,slip::constants<double>::euler());
   ** std::cout<<"slip::logspace(-5.0,5.0,"<<A.size()<<",false,e) = \n"<<A<<std::endl;
   ** slip::logspace(1.0,7.0,7,A.begin(),A.end(),true,2.0);
   ** std::cout<<"slip::logspace(1.0,7.0,"<<A.size()<<",true,2) = \n"<<A<<std::endl;
   ** \endcode
  */ 
   template<typename T,
	   typename RandomAccessIterator>
  void logspace(const T& min,
		const T& max,
		const std::size_t& n,
		RandomAccessIterator first,
		RandomAccessIterator last,
		const bool endpoint=true,
		const T& base = static_cast<T>(10.0))
  {
    assert(min < max);
    assert(static_cast<std::size_t>(last-first) >= n);
    assert(n > 1);
    slip::linspace(min,max,n,first,last,endpoint);
    for (;first!=last;++first)
      {
	*first = std::pow(base,*first);
      }
  }
  
/*!
   ** \brief Generates n points evenly spaced acording to a geometric progression.
   ** \date 2024/03/27 add endpoint parameter.
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param min minimal value of the range.
   ** \param max maximal value of the range.
   ** \param n number of value of the range.
   ** \param first RandomAccessIterator to the range of the vector.
   ** \param last RandomAccessIterator to the range of the vector.
   ** \param endpoint If true (default) the end point is included else end point is not included.
   ** \pre min < max
   ** \pre (last-first) >= n
   ** \pre n > 1
   ** \pre value_type of 
   ** \par Example:
   ** \code
   ** typedef double T;
   ** slip::Array<T> A(7);
   ** slip::geomspace(1.0,1000.0,7,A.begin(),A.end());
   ** std::cout<<"slip::geomspace(1.0,1000.0,"<<A.size()<<") = \n"<<A<<std::endl;
   ** slip::geomspace(2.0,1000.0,7,A.begin(),A.end());
   ** std::cout<<"slip::geomspace(2.0,1000.0,"<<A.size()<<") = \n"<<A<<std::endl;
   ** slip::geomspace(1.0,1000.0,7,A.begin(),A.end(),false);
   ** std::cout<<"slip::geomspace(1.0,1000.0,"<<A.size()<<",false) = \n"<<A<<std::endl;
   ** slip::geomspace(2.0,1000.0,7,A.begin(),A.end(),false);
   ** std::cout<<"slip::geomspace(2.0,1000.0,"<<A.size()<<",false) = \n"<<A<<std::endl;
   ** \endcode
  */ 
    template<typename T,
	     typename RandomAccessIterator>
  void geomspace(const T& min,
		 const T& max,
		 const std::size_t& n,
		 RandomAccessIterator first,
		 RandomAccessIterator last,
		const bool endpoint=true)
  {
    assert(min < max);
    assert(static_cast<std::size_t>(last-first) >= n);
    assert(n > 1);
    T base = T();
    if(endpoint)
      {
	base = std::exp((std::log(max)-std::log(min))/static_cast<T>(n-1));
      }
    else
      {
	base = std::exp((std::log(max)-std::log(min))/static_cast<T>(n));
      }
    std::cout<<"base = "<<base<<std::endl;
    *first = min;
    ++first;
    for (;first!=last;++first)
      {
	*first = *(first-1)*base;
      }

  }
  /* @} */

}//slip::


#endif //SLIP_ARITHMETIC_OP_HPP
