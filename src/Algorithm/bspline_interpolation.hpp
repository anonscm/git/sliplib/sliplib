/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file bspline_interpolation.hpp
 * 
 * \brief Provides some bspline interpolation algorithms.
 * \since 1.2.0
 */
#ifndef SLIP_BSPLINE_INTERPOLATION_HPP
#define SLIP_BSPLINE_INTERPOLATION_HPP

#include <cmath>
#include <limits>
#include "DPoint2d.hpp"
#include "DPoint3d.hpp"
#include "Array.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "macros.hpp"
#include "arithmetic_op.hpp"
#include "dynamic.hpp"
#include "utils_compilation.hpp"
namespace slipalgo
{
 /** \name BSpline interpolation */
  /* @{ */
 /*!
  ** \brief BSpline Poles computations
  ** based on http://bigwww.epfl.ch/thevenaz/interpolation/
  ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
  ** \date 2007/03/19
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param degree spline degree (between 2 and 9)
  ** \param Poles array of computed poles
  ** \pre Poles must have resize function 
  ** \par Example:
  ** \code
  ** slip::Array<double> Poles;
  ** slipalgo::BSplinePoles(3,Poles);
  ** std::cout<<"Poles = \n"<<Poles<<std::endl;
  ** \endcode
  */
template <class Array>
inline
void BSplinePoles(const std::size_t degree,
		  Array &Poles)
{
		
	/* recover the poles from a lookup table */
  switch (degree) 
    {
    case 2L:
      Poles.resize(1);
      Poles[0] = std::sqrt(8.0) - 3.0;
      break;
    case 3L:
      Poles.resize(1);
      Poles[0] = std::sqrt(3.0) - 2.0;
      break;
    case 4L:
      Poles.resize(2);
      Poles[0] = std::sqrt(664.0 - std::sqrt(438976.0)) + std::sqrt(304.0) - 19.0;
      Poles[1] = std::sqrt(664.0 + std::sqrt(438976.0)) - std::sqrt(304.0) - 19.0;
      break;
    case 5L:
      Poles.resize(2);
      Poles[0] = std::sqrt(135.0 / 2.0 - std::sqrt(17745.0 / 4.0)) + std::sqrt(105.0 / 4.0)
	- 13.0 / 2.0;
      Poles[1] = std::sqrt(135.0 / 2.0 + std::sqrt(17745.0 / 4.0)) - std::sqrt(105.0 / 4.0)
	- 13.0 / 2.0;
      break;
    case 6L:
      Poles.resize(3);
      Poles[0] = -0.48829458930304475513011803888378906211227916123938;
      Poles[1] = -0.081679271076237512597937765737059080653379610398148;
      Poles[2] = -0.0014141518083258177510872439765585925278641690553467;
      break;
    case 7L:
      Poles.resize(3);
      Poles[0] = -0.53528043079643816554240378168164607183392315234269;
      Poles[1] = -0.12255461519232669051527226435935734360548654942730;
      Poles[2] = -0.0091486948096082769285930216516478534156925639545994;
      break;
    case 8L:
      Poles.resize(4);
      Poles[0] = -0.57468690924876543053013930412874542429066157804125;
      Poles[1] = -0.16303526929728093524055189686073705223476814550830;
      Poles[2] = -0.023632294694844850023403919296361320612665920854629;
      Poles[3] = -0.00015382131064169091173935253018402160762964054070043;
      break;
    case 9L:
      Poles.resize(4);
      Poles[0] = -0.60799738916862577900772082395428976943963471853991;
      Poles[1] = -0.20175052019315323879606468505597043468089886575747;
      Poles[2] = -0.043222608540481752133321142979429688265852380231497;
      Poles[3] = -0.0021213069031808184203048965578486234220548560988624;
      break;
    default:
      std::cerr<<"Invalid spline degree"<<std::endl;;
      exit(1);
    }
}


/*!
  ** \brief BSpline computation of causal coefficients
  ** based on http://bigwww.epfl.ch/thevenaz/interpolation/
  ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
  ** \date 2007/03/19
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param infirst begin iterator on datas
  ** \param inlast past-to-end iterator on datas
  ** \param z actual pole
  ** \param Tolerance tolerance used
  ** \return causal coefficient
  */
template<typename InputIterator>
inline
double	BSplineInitialCausalCoefficient(InputIterator infirst, 
					InputIterator inlast, 
					double	z,
					double	Tolerance)

{ 
	std::size_t DataLength = inlast-infirst;
	
	/* this initialization corresponds to mirror boundaries */
	std::size_t Horizon = DataLength;
	if (Tolerance > 0.0) 
	  {
	    Horizon = (long)std::ceil(std::log(Tolerance) / std::log(std::fabs(z)));
	  }

	double	Sum, zn, z2n, iz;
	if (Horizon < DataLength) 
	  {
	    /* accelerated loop */
	    zn = z;
	    Sum = *infirst;
	    InputIterator it = infirst+1;
	    for (std::size_t n = 1L; n < Horizon; n++,it++) 
	      {
		Sum += zn * (*it);
		zn *= z;
	      }
	    return(Sum);
	  }
	else 
	  {
	    /* full loop */
	    zn = z;
	    iz = 1.0 / z;
	    z2n = std::pow(z, (double)(DataLength - 1L));
	    Sum = (*infirst) + z2n * (*(inlast-1));
	    z2n *= z2n * iz;
	    for (InputIterator it=infirst+1; it!=inlast-1; it++)
	      {
		Sum += (zn + z2n) * (*it);
		zn *= z;
		z2n *= iz;
	      }
	    return(Sum / (1.0 - zn * zn));
	  }
} 

 /*!
  ** \brief BSpline computation of anticausal coefficients
  ** based on http://bigwww.epfl.ch/thevenaz/interpolation/
  ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
  ** \date 2007/03/19
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param outlast past-to-end iterator on datas
  ** \param z actual pole
  ** \return causal coefficient
  */
template<typename InputIterator>
inline
double	
BSplineInitialAntiCausalCoefficient(InputIterator outlast, 
				    const double  z)

{ 	/* this initialization corresponds to mirror boundaries */
  return ((z / (z * z - 1.0)) * (z * (*(outlast-1)) + (*outlast)));
}
/*!
  ** \brief BSpline computation of coefficients
  ** based on http://bigwww.epfl.ch/thevenaz/interpolation/
  ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
  ** \date 2007/03/19
  ** \since 1.2.0
  ** \version 0.0.1
  ** \param infirst begin iterator on datas
  ** \param inlast past-to-end iterator on datas
  ** \param outfirst begin iterator on results
  ** \param Poles_first begin iterator on Poles.
  ** \param Poles_last past-to-end iterator on Poles.
  ** \param Tolerance tolerance used : admissible relative error
  ** \par Example:
  ** \code
  ** slip::Array<double> A(10);
  ** slip::iota(A.begin(),A.end(),1.0,1.0);
  ** std::cout<<"A = \n"<<A<<std::endl;
  ** slip::Array<double> Poles;
  ** slipalgo::BSplinePoles(3,Poles);
  ** std::cout<<"Poles = \n"<<Poles<<std::endl;
  ** slip::Array<double> Coef(A.size());
  ** slipalgo::BSplineComputeInterpolationCoefficients(A.begin(),A.end(),
  **						       Coef.begin(),
  **                                                   Poles.begin(),Poles.end(),
  **						       std::numeric_limits<double>::epsilon());
  ** std::cout<<"Coef = \n"<<Coef<<std::endl;
  ** \endcode
  */
template<typename InputIterator, 
	 typename OutputIterator, 
	 typename InputIterator2>
inline
void	BSplineComputeInterpolationCoefficients(InputIterator infirst, 
						InputIterator inlast, 
						OutputIterator outfirst, 
						InputIterator2 Poles_first,
						InputIterator2 Poles_last,
						double	Tolerance = std::numeric_limits<double>::epsilon())
{ 
        double	Lambda = 1.0;
	//std::size_t k;

	std::size_t NbPoles = static_cast<std::size_t>(Poles_last - Poles_first);

	std::size_t length = (inlast-infirst);

	/* special case required by mirror boundaries */
	if (length == 1L) 
	  {
		return;
	  }
	/* compute the overall gain */
	InputIterator2 it_poles = Poles_first;
	for (std::size_t k = 0L; k < NbPoles; ++k, ++it_poles) 
	  {
	    Lambda = Lambda * (1.0 - *it_poles) * (1.0 - 1.0 / *it_poles);
	  }
	InputIterator it;
	OutputIterator it2;
	/* apply the gain */
	for (it=infirst,it2=outfirst;it!=inlast;it++,it2++)
	  {
	    *it2 = (*it)*Lambda;
	  }
	
	// loop over all poles
	it_poles = Poles_first;
	for (std::size_t k = 0L; k < NbPoles; ++k, ++it_poles) 
	  {
	    // causal initialization 
	    *outfirst = Lambda*slipalgo::BSplineInitialCausalCoefficient(infirst,inlast, *it_poles, Tolerance);
	    // causal recursion 
	    for (it=infirst+1,it2=outfirst+1;it!=inlast;it++,it2++)
	      {
		*it2 += *it_poles * (*(it2-1));
	      }
	    it2--;
	    // anticausal initialization 
	    *it2 = slipalgo::BSplineInitialAntiCausalCoefficient(it2, *it_poles);
	    // anticausal recursion 
	    for (it2--;it2!=outfirst-1;it2--)
	      {
		*it2 = *it_poles * ((*(it2+1)) - *it2);
	      }

	  }
} 

/*!
  ** \brief BSpline computation of coefficients
  ** based on http://bigwww.epfl.ch/thevenaz/interpolation/
  ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
  ** \date 2007/03/19
  ** \since 1.2.0
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param infirst begin iterator on datas
  ** \param inlast past-to-end iterator on datas
  ** \param outfirst begin iterator on results
  ** \param Poles Poles array
  ** \param Tolerance tolerance used : admissible relative error
  ** \deprecated you should use slipalgo::BSplineComputeInterpolationCoefficients(InputIterator infirst, 
						InputIterator inlast, 
						OutputIterator outfirst, 
						InputIterator2 Poles_first,
						InputIterator2 Poles_last,
						double	Tolerance = std::numeric_limits<double>::epsilon()) instead.

  */
template<typename InputIterator, 
	 typename OutputIterator, 
	 class Array>
inline
void	BSplineComputeInterpolationCoefficients(InputIterator infirst, 
						InputIterator inlast, 
						OutputIterator outfirst, 
						const Array& Poles,
						double	Tolerance)
{ 
  slipalgo::BSplineComputeInterpolationCoefficients(infirst,inlast,
						    outfirst,
						    Poles.begin(),Poles.end(),
						    Tolerance);
} 

/**
 ** \brief Computes the bspline interpolations indices.
 ** based on http://bigwww.epfl.ch/thevenaz/interpolation/
 ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
 ** \date 2007/03/19
 ** \since 1.2.0
 ** \version 0.0.1
 ** \param spline_degree bspline degree.
 ** \param x coordinate to interpolate.
 ** \param index_first RandomAccessIterator to the index range.
 ** \param index_last RandomAccessIterator to the index range.
 */
  template<typename RandomAccessIterator>
  inline
  void BSplineComputeInterpolationIndexes(const std::size_t spline_degree,
					  double x,
					  RandomAccessIterator index_first,
					  RandomAccessIterator index_last)
  {
    assert(static_cast<std::size_t>(index_last - index_first) == (spline_degree + 1));
    long splinedegree=static_cast<long>(spline_degree);
	
    /* compute the interpolation indexes */
    long i = 0;
    if (splinedegree & 1L) 
      {
	i = static_cast<long>(std::floor(x)) - splinedegree / 2L;
      }
    else 
      {
	i = static_cast<long>(std::floor(x + 0.5)) - spline_degree / 2L;
      }
    for (; index_first != index_last; ++index_first) 
	  {
	    *index_first = i++;
	  }
  }

/**
 ** \brief Computes the bspline interpolations weights.
 ** based on http://bigwww.epfl.ch/thevenaz/interpolation/
 ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
 ** \date 2007/03/19
 ** \since 1.2.0
 ** \version 0.0.1
 ** \param spline_degree bspline degree.
 ** \param x coordinate to interpolate.
 ** \param index_first RandomAccessIterator to the index range.
 ** \param index_last RandomAccessIterator to the index range.
 ** \param Weights Array containing the bspline interpolation weights.
 */
  template<typename RandomAccessIterator,
	   typename Array>
  inline
  void BSplineComputeInterpolationWeights(const std::size_t spline_degree,
					  double x,
					  RandomAccessIterator index_first,
					  RandomAccessIterator UNUSED(index_last),
					  Array& Weights)
  {
    double w = 0.0;
    double w2 = 0.0;
    double w4 = 0.0;
    double t = 0.0;
    double t0 = 0.0;
    double t1 = 0.0;
    long SplineDegree=static_cast<long>(spline_degree);

    switch (SplineDegree) 
      {
      case 2L:
	/* x */
	Weights.resize(3);
	w = x - static_cast<double>(index_first[1]);
	Weights[1] = 3.0 / 4.0 - w * w;
	Weights[2] = (1.0 / 2.0) * (w - Weights[1] + 1.0);
	Weights[0] = 1.0 - Weights[1] - Weights[2];
	break;
      case 3L:
	/* x */
	Weights.resize(4);
	w = x - static_cast<double>(index_first[1]);
	Weights[3] = (1.0 / 6.0) * w * w * w;
	Weights[0] = (1.0 / 6.0) + (1.0 / 2.0) * w * (w - 1.0) - Weights[3];
	Weights[2] = w + Weights[0] - 2.0 * Weights[3];
	Weights[1] = 1.0 - Weights[0] - Weights[2] - Weights[3];
	break;
      case 4L:
	/* x */
	Weights.resize(5);
	w = x - static_cast<double>(index_first[2]);
	w2 = w * w;
	t = (1.0 / 6.0) * w2;
	Weights[0] = 1.0 / 2.0 - w;
	Weights[0] *= Weights[0];
	Weights[0] *= (1.0 / 24.0) * Weights[0];
	t0 = w * (t - 11.0 / 24.0);
	t1 = 19.0 / 96.0 + w2 * (1.0 / 4.0 - t);
	Weights[1] = t1 + t0;
	Weights[3] = t1 - t0;
	Weights[4] = Weights[0] + t0 + (1.0 / 2.0) * w;
	Weights[2] = 1.0 - Weights[0] - Weights[1] - Weights[3] - Weights[4];
		
	break;
      case 5L:
	/* x */
	Weights.resize(6);
	w = x - static_cast<double>(index_first[2]);
	w2 = w * w;
	Weights[5] = (1.0 / 120.0) * w * w2 * w2;
	w2 -= w;
	w4 = w2 * w2;
	w -= 1.0 / 2.0;
	t = w2 * (w2 - 3.0);
	Weights[0] = (1.0 / 24.0) * (1.0 / 5.0 + w2 + w4) - Weights[5];
	t0 = (1.0 / 24.0) * (w2 * (w2 - 5.0) + 46.0 / 5.0);
	t1 = (-1.0 / 12.0) * w * (t + 4.0);
	Weights[2] = t0 + t1;
	Weights[3] = t0 - t1;
	t0 = (1.0 / 16.0) * (9.0 / 5.0 - t);
	t1 = (1.0 / 24.0) * w * (w4 - w2 - 5.0);
	Weights[1] = t0 + t1;
	Weights[4] = t0 - t1;
	break;
      case 6L:
	/* x */
	Weights.resize(7);
	w = x - static_cast<double>(index_first[3]);
	Weights[0] = 1.0 / 2.0 - w;
	Weights[0] *= Weights[0] * Weights[0];
	Weights[0] *= Weights[0] / 720.0;
	Weights[1] = (361.0 / 192.0 - w * (59.0 / 8.0 + w
					   * (-185.0 / 16.0 + w * (25.0 / 3.0 + w * (-5.0 / 2.0 + w)
								   * (1.0 / 2.0 + w))))) / 120.0;
	Weights[2] = (10543.0 / 960.0 + w * (-289.0 / 16.0 + w
					     * (79.0 / 16.0 + w * (43.0 / 6.0 + w * (-17.0 / 4.0 + w
										     * (-1.0 + w)))))) / 48.0;
	w2 = w * w;
	Weights[3] = (5887.0 / 320.0 - w2 * (231.0 / 16.0 - w2
					     * (21.0 / 4.0 - w2))) / 36.0;
	Weights[4] = (10543.0 / 960.0 + w * (289.0 / 16.0 + w
					     * (79.0 / 16.0 + w * (-43.0 / 6.0 + w * (-17.0 / 4.0 + w
										      * (1.0 + w)))))) / 48.0;
	Weights[6] = 1.0 / 2.0 + w;
	Weights[6] *= Weights[6] * Weights[6];
	Weights[6] *= Weights[6] / 720.0;
	Weights[5] = 1.0 - Weights[0] - Weights[1] - Weights[2] - Weights[3]
	  - Weights[4] - Weights[6];
	break;
      case 7L:
	/* x */
	Weights.resize(8);
	w = x - static_cast<double>(index_first[3]);
	Weights[0] = 1.0 - w;
	Weights[0] *= Weights[0];
	Weights[0] *= Weights[0] * Weights[0];
	Weights[0] *= (1.0 - w) / 5040.0;
	w2 = w * w;
	Weights[1] = (120.0 / 7.0 + w * (-56.0 + w * (72.0 + w
						      * (-40.0 + w2 * (12.0 + w * (-6.0 + w)))))) / 720.0;
	Weights[2] = (397.0 / 7.0 - w * (245.0 / 3.0 + w * (-15.0 + w
							    * (-95.0 / 3.0 + w * (15.0 + w * (5.0 + w
											      * (-5.0 + w))))))) / 240.0;
	Weights[3] = (2416.0 / 35.0 + w2 * (-48.0 + w2 * (16.0 + w2
							  * (-4.0 + w)))) / 144.0;
	Weights[4] = (1191.0 / 35.0 - w * (-49.0 + w * (-9.0 + w
							* (19.0 + w * (-3.0 + w) * (-3.0 + w2))))) / 144.0;
	Weights[5] = (40.0 / 7.0 + w * (56.0 / 3.0 + w * (24.0 + w
							  * (40.0 / 3.0 + w2 * (-4.0 + w * (-2.0 + w)))))) / 240.0;
	Weights[7] = w2;
	Weights[7] *= Weights[7] * Weights[7];
	Weights[7] *= w / 5040.0;
	Weights[6] = 1.0 - Weights[0] - Weights[1] - Weights[2] - Weights[3]
	  - Weights[4] - Weights[5] - Weights[7];
	break;
      case 8L:
	/* x */
	Weights.resize(9);
	w = x - static_cast<double>(index_first[4]);
	Weights[0] = 1.0 / 2.0 - w;
	Weights[0] *= Weights[0];
	Weights[0] *= Weights[0];
	Weights[0] *= Weights[0] / 40320.0;
	w2 = w * w;
	Weights[1] = (39.0 / 16.0 - w * (6.0 + w * (-9.0 / 2.0 + w2)))
	  * (21.0 / 16.0 + w * (-15.0 / 4.0 + w * (9.0 / 2.0 + w
						   * (-3.0 + w)))) / 5040.0;
	Weights[2] = (82903.0 / 1792.0 + w * (-4177.0 / 32.0 + w
					      * (2275.0 / 16.0 + w * (-487.0 / 8.0 + w * (-85.0 / 8.0 + w
											  * (41.0 / 2.0 + w * (-5.0 + w * (-2.0 + w)))))))) / 1440.0;
	Weights[3] = (310661.0 / 1792.0 - w * (14219.0 / 64.0 + w
					       * (-199.0 / 8.0 + w * (-1327.0 / 16.0 + w * (245.0 / 8.0 + w
											    * (53.0 / 4.0 + w * (-8.0 + w * (-1.0 + w)))))))) / 720.0;
	Weights[4] = (2337507.0 / 8960.0 + w2 * (-2601.0 / 16.0 + w2
						 * (387.0 / 8.0 + w2 * (-9.0 + w2)))) / 576.0;
	Weights[5] = (310661.0 / 1792.0 - w * (-14219.0 / 64.0 + w
					       * (-199.0 / 8.0 + w * (1327.0 / 16.0 + w * (245.0 / 8.0 + w
											   * (-53.0 / 4.0 + w * (-8.0 + w * (1.0 + w)))))))) / 720.0;
	Weights[7] = (39.0 / 16.0 - w * (-6.0 + w * (-9.0 / 2.0 + w2)))
	  * (21.0 / 16.0 + w * (15.0 / 4.0 + w * (9.0 / 2.0 + w
						  * (3.0 + w)))) / 5040.0;
	Weights[8] = 1.0 / 2.0 + w;
	Weights[8] *= Weights[8];
	Weights[8] *= Weights[8];
	Weights[8] *= Weights[8] / 40320.0;
	Weights[6] = 1.0 - Weights[0] - Weights[1] - Weights[2] - Weights[3]
	  - Weights[4] - Weights[5] - Weights[7] - Weights[8];
	break;
      case 9L:
	/* x */
	Weights.resize(10);
	w = x - static_cast<double>(index_first[4]);
	Weights[0] = 1.0 - w;
	Weights[0] *= Weights[0];
	Weights[0] *= Weights[0];
	Weights[0] *= Weights[0] * (1.0 - w) / 362880.0;
	Weights[1] = (502.0 / 9.0 + w * (-246.0 + w * (472.0 + w
						       * (-504.0 + w * (308.0 + w * (-84.0 + w * (-56.0 / 3.0 + w
												  * (24.0 + w * (-8.0 + w))))))))) / 40320.0;
	Weights[2] = (3652.0 / 9.0 - w * (2023.0 / 2.0 + w * (-952.0 + w
							      * (938.0 / 3.0 + w * (112.0 + w * (-119.0 + w * (56.0 / 3.0 + w
													       * (14.0 + w * (-7.0 + w))))))))) / 10080.0;
	Weights[3] = (44117.0 / 42.0 + w * (-2427.0 / 2.0 + w * (66.0 + w
								 * (434.0 + w * (-129.0 + w * (-69.0 + w * (34.0 + w * (6.0 + w
															* (-6.0 + w))))))))) / 4320.0;
	w2 = w * w;
	Weights[4] = (78095.0 / 63.0 - w2 * (700.0 + w2 * (-190.0 + w2
							   * (100.0 / 3.0 + w2 * (-5.0 + w))))) / 2880.0;
	Weights[5] = (44117.0 / 63.0 + w * (809.0 + w * (44.0 + w
							 * (-868.0 / 3.0 + w * (-86.0 + w * (46.0 + w * (68.0 / 3.0 + w
													 * (-4.0 + w * (-4.0 + w))))))))) / 2880.0;
	Weights[6] = (3652.0 / 21.0 - w * (-867.0 / 2.0 + w * (-408.0 + w
							       * (-134.0 + w * (48.0 + w * (51.0 + w * (-4.0 + w) * (-1.0 + w)
											    * (2.0 + w))))))) / 4320.0;
	Weights[7] = (251.0 / 18.0 + w * (123.0 / 2.0 + w * (118.0 + w
							     * (126.0 + w * (77.0 + w * (21.0 + w * (-14.0 / 3.0 + w
												     * (-6.0 + w * (-2.0 + w))))))))) / 10080.0;
	Weights[9] = w2 * w2;
	Weights[9] *= Weights[9] * w / 362880.0;
	Weights[8] = 1.0 - Weights[0] - Weights[1] - Weights[2] - Weights[3]
	  - Weights[4] - Weights[5] - Weights[6] - Weights[7] - Weights[9];
	break;
      default:
	std::cout<<"Invalid spline degree\n"<<std::endl;
      }
  }

/**
 ** \brief BSpline interpolation from the interpolation coefficients.
 ** based on http://bigwww.epfl.ch/thevenaz/interpolation/
 ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
 ** \date 2007/03/19
 ** \since 1.2.0
 ** \version 0.0.1
 ** \param coef_first RandomAccessIterator to the bspline interpolation range.
 ** \param coef_last RandomAccessIterator to the bspline interpolation range.
 ** \param x coordinate to interpolate.
 ** \param SplineDegree bspline degree.
 ** \par Example:
 ** \code
 ** slip::Array<double> A(10);
 **  slip::iota(A.begin(),A.end(),1.0,1.0);
 ** std::cout<<"A = \n"<<A<<std::endl;
 **
 ** slip::Array<double> Poles;
 ** slipalgo::BSplinePoles(3,Poles);
 ** std::cout<<"Poles = \n"<<Poles<<std::endl;
 ** slip::Array<double> Coef(A.size());
 ** slipalgo::BSplineComputeInterpolationCoefficients(A.begin(),A.end(),
 **                                                   Coef.begin(),
 **                                                   Poles.begin(),Poles.end(),
 **                                                   std::numeric_limits<double>::epsilon());
 **
 ** std::cout<<"Coef = \n"<<Coef<<std::endl;
 ** std::cout<<"interpolate at x = 0.5, degree 3 : "<<slipalgo::BSplineInterpolatedValue(Coef.begin(),Coef.end(),1.5,3)<<std::endl;
 ** \endcode
 */
template <class RandomAccessIterator>
inline
double	BSplineInterpolatedValue(RandomAccessIterator coef_first,
				 RandomAccessIterator coef_last,
				 double	x,
				 std::size_t SplineDegree)

{
  
  long coef_size = static_cast<long>(coef_last-coef_first);
  long	Width2 = 2L * coef_size - 2L;

  /* compute the interpolation indexes */
  slip::Array<long> Index(SplineDegree+1);
  slipalgo::BSplineComputeInterpolationIndexes(SplineDegree,
					       x,
					       Index.begin(),
					       Index.end());
  /* compute the interpolation weights */
  slip::Array<double> Weights;
  slipalgo::BSplineComputeInterpolationWeights(SplineDegree,
					       x,
					       Index.begin(),
					       Index.end(),
					       Weights);

  long splinedegree = static_cast<long>(SplineDegree);
  /* apply the mirror boundary conditions */
  for (long k = 0L; k <= splinedegree; ++k) 
    {
      Index[k] = (coef_size == 1L) ? (0L) : ((Index[k] < 0L) ?
		 (-Index[k] - Width2 * ((-Index[k]) / Width2))
		: (Index[k] - Width2 * (Index[k] / Width2)));
		if (coef_size <= Index[k]) 
		  {
		    Index[k] = Width2 - Index[k];
		  }
	  }

  /* perform interpolation */
  double interpolated = 0.0;

  interpolated = 0.0;
  for (long j = 0L; j <= splinedegree; ++j) 
    {
      interpolated += Weights[j] * coef_first[Index[j]];
    }

  return interpolated;
}


/*!
  ** \brief Convert a 2d samples range to a 2d bspline coefficients range.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2011/10/20
  ** \since 1.2.0
  ** \version 0.0.1
  ** \param in_up RandomAccessIterator2d to the input range.
  ** \param in_bot RandomAccessIterator2d to the input range.
  ** \param degree bspline degree (between 2 and 9)
  ** \param out_up RandomAccessIterator2d to the coefficients range.
  ** \param out_bot RandomAccessIterator2d to the coefficients range.
  ** \pre (in_bot-in_up)[0] == (out_bot-out_up)[0]
  ** \pre (in_bot-in_up)[1] == (out_bot-out_up)[1]
  ** \par Example:
  ** \code
  ** const std::size_t rows = 10;
  ** const std::size_t cols = 8;
  ** slip::Array2d<double> A(rows,cols)
  ** const std::size_t spline_degree = 3;
  ** slip::Array<double> Poles;
  ** slipalgo::BSplinePoles(spline_degree,Poles);
  ** slip::Array2d<double> Coef(rows,cols);
  ** slipalgo::BSplineSamplesToCoefficients2d(A.upper_left(),A.bottom_right(),
  **                                          spline_degree,
  **                                          Coef.upper_left(),
  **                                          Coef.bottom_right());
  ** \endcode
  */
template <typename RandomAccessIterator2d1,
	  typename RandomAccessIterator2d2>
inline
void BSplineSamplesToCoefficients2d(RandomAccessIterator2d1 in_up,
				    RandomAccessIterator2d1 in_bot,
				    const std::size_t degree,
				    RandomAccessIterator2d2 out_up,
				    RandomAccessIterator2d2 out_bot,
				    const double tolerance = 
				    std::numeric_limits<double>::epsilon())
{
  assert((in_bot-in_up)[0] == (out_bot-out_up)[0]);
  assert((in_bot-in_up)[1] == (out_bot-out_up)[1]);
  
  std::size_t rows = static_cast<std::size_t>((in_bot-in_up)[0]);
  std::size_t cols = static_cast<std::size_t>((in_bot-in_up)[1]);

  slip::Array<double> poles;
  slipalgo::BSplinePoles< slip::Array<double> >(degree,poles);

  /* convert the image samples into interpolation coefficients */
  /* separable process, along x */
  for (std::size_t y = 0; y < rows; ++y) 
    {
      slipalgo::BSplineComputeInterpolationCoefficients(in_up.row_begin(y),
							in_up.row_end(y),
							out_up.row_begin(y), 
							poles, 
							tolerance);
    }

  /* in-place separable process, along y */
  slip::Array<double> tmp_col(rows);
  for (std::size_t x = 0; x < cols; ++x) 
    {
      std::copy(out_up.col_begin(x),out_up.col_end(x),tmp_col.begin());
      slipalgo::BSplineComputeInterpolationCoefficients(tmp_col.begin(),tmp_col.end(),out_up.col_begin(x), poles,tolerance);

    }
} 



 /*!
  ** \brief BSpline computation of coefficients for 2D container
  ** based on http://bigwww.epfl.ch/thevenaz/interpolation/
  ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
  ** \date 2007/03/19
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param degree spline degree (between 2 and 9)
  ** \param input 2D container of input datas
  ** \param output 2D container of output datas (coefficients)
  ** \return 1 if error else 0
  ** \pre containers must have the same sizes 
  ** \pre containers must have iterators on rows and columns
  ** \pre containers must have width() and height() functions
  ** \pre input.rows()=output.rows()
  ** \pre input.cols()=output.cols()
  ** \deprecated user BSplineSamplesToCoefficients2d instead
  */
template <class Matrix1,
	  class Matrix2>
inline
void BSplineSamplesToCoefficients(const std::size_t degree,
				  const Matrix1 &input,
				  Matrix2 &output)
{
  
  slipalgo::BSplineSamplesToCoefficients2d(input.upper_left(),
					   input.bottom_right(),
					   degree,
					   output.upper_left(),
					   output.bottom_right());
} 

/*!
  ** \brief Convert a 3d samples range to a 3d bspline coefficients range.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2011/10/20
  ** \since 1.2.0
  ** \version 0.0.1
  ** \param in_fup RandomAccessIterator3d to the input range.
  ** \param in_bbot RandomAccessIterator3d to the input range.
  ** \param degree bspline degree (between 2 and 9)
  ** \param out_fup RandomAccessIterator3d to the output range.
  ** \param out_bbot RandomAccessIterator3d to the output range.
  ** \pre (in_bbot-in_fup)[0] <= (out_bbot-out_fup)[0]
  ** \pre (in_bbot-in_fup)[1] <= (out_bbot-out_fup)[1]
  ** \pre (in_bbot-in_fup)[2] <= (out_bbot-out_fup)[3]
  ** \par Example:
  ** \code
  ** slip::Array3d<double> Min3d(5,7,6);
  ** slip::iota(Min3d.begin(),Min3d.end(),1.0,1.0);
  ** std::cout<<"Min3d = \n"<<Min3d<<std::endl;
  ** slip::Array3d<double> Coeff3d(Min3d.slices(),
  **			           Min3d.rows(),
  **			           Min3d.cols());
  **
  ** slipalgo::BSplineSamplesToCoefficients3d(Min3d.front_upper_left(),
  **				              Min3d.back_bottom_right(),
  **				              3,
  **				              Coeff3d.front_upper_left(),
  **				              Coeff3d.back_bottom_right());
  **
  ** std::cout<<"Coeff3d = \n"<<Coeff3d<<std::endl;
  **
  ** \endcode
  */
template <typename RandomAccessIterator3d1,
	  typename RandomAccessIterator3d2>
inline
void BSplineSamplesToCoefficients3d(RandomAccessIterator3d1 in_fup,
				    RandomAccessIterator3d1 in_bbot,
				    const std::size_t degree,
				    RandomAccessIterator3d2 out_fup,
				    RandomAccessIterator3d2 out_bbot,
				    const double tolerance = 
				    std::numeric_limits<double>::epsilon())
{
  assert((in_bbot-in_fup) == (out_bbot-out_fup));
  std::size_t slices = static_cast<std::size_t>((in_bbot-in_fup)[0]);
  std::size_t rows   = static_cast<std::size_t>((in_bbot-in_fup)[1]);
  std::size_t cols   = static_cast<std::size_t>((in_bbot-in_fup)[2]);

  slip::Array<double> poles;
  slipalgo::BSplinePoles< slip::Array<double> >(degree,poles);

  //slip::Array3d<double> tmp(slices,rows,cols);

  /* separable process, along z */
  for (std::size_t x = 0; x < cols; ++x)
    {
      for (std::size_t y = 0; y < rows; ++y)
	{
	  slipalgo::BSplineComputeInterpolationCoefficients(in_fup.slice_begin(y,x),in_fup.slice_end(y,x),out_fup.slice_begin(y,x), poles, tolerance);
	}
    }

  /* in-place separable process, along y */
  slip::Array<double> tmp_row(rows);
  for (std::size_t z = 0; z < slices; ++z)
    {
      for (std::size_t x = 0; x < cols; ++x)
	{
	  slipalgo::BSplineComputeInterpolationCoefficients(out_fup.col_begin(z,x),out_fup.col_end(z,x),tmp_row.begin(), poles,tolerance);
	  std::copy(tmp_row.begin(),tmp_row.end(),out_fup.col_begin(z,x));
	}
    }

  /* in-place separable process, along x */
  slip::Array<double> tmp_col(cols);
  for (std::size_t z = 0; z < slices; ++z)
    {
      for (std::size_t y = 0; y < rows; ++y)
	{
	  slipalgo::BSplineComputeInterpolationCoefficients(out_fup.row_begin(z,y),out_fup.row_end(z,y),tmp_col.begin(), poles, tolerance);
	  std::copy(tmp_col.begin(),tmp_col.end(),out_fup.row_begin(z,y));
	}
    }
} 


/*!
  ** \brief Computes the 2d BSpline interpolation from 2d bspline interpolation coefficients.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2011/10/20
  ** \since 1.2.0
  ** \version 0.0.1
  ** \param Bcoeff_up RandomAccessIterator2d to the 2d bspline interpolation coefficients input range.
  ** \param Bcoeff_bot RandomAccessIterator2d to the 2d bspline interpolation coefficients input range.
  ** \param x x interpolation coordinate.
  ** \param y y interpolation coordinate.
  ** \param SplineDegree bspline degree (between 2 and 9)

  */
template <typename RandomAccessIterator2d>
inline
double	BSplineInterpolatedValue(RandomAccessIterator2d Bcoeff_up,
				 RandomAccessIterator2d Bcoeff_bot,
				 double	x,
				 double	y,
				 std::size_t SplineDegree)

{ 
  long rows = static_cast<long>((Bcoeff_bot - Bcoeff_up)[0]);
  long cols = static_cast<long>((Bcoeff_bot - Bcoeff_up)[1]);

  long	Width2 = 2L * cols - 2L; 
  long Height2 = 2L * rows - 2L;
  long splinedegree = static_cast<long>(SplineDegree);
	
  /* compute the interpolation indexes */
  slip::Array<long> xIndex(SplineDegree+1);
  slipalgo::BSplineComputeInterpolationIndexes(SplineDegree,
					       x,
					       xIndex.begin(),
					       xIndex.end());
  slip::Array<long> yIndex(SplineDegree+1);
  slipalgo::BSplineComputeInterpolationIndexes(SplineDegree,
					       y,
					       yIndex.begin(),
					       yIndex.end());
  
  
  /* compute the interpolation weights */
  slip::Array<double> xWeights;
  slipalgo::BSplineComputeInterpolationWeights(SplineDegree,
					       x,
					       xIndex.begin(),
					       xIndex.end(),
					       xWeights);
  slip::Array<double> yWeights;
  slipalgo::BSplineComputeInterpolationWeights(SplineDegree,
					       y,
					       yIndex.begin(),
					       yIndex.end(),
					       yWeights);

  /* apply the mirror boundary conditions */
  for (long k = 0L; k <= splinedegree; k++) 
    {
      xIndex[k] = (cols == 1L) ? (0L) : ((xIndex[k] < 0L) ?
		  (-xIndex[k] - Width2 * ((-xIndex[k]) / Width2))
		: (xIndex[k] - Width2 * (xIndex[k] / Width2)));
		if (cols <= xIndex[k]) 
		  {
		    xIndex[k] = Width2 - xIndex[k];
		  }
		yIndex[k] = (rows == 1L) ? (0L) : ((yIndex[k] < 0L) ?
			(-yIndex[k] - Height2 * ((-yIndex[k]) / Height2))
			: (yIndex[k] - Height2 * (yIndex[k] / Height2)));
		if (rows <= yIndex[k]) 
		  {
		    yIndex[k] = Height2 - yIndex[k];
		  }

    }

  /* perform interpolation */
  double interpolated = 0.0;
  slip::DPoint2d<int> d(0,0);
  for (long j = 0L; j <= splinedegree; j++) 
    {
      double w = 0.0;
      d[0] = yIndex[j];
      for (long i = 0L; i <= splinedegree; i++) 
	{
	  d[1] = xIndex[i];
	  w += xWeights[i] * Bcoeff_up[d];
	}
      interpolated += yWeights[j] * w;
    }

  return(interpolated);
} 

/*!
  ** \brief Computes the 2d BSpline interpolation from 2d bspline interpolation coefficients.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2011/10/20
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param Bcoeff Matrix of the 2d BSpline coefficients.
  ** \param x x interpolation coordinate.
  ** \param y y interpolation coordinate.
  ** \param SplineDegree bspline degree (between 2 and 9)
  ** \return double interpolated value.
  ** \par Example:
  ** \code
  ** const std::size_t rows = 10;
  ** const std::size_t cols = 8;
  ** slip::Array2d<double> A(rows,cols)
  ** const std::size_t spline_degree = 3;
  ** slip::Array<double> Poles;
  ** slipalgo::BSplinePoles(spline_degree,Poles);
  ** slip::Array2d<double> Coef(rows,cols);
  ** slipalgo::BSplineSamplesToCoefficients2d(A.upper_left(),A.bottom_right(),
  **                                          spline_degree,
  **                                          Coef.upper_left(),
  **                                          Coef.bottom_right());
  ** const std::size_t out_rows = 20;
  ** const std::size_t out_cols = 16;
  ** slip::Array<double> x(out_cols);
  ** slip::iota(x.begin(),x.end(),0.0,1.0);
  ** slip::range_fun_interab<double,double> funx(0.0,double(out_cols-1),0.0,double(in_cols-1));
  ** slip::change_dynamic(x.begin(),x.end(),x.begin(),funx);
  ** 
  ** slip::Array<double> y(out_rows);
  ** slip::iota(y.begin(),y.end(),0.0,1.0);
  ** slip::range_fun_interab<double,double> funy(0.0,double(out_rows-1),0.0,double(in_rows-1));
  ** slip::change_dynamic(y.begin(),y.end(),y.begin(),funy);
  ** for(std::size_t i = 0; i < out_rows; ++i)
  **  {
  **   for(std::size_t j = 0; j < out_cols; ++j)
  **	{
  **	  out[i][j] = 
  **	    slipalgo::BSplineInterpolatedValue(Coef,
  **                                           x[j],
  **					       y[i],
  **					       spline_degree);
  **	}
  **  }
  ** 
  ** \endcode
  */
template <class Matrix1>
inline
double	BSplineInterpolatedValue(Matrix1& Bcoeff,	
				 double	x,
				 double	y,
				 std::size_t SplineDegree)

{ /* begin InterpolatedValue */


	long	Width2 = 2L * Bcoeff.cols() - 2L, Height2 = 2L * Bcoeff.rows() - 2L;
	long splinedegree = static_cast<long>(SplineDegree);
	
	/* compute the interpolation indexes */
	slip::Array<long> xIndex(SplineDegree+1);
	slipalgo::BSplineComputeInterpolationIndexes(SplineDegree,
						     x,
						     xIndex.begin(),
						     xIndex.end());
	slip::Array<long> yIndex(SplineDegree+1);
	slipalgo::BSplineComputeInterpolationIndexes(SplineDegree,
						     y,
						     yIndex.begin(),
						     yIndex.end());


	/* compute the interpolation weights */
	 slip::Array<double> xWeights;
	 slipalgo::BSplineComputeInterpolationWeights(SplineDegree,
						      x,
						      xIndex.begin(),
						      xIndex.end(),
						      xWeights);
	 slip::Array<double> yWeights;
	 slipalgo::BSplineComputeInterpolationWeights(SplineDegree,
						      y,
						      yIndex.begin(),
						      yIndex.end(),
						      yWeights);

	/* apply the mirror boundary conditions */
	for (long k = 0L; k <= splinedegree; k++) 
	  {
	    
	    xIndex[k] = (Bcoeff.cols() == 1L) ? (0L) : ((xIndex[k] < 0L) ?
			(-xIndex[k] - Width2 * ((-xIndex[k]) / Width2))
		      : (xIndex[k] - Width2 * (xIndex[k] / Width2)));
		if (((long) Bcoeff.cols()) <= xIndex[k]) 
		  {
		    xIndex[k] = Width2 - xIndex[k];
		  }
		yIndex[k] = (Bcoeff.rows() == 1L) ? (0L) : ((yIndex[k] < 0L) ?
			(-yIndex[k] - Height2 * ((-yIndex[k]) / Height2))
			: (yIndex[k] - Height2 * (yIndex[k] / Height2)));
		if (((long) Bcoeff.rows()) <= yIndex[k]) 
		  {
		    yIndex[k] = Height2 - yIndex[k];
		  }

	  }

	/* perform interpolation */
	double interpolated = 0.0;
	for (long j = 0L; j <= splinedegree; j++) 
	  {
	    double w = 0.0;
	    for (long i = 0L; i <= splinedegree; i++) 
	      {
		w += xWeights[i] * Bcoeff[yIndex[j]][xIndex[i]];
	      }
	    interpolated += yWeights[j] * w;
	  }

	return(interpolated);
} 

/*!
  ** \brief Computes the 3d BSpline interpolation from 3d bspline interpolation coefficients.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2011/10/20
  ** \since 1.2.0
  ** \version 0.0.1
  ** \param Bcoeff 3d bspline interpolation coefficients.
  ** \param x x interpolation coordinate.
  ** \param y y interpolation coordinate.
  ** \param z z interpolation coordinate.
  ** \param SplineDegree bspline degree (between 2 and 9)
  ** \return double interpolated value
  ** \par Example:
  ** \code
  ** const std::size_t in_slices = 4;
  ** const std::size_t in_rows   = 8;
  ** const std::size_t in_cols   = 10;
  ** slip::Array3d<double> Ain(in_slices, in_rows,in_cols);
  ** const std::size_t out_slices = 2*in_slices;
  ** const std::size_t out_rows   = 2*in_rows;
  ** const std::size_t out_cols   = 2*in_cols;
  ** slip::Array3d<double> Aout(out_slices, out_rows,out_cols);
  ** slip::Array<double> Poles;
  ** slipalgo::BSplinePoles(spline_degree,Poles);
  ** slip::Array3d<double> Coef(in_slices,in_rows,in_cols);
  ** slipalgo::BSplineSamplesToCoefficients3d(Ain.front_upper_left(),
  **                                          Ain.back_bottom_right(),
  **                                          spline_degree,
  **                                          Coef.front_upper_left(),
  **                                          Coef.back_bottom_right());
  ** slip::Array<double> x(out_cols);
  ** slip::iota(x.begin(),x.end(),0.0,1.0);
  ** slip::range_fun_interab<double,double> funx(0.0,double(out_cols-1),0.0,double(in_cols-1));
  ** slip::change_dynamic(x.begin(),x.end(),x.begin(),funx);
  ** slip::Array<double> y(out_rows);
  ** slip::iota(y.begin(),y.end(),0.0,1.0);
  ** slip::range_fun_interab<double,double> funy(0.0,double(out_rows-1),0.0,double(in_rows-1));
  ** slip::change_dynamic(y.begin(),y.end(),y.begin(),funy);
  **
  ** slip::Array<double> z(out_slices);
  ** slip::iota(z.begin(),z.end(),0.0,1.0);
  ** slip::range_fun_interab<double,double> funz(0.0,double(out_slices-1),0.0,double(in_slices-1));
  ** slip::change_dynamic(z.begin(),z.end(),z.begin(),funz);
  ** 
  ** for(std::size_t k = 0; k < out_slices; ++k)
  **  {
  **    for(std::size_t i = 0; i < out_rows; ++i)
  **	{
  **	  for(std::size_t j = 0; j < out_cols; ++j)
  **	    {
  **	      out[k][i][j] = 
  **		slipalgo::BSplineInterpolatedValue(Coef,
  **						   x[j],
  **						   y[i],
  **						   z[k],
  **						   spline_degree);
  **	      
  **	    }
  **	}
  **  }
  ** \endcode
  */
template <class Container3d>
inline
double	BSplineInterpolatedValue(Container3d& Bcoeff,	
				 double	x,
				 double	y,
				 double z,
				 std::size_t SplineDegree)

{ 

  long	Width2  = 2L * Bcoeff.cols()   - 2L;
  long  Height2 = 2L * Bcoeff.rows()   - 2L;
  long  Depth2  = 2L * Bcoeff.slices() - 2L;
  
  long splinedegree = static_cast<long>(SplineDegree);
	
  /* compute the interpolation indexes */
  slip::Array<long> xIndex(SplineDegree+1);
  slipalgo::BSplineComputeInterpolationIndexes(SplineDegree,
					       x,
					       xIndex.begin(),
					       xIndex.end());
  slip::Array<long> yIndex(SplineDegree+1);
  slipalgo::BSplineComputeInterpolationIndexes(SplineDegree,
					       y,
					       yIndex.begin(),
					       yIndex.end());

  slip::Array<long> zIndex(SplineDegree+1);
  slipalgo::BSplineComputeInterpolationIndexes(SplineDegree,
					       z,
					       zIndex.begin(),
					       zIndex.end());


  /* compute the interpolation weights */
  slip::Array<double> xWeights;
  slipalgo::BSplineComputeInterpolationWeights(SplineDegree,
					       x,
					       xIndex.begin(),
					       xIndex.end(),
					       xWeights);
  slip::Array<double> yWeights;
  slipalgo::BSplineComputeInterpolationWeights(SplineDegree,
					       y,
					       yIndex.begin(),
					       yIndex.end(),
					       yWeights);

  slip::Array<double> zWeights;
  slipalgo::BSplineComputeInterpolationWeights(SplineDegree,
					       z,
					       zIndex.begin(),
					       zIndex.end(),
					       zWeights);

  /* apply the mirror boundary conditions */
  for (long k = 0L; k <= splinedegree; k++) 
    {
	    
      xIndex[k] = (Bcoeff.cols() == 1L) ? (0L) : ((xIndex[k] < 0L) ?
						  (-xIndex[k] - Width2 * ((-xIndex[k]) / Width2))
						  : (xIndex[k] - Width2 * (xIndex[k] / Width2)));
      if (((long) Bcoeff.cols()) <= xIndex[k]) 
	{
	  xIndex[k] = Width2 - xIndex[k];
	}
      yIndex[k] = (Bcoeff.rows() == 1L) ? (0L) : ((yIndex[k] < 0L) ?
						  (-yIndex[k] - Height2 * ((-yIndex[k]) / Height2))
						  : (yIndex[k] - Height2 * (yIndex[k] / Height2)));
      if (((long) Bcoeff.rows()) <= yIndex[k]) 
	{
	  yIndex[k] = Height2 - yIndex[k];
	}
      zIndex[k] = (Bcoeff.slices() == 1L) ? (0L) : ((zIndex[k] < 0L) ?
						    (-zIndex[k] - Depth2 * ((-zIndex[k]) / Depth2))
						    : (zIndex[k] - Depth2 * (zIndex[k] / Depth2)));
      if (((long) Bcoeff.slices()) <= zIndex[k]) 
	{
	  zIndex[k] = Depth2 - zIndex[k];
	}

    }

  /* perform interpolation */
  double interpolated = 0.0;
  for (long j = 0L; j <= splinedegree; ++j) 
    {
      double w = 0.0;
      for (long i = 0L; i <= splinedegree; ++i) 
	{
	  double w2 = 0.0;
	  for (long k = 0L; k <= splinedegree; ++k) 
	    {
	      w2 += zWeights[k] * Bcoeff[zIndex[k]][yIndex[j]][xIndex[i]];
	    }
	  w += xWeights[i] * w2;
	}
      interpolated += yWeights[j] * w;
    }	

  return(interpolated);
} 

/*!
  ** \brief Computes the 3d BSpline interpolation from 3d bspline interpolation coefficients.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2011/10/20
  ** \since 1.2.0
  ** \version 0.0.1
  ** \param Bcoeff_fup RandomAccessIterator3d to the 3d bspline interpolation coefficients input range.
  ** \param Bcoeff_bbot RandomAccessIterator3d to the 3d bspline interpolation coefficients input range.
  ** \param x x interpolation coordinate.
  ** \param y y interpolation coordinate.
  ** \param z z interpolation coordinate.
  ** \param SplineDegree bspline degree (between 2 and 9)
  */
template <typename RandomAccessIterator3d>
inline
double	BSplineInterpolatedValue(RandomAccessIterator3d Bcoeff_fup,
				 RandomAccessIterator3d Bcoeff_bbot,
				 double	x,
				 double	y,
				 double z,
				 std::size_t SplineDegree)

{ 
  long slices = static_cast<long>((Bcoeff_bbot - Bcoeff_fup)[0]);
  long rows   = static_cast<long>((Bcoeff_bbot - Bcoeff_fup)[1]);
  long cols   = static_cast<long>((Bcoeff_bbot - Bcoeff_fup)[2]);

  
  long	Width2  = 2L * cols   - 2L;
  long  Height2 = 2L * rows   - 2L;
  long  Depth2  = 2L * slices - 2L;
  
  long splinedegree = static_cast<long>(SplineDegree);
	
  /* compute the interpolation indexes */
  slip::Array<long> xIndex(SplineDegree+1);
  slipalgo::BSplineComputeInterpolationIndexes(SplineDegree,
					       x,
					       xIndex.begin(),
					       xIndex.end());
  slip::Array<long> yIndex(SplineDegree+1);
  slipalgo::BSplineComputeInterpolationIndexes(SplineDegree,
					       y,
					       yIndex.begin(),
					       yIndex.end());

  slip::Array<long> zIndex(SplineDegree+1);
  slipalgo::BSplineComputeInterpolationIndexes(SplineDegree,
					       z,
					       zIndex.begin(),
					       zIndex.end());


  /* compute the interpolation weights */
  slip::Array<double> xWeights;
  slipalgo::BSplineComputeInterpolationWeights(SplineDegree,
					       x,
					       xIndex.begin(),
					       xIndex.end(),
					       xWeights);
  slip::Array<double> yWeights;
  slipalgo::BSplineComputeInterpolationWeights(SplineDegree,
					       y,
					       yIndex.begin(),
					       yIndex.end(),
					       yWeights);

  slip::Array<double> zWeights;
  slipalgo::BSplineComputeInterpolationWeights(SplineDegree,
					       z,
					       zIndex.begin(),
					       zIndex.end(),
					       zWeights);

  /* apply the mirror boundary conditions */
  for (long k = 0L; k <= splinedegree; k++) 
    {
	    
      xIndex[k] = (cols == 1L) ? (0L) : ((xIndex[k] < 0L) ?
						  (-xIndex[k] - Width2 * ((-xIndex[k]) / Width2))
						  : (xIndex[k] - Width2 * (xIndex[k] / Width2)));
      if (cols <= xIndex[k]) 
	{
	  xIndex[k] = Width2 - xIndex[k];
	}
      yIndex[k] = (rows == 1L) ? (0L) : ((yIndex[k] < 0L) ?
						  (-yIndex[k] - Height2 * ((-yIndex[k]) / Height2))
						  : (yIndex[k] - Height2 * (yIndex[k] / Height2)));
      if (rows <= yIndex[k]) 
	{
	  yIndex[k] = Height2 - yIndex[k];
	}
      zIndex[k] = (slices == 1L) ? (0L) : ((zIndex[k] < 0L) ?
						    (-zIndex[k] - Depth2 * ((-zIndex[k]) / Depth2))
						    : (zIndex[k] - Depth2 * (zIndex[k] / Depth2)));
      if (slices <= zIndex[k]) 
	{
	  zIndex[k] = Depth2 - zIndex[k];
	}

    }

  /* perform interpolation */
  double interpolated = 0.0;
  slip::DPoint3d<int> d(0,0,0);
  for (long j = 0L; j <= splinedegree; ++j) 
    {
      double w = 0.0;
      d[1] = yIndex[j];
      for (long i = 0L; i <= splinedegree; ++i) 
	{
	  double w2 = 0.0;
	   d[2] = xIndex[i];
	  for (long k = 0L; k <= splinedegree; ++k) 
	    {
	      d[0] = zIndex[k];
	      w2 += zWeights[k] * Bcoeff_fup[d];
	    }
	  w += xWeights[i] * w2;
	}
      interpolated += yWeights[j] * w;
    }	

  return(interpolated);
} 


 /*!
  ** \brief Resample a range using bspline interpolation.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2011/10/20
  ** \since 1.2.0
  ** \version 0.0.1
  ** \param in_first RandomAccessIterator to the input range.
  ** \param in_last RandomAccessIterator to the input range.
  ** \param degree bspline degree (between 2 and 9)
  ** \param out_first RandomAccessIterator to the output range.
  ** \param out_last RandomAccessIterator to the output range.
  ** \pre (out_last-out_first) >= (in_last-in_first)
  ** \par Example:
  ** \code
  ** slip::Array<double> A(10);
  ** slip::iota(A.begin(),A.end(),1.0,1.0);
  ** std::cout<<"A = \n"<<A<<std::endl;
  ** slip::Array<double> Aup2(A.size()*3);
  ** slipalgo::bspline_resampling_1d(A.begin(),A.end(),
  **				     3,
  **				     Aup2.begin(),Aup2.end());
  ** std::cout<<"Aup 3 = \n"<<Aup2<<std::endl;
  ** \endcode
  */
template <typename RandomAccessIterator1,
	  typename RandomAccessIterator2>
void bspline_resampling_1d(RandomAccessIterator1 in_first, 
			   RandomAccessIterator1 in_last,
			   const std::size_t spline_degree,
			   RandomAccessIterator2 out_first, 
			   RandomAccessIterator2 out_last)
{
  assert(static_cast<std::size_t>(out_last-out_first) >= static_cast<std::size_t>(in_last-in_first));
  std::size_t in_size = static_cast<std::size_t>(in_last-in_first);
  std::size_t out_size = static_cast<std::size_t>(out_last-out_first);
  slip::Array<double> Poles;
  slipalgo::BSplinePoles(spline_degree,Poles);
  slip::Array<double> Coef(in_size);
  slipalgo::BSplineComputeInterpolationCoefficients(in_first,in_last,
						    Coef.begin(),
						    Poles,
						    std::numeric_limits<double>::epsilon());
  
  slip::Array<double> x(out_size);
  slip::iota(x.begin(),x.end(),0.0,1.0);
  slip::range_fun_interab<double,double> funab(0.0,double(out_size-1),0.0,double(in_size-1));
  slip::change_dynamic(x.begin(),x.end(),x.begin(),funab);
  
  slip::Array<double>::iterator it_x = x.begin();
  for(; out_first != out_last; ++out_first,++it_x)
    {
      *out_first = 
	slipalgo::BSplineInterpolatedValue(Coef.begin(),Coef.end(),
					   *it_x,spline_degree);
    }
}


/*!
  ** \brief Resample a 2d range using bspline interpolation.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2011/10/20
  ** \since 1.2.0
  ** \version 0.0.1
  ** \param in_up RandomAccessIterator2d to the input range.
  ** \param in_bot RandomAccessIterator2d to the input range.
  ** \param degree bspline degree (between 2 and 9)
  ** \param out_up RandomAccessIterator2d to the output range.
  ** \param out_bot RandomAccessIterator2d to the output range.
  ** \pre (in_bot-in_up)[0] <= (out_bot-out_up)[0]
  ** \pre (in_bot-in_up)[1] <= (out_bot-out_up)[1]
  ** \par Example:
  ** \code
  ** slip::Array2d<double> Min(8,7);
  ** slip::iota(Min.begin(),Min.end(),1.0,1.0);
  ** std::cout<<"Min = \n"<<Min<<std::endl;
  ** slip::Array2d<double> Mout(Min.rows()*2,
  **			     Min.cols()*3);
  **
  ** slipalgo::bspline_resampling_2d(Min.upper_left(),Min.bottom_right(),
  **				     3,
  **				     Mout.upper_left(),Mout.bottom_right());
  **
  ** std::cout<<"Mout = \n"<<Mout<<std::endl;
  ** \endcode
  */
template <typename RandomAccessIterator2d1,
	  typename RandomAccessIterator2d2>
void bspline_resampling_2d(RandomAccessIterator2d1 in_up, 
			   RandomAccessIterator2d1 in_bot,
			   const std::size_t spline_degree,
			   RandomAccessIterator2d2 out_up, 
			   RandomAccessIterator2d2 out_bot)
{
  assert((in_bot-in_up)[0] <= (out_bot-out_up)[0]);
  assert((in_bot-in_up)[1] <= (out_bot-out_up)[1]);
  
  std::size_t in_rows = static_cast<std::size_t>((in_bot-in_up)[0]);
  std::size_t in_cols = static_cast<std::size_t>((in_bot-in_up)[1]);

  std::size_t out_rows = static_cast<std::size_t>((out_bot-out_up)[0]);
  std::size_t out_cols = static_cast<std::size_t>((out_bot-out_up)[1]);

  slip::Array<double> Poles;
  slipalgo::BSplinePoles(spline_degree,Poles);
  slip::Array2d<double> Coef(in_rows,in_cols);
  slipalgo::BSplineSamplesToCoefficients2d(in_up,in_bot,
					   spline_degree,
					   Coef.upper_left(),
					   Coef.bottom_right());
  
  slip::Array<double> x(out_cols);
  slip::iota(x.begin(),x.end(),0.0,1.0);
  slip::range_fun_interab<double,double> funx(0.0,double(out_cols-1),0.0,double(in_cols-1));
  slip::change_dynamic(x.begin(),x.end(),x.begin(),funx);

  slip::Array<double> y(out_rows);
  slip::iota(y.begin(),y.end(),0.0,1.0);
  slip::range_fun_interab<double,double> funy(0.0,double(out_rows-1),0.0,double(in_rows-1));
  slip::change_dynamic(y.begin(),y.end(),y.begin(),funy);

 
  slip::DPoint2d<int> d(0,0);
  for(std::size_t i = 0; i < out_rows; ++i)
    {
      d[0] = i;
      for(std::size_t j = 0; j < out_cols; ++j)
	{
	  d[1] = j;
	  out_up[d] = 
	    slipalgo::BSplineInterpolatedValue(Coef.upper_left(),
					       Coef.bottom_right(),
					       x[j],
					       y[i],
					       spline_degree);
	  
	}
    }
 
}



/*!
  ** \brief Resample a 3d range using bspline interpolation.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2011/10/20
  ** \since 1.2.0
  ** \version 0.0.1
  ** \param in_fup RandomAccessIterator3d to the input range.
  ** \param in_bbot RandomAccessIterator3d to the input range.
  ** \param degree bspline degree (between 2 and 9)
  ** \param out_fup RandomAccessIterator3d to the output range.
  ** \param out_bbot RandomAccessIterator3d to the output range.
  ** \pre (in_bbot-in_fup)[0] <= (out_bbot-out_fup)[0]
  ** \pre (in_bbot-in_fup)[1] <= (out_bbot-out_fup)[1]
  ** \pre (in_bbot-in_fup)[2] <= (out_bbot-out_fup)[3]
  ** \par Example:
  ** \code
  ** slip::Array3d<double> Min3d(5,7,6);
  ** slip::iota(Min3d.begin(),Min3d.end(),1.0,1.0);
  ** std::cout<<"Min3d = \n"<<Min3d<<std::endl;
  ** slip::Array3d<double> Mout3d(Min3d.slices()*2,
  **			       Min3d.rows()*2,
  **			       Min3d.cols()*2);
  **
  ** slipalgo::bspline_resampling_3d(Min3d.front_upper_left(),
  **				  Min3d.back_bottom_right(),
  **				  3,
  **				  Mout3d.front_upper_left(),
  **				  Mout3d.back_bottom_right());
  **
  ** std::cout<<"Mout3d = \n"<<Mout3d<<std::endl;
  **
  ** \endcode
  */
template <typename RandomAccessIterator3d1,
	  typename RandomAccessIterator3d2>
void bspline_resampling_3d(RandomAccessIterator3d1 in_fup, 
			    RandomAccessIterator3d1 in_bbot,
			    const std::size_t spline_degree,
			    RandomAccessIterator3d2 out_fup, 
			    RandomAccessIterator3d2 out_bbot)
{
  // assert(static_cast<std::size_t>(in_last-in_first)
  std::size_t in_slices = static_cast<std::size_t>((in_bbot-in_fup)[0]); 
  std::size_t in_rows   = static_cast<std::size_t>((in_bbot-in_fup)[1]);
  std::size_t in_cols   = static_cast<std::size_t>((in_bbot-in_fup)[2]);

  std::size_t out_slices = static_cast<std::size_t>((out_bbot-out_fup)[0]);
  std::size_t out_rows   = static_cast<std::size_t>((out_bbot-out_fup)[1]);
  std::size_t out_cols   = static_cast<std::size_t>((out_bbot-out_fup)[2]);

  slip::Array<double> Poles;
  slipalgo::BSplinePoles(spline_degree,Poles);
  slip::Array3d<double> Coef(in_slices,in_rows,in_cols);
  slipalgo::BSplineSamplesToCoefficients3d(in_fup,in_bbot,
					   spline_degree,
					   Coef.front_upper_left(),Coef.back_bottom_right());
  
  slip::Array<double> x(out_cols);
  slip::iota(x.begin(),x.end(),0.0,1.0);
  slip::range_fun_interab<double,double> funx(0.0,double(out_cols-1),0.0,double(in_cols-1));
  slip::change_dynamic(x.begin(),x.end(),x.begin(),funx);

  slip::Array<double> y(out_rows);
  slip::iota(y.begin(),y.end(),0.0,1.0);
  slip::range_fun_interab<double,double> funy(0.0,double(out_rows-1),0.0,double(in_rows-1));
  slip::change_dynamic(y.begin(),y.end(),y.begin(),funy);

  slip::Array<double> z(out_slices);
  slip::iota(z.begin(),z.end(),0.0,1.0);
  slip::range_fun_interab<double,double> funz(0.0,double(out_slices-1),0.0,double(in_slices-1));
  slip::change_dynamic(z.begin(),z.end(),z.begin(),funz);

 
  slip::DPoint3d<int> d(0,0,0);
  for(std::size_t k = 0; k < out_slices; ++k)
    {
      d[0] = k;
      for(std::size_t i = 0; i < out_rows; ++i)
	{
	  d[1] = i;
	  for(std::size_t j = 0; j < out_cols; ++j)
	    {
	      d[2] = j;
	      out_fup[d] = 
		slipalgo::BSplineInterpolatedValue(Coef.front_upper_left(),
						   Coef.back_bottom_right(),
						   x[j],
						   y[i],
						   z[k],
						   spline_degree);
	      
	    }
	}
    }
}


/*!
  ** \brief BSpline computation of a geometric transformation.
  ** based on http://bigwww.epfl.ch/thevenaz/interpolation/
  ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
  ** \date 2007/03/19
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param InputImage Input Image
  ** \param OutputImage Output Image
  ** \param SplineDegree Spline degree
  ** \param OriginX Origin of the x
  ** \param OriginY Origin of the y
  ** \param Angle Rotation angle
  ** \param ShiftX Shift for x
  ** \param ShiftY Shift for y
  ** \param Masking Masking uncover areas
  ** \par Example:
  ** \code
  ** slip::GrayscaleImage<double> ImageIn;
  ** ImageIn.read("lena.jpg");
  ** slip::GrayscaleImage<double> ImageOut(ImageIn.rows(),ImageIn.cols());
  ** slip::BSplineGeometricTransformation(ImageIn,ImageOut,3,0,0,15,0,0,true);
  ** ImageOut.write("interpol.png");
  ** \endcode
  */
template <class GrayScaleImage>
inline
void BSplineGeometricTransformation (const GrayScaleImage &InputImage,
				     GrayScaleImage& OutputImage,
				     const int SplineDegree=3,
				     const double OriginX=0.0,
				     const double OriginY=0.0,
				     const double Angle=0.0,
				     const double ShiftX=0.0,
				     const double ShiftY=0.0,
				     const bool Masking=true)
  
{ 
  std::size_t Width = OutputImage.rows();
  std::size_t Height = OutputImage.cols();
  double width  = static_cast<double>(Width) - 0.5;
  double height = static_cast<double>(Height) - 0.5;
  
  /* convert between a representation based on image samples */
  /* and a representation based on image B-spline coefficients */
  slip::Array2d<double> Coefficients(InputImage.rows(), InputImage.cols(),0.0);
  slipalgo::BSplineSamplesToCoefficients2d(InputImage.upper_left(),
					   InputImage.bottom_right(),
					   SplineDegree,
					   Coefficients.upper_left(),
					   Coefficients.bottom_right());
  /* prepare the geometry */
  double angle = Angle * slip::constants<double>::pi() / 180.0;
  double a11 = std::cos(angle);
  double a12 = -std::sin(angle);
  double a21 = -a12;
  double a22 = a11;
  double x0 = a11 * (ShiftX + OriginX) + a12 * (ShiftY + OriginY);
  double y0 = a21 * (ShiftX + OriginX) + a22 * (ShiftY + OriginY);
  double shiftX = OriginX - x0;
  double shiftY = OriginY - y0;

  /* visit all pixels of the output image and assign their value */
  //	p = OutputImage;

  for (std::size_t y = 0; y < Height; ++y) 
    {
      x0 = a12 * (double)y + shiftX;
      y0 = a22 * (double)y + shiftY;
      for (std::size_t x = 0; x < Width; ++x) 
	{
	  double x1 = x0 + a11 * static_cast<double>(x);
	  double y1 = y0 + a21 * static_cast<double>(x);

	  if (Masking) 
	    {
	      if (   (x1 <= -0.5) || (width  <= x1)
		  || (y1 <= -0.5) || (height <= y1)) 
		{
		  OutputImage[y][x] = 0.0F;
		}
	      else 
		{
		  OutputImage[y][x] = 
		    slipalgo::BSplineInterpolatedValue(Coefficients, 
						       x1, 
						       y1, 
						       SplineDegree);
		}
	    }
	  else 
	    {
	      OutputImage[y][x] = 
		slipalgo::BSplineInterpolatedValue(Coefficients, 
						   x1, 
						   y1, 
						   SplineDegree);
	    }
	}
    }
}

 /* @} */

}//::slipalgo
#endif //SLIP_BSPLINE_INTERPOLATION_HPP
