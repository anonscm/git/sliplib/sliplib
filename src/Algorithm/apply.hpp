/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file apply.hpp
 * 
 * \brief Provides some algorithms to apply C like functions to ranges.
 * 
 */
#ifndef SLIP_APPLY_HPP
#define SLIP_APPLY_HPP

#include <cmath>
#include <iterator>

namespace slip
{

  /** \name Applies an unary C-function with non const parameter*/
  /* @{ */
  /**
   ** \brief Applies a C-function to each element of a range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/16
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first    An InputIterator.
   ** \param last     An InputIterator.
   ** \param result   An OutputIterator.
   ** \param function A one parameter C-function.
   **
   **  Applies the C-function \a function to each element in the input range 
   **  and assigns the results to successive elements of the output range.
   **  Applies the following code
   **  \code
   **   for (;first!=last;++first,++result)
   **   result=(*function)(*first);
   **  \endcode
   **
  
   ** \pre [first,last) must be valid.
   ** \pre The size of the result range must be greater or equal than the
   **      input range one.
   ** \pre The input parameter of the function must have the same value type 
   ** as the input range one.
   ** \pre The output parameter of the function must have the same value type
   **      as the output range one.
   ** \par Example:
   ** \code
   ** //definition of a square function
   ** float sqr(const float x)
   ** {
   **   return x * x;
   ** }
   ** //construction of an Array2d
   ** slip::Array2d<float> M(4,5);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** //apply sqr to each element of M
   ** slip::apply(M.begin(),M.end(),M.begin(),sqr);
   ** \endcode
   */   
  template<typename InputIterator, typename OutputIterator> 
  inline
  void apply(InputIterator first, InputIterator last, 
	     OutputIterator result, 
	     typename std::iterator_traits<OutputIterator>::value_type 
	     (*function)(typename std::iterator_traits<InputIterator>::value_type))
  {
    for (;first!=last;++first,++result)
      *result=(*function)(*first);
  }


  /*!
  **\brief Applies a C-function to each element of a range according to a mask range 
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/16
   ** \version 0.0.2
   ** \since 1.0.0
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param  mask_first An InputIterator.
   ** \param result   An OutputIterator.
   ** \param function A one parameter C-function.
   ** \param  value true value of the mask range. Default is 1.
   ** \pre [first,last) must be valid.
   ** \pre The size of the mask and result ranges must be greater or equal 
   **  than the input range one.
   ** \pre The input parameter of the function must have the same value type 
   ** as the input range one.
   ** \pre The output parameter of the function must have the same value type
   **      as the output range one.
   ** \par Example:
   ** \code
   ** //definition of a square function
   ** float sqr(float x)
   ** {
   **   return x * x;
   ** }
   ** //construction of an Array2d
   ** slip::Array2d<float> M(3,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** int f[]={1,1,1,0,1,1,0,1,0};
   ** slip::Array2d<int>Mask(3,3,f);
   ** //apply sqr to each element of M
   ** slip::apply_mask(M.begin(),M.end(),Mask.begin(),M.begin(),sqr,1);
   ** \endcode
   */   
template<typename InputIterator,typename OutputIterator,typename MaskIterator>
  inline
   void apply_mask(InputIterator first,
                   InputIterator last,
		   MaskIterator mask_first,
		   OutputIterator result,
		   typename std::iterator_traits<OutputIterator>::value_type (*function)(typename std::iterator_traits<InputIterator>::value_type),
                   typename std::iterator_traits<MaskIterator>::value_type value=typename                          std::iterator_traits<MaskIterator>::value_type(1))
{
  for (;first!=last;++first,++result,++mask_first)
     {
        if(*mask_first == value)
         {
           *result=(*function)(*first);
         }
     }
 }
/*!
   ** \brief Applies a C-function to each element of a range according to a predicate
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/16
   ** \version 0.0.2
   ** \since 1.0.0
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param result   An OutputIterator.
   ** \param pred       A predicate.
   ** \param function A one parameter C-function.
   ** \pre [first,last) must be valid.
   ** \pre The size of the result range must be greater or equal than the
   **      input range one.
   ** \pre The input parameter of the function must have the same value type 
   ** as the input range one.
   ** \pre The output parameter of the function must have the same value type
   **      as the output range one.
   ** \par Example:
   ** \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** //definition of a square function
   ** float sqr(const float x)
   ** {
   **   return x * x;
   ** }
   ** \endcode
   ** \code
   ** //construction of an Array2d
   ** slip::Array2d<float> M(4,5);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** //apply sqr to each element of M
   ** slip::apply_if(M.begin(),M.end(),M.begin(),lt5Predicate<float>,sqr);
   ** \endcode
   */   
template<typename InputIterator,typename OutputIterator,typename Predicate>
  inline
   void apply_if(InputIterator first,
                   InputIterator last,
                   OutputIterator result,
                   Predicate pred,
                   typename std::iterator_traits<OutputIterator>::value_type (*function)(typename std::iterator_traits<InputIterator>::value_type))
{    
    for (;first!=last;++first,++result)
     {
        if(pred(*first))
         {
           *result=(*function)(*first);
         }
     }
 }
 /* @} */

  /** \name Apply an unary C-function with a const parameter*/
  /* @{ */
   /**
   ** \brief Applies a C-function to each element of a range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/16
   ** \version 0.0.2
   ** \since 1.0.0
   ** \param first    An InputIterator.
   ** \param last     An InputIterator.
   ** \param result   An OutputIterator.
   ** \param function A one const-parameter C-function.
   **
   **  Applies the C-function \a function to each element in the input range 
   **  and assigns the results to successive elements of the output range.
   **  Applies the following code
   **  \code
   **   for (;first!=last;++first,++result)
   **   result=(*function)(*first);
   **  \endcode
 
   ** \pre [first,last) must be valid.
   ** \pre The size of the result range must be greater or equal than the
   **      input range one.
   ** \pre The input parameter of the function must have the same value type 
   ** as the input range one.
   ** \pre The output parameter of the function must have the same value type
   **      as the output range one.
   ** \par Example:
   ** \code
   ** //definition of a square function
   ** float sqr(const float x)
   ** {
   **   return x * x;
   ** }
   ** //construction of an Array2d
   ** slip::Array2d<float> M(4,5);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** //apply sqr to each element of M
   ** slip::apply(M.begin(),M.end(),M.begin(),sqr);
   ** \endcode
   */   
  template<typename InputIterator, typename OutputIterator> 
  inline
  void apply(InputIterator first, InputIterator last, 
	     OutputIterator result, 
	     typename std::iterator_traits<OutputIterator>::value_type 
	     (*function)(const typename std::iterator_traits<InputIterator>::value_type&))
  {
    for (;first!=last;++first,++result)
      *result=(*function)(*first);
  }



/*!
   ** \brief Applies a C-function to each element of a range according to a mask range 
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/16
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param  mask_first An InputIterator.
   ** \param result   An OutputIterator.
   ** \param function A one parameter C-function.
   ** \param  value true value of the mask range. Default is 1.
   **
   ** \pre [first,last) must be valid.
   ** \pre The size of the result and mask range must be greater or equal 
   **  than the input range one.
   ** \pre The input parameter of the function must have the same value type 
   ** as the input range one.
   ** \pre The output parameter of the function must have the same value type
   **      as the output range one.
   ** \par Example:
   ** \code
   ** //definition of a square function
   ** float sqr(const float x)
   ** {
   **   return x * x;
   ** }
   ** //construction of an Array2d
   ** slip::Array2d<float> M(3,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** int f[]={1,1,1,0,1,1,0,1,0};
   ** slip::Array2d<int>Mask(3,3,f);
   ** //apply sqr to each element of M
   ** slip::apply_mask(M.begin(),M.end(),Mask.begin(),M.begin(),sqr,1);
   ** \endcode
   */   

template<typename InputIterator,typename OutputIterator,typename MaskIterator>
  inline
   void apply_mask(InputIterator first,
		   InputIterator last,
		   MaskIterator mask_first,
		   OutputIterator result,
		   typename std::iterator_traits<OutputIterator>::value_type (*function)(const typename std::iterator_traits<InputIterator>::value_type&),
                      typename std::iterator_traits<MaskIterator>::value_type value=typename
                           std::iterator_traits<MaskIterator>::value_type(1))
{
    
    for (;first!=last;++first,++result,++mask_first)
     {
        if(*mask_first == value)
         {
           *result=(*function)(*first);
         }
     }
 }
  /*!
   ** \brief Applies a C-function to each element of a range according to a predicate
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/16
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param result   An OutputIterator.
   ** \param pred       A predicate.
   ** \param function A one parameter C-function.
   ** \pre [first,last) must be valid.
   ** \pre The size of the result range must be greater or equal than the
   **      input range one.
   ** \pre The input parameter of the function must have the same value type 
   ** as the input range one.
   ** \pre The output parameter of the function must have the same value type
   **      as the output range one.
   ** \par Example:
   ** \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** //definition of a square function
   ** float sqr(const float x)
   ** {
   **   return x * x;
   ** }
   ** \endcode
   ** \code
   ** //construction of an Array2d
   ** slip::Array2d<float> M(4,5);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** //apply sqr to each element of M
   ** slip::apply_if(M.begin(),M.end(),M.begin(),lt5Predicate<float>,sqr);
   ** \endcode
   */   
template<typename InputIterator,typename OutputIterator,typename Predicate>
  inline
   void apply_if(InputIterator first,
                    InputIterator last,
                     OutputIterator result,
                      Predicate pred,
                      typename std::iterator_traits<OutputIterator>::value_type (*function)(const typename std::iterator_traits<InputIterator>::value_type&))
{
    
    for (;first!=last;++first,++result)
     {
        if(pred(*first))
         {
           *result=(*function)(*first);
         }
     }
 }
   /* @} */

  /** \name Applies a binary C-function with two non const parameters*/
  /* @{ */
  /**
   ** \brief Applies a two-parameter C-function to each element of two ranges.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/16
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first    An InputIterator.
   ** \param last     An InputIterator.
   ** \param first2   An InputIterator.
   ** \param result   An OutputIterator.
   ** \param function A two-parameter C-function.
   **
   **  Applies the C-function \a function to each element in the two input 
   **  ranges and assigns the results to successive elements of the output 
   **  range.
   **  Applies the following code
   **  \code
   **  for (;first!=last;++first,++first2,++result)
   **   *result=(*function)(*first,*first2);
   **  \endcode
   **
   ** \pre [first,last) must be valid.
   ** \pre The sizes of the second input and result ranges must be greater 
   ** or equal than the first input range one.
   ** \pre The input parameters of the function must have the same value type 
   ** as the input ranges ones.
   ** \pre The output parameter of the function must have the same value type
   **      as the output range one.
   ** \par Example:
   ** \code
   ** //definition of binary function
   ** float atimesb(const float& a, const float& b)
   ** {
   **  return a * b;
   ** }
   ** //construction of the Array2d
   ** slip::Array2d<float> M(4,5);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** slip::Array2d<float> M2(M);
   ** slip::Array2d<float> M3(M.dim1(),M.dim2());
   ** //apply atimesb to M and M2 and copy the result in M3
   ** slip::apply(M.begin(),M.end(),M2.begin(),M3.begin(),atimesb);
   **
   ** \endcode
   */ 
 template<typename InputIterator1, 
	  typename InputIterator2,
	  typename OutputIterator> 
 inline 
 void apply(InputIterator1 first, InputIterator1 last, 
	     InputIterator2 first2, 
	     OutputIterator result, 
	     typename std::iterator_traits<OutputIterator>::value_type 
	     (*function)(typename std::iterator_traits<InputIterator1>::value_type, 
			 typename std::iterator_traits<InputIterator2>::value_type ))
 {
    for (;first!=last;++first,++first2,++result)
      *result=(*function)(*first,*first2);
  }


/** \brief Applies a C-function to each element of a range according to a mask range 
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/16
   ** \version 0.0.2
   ** \since 1.0.0
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param  mask_first An InputIterator.
   ** \param first2 An InputIterator.
   ** \param result   An OutputIterator.
   ** \param function A two-parameter C-function
   ** \param  value true value of the mask range. Default is 1.
   **
   ** \pre [first,last) must be valid.
   ** \pre The sizes of the second input, mask and result ranges must be 
   ** greater or equal than the first input range one.
   ** \pre The input parameters of the function must have the same value type 
   ** as the input ranges ones.
   ** \pre The output parameter of the function must have the same value type
   **      as the output range one.
   ** \par Example:
   ** \code
   ** //definition of binary function
   ** float atimesb(const float& a, const float& b)
   ** {
   **  return a * b;
   ** }
   ** //construction of the Array2d
   ** slip::Array2d<float> M(3,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** slip::Array2d<float> M2(M);
   ** slip::Array2d<float> M3(M.dim1(),M.dim2());
   ** int f[]={1,1,1,0,1,1,0,1,0};
   ** slip::Array2d<int>Mask(3,3,f);
   ** //apply atimesb to M and M2 and copy the result in M3
   ** slip::apply_mask(M.begin(),M.end(),Mask.begin(),M2.begin(),M3.begin(),atimesb,1);
   **
   ** \endcode
   */ 
template<typename InputIterator1,
	 typename InputIterator2,
	 typename OutputIterator,
	 typename MaskIterator>
  inline
   void apply_mask(InputIterator1 first,
                    InputIterator1 last,
		   MaskIterator mask_first,
		   InputIterator2 first2,
                     OutputIterator result,
                     
                      typename std::iterator_traits<OutputIterator>::value_type 
	     (*function)(typename std::iterator_traits<InputIterator1>::value_type, 
			 typename std::iterator_traits<InputIterator2>::value_type ),
                      typename std::iterator_traits<MaskIterator>::value_type value=typename
                           std::iterator_traits<MaskIterator>::value_type(1))

{
    for (;first!=last;++first,++first2,++result,++mask_first)
      if(*mask_first == value)
         {
          *result=(*function)(*first,*first2);
         }
  }

/**
   ** \brief Applies a C-function to each element of a range according to a predicate
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/16
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param first2 An InputIterator.
   ** \param result   An OutputIterator.
   ** \param pred       A predicate.
   ** \param function A two-parameter C-function
   ** \pre [first,last) must be valid.
   ** \pre The sizes of the second input and result ranges must be greater 
   ** or equal than the first input range one.
   ** \pre The input parameters of the function must have the same value type 
   ** as the input ranges ones.
   ** \pre The output parameter of the function must have the same value type
   **      as the output range one.
   ** \par Example:
   ** \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** //definition of binary function
   ** float atimesb(const float& a, const float& b)
   ** {
   **  return a * b;
   ** }
   ** \endcode
   ** \code
   ** //construction of the Array2d
   ** slip::Array2d<float> M(3,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** slip::Array2d<float> M2(M);
   ** slip::Array2d<float> M3(M.dim1(),M.dim2());
   ** //apply atimesb to M and M2 and copy the result in M3
   ** slip::apply_if(M.begin(),M.end(),M2.begin(),M3.begin(),lt5Predicate<float>,atimesb);
   ** \endcode
   
   */   
template<typename InputIterator1,
	 typename InputIterator2,
	 typename OutputIterator,
	 typename Predicate>
  inline
   void apply_if(InputIterator1 first,
		 InputIterator1 last,
		 InputIterator2 first2,
		 OutputIterator result,
		 Predicate pred,
		 typename std::iterator_traits<OutputIterator>::value_type 
		 (*function)(typename std::iterator_traits<InputIterator1>::value_type, 
			     typename std::iterator_traits<InputIterator2>::value_type ))

{
    for (;first!=last;++first,++first2,++result)
      if(pred(*first))
         {
          *result=(*function)(*first,*first2);
         }
  }
/* @} */

  /** \name Applies a binary C-function with two const parameters*/
  /* @{ */
   /**
   ** \brief Applies a two-const-parameter C-function to each element of 
   **  two ranges.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/16
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first    An InputIterator.
   ** \param last     An InputIterator.
   ** \param first2   An InputIterator.
   ** \param result   An OutputIterator.
   ** \param function A two-parameter C-function.
   **
   **  Applies the C-function \a function to each element in the two input 
   **  ranges and assigns the results to successive elements of the output 
   **  range.
   **  Applies the following code
   **  \code
   **  for (;first!=last;++first,++first2,++result)
   **   *result=(*function)(*first,*first2);
   **  \endcode
   **
   ** \pre [first,last) must be valid.
   ** \pre The sizes of the second input and result ranges must be greater 
   ** or equal than the first input range one.
   ** \pre The input parameters of the function must have the same value type 
   ** as the input ranges one.
   ** \pre The output parameter of the function must have the same value type
   **      as the output range one.
   ** \par Example:
   ** \code
   ** //definition of binary function
   ** float atimesb(const float& a, const float& b)
   ** {
   **  return a * b;
   ** }
   ** //construction of the Array2d
   ** slip::Array2d<float> M(4,5);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** slip::Array2d<float> M2(M);
   ** slip::Array2d<float> M3(M.dim1(),M.dim2());
   ** //apply atimesb to M and M2 and copy the result in M3
   ** slip::apply(M.begin(),M.end(),M2.begin(),M3.begin(),atimesb);
   **
   ** \endcode
   */ 
  template<typename InputIterator1,
	   typename InputIterator2,
	   typename OutputIterator> 
  inline
  void apply(InputIterator1 first, InputIterator1 last, 
	     InputIterator2 first2,
	     OutputIterator result, 
	     typename std::iterator_traits<OutputIterator>::value_type 
	     (*function)(const typename std::iterator_traits<InputIterator1>::value_type&, const typename std::iterator_traits<InputIterator2>::value_type& ))
  {
    for (;first!=last;++first,++first2,++result)
      *result=(*function)(*first,*first2);
  }


/** \brief Applies a C-function to each element of a range according to a mask range 
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/16
   ** \version 0.0.2
   ** \since 1.0.0
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param  mask_first An InputIterator.
   ** \param first2 An InputIterator.
   ** \param result   An OutputIterator.
   ** \param function A two-parameter C-function
   ** \param  value true value of the mask range. Default is 1.
   **
   ** \pre [first,last) must be valid.
   ** \pre The sizes of the second input, mask and result ranges must be 
   ** greater or equal than the first input range one.
   ** \pre The input parameters of the function must have the same value type 
   ** as the input ranges ones.
   ** \pre The output parameter of the function must have the same value type
   **      as the output range one.
   ** \par Example:
   ** \code
   ** //definition of binary function
   ** float atimesb(const float& a, const float& b)
   ** {
   **  return a * b;
   ** }
   ** //construction of the Array2d
   ** slip::Array2d<float> M(3,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** slip::Array2d<float> M2(M);
   ** slip::Array2d<float> M3(M.dim1(),M.dim2());
   ** int f[]={1,1,1,0,1,1,0,1,0};
   ** slip::Array2d<int>Mask(3,3,f);
   ** //apply atimesb to M and M2 and copy the result in M3
   ** slip::apply_mask(M.begin(),M.end(),Mask.begin(),M2.begin(),M3.begin(),atimesb,1);
   **
   ** \endcode
   */ 
template<typename InputIterator1,
	 typename InputIterator2,
	 typename OutputIterator,
	 typename MaskIterator>
  inline
   void apply_mask(InputIterator1 first,
		   InputIterator1 last,
		   MaskIterator mask_first,
		   InputIterator2 first2,
		   OutputIterator result,
                     
		   typename std::iterator_traits<OutputIterator>::value_type 
	     (*function)(const typename std::iterator_traits<InputIterator1>::value_type&, const typename std::iterator_traits<InputIterator2>::value_type& ),
                      typename std::iterator_traits<MaskIterator>::value_type value=typename
                           std::iterator_traits<MaskIterator>::value_type(1))

{
    for (;first!=last;++first,++first2,++result,++mask_first)
      if(*mask_first == value)
         {
          *result=(*function)(*first,*first2);
         }
  }

  /*!
   ** \brief Applies a C-function to each element of a range according to a predicate
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/16
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param first2 An InputIterator.
   ** \param result   An OutputIterator.
   ** \param pred       A predicate.
   ** \param function A two-parameter C-function
   ** \pre [first,last) must be valid.
   ** \pre The sizes of the second input and result ranges must be greater 
   ** or equal than the first input range one.
   ** \pre The input parameters of the function must have the same value type 
   ** as the input ranges ones.
   ** \pre The output parameter of the function must have the same value type
   **      as the output range one.
   ** \par Example:
   ** \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** //definition of binary function
   ** float atimesb(const float& a, const float& b)
   ** {
   **  return a * b;
   ** }
   ** \endcode
   ** \code
   ** //construction of the Array2d
   ** slip::Array2d<float> M(3,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** slip::Array2d<float> M2(M);
   ** slip::Array2d<float> M3(M.dim1(),M.dim2());
   ** //apply atimesb to M and M2 and copy the result in M3
   ** slip::apply_if(M.begin(),M.end(),M2.begin(),M3.begin(),lt5Predicate<float>,atimesb);
   ** \endcode
*/
template<typename InputIterator1,
	 typename InputIterator2,
	 typename OutputIterator,typename Predicate>
  inline
   void apply_if(InputIterator1 first,
                    InputIterator1 last,
                    InputIterator2 first2,
                    OutputIterator result,
                    Predicate pred,
                    typename std::iterator_traits<OutputIterator>::value_type 
	     (*function)(const typename std::iterator_traits<InputIterator1>::value_type&, const typename std::iterator_traits<InputIterator2>::value_type& ))

{
    for (;first!=last;++first,++first2,++result)
      if(pred(*first))
         {
          *result=(*function)(*first,*first2);
         }
  }
/* @} */

}//slip::


#endif //SLIP_APPLY_HPP
