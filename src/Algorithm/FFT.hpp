/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file FFT.hpp
 * 
 * \brief Provides some FFT algorithms.
 * \since 1.0.0
 */

#ifndef FFT_HPP
#define FFT_HPP

#include <iostream>
#include <complex>
#include <cmath>
#include <iterator>
#include <algorithm>
#include "Array.hpp"
#include "macros.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "statistics.hpp"
#include "FFTPACK.hpp"
#include <vector>

#ifdef HAVE_FFTW
#include <fftw3.h>
#endif





namespace slip
{

  ////////////////////////////////// FFT Tools functions/////////////////////////////////////////////////

  /**
   ** \name FFT Tools Functions
   */
  /*@{*/
  
  /**
   ** \brief bitReverse function used in the Cooley-Tukey FFT algorithm
   ** \param x Integer to reverse
   ** \param log2n The log2 of the dimension 
   ** \return the bit reversed integer
   **/
  unsigned int inline bitReverse(unsigned int x,
				 const int log2n)
  {
    int n = 0;
    for(int i = 0; i < log2n; i++)
      {
	n <<= 1;
	n |= (x & 1);
	x >>= 1;
      }
    return n;
  }

  /**
   ** \brief Calculates the log2 of an integer
   ** \param x Unsigned integer to reverse
   ** \return log2(x)
   ** \pre x != 0
   **/
  unsigned int inline log2(unsigned int x)
  {
    assert (x!= 0);
    unsigned int n = 0;
    while(x > 1)
      {
	x = x >> 1;
	n++;
      }
    return n;
  }
  
  /**
   ** \brief Performs a shift of a container, for use with the fft and ifft 
   **        functions, in order to move the frequency 0 to the center of 
   **        the container.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2007/12/07
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first A ForwardIterator.
   ** \param last  A ForwardIterator.
   ** \n
   **
   ** \image html fftshift.jpg "FFT shifting convention"
   ** \image latex fftshift.eps "FFT shifting convention" width=5cm
   ** \internal created in 2006/12/12, changed in 2007/12/07 (add +1 after the cardinal)
   */
  template<class ForwardIterator>
  inline
  void fftshift(ForwardIterator first, 
		ForwardIterator last)
  {
    ForwardIterator middle = first + ((slip::cardinal<std::size_t>(first,last) + 1) / 2);
    std::rotate(first,middle,last);
  }
  
  /**
   ** \brief Performs a inverse shift of a container, for use with ifft 
   **        functions.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2024/12/13
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param first A ForwardIterator.
   ** \param last  A ForwardIterator.
   ** \par Example:
   ** \code
   ** const std::size_t N_odd = 7;
   ** const std::size_t N_even = 8;
   ** slip::Array<double> Aodd(N_odd);
   ** slip::iota(Aodd.begin(),Aodd.end(),1.0);
   ** slip::Array<double> Aeven(N_even);
   ** slip::iota(Aeven.begin(),Aeven.end(),1.0);
   ** std::cout<<"Aodd = \n"<<Aodd<<std::endl;
   ** std::cout<<"Aeven = \n"<<Aeven<<std::endl;
   ** slip::fftshift(Aodd.begin(),Aodd.end());
   ** slip::fftshift(Aeven.begin(),Aeven.end());
   ** std::cout<<"Aodd = \n"<<Aodd<<std::endl;
   ** std::cout<<"Aeven = \n"<<Aeven<<std::endl;
   ** slip::iota(Aodd.begin(),Aodd.end(),1.0);
   ** slip::iota(Aeven.begin(),Aeven.end(),1.0);
   ** slip::ifftshift(Aodd.begin(),Aodd.end());
   ** slip::ifftshift(Aeven.begin(),Aeven.end());
   ** std::cout<<"Aodd = \n"<<Aodd<<std::endl;
   ** std::cout<<"Aeven = \n"<<Aeven<<std::endl;
   ** \endcode
   */
  template<class ForwardIterator>
  inline
  void ifftshift(ForwardIterator first, 
		ForwardIterator last)
  {
    ForwardIterator middle = first + ((slip::cardinal<std::size_t>(first,last)) / 2);
    std::rotate(first,middle,last);
  }

  
  /**
   ** \brief Performs a shift of a 2d container, for use with the fft2d and ifft2d 
   **        functions, in order to move the frequency 0 to the center of 
   **        the container.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2007/12/07
   ** \version 0.0.1
   ** \since 1.0.0
   ** \param upper_left A ForwardIterator2D.
   ** \param bottom_right  A ForwardIterator2D.
   ** \n
   **
   ** \image html fftshift2d.jpg "FFT shifting convention"
   ** \image latex fftshift2d.eps "FFT shifting convention" width=8cm
   **
   */
  template<class ForwardIterator2D>
  inline
  void fftshift2d(ForwardIterator2D upper_left, 
		  ForwardIterator2D bottom_right)
  {
    //recuperer la taille du container
    typename ForwardIterator2D::difference_type size2d = bottom_right - upper_left;
    
    std::size_t nb_lig = size2d[0];
    std::size_t nb_col = size2d[1];
    
    
    //centered
    for(size_t i = 0; i < nb_lig; ++i)
      {
	slip::fftshift(upper_left.row_begin(i),upper_left.row_end(i));
      } 
    for(size_t j = 0; j < nb_col; ++j)
      {
	slip::fftshift(upper_left.col_begin(j),upper_left.col_end(j));
      }
  }


 /**
   ** \brief Performs a shift of a 2d container, for use with the ifft2d 
   **        functions.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/12/06
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left A ForwardIterator2D.
   ** \param bottom_right  A ForwardIterator2D.
   ** \par Example:
   ** \code
   ** const std::size_t N_odd = 7;
   ** const std::size_t N_even = 8;
   ** slip::GrayscaleImage<double> M_odd(N_odd,N_odd);
   ** slip::iota(M_odd.begin(),M_odd.end(),0.0);
   ** std::cout<<"M_odd = \n"<<M_odd<<std::endl;
   ** slip::ifftshift2d(M_odd.upper_left(),M_odd.bottom_right());
   ** std::cout<<"shifted M_odd = \n"<<M_odd<<std::endl;
   ** slip::GrayscaleImage<double> M_even(N_even,N_even);
   ** slip::iota(M_even.begin(),M_even.end(),0.0);
   ** std::cout<<"M_even = \n"<<M_even<<std::endl;
   ** slip::ifftshift2d(M_even.upper_left(),M_even.bottom_right());
   ** std::cout<<"shifted M_even = \n"<<M_even<<std::endl;
   ** \endcode
   */
  template<class ForwardIterator2D>
  inline
  void ifftshift2d(ForwardIterator2D upper_left, 
		   ForwardIterator2D bottom_right)
  {
    //recuperer la taille du container
    typename ForwardIterator2D::difference_type size2d = bottom_right - upper_left;
    
    const std::size_t nb_lig = size2d[0];
    const std::size_t nb_col = size2d[1];
    
    
    //centered
    for(size_t i = 0; i < nb_lig; ++i)
      {
	slip::ifftshift(upper_left.row_begin(i),upper_left.row_end(i));
      } 
    for(size_t j = 0; j < nb_col; ++j)
      {
	slip::ifftshift(upper_left.col_begin(j),upper_left.col_end(j));
      }
  }

  
  /**
   ** \brief Performs a shift of a 3d container, for use with the fft3d and ifft3d 
   **        functions, in order to move the frequency 0 to the center of 
   **        the container.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2007/12/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param front_upper_left A ForwardIterator3D.
   ** \param back_bottom_right  A ForwardIterator3D.
   ** \n
   ** 
   ** \image html fftshift3d.jpg "FFT shifting convention"
   ** \image latex fftshift3d.eps "FFT shifting convention" width=15cm
   **
   */
  template<class ForwardIterator3D>
  inline
  void fftshift3d(ForwardIterator3D front_upper_left, 
		  ForwardIterator3D back_bottom_right)
  {
    //recuperer la taille du container
    typename ForwardIterator3D::difference_type size3d = back_bottom_right - front_upper_left;
    
    std::size_t nb_sli = size3d[0];
    std::size_t nb_lig = size3d[1];
    std::size_t nb_col = size3d[2];

    for(size_t k = 0; k < nb_sli; ++k)
      for(size_t i = 0; i < nb_lig; ++i)
	{
	  slip::fftshift(front_upper_left.row_begin(k,i),front_upper_left.row_end(k,i));
	} 
    for(size_t k = 0; k < nb_sli; ++k)
      for(size_t j = 0; j < nb_col; ++j)
	{
	  slip::fftshift(front_upper_left.col_begin(k,j),front_upper_left.col_end(k,j));
	}
    for(size_t i = 0; i < nb_lig; ++i)
      for(size_t j = 0; j < nb_col; ++j)
	{
	   slip::fftshift(front_upper_left.slice_begin(i,j),front_upper_left.slice_end(i,j));
	}   
  }


   /**
   ** \brief Performs a shift of a 3d container, for use with the ifft3d 
   **        function.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/12/06
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param front_upper_left A ForwardIterator3D.
   ** \param back_bottom_right  A ForwardIterator3D.
   ** \par Example:
   ** \code
   ** const std::size_t N_odd = 7;
   ** const std::size_t N_even = 8;
   ** slip::Volume<double> M3_odd(N_odd,N_odd,N_odd);
   ** slip::iota(M3_odd.begin(),M3_odd.end(),0.0);
   ** std::cout<<"M3_odd = \n"<<M3_odd<<std::endl;
   ** slip::ifftshift3d(M3_odd.front_upper_left(),M3_odd.back_bottom_right());
   ** std::cout<<"shifted M3_odd = \n"<<M3_odd<<std::endl;
   ** slip::Volume<double> M3_even(N_even,N_even,N_even);
   ** slip::iota(M3_even.begin(),M3_even.end(),0.0);
   ** std::cout<<"M3_even = \n"<<M3_even<<std::endl;
   ** slip::ifftshift3d(M3_even.front_upper_left(),M3_even.back_bottom_right());
   ** std::cout<<"shifted M3_even = \n"<<M3_even<<std::endl;
   ** \encode
   **
   */
  template<class ForwardIterator3D>
  inline
  void ifftshift3d(ForwardIterator3D front_upper_left, 
		   ForwardIterator3D back_bottom_right)
  {
    //recuperer la taille du container
    typename ForwardIterator3D::difference_type size3d = back_bottom_right - front_upper_left;
    
    const std::size_t nb_sli = size3d[0];
    const std::size_t nb_lig = size3d[1];
    const std::size_t nb_col = size3d[2];

    for(size_t k = 0; k < nb_sli; ++k)
      for(size_t i = 0; i < nb_lig; ++i)
	{
	  slip::ifftshift(front_upper_left.row_begin(k,i),front_upper_left.row_end(k,i));
	}
    
    for(size_t k = 0; k < nb_sli; ++k)
      for(size_t j = 0; j < nb_col; ++j)
	{
	  slip::ifftshift(front_upper_left.col_begin(k,j),front_upper_left.col_end(k,j));
	}
    for(size_t i = 0; i < nb_lig; ++i)
      for(size_t j = 0; j < nb_col; ++j)
	{
	   slip::ifftshift(front_upper_left.slice_begin(i,j),front_upper_left.slice_end(i,j));
	}   
  }
  

  
  /*@} End FFT Tools Functions */
  


//////////////////////////////////  FFT Radix2 (Cooley-Tukey) algorithms /////////////////////////////////////////////////
  
  /**
   ** \name FFT Radix2 (Cooley-Tukey) algorithms.
   */
  /*@{*/

  /**
   ** \brief Computes the Cooley-Tukey (radix2) fft of a container.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/10/23
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param a A BidirectionalIterator to iterate throw the input data
   ** \param b A BidirectionalIterator to iterate throw the output data
   ** \param log2n Corresponds to log2(size(a)).
   **
   ** \pre size(a) == size(b).
   ** \par Example:
   ** \code
   **  typedef std::complex<double> cx;
   **  cx a[] = { cx(0,0), cx(1,1), cx(3,3), cx(4,4), cx(4,4), cx(3,3),cx(1,1),cx(0,0)};
   **  cx b[8];
   **  slip::radix2_fft(a,b,3);
   **  for(int i = 0; i < 8; ++i)
   **      std::cout<<b[i]<<" ";
   **  std::cout<<std::endl;
   ** \endcode   
   */
  template<class InputIter_T,class Iter_T>
  inline
  void radix2_fft(InputIter_T a, Iter_T b, const int log2n)
  {
    typedef typename std::iterator_traits<Iter_T>::value_type complex;
    typedef typename complex::value_type real;

   //  typedef typename std::complex<typename std::iterator_traits<Iter_T>::value_type> complex;
    real pi = slip::constants<real>::pi();
    const complex J(0,1);
    unsigned int n = 1 << log2n;
    for(unsigned int i = 0; i < n; ++i)
      {
	b[bitReverse(i,log2n)] = a[i];
      }
    for(int s = 1; s <= log2n; ++s)
      {
	unsigned int m = 1 << s;
	unsigned int m2 = m >> 1;
	complex w(1,0);
	complex wm = exp(-J*(pi/m2));
	for(unsigned int j = 0; j < m2; ++j)
	  {
	    for(unsigned int k = j ; k < n; k += m)
	      {
		complex t = w * b[k + m2];
		complex u = b[k];
		b[k] = u + t;
		b[k + m2] = u - t;
	      }
	    w *= wm;
	  }
      }

  }

 /**
  ** \brief Computes the inverse Cooley-Tukey (radix2) fft of a container.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2006/10/23
  ** \since 1.0.0
  ** \version 0.0.2
  ** \param a A BidirectionalIterator to iterate throw the input data
  ** \param b A BidirectionalIterator to iterate throw the output data
  ** \param log2n Corresponds to log2(size(a)).
  **
  ** \pre size(a) == size(b).
  ** \par Example:
  ** \link slip::radix2_fft(InputIter_T a, Iter_T b, const int log2n)
  **  ...
  ** \endlink
  ** \code
  **  cx b2[8];
  **  slip::radix2_ifft(b,b2,3);
  **  for(int i = 0; i < 8; ++i)
  **    std::cout<<b2[i]<<" ";
  **  std::cout<<std::endl<<std::endl;
  ** \endcode
  */
  template<class InputIter_T,class Iter_T>
  inline
  void radix2_ifft(InputIter_T a, Iter_T b, const int log2n)
  {
    typedef typename std::iterator_traits<InputIter_T>::value_type complex;
    typedef typename complex::value_type real;
    real pi = slip::constants<real>::pi(); 
    const complex J(0,1);
    unsigned int n = 1 << log2n;
    real coef = 1.0 / (real)n;
    for(unsigned int i = 0; i < n; ++i)
      {
	b[bitReverse(i,log2n)] = a[i];
      }
    for(int s = 1; s <= log2n; ++s)
      {
	unsigned int m = 1 << s;
	unsigned int m2 = m >> 1;
	complex w(1,0);
	complex wm = exp(J*(pi/m2));
	for(unsigned int j = 0; j < m2; ++j)
	  {
	    for(unsigned int k = j ; k < n; k += m)
	      {
		complex t = w * b[k + m2];
		complex u = b[k];
		b[k]      = (u + t);
		b[k + m2] = (u - t);
	      }
	    w *= wm;
	  }
      }
    for(unsigned int i = 0; i < n; ++i)
      {
	b[i] = coef * b[i];
      }

  }


  /**
   ** \brief Computes the Cooley-Tukey (radix2) real_fft of a container.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/10/23
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param a A BidirectionalIterator to iterate throw the input data
   ** \param b A BidirectionalIterator to iterate throw the output data
   ** \param log2n Corresponds to log2(size(a)).
   **
   ** \pre size(a) == size(b).
   **
   ** \link slip::radix2_fft(InputIter_T a, Iter_T b, const int log2n)
   **  Code.
   ** \endlink
   */
  template<class InputIter_T, class Iter_T>
  inline
  void real_radix2_fft(InputIter_T a, Iter_T b, const int log2n)
  {
    slip::radix2_fft(a,b,log2n);
  }


 /**
   ** \brief Computes the radix 2 fft2d of a container with 1d iterator (compatible with simple pointers).
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2007/11/19
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param begin1 The begin iterator of the first container
   ** \param begin2 The begin iterator of the second container
   ** \param nb_lig The number of rows
   ** \param nb_col The number of columns
   **
   ** \pre The data have to be 2D datas registered linearly (the first row just before the second row and so on...)
   ** \pre Both container must have the same dimensions
   ** \pre Dimensions must be power of 2
   ** \par Example:
   ** \code
   **  slip::GrayscaleImage<double> I;
   **  I.read("lena.gif");
   **  slip::GrayscaleImage<double> Norm(I.dim1(),I.dim2());
   **  int nblig = I.dim1();
   **  int nbcol = I.dim2();
   **  std::complex<double>* I2 = new std::complex<double>[nblig * nbcol];
   **  slip::GrayscaleImage<double>::row_iterator it_row_I_begin = I.row_begin(0);
   **  //fft1d2d
   **  slip::radix2_fft1d2d(it_row_I_begin,I2,nblig,nbcol);
   **  //log(magnitude)
   **  for(size_t i = 0; i < I.dim1(); ++i)
   **	 for(size_t j = 0; j < I.dim2(); ++j)
   **	    Norm[i][j] = std::log(1.0+std::abs(I2[nbcol * i + j]));
   **	    
   **  slip::fftshift2d(Norm.upper_left(),Norm.bottom_right());
   **  slip::change_dynamic_01(Norm.begin(),Norm.end(),Norm.begin(),slip::AFFINE_FUNCTION);
   **  Norm.write("lena_fft.gif");
   ** \endcode
   */
  template<typename InputIterator1d,typename OuputIterator1d>
  inline
  void radix2_fft1d2d(InputIterator1d begin1,OuputIterator1d begin2,std::size_t nb_lig, std::size_t nb_col)
  {
    std::size_t n1 = slip::log2(nb_lig);
    std::size_t n2 = slip::log2(nb_col);
    typename std::iterator_traits<InputIterator1d>::difference_type step_horiz1 = 1; 
    typename std::iterator_traits<OuputIterator1d>::difference_type step_horiz2 = 1; 
    typename std::iterator_traits<OuputIterator1d>::difference_type step_vert2 = nb_col; 

    //fft2d
    for(size_t i = 0; i < nb_lig; ++i)
      {
	typename slip::stride_iterator<typename std::iterator_traits<InputIterator1d>::pointer> it_row1(&begin1[i*nb_col],step_horiz1);
	typename slip::stride_iterator<typename std::iterator_traits<OuputIterator1d>::pointer> it_row2(&begin2[i*nb_col],step_horiz2);
	slip::radix2_fft(it_row1,it_row2,n2);
      } 
     slip::Vector<std::complex<double> > Vtmp(nb_lig);
     for(size_t j = 0; j < nb_col; ++j)
       {
 	typename slip::stride_iterator<typename std::iterator_traits<OuputIterator1d>::pointer> it_col2(&begin2[j],step_vert2);
  	slip::radix2_fft(it_col2,Vtmp.begin(),n1);
  	std::copy(Vtmp.begin(),Vtmp.end(),it_col2);
       }
  }
 
  /**
   ** \brief Computes the inverse radix 2 fft2d of a container with 1d iterators (compatible with simple pointers).
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2007/11/19
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param begin1 The begin iterator of the first container
   ** \param begin2 The begin iterator of the second container
   ** \param nb_lig The number of rows
   ** \param nb_col The number of columns
   **
   ** \pre The data have to be 2D datas registered linearly (the first row just before the second row and so on...)
   ** \pre Both container must have the same dimensions
   ** \pre Dimensions must be power of 2
   ** \par Example:
   ** \link slip::radix2_fft1d2d(InputIterator1d begin1,OuputIterator1d begin2,std::size_t nb_lig, std::size_t nb_col)
   **  ...
   ** \endlink
   ** \code
   **  std::complex<double>* I3 = new std::complex<double>[nblig*nbcol];
   **  //ifft1d2d
   **  slip::radix2_ifft1d2d(I2,I3,nblig,nbcol);
   **  for(size_t i = 0; i < I.dim1(); ++i)
   **	  for(size_t j = 0; j < I.dim2(); ++j)
   **	    {
   **	      Norm[i][j] = std::abs(I3[nbcol * i + j]);
   **	    }
   **      slip::change_dynamic_01(Norm.begin(),Norm.end(),Norm.begin(),slip::AFFINE_FUNCTION);
   **  Norm.write("lena_ifft.gif");
   ** \endcode
   */
  template<typename InputIterator1d,typename OuputIterator1d>
  inline
  void radix2_ifft1d2d(InputIterator1d begin1,OuputIterator1d begin2,std::size_t nb_lig, std::size_t nb_col)
  {
    std::size_t n1 = slip::log2(nb_lig);
    std::size_t n2 = slip::log2(nb_col);
    typename std::iterator_traits<InputIterator1d>::difference_type step_horiz1 = 1; 
    typename std::iterator_traits<OuputIterator1d>::difference_type step_horiz2 = 1; 
    typename std::iterator_traits<OuputIterator1d>::difference_type step_vert2 = nb_col; 
    //ifft2d
    for(size_t i = 0; i < nb_lig; ++i)
      {
	typename slip::stride_iterator<typename std::iterator_traits<InputIterator1d>::pointer> it_row1(&begin1[i*nb_col],step_horiz1);
	typename slip::stride_iterator<typename std::iterator_traits<OuputIterator1d>::pointer> it_row2(&begin2[i*nb_col],step_horiz2);
	slip::radix2_ifft(it_row1,it_row2,n2);
      } 
    slip::Vector<std::complex<double> > Vtmp(nb_lig);
    for(size_t j = 0; j < nb_col; ++j)
      {
	typename slip::stride_iterator<typename std::iterator_traits<OuputIterator1d>::pointer> it_col2(&begin2[j],step_vert2);
 	slip::radix2_ifft(it_col2,Vtmp.begin(),n1);
 	std::copy(Vtmp.begin(),Vtmp.end(),it_col2);
      }
  }

  /**
   ** \brief Computes the radix2 (Cooley-Tukey) fft2d of a container with 2d iterators.
   ** \since 1.0.0
   ** \param in_upper_left
   ** \param in_bottom_right
   ** \param out_upper_left
   **
   ** \pre dimensions must be power of 2
   ** \pre the 2d range must have the same sizes
   ** \par Example:
   ** \code
   **  slip::GrayscaleImage<double> I;
   **  I.read("lena.gif");
   **  slip::GrayscaleImage<double> Norm(I.dim1(),I.dim2());
   **  slip::Matrix<std::complex<double> > I2(I.dim1(),I.dim2());
   **  slip::radix2_fft2d(I.upper_left(),I.bottom_right(),I2.upper_left());
   **
   **  //log(magnitude)
   **  for(size_t i = 0; i < I2.dim1(); ++i)
   **     for(size_t j = 0; j < I2.dim2(); ++j)
   **        Norm[i][j] = std::log(1.0+std::abs(I2[i][j]));
   **
   **  slip::change_dynamic_01(Norm.begin(),Norm.end(),Norm.begin(),slip::AFFINE_FUNCTION);
   **  Norm.write("fft.gif");
   ** \endcode
   */
  template<typename InputBidirectionalIterator2d,
	   typename OutputBidirectionalIterator2d>
  inline
  void radix2_fft2d(InputBidirectionalIterator2d in_upper_left,
		    InputBidirectionalIterator2d in_bottom_right,
		    OutputBidirectionalIterator2d out_upper_left)
  {
    //recuperer la taille du container
    typename InputBidirectionalIterator2d::difference_type size2d = in_bottom_right - in_upper_left;
    
    std::size_t nb_lig = size2d[0];
    std::size_t nb_col = size2d[1];
       
    // std::size_t n1 = std::size_t(std::log(nb_lig)/std::log(2) + 0.5);
    // std::size_t n2 = std::size_t(std::log(nb_col)/std::log(2) + 0.5);
    std::size_t n1 = slip::log2(nb_lig);
    std::size_t n2 = slip::log2(nb_col);

    //fft2d
    for(size_t i = 0; i < nb_lig; ++i)
      {
	slip::radix2_fft(in_upper_left.row_begin(i),out_upper_left.row_begin(i),n2);
      } 
    
    slip::Vector<std::complex<double> > Vtmp(nb_lig);
    for(size_t j = 0; j < nb_col; ++j)
      {
	slip::radix2_fft(out_upper_left.col_begin(j),Vtmp.begin(),n1);
	std::copy(Vtmp.begin(),Vtmp.end(),out_upper_left.col_begin(j));
      }
    
    
    //centered fft2d
    //  slip::fftshift2d(out_upper_left,out_upper_left+size2d);
  } 

  /**
   ** \brief Computes the radix2 ifft2d of a container with 2d iterators.
   ** \since 1.0.0
   ** \param in_upper_left
   ** \param in_bottom_right
   ** \param out_upper_left
   **
   ** \pre dimensions must be power of 2
   ** \pre the 2d range must have the same sizes
   ** \par Example:
   ** \link slip::radix2_fft2d(InputBidirectionalIterator2d in_upper_left, InputBidirectionalIterator2d in_bottom_right, OutputBidirectionalIterator2d out_upper_left)
   **  ...
   ** \endlink
   **
   ** \code
   **  slip::Matrix<std::complex<double> > I3(I.dim1(),I.dim2());
   **  slip::radix2_ifft2d(I2.upper_left(),I2.bottom_right(),I3.upper_left());
   **  for(size_t i = 0; i < I2.dim1(); ++i)
   **   for(size_t j = 0; j < I2.dim2(); ++j)
   **     Norm[i][j] = std::abs(I3[i][j]);
   **  slip::change_dynamic_01(Norm.begin(),Norm.end(),Norm.begin(),slip::AFFINE_FUNCTION);
   **  Norm.write("ifft.gif");
   ** \endcode
   **
   */
  template<typename InputBidirectionalIterator2d,
	   typename OutputBidirectionalIterator2d>
  inline
  void radix2_ifft2d(InputBidirectionalIterator2d in_upper_left,
		     InputBidirectionalIterator2d in_bottom_right,
		     OutputBidirectionalIterator2d out_upper_left)
  {
    //recuperer la taille du container
    typename InputBidirectionalIterator2d::difference_type size2d = in_bottom_right - in_upper_left;
    
    std::size_t nb_lig = size2d[0];
    std::size_t nb_col = size2d[1];
    
    
    //   std::size_t n1 = size_t(std::log(nb_lig)/std::log(2) + 0.5);
    //   std::size_t n2 = size_t(std::log(nb_col)/std::log(2) + 0.5);
    std::size_t n1 = slip::log2(nb_lig);
    std::size_t n2 = slip::log2(nb_col);
    //centered fft2d
    // slip::fftshift2d(in_upper_left,in_bottom_right);

    //fft2d
    for(size_t i = 0; i < nb_lig; ++i)
      {
	slip::radix2_ifft(in_upper_left.row_begin(i),out_upper_left.row_begin(i),n2);
      } 
    
    slip::Vector<std::complex<double> > Vtmp(nb_lig);
    for(size_t j = 0; j < nb_col; ++j)
      {
	slip::radix2_ifft(out_upper_left.col_begin(j),Vtmp.begin(),n1);
	std::copy(Vtmp.begin(),Vtmp.end(),out_upper_left.col_begin(j));
      }
  } 

  /**
   ** \brief Computes the radix2 fft2d of a container.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : conceptor
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2007/11/21
   ** \since 1.0.0
   ** \version 0.0.3
   ** \param datain The input datas
   ** \param dataout The output datas
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre dimensions must be power of 2
   ** \pre datain and dataout must have cols and rows iterator
   ** \par Example:
   ** \code
   **  slip::GrayscaleImage<double> I;
   **  I.read("lena.gif");
   **  slip::GrayscaleImage<double> Norm(I.dim1(),I.dim2());
   **  slip::Matrix<std::complex<double> > I2(I.dim1(),I.dim2());
   **  slip::radix2_fft2d(I,I2);
   **
   **  //log(magnitude)
   **  for(size_t i = 0; i < I2.dim1(); ++i)
   **     for(size_t j = 0; j < I2.dim2(); ++j)
   **        Norm[i][j] = std::log(1.0+std::abs(I2[i][j]));
   **
   **  slip::change_dynamic_01(Norm.begin(),Norm.end(),Norm.begin(),slip::AFFINE_FUNCTION);
   **  Norm.write("fft.gif");
   ** \endcode
   **
   */
  template<typename Matrix1, typename Matrix2>
  inline
  void radix2_fft2d(Matrix1 &datain, Matrix2 &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    slip::radix2_fft2d(datain.upper_left(),datain.bottom_right(),dataout.upper_left());
  }

 /**
   ** \brief Computes the radix2 fft2d of a container.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : conceptor
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2007/11/21
   ** \since 1.0.0
   ** \version 0.0.3
   ** \param datain The input datas
   ** \param dataout The output datas
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre dimensions must be power of 2
   ** \pre datain and dataout must have cols and rows iterator
   ** \par Example:
   ** \link slip::radix2_fft2d(Matrix1 &datain, Matrix2 &dataout)
   **  ...
   ** \endlink
   **
   ** \code
   **  slip::Matrix<std::complex<double> > I3(I.dim1(),I.dim2());
   **  slip::radix2_ifft2d(I2,I3);
   **  for(size_t i = 0; i < I2.dim1(); ++i)
   **   for(size_t j = 0; j < I2.dim2(); ++j)
   **     Norm[i][j] = std::abs(I3[i][j]);
   **  slip::change_dynamic_01(Norm.begin(),Norm.end(),Norm.begin(),slip::AFFINE_FUNCTION);
   **  Norm.write("ifft.gif");
   ** \endcode
   **
   */
  template<typename Matrix1, typename Matrix2>
  inline
  void radix2_ifft2d(Matrix1 &datain, Matrix2 &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    slip::radix2_ifft2d(datain.upper_left(),datain.bottom_right(),dataout.upper_left());
  }

  /*@} End FFT Radix2 (Cooley-Tukey) algorithms. */

  ////////////////////////////////// FFT Split-Radix algorithms (from FFTPACK.hpp) /////////////////////////////////////////////////  
  
  /**
   ** \name FFT Split-Radix algorithms (from FFTPACK.hpp).
   */
  /*@{*/


  /**
   ** \brief Computes the real split radix fft of a container (from FFTPACK.hpp).
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2007/11/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \pre size(b) >= size(a).
   ** \pre the input data have to be real and out put data have to be std::complex
   ** \par Example:
   ** \code
   **  template <typename T>
   **  T aleat10(void)
   **  {
   **   return 10.0 * (rand()/static_cast<T>(RAND_MAX));
   **  }
   **  int main(int argc, char **argv)
   **  {
   **   typedef double real;
   **   typedef std::complex<real> cx;
   **   slip::Array<real> I(N,real(0));
   **   std::generate(I.begin(),I.end(),aleat10<real>);
   **   slip::Array<cx> b(N,cx(0));
   **   slip::real_split_radix_fft(I.begin(),I.end(),b.begin());
   **   ...
   ** \endcode
   */
  template<class InputIter,class OutputIter>
  inline
  void real_split_radix_fft(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    typedef typename std::iterator_traits<InputIter>::value_type real;
    typedef typename std::iterator_traits<OutputIter>::value_type complex;
    const complex J(0,1);
    size_t length = in_end - in_begin;
    assert(length > 1);
    //data copy
    slip::Array<real> in(2*length,0.0);
    typename slip::stride_iterator<typename slip::Array<real>::iterator> itst_in_begin(in.begin(),2);
    std::copy(in_begin,in_end,itst_in_begin);
       //FFT initialization
    slip::Array<real> r(4*length+15,0.0);
    slip::Array<size_t> ifac(4*length+15,(size_t)0);
    size_t double_length = 2*length;
    slip::rffti(&double_length,r.begin(),ifac.begin());

    //FFT
    slip::rfftf(&double_length,in.begin(),r.begin(),ifac.begin());
  
    //Output copy in complex type
    out_begin[0] = in[0];
    for(size_t i = 1; i < length; i++)
      {
	out_begin[i] = in[2*i-1] + J * in[2*i];
      }
    if(in[0] != in[2*length - 1])
      out_begin[0]+= J * in[2*length - 1];
  }

  /**
   ** \brief Computes the complex split radix fft of a container (from FFTPACK.hpp).
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2007/11/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \pre size(b) >= size(a).
   ** \pre the input and output data have to be std::complex
   ** \par Example:
   ** \link slip::real_split_radix_fft(InputIter in_begin, InputIter in_end, OutputIter out_begin)
   **  ...
   ** \endlink
   ** \code
   **  slip::Array<cx> Ic(N,cx(0));
   **  std::copy(I.begin(),I.end(),Ic.begin());
   **  slip::complex_split_radix_fft(Ic.begin(),Ic.end(),b.begin());
   ** \endcode
   */
  template<class InputIter,class OutputIter>
  inline
  void complex_split_radix_fft(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    typedef typename std::iterator_traits<InputIter>::value_type complex;
    typedef typename complex::value_type real;
    const complex J(0,1);
    size_t length = in_end - in_begin;
    assert(length > 1);

    //data copy
    slip::Array<real> in(2*length,0.0);
    typename slip::stride_iterator<typename slip::Array<real>::iterator> itst_in_oddbegin(in.begin(),2);
    typename slip::stride_iterator<typename slip::Array<real>::iterator> itst_in_evenbegin(in.begin()+1,2);
    std::transform(in_begin,in_end,itst_in_oddbegin,slip::un_real<complex,real>());
    std::transform(in_begin,in_end,itst_in_evenbegin,slip::un_imag<complex,real>());

    //FFT initialization
    slip::Array<real> r(8*length+15,0.0);
    slip::Array<size_t> ifac(8*length+15,(size_t)0);
    slip::cffti(&length,r.begin(),ifac.begin());

    //FFT
    slip::cfftf(&length,in.begin(),r.begin(),ifac.begin());
   
    //Output copy
    for(size_t i = 0; i < length; i++)
       {
 	out_begin[i] = in[2*i] + J * in[2*i+1];
       }
  }

  /**
   ** \brief Computes the split radix fft backward of a container (from FFTPACK.hpp).
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2007/11/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \pre the input data have to be std::complex 
   ** \par Example:
   ** \link slip::real_split_radix_fft(InputIter in_begin, InputIter in_end, OutputIter out_begin)
   **  ...
   ** \endlink
   ** \code
   **  slip::Array<cx> b2(N,cx(0));
   **  slip::split_radix_ifft(b.begin(),b.end(),b2.begin());
   ** \endcode
   */
  template<class InputIter,class OutputIter>
  inline
  void split_radix_ifft(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    typedef typename std::iterator_traits<InputIter>::value_type complex;
    typedef typename complex::value_type real;
    const complex J(0,1);
    size_t length = in_end - in_begin;
    assert(length > 1);

    // data copy
    slip::Array<real> in(2*length,0.0);
    typename slip::stride_iterator<typename slip::Array<real>::iterator> itst_in_oddbegin(in.begin(),2);
    typename slip::stride_iterator<typename slip::Array<real>::iterator> itst_in_evenbegin(in.begin()+1,2);
    std::transform(in_begin,in_end,itst_in_oddbegin,slip::un_real<complex,real>());
    std::transform(in_begin,in_end,itst_in_evenbegin,slip::un_imag<complex,real>());

    //  FFT initialization
    slip::Array<real> r(5*length,0.0);
    slip::Array<size_t> ifac(5*length,(size_t)0);
    slip::cffti(&length,r.begin(),ifac.begin());
    
    //  FFT
    slip::cfftb(&length,in.begin(),r.begin(),ifac.begin());

    // output data copy
    for(size_t i = 0; i < length; i++)
      {
	out_begin[i] = (in[2*i] + J * in[2*i+1]) / (real)length;
      }
  }
  
 

 /**
   ** \brief Computes the real split-radix fft2d of a container with 2d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2007/11/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_upper_left .
   ** \param in_bottom_right
   ** \param out_upper_left
   **
   ** \pre the input data have to be real (not complex)
   ** \pre the 2d range must have the same sizes
   ** \par Example:
   ** \code
   **  slip::GrayscaleImage<double> I;
   **  I.read("lena.bmp");
   **  //fft2d
   **  slip::Matrix<std::complex<double> > IFFT(I.dim1(),I.dim2());
   **  slip::real_split_radix_fft2d(I.upper_left(),I.bottom_right(),IFFT.upper_left());
   **  slip::GrayscaleImage<double> INorm(IFFT.dim1(),IFFT.dim2());
   **  //log(magnitude)
   **  for(size_t i = 0; i < IFFT.dim1(); ++i)
   **     for(size_t j = 0; j < IFFT.dim2(); ++j)
   **        INorm[i][j] = std::log(1.0+std::abs(IFFT[i][j]));
   **  slip::fftshift2d(INorm.upper_left(),INorm.bottom_right());
   **  slip::change_dynamic_01(INorm.begin(),INorm.end(),INorm.begin(),slip::AFFINE_FUNCTION);
   **  INorm.write("lena_fft.jpg");
   ** \endcode
   */
  template<typename InputBidirectionalIterator2d,
	   typename OutputBidirectionalIterator2d>
  inline
  void real_split_radix_fft2d(InputBidirectionalIterator2d in_upper_left,
			      InputBidirectionalIterator2d in_bottom_right,
			      OutputBidirectionalIterator2d out_upper_left)
  {
    //recuperer la taille du container
    typename InputBidirectionalIterator2d::difference_type size2d = in_bottom_right - in_upper_left;
    
    std::size_t nb_lig = size2d[0];
    std::size_t nb_col = size2d[1];
    
    // fft2d
    for(size_t i = 0; i < nb_lig; ++i)
      {
	slip::real_split_radix_fft(in_upper_left.row_begin(i),in_upper_left.row_end(i),out_upper_left.row_begin(i));
      } 
    
    slip::Vector<std::complex<double> > Vtmp(nb_lig);
    for(size_t j = 0; j < nb_col; ++j)
      {
	slip::complex_split_radix_fft(out_upper_left.col_begin(j),out_upper_left.col_end(j),Vtmp.begin());
	std::copy(Vtmp.begin(),Vtmp.end(),out_upper_left.col_begin(j));
      }
  } 

  /**
   ** \brief Computes the complex split-radix fft2d of a container with 2d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/01/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_upper_left
   ** \param in_bottom_right
   ** \param out_upper_left
   **
   ** \pre the input data have to be complex
   ** \pre the 2d range must have the same sizes
   */
  template<typename InputBidirectionalIterator2d,
	   typename OutputBidirectionalIterator2d>
  inline
  void complex_split_radix_fft2d(InputBidirectionalIterator2d in_upper_left,
			      InputBidirectionalIterator2d in_bottom_right,
			      OutputBidirectionalIterator2d out_upper_left)
  {
    //recuperer la taille du container
    typename InputBidirectionalIterator2d::difference_type size2d = in_bottom_right - in_upper_left;
    
    std::size_t nb_lig = size2d[0];
    std::size_t nb_col = size2d[1];
    
    // fft2d
    for(size_t i = 0; i < nb_lig; ++i)
      {
	slip::complex_split_radix_fft(in_upper_left.row_begin(i),in_upper_left.row_end(i),out_upper_left.row_begin(i));
      } 
    
    slip::Vector<std::complex<double> > Vtmp(nb_lig);
    for(size_t j = 0; j < nb_col; ++j)
      {
	slip::complex_split_radix_fft(out_upper_left.col_begin(j),out_upper_left.col_end(j),Vtmp.begin());
	std::copy(Vtmp.begin(),Vtmp.end(),out_upper_left.col_begin(j));
      }
  } 
 
  /**
   ** \brief Computes the split-radix ifft2d of a container with 2d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2007/11/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_upper_left
   ** \param in_bottom_right
   ** \param out_upper_left
   **
   ** \pre the input data have to be std::complex
   ** \pre the 2d range must have the same sizes
   ** \par Example:
   ** \link slip::real_split_radix_fft2d(InputBidirectionalIterator2d in_upper_left,InputBidirectionalIterator2d in_bottom_right,OutputBidirectionalIterator2d out_upper_left)
   **  ...
   ** \endlink
   ** \code
   **  slip::Matrix<std::complex<double> > IOut(I.dim1(),I.dim2());
   **  //ifft2d
   **  slip::split_radix_ifft2d(IFFT.upper_left(),IFFT.bottom_right(),IOut.upper_left());
   **  for(size_t i = 0; i < IOut.dim1(); ++i)
   **     for(size_t j = 0; j < IOut.dim2(); ++j)
   **	      INorm[i][j] = std::abs(IOut[i][j]);
   **  slip::change_dynamic_01(INorm.begin(),INorm.end(),INorm.begin(),slip::AFFINE_FUNCTION);
   **  INorm.write("lena_ifft.png");
   ** \endcode
   **
   */
  template<typename InputBidirectionalIterator2d,
	   typename OutputBidirectionalIterator2d>
  inline
  void split_radix_ifft2d(InputBidirectionalIterator2d in_upper_left,
			  InputBidirectionalIterator2d in_bottom_right,
			  OutputBidirectionalIterator2d out_upper_left)
  {
    //recuperer la taille du container
    typename InputBidirectionalIterator2d::difference_type size2d = in_bottom_right - in_upper_left;
    
    std::size_t nb_lig = size2d[0];
    std::size_t nb_col = size2d[1];
    
    
    //centered fft2d
    //   slip::fftshift2d(in_upper_left,in_bottom_right);
    
    //fft2d
    for(size_t i = 0; i < nb_lig; ++i)
      {
	slip::split_radix_ifft(in_upper_left.row_begin(i),in_upper_left.row_end(i),out_upper_left.row_begin(i));
      } 
    
    slip::Vector<std::complex<double> > Vtmp(nb_lig);
    for(size_t j = 0; j < nb_col; ++j)
      {
	slip::split_radix_ifft(out_upper_left.col_begin(j),out_upper_left.col_end(j),Vtmp.begin());
	std::copy(Vtmp.begin(),Vtmp.end(),out_upper_left.col_begin(j));
      }
  } 

  /**
   ** \brief Computes the real split-radix fft2d of a container
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2007/12/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param datain The input datas
   ** \param dataout The output datas
   ** 
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain and dataout must have cols and rows iterator
   ** \pre the input data have to be real (not complex)
   ** \pre datin and dataout must have the same size 
   ** \par Example:
   ** \code
   **  slip::GrayscaleImage<double> I;
   **  I.read("lena.bmp");
   **  //fft2d
   **  slip::Matrix<std::complex<double> > IFFT(I.dim1(),I.dim2());
   **  slip::real_split_radix_fft2d(I,IFFT);
   **  slip::GrayscaleImage<double> INorm(IFFT.dim1(),IFFT.dim2());
   **  //log(magnitude)
   **  for(size_t i = 0; i < IFFT.dim1(); ++i)
   **     for(size_t j = 0; j < IFFT.dim2(); ++j)
   **        INorm[i][j] = std::log(1.0+std::abs(IFFT[i][j]));
   **  slip::fftshift2d(INorm.upper_left(),INorm.bottom_right());
   **  slip::change_dynamic_01(INorm.begin(),INorm.end(),INorm.begin(),slip::AFFINE_FUNCTION);
   **  INorm.write("lena_fft.jpg");
   ** \endcode
   **
   */
  template<typename InputMatrix, typename OutputMatrix>
  inline void real_split_radix_fft2d(InputMatrix &datain, OutputMatrix &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    slip::real_split_radix_fft2d(datain.upper_left(),datain.bottom_right(),dataout.upper_left());
  } 

  /**
   ** \brief Computes the split-radix ifft2d of a container.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2007/12/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param datain The input datas
   ** \param dataout The output datas
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain and dataout must have cols and rows iterator
   ** \pre datin and dataout must have the same size
   ** \pre the input data have to be std::complex
   ** \par Example:
   ** \link slip::real_split_radix_fft2d(InputBidirectionalIterator2d in_upper_left,InputBidirectionalIterator2d in_bottom_right,OutputBidirectionalIterator2d out_upper_left)
   **  ...
   ** \endlink
   ** \code
   **  slip::Matrix<std::complex<double> > IOut(I.dim1(),I.dim2());
   **  //ifft2d
   **  slip::split_radix_ifft2d(IFFT,IOut);
   **  for(size_t i = 0; i < IOut.dim1(); ++i)
   **     for(size_t j = 0; j < IOut.dim2(); ++j)
   **	      INorm[i][j] = std::abs(IOut[i][j]);
   **  slip::change_dynamic_01(INorm.begin(),INorm.end(),INorm.begin(),slip::AFFINE_FUNCTION);
   **  INorm.write("lena_ifft.png");
   ** \endcode
   */
  template<typename InputMatrix, typename OutputMatrix>
  inline
  void split_radix_ifft2d(InputMatrix &datain, OutputMatrix &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    slip::split_radix_ifft2d(datain.upper_left(),datain.bottom_right(),dataout.upper_left());
  }

  /**
   ** \brief Computes the real split-radix fft3d of a container with 3d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/01/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_front_upper_left
   ** \param in_back_bottom_right
   ** \param out_front_upper_left
   **
   ** \pre the input data have to be real (not complex)
   ** \pre the 3d range must have the same sizes
   ** \par Example:
   ** \code
   **   slip::Volume<double> M;
   **   M.read_raw("volume.vol",dim1,dim2,dim3);
   **   slip::Volume<double> Out(M.dim1(),M.dim2(),M.dim3(),1);
   **   //FFT3D
   **   slip::Matrix3d<std::complex<double> > OFFT(M.dim1(),M.dim2(),M.dim3(),Czero);
   **   slip::real_split_radix_fft3d(M.front_upper_left(),M.back_bottom_right(),OFFT.front_upper_left());
   **   slip::Volume<double> NFFT(OFFT.dim1(),OFFT.dim2(),OFFT.dim3(),M.front_upper_left(),M.back_bottom_right());
   **   //  log(magnitude)
   **   for(size_t z = 0; z < OFFT.dim1(); ++z)
   **     for(size_t i = 0; i < OFFT.dim2(); ++i)
   **       for(size_t j = 0; j < OFFT.dim3(); ++j)
   **	      NFFT[z][i][j] = std::log(1.0+std::abs(OFFT[z][i][j]));
   **
   **   slip::fftshift3d(NFFT.front_upper_left(),NFFT.back_bottom_right());
   **   slip::change_dynamic_01(NFFT.begin(),NFFT.end(),NFFT.begin(),slip::AFFINE_FUNCTION);
   **   NFFT.write_raw("volume_fft.vol");
   **  \endcode
   */
  template<typename InputBidirectionalIterator3d,
	   typename OutputBidirectionalIterator3d>
  inline
  void real_split_radix_fft3d(InputBidirectionalIterator3d in_front_upper_left,
			      InputBidirectionalIterator3d in_back_bottom_right,
			      OutputBidirectionalIterator3d out_front_upper_left)
  {
    //recuperer la taille du container
    typename InputBidirectionalIterator3d::difference_type size3d = in_back_bottom_right - in_front_upper_left;
    //    typedef typename std::iterator_traits<InputBidirectionalIterator3d>::value_type real;
    //typedef typename std::iterator_traits<OutputBidirectionalIterator3d>::value_type complex;

    std::size_t nb_sli = size3d[0];
    std::size_t nb_lig = size3d[1];
    std::size_t nb_col = size3d[2];
    //fft3d
    for(size_t k = 0; k < nb_sli; ++k)
      for(size_t i = 0; i < nb_lig; ++i)
      {
	slip::real_split_radix_fft(in_front_upper_left.row_begin(k,i),in_front_upper_left.row_end(k,i),out_front_upper_left.row_begin(k,i));
      } 

     slip::Vector<std::complex<double> > Vtmp(nb_lig);
     for(size_t k = 0; k < nb_sli; ++k)
       for(size_t j = 0; j < nb_col; ++j)
 	{
	  slip::complex_split_radix_fft(out_front_upper_left.col_begin(k,j),out_front_upper_left.col_end(k,j),Vtmp.begin());
	  std::copy(Vtmp.begin(),Vtmp.end(),out_front_upper_left.col_begin(k,j));
 	}
     
     Vtmp.resize(nb_sli);
     for(size_t i = 0; i < nb_lig; ++i)
      for(size_t j = 0; j < nb_col; ++j)
	{
	  slip::complex_split_radix_fft(out_front_upper_left.slice_begin(i,j),out_front_upper_left.slice_end(i,j),Vtmp.begin());
	  std::copy(Vtmp.begin(),Vtmp.end(),out_front_upper_left.slice_begin(i,j));
	}
  } 

  /**
   ** \brief Computes the split-radix ifft3d of a container with 3d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/01/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_front_upper_left
   ** \param in_back_bottom_right
   ** \param out_front_upper_left
   **
   ** \pre the input data have to be std::complex
   ** \pre the 3d range must have the same sizes
   ** \par Example:
   ** \link slip::real_split_radix_fft3d(InputBidirectionalIterator3d,InputBidirectionalIterator3d,OutputBidirectionalIterator3d)
   **  ...
   ** \endlink
   ** \code
   **  std::complex<double> Czero(0,0);
   **  slip::Matrix3d<std::complex<double> > OC(M.dim1(),M.dim2(),M.dim3(),Czero);
   **  slip::Volume<double> Out(M.dim1(),M.dim2(),M.dim3(),1);
   **  slip::split_radix_ifft3d(OFFT.front_upper_left(),OFFT.back_bottom_right(),OC.front_upper_left());
   **  for(size_t z = 0; z < OFFT.dim1(); ++z)
   **	for(size_t i = 0; i < OFFT.dim2(); ++i)
   **	  for(size_t j = 0; j < OFFT.dim3(); ++j)
   **	    {
   **	      Out[z][i][j] = std::abs(OC[z][i][j]);
   **	    }
   **  slip::change_dynamic_01(Out.begin(),Out.end(),Out.begin(),slip::AFFINE_FUNCTION);
   **  Out.write_raw("volume_ifft.vol");
   ** \endcode
   **
   */
  template<typename InputBidirectionalIterator3d,
	   typename OutputBidirectionalIterator3d>
  inline
  void split_radix_ifft3d(InputBidirectionalIterator3d in_front_upper_left,
			  InputBidirectionalIterator3d in_back_bottom_right,
			  OutputBidirectionalIterator3d out_front_upper_left)
  {
    //recuperer la taille du container
    typename InputBidirectionalIterator3d::difference_type size3d = in_back_bottom_right - in_front_upper_left;
    
    std::size_t nb_sli = size3d[0];
    std::size_t nb_lig = size3d[1];
    std::size_t nb_col = size3d[2];
        
    //ifft3d
    for(size_t k = 0; k < nb_sli; ++k)
      for(size_t i = 0; i < nb_lig; ++i)
	{
	  slip::split_radix_ifft(in_front_upper_left.row_begin(k,i),in_front_upper_left.row_end(k,i),out_front_upper_left.row_begin(k,i));
	} 
    
    slip::Vector<std::complex<double> > Vtmp(nb_lig);
    for(size_t k = 0; k < nb_sli; ++k)
      for(size_t j = 0; j < nb_col; ++j)
 	{
 	  slip::split_radix_ifft(out_front_upper_left.col_begin(k,j),out_front_upper_left.col_end(k,j),Vtmp.begin());
 	  std::copy(Vtmp.begin(),Vtmp.end(),out_front_upper_left.col_begin(k,j));
 	}
    
    Vtmp.resize(nb_sli);
    for(size_t i = 0; i < nb_lig; ++i)
      for(size_t j = 0; j < nb_col; ++j)
	{
	  slip::split_radix_ifft(out_front_upper_left.slice_begin(i,j),out_front_upper_left.slice_end(i,j),Vtmp.begin());
	  std::copy(Vtmp.begin(),Vtmp.end(),out_front_upper_left.slice_begin(i,j));
	}
  } 


  /*!
 ** \version Fluex 1.0
 ** \date 2013/10/22
 ** \since 1.4.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \brief Computes the complex split-radix fft3d of a container with 3d iterators.
 ** \param in_front_upper_left
 ** \param in_back_bottom_right
 ** \param out_front_upper_left
 **
 ** \pre the input data have to be complex
 ** \pre the 3d range must have the same sizes
 */
template<typename InputBidirectionalIterator3d,
typename OutputBidirectionalIterator3d>
inline
void complex_split_radix_fft3d(InputBidirectionalIterator3d in_front_upper_left,
		InputBidirectionalIterator3d in_back_bottom_right,
		OutputBidirectionalIterator3d out_front_upper_left)
{
	//recuperer la taille du container
	typename InputBidirectionalIterator3d::difference_type size3d = in_back_bottom_right - in_front_upper_left;
	//	typedef typename std::iterator_traits<InputBidirectionalIterator3d>::value_type complex;
	//	typedef typename complex::value_type real;


	std::size_t nb_sli = size3d[0];
	std::size_t nb_lig = size3d[1];
	std::size_t nb_col = size3d[2];
	//fft3d
	for(size_t k = 0; k < nb_sli; ++k)
		for(size_t i = 0; i < nb_lig; ++i)
		{
			slip::complex_split_radix_fft(in_front_upper_left.row_begin(k,i),
					in_front_upper_left.row_end(k,i),out_front_upper_left.row_begin(k,i));
		}

	slip::Vector<std::complex<double> > Vtmp(nb_lig);
	for(size_t k = 0; k < nb_sli; ++k)
		for(size_t j = 0; j < nb_col; ++j)
		{
			slip::complex_split_radix_fft(out_front_upper_left.col_begin(k,j),out_front_upper_left.col_end(k,j),Vtmp.begin());
			std::copy(Vtmp.begin(),Vtmp.end(),out_front_upper_left.col_begin(k,j));
		}

	Vtmp.resize(nb_sli);
	for(size_t i = 0; i < nb_lig; ++i)
		for(size_t j = 0; j < nb_col; ++j)
		{
			slip::complex_split_radix_fft(out_front_upper_left.slice_begin(i,j),out_front_upper_left.slice_end(i,j),Vtmp.begin());
			std::copy(Vtmp.begin(),Vtmp.end(),out_front_upper_left.slice_begin(i,j));
		}
}


/**
 ** \brief Computes the complex split-radix fft3d of a container 3d
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/10/22
 ** \since 1.4.0
 ** \pre datain.cols() == dataout.cols()
 ** \pre datain.rows() == dataout.rows()
 ** \pre datain.slices() == dataout.slices()
 ** \pre datain and dataout must have iterator3d available
 ** \pre datin and dataout must have the same size
 ** \pre the input data have to be complex
 **
 */
template<typename InputMatrix3d,
typename OutputMatrix3d>
inline
void complex_split_radix_fft3d(InputMatrix3d & datain,OutputMatrix3d & dataout)
{
	assert(datain.cols() == dataout.cols());
	assert(datain.rows() == dataout.rows());
	assert(datain.slices() == dataout.slices());
	slip::complex_split_radix_fft3d(datain.front_upper_left(),datain.back_bottom_right(),dataout.front_upper_left());
}

  /**
   ** \brief Computes the real split-radix fft3d of a container 3d
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/01/28
   ** \since 1.0.0
   ** \version 0.0.1
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain.slices() == dataout.slices()
   ** \pre datain and dataout must have iterator3d available
   ** \pre datin and dataout must have the same size
   ** \pre the input data have to be real
   ** \par Example:
   ** \code
   **   slip::Volume<double> M;
   **   M.read_raw("volume.vol",dim1,dim2,dim3);
   **   slip::Volume<double> Out(M.dim1(),M.dim2(),M.dim3(),1);
   **   //FFT3D
   **   slip::Matrix3d<std::complex<double> > OFFT(M.dim1(),M.dim2(),M.dim3(),Czero);
   **   slip::real_split_radix_fft3d(M,OFFT);
   **   slip::Volume<double> NFFT(OFFT.dim1(),OFFT.dim2(),OFFT.dim3(),M.front_upper_left(),M.back_bottom_right());
   **   //  log(magnitude)
   **   for(size_t z = 0; z < OFFT.dim1(); ++z)
   **     for(size_t i = 0; i < OFFT.dim2(); ++i)
   **       for(size_t j = 0; j < OFFT.dim3(); ++j)
   **	      NFFT[z][i][j] = std::log(1.0+std::abs(OFFT[z][i][j]));
   **
   **   slip::fftshift3d(NFFT.front_upper_left(),NFFT.back_bottom_right());
   **   slip::change_dynamic_01(NFFT.begin(),NFFT.end(),NFFT.begin(),slip::AFFINE_FUNCTION);
   **   NFFT.write_raw("volume_fft.vol");
   **  \endcode
   **
   */
  template<typename InputMatrix3d,
	   typename OutputMatrix3d>
  inline
  void real_split_radix_fft3d(InputMatrix3d & datain,OutputMatrix3d & dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    assert(datain.slices() == dataout.slices());
    slip::real_split_radix_fft3d(datain.front_upper_left(),datain.back_bottom_right(),dataout.front_upper_left());
  } 






  /**
   ** \brief Computes the split-radix ifft3d of a container 3d.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/01/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param datain The input datas
   ** \param dataout The output datas
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain.slices() == dataout.slices()
   ** \pre datain and dataout must have iterator3d available
   ** \pre datin and dataout must have the same size
   ** \pre the input data have to be std::complex
   ** \par Example:
   ** \link slip::real_split_radix_fft3d(InputMatrix3d & datain,OutputMatrix3d & dataout)
   **  ...
   ** \endlink
   ** \code
   **  std::complex<double> Czero(0,0);
   **  slip::Matrix3d<std::complex<double> > OC(M.dim1(),M.dim2(),M.dim3(),Czero);
   **  slip::Volume<double> Out(M.dim1(),M.dim2(),M.dim3(),1);
   **  slip::split_radix_ifft3d(OFFT,OC);
   **  for(size_t z = 0; z < OFFT.dim1(); ++z)
   **	for(size_t i = 0; i < OFFT.dim2(); ++i)
   **	  for(size_t j = 0; j < OFFT.dim3(); ++j)
   **	    {
   **	      Out[z][i][j] = std::abs(OC[z][i][j]);
   **	    }
   **  slip::change_dynamic_01(Out.begin(),Out.end(),Out.begin(),slip::AFFINE_FUNCTION);
   **  Out.write_raw("volume_ifft.vol");
   ** \endcode
   **
   */
  template<typename InputMatrix3d,
	   typename OutputMatrix3d>
  inline
  void split_radix_ifft3d(InputMatrix3d & datain,OutputMatrix3d & dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    assert(datain.slices() == dataout.slices());
    slip::split_radix_ifft3d(datain.front_upper_left(),datain.back_bottom_right(),dataout.front_upper_left());
  } 

 
  //Discrete Cosinus Transform

  /**
   ** \brief Computes the split radix Discrete Cosinus Transform III (i.e. idct) of a container (from FFTPACK.hpp).
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/03/20
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \pre size(b) >= size(a).) 
   ** \post ouput data are normalized by \f$\frac{1}{\sqrt{2\times N}}\f$
   ** \par Example:
   ** \link split_radix_dct(InputIter,InputIter,OutputIter)
   **  ...
   ** \endlink
   ** \code
   **  tt b2[8];
   **  slip::split_radix_idct(b,b+8,b2);
   **  for(int i = 0; i < 8; ++i)
   **     std::cout<<b2[i]<<" ";
   **  std::cout<<std::endl<<std::endl;
   ** \endcode
   */
  template<class InputIter,class OutputIter>
  inline
  void split_radix_idct(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    typedef typename std::iterator_traits<InputIter>::value_type real;
    size_t length = in_end - in_begin;
    assert(length > 1);
    
    // data copy
    std::copy(in_begin,in_end,out_begin);
    *out_begin *= slip::constants<real>::sqrt2();
    //DCT initialization
    slip::Array<real> r(8*length+15,0.0);
    slip::Array<size_t> ifac(8*length+15,(size_t)0);
    slip::cosqi(&length,r.begin(),ifac.begin());

    //DCT
    slip::cosqf(&length,out_begin,r.begin(),ifac.begin());
    std::transform(out_begin, out_begin+length, out_begin,std::bind2nd(std::divides<real>(),std::sqrt(2*length)));
   
  }

  /**
   ** \brief Computes the split radix Discrete Cosinus Transform II of a container (from FFTPACK.hpp).
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2007/11/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data) 
   ** \post ouput data are normalized by \f$\frac{1}{\sqrt{2\times N}}\f$
   ** \post another normalization by \f$\frac{1}{2}\f$ was done in order to obtain 
   ** the same results than the fftw_dct function
   ** \par Example:
   ** \code
   **  typedef double tt;
   **  tt a[] = { tt(0), tt(1), tt(3), tt(4), tt(4), tt(3),tt(1),tt(0)};
   **  tt b[8];
   **  slip::split_radix_dct(a,a+8,b);
   **  for(int i = 0; i < 8; ++i)
   **      std::cout<<b[i]<<" ";
   **  std::cout<<std::endl;
   ** \endcode
   */
  template<class InputIter,class OutputIter>
  inline
  void split_radix_dct(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    typedef typename std::iterator_traits<InputIter>::value_type real;
    size_t length = in_end - in_begin;
    assert(length > 1);
    
    // data copy
    std::copy(in_begin,in_end,out_begin);

    // DCT initialization
    slip::Array<real> r(5*length,0.0);
    slip::Array<size_t> ifac(5*length,(size_t)0);
    slip::cosqi(&length,r.begin(),ifac.begin());
    
    //  DCT
    slip::cosqb(&length,out_begin,r.begin(),ifac.begin());
    std::transform(out_begin, out_begin+length, out_begin,std::bind2nd(std::divides<real>(),2*std::sqrt(2*length)));
    *out_begin *= slip::constants<real>::sqrt1_2();
  }

  /*@} End FFT Split-Radix algorithms (from FFTPACK.hpp). */

#ifdef HAVE_FFTW  
 
  ////////////////////////////////// fftw algorithms (from fftw3.h)./////////////////////////////////////////////////  
  
  /**
   ** \name fftw algorithms (from fftw3.h).
   */
  /*@{*/

  /**
   ** \brief Calculates the conjugates of the begin std::complex elements of a first container. 
   ** Writes them at the end of a second container. This function is used to calculate the negative 
   ** frequencies after a real fftw.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/04
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param begin1 A ForwardIterator at the beginning of the input container
   ** \param begin2 A ForwardIterator at the beginning of the output container
   ** \param end2 A ForwardIterator at the end of the output container.
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   ** \pre both containers must have std::complex elements
   */
  template<class ForwardIterator1, class ForwardIterator2>
  inline
  void fftduplicate(ForwardIterator1 begin1, ForwardIterator2 begin2,
		    ForwardIterator2 end2)
  {
    typedef typename std::iterator_traits<ForwardIterator1>::value_type complex;
    complex J(0,1);
    begin1++;
    begin2++;
    end2--;
    while (begin2 < end2)
      {
	*end2 = std::real(*begin1) - J*std::imag(*begin1);
	begin1++;
	begin2++;
	end2--;
      }
  }

  /**
   ** \brief Performs a duplicate of a 2D container, for use with the fft2d and ifft2d 
   **        functions, in order to calculate the negative frequencies with the real fftw2d
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/04
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param upper_left A ForwardIterator on std::complex numbers
   ** \param bottom_right A ForwardIterator on std::complex numbers
   **
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   */
  template<class ForwardIterator2D>
  inline
  void fftduplicate2d(ForwardIterator2D upper_left, 
		      ForwardIterator2D bottom_right)
  {
    typename ForwardIterator2D::difference_type size2d = bottom_right - upper_left;
    std::size_t nb_lig = size2d[0];
    slip::fftduplicate(upper_left.row_begin(0),upper_left.row_begin(0),upper_left.row_end(0));
    for(size_t i = 1; i < nb_lig; ++i)
      {
    	slip::fftduplicate(upper_left.row_begin(i),upper_left.row_begin(nb_lig - i),upper_left.row_end(nb_lig - i));
      }
  }
 
  /**
   ** \brief Computes the real fftw of a container (from fftw3.h).
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/01
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \pre size(b) >= size(a).
   ** \pre the input data have to be real and out put data have to be std::complex
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   */
  template<class InputIter,class OutputIter>
  inline 
  void real_fftw(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    //   typedef typename std::iterator_traits<InputIter>::value_type real;
    typedef typename std::iterator_traits<OutputIter>::value_type complex;
    typedef typename complex::value_type real;
    complex J(0,1);
    size_t length = in_end - in_begin;
    size_t new_length = length/2 + 1;
    assert(length > 1);
    slip::Array<double> in(length);
    slip::Array<std::complex<double> > out(new_length);
    fftw_plan plan;
    
    std::copy(in_begin,in_end,in.begin());
    //With the FFTW_ESTIMATE flag, the input/output arrays are not overwritten during planning. !!! Default flag
    //gave some unexplained segmentation faults with the odd dimensions greater than 40 !!!
    plan  = fftw_plan_dft_r2c_1d(int(length),(double*)&(*in.begin()),(fftw_complex*)&(*out.begin()),FFTW_ESTIMATE);
    fftw_execute(plan);

    slip::Array<std::complex<double> >::iterator it = out.begin(); 
    OutputIter itout = out_begin;
    while(it < out.end())
      {
	*itout = (real)std::real(*it) + J * (real)std::imag(*it);
	itout++;
	it++;
      } 

    slip::fftduplicate(out_begin,out_begin,out_begin+length);
    
    fftw_destroy_plan(plan);
  }
  
 /**
   ** \brief complex fftw of a container (from fftw3.h).
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \pre size(b) >= size(a).
   ** \pre the input and output data have to be std::complex
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   */
  template<class InputIter,class OutputIter>
  inline
  void complex_fftw(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    typedef typename std::iterator_traits<OutputIter>::value_type complex;
    typedef typename complex::value_type real;
    complex J(0,1);
    //    typedef typename complex::value_type real;
    size_t length = in_end - in_begin;
    assert(length > 1);

    slip::Array<std::complex<double> > in(length);
    slip::Array<std::complex<double> > out(length);
    fftw_plan plan;
    
    std::copy(in_begin,in_end,in.begin());
    plan  = fftw_plan_dft_1d(int(length),(fftw_complex*)&(*in.begin()),(fftw_complex*)&(*out.begin()),-1,FFTW_ESTIMATE);
    fftw_execute(plan);
    slip::Array<std::complex<double> >::iterator it = out.begin(); 
    OutputIter itout = out_begin;
    while(it < out.end())
      {
	*itout = ((real)std::real(*it) + J * (real)std::imag(*it));
	itout++;
	it++;
      } 
    
    fftw_destroy_plan(plan);
  }
   
  /**
   ** \brief Computes the fftw backward of a container (from fftw3.h).
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/01
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \pre the input data have to be std::complex
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration)  
   */
  template<class InputIter,class OutputIter>
  inline 
  void ifftw(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    typedef typename std::iterator_traits<OutputIter>::value_type complex;
    typedef typename complex::value_type real;
    complex J(0,1);
    size_t length = in_end - in_begin;
    assert(length > 1);

    slip::Array<std::complex<double> > in(length);
    slip::Array<std::complex<double> > out(length);
    fftw_plan plan;

    std::copy(in_begin,in_end,in.begin());
    plan  = fftw_plan_dft_1d(int(length),(fftw_complex*)&(*in.begin()),(fftw_complex*)&(*out.begin()),1,FFTW_ESTIMATE);
    fftw_execute(plan);
    slip::Array<std::complex<double> >::iterator it = out.begin(); 
    OutputIter itout = out_begin;
    while(it < out.end())
      {
	*itout = ((real)std::real(*it) + J * (real)std::imag(*it)) / (real)length;
	itout++;
	it++;
      } 

    fftw_destroy_plan(plan);
  }
  
  /**
   ** \brief Computes the real fftw2d of a container with 2d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_upper_left
   ** \param in_bottom_right
   ** \param out_upper_left
   **
   ** \pre the input data have to be real (not complex)
   ** \pre the 2d range must have the same sizes
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   **
   */
  template<typename InputBidirectionalIterator2d,
	   typename OutputBidirectionalIterator2d>
  inline
  void real_fftw2d(InputBidirectionalIterator2d in_upper_left,
		   InputBidirectionalIterator2d in_bottom_right,
		   OutputBidirectionalIterator2d out_upper_left)
  {
    typedef typename std::iterator_traits<OutputBidirectionalIterator2d>::value_type complex;
    typedef typename complex::value_type real;
    complex J(0,1);

    typename InputBidirectionalIterator2d::difference_type size2d = in_bottom_right - in_upper_left;
    std::size_t nb_lig = size2d[0];
    std::size_t nb_col = size2d[1];
    int new_nb_col = (nb_col/2)+1;
    assert((nb_lig > 1)&&(nb_col>1));

    slip::Array2d<double> in(nb_lig,nb_col);
    slip::Array2d<std::complex<double> > out(nb_lig,new_nb_col);
    fftw_plan plan;

    std::copy(in_upper_left,in_bottom_right,in.upper_left());
    //With the FFTW_ESTIMATE flag, the input/output arrays are not overwritten during planning. !!! Default flag
    //gave some unexplained segmentation faults with the odd dimensions greater than 40 !!!
    plan  = fftw_plan_dft_r2c_2d(int(nb_lig),int(nb_col),(double*)&(*in.upper_left()),(fftw_complex*)&(*out.upper_left()),FFTW_ESTIMATE);
    fftw_execute(plan);

    for(int i=0; i<int(nb_lig);i++)
      for(int j=0; j<new_nb_col;j++)
	{
	  slip::DPoint2d<int> dp(i,j);
	  *(out_upper_left + dp) = (real)std::real(out[i][j]) + J * (real)std::imag(out[i][j]);
	}
    slip::fftduplicate2d(out_upper_left, out_upper_left+size2d);

    fftw_destroy_plan(plan);
  } 
 
  /**
   ** \brief Computes the complex fftw2d of a container with 2d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/04
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_upper_left
   ** \param in_bottom_right
   ** \param out_upper_left
   **
   ** \pre the input data have to be complex
   ** \pre the 2d range must have the same sizes
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   **
   */
  template<typename InputBidirectionalIterator2d,
	   typename OutputBidirectionalIterator2d>
  inline
  void complex_fftw2d(InputBidirectionalIterator2d in_upper_left,
		      InputBidirectionalIterator2d in_bottom_right,
		      OutputBidirectionalIterator2d out_upper_left)
  {
    typedef typename std::iterator_traits<OutputBidirectionalIterator2d>::value_type complex;
    typedef typename complex::value_type real;
    complex J(0,1);

    typename InputBidirectionalIterator2d::difference_type size2d = in_bottom_right - in_upper_left;
    std::size_t nb_lig = size2d[0];
    std::size_t nb_col = size2d[1];
    assert((nb_lig > 1)&&(nb_col>1));

    slip::Array2d<std::complex<double> > in(nb_lig,nb_col);
    slip::Array2d<std::complex<double> > out(nb_lig,nb_col);
    fftw_plan plan;

    std::copy(in_upper_left,in_bottom_right,in.upper_left());
    plan  = fftw_plan_dft_2d(int(nb_lig),int(nb_col),(fftw_complex*)&(*in.upper_left()),(fftw_complex*)&(*out.upper_left()),-1,FFTW_ESTIMATE);
    fftw_execute(plan);
    slip::Array2d<std::complex<double> >::iterator it = out.begin(); 
    OutputBidirectionalIterator2d itout = out_upper_left;
    while(it < out.end())
      {
	*itout = ((real)std::real(*it) + J * (real)std::imag(*it));
	itout++;
	it++;
      } 
    
    fftw_destroy_plan(plan);
  } 
 
  /**
   ** \brief Computes the ifftw2d of a container with 2d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_upper_left
   ** \param in_bottom_right
   ** \param out_upper_left
   **
   ** \pre the input data have to be std::complex
   ** \pre the 2d range must have the same sizes
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   **
   */
  template<typename InputBidirectionalIterator2d,
	   typename OutputBidirectionalIterator2d>
  inline
  void ifftw2d(InputBidirectionalIterator2d in_upper_left,
	       InputBidirectionalIterator2d in_bottom_right,
	       OutputBidirectionalIterator2d out_upper_left)
  {
    typedef typename std::iterator_traits<OutputBidirectionalIterator2d>::value_type complex;
    typedef typename complex::value_type real;
    complex J(0,1);

    typename InputBidirectionalIterator2d::difference_type size2d = in_bottom_right - in_upper_left;
    std::size_t nb_lig = size2d[0];
    std::size_t nb_col = size2d[1];
    int length(nb_lig*nb_col);
    assert((nb_lig > 1)&&(nb_col>1));

    slip::Array2d<std::complex<double> > in(nb_lig,nb_col);
    slip::Array2d<std::complex<double> > out(nb_lig,nb_col);
    fftw_plan plan;

    std::copy(in_upper_left,in_bottom_right,in.upper_left());
    plan  = fftw_plan_dft_2d(int(nb_lig),int(nb_col),(fftw_complex*)&(*in.upper_left()),(fftw_complex*)&(*out.upper_left()),1,FFTW_ESTIMATE);
    fftw_execute(plan);

    slip::Array2d<std::complex<double> >::iterator it = out.begin(); 
    OutputBidirectionalIterator2d itout = out_upper_left;
    while(it < out.end())
      {
	*itout = ((real)std::real(*it) + J * (real)std::imag(*it)) / (real)length;
	itout++;
	it++;
      } 
    
    fftw_destroy_plan(plan);
  } 

  /**
   ** \brief Computes the real fftw2d of a container
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param datain The input datas
   ** \param dataout The output datas
   ** 
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain and dataout must have cols and rows iterator
   ** \pre the input data have to be real (not complex)
   ** \pre datin and dataout must have the same size 
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   **
   */
  template<typename InputMatrix, typename OutputMatrix>
  inline void real_fftw2d(InputMatrix &datain, OutputMatrix &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    slip::real_fftw2d(datain.upper_left(),datain.bottom_right(),dataout.upper_left());
  } 

  /**
   ** \brief Computes the ifftw2d of a container.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/04/03
   ** \version 0.0.1
   ** \param datain The input datas
   ** \param dataout The output datas
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain and dataout must have cols and rows iterator
   ** \pre datin and dataout must have the same size
   ** \pre the input data have to be std::complex
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   */
  template<typename InputMatrix, typename OutputMatrix>
  inline
  void ifftw2d(InputMatrix &datain, OutputMatrix &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    slip::ifftw2d(datain.upper_left(),datain.bottom_right(),dataout.upper_left());
  }
  
  /**
   ** \brief Computes the real fftw3d of a container with 3d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_front_upper_left
   ** \param in_back_bottom_right
   ** \param out_front_upper_left
   **
   ** \pre the input data have to be real (not complex)
   ** \pre the 3d range must have the same sizes
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   **
   */
  template<typename InputBidirectionalIterator3d,
	   typename OutputBidirectionalIterator3d>
  inline
  void real_fftw3d(InputBidirectionalIterator3d in_front_upper_left,
		   InputBidirectionalIterator3d in_back_bottom_right,
		   OutputBidirectionalIterator3d out_front_upper_left)
  {
    //recuperer la taille du container
    typename InputBidirectionalIterator3d::difference_type size3d = in_back_bottom_right - in_front_upper_left;
    //    typedef typename std::iterator_traits<InputBidirectionalIterator3d>::value_type real;
    //    typedef typename std::iterator_traits<OutputBidirectionalIterator3d>::value_type complex;

    std::size_t nb_sli = size3d[0];
    std::size_t nb_lig = size3d[1];
    std::size_t nb_col = size3d[2];
    assert((nb_sli > 1)&&(nb_lig > 1)&&(nb_col>1));
     //fft3d
    for(size_t k = 0; k < nb_sli; ++k)
      for(size_t i = 0; i < nb_lig; ++i)
      {
	slip::real_fftw(in_front_upper_left.row_begin(k,i),in_front_upper_left.row_end(k,i),out_front_upper_left.row_begin(k,i));
      } 

     slip::Vector<std::complex<double> > Vtmp(nb_lig);
     for(size_t k = 0; k < nb_sli; ++k)
       for(size_t j = 0; j < nb_col; ++j)
 	{
	  slip::complex_fftw(out_front_upper_left.col_begin(k,j),out_front_upper_left.col_end(k,j),Vtmp.begin());
	  std::copy(Vtmp.begin(),Vtmp.end(),out_front_upper_left.col_begin(k,j));
 	}
     
     Vtmp.resize(nb_sli);
     for(size_t i = 0; i < nb_lig; ++i)
      for(size_t j = 0; j < nb_col; ++j)
	{
	  slip::complex_fftw(out_front_upper_left.slice_begin(i,j),out_front_upper_left.slice_end(i,j),Vtmp.begin());
	  std::copy(Vtmp.begin(),Vtmp.end(),out_front_upper_left.slice_begin(i,j));
	}
  } 

 /**
   ** \brief Computes the complex fftw3d of a container with 3d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_front_upper_left
   ** \param in_back_bottom_right
   ** \param out_front_upper_left
   **
   ** \pre the input and output data have to be std::complex
   ** \pre the 3d range must have the same sizes
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   **
   */
  template<typename InputBidirectionalIterator3d,
	   typename OutputBidirectionalIterator3d>
  inline
  void complex_fftw3d(InputBidirectionalIterator3d in_front_upper_left,
		   InputBidirectionalIterator3d in_back_bottom_right,
		   OutputBidirectionalIterator3d out_front_upper_left)
  {
    //recuperer la taille du container
    typename InputBidirectionalIterator3d::difference_type size3d = in_back_bottom_right - in_front_upper_left;
    //    typedef typename std::iterator_traits<InputBidirectionalIterator3d>::value_type complex;
    //    typedef typename complex::value_type real;

    std::size_t nb_sli = size3d[0];
    std::size_t nb_lig = size3d[1];
    std::size_t nb_col = size3d[2];
    assert((nb_sli > 1)&&(nb_lig > 1)&&(nb_col>1));

    //fft3d
    for(size_t k = 0; k < nb_sli; ++k)
      for(size_t i = 0; i < nb_lig; ++i)
      {
	slip::complex_fftw(in_front_upper_left.row_begin(k,i),in_front_upper_left.row_end(k,i),out_front_upper_left.row_begin(k,i));
      } 

     slip::Vector<std::complex<double> > Vtmp(nb_lig);
     for(size_t k = 0; k < nb_sli; ++k)
       for(size_t j = 0; j < nb_col; ++j)
 	{
	  slip::complex_fftw(out_front_upper_left.col_begin(k,j),out_front_upper_left.col_end(k,j),Vtmp.begin());
	  std::copy(Vtmp.begin(),Vtmp.end(),out_front_upper_left.col_begin(k,j));
 	}
     
     Vtmp.resize(nb_sli);
     for(size_t i = 0; i < nb_lig; ++i)
      for(size_t j = 0; j < nb_col; ++j)
	{
	  slip::complex_fftw(out_front_upper_left.slice_begin(i,j),out_front_upper_left.slice_end(i,j),Vtmp.begin());
	  std::copy(Vtmp.begin(),Vtmp.end(),out_front_upper_left.slice_begin(i,j));
	}
  }   
/**
 ** \brief Computes the complex fft3d of a container with 3d iterators.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/10/22
 ** \since 1.4.0
 ** \param in_front_upper_left
 ** \param in_back_bottom_right
 ** \param out_front_upper_left
 **
 ** \pre the 3d range must have the same sizes
 ** \pre input data have to be std::complex
 ** \pre out data have to be std::complex
 **
 ** \post the fft algorithm used is the fftw if enabled
 ** (--enable-fftw3 option of configuration) or the split radix one.
 ** \par Example:
 ** \code
 **   //input slip::Volume<std::complex<double> > M of dimensions dim1, dim2, dim3;
 **   slip::Volume<double> Out(M.dim1(),M.dim2(),M.dim3(),1);
 **   //FFT3D
 **   slip::Matrix3d<std::complex<double> > OFFT(M.dim1(),M.dim2(),M.dim3(),Czero);
 **   slip::complex_fft3d(M.front_upper_left(),M.back_bottom_right(),OFFT.front_upper_left());
 **   slip::Volume<double> NFFT(OFFT.dim1(),OFFT.dim2(),OFFT.dim3(),M.front_upper_left(),M.back_bottom_right());
 **   //  log(magnitude)
 **   for(size_t z = 0; z < OFFT.dim1(); ++z)
 **     for(size_t i = 0; i < OFFT.dim2(); ++i)
 **       for(size_t j = 0; j < OFFT.dim3(); ++j)
 **	      NFFT[z][i][j] = std::log(1.0+std::abs(OFFT[z][i][j]));
 **
 **   slip::fftshift3d(NFFT.front_upper_left(),NFFT.back_bottom_right());
 **   slip::change_dynamic_01(NFFT.begin(),NFFT.end(),NFFT.begin(),slip::AFFINE_FUNCTION);
 **   NFFT.write_raw("volume_fft.vol");
 **  \endcode
 **
 */
template<typename InputBidirectionalIterator3d,
typename OutputBidirectionalIterator3d>
inline
void complex_fft3d(InputBidirectionalIterator3d in_front_upper_left,
		InputBidirectionalIterator3d in_back_bottom_right,
		OutputBidirectionalIterator3d out_front_upper_left)
{
#ifdef HAVE_FFTW
	slip::complex_fftw3d(in_front_upper_left,in_back_bottom_right,out_front_upper_left);
#else
	slip::complex_split_radix_fft3d(in_front_upper_left,in_back_bottom_right,out_front_upper_left);
#endif
}


/**
 ** \brief Computes the complex fft3d of a container.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \param datain The input datas
 ** \param dataout The output datas
 **
 ** \pre datain.cols() == dataout.cols()
 ** \pre datain.rows() == dataout.rows()
 ** \pre datain.slices() == dataout.slices()
 ** \pre datain and dataout must have cols, rows and slices iterator
 ** \pre datain must be std::complex
 ** \pre dataout must be std::complex
 **
 ** \post the fft algorithm used is the fftw if enabled
 ** (--enable-fftw3 option of configuration) or the split radix one.
 ** \par Example:
 ** \code
 **   //input slip::Volume<std::complex<double> > M of dimensions dim1, dim2, dim3;
 **   slip::Volume<double> Out(M.dim1(),M.dim2(),M.dim3(),1);
 **   //FFT3D
 **   slip::Matrix3d<std::complex<double> > OFFT(M.dim1(),M.dim2(),M.dim3(),Czero);
 **   slip::complex_fft3d(M,OFFT);
 **   slip::Volume<double> NFFT(OFFT.dim1(),OFFT.dim2(),OFFT.dim3(),M.front_upper_left(),M.back_bottom_right());
 **   //  log(magnitude)
 **   for(size_t z = 0; z < OFFT.dim1(); ++z)
 **     for(size_t i = 0; i < OFFT.dim2(); ++i)
 **       for(size_t j = 0; j < OFFT.dim3(); ++j)
 **	      NFFT[z][i][j] = std::log(1.0+std::abs(OFFT[z][i][j]));
 **
 **   slip::fftshift3d(NFFT.front_upper_left(),NFFT.back_bottom_right());
 **   slip::change_dynamic_01(NFFT.begin(),NFFT.end(),NFFT.begin(),slip::AFFINE_FUNCTION);
 **   NFFT.write_raw("volume_fft.vol");
 **  \endcode
 */
template<typename Volume1, typename Volume2>
inline
void complex_fft3d(Volume1 &datain, Volume2 &dataout)
{
	assert(datain.cols() == dataout.cols());
	assert(datain.rows() == dataout.rows());
	assert(datain.slices() == dataout.slices());
#ifdef HAVE_FFTW
	slip::complex_fftw3d(datain.front_upper_left(),datain.back_bottom_right(),dataout.front_upper_left());
#else
	slip::complex_split_radix_fft3d(datain.front_upper_left(),datain.back_bottom_right(),dataout.front_upper_left());
#endif
}

  /**
   ** \brief Computes the ifftw3d of a container with 3d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_front_upper_left
   ** \param in_back_bottom_right
   ** \param out_front_upper_left
   **
   ** \pre the input data have to be std::complex
   ** \pre the 3d range must have the same sizes
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   **
   */
  template<typename InputBidirectionalIterator3d,
	   typename OutputBidirectionalIterator3d>
  inline
  void ifftw3d(InputBidirectionalIterator3d in_front_upper_left,
	       InputBidirectionalIterator3d in_back_bottom_right,
	       OutputBidirectionalIterator3d out_front_upper_left)
  {
    //recuperer la taille du container
    typename InputBidirectionalIterator3d::difference_type size3d = in_back_bottom_right - in_front_upper_left;
    
    std::size_t nb_sli = size3d[0];
    std::size_t nb_lig = size3d[1];
    std::size_t nb_col = size3d[2];
        
    //ifft3d
    for(size_t k = 0; k < nb_sli; ++k)
      for(size_t i = 0; i < nb_lig; ++i)
	{
	  slip::ifftw(in_front_upper_left.row_begin(k,i),in_front_upper_left.row_end(k,i),out_front_upper_left.row_begin(k,i));
	} 
    
    slip::Vector<std::complex<double> > Vtmp(nb_lig);
    for(size_t k = 0; k < nb_sli; ++k)
      for(size_t j = 0; j < nb_col; ++j)
 	{
 	  slip::ifftw(out_front_upper_left.col_begin(k,j),out_front_upper_left.col_end(k,j),Vtmp.begin());
 	  std::copy(Vtmp.begin(),Vtmp.end(),out_front_upper_left.col_begin(k,j));
 	}
    
    Vtmp.resize(nb_sli);
    for(size_t i = 0; i < nb_lig; ++i)
      for(size_t j = 0; j < nb_col; ++j)
	{
	  slip::ifftw(out_front_upper_left.slice_begin(i,j),out_front_upper_left.slice_end(i,j),Vtmp.begin());
	  std::copy(Vtmp.begin(),Vtmp.end(),out_front_upper_left.slice_begin(i,j));
	}
  } 

  /**
   ** \brief Computes the real fftw3d of a container 3d
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/03
   ** \since 1.0.0
   ** \version 0.0.1
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain.slices() == dataout.slices()
   ** \pre datain and dataout must have iterator3d available
   ** \pre datin and dataout must have the same size
   ** \pre the input data have to be real
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   **
   */
  template<typename InputMatrix3d,
	   typename OutputMatrix3d>
  inline
  void real_fftw3d(InputMatrix3d & datain,OutputMatrix3d & dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    assert(datain.slices() == dataout.slices());
    slip::real_fftw3d(datain.front_upper_left(),datain.back_bottom_right(),dataout.front_upper_left());
  } 

  /**
   ** \brief Computes the complex fftw3d of a container 3d
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/10
   ** \since 1.0.0
   ** \version 0.0.1
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain.slices() == dataout.slices()
   ** \pre datain and dataout must have iterator3d available
   ** \pre datin and dataout must have the same size
   ** \pre the input data have to be complex
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   **
   */
  template<typename InputMatrix3d,
	   typename OutputMatrix3d>
  inline
  void complex_fftw3d(InputMatrix3d & datain,OutputMatrix3d & dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    assert(datain.slices() == dataout.slices());
    slip::complex_fftw3d(datain.front_upper_left(),datain.back_bottom_right(),dataout.front_upper_left());
  } 

  /**
   ** \brief Computes the ifftw3d of a container 3d.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param datain The input datas
   ** \param dataout The output datas
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain.slices() == dataout.slices()
   ** \pre datain and dataout must have iterator3d available
   ** \pre datin and dataout must have the same size
   ** \pre the input data have to be std::complex
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   **
   */
  template<typename InputMatrix3d,
	   typename OutputMatrix3d>
  inline
  void ifftw3d(InputMatrix3d & datain,OutputMatrix3d & dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    assert(datain.slices() == dataout.slices());
    slip::ifftw3d(datain.front_upper_left(),datain.back_bottom_right(),dataout.front_upper_left());
  } 

  /**
   ** \brief Computes the Discrete Cosinus Transform II (forward) of a container (from fftw).
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   ** \post ouput data are normalized by \f$\frac{1}{\sqrt{2\times N}}\f$
   */
  template<class InputIter,class OutputIter>
  inline
  void fftw_dct(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    size_t length = in_end - in_begin;
    assert(length > 1);

    double *in = (double*) fftw_malloc(sizeof(double) * int(length));
    double *out = (double*) fftw_malloc(sizeof(double) * int(length));
    fftw_plan plan;
    fftw_r2r_kind kind = FFTW_REDFT10;

    std::copy(in_begin,in_end,in);
    plan  = fftw_plan_r2r_1d(int(length),in,out,kind,FFTW_ESTIMATE);
    fftw_execute(plan);
    std::transform(out,out+length,out_begin,std::bind2nd(std::divides<double>(),std::sqrt(2*length)));
    *(out_begin) *= slip::constants<double>::sqrt1_2();
    fftw_destroy_plan(plan);
    fftw_free(in);
    fftw_free(out);
  }

  /**
   ** \brief Computes the Discrete Cosinus Transform III (backward) of a container (from fftw).
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration)) 
   ** \post ouput data are normalized by \f$\frac{1}{\sqrt{2\times N}}\f$ 
   */
  template<class InputIter,class OutputIter>
  inline
  void fftw_idct(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    size_t length = in_end - in_begin;
    assert(length > 1);

    double *in = (double*) fftw_malloc(sizeof(double) * int(length));
    double *out = (double*) fftw_malloc(sizeof(double) * int(length));
    fftw_plan plan;
    fftw_r2r_kind kind = FFTW_REDFT01;

    std::copy(in_begin,in_end,in);
    *in *= slip::constants<double>::sqrt2(); 
    plan  = fftw_plan_r2r_1d(int(length),in,out,kind,FFTW_ESTIMATE);
    fftw_execute(plan);
    std::transform(out,out+length,out_begin,std::bind2nd(std::divides<double>(),std::sqrt(2*length)));

    fftw_destroy_plan(plan);
    fftw_free(in);
    fftw_free(out);
  }

 /*@} End fftw algorithms (from fftw3.h). */

#endif //HAVE_FFTW
  
  ////////////////////////////////// FFT Generic Algorithms/////////////////////////////////////////////////
  
  /**
   ** \name FFT Generic Algorithms
   */
  /*@{*/
  
  /**
   ** \brief Computes the real fft of a container.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate throw the input data
   ** \param in_end A BidirectionalIterator at the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate throw the output data
   **
   ** \pre size(in) == size(out).
   ** \pre input data have to be real
   ** \pre out data have to be std::complex
   **
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   ** \par Example:
   ** \code
   **  template <typename T>
   **  T aleat10(void)
   **  {
   **   return 10.0 * (rand()/static_cast<T>(RAND_MAX));
   **  }
   **  int main(int argc, char **argv)
   **  {
   **   typedef double real;
   **   typedef std::complex<real> cx;
   **   slip::Array<real> I(N,real(0));
   **   std::generate(I.begin(),I.end(),aleat10<real>);
   **   slip::Array<cx> b(N,cx(0));
   **   slip::real_fft(I.begin(),I.end(),b.begin());
   **   ...
   ** \endcode
   **
   */
  template<class InputIter, class OutputIter>
  inline
  void real_fft(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
#ifdef HAVE_FFTW
    slip::real_fftw(in_begin,in_end,out_begin);
#else
    slip::real_split_radix_fft(in_begin,in_end,out_begin);
#endif 
  }


  /**
   ** \brief Computes the complex fft of a container.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate throw the input data
   ** \param in_end A BidirectionalIterator at the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate throw the output data
   **
   ** \pre size(in) == size(out).
   ** \pre input data have to be std::complex
   ** \pre out data have to be std::complex
   **
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   ** \par Example:
   ** \link slip::real_fft(InputIter,InputIter,OutputIter)
   **  ...
   ** \endlink
   ** \code
   **  slip::Array<cx> Ic(N,cx(0));
   **  std::copy(I.begin(),I.end(),Ic.begin());
   **  slip::complex_fft(Ic.begin(),Ic.end(),b.begin());
   ** \endcode
   **
   **
   */
  template<class InputIter, class OutputIter>
  inline
  void complex_fft(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
#ifdef HAVE_FFTW
    slip::complex_fftw(in_begin,in_end,out_begin);
#else
    slip::complex_split_radix_fft(in_begin,in_end,out_begin);
#endif 
}
  
  /**
   ** \brief Computes the inverse fft of a container.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate throw the input data
   ** \param in_end A BidirectionalIterator at the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate throw the output data
   **
   ** \pre size(in) == size(out).
   ** \pre input data have to be std::complex
   ** \pre out data have to be std::complex
   **
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   ** \par Example:
   ** \link slip::real_fft(InputIter,InputIter,OutputIter)
   **  ...
   ** \endlink
   ** \code
   **  slip::Array<cx> b2(N,cx(0));
   **  slip::ifft(b.begin(),b.end(),b2.begin());
   ** \endcode
   **
   */
  template<class InputIter, class OutputIter>
  inline
  void ifft(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
#ifdef HAVE_FFTW
    slip::ifftw(in_begin,in_end,out_begin);
#else
    slip::split_radix_ifft(in_begin,in_end,out_begin);
#endif 
  }
  
  /**
   ** \brief Computes the real fft2d of a container with 2d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_upper_left
   ** \param in_bottom_right
   ** \param out_upper_left
   **
   ** \pre the 2d range must have the same sizes
   ** \pre input data have to be real
   ** \pre out data have to be std::complex
   **
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   ** \par Example:
   ** \code
   **  slip::GrayscaleImage<double> I;
   **  I.read("lena.bmp");
   **  //fft2d
   **  slip::Matrix<std::complex<double> > IFFT(I.dim1(),I.dim2());
   **  slip::real_fft2d(I.upper_left(),I.bottom_right(),IFFT.upper_left());
   **  slip::GrayscaleImage<double> INorm(IFFT.dim1(),IFFT.dim2());
   **  //log(magnitude)
   **  for(size_t i = 0; i < IFFT.dim1(); ++i)
   **     for(size_t j = 0; j < IFFT.dim2(); ++j)
   **        INorm[i][j] = std::log(1.0+std::abs(IFFT[i][j]));
   **  slip::fftshift2d(INorm.upper_left(),INorm.bottom_right());
   **  slip::change_dynamic_01(INorm.begin(),INorm.end(),INorm.begin(),slip::AFFINE_FUNCTION);
   **  INorm.write("lena_fft.jpg");
   ** \endcode
   **
   */
  template<typename InputBidirectionalIterator2d,
	   typename OutputBidirectionalIterator2d>
  inline
  void real_fft2d(InputBidirectionalIterator2d in_upper_left,
		  InputBidirectionalIterator2d in_bottom_right,
		  OutputBidirectionalIterator2d out_upper_left)
  {
#ifdef HAVE_FFTW
    slip::real_fftw2d(in_upper_left,in_bottom_right,out_upper_left);
#else
    slip::real_split_radix_fft2d(in_upper_left,in_bottom_right,out_upper_left);
#endif 
  } 

  /**
   ** \brief Computes the complex fft2d of a container with 2d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_upper_left
   ** \param in_bottom_right
   ** \param out_upper_left
   **
   ** \pre the 2d range must have the same sizes
   ** \pre input data have to be std::complex
   ** \pre out data have to be std::complex
   **
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   **
   */
  template<typename InputBidirectionalIterator2d,
	   typename OutputBidirectionalIterator2d>
  inline
  void complex_fft2d(InputBidirectionalIterator2d in_upper_left,
		     InputBidirectionalIterator2d in_bottom_right,
		     OutputBidirectionalIterator2d out_upper_left)
  {
#ifdef HAVE_FFTW
    slip::complex_fftw2d(in_upper_left,in_bottom_right,out_upper_left);
#else
    slip::complex_split_radix_fft2d(in_upper_left,in_bottom_right,out_upper_left);
#endif 
  }

  /**
   ** \brief Computes the ifft2d of a container with 2d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_upper_left
   ** \param in_bottom_right
   ** \param out_upper_left
   **
   ** \pre the input data have to be std::complex
   ** \pre the 2d range must have the same sizes
   **
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   ** \par Example:
   ** \link slip::real_fft2d(InputBidirectionalIterator2d,InputBidirectionalIterator2d,OutputBidirectionalIterator2d)
   **  ...
   ** \endlink
   ** \code
   **  slip::Matrix<std::complex<double> > IOut(I.dim1(),I.dim2());
   **  //ifft2d
   **  slip::ifft2d(IFFT.upper_left(),IFFT.bottom_right(),IOut.upper_left());
   **  for(size_t i = 0; i < IOut.dim1(); ++i)
   **     for(size_t j = 0; j < IOut.dim2(); ++j)
   **	      INorm[i][j] = std::abs(IOut[i][j]);
   **  slip::change_dynamic_01(INorm.begin(),INorm.end(),INorm.begin(),slip::AFFINE_FUNCTION);
   **  INorm.write("lena_ifft.png");
   ** \endcode
   */
  template<typename InputBidirectionalIterator2d,
	   typename OutputBidirectionalIterator2d>
  inline
  void ifft2d(InputBidirectionalIterator2d in_upper_left,
	      InputBidirectionalIterator2d in_bottom_right,
	      OutputBidirectionalIterator2d out_upper_left)
  {
#ifdef HAVE_FFTW
    slip::ifftw2d(in_upper_left,in_bottom_right,out_upper_left);
#else
    slip::split_radix_ifft2d(in_upper_left,in_bottom_right,out_upper_left);
#endif
  } 

  /**
   ** \brief Computes the real fft2d of a container.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param datain The input datas
   ** \param dataout The output datas
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain and dataout must have cols and rows iterator
   ** \pre datain must be real
   ** \pre dataout must be std::complex
   ** 
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   ** \par Example:
   ** \code
   **  slip::GrayscaleImage<double> I;
   **  I.read("lena.bmp");
   **  //fft2d
   **  slip::Matrix<std::complex<double> > IFFT(I.dim1(),I.dim2());
   **  slip::real_fft2d(I,IFFT);
   **  slip::GrayscaleImage<double> INorm(IFFT.dim1(),IFFT.dim2());
   **  //log(magnitude)
   **  for(size_t i = 0; i < IFFT.dim1(); ++i)
   **     for(size_t j = 0; j < IFFT.dim2(); ++j)
   **        INorm[i][j] = std::log(1.0+std::abs(IFFT[i][j]));
   **  slip::fftshift2d(INorm.upper_left(),INorm.bottom_right());
   **  slip::change_dynamic_01(INorm.begin(),INorm.end(),INorm.begin(),slip::AFFINE_FUNCTION);
   **  INorm.write("lena_fft.jpg");
   ** \endcode
   */
  template<typename Matrix1, typename Matrix2>
  inline
  void real_fft2d(Matrix1 &datain, Matrix2 &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
#ifdef HAVE_FFTW
    slip::real_fftw2d(datain.upper_left(),datain.bottom_right(),dataout.upper_left());
#else
    slip::real_split_radix_fft2d(datain.upper_left(),datain.bottom_right(),dataout.upper_left());
#endif
  }

  /**
   ** \brief Computes the complex fft2d of a container.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param datain The input datas
   ** \param dataout The output datas
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain and dataout must have cols and rows iterator
   ** \pre datain must be std::complex
   ** \pre dataout must be std::complex
   ** 
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   */
  template<typename Matrix1, typename Matrix2>
  inline
  void complex_fft2d(Matrix1 &datain, Matrix2 &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
#ifdef HAVE_FFTW
    slip::complex_fftw2d(datain.upper_left(),datain.bottom_right(),dataout.upper_left());
#else
    slip::complex_split_radix_fft2d(datain.upper_left(),datain.bottom_right(),dataout.upper_left());
#endif
  }

  /**
   ** \brief Computes the fft2d of a container.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param datain The input datas
   ** \param dataout The output datas
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain and dataout must have cols and rows iterator
   ** \pre datain must be std::complex
   ** \pre dataout must be std::complex
   ** 
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   ** \par Example:
   ** \link slip::real_fft2d(Matrix1 &datain, Matrix2 &dataout)
   **  ...
   ** \endlink
   ** \code
   **  slip::Matrix<std::complex<double> > IOut(I.dim1(),I.dim2());
   **  //ifft2d
   **  slip::ifft2d(IFFT,IOut);
   **  for(size_t i = 0; i < IOut.dim1(); ++i)
   **     for(size_t j = 0; j < IOut.dim2(); ++j)
   **	      INorm[i][j] = std::abs(IOut[i][j]);
   **  slip::change_dynamic_01(INorm.begin(),INorm.end(),INorm.begin(),slip::AFFINE_FUNCTION);
   **  INorm.write("lena_ifft.png");
   ** \endcode
   **
   */
  template<typename Matrix1, typename Matrix2>
  inline
  void ifft2d(Matrix1 &datain, Matrix2 &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
#ifdef HAVE_FFTW
    slip::ifftw2d(datain.upper_left(),datain.bottom_right(),dataout.upper_left());
#else
    slip::split_radix_ifft2d(datain.upper_left(),datain.bottom_right(),dataout.upper_left());
#endif
  }
 
  /**
   ** \brief Computes the real fft3d of a container with 3d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_front_upper_left
   ** \param in_back_bottom_right
   ** \param out_front_upper_left
   **
   ** \pre the 3d range must have the same sizes
   ** \pre input data have to be real
   ** \pre out data have to be std::complex
   **
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   ** \par Example:
   ** \code
   **   slip::Volume<double> M;
   **   M.read_raw("volume.vol",dim1,dim2,dim3);
   **   slip::Volume<double> Out(M.dim1(),M.dim2(),M.dim3(),1);
   **   //FFT3D
   **   slip::Matrix3d<std::complex<double> > OFFT(M.dim1(),M.dim2(),M.dim3(),Czero);
   **   slip::real_fft3d(M.front_upper_left(),M.back_bottom_right(),OFFT.front_upper_left());
   **   slip::Volume<double> NFFT(OFFT.dim1(),OFFT.dim2(),OFFT.dim3(),M.front_upper_left(),M.back_bottom_right());
   **   //  log(magnitude)
   **   for(size_t z = 0; z < OFFT.dim1(); ++z)
   **     for(size_t i = 0; i < OFFT.dim2(); ++i)
   **       for(size_t j = 0; j < OFFT.dim3(); ++j)
   **	      NFFT[z][i][j] = std::log(1.0+std::abs(OFFT[z][i][j]));
   **
   **   slip::fftshift3d(NFFT.front_upper_left(),NFFT.back_bottom_right());
   **   slip::change_dynamic_01(NFFT.begin(),NFFT.end(),NFFT.begin(),slip::AFFINE_FUNCTION);
   **   NFFT.write_raw("volume_fft.vol");
   **  \endcode
   **
   */
  template<typename InputBidirectionalIterator3d,
	   typename OutputBidirectionalIterator3d>
  inline
  void real_fft3d(InputBidirectionalIterator3d in_front_upper_left,
		  InputBidirectionalIterator3d in_back_bottom_right,
		  OutputBidirectionalIterator3d out_front_upper_left)
  {
#ifdef HAVE_FFTW
    slip::real_fftw3d(in_front_upper_left,in_back_bottom_right,out_front_upper_left);
#else
    slip::real_split_radix_fft3d(in_front_upper_left,in_back_bottom_right,out_front_upper_left);
#endif 
  } 

  /**
   ** \brief Computes the ifft3d of a container with 3d iterators.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_front_upper_left
   ** \param in_back_bottom_right
   ** \param out_front_upper_left
   **
   ** \pre the input data have to be std::complex
   ** \pre the 3d range must have the same sizes
   **
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   ** \par Example:
   ** \link slip::real_fft3d(InputBidirectionalIterator3d,InputBidirectionalIterator3d,OutputBidirectionalIterator3d)
   **  ...
   ** \endlink
   ** \code
   **  std::complex<double> Czero(0,0);
   **  slip::Matrix3d<std::complex<double> > OC(M.dim1(),M.dim2(),M.dim3(),Czero);
   **  slip::Volume<double> Out(M.dim1(),M.dim2(),M.dim3(),1);
   **  slip::ifft3d(OFFT.front_upper_left(),OFFT.back_bottom_right(),OC.front_upper_left());
   **  for(size_t z = 0; z < OFFT.dim1(); ++z)
   **	for(size_t i = 0; i < OFFT.dim2(); ++i)
   **	  for(size_t j = 0; j < OFFT.dim3(); ++j)
   **	    {
   **	      Out[z][i][j] = std::abs(OC[z][i][j]);
   **	    }
   **  slip::change_dynamic_01(Out.begin(),Out.end(),Out.begin(),slip::AFFINE_FUNCTION);
   **  Out.write_raw("volume_ifft.vol");
   ** \endcode
   */
  template<typename InputBidirectionalIterator3d,
	   typename OutputBidirectionalIterator3d>
  inline
  void ifft3d(InputBidirectionalIterator3d in_front_upper_left,
	      InputBidirectionalIterator3d in_back_bottom_right,
	      OutputBidirectionalIterator3d out_front_upper_left)
  {
#ifdef HAVE_FFTW
    slip::ifftw3d(in_front_upper_left,in_back_bottom_right,out_front_upper_left);
#else
    slip::split_radix_ifft3d(in_front_upper_left,in_back_bottom_right,out_front_upper_left);
#endif
  } 

  /**
   ** \brief Computes the real fft3d of a container.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param datain The input datas
   ** \param dataout The output datas
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain.slices() == dataout.slices()
   ** \pre datain and dataout must have cols, rows and slices iterator
   ** \pre datain must be real
   ** \pre dataout must be std::complex
   ** 
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   ** \par Example:
   ** \code
   **   slip::Volume<double> M;
   **   M.read_raw("volume.vol",dim1,dim2,dim3);
   **   slip::Volume<double> Out(M.dim1(),M.dim2(),M.dim3(),1);
   **   //FFT3D
   **   slip::Matrix3d<std::complex<double> > OFFT(M.dim1(),M.dim2(),M.dim3(),Czero);
   **   slip::real_fft3d(M,OFFT);
   **   slip::Volume<double> NFFT(OFFT.dim1(),OFFT.dim2(),OFFT.dim3(),M.front_upper_left(),M.back_bottom_right());
   **   //  log(magnitude)
   **   for(size_t z = 0; z < OFFT.dim1(); ++z)
   **     for(size_t i = 0; i < OFFT.dim2(); ++i)
   **       for(size_t j = 0; j < OFFT.dim3(); ++j)
   **	      NFFT[z][i][j] = std::log(1.0+std::abs(OFFT[z][i][j]));
   **
   **   slip::fftshift3d(NFFT.front_upper_left(),NFFT.back_bottom_right());
   **   slip::change_dynamic_01(NFFT.begin(),NFFT.end(),NFFT.begin(),slip::AFFINE_FUNCTION);
   **   NFFT.write_raw("volume_fft.vol");
   **  \endcode
   */
  template<typename Volume1, typename Volume2>
  inline
  void real_fft3d(Volume1 &datain, Volume2 &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    assert(datain.slices() == dataout.slices());
#ifdef HAVE_FFTW
    slip::real_fftw3d(datain.front_upper_left(),datain.back_bottom_right(),dataout.front_upper_left());
#else
    slip::real_split_radix_fft3d(datain.front_upper_left(),datain.back_bottom_right(),dataout.front_upper_left());
#endif
  }

  /**
   ** \brief Computes the fft3d of a container.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param datain The input datas
   ** \param dataout The output datas
   **
   ** \pre datain.cols() == dataout.cols()
   ** \pre datain.rows() == dataout.rows()
   ** \pre datain.slices() == dataout.slices()
   ** \pre datain and dataout must have cols, rows and slices iterator
   ** \pre datain must be std::complex
   ** \pre dataout must be std::complex
   ** 
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   ** \par Example:
   ** \link slip::real_fft3d(Volume1&,Volume2&)
   **  ...
   ** \endlink
   ** \code
   **  std::complex<double> Czero(0,0);
   **  slip::Matrix3d<std::complex<double> > OC(M.dim1(),M.dim2(),M.dim3(),Czero);
   **  slip::Volume<double> Out(M.dim1(),M.dim2(),M.dim3(),1);
   **  slip::ifft3d(OFFT,OC);
   **  for(size_t z = 0; z < OFFT.dim1(); ++z)
   **	for(size_t i = 0; i < OFFT.dim2(); ++i)
   **	  for(size_t j = 0; j < OFFT.dim3(); ++j)
   **	    {
   **	      Out[z][i][j] = std::abs(OC[z][i][j]);
   **	    }
   **  slip::change_dynamic_01(Out.begin(),Out.end(),Out.begin(),slip::AFFINE_FUNCTION);
   **  Out.write_raw("volume_ifft.vol");
   ** \endcode
   **
   */
  template<typename Volume1, typename Volume2>
  inline
  void ifft3d(Volume1 &datain, Volume2 &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    assert(datain.slices() == dataout.slices());
#ifdef HAVE_FFTW
    slip::ifftw3d(datain.front_upper_left(),datain.back_bottom_right(),dataout.front_upper_left());
#else
    slip::split_radix_ifft3d(datain.front_upper_left(),datain.back_bottom_right(),dataout.front_upper_left());
#endif
  }
 
  /**
   ** \brief Computes the Discrete Cosinus Transform II (forward) of a container
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   ** \post ouput data are normalized by \f$\frac{1}{\sqrt{2\times N}}\f$
   ** \par Example:
   ** \code
   **  typedef double tt;
   **  tt a[] = { tt(0), tt(1), tt(3), tt(4), tt(4), tt(3),tt(1),tt(0)};
   **  tt b[8];
   **  slip::dct(a,a+8,b);
   **  for(int i = 0; i < 8; ++i)
   **      std::cout<<b[i]<<" ";
   **  std::cout<<std::endl;
   ** \endcode
   */
  template<class InputIter,class OutputIter>
  inline
  void dct(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
#ifdef HAVE_FFTW
    slip::fftw_dct(in_begin,in_end,out_begin);
#else
      slip::split_radix_dct(in_begin,in_end,out_begin);
#endif
 }

  /**
   ** \brief Computes the Discrete Cosinus Transform III (backward) of a container
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2008/04/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \post the fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one.
   ** \post ouput data are normalized by \f$\frac{1}{\sqrt{2\times N}}\f$ 
    ** \par Example:
   ** \link slip::dct(InputIter,InputIter,OutputIter)
   **  ...
   ** \endlink
   ** \code
   **  tt b2[8];
   **  slip::idct(b,b+8,b2);
   **  for(int i = 0; i < 8; ++i)
   **     std::cout<<b2[i]<<" ";
   **  std::cout<<std::endl<<std::endl;
   ** \endcode
   */
  template<class InputIter,class OutputIter>
  inline
  void idct(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
#ifdef HAVE_FFTW
    slip::fftw_idct(in_begin,in_end,out_begin);
#else
    slip::split_radix_idct(in_begin,in_end,out_begin);
#endif
  }


/**
 ** \name FFT Tools Functions
 */
/*@{*/

/*!
 ** \version Fluex 1.0
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \brief Performs a shift of a 4d container, for use with the fft4d and ifft4d
 **        functions, in order to move the frequency 0 to the center of
 **        the container.
 ** \param first_front_upper_left A ForwardIterator4D.
 ** \param last_back_bottom_right  A ForwardIterator4D.
 **
 */
template<class ForwardIterator4D>
inline
void fftshift4d(ForwardIterator4D first_front_upper_left,
		ForwardIterator4D last_back_bottom_right)
{
	typename ForwardIterator4D::difference_type size4d = last_back_bottom_right - first_front_upper_left;

	std::size_t nb_sla = size4d[0];
	std::size_t nb_sli = size4d[1];
	std::size_t nb_lig = size4d[2];
	std::size_t nb_col = size4d[3];

	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t i = 0; i < nb_lig; ++i)
			{
				slip::fftshift(first_front_upper_left.row_begin(t,k,i),first_front_upper_left.row_end(t,k,i));
			}

	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::fftshift(first_front_upper_left.col_begin(t,k,j),first_front_upper_left.col_end(t,k,j));
			}
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::fftshift(first_front_upper_left.slice_begin(t,i,j),first_front_upper_left.slice_end(t,i,j));
			}
	for(size_t k = 0; k < nb_sli; ++k)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::fftshift(first_front_upper_left.slab_begin(k,i,j),first_front_upper_left.slab_end(k,i,j));
			}
}

/*@} End FFT Tools Functions */

/**
 ** \name FFT Split-Radix algorithms (from FFTPACK.hpp).
 */
/*@{*/


/**
 ** \brief Computes the real split-radix fft4d of a container with 4d iterators.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \param in_first_front_upper_left input start iterator4d
 ** \param in_last_back_bottom_right input end interator4d
 ** \param out_first_front_upper_left output start iterator4d
 **
 ** \pre the input data have to be real (not complex)
 ** \pre the input and output container must have the same sizes
 ** \par Example:
 ** \code
 **   //input slip::HyperHyperVolume<double> M of dimensions dim1, dim2, dim3 and dim4;
 **   slip::HyperHyperVolume<double> Out(M.dim1(),M.dim2(),M.dim3(),M.dim4(),1);
 **   //FFT4D
 **   slip::Matrix4d<std::complex<double> > OFFT(M.dim1(),M.dim2(),M.dim3(),M.dim4(),std::complex<double>(0,0));
 **   slip::real_split_radix_fft4d(M.first_front_upper_left(),M.last_back_bottom_right(),OFFT.first_front_upper_left());
 **   slip::HyperHyperVolume<double> NFFT(OFFT.dim1(),OFFT.dim2(),OFFT.dim3(),OFFT.dim4(),M.first_front_upper_left(),M.last_back_bottom_right());
 **   //  log(magnitude)
 **   for(size_t t = 0; t < OFFT.dim1(); ++t)
 **    for(size_t k = 0; k < OFFT.dim2(); ++k)
 **     for(size_t i = 0; i < OFFT.dim3(); ++i)
 **       for(size_t j = 0; j < OFFT.dim4(); ++j)
 **	       NFFT[t][k][i][j] = std::log(1.0+std::abs(OFFT[t][k][i][j]));
 **
 **   slip::fftshift4d(NFFT.first_front_upper_left(),NFFT.last_back_bottom_right());
 **   slip::change_dynamic_01(NFFT.begin(),NFFT.end(),NFFT.begin(),slip::AFFINE_FUNCTION);
 **  \endcode
 */
template<typename InputBidirectionalIterator4d,
typename OutputBidirectionalIterator4d>
inline
void real_split_radix_fft4d(InputBidirectionalIterator4d in_first_front_upper_left,
		InputBidirectionalIterator4d in_last_back_bottom_right,
		OutputBidirectionalIterator4d out_first_front_upper_left)
{
	//recuperer la taille du container
	typename InputBidirectionalIterator4d::difference_type size4d = in_last_back_bottom_right - in_first_front_upper_left;
	//	typedef typename std::iterator_traits<InputBidirectionalIterator4d>::value_type real;
	//typedef typename std::iterator_traits<OutputBidirectionalIterator4d>::value_type complex;

	std::size_t nb_sla = size4d[0];
	std::size_t nb_sli = size4d[1];
	std::size_t nb_lig = size4d[2];
	std::size_t nb_col = size4d[3];
	//fft4d
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t i = 0; i < nb_lig; ++i)
			{
				slip::real_split_radix_fft(in_first_front_upper_left.row_begin(t,k,i),
						in_first_front_upper_left.row_end(t,k,i),out_first_front_upper_left.row_begin(t,k,i));
			}

	slip::Vector<std::complex<double> > Vtmp(nb_lig);
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::complex_split_radix_fft(out_first_front_upper_left.col_begin(t,k,j),
						out_first_front_upper_left.col_end(t,k,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.col_begin(t,k,j));
			}

	Vtmp.resize(nb_sli);
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::complex_split_radix_fft(out_first_front_upper_left.slice_begin(t,i,j),
						out_first_front_upper_left.slice_end(t,i,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.slice_begin(t,i,j));
			}

	Vtmp.resize(nb_sla);
	for(size_t k = 0; k < nb_sli; ++k)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::complex_split_radix_fft(out_first_front_upper_left.slab_begin(k,i,j),
						out_first_front_upper_left.slab_end(k,i,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.slab_begin(k,i,j));
			}
}

/**
 ** \brief Computes the complex split-radix fft4d of a container with 4d iterators.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \param in_first_front_upper_left input start iterator4d
 ** \param in_last_back_bottom_right input end interator4d
 ** \param out_first_front_upper_left output start iterator4d
 **
 ** \pre the input data have to be complex
 ** \pre the input and output container must have the same sizes
 */
template<typename InputBidirectionalIterator4d,
typename OutputBidirectionalIterator4d>
inline
void complex_split_radix_fft4d(InputBidirectionalIterator4d in_first_front_upper_left,
		InputBidirectionalIterator4d in_last_back_bottom_right,
		OutputBidirectionalIterator4d out_first_front_upper_left)
{
	//recuperer la taille du container
	typename InputBidirectionalIterator4d::difference_type size4d = in_last_back_bottom_right - in_first_front_upper_left;
	//	typedef typename std::iterator_traits<InputBidirectionalIterator4d>::value_type complex;

	std::size_t nb_sla = size4d[0];
	std::size_t nb_sli = size4d[1];
	std::size_t nb_lig = size4d[2];
	std::size_t nb_col = size4d[3];
	//fft4d
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t i = 0; i < nb_lig; ++i)
			{
				slip::complex_split_radix_fft(in_first_front_upper_left.row_begin(t,k,i),
						in_first_front_upper_left.row_end(t,k,i),out_first_front_upper_left.row_begin(t,k,i));
			}

	slip::Vector<std::complex<double> > Vtmp(nb_lig);
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::complex_split_radix_fft(out_first_front_upper_left.col_begin(t,k,j),
						out_first_front_upper_left.col_end(t,k,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.col_begin(t,k,j));
			}

	Vtmp.resize(nb_sli);
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::complex_split_radix_fft(out_first_front_upper_left.slice_begin(t,i,j),
						out_first_front_upper_left.slice_end(t,i,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.slice_begin(t,i,j));
			}

	Vtmp.resize(nb_sla);
	for(size_t k = 0; k < nb_sli; ++k)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::complex_split_radix_fft(out_first_front_upper_left.slab_begin(k,i,j),
						out_first_front_upper_left.slab_end(k,i,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.slab_begin(k,i,j));
			}
}

/**
 ** \brief Computes the split-radix ifft4d of a container with 4d iterators.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \param in_first_front_upper_left
 ** \param in_last_back_bottom_right
 ** \param out_first_front_upper_left
 **
 ** \pre the input data have to be std::complex
 ** \pre the 4d range must have the same sizes
 ** \par Example:
 ** \link slip::real_split_radix_fft4d(InputBidirectionalIterator4d,InputBidirectionalIterator4d,OutputBidirectionalIterator4d)
 **  ...
 ** \endlink
 ** \code
 **  std::complex<double> Czero(0,0);
 **  slip::Matrix4d<std::complex<double> > OC(M.dim1(),M.dim2(),M.dim3(),M.dim4(),Czero);
 **  slip::HyperHyperVolume<double> Out(M.dim1(),M.dim2(),M.dim3(),M.dim4(),1);
 **  slip::split_radix_ifft4d(OFFT.first_front_upper_left(),OFFT.last_back_bottom_right(),OC.first_front_upper_left());
 **  for(size_t t = 0; t < OFFT.dim1(); ++t)
 **   for(size_t k = 0; k < OFFT.dim2(); ++k)
 **    for(size_t i = 0; i < OFFT.dim3(); ++i)
 **	    for(size_t j = 0; j < OFFT.dim4(); ++j)
 **	     {
 **	       Out[t][k][i][j] = std::abs(OC[t][k][i][j]);
 **	     }
 **  slip::change_dynamic_01(Out.begin(),Out.end(),Out.begin(),slip::AFFINE_FUNCTION);
 ** \endcode
 **
 */
template<typename InputBidirectionalIterator4d,
typename OutputBidirectionalIterator4d>
inline
void split_radix_ifft4d(InputBidirectionalIterator4d in_first_front_upper_left,
		InputBidirectionalIterator4d in_last_back_bottom_right,
		OutputBidirectionalIterator4d out_first_front_upper_left)
{
	//recuperer la taille du container
	typename InputBidirectionalIterator4d::difference_type size4d = in_last_back_bottom_right - in_first_front_upper_left;

	std::size_t nb_sla = size4d[0];
	std::size_t nb_sli = size4d[1];
	std::size_t nb_lig = size4d[2];
	std::size_t nb_col = size4d[3];

	//ifft4d
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t i = 0; i < nb_lig; ++i)
			{
				slip::split_radix_ifft(in_first_front_upper_left.row_begin(t,k,i),
						in_first_front_upper_left.row_end(t,k,i),out_first_front_upper_left.row_begin(t,k,i));
			}

	slip::Vector<std::complex<double> > Vtmp(nb_lig);
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::split_radix_ifft(out_first_front_upper_left.col_begin(t,k,j),
						out_first_front_upper_left.col_end(t,k,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.col_begin(t,k,j));
			}

	Vtmp.resize(nb_sli);
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::split_radix_ifft(out_first_front_upper_left.slice_begin(t,i,j),
						out_first_front_upper_left.slice_end(t,i,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.slice_begin(t,i,j));
			}

	Vtmp.resize(nb_sla);
	for(size_t k = 0; k < nb_sli; ++k)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::split_radix_ifft(out_first_front_upper_left.slab_begin(k,i,j),
						out_first_front_upper_left.slab_end(k,i,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.slab_begin(k,i,j));
			}
}



/**
 ** \brief Computes the real split-radix fft4d of a container 4d
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \pre datain.cols() == dataout.cols()
 ** \pre datain.rows() == dataout.rows()
 ** \pre datain.slices() == dataout.slices()
 ** \pre datain and dataout must have iterator4d available
 ** \pre datin and dataout must have the same size
 ** \pre the input data have to be real
 ** \par Example:
 ** \code
 ** //input slip::HyperHyperVolume<double> M of dimensions dim1, dim2, dim3 and dim4;
 **   slip::HyperHyperVolume<double> Out(M.dim1(),M.dim2(),M.dim3(),M.dim4(),1);
 **   //FFT4D
 **   slip::Matrix4d<std::complex<double> > OFFT(M.dim1(),M.dim2(),M.dim3(),M.dim4(),Czero);
 **   slip::real_split_radix_fft4d(M,OFFT);
 **   slip::HyperVolume<double> NFFT(OFFT.dim1(),OFFT.dim2(),OFFT.dim3(),OFFT.dim4(),M.first_front_upper_left(),
 **   M.last_back_bottom_right());
 **   //  log(magnitude)
 **   for(size_t t = 0; t < OFFT.dim1(); ++t)
 **    for(size_t k = 0; k < OFFT.dim2(); ++k)
 **     for(size_t i = 0; i < OFFT.dim3(); ++i)
 **	     for(size_t j = 0; j < OFFT.dim4(); ++j)
 **	       NFFT[t][k][i][j] = std::log(1.0+std::abs(OFFT[t][k][i][j]));
 **
 **   slip::fftshift4d(NFFT.first_front_upper_left(),NFFT.last_back_bottom_right());
 **   slip::change_dynamic_01(NFFT.begin(),NFFT.end(),NFFT.begin(),slip::AFFINE_FUNCTION);
 **  \endcode
 **
 */
template<typename InputMatrix4d,
typename OutputMatrix4d>
inline
void real_split_radix_fft4d(const InputMatrix4d & datain,OutputMatrix4d & dataout)
{
	assert(datain.cols() == dataout.cols());
	assert(datain.rows() == dataout.rows());
	assert(datain.slices() == dataout.slices());
	real_split_radix_fft4d(datain.first_front_upper_left(),datain.last_back_bottom_right(),
			dataout.first_front_upper_left());
}

/**
 ** \brief Computes the complex split-radix fft4d of a container 4d
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/10/22
 ** \since 1.4.0
 ** \pre datain.cols() == dataout.cols()
 ** \pre datain.rows() == dataout.rows()
 ** \pre datain.slices() == dataout.slices()
 ** \pre datain and dataout must have iterator4d available
 ** \pre datin and dataout must have the same size
 ** \pre the input data have to be complex
 **
 */
template<typename InputMatrix4d,
typename OutputMatrix4d>
inline
void complex_split_radix_fft4d(const InputMatrix4d & datain,OutputMatrix4d & dataout)
{
	assert(datain.cols() == dataout.cols());
	assert(datain.rows() == dataout.rows());
	assert(datain.slices() == dataout.slices());
	complex_split_radix_fft4d(datain.first_front_upper_left(),datain.last_back_bottom_right(),
			dataout.first_front_upper_left());
}

/**
 ** \brief Computes the split-radix ifft4d of a container 4d.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \param datain The input datas
 ** \param dataout The output datas
 **
 ** \pre datain.cols() == dataout.cols()
 ** \pre datain.rows() == dataout.rows()
 ** \pre datain.slices() == dataout.slices()
 ** \pre datain and dataout must have iterator4d available
 ** \pre datin and dataout must have the same size
 ** \pre the input data have to be std::complex
 ** \par Example:
 ** \link slip::real_split_radix_fft4d(InputMatrix4d & datain,OutputMatrix4d & dataout)
 **  ...
 ** \endlink
 ** \code
 **  std::complex<double> Czero(0,0);
 **  slip::Matrix4d<std::complex<double> > OC(M.dim1(),M.dim2(),M.dim3(),M.dim4(),Czero);
 **  slip::HyperHyperVolume<double> Out(M.dim1(),M.dim2(),M.dim3(),M.dim4(),1);
 **  slip::split_radix_ifft4d(OFFT,OC);
 **   for(size_t t = 0; t < OFFT.dim1(); ++t)
 **    for(size_t k = 0; k < OFFT.dim2(); ++k)
 **     for(size_t i = 0; i < OFFT.dim3(); ++i)
 **	     for(size_t j = 0; j < OFFT.dim4(); ++j)
 **	    {
 **	      Out[t][k][i][j] = std::abs(OC[t][k][i][j]);
 **	    }
 **  slip::change_dynamic_01(Out.begin(),Out.end(),Out.begin(),slip::AFFINE_FUNCTION);
 ** \endcode
 **
 */
template<typename InputMatrix4d,
typename OutputMatrix4d>
inline
void split_radix_ifft4d(const InputMatrix4d & datain,OutputMatrix4d & dataout)
{
	assert(datain.cols() == dataout.cols());
	assert(datain.rows() == dataout.rows());
	assert(datain.slices() == dataout.slices());
	split_radix_ifft4d(datain.first_front_upper_left(),datain.last_back_bottom_right(),dataout.first_front_upper_left());
}

/*@} End FFT Split-Radix algorithms (from FFTPACK.hpp). */

#ifdef HAVE_FFTW  

/**
 ** \name fftw algorithms (from fftw3.h).
 */
/*@{*/

/**
 ** \brief Computes the real fftw4d of a container with 4d iterators.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \param in_first_front_upper_left
 ** \param in_last_back_bottom_right
 ** \param out_first_front_upper_left
 **
 ** \pre the input data have to be real (not complex)
 ** \pre the 4d range must have the same sizes
 ** \pre the HAVE_FFTW macro must be defined
 **
 */
template<typename InputBidirectionalIterator4d,
typename OutputBidirectionalIterator4d>
inline
void real_fftw4d(InputBidirectionalIterator4d in_first_front_upper_left,
		InputBidirectionalIterator4d in_last_back_bottom_right,
		OutputBidirectionalIterator4d out_first_front_upper_left)
{
	//recuperer la taille du container
	typename InputBidirectionalIterator4d::difference_type size4d = in_last_back_bottom_right - in_first_front_upper_left;
	//	typedef typename std::iterator_traits<InputBidirectionalIterator4d>::value_type real;
	//	typedef typename std::iterator_traits<OutputBidirectionalIterator4d>::value_type complex;

	std::size_t nb_sla = size4d[0];
	std::size_t nb_sli = size4d[1];
	std::size_t nb_lig = size4d[2];
	std::size_t nb_col = size4d[3];
	assert((nb_sla > 1)&&(nb_sli > 1)&&(nb_lig > 1)&&(nb_col>1));
	//fft4d
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t i = 0; i < nb_lig; ++i)
			{
				slip::real_fftw(in_first_front_upper_left.row_begin(t,k,i),
						in_first_front_upper_left.row_end(t,k,i),out_first_front_upper_left.row_begin(t,k,i));
			}

	slip::Vector<std::complex<double> > Vtmp(nb_lig);
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::complex_fftw(out_first_front_upper_left.col_begin(t,k,j),
						out_first_front_upper_left.col_end(t,k,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.col_begin(t,k,j));
			}

	Vtmp.resize(nb_sli);
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::complex_fftw(out_first_front_upper_left.slice_begin(t,i,j),
						out_first_front_upper_left.slice_end(t,i,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.slice_begin(t,i,j));
			}

	Vtmp.resize(nb_sla);
	for(size_t k = 0; k < nb_sli; ++k)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::complex_fftw(out_first_front_upper_left.slab_begin(k,i,j),
						out_first_front_upper_left.slab_end(k,i,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.slab_begin(k,i,j));
			}
}

/**
 ** \brief Computes the complex fftw4d of a container with 4d iterators.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \param in_first_front_upper_left
 ** \param in_last_back_bottom_right
 ** \param out_first_front_upper_left
 **
 ** \pre the input and output data have to be std::complex
 ** \pre the 4d range must have the same sizes
 ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration)
 **
 */
template<typename InputBidirectionalIterator4d,
typename OutputBidirectionalIterator4d>
inline
void complex_fftw4d(InputBidirectionalIterator4d in_first_front_upper_left,
		InputBidirectionalIterator4d in_last_back_bottom_right,
		OutputBidirectionalIterator4d out_first_front_upper_left)
{
	//recuperer la taille du container
	typename InputBidirectionalIterator4d::difference_type size4d = in_last_back_bottom_right - in_first_front_upper_left;
	//	typedef typename std::iterator_traits<InputBidirectionalIterator4d>::value_type complex;
	//	typedef typename complex::value_type real;

	std::size_t nb_sla = size4d[0];
	std::size_t nb_sli = size4d[1];
	std::size_t nb_lig = size4d[2];
	std::size_t nb_col = size4d[3];
	assert((nb_sla > 1)&&(nb_sli > 1)&&(nb_lig > 1)&&(nb_col>1));

	//fft4d
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t i = 0; i < nb_lig; ++i)
			{
				slip::complex_fftw(in_first_front_upper_left.row_begin(t,k,i),
						in_first_front_upper_left.row_end(t,k,i),out_first_front_upper_left.row_begin(t,k,i));
			}

	slip::Vector<std::complex<double> > Vtmp(nb_lig);
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::complex_fftw(out_first_front_upper_left.col_begin(t,k,j),
						out_first_front_upper_left.col_end(t,k,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.col_begin(t,k,j));
			}

	Vtmp.resize(nb_sli);
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::complex_fftw(out_first_front_upper_left.slice_begin(t,i,j),
						out_first_front_upper_left.slice_end(t,i,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.slice_begin(t,i,j));
			}

	Vtmp.resize(nb_sla);
	for(size_t k = 0; k < nb_sli; ++k)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::complex_fftw(out_first_front_upper_left.slab_begin(k,i,j),
						out_first_front_upper_left.slab_end(k,i,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.slab_begin(k,i,j));
			}
}

/**
 ** \brief Computes the ifftw4d of a container with 4d iterators.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \param in_first_front_upper_left
 ** \param in_last_back_bottom_right
 ** \param out_first_front_upper_left
 **
 ** \pre the input data have to be std::complex
 ** \pre the 4d range must have the same sizes
 ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration)
 **
 */
template<typename InputBidirectionalIterator4d,
typename OutputBidirectionalIterator4d>
inline
void ifftw4d(InputBidirectionalIterator4d in_first_front_upper_left,
		InputBidirectionalIterator4d in_last_back_bottom_right,
		OutputBidirectionalIterator4d out_first_front_upper_left)
{
	//recuperer la taille du container
	typename InputBidirectionalIterator4d::difference_type size4d = in_last_back_bottom_right - in_first_front_upper_left;

	std::size_t nb_sla = size4d[0];
	std::size_t nb_sli = size4d[1];
	std::size_t nb_lig = size4d[2];
	std::size_t nb_col = size4d[3];

	//ifft4d
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t i = 0; i < nb_lig; ++i)
			{
				slip::ifftw(in_first_front_upper_left.row_begin(t,k,i),
						in_first_front_upper_left.row_end(t,k,i),out_first_front_upper_left.row_begin(t,k,i));
			}

	slip::Vector<std::complex<double> > Vtmp(nb_lig);
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t k = 0; k < nb_sli; ++k)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::ifftw(out_first_front_upper_left.col_begin(t,k,j),
						out_first_front_upper_left.col_end(t,k,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.col_begin(t,k,j));
			}

	Vtmp.resize(nb_sli);
	for(size_t t = 0; t < nb_sla; ++t)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::ifftw(out_first_front_upper_left.slice_begin(t,i,j),
						out_first_front_upper_left.slice_end(t,i,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.slice_begin(t,i,j));
			}

	Vtmp.resize(nb_sla);
	for(size_t k = 0; k < nb_sli; ++k)
		for(size_t i = 0; i < nb_lig; ++i)
			for(size_t j = 0; j < nb_col; ++j)
			{
				slip::ifftw(out_first_front_upper_left.slab_begin(k,i,j),
						out_first_front_upper_left.slab_end(k,i,j),Vtmp.begin());
				std::copy(Vtmp.begin(),Vtmp.end(),out_first_front_upper_left.slab_begin(k,i,j));
			}
}

/**
 ** \brief Computes the real fftw4d of a container 4d
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \pre datain.cols() == dataout.cols()
 ** \pre datain.rows() == dataout.rows()
 ** \pre datain.slices() == dataout.slices()
 ** \pre datain.slabs() == dataout.slabes()
 ** \pre datain and dataout must have iterator4d available
 ** \pre datin and dataout must have the same size
 ** \pre the input data have to be real
 ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration)
 **
 */
template<typename InputMatrix4d,
typename OutputMatrix4d>
inline
void real_fftw4d(const InputMatrix4d & datain,OutputMatrix4d & dataout)
{
	assert(datain.cols() == dataout.cols());
	assert(datain.rows() == dataout.rows());
	assert(datain.slices() == dataout.slices());
	assert(datain.slabs() == dataout.slabs());
	real_fftw4d(datain.first_front_upper_left(),datain.last_back_bottom_right(),dataout.first_front_upper_left());
}

/**
 ** \brief Computes the complex fftw4d of a container 4d
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \pre datain.cols() == dataout.cols()
 ** \pre datain.rows() == dataout.rows()
 ** \pre datain.slices() == dataout.slices()
 ** \pre datain.slabs() == dataout.slabes()
 ** \pre datain and dataout must have iterator4d available
 ** \pre datin and dataout must have the same size
 ** \pre the input data have to be complex
 ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration)
 **
 */
template<typename InputMatrix4d,
typename OutputMatrix4d>
inline
void complex_fftw4d(const InputMatrix4d & datain,OutputMatrix4d & dataout)
{
	assert(datain.cols() == dataout.cols());
	assert(datain.rows() == dataout.rows());
	assert(datain.slices() == dataout.slices());
	assert(datain.slabs() == dataout.slabs());
	complex_fftw4d(datain.first_front_upper_left(),datain.last_back_bottom_right(),dataout.first_front_upper_left());
}

/**
 ** \brief Computes the ifftw4d of a container 4d.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \param datain The input datas
 ** \param dataout The output datas
 **
 ** \pre datain.cols() == dataout.cols()
 ** \pre datain.rows() == dataout.rows()
 ** \pre datain.slices() == dataout.slices()
 ** \pre datain.slabs() == dataout.slabes()
 ** \pre datain and dataout must have iterator4d available
 ** \pre datin and dataout must have the same size
 ** \pre the input data have to be std::complex
 ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration)
 **
 */
template<typename InputMatrix4d,
typename OutputMatrix4d>
inline
void ifftw4d(const InputMatrix4d & datain,OutputMatrix4d & dataout)
{
	assert(datain.cols() == dataout.cols());
	assert(datain.rows() == dataout.rows());
	assert(datain.slices() == dataout.slices());
	assert(datain.slabs() == dataout.slabs());
	ifftw4d(datain.first_front_upper_left(),datain.last_back_bottom_right(),dataout.first_front_upper_left());
}


/*@} End fftw algorithms (from fftw3.h). */

#endif //HAVE_FFTW

/**
 ** \name FFT Generic Algorithms
 */
/*@{*/

/**
 ** \brief Computes the real fft4d of a container with 4d iterators.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \param in_first_front_upper_left
 ** \param in_last_back_bottom_right
 ** \param out_first_front_upper_left
 **
 ** \pre the 4d range must have the same sizes
 ** \pre input data have to be real
 ** \pre out data have to be std::complex
 **
 ** \post the fft algorithm used is the fftw if enabled
 ** (--enable-fftw3 option of configuration) or the split radix one.
 ** \par Example:
 ** \code
 **   //input slip::HyperVolume<double> M of dimensions dim1, dim2, dim3 and dim4;
 **   slip::HyperVolume<double> Out(M.dim1(),M.dim2(),M.dim3(),M.dim4(),1);
 **   //FFT4D
 **   slip::Matrix4d<std::complex<double> > OFFT(M.dim1(),M.dim2(),M.dim3(),M.dim4(),Czero);
 **   slip::real_fft4d(M.first_front_upper_left(),M.last_back_bottom_right(),OFFT.first_front_upper_left());
 **   slip::HyperVolume<double> NFFT(OFFT.dim1(),OFFT.dim2(),OFFT.dim3(),OFFT.dim4(),M.first_front_upper_left(),M.last_back_bottom_right());
 **   //  log(magnitude)
 **   for(size_t t = 0; t < OFFT.dim1(); ++t)
 **    for(size_t k = 0; k < OFFT.dim2(); ++k)
 **     for(size_t i = 0; i < OFFT.dim3(); ++i)
 **	     for(size_t j = 0; j < OFFT.dim4(); ++j)
 **	       NFFT[t][k][i][j] = std::log(1.0+std::abs(OFFT[t][k][i][j]));
 **
 **   slip::fftshift4d(NFFT.first_front_upper_left(),NFFT.last_back_bottom_right());
 **   slip::change_dynamic_01(NFFT.begin(),NFFT.end(),NFFT.begin(),slip::AFFINE_FUNCTION);
 **  \endcode
 **
 */
template<typename InputBidirectionalIterator4d,
typename OutputBidirectionalIterator4d>
inline
void real_fft4d(InputBidirectionalIterator4d in_first_front_upper_left,
		InputBidirectionalIterator4d in_last_back_bottom_right,
		OutputBidirectionalIterator4d out_first_front_upper_left)
{
#ifdef HAVE_FFTW
	real_fftw4d(in_first_front_upper_left,in_last_back_bottom_right,out_first_front_upper_left);
#else
	real_split_radix_fft4d(in_first_front_upper_left,in_last_back_bottom_right,out_first_front_upper_left);
#endif 
}

/**
 ** \brief Computes the complex fft4d of a container with 4d iterators.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/10/22
 ** \since 1.4.0
 ** \param in_first_front_upper_left
 ** \param in_last_back_bottom_right
 ** \param out_first_front_upper_left
 **
 ** \pre the 4d range must have the same sizes
 ** \pre input data have to be std::complex
 ** \pre out data have to be std::complex
 **
 ** \post the fft algorithm used is the fftw if enabled
 ** (--enable-fftw3 option of configuration) or the split radix one.
 ** \par Example:
 ** \code
 **   //input slip::HyperVolume<std::complex<double> > M of dimensions dim1, dim2, dim3 and dim4;
 **
 **   //FFT4D
 **   slip::Matrix4d<std::complex<double> > OFFT(M.dim1(),M.dim2(),M.dim3(),M.dim4(),Czero);
 **   slip::complex_fft4d(M.first_front_upper_left(),M.last_back_bottom_right(),OFFT.first_front_upper_left());
 **   slip::HyperVolume<double> NFFT(OFFT.dim1(),OFFT.dim2(),OFFT.dim3(),OFFT.dim4(),M.first_front_upper_left(),M.last_back_bottom_right());
 **   //  log(magnitude)
 **   for(size_t t = 0; t < OFFT.dim1(); ++t)
 **    for(size_t k = 0; k < OFFT.dim2(); ++k)
 **     for(size_t i = 0; i < OFFT.dim3(); ++i)
 **	     for(size_t j = 0; j < OFFT.dim4(); ++j)
 **	       NFFT[t][k][i][j] = std::log(1.0+std::abs(OFFT[t][k][i][j]));
 **
 **   slip::fftshift4d(NFFT.first_front_upper_left(),NFFT.last_back_bottom_right());
 **   slip::change_dynamic_01(NFFT.begin(),NFFT.end(),NFFT.begin(),slip::AFFINE_FUNCTION);
 **  \endcode
 **
 */
template<typename InputBidirectionalIterator4d,
typename OutputBidirectionalIterator4d>
inline
void complex_fft4d(InputBidirectionalIterator4d in_first_front_upper_left,
		InputBidirectionalIterator4d in_last_back_bottom_right,
		OutputBidirectionalIterator4d out_first_front_upper_left)
{
#ifdef HAVE_FFTW
	complex_fftw4d(in_first_front_upper_left,in_last_back_bottom_right,out_first_front_upper_left);
#else
	complex_split_radix_fft4d(in_first_front_upper_left,in_last_back_bottom_right,out_first_front_upper_left);
#endif
}

/**
 ** \brief Computes the ifft4d of a container with 4d iterators.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \param in_first_front_upper_left
 ** \param in_last_back_bottom_right
 ** \param out_first_front_upper_left
 **
 ** \pre the input data have to be std::complex
 ** \pre the 4d range must have the same sizes
 **
 ** \post the fft algorithm used is the fftw if enabled
 ** (--enable-fftw3 option of configuration) or the split radix one.
 ** \par Example:
 ** \link slip::real_fft4d(InputBidirectionalIterator4d,InputBidirectionalIterator4d,OutputBidirectionalIterator4d)
 **  ...
 ** \endlink
 ** \code
 **  std::complex<double> Czero(0,0);
 **  slip::Matrix4d<std::complex<double> > OC(M.dim1(),M.dim2(),M.dim3(),M.dim4(),Czero);
 **  slip::HyperVolume<double> Out(M.dim1(),M.dim2(),M.dim3(),M.dim4(),1);
 **  slip::ifft4d(OFFT.first_front_upper_left(),OFFT.last_back_bottom_right(),OC.first_front_upper_left());
 **   for(size_t t = 0; t < OFFT.dim1(); ++t)
 **    for(size_t k = 0; k < OFFT.dim2(); ++k)
 **     for(size_t i = 0; i < OFFT.dim3(); ++i)
 **	     for(size_t j = 0; j < OFFT.dim4(); ++j)
 **	    {
 **	      Out[t][k][i][j] = std::abs(OC[t][k][i][j]);
 **	    }
 **  slip::change_dynamic_01(Out.begin(),Out.end(),Out.begin(),slip::AFFINE_FUNCTION);
 ** \endcode
 */
template<typename InputBidirectionalIterator4d,
typename OutputBidirectionalIterator4d>
inline
void ifft4d(InputBidirectionalIterator4d in_first_front_upper_left,
		InputBidirectionalIterator4d in_last_back_bottom_right,
		OutputBidirectionalIterator4d out_first_front_upper_left)
{
#ifdef HAVE_FFTW
	ifftw4d(in_first_front_upper_left,in_last_back_bottom_right,out_first_front_upper_left);
#else
	split_radix_ifft4d(in_first_front_upper_left,in_last_back_bottom_right,out_first_front_upper_left);
#endif
}


/**
 ** \brief Computes the real fft4d of a container.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \param datain The input datas
 ** \param dataout The output datas
 **
 ** \pre datain.cols() == dataout.cols()
 ** \pre datain.rows() == dataout.rows()
 ** \pre datain.slices() == dataout.slices()
 ** \pre datain and dataout must have cols, rows and slices iterator
 ** \pre datain must be real
 ** \pre dataout must be std::complex
 **
 ** \post the fft algorithm used is the fftw if enabled
 ** (--enable-fftw3 option of configuration) or the split radix one.
 ** \par Example:
 ** \code
 **   //input slip::HyperVolume<double> M of dimensions dim1, dim2, dim3 and dim4;
 **   slip::HyperVolume<double> Out(M.dim1(),M.dim2(),M.dim3(),M.dim4(),1);
 **   //FFT4D
 **   slip::Matrix4d<std::complex<double> > OFFT(M.dim1(),M.dim2(),M.dim3(),M.dim4(),Czero);
 **   slip::real_fft4d(M,OFFT);
 **   slip::HyperVolume<double> NFFT(OFFT.dim1(),OFFT.dim2(),OFFT.dim3(),OFFT.dim4(),M.first_front_upper_left(),M.last_back_bottom_right());
 **   //  log(magnitude)
 **   for(size_t t = 0; t < OFFT.dim1(); ++t)
 **    for(size_t k = 0; k < OFFT.dim2(); ++k)
 **     for(size_t i = 0; i < OFFT.dim3(); ++i)
 **	     for(size_t j = 0; j < OFFT.dim4(); ++j)
 **	      NFFT[t][k][i][j] = std::log(1.0+std::abs(OFFT[t][k][i][j]));
 **
 **   slip::fftshift4d(NFFT.first_front_upper_left(),NFFT.last_back_bottom_right());
 **   slip::change_dynamic_01(NFFT.begin(),NFFT.end(),NFFT.begin(),slip::AFFINE_FUNCTION);
 **  \endcode
 */
template<typename HyperVolume1, typename HyperVolume2>
inline
void real_fft4d(const HyperVolume1 &datain, HyperVolume2 &dataout)
{
	assert(datain.cols() == dataout.cols());
	assert(datain.rows() == dataout.rows());
	assert(datain.slices() == dataout.slices());
#ifdef HAVE_FFTW
	real_fftw4d(datain.first_front_upper_left(),datain.last_back_bottom_right(),dataout.first_front_upper_left());
#else
	real_split_radix_fft4d(datain.first_front_upper_left(),datain.last_back_bottom_right(),dataout.first_front_upper_left());
#endif
}

/**
 ** \brief Computes the complex fft4d of a container.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/10/22
 ** \since 1.4.0
 ** \param datain The input datas
 ** \param dataout The output datas
 **
 ** \pre datain.cols() == dataout.cols()
 ** \pre datain.rows() == dataout.rows()
 ** \pre datain.slices() == dataout.slices()
 ** \pre datain and dataout must have cols, rows and slices iterator
 ** \pre datain must be std::complex
 ** \pre dataout must be std::complex
 **
 ** \post the fft algorithm used is the fftw if enabled
 ** (--enable-fftw3 option of configuration) or the split radix one.
 ** \par Example:
 ** \code
 **   //input slip::HyperVolume<std::complex<double> > M of dimensions dim1, dim2, dim3 and dim4;
 **
 **   //FFT4D
 **   slip::Matrix4d<std::complex<double> > OFFT(M.dim1(),M.dim2(),M.dim3(),M.dim4(),Czero);
 **   slip::complex_fft4d(M,OFFT);
 **   slip::HyperVolume<double> NFFT(OFFT.dim1(),OFFT.dim2(),OFFT.dim3(),OFFT.dim4(),M.first_front_upper_left(),M.last_back_bottom_right());
 **   //  log(magnitude)
 **   for(size_t t = 0; t < OFFT.dim1(); ++t)
 **    for(size_t k = 0; k < OFFT.dim2(); ++k)
 **     for(size_t i = 0; i < OFFT.dim3(); ++i)
 **	     for(size_t j = 0; j < OFFT.dim4(); ++j)
 **	      NFFT[t][k][i][j] = std::log(1.0+std::abs(OFFT[t][k][i][j]));
 **
 **   slip::fftshift4d(NFFT.first_front_upper_left(),NFFT.last_back_bottom_right());
 **   slip::change_dynamic_01(NFFT.begin(),NFFT.end(),NFFT.begin(),slip::AFFINE_FUNCTION);
 **  \endcode
 */
template<typename HyperVolume1, typename HyperVolume2>
inline
void complex_fft4d(const HyperVolume1 &datain, HyperVolume2 &dataout)
{
	assert(datain.cols() == dataout.cols());
	assert(datain.rows() == dataout.rows());
	assert(datain.slices() == dataout.slices());
#ifdef HAVE_FFTW
	complex_fftw4d(datain.first_front_upper_left(),datain.last_back_bottom_right(),dataout.first_front_upper_left());
#else
	complex_split_radix_fft4d(datain.first_front_upper_left(),datain.last_back_bottom_right(),dataout.first_front_upper_left());
#endif
}

/**
 ** \brief Computes the fft4d of a container.
 ** \version Fluex 1.0
 ** \author Denis Arrivault <Denis.Arrivault_AT_inria.fr>
 ** \date 2013/08/23
 ** \since 1.4.0
 ** \param datain The input datas
 ** \param dataout The output datas
 **
 ** \pre datain.cols() == dataout.cols()
 ** \pre datain.rows() == dataout.rows()
 ** \pre datain.slices() == dataout.slices()
 ** \pre datain and dataout must have cols, rows and slices iterator
 ** \pre datain must be std::complex
 ** \pre dataout must be std::complex
 **
 ** \post the fft algorithm used is the fftw if enabled
 ** (--enable-fftw3 option of configuration) or the split radix one.
 ** \par Example:
 ** \link slip::real_fft4d(HyperVolume1&,HyperVolume2&)
 **  ...
 ** \endlink
 ** \code
 **  std::complex<double> Czero(0,0);
 **  slip::Matrix4d<std::complex<double> > OC(M.dim1(),M.dim2(),M.dim3(),M.dim4(),Czero);
 **  slip::HyperVolume<double> Out(M.dim1(),M.dim2(),M.dim3(),M.dim4(),1);
 **  slip::ifft4d(OFFT,OC);
 **   for(size_t t = 0; t < OFFT.dim1(); ++t)
 **    for(size_t k = 0; k < OFFT.dim2(); ++k)
 **     for(size_t i = 0; i < OFFT.dim3(); ++i)
 **	     for(size_t j = 0; j < OFFT.dim4(); ++j)
 **	    {
 **	      Out[t][k][i][j] = std::abs(OC[t][k][i][j]);
 **	    }
 **  slip::change_dynamic_01(Out.begin(),Out.end(),Out.begin(),slip::AFFINE_FUNCTION);
 ** \endcode
 **
 */
template<typename HyperVolume1, typename HyperVolume2>
inline
void ifft4d(const HyperVolume1 &datain, HyperVolume2 &dataout)
{
	assert(datain.cols() == dataout.cols());
	assert(datain.rows() == dataout.rows());
	assert(datain.slices() == dataout.slices());
#ifdef HAVE_FFTW
	ifftw4d(datain.first_front_upper_left(),datain.last_back_bottom_right(),dataout.first_front_upper_left());
#else
	split_radix_ifft4d(datain.first_front_upper_left(),datain.last_back_bottom_right(),dataout.first_front_upper_left());
#endif
}






 /*@} End FFT Generic Algorithms */

}//slip::




 
#endif //FFT_HPP

