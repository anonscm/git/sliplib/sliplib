/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file levenberg_marquardt.hpp
 * 
 * \brief Provides a simple implementation of the Levendberg-Marquardt algorithm.
 * \since 1.0.0
 */
#ifndef SLIP_LEVENBERG_MARQUARDT_HPP
#define SLIP_LEVENBERG_MARQUARDT_HPP

#include "Vector3d.hpp"
#include "KVector.hpp"
#include "Matrix.hpp"
#include "linear_algebra.hpp"



namespace slip
{
/**
 ** \brief Computes Chi2 for Lebvenberg Marquardt algorithm
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \param first RandomAccessIterator to the first element of the data.
** \param last RandomAccessIterator to one-past-the-end element of the data.
**
*/
template <typename T,typename RandomAccessIterator>
T LM_chi2(RandomAccessIterator first, RandomAccessIterator last)
{
  T chisq = T();
  const T N = static_cast<T>(last-first);
  for(; first != last; ++first)
    {
      chisq += (*first * *first)/N;
    }
  return chisq;
}


/**
** \brief Optimization of function using the Levenberg-Marquardt algorithm
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \author Guillaume Gomit <tremblais_AT_sic.univ-poitiers.fr>
** \param fun function to optimize.
** \param df Derivative Function.
** \param par Vector of paramameters.
** \param r Residual vector.
** \param chi2 Residual result.
** \param maxiter Max number of iterations.
** \param eps Criteria of convergence.
** \remarks Simple implementation of the levendberg Marquardt algorithm. Use at your own risk...
** \par Example:
** \code
** //definition of a fonction
** template <typename Type>
** struct funLM_ex1 : public std::unary_function <slip::Vector<Type>,slip::Vector<Type> >
** {
**  typedef funLM_ex1<Type> self;
**  funLM_ex1(const slip::Vector<Type>& x, const slip::Vector<Type>& y) : 
**  x_(x),
**  r_(slip::Vector<Type>(x.size())),
**  y_(y)
** {
**   assert(x.size() == y.size());
** }
** slip::Vector<Type>& operator() (const slip::Vector<Type>& pt) // define the function
** {
**   for(std::size_t i = 0; i < x_.size(); ++i)
**   {
**     r_[i]= y_[i] - (pt[0]*exp(-pt[1]*x_[i]) + pt[2]);
**   }
** return r_;
** }
** 
** slip::Vector<Type> x_;//x
** slip::Vector<Type> r_; //residue ||y - f(x,pk)||
** slip::Vector<Type> y_; //y = f(x,p)
** };
** //....
** typedef double Type;
**  std::size_t n = 40;
**  slip::Vector<Type> x(n);
** slip::iota(x.begin(),x.end(),Type(0.0));
** slip::Vector<Type> y(n);
** for(std::size_t i = 0; i < n; ++i)
** {
**   //p[0] = 5.0, p[1] = 0.1, p[2] = 1.0
**   y[i]=(5.0*exp(-0.1*x[i]) + 1.0);
** }
**  
** std::cout<<"y = \n"<<y<<std::endl;
** slip::add_gaussian_noise(y.begin(),y.end(),y.begin(),0.0,0.01);
**
** std::cout<<"y with noise = \n"<<y<<std::endl;
** funLM_ex1<Type> fun(x,y);
** //parameters vector
** slip::Vector<Type> p(3);
** p[0] = 1.0;
** p[1] = 0.0;
** p[2] = 0.0;
** //residue vector
** slip::Vector<Type> r(n);
**  
**  
**slip::LMDerivFunctor<funLM_ex1<Type>, Type> df(fun,n,p.size());
** Type chi2 = 0.0;
** slip::marquardt(fun,df,p,r,chi2);
** std::cout<<"estimated p = \n"<<p<<std::endl;
** std::cout<<"r = \n"<<r<<std::endl;
** std::cout<<"chi2 = "<<chi2<<std::endl;
** \endcode
*/
template<typename Function, 
	 typename Real,
	 typename DerivativeFunction>
void marquardt (Function& fun,
		DerivativeFunction& df,
		slip::Vector<Real> &par, 
		slip::Vector<Real>& r,
		Real& calchi2,
		const int maxiter = 10000,
		const Real eps = static_cast<Real>(1e-6))
{

  //const int maxiter = 10000;
   const std::size_t N = r.size();
   const std::size_t npar = par.size();

   slip::Vector<Real> par2(npar);
   slip::Vector<Real> delta(npar);
   slip::Vector<Real> b(npar);
   //  double tmp;
   slip::Matrix<Real> aprime(npar,npar);
   //std::cout<<"npar = "<<npar<<" N = "<<N<<std::endl;
   slip::Matrix<Real> A(npar,N);


   //const Real eps = static_cast<Real>(1e-6);
   Real lamfac = static_cast<Real>(2.0);
   Real two = static_cast<Real>(2.0);
   Real minus_two = - two;
   Real zero = static_cast<Real>(0.0);
   Real lambda = static_cast<Real>(0.5); 
   bool done = 0; 
   int decrease = 0; 
   int niter = 1;

   //value for the initial fit
   r = fun(par);
   //compute jacobian
   df(par.begin(),par.end(),A); /* A= matrix jacobien transpose */

   //compute chi^2
   Real chisq = LM_chi2<Real>(r.begin(),r.end());

//initial matrices; curvature matrix H and b
   slip::Matrix<Real> AAt(npar,npar);
   slip::matrix_matrixt_multiplies(A,AAt);

   for(std::size_t i = 0; i < npar; ++i) 
      {
	b[i] = zero;
	for(std::size_t k=0; k < N; ++k)
	  {
	    b[i] += minus_two*A[i][k]*r[k] ;
	  }
      }

//Main Loop
   while(!done) 
     {
       /* Set up a' */
       for (std::size_t k = 0; k < npar; ++k) 
	 {
	   for (std::size_t i = 0; i < npar; ++i) 
	     {
	       aprime[i][k] = two * AAt[i][k];
	     }
	   aprime[k][k] = (two*AAt[k][k] + lambda);
	 }
       /* Solve Matrix Equation: a'.delta = b */
       slip::lu_solve(aprime,delta,b);

       /* Increment parameters in work space and evaluate new chisq */
       for (std::size_t i = 0; i < npar; ++i)
	 {
	   par2[i] = par[i] + delta[i];
	 }
       r = fun(par2);

       //compute chisq2
       Real chisq2 = LM_chi2<Real>(r.begin(),r.end());

       if (chisq2 <= chisq) 
	 {
	   decrease = 1;
	   if (chisq == chisq2)
	     {
	       done = 1;
	     }
	   if (fabs(((chisq - chisq2)/chisq)) < eps && decrease )
	     {
	       done = 1;
	     }
           std::copy(par2.begin(),par2.end(),par.begin());

           if (!done) 
	     {
	       // new Hessien and b
	       df(par.begin(),par.end(),A);
	       slip::matrix_matrixt_multiplies(A,AAt);
	       for(std::size_t i = 0; i < npar; ++i) 
		 {
		   b[i] = zero;
		   for(std::size_t k = 0; k < N; ++k)
		     {
		       b[i] += minus_two* A[i][k] * r[k] ;
		     }
		 }
	       chisq = chisq2; 
	       lambda = lambda / lamfac; 
	       decrease = 1;
	     }
	 }
       else 
	 {
	   decrease = 0;
	   lambda = two*lambda;
	 }

       if (niter > maxiter) 
	 {
	   done = 1;
	 }
       niter++;
     }
   
   calchi2 = LM_chi2<Real>(r.begin(),r.end());
}

/**
** \brief  generic derivative functor
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
**
*/
template <typename Function,
	  typename Type>
struct LMDerivFunctor
{
  
  LMDerivFunctor(const Function& fun,
		 const int N,
		 const int npar):
    fun_(fun),
    N_(N),
    npar_(npar),
    p_inf_(slip::Vector<Type>(npar)),
    p_sup_(slip::Vector<Type>(npar)),
    rinf_(slip::Vector<Type>(N,static_cast<Type>(0.0))),
    rsup_(slip::Vector<Type>(N,static_cast<Type>(0.0))),
    delta_(0.001),
    delta2_(0.002)
     {}

   LMDerivFunctor(const Function& fun,
		  const int N,
		  const int npar,
		  const Type& delta):
    fun_(fun),
    N_(N),
    npar_(npar),
    p_inf_(slip::Vector<Type>(npar)),
    p_sup_(slip::Vector<Type>(npar)),
    rinf_(slip::Vector<Type>(N,static_cast<Type>(0.0))),
    rsup_(slip::Vector<Type>(N,static_cast<Type>(0.0))),
    delta_(delta),
    delta2_(static_cast<Type>(2.0)*delta)
     {}
  
 
  template <typename RandomAccessIterator>
  void operator()(RandomAccessIterator p_first, 
		  RandomAccessIterator p_last,
		  slip::Matrix<Type>& A)
  {
    assert(static_cast<int>(p_last-p_first)==npar_);
    
    std::copy(p_first,p_last,p_sup_.begin());
    std::copy(p_first,p_last,p_inf_.begin());
    typename slip::Vector<Type>::iterator it_p_inf = p_inf_.begin();
    typename slip::Vector<Type>::iterator it_p_sup = p_sup_.begin();
    
    for(int k =0; p_first != p_last; ++k,++p_first, ++it_p_inf,++it_p_sup)
      {
	Type pk = *p_first;
	*it_p_inf = pk - delta_;
	*it_p_sup = pk + delta_;
	rsup_ = fun_(p_sup_);
	rinf_ = fun_(p_inf_);
	for (int i = 0; i < N_ ; ++i)
	  {	
	    A[k][i] =(rsup_[i] - rinf_[i]) / delta2_;
	  }
	*it_p_inf = pk;
	*it_p_sup = pk;
      }
  }

  //attributes
  Function fun_;
  int N_;
  int npar_;
  slip::Vector<Type> p_inf_;
  slip::Vector<Type> p_sup_;
  slip::Vector<Type> rinf_;
  slip::Vector<Type> rsup_;
  Type delta_;
  Type delta2_;
};



}//slip::

#endif // SLIP_LEVENBERG_MARQUARDT_HPP
