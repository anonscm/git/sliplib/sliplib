/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file norms.hpp
 * 
 * \brief Provides some algorithms to compute norms.
 * 
 */
#ifndef SLIP_NORMS_HPP
#define SLIP_NORMS_HPP


#include <algorithm>
#include <numeric>
#include <cassert>
#include <cmath>
#include "complex_cast.hpp"

namespace slip
{

  /**
   ** \brief Computes the L22 norm (\f$\sum_i x_i^2\f$) of a range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/09/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \return  the L22 norm of the range.
   ** \pre [first, last) is a valid range
   ** \pre Value_T is Assignable
   ** \pre If x is an object of type Value_T, y 
   **      and z are objects of InputIterator's 
   **      value type, then x + y * z is defined
   ** \pre The type of x + y * z is convertible to Value_T
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M(4,5);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** std::cout<<slip::L22_norm<float>(M.begin(),M.end())<<std::endl;
   ** \endcode
   */
  template<typename Value_T, typename InputIterator>
  inline
  Value_T L22_norm(InputIterator first,
		   InputIterator last)
  {
    assert(first != last);
    return std::inner_product(first,last,first,Value_T(0));
  }


 

 /**
   ** \brief Computes the L22 norm \f$ \sum_i x_i^2\f$ of a range according to a mask sequence.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/12
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  
   ** \pre [first, last) is a valid range
   ** \pre [mask_first, (last-first)) is a valid range
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M(2,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** float d[] = {0,1,1,0,0,1};
   ** slip::Array2d<float> Mask(2,3,d);
   ** std::cout<<M<<std::endl;
   ** std::cout<<Mask<<std::endl;
   ** std::cout<<slip::L22_norm_mask<float>(M.begin(),M.end(),Mask.begin(),1)<<std::endl
   ** \endcode
   **
   */
template<typename Value_T, typename InputIterator,typename MaskIterator>
  inline
   Value_T L22_norm_mask(InputIterator first,
                         InputIterator last,
                         MaskIterator mask_first, 
                         typename std::iterator_traits<MaskIterator>::value_type value=typename
                           std::iterator_traits<MaskIterator>::value_type(1))
{
assert(first != last);
    
    Value_T __init = Value_T();
    for (; first != last; ++first,++mask_first)
     {
        if(*mask_first == value)
         {
         __init +=(*first)*(*first);
          }
     }
    return __init;
}

/**  \brief Computes the L22 norm \f$ \sum_i x_i^2\f$ a range according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/12
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  pred       A predicate.
   **  \return 
   ** \pre [first, last) is a valid range
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   **  slip::Array2d<float> M(2,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0)
   ** std::cout<<slip::L22_norm_if<float>(M.begin(),M.end(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/

template<typename Value_T,
           typename InputIterator, 
           typename Predicate>
Value_T L22_norm_if(InputIterator first,
                       InputIterator last,
                       Predicate pred)
{
assert(first != last);
    
    Value_T __init = Value_T();
    for (; first != last; ++first)
     {
        if(pred(*first))
         {
         __init +=(*first)*(*first);
          }
     }
    return __init;
}


  /**
   ** \brief Computes the L2 norm \f$ \sqrt{\sum_i x_i^2}\f$ of a range 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/09/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \return  the L2 norm of the range.
    **
   ** \pre [first, last) is a valid range
   ** \pre Value_T is Assignable
   ** \pre If x is an object of type Value_T, y and z are objects of InputIterator's 
   **      value type, then x + y * z is defined
   ** \pre The type of x + y * z is convertible to Value_T
   **
   ** \note Calls std::sqrt(slip::L22_norm<Value_T>(first,last));
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M(4,5);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** std::cout<<slip::L2_norm<float>(M.begin(),M.end())<<std::endl;
   ** \endcode
 
   */
  template<typename Value_T, typename InputIterator>
  inline
  Value_T L2_norm(InputIterator first,
		  InputIterator last)
  {
    return std::sqrt(slip::L22_norm<Value_T>(first,last));
  }

/**
   ** \brief Computes the Euclidean norm \f$ \sqrt{\sum_i x_i^2}\f$ of a range according to a mask sequence.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/12
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \pre [first, last) is a valid range
   ** \pre [mask_first, (last-first)) is a valid range
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M(2,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** float d[] = {0,1,1,0,0,1};
   ** slip::Array2d<float> Mask(2,3,d);
   ** std::cout<<M<<std::endl;
   ** std::cout<<Mask<<std::endl;
   ** std::cout<<slip::L2_norm_mask<float>(M.begin(),M.end(),Mask.begin(),1)<<std::endl
   ** \endcode
   **
   */
template<typename Value_T, typename InputIterator,typename MaskIterator>
  inline
   Value_T L2_norm_mask(InputIterator first,
                         InputIterator last,
                         MaskIterator mask_first, 
                         typename std::iterator_traits<MaskIterator>::value_type value=typename
                           std::iterator_traits<MaskIterator>::value_type(1))
{
return std::sqrt(slip::L22_norm_mask<Value_T>(first,last,mask_first,value));
}


/**  \brief Computes the Euclidean norm \f$ \sqrt{\sum_i x_i^2}\f$ a range according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/12
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  pred       A predicate.
   **  \return 
   ** \pre [first, last) is a valid range
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   **  slip::Array2d<float> M(2,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0)
   ** std::cout<<slip::L2_norm_if<float>(M.begin(),M.end(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/
template<typename Value_T,
           typename InputIterator, 
           typename Predicate>
Value_T L2_norm_if(InputIterator first,
                       InputIterator last,
                      Predicate pred)

{
return std::sqrt(slip::L22_norm_if<Value_T>(first,last,pred));
}

   /**
   ** \brief Computes the Euclidean norm \f$ \sqrt{\sum_i x_i^2}\f$ of a range 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/09/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \return  the Euclidean norm of the range.
   **
 
   ** \pre [first, last) is a valid range
   ** \pre Value_T is Assignable
   ** \pre If x is an object of type Value_T, y and z are objects of InputIterator's 
   **      value type, then x + y * z is defined
   ** \pre The type of x + y * z is convertible to Value_T
   **
   ** \note Calls slip::L2_norm<Value_T>(first,last);
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M(4,5);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** std::cout<<slip::Euclidean_norm<float>(M.begin(),M.end())<<std::endl;
   ** \endcode
   */
  template<typename Value_T, typename InputIterator>
  inline
  Value_T Euclidean_norm(InputIterator first,
			 InputIterator last)
  {
    return slip::L2_norm<Value_T>(first,last);
  }

 /**
   ** \brief Computes the Euclidean norm \f$ \sqrt{\sum_i x_i^2}\f$ of a range according to a mask sequence.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/12
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  the infinite norm of the range.
   ** \pre [first, last) is a valid range
   ** \pre [mask_first, (last-first)) is a valid range
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M(2,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** float d[] = {0,1,1,0,0,1};
   ** slip::Array2d<float> Mask(2,3,d);
   ** std::cout<<M<<std::endl;
   ** std::cout<<Mask<<std::endl;
   ** std::cout<<slip::Euclidean_norm_mask<float>(M.begin(),M.end(),Mask.begin(),1)<<std::endl
   ** \endcode
   **
   */
 template<typename Value_T, typename InputIterator,typename MaskIterator>
  inline
  Value_T Euclidean_norm_mask(InputIterator first,
			      InputIterator last,
                              MaskIterator mask_first, 
                              typename std::iterator_traits<MaskIterator>::value_type value=typename
			      std::iterator_traits<MaskIterator>::value_type(1))
  {
    return slip::L2_norm_mask<Value_T>(first,last,mask_first,value);
    }


/**  \brief Computes the  Euclidean norm \f$ \sqrt{\sum_i x_i^2}\f$ a range according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/12
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  pred       A predicate.
   **  \return 
   ** \pre [first, last) is a valid range
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   **  slip::Array2d<float> M(2,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0)
   ** std::cout<<slip:: Euclidean_norm_if<float>(M.begin(),M.end(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/
template<typename Value_T, typename InputIterator, typename Predicate>
  inline
  Value_T Euclidean_norm_if(InputIterator first,
			      InputIterator last,
                              Predicate pred)
  {
    return slip::L2_norm_if<Value_T>(first,last,pred);
   
 }
 
  /**
   ** \brief Computes the L1 norm \f$ \sum_i{|x_i|}\f$ of a range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/09/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \return  the L1 norm of the range.
   ** \pre [first, last) is a valid range
   ** \pre Value_T is Assignable
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M(4,5);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** std::cout<<slip::L1_norm<float>(M.begin(),M.end())<<std::endl;
   ** \endcode
   **
   */
  template<typename Value_T, typename InputIterator>
  inline
  Value_T L1_norm(InputIterator first,
		  InputIterator last)
  {
    assert(first != last);
    
    Value_T __init = Value_T(0);
    for (; first != last; ++first)
      __init = __init + std::abs(*first);
    return __init;
  }
/**
   ** \brief Computes the L1 norm \f$ \sum_i{|x_i|}\f$ of a range according to a mask sequence.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/12
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  the infinite norm of the range.
   ** \pre [first, last) is a valid range
   ** \pre [mask_first, (last-first)) is a valid range
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M(2,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** float d[] = {0,1,1,0,0,1};
   ** slip::Array2d<float> Mask(2,3,d);
   ** std::cout<<M<<std::endl;
   ** std::cout<<Mask<<std::endl;
   ** std::cout<<slip::L1_norm_mask<float>(M.begin(),M.end(),Mask.begin(),1)<<std::endl
   ** \endcode
   **
   */
template<typename Value_T, typename InputIterator,typename MaskIterator>
  inline
   Value_T L1_norm_mask(InputIterator first,
                         InputIterator last,
                         MaskIterator mask_first, 
                         typename std::iterator_traits<MaskIterator>::value_type value=typename
			std::iterator_traits<MaskIterator>::value_type(1))
{
 assert(first != last);
    
    Value_T __init = Value_T(0);
    for (; first != last; ++first,++mask_first)
     {
        if(*mask_first == value)
         {
         __init = __init + std::abs(*first);
          }
     }
    return __init;
 }

/**  \brief Computes the L1 norm \f$ \sum_i{|x_i|}\f$ a range according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/12
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  pred       A predicate.
   **  \return 
   ** \pre [first, last) is a valid range
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   **  slip::Array2d<float> M(2,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0)
   ** std::cout<<slip::L1_norm_if<float>(M.begin(),M.end(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/
template<typename Value_T,
           typename InputIterator, 
           typename Predicate>
Value_T L1_norm_if(InputIterator first,
                       InputIterator last,
                       Predicate pred)
{
assert(first != last);
    
    Value_T __init = Value_T(0);
    for (; first != last; ++first)
     {
         if(pred(*first))
         {
         __init = __init + std::abs(*first);
          }
     }
    return __init;
}

  /**
   ** \brief Computes the infinite norm \f$\max_i{|x_i|}\f$ of a range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/09/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \return  the infinite norm of the range.
   **
   ** \pre [first, last) is a valid range
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M(4,5);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** std::cout<<slip::infinite_norm<float>(M.begin(),M.end())<<std::endl;
   ** \endcode
   **
   */
  template<typename Value_T, typename InputIterator>
  inline
  Value_T infinite_norm(InputIterator first,
			InputIterator last)
  {
   
    if(first == last)
      return std::abs(*first);
    
    InputIterator result = first;
   
      while (++first != last)
	{
	  
        if (std::abs(*result) < std::abs(*first))
          result = first;
	}
      return std::abs(*result);

  }
 
/**
   ** \brief Computes the infinite norm \f$\max_i{|x_i|}\f$ of a range according to a mask sequence.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/12
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  the infinite norm of the range.
   ** \pre [first, last) is a valid range
   ** \pre [mask_first, (last-first)) is a valid range
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M(2,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0);
   ** float d[] = {0,1,1,0,0,1};
   ** slip::Array2d<float> Mask(2,3,d);
   ** std::cout<<M<<std::endl;
   ** std::cout<<Mask<<std::endl;
   ** std::cout<<slip::infinite_norm_mask<float>(M.begin(),M.end(),Mask.begin(),1)<<std::endl
   ** \endcode
   **
   */
template<typename Value_T, typename InputIterator,typename MaskIterator>
  inline
   Value_T infinite_norm_mask(InputIterator first,
                         InputIterator last,
                         MaskIterator mask_first, 
                         typename std::iterator_traits<MaskIterator>::value_type value=typename
			      std::iterator_traits<MaskIterator>::value_type(1))

{
    if(first == last)
      return std::abs(*first);
   
    InputIterator result = first;
    ++mask_first;
    
      while (++first != last)
	{
	  if(*mask_first==value)
	    {
             if (std::abs(*result) < std::abs(*first))
               {
                result = first;
               }
          
            }
	  ++mask_first;
	}
      return std::abs(*result);

  }


/**  \brief Computes the infinite norm \f$\max_i{|x_i|}\f$ a range according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/12
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first     An input iterator.
   **  \param  last      An input iterator.
   **  \param  pred       A predicate.
   **  \return 
   ** \pre [first, last) is a valid range
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   **  slip::Array2d<float> M(2,3);
   ** slip::iota(M.begin(),M.end(),1.0,1.0)
   ** std::cout<<slip::infinite_norm_if<float>(M.begin(),M.end(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/
template<typename Value_T,
           typename InputIterator, 
           typename Predicate>
Value_T infinite_norm_if(InputIterator first,
                         InputIterator last,
			  Predicate pred)

{
 if(first == last)
      return std::abs(*first);
   
    InputIterator result = first;
   
    
      while (++first != last)
	{
	  if(pred(*first))
	    {
             if (std::abs(*result) < std::abs(*first))
               {
                result = first;
               }
          
            }
	
	}
      return std::abs(*result);
}



}//slip::


#endif //SLIP_NORMS_HPP
