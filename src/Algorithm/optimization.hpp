/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file optimization.hpp
 * \brief Provides some numerical optimization algorithms.
 * \since 1.5.0
 */
#ifndef SLIP_OPTIMIZATION_HPP
#define SLIP_OPTIMIZATION_HPP

#include <iostream>
#include <iterator>
#include <cmath>

namespace slip
{


 /**
   ** \brief Computes the zeros of a function using Newton-Raphson algorithm.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2015/03/03
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param f unary_function corresponding to the function.
   ** \param x0 x initialisation value.
   ** \param h derivative step.
   ** \param iterations Number of iterations.
   ** \param error approximation error.
   ** \param tolerance of the algorithm.
   ** \return a Real corresponding to the x such that f(x) = 0.
   ** \par Example of functor:
   ** \code
   ** template<typename Real>
   ** struct cubic_func : public std::unary_function<Real,Real>
   ** {
   **   Real operator()(const Real& x)
   **   {
   **   return x*x*x-static_cast<Real>(2.0)*x-static_cast<Real>(5.0);
   **   }
   ** };
   ** \endcode
   ** \par Example:
   ** \code
   **  std::size_t iterations = 200;
   ** double h = 0.0001;
   ** double x0 = 2.0;
   ** double tolerance = 1E-06;
   ** double error = 0.0;
   ** cubic_func<double> cubic_f;
   ** double x = slip::newton_raphson(cubic_f,x0,h,iterations,error,tolerance);
   ** std::cout<<"x = "<<x<<" iterations = "<<iterations<<" error = "<<error<<std::endl;
   ** \endcode
   */
template<typename Real,
	 typename Function>
Real newton_raphson(Function& f, 
		    const Real& x0,
		    const Real& h,
		    std::size_t& iterations,
		    Real& error,
		    const Real& tolerance = static_cast<Real>(1E-05))
{

  Real x = x0;
  std::size_t i = 0; 
  Real c = x + 1;
  Real df = (c-x);
  Real critere = std::abs(df);
  while((critere > tolerance) && (i < iterations))
    {
      c = x;
      Real y = f(x);
      x = x - (h*y)/(f(x+h)-y);
      df = (c-x);
      critere = std::abs(df);
      i = i + 1;
    }
  iterations = i;
  error = critere;
  return x;
}

}//::slip
#endif //SLIP_OPTIMIZATION_HPP
