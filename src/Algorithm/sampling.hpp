/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */
/** 
 * \file sampling.hpp
 * 
 * \brief Provides some algorithms to downsample and upsample ranges.
 * \since 1.5.0
 * 
 */

#ifndef SLIP_SAMPLING_HPP
#define SLIP_SAMPLING_HPP


#include <algorithm>
#include <numeric>
#include <cassert>
#include <cmath>
#include <functional>
#include "Array.hpp"
#include "Array2d.hpp"
#include "Vector.hpp"
#include "stl_algo_ext.hpp"
#include "statistics.hpp"
#include "kstride_iterator.hpp"
#include "stride_iterator.hpp"
#include "utils_compilation.hpp"
#include "container_cast.hpp"


namespace slip
{


  
  /**
   ** \brief Downsamples the range [first1, last1) by a factor M.  
   ** \author Thibault Roulier
   ** \date 2009/03/30
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param first1 A RandomAccessIterator to the beginning of the data range.
   ** \param last1  A RandomAccessIterator to one-past-the-end of the data range.
   ** \param result A RandomAccessIterator to the beginning of the data downsampled data range.
   ** \param M Downsampling factor (2 by default).
   ** \pre [first1,last1) is a valid range.
   ** \pre [result,result + (last1 -  first1)/M) is a valid range.
   ** \par Example:
   ** \code
   ** std::size_t M = 3;
   ** slip::Array<float> I(21);
   ** slip::iota(I.begin(),I.end(),1.0f);
   ** std::cout<<"I = \n"<<I<<std::endl;
   ** slip::Array<float> res_3(I.size()/M);
   ** slip::downsample1d(I.begin(), I.end(), res_3.begin(),M);
   ** std::cout<<"downsample_"<<M<<"(I) = \n"<<res<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator1, 
	   typename RandomAccessIterator2>
  inline
  void downsample1d(RandomAccessIterator1 first1,
		    RandomAccessIterator1 last1,
		    RandomAccessIterator2 result,
		    const std::size_t M = 2)
  {

    assert(first1 != last1);
    slip::stride_iterator<RandomAccessIterator1>  first(first1,M);
    slip::stride_iterator<RandomAccessIterator1>  last(last1,M);
    std::copy(first,last,result);

  }

 
  
  /**
   ** \brief Upsamples the range [first,last) by a factor M.
   ** \author Thibault Roulier
   ** \date 2009/03/30
   ** \version 0.0.1 
   ** \since 1.5.0
   ** \param first A RandomAccessIterator to the beginning of the data range.
   ** \param last  A RandomAccessIterator to one-past-the-end of the data range.
   ** \param result_first A RandomAccessIterator to the beginning of the up-sampled range.
   ** \param result_last A RandomAccessIterator to one-past-the-end up-sampled range.
   ** \param M The Upsampling factor (2 by default).
   ** \param zero A boolean to indicate if we copy (M-1) times the data (false) or if we copy (M-1) times zero (true) between the sample.
   ** \pre [first,last1) is a valid range.
   ** \pre [result,result + (last1 -  first)*M) is a valid range.
   ** \pre M >= 1
   ** \par Example:
   ** \code
   ** slip::Array<float> I(21);
   ** slip::iota(I.begin(),I.end(),1.0f);
   ** std::cout<<"I = \n"<<I<<std::endl; 
   ** slip::Array<float> res_up2(I.size()*2);
   ** slip::upsample1d(I.begin(), I.end(), res_up2.begin(),res_up2.end()); 
   ** std::cout<<"upsample_2(I) = \n"<<res_up2<<std::endl;
   ** slip::upsample1d(I.begin(), I.end(), res_up2.begin(),res_up2.end(),false)
   ** std::cout<<"upsample_2(I) = \n"<<res_up2<<std::endl;
   ** std::size_t M = 3;
   ** slip::Array<float> res_upM(I.size()*M);
   ** slip::upsample1d(I.begin(), I.end(), res_upM.begin(),res_upM.end(),M);
   ** std::cout<<"upsample_"<<M<<"(I) = \n"<<res_upM<<std::endl;
   ** slip::upsample1d(I.begin(), I.end(), res_upM.begin(),res_upM.end(),M,false);
   ** std::cout<<"upsample_"<<M<<"(I) = \n"<<res_upM<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator1, 
	   typename RandomAccessIterator2>
  inline
  void upsample1d(RandomAccessIterator1 first,
		RandomAccessIterator1 last,
		RandomAccessIterator2 result_first,
		RandomAccessIterator2 UNUSED(result_last),
		const std::size_t M = std::size_t(2),
		const bool zero = true)
  {
    std::cout<<"M = "<<M<<std::endl;
    assert(M >= 1);
    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_T;
    
    for(; first != last; ++first)
      {
	*result_first = *first;
	if(zero)
	  {
	    std::fill_n(result_first+1,M-1,value_T());
	  }
	else
	  {
	    std::fill_n(result_first+1,M-1,*first);
	  }
	result_first+=M;
      }
    
  }




  /**
   ** \brief Downsamples a 2d range by a factor M.
   ** \author Thibault Roulier
   ** \date 2009/03/30
   ** \version 0.0.1 
   ** \since 1.5.0
   ** \param upper_left A RandomAccessIterator2d to the beginning of the data range.
   ** \param bottom_right  A RandomAccessIterator2d to one-past-the-end of the data range.
   ** \param res_up A RandomAccessIterator2d to the beginning of the downsampled data range.
   ** \param M Downsampling factor.
   ** \pre [upper_left,bottom_right) is a valid range.
   ** \pre [res_up,res_up + (bottom_right -  upper_left)/M) is a valid range.
   ** \par Example:
   ** \code
   ** std::size_t M = 2;
   ** slip::Array2d<float> I2d(12, 21);
   ** slip::iota(I2d.begin(),I2d.end(),1.0f);
   ** std::size_t new_rows = std::ceil(double(I2d.rows())/double(M));
   ** std::size_t new_cols = std::ceil(double(I2d.cols())/double(M));
   ** slip::Array2d<float> res2d_2(new_rows, new_cols);
   **
   ** slip::downsample2d(I2d.upper_left(), I2d.bottom_right(), 
   **                       res2d_2.upper_left(),M);
   ** std::cout<<"I2d = \n"<<I2d<<std::endl;
   ** std::cout<<"downsample_"<<M<<"(I2d) = \n"<<res2d_2<<std::endl;
   **
   ** M = 3;
   ** new_rows = std::ceil(double(I2d.rows())/double(M));
   ** new_cols = std::ceil(double(I2d.cols())/double(M))
   ** slip::Array2d<float> res2d_3(new_rows,new_cols);
   ** slip::downsample2d(I2d.upper_left(), I2d.bottom_right(), 
   **			     res2d_3.upper_left(),M);
   ** std::cout<<"I2d = \n"<<I2d<<std::endl;
   ** std::cout<<"downsample_"<<M<<"(I2d) = \n"<<res2d_3<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator1, 
	   typename RandomAccessIterator>
  inline
  void downsample2d(RandomAccessIterator1 upper_left,
		    RandomAccessIterator1 bottom_right,
		    RandomAccessIterator res_up,
		    const std::size_t M = 2)
  {

    assert(upper_left != bottom_right);
    //    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_T;

    const std::size_t nb_rows = static_cast<std::size_t>((bottom_right - upper_left)[0]);
    const std::size_t nb_cols = static_cast<std::size_t>((bottom_right - upper_left)[1]);

    slip::DPoint2d<int> d(0,0);
    slip::DPoint2d<int> d2(0,0);
    for (std::size_t i = 0, ii = 0; i < nb_rows; i+=M, ++ii)
       {
	 d[0] = i; 
	 d2[0] = ii;
	 for(std::size_t j = 0, jj = 0 ; j < nb_cols; j+=M, ++jj)
	   {
	     d[1] = j;
	     d2[1] = jj;
	     *(res_up + d2) = *(upper_left + d);
	   }
       }
  }

  
   /**
   ** \brief up-samples a 2d range by a factor M.
   ** \author Thibault Roulier
   ** \date 2009/03/30
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left A RandomAccessIterator2d to the beginning of the data range.
   ** \param bottom_right A RandomAccessIterator2d to one-past-the-end of the data range.
   ** \param res_up A RandomAccessIterator2d to the beginning of the up-sampled data range.
   ** \param res_bot A RandomAccessIterator2d to one-past-the-end of the up-sampled data range.
   ** \param M upsampling factor.
   ** \param zero a boolean to indicate if we copy the data (false) or if we add zero(true)
   ** \pre [upper_left,bottom_right) is a valid range.
   ** \pre [res_up,res_up + (bottom_right -  upper_left)*M) is a valid range.
   ** \par Example:
   ** \code
   ** slip::Array2d<float> I2d(12, 21);
   ** slip::iota(I2d.begin(),I2d.end(),1.0f);
   ** slip::Array2d<float> I2d_up2(I2d.rows()*2,I2d.cols()*2);
   ** slip::upsample2d(I2d.upper_left(),I2d.bottom_right(),
   **		           I2d_up2.upper_left(),I2d_up2.bottom_right());
   ** std::cout<<"I2d  = \n"<<I2d<<std::endl;
   ** std::cout<<"I2d_up = \n"<<I2d_up2<<std::endl;
   ** std::size_t M = 2;
   ** I2d_up2.fill(0.0f);
   ** slip::upsample2d(I2d.upper_left(),I2d.bottom_right(),
   ** 		      I2d_up2.upper_left(),I2d_up2.bottom_right(),
   ** 		      M,
   ** 		      false);
   ** std::cout<<"I2d_up = \n"<<I2d_up2<<std::endl;
   ** M = 3;
   ** slip::Array2d<float> I2d_upM(I2d.rows()*M,I2d.cols()*M);
   ** slip::upsample2d(I2d.upper_left(),I2d.bottom_right(),
   ** 		      I2d_upM.upper_left(),I2d_upM.bottom_right(),
   **		      M,
   **		      true);
   ** std::cout<<"I2d_up_"<<M<<" = \n"<<I2d_upM<<std::endl;
   ** slip::upsample2d(I2d.upper_left(),I2d.bottom_right(),
   ** 		      I2d_upM.upper_left(),I2d_upM.bottom_right(),
   ** 		      M,
   ** 		      false);
   ** std::cout<<"I2d_up_"<<M<<" = \n"<<I2d_upM<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator2d1, 
	   typename RandomAccessIterator2d2>
  inline
  void upsample2d(RandomAccessIterator2d1 upper_left,
		  RandomAccessIterator2d1 bottom_right,
		  RandomAccessIterator2d2 res_up,
		  RandomAccessIterator2d2 res_bot,
		  const std::size_t M = 2,
		  const bool zero = true)
  {
    typedef typename std::iterator_traits<RandomAccessIterator2d1>::value_type T;
    const std::size_t rows = static_cast<std::size_t>((bottom_right - upper_left)[0]);
    const std::size_t cols = static_cast<std::size_t>((bottom_right - upper_left)[1]);
    assert(static_cast<std::size_t>((res_bot-res_up)[0]) == M*rows);
    assert(static_cast<std::size_t>((res_bot-res_up)[1]) == M*cols);

    if(zero)
      {
	std::fill(res_up,res_bot,T());
	slip::DPoint2d<int> d(0,0);
	slip::DPoint2d<int> d2(0,0);

	for (std::size_t i = 0, ii  = 0; i < rows; ++i, ii+=M)
	  {
	    d[0] = i; 
	    d2[0] = ii;
	    for(std::size_t j = 0, jj = 0 ; j < cols; ++j, jj+=M)
	      {
		d[1] = j;
		d2[1] = jj;
		*(res_up + d2) = *(upper_left + d);
	      }
	  }
      }
    else
      {
	slip::DPoint2d<int> d(0,0);
	slip::DPoint2d<int> d2_up(0,0);
	slip::DPoint2d<int> d2_bot(0,0);
	
	for (std::size_t i = 0, ii  = 0; i < rows; ++i, ii+=M)
	  {
	    d[0] = i; 
	    d2_up[0] = ii;
	    for(std::size_t j = 0, jj=0; j < cols; ++j, jj+=M)
	      {
		d[1] = j;
		d2_up[1] = jj;
		T val = *(upper_left + d);
		RandomAccessIterator2d2 it_b = res_up + d2_up;
		for(std::size_t k = 0; k < M; ++k)
		  {
		    std::fill(it_b.row_begin(ii+k)+jj,
			      it_b.row_begin(ii+k)+jj+M,
			      val);
		  }
	      }
	  }
      }
    
  }

  
 /**
   ** \brief Downsamples a 3d range by a factor M.
   ** \author Thibault Roulier
   ** \date 2009/03/30
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param fup A RandomAccessIterator3d to the beginning of the data range..
   ** \param bbr  A RandomAccessIterator3d to one-past-the-end of the data range.
   ** \param res_fup A RandomAccessIterator3d to the beginning of the downsampled data range.
   ** \param M Downsampling factor.
   ** \pre [fup,bbr) is a valid range.
   ** \pre [res_fup,res_fup + (res_bbr -  res_fup)*M) is a valid range.
   ** \par Example:
   ** \code
   ** std::size_t M = 2;
   ** slip::Array3d<float> I3d(6,5,4);
   ** slip::iota(I3d.begin(),I3d.end(),1.0f);
   ** std::size_t new_slices = std::ceil(double(I3d.slices())/double(M));
   ** std::size_t new_rows = std::ceil(double(I3d.rows())/double(M));
   ** std::size_t new_cols = std::ceil(double(I3d.cols())/double(M));
   ** slip::Array3d<float> res3d_2(new_slices,new_rows,new_cols);
   ** slip::downsample3d(I3d.front_upper_left(), I3d.back_bottom_right(), 
   **			     res3d_2.front_upper_left());
   ** std::cout<<"I3d = \n"<<I3d<<std::endl;
   ** M = 3;
   ** new_slices = std::ceil(double(I3d.slices())/double(M));
   ** new_rows = std::ceil(double(I3d.rows())/double(M));
   ** new_cols = std::ceil(double(I3d.cols())/double(M));
   ** slip::Array3d<float> res3d_M(new_slices,new_rows,new_cols);
   ** slip::downsample3d(I3d.front_upper_left(), I3d.back_bottom_right(), 
   **	   	 	     res3d_M.front_upper_left(),M);
   ** \endcode
   */
 template<typename RandomAccessIterator3d1, 
	  typename RandomAccessIterator3d2>
  inline
  void downsample3d(RandomAccessIterator3d1 fup,
		    RandomAccessIterator3d1 bbr,
		    RandomAccessIterator3d2 res_fup,
		    const std::size_t M = 2)
  {

    //typedef typename std::iterator_traits<RandomAccessIterator3d1>::value_type value_T;
    assert(fup != bbr);

    const std::size_t slices = static_cast<std::size_t>((bbr - fup)[0]);    
    const std::size_t rows = static_cast<std::size_t>((bbr - fup)[1]);
    const std::size_t cols = static_cast<std::size_t>((bbr - fup)[2]);

    slip::DPoint3d<int> d(0,0,0);
    slip::DPoint3d<int> d2(0,0,0);

    for (std::size_t k = 0, kk = 0; k < slices ; k+=M , ++kk)
    {
	    d[0] = k;
	    d2[0] = kk;
	    for (std::size_t i = 0, ii = 0; i < rows; i+=M, ++ii)
	       {
		 d[1] = i; 
		 d2[1] = ii;
		 for(std::size_t j = 0, jj = 0 ; j < cols; j+=M, ++jj)
		   {
		     d[2] = j;
		     d2[2] = jj;
		     *(res_fup + d2) = *(fup + d);
		   }
	    }
     }

  }
  
  /**
   ** \brief Subsamples a 3D range by a factor M.
   ** \author Thibault Roulier
   ** \date 2009/03/30
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param fup A RandomAccessIterator3d to the beginning of the data range.
   ** \param bbr   A RandomAccessIterator3d to one-past-the-end of the data range.
   ** \param res_fup A RandomAccessIterator3d to the beginning of the up-sampled data range.
   ** \param res_bbr A RandomAccessIterator3d to one-past-the-end of the up-sampled data range.
   ** \param zero a boolean to indicate if we copy the data (false) or if we add zero (true).
   ** \pre [fup,bbr) is a valid range.
   ** \pre [res_fup,res_fup + (res_bbr -  res_fup)*M) is a valid range.
   ** \par Example:
   ** \code
   ** std::size_t M = 2;
   ** slip::Array3d<float> I3d(6,5,4);
   ** slip::iota(I3d.begin(),I3d.end(),1.0f);
   ** slip::Array3d<float> I3d_up2(I3d.slices()*2,I3d.rows()*2,I3d.cols()*2);
   ** slip::upsample3d(I3d.front_upper_left(),I3d.back_bottom_right(),
   **	 	           I3d_up2.front_upper_left(),I3d_up2.back_bottom_right());
   ** std::cout<<"I3d  = \n"<<I3d<<std::endl;
   ** std::cout<<"I3d_up2 = \n"<<I3d_up2<<std::endl;
   ** I3d_up2.fill(0.0f);
   ** slip::upsample3d(I3d.front_upper_left(),I3d.back_bottom_right(),
   ** 		      I3d_up2.front_upper_left(),I3d_up2.back_bottom_right(),M,
   **		      false);
   ** std::cout<<"I3d  = \n"<<I3d<<std::endl;
   ** std::cout<<"I3d_up2 = \n"<<I3d_up2<<std::endl;
   ** M = 3;
   ** slip::Array3d<float> I3d_upM(I3d.slices()*M,I3d.rows()*M,I3d.cols()*M);
   ** slip::upsample3d(I3d.front_upper_left(),I3d.back_bottom_right(),
   ** 		           I3d_upM.front_upper_left(),I3d_upM.back_bottom_right(),M);
   ** std::cout<<"I3d  = \n"<<I3d<<std::endl;
   ** std::cout<<"I3d_upM = \n"<<I3d_upM<<std::endl;
   ** I3d_up2.fill(0.0f);
   ** slip::upsample3d(I3d.front_upper_left(),I3d.back_bottom_right(),
   **    		   I3d_upM.front_upper_left(),I3d_upM.back_bottom_right(),M,false);
   ** std::cout<<"I3d  = \n"<<I3d<<std::endl;
   ** std::cout<<"I3d_up"<<M<<" = \n"<<I3d_upM<<std::endl;
   ** \endcode
   */
 
 template<typename RandomAccessIterator3d1, 
	   typename RandomAccessIterator3d2>
  inline
  void upsample3d(RandomAccessIterator3d1 fup,
		  RandomAccessIterator3d1 bbr,
		  RandomAccessIterator3d2 res_fup,
		  RandomAccessIterator3d2 res_bbr,
		  const std::size_t M = 2,
		  const bool zero = true)
  {
    typedef typename std::iterator_traits<RandomAccessIterator3d1>::value_type T;
    const std::size_t slices = static_cast<std::size_t>((bbr - fup)[0]);
    const std::size_t rows   = static_cast<std::size_t>((bbr - fup)[1]);
    const std::size_t cols   = static_cast<std::size_t>((bbr - fup)[2]);
  
    assert(static_cast<std::size_t>((res_bbr-res_fup)[0]) == M*slices);
    assert(static_cast<std::size_t>((res_bbr-res_fup)[1]) == M*rows);
    assert(static_cast<std::size_t>((res_bbr-res_fup)[2]) == M*cols);

    if(zero)
      {
	std::fill(res_fup,res_bbr,T());
	slip::DPoint3d<int> d(0,0,0);
	slip::DPoint3d<int> d2(0,0,0);
	for (std::size_t k = 0, kk  = 0; k < slices; ++k, kk+=M)
	  {
	    d[0] = k; 
	    d2[0] = kk;
	    for (std::size_t i = 0, ii  = 0; i < rows; ++i, ii+=M)
	      {
		d[1] = i; 
		d2[1] = ii;
		for(std::size_t j = 0, jj = 0 ; j < cols; ++j, jj+=M)
		  {
		    d[2] = j;
		    d2[2] = jj;
		    *(res_fup + d2) = *(fup + d);
		  }
	      }
	  }
      }
    else
      {
	slip::DPoint3d<int> d(0,0,0);
	slip::DPoint3d<int> d2_fup(0,0,0);
	slip::DPoint3d<int> d2_bbr(0,0,0);
	for (std::size_t k = 0, kk  = 0; k < slices; ++k, kk+=M)
	  {
	    d[0] = k; 
	    d2_fup[0] = kk;
	    for (std::size_t i = 0, ii  = 0; i < rows; ++i, ii+=M)
	      {
		d[1] = i; 
		d2_fup[1] = ii;
		for(std::size_t j = 0, jj=0; j < cols; ++j, jj+=M)
		  {
		    d[2] = j;
		    d2_fup[2] = jj;
		    T val = *(fup + d);
		    RandomAccessIterator3d2 it_b = res_fup + d2_fup;
		    for(std::size_t l = 0; l < M; ++l)
		      {
			for(std::size_t m = 0; m < M; ++m)
			  {
			    std::fill(it_b.row_begin(kk+l,ii+m)+jj,
				      it_b.row_begin(kk+l,ii+m)+jj+M,
				      val);
			  }
		      }
		  }
	      }
	  }
      }
    
  }


template<typename>
  struct __downsample
       {
    template<typename RandomAccessIterator1, 
	     typename RandomAccessIterator2>
    static void 
   downsample(RandomAccessIterator1 first1,
	      RandomAccessIterator1 last1,
	      RandomAccessIterator2 result,
	      const std::size_t M = 2)
    {
      std::clog<<"generic downsample"<<std::endl;
    }

};


  template<>
  struct __downsample<std::random_access_iterator_tag>
  {
  
    template<typename RandomAccessIterator1, 
	     typename RandomAccessIterator2>
    static void 
    downsample(RandomAccessIterator1 in_first,
	       RandomAccessIterator1 in_last,
	       RandomAccessIterator2 out_first,
	       const std::size_t M = 2)
    {
      std::clog<<"downsample 1d"<<std::endl;
      slip::downsample1d(in_first,in_last,
			     out_first,
			     M);
    }
  };
    template<>
  struct __downsample<std::random_access_iterator2d_tag>
  {
  
    template<typename RandomAccessIterator1, 
	     typename RandomAccessIterator2>
    static void 
    downsample(RandomAccessIterator1 in_first,
	       RandomAccessIterator1 in_last,
	       RandomAccessIterator2 out_first,
	       const std::size_t M = 2)
    {
      std::clog<<"downsample 2d"<<std::endl;
      slip::downsample2d(in_first,in_last,
			     out_first,
			     M);
    }
   
  };

  template<>
  struct __downsample<std::random_access_iterator3d_tag>
  {
  
    template<typename RandomAccessIterator1, 
	     typename RandomAccessIterator2>
    static void 
    downsample(RandomAccessIterator1 in_first,
	       RandomAccessIterator1 in_last,
	       RandomAccessIterator2 out_first,
	       const std::size_t M = 2)
    {
      std::clog<<"downsample 3d"<<std::endl;
      slip::downsample3d(in_first,in_last,
			     out_first,
			     M);
    }
   
  };  

 /**
   ** \brief Downsamples the range [in_first, in_last) by a factor M.  
   ** The category (dimension) of the iterator is detected to apply 1d,
   ** 2d or 3d downsampling.
   ** \author Benoit Tremblais
   ** \date 2024/04/08
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param in_first A RandomAccessIterator to the beginning of the data range.
   ** \param in_last  A RandomAccessIterator to one-past-the-end of the data range.
   ** \param out_first A RandomAccessIterator to the beginning of the data downsampled data range.
   ** \param out_last A RandomAccessIterator  to one-past-the-end of the data downsampled data range.
   ** \param M Downsampling factor (2 by default).
   ** \pre [in_first,in_last) is a valid range.
   ** \pre [out_first,out_first + (in_last -  in_first)/M) is a valid range.
   ** \par Example:
   ** \code
   ** std::size_t M = 3;
   ** slip::Array<float> I(21);
   ** slip::iota(I.begin(),I.end(),1.0f);
   ** std::cout<<"I = \n"<<I<<std::endl;
   ** slip::Array<float> res_3(I.size()/M);
   ** slip::downsample(I.begin(), I.end(), res_3.begin(), res_3.end(),M);
   ** std::cout<<"downsample_"<<M<<"(I) = \n"<<res<<std::endl;
   ** \endcode
   ** \par Example 2:
   ** \code  
   ** std::size_t M = 3;
   ** slip::Array3d<float> I3d(6,5,4);
   ** slip::iota(I3d.begin(),I3d.end(),1.0f);
   ** std::size_t new_slices = std::ceil(double(I3d.slices())/double(M));
   ** std::size_t new_rows = std::ceil(double(I3d.rows())/double(M));
   ** std::size_t new_cols = std::ceil(double(I3d.cols())/double(M));
   ** slip::Array3d<float> res3d_M(new_slices,new_rows,new_cols);
   ** slip::downsample(I3d.front_upper_left(), I3d.back_bottom_right(), 
   **            res3d_M.front_upper_left(),res3d_M.back_bottom_right(),M);
   ** std::cout<<"I3d = \n"<<I3d<<std::endl;
   ** std::cout<<"downsample_"<<M<<"(I3d) = \n"<<res3d_M<<std::endl;
   ** \encode 
   */
   template<typename RandomAccessIterator1, 
	    typename RandomAccessIterator2>
    void downsample(RandomAccessIterator1 in_first,
		    RandomAccessIterator1 in_last,
		    RandomAccessIterator2 out_first,
		    RandomAccessIterator2 UNUSED(out_last),
		    const std::size_t M = 2)
   {
     typedef typename std::iterator_traits<RandomAccessIterator1>::iterator_category _Category;
     slip::__downsample<_Category>::downsample(in_first,in_last,
						    out_first,M);
   }



   /**
   ** \brief Downsamples the range [in_first, in_last) by a factor M.  
   ** The category (dimension) of the iterator is detected to apply 1d,
   ** 2d or 3d downsampling.
   ** \author Benoit Tremblais
   ** \date 2024/04/08
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param container1 A container 1d, 2d or 3d containing the data.

   ** \param container2  A container 1d, 2d or 3d containing the downsampled data.
   ** \param M Downsampling factor (2 by default).
   ** \pre container2.dimx() == ceil(double(container1.dimx())/double(M)), with x = 1, 2 or 3.
   ** \pre container1 and container2 elements are of the same value type.
   ** \par Example:
   ** \code
   ** std::size_t M = 3;
   ** slip::Array<float> I(21);
   ** slip::iota(I.begin(),I.end(),1.0f);
   ** std::cout<<"I = \n"<<I<<std::endl;
   ** slip::Array<float> res_3(I.size()/M);
   ** slip::downsample(I, res_3, M);
   ** std::cout<<"downsample_"<<M<<"(I) = \n"<<res<<std::endl;
   ** \endcode
   ** \par Example 2:
   ** \code  
   ** std::size_t M = 3;
   ** slip::Array3d<float> I3d(6,5,4);
   ** slip::iota(I3d.begin(),I3d.end(),1.0f);
   ** std::size_t new_slices = std::ceil(double(I3d.slices())/double(M));
   ** std::size_t new_rows = std::ceil(double(I3d.rows())/double(M));
   ** std::size_t new_cols = std::ceil(double(I3d.cols())/double(M));
   ** slip::Array3d<float> res3d_M(new_slices,new_rows,new_cols);
   ** slip::downsample(I3d,res3d_M,M);
   ** std::cout<<"I3d = \n"<<I3d<<std::endl;
   ** std::cout<<"downsample_"<<M<<"(I3d) = \n"<<res3d_M<<std::endl;
   ** \encode 
   */
    template<typename Container1,
	     typename Container2>
    void downsample(Container1& container1,
		    Container2& container2,
		    const std::size_t M = 2)
    {
      typedef typename std::iterator_traits<typename Container1::default_iterator>::iterator_category _Category;
      typename Container1::default_iterator in_first;
      typename Container1::default_iterator in_last;
      slip::container_cast(container1,in_first,in_last);
      typename Container2::default_iterator out_first;
      typename Container2::default_iterator out_last;
      slip::container_cast(container2,out_first,out_last);
      
      slip::__downsample<_Category>::downsample(in_first,in_last,
						    out_first,M);
    }

  template<typename>
  struct __upsample
       {
    template<typename RandomAccessIterator1, 
	     typename RandomAccessIterator2>
    static void 
    upsample(RandomAccessIterator1 first1,
	     RandomAccessIterator1 last1,
	     RandomAccessIterator2 res_first,
	     RandomAccessIterator2 res_last,
	     const std::size_t M = 2,
	     const bool zero = true)
    {
      std::clog<<"generic upsample"<<std::endl;
    }

  };
template<>
  struct __upsample<std::random_access_iterator_tag>
  {
  
    template<typename RandomAccessIterator1, 
	     typename RandomAccessIterator2>
    static void
    upsample(RandomAccessIterator1 in_first,
	     RandomAccessIterator1 in_last,
	     RandomAccessIterator2 res_first,
	     RandomAccessIterator2 res_last,
	     const std::size_t M = 2,
	     const bool zero = true)
    {
      std::clog<<"upsample 1d"<<std::endl;
      slip::upsample1d(in_first,in_last,
			   res_first,res_last,
			   M,
			   zero);
    }
  };
  
template<>
  struct __upsample<std::random_access_iterator2d_tag>
  {
  
    template<typename RandomAccessIterator1, 
	     typename RandomAccessIterator2>
    static void
    upsample(RandomAccessIterator1 in_first,
	     RandomAccessIterator1 in_last,
	     RandomAccessIterator2 res_first,
	     RandomAccessIterator2 res_last,
	     const std::size_t M = 2,
	     const bool zero = true)
    {
      std::clog<<"upsample 2d"<<std::endl;
      slip::upsample2d(in_first,in_last,
			   res_first,res_last,
			   M,
			   zero);
    }
  };

  template<>
  struct __upsample<std::random_access_iterator3d_tag>
  {
  
    template<typename RandomAccessIterator1, 
	     typename RandomAccessIterator2>
    static void
    upsample(RandomAccessIterator1 in_first,
	     RandomAccessIterator1 in_last,
	     RandomAccessIterator2 res_first,
	     RandomAccessIterator2 res_last,
	     const std::size_t M = 2,
	     const bool zero = true)
    {
      std::clog<<"upsample 2d"<<std::endl;
      slip::upsample3d(in_first,in_last,
			   res_first,res_last,
			   M,
			   zero);
    }
  };
 /**
   ** \brief Upsamples the range [in_first,in_last) by a factor M. 
   ** The category (dimension) of the iterator is detected to call the 1d, 2d
   ** or 3d upsampling function.
   ** \author Benoit Tremblais
   ** \date 2024/04/08
   ** \version 0.0.1 
   ** \since 1.5.0
   ** \param in_first A RandomAccessIterator to the beginning of the data range.
   ** \param in_last  A RandomAccessIterator to one-past-the-end of the data range.
   ** \param out_first A RandomAccessIterator to the beginning of the up-sampled range.
   ** \param out_last A RandomAccessIterator to one-past-the-end up-sampled range.
   ** \param M The Upsampling factor (2 by default).
   ** \param zero A boolean to indicate if we copy (M-1) times the data (false) or if we copy (M-1) times zero (true) between the sample.
   ** \pre [in_first,in_last) is a valid range.
   ** \pre [out_first,out_first + (in_last -  in_first)*M) is a valid range.
   ** \pre M >= 1
   ** \par Example:
   ** \code
   ** slip::Array<float> I(21);
   ** slip::iota(I.begin(),I.end(),1.0f);
   ** std::cout<<"I = \n"<<I<<std::endl; 
   ** slip::Array<float> res_up2(I.size()*2);
   ** slip::upsample(I.begin(), I.end(), res_up2.begin(),res_up2.end()); 
   ** std::cout<<"upsample_2(I) = \n"<<res_up2<<std::endl;
   ** slip::upsample(I.begin(), I.end(), res_up2.begin(),res_up2.end(),false)
   ** std::cout<<"upsample_2(I) = \n"<<res_up2<<std::endl;
   ** std::size_t M = 3;
   ** slip::Array<float> res_upM(I.size()*M);
   ** slip::upsample(I.begin(), I.end(), res_upM.begin(),res_upM.end(),M);
   ** std::cout<<"upsample_"<<M<<"(I) = \n"<<res_upM<<std::endl;
   ** slip::upsample(I.begin(), I.end(), res_upM.begin(),res_upM.end(),M,false);
   ** std::cout<<"upsample_"<<M<<"(I) = \n"<<res_upM<<std::endl;
   ** \endcode
   ** \par Example 2:
   ** \code 
   ** slip::Array3d<float> I3d(6,5,4);
   ** slip::iota(I3d.begin(),I3d.end(),1.0f);
   ** std::size_t M = 3;
   ** slip::Array3d<float> I3d_upM(I3d.slices()*M,I3d.rows()*M,I3d.cols()*M);
   ** slip::upsample(I3d.front_upper_left(),I3d.back_bottom_right(),
   **                    I3d_upM.front_upper_left(),I3d_upM.back_bottom_right(),
   **                    M, false);
   ** std::cout<<"I3d  = \n"<<I3d<<std::endl; 
   ** std::cout<<"I3d_up"<<M<<" = \n"<<I3d_upM<<std::endl;
   ** \endcode
   */
   template<typename RandomAccessIterator1, 
	    typename RandomAccessIterator2>
    void upsample(RandomAccessIterator1 in_first,
		  RandomAccessIterator1 in_last,
		  RandomAccessIterator2 out_first,
		  RandomAccessIterator2 out_last,
		  const std::size_t M = 2,
		  const bool zero = true)
   {
     typedef typename std::iterator_traits<RandomAccessIterator1>::iterator_category _Category;
     slip::__upsample<_Category>::upsample(in_first,in_last,
					       out_first,out_last,
					       M,
					       zero);
   }
  
  /**
   ** \brief Upsamples the 1d, 2d or 3d container container1 by a factor M. 
   ** The category (dimension) of the iterator is detected to call the 
   ** corresponding 1d, 2d or 3d upsampling function.
   ** \author Benoit Tremblais
   ** \date 2024/04/08
   ** \version 0.0.1 
   ** \since 1.5.0
   ** \param container1 A 1d, 2d or 3d container containing the data.
   ** \param container2 A 1d, 2d or 3d container containing the upsampled data.
   ** \param M The Upsampling factor (2 by default).
   ** \param zero A boolean to indicate if we copy (M-1) times the data (false) or if we copy (M-1) times zero (true) between the sample.
   ** \pre container2.dimx() = container1.dimx()*M
   ** \pre M >= 1
   ** \par Example:
   ** \code
   ** slip::Array<float> I(21);
   ** slip::iota(I.begin(),I.end(),1.0f);
   ** std::cout<<"I = \n"<<I<<std::endl; 
   ** slip::Array<float> res_up2(I.size()*2);
   ** slip::upsample(I, res_up2); 
   ** std::cout<<"upsample_2(I) = \n"<<res_up2<<std::endl;
   ** slip::upsample(I, res_up2,false)
   ** std::cout<<"upsample_2(I) = \n"<<res_up2<<std::endl;
   ** std::size_t M = 3;
   ** slip::Array<float> res_upM(I.size()*M);
   ** slip::upsample(I,res_upM,M);
   ** std::cout<<"upsample_"<<M<<"(I) = \n"<<res_upM<<std::endl;
   ** slip::upsample(I, res_upM,M,false);
   ** std::cout<<"upsample_"<<M<<"(I) = \n"<<res_upM<<std::endl;
   ** \endcode
   ** \par Example 2:
   ** \code 
   ** slip::Array3d<float> I3d(6,5,4);
   ** slip::iota(I3d.begin(),I3d.end(),1.0f);
   ** std::size_t M = 3;
   ** slip::Array3d<float> I3d_upM(I3d.slices()*M,I3d.rows()*M,I3d.cols()*M);
   ** slip::upsample(I3d,I3d_upM,M, false);
   ** std::cout<<"I3d  = \n"<<I3d<<std::endl; 
   ** std::cout<<"I3d_up"<<M<<" = \n"<<I3d_upM<<std::endl;
   ** \endcode
   */
    template<typename Container1,
	     typename Container2>
    void upsample(Container1& container1,
		  Container2& container2,
		  const std::size_t M = 2,
		  const bool zero = true)
    {
      typedef typename std::iterator_traits<typename Container1::default_iterator>::iterator_category _Category;
      typename Container1::default_iterator in_first;
      typename Container1::default_iterator in_last;
      slip::container_cast(container1,in_first,in_last);
      typename Container2::default_iterator out_first;
      typename Container2::default_iterator out_last;
      slip::container_cast(container2,out_first,out_last);
      
      slip::__upsample<_Category>::upsample(in_first,in_last,
						out_first,out_last,
						M,
						zero);
    }

  
}//::slip

#endif //SLIP_SAMPLING



