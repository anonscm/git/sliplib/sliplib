/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file relaxation.hpp
 * 
 * \brief Provides some system relaxation (iterative) solving algorithms.
 * 
 */
#ifndef SLIP_RELAXATION_HHP
#define SLIP_RELAXATION_HPP

#include <numeric>
#include <iostream>
#include "linear_algebra.hpp"
#include "arithmetic_op.hpp"
#include "norms.hpp"
#include "Array2d.hpp"
#include "DPoint2d.hpp"

namespace slip
{


  /**
   ** \brief Solve linear system AX = B by the Gauss-Siedel relaxation algorithm.
   ** The Gauss–Seidel method, also known as the Liebmann method or the method of successive displacement, is an iterative method used to solve system of linear equations.
   ** Though it can be applied to any matrix with non-zero elements on the diagonals, convergence is only guaranteed if the matrix is either strictly diagonally dominant, or symmetric and positive definite.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/01/25
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param A_up RandomAccessIterator2d to the matrix A of the system to solve.
   ** \param A_bot RandomAccessIterator2d to the matrix A of the system to solve.
   ** \param B_first RandomAccessIterator to the vector B of the system to solve.
   ** \param B_last RandomAccessIterator to the vector B of the system to solve.
   ** \param X0_first RandomAccessIterator to the Initial vector of the solution.
   ** \param X0_last RandomAccessIterator to the Initial vector of the solution.
   ** \param X_first RandomAccessIterator to the solution of the system.
   ** \param X_last RandomAccessIterator to the solution of the system.
   ** \param tol Tolerance on the error of the solution.
   ** \param kmax Maximal number of iterations.
   ** \pre (A_bot-A_up)[0] == (B_last-B_first)
   ** \pre (B_last - B_first) == (X0_last - X0_first)
   ** \pre (X_last - X_first) == (X0_last - X0_first)
   ** \pre The data of A, B, X0 and X should be of the same type.
   ** \par Example:
   ** \code
   ** double taba[] = {10.0,-1.0,2.0,0.0,-1.0,11.0,-1.0,3.0,2.0,-1.0,10.0,-1.0,0.0,3.0,-1.0,8.0};
   ** slip::Matrix<double> A(4,4,taba);
   ** double tabb[] = {6.0,25.0,-11.0,15.0};
   ** slip::Vector<double> B(4,tabb); 
   ** slip::Vector<double> X0(B.size()); 
   ** slip::Vector<double> X(B.size());
   ** slip::Vector<double> R(B.size());
   ** double tol = 1e-6;
   **  slip::gauss_seidel(A.upper_left(),
   ** 		          A.bottom_right(),
   **		          B.begin(),B.end(),
   **	                  X0.begin(),X0.end(),
   **		          X.begin(),X.end(),
   **		          tol,
   **		          100);
   **
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** std::cout<<"B = \n"<<B<<std::endl;
   ** std::cout<<"X0 = \n"<<X0<<std::endl;
   ** std::cout<<"X = \n"<<X<<std::endl;
   ** std::cout<<"R = AX\n"<<R<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator2d,
	   typename RandomAccessIterator1, 
	   typename RandomAccessIterator2,
	   typename RandomAccessIterator3>
  void gauss_seidel(RandomAccessIterator2d A_up,
		    RandomAccessIterator2d A_bot,
		    RandomAccessIterator1 B_first,
		    RandomAccessIterator1 B_last,
		    RandomAccessIterator2 X0_first,
		    RandomAccessIterator2 X0_last,
		    RandomAccessIterator3 X_first,
		    RandomAccessIterator3 X_last,
		    const typename std::iterator_traits<RandomAccessIterator2d>::value_type tol = static_cast<typename std::iterator_traits<RandomAccessIterator2d>::value_type>(1e-6),
		    const std::size_t kmax = std::size_t(100))
  {
    assert((A_bot-A_up)[0] == (B_last-B_first));
    assert((B_last - B_first) == (X0_last - X0_first));
    assert((X_last - X_first) == (X0_last - X0_first));

    typedef typename std::iterator_traits<RandomAccessIterator2d>::value_type value_type;
    std::size_t k = 0; 
    std::size_t size = static_cast<std::size_t>(B_last-B_first);
    //init X to X0
    std::copy(X0_first,X0_last,X_first);
    //compute the residual error
    slip::Array<value_type> r(size);
    slip::Array<value_type> tmp(size);
    slip::matrix_vector_multiplies(A_up,A_bot,
				   X_first,X_last,
				   tmp.begin(),tmp.end());
    slip::minus(B_first,B_last,tmp.begin(),r.begin());
    value_type test = slip::L2_norm<value_type>(r.begin(),r.end());
    slip::DPoint2d<int> d;
    while ((test >= tol) && (k < kmax))
      {
	for(std::size_t i = 0; i < size; ++i)
	  {
	    d.set_coord(i,i);
	
	    value_type s = 
	      std::inner_product(A_up.row_begin(i),A_up.row_begin(i)+i,
				 X_first,
				 value_type(0));
	  
	    s = std::inner_product(A_up.row_begin(i)+(i+1),A_up.row_end(i),
			       X_first+(i+1),
			       s);
	    *(X_first+i) = (*(B_first + i) - s)/A_up[d];
	     
	  }
	//computes the residual error
	slip::matrix_vector_multiplies(A_up,A_bot,
				       X_first,X_last,
				       tmp.begin(),tmp.end());
	slip::minus(B_first,B_last,tmp.begin(),r.begin());
	test = slip::L2_norm<value_type>(r.begin(),r.end());
	k += 1;
      }
  }
  

   /**
   ** \brief Solve linear system AX = B by the Gauss-Siedel relaxation algorithm.
   ** The Gauss–Seidel method, also known as the Liebmann method or the method of successive displacement, is an iterative method used to solve system of linear equations.
   ** Though it can be applied to any matrix with non-zero elements on the diagonals, convergence is only guaranteed if the matrix is either strictly diagonally dominant, or symmetric and positive definite.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : conceptor  
   ** \date 2009/01/25
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param A. The matrix of the system.
   ** \param B. The vector of the system.
   ** \param X0. Initial value of the solution.
   ** \param X. Solution of the linear system.
   ** \param tol. Tolerance on the error of the solution.
   ** \param kmax. Maximal number of iterations.
   ** \pre A.cols() = B.size()
   ** \pre B, X0 and X must have the same sizes
   ** \pre The data of A, B, X0 and X should be of the same type.
   ** \par Example:
   ** \code
   ** double taba[] = {10.0,-1.0,2.0,0.0,-1.0,11.0,-1.0,3.0,2.0,-1.0,10.0,-1.0,0.0,3.0,-1.0,8.0};
   ** slip::Matrix<double> A(4,4,taba);
   ** double tabb[] = {6.0,25.0,-11.0,15.0};
   ** slip::Vector<double> B(4,tabb); 
   ** slip::Vector<double> X0(B.size()); 
   ** slip::Vector<double> X(B.size());
   ** slip::Vector<double> R(B.size());
   ** double tol = 1e-6;

   ** slip::gauss_seidel(A,B,X0,X,tol,100);
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** std::cout<<"B = \n"<<B<<std::endl;
   ** std::cout<<"X0 = \n"<<X0<<std::endl;
   ** std::cout<<"X = \n"<<X<<std::endl;
  
   ** slip::matrix_vector_multiplies(A,X,R);
   ** std::cout<<"R = AX\n"<<R<<std::endl;
   ** \endcode
   */
  template<typename Matrix,
	   typename Vector1,
	   typename Vector2>
  void gauss_seidel(const Matrix& A, 
		    const Vector1& B,
		    const Vector2& X0,
		    Vector2& X,
		    const typename Matrix::value_type tol = static_cast<typename Matrix::value_type>(1e-6),
		    const std::size_t kmax = std::size_t(100))
  {
    assert(A.rows() == B.size());
    assert(B.size() == X0.size());
    assert(X0.size() == X.size());
    typedef typename Matrix::value_type value_type;
    std::size_t k = 0; 
    std::size_t size = B.size();
    //init X to X0
    std::copy(X0.begin(),X0.end(),X.begin());
    //compute the residual error
    Vector2 r(size);
    Vector2 tmp(size);
    slip::matrix_vector_multiplies(A,X,tmp);
    slip::minus(B.begin(),B.end(),tmp.begin(),r.begin());
    value_type test = slip::L2_norm<value_type>(r.begin(),r.end());
  
    while ((test >= tol) && (k < kmax))
      {
	for(std::size_t i = 0; i < size; ++i)
	  {
	    value_type s = value_type(0);
	    for(std::size_t j = 0; j < i ; ++j)
	      {
		s = s + A[i][j] * X[j];
	      }
	    for(std::size_t j = (i+1); j < size; ++j)
	      {
		s = s + A[i][j] * X[j];
	      }
	    X[i] = (B[i] - s)/A[i][i];
	  }
	//computes the residual error
	slip::matrix_vector_multiplies(A,X,tmp);
	slip::minus(B.begin(),B.end(),tmp.begin(),r.begin());
	test = slip::L2_norm<value_type>(r.begin(),r.end());
	k += 1;
      }
  }
  


 /**
   ** \brief Solve linear system AX = B by the Successive Over Relaxation algorithm relaxation algorithm.
   ** The method of successive over-relaxation (SOR) is a variant of the Gauss–Seidel method for solving a linear system of equations, resulting in faster convergence. 
   ** The choice of relaxation factor w is not necessarily easy, and depends upon the properties of the coefficient matrix.
   ** In 1947, Ostrowski proved that if A is symmetric and positive-definite then  \f$ 0.0 < w < 2.0\f$ the algorithm converge.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/01/25
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param A_up RandomAccessIterator2d to the matrix A of the system to solve.
   ** \param A_bot RandomAccessIterator2d to the matrix A of the system to solve.
   ** \param B_first RandomAccessIterator to the vector B of the system to solve.
   ** \param B_last RandomAccessIterator to the vector B of the system to solve.
   ** \param X0_first RandomAccessIterator to the Initial vector of the solution.
   ** \param X0_last RandomAccessIterator to the Initial vector of the solution.
   ** \param w. relaxation parameter.
   ** \param X_first RandomAccessIterator to the solution of the system.
   ** \param X_last RandomAccessIterator to the solution of the system.
   ** \param tol Tolerance on the error of the solution.
   ** \param kmax Maximal number of iterations.
   ** \pre (A_bot-A_up)[0] == (B_last-B_first)
   ** \pre (B_last - B_first) == (X0_last - X0_first)
   ** \pre (X_last - X_first) == (X0_last - X0_first)
   ** \pre w must belong to ]0.0,2.0[
   ** \pre The data of A, B, X0 and X should be of the same type.
   ** \par Example:
   ** \code
   ** double taba[] = {10.0,-1.0,2.0,0.0,-1.0,11.0,-1.0,3.0,2.0,-1.0,10.0,-1.0,0.0,3.0,-1.0,8.0};
   ** slip::Matrix<double> A(4,4,taba);
   ** double tabb[] = {6.0,25.0,-11.0,15.0};
   ** slip::Vector<double> B(4,tabb); 
   ** slip::Vector<double> X0(B.size()); 
   ** slip::Vector<double> X(B.size());
   ** slip::Vector<double> R(B.size());
   ** double tol = 1e-6; 
   ** double w = 1.0;
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** std::cout<<"B = \n"<<B<<std::endl;
   ** std::cout<<"X0 = \n"<<X0<<std::endl;
   ** std::cout<<"w = "<<w<<std::endl;
   **
   ** slip::SOR(A.upper_left(),A.bottom_right(),
   **  	       B.begin(),B.end(),
   **           X0.begin(),X0.end(),
   **           w,
   **	       X.begin(),X.end(),
   **           tol,
   **           100);
   ** std::cout<<"X = \n"<<X<<std::endl;
   ** slip::matrix_vector_multiplies(A,X,R);
   ** std::cout<<"R = AX\n"<<R<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator2d,
	   typename RandomAccessIterator1, 
	   typename RandomAccessIterator2,
	   typename RandomAccessIterator3>
  void SOR(RandomAccessIterator2d A_up,
	   RandomAccessIterator2d A_bot,
	   RandomAccessIterator1 B_first,
	   RandomAccessIterator1 B_last,
	   RandomAccessIterator2 X0_first,
	   RandomAccessIterator2 X0_last,
	 
	   const typename std::iterator_traits<RandomAccessIterator2d>::value_type w,
	   RandomAccessIterator3 X_first,
	   RandomAccessIterator3 X_last,
	   const typename std::iterator_traits<RandomAccessIterator2d>::value_type tol= static_cast<typename std::iterator_traits<RandomAccessIterator2d>::value_type>(1e-6),
	   const std::size_t kmax= std::size_t(100))
  {
    assert((A_bot-A_up)[0] == (B_last-B_first));
    assert((B_last - B_first) == (X0_last - X0_first));
    assert((X_last - X_first) == (X0_last - X0_first));
    assert(w > typename std::iterator_traits<RandomAccessIterator2d>::value_type() && w < static_cast<typename std::iterator_traits<RandomAccessIterator2d>::value_type>(2.0));

    typedef typename std::iterator_traits<RandomAccessIterator2d>::value_type value_type;

    std::size_t k = 0; 
    std::size_t size = static_cast<std::size_t>(B_last - B_first);
    //init X to X0
    std::copy(X0_first,X0_last,X_first);
    //computes the residual error
    slip::Array<value_type> r(size);
    slip::Array<value_type> tmp(size);
    slip::matrix_vector_multiplies(A_up,A_bot,
				   X_first,X_last,
				   tmp.begin(),tmp.end());
    slip::minus(B_first,B_last,tmp.begin(),r.begin());
    value_type test = slip::L2_norm<value_type>(r.begin(),r.end());
    //auxilary vector
    slip::Array<value_type> Xaux(size);
    slip::DPoint2d<int> d;
    while ((test >= tol) && (k < kmax))
      {
	for(std::size_t i = 0; i < size; ++i)
	  {
	    d.set_coord(i,i);
	    Xaux[i] = 
	      std::inner_product(A_up.row_begin(i),A_up.row_begin(i)+i,
				 Xaux.begin(),
				 value_type(0));
	     Xaux[i] = std::inner_product(A_up.row_begin(i)+(i+1),
					  A_up.row_end(i),
					  X_first+(i+1),
					  Xaux[i]);
	     Xaux[i] = (*(B_first + i) - Xaux[i])/A_up[d];
	     Xaux[i] = *(X_first + i) + w * (Xaux[i] - *(X_first + i));
	  }
	std::copy(Xaux.begin(),Xaux.end(),X_first);
	//computes the residual error
	slip::matrix_vector_multiplies(A_up,A_bot,
				       X_first,X_last,
				       tmp.begin(),tmp.end());
	slip::minus(B_first,B_last,tmp.begin(),r.begin());
	test = slip::L2_norm<value_type>(r.begin(),r.end());
	k += 1;
      }
  }

  /**
   ** \brief Solve linear system AX = B by the Successive Over Relaxation algorithm.
   ** The method of successive over-relaxation (SOR) is a variant of the Gauss–Seidel method for solving a linear system of equations, resulting in faster convergence. 
   ** The choice of relaxation factor w is not necessarily easy, and depends upon the properties of the coefficient matrix.
   ** In 1947, Ostrowski proved that if A is symmetric and positive-definite then  /f$ 0.0 < w < 2.0/f$ the algorithm converge.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : conceptor  
   ** \date 2009/01/25
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param A. The matrix of the system.
   ** \param B. The vector of the system.
   ** \param X0. Initial value of the solution.
  
   ** \param w. relaxation parameter.
   ** \param X. The solution of the linear system.
   ** \param tol. Tolerance on the error of the solution.
   ** \param kmax. Maximal number of iterations.
   ** \pre A.cols() = B.size()
   ** \pre B, X0 and X must have the same sizes
   ** \pre The data of A, B, X0 and X should be of the same type.
   ** \pre w must belong to ]0.0,2.0[
   ** \par Example:
   ** \code
   ** double taba[] = {10.0,-1.0,2.0,0.0,-1.0,11.0,-1.0,3.0,2.0,-1.0,10.0,-1.0,0.0,3.0,-1.0,8.0};
  ** slip::Matrix<double> A(4,4,taba);
  ** double tabb[] = {6.0,25.0,-11.0,15.0};
  ** slip::Vector<double> B(4,tabb); 
  ** slip::Vector<double> X0(B.size()); 
  ** slip::Vector<double> X(B.size());
  ** slip::Vector<double> R(B.size());
  ** double tol = 1e-6; 
  ** double w = 1.0;
  ** std::cout<<"A = \n"<<A<<std::endl;
  ** std::cout<<"B = \n"<<B<<std::endl;
  ** std::cout<<"X0 = \n"<<X0<<std::endl;
  ** std::cout<<"w = "<<w<<std::endl;
  **
  ** slip::SOR(A,B,X0,w,X,tol,100);
  **
  ** std::cout<<"X = \n"<<X<<std::endl;
  ** slip::matrix_vector_multiplies(A,X,R);
  ** std::cout<<"R = AX\n"<<R<<std::endl;
  ** \endcode
  */
  template<typename Matrix,
	   typename Vector1,
	   typename Vector2>
  void SOR(const Matrix& A, 
	   const Vector1& B,
	   const Vector2& X0,
	   const typename Matrix::value_type w,
	   Vector2& X,
	   const typename Matrix::value_type tol = static_cast<typename Matrix::value_type>(1e-6),
	   const std::size_t kmax = std::size_t(100))
  {
    assert(A.rows() == B.size());
    assert(B.size() == X0.size());
    assert(X0.size() == X.size());
    typedef typename Matrix::value_type value_type;
    std::size_t k = 0; 
    std::size_t size = B.size();
    //init X to X0
    std::copy(X0.begin(),X0.end(),X.begin());
    //computes the residual error
    Vector2 r(size);
    Vector2 tmp(size);
    slip::matrix_vector_multiplies(A,X,tmp);
    slip::minus(B.begin(),B.end(),tmp.begin(),r.begin());
    value_type test = slip::L2_norm<value_type>(r.begin(),r.end());
    //auxilary vector
    Vector2 Xaux(size);

    while ((test >= tol) && (k < kmax))
      {
	for(std::size_t i = 0; i < size; ++i)
	  {
	    Xaux[i] = value_type(0);
	    for(std::size_t j = 0; j < i; ++j)
	      {
		Xaux[i] = Xaux[i] + A[i][j] * Xaux[j];
	      }
	    for(std::size_t j = (i+1); j < size; ++j)
	      {
		Xaux[i] = Xaux[i] + A[i][j] * X[j];
	      }
	    Xaux[i] = (B[i] - Xaux[i])/A[i][i];
	    Xaux[i] = X[i] + w * (Xaux[i] - X[i]);
	  }
	std::copy(Xaux.begin(),Xaux.end(),X.begin());
	//computes the residual error
	slip::matrix_vector_multiplies(A,X,tmp);
	slip::minus(B.begin(),B.end(),tmp.begin(),r.begin());
	test = slip::L2_norm<value_type>(r.begin(),r.end());
	k += 1;
      }
  }

/**
   ** \brief Solve linear system AX = B by the Jacobi relaxation algorithm.
   ** Jacobi algorithm is an iterative algorithm for determining the solutions of a strictly diagonally dominant (\f$|a_{ii}| \ge \sum_{j\ne i} |a_{ij}|\, \forall i \f$) system of linear equations. Each diagonal element is solved for, and an approximate value is plugged in. The process is then iterated until it converges.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/01/25
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param A_up RandomAccessIterator2d to the matrix A of the system to solve.
   ** \param A_bot RandomAccessIterator2d to the matrix A of the system to solve.
   ** \param B_first RandomAccessIterator to the vector B of the system to solve.
   ** \param B_last RandomAccessIterator to the vector B of the system to solve.
   ** \param X0_first RandomAccessIterator to the Initial vector of the solution.
   ** \param X0_last RandomAccessIterator to the Initial vector of the solution.
   ** \param X_first RandomAccessIterator to the solution of the system.
   ** \param X_last RandomAccessIterator to the solution of the system.
   ** \param tol Tolerance on the error of the solution.
   ** \param kmax Maximal number of iterations.
   ** \pre (A_bot-A_up)[0] == (B_last-B_first)
   ** \pre (B_last - B_first) == (X0_last - X0_first)
   ** \pre (X_last - X_first) == (X0_last - X0_first)
   ** \pre \f$|a_{ii}| \ge \sum_{j\ne i} |a_{ij}|\, \forall i \f$
   ** \pre The data of A, B, X0 and X should be of the same type.
   ** \par Example:
   ** \code
   **  double taba[] = {10.0,-1.0,2.0,0.0,-1.0,11.0,-1.0,3.0,2.0,-1.0,10.0,-1.0,0.0,3.0,-1.0,8.0};
   ** slip::Matrix<double> A(4,4,taba);
   ** double tabb[] = {6.0,25.0,-11.0,15.0};
   ** slip::Vector<double> B(4,tabb); 
   ** slip::Vector<double> X0(B.size()); 
   ** slip::Vector<double> X(B.size());
   ** slip::Vector<double> R(B.size());
   ** double tol = 1e-6;
   **
   ** slip::jacobi(A.upper_left(),A.bottom_right(),
   **              B.begin(),B.end(),
   **	           X0.begin(),X0.end(),
   **	           X.begin(),X.end(),
   **	           tol,100);
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** std::cout<<"B = \n"<<B<<std::endl;
   ** std::cout<<"X0 = \n"<<X0<<std::endl;
   ** std::cout<<"X = \n"<<X<<std::endl;
   ** slip::matrix_vector_multiplies(A,X,R);
   ** std::cout<<"R = AX\n"<<R<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator2d,
	   typename RandomAccessIterator1, 
	   typename RandomAccessIterator2,
	   typename RandomAccessIterator3>
  void jacobi(RandomAccessIterator2d A_up,
	      RandomAccessIterator2d A_bot,
	      RandomAccessIterator1 B_first,
	      RandomAccessIterator1 B_last,
	      RandomAccessIterator2 X0_first,
	      RandomAccessIterator2 X0_last,
	     
	      RandomAccessIterator3 X_first,
	      RandomAccessIterator3 X_last,
	      const typename std::iterator_traits<RandomAccessIterator2d>::value_type tol=static_cast<typename std::iterator_traits<RandomAccessIterator2d>::value_type>(1e-6),
	      const std::size_t kmax=std::size_t(100))
  {
    assert((A_bot-A_up)[0] == (B_last-B_first));
    assert((B_last - B_first) == (X0_last - X0_first));
    assert((X_last - X_first) == (X0_last - X0_first));

    typedef typename std::iterator_traits<RandomAccessIterator2d>::value_type value_type;
    std::size_t k = 0; 
    std::size_t size = static_cast<std::size_t>(B_last - B_first);
    //init X to X0
    std::copy(X0_first,X0_last,X_first);
    
    //auxilary vector
    slip::Array<value_type> Xaux(size);
    //residual vector
    slip::Array<value_type> r(size);
    //init stop criteria
    value_type test = tol + value_type(1);
    slip::DPoint2d<int> d;
    while ((test >= tol) && (k < kmax))
      {
	for(std::size_t i = 0; i < size; ++i)
	  {
	    d.set_coord(i,i);
	    Xaux[i] = 
	      std::inner_product(A_up.row_begin(i),A_up.row_begin(i)+i,
				 X_first,
				 value_type(0));
	    Xaux[i] = std::inner_product(A_up.row_begin(i)+(i+1),
					 A_up.row_end(i),
					 X_first+(i+1),
					 Xaux[i]);
	    Xaux[i] = (*(B_first + i) - Xaux[i])/A_up[d];
	  }

	slip::minus(Xaux.begin(),Xaux.end(),X_first,r.begin());
	test = slip::L2_norm<value_type>(r.begin(),r.end());
	std::copy(Xaux.begin(),Xaux.end(),X_first);
	k += 1;
      }
  }

 /**
  ** \brief Solve linear system AX = B by the Jacobi relaxation algorithm.
  ** Jacobi algorithm is an iterative algorithm for determining the solutions of a strictly diagonally dominant (\f$|a_ii| \ge \sum_{j\ne i} |a_{ij}|\, \forall i \f$) system of linear equations. Each diagonal element is solved for, and an approximate value is plugged in. The process is then iterated until it converges.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : conceptor  
   ** \date 2009/01/25
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param A. The matrix of the system.
   ** \param B. The vector of the system.
   ** \param X0. Initial value of the solution.
   ** \param X. The solution of the linear system. 
   ** \param tol. Tolerance on the error of the solution.
   ** \param kmax. Maximal number of iterations.
   ** \pre A.cols() = B.size()
   ** \pre B, X0 and X must have the same sizes
   ** \pre The data of A, B, X0 and X should be of the same type.
   ** \pre \f$|a_ii| \ge \sum_{j\ne i} |a_{ij}|\, \forall i \f$
   ** \par Example:
   ** \code
   **  double taba[] = {10.0,-1.0,2.0,0.0,-1.0,11.0,-1.0,3.0,2.0,-1.0,10.0,-1.0,0.0,3.0,-1.0,8.0};
   ** slip::Matrix<double> A(4,4,taba);
   ** double tabb[] = {6.0,25.0,-11.0,15.0};
   ** slip::Vector<double> B(4,tabb); 
   ** slip::Vector<double> X0(B.size()); 
   ** slip::Vector<double> X(B.size());
   ** slip::Vector<double> R(B.size());
   ** double tol = 1e-6;
   **
   ** slip::jacobi(A,B,X0,X,tol,100);
   **
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** std::cout<<"B = \n"<<B<<std::endl;
   ** std::cout<<"X0 = \n"<<X0<<std::endl;
   ** std::cout<<"X = \n"<<X<<std::endl;
   ** slip::matrix_vector_multiplies(A,X,R);
   ** std::cout<<"R = AX\n"<<R<<std::endl;
   ** \endcode
   */
 template<typename Matrix,
	  typename Vector1,
	  typename Vector2>
  void jacobi(const Matrix& A, 
	      const Vector1& B,
	      const Vector2& X0,
	      Vector2& X,
	      const typename Matrix::value_type tol = static_cast<typename Matrix::value_type>(1e-6),
	      const std::size_t kmax= std::size_t(100))
  {
    assert(A.rows() == B.size());
    assert(B.size() == X0.size());
    assert(X0.size() == X.size());
    typedef typename Matrix::value_type value_type;
    std::size_t k = 0; 
    std::size_t size = B.size();
    //init X to X0
    std::copy(X0.begin(),X0.end(),X.begin());
    
    //auxilary vector
    Vector2 Xaux(size);
    //residual vector
    Vector2 r(size);
    //init stop criteria
    value_type test = tol + value_type(1);
    while ((test >= tol) && (k < kmax))
      {
	for(std::size_t i = 0; i < size; ++i)
	  {
	    Xaux[i] = value_type(0);
	    for(std::size_t j = 0; j < i; ++j)
	      {
		Xaux[i] = Xaux[i] + A[i][j] * X[j];
	      }
	    for(std::size_t j = (i+1); j < size; ++j)
	      {
		Xaux[i] = Xaux[i] + A[i][j] * X[j];
	      }
	    Xaux[i] = (B[i] - Xaux[i])/A[i][i];
	  }

	slip::minus(Xaux.begin(),Xaux.end(),X.begin(),r.begin());
	test = slip::L2_norm<value_type>(r.begin(),r.end());
	std::copy(Xaux.begin(),Xaux.end(),X.begin());
	//computes the residual error

	k += 1;
      }
  }


}//slip::
#endif //SLIP_RELAXATION
