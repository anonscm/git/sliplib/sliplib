/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file color_spaces.hpp
 * 
 * \brief Provides some functors to change of color space.
 * \since 1.0.0
 */
#ifndef SLIP_COLOR_SPACES_HPP
#define SLIP_COLOR_SPACES_HPP

#include "Color.hpp"
#include <iostream>
#include <cassert>
#include <cmath>
#include "macros.hpp"

namespace slip
{

   /**
   ** \brief Converts a RGB color to a luminance value.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/09/13
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param RGB A const slip::Color<InType> reference.
   ** \return An OutType value corresponding to the luminance.
   **
   ** \code  
   **  L = 0.3 * R + 0.59 * G + 0.11 * B
   ** \endcode
   **
   */
  template<typename InType, typename OutType>
  struct RGBtoL
  {
    OutType operator()(const slip::Color<InType>& RGB) const
    {
      return OutType(0.3 * RGB[0] + 0.59 * RGB[1] + 0.11 * RGB[3]);
    }
  };

   /**
   ** \brief Converts a RGB color to XYZ color.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/09/13
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param RGB A const slip::Color<InType> reference.
   ** \return A slip::Color<OutType> value corresponding to the XYZ value.
   **
   ** \code 
   **  X = 0.490 * R + 0.310 * G + 0.200 * B;
   **  Y = 0.177 * R + 0.812 * G + 0.010 * B;
   **  Z =             0.010 * G + 0.990 * B;
   ** \endcode
   */
  template<typename InType, typename OutType>
  struct RGBtoXYZ
  {
    slip::Color<OutType> operator()(const slip::Color<InType>& RGB) const
    {
      float x   = 0.490 * RGB[0] + 0.310 * RGB[1] + 0.200 * RGB[2];
      float y   = 0.177 * RGB[0] + 0.812 * RGB[1] + 0.010 * RGB[2];
      float z   =                  0.010 * RGB[1] + 0.990 * RGB[2];
      return slip::Color<OutType>(OutType(x),OutType(y),OutType(z));
    }
  };

   /**
   ** \brief Converts a XYZ color to RGB color.
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param XYZ A const slip::Color<InType> reference.
   ** \return A slip::Color<OutType> value corresponding to the RGB value.
   **
   ** \code 
   **  R = 2.364 * X  - 0.896 * Y - 0.564 * Z;
   **  G = -0.515 * X + 1.427 * Y + 0.111 * Z;
   **  B = 0.005 * X  - 0.014 * Y + 1.001 * Z;
   ** \endcode
   */
  template<typename InType, typename OutType>
  struct XYZtoRGB
  {
    slip::Color<OutType> operator()(const slip::Color<InType>& XYZ) const
    {
      float r   =  2.364 * XYZ[0]  - 0.896 * XYZ[1] - 0.564 * XYZ[2];
      float g   = -0.515 * XYZ[0]  + 1.427 * XYZ[1] + 0.111 * XYZ[2];
      float b   =  0.005 * XYZ[0]  - 0.014 * XYZ[1] + 1.001 * XYZ[2];
      return slip::Color<OutType>(OutType(r),OutType(g),OutType(b));
    }
  };

   /**
   ** \brief Converts a RGB color to XYZ color. Another version (not the same white reference)
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param RGB A const slip::Color<InType> reference.
   ** \return A slip::Color<OutType> value corresponding to the XYZ value.
   **
   ** \code 
   **  X = 0.412453 * R + 0.357580 * G + 0.180423 * B;
   **  Y = 0.212671 * R + 0.715160 * G + 0.072169 * B;
   **  Z = 0.019334 * R + 0.119193 * G + 0.950227 * B;
   ** \endcode
   */
  template<typename InType, typename OutType>
  struct RGBtoXYZ_v2
  {
    slip::Color<OutType> operator()(const slip::Color<InType>& RGB) const
    {
      float x   = 0.412453 * RGB[0] + 0.357580 * RGB[1] + 0.180423 * RGB[2];
      float y   = 0.212671 * RGB[0] + 0.715160 * RGB[1] + 0.072169 * RGB[2];
      float z   = 0.019334 * RGB[0] + 0.119193 * RGB[1] + 0.950227 * RGB[2];
      return slip::Color<OutType>(OutType(x),OutType(y),OutType(z));
    }
  };


   /**
   ** \brief Converts a XYZ color to RGB color. Another version (not the same white reference)
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param XYZ A const slip::Color<InType> reference.
   ** \return A slip::Color<OutType> value corresponding to the RGB value.
   **
   ** \code 
   **  R =  3.240479 * X - 1.537150 * Y - 0.498535 * Z;
   **  G = -0.969256 * X + 1.875992 * Y + 0.041556 * Z;
   **  B =  0.055648 * X - 0.204043 * Y + 1.057311 * Z;
   ** \endcode
   */
  template<typename InType, typename OutType>
  struct XYZtoRGB_v2
  {
    slip::Color<OutType> operator()(const slip::Color<InType>& XYZ) const
    {
      float r   =  3.240479 * XYZ[0] - 1.537150 * XYZ[1] - 0.498535 * XYZ[2];
      float g   = -0.969256 * XYZ[0] + 1.875992 * XYZ[1] + 0.041556 * XYZ[2];
      float b   =  0.055648 * XYZ[0] - 0.204043 * XYZ[1] + 1.057311 * XYZ[2];
      return slip::Color<OutType>(OutType(r),OutType(g),OutType(b));
    }
  };


   /**
   ** \brief Converts a RGB color to YCrCb color.
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param RGB A const slip::Color<InType> reference.
   ** \return A slip::Color<OutType> value corresponding to the YCrCb value.
   **
   ** \code 
   **  Y = 0.299 * R + 0.587 * G + 0.114 * B;
   **  Cr = 0.500 * R - 0.419 * G - 0.081 * B = 0.713(R - Y) ;
   **  Cb = -0.169 * R - 0.331 * G + 0.500 * B = 0.564(B - Y) ;
   ** \endcode
   */
  template<typename InType, typename OutType>
  struct RGBtoYCrCb
  {
    slip::Color<OutType> operator()(const slip::Color<InType>& RGB) const
    {
      float Y   =  0.299 * RGB[0] + 0.587 * RGB[1] + 0.114 * RGB[2];
      float Cr   = 0.713 *(RGB[0] - Y);
      float Cb   = 0.564 *(RGB[2] - Y);
      return slip::Color<OutType>(OutType(Y),OutType(Cr),OutType(Cb));
    }
  };


   /**
   ** \brief Converts a YCrCb color to RGB color.
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param YCrCb A const slip::Color<InType> reference.
   ** \return A slip::Color<OutType> value corresponding to the RGB value.
   **
   ** \code 
   **  R =  1 * Y + 1.402 * Cr - 0.001 * Cb;
   **  G =  1 * Y - 0.714 * Cr - 0.344 * Cb;
   **  B =  1 * Y + 0.001 * Cr + 1.772 * Cb;
   ** \endcode
   ** 
   */
  template<typename InType, typename OutType>
  struct YCrCbtoRGB
  {
    slip::Color<OutType> operator()(const slip::Color<InType>& YCrCb) const
    {
      float r   =  YCrCb[0] + 1.402 * YCrCb[1] - 0.001 * YCrCb[2];
      float g   =  YCrCb[0] - 0.714 * YCrCb[1] - 0.344 * YCrCb[2];
      float b   =  YCrCb[0] - 0.001 * YCrCb[1] + 1.772 * YCrCb[2];
      return slip::Color<OutType>(OutType(r),OutType(g),OutType(b));
    }
  };

template <typename Real>
  inline
  Real fCieLab(const Real x)
  {
    Real res = Real(0.0);
    if (x > static_cast<Real>(0.008856))
      {
	res = static_cast<Real>(std::pow(x,static_cast<Real>(1.0/3.0)));
      }
    else
      {
	res = static_cast<Real>(7.787) * x + static_cast<Real>(16.0) / static_cast<Real>(116.0);
      }
    
    return res;
  }


/**
   ** \brief Converts a RGB color to CieLab color.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2011/03/24
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param RGB A const slip::Color<InType> reference.
   ** \return A slip::Color<OutType> value corresponding to the CieLab value.
   **
   ** \code 
 
   ** \endcode
   */
  template<typename InType, typename OutType>
  struct RGBtoCieLab
  {
    RGBtoCieLab():
      Xn_(251.14),
      Yn_(256.00),
      Zn_(301.31)
    {}

    slip::Color<OutType> operator()(const slip::Color<InType>& RGB) const
    {
      double X    =   0.607 * static_cast<double>(RGB[0]) + 0.174 * static_cast<double>(RGB[1]) + 0.2 * static_cast<double>(RGB[2]);
      double Y    =   0.299 * static_cast<double>(RGB[0]) + 0.587 * static_cast<double>(RGB[1]) + 0.114 * static_cast<double>(RGB[2]);
      double Z    =    0.066 * static_cast<double>(RGB[1]) + 1.111 * static_cast<double>(RGB[2]);

	  X = X / Xn_;
	  Y = Y / Yn_;
	  Z = Z / Zn_;

	  double L = double(0);
	  if(Y  > 0.008856)
	    {
	      L = static_cast<OutType>(116.0 * fCieLab(Y) - 16.0);
	    }
	  else
	    {
	      L = 903.3 * Y;
	    }
	  double a = 500.0 * (fCieLab(X) - fCieLab(Y));
	  double b = 200.0 * (fCieLab(Y) - fCieLab(Z));
	
      return slip::Color<OutType>(static_cast<OutType>(L),
				  static_cast<OutType>(a),
				  static_cast<OutType>(b));
    }


    double Xn_;
    double Yn_;
    double Zn_;
  };
  


 template<typename InType, typename OutType>
  struct CieLabtoRGB
  {
    CieLabtoRGB():
      Xn_(251.14),
      Yn_(256.00),
      Zn_(301.31)
    {}

    slip::Color<OutType> operator()(const slip::Color<InType>& CieLab) const
    {
      double P = (static_cast<double>(CieLab[0]) + 16.0 ) / 116.0;
      double X    =   Xn_ * (P + static_cast<double>(CieLab[1]) / 500.0)*(P + static_cast<double>(CieLab[1]) / 500.0)*(P + static_cast<double>(CieLab[1]) / 500.0);
      double Y    =   Yn_ * P * P * P;
      double Z    =   Zn_ * (P - static_cast<double>(CieLab[2])/200.0) * (P - static_cast<double>(CieLab[2])/200.0) * (P - static_cast<double>(CieLab[2])/200.0);


	  
      double R = (1.910374 * X - 0.533769 * Y - 0.289132 * Z);
      double G = (-0.98444 * X + 1.998520 * Y -0.027851 * Z);
      double B = (0.05842 * X - 0.118724 * Y + 0.901745 * Z);

      return slip::Color<OutType>(static_cast<OutType>(R),
				  static_cast<OutType>(G),
				  static_cast<OutType>(B));
    }


    double Xn_;
    double Yn_;
    double Zn_;
 };


/**
   ** \brief Converts a RGB color to HSV color.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2011/06/14
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param RGB A const slip::Color<InType> reference.
   ** \return A slip::Color<OutType> value corresponding to HSV value.
   ** \pre [RGB]: [0-255][0-255][0-255]
   ** \pre [HSV]: [0-360][0-1][0-1] 
   ** \code 
 
   ** \endcode
   */
  template<typename InType, typename OutType>
  struct RGBtoHSV
  {
    RGBtoHSV(){}

    slip::Color<OutType> operator()(const slip::Color<InType>& RGB) const
    {
      double r    =  static_cast<double>(RGB[0])/255.0;
      double g    =  static_cast<double>(RGB[1])/255.0;
      double b    =  static_cast<double>(RGB[2])/255.0;
      
      double maxrg  = std::max(r,g);
      double maxrgb = std::max(maxrg,b);
      double minrg  = std::min(r,g);
      double minrgb = std::min(maxrg,b);
     
      double delta_minmax = maxrgb - minrgb;
      double H = 0.0;
      double S = 0.0;
      double V = maxrgb;
     
      if(delta_minmax != 0.0)
	{
	  
	  S = (delta_minmax / maxrgb);
	  if(S != 0.0)
	    {
	      if(r == maxrgb)
		{
		  H = (g-b) / delta_minmax;
		}
	      else if(g == maxrgb)
		{
		  H = 2.0 + (b-r) / delta_minmax;
		}
	     else if(b == maxrgb)
		{
		  H = 4.0 + (r-g) / delta_minmax;
		}
	      //convert to degree
	      H = H * 60.0;
	      if( H < 0.0)
		{
		  H += 360.0;
		}
	    }
	  else
	    {
	      H = static_cast<OutType>(-1.0);
	    }
	}
      else
	{
	  H = 0.0;
	  S = 0.0;
	}
      
	
	
      return slip::Color<OutType>(static_cast<OutType>(H),
				  static_cast<OutType>(S),
				  static_cast<OutType>(V));
    }

  };
  


template<typename InType, typename OutType>
  struct HSVtoRGB
  {
    HSVtoRGB(){}

    slip::Color<OutType> operator()(const slip::Color<InType>& HSV) const
    {

      double h = static_cast<double>(HSV[0]);
      double s = static_cast<double>(HSV[1]);
      double v = static_cast<double>(HSV[2]);
      
      double R = 0.0;
      double G = 0.0;
      double B = 0.0;
      
      if( s == 0.0 || h == -1.0) 
	    {
	      R = 255.0 * v;
	      G = 255.0 * v;
	      B = 255.0 * v;
	    }
	  else
	    {
	      double hr = h / 60.0;
	      int primary_colour = static_cast<int>(std::floor(hr));
	      double secondary_colour = hr - primary_colour;
	      double a = (1.0 - s) * v;
	      double b = (1.0 - (s * secondary_colour)) * v;
	      double c = (1.0 - (s * (1.0 - secondary_colour))) * v;
	      
	      switch( primary_colour)
		{
		case 0: 
		  {
		    R = 255.0 * v;
		    G = 255.0 * c;
		    B = 255.0 * a;
		  }
		  break;
		case 1: 
		  {
		    R = 255.0 * b;
		    G = 255.0 * v;
		    B = 255.0 * a;
		  }
		  break;
		case 2: 
		  {
		    R = 255.0 * a;
		    G = 255.0 * v;
		    B = 255.0 * c;
		  }
		  break;
		case 3: 
		  {
		    R = 255.0 * a;
		    G = 255.0 * b;
		    B = 255.0 * v;
		  }
		  break;
		case 4: 
		  R = 255.0 * c;
		  G = 255.0 * a;
		  B = 255.0 * v;
		  break;
		case 5: 
		  R = 255.0 * v;
		  G = 255.0 * a;
		  B = 255.0 * b;
		  break;
		}

	    }	  
      return slip::Color<OutType>(static_cast<OutType>(R),
				  static_cast<OutType>(G),
				  static_cast<OutType>(B));
    }
 };

  
  /**
   ** \brief Converts a RGB color to HSI color.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2013/06/24
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param RGB A const slip::Color<InType> reference.
   ** \return A slip::Color<OutType> value corresponding to HSI value.
   ** \pre [RGB]: [0-255][0-255][0-255]
   ** \pre [HSI]: [0-360][0-100][0-255] 
   ** \code 
 
   ** \endcode
   */
 template<typename InType, typename OutType>
  struct RGBtoHSI
  {
    RGBtoHSI(){}

    slip::Color<OutType> operator()(const slip::Color<InType>& RGB) const
    {

      double R = static_cast<double>(RGB[0]);
      double G = static_cast<double>(RGB[1]);
      double B = static_cast<double>(RGB[2]);
      double RpGpB = R + G + B;
      double H = 0.0;
      double S = 0.0;
      double I = 0.0;
      if(RpGpB != 0.0)
	{
	  double r = R/RpGpB;
	  double g = G/RpGpB;
	  double b = B/RpGpB;
          
	  double rmg =  r - g;
	  double rmb =  r - b;
	  double gmb =  g - b;
	  double rmg2 =  rmg * rmg;
      
      
	  double minrgb = std::min(std::min(r,g),b);
   
	  S = 1.0 - (3.0*minrgb);
	  I = RpGpB / 3.0;

	  double theta = std::acos((0.5*(rmg + rmb))/ std::sqrt(rmg2 + rmb*gmb));
	  theta *= (180/slip::constants<double>::pi());
	  H = theta ;
	  if(gmb <  0.0)
	    {
	      H = 360.0 - theta;
	    }
	  S *= 100.0;
	}
      
      return slip::Color<OutType>(static_cast<OutType>(H),
				  static_cast<OutType>(S),
				  static_cast<OutType>(I));
    }

  };
  
   /**
   ** \brief Converts a HSI color to RGB color.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2013/06/24
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param HSI A const slip::Color<InType> reference.
   ** \return A slip::Color<OutType> value corresponding to RGB value.
   ** \pre [HSI]: [0-360][0-100][0-255]
   ** \pre [RGB]: [0-255][0-255][0-255] 
   ** \code 
 
   ** \endcode
   */
template<typename InType, typename OutType>
  struct HSItoRGB
  {
    HSItoRGB(){}

    slip::Color<OutType> operator()(const slip::Color<InType>& HSI) const
    {

      double ratio = slip::constants<double>::pi() / 180.0;
      double h = static_cast<double>(HSI[0]) * ratio;
      
      double s = static_cast<double>(HSI[1]) / 100.0;
      double i = static_cast<double>(HSI[2]) / 255.0;
      double x = i * (1.0 - s);
      double angle = (slip::constants<double>::pi_3()-h);
      double y = i * (1.0 + (s*std::cos(h))/std::cos(angle));
      double z = 3*i - (x+y);
     
      double R = 0.0;
      double G = 0.0;
      double B = 0.0;
      
      if((h >= 0.0) && (h < 2.0*slip::constants<double>::pi_3()))
	{
	  // 0<=h<120 
	  B = x ;
	  R = y ;
	  G = z ;
	}
      else if((h >= 2.0*slip::constants<double>::pi_3()) && (h < 4.0*slip::constants<double>::pi_3()))
	{
	  //120<=h<240
	  h = h - 2.0*slip::constants<double>::pi_3();
	  angle = (slip::constants<double>::pi_3()-h);
	  y = i * (1.0 + (s*std::cos(h))/std::cos(angle));
	  z = 3.0*i - (x+y);
	  R = x ;
	  G = y ;
	  B = z ;
	}
      else if((h >= 4.0*slip::constants<double>::pi_3()) && (h < 2.0*slip::constants<double>::pi()))
	{
	  //240<=h <=360
	  h = h - 4.0*slip::constants<double>::pi_3();
	  angle = (slip::constants<double>::pi_3()-h);
	  y = i * (1.0 + (s*std::cos(h))/std::cos(angle));
	  z = 3.0*i - (x+y);
	  G = x ;
	  B = y ;
	  R = z ;
	}
      R *=255.0;
      G *=255.0;       
      B *=255.0;   
      return slip::Color<OutType>(static_cast<OutType>(R),
				  static_cast<OutType>(G),
				  static_cast<OutType>(B));
    }
 };

 /**
   ** \brief Converts a RGB color to HSI color.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2013/06/24
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param RGB A const slip::Color<InType> reference.
   ** \return A slip::Color<OutType> value corresponding to HSI value.
   ** \pre [RGB]: [0.0-1.0][0.0-1.0][0.0-1.0]
   ** \pre [HSI]: [0.0-1.0][0.0-1.0][0.0-1.0] 
   ** \code 
 
   ** \endcode
   */
 template<typename InType, typename OutType>
  struct RGBtoHSI_01
  {
    RGBtoHSI_01(){}

    slip::Color<OutType> operator()(const slip::Color<InType>& RGB) const
    {

      double r = static_cast<double>(RGB[0]);
      double g = static_cast<double>(RGB[1]);
      double b = static_cast<double>(RGB[2]);
      double RpGpB = r + g + b;
       
      double rmg =  r - g;
      double rmb =  r - b;
      double gmb =  g - b;
      double rmg2 =  rmg * rmg;
      
      
      double minrgb = std::min(std::min(r,g),b);
   
      double S = 0.0;
      if(RpGpB != 0.0)
	{
	  S = 1.0 - (3.0*minrgb)/(RpGpB);
	}
      double I = RpGpB / 3.0;

      double theta = std::acos((0.5*(rmg + rmb))/ std::sqrt(rmg2 + rmb*gmb));
      double H = theta ;
      if(gmb <  0.0)
	{
	  H = 2.0*slip::constants<double>::pi() - theta;
	}
      H/=2.0*slip::constants<double>::pi();
      if(S == 0.0)
      	{
      	  H = 0.0;
      	}
      return slip::Color<OutType>(static_cast<OutType>(H),
				  static_cast<OutType>(S),
				  static_cast<OutType>(I));
    }

  };
  
   /**
   ** \brief Converts a HSI color to RGB color.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2013/06/24
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param HSI A const slip::Color<InType> reference.
   ** \return A slip::Color<OutType> value corresponding to RGB value.
   ** \pre [HSI]: [0.0-1.0][0-1.0][0.0-1.0]
   ** \pre [RGB]: [0-1.0][0-1.0][0-1.0] 
   ** \code 
 
   ** \endcode
   */
template<typename InType, typename OutType>
  struct HSItoRGB_01
  {
    HSItoRGB_01(){}

    slip::Color<OutType> operator()(const slip::Color<InType>& HSI) const
    {

      double h = static_cast<double>(HSI[0]) * 2.0 * slip::constants<double>::pi();
      double s = static_cast<double>(HSI[1]);
      double i = static_cast<double>(HSI[2]);
      double x = i * (1.0 - s);
      double angle = (slip::constants<double>::pi_3()-h);
      double y = i * (1.0 + (s*std::cos(h))/std::cos(angle));
      double z = 3*i - (x+y);
     
      double R = 0.0;
      double G = 0.0;
      double B = 0.0;
      
      if((h >= 0.0) && (h < 2.0*slip::constants<double>::pi_3()))
	{
	  // 0<=h<120 
	  B = x ;
	  R = y ;
	  G = z ;
	}
      else if((h >= 2.0*slip::constants<double>::pi_3()) && (h < 4.0*slip::constants<double>::pi_3()))
	{
	  //120<=h<240
	  h = h - 2.0*slip::constants<double>::pi_3();
	  angle = (slip::constants<double>::pi_3()-h);
	  y = i * (1.0 + (s*std::cos(h))/std::cos(angle));
	  z = 3.0*i - (x+y);
	  R = x ;
	  G = y ;
	  B = z ;
	}
      else if((h >= 4.0*slip::constants<double>::pi_3()) && (h < 2.0*slip::constants<double>::pi()))
	{
	  //240<=h <=360
	  h = h - 4.0*slip::constants<double>::pi_3();
	  angle = (slip::constants<double>::pi_3()-h);
	  y = i * (1.0 + (s*std::cos(h))/std::cos(angle));
	  z = 3.0*i - (x+y);
	  G = x ;
	  B = y ;
	  R = z ;
	}
	       
     
      return slip::Color<OutType>(static_cast<OutType>(R),
				  static_cast<OutType>(G),
				  static_cast<OutType>(B));
    }
 };

}//slip::


#endif //SLIP_COLOR_SPACES_HPP
