/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file complex_cast.hpp
 * 
 * \brief Provides some macros which are used for using complex as real 
 * 
 * \author Denis Arrivault <arrivault@sic.univ-poitiers.fr>
 * \date 21/03/2014
 * \since 1.0.0
 */

#ifndef SLIP_COMPLEX_CAST_HPP
#define SLIP_COMPLEX_CAST_HPP

#include <complex>
#include "utils_compilation.hpp"

#if __cplusplus <= 199711L //if < C++11
namespace std
{

  inline int       conj (const int arg)         {return arg;}
  inline unsigned  conj (const unsigned arg)    {return arg;}
  inline long      conj (const long arg)	{return arg;}
  inline short	   conj (const short arg)	{return arg;}
  inline char	   conj (const char arg)        {return arg;}
  inline float	   conj (const float arg)	{return arg;}
  inline double    conj (const double arg) 	{return arg;}
  inline long double    conj (const long double arg) 	{return arg;}
  
  inline int	   real (const int d)		{ return d; }
  inline unsigned  real (const unsigned d)      { return d; }
  inline float	   real (const float d)		{ return d; }
  inline double    real (const double d)	{ return d; }
  inline long double    real (const long double d)	{ return d; }
   
  inline int	   imag (const int UNUSED(d))		{ return 0; }
  inline unsigned  imag (const unsigned UNUSED(d))	{ return 0; }
  inline float	   imag (const float UNUSED(d))		{ return 0.0f; }
  inline double    imag (const double UNUSED(d))	{ return 0.0; }
  inline long double    imag (const long double UNUSED(d))	{ return 0.0; }
  
  inline int       norm (const int arg)         {return arg;}
  inline unsigned  norm (const unsigned arg)    {return arg;}
  inline long      norm (const long arg)	{return arg;}
  inline short	   norm (const short arg)	{return arg;}
  inline char	   norm (const char arg)        {return arg;}
  inline float	   norm (const float arg)	{return arg;}
  inline double    norm (const double arg) 	{return arg;}
  inline long double    norm (const long double arg) 	{return arg;}
}//::std
#endif //__cplusplus <= 199711L (<c++11)

#if __cplusplus <= 199711L //if < C++11
namespace slip
{
  inline int       conj (const int arg)         {return arg;}
  inline unsigned  conj (const unsigned arg)    {return arg;}
  inline long      conj (const long arg)	{return arg;}
  inline short	   conj (const short arg)	{return arg;}
  inline char	   conj (const char arg)        {return arg;}
  inline float	   conj (const float arg)	{return arg;}
  inline double    conj (const double arg) 	{return arg;}
  inline long double    conj (const long double arg) 	{return arg;}
  template <typename T>
  inline std::complex<T> conj(const std::complex<T>& arg) {return std::conj(arg);}

}//::slip
#else //if > c++11
namespace slip
{
  inline int       conj (const int arg)         {return std::real(std::conj(arg));}
  inline unsigned  conj (const unsigned arg)    {return std::real(std::conj(arg));}
      inline long      conj (const long arg)	{return std::real(std::conj(arg));}
  inline short	   conj (const short arg)	{return std::real(std::conj(arg));}
  inline char	   conj (const char arg)        {return std::real(std::conj(arg));}
  inline float	   conj (const float arg)	{return std::real(std::conj(arg));}
  inline double    conj (const double arg) 	{return std::real(std::conj(arg));}
  inline long double    conj (const long double arg) 	{return std::real(std::conj(arg));}
 template <typename T>
  inline std::complex<T> conj(const std::complex<T>& arg) {return std::conj(arg);}
}//::slip
#endif //__cplusplus <= 199711L (<c++11)




namespace std
{
 

  ///redefinition of std::less for complex: return true if std::norm(__x) < std::norm(__y)
  template<class _Tp>
  struct less<std::complex<_Tp> >
    {
      bool
      operator()(const std::complex<_Tp>& __x, const std::complex<_Tp>& __y) const
      { return std::norm(__x) < std::norm(__y); }
    };

 ///redefinition of std::greater for complex: return true if std::norm(__x) > std::norm(__y)
  template<class _Tp>
  struct greater<std::complex<_Tp> >
    {
      bool
      operator()(const std::complex<_Tp>& __x, const std::complex<_Tp>& __y) const
      { return std::norm(__x) > std::norm(__y); }
    };

}


#endif /* SLIP_COMPLEX_CAST_HPP */
