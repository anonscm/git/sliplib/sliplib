/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file linear_algebra_svd.hpp
 * 
 * \brief Provides some Singular Value Decomposition (SVD) algorithms.
 * 
 */
#ifndef SLIP_LINEAR_ALGEBRA_SVD_HPP
#define SLIP_LINEAR_ALGEBRA_SVD_HPP
#include <cmath>
#include <algorithm>
#include <limits>
#include "Vector.hpp"
#include "Array2d.hpp"
#include "linear_algebra.hpp"
#include "macros.hpp"
#include "norms.hpp"
#include "linear_algebra_traits.hpp"
#include "complex_cast.hpp"

namespace slip
{

 /** \name SVD internal algorithms */
  /* @{ */  
 /**
   ** \brief BiReduce. 
   ** Given an n*p matrix X with n>=p
   ** Bireduce reduce X to an upper bidiagola form
   ** The tranformations from the left are accumulmated in the n*p matrix U 
   ** and the transformations from the right in the matrix V.
   ** on return X contains the genrators of the householder
   ** transformation used in the reduction X=U[d,e]V
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \author  <hammoud_AT_sic.univ-poitiers.fr>
   **  
   ** \date 2009/1/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param X 2D Container of the input Matrix n*p
   ** \param U 2D Container of the tranformations from the left n*p
   ** \param V 2D Container of the transformations from the right p*p
   ** \param d 1D Container 
   ** \param e 1D Container.
   ** \pre X.rows() >= X.rows()
   ** \pre U.rows() == X.rows()
   ** \pre U.cols() == X.cols()
   ** \pre V.rows() == V.cols()
   ** \pre V.rows() == X.cols()
   ** \pre d.size() == X.rows()
   ** \pre (e.size() + 1) == X.rows()
   ** \note BiReduce returns V^H not V.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> X(7,6);
   ** slip::iota(X.begin(),X.end(),1.0,1.0);
   ** slip::Matrix<double> U(X.rows(),X.cols());
   ** slip::Matrix<double> V(X.cols(),X.cols());
   ** slip::Vector<double>d(X.cols());
   ** slip::Vector<double>e(X.cols()-1);
   ** slip::BiReduce(X,d,e,U,V);
   ** slip::fill(d,e,S);
   ** std::cout<<"d"<<d<<std::endl;
   ** std::cout<<"e"<<e<<std::endl;
   ** std::cout<<"U="<<std::endl<<U<<std::endl;
   **  std::cout<<"V="<<std::endl<<V<<std::endl;
   **  std::cout<<"S="<<std::endl<<S<<std::endl;
   ** ///verify if X=USV
   ** slip::Matrix<double> US(U.rows(),S.cols());
   ** slip::Matrix<double> USV(U.rows(),V.cols());
   ** slip::gen_matrix_matrix_multiplies(U,S,US);
   ** slip::gen_matrix_matrix_multiplies(US,V,USV);
   ** std::cout<<"USV="<<std::endl<<USV<<std::endl;
   ** \endcode
   */
  
template<typename Matrix1, 
	 typename Vector,
	 typename Matrix2,
	 typename Matrix3>

void BiReduce( Matrix1 &X,
               Vector &d,
               Vector &e,
               Matrix2 &U,
               Matrix3 &V)

 {
   assert(X.rows() >= X.rows());
   assert(U.rows() == X.rows());
   assert(U.cols() == X.cols());
   assert(V.rows() == V.cols());
   assert(V.rows() == X.cols());
   assert(d.size() == X.cols());
   assert((e.size() + 1) == X.cols());
   
   typedef typename Matrix1::value_type value_type;
   //   typedef typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type norm_type;
   std::size_t X_rows = X.rows();
   std::size_t X_cols = X.cols();

   std::fill(U.begin(),U.end(),value_type(0));
   std::fill(V.begin(),V.end(),value_type(0));

   // temporary variables used for householder methode
   slip::Vector<value_type > a(X_rows,value_type(0));//a
   slip::Vector<value_type > b(X_cols-1,value_type(0));//b
   
   slip::Box2d<int> box;
   slip::Box2d<int> box2;
   
   for(std::size_t k=0;k<X_cols;++k)
     {
       if((k<X_cols-1)||(X_rows>X_cols))
	 { 
	   slip::housegen(X.col_begin(k)+k,X.col_end(k),a.begin()+(k),a.end(),d[k]);
	   //X[k:n,k]=a
	   std::copy(a.begin()+(k),a.end(),X.col_begin(k)+k);
	   box.set_coord(int(k),int(k+1),int(X_rows-1),int(X_cols-1));
	   //X[k:n,k+1:p]
	   slip::left_householder_update(a.begin()+k,a.end(),
					 X.upper_left(box),X.bottom_right(box));
		  
	 }

       if(k<X_cols-2)
	 {
	   //housgen(X[k,k+1:p],b^T,e[k])
	   slip::housegen(X.row_begin(k)+(k+1),X.row_end(k),b.begin()+(k),b.end(),e[k]);
	   //X[k,k+1:p]=b^T
	   std::copy(b.begin()+(k),b.end(),X.row_begin(k)+(k+1));
	  box2.set_coord(int(k+1),int(k+1),int(X_rows-1),int(X_cols-1));
	   //M=X[k+1:n,k+1:p]
	   slip::right_householder_update(b.begin()+(k),b.end(),
					  X.upper_left(box2),X.bottom_right(box2));
		   
	
	 }

     }

   e[X_cols-2]=X[X_cols-2][X_cols-1];

   if((X_rows)==(X_cols))
     {
       d[X_cols-1]=X[X_cols-1][X_cols-1];
     }

   //Accumulate U
   // U=(   I_p  )
   //    ( 0_n-p,p)
   U.fill(value_type(0));
   std::size_t U_cols = X.cols();
   for (std::size_t i = 0; i < U_cols; ++i)
     {
       U[i][i] = value_type(1);
     }
     
   std::size_t min_np=std::min(X_cols-1,X_rows-2);
   for(int k=min_np;k>=0;k--)
     {
       std::copy(X.col_begin(k)+k,X.col_end(k),a.begin()+k);
       box.set_coord(int(k),int(k),int(X.rows()-1),int(X.cols()-1));//U[k:n,k:p]
       slip::left_householder_update(a.begin()+k,a.end(),
				     U.upper_left(box),U.bottom_right(box));
     }
   //Accumulate V
   //V=I_p()

   V.fill(value_type(0.0));
   std::size_t V_cols = X.cols();
   for (std::size_t i = 0; i < V_cols; ++i)
     {
       V[i][i] = value_type(1.0);
     }

   for(int k=int((X_cols-3));k>=0;k--)
     {
       std::copy(X.row_begin(k)+(k+1),X.row_end(k),b.begin()+k);
       box2.set_coord(int(k+1),int(k+1),int(X_cols-1),int(X_cols-1));
       slip::left_householder_update(b.begin()+(k),b.end(),
			     V.upper_left(box2),V.bottom_right(box2));
    
     }

}

/**
   ** \brief BiQR
   ** Given an upper bidiagonal matrix B, 
   ** Bi_complex_to_real transforms the complex bidiagonal form into a real form. 
   ** The tranformations from the left are accumulmated in the matrix U 
   ** and the transformations from the right in the matrix V.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \author  <hammoud_AT_sic.univ-poitiers.fr>  
   ** \date 2009/1/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param U 2D Container of the tranformations from the left n*p
   ** \param V 2D Container of the transformations from the right p*p
   ** \param d 1D Container size p
   ** \param e 1D Container.size p-1
   ** \pre d.size() == U.cols()
   ** \pre (d.size()-1) == e.size()
   ** \pre V.rows() == V.cols()
   ** \pre U.cols() == V.rows()
   */


template<typename Matrix,typename Vector>
void Bi_complex_to_real(Vector &d,Vector &e,
                        Matrix &U,
                        Matrix &V)
{
  assert(d.size() == U.cols());
  assert((d.size()-1) == e.size());
  assert(V.rows() == V.cols());
  assert(U.cols() == V.rows());
  
   typedef typename Matrix::value_type value_type;
   std::size_t d_size = d.size();//d and X have the same size : d.size()=X.cols();
   value_type mu;
   double a;
  for(std::size_t k=0;k<d_size;k++)
  {
    a=std::abs(d[k]);
    if(a!=0.0)
    {
      mu=slip::conj(d[k])/a;
      d[k]=a;
      if(k!=d_size-1)
       {
        e[k]=mu*e[k];
       }
       slip::vector_scalar_multiplies(U.col_begin(k),U.col_end(k),slip::conj(mu),U.col_begin(k));//U[:,k]=conj(mu)*U[:,k];
    } 
  if(k==(d_size-1)) break;
 
   a=std::abs(e[k]);
  if(a!=value_type(0))
    {
      mu=slip::conj(e[k])/a;
      e[k]=a;
      d[k+1]=mu*d[k+1];
      slip::vector_scalar_multiplies(V.col_begin(k+1),V.col_end(k+1),mu,V.col_begin(k+1));
   }

  }
}



/**
   ** \brief BiQR
   ** Given an upper bidiagonal matrix B and a shift sigma, 
   ** BiQR performs a QR step between rows row1 ans row2. 
   ** The tranformations from the left are accumulmated in the matrix U 
   ** and the transformations from the right in the matrix V.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \author  <hammoud_AT_sic.univ-poitiers.fr>  
   ** \date 2009/1/07
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param d 1D Container size p
   ** \param e 1D Container.size p-1
   ** \param sigma shift value.
   ** \param row1 first row index.
   ** \param row2 second row index.
   ** \param U 2D Container of the tranformations from the left n*p
   ** \param V 2D Container of the transformations from the right p*p
   ** \pre d.size() == U.cols()
   ** \pre (d.size()-1) == e.size()
   ** \pre V.rows() == V.cols()
   ** \pre U.cols() == V.rows()
   */
template<typename Matrix,typename Vector, typename Real,typename SizeType >
void BiQR( Vector &d,
           Vector &e,
           Real &sigma,
           const SizeType& row1,
           const SizeType& row2,
           Matrix &U,
           Matrix &V)
  { 
    assert(d.size() == U.cols());
    assert((d.size()-1) == e.size());
    assert(V.rows() == V.cols());
    assert(U.cols() == V.rows());
    //    typedef typename Vector::value_type value_type;
    Real scl = std::max(std::abs(d[row1]),std::abs(e[row1]));
    scl = std::max(std::abs(scl),std::abs(sigma));
    Real d1  = d[row1]/scl;
    Real e1  = e[row1]/scl;
    Real sig = sigma/scl;
    Real f   = (d1+sig) * (d1-sig);
    Real g   = d1 * e1;
    SizeType row2_m1 = row2 - 1;
    SizeType V_rows_m1  = V.rows() - 1;
    SizeType U_rows_m1  = U.rows() - 1;
   
    //sinus and cosinus
    Real s = Real(0); 
    Real c = Real(0);
    for (SizeType k = row1; k <= row2_m1; ++k)
      {
	slip::rotgen(f,g,c,s);
	if(k != row1)
	  {
	    e[k-1]=f;
	  }
        f = (c*d[k]) + (s*e[k]);
	e[k] = (c*e[k]) - (s*d[k]);
	g = s * d[k+1];
	d[k+1]=c*d[k+1];
	slip::right_givens(V,k,k+1,s,c,SizeType(0),V_rows_m1);
	slip::rotgen(f,g,c,s);  
        d[k] = f;
	f = (c * e[k]) + (s * d[k+1]);
	d[k+1] = (c * d[k+1]) - (s * e[k]);
		
	if(k < row2_m1)
	{
	  g = s * e[k+1];
	  e[k+1] = c * e[k+1];
	}
	slip::right_givens(U,k,k+1,s,c,SizeType(0),U_rows_m1);
      }
    e[row2-1] = f;
  }

/**
 ** \brief Given a matrix 
 ** \f[
 ** M = \left(
 ** \begin{array}{cc}
 **  p&r\\
 **  0&q\\
 ** \end{array}
 ** \right)
 ** \f]
 ** compute the smallest singular value of this matrix 
 ** \author  <hammoud_AT_sic.univ-poitiers.fr>
 ** \date 2009/02/11
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param p element of the matrix M 
 ** \param r element of the matrix M.
 ** \param q element of the matrix M.
 ** \param sigma  smallest singular value of this matrix.
 */
template <typename Real >
void shift(Real &p,
	   Real &r, 
	   Real &q,
	   Real& sigma)
{
  Real ppq = p + q;
  Real pmq = p - q;
  Real r2 = r * r;
  Real a = (ppq * ppq) + r2;
  Real b = (pmq * pmq) + r2;
  Real sigma_max = (std::sqrt(a) + std::sqrt(b)) * Real(0.5);
  if(sigma_max != Real(0.0))
   {
    sigma = (std::abs(p*q)) / sigma_max;
   }

}
/**
 ** \brief Back searching a bidiagonal Matrix
 ** this algorithm takes a bidiagobal matrix B, a tolerance tol and an index l(0<=l<=p-1)
 ** and determines indices i1,i2<=l such one of the following condictions holds
 ** 1) 0<=i1<i2<=l, in which case the matrix deflates as rows i1 and i2.
 ** 2) 0=i1=i2, in which case all the superdiagonal elements of B in
 ** rows 0 through l-1 are zero
 ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
 ** \author  <hammoud_AT_sic.univ-poitiers.fr>  
 ** \date 2009/1/07
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param d The diagonal vector of B.
 ** \param e The lower diagonal vector of B.
 ** \param tol the relative error we can expect from setting the curent superdiagonal to zero
 ** \param l maximum index
 ** \param i1 first index
 ** \param i2 second index
 ** \pre d.size() == (e.size() + 1)
 ** \pre l < d.size()
 ** \pre i1 < d.size()
 ** \pre i2 < d.size()
 */
template<typename Vector, typename SizeType>
void BdBacksearch( Vector &d,
                   Vector &e,
                   const typename slip::lin_alg_traits<typename Vector::value_type>::value_type tol,
                   SizeType& l,
                   SizeType& i1,
                   SizeType& i2)
 {
   assert(d.size() == (e.size() + 1));
   assert(l < d.size());
   assert(i1 < d.size());
   assert(i2 < d.size());
   
   typedef typename Vector::value_type value_type;
   value_type tau = value_type(0);
   i1 = i2 = l;
   while(i1>0)
     {
       if(i1 == i2)
	 {
	   tau = d[i1];
	 }
       else
	 {
	   if(e[i1-1] != value_type(0.0))
	     {
	       tau = d[i1] * (tau/(tau + std::abs(e[i1-1])));
	     }
	 }
       
         if(std::abs(e[i1-1]) <= (tol*std::abs(tau)))
           {
	     e[i1-1] = value_type(0.0);
	     
	     if(i1 == i2)
	       {
		 i2 = i1-1;
		 i1 = i2;
	       }
	     else
	       {
		 return;
	       }
           }
	 else
           {
	     i1 = i1 - 1;
           }
     }
 }





/* @} */

/** \name SVD Decomposition algorithms */
  /* @{ */
/**
  ** \brief (thin) Singular Value Decomposition
   ** Given a matrix M, this function computes its singular value decomposition,
   ** Any n*p matrix M whose number of rows n is greater than
   ** or equal to its number of columns p, can be written as the product
   ** of an n*p column-orthogonal matrix Up, an p*p diagonal
   ** matrix S with positive or zero elements (the singular values), and 
   ** the hermitian transpose of an p*p orthogonal matrix V
   ** \f[
      M = \left( 
      \begin{array}{ccc}
      u_{1,1}&\cdots&u_{1,p}\\
      \vdots&&\vdots\\
      u_{n,1}&\cdots&u_{n,p}\\
      \end{array}
      \right)
      \left( 
      \begin{array}{cccc}
      s_{1,1}&0&\cdots&0\\
      0&\ddots&\ddots&\vdots\\
      \vdots&\ddots&\ddots&0\\
      0&\cdots&0&s_{p,p}\\
      \end{array}
      \right)
      \left( 
      \begin{array}{ccc}
      v_{1,1}&\cdots&v_{1,p}\\
      \vdots&&\vdots\\
      v_{p,1}&\cdots&v_{p,p}\\
      \end{array}
      \right)^H
      \f]
      ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
      ** \author  <hammoud_AT_sic.univ-poitiers.fr>  
   ** \date 2009/02/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M n*p matrix
   ** \param U n*p orthogonal matrix 
   ** \param S_first diagonal of the Singular matrix with positive or zero elements
   ** \param S_last diagonal of the Singular matrix with positive or zero elements
   ** \param V p*p orthogonal matrix (not its transpose)
   ** \param max_it maximum number of iterations.
   ** \pre M.rows() >= M.cols()
   ** \pre M.rows() == U.rows()
   ** \pre M.cols() == U.cols()
   ** \pre M.cols() == (S_last-S_first)
   ** \pre V.rows() == M.cols()
   ** \par Example:
   ** \code
   ** slip::Matrix<double> X(7,6);
   ** slip::iota(X.begin(),X.end(),1.0,1.0);
   ** slip::Matrix<double> U(X.rows(),X.cols());
   ** slip::Matrix<double> V(X.cols(),X.cols());
   ** slip::Vector<double>d(X.cols());
   ** slip::Vector<double>e(X.cols()-1);
   ** slip::svd(X,U,S,V);
   ** std::cout<<"U="<<std::endl<<U<<std::endl;
   **  std::cout<<"V="<<std::endl<<V<<std::endl;
   **  std::cout<<"S="<<std::endl<<S<<std::endl;
   ** ///verify if M=USV^T
   ** slip::Matrix<double> VT(X.cols(),X.cols());
   ** slip::hermitian_transpose(V,VT);
   ** slip::Matrix<double> US(U.rows(),S.cols());
   ** slip::Matrix<double> USVT(U.rows(),V.cols());
   ** slip::matrix_matrix_multiplies(U,S,US);
   ** slip::matrix_matrix_multiplies(US,VT,USVT);
   ** std::cout<<"USVT="<<std::endl<<USVT<<std::endl;
   ** \endcode
   */
template<typename Matrix1,
	 typename Matrix2,
	 typename RandomAccessIterator,
	 typename Matrix3>
void svd(const Matrix1&M, 
	 Matrix2 &U, 
	 RandomAccessIterator S_first, 
	 RandomAccessIterator S_last,
	 Matrix3 &V, 
	 const int max_it = 75)
{
  
  assert(M.rows() >= M.cols());
  assert(M.rows() == U.rows());
  assert(M.cols() == U.cols());
  assert(int(M.cols()) == int((S_last-S_first)));
  assert(V.rows() == M.cols());
  
  typedef typename Matrix1::value_type value_type;
  //typedef typename Matrix2::value_type real;
  typedef typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type real;		
  slip::Array2d<value_type> X(M.rows(),M.cols());
  std::copy(M.begin(),M.end(),X.begin());             
  std::size_t X_cols = X.cols();
  std::size_t X_cols_m1 = X_cols - 1;
  
  // Reduce X to bidiagonal form, storing the diagonal elements
  // in d and the super-diagonal elements in e. 
  slip::Vector<value_type> d1(X.cols());
  slip::Vector<value_type> e1(X.cols()-1);
  slip::Array2d<value_type> B(X.cols(),X.cols());
  slip::BiReduce(X,d1,e1,U,V);
 
  //if X is complex then apply Bi_complex_to_real
  if(slip::is_complex(value_type()))
    {
      Bi_complex_to_real(d1,e1,U,V);
    }
     
  //copy d1 to d: to work on real data, because the type of d1 
  //by defenition is complex idem for e1
  slip::Vector<real> d(X_cols);
  slip::Vector<real> e(X_cols_m1);
  std::size_t e1_size = e1.size();
  for(std::size_t i = 0; i < e1_size; ++i)
    {
      d[i] = std::real(d1[i]);
      e[i] = std::real(e1[i]);
    }
  //d[d1.size()-1] = std::real(d1[d1.size()-1]);
  d[e1_size] = std::real(d1[e1_size]);
  
  //Main iteration loop for the singular values
  int iter = 0;
  real tol=std::numeric_limits<real>::epsilon();
  std::size_t i1 = 0;
  std::size_t i2 = X_cols_m1;
  //std::size_t l = i2;
  real sigma = real(0);
  std::size_t oldi2 = 0;
  bool error = false;
		
  // while(true)
  //   {
  //     iter++;
  //     if(iter > max_it)
  // 	{
  // 	  error = true;
  // 	  //std::cout<<"error: no convergence"<<std::endl<<std::endl;
  // 	  break;
  // 	}
  //     oldi2 = i2;
  //     slip::BdBacksearch(d,e,tol,i2,i1,i2);

  //     if (i2 == 0) break;
  //     if(i2 != oldi2) 
  // 	{
  // 	  iter = 0;
  // 	}
  //     slip::shift(d[i2-1],e[i2-1],d[i2],sigma);
  //     slip::BiQR(d,e,sigma,i1,i2,U,V);

  //   }; 
    
  while(!error)
    {
      iter++;
      if(iter > max_it)
  	{
  	  error = true;
  	  //std::cout<<"error: no convergence"<<std::endl<<std::endl;
  	  break;
  	}
      oldi2 = i2;
      slip::BdBacksearch(d,e,tol,i2,i1,i2);

      if (i2 == 0) break;
      if(i2 != oldi2) 
  	{
  	  iter = 0;
  	}
      slip::shift(d[i2-1],e[i2-1],d[i2],sigma);
      slip::BiQR(d,e,sigma,i1,i2,U,V);

    }; 
 
  // Convergence.
  // Make the singular values positive.
  //if(i1==i2)
  for(std::size_t k = 0; k < X_cols; ++k)
    {
      if (d[k] <= 0.0)
	{
     	  d[k] = -d[k];
	  slip::vector_scalar_multiplies(V.col_begin(k),V.col_end(k),
					 real(-1),V.col_begin(k));
	}
    }
  // sort the singular values
  for(std::size_t k = 0; k < X_cols_m1; ++k)
    {

      if(d[k] <= d[k+1])
	{
	  real t = d[k];
	  d[k] = d[k+1];
	  d[k+1] = t;
	  std::swap_ranges(V.col_begin(k),V.col_end(k),V.col_begin(k+1));
	  std::swap_ranges(U.col_begin(k),U.col_end(k),U.col_begin(k+1));
	}
}

  std::copy(d.begin(),d.end(),S_first);
 
}

  /**
   ** \brief (thin) Singular Value Decomposition
   ** Given a matrix M, this function computes its singular value decomposition,
   ** Any n*p matrix M whose number of rows n is greater than
   ** or equal to its number of columns p, can be written as the product
   ** of an n*p column-orthogonal matrix Up, an p*p diagonal
   ** matrix S with positive or zero elements (the singular values), and 
   ** the transpose of an p*p orthogonal matrix V
   ** \f[
   **  M = \left( 
   **  \begin{array}{ccc}
   **  u_{1,1}&\cdots&u_{1,p}\\
   **  \vdots&&\vdots\\
   ** u_{n,1}&\cdots&u_{n,p}\\
   **  \end{array}
   **  \right)
   **  \left( 
   **  \begin{array}{cccc}
   **  s_{1,1}&0&\cdots&0\\
   **  0&\ddots&\ddots&\vdots\\
   **  \vdots&\ddots&\ddots&0\\
   **  0&\cdots&0&s_{p,p}\\
   **  \end{array}
   **  \right)
   **  \left( 
   **  \begin{array}{ccc}
   **  v_{1,1}&\cdots&v_{1,p}\\
   **  \vdots&&\vdots\\
   **  v_{p,1}&\cdots&v_{p,p}\\
   **  \end{array}
   **  \right)^H
   **  \f]
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>

   ** \author  <hammoud_AT_sic.univ-poitiers.fr>: conceptor  
   ** \date 2009/02/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param X n*p matrix
   ** \param U n*p orthogonal matrix 
   ** \param S p*p diagonal matrix with positive or zero elements
   ** \param V p*p orthogonal matrix (not its transpose)
   ** \param max_it maximum number of iterations.
   ** \pre X.rows() >= X.cols()
   ** \pre X.rows() == U.rows()
   ** \pre X.cols() == U.cols()
   ** \pre S.rows() == S.cols()
   ** \pre X.cols() == S.cols()
   ** \pre V.rows() == X.cols()
   ** \par Example:
   ** \code
   ** slip::Matrix<double> X(7,6);
   ** slip::iota(X.begin(),X.end(),1.0,1.0);
   ** slip::Matrix<double> U(X.rows(),X.cols());
   ** slip::Matrix<double> V(X.cols(),X.cols());
   ** slip::Vector<double>d(X.cols());
   ** slip::Vector<double>e(X.cols()-1);
   ** slip::svd(X,U,S,V);
   ** std::cout<<"U="<<std::endl<<U<<std::endl;
   **  std::cout<<"V="<<std::endl<<V<<std::endl;
   **  std::cout<<"S="<<std::endl<<S<<std::endl;
   ** ///verify if M=USV^T
   ** slip::Matrix<double> VT(X.cols(),X.cols());
   ** slip::hermitian_transpose(V,VT);
   ** slip::Matrix<double> US(U.rows(),S.cols());
   ** slip::Matrix<double> USVT(U.rows(),V.cols());
   ** slip::matrix_matrix_multiplies(U,S,US);
   ** slip::matrix_matrix_multiplies(US,VT,USVT);
   ** std::cout<<"USVT="<<std::endl<<USVT<<std::endl;
   ** \endcode
   */
template<typename Matrix,typename Matrix2>
void svd(const Matrix&X, 
	 Matrix &U, 
	 Matrix2 &S, 
	 Matrix &V, 
	 const int max_it = 75)
{
  assert(X.rows() >= X.cols());
  assert(X.rows() == U.rows());
  assert(X.cols() == U.cols());
  assert(S.rows() == S.cols());
  assert(X.cols() == S.cols());
  assert(V.rows() == X.cols());
  
  typedef typename slip::lin_alg_traits<typename Matrix::value_type>::value_type real;		
  

  slip::Vector<real> Svect(X.cols());
  slip::svd(X,U,Svect.begin(),Svect.end(),V,max_it);
  std::fill(S.begin(),S.end(),0);
  slip::set_diagonal(Svect.begin(),Svect.end(),S);
}

/* @} */

/** \name SVD approximation algorithms */
/* @{ */
/**
 ** \brief Singular Value Approximation of a matrix from its Singular Value Decomposition given by U, W and V: \f$ X_K =  \sum_k^K w_k U_kV_k^H \f$ 
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2007/03/13
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param U matrix container
 ** \param S_first RandomAccessIterator to the first element of the singular values vector
 ** \param S_last RandomAccessIterator to one-past-the-end element of the singular values vector
 ** \param V matrix container
 ** \param K length of the approximation
 ** \param X matrix container
 ** \pre X.rows() >= X.cols()
 ** \pre V.rows() == V.cols()
 ** \pre X.rows() == U.rows()
 ** \pre X.cols() == U.cols()
 ** \pre int(S_last-S_first) == int(U.cols())
 ** \pre int(S_last-S_first) == int(V.rows())
 ** \pre K <= U.cols()
 ** \remarks K = U.cols() corresponds to an exact approximation.
 ** \par Example:
 ** \code
 **  double f[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0, 16.0 , 18.0, 5.0};
 ** slip::Matrix<double> SVDin(4,3,f);
 ** std::cout <<"SVDin = \n"<<SVDin<<std::endl<<std::endl;
 ** slip::Vector<double> Wvect(3,0.0);
 ** slip::svd(SVDin,U,Wvect.begin(),Wvect.end(),V);
 ** std::cout <<"U = \n"<<U<<std::endl<<std::endl;
 ** std::cout <<"Wvect = \n"<<Wvect<<std::endl<<std::endl;
 ** std::cout <<"V = \n"<<V<<std::endl<<std::endl;
 ** slip::svd_approx(U,Wvect.begin(),Wvect.end(),V,U.cols(),SVDin2);
 ** std::cout <<"SVDin2 = \n"<<SVDin2<<std::endl<<std::endl;
 ** \endcode
 */  
template<typename Matrix1,typename RandomAccessIterator, typename Matrix2, typename Matrix3>
inline
void svd_approx(const Matrix1 &U, 
		RandomAccessIterator S_first, 
		RandomAccessIterator S_last,
		const Matrix2 &V, 
		const std::size_t K,
		Matrix3& X)
{
  assert(X.rows() >= X.cols());
  assert(V.rows() == V.cols());
  assert(X.rows() == U.rows());
  assert(X.cols() == U.cols());
  assert(int(S_last-S_first) == int(U.cols()));
  assert(int(S_last-S_first) == int(V.rows()));
  assert(K <= U.cols());
  
  std::size_t U_rows = U.rows();
  //std::size_t U_cols = U.cols();
  std::size_t V_rows = V.rows();
  for(std::size_t i = 0; i < U_rows; ++i)
    {
      for(std::size_t j = 0; j < V_rows; ++j)
	{
	  X[i][j] = 0;
	  for(std::size_t k = 0; k < K; ++k)
	    {
	      X[i][j] += S_first[k] * U[i][k] * slip::conj(V[j][k]);
	    }
	}
    }
  }



 /**
   ** \brief Singular Value Approximation of a matrix from its Singular Value Decomposition given by U, W and V: \f$ M_K =  \sum_k^K w_k U_kV_k^H \f$ 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/03/13
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param U matrix container
   ** \param W Matrix container
   ** \param V matrix container
   ** \param K length of the approximation
   ** \param M matrix container
   ** \pre U.rows() >= U.cols()
   ** \pre W.rows() == W.cols()
   ** \pre V.rows() == V.cols()
   ** \pre U.cols() == W.rows()
   ** \remarks K = U.cols() corresponds to an exact approximation.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> SVDinH(4,4);
   ** slip::hilbert(SVDinH);
   ** std::cout <<"SVDinH = \n"<<SVDinH<<std::endl<<std::endl;
   ** slip::Matrix<double> UH(4,4,0.0);
   ** slip::Matrix<double> WH(4,4,0.0);
   ** slip::Matrix<double> VH(4,4,0.0);
   ** slip::svd(SVDinH,UH,WH,VH);
   ** std::cout <<"Uh = \n"<<UH<<std::endl<<std::endl;
   ** std::cout <<"Wh = \n"<<WH<<std::endl<<std::endl;
   ** std::cout <<"Vh = \n"<<VH<<std::endl<<std::endl;
   ** slip::Matrix<double> SVDinH2(4,4);
   ** slip::svd_approx(UH,WH,VH,UH.cols(),SVDinH2);
   ** std::cout <<"SVDinH2 = \n"<<SVDinH2<<std::endl<<std::endl;
   ** \endcode
   */  
  template <class Matrix1,class Matrix2,class Matrix3,class Matrix4>
  inline
  void svd_approx(const Matrix1 & U,
		  const Matrix2 & W,
		  const Matrix3 & V,
		  const std::size_t K,
		  Matrix4 & M)
  {
    slip::Array<typename Matrix1::value_type> Wvect(W.rows());
    slip::get_diagonal(W,Wvect.begin(),Wvect.end());
    slip::svd_approx(U,Wvect.begin(),Wvect.end(),V,K,M);
  }  


/* @} */

  /** \name SVD system solve algorithms */
  /* @{ */

/**
   ** \brief Solve Linear  (USV^H).X = B where U, S and V are provided
   ** by a svd decomposition.
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>: conceptor
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>: adapt
   ** to complex data
   ** \date 2009/03/23
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param U matrix U.
   ** \param W_first RandomAccesIterator to the first element of the
   ** singular values vector
   ** \param W_last RandomAccesIterator to one-past-the-end element of
   ** singular values vector
  
   ** \param V matrix V.
   ** \param B_first RandomAccesIterator to the first element of the vector to match
   ** \param B_last RandomAccesIterator to one-past-the-end element of
   ** the  vector to match
   ** \param X_first RandomAccesIterator to the first element of the Result vector
   ** \param X_last RandomAccesIterator to one-past-the-end element of
   ** the Result vector
   */
template <class Matrix1, 
	  typename RandomAccessIterator1, 
	  class Matrix2, 
	  typename RandomAccessIterator2,
	  typename RandomAccessIterator3>
  inline
  void svd_solve(const Matrix1 & U,
		 RandomAccessIterator1 W_first,
		 RandomAccessIterator1 W_last,
		 const Matrix2 & V,
		 RandomAccessIterator2 B_first,
		 RandomAccessIterator2 B_last,
		 RandomAccessIterator3 X_first,
		 RandomAccessIterator3 X_last)
  {
    assert(U.rows() >= U.cols());
    assert(int(X_last - X_first) == int(U.cols()));
    assert(int(B_last - B_first) == int(U.rows()));
    assert(int(W_last - W_first) == int(U.cols()));
    assert(V.rows() == U.cols());
    typedef typename Matrix1::value_type value_type;
    typedef typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type real;
    //std::size_t U_rows = U.rows();
    std::size_t U_cols = U.cols();
    
    slip::Vector<value_type> tmp(U_cols);
    // Calculate U^HB.
    for (std::size_t j = 0; j < U_cols; ++j) 
      { 
	
	value_type s = value_type(0.0);
	
	// Nonzero result only if wj is nonzero.
	if (W_first[j] != real(0.0)) 
	  { 
	    s = slip::hermitian_inner_product(U.col_begin(j),U.col_end(j),
					      B_first,value_type(0.0));
	    s /= W_first[j];
	  }
	tmp[j] = s;
      }
    // Matrix multiply by V to get answer.
    slip::matrix_vector_multiplies(V.upper_left(),V.bottom_right(),
				   tmp.begin(),tmp.end(),
				   X_first,X_last);
  }

/**
   ** \brief Solve Linear Equation A.X = B using SVD decomposition
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>: conceptor
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>: adapt
   ** to complex data
   ** \date 2009/03/23
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param A matrix A.
   ** \param X_first RandomAccesIterator to the first element of the Result vector
   ** \param X_last RandomAccesIterator to one-past-the-end element of
   ** the Result vector
   ** \param B_first RandomAccesIterator to the first element of the vector to match
   ** \param B_last RandomAccesIterator to one-past-the-end element of
   ** the  vector to match
   ** \remarks A svd truncature is done: small singular values (less
   ** than Smax * slip::epsilon<value_type>(), with Smax the greatest
   ** singular value) are put to 0
   ** \pre A.rows() >= A.cols()
   ** \pre (X_last - X_first) == A.cols()
   ** \pre (B_last - B_first) == A.rows()
   ** \par Example:
   ** \code
   ** double x[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0, 16.0 , 18.0, 5.0};
   ** double y[] ={7.0, 14.0, 11.0, 21.0, 15.0, 17.0, 31.0, 23.0, 25.0, 33.0 , 37.0, 11.0};
   ** slip::Matrix<double> Msol(12,2);
   ** slip::Vector<double> Bsol(12);
   ** std::fill(Msol.col_begin(0),Msol.col_end(0),1.0);
   ** std::copy(x,x+12,Msol.col_begin(1));
   ** std::copy(y,y+12,Bsol.begin());
   ** std::cout<<"Msol = \n"<<Msol<<std::endl;
   ** std::cout<<"Bsol = \n"<<Bsol<<std::endl;
   ** slip::Vector<double> Xsol(2);
   ** slip::svd_solve(Msol,Xsol.begin(),Xsol.end(),Bsol.begin(),Bsol.end());
   ** std::cout<<"Xsol = \n"<<Xsol<<std::endl;
   ** slip::Vector<double> Bsol2(12);
   ** slip::matrix_vector_multiplies(Msol,Xsol,Bsol2);
   ** std::cout<<"Bsol2 = \n"<<Bsol2<<std::endl;
   ** \endcode
   */
 template <class Matrix1, 
	  typename RandomAccessIterator1, 
	  typename RandomAccessIterator2>
  inline
  void svd_solve(const Matrix1 & A,
		 RandomAccessIterator1 X_first,
		 RandomAccessIterator1 X_last,
		 RandomAccessIterator2 B_first,
		 RandomAccessIterator2 B_last)
  {
    assert(A.rows() >= A.cols());
    assert(int(X_last - X_first) == int(A.cols()));
    assert(int(B_last - B_first) == int(A.rows()));
    
    typedef typename Matrix1::value_type value_type;
    typedef typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type real;		
  

    slip::Vector<real>  S(A.cols());
    slip::Array2d<value_type> U(A.rows(),A.cols());
    slip::Array2d<value_type> V(A.cols(),A.cols());

    //-----------------------------------------------
    // svd decomposition.
    //-----------------------------------------------
    slip::svd(A,U,S.begin(),S.end(),V);
    //------------------------------------------------------
    // svd truncature (small singular values are put to 0)
    //------------------------------------------------------
    real Smax = S[0];//*std::max_element(S.begin(),S.end());
    real Smin = Smax * slip::epsilon<value_type>();
    std::replace_if(S.begin(),S.end(),std::bind2nd(std::less<real>(), Smin),real(0));
    //-----------------------------------------------
    // svd solve
    //-----------------------------------------------
    slip::svd_solve(U,
		    S.begin(),S.end(),
		    V,
		    B_first,B_last,
		    X_first,X_last);
  }

  /**
   ** \brief Solve Linear Equation A.X = B using SVD decomposition
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>: conceptor
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>: adapt
   ** to complex data
   ** \date 2009/03/23
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param A matrix A.
   ** \param X Result vector
   ** \param B vector to match
   ** \remarks A svd truncature is done: small singular values (less
   ** than Smax * slip::epsilon<value_type>(), with Smax the greatest
   ** singular value) are put to 0
   ** \pre A.rows() >= A.cols()
   ** \pre X.size() == A.cols()
   ** \pre B.size() == A.rows()
   ** \par Example:
   ** \code
   ** double x[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0, 16.0 , 18.0, 5.0};
   ** double y[] ={7.0, 14.0, 11.0, 21.0, 15.0, 17.0, 31.0, 23.0, 25.0, 33.0 , 37.0, 11.0};
   ** slip::Matrix<double> Msol(12,2);
   ** slip::Vector<double> Bsol(12);
   ** std::fill(Msol.col_begin(0),Msol.col_end(0),1.0);
   ** std::copy(x,x+12,Msol.col_begin(1));
   ** std::copy(y,y+12,Bsol.begin());
   ** std::cout<<"Msol = \n"<<Msol<<std::endl;
   ** std::cout<<"Bsol = \n"<<Bsol<<std::endl;
   ** slip::Vector<double> Xsol(2);
   ** slip::svd_solve(Msol,Xsol,Bsol);
   ** std::cout<<"Xsol = \n"<<Xsol<<std::endl;
   ** slip::Vector<double> Bsol2(12);
   ** slip::matrix_vector_multiplies(Msol,Xsol,Bsol2);
   ** std::cout<<"Bsol2 = \n"<<Bsol2<<std::endl;
   ** \endcode
   **
   */  
  template <class Matrix1, 
	    class Vector1, 
	    class Vector2>
  inline
  void svd_solve(const Matrix1 & A,
		 Vector1 & X,
		 const Vector2 & B)
  {
    
     slip::svd_solve(A,X.begin(),X.end(),B.begin(),B.end());
  }

/**
   ** \brief Solve Linear Equation A.X = B using a SVD decomposition and a normalization matrix of A: \f[ (NAN)\underline{X} = N\underline{B}\f] with 
   ** \f[N=\left(\begin{array}{cccc}
   ** \frac{1}{\sqrt{A_{11}}}&0&\cdots&0\\
   ** 0&\ddots&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&0\\
   ** 0&\cdots&0&\frac{1}{\sqrt{A_{nn}}}\\
   ** \end{array} \right) \f]
   ** 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2023/04/17
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param A matrix A.
   ** \param x_first RandomAccesIterator to the first element of the Result vector
   ** \param x_last RandomAccesIterator to one-past-the-end element of
   ** the Result vector
   ** \param b_first RandomAccesIterator to the first element of the vector to match
   ** \param b_last RandomAccesIterator to one-past-the-end element of
   ** the  vector to match
   ** \param tolerance svd truncature tolerance : small singular values less than Smax * tolerance, with Smax the greatest singular value are put to 0. Default tolerance is set to static_cast<typename Matrix::value_type>(1e-06)
   ** \pre A.rows() == A.cols()
   ** \pre (x_last - x_first) == A.cols()
   ** \pre (b_last - b_first) == A.rows()
   ** \pre A diagonal values shoud be strictly positives
   ** \par Example:
   ** \code
   
   ** \endcode
   */
  template <typename Matrix1,
	    typename RandomAccessIterator1,
	    typename RandomAccessIterator2>
  void normalized_svd_solve(const Matrix1& A, 
			    RandomAccessIterator2  x_first,
			    RandomAccessIterator2  x_last,
			    RandomAccessIterator1  b_first,
			    RandomAccessIterator1  b_last,
			    const typename Matrix1::value_type & tolerance = static_cast<typename Matrix1::value_type>(1e-06))
  {
    assert(A.rows() == A.cols());
    assert(int(x_last - x_first) == int(A.cols()));
    assert(int(b_last - b_first) == int(A.rows()));
    
    typedef typename Matrix1::value_type T;
    //computes normalization vector
    const std::size_t N = A.rows();
    slip::Vector<T> S(N);
    for(std::size_t i = 0; i < N; ++i)
      {
	S[i] = slip::constants<T>::one()/std::sqrt(A[i][i]);
      }
    Matrix1 SAS(N,N);
    for(std::size_t j = 0; j < N; ++j)
      {
	slip::multiplies(S.begin(),S.end(),A.col_begin(j),SAS.col_begin(j));
      }
    for(std::size_t i = 0; i < N; ++i)
      {
	slip::multiplies(S.begin(),S.end(),SAS.row_begin(i),SAS.row_begin(i));
      }

    slip::Vector<T> Sc(N);
    slip::multiplies(S.begin(),S.end(),b_first,Sc.begin());
    
    slip::Vector<T>  Ssvd(SAS.cols());
    Matrix1 U(SAS.rows(),SAS.cols());
    Matrix1 V(SAS.cols(),SAS.cols());

    //-----------------------------------------------
    // svd decomposition.
    //-----------------------------------------------
    slip::svd(SAS,U,Ssvd.begin(),Ssvd.end(),V);
    //------------------------------------------------------
    // svd truncature (small singular values are put to 0)
    //------------------------------------------------------
    const T Smax = S[0];//*std::max_element(S.begin(),S.end());
    const T Smin = Smax * tolerance;
    std::replace_if(Ssvd.begin(),Ssvd.end(),std::bind2nd(std::less<T>(), Smin),T
());
    //-----------------------------------------------
    // svd solve
    //-----------------------------------------------
    slip::svd_solve(U,
		    Ssvd.begin(),Ssvd.end(),
		    V,
		    Sc.begin(),Sc.end(),
		    x_first,x_last);

   slip::multiplies(x_first,x_last,S.begin(),x_first);
 }

/**
   ** \brief Solve Linear Equation A.X = B using a SVD decomposition and a normalization matrix of A: \f[ (NAN)\underline{X} = N\underline{B}\f] with 
   ** \f[N=\left(\begin{array}{cccc}
   ** \frac{1}{\sqrt{A_{11}}}&0&\cdots&0\\
   ** 0&\ddots&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&0\\
   ** 0&\cdots&0&\frac{1}{\sqrt{A_{nn}}}\\
   ** \end{array} \right) \f]
   ** 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2023/04/17
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param A matrix A.
   ** \param X Result vector
   ** \param B vector to match
   ** \param tolerance svd truncature tolerance : small singular values less than Smax * tolerance, with Smax the greatest singular value are put to 0. Default tolerance is set to static_cast<typename Matrix::value_type>(1e-06)
   ** \pre A.rows() == A.cols()
   ** \pre X.size() == A.cols()
   ** \pre B.size() == A.rows()
   ** \pre A diagonal values shoud be strictly positives
   ** \par Example:
   ** \code
   ** double gnsvd[] ={26.0, 32.0, 39.0, 32.0, 40.0, 50.0, 39.0, 50.0, 65.0};
   ** double xnsvd[] = {1.0, 2.0, 3.0};
   ** double bnsvd[] = {207.0, 262.0, 334.0};
   ** slip::Matrix<double> Gnsvd(3,3, gnsvd, gnsvd+9);
   ** slip::Vector<double> Xnsvd(xnsvd, xnsvd+3);
   ** slip::Vector<double> Bnsvd(bnsvd, bnsvd+3);
   ** std::cout<<"Gnsvd = \n"<<Gnsvd<<std::endl;
   ** std::cout<<"Xnsvd = \n"<<Xnsvd<<std::endl;
   ** std::cout<<"Bnsvd = \n"<<Bnsvd<<std::endl;
   ** slip::Vector<double> Xsolnsvd(3);
   ** slip::normalized_svd_solve(Gnsvd,Xsolnsvd,Bnsvd, 1e-10);
   ** std::cout<<"Xsolnsvd = \n"<<Xsolnsvd<<std::endl;
   ** slip::Vector<double> Bnsvd2(3);
   ** slip::matrix_vector_multiplies(Gnsvd,Xsolnsvd,Bnsvd2);
   ** std::cout<<"Bnsvd2 = \n"<<Bnsvd2<<std::endl;
   ** \endcode
   */
template <class Matrix1, 
	  class Vector1, 
	  class Vector2>
inline
void normalized_svd_solve(const Matrix1 & A,
			  Vector1 & X,
			  const Vector2 & B,
			  const typename Matrix1::value_type & tolerance = static_cast<typename Matrix1::value_type>(1e-06))
{
  slip::normalized_svd_solve(A,X.begin(),X.end(),B.begin(),B.end(), tolerance);
}

/* @} */

/** \name SVD inverse algorithms */
  /* @{ */
/**
 ** \brief Singular Value inverse of a matrix from its Singular Value Decomposition given by U, W and V. 
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2007/03/13
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param U matrix container
 ** \param W_first RandomAccessIterator to the first element of the singular values vector
 ** \param W_last RandomAccessIterator to one-past-the-end element of the singular values vector
 ** \param V matrix
 ** \param Ainv Invert matrix.
 ** \pre U.rows() >= U.cols()
 ** \pre V.rows() == V.cols()
 ** \pre U.cols() == (W_last_W_first)
 ** \pre U.rows() == Ainv.cols()
 ** \pre U.cols() == Ainv.rows()
 */
template <class Matrix1, 
	  typename RandomAccessIterator1, 
	  class Matrix2, 
	  class Matrix3>
  inline
  void svd_inv(const Matrix1 & U,
	       RandomAccessIterator1 W_first,
	       RandomAccessIterator1 W_last,
	       const Matrix2 & V,
	       Matrix3& Ainv)

  {
    typedef typename Matrix1::value_type value_type;
    typedef typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type real;
    std::size_t U_rows = U.rows();
    std::size_t U_cols = U.cols();

    //------------------------------------------------------
    // svd truncature (small singular values are put to 0)
    //------------------------------------------------------
    real Wmax = *W_first;
    real Wmin = Wmax * slip::epsilon<value_type>();
    std::replace_if(W_first,W_last,std::bind2nd(std::less<real>(), Wmin),real(0));
    //------------------------------------------------------
    // find the first 0 value
    //------------------------------------------------------
    RandomAccessIterator1 it = std::find(W_first,W_last,real(0));
    std::size_t K = std::size_t(it - W_first);
     
    
    for(std::size_t i = 0; i < U_cols; ++i)
      {
	for(std::size_t j = 0; j < U_rows; ++j)
	  {
	    Ainv[i][j] = 0;
	    
	    for(std::size_t k = 0; k < K; ++k)
	      {

		Ainv[i][j] += (real(1)/W_first[k]) * slip::conj(U[j][k]) * V[i][k];

	      }
	  }
      }
  }

/**
 ** \brief Singular Value inverse of a matrix.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2007/03/13
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param A matrix to invert.
 ** \param Ainv inverted matrix.
 ** \pre A.rows() >= A.cols()
 ** \pre A.rows() == Ainv.cols()
 ** \pre A.cols() == Ainv.rows()
 ** \par Example:
 ** \code
 ** double x[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0, 16.0 , 18.0, 5.0};
 ** double y[] ={7.0, 14.0, 11.0, 21.0, 15.0, 17.0, 31.0, 23.0, 25.0, 33.0 , 37.0, 11.0};
 ** slip::Matrix<double> Msol(12,2);
 ** slip::Vector<double> Bsol(12);
 ** std::fill(Msol.col_begin(0),Msol.col_end(0),1.0);
 ** std::copy(x,x+12,Msol.col_begin(1));
 ** std::copy(y,y+12,Bsol.begin());
 ** std::cout<<"Msol = \n"<<Msol<<std::endl;
 ** slip::Vector<double> Xsol(2);
 ** slip::Matrix<double> Msolinv(2,12);
 ** slip::svd_inv(Msol,Msolinv);
 ** std::cout<<"Msolinv = \n"<<Msolinv<<std::endl;
 ** slip::matrix_vector_multiplies(Msolinv,Bsol,Xsol);
 ** std::cout<<"Xsol = \n"<<Xsol<<std::endl;
 ** \endcode
 */
template <class Matrix1, 
	  class Matrix2>
inline
void svd_inv(const Matrix1 & A,
	     Matrix2& Ainv)
{
  typedef typename Matrix1::value_type value_type;
    typedef typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type real;		
  

    slip::Vector<real>  S(A.cols());
    slip::Array2d<value_type> U(A.rows(),A.cols());
    slip::Array2d<value_type> V(A.cols(),A.cols());
    
    //-----------------------------------------------
    // svd decomposition.
    //-----------------------------------------------
    slip::svd(A,U,S.begin(),S.end(),V);
    //-----------------------------------------------
    // svd inverse
    //-----------------------------------------------
    slip::svd_inv(U,S.begin(),S.end(),V,Ainv);
}

/**
 ** \brief Pseudo inverse of a matrix 
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2007/03/13
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param A matrix to invert.
 ** \param Ainv inverted matrix.
 ** \pre A.rows() >= A.cols()
 ** \pre A.rows() == Ainv.cols()
 ** \pre A.cols() == Ainv.rows()
 ** \par Example:
 ** \code
 ** double x[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0, 16.0 , 18.0, 5.0};
 ** double y[] ={7.0, 14.0, 11.0, 21.0, 15.0, 17.0, 31.0, 23.0, 25.0, 33.0 , 37.0, 11.0};
 ** slip::Matrix<double> Msol(12,2);
 ** slip::Vector<double> Bsol(12);
 ** std::fill(Msol.col_begin(0),Msol.col_end(0),1.0);
 ** std::copy(x,x+12,Msol.col_begin(1));
 ** std::copy(y,y+12,Bsol.begin());
 ** std::cout<<"Msol = \n"<<Msol<<std::endl;
 ** slip::Vector<double> Xsol(2);
 ** slip::Matrix<double> Msolinv(2,12);
 ** slip::pinv(Msol,Msolinv);
 ** std::cout<<"Msolinv = \n"<<Msolinv<<std::endl;
 ** slip::matrix_vector_multiplies(Msolinv,Bsol,Xsol);
 ** std::cout<<"Xsol = \n"<<Xsol<<std::endl;
 ** \endcode
 */
template <class Matrix1, 
	  class Matrix2>
inline
void pinv(const Matrix1 & A,
	  Matrix2& Ainv)
{
  slip::svd_inv(A,Ainv);
}
/* @} */
} // end slip

#endif //SLIP_LINEAR_ALGEBRA_SVD_HPP
