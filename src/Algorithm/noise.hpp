/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file noise.hpp
 * 
 * \brief Provides some algorithms to add noise to ranges.
 * 
 */
#ifndef SLIP_NOISE_HPP
#define SLIP_NOISE_HPP


#include <ctime>
#include <boost/random.hpp>
#include "macros.hpp"

namespace slip
{

  /** \name Gaussian noise algorithms */
  /* @{ */
  /**
   ** \brief Add a gaussian noise to a container
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2007/06/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param result begin OutputIterator on results
   ** \param mean Mean of the gaussian noise.
   ** \param var Standard deviation of the gaussian noise.
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \par Example:
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::add_gaussian_noise(tab.begin(),tab.end(),noisy.begin(),1.0,0.02);
   ** \endcode
  */
  template<typename InputIterator, typename OutputIterator, typename Real>
  inline
  void add_gaussian_noise(InputIterator first, 
			  InputIterator last, 
			  OutputIterator result, 
			  const Real mean = Real(0.0), const Real var = Real(0.01))
  {
    assert(first != last);
	boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
	boost::normal_distribution<> distrib(mean,var);
  	boost::variate_generator<boost::mt19937&, boost::normal_distribution<> > deg(generator, distrib);	
	for (;first!=last;++first,++result)
	  {
	    *result = *first + deg();
	  }
  }
/**
   ** \brief Add a gaussian noise to a container according to a mask sequence
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/16
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param mask_first  An inputIterator.
   ** \param result begin OutputIterator on results
   ** \param mean Mean of the gaussian noise.
   ** \param var Standard deviation of the gaussian noise.
   ** \param value true value of the mask range. Default is 1.
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \pre [mask_first + (last-first)) is a valid range
   ** \par Example:
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::Matrix<int> Mask(5,5,0);
   ** mask(0,0)=1;mask(1,1)=1; mask(2,2)=1; mask(3,3)=1; mask(4,4)=1;  
   ** slip::add_gaussian_mask(tab.begin(),tab.end(),Mask.begin(),noisy.begin(),1.0,0.02,1);
   ** \endcode
  */
 template<typename InputIterator, typename OutputIterator, typename Real,typename MaskIterator>
  inline
  void add_gaussian_noise_mask(InputIterator first, 
			       InputIterator last, 
			       MaskIterator mask_first,
			       OutputIterator result,
			       const Real mean = Real(0.0), const Real var = Real(0.01), typename std::iterator_traits<MaskIterator>::value_type value=typename                          std::iterator_traits<MaskIterator>::value_type(1))
  {
        assert(first != last);
	boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
	boost::normal_distribution<> distrib(mean,var);
  	boost::variate_generator<boost::mt19937&, boost::normal_distribution<> > deg(generator, distrib);	
	 for (;first!=last;++first,++result,++mask_first)
             {

                 if(*mask_first == value)
	         {
                    *result = *first + deg();
                 }
		 else
		   {
		     *result = *first;
		   }
	  }
  }

/**
   ** \brief Add a gaussian noise to a container according to a predicate
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/16
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param result begin OutputIterator on results
   ** \param pred   a predicate
   ** \param mean Mean of the gaussian noise.
   ** \param var Standard deviation of the gaussian noise.
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \par Example:
   ** \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(15));
   **   };
   ** \endcode
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::iota(tab.begin(),tab.end(),1.0,1.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::add_gaussian_noise_if(tab.begin(),tab.end(),noisy.begin(),lt5Predicate<double>,1.0,0.02);
   ** \endcode
  */
 template<typename InputIterator, typename OutputIterator, typename Real,typename Predicate>
  inline
  void add_gaussian_noise_if(InputIterator first, 
			  InputIterator last, 
			  OutputIterator result,
                          Predicate pred,
			  const Real mean = Real(0.0), const Real var = Real(0.01))
  {
        assert(first != last);
	boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
	boost::normal_distribution<> distrib(mean,var);
  	boost::variate_generator<boost::mt19937&, boost::normal_distribution<> > deg(generator, distrib);	
	 for (;first!=last;++first,++result)
             {

                 if(pred(*first))
	         {
                    *result = *first + deg();
                 }
		 else
		   {
		     *result = *first;
		   }
	  }
  }

 /* @} */

  /** \name Speckle noise algorithms */
  /* @{ */
  /**
   ** \brief Add a speckle noise to a container
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2007/06/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param result begin OutputIterator on results
   ** \param var Standard deviation of the speckle noise.
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \pre 0 <= var <= 1
   ** \par Example:
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::add_speckle_noise(tab.begin(),tab.end(),noisy.begin(),0.04);
   ** \endcode
   
  */
  template<typename InputIterator, typename OutputIterator, typename Real>
  inline
  void add_speckle_noise(InputIterator first, 
			 InputIterator last, 
			 OutputIterator result, 
			 const Real var = 0.04)
  {
    assert(first != last);
    assert (var <= Real(1));
    assert (var >= Real(0));
    boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
    boost::normal_distribution<> distrib(-1.0,1.0);
    boost::variate_generator<boost::mt19937&, boost::normal_distribution<> > deg(generator, distrib);	
    Real sqrt_value=std::sqrt(var);
    for (;first!=last;++first,++result)
      {
	*result = *first * (1 + deg() * sqrt_value);
      }
  }
/**
   ** \brief Add a speckle noise to a container according to a mask sequence
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/16
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param mask_first  An inputIterator.
   ** \param result begin OutputIterator on results
   ** \param var Standard deviation of the speckle noise.
   ** \param  value true value of the mask range. Default is 1.
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \pre [mask_first + (last-first)) is a valid range
   ** \pre 0 <= var <= 1
   ** \par Example:
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::Matrix<int> Mask(5,5,0);
   ** mask(0,0)=1;mask(1,1)=1; mask(2,2)=1; mask(3,3)=1; mask(4,4)=1;  
   ** slip::add_speckle_noise_mask(tab.begin(),tab.end(),Mask.begin(),noisy.begin(),0.04,1);
   ** \endcode
   
  */
template<typename InputIterator, typename OutputIterator, typename Real,typename MaskIterator>
  inline
  void add_speckle_noise_mask(InputIterator first, 
			      InputIterator last, 
			      MaskIterator mask_first,
			      OutputIterator result,
			      const Real var = 0.04, 
			      typename std::iterator_traits<MaskIterator>::value_type value=typename                          std::iterator_traits<MaskIterator>::value_type(1))
{
    assert(first != last);
    assert (var <= Real(1));
    assert (var >= Real(0));
    boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
    boost::normal_distribution<> distrib(-1.0,1.0);
    boost::variate_generator<boost::mt19937&, boost::normal_distribution<> > deg(generator, distrib);	
    Real sqrt_value=std::sqrt(var);
    for (;first!=last;++first,++result,++mask_first)
      {
        if(*mask_first == value)
	{
          *result = *first * (1 + deg() * sqrt_value);
        }
	else
	  {
	    *result = *first;
	  }
     }
}

/**
   ** \brief Add a speckle noise to a container according to a predicate
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/16
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param result begin OutputIterator on results
   ** \param pred   a predicate
   ** \param var Standard deviation of the speckle noise.
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \pre 0 <= var <= 1
   ** \par Example:
   ** \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(15));
   **   };
   ** \endcode

   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::iota(tab.begin(),tab.end(),1.0,1.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::add_speckle_noise_if(tab.begin(),tab.end(),noisy.begin(),lt5Predicate<double>,0.04);
    ** \endcode
*/
template<typename InputIterator, typename OutputIterator, typename Real, typename Predicate>
  inline
  void add_speckle_noise_if(InputIterator first, 
			  InputIterator last, 
			  OutputIterator result,
                          Predicate pred,
			  const Real var = 0.04)
{
    assert(first != last);
    assert (var <= Real(1));
    assert (var >= Real(0));
    boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
    boost::normal_distribution<> distrib(-1.0,1.0);
    boost::variate_generator<boost::mt19937&, boost::normal_distribution<> > deg(generator, distrib);	
    Real sqrt_value=std::sqrt(var);
    for (;first!=last;++first,++result)
      {
        if(pred(*first))
	{
          *result = *first * (1 + deg() * sqrt_value);
        }
	else
	  { 
	    *result = *first;
	  }
     }
}

  /* @} */

  /** \name Uniform noise algorithms */
  /* @{ */
  /**
   ** \brief Add a uniform noise to a container
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2007/06/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param result begin. OutputIterator on results
   ** \param min Minimum noise value
   ** \param max Maximum noise value
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \par Example:
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::add_uniform_noise(tab.begin(),tab.end(),noisy.begin(),-1.0,1.0);
   ** \endcode
   */
  template<typename InputIterator, typename OutputIterator, typename Real>
  inline
  void add_uniform_noise(InputIterator first, 
			 InputIterator last, 
			 OutputIterator result, 
			 const Real min = Real(0.0), 
			 const Real max = Real(1.0))
  {
    assert(first != last);
    boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
    boost::uniform_real<> distrib(min,max);
    boost::variate_generator<boost::mt19937&, boost::uniform_real<> > deg(generator, distrib);	
    for (;first!=last;++first,++result)
      {
	*result = *first + deg();
      }
  }
/**
   ** \brief Add a uniform noise to a container according to mask sequence
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/16
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param mask_first  An inputIterator.
   ** \param result begin OutputIterator on results
   ** \param min Minimum noise value
   ** \param max Maximum noise value
   ** \param  value true value of the mask range. Default is 1.
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \pre [mask_first + (last-first)) is a valid range
   ** \pre The input and output container must have the same size.
   ** \par Example:
   ** \code
    ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::Matrix<int> mask(5,5,0);
   ** mask(0,0)=1;mask(1,1)=1; mask(2,2)=1; mask(3,3)=1; mask(4,4)=1; 
   ** slip::add_uniform_noise_mask(tab.begin(),tab.end(),mask.begin(),noisy.begin(),-1.0,1.0,1);
   ** \endcode
   */
template<typename InputIterator, typename OutputIterator, typename Real,typename MaskIterator>
  inline
  void add_uniform_noise_mask(InputIterator first, 
			      InputIterator last, 
			      MaskIterator mask_first,
			      OutputIterator result,
			      const Real min = Real(0.0), 
			      const Real max = Real(1.0),
                              typename std::iterator_traits<MaskIterator>::value_type value=typename                          std::iterator_traits<MaskIterator>::value_type(1))
{
    assert(first != last);
    boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
    boost::uniform_real<> distrib(min,max);
    boost::variate_generator<boost::mt19937&, boost::uniform_real<> > deg(generator, distrib);	
    for (;first!=last;++first,++result,++mask_first)
      {
       if(*mask_first == value)
	{
          *result = *first + deg();
        }
       else
	 {
	   *result = *first;
	 }
      }
}
/**
   ** \brief Add a uniform noise to a container according to a predicate
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/16
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param result begin OutputIterator on results
   ** \param pred an predicate.
   ** \param min Minimum noise value
   ** \param max Maximum noise value
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \par Example:
   ** \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(15));
   **   };
   ** \endcode
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::iota(tab.begin(),tab.end(),1.0,1.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::add_uniform_noise_if(tab.begin(),tab.end(),noisy.begin(),lt5Predicate<double>,-1.0,1.0);
   ** \endcode
   */
template<typename InputIterator, typename OutputIterator, typename Real,typename Predicate>
  inline
  void add_uniform_noise_if(InputIterator first, 
			      InputIterator last, 
			      OutputIterator result,
                              Predicate pred,
			      const Real min = Real(0.0), 
			      const Real max = Real(1.0))
{
    assert(first != last);
    boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
    boost::uniform_real<> distrib(min,max);
    boost::variate_generator<boost::mt19937&, boost::uniform_real<> > deg(generator, distrib);	
    for (;first!=last;++first,++result)
      {
       if(pred(*first))
	{
          *result = *first + deg();
        }
       else
	 {
	   *result = *first;
	 }
      }
}
  /* @} */

  /** \name Salt and pepper noise algorithms */
  /* @{ */
  /**
   ** \brief Add a salt and pepper noise to a container
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2007/06/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end Inputiterator on datas
   ** \param result begin OutputIterator on results
   ** \param density Density of noisy items
   ** \param salt_value Value used for "white" items
   ** \param pepper_value Value used for "black" items
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \pre 0 <= density <= 1
   ** \par Example:
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::add_salt_and_pepper_noise(tab.begin(),tab.end(),noisy.begin(),0.20,0.0,255.0);
   ** \endcode
  */
template<typename InputIterator, typename OutputIterator, typename Real> 
  inline
  void add_salt_and_pepper_noise(InputIterator first, 
				 InputIterator last, 
				 OutputIterator result, 
				 const Real density = Real(0.05), 
				 const typename std::iterator_traits<OutputIterator>::value_type salt_value = typename std::iterator_traits<OutputIterator>::value_type(1.0), 
				 const typename std::iterator_traits<OutputIterator>::value_type pepper_value = typename std::iterator_traits<OutputIterator>::value_type(0.0))
  {
	assert (density <= Real(1));
	assert (density >= Real(0));

	boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
	boost::uniform_real<> distrib(0.0,1.0);
  	boost::variate_generator<boost::mt19937&, boost::uniform_real<> > deg(generator, distrib);	
	for (;first!=last;++first,++result)
	  {
	    Real rand = deg();
	    // salt or pepper
	    if (rand < density)
	      {
		// Salt case
		if ((2 * rand) < density)
		  {
		    *result = salt_value;
		  } 
		else 
		  {
		    // Pepper case
		    *result = pepper_value;
		  }
	      } 
	    else 
	      {
		*result = *first;
	      }
	  }
  }

/**
   ** \brief Add a salt and pepper noise to a container according to a mask sequence
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/16
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param mask_first  An inputIterator.
   ** \param result begin OutputIterator on results
   ** \param density Density of noisy items
   ** \param salt_value Value used for "white" items
   ** \param pepper_value Value used for "black" items
   ** \param value true value of the mask range. Default is 1.
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \pre [mask_first + (last-first)) is a valid range
   ** \pre 0 <= density <= 1
   ** \pre The input and output container must have the same size.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::Matrix<int> mask(5,5,0);
   ** mask(0,0)=1;mask(1,1)=1; mask(2,2)=1; mask(3,3)=1; mask(4,4)=1; 
   ** slip::add_salt_and_pepper_noise_mask(tab.begin(),tab.end(),mask.begin(),noisy.begin(),0.20,0.0,255.0,1);
   ** \endcode
*/
template<typename InputIterator, typename OutputIterator, typename Real,typename MaskIterator >
  inline
  void add_salt_and_pepper_noise_mask(InputIterator first, 
				      InputIterator last, 
				      MaskIterator mask_first,
				      OutputIterator result, 
				      const Real density = Real(0.05), 
				      const Real salt_value = Real(1.0), 
				      const Real pepper_value = Real(0.0),
				      typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
	assert (density <= Real(1));
	assert (density >= Real(0));

	boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
	boost::uniform_real<> distrib(0.0,1.0);
  	boost::variate_generator<boost::mt19937&, boost::uniform_real<> > deg(generator, distrib);	
	for (;first!=last;++first,++result,++mask_first)
	  {
	   if(*mask_first==value)
            {
             Real rand = deg();
	    // salt or pepper
	    if (rand < density)
	      {
		// Salt case
		if ((2 * rand) < density)
		  {
		    *result = salt_value;
		  } 
		else 
		  {
		    // Pepper case
		    *result = pepper_value;
		  }
	      } 
	    else 
	      {
		*result = *first;
	      }
	    }
	   else
	     {
	       *result = *first;
	     }
       } 
  }

/**
   ** \brief Add a salt and pepper noise to a container according to a predicate
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/16
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param result begin OutputIterator on results
   ** \param pred  A predicate.
   ** \param density Density of noisy items
   ** \param salt_value Value used for "white" items
   ** \param pepper_value Value used for "black" items
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \pre 0 <= density <= 1
   ** \par Example:
   ** \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(15));
   **   };
   ** \endcode
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::iota(tab.begin(),tab.end(),1.0,1.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::add_salt_and_pepper_noise_if(tab.begin(),tab.end(),noisy.begin(),lt5Predicate<double>,0.20,0.0,255.0);
   ** \endcode
*/
template<typename InputIterator, typename OutputIterator, typename Real,typename Predicate >
  inline
  void add_salt_and_pepper_noise_if(InputIterator first, 
				    InputIterator last, 
				    OutputIterator result, 
                                    Predicate pred,
				 const Real density = Real(0.05), 
				 const Real salt_value = Real(1.0), 
				 const Real pepper_value = Real(0.0))
  {
	assert (density <= Real(1));
	assert (density >= Real(0));

	boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
	boost::uniform_real<> distrib(0.0,1.0);
  	boost::variate_generator<boost::mt19937&, boost::uniform_real<> > deg(generator, distrib);	
	for (;first!=last;++first,++result)
	  {
	   if(pred(*first))
            {
             Real rand = deg();
	    // salt or pepper
	    if (rand < density)
	      {
		// Salt case
		if ((2 * rand) < density)
		  {
		    *result = salt_value;
		  } 
		else 
		  {
		    // Pepper case
		    *result = pepper_value;
		  }
	      } 
	    else 
	      {
		*result = *first;
	      }
	  }
	   else
	     {
	       *result = *first;
	     }
       } 
  }
/* @} */


 /** \name Add exponential noise algorithms */
  /* @{ */
  /**
   ** \brief Add an exponential noise to a container. That is to say that the noise follow the distribution which PDF is \f$ f(x)=\lambda \exp(-\lambda x) \f$.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2017/01/20
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param result begin OutputIterator on results
   ** \param lambda rate parameter of the exponential noise.
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \pre lambda > 0
   ** \par Example:
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::add_exponential_noise(tab.begin(),tab.end(),noisy.begin(),0.3);
   ** \endcode
  */
template<typename InputIterator, 
	 typename OutputIterator, 
	 typename Real>
inline
void add_exponential_noise(InputIterator first,
			   InputIterator last,
			   OutputIterator result,
			   const Real lambda = Real(1.0))
{
  assert(first != last);
  assert(lambda > static_cast<Real>(0.0));
  boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
  boost::exponential_distribution<Real> distrib(lambda);
  boost::variate_generator<boost::mt19937&, boost::exponential_distribution<Real> > deg(generator, distrib); 
 
  for (;first!=last;++first,++result)
    {
      *result = *first + deg();
    }
 }


/**
   ** \brief Add a exponential noise to a container according to a mask sequence.
   ** That is to say that the noise follow the distribution which PDF is \f$ f(x)=\lambda \exp(-\lambda x) \f$ inside the defined mask.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2017/01/20
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param mask_first  An inputIterator.
   ** \param result begin OutputIterator on results
   ** \param lambda rate parameter of the exponential noise.
   ** \param value true value of the mask range. Default is 1.
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \pre [mask_first + (last-first)) is a valid range
   ** \pre lambda > 0
   ** \par Example:
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::Matrix<int> Mask(5,5,0);
   ** mask(0,0)=1;mask(1,1)=1; mask(2,2)=1; mask(3,3)=1; mask(4,4)=1;  
   ** slip::add_exponential_noise_mask(tab.begin(),tab.end(),Mask.begin(),noisy.begin(),0.3,1);
   ** \endcode
  */
template<typename InputIterator, 
	 typename OutputIterator, 
	 typename Real,
	 typename MaskIterator>
  inline
  void add_exponential_noise_mask(InputIterator first, 
				  InputIterator last, 
				  MaskIterator mask_first,
				  OutputIterator result,
				  const Real lambda = Real(1.0),
				  typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
        assert(first != last); 
	assert(lambda > static_cast<Real>(0.0));
	 boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
	 boost::exponential_distribution<Real> distrib(lambda);
	 boost::variate_generator<boost::mt19937&, boost::exponential_distribution<Real> > deg(generator, distrib); 
	 for (;first!=last;++first,++result,++mask_first)
	  {

	    if(*mask_first == value)
	      {
		*result = *first + deg();
	      }
	    else
	      {
		*result = *first;
	      }
	  }
  }

/**
   ** \brief Add a exponential noise to a container according to a predicate.
   ** That is to say that the noise follow the distribution which PDF is \f$ f(x)=\lambda \exp(-\lambda x) \f$ when the predicate is valid.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2017/01/20
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param result begin OutputIterator on results
   ** \param pred   a predicate
   ** \param lambda rate parameter of the exponential noise.
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \par Example:
   ** \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(15));
   **   };
   ** \endcode
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::iota(tab.begin(),tab.end(),1.0,1.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::add_exponential_noise_if(tab.begin(),tab.end(),noisy.begin(),lt5Predicate<double>,0.3);
   ** \endcode
  */
  template<typename InputIterator, 
	   typename OutputIterator, 
	   typename Real,
	   typename Predicate>
  inline
void add_exponential_noise_if(InputIterator first, 
			      InputIterator last, 
			      OutputIterator result,
			      Predicate pred, 
			      const Real lambda = Real(1.0))
  {
        assert(first != last); 
	assert(lambda > static_cast<Real>(0.0));
	boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
	boost::exponential_distribution<Real> distrib(lambda);
	boost::variate_generator<boost::mt19937&, boost::exponential_distribution<Real> > deg(generator, distrib); 

	for (;first!=last;++first,++result)
             {

                 if(pred(*first))
	         {
                    *result = *first + deg();
                 }
		 else
		   {
		     *result = *first;
		   }
	  }
	
  }
 /* @} */


 /** \name Add gamma (Erlang) noise algorithms */
  /* @{ */
   /**
   ** \brief Add a gamma (Erlang) noise to a container. That is to say that the noise follow the distribution which PDF is \f$ f(x,\alpha,\beta)=\frac{\beta^{\alpha}x^{\alpha-1}\exp(-\beta x)}{\Gamma(\alpha)}\, x \ge 0\, \alpha,\beta > 0\f$.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2017/01/20
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param result begin OutputIterator on results
   ** \param alpha shape parameter.
   ** \param beta  inverse scale parameter (rate parameter)
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \pre alpha > 0
   ** \pre beta > 0
   ** \par Example:
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::add_gamma_noise(tab.begin(),tab.end(),noisy.begin(),2.0,0.5);
   ** \endcode
  */
  template<typename InputIterator, 
	 typename OutputIterator, 
	 typename Real>
inline
void add_gamma_noise(InputIterator first,
		     InputIterator last,
		     OutputIterator result,
		     const Real alpha = Real(1.0),
		     const Real beta  = Real(1.0))
{
  assert(first != last);
  assert(alpha > static_cast<Real>(0.0));
  assert(beta > static_cast<Real>(0.0));
  boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
  boost::gamma_distribution<Real> distrib(alpha,beta);
  boost::variate_generator<boost::mt19937&, boost::gamma_distribution<Real> > deg(generator, distrib); 
 
  for (;first!=last;++first,++result)
    {
      *result = *first + deg();
    }
 }

/**
   ** \brief Add a gamma (Erlang) noise to a container according to a mask sequence.
   ** That is to say that the noise follow the distribution which PDF is \f$ f(x,\alpha,\beta)=\frac{\beta^{\alpha}x^{\alpha-1}\exp(-\beta x)}{\Gamma(\alpha)}\, x \ge 0\, \alpha,\beta > 0\f$  inside the defined mask.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2017/01/20
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param mask_first  An inputIterator.
   ** \param result begin OutputIterator on results
   ** \param alpha shape parameter.
   ** \param beta  inverse scale parameter (rate parameter)
   ** \param value true value of the mask range. Default is 1.
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \pre [mask_first + (last-first)) is a valid range
   ** \pre alpha > 0
   ** \pre beta > 0
   ** \par Example:
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::Matrix<int> Mask(5,5,0);
   ** mask(0,0)=1;mask(1,1)=1; mask(2,2)=1; mask(3,3)=1; mask(4,4)=1;  
   ** slip::add_gamma_noise_mask(tab.begin(),tab.end(),Mask.begin(),noisy.begin(),2.0,0.5,1);
   ** \endcode
  */
  template<typename InputIterator, 
	 typename OutputIterator, 
	 typename Real,
	 typename MaskIterator>
  inline
  void add_gamma_noise_mask(InputIterator first, 
			    InputIterator last, 
			    MaskIterator mask_first,
			    OutputIterator result,
			    const Real alpha = Real(1.0),
			    const Real beta  = Real(1.0), 
			    typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
        assert(first != last);
	assert(alpha > static_cast<Real>(0.0));
	assert(beta > static_cast<Real>(0.0));
	boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
	boost::gamma_distribution<Real> distrib(alpha,beta);
	boost::variate_generator<boost::mt19937&, boost::gamma_distribution<Real> > deg(generator, distrib); 
	for (;first!=last;++first,++result,++mask_first)
	  {

	    if(*mask_first == value)
	      {
		*result = *first + deg();
	      }
	    else
	      {
		*result = *first;
	      }
	  }
  }

  /**
   ** \brief Add a gamma (Erlang) noise to a container according to a predicate.
   ** That is to say that the noise follow the distribution which PDF is \f$ f(x,\alpha,\beta)=\frac{\beta^{\alpha}x^{\alpha-1}\exp(-\beta x)}{\Gamma(\alpha)}\, x \ge 0\, \alpha,\beta > 0\f$ when the predicate is valid.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2017/01/20
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param result begin OutputIterator on results
   ** \param pred   a predicate
   ** \param alpha shape parameter.
   ** \param beta  inverse scale parameter (rate parameter)
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \pre alpha > 0
   ** \pre beta > 0
   ** \par Example:
   ** \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(15));
   **   };
   ** \endcode
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::iota(tab.begin(),tab.end(),1.0,1.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::add_gamma_noise_if(tab.begin(),tab.end(),noisy.begin(),lt5Predicate<double>,2.0,0.5);
   ** \endcode
  */
  template<typename InputIterator, 
	   typename OutputIterator, 
	   typename Real,
	   typename Predicate>
  inline
  void add_gamma_noise_if(InputIterator first, 
			  InputIterator last, 
			  OutputIterator result,
                          Predicate pred,
			  const Real alpha = Real(1.0),
			  const Real beta  = Real(1.0))
  {
        assert(first != last);
	 assert(alpha > static_cast<Real>(0.0));
	 assert(beta > static_cast<Real>(0.0));
	boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
	boost::gamma_distribution<Real> distrib(alpha,beta);
	boost::variate_generator<boost::mt19937&, boost::gamma_distribution<Real> > deg(generator, distrib); 
	 for (;first!=last;++first,++result)
             {

                 if(pred(*first))
	         {
                    *result = *first + deg();
                 }
		 else
		   {
		     *result = *first;
		   }
	  }
  }


 /* @} */

/** \name Add Noise Level Fonction noise algorithms */
/* @{ */
/**
   ** \brief Add a Noise Level Fonction (NLF) noise to a container. Considering that the input container ith sample is denoted  \f$ f_i\f$ and the ith sample of the container noise version is denoted  \f$ g_i\f$. The variance of the noise follow a second order polynomial of the sample value of the container. That is to say that the noise version of the container follow: 
   ** \f{eqnarray*}{
   ** \mathbb{E}\left[g_i\right] &=& f_i\\
   ** Var\left[g_i\right] &=&NLF(f_i)=a_2 f_i^2 +a_1 f_i + a_0
   ** \f}
   ** 
   **  - if \f$(a_2, a_1) = (0, 0)\f$ it is a gaussian additive noise.
   **  - if \f$(a_2, a_0) = (0, 0)\f$ it is a Poisson noise.
   **  - if \f$(a_1, a_0) = (0, 0)\f$ it is a multiplicative noise.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2017/01/20
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param first begin InputIterator on datas
   ** \param last past-to-end InputIterator on datas
   ** \param result begin OutputIterator on results
   ** \param a2 second order polynomial coefficient.
   ** \param a1 first order polynomial coefficient.
   ** \param a0 zero order polynomial coefficient.
   ** \pre [first, last) is a valid range
   ** \pre [result + (last-first)) is a valid range
   ** \par Example:
   ** \code
   ** slip::Matrix<double> tab(5,5,100.0);
   ** slip::iota(tab.begin(),tab.end(),1.0,1.0);
   ** slip::Matrix<double> noisy(5,5);
   ** slip::add_NLF_noise(tab.begin(),tab.end(),noisy.begin(),0.1,0.001,0.1);
   ** \endcode
  */
template<typename InputIterator, 
	 typename OutputIterator, 
	 typename Real>
inline
void apply_NLF_noise(InputIterator first, 
		     InputIterator last, 
		     OutputIterator result, 
		     const Real& a2, 
		     const Real& a1,
		     const Real& a0)
{
  assert(first != last);
  boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
  boost::uniform_real<> distrib(Real(0.0),Real(1.0));
  boost::variate_generator<boost::mt19937&, boost::uniform_real<> > deg(generator, distrib);	
  for (;first!=last;++first,++result)
    {
      Real std = std::sqrt(a2*(*first * *first) + a1* *first + a0);
      Real u = deg();
      Real v = deg();
      Real n = std*std::sqrt(-slip::constants<Real>::two()*std::log(u))*std::cos(slip::constants<Real>::two()*slip::constants<Real>::pi()*v);
      
      *result = *first + n;
    }
}
 /* @} */
  
}//slip::



#endif //SLIP_NOISE_HPP
