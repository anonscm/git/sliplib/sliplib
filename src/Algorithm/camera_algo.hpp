/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file camera_algo.hpp
 * 
 * \brief Provides a camera algorithms.
 * \since 1.2.0
 */

#ifndef SLIP_CAMERA_ALGO_HPP
#define SLIP_CAMERA_ALGO_HPP

#include "Point2d.hpp"
#include "Point3d.hpp"
#include "Vector3d.hpp"
#include "Vector.hpp"
#include "Matrix.hpp"
#include "io_tools.hpp"
#include "linear_algebra_eigen.hpp"
#include "linear_algebra_qr.hpp"
#include "norms.hpp"
#include "macros.hpp"
#include "MultivariatePolynomial.hpp"
namespace slip
{
  enum DECOMP_TYPE 
    {
      RQ,
      direct
    };
}

namespace slip 
{

 /**
   ** \brief Computes the RQ decomposition of a matrix
   **
   ** Compute the RQ decomposition of A.
   ** A = RQ
   ** where R is an upper triangular matrix and Q is an orthogonal matrix.
   **
   ** code is based on the QR decomposition qr_decomp
   **
   ** \author Markus Jehle <jehle_markus@yahoo.de> : conceptor
   ** \date 2008/03/21
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param R Matrix container
   ** \param Q Matrix container
   **
   ** \return 0 if M is not singular, 1 if M is singular
   **
   ** \pre Matrix must have the double bracket element accessor
   ** \pre Matrix are supposed to be allocated first
   ** \pre A.dim1()==A.dim2()
   ** \pre A.dim1()==R.dim1()
   ** \pre A.dim2()==R.dim2()
   ** \pre A.dim1()==Q.dim1()
   ** \pre A.dim2()==Q.dim2()
   **
   */

template <class Matrix1, class Matrix2, class Matrix3>
  inline
  int rq_decomp(const Matrix1 & A, Matrix2 & R, Matrix3 & Q)
  {
	assert(A.dim1()==A.dim2());
	assert(A.dim1()==R.dim1());
	assert(A.dim2()==R.dim2());
	assert(A.dim1()==Q.dim1());
	assert(A.dim2()==Q.dim2());
	typedef typename Matrix1::value_type Type;
	const std::size_t n = A.dim1();

	slip::Matrix<Type> P(n,n);
	for(std::size_t i = 0; i < n; ++i) 
	  {
	    P[i][n-1-i] = static_cast<Type>(1.0);
	  }
	slip::Matrix<Type> At(n,n); 
	slip::transpose(A,At);
	slip::Matrix<Type> AtP(n,n); 
	slip::multiplies(At,P,AtP);
	slip::Matrix<Type> Q2(n,n); 
	slip::Matrix<Type> R2(n,n);
	slip::householder_qr(AtP,Q2,R2);
	//slip::qr_decomp(AtP,Q2,R2);
	slip::Matrix<Type> H(n,n);
	slip::multiplies(Q2,P,H); 
	slip::transpose(H,Q);
	slip::multiplies(R2,P,H); 
	slip::multiplies(P,H,R2); 
	slip::transpose(R2,R);

	return 0;
  }

	/*!
  	** \author Markus Jehle <jehle_markus_AT_yahoo.de>
	** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
	** \author Gomit Guillaume <gomit.guillaume_AT_univ-poitiers.fr>
  	** \version 0.0.1
  	** \date 2013/02/13
	** \since 1.2.0
  	** \brief Get 	calibration parameters using the DLT
  	** \param P 	Matrix containing the input data.
  	** \param Mat 	3x4-Matrix containing the calibration parameters
	** \remarks Format:
	** \f$(x,y)\f$ are the image coordinates, \f$(X,Y,Z)\f$ are the world coordinates.
	** \f[
	** \begin{array}{ccccc}
	** \cdots & \cdots & \cdots & \cdots & \cdots \\
	** x_i & y_i & X_i & Y_i & Z_i  \\
	** \cdots & \cdots & \cdots & \cdots & \cdots \\
	** \end{array}
	** \f]
  	*/
  template <typename Type>
  void getpars_DLT(const slip::Matrix<Type>& P, 
		   slip::Matrix<Type>& Mat) 
  {
 const std::size_t P_rows = P.rows();
    Type zero = static_cast<Type>(0.0);  
    const std::size_t A_rows = 2*P_rows;
    const std::size_t A_cols = 11;
    slip::Matrix<Type> A(A_rows,A_cols,zero);
    slip::Vector<Type> Y(A_rows);
    //    const Type m34 = static_cast<Type>(1.0);
     
    for(std::size_t i = 0; i < P_rows; ++i) 
      {
	std::size_t k = 2*i;
	A[k][0]=P[i][2]; //X_i
	A[k][1]=P[i][3]; //Y_i
	A[k][2]=P[i][4]; //Z_i
	A[k][3] = slip::constants<Type>::one();
	A[k][8]=-P[i][0]*P[i][2]; //-x_i*X_i
	A[k][9]=-P[i][0]*P[i][3]; //-x_i*Y_i
	A[k][10]=-P[i][0]*P[i][4]; //-x_i*Z_i
	A[k+1][4]=P[i][2]; //X_i
	A[k+1][5]=P[i][3]; //Y_i
	A[k+1][6]=P[i][4]; //Z_i
	A[k+1][7]=slip::constants<Type>::one();
	A[k+1][8]=-P[i][1]*P[i][2]; //-y_i*X_i
	A[k+1][9]=-P[i][1]*P[i][3]; //-y_i*Y_i
	A[k+1][10]=-P[i][1]*P[i][4]; //-y_i*Z_i

	Y[k]=P[i][0];//*m34; //x_i
	Y[k+1]=P[i][1];//*m34;	//y_i
      }

    //compute pseudo-inverse
    slip::Matrix<Type> ATA(A_cols,A_cols);
    slip::hmatrix_matrix_multiplies(A,A,ATA);
    slip::Matrix<Type> ATAm1(A_cols,A_cols);
    slip::inverse(ATA,ATAm1);
    slip::Matrix<Type> ATAm1AT(A_cols,A_rows);
    for(std::size_t i = 0; i < A_cols; ++i)
      {
	for(std::size_t j = 0; j < A_rows; ++j)
	  {
	    ATAm1AT[i][j] = std::inner_product(ATAm1.row_begin(i),ATAm1.row_end(i),
					   A.row_begin(j),
					   Type());
	  }
      }
    slip::matrix_vector_multiplies(ATAm1AT.upper_left(),
				   ATAm1AT.bottom_right(),
				   Y.begin(),Y.end(),
				   Mat.begin(),Mat.begin()+A_cols);


    Mat[2][3] = static_cast<Type>(1.0);
  }

/*!
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \author Gomit Guillaume <gomit.guillaume_AT_univ-poitiers.fr>
** \since 1.2.0
** \brief Get 	calibration parameters using the DLT.
** \param P 	Matrix containing the input data.
** \param Mat 	3x4-Matrix containing the calibration parameters.
** \remarks Format:
** \f$(x,y)\f$ are the image coordinates, \f$(X,Y,Z)\f$ are the world coordinates.
** \f[
** \begin{array}{ccccc}
** \cdots & \cdots & \cdots & \cdots & \cdots \\
** x_i & y_i & X_i & Y_i & Z_i  \\
** \cdots & \cdots & \cdots & \cdots & \cdots \\
** \end{array}
** \f]
*/
  template <typename Type>
  void getpars_DLT_norm(const slip::Matrix<Type>& P, 
			slip::Matrix<Type>& Mat) 
  {
    slip::getpars_DLT(P,Mat);
    Type norm = std::sqrt(  Mat[2][0]*Mat[2][0]
			  + Mat[2][1]*Mat[2][1]
			  + Mat[2][2]*Mat[2][2]);
    slip::matrix_scalar_multiplies(Mat,
				   static_cast<Type>(-1.0)/norm,
				   Mat);
  }


	/*!
  	** \author Markus Jehle <jehle_markus@yahoo.de>
	** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
	** \author Gomit Guillaume <gomit.guillaume_AT_univ-poitiers.fr>
  	** \version 0.0.1
  	** \date 2008/07/18
	** \since 1.2.0
  	** \brief Get calibration parameters using the Faugeras-Algorithm
  	** \param P Matrix containing the input data.
  	** \param Mat 3x4-Matrix containing the calibration parameters
	** \remarks Format:
	** \f$(x,y)\f$ are the image coordinates, \f$(X,Y,Z)\f$ are the world coordinates.
	** \f[
	** \begin{array}{ccccc}
	** \cdots & \cdots & \cdots & \cdots & \cdots \\
	** x_i & y_i & X_i & Y_i & Z_i  \\
	** \cdots & \cdots & \cdots & \cdots & \cdots \\
	** \end{array}
	** \f]
	** \todo optimize pseudo inverse...
  	*/
	template <typename Type>
	void getpars_Faugeras(const slip::Matrix<Type>& P, 
			      slip::Matrix<Type>& Mat) 
	{
	  const std::size_t P_rows = P.rows();
	  Matrix<Type> B(2*P_rows,9,static_cast<Type>(0.0));
	  Matrix<Type> C(2*P_rows,3);
	  
	  
	  for(std::size_t i = 0; i < P_rows; ++i) 
	    {
	      std::size_t k=2*i;
	      
	      B[k][0]=P[i][2]; //X_i
	      B[k][1]=P[i][3]; //Y_i
	      B[k][2]=P[i][4]; //Z_i
	      B[k][3]=static_cast<Type>(1.0);
	      B[k][8]=-P[i][0]; //-x_i
	      B[k+1][4]=P[i][2]; //X_i
	      B[k+1][5]=P[i][3]; //Y_i
	      B[k+1][6]=P[i][4]; //Z_i
	      B[k+1][7]=static_cast<Type>(1.0);
	      B[k+1][8]=-P[i][1]; //-y_i
	      
	      C[k][0]=-P[i][0]*P[i][2]; //-x_i*X_i
	      C[k][1]=-P[i][0]*P[i][3]; //-x_i*Y_i
	      C[k][2]=-P[i][0]*P[i][4]; //-x_i*Z_i
	      
	      C[k+1][0]=-P[i][1]*P[i][2]; //-y_i*X_i
	      C[k+1][1]=-P[i][1]*P[i][3]; //-y_i*Y_i
	      C[k+1][2]=-P[i][1]*P[i][4]; //-y_i*Z_i
	    }



	Matrix<Type> Ct(3,2*P_rows);
	slip::transpose(C,Ct);

	Matrix<Type> Bt(9,2*P_rows);
	slip::transpose(B,Bt);

	Matrix<Type> CtC(3,3);
	slip::matrix_matrix_multiplies(Ct,C,CtC);

	Matrix<Type> BtC(9,3);
	slip::matrix_matrix_multiplies(Bt,C,BtC);

	Matrix<Type> BtC_t(3,9);
	slip::transpose(BtC,BtC_t);

	Matrix<Type> CtB(3,9);
	slip::matrix_matrix_multiplies(Ct,B,CtB);

	Matrix<Type> BtB(9,9);
	slip::matrix_matrix_multiplies(Bt,B,BtB);

	Matrix<Type> BtB_inv(9,9);
	slip::inverse(BtB,BtB_inv);

	Matrix<Type> Tmp(9,3);
	slip::matrix_matrix_multiplies(BtB_inv,BtC,Tmp);


	Matrix<Type> D2(3,3);
	slip::matrix_matrix_multiplies(CtB,Tmp,D2);

	Matrix<Type> D(3,3);
	D = CtC-D2;

	slip::Vector<Type> EigenValues(3);
	slip::Matrix<Type> EigenVectors(3,3);
	slip::hermitian_eigen(D,EigenValues,EigenVectors);

	Vector<Type> x3_1(3);
	x3_1[0]=EigenVectors[0][2]; x3_1[1]=EigenVectors[1][2]; x3_1[2]=EigenVectors[2][2];

	Type norm=std::sqrt(x3_1[0]*x3_1[0]+x3_1[1]*x3_1[1]+x3_1[2]*x3_1[2]);

	x3_1/=norm;
	Vector<Type> x9_1(9);
	slip::matrix_vector_multiplies(Tmp,x3_1,x9_1);
	x9_1=-x9_1;

	Vector<Type> x3(3); Vector<Type> x9(9);
	x9=x9_1[8]>0 ? -x9_1 : x9_1;
	x3=x9_1[8]>0 ? -x3_1 : x3_1;

	Mat[0][0]=x9[0]; Mat[0][1]=x9[1]; Mat[0][2]=x9[2]; Mat[0][3]=x9[3];
	Mat[1][0]=x9[4]; Mat[1][1]=x9[5]; Mat[1][2]=x9[6]; Mat[1][3]=x9[7];
	Mat[2][0]=x3[0]; Mat[2][1]=x3[1]; Mat[2][2]=x3[2]; Mat[2][3]=x9[8];

	}



 template <typename Type>
 void Soloff_matrix(const slip::Matrix<Type>& P,
		    const slip::Vector<std::size_t>& columns,
		    slip::Matrix<Type>& A)
 {
   assert(columns.size() == 3);
   
   //x_i     y_i     X_i     Y_i     Z_i
   //P[i][0] P[i][1] P[i][2] P[i][3] P[i][4]
   const std::size_t P_rows = P.rows();
   const std::size_t indX = columns[0];
   const std::size_t indY = columns[1];
   const std::size_t indZ = columns[2];
   
   A.resize(P_rows,19);
   for(size_t i = 0; i < P_rows; i++) 
     {
       A[i][0]  = static_cast<Type>(1.0);
       A[i][1]  = P[i][indX];  		// X
       A[i][2]  = P[i][indX]*P[i][indX];	// XX
       A[i][3]  = A[i][2]*P[i][indX];	// XXX
       A[i][4]  = P[i][indY];  		// Y
       A[i][5]  = P[i][indX]*P[i][indY];	// XY
       A[i][6]  = P[i][indX]*A[i][5];	// XXY
       A[i][7]  = P[i][indY]*P[i][indY];	// YY
       A[i][8]  = P[i][indX]*A[i][7];	// XYY
       A[i][9]  = A[i][7]*P[i][indY];	// YYY
       A[i][10] = P[i][indZ];	        // Z
       A[i][11] = P[i][indX]*P[i][indZ];	// XZ
       A[i][12] = P[i][indX]*A[i][11];	// XXZ
       A[i][13] = P[i][indY]*P[i][indZ];	// YZ
       A[i][14] = P[i][indX]*A[i][13];	// XYZ
       A[i][15] = P[i][indY]*A[i][13];	// YYZ
       A[i][16] = P[i][indZ]*P[i][indZ];	// ZZ
       A[i][17] = P[i][indX]*A[i][16];	// XZZ
       A[i][18] = P[i][indY]*A[i][16];     // YZZ
     }
   
 }
  
 template <typename Type>
 void SoloffUV_matrix(const slip::Matrix<Type>& P,
		      slip::Matrix<Type>& A)
 {
   slip::Vector<std::size_t> columns(3);
   columns[0] = 2;//X
   columns[1] = 3;//Y
   columns[2] = 4;//Z
   slip::Soloff_matrix(P, columns, A);
 }
		       
  template <typename Type>
 void SoloffXY_matrix(const slip::Matrix<Type>& P,
		      slip::Matrix<Type>& A)
 {
   slip::Vector<std::size_t> columns(3);
   columns[0] = 0;//x
   columns[1] = 1;//y
   columns[2] = 4;//Z
   slip::Soloff_matrix(P, columns, A);
 }
  
 template <typename Type>
 void SoloffXZ_matrix(const slip::Matrix<Type>& P,
		      slip::Matrix<Type>& A)
 {
   slip::Vector<std::size_t> columns(3);
   columns[0] = 0;//x
   columns[1] = 1;//y
   columns[2] = 3;//Y
   slip::Soloff_matrix(P, columns, A);
 }

  template <typename Type>
 void SoloffYZ_matrix(const slip::Matrix<Type>& P,
		      slip::Matrix<Type>& A)
 {
   slip::Vector<std::size_t> columns(3);
   columns[0] = 0;//x
   columns[1] = 1;//y
   columns[2] = 2;//Y
   slip::Soloff_matrix(P, columns, A);
 }

  //add other solving methods
  template <typename Type>
  void computes_SoloffUV(const slip::Matrix<Type>& P,
			 slip::MultivariatePolynomial<Type,3>& Pol_x,
			 slip::MultivariatePolynomial<Type,3>& Pol_y) 
 {

   slip::Matrix<Type> AUV;
   slip::SoloffUV_matrix(P,AUV);
   slip::Vector<Type> bx(P.col_begin(0), P.col_end(0));//x vector
   slip::Vector<Type> mx(19);
   //solve AUV*mx=bx
   slip::least_square_ne_solve(AUV,mx,bx);
   
   slip::Vector<Type> mx2(20);
   std::copy(mx.begin(),mx.end(),mx2.begin());
   slip::MultivariatePolynomial<Type,3> Px(3,mx2.begin(),mx2.end());
   Pol_x=Px;

   slip::Vector<Type> by(P.col_begin(1), P.col_end(1));//y vector
   slip::Vector<Type> my(19);
   //solve AUV*my=by
   slip::least_square_ne_solve(AUV,mx,bx);
   
   
   slip::Vector<Type> my2(20);
   std::copy(my.begin(),my.end(),mx2.begin());
   slip::MultivariatePolynomial<Type,3> Py(3,my2.begin(),my2.end());
   Pol_y=Py;
 }
  
  template <typename Type>
  void computes_SoloffXY(const slip::Matrix<Type>& P,
			 slip::MultivariatePolynomial<Type,3>& Pol_XZ,
			 slip::MultivariatePolynomial<Type,3>& Pol_YZ) 
 {

   slip::Matrix<Type> AXY;
   slip::SoloffXY_matrix(P,AXY);
   slip::Vector<Type> bX(P.col_begin(2), P.col_end(2));//X vector
   slip::Vector<Type> mXZ(19);
   //solve AXY*mXZ=bX
   slip::least_square_ne_solve(AXY,mXZ,bX);
   
   slip::Vector<Type> mXZ2(20);
   std::copy(mXZ.begin(),mXZ.end(),mXZ2.begin());
   slip::MultivariatePolynomial<Type,3> PXZ(3,mXZ2.begin(),mXZ2.end());
   Pol_XZ = PXZ;

   slip::Vector<Type> bY(P.col_begin(3), P.col_end(3));//Y vector
   slip::Vector<Type> mYZ(19); 
   //solve AXY*mYZ=bY
   slip::least_square_ne_solve(AXY,mYZ,bY);
   
   slip::Vector<Type> mYZ2(20);
   std::copy(mYZ.begin(),mYZ.end(),mYZ2.begin());
   slip::MultivariatePolynomial<Type,3> PYZ(3,mYZ2.begin(),mYZ2.end());
   Pol_YZ = PYZ;
 }

 template <typename Type>
  void computes_SoloffXZ(const slip::Matrix<Type>& P,
			 slip::MultivariatePolynomial<Type,3>& Pol_XY,
			 slip::MultivariatePolynomial<Type,3>& Pol_ZY) 
 {

   slip::Matrix<Type> AXZ;
   slip::SoloffXZ_matrix(P,AXZ);
   slip::Vector<Type> bX(P.col_begin(2), P.col_end(2));//X vector
   slip::Vector<Type> mXY(19);
   //solve AXZ*mXY=bX
   slip::least_square_ne_solve(AXZ,mXY,bX);
   
   slip::Vector<Type> mXY2(20);
   std::copy(mXY.begin(),mXY.end(),mXY2.begin());
   slip::MultivariatePolynomial<Type,3> PXY(3,mXY2.begin(),mXY2.end());
   Pol_XY = PXY;

   slip::Vector<Type> bZ(P.col_begin(4), P.col_end(4));//Z vector
   slip::Vector<Type> mZY(19); 
   //solve AXZ*mZY=bZ
   slip::least_square_ne_solve(AXZ,mZY,bZ);
   
   
   slip::Vector<Type> mZY2(20);
   std::copy(mZY.begin(),mZY.end(),mZY2.begin());
   slip::MultivariatePolynomial<Type,3> PZY(3,mZY2.begin(),mZY2.end());
   Pol_ZY = PZY;
 }

template <typename Type>
  void computes_SoloffYZ(const slip::Matrix<Type>& P,
			 slip::MultivariatePolynomial<Type,3>& Pol_YX,
			 slip::MultivariatePolynomial<Type,3>& Pol_ZX) 
 {

   slip::Matrix<Type> AYZ;
   slip::SoloffYZ_matrix(P,AYZ);
   slip::Vector<Type> bY(P.col_begin(3), P.col_end(3));//Y vector
   slip::Vector<Type> mYX(19);
   //solve AYZ*mYX=bY
   slip::least_square_ne_solve(AYZ,mYX,bY);
   
   slip::Vector<Type> mYX2(20);
   std::copy(mYX.begin(),mYX.end(),mYX2.begin());
   slip::MultivariatePolynomial<Type,3> PYX(3,mYX2.begin(),mYX2.end());
   Pol_YX = PYX;

   slip::Vector<Type> bZ(P.col_begin(4), P.col_end(4));//Z vector
   slip::Vector<Type> mZX(19); 
   //solve AYZ*mZX=bZ
   slip::least_square_ne_solve(AYZ,mZX,bZ);
   
   slip::Vector<Type> mZX2(20);
   std::copy(mZX.begin(),mZX.end(),mZX2.begin());
   slip::MultivariatePolynomial<Type,3> PZX(3,mZX2.begin(),mZX2.end());
   Pol_ZX = PZX;
 }
  
  /*!
  	** \author Markus Jehle <jehle_markus@yahoo.de>
	** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
	** \author Gomit Guillaume <gomit.guillaume_AT_univ-poitiers.fr>
  	** \version 0.0.1
  	** \date 2008/07/18
	** \since 1.2.0
  	** \brief Get calibration parameters using a polynomial fit (computation "by hand")
  	** \param P Matrix containing the input data.
  	** \param Pol_x 1st MultivariatePolynomial containing the calibration parameters
  	** \param Pol_y 2nd MultivariatePolynomial containing the calibration parameters
	** \remarks Format:
	** \f$(x,y)\f$ are the image coordinates, \f$(X,Y,Z)\f$ are the world coordinates.
	** \f[
	** \begin{array}{ccccc}
	** \cdots & \cdots & \cdots & \cdots & \cdots \\
	** x_i & y_i & X_i & Y_i & Z_i  \\
	** \cdots & \cdots & \cdots & \cdots & \cdots \\
	** \end{array}
	** \f]
  	*/
 template <typename Type>
 void getpars_SoloffUV(const slip::Matrix<Type>& P,
		       slip::MultivariatePolynomial<Type,3>& Pol_x,
		       slip::MultivariatePolynomial<Type,3>& Pol_y) 
 {
   
   const std::size_t P_rows = P.rows();
   slip::Matrix<Type> A(P_rows,19);
   slip::Vector<Type> bx(P_rows,1);
   slip::Vector<Type> by(P_rows,1);
   
   for(size_t i = 0; i < P_rows; i++) 
     {
       A[i][0]=static_cast<Type>(1.0);
       A[i][1]=P[i][2];  		// X
       A[i][2]=P[i][2]*P[i][2];	// XX
       A[i][3]=A[i][2]*P[i][2];	// XXX
       A[i][4]=P[i][3];  		// Y
       A[i][5]=P[i][2]*P[i][3];	// XY
       A[i][6]=P[i][2]*A[i][5];	// XXY
       A[i][7]=P[i][3]*P[i][3];	// YY
       A[i][8]=P[i][2]*A[i][7];	// XYY
       A[i][9]=A[i][7]*P[i][3];	// YYY
       A[i][10]=P[i][4];	       // Z
       A[i][11]=P[i][2]*P[i][4];	// XZ
       A[i][12]=P[i][2]*A[i][11];	// XXZ
       A[i][13]=P[i][3]*P[i][4];	// YZ
       A[i][14]=P[i][2]*A[i][13];	// XYZ
       A[i][15]=P[i][3]*A[i][13];	// YYZ
       A[i][16]=P[i][4]*P[i][4];	// ZZ
       A[i][17]=P[i][2]*A[i][16];	// XZZ
       A[i][18]=P[i][3]*A[i][16];	// YZZ
       
       bx[i]=P[i][0];	 // x
       by[i]=P[i][1];	 // y
     }

   // calculate the pseudo-inverse
   slip::Matrix<Type> AT(A.dim2(),A.dim1());
   slip::transpose(A,AT);
   slip::Matrix<Type> ATA(A.dim2(),A.dim2());
   slip::matrix_matrix_multiplies(AT,A,ATA);
   slip::Matrix<Type> ATAm1(A.dim2(),A.dim2());
   slip::inverse(ATA,ATAm1);
   slip::Matrix<Type> ATAm1AT(A.dim2(),A.dim1());
   slip::matrix_matrix_multiplies(ATAm1,AT,ATAm1AT);
   
   // calculate parameters m
   slip::Vector<Type> mx(19);
   slip::Vector<Type> my(19);
   slip::matrix_vector_multiplies(ATAm1AT,bx,mx);
   slip::matrix_vector_multiplies(ATAm1AT,by,my);

   slip::Vector<Type> mx2(20);
   slip::Vector<Type> my2(20);
   for(int i=0; i<19; i++) 
     {
     mx2[i]=mx[i]; my2[i]=my[i];
     }
   
   slip::MultivariatePolynomial<Type,3> Px(3,mx2.begin(),mx2.end());
   slip::MultivariatePolynomial<Type,3> Py(3,my2.begin(),my2.end());
   Pol_x=Px; 
   Pol_y=Py;
 }



/*!
** \author Damien Calluaud <damien.calluaud_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2009/10/12
** \since 1.2.0
** \brief Get calibration parameters using a polynomial fit (computation "by hand")
** \param P Matrix containing the input data.
** \param Pol_x 1st MultivariatePolynomial containing the calibration parameters
** \param Pol_y 2nd MultivariatePolynomial containing the calibration parameters
** \remarks Format:
** \f$(x,y)\f$ are the image coordinates, \f$(X,Y,Z)\f$ are the world coordinates.
** \f[
** \begin{array}{ccccc}
** \cdots & \cdots & \cdots & \cdots & \cdots \\
** x_i & y_i & X_i & Y_i & Z_i  \\
** \cdots & \cdots & \cdots & \cdots & \cdots \\
** \end{array}
** \f]
*/
  template <typename Type>
  void getpars_SoloffXY(const slip::Matrix<Type>& P,
			slip::MultivariatePolynomial<Type,3>& Pol_x,
			slip::MultivariatePolynomial<Type,3>& Pol_y) 
  {
    
    const std::size_t P_rows = P.rows();
    slip::Matrix<Type> A(P_rows,19);
    slip::Vector<Type> bx(P_rows,1);
    slip::Vector<Type> by(P_rows,1);
    
    for(std::size_t i = 0; i < P_rows; ++i) 
      {
	A[i][0]=static_cast<Type>(1.0);
	A[i][1]=P[i][0];  		 // x
	A[i][2]=P[i][0]*P[i][0];	 // xx
	A[i][3]=P[i][0]*A[i][2];	// xxx
	A[i][4]=P[i][1];  		// Y
	A[i][5]=P[i][0]*P[i][1];	// xY
	A[i][6]=P[i][0]*A[i][5];	// xxY
	A[i][7]=P[i][1]*P[i][1];	// YY
	A[i][8]=P[i][0]*A[i][7];	// xYY
	A[i][9]=P[i][1]*A[i][7];	// YYY
	A[i][10]=P[i][4];		// Z
	A[i][11]=P[i][0]*P[i][4];	// xZ
	A[i][12]=P[i][0]*A[i][11];	// xxZ
	A[i][13]=P[i][1]*P[i][4];	// YZ
	A[i][14]=P[i][0]*A[i][13];	// xYZ
	A[i][15]=P[i][1]*A[i][13];	// YYZ
	A[i][16]=P[i][4]*P[i][4];	// ZZ
	A[i][17]=P[i][0]*A[i][16];	// xZZ
	A[i][18]=P[i][1]*A[i][16];	// YZZ
	
	bx[i]=P[i][2];		        // X
	by[i]=P[i][3];			// Y
      }

    // calculate the pseudo-inverse
    slip::Matrix<Type> AT(A.dim2(),A.dim1());
    slip::transpose(A,AT);
    slip::Matrix<Type> ATA(A.dim2(),A.dim2());
    slip::matrix_matrix_multiplies(AT,A,ATA);
    slip::Matrix<Type> ATAm1(A.dim2(),A.dim2());
    slip::inverse(ATA,ATAm1);
    slip::Matrix<Type> ATAm1AT(A.dim2(),A.dim1());
    slip::matrix_matrix_multiplies(ATAm1,AT,ATAm1AT);

    // calculate parameters m
    slip::Vector<Type> mx(19);
    slip::Vector<Type> my(19);
    slip::matrix_vector_multiplies(ATAm1AT,bx,mx);
    slip::matrix_vector_multiplies(ATAm1AT,by,my);
    
    slip::Vector<Type> mx2(20);
    slip::Vector<Type> my2(20);
    for(std::size_t i=0; i<19; ++i) 
      {
	mx2[i]=mx[i]; 
	my2[i]=my[i];
      }

    slip::MultivariatePolynomial<Type,3> Px(3,mx2.begin(),mx2.end());
    slip::MultivariatePolynomial<Type,3> Py(3,my2.begin(),my2.end());
    Pol_x=Px; 
    Pol_y=Py;
  }


	/*!
  	** \author Markus Jehle <jehle_markus@yahoo.de>
  	** \version 0.0.2
  	** \date 2012/07/04
	** \since 1.2.0
  	** \brief RQ-Decomposition 3x4-Matrix into internal and external parameters
  	** \param M 	3x4-Matrix
  	** \param K	3x3-Matrix containing the internal parameters
  	** \param R	3x3-Matrix containing the rotations
  	** \param c	Vector3d containing the camera centre
  	** \return 		0, if the input Matrix M was singular
  	** \todo 		Is it necessary to negate R at the end?
  	*/
	template <typename Type>
	int decompose_RQ(const slip::Matrix<Type>& M,
			 slip::Matrix<Type>& K, 
			 slip::Matrix<Type>& R, 
			 slip::Vector3d<Type>& c) 
	{
	 	  
	  slip::Matrix<Type> Mat(3,3); 
	  //
	  Type D1 = M[1][2]*M[2][3] - M[2][2]*M[1][3];
	  Type D2 = M[1][1]*M[2][3] - M[2][1]*M[1][3];
	  Type D3 = M[1][1]*M[2][2] - M[2][1]*M[1][2];
	  Type D4 = M[1][0]*M[2][3] - M[2][0]*M[1][3];
	  Type D5 = M[1][0]*M[2][1] - M[2][0]*M[1][1];
	  Type D6 = M[1][0]*M[2][2] - M[2][0]*M[1][2];
	  

	  c[0]=  M[0][1]*D1 - M[0][2]*D2 + M[0][3]*D3;
	  c[1]= -M[0][0]*D1 + M[0][2]*D4 - M[0][3]*D6;
	  c[2]=  M[0][0]*D2 - M[0][1]*D4 + M[0][3]*D5;
	  c[3]= -M[0][0]*D3 + M[0][1]*D6 - M[0][2]*D5;
	  c[0]=c[0]/c[3]; 
	  c[1]=c[1]/c[3]; 
	  c[2]=c[2]/c[3];
	  std::copy(M.row_begin(0),M.row_begin(0)+3,Mat.row_begin(0));
	  std::copy(M.row_begin(1),M.row_begin(1)+3,Mat.row_begin(1));
	  std::copy(M.row_begin(2),M.row_begin(2)+3,Mat.row_begin(2));
	  
	  int sing = slip::rq_decomp(Mat,K,R);
	  R=-R;
	  K/=K[2][2];
	  return sing;
	}
  

	/*!
  	** \author Markus Jehle <jehle_markus@yahoo.de>
  	** \version 0.0.1
  	** \date 2008/07/18
	** \since 1.2.0
  	** \brief Direct-Decomposition a 3x4-Matrix into internal and external parameters
  	** \param M 	3x4-Matrix
  	** \param K	3x3-Matrix containing the internal parameters
  	** \param R	3x3-Matrix containing the rotations
  	** \param c	Vector3d containing the camera centre
  	** \return 	1 (always)
  	*/

	template <typename Type>
	int decompose_direct(const slip::Matrix<Type>& M,
			     slip::Matrix<Type>& K, 
			     slip::Matrix<Type>& R, 
			     slip::Vector3d<Type>& c) 
	{

	  
	  
	  slip::Vector3d<Type> m1(M[0][0],M[0][1],M[0][2]);
	  slip::Vector3d<Type> m2(M[1][0],M[1][1],M[1][2]);
	  slip::Vector3d<Type> m3(M[2][0],M[2][1],M[2][2]);
	  
	  slip::Vector3d<Type> r3(m3);
	  
	  Type u0 = std::inner_product(m1.begin(),m1.end(),m3.begin(),
				       Type());
	  Type v0 = std::inner_product(m2.begin(),m2.end(),m3.begin(),
				       Type());
	  slip::Vector3d<Type>  tmp;
	  tmp = m1.product(m3);
	  Type alpha_u = -tmp.Euclidean_norm();
	  tmp = m2.product(m3);
	  Type alpha_v = tmp.Euclidean_norm();
	  Type zero = static_cast<Type>(0.0);
	  Type s = zero;
	  
	  slip::Vector3d<Type> r1;
	  slip::Vector3d<Type> r2;
	  
	  r1 = (m1-(u0*m3))/alpha_u;
	  r2 = (m2-(v0*m3))/alpha_v;
	  
	  slip::Vector3d<Type> t;
	  t[0] = (M[0][3]-u0*M[2][3])/alpha_u;
	  t[1] = (M[1][3]-v0*M[2][3])/alpha_v;
	  t[2] = M[2][3];
	  
	  R[0][0]=r1[0]; R[0][1]=r1[1]; R[0][2]=r1[2];
	  R[1][0]=r2[0]; R[1][1]=r2[1]; R[1][2]=r2[2];
	  R[2][0]=r3[0]; R[2][1]=r3[1]; R[2][2]=r3[2];
	
	  K[0][0]=alpha_u; K[0][1]=s;       K[0][2]=u0;
	  K[1][0]=zero;    K[1][1]=alpha_v; K[1][2]=v0;
	  K[2][0]=zero;    K[2][1]=zero;    K[2][2]=static_cast<Type>(1.0);
	  //PR(u0)PR(v0)PR(alpha_u)PR(alpha_v)
	  slip::Matrix<Type> Rinv(3,3);
	  slip::inverse(R,Rinv);
	  slip::matrix_vector_multiplies(Rinv,t,c);
	  c = -c;
	  
	  return 1;
	}


	/*!
  	** \author Markus Jehle <jehle_markus@yahoo.de>
	** \author Gomit Guillaume <gomit.guillaume_AT_univ-poitiers.fr>
  	** \version 0.0.2
  	** \date 2012/07/04
	** \since 1.2.0
  	** \brief Inverts distortion model using the Newton-method
  	** \param pd	slip::Point2d (distorted camera coordinates)
  	** \param p   Vector containing all camera parameters (size of 23)
  	** \return slip::Point2d (undistorted camera coordinates)
  	*/
	template <typename Type>
	slip::Point2d<Type> invert_distortion_model(const slip::Point2d<Type> &pd, 
						    const slip::Vector<Type> &p) {
	  
	  Type EPS = 1E-13;
	  
	  Type xd = pd[0];
	  Type yd = pd[1];
	  Type x = xd; 
	  Type y = yd;
	  
	  std::size_t ctr = 0; 
	  Type epsilon = static_cast<Type>(1);
	  Type one = static_cast<Type>(1);
	  Type two = static_cast<Type>(2);
	  Type three = static_cast<Type>(3);
	  Type six = static_cast<Type>(6);
	  bool done = 0;
	  while(!done) 
	    {
	      Type x_m_U0 = (x-p[12]);
	      Type y_m_V0 = (y-p[13]);
	      Type x_m_U02 = x_m_U0 * x_m_U0;
	      Type y_m_V02 = y_m_V0 * y_m_V0;
	      Type x_m_U0_y_m_V0 = x_m_U0 * y_m_V0;
	      Type r = x_m_U02 + y_m_V02;
	      Type r2 = r * r;
	      Type z1 = p[14]*r + p[15]*r2 + p[16]*r2*r;
	      Type z2 =  p[14] 
		       + two*p[15]*r 
		       + three*p[16]*r2;
	      Type two_z2 = two*z2;
	      Type two_d1 = two*p[17];
	      Type two_d2 = two*p[18];
	      Type one_p_z1 = one + z1;
	      //J matrix
	      Type J00 =   one_p_z1 
		    + two_z2*x_m_U02 
		    + two_d1*y_m_V0 
		    + (six*p[18]+two*p[19])*x_m_U0; 
	      Type J01 =   two_z2*x_m_U0_y_m_V0 
		    + two_d1*x_m_U0 
		    + (two*(p[18] + p[19]))*y_m_V0 ;
	      Type J10 =   two_z2*x_m_U0_y_m_V0 
		    + two_d2*y_m_V0 
		    + (two*(p[17] + p[20]))*x_m_U0;
	      Type J11 =   one_p_z1 
		    + two_z2*y_m_V02
		    + two_d2*x_m_U0 
		    + (six*p[17]+ two*p[20])*y_m_V0;
	      //J inverse matrix
	      Type det = J00*J11-J01*J10;
	      Type Jinv00 =  J11/det; 
	      Type Jinv01 = -J01/det;
	      Type Jinv10 = -J10/det; 
	      Type Jinv11 =  J00/det;
	      
	      Type f = x + (  x_m_U0*z1 
		         + two_d1*x_m_U0_y_m_V0 
		         + p[18]*(three*x_m_U02 + y_m_V02) 
		         + p[19]*r)-xd; 
	      Type g = y + (  y_m_V0*z1 
		         + two_d2*x_m_U0_y_m_V0 
		         + p[17]*(three*y_m_V02 + x_m_U02) 
		         + p[20]*r)-yd; 
	      
	      x -= (Jinv00*f + Jinv01*g);
	      y -= (Jinv10*f + Jinv11*g);
	      epsilon = std::sqrt(f*f+g*g);
	      
	      if (epsilon < EPS)
		{
		  done = 1;
		}
	      if (ctr > 10000)
		{
		  done = 1;
		}
	      ctr++;
	    }
	  return slip::Point2d<Type>(x,y);
	}
  





  
}  //slip::
#endif //SLIP_CAMERA_ALGO_HPP
