/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file linear_least_squares.hpp
 * 
 * \brief Provides some linear least square algorithms.
 * 
 */
#ifndef SLIP_LINEAR_LEAST_SQUARES_HPP
#define SLIP_LINEAR_LEAST_SQUARES_HPP

#include "Vector.hpp"
#include "Matrix.hpp"
#include "Array2d.hpp"
#include "linear_algebra.hpp"
#include "linear_algebra_svd.hpp"
#include "polynomial_algo.hpp"

namespace slip
{

  /** \name 1d linear least square algorithms */
  /* @{ */

  /**
   ** \brief Linear Least Square fitting using SVD.
   **
   ** Given a set of data points x =(x1,...,xN), y = (y1,..yN) 
   ** with individual standard deviations s = (s1,..sN)
   ** use chi2 minimization to determine the coefficients p = (p1,...,pM) of 
   ** the fitting function y = sum_j pj Fj(x). Here we solve the
   ** fitting equations using singular value decomposition (SVD) of
   ** the NxM matrix A such that aij = Fj(xi)/si. The program returns
   ** values for the fit parameters [p_first,p_last) and chi2. 
   ** The user supplies a functor EvalBasis that return a basis
   ** function evaluated at x (Fj(xi)).
   **
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/04/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x_first RandomAccessIterator to the x datas
   ** \param x_last RandomAccessIterator to the x datas
   ** \param y_first RandomAccessIterator to the y datas
   ** \param s_first RandomAccessIterator to the sigma datas
   ** \param p_first RandomAccessIterator to the estimated parameters
   ** \param p_last RandomAccessIterator to the estimated parameters
   ** \param basis_fun functor to the function which gives the values 
   **        obtained by the basis functions Fj. For example,
   **        slip::EvalPowerBasis gives (1,x,x^2,...,x^M) and is
   **        adapted to a polynomial fitting
   **
   ** \return the chi2 value.
   **
   ** \pre [x_first,x_last) is a valid range
   ** \pre [y_first,y_first + (x_last - x_last)) is a valid range
   ** \pre [s_first,s_first + (x_last - x_last)) is a valid range
   **
   ** \pre (p_last - p_first) == number of element of the basis
   ** function
   ** \par Example:
   ** \code
   **   double x[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0, 16.0 , 18.0, 5.0};
   ** double y[] ={7.0, 14.0, 11.0, 21.0, 15.0, 17.0, 31.0, 23.0, 25.0,33.0 , 37.0, 11.0};
   ** std::size_t data_size=12;
   ** std::size_t coeff_size=3;
   ** slip::Vector<double> datax(data_size,x);
   ** slip::Vector<double> datay(data_size,y);
   ** slip::Vector<double> datasig(data_size,1.0);
   ** slip::Vector<double> coeff(coeff_size,1.0);
   ** double chisq;
   ** slip::EvalPowerBasis<double,slip::Vector<double>::iterator> power_basis;
   ** chisq = 
   ** slip::svd_least_square(datax.begin(),
   **			     datax.end(),
   **			     datay.begin(),
   **			     datasig.begin(), 
   **			     coeff.begin(),
   **			     coeff.end(),
   ** 		             power_basis);
   ** std::cout<< coeff<<std::endl;
   ** std::cout<< chisq<<std::endl;
   ** \endcode
   */  
  template<typename RandomAccessIterator1, 
	   typename RandomAccessIterator2,
	   typename RandomAccessIterator3,
	   typename RandomAccessIterator4>
  inline
  double svd_least_square(RandomAccessIterator1 x_first,
			  RandomAccessIterator1 x_last,
			  RandomAccessIterator2 y_first,
			  RandomAccessIterator3 s_first,
			  RandomAccessIterator4 p_first,
			  RandomAccessIterator4 p_last,
			  slip::EvalBasis<typename std::iterator_traits<RandomAccessIterator1>::value_type,RandomAccessIterator2>& basis_fun)
{
  typedef  typename std::iterator_traits<RandomAccessIterator2>::difference_type  _Distance; 
  _Distance x_size = x_last - x_first;
  _Distance p_size = p_last - p_first;
  
  assert(x_first != x_last);
  assert(y_first != (y_first + x_size));
  assert(s_first != (s_first + x_size));
  assert(p_first != p_last);

  typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
  
  //Vector B such that bi = yi/si
  slip::Vector<value_type> B(x_size);
  //Fitting matrix A
  slip::Matrix<value_type> A(x_size,p_size);
  
  //slip::EvalPowerBasis<value_type,RandomAccessIterator1> basis_fun;
   //-----------------------------------------------
   // Computes coefficients of the fitting matrix A
   //-----------------------------------------------
  for (_Distance i = 0; i < x_size; ++i) 
    { 
      //computes Fj(x_first[i])
      basis_fun(x_first[i],(p_size - 1),A.row_begin(i),A.row_end(i));
      //divides the aij by si
      double tmp = value_type(1.0) / s_first[i];
      std::transform(A.row_begin(i),A.row_end(i),
		     A.row_begin(i),std::bind2nd(std::multiplies<value_type>(),tmp));
      //computes bi = yi/si
      B[i] = y_first[i] * tmp;
    }
  //-----------------------------------------------
  // svd solve
  //-----------------------------------------------
  slip::svd_solve(A,p_first,p_last,B.begin(),B.end());

  //-----------------------------------------------
  // Evaluate chi-square
  //-----------------------------------------------
  value_type chisq = value_type(0); 
  for (_Distance i = 0; i < x_size; ++i) 
    {
      value_type err = (B[i] - std::inner_product(p_first,p_last,A.row_begin(i),value_type(0))); 
      chisq +=  (err * err);
      
    }
  return double(chisq);
}

/**
   ** \brief Linear Least Square fitting using SVD.
   **
   ** Given a set of data points x =(x1,...,xN), y = (y1,..yN) 
   ** with individual standard deviations s = (s1,..sN)
   ** use chi2 minimization to determine the coefficients p = (p1,...,pM) of 
   ** the fitting function y = sum_j pj Fj(x). Here we solve the
   ** fitting equations using singular value decomposition (SVD) of
   ** the NxM matrix A such that aij = Fj(xi)/si. The program returns
   ** values for the fit parameters [p_first,p_last) and chi2. 
   ** The user supplies a functor EvalBasis that return a basis
   ** function evaluated at x (Fj(xi)).
   **
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/04/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param X Vector which contains the x datas.
   ** \param Y Vector which contains the y datas.
   ** \param S Vector which contains the standard deviation of the datas.
   ** \param P Vector which contains the parameters datas.
   ** \param basis_fun functor to the function which gives the values 
   **        obtained by the basis functions Fj. For example,
   **        slip::EvalPowerBasis gives (1,x,x^2,...,x^M) and is
   **        adapted to a polynomial fitting
   ** \return the chi2 value.
   **
   ** \pre  X.size() == Y.size()
   ** \pre  Y.size() == S.size()
   ** \pre  P.size() == number of element of the basis function
 ** \par Example:
   ** \code
   **   double x[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0, 16.0 , 18.0, 5.0};
   ** double y[] ={7.0, 14.0, 11.0, 21.0, 15.0, 17.0, 31.0, 23.0, 25.0,33.0 , 37.0, 11.0};
   ** std::size_t data_size=12;
   ** std::size_t coeff_size=3;
   ** slip::Vector<double> datax(data_size,x);
   ** slip::Vector<double> datay(data_size,y);
   ** slip::Vector<double> datasig(data_size,1.0);
   ** slip::Vector<double> coeff(coeff_size,1.0);
   ** double chisq;
   ** slip::EvalPowerBasis<double,slip::Vector<double>::iterator> power_basis;
   ** chisq = 
   ** slip::svd_least_square(datax,
   **		   datay,
   **		   datasig, 
   **		   coeff,
   **		   power_basis);
   ** std::cout<< coeff<<std::endl;
   ** std::cout<< chisq<<std::endl;
   ** \endcode
   */  
  template<typename Vector1, 
	   typename Vector2,
	   typename Vector3,
	   typename Vector4>
  inline
  double svd_least_square(Vector1& X,
			  Vector2& Y,
			  Vector3& S,
			  Vector4& P,
			  slip::EvalBasis<typename Vector1::value_type,typename Vector2::iterator>& basis_fun)
{
	return slip::svd_least_square<typename Vector1::iterator,
		                      typename Vector2::iterator,
		                      typename Vector3::iterator,
		                      typename Vector4::iterator>(X.begin(),X.end(),Y.begin(),S.begin(),P.begin(),P.end(),basis_fun);
}



/**
   ** \brief Linear Least Square fitting using SVD.
   **
   ** Given a set of data points x =(x1,...,xN), y = (y1,..yN) 
   ** use chi2 minimization to determine the coefficients p = (p1,...,pM) of 
   ** the fitting function y = sum_j pj Fj(x). Here we solve the
   ** fitting equations using singular value decomposition (SVD) of
   ** the NxM matrix A such that aij = Fj(xi)/si. The program returns
   ** values for the fit parameters [p_first,p_last) and chi2. 
   ** The user supplies a functor EvalBasis that return a basis
   ** function evaluated at x (Fj(xi)).
   **
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/04/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x_first RandomAccessIterator to the x datas
   ** \param x_last RandomAccessIterator to the x datas
   ** \param y_first RandomAccessIterator to the y datas
   ** \param p_first RandomAccessIterator to the estimated parameters
   ** \param p_last RandomAccessIterator to the estimated parameters
   ** \param basis_fun functor to the function which gives the values 
   **        obtained by the basis functions Fj. For example,
   **        slip::EvalPowerBasis gives (1,x,x^2,...,x^M) and is
   **        adapted to a polynomial fitting
   **
   ** \return the chi2 value.
   **
   ** \pre [x_first,x_last) is a valid range
   ** \pre [y_first,y_first + (x_last - x_last)) is a valid range
   **
   ** \pre (p_last - p_first) == number of element of the basis
   ** function
    ** \par Example:
   ** \code
   **   double x[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0, 16.0 , 18.0, 5.0};
   ** double y[] ={7.0, 14.0, 11.0, 21.0, 15.0, 17.0, 31.0, 23.0, 25.0,33.0 , 37.0, 11.0};
   ** std::size_t data_size=12;
   ** std::size_t coeff_size=3;
   ** slip::Vector<double> datax(data_size,x);
   ** slip::Vector<double> datay(data_size,y);
   ** slip::Vector<double> datasig(data_size,1.0);
   ** slip::Vector<double> coeff(coeff_size,1.0);
   ** double chisq;
   ** slip::EvalPowerBasis<double,slip::Vector<double>::iterator> power_basis;
   ** chisq = 
   **    slip::svd_least_square(datax.begin(),
   **		   datax.end(),
   **		   datay.begin(),
   **		   coeff.begin(),
   **		   coeff.end(),
   **		   power_basis);
   ** std::cout<< coeff<<std::endl;
   ** std::cout<< chisq<<std::endl;
   ** \endcode
   */  
   template<typename RandomAccessIterator1, 
	   typename RandomAccessIterator2,
	   typename RandomAccessIterator3>
  inline
  double svd_least_square(RandomAccessIterator1 x_first,
			  RandomAccessIterator1 x_last,
			  RandomAccessIterator2 y_first,
			  RandomAccessIterator3 p_first,
			  RandomAccessIterator3 p_last,
			  slip::EvalBasis<typename std::iterator_traits<RandomAccessIterator1>::value_type,RandomAccessIterator2>& basis_fun)
{
  typedef  typename std::iterator_traits<RandomAccessIterator2>::difference_type  _Distance; 
  _Distance x_size = x_last - x_first;
  _Distance p_size = p_last - p_first;
  
  assert(x_first != x_last);
  assert(y_first != (y_first + x_size));
  assert(p_first != p_last);

  typedef typename std::iterator_traits<RandomAccessIterator3>::value_type value_type;
  
  //Fitting matrix A
  slip::Matrix<value_type> A(x_size,p_size);
  
  //-----------------------------------------------
   // Computes coefficients of the fitting matrix A
   //-----------------------------------------------
  for (_Distance i = 0; i < x_size; ++i) 
    { 
      //computes Fj(x_first[i])
      basis_fun(x_first[i],(p_size - 1),A.row_begin(i),A.row_end(i));
    }

  //-----------------------------------------------
  // svd solve
  //-----------------------------------------------
  slip::svd_solve(A,p_first,p_last,y_first,y_first+x_size);

  //-----------------------------------------------
  // Evaluate chi-square
  //-----------------------------------------------
  value_type chisq = value_type(0); 
  for (_Distance i = 0; i < x_size; ++i) 
    {
      value_type err = (y_first[i] - std::inner_product(p_first,p_last,A.row_begin(i),value_type(0))); 
      chisq +=  (err * err);
      
    }
  return double(chisq);
}


/**
   ** \brief Linear Least Square fitting using SVD.
   **
   ** Given a set of data points x =(x1,...,xN), y = (y1,..yN) 
   ** use chi2 minimization to determine the coefficients p = (p1,...,pM) of 
   ** the fitting function y = sum_j pj Fj(x). Here we solve the
   ** fitting equations using singular value decomposition (SVD) of
   ** the NxM matrix A such that aij = Fj(xi)/si. The program returns
   ** values for the fit parameters [p_first,p_last) and chi2. 
   ** The user supplies a functor EvalBasis that return a basis
   ** function evaluated at x (Fj(xi)).
   **
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/04/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param X Vector which contains the x datas.
   ** \param Y Vector which contains the y datas.
   ** \param P Vector which contains the parameters datas.
   ** \param basis_fun functor to the function which gives the values 
   **        obtained by the basis functions Fj. For example,
   **        slip::EvalPowerBasis gives (1,x,x^2,...,x^M) and is
   **        adapted to a polynomial fitting
   ** \return the chi2 value.
   **
   ** \pre  X.size() == Y.size()
   ** \pre  P.size() == number of element of the basis function
 ** \par Example:
   ** \code
   **   double x[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0, 16.0 , 18.0, 5.0};
   ** double y[] ={7.0, 14.0, 11.0, 21.0, 15.0, 17.0, 31.0, 23.0, 25.0,33.0 , 37.0, 11.0};
   ** std::size_t data_size=12;
   ** std::size_t coeff_size=3;
   ** slip::Vector<double> datax(data_size,x);
   ** slip::Vector<double> datay(data_size,y);
   ** slip::Vector<double> datasig(data_size,1.0);
   ** slip::Vector<double> coeff(coeff_size,1.0);
   ** double chisq;
   ** slip::EvalPowerBasis<double,slip::Vector<double>::iterator> power_basis;
   ** chisq = 
   ** slip::svd_least_square(datax,
   **		   datay,
   **		   coeff,
   **		   power_basis);
   ** std::cout<< coeff<<std::endl;
   ** std::cout<< chisq<<std::endl;
   ** \endcode
   */  
  template<typename Vector1, 
	   typename Vector2,
	   typename Vector3>
  inline
  double svd_least_square(Vector1& X,
			  Vector2& Y,
			  Vector3& P,
			  slip::EvalBasis<typename Vector1::value_type,typename Vector2::iterator>& basis_fun)
{
	return slip::svd_least_square<typename Vector1::iterator,
		                      typename Vector2::iterator,
		                      typename Vector3::iterator>(X.begin(),X.end(),Y.begin(),P.begin(),P.end(),basis_fun);
}
/* @} */

  /** \name Multidimensional linear least square algorithms */
  /* @{ */

/**
   ** \brief nd Linear Least Square fitting using SVD.
   **
   ** Given a set of data points X =(X1,...,XN) of dimension N, y = (y1,..yN) 
   ** with individual standard deviations s = (s1,..sN)
   ** use chi2 minimization to determine the coefficients p = (p1,...,pM) of 
   ** the fitting function y = sum_j pj Fj(X). Here we solve the
   ** fitting equations using singular value decomposition (SVD) of
   ** the NxM matrix A such that aij = Fj(Xi)/si. The program returns
   ** values for the fit parameters [p_first,p_last) and chi2. 
   ** The user supplies a functor EvalBasis that return a basis
   ** function evaluated at x (Fj(Xi)).
   **
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/04/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x_first RandomAccessIterator to the x datas
   ** \param x_last RandomAccessIterator to the x datas
   ** \param y_first RandomAccessIterator to the y datas
   ** \param s_first RandomAccessIterator to the sigma datas
   ** \param p_first RandomAccessIterator to the estimated parameters
   ** \param p_last RandomAccessIterator to the estimated parameters
   ** \param order Order of the process.
   ** \param basis_fun functor to the function which gives the values 
   **        obtained by the basis functions Fj. For example,
   **        slip::EvalPower2dBasis gives (1,x1,x1^2,x2,x1x2,x2^2...) and is
   **        adapted to a 2d-polynomial fitting
   **
   ** \return the chi2 value.
   **
   ** \pre [x_first,x_last) is a valid range
   ** \pre [y_first,y_first + (x_last - x_last)) is a valid range
   ** \pre [s_first,s_first + (x_last - x_last)) is a valid range
   **
   ** \pre (p_last - p_first) == number of element of the basis
   ** function
   ** \par Example:
   ** \code
   ** //construction of the 2d-polynomial data
   ** std::size_t n = 32;
   ** slip::Vector<double> x2(n+1);
   ** slip::iota(x2.begin(),x2.end(),-1.0,1.0/double(n));
   ** slip::Vector<double> y2(x2);
   ** slip::Vector<slip::Vector2d<double> > datax2(x2.size()*x2.size());  
   ** std::size_t k = 0;
   ** for(std::size_t i = 0; i < y2.size(); ++i)
   **  {
   **    for(std::size_t j = 0; j < x2.size(); ++j)
   **	 {
   **	   datax2[k][0] = x2[i];
   **	   datax2[k][1] = y2[j];
   **	   k++;
   **	 }
   **  }
   ** slip::Vector<double> datay2(datax2.size(),0.0);
   ** 
   ** for(std::size_t k = 0; k < datax2.size(); ++k)
   **  {
   **    double x = datax2[k][0];
   **    double y = datax2[k][1];
   **    datay2[k] =  1.2 + 2.0 * x + 0.5* x*x - 1.0 * y + 0.8 * x *
   **     y + 0.3 * y * y;
   **  }
   **
   ** //polynomial fitting
   ** slip::Vector<double> datasig2(datax2.size(),1.0);
   ** std::size_t coeff_size2 = 6;
   ** slip::Vector<double> coeff2(coeff_size2,1.0);
   ** 
   ** slip::EvalPower2dBasis<slip::Vector2d<double>,slip::Vector<double>::iterator> power2d_basis;
   ** slip::Vector<double> power_2d(6);
   ** power2d_basis(datax2[3],2,power_2d.begin(),power_2d.end());
   ** std::cout<<power_2d<<std::endl;
   ** chisq = 
   ** slip::svd_least_square_nd(datax2.begin(),
   **			        datax2.end(),
   **			        datay2.begin(),
   **		                datasig2.begin(),
   **		                coeff2.begin(),
   **		                coeff2.end(),
   **		                slip::Vector2d<double>::SIZE,
   **		                power2d_basis);
   ** std::cout<< coeff2<<std::endl;
   ** std::cout<< chisq<<std::endl;
   ** \endcode
   */  
  template<typename RandomAccessIterator1, 
	   typename RandomAccessIterator2,
	   typename RandomAccessIterator3,
	   typename RandomAccessIterator4>
  inline
  double svd_least_square_nd(RandomAccessIterator1 x_first,
			     RandomAccessIterator1 x_last,
			     RandomAccessIterator2 y_first,
			     RandomAccessIterator3 s_first,
			     RandomAccessIterator4 p_first,
			     RandomAccessIterator4 p_last,
			     const std::size_t order,
			     slip::EvalBasis<typename std::iterator_traits<RandomAccessIterator1>::value_type,RandomAccessIterator2>& basis_fun)
{
  typedef  typename std::iterator_traits<RandomAccessIterator2>::difference_type  _Distance; 
  _Distance x_size = x_last - x_first;
  _Distance p_size = p_last - p_first;
  
  assert(x_first != x_last);
  assert(y_first != (y_first + x_size));
  assert(s_first != (s_first + x_size));
  assert(p_first != p_last);

  typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
  
  //Vector B such that bi = yi/si
  slip::Vector<value_type> B(x_size);
  //Fitting matrix A
  slip::Matrix<value_type> A(x_size,p_size);
  
  //-----------------------------------------------
  // Computes coefficients of the fitting matrix A
  //-----------------------------------------------
  for (_Distance i = 0; i < x_size; ++i) 
    { 
      //computes Fj(x_first[i])
     
      basis_fun(x_first[i],order,A.row_begin(i),A.row_end(i));
     
      //divides the aij by si
      double tmp = value_type(1.0) / s_first[i];
      std::transform(A.row_begin(i),A.row_end(i),
		     A.row_begin(i),std::bind2nd(std::multiplies<value_type>(),tmp));
      //computes bi = yi/si
      B[i] = y_first[i] * tmp;
    }
  
  //-----------------------------------------------
  // svd solve
  //-----------------------------------------------
  slip::svd_solve(A,p_first,p_last,B.begin(),B.end());

  //-----------------------------------------------
  // Evaluate chi-square
  //-----------------------------------------------
  value_type chisq = value_type(0); 
  for (_Distance i = 0; i < x_size; ++i) 
    {
      value_type err = (B[i] - std::inner_product(p_first,p_last,A.row_begin(i),value_type(0))); 
      chisq +=  (err * err);
      
    }
  return double(chisq);
}


/**
   ** \brief nd Linear Least Square fitting using SVD.
   **
   ** Given a set of N points of dimension dim X =(X1,...,XN), y = (y1,..yN) 
   ** use chi2 minimization to determine the coefficients p = (p1,...,pM) of 
   ** the fitting function y = sum_j pj Fj(X). Here we solve the
   ** fitting equations using singular value decomposition (SVD) of
   ** the NxM matrix A such that aij = Fj(Xi). The program returns
   ** values for the fit parameters [p_first,p_last) and chi2. 
   ** The user supplies a functor EvalBasis that return a basis
   ** function evaluated at X (Fj(Xi)).
   **
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/04/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x_first RandomAccessIterator to the x datas
   ** \param x_last RandomAccessIterator to the x datas
   ** \param y_first RandomAccessIterator to the y datas
   ** \param p_first RandomAccessIterator to the estimated parameters
   ** \param p_last RandomAccessIterator to the estimated parameters
   ** \param order Order of the process.
   ** \param basis_fun functor to the function which gives the values 
   **        obtained by the basis functions Fj. For example,
   **        slip::EvalPower2dBasis gives (1,x1,x1^2,x2,x1x2,x2^2...) and is
   **        adapted to a 2d-polynomial fitting
   **
   ** \return the chi2 value.
   **
   ** \pre [x_first,x_last) is a valid range
   ** \pre [y_first,y_first + (x_last - x_last)) is a valid range
   **
   ** \pre (p_last - p_first) == number of element of the basis
   ** function
 ** \par Example:
   ** \code
   ** //construction of the 2d-polynomial data
   ** std::size_t n = 32;
   ** slip::Vector<double> x2(n+1);
   ** slip::iota(x2.begin(),x2.end(),-1.0,1.0/double(n));
   ** slip::Vector<double> y2(x2);
   ** slip::Vector<slip::Vector2d<double> > datax2(x2.size()*x2.size());  
   ** std::size_t k = 0;
   ** for(std::size_t i = 0; i < y2.size(); ++i)
   **  {
   **    for(std::size_t j = 0; j < x2.size(); ++j)
   **	 {
   **	   datax2[k][0] = x2[i];
   **	   datax2[k][1] = y2[j];
   **	   k++;
   **	 }
   **  }
   ** slip::Vector<double> datay2(datax2.size(),0.0);
   ** 
   ** for(std::size_t k = 0; k < datax2.size(); ++k)
   **  {
   **    double x = datax2[k][0];
   **    double y = datax2[k][1];
   **    datay2[k] =  1.2 + 2.0 * x + 0.5* x*x - 1.0 * y + 0.8 * x *
   **     y + 0.3 * y * y;
   **  }
   **
   ** //polynomial fitting
   ** slip::Vector<double> datasig2(datax2.size(),1.0);
   ** std::size_t coeff_size2 = 6;
   ** slip::Vector<double> coeff2(coeff_size2,1.0);
   ** 
   ** slip::EvalPower2dBasis<slip::Vector2d<double>,slip::Vector<double>::iterator> power2d_basis;
   ** slip::Vector<double> power_2d(6);
   ** power2d_basis(datax2[3],2,power_2d.begin(),power_2d.end());
   ** std::cout<<power_2d<<std::endl;
   ** chisq = 
   ** slip::svd_least_square_nd(datax2.begin(),
   **			        datax2.end(),
   **			        datay2.begin(),
   **		                coeff2.begin(),
   **		                coeff2.end(),
   **		                slip::Vector2d<double>::SIZE,
   **		                power2d_basis);
   ** std::cout<< coeff2<<std::endl;
   ** std::cout<< chisq<<std::endl;
   ** \endcode
   */  
  template<typename RandomAccessIterator1, 
	   typename RandomAccessIterator2,
	   typename RandomAccessIterator3>
  inline
  double svd_least_square_nd(RandomAccessIterator1 x_first,
			     RandomAccessIterator1 x_last,
			     RandomAccessIterator2 y_first,
			     RandomAccessIterator3 p_first,
			     RandomAccessIterator3 p_last,
			     const std::size_t order,
			     slip::EvalBasis<typename std::iterator_traits<RandomAccessIterator1>::value_type,RandomAccessIterator2>& basis_fun)
{
  typedef  typename std::iterator_traits<RandomAccessIterator2>::difference_type  _Distance; 
  _Distance x_size = x_last - x_first;
  _Distance p_size = p_last - p_first;
  
  assert(x_first != x_last);
  assert(y_first != (y_first + x_size));
  assert(p_first != p_last);

  typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
  
  //Fitting matrix A
  slip::Matrix<value_type> A(x_size,p_size);
  
  //-----------------------------------------------
  // Computes coefficients of the fitting matrix A
  //-----------------------------------------------
  for (_Distance i = 0; i < x_size; ++i) 
    { 
      //computes Fj(x_first[i])
     
      basis_fun(x_first[i],order,A.row_begin(i),A.row_end(i));
    }
 
  //-----------------------------------------------
  // svd solve
  //-----------------------------------------------
   slip::svd_solve(A,p_first,p_last,y_first,y_first+x_size);


  //-----------------------------------------------
  // Evaluate chi-square
  //-----------------------------------------------
  value_type chisq = value_type(0); 
  for (_Distance i = 0; i < x_size; ++i) 
    {
      value_type err = (y_first[i] - std::inner_product(p_first,p_last,A.row_begin(i),value_type(0))); 
      chisq +=  (err * err);
      
    }
  return double(chisq);
}

  /**
   ** \brief nd Linear Least Square fitting using SVD.
   **
   ** Given a set of data points of N dimension X =(X1,...,XN), y = (y1,..yN) 
   ** use chi2 minimization to determine the coefficients p = (p1,...,pM) of 
   ** the fitting function y = sum_j pj Fj(X). Here we solve the
   ** fitting equations using singular value decomposition (SVD) of
   ** the NxM matrix A such that aij = Fj(Xi). The program returns
   ** values for the fit parameters [p_first,p_last) and chi2. 
   ** The user supplies a functor EvalBasis that return a basis
   ** function evaluated at X (Fj(Xi)).
   **
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/04/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param X Vector which contains the x datas.
   ** \param Y Vector which contains the y datas.
   ** \param P Vector which contains the parameters datas.
   ** \param order Order of the approximation.
   ** \param basis_fun functor to the function which gives the values 
   **        obtained by the basis functions Fj. For example,
   **        slip::EvalPower2dBasis gives (1,x1,x1^2,x2,x1x2,x2^2...) and is
   **        adapted to a 2d polynomial fitting
   ** \return the chi2 value.
   **
   ** \pre  X.size() == Y.size()
   ** \pre  P.size() == number of element of the basis function
    ** \par Example:
   ** \code
   ** //construction of the 2d-polynomial data
   ** std::size_t n = 32;
   ** slip::Vector<double> x2(n+1);
   ** slip::iota(x2.begin(),x2.end(),-1.0,1.0/double(n));
   ** slip::Vector<double> y2(x2);
   ** slip::Vector<slip::Vector2d<double> > datax2(x2.size()*x2.size());  
   ** std::size_t k = 0;
   ** for(std::size_t i = 0; i < y2.size(); ++i)
   **  {
   **    for(std::size_t j = 0; j < x2.size(); ++j)
   **	 {
   **	   datax2[k][0] = x2[i];
   **	   datax2[k][1] = y2[j];
   **	   k++;
   **	 }
   **  }
   ** slip::Vector<double> datay2(datax2.size(),0.0);
   ** 
   ** for(std::size_t k = 0; k < datax2.size(); ++k)
   **  {
   **    double x = datax2[k][0];
   **    double y = datax2[k][1];
   **    datay2[k] =  1.2 + 2.0 * x + 0.5* x*x - 1.0 * y + 0.8 * x *
   **     y + 0.3 * y * y;
   **  }
   **
   ** //polynomial fitting
   ** slip::Vector<double> datasig2(datax2.size(),1.0);
   ** std::size_t coeff_size2 = 6;
   ** slip::Vector<double> coeff2(coeff_size2,1.0);
   ** 
   ** slip::EvalPower2dBasis<slip::Vector2d<double>,slip::Vector<double>::iterator> power2d_basis;
   ** slip::Vector<double> power_2d(6);
   ** power2d_basis(datax2[3],2,power_2d.begin(),power_2d.end());
   ** std::cout<<power_2d<<std::endl;
   ** chisq = 
   ** slip::svd_least_square_nd(datax2,
   **		                datay2,
   **		                coeff2,
   **		                slip::Vector2d<double>::SIZE,
   **		                power2d_basis);
   ** std::cout<< coeff2<<std::endl;
   ** std::cout<< chisq<<std::endl;
   ** \endcode
   */  
  template<typename Vector1, 
	   typename Vector2,
	   typename Vector3>
  inline
  double svd_least_square_nd(Vector1& X,
			  Vector2& Y,
			  Vector3& P,
			  const std::size_t order,
			  slip::EvalBasis<typename Vector1::value_type,typename Vector2::iterator>& basis_fun)
{
	return slip::svd_least_square_nd<typename Vector1::iterator,
		                      typename Vector2::iterator,
	  typename Vector3::iterator>(X.begin(),X.end(),Y.begin(),P.begin(),P.end(),order,basis_fun);
}
/* @} */

 /**
   ** \brief Solve Linear Equation \f$||\mathbf{A}x - b||^2\f$ using normal equation and choleski decomposition :
   ** \f[ \mathbf{A}^H\mathbf{A}x=\mathbf{A}^Hb\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2023/11/20
   ** \since 1.5.0
   ** \param A matrix A.
   ** \param x vector result vector.
   ** \param b vector to match.
   ** \pre A.rows() >= A.cols()
   ** \pre x.size() == A.cols()
   ** \pre b.size() == A.rows()
   ** \par Example:
   ** \code
   **  typedef double T;
   ** slip::Matrix<T> M(3,3);
   ** M[0][0] = 12.0; M[0][1] = -51.0; M[0][2] = 4.0;
   ** M[1][0] =  6.0; M[1][1] = 167.0; M[1][2] = -68.0;
   ** M[2][0] = -4.0; M[2][1] = 24.0; M[2][2] = -41.0;
   ** std::cout<<"M = \n"<<M<<std::endl;
   ** slip::Vector<T> b(3);
   ** b[0] = 1.0; b[1] = 2.0; b[2] = 3.0;
   ** std::cout<<"b = \n"<<b<<std::endl;
   ** std::cout<<"Normal equation solve"<<std::endl;
   ** slip::least_square_ne_solve(M,x,b);
   ** std::cout<<"x = \n"<<x<<std::endl;
   ** \endcode
   */
  template<typename Matrix1,
	   typename Vector1>
  void least_square_ne_solve(const Matrix1& A,
			     Vector1& x,
			     const Vector1& b)
  {
    assert(A.rows() >= A.cols());
    assert(x.size() == A.cols());
    assert(b.size() == A.rows());
    const std::size_t rows = A.rows();
    const std::size_t cols = A.cols();
    
    //A^HA
    Matrix1 ATA(cols,cols);
    slip::hmatrix_matrix_multiplies(A,A,ATA);
    //A^Hb
    Vector1 ATb(b.size());
    slip::hmatrix_vector_multiplies(A,b,ATb);
    slip::cholesky_solve(ATA,x,ATb);
  }

 //solve M2 = AM ie A = M2*pinv(M)
  template <typename Matrix>
  void pseudo_inverse(const Matrix& M,
		      const Matrix& M2,
		      Matrix& A)
{
  typedef typename Matrix::value_type Type;
  const std::size_t M_rows = M.rows();
  const std::size_t M_cols = M.cols();
  A.resize(M2.rows(),M_rows);
  Matrix MMT(M_rows,M_rows);
  slip::matrix_hmatrix_multiplies(M,M,MMT);
  Matrix MMTm1(M_rows,M_rows);
  slip::inverse(MMT,MMTm1);
  Matrix MT_MMTm1(M_cols,M_rows);
  for(std::size_t i = 0; i < M_cols; ++i)
    {
      for(std::size_t j = 0; j < M_rows; ++j)
	  {
	    MT_MMTm1[i][j] = std::inner_product(M.col_begin(i),
						M.col_end(i),
						MMTm1.col_begin(j),
						Type());
	  }
      }
  slip::matrix_matrix_multiplies(M2.upper_left(),M2.bottom_right(),
				 MT_MMTm1.upper_left(), MT_MMTm1.bottom_right(),
				 A.upper_left(),A.bottom_right());

}


}//slip::


#endif //SLIP_LINEAR_LEAST_SQUARES_HPP
