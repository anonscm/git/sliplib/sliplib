/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file compare.hpp
 * 
 * \brief Provides some algorithms to compare ranges.
 * 
 */

#ifndef SLIP_COMPARE_HPP
#define SLIP_COMPARE_HPP


#include <algorithm>
#include <numeric>
#include <cassert>
#include <cmath>
#include <functional>
#include "Array.hpp"
#include "Array2d.hpp"
#include "Vector.hpp"
#include "stl_algo_ext.hpp"
#include "statistics.hpp"
#include "norms.hpp"

namespace slip
{

  

  /** \name Generic distance algorithms */
  /* @{ */


   /**
   ** \brief Computes the sum of the pointwise distances of two 
   ** ranges: \f$ \sum_i d(s_i, b_i)\f$ according to the distance d
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \param distance A binary functor.
   ** \return  square Euclidian distance between two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable
   ** \par Example1:
   ** \code
   ** //create 2 containers
   ** slip::Matrix<float> Md1(2,3);
   ** slip::Matrix<float> Md2(2,3);
   ** slip::iota(Md1.begin(),Md1.end(),1.0f,1.0f);
   ** slip::iota(Md2.begin(),Md2.end(),6.0f,-1.0f);
   ** //print the containers
   ** std::cout<<"Md1 = \n"<<Md1<<std::endl;
   ** std::cout<<"Md2 = \n"<<Md2<<std::endl;
   ** //compute the L1 distance between this 2 containers
   ** std::cout<<"L1_distance(Md1,Md2) = \n";
   ** std::cout<<slip::distance<float>(Md1.begin(),Md1.end(),
   **                                  Md2.begin(),
   **                                  slip::L1_dist<float,float>())<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** //create 2 vector fields
   ** slip::DenseVector2dField2d<float> VF3(4,3);
   ** slip::iota(VF3.begin(),VF3.end(),slip::Vector2d<float>(1.0,1.0),slip::Vector2d<float>(1.0,1.0));
   ** slip::DenseVector2dField2d<float> VF4(4,3);
   ** slip::iota(VF4.begin(),VF4.end(),slip::Vector2d<float>(1.0,0.5),slip::Vector2d<float>(0.5,1.0));
   ** //print the 2 vector fields
   ** std::cout<<"VF3 = \n"<<VF3<<std::endl;
   ** std::cout<<"VF4 = \n"<<VF4<<std::endl;
   ** //computes the L2 distance between the 2 vector fields
   ** float ee = slip::distance<float>(VF3.begin(),VF3.end(),VF4.begin(),slip::L2_dist_vect<slip::Vector2d<float> >());
   ** \endcode
   */
  template<typename Real, 
	   typename InputIterator1, 
	   typename InputIterator2,
	   typename BinaryFunctor>
  inline
  Real distance(InputIterator1 first1,
		InputIterator1 last1,
		InputIterator2 first2,
		BinaryFunctor distance)
  {
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      {
        init += distance(*first1,*first2);
      }
    return init;
  }

   /**
   ** \brief Computes the sum of the pointwise distances of two 
   ** ranges: \f$ \sum_i d(s_i, b_i)\f$ according to the distance d 
   ** and the given mask range
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \param distance A binary functor.
   ** \param  mask_first An input iterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  the sum of the distances. 
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** //create two containers and a mask
   ** slip::Matrix<float> Md1(2,3);
   ** slip::Matrix<float> Md2(2,3);
   ** slip::iota(Md1.begin(),Md1.end(),1.0f,1.0f);
   ** slip::iota(Md2.begin(),Md2.end(),6.0f,-1.0f);
   ** int maskd[] = {2,0,0,2,0,2};
   ** slip::Matrix<int> Maskd(2,3,maskd);
   ** //print the containers and the mask
   ** std::cout<<"Md1 = \n"<<Md1<<std::endl;
   ** std::cout<<"Md2 = \n"<<Md2<<std::endl; 
   ** std::cout<<"Maskd = \n"<<Maskd<<std::endl;
   ** std::cout<<"L2_distance(Md1,Md2) = "<<std::endl;
   ** //computes the L2 distance between the 2 containers
   ** std::cout<<slip::distance_mask<float>(Md1.begin(),Md1.end(),
   **                                       Maskd.begin(),
   **                                       Md2.begin(),
   **                                       slip::L2_dist<float,float>(),2)<<std::endl;

   ** \endcode
   */
template<typename Real, 
	 typename InputIterator1, 
	 typename InputIterator2,
	 typename BinaryFunctor,
	 typename MaskIterator>
  inline
  Real distance_mask(InputIterator1 first1,
		     InputIterator1 last1,
		     MaskIterator mask_first,
		     InputIterator2 first2,
		     BinaryFunctor distance,
		     typename std::iterator_traits<MaskIterator>::value_type value=typename  std::iterator_traits<MaskIterator>::value_type(1)
)
  {
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2,++mask_first)
      {
         if(*mask_first == value)
           {
	     init += distance(*first1,*first2);
           }
      }
    return init;
  }


  /**  \brief Computes the sum of the pointwise distances of two 
   ** ranges: \f$ \sum_i d(s_i, b_i)\f$ according to the distance d 
   ** and the given predicate.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  first2     An input iterator.
   ** \param distance A binary functor.
   **  \param  pred       A predicate.
   **  \return the sum of the distances.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   ** //define a predicate
   ** template<typename T>
   ** bool lt2Predicate (const T& val)
   ** {
   **  return (val <= T(2));
   ** }
   ** \endcode
   ** \code
   ** //create 2 containers 
   ** slip::Matrix<float> Md1(2,3);
   ** slip::Matrix<float> Md2(2,3);
   ** slip::iota(Md1.begin(),Md1.end(),1.0f,1.0f);
   ** slip::iota(Md2.begin(),Md2.end(),6.0f,-1.0f);
   ** //print the containers
   ** std::cout<<"Md1 = \n"<<Md1<<std::endl;
   ** std::cout<<"Md2 = \n"<<Md2<<std::endl; 
   ** //computes the L2 distance between the 2 containers
   ** std::cout<<"L2_distance(Md1,Md2) = "<<std::endl;
   ** std::cout<<slip::distance_if<float>(Md1.begin(),Md1.end(),
   **                                     Md2.begin(),
   **                                     slip::L2_dist<float,float>(),
   **                                     lt2Predicate<float>)<<std::endl;
   ** \endcode
   */
template<typename Real,
	 typename InputIterator1, 
	 typename InputIterator2,
	 typename BinaryFunctor,
	 typename Predicate>

 Real distance_if(InputIterator1 first1, InputIterator1 last1,
		  InputIterator2 first2, 
		  BinaryFunctor distance,
		  Predicate pred)
 {
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      {
         if(pred(*first1))
             { 
               init += distance(*first1,*first2);
             }
      }
  return init;
 }

    /* @} */

  /** \name Generic distance map algorithms */
  /* @{ */
   /**
   ** \brief Computes the pointwise distances of two 
   ** ranges: \f$ d(s_i, b_i)\f$ 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \param result An InputIterator.
   ** \param distance A binary functor.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [result,result + (last1 -  first1)) is a valid range.
   ** \pre result is not an iterator within the range ]first1,last1) or
   **      ]first2,first2 + (last1 -  first1)).
   ** \par Example:
   ** \code
   ** //create 2 input containers and the map container
   ** slip::Matrix<float> Md1(2,3);
   ** slip::Matrix<float> Md2(2,3);
   ** slip::Matrix<float> Resultd(2,3);
   ** slip::iota(Md1.begin(),Md1.end(),1.0f,1.0f);
   ** slip::iota(Md2.begin(),Md2.end(),6.0f,-1.0f);
   ** //print the containers
   ** std::cout<<"Md1 = \n"<<Md1<<std::endl;
   ** std::cout<<"Md2 = \n"<<Md2<<std::endl; 
   ** //computes the L1 distance map
   ** slip::distance(Md1.begin(),Md1.end(),
   **                Md2.begin(),
   **                Resultd.begin(),
   **                slip::L1_dist<float,float>());
   ** std::cout<<"Resultd L1 = \n"<<Resultd<<std::endl; 
   ** \endcode
   */
  template<typename InputIterator1, 
	   typename InputIterator2,
	   typename OutputIterator,
	   typename BinaryFunctor>
  inline
  void distance(InputIterator1 first1,
		InputIterator1 last1,
		InputIterator2 first2,
		OutputIterator result,
		BinaryFunctor distance)
  {
    assert(first1 != last1);
    std::transform(first1,last1,first2,result,distance);
  }

   /**
   ** \brief Computes the pointwise distances of two 
   ** ranges: \f$ d(s_i, b_i)\f$  according to a given mask range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param mask_first  An InputIterator.
   ** \param first2 An InputIterator.
   ** \param result An InputIterator.
   ** \param distance A binary functor.
   ** \param  value true value of the mask range. Default is 1.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \pre [result,result + (last1 -  first1)) is a valid range.
   ** \pre result is not an iterator within the range ]first1,last1) or
   **      ]first2,first2 + (last1 -  first1)).
   ** \par Example:
   ** \code
   ** //create two containers, a mask and the map container
   ** slip::Matrix<float> Md1(2,3);
   ** slip::Matrix<float> Md2(2,3);
   ** slip::Matrix<float> Resultd(2,3);
   ** slip::iota(Md1.begin(),Md1.end(),1.0f,1.0f);
   ** slip::iota(Md2.begin(),Md2.end(),6.0f,-1.0f);
   ** int maskd[] = {2,0,0,2,0,2};
   ** slip::Matrix<int> Maskd(2,3,maskd);
   ** //print the containers and the mask
   ** std::cout<<"Md1 = \n"<<Md1<<std::endl;
   ** std::cout<<"Md2 = \n"<<Md2<<std::endl; 
   ** std::cout<<"Maskd = \n"<<Maskd<<std::endl;
   ** //computes the L2 distance map between the 2 containers
   ** //according to the mask Maskd
   ** slip::distance_mask(Md1.begin(),Md1.end(),
   **                     Maskd.begin(),
   **                     Md2.begin(),
   **                     Resultd.begin(),
   **                     slip::L2_dist<float,float>(),2);
   ** std::cout<<"Resultd L2 = \n"<<Resultd<<std::endl;
   ** \endcode
   */
  template<typename InputIterator1, 
	   typename InputIterator2,
	   typename MaskIterator,
	   typename OutputIterator,
	   typename BinaryFunctor>
  inline
  void distance_mask(InputIterator1 first1,
		     InputIterator1 last1,
		     MaskIterator mask_first,
		     InputIterator2 first2,
		     OutputIterator result,
		     BinaryFunctor distance,
		     typename std::iterator_traits<MaskIterator>::value_type 
		     value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    assert(first1 != last1);
    slip::transform_mask_bin(first1,last1,mask_first,first2,result,distance,value);
  }

  /**
   **  \brief Computes the pointwise distances of two 
   ** ranges: \f$ d(s_i, b_i)\f$  according to a Predicate.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  first2     An input iterator.
   **  \param  result     An output iterator.
   **  \param  distance  A binary operator.
   **  \param  pred A predicate.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [result,result + (last1 -  first1)) is a valid range.
   ** \pre result is not an iterator within the range ]first1,last1) or
   **      ]first2,first2 + (last1 -  first1)).
   ** \par Example:
   ** \code
   ** //define a predicate
   ** template<typename T>
   ** bool lt2Predicate (const T& val)
   ** {
   **  return (val <= T(2));
   ** }
   ** \endcode
   ** \code
   ** //create two containers, a mask and the map container
   ** slip::Matrix<float> Md1(2,3);
   ** slip::Matrix<float> Md2(2,3);
   ** slip::Matrix<float> Resultd(2,3);
   ** slip::iota(Md1.begin(),Md1.end(),1.0f,1.0f);
   ** slip::iota(Md2.begin(),Md2.end(),6.0f,-1.0f);
   ** std::cout<<"Md1 = \n"<<Md1<<std::endl;
   ** std::cout<<"Md2 = \n"<<Md2<<std::endl; 
   ** //computes the L2 distance map between the 2 containers
   ** //according to the predicate lt2Predicate<float>
   ** slip::distance_if(Md1.begin(),Md1.end(),
   **                   Md2.begin(),
   **                   Resultd.begin(),
   **                   slip::L2_dist<float,float>(),
   **                   lt2Predicate<float>);
   ** std::cout<<"Resultd L2 = \n"<<Resultd<<std::endl;
   ** \endcode
   */
 template<typename _InputIterator1, 
	   typename _InputIterator2,
	   typename _OutputIterator, 
	   typename _BinaryOperation,
	   typename _Predicate>
 inline
 void
 distance_if(_InputIterator1 first1, _InputIterator1 last1,
	     _InputIterator2 first2, 
	     _OutputIterator result,
	     _BinaryOperation distance,
	     _Predicate pred)
 {
   assert(first1 != last1);
   slip::transform_if(first1,last1,first2,result,distance,pred);
 }
  
  /* @} */

   /** \name Specific distance algorithms */
  /* @{ */
  /**
   ** \brief Computes the mean square of the pointwise difference of two
   ** ranges of size N: \f$ \frac{1}{N}\sum_i^N (xi - yi)^2 \f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \return  mean square value of the differences of the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Value_T must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::mean_square_diff<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
   ** \endcode
   */
  template< typename Value_T, typename InputIterator1, typename InputIterator2>
  inline
  Value_T mean_square_diff(InputIterator1 first1,
			   InputIterator1 last1,
			   InputIterator2 first2)
  {
    assert(first1 != last1);
    Value_T init = Value_T(0);
    Value_T count = Value_T(0);
    for (; first1 != last1; ++first1, ++first2)
      {
        init = init + (*first1 - *first2) * (*first1 - *first2);
	count += Value_T(1);
      }
    return init/count;
  }

/** \brief Computes the mean square of the pointwise difference of two
   ** ranges of size N according to a mask sequence :
   ** \f$ \frac{1}{N_{mask}}\sum_{i / mask_i = value} (xi - yi)^2 \f$
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param first2 An InputIterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  mean square value of the differences of the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** float f3[] = {1,1,1,0,0,1};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::Matrix<float> Mask(2,3,f3);
   ** std::cout<<slip::mean_square_diff_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   */
template<typename Real, typename InputIterator1, typename InputIterator2,typename MaskIterator>
  inline

Real mean_square_diff_mask(InputIterator1 first1,
			   InputIterator1 last1,
			   MaskIterator mask_first, 
			   InputIterator2 first2,
			   typename std::iterator_traits<MaskIterator>::value_type value=typename                  std::iterator_traits<MaskIterator>::value_type(1))

{
    assert(first1 != last1);
    Real init = Real(0);
    Real count = Real(0);
    for (; first1 != last1; ++first1, ++first2,++mask_first)
      {
        if(*mask_first == value) 
        {
          init = init + (*first1 - *first2) * (*first1 - *first2);
	 count += Real(1);
        }
      }
    return init/count;
  }


/**  \brief Computes the mean square of the pointwise difference of two
   **  ranges according to a Predicate : \f$ \frac{1}{N_{pred}}\sum_{i / pred=true} (xi - yi)^2 \f$.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  first2     An input iterator.
   **  \param  pred       A predicate.
   **  \return  mean square value of the differences of the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::mean_square_diff_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/
template<typename Real,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>

 Real mean_square_diff_if(InputIterator1 first1, InputIterator1 last1,
                          InputIterator2 first2, 
                          Predicate pred)
{
    assert(first1 != last1);
    Real init = Real(0);
    Real count = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      {
         if(pred(*first1))
          {
           init = init + (*first1 - *first2) * (*first1 - *first2);
	   count += Real(1);
          }
      }
    return init/count;
  }

   /**
   ** \brief Computes the mean absolute difference of two
   ** range of size N: \f$(1/ N)\sum_i^N |xi - yi|\f$ 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \return  mean absolute value of the differences of the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Value_T must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::mean_abs_diff<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
   ** \endcode
   */
  template<typename Value_T, typename InputIterator1, typename InputIterator2>
  inline
  Value_T mean_abs_diff(InputIterator1 first1,
			InputIterator1 last1,
			InputIterator2 first2)
  {
    assert(first1 != last1);
    Value_T init  = Value_T(0);
    Value_T count = Value_T(0);
    for (; first1 != last1; ++first1, ++first2)
      {
        init = init + std::abs(*first1 - *first2); 
	count += Value_T(1);
      }
    return init/count;
  }

/** \brief Computes the mean absolute difference of two
   ** ranges of size N according to a mask sequence :
   ** \f$(1/ N_{mask})\sum_{i / mask_i = value} |xi - yi|\f$ 
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param first2 An InputIterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  mean absolute value of the differences of the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** float f3[] = {1,1,1,0,0,1};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::Matrix<float> Mask(2,3,f3);
   ** std::cout<<slip::mean_abs_diff_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   */
template<typename Value_T, typename InputIterator1, typename InputIterator2,typename MaskIterator>
  inline
Value_T mean_abs_diff_mask(InputIterator1 first1,
			   InputIterator1 last1,
			   MaskIterator mask_first, 
			   InputIterator2 first2,
			   typename std::iterator_traits<MaskIterator>::value_type value=typename                  std::iterator_traits<MaskIterator>::value_type(1))
  
{
    assert(first1 != last1);
    Value_T init  = Value_T(0);
    Value_T count = Value_T(0);
    for (; first1 != last1; ++first1, ++first2,++mask_first)
      {
        if(*mask_first == value) 
        {
          init = init + std::abs(*first1 - *first2); 
	  count += Value_T(1);
        }
      }
    return init/count;
  }



/**  \brief Computes the mean absolute difference of two
   **  ranges according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  first2     An input iterator.
   **  \param  pred       A predicate.
   **  \return  mean absolute value of the differences of the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::mean_abs_diff_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/
template<typename Real,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>

 Real mean_abs_diff_if(InputIterator1 first1, InputIterator1 last1,
                          InputIterator2 first2, 
                          Predicate pred)
{
    assert(first1 != last1);
    Real init  = Real(0);
    Real count = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      {
        if(pred(*first1))
        {
          init = init + std::abs(*first1 - *first2); 
	  count += Real(1);
        }
      }
    return init/count;
  }

  /**
   ** \brief Computes the signal to noise ration (snr) of two
   ** ranges : \f$\sum_i \frac{s_i*s_i}{(s_i - b_i)^2}\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first1 An InputIterator to the first element of the signal.
   ** \param last1  An InputIterator to the one past-the-end element of the signal.
   ** \param first2 An InputIterator to the "noise" range.
   ** \return  snr of the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::snr<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
   ** \endcode
   */
  template<typename Real, typename InputIterator1, typename InputIterator2>
  inline
  Real snr(InputIterator1 first1,
	   InputIterator1 last1,
	   InputIterator2 first2)
  {
    assert(first1 != last1);
    
    Real s_variance  = Real(0);
    Real sb_variance = Real(0);

    for (; first1 != last1; ++first1, ++first2)
      {
        s_variance  = s_variance + ((*first1) * (*first1));
	sb_variance = sb_variance + (*first1 - *first2) * (*first1 - *first2);
      }    
    return (s_variance / sb_variance);
  }
 
/** \brief Computes the signal to noise ration (snr) of two ranges according to a mask sequence :
   ** \f$\sum_{i / mask_i = value} \frac{s_i*s_i}{(s_i - b_i)^2}\f$
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param first2 An InputIterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  snr of the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** float f3[] = {1,1,1,0,0,1};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::Matrix<float> Mask(2,3,f3);
   ** std::cout<<slip::snr_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   */
template<typename Real, typename InputIterator1, typename InputIterator2,typename MaskIterator>
  inline
  Real snr_mask(InputIterator1 first1,
		InputIterator1 last1,
		MaskIterator mask_first,        
		InputIterator2 first2,
		typename std::iterator_traits<MaskIterator>::value_type value=typename                  std::iterator_traits<MaskIterator>::value_type(1))
  
{
    assert(first1 != last1);
    
    Real s_variance  = Real(0);
    Real sb_variance = Real(0);

    for (; first1 != last1; ++first1, ++first2,++mask_first)
      {
          if(*mask_first == value)
         {
           s_variance  = s_variance + ((*first1) * (*first1));
	   sb_variance = sb_variance + (*first1 - *first2) * (*first1 - *first2);
        }
      }    
    return (s_variance / sb_variance);
  }

/**  \brief Computes the signal to noise ration (snr) of two ranges
   **  according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  first2     An input iterator.
   **  \param  pred       A predicate.
   **  \return  snr in dB of the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::snr_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/
template<typename Real,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>

 Real snr_if(InputIterator1 first1, InputIterator1 last1,
                    InputIterator2 first2, 
                   Predicate pred)
{
    assert(first1 != last1);
    
    Real s_variance  = Real(0);
    Real sb_variance = Real(0);

    for (; first1 != last1; ++first1, ++first2)
      {
          if(pred(*first1))
         {
           s_variance  = s_variance + ((*first1) * (*first1));
	   sb_variance = sb_variance + (*first1 - *first2) * (*first1 - *first2);
        }
      }    
    return (s_variance / sb_variance);
  }

 /**
   ** \brief Computes the decibel signal to noise ration (snr) of two
   ** ranges : \f$ 20.0*\log{\sum_i \frac{s_i*s_i}{(s_i - b_i)^2}}\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first1 An InputIterator to the first element of the signal.
   ** \param last1  An InputIterator to the one past-the-end element of the signal.
   ** \param first2 An InputIterator to the "noise" range.
   ** \return  snr in dB unit of the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::snr_dB<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
   ** \endcode
   */
  template<typename Real, typename InputIterator1, typename InputIterator2>
  inline
  Real snr_dB(InputIterator1 first1,
	      InputIterator1 last1,
	      InputIterator2 first2)
  {
    return 20.0 * std::log10(slip::snr<Real>(first1,last1,first2));
  }


/** \brief Computes the decibel signal to noise ration (snr) of two ranges according to a mask sequence :
   ** \f$ 20.0*\log{\sum_{i / mask_i = value} \frac{s_i*s_i}{(s_i - b_i)^2}}\f$
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/11
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param first2 An InputIterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  snr_dB of the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** float f3[] = {1,1,1,0,0,1};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::Matrix<float> Mask(2,3,f3);
   ** std::cout<<slip::snr_dB_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   */
template<typename Real, typename InputIterator1, typename InputIterator2,typename MaskIterator>
  inline
  Real snr_dB_mask(InputIterator1 first1,
		   InputIterator1 last1,
		   MaskIterator mask_first,
		   InputIterator2 first2,
		   typename std::iterator_traits<MaskIterator>::value_type value=typename                  std::iterator_traits<MaskIterator>::value_type(1))
{
  return 20.0 * std::log10(slip::snr_mask<Real>(first1,last1,mask_first,first2,value));
}
  /** \brief Computes the decibel signal to noise ration (snr) of two ranges according to a predicate:
   ** \f$ 20.0*\log{\sum_{i / pred = true} \frac{s_i*s_i}{(s_i - b_i)^2}}\f$
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \param  pred A predicate.
   ** \return  snr_dB of the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
    **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   **  std::cout<<slip::snr_dB_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;

   ** \endcode
   */
template<typename Real,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>

 Real snr_dB_if(InputIterator1 first1, InputIterator1 last1,
                    InputIterator2 first2, 
                   Predicate pred)
{
return 20.0 * std::log10(slip::snr_if<Real>(first1,last1,first2,pred));
}

 /**
   ** \brief Computes the square euclidian distance of two 
   ** ranges: \f$ \sum_i (s_i - b_i)^2\f$
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2007/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \return  square Euclidian distance between two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::L22_distance<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
   ** \endcode
   */
  template<typename Real, typename InputIterator1, typename InputIterator2>
  inline
  Real L22_distance(InputIterator1 first1,
		    InputIterator1 last1,
		    InputIterator2 first2)
  {
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      {
        init += (*first1 - *first2) * (*first1 - *first2);
      }
    return init;
  }
/**
   ** \brief Computes the square eucludian distance of two ranges according to a mask sequence :
   ** \f$ \sum_{i / mask_i = value} (s_i - b_i)^2\f$
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param first2 An InputIterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  square Euclidian distance between two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** float f3[] = {1,1,1,0,0,1};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::Matrix<float> Mask(2,3,f3);
   ** std::cout<<slip::L22_distance_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   */
template<typename Real, typename InputIterator1, typename InputIterator2,typename MaskIterator>
  inline
  Real L22_distance_mask(InputIterator1 first1,
		         InputIterator1 last1,
			 MaskIterator mask_first,
			 InputIterator2 first2,
			 typename std::iterator_traits<MaskIterator>::value_type value=typename  std::iterator_traits<MaskIterator>::value_type(1)
			 )
  {
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2,++mask_first)
      {
         if(*mask_first == value)
           {
              init += (*first1 - *first2) * (*first1 - *first2);
           }
      }
    return init;
  }


/**  \brief Computes the square eucludian distance of two ranges
   **  according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  first2     An input iterator.
   **  \param  pred       A predicate.
   **  \return  square Euclidian distance between two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::L22_distance_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/
template<typename Real,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>

 Real L22_distance_if(InputIterator1 first1, InputIterator1 last1,
                    InputIterator2 first2, 
                    Predicate pred)
 {
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      {
         if(pred(*first1))
             { 
               init += (*first1 - *first2) * (*first1 - *first2);
             }
      }
  return init;
 }

 /**
   ** \brief Computes the Euclidian distance of two 
   ** ranges : \f$ \sqrt{\sum_i (s_i - b_i)^2}\f$
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2007/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \return  Euclidian distance between the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::L2_distance<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
   ** \endcode
   */
  template<typename Real, typename InputIterator1, typename InputIterator2>
  inline
  Real L2_distance(InputIterator1 first1,
		   InputIterator1 last1,
		   InputIterator2 first2)
  {
    return std::sqrt(slip::L22_distance<Real>(first1,last1,first2));
  }


/** \brief Computes the eucludian distance of two ranges according to a mask sequence :
   ** \f$ \sqrt{\sum_{i / mask_i = value} (s_i - b_i)^2}\f$
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param first2 An InputIterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  Euclidian distance between two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** float f3[] = {1,1,1,0,0,1};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::Matrix<float> Mask(2,3,f2);
   ** std::cout<<slip::L2_distance_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   */
template<typename Real, typename InputIterator1, typename InputIterator2,typename MaskIterator>
  inline
  Real L2_distance_mask(InputIterator1 first1,
			InputIterator1 last1,
			MaskIterator mask_first,
			InputIterator2 first2,
                      
                        typename std::iterator_traits<MaskIterator>::value_type value=typename  std::iterator_traits<MaskIterator>::value_type(1))
  {
    return std::sqrt(slip::L22_distance_mask<Real>(first1,last1,mask_first,first2,value));
  }



/**  \brief Computes the eucludian distance of two ranges
   **  according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  first2     An input iterator.
   **  \param  pred       A predicate.
   **  \return  Euclidian distance between two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::L2_distance_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/
template<typename Real,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>

 Real L2_distance_if(InputIterator1 first1, InputIterator1 last1,
                    InputIterator2 first2, 
                    Predicate pred)
{
    return std::sqrt(slip::L22_distance_if<Real>(first1,last1,first2,pred));
  }

 /**
   ** \brief Computes the L1 distance of two 
   ** ranges : \f$ \sum_i |s_i - b_i|\f$
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2007/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \return  L1 distance between the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::L1_distance<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
   ** \endcode
   */
  template<typename Real, typename InputIterator1, typename InputIterator2>
  inline
  Real L1_distance(InputIterator1 first1,
		   InputIterator1 last1,
		   InputIterator2 first2)
  {
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      {
        init += std::abs(*first1 - *first2) ;
      }
    return init;
  } 

/**
   ** \brief Computes the L1 distance of two ranges according to a mask sequence :
   ** \f$ \sum_{i / mask_i = value} |s_i - b_i|\f$
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param first2 An InputIterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  L1 distance between the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** float f3[] = {1,1,1,0,0,1};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::Matrix<float> Mask(2,3,f3);
   **std::cout<<slip::L1_distance_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   */
template<typename Real, typename InputIterator1, typename InputIterator2,typename MaskIterator>
  inline
   Real L1_distance_mask(InputIterator1 first1,
                         InputIterator1 last1,
                         MaskIterator mask_first, 
			 InputIterator2 first2,
			 typename std::iterator_traits<MaskIterator>::value_type value=typename
			 std::iterator_traits<MaskIterator>::value_type(1))
  {
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2,++mask_first)
       {
         if(*mask_first == value)
           {
              init += std::abs(*first1 - *first2) ;
           }
      }
     return init;
   } 



/**  \brief Computes the L1 distance of two ranges
   **  according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  first2     An input iterator.
   **  \param  pred       A predicate.
   **  \return  L1 distance between the two ranges
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::L1_distance_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/
template<typename Real,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>

 Real L1_distance_if(InputIterator1 first1, InputIterator1 last1,
                    InputIterator2 first2, 
                    Predicate pred)
 {
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      {
         if(pred(*first1))
             { 
               init += std::abs(*first1 - *first2) ;
             }
      }
  return init;
 }


 /**
   ** \brief Computes the Minkowski order n distance of two 
   ** ranges : \f$ (\sum_i |s_i - b_i|^n)^\frac{1}{n}\f$
   ** Use L1 and L2 distance for n=1 or 2
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2007/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param n order of the minkowksi distance
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \return  Minksowksi distance between the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \pre n > 0
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::Minkowski_distance<float>(3,M1.begin(),M1.end(),M2.begin())<<std::endl;
   ** \endcode
   */
  template<typename Real, typename InputIterator1, typename InputIterator2>
  inline
  Real Minkowski_distance(const std::size_t n, 
			  InputIterator1 first1,
			  InputIterator1 last1,
			  InputIterator2 first2)
  {
    assert(first1 != last1);
    assert(n > 0);
    Real init = Real(0);
    Real tmp = Real(1);
    Real tmp2 = Real(1);

    for (; first1 != last1; ++first1, ++first2)
      {
	tmp = std::abs(*first1 - *first2);
	tmp2 = Real(1);
	for (std::size_t i = 0; i < n; ++i){
		tmp2 *= tmp;
	}
	
        init += tmp2 ;
      }
    return std::pow(init,1/Real(n));
  } 
/** \brief Computes the Minkowski order n distance of two 
   ** ranges according to a mask sequence :
   ** \f$ (\sum_{i / mask_i = value} |s_i - b_i|^n)^\frac{1}{n}\f$
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param n order of the minkowksi distance
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param first2 An InputIterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  Minksowksi distance between the two ranges
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \pre n > 0
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** float f3[] = {1,1,1,0,0,1};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::Matrix<float> Mask(2,3,f3);
   ** std::cout<<slip::Minkowski_distance_mask<float>(3,M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   */
template<typename Real, typename InputIterator1, typename InputIterator2,typename MaskIterator>
  inline
Real Minkowski_distance_mask(const std::size_t n,
			     InputIterator1 first1,
			     InputIterator1 last1,
			     MaskIterator mask_first,
			     InputIterator2 first2,
			     typename std::iterator_traits<MaskIterator>::value_type value=typename  std::iterator_traits<MaskIterator>::value_type(1))

{
    assert(first1 != last1);
    assert(n > 0);
    Real init = Real(0);
    Real tmp = Real(1);
    Real tmp2 = Real(1);

    for (; first1 != last1; ++first1, ++first2,++mask_first)
      {
        if(*mask_first == value) 
         {
	  tmp = std::abs(*first1 - *first2);
	  tmp2 = Real(1);
	  for (std::size_t i = 0; i < n; ++i)
          {
		tmp2 *= tmp;
	  }
	
        init += tmp2 ;
        }
      }
    return std::pow(init,1/Real(n));
  } 



/**  \brief Computes the Minkowski order n distance of two 
   **  ranges according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param n order of the minkowksi distance
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  first2     An input iterator.
   **  \param  pred       A predicate.
   **  \return  Minksowksi distance between the two ranges
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::Minkowski_distance_if<float>(3,M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/
template<typename Real,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>
inline
Real Minkowski_distance_if(const std::size_t n,
                              InputIterator1 first1,
		              InputIterator1 last1,
		              InputIterator2 first2,
                              Predicate pred)
{
    assert(first1 != last1);
    assert(n > 0);
    Real init = Real(0);
    Real tmp = Real(1);
    Real tmp2 = Real(1);

    for (; first1 != last1; ++first1, ++first2)
      {
        if(pred(*first1))
         {
	  tmp = std::abs(*first1 - *first2);
	  tmp2 = Real(1);
	  for (std::size_t i = 0; i < n; ++i)
          {
		tmp2 *= tmp;
	  }
	
        init += tmp2 ;
        }
      }
    return std::pow(init,1/Real(n));
  } 


  /**
   ** \brief Computes the Linfinite distance of two 
   ** ranges : \f$ sup \{|s_i - b_i|\}\f$
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2007/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \return  Linf distance between the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::Linf_distance<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
   ** \endcode
  */
  template<typename Real, typename InputIterator1, typename InputIterator2>
  inline
  Real Linf_distance(InputIterator1 first1,
		     InputIterator1 last1,
		     InputIterator2 first2)
  {
    assert(first1 != last1);
    Real init = Real(0);
    Real tmp = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      {
	tmp = std::abs(*first1 - *first2);
        if (tmp > init) init = tmp;
      }
    return init;
  } 
/** \brief Computes the Linfinite distance of two ranges according to a mask sequence :
   ** \f$ sup_{mask_i = value} \{|s_i - b_i|\}\f$
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param first2 An InputIterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  Linf distance between the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** float f3[] = {1,1,1,0,0,1};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::Matrix<float> Mask(2,3,f3);
   ** std::cout<<slip::Linf_distance_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   */
template<typename Real, typename InputIterator1, typename InputIterator2,typename MaskIterator>
  inline
  Real Linf_distance_mask(InputIterator1 first1,
			  InputIterator1 last1,
			  MaskIterator mask_first,
			  InputIterator2 first2,
                       
                        typename std::iterator_traits<MaskIterator>::value_type value=typename                  std::iterator_traits<MaskIterator>::value_type(1))
{
    assert(first1 != last1);
    Real init = Real(0);
    Real tmp = Real(0);
    for (; first1 != last1; ++first1, ++first2,++mask_first)
      {
           if(*mask_first == value)
            {
                 tmp = std::abs(*first1 - *first2);
                 if (tmp > init) init = tmp;
            }
     }
   return init;
  } 

/**  \brief Computes the Linfinite distance of two ranges
   **  according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  first2     An input iterator.
   **  \param  pred       A predicate.
   **  \return  Linf distance between the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::Linf_distance_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/
template<typename Real,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>

 Real Linf_distance_if(InputIterator1 first1, InputIterator1 last1,
                    InputIterator2 first2, 
                   Predicate pred)
{ 
    assert(first1 != last1);
    Real init = Real(0);
    Real tmp = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      {
           if(pred(*first1))
            {
                 tmp = std::abs(*first1 - *first2);
                 if (tmp > init) 
                   { 
                    init = tmp;
                   }
             }
      }
   return init;
}
 /**
   ** \brief Computes the Kullback-Leibler "distance" of two 
   ** ranges : \f$ \sum_i (s_i log2(s_i/b_i))\f$
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2007/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \return  Kullback-Leibler "distance" distance between the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::KullbackLeibler_distance<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
   ** \endcode
   */
  template<typename Real, typename InputIterator1, typename InputIterator2>
  inline
  Real KullbackLeibler_distance(InputIterator1 first1,
				InputIterator1 last1,
				InputIterator2 first2)
  {
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      {
	init += (*first1) * log2((*first1) / (*first2));
      }
    return init;
  } 

/** \brief Computes the Kullback-Leibler "distance" of two 
   ** ranges according to a mask sequence :
   ** \f$ \sum_{i / mask_i = value} (s_i log2(s_i/b_i))\f$
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param first2 An InputIterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  Kullback-Leibler "distance" distance between the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** float f3[] = {1,1,1,0,0,1};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::Matrix<float> Mask(2,3,f3);
   ** std::cout<<slip::KullbackLeibler_distance_mask<float>(M1.begin(),M1.end(),,Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   */
template<typename Real, typename InputIterator1, typename InputIterator2,typename MaskIterator>
  inline
Real KullbackLeibler_distance_mask(InputIterator1 first1,
				   InputIterator1 last1,
				   MaskIterator mask_first,
				   InputIterator2 first2,
				   typename std::iterator_traits<MaskIterator>::value_type value=typename                  std::iterator_traits<MaskIterator>::value_type(1))
  {
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2,++mask_first)
      { 
        if(*mask_first == value) 
        {
	  init += (*first1) * log2((*first1) / (*first2));
        }
      }
    return init;
  } 



/**  \brief Computes the Kullback-Leibler "distance" of two
   **  ranges according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  first2     An input iterator.
   **  \param  pred       A predicate.
   **  \return  Kullback-Leibler "distance" distance between the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::KullbackLeibler_distance_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/
template<typename Real,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>
  inline
Real KullbackLeibler_distance_if(InputIterator1 first1,
				InputIterator1 last1,
				InputIterator2 first2,
                                Predicate pred)
{
assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      { 
        if(pred(*first1))
        {
	  init += (*first1) * log2((*first1) / (*first2));
        }
      }
    return init;
}


 /**
   ** \brief Computes the Chi2 "distance" of two 
   ** ranges : \f$ \sum_i ((s_i-b_i)*(s_i-b_i)/s_i)\f$
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2007/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \return  the Chi2 "distance" between the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::Chi2_distance<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
   ** \endcode
   */
  template<typename Real, typename InputIterator1, typename InputIterator2>
  inline
  Real Chi2_distance(InputIterator1 first1,
		     InputIterator1 last1,
		     InputIterator2 first2)
  {
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      {
        init += (*first1 - *first2) * (*first1 - *first2) / (*first1);
      }

    return init;
  } 
/** \brief Computes the Chi2 "distance" of two
   ** ranges according to a mask sequence :
   ** \f$ \sum_{i / mask_i = value} ((s_i-b_i)*(s_i-b_i)/s_i)\f$
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param first2 An InputIterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return  the Chi2 "distance" between the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \pre Real must be Assignable.
   ** \par Example:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** float f3[] = {1,1,1,0,0,1};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::Matrix<float> Mask(2,3,f3);
   ** std::cout<<slip::Chi2_distance_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   */
template<typename Real, typename InputIterator1, typename InputIterator2,typename MaskIterator>
  inline
Real Chi2_distance_mask(InputIterator1 first1,
			InputIterator1 last1,
			MaskIterator mask_first,
			InputIterator2 first2,
			typename std::iterator_traits<MaskIterator>::value_type value=typename                  std::iterator_traits<MaskIterator>::value_type(1))

{
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2,++mask_first)
      {
       if(*mask_first == value) 
        {
          init += (*first1 - *first2) * (*first1 - *first2) / (*first1);
        }
      }

    return init;
  } 

/**  \brief Computes the Chi2 "distance" of two
   **  ranges according to a Predicate.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/11
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An input iterator.
   **  \param  last1      An input iterator.
   **  \param  first2     An input iterator.
   **  \param  pred       A predicate.
   **  \return  the Chi2 "distance" between the two ranges.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   **  \pre Real must be Assignable.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::Chi2_distance_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode

*/

template<typename Real,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>
  inline
Real Chi2_distance_if(InputIterator1 first1,
				InputIterator1 last1,
				InputIterator2 first2,
                                Predicate pred)
{
    assert(first1 != last1);
    Real init = Real(0);
    for (; first1 != last1; ++first1, ++first2)
      {
       if(pred(*first1))
        {
          init += (*first1 - *first2) * (*first1 - *first2) / (*first1);
        }
      }

    return init;
  } 



   /*!
   ** \author Thibault Roulier <troulier_AT_sic.univ-poitiers.fr>
   ** \date 2010/03/01
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1   An InputIterator.
   ** \param first2 An InputIterator. for the second datas
   ** \pre  The two ranges must have the same
   **       value type.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 - first1)) is a valid range.
   ** \par Example:
   ** \code
   ** //compute PSNR
    ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::psnr<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
   ** \endcode
   */

 template< typename Value_T,
	   typename InputIterator1, 
	   typename InputIterator2>
  inline
  Value_T psnr(InputIterator1 first1,
	       InputIterator1 last1,
	       InputIterator2 first2,
	       Value_T d = static_cast<Value_T>(255))

  {
       Value_T d_carre = d*d;
       Value_T  EQM = slip::mean_square_diff<Value_T>(first1, last1, first2);
       Value_T result = static_cast<Value_T>(10) * std::log10(d_carre/EQM);   	
       return result;       
   
  }

 /**
   ** \author Thibault Roulier <troulier_AT_sic.univ-poitiers.fr>
   ** \date 2010/03/01
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1   An InputIterator.
   ** \param mask1 An InputIterator.
   ** \param first2 An InputIterator. 
   ** \param value An value corresponding to the mask
   ** \pre  The two ranges must have the same
   **       value type.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 - first1)) is a valid range.
   ** \par Example:
   ** \code
   ** //compute PSNR
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** float f3[] = {1,1,1,0,0,1};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::Matrix<float> Mask(2,3,f3);
   ** std::cout<<slip::psnr_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   */

 template< typename Value_T,
	   typename InputIterator1, 
	   typename InputIterator2,
	   typename MaskIterator>
  inline
  Value_T psnr_mask(InputIterator1 first1,
		    InputIterator1 last1,
		    MaskIterator mask1,
		    InputIterator2 first2,
		    typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1),
	    Value_T d = static_cast<Value_T>(255))
  {
        Value_T d_carre = d*d;

        Value_T EQM = slip::mean_square_diff_mask<Value_T>(first1, last1, mask1,first2, value);

        Value_T result = static_cast<Value_T>(10) * std::log10(d_carre/EQM);   	
        return result;       
   
  }

 /**
   ** \author Thibault Roulier <troulier_AT_sic.univ-poitiers.fr>
   ** \date 2010/03/01
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1   An InputIterator.
   ** \param first2 An InputIterator. 
   ** \param pred A predicate
   ** \pre  The two ranges must have the same
   **       value type.
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 - first1)) is a valid range.
   ** \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::psnr_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode
  
   */

 template<typename Value_T,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>
 inline
  Value_T psnr_if(InputIterator1 first1, InputIterator1 last1,
		  InputIterator2 first2, 
		  Predicate pred,
		  Value_T d = static_cast<Value_T>(255))
  {
	
    Value_T d_carre = d*d;
        
    Value_T EQM = slip::mean_square_diff_if<Value_T>(first1, last1, first2, pred);
    
    Value_T result = static_cast<Value_T>(10) * std::log10(d_carre/EQM);   	
    return result;       
   
  }


 /**
   ** \brief Computes the Structural SIMilarity (SSIM) beetween two data ranges
    ** \f[ SSIM(x,y) = \frac{(2\mu_x\mu_y + c_1)(2cov_{xy}+c_2)}{(\mu_x^2+\mu_y^2+c_1)(\sigma_x^2+\sigma_y^2+c_2)} \f]
   ** where
   ** \li \f$\mu_x\f$ is the mean of x
   ** \li \f$\mu_y\f$ is the mean of y
   ** \li \f$\sigma_x^2\f$ is the variance of x
   ** \li \f$\sigma_y^2\f$ is the variance of y
   ** \li \f$cov_{xy}\f$ is the covariance of x and y
   ** \li \f$c_1 = (0.01d)^2\f$, \f$c_2 = (0.03d)^2\f$  two variable to avoid division by 0
   ** \par Description:
   ** The SSIM as been created to measure the quality of a compressed image  
   ** with the original one. Usually the SSIM is computed on 8x8 windows.
   ** 
   ** \par Reference: 
   ** Z. Wang, A. C. Bovik, H. R. Sheikh and E. P. Simoncelli, "Image quality assessment: From error visibility to structural similarity," IEEE Transactions on Image Processing, vol. 13, no. 4, pp. 600-612, avril 2004.
     
   ** \author Thibault Roulier <troulier_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \param d pixel dynamic (255 by default).
   ** \return  the structural Similarity value
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable
   ** \par Example1:
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::SSIM<float>(M1.begin(),M1.end(),M2.begin())<<std::endl;
  
   ** \endcode
   */
  template<typename Real, 
	   typename InputIterator1, 
	   typename InputIterator2>
  inline
  Real SSIM(InputIterator1 first1,
	    InputIterator1 last1,
	    InputIterator2 first2,
	    Real d = static_cast<Real>(255))
  {
    Real c1 = d*static_cast<Real>(0.01) * d*static_cast<Real>(0.01);
    Real c2 = d*static_cast<Real>(0.03) * d*static_cast<Real>(0.03);
    Real meanFirst1 = slip::mean<Real>(first1, last1);
    Real meanFirst2 = slip::mean<Real>(first2, first2 + (last1 - first1));
    Real varFirst1 = slip::variance(first1, last1, meanFirst1);
    Real varFirst2 = slip::variance(first2, first2 + (last1 - first1), meanFirst2);
    Real cov = slip::covariance(first1, last1, first2, meanFirst1, meanFirst2);

    return ((static_cast<Real>(2) * meanFirst1 * meanFirst2 + c1) * (static_cast<Real>(2) * cov + c2)) / (( meanFirst1 * meanFirst1 + meanFirst2 * meanFirst2 + c1) * (varFirst1 * varFirst1 + varFirst2 * varFirst2 + c2));    

  }


/*!
   ** \brief Computes the structural similarity beetween two data ranges according to a mask range
 ** \f[ SSIM(x,y) = \frac{(2\mu_x\mu_y + c_1)(2cov_{xy}+c_2)}{(\mu_x^2+\mu_y^2+c_1)(\sigma_x^2+\sigma_y^2+c_2)} \f]
   ** where
   ** \li \f$\mu_x\f$ is the mean of x
   ** \li \f$\mu_y\f$ is the mean of y
   ** \li \f$\sigma_x^2\f$ is the variance of x
   ** \li \f$\sigma_y^2\f$ is the variance of y
   ** \li \f$cov_{xy}\f$ is the covariance of x and y
   ** \li \f$c_1 = (0.01d)^2\f$, \f$c_2 = (0.03d)^2\f$  two variable to avoid division by 0
   ** \par Description:
   ** The SSIM as been created to measure the quality of a compressed image  
   ** with the original one. Usually the SSIM is computed on 8x8 windows.
   ** 
   ** \par Reference: 
   ** Z. Wang, A. C. Bovik, H. R. Sheikh and E. P. Simoncelli, "Image quality assessment: From error visibility to structural similarity," IEEE Transactions on Image Processing, vol. 13, no. 4, pp. 600-612, avril 2004.
     
   ** \author Thibault Roulier <troulier_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param maskFirst A MaskIterator
   ** \param first2 An InputIterator.
   ** \param value A value corresponding to the mask
   ** \param d pixel dynamic (255 by default)
   ** \return  the structural Similarity algorithms
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable
   ** \par Example:
    ** \code
   ** //compute SSIM
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** float f3[] = {1,1,1,0,0,1};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::Matrix<float> Mask(2,3,f3);
   ** std::cout<<slip::SSIM_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   */
  template<typename Real, 
	   typename InputIterator1, 
	   typename InputIterator2,
	   typename MaskIterator>
  inline
  Real SSIM_mask(InputIterator1 first1,
		 InputIterator1 last1,
		 MaskIterator maskFirst,
		 InputIterator2 first2,
		 typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1),
		Real d = static_cast<Real>(255) )
		

  {

    Real c1 = d*static_cast<Real>(0.01) * d*static_cast<Real>(0.01);
    Real c2 = d*static_cast<Real>(0.03) * d*static_cast<Real>(0.03);

    Real meanFirst1 = slip::mean_mask<Real>(first1, last1, maskFirst, value);
    Real meanFirst2 = slip::mean_mask<Real>(first2, first2 + (last1 - first1), maskFirst, value);
    Real varFirst1 = slip::variance_mask(first1, last1, 
					 maskFirst, meanFirst1, value);
    Real varFirst2 = slip::variance_mask(first2, first2 + (last1 - first1), 
					 maskFirst, meanFirst2, value);
    Real cov = slip::covariance_mask(first1, last1, 
				     first2, 
				     maskFirst, 
				     meanFirst1, meanFirst2, value);

    return ((static_cast<Real>(2) * meanFirst1 * meanFirst2 + c1) * (static_cast<Real>(2) * cov + c2)) / (( meanFirst1 * meanFirst1 + meanFirst2 * meanFirst2 + c1) * (varFirst1 * varFirst1 + varFirst2 * varFirst2 + c2));    

  }

 /*!
   ** \brief Computes the Sructural SIMilarity (SSIM) beetween two data ranges according to a predicate
   ** \f[ SSIM(x,y) = \frac{(2\mu_x\mu_y + c_1)(2cov_{xy}+c_2)}{(\mu_x^2+\mu_y^2+c_1)(\sigma_x^2+\sigma_y^2+c_2)} \f]
   ** where
   ** \li \f$\mu_x\f$ is the mean of x
   ** \li \f$\mu_y\f$ is the mean of y
   ** \li \f$\sigma_x^2\f$ is the variance of x
   ** \li \f$\sigma_y^2\f$ is the variance of y
   ** \li \f$cov_{xy}\f$ is the covariance of x and y
   ** \li \f$c_1 = (0.01d)^2\f$, \f$c_2 = (0.03d)^2\f$  two variable to avoid division by 0
   ** \par Description:
   ** The SSIM as been created to measure the quality of a compressed image  
   ** with the original one. Usually the SSIM is computed on 8x8 windows.
   ** 
   ** \par Reference: 
   ** Z. Wang, A. C. Bovik, H. R. Sheikh and E. P. Simoncelli, "Image quality assessment: From error visibility to structural similarity," IEEE Transactions on Image Processing, vol. 13, no. 4, pp. 600-612, avril 2004.
     
 
   ** \author Thibault Roulier <troulier_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param first2 An InputIterator.
   ** \param pred A predicate.
   ** \param d pixel dynamic (255 by default)
   ** \return  the structural Similarity algorithms
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre Real must be Assignable
   ** \par Example:
    **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {1.3,2.0,3.0,4.5,5.0,6.0};
   ** float f2[] = {5.0,4.0,5.0,4.4,5.0,4.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::SSIM_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode
   */
  template<typename Real, 
	   typename InputIterator1, 
	   typename InputIterator2,
	   typename Predicate>
  inline
  Real SSIM_if(InputIterator1 first1,
	       InputIterator1 last1,
	       InputIterator2 first2,
	       Predicate pred,
	       Real d = 255)
  {

    Real c1 = d*static_cast<Real>(0.01) * d*static_cast<Real>(0.01);
    Real c2 = d*static_cast<Real>(0.03) * d*static_cast<Real>(0.03);

    Real meanFirst1 = slip::mean_if<Real>(first1, last1, pred);
    Real meanFirst2 = slip::mean_if<Real>(first2,first2 + (last1 - first1), pred);
    Real varFirst1 = slip::variance_if(first1, last1, meanFirst1, pred);
    Real varFirst2 = slip::variance_if(first2, first2 + (last1 - first1), meanFirst2, pred);
    Real cov = slip::covariance_if(first1, last1, first2, meanFirst1, meanFirst2, pred);

    return ((2 * meanFirst1 * meanFirst2 + c1) * (2 * cov + c2)) / (( meanFirst1 * meanFirst1 + meanFirst2 * meanFirst2 + c1) * (varFirst1 * varFirst1 + varFirst2 * varFirst2 + c2));    

  }


/* @} */


/** \name Distance functors */
  /* @{ */

  /**
   ** \brief Computes the L1 distance between two values x and y: \f$ |x - y|\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x x value.
   ** \param y y value.
   ** \return \f$ |x - y|\f$
   */
  template <class _Rp,class _Tp>
  struct L1_dist : public std::binary_function<_Rp, _Tp, _Tp>
    {
      _Rp
      operator()(const _Tp& x, const _Tp& y) const
      { 
	return std::abs(x - y); 
      }
    };
    
  /**
   ** \brief Computes the L2 distance between two values x and y: \f$ \sqrt{(x - y)^2}\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x x value.
   ** \param y y value.
   ** \return \f$ \sqrt{(x - y)^2}\f$
   */
  template <class _Rp,class _Tp>
  struct L2_dist : public std::binary_function<_Rp, _Tp, _Tp>
    {
      _Rp
      operator()(const _Tp& x, const _Tp& y) const
	
      {
	_Tp diff = (x -y);
	return std::sqrt(diff * diff); 
      }
    };

  /**
   ** \brief Computes the L22 distance between two values x and y: \f$ (x - y)^2\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \version 0.0.1
   ** \param x x value.
   ** \param y y value.
   ** \return \f$ (x - y)^2\f$
   */
  template <typename _Rp,typename _Tp>
  struct L22_dist : public std::binary_function<_Rp, _Tp, _Tp>
    {
      _Rp
      operator()(const _Tp& x, const _Tp& y) const
	
      {
	_Tp diff = (x -y);
	return (diff * diff); 
      }
    };
 
   /**
   ** \brief Computes the L1 relative error between two values x and y: \f$ \frac{|x - y|}{|x|}\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x x value.
   ** \param y y value.
   ** \return \f$ \frac{|x - y|}{|x|}\f$
   ** \remarks return |y| if x == 0
   */
  template <typename _Rp,typename _Tp>
  struct Relative_error_L1 : public std::binary_function<_Rp, _Tp, _Tp>
    {
      _Rp
      operator()(const _Tp& x, const _Tp& y) const
	
      {
	if(x != _Tp())
	  {
	    return std::abs(x -y) / std::abs(x);
	  }
	else
	  {
	    return std::abs(y);
	  }
      }
    };
 /**
   ** \brief Computes the L2 relative error between two values x and y: \f$ \frac{\sqrt{(x - y)^2}}{|x|}\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x x value.
   ** \param y y value.
   ** \return \f$ \frac{\sqrt{(x - y)^2}}{|x|}\f$
   ** \remarks return |y| if x == 0
   */
  template <typename _Rp,typename _Tp>
  struct Relative_error_L2 : public std::binary_function<_Rp, _Tp, _Tp>
    {
      _Rp
      operator()(const _Tp& x, const _Tp& y) const
	
      {
	if(x != _Tp())
	  {
	    _Tp diff = (x -y);
	    return std::sqrt(diff * diff) / std::abs(x); 
	  }
	else
	  {
	    return std::abs(y);
	  }
      }
    };
 
  /**
   ** \brief Computes the Relative L22 error between two values x and y: \f$ \frac{(x - y)^2}{x^2}\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x x value.
   ** \param y y value.
   ** \return \f$ \frac{(x - y)^2}{x^2}\f$
   ** \remarks return y^2 if x == 0
   */
  template <typename _Rp,typename _Tp>
  struct Relative_error_L22 : public std::binary_function<_Rp, _Tp, _Tp>
    {
      _Rp
      operator()(const _Tp& x, const _Tp& y) const
	
      {
	if(x != _Tp())
	  {
	    _Tp diff = (x -y);
	    return (diff * diff) / (x*x); 
	  }
	else
	  {
	    return y*y;
	  }
      }
    };
  /**
   ** \brief Computes the L1 distance between two Vectors x and y: \f$ \sum_i |x_i - y_i|\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x x value.
   ** \param y y value.
   ** \return \f$ \sum_i |x_i - y_i|\f$
   */
 template <typename Vector>
struct L1_dist_vect: public std::binary_function<typename Vector::value_type, Vector, Vector>
    {
      typename Vector::value_type
      operator()(const Vector& x, 
		 const Vector& y) const
	
      {
	typename Vector::value_type dist = slip::L1_distance<typename Vector::value_type>(x.begin(),x.end(),y.begin());
	return dist;
      }
    };

/**
   ** \brief Computes the L2 distance between two Vectors x and y: \f$ \sqrt{\sum_i (x_i - y_i)^2}\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x x value.
   ** \param y y value.
   ** \return \f$ \sqrt{\sum_i (x_i - y_i)^2}\f$
   */
template <typename Vector>
struct L2_dist_vect: public std::binary_function<typename Vector::value_type, Vector, Vector>
    {
      typename Vector::value_type
      operator()(const Vector& x, 
		 const Vector& y) const
	
      {
	typename Vector::value_type dist = slip::L2_distance<typename Vector::value_type>(x.begin(),x.end(),y.begin());
	return dist;
      }
    };

 /**
   ** \brief Computes the L22 distance between two Vectors x and y: \f$ \sum_i (x_i - y_i)^2\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/30
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x x value.
   ** \param y y value.
   ** \return \f$ \sum_i (x_i - y_i)^2\f$
   */
template <typename Vector>
struct L22_dist_vect: public std::binary_function<typename Vector::value_type, Vector, Vector>
    {
      typename Vector::value_type
      operator()(const Vector& x, 
		 const Vector& y) const
	
      {
	typename Vector::value_type dist = slip::L22_distance<typename Vector::value_type>(x.begin(),x.end(),y.begin());
	return dist;
      }
    };

  /**
   ** \brief Computes the L2 relative error between two vectors x and y: \f$ \frac{||x|| - ||y||}{||x||}\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2011/05/03
   ** \since 1.4.0
   ** \version 0.0.1
   ** \param x x value.
   ** \param y y value.
   ** \return \f$ \frac{||x|| - ||y||}{||x||}\f$
   ** \remarks return ||y|| if x == 0
   */
  template <typename _Rp,typename _Tp>
  struct Relative_vect_error_L2 : public std::binary_function<_Rp, _Tp, _Tp>
    {
      _Rp
      operator()(const _Tp& x, const _Tp& y) const
	
      {
	_Rp norm_x = slip::L2_norm<_Rp>(x.begin(),x.end());
	_Rp norm_y = slip::L2_norm<_Rp>(y.begin(),y.end());
	if(norm_x != _Rp())
	  {
	    return std::abs(norm_x - norm_y) / norm_x;
	  }
	else
	  {
	    return norm_y;
	  }
      }
    };


/**
   ** \brief Computes the angle distance between two vectors x and y: \f$ \arccos\left(\frac{x.y}{||x||_2||y||_2}\right) \f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2011/05/03
   ** \version 0.0.1
   ** \since 1.4.0
   ** \param x x value.
   ** \param y y value.
   ** \return \f$ \arccos\left(\frac{x.y}{||x||_2||y||_2}\right)\f$
   ** \remarks return \f$\arccos(0)\quad if \quad ||x||\cdot||y|| = 0\f$
   */
  template <typename _Rp,typename _Tp>
  struct Angle_vect_error : public std::binary_function<_Rp, _Tp, _Tp>
    {
      _Rp
      operator()(const _Tp& x, const _Tp& y) const
	
      {
	_Rp norm_x = slip::L2_norm<_Rp>(x.begin(),x.end());
	_Rp norm_y = slip::L2_norm<_Rp>(y.begin(),y.end());
	if((norm_x != _Rp()) && (norm_y != _Rp()))
	  {
	    return std::acos(std::inner_product(x.begin(),x.end(),
						y.begin(),_Rp())/
			     (norm_x*norm_y));
	  }
	else
	  {
	    return std::acos(_Rp());
	  }
      }
    };

  /* @} */


}//slip::



#endif //SLIP_COMPARE_HPP
