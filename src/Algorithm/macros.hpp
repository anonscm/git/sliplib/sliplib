/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file macros.hpp
 * 
 * \brief Provides some mathematical functors and constants.
 * 
 */

#ifndef SLIP_MACROS_HPP
#define SLIP_MACROS_HPP

#include <complex>
#include <cstring>
#include <limits>
#include <cmath>



namespace slip
{
  /**
   ** \brief Real functor. Return the real part of x.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/01
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x
   ** \return the real part of x.
   ** \note Functor object that calls the std::real function
   */
  template<typename T1, typename T2> struct un_real : public std::unary_function<T1,T2>
{
  un_real(){}
  T2 operator() (const T1& x){return std::real(x);}
};
  /**
   ** \brief Imag functor. Return the imaginary part of x.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/01
   ** \version 0.0.1
   ** \param x
   ** \return the imaginary part of x.
   ** \note Functor object that calls the std::imag function
   */
template<typename T1, typename T2> struct un_imag : public std::unary_function<T1,T2>
{
  un_imag(){}
  T2 operator() (const T1& x){return std::imag(x);}
};
   /**
   ** \brief Abs functor. Return the absolute value of x.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/01
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x
   ** \return the absolute value of x.
   ** \note Functor object that calls the std::abs function
   */
template<typename T1, typename T2> 
struct un_abs : public std::unary_function<T1,T2>
{
  un_abs(){}
  T2 operator() (const T1& x){return (std::abs(x));}
};


 

  
   /**
   ** \brief Compare two element according to their absolute value.
   ** Return true if std::abs(__x) < std::abs( __y).
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/01
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param __x The first value.
   ** \param __y The second value.
   ** \return true if std::abs(__x) < std::abs( __y), flase else.
   ** \remarks Used to compute the max element of a range according to the 
   **  absolute value.
   */
  template <class _Tp>
  struct less_abs : public std::binary_function<_Tp, _Tp, bool>
  {
    inline
    bool
    operator()(const _Tp& __x, const _Tp& __y) const
    { 
      return std::abs(__x) < std::abs( __y);
    }
  };

  /**
   ** \brief Compare two element according to their absolute value.
   ** Return true if std::abs(__x) > std::abs( __y).
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/01
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param __x The first value.
   ** \param __y The second value.
   ** \return true if std::abs(__x) > std::abs( __y), flase else.
   ** \remarks Used to compute the max element of a range according to the 
   **  absolute value.
   */
  template <class _Tp>
  struct greater_abs : public std::binary_function<_Tp, _Tp, bool>
  {
    inline
    bool
    operator()(const _Tp& __x, const _Tp& __y) const
    { 
      return std::abs(__x) > std::abs( __y);
    }
  };

   /**
   ** \brief Computes the minimum value between two values.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>: conceptor
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param __x The first value.
   ** \param __y The second value.
   ** \return The minimum value.
   **
   ** \note Functor object that calls the std::min function
   */
  template <class _Tp>
  struct mini : public std::binary_function<_Tp, _Tp, _Tp>
  {
    _Tp
    operator()(const _Tp& __x, const _Tp& __y) const
    { return std::min(__x,__y); }
  };

  /**
   ** \brief Computes the maximum value between two values.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>: conceptor
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param __x The first value.
   ** \param __y The second value.
   ** \return The maximum value.
   **
   ** \note Functor object that calls the std::max function
   */
  template <class _Tp>
  struct maxi : public std::binary_function<_Tp, _Tp, _Tp>
  {
    _Tp
    operator()(const _Tp& __x, const _Tp& __y) const
    { return std::max(__x,__y); }
  };


 /**
   ** \brief Computes the sign of a 
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/07/16
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param a of generic type T
   ** \return \f$ \frac{a}{abs(a)}\f$ if a != 0; T(1) if a == 0. 
   */
  template<typename T>
  inline
  T sign(T a)
  {
    if (std::abs(a) > std::abs(std::numeric_limits<T>::epsilon()))
      return (a/std::abs(a));
    return T(1);
  }

template <typename T,typename R>
struct Sign : public std::unary_function<R,T>
{
  Sign()
  {}
  R operator() (const T& x)
  {
    R s = (x > T(0) ? R(1) : R(-1));
    if(x == T(0)) 
      {
	s = R(0);
      }
    return s;
  }
};


template <typename Complex>
struct complex_sign : public std::unary_function<Complex,Complex>
{
  complex_sign()
  {}
  Complex operator() (const Complex& x)
  {
    Complex r = Complex(1);
    if( x != Complex(0))
      {
	r = x/std::abs(x);
      }
    return r;
  }
};


  /**
   ** \brief Computes \f$ (x^2+y^2)^\frac{1}{2}\f$ without destructive underflow or overflow
   **
   ** \author Julien Dombre <dombre@univ-poitiers.fr>
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : adaptator
   ** \date 2008/03/05
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param x first value 
   ** \param y second value
   ** \return solution
   **
   */  
  template <typename Real>
  inline 
  Real pythagore(const Real& x, const Real& y)
  {
    Real absx = std::abs(x), absy = std::abs(y);
    if (absx > absy)
      { 
	return(absx * std::sqrt(1.0+(absy/absx)*(absy/absx)));
      }
    else
      {
	return(absy == 0.0 ? 0.0 : absy * std::sqrt(1.0+(absx/absy)*(absx/absy)));
      }
  }


/*!
  ** helper functor to n_max_elements algorithm.
  */
  template<typename Iterator>
  struct gt_it
  {
    bool operator()(Iterator s1, Iterator s2) const
    {
      return  *s1 > *s2;
    }
  };

  /*!
  ** helper functor to n_min_elements algorithm.
  */
  template<typename Iterator>
  struct lt_it
  {
    bool operator()(Iterator s1, Iterator s2) const
    {
      return  *s1 < *s2;
    }
  };

 /**
   ** \brief Computes the nth power of an element \f$ x^n\f$.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \date 2023/09/25 fix bug for N = 0
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param x the element to power
   ** \return the nth power of \a x
   ** \par Example:
   ** \code
   ** std::cout<<" 2 power 8 = "<<slip::nth_power<8,double>(2.0)<<std::endl;
   ** \endcode
   */
   template <int N, typename T>
   inline
   T nth_power(T x)
   {
     T result = static_cast<T>(1.0L);

     for(int i = 0; i < N; ++i)
       {
 	result *=  x;
       }
     return result;
    } 

 /**
   ** \brief function to compute. 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/04/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param x The element to power
   ** \param N The power. 
   ** \return the nth power of \a x
   ** \pre N >=0
   ** \par Example:
   ** \code
   ** std::cout<<" 2.4 power 8 = "<<slip::power(2.0,8)<<std::endl;
   ** \endcode
   */
  template <typename T,typename Integer>
  inline
  T power(T x, Integer N)
  {
    assert( N >= 0);
    T result = T(1);
    if(N !=0)
      {
	result = x;
	for(Integer i = 1; i < N; ++i)
	  {
	    result = result * x;
	  }
      }
   
    return result;
  }

  /**
   ** \brief is_zero returns true if value x is near zero, false else.
   ** \since 1.5.0
   ** \param x value to evaluate.
   ** \param epsilon precision.
   ** \return true if abs(x) < epsilon, false else.
   ** \pre x should be a real data type
   ** \par Example:
   ** \code
   ** float f1 = 0.001f;
   ** float f2 = 1e-10f;
   ** float f3 = -0.001f;
   ** float f4 = -1e-10f;
   ** double d1 = 0.001;
   ** double d2 = 1e-10;
   ** double d3 = -0.001;
   ** double d4 = -1e-10;
   **
   ** std::cout<<"slip::is_zero("<<f1<<") = "<<slip::is_zero(f1)<<std::endl;
   ** std::cout<<"slip::is_zero("<<f2<<") = "<<slip::is_zero(f2)<<std::endl;
   ** std::cout<<"slip::is_zero("<<f3<<") = "<<slip::is_zero(f3)<<std::endl;
   ** std::cout<<"slip::is_zero("<<f4<<") = "<<slip::is_zero(f4)<<std::endl;
   **
   ** std::cout<<"slip::is_zero("<<d1<<") = "<<slip::is_zero(d1)<<std::endl;
   ** std::cout<<"slip::is_zero("<<d2<<") = "<<slip::is_zero(d2)<<std::endl;
   ** std::cout<<"slip::is_zero("<<d3<<") = "<<slip::is_zero(d3)<<std::endl;
   ** std::cout<<"slip::is_zero("<<d4<<") = "<<slip::is_zero(d4)<<std::endl;

   ** std::cout<<"slip::is_zero("<<f1<<",0.1f) = "<<slip::is_zero(f1,0.1f)<<std::endl;
  ** std::cout<<"slip::is_zero("<<f2<<",0.1f) = "<<slip::is_zero(f2,0.1f)<<std::endl;
  ** std::cout<<"slip::is_zero("<<f3<<",0.1f) = "<<slip::is_zero(f3,0.1f)<<std::endl;
  ** std::cout<<"slip::is_zero("<<f4<<",0.1f) = "<<slip::is_zero(f4,0.1f)<<std::endl;
  ** std::cout<<"slip::is_zero("<<d1<<",0.1) = "<<slip::is_zero(d1,0.1)<<std::endl;
  ** std::cout<<"slip::is_zero("<<d2<<",0.1) = "<<slip::is_zero(d2,0.1)<<std::endl;
  ** std::cout<<"slip::is_zero("<<d3<<",0.1) = "<<slip::is_zero(d3,0.1)<<std::endl;
  ** std::cout<<"slip::is_zero("<<d4<<",0.1) = "<<slip::is_zero(d4,0.1)<<std::endl;
  ** \endcode
   */
  template<typename T>
  bool is_zero(const T& x, const T& epsilon = static_cast<T>(1e-6))
  {
    return (std::abs(x) < epsilon);
  }
 
/**
   ** \brief is_greater_zero returns true if value x is near greater zero, false else.  
   ** \since 1.5.0
   ** \param x value to evaluate.
   ** \param epsilon precision.
   ** \return true if abs(x) > epsilon, false else.
   ** \pre x should be a real data type
   ** \par Example:
   ** \code
   ** float f1 = 0.001f;
   ** std::cout<<"slip::is_greater_zero("<<f1<<") = "<<slip::is_greater_zero(f1)<<std::endl;
   ** std::cout<<"slip::is_greater_zero("<<f1<<",1e-12f) = "<<slip::is_greater_zero(f1,1e-12f)<<std::endl;
   ** \endcode
   */
   template<typename T>
   bool is_greater_zero(const T& x, const T& epsilon = static_cast<T>(1e-6))
  {
    return (x > epsilon);
  }


  /**
   ** \brief is_greater_equal_zero return true if value x is near greater or equal to zero, false else.
   ** \since 1.5.0
   ** \param x value to evaluate.
   ** \param epsilon precision.
   ** \return true if abs(x) >= epsilon, false else.
   ** \pre x should be a real data type
   ** \par Example:
   ** \code
   ** float f1 = 0.001f;
   ** std::cout<<"slip::is_greater_equal_zero("<<f1<<") = "<<slip::is_greater_equal_zero(f1)<<std::endl;
   ** std::cout<<"slip::is_greater_equal_zero("<<f1<<",1e-12f) = "<<slip::is_greater_equal_zero(f1,1e-12f)<<std::endl;
   ** \endcode
   */
   template<typename T>
   bool is_greater_equal_zero(const T& x, const T& epsilon = static_cast<T>(1e-6))
  {
    return (x >= epsilon);
  }

  /**
   ** \brief is_less_zero returns true if value x is near less than zero, false else.  
   ** \since 1.5.0
   ** \param x value to evaluate.
   ** \param epsilon precision.
   ** \return true if abs(x) > epsilon, false else.
   ** \pre x should be a real data type
   ** \par Example:
   ** \code
   ** float f1 = 0.001f;
   ** std::cout<<"slip::is_less_zero("<<f1<<") = "<<slip::is_less_zero(f1)<<std::endl;
   ** \endcode
   */
   template<typename T>
   bool is_less_zero(const T& x, const T& epsilon = static_cast<T>(1e-6))
  {
    return (x < -epsilon);
  }


  /**
   ** \brief is_less_equal_zero returns true if value x is near less equal to zero, false else.
   ** \since 1.5.0
   ** \param x value to evaluate.
   ** \param epsilon precision.
   ** \return true if abs(x) >= epsilon, false else.
   ** \pre x should be a real data type
   ** \par Example:
   ** \code
   ** float f1 = 0.001f;
   ** std::cout<<"slip::is_less_equal_zero("<<f1<<") = "<<slip::is_less_equal_zero(f1)<<std::endl;
   ** \endcode
   */
   template<typename T>
   bool is_less_equal_zero(const T& x, const T& epsilon = static_cast<T>(1e-6))
  {
    return (x <= -epsilon);
  }


 /**
   ** \brief is_zero functor. Return true if abs(x) < epsilon.
   ** \author Benoit Tremblais <tremblais_AT_univ-poitiers.fr>
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param T data type value.
   ** \par Example:
   ** \code
   ** double d1 = 0.001;
   ** std::cout<<"slip::is_zero_fun<double>("<<d1<<",1e-12) = "<<slip::is_zero_fun<double>()(d1,1e-12)<<std::endl;
   ** \endcode
   */

template<typename T> 
struct is_zero_fun : public std::binary_function<T,T,bool>
{
  /**
   ** \brief 
   ** \param x value to evaluate.
   ** \return true if abs(x) < epsilon, false else
   */
  bool operator() (const T& x, const T& epsilon)
  {
    return slip::is_zero(x,epsilon);
  }

};
  
  /**
   ** \struct constants
   ** \brief  A structure for numeric constants.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>: conceptor
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \par Examples:
   ** \code
   ** double pi = slip::constants<double>::pi();
   ** float two = slip::constants<float>::two();
   ** \endcode
   */
template<typename _Tp>
struct constants
{
  ///  Constant @f$ \pi @f$.
  static _Tp pi() throw()
  { return static_cast<_Tp>(3.1415926535897932384626433832795029L); }
  ///  Constant @f$ \pi / 2 @f$.
  static _Tp pi_2() throw()
  { return static_cast<_Tp>(1.5707963267948966192313216916397514L); }
  ///  Constant @f$ \pi / 3 @f$.
  static _Tp pi_3() throw()
  { return static_cast<_Tp>(1.0471975511965977461542144610931676L); }
  ///  Constant @f$ \pi / 4 @f$.
  static _Tp pi_4() throw()
  { return static_cast<_Tp>(0.7853981633974483096156608458198757L); }
  ///  Constant @f$ 1 / \pi @f$.
  static _Tp _1_pi() throw()
  { return static_cast<_Tp>(0.3183098861837906715377675267450287L); }
  ///  Constant @f$ 2 / \sqrt(\pi) @f$.
  static _Tp _2_sqrtpi() throw()
  { return static_cast<_Tp>(1.1283791670955125738961589031215452L); }
  ///  Constant @f$ 1) @f$.
  static _Tp one() throw()
  { return static_cast<_Tp>(1.0L); }
  ///  Constant @f$ 2) @f$.
  static _Tp two() throw()
  { return static_cast<_Tp>(2.0L); }
  ///  Constant @f$ 1/2) @f$.
  static _Tp half() throw()
  { return static_cast<_Tp>(0.5L); }
  ///  Constant @f$ \sqrt(2) @f$.
  static _Tp sqrt2() throw()
  { return static_cast<_Tp>(1.4142135623730950488016887242096981L); }
  ///  Constant @f$ \sqrt(3) @f$.
  static _Tp sqrt3() throw()
  { return static_cast<_Tp>(1.7320508075688772935274463415058723L); }
  ///  Constant @f$ \sqrt(\pi/2) @f$.
  static _Tp sqrtpi_2() throw()
  { return static_cast<_Tp>(1.2533141373155002512078826424055226L); }
  ///  Constant @f$ 1 / sqrt(2) @f$.
  static _Tp sqrt1_2() throw()
  { return static_cast<_Tp>(0.7071067811865475244008443621048490L); }
  ///  Constant @f$ \log(\pi) @f$.
  static _Tp lnpi() throw()
  { return static_cast<_Tp>(1.1447298858494001741434273513530587L); }
  ///  Constant Euler's constant @f$ \gamma_E @f$.
  static _Tp gamma_e() throw()
  { return static_cast<_Tp>(0.5772156649015328606065120900824024L); }
  ///  Constant Euler-Mascheroni @f$ e @f$
  static _Tp euler() throw()
  { return static_cast<_Tp>(2.7182818284590452353602874713526625L); }
};


/*!
  ** \brief sinus cardinal function: 
  ** given a 1d point \f$ x \f$, this routine returns a sinus cardinal value: \f$ y=f(x)=\frac{sin(\pi x)}{\pi x} \f$
  ** \author Mouhamed Hammoud <hammoud@sic.univ-poitiers.fr> : conceptor
  ** \date 2009/06/23
  ** \since 1.5.0
  ** \version 0.0.1
  ** \param _Tp 
  ** \return sinus cardinal value
  ** \remarks Will be delocated in macros.hpp in the future
  ** \par Example:
  ** \code 
  ** slip::Array<double> sincc(7);
  ** slip::Array<double> x(7);
  ** slip::iota(x.begin(),x.end(),-static_cast<double>(x.size())/static_cast<double>(2),static_cast<double>(x.size())/static_cast<double>(x.size()-1));
  ** std::cout<<"x = \n"<<x<<std::endl;
  ** std::transform(x.begin(),x.end(),sincc.begin(),slip::sinc<double>());
  ** std::cout<<"sinc_window = \n"<<sincc<<std::endl;
  ** \endcode
  **
 */

template <class _Tp>
struct sinc : public std::unary_function<_Tp, _Tp>
  {
    _Tp
    operator()(const _Tp& point) const
    { 
      if (point == _Tp()) 
    	{
	  return static_cast<_Tp>(1.0);
    	}
    	else
    	{
	  return std::sin(slip::constants<_Tp>::pi()*point)/(slip::constants<_Tp>::pi()*point);
	    }
     }
  };

  
} //::slip
#endif //SLIP_MACROS_HPP
