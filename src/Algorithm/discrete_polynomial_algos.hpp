



#ifndef SLIP_DISCRETE_POLYNOMIAL_ALGOS_HPP
#define SLIP_DISCRETE_POLYNOMIAL_ALGOS_HPP

#include <iostream>
#include <algorithm>
#include "Array.hpp"
#include "Vector.hpp"
#include "macros.hpp"

namespace slip
{

  /** 
   ** \enum BASE_REORTHOGONALIZATION_METHOD
   ** \brief Choose between different reorthogonalization methods.
   ** \code
   enum BASE_REORTHOGONALIZATION_METHOD
    {
    // No reorthogonalization
    BASE_REORTHOGONALIZATION_NONE,
    //Modified Gram-Schmidt reorthogonalization
    BASE_REORTHOGONALIZATION_GRAM_SCHMIDT;
    //Householder QR decomposition
    BASE_REORTHOGONALIZATION_QR_HOUSEHOLDER,
    //SVD reorthogonalization
    BASE_REORTHOGONALIZATION_SVD,
    //Proscrite reorthogonalization
    BASE_REORTHOGONALIZATION_PROCRUSTES
    
   };
    \endcode
*/
 enum BASE_REORTHOGONALIZATION_METHOD
    {
     BASE_REORTHOGONALIZATION_NONE,
     BASE_REORTHOGONALIZATION_GRAM_SCHMIDT,
     BASE_REORTHOGONALIZATION_QR_HOUSEHOLDER,
     BASE_REORTHOGONALIZATION_SVD,
     BASE_REORTHOGONALIZATION_PROCRUSTES
   };


  enum POLY_BASE_STORAGE
    {
      POLY_BASE_1D_RAM_STORAGE,
      POLY_BASE_ND_RAM_STORAGE,
      POLY_BASE_ND_FILE_STORAGE
    };


}//::slip


namespace slip
{
  template<typename RandomAccessIterator1,
	   typename RandomAccessIterator2,
	   typename T> 
  struct discrete_poly_inner_prod : 
    public std::binary_function<std::pair<RandomAccessIterator1,RandomAccessIterator1>, 
				std::pair<RandomAccessIterator2,RandomAccessIterator2>,T>
  {
    discrete_poly_inner_prod()
    {}
    T operator() (const std::pair<RandomAccessIterator1,RandomAccessIterator1>& Pi,
		  const std::pair<RandomAccessIterator2,RandomAccessIterator2>& Pj)
  {

    T result = T();
    RandomAccessIterator1 it_pi = Pi.first;
    RandomAccessIterator2 it_pj = Pj.first;
    for (; it_pi != Pi.second; 
	   ++it_pi, 
	   ++it_pj)
      {
	result += (*it_pi * *it_pj);
      }
    return result;

  }
  };

 template<typename RandomAccessIterator1,
	   typename RandomAccessIterator2,
	   typename T> 
  struct discrete_gram_inner_prod : 
    public std::binary_function<std::pair<RandomAccessIterator1,RandomAccessIterator1>, 
				std::pair<RandomAccessIterator2,RandomAccessIterator2>,T>
  {
    discrete_gram_inner_prod(const std::size_t N):
      one_o_N_(slip::constants<T>::one()/static_cast<T>(N))
    {}
    T operator() (const std::pair<RandomAccessIterator1,RandomAccessIterator1>& Pi,
		  const std::pair<RandomAccessIterator2,RandomAccessIterator2>& Pj)
  {

    T result = T();
    RandomAccessIterator1 it_pi = Pi.first;
    RandomAccessIterator2 it_pj = Pj.first;
    for (; it_pi != Pi.second; 
	   ++it_pi, 
	   ++it_pj)
      {
	result += (*it_pi * *it_pj);
      }
    return one_o_N_*result;

  }
    T one_o_N_;
  };


 template<typename RandomAccessIterator1,
	   typename RandomAccessIterator2,
	   typename T> 
  struct discrete_poly_wheighted_inner_prod : 
    public std::binary_function<std::pair<RandomAccessIterator1,RandomAccessIterator1>, 
				std::pair<RandomAccessIterator2,RandomAccessIterator2>,T>
  {
    discrete_poly_wheighted_inner_prod(const slip::Vector<T>* weight):
      weight_(const_cast<slip::Vector<T>*>(weight))
    {}
    T operator() (const std::pair<RandomAccessIterator1,RandomAccessIterator1>& Pi,
		  const std::pair<RandomAccessIterator2,RandomAccessIterator2>& Pj)
  {

    T result = T();
    typename slip::Vector<T>::const_iterator it_weight = weight_->begin();
    RandomAccessIterator1 it_pi = Pi.first;
    RandomAccessIterator2 it_pj = Pj.first;
    for (; it_pi != Pi.second; 
	   ++it_pi, 
	   ++it_pj,
	   ++it_weight)
      {
	result = result + *it_weight * (*it_pi * *it_pj);
      }
    return result;

  }
		  
 slip::Array<T>* weight_;


 };


}//::slip

#endif //SLIP_DISCRETE_POLYNOMIAL_ALGOS_HPP
