/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file stl_numeric_ext.hpp
 * 
 * \brief Provides some numeric extensions to STL algorithms.
 * 
 */
#ifndef SLIP_STL_NUMERIC_EXT_HPP
#define SLIP_STL_NUMERIC_EXT_HPP
#include <iostream>
#include <cassert>


namespace slip
{
 /**
  **  \brief  Compute inner product of two ranges according to a mask range.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
  ** \date 2008/09/27
  ** \since 1.0.0
  ** \version 0.0.1
  **  \param  first1  Start of range 1.
  **  \param  last1  End of range 1.
  **  \param mask_first Start of the mask range.
  **  \param  first2  Start of range 2.
  **  \param  init  Starting value to add other values to.
  **  \param  value true value of the mask range. Default is 1.
  **  \return  The final inner product.
  **  \pre [first1,last1) must be valid.
  **  \pre [mask_first, mask_first + (last1-first1)) must be valid.
  **  \par Description:
  **  Starting with an initial value of \a init, multiplies successive
  **  elements from the two ranges according to the mask range and adds 
  **  each product into the accumulated value using operator+().  
  **  The values in the ranges are processed in order.
  ** \par Example:
  ** \code
  **  slip::Array<int> v1(10);
  **  slip::iota(v1.begin(),v1.end(),1,1);
  **  std::cout<<"v1 = "<<v1<<std::endl;
  **  slip::Array<int> v2(v1);
  **  std::cout<<"v2 = "<<v2<<std::endl;
  **  slip::Array<bool> mask(v1.size());
  **  mask[2] = 1;
  **  mask[4] = 1;
  **  mask[9] = 1;
  **  std::cout<<"mask = "<<mask<<std::endl;
  **  std::cout<<slip::inner_product_mask(v1.begin(),v1.end(),
  **			      mask.begin(),
  **			      v2.begin(),int(1))<<std::endl;
  **
  **  std::cout<<slip::inner_product_mask(v1.begin(),v1.end(),
  **			      maskint.begin(),
  **			      v2.begin(),int(1),int(2))<<std::endl;
  ** \endcode
  ** 
  */
  template<typename _InputIterator1, typename _InputIterator2, 
	   typename _MaskIterator,
	   typename _Tp>
    _Tp
    inner_product_mask(_InputIterator1 first1, _InputIterator1 last1,
		       _MaskIterator mask_first,
		       _InputIterator2 first2, _Tp init,
		       typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
    {
      assert(first1 != last1);
      
      for (; first1 != last1; ++first1, ++first2, ++mask_first)
	{
	  if(*mask_first == value)
	    {
	      init = init + (*first1 * *first2);
	    }
	}
      return init;
    }



  /**
   **  \brief  Compute inner product of two ranges according to a mask range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2008/09/27
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1  Start of range 1.
   **  \param  last1  End of range 1.
   **  \param mask_first Start of the mask range.
   **  \param  first2  Start of range 2.
   **  \param  init  Starting value to add other values to.
   **  \param  binary_op1  Function object to accumulate with.
   **  \param  binary_op2  Function object to apply to pairs of input values.
   **  \param  value true value of the mask range. Default is 1.
   **  \return  The final inner product.
   **  \pre [first1,last1) must be valid.
   **  \pre [first2, first2 + (last1-first1)) must be valid.
   **  \pre [mask_first, mask_first + (last1-first1)) must be valid.
   ** \par Description:
   **  Starting with an initial value of \a init, applies \a binary_op2 to
   **  successive elements from the two ranges according to the mask range 
   **  and accumulates each result into the accumulated value using 
   **  \a binary_op1.  The values in the ranges are processed in order.
   ** \par Example:
  ** \code
  **  slip::Array<int> v1(10);
  **  slip::iota(v1.begin(),v1.end(),1,1);
  **  std::cout<<"v1 = "<<v1<<std::endl;
  **  slip::Array<int> v2(v1);
  **  std::cout<<"v2 = "<<v2<<std::endl;
  **  slip::Array<bool> mask(v1.size());
  **  mask[2] = 1;
  **  mask[4] = 1;
  **  mask[9] = 1;
  **  std::cout<<"mask = "<<mask<<std::endl;
  **  slip::Array<int> maskint(v1.size());
  **  maskint[2] = 2;
  **  maskint[4] = 2;
  **  maskint[8] = 2;
  **  std::cout<<"maskint = "<<maskint<<std::endl;
  ** std::cout<<slip::inner_product_mask(v1.begin(),v1.end(),
  **			      mask.begin(),
  **			      v2.begin(),
  **			      int(1),
  **			      std::plus<int>(),
  **			      std::multiplies<int>())<<std::endl;
  **  std::cout<<slip::inner_product_mask(v1.begin(),v1.end(),
  **			      maskint.begin(),
  **			      v2.begin(),
  **			      int(1),
  **			      std::plus<int>(),
  **			      std::multiplies<int>(),
  **			      int(2))<<std::endl;
  ** \endcode
  */
  template<typename _InputIterator1, typename _InputIterator2, 
	   typename _MaskIterator,
	   typename _Tp,
	   typename _BinaryOperation1, typename _BinaryOperation2>
    _Tp
    inner_product_mask(_InputIterator1 first1, _InputIterator1 last1,
		         _MaskIterator mask_first,
		  _InputIterator2 first2, _Tp init,
		  _BinaryOperation1 binary_op1,
		  _BinaryOperation2 binary_op2,
		  typename std::iterator_traits<_MaskIterator>::value_type value=typename std::iterator_traits<_MaskIterator>::value_type(1))
    {
      // concept requirements
      assert(first1 != last1);

      for (; first1 != last1; ++first1, ++first2, ++mask_first)
	{
	  if(*mask_first == value)
	    {
	      init = binary_op1(init, binary_op2(*first1, *first2));
	    }
	}
      return init;
    }




 /**
  **  \brief  Compute inner product of two ranges according to a Predicate.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
  ** \date 2008/09/27
  ** \since 1.0.0
  ** \version 0.0.1
  **
  **  \param  first1  Start of range 1.
  **  \param  last1  End of range 1.
  **  \param  first2  Start of range 2.
  **  \param  init  Starting value to add other values to.
  **  \param pred Predicate.  
  **  \return  The final inner product.
  **  \pre [first1,last1) must be valid.
  **  \pre [first2, first2 + (last1-first1)) must be valid.
  ** \par Description:
  **  Starting with an initial value of \a init, multiplies successive
  **  elements from the two ranges according to the predicate pred and adds 
  **  each product into the accumulated value using operator+().  
  **  The values in the ranges are processed in order.
  ** \par Example:
  ** \code
  **  template<typename T>
  **  bool lt5Predicate (const T& val)
  **  {
  **    return (val < T(5));
  **  }
  **  \endcode
  **  \code
  **  slip::Array<int> v1(10);
  **  slip::iota(v1.begin(),v1.end(),1,1);
  **  std::cout<<"v1 = "<<v1<<std::endl;
  **  slip::Array<int> v2(v1);
  **  std::cout<<"v2 = "<<v2<<std::endl;
  **  std::cout<<slip::inner_product_if(v1.begin(),v1.end(),
  **			    v2.begin(),
  **			    int(1),
  **			    lt5Predicate<int>)<<std::endl;
  ** \endcode
  ** 
  */
  template<typename _InputIterator1, typename _InputIterator2, 
	   typename _Predicate,
	   typename _Tp>
    _Tp
    inner_product_if(_InputIterator1 first1, _InputIterator1 last1,
		     _InputIterator2 first2, _Tp init,
		     _Predicate pred)
    {
      assert(first1 != last1);
      
      for (; first1 != last1; ++first1, ++first2)
	{
	  if(pred(*first1))
	    {
	      init = init + (*first1 * *first2);
	    }
	}
      return init;
    }

  /**
   **  \brief  Compute inner product of two ranges according to a Predicate.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2008/09/27
   ** \since 1.0.0
   ** \version 0.0.1
  
   **
   **  \param  first1  Start of range 1.
   **  \param  last1  End of range 1.
   **  \param  first2  Start of range 2.
   **  \param  init  Starting value to add other values to.
   **  \param  binary_op1  Function object to accumulate with.
   **  \param  binary_op2  Function object to apply to pairs of input values.
   **  \param pred Predicate.
   **  \return  The final inner product.
   **  \pre [first1,last1) must be valid.
   **  \pre [first2, first2 + (last1-first1)) must be valid.
   **  \par Description:
   **  Starting with an initial value of \a init, applies \a binary_op2 to
   **  successive elements from the two ranges according to the Predicate pred 
   **  and accumulates each result into the accumulated value using 
   **  \a binary_op1.  The values in the ranges are processed in order.
   ** \par Example:
   ** \code
   **  template<typename T>
   **  bool lt5Predicate (const T& val)
   **  {
   **    return (val < T(5));
   **  }
   **  \endcode
   **  \code
   **  slip::Array<int> v1(10);
   **  slip::iota(v1.begin(),v1.end(),1,1);
   **  std::cout<<"v1 = "<<v1<<std::endl;
   **  slip::Array<int> v2(v1);
   **  std::cout<<"v2 = "<<v2<<std::endl;
   **  std::cout<<slip::inner_product_if(v1.begin(),v1.end(),
   **			    v2.begin(),
   **			    int(1),
   **			    std::plus<int>(),
   **			    std::multiplies<int>(),
   **			    lt5Predicate<int>)<<std::endl;
   ** \endcode
   */
  template<typename _InputIterator1, typename _InputIterator2, 
	   typename _Tp,
	   typename _BinaryOperation1, typename _BinaryOperation2,
	   typename _Predicate>
    _Tp
    inner_product_if(_InputIterator1 first1, _InputIterator1 last1,
		     _InputIterator2 first2, _Tp init,
		     _BinaryOperation1 binary_op1,
		     _BinaryOperation2 binary_op2,
		     _Predicate pred)
    {
      // concept requirements
      assert(first1 != last1);

      for (; first1 != last1; ++first1, ++first2)
	{
	  if(pred(*first1))
	    {
	      init = binary_op1(init, binary_op2(*first1, *first2));
	    }
	}
      return init;
    }


}//slip::

#endif //SLIP_STL_NUMERIC_EXT_HPP
