/** 
 * \file colormap_algo.hpp
 * 
 * \brief Provides colormap generation algorithm
 * \since 1.5.0
 * 
 */
#ifndef COLORMAP_ALGO_HPP
#define COLORMAP_ALGO_HPP

#include <cmath>
#include <algorithm>
#include "histo.hpp"
#include "Array.hpp"
#include "Colormap.hpp"
#include "arithmetic_op.hpp"


namespace slip
{


 /*!
   ** \brief resize a colormap and calculate the rgb color following the summer color algorithm
   ** \param colormap the color we want to modify
   ** \param size_map the size the colormap will have after process
  */
  template<typename T>
  inline
  void summer(slip::Colormap<T>  &colorMap,
			   int size_map)
  {
	colorMap.resize(size_map, slip::Color<T>(T(0),T(0),T(0)));

	slip::Color<T> rgb;

	for (int i=0 ; i<size_map; ++i)
	{
		rgb.set_red(static_cast<T>(i)/ static_cast<T>((size_map-1)));
		rgb.set_green(T(0.5) + rgb.get_red() / T(2));
		rgb.set_blue(T(0.4));

                //colorMap.set_data_ind(rgb,i);
                colorMap[i] = rgb;
	}
  }


/*!
   ** \brief resize a colormap and calculate the rgb color following the autumn color algorithm
   ** \param colormap the color we want to modify
   ** \param size_map the size the colormap will have after process
  */
  template<typename T>
  inline
  void autumn(slip::Colormap<T>  &colorMap,
			   int size_map)
  {
	colorMap.resize(size_map, slip::Color<T>(T(0),T(0),T(0)));

	slip::Color<T> rgb;

	for (int i=0 ; i<size_map; ++i)
	{
		rgb.set_red(T(1));
		rgb.set_green(static_cast<T>(i)/ static_cast<T>((size_map-1)));
		rgb.set_blue(T(0));

//		colorMap.set_data_ind(rgb,i);
                colorMap[i] = rgb;
	}
  }



/*!
   ** \brief resize a colormap and calculate the rgb color following the pink color algorithm
   ** \param colormap the color we want to modify
   ** \param size_map the size the colormap will have after process
  */
  template<typename T>
  inline
  void pink(slip::Colormap<T>  &colorMap,
			   int size_map)
  {
	colorMap.resize(size_map, slip::Color<T>(T(0),T(0),T(0)));

	slip::Color<T> rgb;
	double step = static_cast<double>(1.0) / static_cast<double>((size_map-1));
	T step_value = T(step);

	slip::Array<T> array_iota(size_map);
	slip::iota(array_iota.begin(), array_iota.end(), T(0), step_value); 


	for (int i=0 ; i<size_map; ++i)
	{
		if (array_iota[i] < (T(3)/T(8)))
		{
			rgb.set_red(std::sqrt((T(14)/T(9))*T(array_iota[i])));
			rgb.set_green(std::sqrt((T(2)/T(3))*array_iota[i]));

		}
		

		if (array_iota[i] >= (T(3)/T(8)))
		{			
			rgb.set_red(std::sqrt((T(2)/T(3))*T(array_iota[i]) + (T(1)/T(3))));
		}

		if (array_iota[i] >= (T(3)/T(8)) && array_iota[i] < (T(3)/T(4)))
		{
			rgb.set_green(std::sqrt((T(14)/T(9))*array_iota[i] - (T(1)/T(3))));
		}

		if (array_iota[i] >= (T(3)/T(4)))
		{
			rgb.set_green(std::sqrt((T(2)/T(3))*array_iota[i] + (T(1)/T(3))));
			rgb.set_blue(std::sqrt(T(2)*array_iota[i] - T(1)));
		}

		if (array_iota[i] < (T(3)/T(4)))
		{
			rgb.set_blue(std::sqrt((T(2)/T(3))*array_iota[i]));
		}
		
                //colorMap.set_data_ind(rgb,i);
                colorMap[i] = rgb;
	}
  }


/*!
   ** \brief resize a colormap and calculate the rgb color following the bone color algorithm
   ** \param colormap the color we want to modify
   ** \param size_map the size the colormap will have after process
  */
 template<typename T>
  inline
  void bone(slip::Colormap<T>  &colorMap,
			   int size_map)
  {
	colorMap.resize(size_map, slip::Color<T>(T(0),T(0),T(0)));

	slip::Color<T> rgb;
	double step = static_cast<double>(1.0) / static_cast<double>((size_map-1));
	T step_value = T(step);

	slip::Array<T> array_iota(size_map);
	slip::iota(array_iota.begin(), array_iota.end(), T(0), step_value); 


	for (int i=0 ; i<size_map; ++i)
	{
		if (array_iota[i] < (T(3)/T(8)))
		{
			rgb.set_green((T(7)/T(8))*array_iota[i]);
			rgb.set_blue((T(29)/T(24))*array_iota[i]);
		}
		

		if (array_iota[i] >= (T(3)/T(8)))
		{			
			rgb.set_blue((T(7)/T(8))*T(array_iota[i]) + (T(1)/T(8)));
		}

		if (array_iota[i] >= (T(3)/T(8)) && array_iota[i] < (T(3)/T(4)))
		{
			rgb.set_green((T(29)/T(24))*array_iota[i] - (T(1)/T(8)));
		}

		if (array_iota[i] >= (T(3)/T(4)))
		{
			rgb.set_red((T(11)/T(8))*T(array_iota[i]) - (T(3)/T(8)));
			rgb.set_green((T(7)/T(8))*array_iota[i] + (T(1)/T(8)));
		}

		if (array_iota[i] < (T(3)/T(4)))
		{
			rgb.set_red((T(7)/T(8))*T(array_iota[i]));
		}
		
                //colorMap.set_data_ind(rgb,i);
                colorMap[i] = rgb;
	}
  }


/*!
   ** \brief resize a colormap and calculate the rgb color following the cool color algorithm
   ** \param colormap the color we want to modify
   ** \param size_map the size the colormap will have after process
  */
template<typename T>
  inline
  void cool(slip::Colormap<T>  &colorMap,
			   int size_map)
  {
	colorMap.resize(size_map, slip::Color<T>(T(0),T(0),T(0)));

	slip::Color<T> rgb;
	
	for (int i=0 ; i<size_map; ++i)
	{
		rgb.set_red(static_cast<T>(i)/ static_cast<T>((size_map-1)));
		rgb.set_green(T(1)-T(rgb.get_red()));
		rgb.set_blue(T(1));
		
                //colorMap.set_data_ind(rgb,i);
                colorMap[i] = rgb;
	}
  }


/*!
   ** \brief resize a colormap and calculate the rgb color following the jet color algorithm
   ** \param colormap the color we want to modify
   ** \param size_map the size the colormap will have after process
  */
  template<typename T>
  inline
  void jet(slip::Colormap<T>  &colorMap,
			   int size_map)
  {
	colorMap.resize(size_map, slip::Color<T>(T(0),T(0),T(0)));

	slip::Color<T> rgb;
	double step = static_cast<double>(1.0) / static_cast<double>((size_map-1));
	T step_value = T(step);

	slip::Array<T> array_iota(size_map);
	slip::iota(array_iota.begin(), array_iota.end(), T(0), step_value); 


	for (int i=0 ; i<size_map; ++i)
	{
		if (array_iota[i] >= (T(3)/T(8)) && array_iota[i] < (T(5)/T(8)))
		{
			rgb.set_red(T(4)*array_iota[i] - (T(3)/T(2)));
			rgb.set_green(T(1));
			rgb.set_blue(T(-4)*array_iota[i] + (T(5)/T(2)));
		}

		if (array_iota[i] >= (T(5)/T(8)) && array_iota[i] < (T(7)/T(8)))
		{
			rgb.set_red(T(1));
			rgb.set_green(T(-4)*array_iota[i] + (T(7)/T(2)));
			rgb.set_blue(T(0));
		}

		if (array_iota[i] >= (T(7)/T(8)))
		{
			rgb.set_red(T(-4)*array_iota[i] + (T(9)/T(2)));
			rgb.set_green(T(0));
			rgb.set_blue(T(0));
		}
		

		if (array_iota[i] >= (T(1)/T(8)) && array_iota[i] < (T(3)/T(8)))
		{
			rgb.set_green(T(4)*array_iota[i] - (T(1)/T(2)));
			rgb.set_blue(T(1));

		}


		if (array_iota[i] < (T(1)/T(8)))
		{
			rgb.set_blue(T(4)*array_iota[i] + (T(1)/T(2)));
		}
		
                //colorMap.set_data_ind(rgb,i);
                colorMap[i] = rgb;
	}
  }


/*!
   ** \brief resize a colormap and calculate the rgb color following the spring color algorithm
   ** \param colormap the color we want to modify
   ** \param size_map the size the colormap will have after process
  */
template<typename T>
  inline
  void spring(slip::Colormap<T>  &colorMap,
			   int size_map)
  {
	colorMap.resize(size_map);

	slip::Color<T> rgb;
	
	for (int i=0 ; i<size_map; ++i)
	{
		rgb.set_red(T(1));
		rgb.set_green(T(i)/T(size_map-1));
		rgb.set_blue(T(1) - T(rgb.get_green()));
		
                //colorMap.set_data_ind(rgb,i);
                colorMap[i] = rgb;
	}
  }

/*!
   ** \brief resize a colormap and calculate the rgb color following the winter color algorithm
   ** \param colormap the color we want to modify
   ** \param size_map the size the colormap will have after process
  */
template<typename T>
  inline
  void winter(slip::Colormap<T>  &colorMap,
			   int size_map)
  {
	colorMap.resize(size_map);

	slip::Color<T> rgb;
	
	for (int i=0 ; i<size_map; ++i)
	{
		rgb.set_red(T(0));
		rgb.set_green(T(i)/T(size_map-1));
		rgb.set_blue(T(1) - (T(rgb.get_green())/T(2)));
		
                //colorMap.set_data_ind(rgb,i);
                colorMap[i] = rgb;
	}
  }


/*!
   ** \brief resize a colormap and calculate the rgb color following the hot color algorithm
   ** \param colormap the color we want to modify
   ** \param size_map the size the colormap will have after process
  */
template<typename T>
  inline
  void hot(slip::Colormap<T>  &colorMap,
			   int size_map)
  {
	colorMap.resize(size_map, slip::Color<T>(T(0),T(0),T(0)));

	slip::Color<T> rgb;
	double step = static_cast<double>(1.0) / static_cast<double>((size_map-1));
	T step_value = T(step);

	slip::Array<T> array_iota(size_map);
	slip::iota(array_iota.begin(), array_iota.end(), T(0), step_value); 


	for (int i=0 ; i<size_map; ++i)
	{
		if (array_iota[i] < (T(2)/T(5)))
		{
			rgb.set_red((T(5)/T(2))*array_iota[i]);
		}

		if (array_iota[i] >= (T(2)/T(5)))
		{
			rgb.set_red(T(1));
		}

		if (array_iota[i] >= (T(2)/T(5)) && array_iota[i] < (T(4)/T(5)))
		{
			rgb.set_green((T(5)/T(2))*array_iota[i] - T(1));
		}

		if (array_iota[i] >= (T(4)/T(5)))
		{
			rgb.set_green(T(1));
			rgb.set_blue(T(5)*array_iota[i] - T(4));
		}

		
		
                //colorMap.set_data_ind(rgb,i);
                colorMap[i] = rgb;
	}
  }

/*!
   ** \brief resize a colormap and calculate the rgb color following the rainbow color algorithm
   ** \param colormap the color we want to modify
   ** \param size_map the size the colormap will have after process
  */
template<typename T>
  inline
  void rainbow(slip::Colormap<T> &colorMap,
			   int size_map)
  {
	colorMap.resize(size_map, slip::Color<T>(T(0),T(0),T(0)));

	slip::Color<T> rgb;
	double step = static_cast<double>(1.0) / static_cast<double>((size_map-1));
	T step_value = T(step);

	slip::Array<T> array_iota(size_map);
	slip::iota(array_iota.begin(), array_iota.end(), T(0), step_value); 


	for (int i=0 ; i<size_map; ++i)
	{
		if (array_iota[i] < (T(2)/T(5)))
		{
			rgb.set_red(T(1));
			rgb.set_green((T(5)/T(2))*array_iota[i] );
		}

		if (array_iota[i] >= (T(2)/T(5)) && array_iota[i] < (T(3)/T(5)))
		{
			rgb.set_red(T(-5)*array_iota[i] + T(3));
			rgb.set_green(T(1));
		}


		if (array_iota[i] >= (T(3)/T(5)) && array_iota[i] < (T(4)/T(5)))
		{
			rgb.set_green(T(-5)*array_iota[i] + T(4));
			rgb.set_blue(T(5)*array_iota[i] - T(3));

		}

		if (array_iota[i] >= (T(4)/T(5)))
		{
			rgb.set_red((T(10)/T(3))*array_iota[i] - (T(8)/T(3)));
			rgb.set_green(T(0));
			rgb.set_blue(T(1));
		}

		
		
                //colorMap.set_data_ind(rgb,i);
                colorMap[i] = rgb;
	}
  }



/*!
   ** \brief resize a colormap and calculate the rgb color following the copper color algorithm
   ** \param colormap the color we want to modify
   ** \param size_map the size the colormap will have after process
  */
template<typename T>
  inline
  void copper(slip::Colormap<T>  &colorMap,
			   int size_map)
  {
	colorMap.resize(size_map, slip::Color<T>(T(0),T(0),T(0)));

	slip::Color<T> rgb;
	double step = static_cast<double>(1.0) / static_cast<double>((size_map-1));
	T step_value = T(step);

	slip::Array<T> array_iota(size_map);
	slip::iota(array_iota.begin(), array_iota.end(), T(0), step_value); 


	for (int i=0 ; i<size_map; ++i)
	{
		if (array_iota[i] < (T(4)/T(5)))
		{
			rgb.set_red((T(5)/T(4))*array_iota[i]);

		}

		if (array_iota[i] >= (T(4)/T(5)))
		{
			rgb.set_red(T(1));
		}

		rgb.set_green((T(4)/T(5)) * array_iota[i]);	
		rgb.set_blue((T(1)/T(2)) * array_iota[i]);

                //colorMap.set_data_ind(rgb,i);
                colorMap[i] = rgb;
	}
  }
  /*!
  **   \brief Create a colormap to vizualise a color image of optical flow.
  **   It is used by serval optical flow challenge web site.
  **   The relative lengths of color transitions:
  **  these are chosen based on perceptual similarity
  **  (e.g. one can distinguish more shades between red and yellow 
  **  than between yellow and green)
  **   The following code is adapted from 
  **   http://vision.middlebury.edu/flow/code/flow-code/colorcode.cpp
  **  \param colorMap computed colormap
  **  \post slip::Color<T> values of the colormap are in the range [0-255][0-255][0-255]
  */
template<typename T>
inline
void perceptual_optical_flow_colormap(slip::Colormap<T>& colorMap)
{
   // relative lengths of color transitions:
    // these are chosen based on perceptual similarity
    // (e.g. one can distinguish more shades between red and yellow 
    //  than between yellow and green)
  const std::size_t RY = 15;
  const std::size_t YG = 6;
  const std::size_t GC = 4;
  const std::size_t CB = 11;
  const std::size_t BM = 13;
  const std::size_t MR = 6;
  const std::size_t ncols = RY + YG + GC + CB + BM + MR;
  colorMap.resize(ncols,slip::Color<T>(T(),T(),T()));
  
  std::size_t k = 0;
  for (std::size_t i = 0; i < RY; ++i, ++k) 
    {
      colorMap[k] = slip::Color<T>(T(255),T(255*i/RY),T());
    }
  for(std::size_t i = 0; i < YG; ++i, ++k)
    {
      colorMap[k] = slip::Color<T>(T(255-255*i/YG), T(255),T());
    }
  for(std::size_t i = 0; i < GC; ++i, ++k) 
    {
      colorMap[k] = slip::Color<T>(T(),T(255),T(255*i/GC));
    }
  for(std::size_t i = 0; i < CB; ++i, ++k) 
    {
      colorMap[k] = slip::Color<T>(T(),T(255-255*i/CB),T(255));
    }
  for (std::size_t i = 0; i < BM; ++i, ++k)
    {
      colorMap[k] = slip::Color<T>(T(255*i/BM),T(),T(255));
    }
  for (std::size_t i = 0; i < MR; ++i, ++k) 
    {
      colorMap[k] = slip::Color<T>(T(255),T(),T(255-255*i/MR));
    }


}

}//::slip


#endif //SLIP_COLORMAP_ALGO_HPP













