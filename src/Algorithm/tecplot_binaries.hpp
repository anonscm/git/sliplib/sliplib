/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */
/** 
 * \file tecplot_binaries.hpp
 * 
 * \brief Provides some tecplot binary input/ouput algorithms.
 * \since 1.4.0
 */
#ifndef SLIP_TECPLOT_BINARIES_HPP
#define SLIP_TECPLOT_BINARIES_HPP
#include <typeinfo>
#include <vector>
#include <string>
#include <fstream>
#include <utility>
#include "Vector2d.hpp"
#include "Vector3d.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "Point2d.hpp"
#include "Point3d.hpp"

namespace slip
{

 template <typename T>
	class GenericComparator
	{
	public:
		GenericComparator(int s_) : s(s_) {};
		bool operator()(const T &i,const T &j) const
		{
			return i[s]<j[s];
		}
		int s;
	};

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2014/03/19
** \brief Convert the type of a variable to an integer using Tecplot conventions.
** \param a The variable to get the Tecplot type.
** \par Example:
** \code
** std::cout<<"The Tecplot type for double is : "<<get_tecplot_type<double>()<<std::endl;
** \endcode
*/
template<typename T>
int get_tecplot_type()
{
	int type=0;
	if (typeid(T)==typeid((float) 0.))
		type=1;
	else if (typeid(T)==typeid((double) 0.))
		type=2;
	else if (typeid(T)==typeid((int) 0.))
		type=3;
	else if (typeid(T)==typeid((short int) 0.))
		type=4;
	else if (typeid(T)==typeid((char) 0.))
		type=5;
	else
		type=6;
	return type;
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a binary tecplot file, move the stream pointer and return header informations.
** \param fp Input stream to the file path name.
** \par Example:
** \code
** // Read the header of the "test_3D.plt" file and get the file type (Full, Grid only or Solution only).
** std::string file_name="test_3D.plt";
** std::ifstream fp (file_name.c_str(), std::ios::in|std::ios::binary);
** std::cout<<"The file order is :"<< slip::plt_get_order(fp) <<std::endl;
** \endcode
*/
int plt_get_order(std::ifstream &fp)
{
	if (!fp.is_open())
	{
		std::cerr<<"Error in the \"plt_read_header\" function : the file is not opened"<<std::endl;
		return -1;
	}
	long pos = fp.tellg();
	fp.seekg (0);
// Read tecplot file version
	for (size_t i = 0; i < 8; ++i)
	{
		char tmp;
		fp.get(tmp);
	}

// Read order (???)
	int Order=-1;
	{
		int *tmp=new int;
		fp.read((char*)tmp, sizeof(int));
		Order=*tmp;
		delete tmp;
	}

	fp.seekg (pos);
	return Order;
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a binary tecplot file, move the stream pointer and return header informations.
** \param fp Input stream to the file path name.
** \par Example:
** \code
** // Read the header of the "test_3D.plt" file and get the file type (Full, Grid only or Solution only).
** std::string file_name="test_3D.plt";
** std::cout<<"The file order is :"<< slip::plt_get_order(file_name) <<std::endl;
** \endcode
*/
int plt_get_order(const std::string & file_path_name)
{
	std::ifstream fp (file_path_name.c_str(), std::ios::in|std::ios::binary);
	return plt_get_order(fp);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a binary tecplot file, move the stream pointer and return header informations.
** \param fp Input stream to the file path name.
** \par Example:
** \code
** // Read the header of the "test_3D.plt" file and get the file type (Full, Grid only or Solution only).
** std::string file_name="test_3D.plt";
** std::ifstream fp (file_name.c_str(), std::ios::in|std::ios::binary);
** std::cout<<"The file type is :"<<std::flush;
** switch( slip::plt_get_file_type(fp) );
** {
** 		case 0:
			std::cout<<"Full"<<std::endl;
			break;
		case 1:
			std::cout<<"Grid only"<<std::endl;
			break;
		case 2:
			std::cout<<"Solution only"<<std::endl;
			break;
		default:
			std::cout<<"Bad type"<<std::endl;
			break;
** }
** \endcode
*/
int plt_get_file_type(std::ifstream &fp)
{
	if (!fp.is_open())
	{
		std::cerr<<"Error in the \"plt_read_header\" function : the file is not opened"<<std::endl;
		return -1;
	}
	long pos = fp.tellg();
	fp.seekg (0);
// Read tecplot file version
	for (size_t i = 0; i < 8; ++i)
	{
		char tmp;
		fp.get(tmp);
	}

// Read order (???)
	{
		int *tmp=new int;
		fp.read((char*)tmp, sizeof(int));
		delete tmp;
	}

// Read file type
	int Type=-1;
	{
		int *tmp=new int;
		fp.read((char*)tmp, sizeof(int));
		Type=*tmp;
		delete tmp;
	}

	fp.seekg (pos);
	return Type;
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a binary tecplot file, move the stream pointer and return header informations.
** \param fp Input stream to the file path name.
** \par Example:
** \code
** // Read the header of the "test_3D.plt" file and get the file type (Full, Grid only or Solution only).
** std::string file_name="test_3D.plt";
** std::cout<<"The file type is :"<<std::flush;
** switch( slip::plt_get_file_type(file_name) );
** {
** 		case 0:
			std::cout<<"Full"<<std::endl;
			break;
		case 1:
			std::cout<<"Grid only"<<std::endl;
			break;
		case 2:
			std::cout<<"Solution only"<<std::endl;
			break;
		default:
			std::cout<<"Bad type"<<std::endl;
			break;
** }
** \endcode
*/
int plt_get_file_type(const std::string & file_path_name)
{
	std::ifstream fp (file_path_name.c_str(), std::ios::in|std::ios::binary);
	return plt_get_file_type(fp);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a binary tecplot file, move the stream pointer and return header informations.
** \param fp Input stream to the file path name.
** \param NbVars Number of variables.
** \param NbZones Number of zones.
** \param K Number of slices.
** \param I Number of rows.
** \param J Number of cols.
** \param title String for the frame title.
** \param title String for the zone name.
** \param varnames Vector containing the variable names.
** \param SolTime Solution Time.
** \par Example:
** \code
** // Read the header of the "test_3D.plt" file and get the number of variables, the number of zones, K, I, J and the Solution Time.
** std::string file_name="test_3D.plt";
** std::ifstream fp (file_name.c_str(), std::ios::in|std::ios::binary);
** int NbVars, NbZones, Kmax, Imax, Jmax, NumZone=1;
** std::string title, zonename;
** std::vector<std::string> varnames;
** double SolTime;
** slip::plt_read_header(fp, NbVars, NbZones, Kmax, Imax, Jmax, title, zonename, varnames, SolTime, NumZone);
** std::cout<<"The file title is : \""<<title<<"\" and the zone name is \""<<zonename<<"\""<<std::endl;
** std::cout<<"The file contains : "<<NbVars<<" variables and "<<NbZones<<" zones."<<std::endl;
** std::cout<<"The variable names are : "<<std::endl;
** for (size_t i=0; i<NbVars; ++i)
**   std::cout<<varnames[i]<<std::endl;
** std::cout<<"The dimensions of the zone number "<<NumZone<<" : "<<Kmax<<", "<<Imax<<", "<<Jmax<<std::endl;
** std::cout<<"The Solution Time is : "<<SolTime<<std::endl;
** \endcode
*/
void plt_read_header(std::ifstream &fp, int &NbVars, int &NbZones, int &K, int &I, int &J, std::string &title, std::string &zonename, std::vector<std::string> &varnames, double &SolTime, int NumZone=1)
{
	title.clear();
	zonename.clear();
	varnames.clear();
	NbVars=0;
	NbZones=0;
	K=0;
	I=0;
	J=0;
	SolTime=0.;
	if (NumZone<1)
		NumZone=1;
	if (!fp.is_open())
	{
		std::cerr<<"Error in the \"plt_read_header\" function : the file is not opened"<<std::endl;
		return;
	}
/****************************************************************************/
/********************************** HEADER **********************************/
/****************************************************************************/
// Read tecplot file version
	for (size_t i = 0; i < 8; ++i)
	{
		char tmp;
		fp.get(tmp);
	}

// Read order (???)
	{
		int *tmp=new int;
		fp.read((char*)tmp, sizeof(int));
		delete tmp;
	}

// Read file type
	// int Type=0; // Not used
	{
		int *tmp=new int;
		fp.read((char*)tmp, sizeof(int));
		// Type=*tmp;
		delete tmp;
	}

// read title
	{
		int *tmp=new int;
		*tmp=-10;
		int i=0;
		while (*tmp!=0)
		{
			fp.read((char*)tmp, sizeof(int));
			title.append((char*)tmp);
			++i;
			if (i>255) break;
		}
		delete tmp;
	}

// Read the number of variables
	{
		int *tmp=new int;
		fp.read((char*)tmp, sizeof(int));
		NbVars=*tmp;
		delete tmp;
	}

// Read the variables name
	{
		for (int i = 0; i < NbVars; ++i)
		{
			int *tmp=new int;
			*tmp=-10;
			int j=0;
			std::string nom_temp;
			while (*tmp!=0)
			{
				fp.read((char*)tmp, sizeof(int));
				nom_temp.append((char*)tmp);
				++j;
				if (j>255) break;
			}
			varnames.push_back(nom_temp);
			delete tmp;
		}
	}

// Read zone parameters
	int ZoneType=0;
	NbZones=0;
	// int size=0; // Not used
	I=1;
	J=1;
	K=1;
	float *marker=new float;
	*marker=-1.;
	while ((*marker!=357.0) && (*marker!=399.0))
	{
// Read zone marker
		if (*marker!=357.0)
		{
			float *tmp=new float;
			*tmp=-10;
			int i=0;
			while (*tmp!=299.0)
			{
				fp.read((char*)tmp, sizeof(float));
				++i;
				if (i>255) break;
				if (*tmp==357.0)
				{
					*marker=357.0;
					break;
				}
			}
			if (*tmp==299.0)
			{
				++NbZones;
			}
			delete tmp;
		}

		if (*marker!=357.0)
		{
// Read zone name
			{
				int *tmp=new int;
				*tmp=-10;
				int i=0;
				while (*tmp!=0)
				{
					fp.read((char*)tmp, sizeof(int));
					if (NbZones==NumZone)
						zonename.append((char*)tmp);
					++i;
					if (i>255) break;
					if (*tmp==357.0)
					{
						*marker=357.0;
						break;
					}
				}
				delete tmp;
			}

// Read Parent zone
			{
				int *tmp=new int;
				fp.read((char*)tmp, sizeof(int));
				if (*tmp==357.0)
				{
					*marker=357.0;
					break;
				}
				delete tmp;
			}

// Read StrandID
			{
				int *tmp=new int;
				fp.read((char*)tmp, sizeof(int));
				if (*tmp==357.0)
				{
					*marker=357.0;
					break;
				}
				delete tmp;
			}

// Read Solution time
			{
				double *tmp=new double;
				fp.read((char*)tmp, sizeof(double));
				SolTime=*tmp;
				if (*tmp==357.0)
				{
					*marker=357.0;
					break;
				}
				delete tmp;
			}

// Read useless -1
			{
				int *tmp=new int;
				fp.read((char*)tmp, sizeof(int));
				if (*tmp==357.0)
				{
					*marker=357.0;
					break;
				}
				else if (*tmp!=-1)
				{
					std::cerr<<"Reading error in plt_to_array"<<std::endl;
					break;
				}
				delete tmp;
			}

// Read ZoneType
			{
				int *tmp=new int;
				fp.read((char*)tmp, sizeof(int));
				ZoneType=*tmp;
				if (*tmp==357.0)
				{
					*marker=357.0;
					break;
				}
				delete tmp;
			}

// Read Specify Var Location
			{
				int *tmp=new int;
				fp.read((char*)tmp, sizeof(int));
				if (*tmp==357.0)
				{
					*marker=357.0;
					break;
				}
				else if (*tmp==1)
				{
					for (int z=0; z<NbVars; ++z)
					{
						fp.read((char*)tmp, sizeof(int));
						if (*tmp==357.0)
						{
							*marker=357.0;
							break;
						}
					}
				}
				delete tmp;
			}

// Read Supplied Face Neighboring
			{
				int *tmp=new int;
				fp.read((char*)tmp, sizeof(int));
				if (*tmp==357.0)
				{
					*marker=357.0;
					break;
				}
				delete tmp;
			}

// Read User Defined Face Connections
			{
				int *tmp=new int;
				fp.read((char*)tmp, sizeof(int));
				if (*tmp==357.0)
				{
					*marker=357.0;
					break;
				}
				else if (*tmp==1)
				{
					int *tmp2=new int;
					fp.read((char*)tmp2, sizeof(int));
					if (*tmp2==357.0)
					{
						*marker=357.0;
						break;
					}
					delete tmp2;
				}
				delete tmp;
			}

// Read Finite Element Face Neighbors
			if (ZoneType!=0)
			{
				int *tmp=new int;
				fp.read((char*)tmp, sizeof(int));
				if (*tmp==357.0)
				{
					*marker=357.0;
					break;
				}
				delete tmp;
			}
// Read KMax, IMax, JMax
			else
			{
				int *IMax=new int, *JMax=new int, *KMax=new int;
				fp.read((char*)JMax, sizeof(int));
				fp.read((char*)IMax, sizeof(int));
				fp.read((char*)KMax, sizeof(int));
				// size=(*IMax)*(*JMax)*(*KMax);
				if (NbZones==NumZone)
				{
					K=*KMax;
					I=*IMax;
					J=*JMax;
				}
				delete IMax;
				delete JMax;
				delete KMax;
			}

// Read Auxiliary Name
			{
				int *tmp=new int;
				fp.read((char*)tmp, sizeof(int));
				if (*tmp==357.0)
				{
					*marker=357.0;
					break;
				}
				delete tmp;
			}
		}
	}
	delete marker;
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a binary tecplot file and return header informations.
** \param fp Input stream to the file.
** \param NbVars Number of variables.
** \param NbZones Number of zones.
** \param K Number of slices.
** \param I Number of rows.
** \param J Number of cols.
** \param NumZone Index of the zone for which the I, J and K are extracted. This parameter is optional and the first zone is extracted by default.
** \par Example:
** \code
** // Read the header of the "test_3D.plt" file and get the number of variables, the number of zones, K, I and J.
** std::string file_name="test_3D.plt";
** std::ifstream fp (file_name.c_str(), std::ios::in|std::ios::binary);
** int NbVars, NbZones, Kmax, Imax, Jmax, NumZone=1;
** slip::plt_read_header(fp, NbVars, NbZones, Kmax, Imax, Jmax, NumZone);
** std::cout<<"The file contains : "<<NbVars<<" variables and "<<NbZones<<" zones."<<std::endl;
** std::cout<<"The dimensions of the zone number "<<NumZone<<" : "<<Kmax<<", "<<Imax<<", "<<Jmax<<std::endl;
** \endcode
*/
void plt_read_header(std::ifstream &fp, int &NbVars, int &NbZones, int &K, int &I, int &J, int NumZone=1)
{
	std::string _useless_string;
	std::vector<std::string> _useless_varnames;
	double SolTime;
	plt_read_header(fp, NbVars, NbZones, K, I, J, _useless_string, _useless_string, _useless_varnames, SolTime, NumZone);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a binary tecplot file and return header informations.
** \param file_path_name String of the file path name.
** \param NbVars Number of variables.
** \param NbZones Number of zones.
** \param K Number of slices.
** \param I Number of rows.
** \param J Number of cols.
** \param NumZone Index of the zone for which the I, J and K are extracted. This parameter is optional and the first zone is extracted by default.
** \par Example:
** \code
** // Read the header of the "test_3D.plt" file and get the number of variables, the number of zones, K, I and J.
** std::string file_name="test_3D.plt";
** int NbVars, NbZones, Kmax, Imax, Jmax, NumZone=1;
** slip::plt_read_header(file_name, NbVars, NbZones, Kmax, Imax, Jmax, NumZone);
** std::cout<<"The file contains : "<<NbVars<<" variables and "<<NbZones<<" zones."<<std::endl;
** std::cout<<"The dimensions of the zone number "<<NumZone<<" : "<<Kmax<<", "<<Imax<<", "<<Jmax<<std::endl;
** \endcode
*/
void plt_read_header(const std::string & file_path_name, int &NbVars, int &NbZones, int &K, int &I, int &J, int NumZone=1)
{
	double SolTime;
	std::string _useless_string;
	std::vector<std::string> _useless_varnames;
	std::ifstream fp (file_path_name.c_str(), std::ios::in|std::ios::binary);
	plt_read_header(fp, NbVars, NbZones, K, I, J, _useless_string, _useless_string, _useless_varnames, SolTime, NumZone);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a binary tecplot file and return header informations.
** \param file_path_name String of the file path name.
** \param NbVars Number of variables.
** \param NbZones Number of zones.
** \param K Number of slices.
** \param I Number of rows.
** \param J Number of cols.
** \param SolTime Solution time.
** \param NumZone Index of the zone for which the I, J and K are extracted. This parameter is optional and the first zone is extracted by default.
** \par Example:
** \code
** // Read the header of the "test_3D.plt" file and get the number of variables, the number of zones, K, I, J and the Solution Time.
** std::string file_name="test_3D.plt";
** int NbVars, NbZones, Kmax, Imax, Jmax, NumZone=1;
** double SolTime;
** slip::plt_read_header(file_name, NbVars, NbZones, Kmax, Imax, Jmax, SolTime, NumZone);
** std::cout<<"The file contains : "<<NbVars<<" variables and "<<NbZones<<" zones."<<std::endl;
** std::cout<<"The dimensions of the zone number "<<NumZone<<" : "<<Kmax<<", "<<Imax<<", "<<Jmax<<std::endl;
** std::cout<<"The Solution Time is : "<<SolTime<<std::endl;
** \endcode
*/
void plt_read_header(const std::string & file_path_name, int &NbVars, int &NbZones, int &K, int &I, int &J, double &SolTime, int NumZone=1)
{
	std::string _useless_string;
	std::vector<std::string> _useless_varnames;
	std::ifstream fp (file_path_name.c_str(), std::ios::in|std::ios::binary);
	plt_read_header(fp, NbVars, NbZones, K, I, J, _useless_string, _useless_string, _useless_varnames, SolTime, NumZone);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a binary tecplot file and return header informations.
** \param file_path_name String of the file path name.
** \param NbVars Number of variables.
** \param NbZones Number of zones.
** \param K Number of slices.
** \param I Number of rows.
** \param J Number of cols.
** \param SolTime Solution time.
** \param NumZone Index of the zone for which the I, J and K are extracted. This parameter is optional and the first zone is extracted by default.
** \par Example:
** \code
** // Read the header of the "test_3D.plt" file and get the number of variables, the number of zones, K, I, J and the Solution Time.
** std::string file_name="test_3D.plt";
** int NbVars, NbZones, Kmax, Imax, Jmax, NumZone=1;
** std::string title, zonename;
** std::vector<std::string> varnames;
** double SolTime;
** slip::plt_read_header(file_name, NbVars, NbZones, Kmax, Imax, Jmax, title, zonename, varnames, SolTime, NumZone);
** std::cout<<"The file title is : \""<<title<<"\" and the zone name is \""<<zonename<<"\""<<std::endl;
** std::cout<<"The file contains : "<<NbVars<<" variables and "<<NbZones<<" zones."<<std::endl;
** std::cout<<"The variable names are : "<<std::endl;
** for (size_t i=0; i<NbVars; ++i)
**   std::cout<<varnames[i]<<std::endl;
** std::cout<<"The dimensions of the zone number "<<NumZone<<" : "<<Kmax<<", "<<Imax<<", "<<Jmax<<std::endl;
** std::cout<<"The Solution Time is : "<<SolTime<<std::endl;
** \endcode
*/
void plt_read_header(const std::string & file_path_name, int &NbVars, int &NbZones, int &K, int &I, int &J, std::string &title, std::string &zonename, std::vector<std::string> &varnames, double &SolTime, int NumZone=1)
{
	std::ifstream fp (file_path_name.c_str(), std::ios::in|std::ios::binary);
	plt_read_header(fp, NbVars, NbZones, K, I, J, title, zonename, varnames, SolTime, NumZone);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a binary tecplot file and put the data in a 2d container.
** \param file_path_name String of the file path name.
** \param reg Container2d for the read data.
** \param zone_loaded Zone index to read (1 by default).
** \par Example:
** \code
** // Read the "test_3D.plt" file and put the data in the container2d Reg.
** slip::Array2d<double> Reg;
** std::string file_name="test_3D.plt";
** plt_to_Array2d(file_name, Reg);
** std::cout<<"The dimensions of the array are "<<Reg.dim1()<<", "<<Reg.dim2()<<std::endl;
** \endcode
*/
template<typename Container2d>
void plt_to_Array2d(const std::string & file_path_name,
	Container2d & reg,
	const int zone_loaded=1)
{
	typedef typename Container2d::value_type T;

	std::ifstream fp (file_path_name.c_str(), std::ios::in|std::ios::binary);
	int ZoneLoaded = zone_loaded;
/****************************************************************************/
/********************************** HEADER **********************************/
/****************************************************************************/
	int nbvars=0;
	int nbzones=0, size=0;
	int Imax=1, Jmax=1, Kmax=1;
	plt_read_header(fp, nbvars, nbzones, Kmax, Imax, Jmax, ZoneLoaded);
	size=Imax*Jmax*Kmax;
/****************************************************************************/
/*********************************** DATA ***********************************/
/****************************************************************************/
	reg.resize(size,nbvars);
	if (ZoneLoaded>nbzones)
	{
		ZoneLoaded=nbzones;
	}
	if (ZoneLoaded<1)
	{
		ZoneLoaded=1;
	}
//	Read zones data
	for (int NumZone = 1; NumZone <= nbzones; ++NumZone)
	{
		int *datatype=new int[nbvars];
		{
			float *tmp=new float;
			fp.read((char*)tmp, sizeof(float));
			delete tmp;
		}

// Read data type
		for (int i = 0; i < nbvars; ++i)
		{
			int *tmp=new int;
			fp.read((char*)tmp, sizeof(int));
			datatype[i]=*tmp;
			delete tmp;
		}

// Read useless things
		for (size_t i = 0; i < 3; ++i)
		{
			int *tmp=new int;
			fp.read((char*)tmp, sizeof(int));
			if ((i==0||i==1)&&(*tmp)!=0)
			{
				for (int j = 0; j < nbvars; ++j)
				{
					fp.read((char*)tmp, sizeof(int));
				}
			}
			delete tmp;
		}

// Read variable min et max value
		for (int i = 0; i < nbvars; ++i)
		{
			double *min=new double, *max=new double;
			fp.read((char*)min, sizeof(double));
			fp.read((char*)max, sizeof(double));
			delete min;
			delete max;
		}
		if (NumZone!=ZoneLoaded)
		{
			for (int i = 0; i < nbvars; ++i)
			{
				switch (datatype[i])
				{
					case 1:
					fp.seekg((size)*sizeof(float), std::ios::cur);
					break;
					case 2:
					fp.seekg((size)*sizeof(double), std::ios::cur);
					break;
					case 3:
					fp.seekg((size)*sizeof(int), std::ios::cur);
					break;
					case 4:
					fp.seekg((size)*sizeof(short int), std::ios::cur);
					break;
					case 5:
					case 6:
					fp.seekg((size)*sizeof(char), std::ios::cur);
					break;
					default:
					break;
				}
			}
		}
		else
		{
			for (int i = 0; i < nbvars; ++i)
			{
				switch (datatype[i])
				{
					case 1:
					{
						float *tmp=new float[size];
						fp.read((char*)tmp, size*sizeof(float));
						for (int ii = 0; ii < size; ii++)
							reg(ii,i)=static_cast<T>(*(tmp+ii));
						delete[] tmp;
						break;
					}
					case 2:
					{
						double *tmp=new double[size];
						fp.read((char*)tmp, size*sizeof(double));
						for (int ii = 0; ii < size; ii++)
							reg(ii,i)=static_cast<T>(*(tmp+ii));
						delete[] tmp;
						break;
					}
					case 3:
					{
						int *tmp=new int[size];
						fp.read((char*)tmp, size*sizeof(int));
						for (int ii = 0; ii < size; ii++)
							reg(ii,i)=static_cast<T>(*(tmp+ii));
						delete[] tmp;
						break;
					}
					case 4:
					{
						short int *tmp=new short int[size];
						fp.read((char*)tmp, size*sizeof(short int));
						for (int ii = 0; ii < size; ii++)
							reg(ii,i)=static_cast<T>(*(tmp+ii));
						delete[] tmp;
						break;
					}
					case 5:
					case 6:
					{
						char *tmp=new char[size];
						fp.read((char*)tmp, size*sizeof(char));
						for (int ii = 0; ii < size; ii++)
							reg(ii,i)=static_cast<T>(*(tmp+ii));
						delete[] tmp;
						break;
					}
				}
			}
		}
		delete[] datatype;
	}
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a slip::RegularVector3dField3d from a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector3dField3d container.
** \param title Title of the tecplot frame.
** \param zonename Name of the tecplot zone.
** \param varnames Name of all the variables.
** \param zone_loaded Zone index to read (1 by default).
** \bug Warning : data must be properly sorted. If you are not sure they are, use generic_plt_to_RegularVector3dField3d().
** \par Example:
** \code
** // Read the "test_3D.plt" file, put the data in the container3d Reg and get the title, zone name and variable names.
** slip::RegularVector3dField3d<double> Reg;
** std::string file_name="test_3D.plt", title, zonename;
** std::vector<std::string> varnames;
** plt_to_RegularVector3dField3d(file_name, Reg, title, zonename, varnames);
** std::cout<<"The file title is : \""<<title<<"\" and the zone name is \""<<zonename<<"\""<<std::endl;
** std::cout<<"The variable names are : "<<std::endl;
** for (size_t i=0; i<varnames.size(); ++i)
**   std::cout<<varnames[i]<<std::endl;
** std::cout<<"The dimensions of the RegularVector3dField3d are "<<Reg.dim1()<<", "<<Reg.dim2()<<", "<<Reg.dim3()<<std::endl;
** std::cout<<"The init_point is "<<Reg.get_init_point()<<std::endl;
** std::cout<<"The grid_step is "<<Reg.get_grid_step()<<std::endl;
** \endcode
*/

template<typename RegularVector3dField3d>
void plt_to_RegularVector3dField3d(const std::string &file_path_name,
	RegularVector3dField3d &reg,
	std::string &title,
	std::string &zonename,
	std::vector<std::string> &varnames,
	const int zone_loaded=1)
{
	typedef typename RegularVector3dField3d::vector3d_value_type T;
	typedef typename RegularVector3dField3d::grid_value_type GridT;
	int ZoneLoaded = zone_loaded;
	double SolTime;
	title.clear();
	zonename.clear();
	varnames.clear();
	std::ifstream fp (file_path_name.c_str(), std::ios::in|std::ios::binary);
	slip::Point3d<GridT> init;
	slip::Point3d<GridT> grid;
/**********************************************************************/
/********************************** HEADER ****************************/
/**********************************************************************/
	int nbvars=0;
	int nbzones=0, size=0;
	int Imax=1, Jmax=1, Kmax=1;
	const int FileType=plt_get_file_type(fp);
	plt_read_header(fp, nbvars, nbzones, Kmax, Imax, Jmax, title, zonename, varnames, SolTime, ZoneLoaded);
	size=Imax*Jmax*Kmax;
/**********************************************************************/
/*********************************** DATA *****************************/
/**********************************************************************/
	reg.resize(Kmax,Imax,Jmax);
	if (ZoneLoaded>nbzones) ZoneLoaded=nbzones;
	if (ZoneLoaded<1) ZoneLoaded=1;
	// Lecture des données des zones
	for (int NumZone = 1; NumZone <= nbzones; ++NumZone)
	{
		{
			float *tmp=new float;
			fp.read((char*)tmp, sizeof(float));
			delete tmp;
		}

// Lecture du type des données
		int *datatype=new int[nbvars];
		for (int i = 0; i < nbvars; ++i)
		{
			int *tmp=new int;
			fp.read((char*)tmp, sizeof(int));
			datatype[i]=*tmp;
			delete tmp;
		}

// Lecture de trucs inutiles pour nous
		for (size_t i = 0; i < 3; ++i)
		{
			int *tmp=new int;
			fp.read((char*)tmp, sizeof(int));
			if ((i==0||i==1)&&(*tmp)!=0)
			{
				for (int j = 0; j < nbvars; ++j)
				{
					fp.read((char*)tmp, sizeof(int));
				}
			}
			delete tmp;
		}

// Lecture des min et max de chaque variable
		for (int i = 0; i < nbvars; ++i)
		{
			double *min=new double, *max=new double;
			fp.read((char*)min, sizeof(double));
			fp.read((char*)max, sizeof(double));
			delete min;
			delete max;
		}
		if (NumZone!=ZoneLoaded)
		{
			for (int num = 0; num < nbvars; num++)
			{
				switch (datatype[num])
				{
					case 1:
					fp.seekg((size)*sizeof(float), std::ios::cur);
					break;
					case 2:
					fp.seekg((size)*sizeof(double), std::ios::cur);
					break;
					case 3:
					fp.seekg((size)*sizeof(int), std::ios::cur);
					break;
					case 4:
					fp.seekg((size)*sizeof(short int), std::ios::cur);
					break;
					case 5:
					case 6:
					fp.seekg((size)*sizeof(char), std::ios::cur);
					break;
					default:
					break;
				}
			}
		}
		else
		{
			for (int i = 0; i < nbvars; ++i)
			{
				if ( FileType!=2 && i<3 )
				{
					GridT *min=new GridT, *max=new GridT;
					switch (datatype[i])
					{
						case 1:
						{
							float *tmp=new float;
							fp.read((char*)tmp, sizeof(float));
							*min=static_cast<GridT>(*tmp);
							fp.seekg((size-2)*sizeof(float), std::ios::cur);
							fp.read((char*)tmp, sizeof(float));
							*max=static_cast<GridT>(*tmp);
							delete tmp;
							break;
						}
						case 2:
						{
							double *tmp=new double;
							fp.read((char*)tmp, sizeof(double));
							*min=static_cast<GridT>(*tmp);
							fp.seekg((size-2)*sizeof(double), std::ios::cur);
							fp.read((char*)tmp, sizeof(double));
							*max=static_cast<GridT>(*tmp);
							delete tmp;
							break;
						}
						case 3:
						{
							int *tmp=new int;
							fp.read((char*)tmp, sizeof(int));
							*min=static_cast<GridT>(*tmp);
							fp.seekg((size-2)*sizeof(int), std::ios::cur);
							fp.read((char*)tmp, sizeof(int));
							*max=static_cast<GridT>(*tmp);
							delete tmp;
							break;
						}
						case 4:
						{
							short int *tmp=new short int;
							fp.read((char*)tmp, sizeof(short int));
							*min=static_cast<GridT>(*tmp);
							fp.seekg((size-2)*sizeof(short int), std::ios::cur);
							fp.read((char*)tmp, sizeof(short int));
							*max=static_cast<GridT>(*tmp);
							delete tmp;
							break;
						}
						case 5:
						case 6:
						{
							char *tmp=new char;
							fp.read((char*)tmp, sizeof(char));
							*min=static_cast<GridT>(*tmp);
							fp.seekg((size-2)*sizeof(char), std::ios::cur);
							fp.read((char*)tmp, sizeof(char));
							*max=static_cast<GridT>(*tmp);
							delete tmp;
							break;
						}
					}

					int Nmax=1;
					switch (i)
					{
						case 0:
						Nmax=Jmax;
						break;
						case 1:
						Nmax=Imax;
						break;
						case 2:
						Nmax=Kmax;
						break;
						default:
						break;
					}
					init[i]=*min;
					if (Nmax>1)
						grid[i]=std::abs((*min)-(*max))/(Nmax-1.);
					else
						grid[i]=1.;
					delete min;
					delete max;
				}
				else if ( ( FileType==0 && i<6 ) || ( FileType==2 && i<3 ) )
				{
					int n=0, comp=i-3;
					if (FileType==2)
						comp=i;
					switch (datatype[i])
					{
						case 1:
						{
							float *tmp=new float[size];
							fp.read((char*)tmp, size*sizeof(float));
							for (int kk = Kmax-1; kk >= 0; --kk)
							{
								for (int ii = Imax-1; ii >= 0; --ii)
								{
									for (int jj = 0; jj < Jmax && n<size; ++jj, ++n)
										reg(kk,ii,jj)[comp]=static_cast<T>(*(tmp+n));
								}
							}
							delete[] tmp;
							break;
						}
						case 2:
						{
							double *tmp=new double[size];
							fp.read((char*)tmp, size*sizeof(double));
							for (int kk = Kmax-1; kk >= 0; --kk)
							{
								for (int ii = Imax-1; ii >= 0; --ii)
								{
									for (int jj = 0; jj < Jmax && n<size; ++jj, ++n)
										reg(kk,ii,jj)[comp]=static_cast<T>(*(tmp+n));
								}
							}
							delete[] tmp;
							break;
						}
						case 3:
						{
							int *tmp=new int[size];
							fp.read((char*)tmp, size*sizeof(int));
							for (int kk = Kmax-1; kk >= 0; --kk)
							{
								for (int ii = Imax-1; ii >= 0; --ii)
								{
									for (int jj = 0; jj < Jmax && n<size; ++jj, ++n)
										reg(kk,ii,jj)[comp]=static_cast<T>(*(tmp+n));
								}
							}
							delete[] tmp;
							break;
						}
						case 4:
						{
							short int *tmp=new short int[size];
							fp.read((char*)tmp, size*sizeof(short int));
							for (int kk = Kmax-1; kk >= 0; --kk)
							{
								for (int ii = Imax-1; ii >= 0; --ii)
								{
									for (int jj = 0; jj < Jmax && n<size; ++jj, ++n)
										reg(kk,ii,jj)[comp]=static_cast<T>(*(tmp+n));
								}
							}
							delete[] tmp;
							break;
						}
						case 5:
						case 6:
						{
							char *tmp=new char[size];
							fp.read((char*)tmp, size*sizeof(char));
							for (int kk = Kmax-1; kk >= 0; --kk)
							{
								for (int ii = Imax-1; ii >= 0; --ii)
								{
									for (int jj = 0; jj < Jmax && n<size; ++jj, ++n)
										reg(kk,ii,jj)[comp]=static_cast<T>(*(tmp+n));
								}
							}
							delete[] tmp;
							break;
						}
					}
				}
			}
		}
		delete[] datatype;
	}
	if ( FileType!=2 )
	{
		reg.set_init_point(init);
		reg.set_grid_step(grid);
	}
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \brief Read a slip::RegularVector3dField3d from a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector3dField3d container.
** \param zone_loaded Zone index to read (1 by default).
** \bug Warning : data must be properly sorted. If you are not sure they are, use generic_plt_to_RegularVector3dField3d().
** \par Example:
** \code
** // Read the "test_3D.plt" file and put the data in the container3d Reg.
** slip::RegularVector3dField3d<double> Reg;
** std::string file_name="test_3D.plt";
** plt_to_RegularVector3dField3d(file_name, Reg);
** std::cout<<"The dimensions of the RegularVector3dField3d are "<<Reg.dim1()<<", "<<Reg.dim2()<<", "<<Reg.dim3()<<std::endl;
** std::cout<<"The init_point is "<<Reg.get_init_point()<<std::endl;
** std::cout<<"The grid_step is "<<Reg.get_grid_step()<<std::endl;
** \endcode
*/
template<typename RegularVector3dField3d>
void plt_to_RegularVector3dField3d(const std::string &file_path_name,
	RegularVector3dField3d &reg,
	const int zone_loaded=1)
{
  //	typedef typename RegularVector3dField3d::vector3d_value_type T;
	std::string _useless_string;
	std::vector<std::string> _useless_varnames;
	slip::plt_to_RegularVector3dField3d(file_path_name,
		reg,
		_useless_string,
		_useless_string,
		_useless_varnames,
		zone_loaded);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2014/05/07
** \since 1.4.0
** \brief Comparison fonctor used to sort an Array2d.
** \param First pair.
** \param Second pair.
*/
template<class T>
bool ComparePairFirst(const std::pair<T,size_t> &i_lhs, const std::pair<T,size_t> &i_rhs)
{
	return i_lhs.first < i_rhs.first;
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2014/05/07
** \since 1.4.0
** \brief Create a reference index vector used to sort an Array2d.
** \param Index vector containing the reference indices.
** \param in array2d to sort.
** \param col column used for sorting (0 by default).
*/
template<class T, class TT>
void paired_sort(std::vector<size_t> & Index, const slip::Array2d<T> & data, TT &F, const size_t col=0)
{
	size_t size=data.dim1();
	std::vector<std::pair<T,size_t> > IndexedPair(size);
	for(size_t i=0;i<size;++i)
	{
		IndexedPair[i].first = data(i,col);
		IndexedPair[i].second = i;
	}
	std::sort(IndexedPair.begin(), IndexedPair.end(), &F);
	Index.resize(size);
	for(size_t i = 0; i < size; ++i)
		Index[i] = IndexedPair[i].second;
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2014/05/07
** \since 1.4.0
** \brief Create a reference index vector used to sort an Array2d.
** \param Index vector containing the reference indices.
** \param in array2d to sort.
** \param col column used for sorting (0 by default).
*/
template<class T>
void paired_sort(std::vector<size_t> & Index, const slip::Array2d<T> & data, const size_t col=0)
{
	paired_sort(Index, data, ComparePairFirst<T>, col);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2014/05/07
** \since 1.4.0
** \brief Create a reference index vector used to sort an Array2d (stable version).
** \param Index vector containing the reference indices.
** \param in array2d to sort.
** \param col column used for sorting (0 by default).
*/
template<class T, class TT>
void paired_stable_sort(std::vector<size_t> & Index, const slip::Array2d<T> & data, TT &F, const size_t col=0)
{
	size_t size=data.dim1();
	std::vector<std::pair<T,size_t> > IndexedPair(size);
	for(size_t i=0;i<size;++i)
	{
		IndexedPair[i].first = data(i,col);
		IndexedPair[i].second = i;
	}
	std::stable_sort(IndexedPair.begin(), IndexedPair.end(), &F);
	Index.resize(size);
	for(size_t i = 0; i < size; ++i)
		Index[i] = IndexedPair[i].second;
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2014/05/07
** \since 1.4.0
** \brief Create a reference index vector used to sort an Array2d (stable version).
** \param Index vector containing the reference indices.
** \param in array2d to sort.
** \param col column used for sorting (0 by default).
*/
template<class T>
void paired_stable_sort(std::vector<size_t> & Index, const slip::Array2d<T> & data, const size_t col=0)
{
	paired_stable_sort(Index, data, ComparePairFirst<T>, col);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2014/05/07
** \since 1.4.0
** \brief Create a reference index vector used to sort an Array2d (stable version).
** \param Index vector containing the reference indices.
** \param in array2d to sort.
** \param cols vector of columns used for sorting (0 by default).
*/
template<class T, class TT>
void paired_sort(std::vector<size_t> & Index, const slip::Array2d<T> & data, TT &F, std::vector<size_t> cols=std::vector<size_t> () )
{
	if (cols.empty())
		cols.push_back(0);
	size_t size=data.dim1();
	std::vector<std::pair<T,size_t> > IndexedPair(size);
	for(size_t i=0;i<size;++i)
	{
		IndexedPair[i].first = data(i,cols.front());
		IndexedPair[i].second = i;
	}
	std::sort(IndexedPair.begin(), IndexedPair.end(), &F);
	for(size_t n=1;n<cols.size();++n)
	{
		for(size_t i=0;i<size;++i)
		{
			IndexedPair[i].first = data(i,cols.at(n));
			IndexedPair[i].second = i;
		}
		std::stable_sort(IndexedPair.begin(), IndexedPair.end(), &F);
	}
	Index.resize(size);
	for(size_t i = 0; i < size; ++i)
		Index[i] = IndexedPair[i].second;
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2014/05/07
** \since 1.4.0
** \brief Create a reference index vector used to sort an Array2d (stable version).
** \param Index vector containing the reference indices.
** \param in array2d to sort.
** \param cols vector of columns used for sorting (0 by default).
*/
template<class T>
void paired_sort(std::vector<size_t> & Index, const slip::Array2d<T> & data, const std::vector<size_t> cols=std::vector<size_t> () )
{
	paired_sort(Index, data, ComparePairFirst<T>, cols);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2014/05/07
** \since 1.4.0
** \brief Sort an Array2d from a reference index vector.
** \param in array2d to sort.
** \param reference vector containing the reference indices.
** \param col column used for sorting (0 by default).
** \par Example:
** \code
** // Create a Array2d and sort it by the 3rd column, then by the 2nd then by the 1st.
** slip::Array2d<double> data(10,5);
** std::generate(data.begin(), data.end(),std::rand);
** cout<<"data before sorting"<<endl;
** cout<<data<<endl;
** vector<size_t> Index(data.dim1());
** paired_sort(Index,data);
** for (size_t j = 0; j < data.dim2(); ++j)
**   sort_from_ref(data, Index, j);
** cout<<"data after sorting"<<endl;
** cout<<data<<endl;
** \endcode
*/
template <typename T>
void sort_from_ref(slip::Array2d<T> & in, const std::vector<size_t> & reference, const size_t col=0)
{
	const size_t size = in.dim1();
	assert(size==reference.size());
	std::vector<T> ret(size);
	for (size_t i = 0; i < size; ++i)
	{
		ret[i] = in(reference[i],col);
	}
	std::copy(ret.begin(), ret.end(), in.col_begin(col));
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2013/11/20
** \since 1.4.0
** \brief Read a slip::RegularVector3dField3d from a binary tecplot file even if data are not properly sorted. This function is slower than plt_to_RegularVector3dField3d().
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector3dField3d container.
** \param title Title of the tecplot frame.
** \param zonename Name of the tecplot zone.
** \param varnames Name of all the variables.
** \param zone_loaded Zone index to read (1 by default).
** \par Example:
** \code
** // Read the "test_sort_3D.plt" file, put the data in the container3d Reg and get the title, zone name and variable names.
** slip::RegularVector3dField3d<double> Reg;
** std::string file_name="test_sort_3D.plt", title, zonename;
** std::vector<std::string> varnames;
** generic_plt_to_RegularVector3dField3d(file_name, Reg, title, zonename, varnames);
** std::cout<<"The file title is : \""<<title<<"\" and the zone name is \""<<zonename<<"\""<<std::endl;
** std::cout<<"The variable names are : "<<std::endl;
** for (size_t i=0; i<varnames.size(); ++i)
**   std::cout<<varnames[i]<<std::endl;
** std::cout<<"The dimensions of the RegularVector3dField3d are "<<Reg.dim1()<<", "<<Reg.dim2()<<", "<<Reg.dim3()<<std::endl;
** std::cout<<"The init_point is "<<Reg.get_init_point()<<std::endl;
** std::cout<<"The grid_step is "<<Reg.get_grid_step()<<std::endl;
** \endcode
*/
template<typename RegularVector3dField3d>
void generic_plt_to_RegularVector3dField3d(const std::string &file_path_name,
	RegularVector3dField3d &reg,
	std::string &title,
	std::string &zonename,
	std::vector<std::string> &varnames,
	const int zone_loaded=1)
{
	typedef typename RegularVector3dField3d::vector3d_value_type T;
	typedef typename RegularVector3dField3d::grid_value_type GridT;
	int ZoneLoaded = zone_loaded;
	double SolTime;
	title.clear();
	zonename.clear();
	varnames.clear();
	std::ifstream fp (file_path_name.c_str(), std::ios::in|std::ios::binary);
	slip::Point3d<GridT> init;
	slip::Point3d<GridT> grid;

	slip::Array2d<T> data, test_data;
	const int FileType=plt_get_file_type(file_path_name);
	slip::plt_to_Array2d(file_path_name, data, ZoneLoaded);

	{
		// sorting array
		std::vector<size_t> Index(data.dim1());
		size_t ccc[3]={2,1,0};
		std::vector<size_t> cols(ccc, ccc+3);
		paired_sort(Index, data, ComparePairFirst<double>, cols);

		for (size_t j = 0; j < data.dim2(); ++j)
			sort_from_ref(data, Index, j);
	}

	std::size_t size =  data.rows();
	std::size_t size_xy = std::count(data.col_begin(2),data.col_end(2),data[0][2]);
	std::size_t size_z = size / size_xy;
	std::size_t size_y = std::count(data.col_begin(0),data.col_end(0),data[0][0]);
	size_y = size_y / size_z;
	std::size_t size_x = size / (size_y * size_z);

	init[0]=static_cast<GridT>(std::min(data[0][0],data[data.rows()-1][0]));
	init[1]=static_cast<GridT>(std::min(data[0][1],data[data.rows()-1][1]));
	init[2]=static_cast<GridT>(std::min(data[0][2],data[data.rows()-1][2]));

	bool y_direct = (init[1] == data[0][1]);
   
	reg.resize(size_z,size_y,size_x);

	if( data[0][0] == data[1][0] || FileType==1 )
	{
	//cas x fixe, y varie
	grid[0]=static_cast<GridT>(std::abs(*(data.col_begin(0)+size_y)-(*(data.col_begin(0)))));
	grid[1]=static_cast<GridT>(std::abs(*(data.col_begin(1)+1)-*(data.col_begin(1))));
 	grid[2]=static_cast<GridT>(std::abs(*(data.col_begin(2)+size_xy)-*(data.col_begin(2))));
	if ( FileType==0 )
	{
	std::size_t gridz = 0;
	for(std::size_t k = 0 ; k < size_z; ++k)
	  {
		std::size_t gridy = 0;
		
		for(std::size_t j = 0; j < size_x; ++j)
		  {
		if(y_direct)
		  {
			std::copy(data.col_begin(4) + gridz + gridy,
				  data.col_begin(4) + (gridz + gridy + size_y),
				  reg.col_rbegin(1,(size_z-1)-k,j));
			std::copy(data.col_begin(3) + gridz + gridy,
				  data.col_begin(3) + (gridz + gridy + size_y),
				  reg.col_rbegin(0,(size_z-1)-k,j));
			std::copy(data.col_begin(5) + gridz + gridy,
				  data.col_begin(5) + (gridz + gridy + size_y),
				  reg.col_rbegin(2,(size_z-1)-k,j));
			gridy += size_y;
		  }
		else
		  {
			std::copy(data.col_begin(4) + gridz + gridy,
				  data.col_begin(4) + (gridz + gridy + size_y),
				  reg.col_begin(1,(size_z-1)-k,j));
			std::copy(data.col_begin(3) + gridz + gridy,
				  data.col_begin(3) + (gridz + gridy + size_y),
				  reg.col_begin(0,(size_z-1)-k,j));
			std::copy(data.col_begin(5) + gridz + gridy,
				  data.col_begin(5) + (gridz + gridy + size_y),
				  reg.col_begin(2,(size_z-1)-k,j));
			gridy += size_y;
		  }
		  }
		gridz+=size_xy;
	  }
	}
	}
	else
	{
	//cas x varie, y fixe
	grid[0]=static_cast<GridT>(std::abs(*(data.col_begin(0)+1)-(*(data.col_begin(0)))));
	grid[1]=static_cast<GridT>(std::abs(*(data.col_begin(1)+size_x) - *(data.col_begin(1))));
	grid[2]=static_cast<GridT>(std::abs(*(data.col_begin(2)+size_xy) - *(data.col_begin(2))));
	std::size_t gridz = 0;
	for(std::size_t k = 0 ; k < size_z; ++k)
	  {
		std::size_t gridx = 0;
		for(std::size_t j = 0; j < size_y; ++j)
		  {
		if(y_direct)
		  {
			std::copy(data.col_begin(3) + gridz + gridx,
				  data.col_begin(3) + (gridz + gridx + size_x),
				  reg.row_begin(0,(size_z-1)-k,(size_y - 1) - j));
			std::copy(data.col_begin(4) + gridz + gridx, 
				  data.col_begin(4) + (gridz + gridx + size_x),
				  reg.row_begin(1,(size_z-1)-k,(size_y - 1) - j));
			std::copy(data.col_begin(5) + gridz + gridx, 
				  data.col_begin(5) + (gridz + gridx + size_x),
				  reg.row_begin(2,(size_z-1)-k,(size_y - 1) - j));
		
		  }
		else
		  {
			std::copy(data.col_begin(3) + gridz + gridx,
				  data.col_begin(3) + gridz + (gridx + size_x),
				  reg.row_begin(0,(size_z-1)-k,j));
			std::copy(data.col_begin(4) + gridz + gridx, 
				  data.col_begin(4) + (gridz + gridx + size_x),
				  reg.row_begin(1,(size_z-1)-k,j));
			std::copy(data.col_begin(5) + gridz + gridx, 
				  data.col_begin(5) + (gridz + gridx + size_x),
				  reg.row_begin(2,(size_z-1)-k,j));
		  }
		gridx += size_x;
		  }
		gridz+=size_xy;
	  }
	}

	if ( FileType!=2 )
	{
		reg.set_init_point(init);
		reg.set_grid_step(grid);
	}
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a slip::RegularVector3dField3d from a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector3dField3d container.
** \param zone_loaded Zone index to read (1 by default).
** \par Example:
** \code
** // Read the "test_sort_3D.plt" file and put the data in the container3d Reg.
** slip::RegularVector3dField3d<double> Reg;
** std::string file_name="test_sort_3D.plt";
** generic_plt_to_RegularVector3dField3d(file_name, Reg);
** std::cout<<"The dimensions of the RegularVector3dField3d are "<<Reg.dim1()<<", "<<Reg.dim2()<<", "<<Reg.dim3()<<std::endl;
** std::cout<<"The init_point is "<<Reg.get_init_point()<<std::endl;
** std::cout<<"The grid_step is "<<Reg.get_grid_step()<<std::endl;
** \endcode
*/
template<typename RegularVector3dField3d>
void generic_plt_to_RegularVector3dField3d(const std::string &file_path_name,
	RegularVector3dField3d &reg,
	const int zone_loaded=1)
{
  //	typedef typename RegularVector3dField3d::vector3d_value_type T;
	std::string _useless_string;
	std::vector<std::string> _useless_varnames;
	slip::generic_plt_to_RegularVector3dField3d(file_path_name,
		reg,
		_useless_string,
		_useless_string,
		_useless_varnames,
		zone_loaded);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Write a slip::RegularVector3dField3d to a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector3dField3d container.
** \param title Title of the tecplot frame ("Title" by default).
** \param zone Name of the tecplot zone ("Zone" by default).
** \param SolTime Solution Time (0 by default).
** \param varnames Vector containing the variable names ("X", "Y", "Z", "U", "V", "W" by default).
** \param FileType Type of file : 0=FULL, 1=Grid only, 2=Solution only (0 by default).
** \par Example:
** \code
** // Write the RegularVector3dField3d Reg in the "test_3D.plt" file using defined title, zone name and variable names.
** slip::RegularVector3dField3d<double> Reg(10,20,30,1.);
** std::string file_name="test_3D_write.plt", title="Title of the file", zonename="Name of the zone";
** std::vector<std::string> varnames; // if varnames is empty, the default values X, Y, Z, U, V, W are used
** RegularVector3dField3d_to_plt(file_name, Reg, title, zonename, varnames);
** \endcode
*/
template<typename RegularVector3dField3d>
void RegularVector3dField3d_to_plt(const std::string &file_path_name,
	RegularVector3dField3d &reg,
	std::string title,
	std::string zone,
	const double SolTime,
	std::vector<std::string> varnames=std::vector<std::string>(),
	const int FileType=0)
{
	typedef typename RegularVector3dField3d::vector3d_value_type T;
	typedef typename RegularVector3dField3d::grid_value_type GridT;
	std::ofstream fp (file_path_name.c_str(), std::ios::out|std::ios::binary);
/******************************************************************************/
/********************************** HEADER ************************************/
/******************************************************************************/

// Lecture de la version du fichier Tecplot
	fp.write("#!TDV112",8);
	int *myint=new int;
	char *mychar=new char;
	float *myfloat=new float;
	double *mydouble=new double;
// Écriture de l'ordre
	*myint=1;
	fp.write((char*)myint, sizeof(int));

// Écriture du type de fichier
	*myint=FileType;
	fp.write((char*)myint, sizeof(int));

//	// Écriture du titre
	std::string strtmp=title;
	for (size_t i = 0; i <= strtmp.size(); i++)
	{
		*myint=(int)strtmp[i];
		fp.write((char*)myint,sizeof(int));
	}

// Définition des variables en fonction du type de fichier
	size_t imin=0, imax=6, nbvars=6;
	switch (FileType)
	{
		case 1:
			imin=0;
			imax=3;
			nbvars=3;
			if (varnames.size()<3)
			{
				varnames.clear();
				varnames.push_back("X");
				varnames.push_back("Y");
				varnames.push_back("Z");
			}
			break;
		case 2:
			imin=0;
			imax=3;
			nbvars=3;
			if (varnames.size()<3)
			{
				varnames.clear();
				varnames.push_back("U");
				varnames.push_back("V");
				varnames.push_back("W");
			}
			break;
		default:
			if (varnames.size()<6)
			{
				varnames.clear();
				varnames.push_back("X");
				varnames.push_back("Y");
				varnames.push_back("Z");
				varnames.push_back("U");
				varnames.push_back("V");
				varnames.push_back("W");
			}
			break;
	}

// Écriture du nombre de variables
	*myint=nbvars;
	fp.write((char*)myint, sizeof(int));

// Écriture des noms des variables
	for (size_t i = imin; i < imax; i++)
	{
		for (size_t j = 0; j <= varnames[i].size(); j++)
		{
			*myint=(int)varnames[i][j];
			fp.write((char*)myint,sizeof(int));
		}
	}

// Écriture des paramètres des zones
// Écriture du zone marker
	*myfloat=299.0;
	fp.write((char*)myfloat, sizeof(float));
// Écriture du nom de la zone
	strtmp=zone;
	for (size_t i = 0; i <= strtmp.size(); i++)
	{
		*myint=(int)strtmp[i];
		fp.write((char*)myint,sizeof(int));
	}
// Écriture du Parent zone
	*myint=-1;
	fp.write((char*)myint, sizeof(int));
// Écriture du StrandID
	*myint=-1;
	fp.write((char*)myint, sizeof(int));
// Écriture du Solution time
	*mydouble=SolTime;
	fp.write((char*)mydouble, sizeof(double));
// Écriture du -1 inutile
	*myint=-1;
	fp.write((char*)myint, sizeof(int));
// Écriture du ZoneType
	*myint=0;
	fp.write((char*)myint, sizeof(int));
// Écriture du Specify Var Location
	fp.write((char*)myint, sizeof(int));
// Écriture du Supplied Face Neighboring
	fp.write((char*)myint, sizeof(int));
// Écriture du User Defined Face Connections
	fp.write((char*)myint, sizeof(int));
// Écriture des IMax, JMax et KMax
	*myint=reg.dim3();
	fp.write((char*)myint, sizeof(int));
	*myint=reg.dim2();
	fp.write((char*)myint, sizeof(int));
	*myint=reg.dim1();
	fp.write((char*)myint, sizeof(int));
// Écriture du Auxiliary Name
	*myint=0;
	fp.write((char*)myint, sizeof(int));
// Écriture du marker de fin de header
	*myfloat=357.0;
	fp.write((char*)myfloat, sizeof(float));

/******************************************************************************/
/*********************************** DATA *************************************/
/******************************************************************************/
// Écriture des données des zones
// Écriture du marker de début de zone
	*myfloat=299.0;
	fp.write((char*)myfloat, sizeof(float));
// Écriture du type des données
	*myint=get_tecplot_type<GridT>();
	if (FileType==0 || FileType==1)
	{
		for (size_t i = 0; i < 3; ++i)
			fp.write((char*)myint, sizeof(int));
	}
	*myint=get_tecplot_type<T>();
	if (FileType==0 || FileType==2)
	{
		for (size_t i = 0; i < 3; ++i)
			fp.write((char*)myint, sizeof(int));
	}
// Écriture de trucs inutiles pour nous
	*myint=0;
	fp.write((char*)myint, sizeof(int));
	fp.write((char*)myint, sizeof(int));
	*myint=-1;
	fp.write((char*)myint, sizeof(int));
// Écriture des min et max de chaque variable
	if (FileType==0 || FileType==1)
	{
		*mydouble=static_cast<double>(reg.get_init_point()[0]);
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=static_cast<double>(reg.get_init_point()[0]+(reg.dim3()-1)*reg.get_grid_step()[0]);
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=static_cast<double>(reg.get_init_point()[1]);
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=static_cast<double>(reg.get_init_point()[1]+(reg.dim2()-1)*reg.get_grid_step()[1]);
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=static_cast<double>(reg.get_init_point()[2]);
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=static_cast<double>(reg.get_init_point()[2]+(reg.dim1()-1)*reg.get_grid_step()[2]);
		fp.write((char*)mydouble, sizeof(double));
	}

	if (FileType==0 || FileType==2)
	{
		double umin, umax, vmin, vmax, wmin, wmax;
		umin=static_cast<double>((*std::min_element(reg.begin(),reg.end(),slip::GenericComparator<slip::Vector3d<T> >(0)))[0]);
		umax=static_cast<double>((*std::max_element(reg.begin(),reg.end(),slip::GenericComparator<slip::Vector3d<T> >(0)))[0]);
		vmin=static_cast<double>((*std::min_element(reg.begin(),reg.end(),slip::GenericComparator<slip::Vector3d<T> >(1)))[1]);
		vmax=static_cast<double>((*std::max_element(reg.begin(),reg.end(),slip::GenericComparator<slip::Vector3d<T> >(1)))[1]);
		wmin=static_cast<double>((*std::min_element(reg.begin(),reg.end(),slip::GenericComparator<slip::Vector3d<T> >(2)))[2]);
		wmax=static_cast<double>((*std::max_element(reg.begin(),reg.end(),slip::GenericComparator<slip::Vector3d<T> >(2)))[2]);
		*mydouble=umin;
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=umax;
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=vmin;
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=vmax;
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=wmin;
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=wmax;
		fp.write((char*)mydouble, sizeof(double));
	}

	if (FileType==0 || FileType==1)
	{
		const int size_grid=sizeof(GridT);
		for (int kk = reg.dim1()-1; kk >= 0; --kk)
		{
			for (int ii = reg.dim2()-1; ii >= 0; --ii)
			{
				for (int jj = 0; jj < (int)reg.dim3(); ++jj)
				{
					GridT *temp=new GridT;
					*temp=reg.x(kk,ii,jj);
					fp.write((char*)temp, size_grid);
					delete temp;
				}
			}
		}
		for (int kk = reg.dim1()-1; kk >= 0; --kk)
		{
			for (int ii = reg.dim2()-1; ii >= 0; --ii)
			{
				for (int jj = 0; jj < (int)reg.dim3(); ++jj)
				{
					GridT *temp=new GridT;
					*temp=reg.y(kk,ii,jj);
					fp.write((char*)temp, size_grid);
					delete temp;
				}
			}
		}
		for (int kk = reg.dim1()-1; kk >= 0; --kk)
		{
			for (int ii = reg.dim2()-1; ii >= 0; --ii)
			{
				for (int jj = 0; jj < (int)reg.dim3(); ++jj)
				{
					GridT *temp=new GridT;
					*temp=reg.z(kk,ii,jj);
					fp.write((char*)temp, size_grid);
					delete temp;
				}
			}
		}
	}

	if (FileType==0 || FileType==2)
	{
		const int size_data=sizeof(T);
		for (size_t i = 0; i < 3; ++i)
		{
			for (int kk = reg.dim1()-1; kk >= 0; --kk)
			{
				for (int ii = reg.dim2()-1; ii >= 0; --ii)
				{
					for (int jj = 0; jj < (int)reg.dim3(); ++jj)
					{
						T *temp=new T;
						*temp=reg(kk,ii,jj)[i];
						fp.write((char*)temp, size_data);
						delete temp;
					}
				}
			}
		}
	}
	delete myint;
	delete mychar;
	delete myfloat;
	delete mydouble;
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Write a slip::RegularVector3dField3d to a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector3dField3d container.
** \param FileType Type of file : 0=FULL, 1=Grid only, 2=Solution only (0 by default).
** \par Example:
** \code
** // Write the RegularVector3dField3d Reg in the "test_3D.plt" file.
** slip::RegularVector3dField3d<double> Reg(10,20,30,1.);
** std::string file_name="test_3D_write.plt";
** RegularVector3dField3d_to_plt(file_name, Reg);
** \endcode
*/
template<typename RegularVector3dField3d>
void RegularVector3dField3d_to_plt(const std::string &file_path_name,
	RegularVector3dField3d &reg,
	const int FileType=0)
{
	RegularVector3dField3d_to_plt(file_path_name, reg, "Title", "Zone", 0., std::vector<std::string>(), FileType);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Write a slip::RegularVector3dField3d to a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector3dField3d container.
** \param SolTime Solution Time.
** \param FileType Type of file : 0=FULL, 1=Grid only, 2=Solution only (0 by default).
** \par Example:
** \code
** // Write the RegularVector3dField3d Reg in the "test_3D.plt" file using defined title, zone name and variable names.
** slip::RegularVector3dField3d<double> Reg(10,20,30,1.);
** std::string file_name="test_3D_write.plt";
** const double t=0.5;
** RegularVector3dField3d_to_plt(file_name, Reg, t);
** \endcode
*/
template<typename RegularVector3dField3d>
void RegularVector3dField3d_to_plt(const std::string &file_path_name,
	RegularVector3dField3d &reg,
	const double SolTime,
	const int FileType=0)
{
	RegularVector3dField3d_to_plt(file_path_name, reg, "Title", "Zone", SolTime, std::vector<std::string>(), FileType);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Write a slip::RegularVector3dField3d to a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector3dField3d container.
** \param title Title of the tecplot frame ("Title" by default).
** \param zone Name of the tecplot zone ("Zone" by default).
** \param FileType Type of file : 0=FULL, 1=Grid only, 2=Solution only (0 by default).
** \par Example:
** \code
** // Write the RegularVector3dField3d Reg in the "test_3D.plt" file using defined title, zone name and variable names.
** slip::RegularVector3dField3d<double> Reg(10,20,30,1.);
** std::string file_name="test_3D_write.plt", title="Title of the file", zonename="Name of the zone";
** RegularVector3dField3d_to_plt(file_name, Reg, title, zone);
** \endcode
*/
template<typename RegularVector3dField3d>
void RegularVector3dField3d_to_plt(const std::string &file_path_name,
	RegularVector3dField3d &reg,
	std::string title,
	std::string zone,
	const int FileType=0)
{
	RegularVector3dField3d_to_plt(file_path_name, reg, title, zone, 0., std::vector<std::string>(), FileType);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Write a slip::RegularVector3dField3d to a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector3dField3d container.
** \param title Title of the tecplot frame ("Title" by default).
** \param zone Name of the tecplot zone ("Zone" by default).
** \param FileType Type of file : 0=FULL, 1=Grid only, 2=Solution only (0 by default).
** \par Example:
** \code
** // Write the RegularVector3dField3d Reg in the "test_3D.plt" file using defined title, zone name and variable names.
** slip::RegularVector3dField3d<double> Reg(10,20,30,1.);
** std::string file_name="test_3D_write.plt", title="Title of the file", zonename="Name of the zone";
** std::vector<std::string> varnames; // if varnames is empty, the default values X, Y, Z, U, V, W are used
** RegularVector3dField3d_to_plt(file_name, Reg, title, zone);
** \endcode
*/
template<typename RegularVector3dField3d>
void RegularVector3dField3d_to_plt(const std::string &file_path_name,
	RegularVector3dField3d &reg,
	std::string title,
	std::string zone,
	std::vector<std::string> varnames,
	const int FileType=0)
{
	RegularVector3dField3d_to_plt(file_path_name, reg, title, zone, 0., varnames, FileType);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a slip::RegularVector2dField2d from a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector2dField2d container.
** \param title Title of the tecplot frame.
** \param zone Name of the tecplot zone.
** \param varnames Vector containing the variable names.
** \param zone_loaded Zone index to read (1 by default).
** \bug Warning : data must be properly sorted. If you are not sure they are, use generic_plt_to_RegularVector2dField2d().
** \par Example:
** \code
** // Read the "test_2D.plt" file, put the data in the container2d Reg and get the title, zone name and variable names.
** slip::RegularVector2dField2d<double> Reg;
** std::string file_name="test_2D.plt", title, zonename;
** std::vector<std::string> varnames;
** plt_to_RegularVector2dField2d(file_name, Reg, title, zonename, varnames);
** std::cout<<"The file title is : \""<<title<<"\" and the zone name is \""<<zonename<<"\""<<std::endl;
** std::cout<<"The variable names are : "<<std::endl;
** for (size_t i=0; i<varnames.size(); ++i)
**   std::cout<<varnames[i]<<std::endl;
** std::cout<<"The dimensions of the RegularVector2dField2d are "<<Reg.dim1()<<", "<<Reg.dim2()<<std::endl;
** std::cout<<"The init_point is "<<Reg.get_init_point()<<std::endl;
** std::cout<<"The grid_step is "<<Reg.get_grid_step()<<std::endl;
** \endcode
*/
template<typename RegularVector2dField2d>
void plt_to_RegularVector2dField2d(const std::string &file_path_name,
	RegularVector2dField2d &reg,
	std::string &title,
	std::string &zonename,
	std::vector<std::string> &varnames,
	const int zone_loaded=1)
{
	typedef typename RegularVector2dField2d::vector2d_value_type T;
	typedef typename RegularVector2dField2d::grid_value_type GridT;
	int ZoneLoaded = zone_loaded;
	if (ZoneLoaded<1) ZoneLoaded=1;
	double SolTime;
	title.clear();
	zonename.clear();
	varnames.clear();
	std::ifstream fp (file_path_name.c_str(), std::ios::in|std::ios::binary);
	slip::Point2d<GridT> init;
	slip::Point2d<GridT> grid;
/**********************************************************************/
/********************************** HEADER ****************************/
/**********************************************************************/
	int nbvars=0;
	int nbzones=0, size=0;
	int Imax=1, Jmax=1, Kmax=1;
	const int FileType=plt_get_file_type(file_path_name);
	plt_read_header(fp, nbvars, nbzones, Kmax, Imax, Jmax, title, zonename, varnames, SolTime, ZoneLoaded);
	size=Imax*Jmax*Kmax;
/**********************************************************************/
/*********************************** DATA *****************************/
/**********************************************************************/
	reg.resize(Imax,Jmax);
	if (ZoneLoaded>nbzones) ZoneLoaded=nbzones;
//	// Lecture des données des zones
	for (int NumZone = 1; NumZone <= nbzones; ++NumZone)
	{
		{
			float *tmp=new float;
			fp.read((char*)tmp, sizeof(float));
			delete tmp;
		}

// Lecture du type des données
		int *datatype=new int[nbvars];
		for (int i = 0; i < nbvars; ++i)
		{
			int *tmp=new int;
			fp.read((char*)tmp, sizeof(int));
			datatype[i]=*tmp;
			delete tmp;
		}
// Lecture de trucs inutiles pour nous
		for (size_t i = 0; i < 3; ++i)
		{
			int *tmp=new int;
			fp.read((char*)tmp, sizeof(int));
			if ((i==0||i==1)&&(*tmp)!=0)
			{
				for (int j = 0; j < nbvars; ++j)
				{
					fp.read((char*)tmp, sizeof(int));
				}
				delete tmp;
			}
		}

// Lecture des min et max de chaque variable
		for (int i = 0; i < nbvars; ++i)
		{
			double *min=new double, *max=new double;
			fp.read((char*)min, sizeof(double));
			fp.read((char*)max, sizeof(double));
			delete min;
			delete max;
		}
		if (NumZone!=ZoneLoaded)
		{
			for (int num = 0; num < nbvars; num++)
				switch (datatype[num])
			{
				case 1:
				fp.seekg((size)*sizeof(float), std::ios::cur);
				break;
				case 2:
				fp.seekg((size)*sizeof(double), std::ios::cur);
				break;
				case 3:
				fp.seekg((size)*sizeof(int), std::ios::cur);
				break;
				case 4:
				fp.seekg((size)*sizeof(short int), std::ios::cur);
				break;
				case 5:
				case 6:
				fp.seekg((size)*sizeof(char), std::ios::cur);
				break;
				default:
				break;
			}
		}
		else
		{
			for (int i = 0; i < nbvars; ++i)
			{
				if ( FileType!=2 && i<2 )
				{
					GridT *min=new GridT, *max=new GridT;
					if (datatype[i]==1)
					{
						float *tmp=new float;
						fp.read((char*)tmp, sizeof(float));
						*min=static_cast<GridT>(*tmp);
						fp.seekg((size-2)*sizeof(float), std::ios::cur);
						fp.read((char*)tmp, sizeof(float));
						*max=static_cast<GridT>(*tmp);
						delete tmp;
					}
					else if (datatype[i]==2)
					{
						int *tmp=new int;
						fp.read((char*)tmp, sizeof(double));
						*min=static_cast<GridT>(*tmp);
						fp.seekg((size-2)*sizeof(double), std::ios::cur);
						fp.read((char*)tmp, sizeof(double));
						*max=static_cast<GridT>(*tmp);
						delete tmp;
					}
					else if (datatype[i]==3)
					{
						int *tmp=new int;
						fp.read((char*)tmp, sizeof(int));
						*min=static_cast<GridT>(*tmp);
						fp.seekg((size-2)*sizeof(int), std::ios::cur);
						fp.read((char*)tmp, sizeof(int));
						*max=static_cast<GridT>(*tmp);
						delete tmp;
					}
					else if (datatype[i]==4)
					{
						short int *tmp=new short int;
						fp.read((char*)tmp, sizeof(short int));
						*min=static_cast<GridT>(*tmp);
						fp.seekg((size-2)*sizeof(short int), std::ios::cur);
						fp.read((char*)tmp, sizeof(short int));
						*max=static_cast<GridT>(*tmp);
						delete tmp;
					}
					else if (datatype[i]==5||datatype[i]==6)
					{
						char *tmp=new char;
						fp.read((char*)tmp, sizeof(char));
						*min=static_cast<GridT>(*tmp);
						fp.seekg((size-2)*sizeof(char), std::ios::cur);
						fp.read((char*)tmp, sizeof(char));
						*max=static_cast<GridT>(*tmp);
						delete tmp;
					}

					int Nmax=1;
					switch (i)
					{
						case 0:
						Nmax=Jmax;
						break;
						case 1:
						Nmax=Imax;
						break;
						default:
						break;
					}
					init[i]=*min;
					if (Nmax>1)
						grid[i]=std::abs((*min)-(*max))/(Nmax-1.);
					else
						grid[i]=1.;
					delete min;
					delete max;
				}
				else if ( ( FileType==0 && i<4 ) || ( FileType==2 && i<2 ) )
				{
					int n=0, comp=i-2;
					if (FileType==2)
						comp=i;
					switch (datatype[i])
					{
						case 1:
						{
							float *tmp=new float[size];
							fp.read((char*)tmp, size*sizeof(float));
							for (int ii = Imax-1; ii >= 0; --ii)
							{
								for (int jj = 0; jj < Jmax && n < size; ++jj, ++n)
								{
									reg(ii,jj)[comp]=static_cast<T>(*(tmp+n));
								}
							}
							delete[] tmp;
							break;
						}
						case 2:
						{
							double *tmp=new double[size];
							fp.read((char*)tmp, size*sizeof(double));
							for (int ii = Imax-1; ii >= 0; --ii)
							{
								for (int jj = 0; jj < Jmax && n < size; ++jj, ++n)
								{
									reg(ii,jj)[comp]=static_cast<T>(*(tmp+n));
								}
							}
							delete[] tmp;
							break;
						}
						case 3:
						{
							int *tmp=new int[size];
							fp.read((char*)tmp, size*sizeof(int));
							for (int ii = Imax-1; ii >= 0; --ii)
							{
								for (int jj = 0; jj < Jmax && n < size; ++jj, ++n)
								{
									reg(ii,jj)[comp]=static_cast<T>(*(tmp+n));
								}
							}
							delete[] tmp;
							break;
						}
						case 4:
						{
							short int *tmp=new short int[size];
							fp.read((char*)tmp, size*sizeof(short int));
							for (int ii = Imax-1; ii >= 0; --ii)
							{
								for (int jj = 0; jj < Jmax && n < size; ++jj, ++n)
								{
									reg(ii,jj)[comp]=static_cast<T>(*(tmp+n));
								}
							}
							delete[] tmp;
							break;
						}
						case 5:
						case 6:
						{
							char *tmp=new char[size];
							fp.read((char*)tmp, size*sizeof(char));
							for (int ii = Imax-1; ii >= 0; --ii)
							{
								for (int jj = 0; jj < Jmax && n < size; ++jj, ++n)
								{
									reg(ii,jj)[comp]=static_cast<T>(*(tmp+n));
								}
							}
							delete[] tmp;
							break;
						}
					}
				}
			}
		}
		delete[] datatype;
	}
	if ( FileType!=2 )
	{
		reg.set_init_point(init);
		reg.set_grid_step(grid);
	}
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a slip::RegularVector2dField2d from a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector2dField2d container.
** \param zone_loaded Zone index to read (1 by default).
** \bug Warning : data must be properly sorted. If you are not sure they are, use generic_plt_to_RegularVector2dField2d().
** \par Example:
** \code
** // Read the "test_2D.plt" file and put the data in the container3d Reg.
** slip::RegularVector2dField2d<double> Reg;
** std::string file_name="test_2D.plt";
** plt_to_RegularVector2dField2d(file_name, Reg);
** std::cout<<"The dimensions of the RegularVector2dField2d are "<<Reg.dim1()<<", "<<Reg.dim2()<<std::endl;
** std::cout<<"The init_point is "<<Reg.get_init_point()<<std::endl;
** std::cout<<"The grid_step is "<<Reg.get_grid_step()<<std::endl;
** \endcode
*/
template<typename RegularVector2dField2d>
void plt_to_RegularVector2dField2d(const std::string &file_path_name,
	RegularVector2dField2d &reg,
	const int zone_loaded=1)
{
	std::string _useless_string;
	std::vector<std::string> _useless_varnames;
	slip::plt_to_RegularVector2dField2d(file_path_name,
		reg,
		_useless_string,
		_useless_string,
		_useless_varnames,
		zone_loaded);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2013/11/20
** \since 1.4.0
** \brief Comparator used to sort an Array2d.
** \param *rowA Pointer to a 1st row.
** \param *rowB Pointer to a second row.
** \bug The iterators are invalidated by the sort algorithm. Use [] operator to access data after a sorting operation.
** \par Example:
** \code
** // Create a Array2d and sort it by the 3rd column, then by the 2nd then by the 1st.
** slip::Array2d<double> data;
** slip::iota(data.begin(), data.end(), 10., -1.);
** for (size_t i = 0; i < data.dim1(); ++i)
** {
**   slip::iota(data.row_begin(i), data.row_end(i), 10.-i*0.3);
**   for (size_t j = 2; j < data.dim2(); ++j)
**   {
**	 data[i][j]=(int)data[i][j];
**   }
** }
** std::cout<<data<<endl<<endl;
** data.sort(&compareTwoRows2D);
** std::cout<<data<<endl<<endl;
** \endcode
*/
bool compareTwoRows2D(double* rowA, double* rowB)
{
	return ( (rowA[0]<rowB[0]) || ( (rowA[0]==rowB[0])&&(rowA[1]<rowB[1]) ) );
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a slip::RegularVector2dField2d from a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector2dField2d container.
** \param title Title of the tecplot frame.
** \param zone Name of the tecplot zone.
** \param varnames Vector containing the variable names.
** \param zone_loaded Zone index to read (1 by default).
** \par Example:
** \code
** // Read the "test_2D.plt" file, put the data in the container2d Reg and get the title, zone name and variable names.
** slip::RegularVector2dField2d<double> Reg;
** std::string file_name="test_2D.plt", title, zonename;
** std::vector<std::string> varnames;
** plt_to_RegularVector2dField2d(file_name, Reg, title, zonename, varnames);
** std::cout<<"The file title is : \""<<title<<"\" and the zone name is \""<<zonename<<"\""<<std::endl;
** std::cout<<"The variable names are : "<<std::endl;
** for (size_t i=0; i<varnames.size(); ++i)
**   std::cout<<varnames[i]<<std::endl;
** std::cout<<"The dimensions of the RegularVector2dField2d are "<<Reg.dim1()<<", "<<Reg.dim2()<<std::endl;
** std::cout<<"The init_point is "<<Reg.get_init_point()<<std::endl;
** std::cout<<"The grid_step is "<<Reg.get_grid_step()<<std::endl;
** \endcode
*/
template<typename RegularVector2dField2d>
void generic_plt_to_RegularVector2dField2d(const std::string &file_path_name,
	RegularVector2dField2d &reg,
	std::string &title,
	std::string &zonename,
	std::vector<std::string> &varnames,
	const int zone_loaded=1)
{
	typedef typename RegularVector2dField2d::vector2d_value_type T;
	typedef typename RegularVector2dField2d::grid_value_type GridT;
	int ZoneLoaded = zone_loaded;
	if (ZoneLoaded<1) ZoneLoaded=1;
	double SolTime;
	title.clear();
	zonename.clear();
	varnames.clear();
	std::ifstream fp (file_path_name.c_str(), std::ios::in|std::ios::binary);
	slip::Point2d<GridT> init;
	slip::Point2d<GridT> grid;

	slip::Array2d<T> data, test_data;
	const int FileType=plt_get_file_type(file_path_name);
	slip::plt_to_Array2d(file_path_name, data, ZoneLoaded);

	{
		// sorting array
		std::vector<size_t> Index(data.dim1());
		size_t ccc[2]={1,0};
		std::vector<size_t> cols(ccc, ccc+2);
		paired_sort(Index, data, ComparePairFirst<double>, cols);

		for (size_t j = 0; j < data.dim2(); ++j)
			sort_from_ref(data, Index, j);
	}

	std::size_t size_y = std::count(data.col_begin(0),data.col_end(0),
					data[0][0]); 
	std::size_t size_x = data.rows() / size_y;

	init[0]=static_cast<GridT>(std::min(data[0][0],data[data.rows()-1][0]));
	init[1]=static_cast<GridT>(std::min(data[0][1],data[data.rows()-1][1]));

	bool y_direct = (init[1] == data[0][1]);

	reg.resize(size_y,size_x);
	if( data[0][0] == data[1][0] || FileType==1 )
	{
	//cas x fixe, y varie
	grid[0]=static_cast<GridT>(std::abs(*(data.col_begin(0)+size_y)-(*(data.col_begin(0)))));
	grid[1]=static_cast<GridT>(std::abs(*(data.col_begin(1)+1)-*(data.col_begin(1))));
	if ( FileType==0 )
	{
	std::size_t gridy = 0;
	
	for(std::size_t j = 0; j < size_x; ++j)
	  {
		 if(y_direct)
		  {
		std::copy(data.col_begin(3) + gridy,
			  data.col_begin(3) + (gridy + size_y),
			  reg.col_rbegin(1,j));
		std::copy(data.col_begin(2) + gridy,
			  data.col_begin(2) + (gridy + size_y),
			  reg.col_rbegin(0,j));
		gridy += size_y;
		  }
		 else
		   {
		 std::copy(data.col_begin(3) + gridy,
			   data.col_begin(3) + (gridy + size_y),
			   reg.col_begin(1,j));
		 std::copy(data.col_begin(2) + gridy,
			   data.col_begin(2) + (gridy + size_y),
			   reg.col_begin(0,j));
		 gridy += size_y;
		   }
	  }
	}
	}
	else
	{
	//cas x varie, y fixe
	grid[0]=static_cast<GridT>(std::abs(*(data.col_begin(0)+1)-(*(data.col_begin(0)))));
	grid[1]=static_cast<GridT>(std::abs(*(data.col_begin(1)+size_x) - *(data.col_begin(1))));
	std::size_t gridx = 0;
	for(std::size_t j = 0; j < size_y; ++j)
	  {
		if(y_direct)
		  {
		std::copy(data.col_begin(2) + gridx,
			  data.col_begin(2) + (gridx + size_x),
			  reg.row_begin(0,(size_y - 1) - j));
		std::copy(data.col_begin(3) + gridx, 
			  data.col_begin(3) + (gridx + size_x),
			  reg.row_begin(1,(size_y - 1) - j));
		
		  }
		else
		  {
		std::copy(data.col_begin(2) + gridx,
			  data.col_begin(2) + (gridx + size_x),
			  reg.row_begin(0,j));
		std::copy(data.col_begin(3) + gridx, 
			  data.col_begin(3) + (gridx + size_x),
			  reg.row_begin(1,j));
		  }
		gridx += size_x;
	   }
	}
	
	if ( FileType!=2 )
	{
		reg.set_init_point(init);
		reg.set_grid_step(grid);
	}
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Read a slip::RegularVector2dField2d from a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector2dField2d container.
** \param zone_loaded Zone index to read (1 by default).
** \par Example:
** \code
** // Read the "test_2D.plt" file and put the data in the container3d Reg.
** slip::RegularVector2dField2d<double> Reg;
** std::string file_name="test_2D.plt";
** plt_to_RegularVector2dField2d(file_name, Reg);
** std::cout<<"The dimensions of the RegularVector2dField2d are "<<Reg.dim1()<<", "<<Reg.dim2()<<std::endl;
** std::cout<<"The init_point is "<<Reg.get_init_point()<<std::endl;
** std::cout<<"The grid_step is "<<Reg.get_grid_step()<<std::endl;
** \endcode
*/
template<typename RegularVector2dField2d>
void generic_plt_to_RegularVector2dField2d(const std::string &file_path_name,
	RegularVector2dField2d &reg,
	const int zone_loaded=1)
{
	std::string _useless_string;
	std::vector<std::string> _useless_varnames;
	slip::generic_plt_to_RegularVector2dField2d(file_path_name,
		reg,
		_useless_string,
		_useless_string,
		_useless_varnames,
		zone_loaded);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Write a slip::RegularVector2dField2d to a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector2dField2d container.
** \param title Title of the tecplot frame ("Title" by default).
** \param zone Name of the tecplot zone ("Zone" by default).
** \param varnames Vector containing the variable names ("X", "Y", "U", "V" by default).
** \par Example:
** \code
** // Write the RegularVector2dField2d Reg to the "test_3D.plt" file.
** slip::RegularVector2dField2d<double> Reg(10,20,1.);
** std::string file_name="test_2D_write.plt", title="Title of the file", zonename="Name of the zone";
** RegularVector2dField2d_to_plt(file_name, Reg, title, zonename);
** \endcode
*/
template<typename RegularVector2dField2d>
void RegularVector2dField2d_to_plt(const std::string &file_path_name,
	RegularVector2dField2d &reg,
	std::string title,
	std::string zone,
	const double SolTime,
	std::vector<std::string> varnames=std::vector<std::string>(),
	const int FileType=0)
{
	typedef typename RegularVector2dField2d::vector2d_value_type T;
	typedef typename RegularVector2dField2d::grid_value_type GridT;
	std::ofstream fp (file_path_name.c_str(), std::ios::out|std::ios::binary);
/******************************************************************************/
/********************************** HEADER ************************************/
/******************************************************************************/

// Lecture de la version du fichier Tecplot
	fp.write("#!TDV112",8);
	int *myint=new int;
	char *mychar=new char;
	float *myfloat=new float;
	double *mydouble=new double;
// Écriture de l'ordre
	*myint=1;
	fp.write((char*)myint, sizeof(int));

// Écriture du type de fichier
	*myint=FileType;
	fp.write((char*)myint, sizeof(int));

//	// Écriture du titre
	std::string strtmp=title;
	for (size_t i = 0; i <= strtmp.size(); i++)
	{
		*myint=(int)strtmp[i];
		fp.write((char*)myint,sizeof(int));
	}

// Définition des variables en fonction du type de fichier
	size_t imin=0, imax=4, nbvars=4;
	switch (FileType)
	{
		case 1:
			imin=0;
			imax=2;
			nbvars=2;
			if (varnames.size()<2)
			{
				varnames.clear();
				varnames.push_back("X");
				varnames.push_back("Y");
			}
			break;
		case 2:
			imin=0;
			imax=2;
			nbvars=2;
			if (varnames.size()<2)
			{
				varnames.clear();
				varnames.push_back("U");
				varnames.push_back("V");
			}
			break;
		default:
			if (varnames.size()<4)
			{
				varnames.clear();
				varnames.push_back("X");
				varnames.push_back("Y");
				varnames.push_back("U");
				varnames.push_back("V");
			}
			break;
	}

// Écriture du nombre de variables
	*myint=nbvars;
	fp.write((char*)myint, sizeof(int));

// Écriture des noms des variables
	for (size_t i = imin; i < imax; i++)
	{
		for (size_t j = 0; j <= varnames[i].size(); j++)
		{
			*myint=(int)varnames[i][j];
			fp.write((char*)myint,sizeof(int));
		}
	}

// Écriture des paramètres des zones
// Écriture du zone marker
	*myfloat=299.0;
	fp.write((char*)myfloat, sizeof(float));
// Écriture du nom de la zone
	strtmp=zone;
	for (size_t i = 0; i <= strtmp.size(); i++)
	{
		*myint=(int)strtmp[i];
		fp.write((char*)myint,sizeof(int));
	}
// Écriture du Parent zone
	*myint=-1;
	fp.write((char*)myint, sizeof(int));
// Écriture du StrandID
	*myint=-1;
	fp.write((char*)myint, sizeof(int));
// Écriture du Solution time
	*mydouble=SolTime;
	fp.write((char*)mydouble, sizeof(double));
// Écriture du -1 inutile
	*myint=-1;
	fp.write((char*)myint, sizeof(int));
// Écriture du ZoneType
	*myint=0;
	fp.write((char*)myint, sizeof(int));
// Écriture du Specify Var Location
	fp.write((char*)myint, sizeof(int));
// Écriture du Supplied Face Neighboring
	fp.write((char*)myint, sizeof(int));
// Écriture du User Defined Face Connections
	fp.write((char*)myint, sizeof(int));
// Écriture des IMax, JMax et KMax
	*myint=reg.dim2();
	fp.write((char*)myint, sizeof(int));
	*myint=reg.dim1();
	fp.write((char*)myint, sizeof(int));
	*myint=1;
	fp.write((char*)myint, sizeof(int));
// Écriture du Auxiliary Name
	*myint=0;
	fp.write((char*)myint, sizeof(int));
// Écriture du marker de fin de header
	*myfloat=357.0;
	fp.write((char*)myfloat, sizeof(float));

/******************************************************************************/
/*********************************** DATA *************************************/
/******************************************************************************/
// Écriture des données des zones
// Écriture du marker de début de zone
	*myfloat=299.0;
	fp.write((char*)myfloat, sizeof(float));
// Écriture du type des données
	*myint=get_tecplot_type<GridT>();
	if (FileType==0 || FileType==1)
	{
		for (size_t i = 0; i < 2; ++i)
			fp.write((char*)myint, sizeof(int));
	}
	*myint=get_tecplot_type<T>();
	if (FileType==0 || FileType==2)
	{
		for (size_t i = 0; i < 2; ++i)
			fp.write((char*)myint, sizeof(int));
	}
// Écriture de trucs inutiles pour nous
	*myint=0;
	fp.write((char*)myint, sizeof(int));
	fp.write((char*)myint, sizeof(int));
	*myint=-1;
	fp.write((char*)myint, sizeof(int));
// Écriture des min et max de chaque variable
	if (FileType==0 || FileType==1)
	{
		*mydouble=static_cast<double>(reg.get_init_point()[0]);
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=static_cast<double>(reg.get_init_point()[0]+(reg.dim2()-1)*reg.get_grid_step()[0]);
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=static_cast<double>(reg.get_init_point()[1]);
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=static_cast<double>(reg.get_init_point()[1]+(reg.dim1()-1)*reg.get_grid_step()[1]);
		fp.write((char*)mydouble, sizeof(double));
	}

	if (FileType==0 || FileType==2)
	{
		double umin, umax, vmin, vmax, wmin, wmax;
		umin=static_cast<double>((*std::min_element(reg.begin(),reg.end(),slip::GenericComparator<slip::Vector2d<T> >(0)))[0]);
		umax=static_cast<double>((*std::max_element(reg.begin(),reg.end(),slip::GenericComparator<slip::Vector2d<T> >(0)))[0]);
		vmin=static_cast<double>((*std::min_element(reg.begin(),reg.end(),slip::GenericComparator<slip::Vector2d<T> >(1)))[1]);
		vmax=static_cast<double>((*std::max_element(reg.begin(),reg.end(),slip::GenericComparator<slip::Vector2d<T> >(1)))[1]);
		*mydouble=umin;
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=umax;
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=vmin;
		fp.write((char*)mydouble, sizeof(double));
		*mydouble=vmax;
		fp.write((char*)mydouble, sizeof(double));
	}

	if (FileType==0 || FileType==1)
	{
		const int size_grid=sizeof(GridT);
		for (int ii = reg.dim1()-1; ii >= 0; --ii)
		{
			for (int jj = 0; jj < (int)reg.dim2(); ++jj)
			{
				GridT *temp=new GridT;
				*temp=reg.x(ii,jj);
				fp.write((char*)temp, size_grid);
				delete temp;
			}
		}
		for (int ii = reg.dim1()-1; ii >= 0; --ii)
		{
			for (int jj = 0; jj < (int)reg.dim2(); ++jj)
			{
				GridT *temp=new GridT;
				*temp=reg.y(ii,jj);
				fp.write((char*)temp, size_grid);
				delete temp;
			}
		}
	}

	if (FileType==0 || FileType==2)
	{
		const int size_data=sizeof(T);
		for (size_t i = 0; i < 2; ++i)
		{
			for (int ii = reg.dim1()-1; ii >= 0; --ii)
			{
				for (int jj = 0; jj < (int)reg.dim2(); ++jj)
				{
					T *temp=new T;
					*temp=reg(ii,jj)[i];
					fp.write((char*)temp, size_data);
					delete temp;
				}
			}
		}
	}
	delete myint;
	delete mychar;
	delete myfloat;
	delete mydouble;
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Write a slip::RegularVector2dField2d to a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector2dField2d container.
** \param FileType Type of file : 0=FULL, 1=Grid only, 2=Solution only (0 by default).
** \par Example:
** \code
** // Write the RegularVector2dField2d Reg in the "test_3D.plt" file.
** slip::RegularVector2dField2d<double> Reg(10,20,1.);
** std::string file_name="test_3D_write.plt";
** RegularVector2dField2d_to_plt(file_name, Reg);
** \endcode
*/
template<typename RegularVector2dField2d>
void RegularVector2dField2d_to_plt(const std::string &file_path_name,
	RegularVector2dField2d &reg,
	const int FileType=0)
{
	RegularVector2dField2d_to_plt(file_path_name, reg, "Title", "Zone", 0., std::vector<std::string>(), FileType);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Write a slip::RegularVector2dField2d to a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector2dField2d container.
** \param SolTime Solution Time.
** \param FileType Type of file : 0=FULL, 1=Grid only, 2=Solution only (0 by default).
** \par Example:
** \code
** // Write the RegularVector2dField2d Reg in the "test_3D.plt" file using defined title, zone name and variable names.
** slip::RegularVector2dField2d<double> Reg(10,20,1.);
** std::string file_name="test_3D_write.plt";
** const double t=0.5;
** RegularVector2dField2d_to_plt(file_name, Reg, t);
** \endcode
*/
template<typename RegularVector2dField2d>
void RegularVector2dField2d_to_plt(const std::string &file_path_name,
	RegularVector2dField2d &reg,
	const double SolTime,
	const int FileType=0)
{
	RegularVector2dField2d_to_plt(file_path_name, reg, "Title", "Zone", SolTime, std::vector<std::string>(), FileType);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Write a slip::RegularVector2dField2d to a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector2dField2d container.
** \param title Title of the tecplot frame ("Title" by default).
** \param zone Name of the tecplot zone ("Zone" by default).
** \param FileType Type of file : 0=FULL, 1=Grid only, 2=Solution only (0 by default).
** \par Example:
** \code
** // Write the RegularVector2dField2d Reg in the "test_3D.plt" file using defined title, zone name and variable names.
** slip::RegularVector2dField2d<double> Reg(10,20,1.);
** std::string file_name="test_3D_write.plt", title="Title of the file", zonename="Name of the zone";
** RegularVector2dField2d_to_plt(file_name, Reg, title, zone);
** \endcode
*/
template<typename RegularVector2dField2d>
void RegularVector2dField2d_to_plt(const std::string &file_path_name,
	RegularVector2dField2d &reg,
	std::string title,
	std::string zone,
	const int FileType=0)
{
	RegularVector2dField2d_to_plt(file_path_name, reg, title, zone, 0., std::vector<std::string>(), FileType);
}

/*!
** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/09/07
** \since 1.4.0
** \brief Write a slip::RegularVector2dField2d to a binary tecplot file.
** \param file_path_name String of the file path name.
** \param reg slip::RegularVector2dField2d container.
** \param title Title of the tecplot frame ("Title" by default).
** \param zone Name of the tecplot zone ("Zone" by default).
** \param FileType Type of file : 0=FULL, 1=Grid only, 2=Solution only (0 by default).
** \par Example:
** \code
** // Write the RegularVector2dField2d Reg in the "test_3D.plt" file using defined title, zone name and variable names.
** slip::RegularVector2dField2d<double> Reg(10,20,1.);
** std::string file_name="test_3D_write.plt", title="Title of the file", zonename="Name of the zone";
** std::vector<std::string> varnames; // if varnames is empty, the default values X, Y, U, V are used
** RegularVector2dField2d_to_plt(file_name, Reg, title, zone);
** \endcode
*/
template<typename RegularVector2dField2d>
void RegularVector2dField2d_to_plt(const std::string &file_path_name,
	RegularVector2dField2d &reg,
	std::string title,
	std::string zone,
	std::vector<std::string> varnames,
	const int FileType=0)
{
	RegularVector2dField2d_to_plt(file_path_name, reg, title, zone, 0., varnames, FileType);
}

}//::slip
#endif
