/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file histo.hpp
 * 
 * \brief Provides some algorithms to compute histograms.
 * \since 1.0.0
 */
#ifndef SLIP_HISTO_HPP
#define SLIP_HISTO_HPP

#include <iterator>
#include <algorithm>
#include <numeric>
#include <cassert>
#include <cmath>
#include <iostream>

#include "Array.hpp"
#include "arithmetic_op.hpp"
#include "statistics.hpp"

namespace slip
{

 /*!
  ** \brief Simple histogram algorithm (uniform step)
  ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
  ** \date 2007/02/08
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first begin InputIterator of the container to histogram
  ** \param last past-the-end InputIterator of the container to histogram
  ** \param histo_first begin RandomAccessIterator of the histogram container
  ** \param histo_last past-the-end RandomAccessIterator of the histogram container
  ** \param minval Minimum value of the histogram
  ** \param maxval Maximum value of the histogram
  ** \param step Step of the histogram. Default value is 1.
  ** \pre (histo_last - histo_first) == (max_val - min_val + 1)/step
  ** \pre step > 0
  ** \par Example:
  ** \code
  ** slip::Matrix<double> M(5,5);
  ** slip::iota(M.begin(),M.end(),1.0,1.0);
  ** slip::Vector<int> Histo(10);
  ** slip::histogram(M.begin(),M.end(),Histo.begin(),Histo.end(),0,30,3);
  ** \endcode
  */
  template<typename InputIterator, typename RandomAccessIterator>
  inline
  void histogram(InputIterator first,
		 InputIterator last,
		 RandomAccessIterator histo_first,
		 RandomAccessIterator histo_last,
		 typename std::iterator_traits<InputIterator>::value_type minval,
		 typename std::iterator_traits<InputIterator>::value_type maxval,
		 typename std::iterator_traits<InputIterator>::value_type step = 1)	 
  {
    //    typedef typename std::iterator_traits<InputIterator>::value_type T;
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type histo_T;
    
    assert(first != last);
    assert(step > 0);
    assert((histo_last - histo_first) == (int)((maxval - minval + 1)/step));
    
    // Initialize result and check values
    std::fill(histo_first,histo_last,histo_T());
    int maxi = static_cast<int>((histo_last-histo_first) - 1);
    
    InputIterator first2 = first;
    bool error = false;
    for (; !error && first2 != last; ++first2)
      {
	if ( (*first2 < minval) || (*first2 > maxval) )
	  {
	    error = true;
	  }
      }
    if (error)
      {
	std::cerr << "Some values of the input datas are outside the defined interval: ["<<minval<<","<<maxval<<"]"<<std::endl;
      }
    // Store the histogram
    while(first != last)
      {
	int i = (int) trunc((*first - minval) / step);
	if ((i > maxi) || (i < 0)) 
	  {
	    if (*first == maxval)
	      {
		(*(histo_first + maxi))++;
	      } 
	    // Else not take into account
	  } 
	else 
	  {
	    (*(histo_first + i))++;
	  }
	++first;
      }
  }

  /*!
  ** \brief Simple histogram algorithm (uniform step) according to a mask sequence.
  ** \author Mouahmed Hammoud <hammoud_AT_sic.univ-poitiers.fr> : conceptor
  ** \date 2009/09/10
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first begin InputIterator of the container to histogram
  ** \param last past-the-end InputIterator of the container to histogram
  ** \param mask_first  An InputIterator on the mask 
  ** \param histo_first begin RandomAccessIterator of the histogram container
  ** \param histo_last past-the-end RandomAccessIterator of the histogram container
  ** \param minval Minimum value of the histogram
  ** \param maxval Maximum value of the histogram
  ** \param step Step of the histogram. Default value is 1.
  ** \param value Mask value.
  ** \pre (histo_last - histo_first) == (max_val - min_val + 1)/step
  ** \pre step > 0
  ** \par Example:
  ** \code
  ** slip::Matrix<double> M(5,5);
  ** slip::iota(M.begin(),M.begin()+5,2.0,0.0);
  ** slip::iota(M.begin()+5,M.begin()+10,3.0,0.0);
  ** slip::iota(M.begin()+10,M.begin()+18,4.0,0.0);
  ** std::cout<<"M = \n"<<M<<std::endl;
  ** slip::Matrix<int> MaskValue(5,5);
  ** std::fill(MaskValue.begin(),MaskValue.begin()+12,1);
  ** std::fill(MaskValue.begin()+12,MaskValue.begin()+21,2);
  ** std::cout<<"MaskValue=\n"<<MaskValue<<std::endl;
  ** slip::Vector<int> Histo_mask(25);
  ** slip::histogram_mask(M.begin(),M.end(),
  **		       MaskValue.begin(),
  ** 		       Histo_mask.begin(),Histo_mask.end(),0,24,1);
  ** std::cout<<"Histo_mask(0,24,1) mask_value = 1 : \n"<<Histo_mask<<std::endl;
  ** slip::histogram_mask(M.begin(),M.end(),
  ** 		       MaskValue.begin(),
  **		       Histo_mask.begin(),Histo_mask.end(),0,24,1,2);
  ** std::cout<<"Histo_mask(0,24,1) mask_value = 2 : \n"<<Histo_mask<<std::endl;
  ** \endcode
  */
  template<typename InputIterator, 
	   typename RandomAccessIterator, 
	   typename MaskIterator>
  inline
  void histogram_mask(InputIterator first,
		      InputIterator last, 
		      MaskIterator mask_first,
		      RandomAccessIterator histo_first,
		      RandomAccessIterator histo_last,
		      typename std::iterator_traits<InputIterator>::value_type minval,
		      typename std::iterator_traits<InputIterator>::value_type maxval,
		      typename std::iterator_traits<InputIterator>::value_type step = 1,
		      typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))	 
  {
    assert(first != last);
    assert(step > 0);
    assert((histo_last - histo_first) == (int)((maxval - minval + 1)/step));

    //    typedef typename std::iterator_traits<InputIterator>::value_type T;
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type histo_T;
    
    // Initialize result and check values
    std::fill(histo_first,histo_last,histo_T());
    int maxi = static_cast<int>((histo_last-histo_first) - 1);

    InputIterator first2 = first;
    MaskIterator  mask_first2 = mask_first;
    bool error = false;
    for (; !error && first2 != last; ++first2, ++mask_first2)
      {
	if(*mask_first2 == value)
	  {
	    if ( (*first2 < minval) || (*first2 > maxval) )
	      {
		error = true;
	      }
	  }
      }
    if (error)
      {
	std::cerr << "Some values of the input datas are outside the defined interval: ["<<minval<<","<<maxval<<"]"<<std::endl;
      }
    // Store the histogram
    
    while(first != last)
      {
	if(*mask_first==value)
	  {  
	    int i = (int) trunc((*first - minval) / step);
	    
	    if ((i > maxi) || (i < 0)) 
	      {
		if (*first == maxval)
		  {
		    (*(histo_first + maxi))++;
		    
		  } 
		// Else not take into account
	      } 
	    else 
	      {
                (*(histo_first + i))++;
	      }
	  }
	++first;
	++mask_first;
      }
       
  }



  /*!
  ** \brief Simple histogram algorithm (uniform step) according to a predicate
  ** \author Mouahmed Hammoud <hammoud_AT_sic.univ-poitiers.fr> : conceptor
  ** \date 2009/09/10
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first begin InputIterator of the container to histogram
  ** \param last past-the-end InputIterator of the container to histogram
  ** \param histo_first begin RandomAccessIterator of the histogram container
  ** \param histo_last past-the-end RandomAccessIterator of the histogram container
  ** \param pred predicate function.
  ** \param minval Minimum value of the histogram
  ** \param maxval Maximum value of the histogram
  ** \param step Step of the histogram. Default value is 1.
 
  ** \pre (histo_last - histo_first) == (max_val - min_val + 1)/step
  ** \pre step > 0
  ** \par Example:
  ** \code
  ** bool myPredicate (const double& val)
  ** {
  **    return (val > 3);
  ** }
  ** \endcode
  ** \code
  ** slip::Matrix<double> M(5,5);
  ** std::fill(M.begin(),M.begin()+5,2.0);
  ** std::fill(M.begin()+5,M.begin()+10,3.0);
  ** std::fill(M.begin()+10,M.begin()+18,4.0);
  ** slip::Vector<int> Histo_if(25);
  ** slip::Vector<int> Histo_if(25);
  ** slip::histogram_if(M.begin(),M.end(),
  **                    Histo_if.begin(),Histo_if.end(),
  **	                myPredicate,
  **                    0,24);
  ** std::cout<<"Histo_if(0,24,mypredicate : val > 3) = \n"<<Histo_if<<std::endl;
  ** \endcode
  */
  template<typename InputIterator, 
	   typename RandomAccessIterator, 
	   typename Predicate>
  inline
  void histogram_if(InputIterator first,
		    InputIterator last,
		    RandomAccessIterator histo_first,
		    RandomAccessIterator histo_last,
		    Predicate pred,
		    typename std::iterator_traits<InputIterator>::value_type minval,
		    typename std::iterator_traits<InputIterator>::value_type maxval,
		    typename std::iterator_traits<InputIterator>::value_type step = 1
		    )
  {
    assert(first != last);
    assert(step > 0);
    assert((histo_last - histo_first) == (int)((maxval - minval + 1)/step));
    //    typedef typename std::iterator_traits<InputIterator>::value_type T;
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type histo_T;
    
   
   

    // Initialize result and check values
    std::fill(histo_first,histo_last,histo_T());
    int maxi = static_cast<int>((histo_last-histo_first) - 1);
   
    InputIterator first2 = first;
    bool error = false;
    for (; !error && first2 != last; ++first2)
      {
	if(pred(*first2))
	  {
	    if ( (*first2 < minval) || (*first2 > maxval) )
	      {
		error = true;
	      }
	  }
      }
    if (error)
      {
	std::cerr << "Some values of the input datas are outside the defined interval: ["<<minval<<","<<maxval<<"]"<<std::endl;
      }
    // Store the histogram
    while(first != last)
      {
	if(pred(*first))
           {  
   	     int i = (int) trunc((*first - minval) / step);

	    if ((i > maxi) || (i < 0)) 
	      {
		if (*first == maxval)
		  {
		    (*(histo_first + maxi))++;

		  } 
		// Else not take into account
	      } 
	    else 
	      {
                (*(histo_first + i))++;
		
              }
            }
	    ++first;

	  }
  }

 /*!
  ** \brief Complex histogram algorithm (variable step)
  ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
  ** \date 2007/02/08
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first begin iterator of the container to histogram
  ** \param last past-the-end iterator of the container to histogram
  ** \param stepfirst begin iterator of the container to steps
  ** \param steplast past-the-end iterator of the container to steps
  ** \param result begin iterator of the result container
  ** \par Example:
  ** \code
  ** slip::Matrix<double> M(5,5);
  ** slip::iota(M.begin(),M.end(),1.0,1.0);
  ** double steps[] = {0.0,2.3,4.5,5.7,7.0,30.0};  
  ** slip::Vector<int> Histo(5);
  ** slip::real_histogram(M.begin(),M.end(),steps,steps+6,Histo.begin());
  ** \endcode
  */
  template<typename InputIterator, typename InputIterator2, typename OutputIterator>
  inline
  void real_histogram(InputIterator first,
		      InputIterator last,
		      InputIterator2 stepfirst,
		      InputIterator2 steplast,
		      OutputIterator result)	 
  {
    assert(first != last);
    assert(stepfirst != steplast);

    InputIterator2 runsteps;
    InputIterator first2 = first;
    bool error = false;
    bool end = false;
    int i = 0;
    // Initialize result and check values
	
    for (runsteps=stepfirst+1,i=0;runsteps!=steplast;i++,runsteps++)
      {
        *(result + i) = 0;
      }

    for (; !error && (first2 != last); ++first2){
        if ( (*first2 < *stepfirst) || (*first2 > *(steplast-1)))
	  {
            error=true;
	  }
    }
    if (error)
      {
        std::cerr << "Some values of the input datas are outside the defined interval: ["<<*stepfirst<<","<<*(steplast-1)<<"]"<<std::endl;
      }


     // Store the histogram
	while(first != last)
	  {
	    i = -1;
	    end = false;
	    
	    for (runsteps=stepfirst;!end && runsteps != steplast; runsteps++)
	      {
		if (*first >= *runsteps)
		  {
		    i++;
		  } 
		else 
		  {
		    end = true;
		  }
	      }
	    // Over value : do not take into account
	    if (!end && (*first >= *(steplast-1)))
	      {
		// High Bound case 
		if (*first == *(steplast-1))
		  {
		    i--;
		  } 
		else 
		  {
		    i = -1;
		  }
	      } 

	    
	    if (i >= 0)
	      {
		(*(result+i))++;
	      }
	    ++first;
	  }
  }
 


/*!
  ** \brief Computes the cumulative histogram from a histogram.
  ** \author Mouhamed hammoud  <hammoud_AT_sic.univ-poitiers.fr> : conceptor
  ** \date 2008/08/24
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param histo_first A RandomAccessIterator to the first element of the histogram.
  ** \param histo_last A RandomAccessIterator to the one past-the-end element of the histogram.
  ** \param cum_histo_first A RandomAccessIterator to the first element of the cumulative histogram.
  ** \param cum_histo_last A RandomAccessIterator to the one past-the-end element of the cumulative histogram.
  ** \pre [histo_first,histo_last) is a valid range.
  ** \pre [cum_histo_first,cum_histo_last) is a valid range.
  ** \pre (histo_last - histo_first) == (cum_histo_last - cum_histo_first)
  ** \par Example:
  ** \code
  ** slip::GrayscaleImage<unsigned char> I1;
  ** I1.read("lena.gif");
  ** slip::Vector<int> Histo(256); 
  ** slip::histogram(I1.begin(),I1.end(),Histo.begin(),Histo.end(),0,255, 1);
  ** slip::Vector<int> CumHisto(256); 
  ** slip::cumulative_histogram(Histo.begin(),Histo.end(),
  **                            CumHisto.begin(),CumHisto.end());
  ** std::cout<<CumHisto<<std::endl;
  ** \endcode
  */
template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  inline
  void cumulative_histogram(RandomAccessIterator1 histo_first,
                            RandomAccessIterator1 histo_last,
			    RandomAccessIterator2 cum_histo_first,
                            RandomAccessIterator2 cum_histo_last)	 
  {

     assert(histo_last != histo_first);
     assert((histo_last - histo_first) == (cum_histo_last - cum_histo_first));

     //     typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
     //std::copy(histo_first,histo_last,cum_histo_first);
     *cum_histo_first++ = *histo_first++;
     for(; histo_first != histo_last; ++histo_first, ++cum_histo_first)
       {
	 *cum_histo_first = *(cum_histo_first-1) + *histo_first;
       }
  }


 template<typename T, typename Real>
 class RealHistogram;

/*! \class RealHistogram
** 
** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/04/12
** \since 1.5.0
** \brief DataStructure to handle  real histogram.
** \todo do not allocate steps_ for uniform histogram
** \pre T must be a real type (float, double, long double)
*/
  template <typename T, typename Real = double>
class RealHistogram
{
public:
  typedef RealHistogram<T> self;
  typedef long CmpT;
  //typedef double Real;

  typedef slip::Array<CmpT>::value_type value_type;
  typedef slip::Array<CmpT>::pointer pointer;
  typedef slip::Array<CmpT>::const_pointer const_pointer;
  typedef slip::Array<CmpT>::reference reference;
  typedef slip::Array<CmpT>::const_reference const_reference;

  typedef slip::Array<CmpT>::difference_type difference_type;
  typedef slip::Array<CmpT>::size_type size_type;

  typedef slip::Array<CmpT>::iterator iterator;
  typedef slip::Array<CmpT>::const_iterator const_iterator;


  static const std::size_t DIM = 1;

  /**
   ** \name Constructors & Destructors
   */
  /*@{*/
  /*!
   ** \brief Default constructor. 
   ** 
   */
  RealHistogram():
    uniform_(true),
    val_min_(static_cast<T>(0)),
    val_max_(static_cast<T>(256)),
    step_(static_cast<T>(1)),
    bins_number_(0),
    steps_(0),
    histo_(0),
    cumul_histo_(0)
   {
   }
   
  RealHistogram(const T& val_min,
		const T& val_max,
		const T& step):
    uniform_(true),
    val_min_(val_min),
    val_max_(val_max),
    step_(step),
    bins_number_(static_cast<CmpT>(std::floor((val_max-val_min)/step))),
    steps_(new slip::Array<T>(bins_number_+1)),
    histo_(new slip::Array<CmpT>(bins_number_)),
    cumul_histo_(0)
   {
     slip::iota(this->steps_->begin(),
		this->steps_->end(),
		this->val_min_,
		this->step_);
   }
  

  RealHistogram(const T& val_min,
	    const T& val_max,
	    const std::size_t& bins_number):
    uniform_(true),
    val_min_(val_min),
    val_max_(val_max),
    step_((val_max-val_min)/static_cast<T>(bins_number+1)),
    bins_number_(bins_number),
    steps_(new slip::Array<T>(bins_number+1)),
    histo_(new slip::Array<CmpT>(bins_number)),
    cumul_histo_(0)
   {
     slip::iota(this->steps_->begin(),
		this->steps_->end(),
		this->val_min_,
		this->step_);
   }

template<typename RandomAccessIterator>
RealHistogram(RandomAccessIterator steps_first,
	  RandomAccessIterator steps_last):
		 uniform_(false),
		 val_min_(*steps_first),
		 val_max_(*(steps_last-1)),
                 step_(0),
		 bins_number_(static_cast<std::size_t>(steps_last-steps_first-1)),
		 steps_(new slip::Array<T>(static_cast<std::size_t>(steps_last-steps_first),steps_first,steps_last)),
		 histo_(new slip::Array<CmpT>(static_cast<std::size_t>(steps_last-steps_first-1))),
		 cumul_histo_(0)
		 {
		 }

  RealHistogram(const self& rhs):
     uniform_(rhs.uniform_),
     val_min_(rhs.val_min_),
     val_max_(rhs.val_max_),
     step_(rhs.step_),
     bins_number_(rhs.bins_number_)
  {
    this->allocate();
    //copy
    if((rhs.bins_number_ + 1)!= 0)
      {
	std::copy(rhs.steps_->begin(),rhs.steps_->end(),this->steps_->begin());
      }
    if(rhs.bins_number_ != 0)
      {
	std::copy(rhs.histo_->begin(),rhs.histo_->end(),this->histo_->begin());
      }
    //allocate cumul_histo_ and copy
    if(rhs.cumul_histo_ != 0)
      {
	this->cumul_histo_ = new slip::Array<CmpT>(*(rhs.cumul_histo_));
      }
    else
      {
	this->cumul_histo_ = 0;
      }
  }

  /*!
   ** \brief Destructor.
   */
  ~RealHistogram()  
  {
    this->desallocate();
  }

  /*@} End Constructors */

 /*! 
  ** \brief Assign a %RealHistogram
  ** \param rhs A %RealHistogram to get the value from.
  ** \return %RealHistogram.
  */
  self& operator=(const self& rhs)
  {
    if(this != &rhs)
      {
	if(this->bins_number_ != rhs.bins_number_)
	  {
	    this->desallocate();
	    this->copy_attributes(rhs);
	    this->allocate();
	  	    
	    if(rhs.cumul_histo_ != 0)
	      {
	    	this->cumul_histo_ = new slip::Array<CmpT>(*(rhs.cumul_histo_));
	      }
	    else
	      {
		this->cumul_histo_ = 0;
	      }
	  }
	else
	   {
	     this->copy_attributes(rhs);
	      if(rhs.cumul_histo_ != 0)
	       {
	 	this->cumul_histo_ = new slip::Array<CmpT>(*(rhs.cumul_histo_));
	       }
	      else
		{
		  this->cumul_histo_ = 0;
		}
	  }

	//copy
	if((rhs.bins_number_ + 1)!= 0)
	  {
	    std::copy(rhs.steps_->begin(),rhs.steps_->end(),this->steps_->begin());
	  }
	if(rhs.bins_number_ != 0)
	  {
	    std::copy(rhs.histo_->begin(),rhs.histo_->end(),this->histo_->begin());
	   }

      }

    return *this;
  }

 const_iterator begin() const
  {
    return histo_->begin();
  }

  iterator begin()
  {
    return histo_->begin();
  }

  const_iterator end() const
  {
    return histo_->end();
  }

  iterator end()
  {
    return histo_->end();
  }
  
  const_iterator cumul_begin() const
  {
    return cumul_histo_->begin();
  }

  iterator cumul_begin()
  {
    return cumul_histo_->begin();
  }

  const_iterator cumul_end() const
  {
    return cumul_histo_->end();
  }

  iterator cumul_end()
  {
    return cumul_histo_->end();
  }
  
   /*!
   ** \brief Print histogram attributes.
   */
  void print() const
  {
    std::cout<<"uniform = "<<uniform_<<std::endl;
    std::cout<<"val_min = "<<val_min_<<std::endl;
    std::cout<<"val_max = "<<val_max_<<std::endl;
    std::cout<<"step    = "<<step_<<std::endl;
    std::cout<<"bins_number = "<<bins_number_<<std::endl;
    if(this->histo_!=0)
      {
	std::cout<<"histo = \n"<<*histo_<<std::endl;
      }
    if(this->cumul_histo_!=0)
      {
	std::cout<<"cumul_histo = \n"<<*cumul_histo_<<std::endl;
      }
    if(this->steps_!=0)
      {
	std::cout<<"steps_ = \n"<<*steps_<<std::endl;
      }
  }

  /*!
   ** \brief Returns name of the class.
   ** \return std::string
   */
  std::string name() const
  {
    return std::string("RealHistogram");
  }

  
   /*!
   ** \brief Returns the bin of the value \a value.
   ** \param value Value to find.
   ** \return bin of the value.
   ** \todo fix for T = integer
   */
  inline
  CmpT find_bin(const T& value)
  {
    typename slip::Array<T>::iterator it = std::lower_bound(steps_->begin(),
							    steps_->end(),
							    value);
    typename slip::Array<T>::iterator itl = it; 
    if(it != this->steps_->begin())
	{
	  if(std::abs(*it - value) < std::numeric_limits<T>::epsilon())
	    //if(*it == value)
	    {
	      //std::cout<<" == "<<std::endl;
	      itl = it;
	    }
	  else
	    {
	      itl = (it - 1);
	    }
	}
   
    
    return (itl - this->steps_->begin());
  }

  /*!
  ** \brief Returns the size of the histogram.
  ** \return std::size_t
  */
  std::size_t size() const
  {
    return this->bins_number_;
  }

  /*!
  ** \brief Init histograms.
  */
  void init()
  {
    this->histo_->fill(static_cast<CmpT>(0));
    if(this->cumul_histo_ != 0)
      {
	 this->cumul_histo_->fill(static_cast<CmpT>(0));
      }
  }

  /*!
  ** \brief Add \a value to the histogram.
  ** \param value Value to add.
  */
  inline
  void add(const T& value)
  {
    (*histo_)[find_bin(value)]++;
  }

  /*!
  ** \brief Remove \a value to the histogram.
  ** \param value Value to remove.
  */
  inline
  void remove(const T& value)
  {
    (*histo_)[find_bin(value)]--;
  }

  /*!
  ** \brief Computes the histogram of the range [first,last).
  ** \param first RandomAccessIterator the data range.
  ** \param last RandomAccessIterator the data range.
  */
  template <typename RandomAccessIterator>
  void compute(RandomAccessIterator first,
	       RandomAccessIterator last)
  {
    for(; first!=last; ++first)
      {
	this->add(*first);
      }
  }

   /*!
  ** \brief Update the histogram adding the range [first,last).
  ** \param first RandomAccessIterator the data range.
  ** \param last RandomAccessIterator the data range.
  */
  template <typename RandomAccessIterator>
  void add(RandomAccessIterator first,
	   RandomAccessIterator last)
  {
    for(; first!=last; ++first)
      {
	this->add(*first);
      }
  }
   

   /*!
  ** \brief Update the histogram removing the range [first,last).
  ** \param first RandomAccessIterator the data range.
  ** \param last RandomAccessIterator the data range.
  */
  template <typename RandomAccessIterator>
  void remove(RandomAccessIterator first,
	   RandomAccessIterator last)
  {
    for(; first!=last; ++first)
      {
	this->remove(*first);
      }
  }

  /*!
  ** \brief Computes the cumulative histogram from the histogram.
  */
   void cumulate()
  {
    if(this->cumul_histo_ == 0)
      {
	this->cumul_histo_ = new slip::Array<CmpT>(this->bins_number_);
      }
    std::partial_sum(this->histo_->begin(),this->histo_->end(),
		     this->cumul_histo_->begin());
  }

  /*!
  ** \brief Computes the number of values of the histogram.
  ** \return integer
  */
  CmpT values() const
  {
    return (this->bins_number_ - std::count(this->histo_->begin(),
					    this->histo_->end(),CmpT(0)));
  }

  /*!
  ** \brief Computes the median from the histogram.
  ** \param data_size Size of the data range.
  ** \return Real
  */
  inline
  Real median(const std::size_t data_size)  const
  {
   
    typedef typename slip::Array<CmpT>::iterator iterator;
    Real __init = Real((*histo_)[0]);
  
    iterator __first = this->histo_->begin()+1;
    iterator __last  = this->histo_->end();
    Real half_size = static_cast<Real>(data_size)*static_cast<Real>(0.5);

    int i = 0;
    for (; (__init < half_size) && (__first != __last); ++__first)
      {
	__init = __init + *__first;
	i++;
      }
  
    Real x1 = static_cast<Real>((*steps_)[i]);
   
  

  Real med = Real();
  if(data_size%2 == 0)
    {
      Real x2 = static_cast<Real>((*steps_)[i+1]);
      med = (x1+x2)/static_cast<Real>(2.0);
    }
  else
    {
      med = x1;
    }

    return med;
  }

/*!
  ** \brief Computes the min from the histogram.
  ** \return T
  */  
  T min() const
  {
     typedef typename slip::Array<CmpT>::iterator iterator;
     T mini = (*steps_)[0];
     iterator it_histo = this->histo_->begin();
     while((*it_histo == CmpT(0)) && (it_histo != this->histo_->end()))
       {
	 ++it_histo;
       }
     if(it_histo !=  this->histo_->end())
       {
	 mini = (*steps_)[it_histo-this->histo_->begin()];
       }
    
     return mini;
     
  }
  
  /*!
  ** \brief Computes the max from the histogram.
  ** \return T
  */
  T max() const
  {
    typedef typename slip::Array<CmpT>::iterator iterator;
     T maxi = (*steps_)[this->bins_number_];
     iterator it_histo = (this->histo_->end() - 1);
     while((*it_histo == CmpT(0)) && (it_histo != this->histo_->begin()))
       {
	 --it_histo;
       }
     if(it_histo !=  this->histo_->begin())
       {
	 maxi = (*steps_)[it_histo-this->histo_->begin()];
       }
    
     return maxi;
  }

  /*!
  ** \brief Computes the mean from the histogram.
  ** \param data_size Size of the data range.
  ** \return Real
  */
  Real mean(const std::size_t data_size) const
  {
     typedef typename slip::Array<CmpT>::iterator iterator_histo;
     typedef typename slip::Array<T>::iterator iterator_steps;
     Real mean = Real(0.0);
     iterator_histo it_histo = this->histo_->begin();
     iterator_steps it_steps = this->steps_->begin();
     for(;it_histo != this->histo_->end(); ++it_histo,++it_steps)
       {
	 mean += *it_histo * *it_steps;
       }
     return mean/static_cast<Real>(data_size);
  }

  /*!
  ** \brief Computes the variance from the histogram.
  ** \param data_size Size of the data range.
  ** \return Real
  */
  Real var(const std::size_t data_size) const
  {
    Real mean = this->mean(data_size);
     typedef typename slip::Array<CmpT>::iterator iterator_histo;
     typedef typename slip::Array<T>::iterator iterator_steps;
     Real var = Real(0.0);
     iterator_histo it_histo = this->histo_->begin();
     iterator_steps it_steps = this->steps_->begin();
     for(;it_histo != this->histo_->end(); ++it_histo,++it_steps)
       {
	 Real tmp = *it_steps - mean;
	 var += *it_histo * (tmp * tmp);
       }
     return var/static_cast<Real>(data_size);
  }

 /*!
  ** \brief Computes the skewness
  ** \param data_size Size of the data range.
  ** \return Real
  */

  Real skewness(const std::size_t& data_size) const
  {
    Real mean = this->mean(data_size);
    typedef typename slip::Array<CmpT>::iterator iterator_histo;
    typedef typename slip::Array<T>::iterator iterator_steps;
    Real skew = Real();
    Real var = Real();
    iterator_histo it_histo = this->histo_->begin();
    iterator_steps it_steps = this->steps_->begin();
     for(;it_histo != this->histo_->end(); ++it_histo,++it_steps)
       {
	 Real tmp = *it_steps - mean;
	 Real tmp2 = tmp*tmp;
	 skew += *it_histo * (tmp2 * tmp);
	 var += *it_histo * tmp2;
       }
     return skew/(static_cast<Real>(data_size)*var*var*var);
  }

  /*!
  ** \brief Computes the normalized kurtosis
  ** \param data_size Size of the data range.
  ** \return Real
  */

  Real kurtosis(const std::size_t& data_size) const
  {
    Real mean = this->mean(data_size);
     typedef typename slip::Array<CmpT>::iterator iterator_histo;
     typedef typename slip::Array<T>::iterator iterator_steps;
     Real kurt = Real();
     Real var = Real();
     iterator_histo it_histo = this->histo_->begin();
     iterator_steps it_steps = this->steps_->begin();
     for(;it_histo != this->histo_->end(); ++it_histo,++it_steps)
       {
	 Real tmp = *it_steps - mean;
	 Real tmp2 = tmp*tmp;
	 kurt += *it_histo * (tmp2 * tmp2);
	 var += *it_histo * tmp2;
       }
     return kurt/(static_cast<Real>(data_size)*var*var*var*var) - static_cast<Real>(3.0);
  }

   /*!
  ** \brief Computes the standard deviation from the histogram.
  ** \param data_size Size of the data range.
  ** \return Real
  */
Real std(const std::size_t& data_size) const
{
  return std::sqrt(this->var(data_size));
}

 /*!
  ** \brief Computes the unbiased variance from the histogram.
  ** \param data_size Size of the data range.
  ** \return Real
  */
Real unbiased_var(const std::size_t data_size) const
  {
    Real mean = this->mean(data_size);
     typedef typename slip::Array<CmpT>::iterator iterator_histo;
     typedef typename slip::Array<T>::iterator iterator_steps;
     Real var = Real(0.0);
     iterator_histo it_histo = this->histo_->begin();
     iterator_steps it_steps = this->steps_->begin();
     for(;it_histo != this->histo_->end(); ++it_histo,++it_steps)
       {
	 Real tmp = *it_steps - mean;
	 var += *it_histo * (tmp * tmp);
       }
     return var/static_cast<Real>(data_size-1);
  }
  
 /*!
  ** \brief Computes the unbiased standard deviation from the histogram.
  ** \param data_size Size of the data range.
  ** \return Real
  */
Real unbiased_std(const std::size_t& data_size) const
{
  return std::sqrt(this->unbiased_var(data_size));
}

  Real entropy(const std::size_t& data_size) const
  {
    typedef typename slip::Array<CmpT>::iterator iterator_histo;
    Real ent = Real();

    const Real N = static_cast<Real>(data_size);
    const Real log2 = std::log(2);
    iterator_histo it_histo = this->histo_->begin();
    for(;it_histo != this->histo_->end(); ++it_histo)
       {
	 Real p = *it_histo/N;
	 if(p != Real())
	   {
	     ent += p * (std::log(p)/log2);
	   }
       }
     return -ent;
  }

  Real energy(const std::size_t& data_size) const
  {
    typedef typename slip::Array<CmpT>::iterator iterator_histo;
    Real E = Real();
    const Real N = static_cast<Real>(data_size);
    iterator_histo it_histo = this->histo_->begin();
    for(;it_histo != this->histo_->end(); ++it_histo)
       {
	 Real p = *it_histo/N;
	 E += p * p;
       }
     return E;
  }

 self& operator+=(const self& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(this->histo_->begin(),this->histo_->end(),
		   (*(rhs.histo_)).begin(),
		   this->histo_->begin(),
		   std::plus<CmpT>());
    if(this->cumul_histo_ != 0)
      {
	this->cumulate();
      }
    return *this;
  }

  self& operator-=(const self& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(this->histo_->begin(),this->histo_->end(),
		   (*(rhs.histo_)).begin(),
		   this->histo_->begin(),
		   std::minus<T>());
    if(this->cumul_histo_ != 0)
      {
	this->cumulate();
      }
    return *this;
  }

private :  
  bool uniform_;
  T val_min_;
  T val_max_;
  T step_;
  std::size_t bins_number_;
  slip::Array<T>* steps_;
  slip::Array<CmpT>* histo_;
  slip::Array<CmpT>* cumul_histo_;

private:
  void desallocate()
  {
    if(this->steps_ != 0)
      {
	delete this->steps_;
      }
    if(this->histo_ != 0)
      {
	delete this->histo_;
      }
    if(this->cumul_histo_ != 0)
      {
	delete this->cumul_histo_;
      }
  }

  void allocate()
  {	
    this->steps_ = 0;
    this->histo_ = 0;
    this->cumul_histo_ = 0;
	
    if(this->bins_number_ != 0)
      {
	

	this->histo_ = new slip::Array<CmpT>(this->bins_number_);
      }
    if((this->bins_number_+1) != 0)
      {
    	this->steps_ = new slip::Array<T>(this->bins_number_+1);
      }
  }
  void copy_attributes(const self& rhs)
  {
    this->uniform_ = rhs.uniform_;
    this->val_min_ = rhs.val_min_;
    this->val_max_ = rhs.val_max_;
    this->step_ = rhs.step_;
    this->bins_number_ = rhs.bins_number_;
  }
};



template<typename T, typename Real>
slip::RealHistogram<T,Real> operator+(const slip::RealHistogram<T,Real>& H1, 
				  const slip::RealHistogram<T,Real>& H2)
{
    assert(H1.size() == H2.size());
    slip::RealHistogram<T,Real> tmp(H1);
    tmp+=H2;
    return tmp;
}

template<typename T, typename Real>
slip::RealHistogram<T,Real> operator-(const slip::RealHistogram<T,Real>& H1, 
				  const slip::RealHistogram<T,Real>& H2)
{
    assert(H1.size() == H2.size());
    slip::RealHistogram<T,Real> tmp(H1);
    tmp-=H2;
    return tmp;
}


template<typename T, typename Real>
class Histogram;

/*! \class Histogram
** 
** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
** \version 0.0.1
** \date 2012/04/12
** \since 1.5.0
** \brief DataStructure to handle  discrete histogram.
*/
template <typename T, typename Real = double>
class Histogram
{
public:
  typedef Histogram<T,Real> self;
  typedef long CmpT;
  
  typedef slip::Array<CmpT>::value_type value_type;
  typedef slip::Array<CmpT>::pointer pointer;
  typedef slip::Array<CmpT>::const_pointer const_pointer;
  typedef slip::Array<CmpT>::reference reference;
  typedef slip::Array<CmpT>::const_reference const_reference;

  typedef slip::Array<CmpT>::difference_type difference_type;
  typedef slip::Array<CmpT>::size_type size_type;

  typedef slip::Array<CmpT>::iterator iterator;
  typedef slip::Array<CmpT>::const_iterator const_iterator;

  //typedef double Real;


  static const std::size_t DIM = 1;

  /**
   ** \name Constructors & Destructors
   */
  /*@{*/
  /*!
   ** \brief Default constructor. 
   ** 
   */
  Histogram():
    val_min_(static_cast<T>(0)),
    val_max_(static_cast<T>(255)),
    bins_number_(0),
    histo_(0),
    cumul_histo_(0)
   {
   }
   
  Histogram(const T& val_min,
	    const T& val_max):
    val_min_(val_min),
    val_max_(val_max),
    bins_number_(static_cast<CmpT>((val_max-val_min)+1)),
    histo_(new slip::Array<CmpT>(bins_number_)),
    cumul_histo_(0)
   {
    
   }
  
  Histogram(const self& rhs):
    val_min_(rhs.val_min_),
    val_max_(rhs.val_max_),
    bins_number_(rhs.bins_number_)
  {
    this->allocate();
    //copy
    if(rhs.bins_number_ != 0)
      {
	std::copy(rhs.histo_->begin(),rhs.histo_->end(),this->histo_->begin());
      }
    //allocate cumul_histo_ and copy
    if(rhs.cumul_histo_ != 0)
      {
	this->cumul_histo_ = new slip::Array<CmpT>(*(rhs.cumul_histo_));
      }
    else
      {
	this->cumul_histo_ = 0;
      }
  }
  /*!
   ** \brief Destructor.
   */
  ~Histogram()  
  {
    this->desallocate();
  }

/*@} End Constructors */

/*! 
  ** \brief Assign a %Histogram
  ** \param rhs A %Histogram to get the value from.
  ** \return %Histogram.
  */
  self& operator=(const self& rhs)
  {
    if(this != &rhs)
      {
	if(this->bins_number_ != rhs.bins_number_)
	  {
	    this->desallocate();
	    this->copy_attributes(rhs);
	    this->allocate();
	  	    
	    if(rhs.cumul_histo_ != 0)
	      {
	    	this->cumul_histo_ = new slip::Array<CmpT>(*(rhs.cumul_histo_));
	      }
	    else
	      {
		this->cumul_histo_ = 0;
	      }
	  }
	else
	   {
	     this->copy_attributes(rhs);
	      if(rhs.cumul_histo_ != 0)
	       {
	 	this->cumul_histo_ = new slip::Array<CmpT>(*(rhs.cumul_histo_));
	       }
	      else
		{
		  this->cumul_histo_ = 0;
		}
	  }

	//copy
	if(rhs.bins_number_ != 0)
	  {
	    std::copy(rhs.histo_->begin(),rhs.histo_->end(),this->histo_->begin());
	   }

      }

    return *this;
  }

  const_iterator begin() const
  {
    return histo_->begin();
  }

  iterator begin()
  {
    return histo_->begin();
  }

  const_iterator end() const
  {
    return histo_->end();
  }

  iterator end()
  {
    return histo_->end();
  }
  
  const_iterator cumul_begin() const
  {
    return cumul_histo_->begin();
  }

  iterator cumul_begin()
  {
    return cumul_histo_->begin();
  }

  const_iterator cumul_end() const
  {
    return cumul_histo_->end();
  }

  iterator cumul_end()
  {
    return cumul_histo_->end();
  }
  

  /*!
   ** \brief Print histogram attributes.
   */
  void print() const
  {
    std::cout<<"val_min = "<<val_min_<<std::endl;
    std::cout<<"val_max = "<<val_max_<<std::endl;
    std::cout<<"bins_number = "<<bins_number_<<std::endl;
    if(this->histo_!=0)
      {
	std::cout<<"histo = \n"<<*histo_<<std::endl;
      }
    if(this->cumul_histo_!=0)
      {
	std::cout<<"cumul_histo = \n"<<*cumul_histo_<<std::endl;
      }
  }

   /*!
   ** \brief Returns name of the class.
   ** \return std::string
   */
  std::string name() const
  {
    return std::string("Histogram");
  }

   /*!
   ** \brief Returns the bin of the value \a value.
   ** \param value Value to find.
   ** \return bin of the value.
   */
  inline
  T find_bin(const T& value) const
  {
    return static_cast<CmpT>(value - this->val_min_);
  }


  /*!
  ** \brief Returns the size of the histogram.
  ** \return std::size_t
  */
  std::size_t size() const
  {
    return this->bins_number_;
  }

   /*!
  ** \brief Init histograms.
  */
  void init()
  {
    this->histo_->fill(static_cast<CmpT>(0));
    if(this->cumul_histo_ != 0)
      {
	 this->cumul_histo_->fill(static_cast<CmpT>(0));
      }
  }

  /*!
  ** \brief Add \a value to the histogram.
  ** \param value Value to add.
  */
  inline
  void add(const T& value)
  {
    (*histo_)[find_bin(value)]++;
  }

  /*!
  ** \brief Remove \a value to the histogram.
  ** \param value Value to remove.
  */
  inline
  void remove(const T& value)
  {
    (*histo_)[find_bin(value)]--;
  }

   /*!
  ** \brief Computes the histogram of the range [first,last).
  ** \param first RandomAccessIterator the data range.
  ** \param last RandomAccessIterator the data range.
  */
  template <typename RandomAccessIterator>
  void compute(RandomAccessIterator first,
	       RandomAccessIterator last)
  {
    for(; first!=last; ++first)
      {
	this->add(*first);
      }
  }
   
  /*!
  ** \brief Computes the cumulative histogram from the histogram.
  */
   void cumulate()
  {
    if(this->cumul_histo_ == 0)
      {
	this->cumul_histo_ = new slip::Array<CmpT>(this->bins_number_);
      }
    std::partial_sum(this->histo_->begin(),this->histo_->end(),
		     this->cumul_histo_->begin());
  }

   /*!
  ** \brief Update the histogram adding the range [first,last).
  ** \param first RandomAccessIterator the data range.
  ** \param last RandomAccessIterator the data range.
  */
  template <typename RandomAccessIterator>
  void add(RandomAccessIterator first,
	   RandomAccessIterator last)
  {
    for(; first!=last; ++first)
      {
	this->add(*first);
      }
  }
   

   /*!
  ** \brief Update the histogram removing the range [first,last).
  ** \param first RandomAccessIterator the data range.
  ** \param last RandomAccessIterator the data range.
  */
  template <typename RandomAccessIterator>
  void remove(RandomAccessIterator first,
	   RandomAccessIterator last)
  {
    for(; first!=last; ++first)
      {
	this->remove(*first);
      }
  }


  /*!
  ** \brief Computes the number of values of the histogram.
  ** \return integer
  */
  CmpT values() const
  {
    return (this->bins_number_ - std::count(this->histo_->begin(),
					    this->histo_->end(),CmpT(0)));
  }

 /*!
  ** \brief Computes the median from the histogram.
  ** \param data_size Size of the data range.
  ** \return Real
  */
  inline
  Real median(const std::size_t data_size) const
  {
   
    typedef typename slip::Array<CmpT>::iterator iterator;
    Real __init = Real((*histo_)[0]);
    iterator __first = this->histo_->begin()+1;
    iterator __last  = this->histo_->end();
    Real half_size = static_cast<Real>(data_size)*static_cast<Real>(0.5);

    T i = 0;
    for (; (__init < half_size) && (__first != __last); ++__first)
      {
	__init = __init + *__first;
	i++;
      }
  
    Real x1 = static_cast<Real>(i +this->val_min_);
    
 
  Real med = Real();
  if(data_size%2 == 0)
    {
      Real x2 = static_cast<Real>((i+1)+this->val_min_);
      med = (x1+x2)/static_cast<Real>(2.0);
    }
  else
    {
      med = x1;
    }

    return med;
  }

  /*!
  ** \brief Computes the min from the histogram.
  ** \return T
  */  
  T min() const
  {
     typedef typename slip::Array<CmpT>::iterator iterator;
     T mini = this->val_min_;
     iterator it_histo = this->histo_->begin();
     while((*it_histo == CmpT(0)) && (it_histo != this->histo_->end()))
       {
  	 ++it_histo;
       }
     if(it_histo !=  this->histo_->end())
       {
  	 mini  = static_cast<T>(it_histo-this->histo_->begin());
       }
    
     return mini + this->val_min_;
     
  }

 
  /*!
  ** \brief Computes the max from the histogram.
  ** \return T
  */  
  T max() const
  {
    typedef typename slip::Array<CmpT>::iterator iterator;
    T maxi =  this->val_max_;
     iterator it_histo = (this->histo_->end() - 1);
     while((*it_histo == CmpT(0)) && (it_histo != this->histo_->begin()))
       {
	 --it_histo;
       }
     if(it_histo !=  this->histo_->begin())
       {
	 maxi = static_cast<T>(it_histo-this->histo_->begin());
       }
    
     return maxi + this->val_min_;
  }

 /*!
  ** \brief Computes the mean from the histogram.
  ** \param data_size Size of the data range.
  ** \return Real
  */
  Real mean(const std::size_t data_size) const
  {
     typedef typename slip::Array<CmpT>::iterator iterator_histo;
     Real mean = Real(0.0);
     iterator_histo it_histo = this->histo_->begin();
     Real i = static_cast<Real>(this->val_min_);
     for(;it_histo != this->histo_->end(); ++it_histo, ++i)
       {
     	 mean += *it_histo* i;
       }
     return mean/static_cast<Real>(data_size);
    
  }

 /*!
  ** \brief Computes the variance from the histogram.
  ** \param data_size Size of the data range.
  ** \return Real
  */
  Real var(const std::size_t data_size) const
  {
    Real mean = this->mean(data_size);
     typedef typename slip::Array<CmpT>::iterator iterator_histo;

     Real var = Real(0.0);
     iterator_histo it_histo = this->histo_->begin();
     Real i = static_cast<Real>(this->val_min_);
     for(;it_histo != this->histo_->end(); ++it_histo,++i)
       {
	 Real tmp = i - mean;
	 var += *it_histo * (tmp * tmp);
       }
     return var/static_cast<Real>(data_size);
  }

 /*!
  ** \brief Computes the standard deviation from the histogram.
  ** \param data_size Size of the data range.
  ** \return Real
  */
Real std(const std::size_t data_size) const
{
  return std::sqrt(this->var(data_size));
}

 /*!
  ** \brief Computes the unbiased variance from the histogram.
  ** \param data_size Size of the data range.
  ** \return Real
  */
Real unbiased_var(const std::size_t data_size) const
  {
     Real mean = this->mean(data_size);
     typedef typename slip::Array<CmpT>::iterator iterator_histo;

     Real var = Real(0.0);
     iterator_histo it_histo = this->histo_->begin();
     Real i = static_cast<Real>(this->val_min_);
     for(;it_histo != this->histo_->end(); ++it_histo,++i)
       {
	 Real tmp = i - mean;
	 var += *it_histo * (tmp * tmp);
       }
        return var/static_cast<Real>(data_size-1);
  }
  
 /*!
  ** \brief Computes the unbiased standard deviation from the histogram.
  ** \param data_size Size of the data range.
  ** \return Real
  */
Real unbiased_std(const std::size_t data_size) const
{
  return std::sqrt(this->unbiased_var(data_size));
}

 Real entropy(const std::size_t& data_size) const
  {
    typedef typename slip::Array<CmpT>::iterator iterator_histo;
    Real ent = Real();

    const Real N = static_cast<Real>(data_size);
    const Real log2 = std::log(2);
    iterator_histo it_histo = this->histo_->begin();
    for(;it_histo != this->histo_->end(); ++it_histo)
       {
	 Real p = *it_histo/N;
	 if(p != Real())
	   {
	     ent += p * (std::log(p)/log2);
	   }
       }
     return -ent;
  }

 self& operator+=(const self& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(this->histo_->begin(),this->histo_->end(),
		   (*(rhs.histo_)).begin(),
		   this->histo_->begin(),
		   std::plus<CmpT>());
    if(this->cumul_histo_ != 0)
      {
	this->cumulate();
      }
    return *this;
  }

  self& operator-=(const self& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(this->histo_->begin(),this->histo_->end(),
		   (*(rhs.histo_)).begin(),
		   this->histo_->begin(),
		   std::minus<T>());
    if(this->cumul_histo_ != 0)
      {
	this->cumulate();
      }
    return *this;
  }


private :  
  T val_min_;
  T val_max_;
  std::size_t bins_number_;
  slip::Array<CmpT>* histo_;
  slip::Array<CmpT>* cumul_histo_;
 

private:
  void desallocate()
  {
    if(this->histo_ != 0)
      {
	delete this->histo_;
      }
    if(this->cumul_histo_ != 0)
      {
	delete this->cumul_histo_;
      }
  }

  void allocate()
  {	
    this->histo_ = 0;
    this->cumul_histo_ = 0;
	
    if(this->bins_number_ != 0)
      {
	

	this->histo_ = new slip::Array<CmpT>(this->bins_number_);
      }
  }
  void copy_attributes(const self& rhs)
  {
    this->val_min_ = rhs.val_min_;
    this->val_max_ = rhs.val_max_;
    this->bins_number_ = rhs.bins_number_;
  }

};



template<typename T, typename Real>
slip::Histogram<T,Real> operator+(const slip::Histogram<T,Real>& H1, 
				  const slip::Histogram<T,Real>& H2)
{
    assert(H1.size() == H2.size());
    slip::Histogram<T,Real> tmp(H1);
    tmp+=H2;
    return tmp;
}

template<typename T, typename Real>
slip::Histogram<T,Real> operator-(const slip::Histogram<T,Real>& H1, 
				  const slip::Histogram<T,Real>& H2)
{
    assert(H1.size() == H2.size());
    slip::Histogram<T,Real> tmp(H1);
    tmp-=H2;
    return tmp;
}



template<typename Histo>
inline
double chi2_histo(const Histo& H1, 
		  const Histo& H2)
{
  assert(H1.size() == H2.size());
  typename Histo::const_iterator it_h1 = H1.begin();
  typename Histo::const_iterator it_h2 = H2.begin();
  double val = 0.0;
  for(; it_h1 != H1.end(); ++it_h1, ++it_h2)
    {
      if(*it_h1 != *it_h2)
	{
	  double h1 = static_cast<double>(*it_h1);
	  double h2 = static_cast<double>(*it_h2);
	  double var = (h1 - h2);
	  double s   = (h1 + h2);
	  val += (var*var) / s;
	}
    }
  return val;
}
 
template<typename Histo>
inline
double inter_histo(const Histo& H1, 
		  const Histo& H2)
{
  assert(H1.size() == H2.size());
  typename Histo::const_iterator it_h1 = H1.begin();
  typename Histo::const_iterator it_h2 = H2.begin();
  double val = 0.0;
  for(; it_h1 != H1.end(); ++it_h1, ++it_h2)
    {
      
	  double h1 = static_cast<double>(*it_h1);
	  double h2 = static_cast<double>(*it_h2);
	  val += std::min(h1,h2);

    }
  return val;
}

  
}//slip::


#endif //SLIP_HISTO_HPP
