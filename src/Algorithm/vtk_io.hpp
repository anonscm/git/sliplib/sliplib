/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file vtk_io.hpp
 * 
 * \brief Provides some Visualization ToolKit (VTK) data format writing algorithms.
 * 
 */
#ifndef SLIP_VTK_IO_HPP
#define SLIP_VTK_IO_HPP

#include <ios>
#include <stdexcept>
#include <string>
#include <iostream>
#include <typeinfo> 
#include "DenseVector2dField2d.hpp"
#include "DenseVector3dField3d.hpp"
#include "Point3d.hpp"
#include "RegularVector3dField3d.hpp"
#include "Matrix.hpp"
#include "Volume.hpp"

namespace slip
{

  bool is_big_endian()
  {
    const int32_t test = 1;
    return ((*(char*)&test) == 0);
  }

  bool is_little_endian()
  {
    return (!is_big_endian());
  }


  template <typename T>
  T swap_endian(const T& u)
    {
        union
        {
            T u;
            unsigned char u8[sizeof(T)];
        } source, dest;
       

        for (size_t k = 0; k < sizeof(T); ++k)
	  {
            dest.u8[k] = source.u8[sizeof(T) - k -1];
	  }

       return dest.u;
    }

  template <typename RandomAccessIterator1,
	    typename RandomAccessIterator2>
  void swap_endian(RandomAccessIterator1 first,
		   RandomAccessIterator1 last,
		   RandomAccessIterator2 swap_first,
		   RandomAccessIterator2 swap_last)
  {
    for(; first != last; ++first, ++swap_first)
      {
	*swap_first = slip::swap_endian(*first);
      }
  }


}//::slip

//are same
namespace slip
{
 template<typename, typename>
    struct __are_same
    {
      enum { __value = 0 };
      typedef __false_type __type;
    };

  template<typename _Tp>
    struct __are_same<_Tp, _Tp>
    {
      enum { __value = 1 };
      typedef __true_type __type;
    };
}//::slip

namespace slip
{

template <typename Container3d>
inline
void write_vtk_3d(const Container3d& field,
		  const std::string& file_path_name)
{
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename Container3d::value_type value_type;
	const int slices = static_cast<int>(field.slices()); //k direction
	const int rows = static_cast<int>(field.rows()); //i direction
	const int cols = static_cast<int>(field.cols()); //j direction
	//write file informations
	output<<"# vtk DataFile Version 3.0"<<std::endl;
	output<<"Volume of scalars"<<std::endl;
	output<<"ASCII"<<std::endl;
	output<<"DATASET STRUCTURED_POINTS"<<std::endl;
	output<<"DIMENSIONS "<<cols<<" "<<rows<<" "<<slices<<std::endl;
	output<<"ORIGIN "<<0<<" "<<0<<" "<<0<<std::endl;
	output<<"SPACING "<<1<<" "<<1<<" "<<1<<std::endl;
	output<<"POINT_DATA "<<slices*rows*cols<<std::endl;
	if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"SCALARS volume_scalars double 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"SCALARS volume_scalars float 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"SCALARS volume_scalars int 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"SCALARS volume_scalars unsigned_int 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"SCALARS volume_scalars char 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"SCALARS volume_scalars unsigned_char 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"SCALARS volume_scalars long 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"SCALARS volume_scalars unsigned_long 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"SCALARS volume_scalars short 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"SCALARS volume_scalars unsigned_short 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"SCALARS volume_scalars bit 1"<<std::endl;
	  }
	else
	  {
	    output<<"SCALARS volume_scalars float 1"<<std::endl;
	  }
	output<<"LOOKUP_TABLE default"<<std::endl;
	
	if((slip::__are_same<value_type,char>::__value) ||
	   (slip::__are_same<value_type,unsigned char>::__value))
	  {
	    //write data
	    for(int k=slices-1; k>=0; --k)
	      {
		for(int i = rows-1; i >=0; --i)
		  {
		    for(int j = 0; j < cols ; ++j)
		      {
			output<<static_cast<int>(field[k][i][j])<<" ";
		      }
		  }
		output<<std::endl;
	      }          
	  }
	else
	  {
	    for(int k=slices-1; k>=0; --k)
	      {
		for(int i = rows-1; i >=0; --i)
		  {
		    for(int j = 0; j < cols ; ++j)
		      {
			output<<field[k][i][j]<<" ";
		      }
		  }
		output<<std::endl;
	      }          
	  }
	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }




template <typename Container3d>
inline
void write_vtk_3d_binary(const Container3d& field,
			 const std::string& file_path_name)
{
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename Container3d::value_type value_type;

	int slices = static_cast<int>(field.slices()); //k direction
	int rows = static_cast<int>(field.rows()); //i direction
	int cols = static_cast<int>(field.cols()); //j direction
	//write file informations
	output<<"# vtk DataFile Version 3.0"<<std::endl;
	output<<"Volume of scalars"<<std::endl;
	output<<"BINARY"<<std::endl;
	output<<"DATASET STRUCTURED_POINTS"<<std::endl;
	output<<"DIMENSIONS "<<cols<<" "<<rows<<" "<<slices<<std::endl;
	output<<"ORIGIN "<<0<<" "<<0<<" "<<0<<std::endl;
	output<<"SPACING "<<1<<" "<<1<<" "<<1<<std::endl;
	output<<"POINT_DATA "<<field.size()<<std::endl;
	if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"SCALARS volume_scalars double 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"SCALARS volume_scalars float 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"SCALARS volume_scalars int 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"SCALARS volume_scalars unsigned_int 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"SCALARS volume_scalars char 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"SCALARS volume_scalars unsigned_char 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"SCALARS volume_scalars long 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"SCALARS volume_scalars unsigned_long 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"SCALARS volume_scalars short 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"SCALARS volume_scalars unsigned_short 1"<<std::endl;
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"SCALARS volume_scalars bit 1"<<std::endl;
	  }
	else
	  {
	    output<<"SCALARS volume_scalars float 1"<<std::endl;
	  }
	output<<"LOOKUP_TABLE default"<<std::endl;
	//if not big endian swap to big endian
	if(slip::is_little_endian() 
	   && 
	   (!slip::__are_same<value_type,unsigned char>::__value)
	  &&
	   (!slip::__are_same<value_type,char>::__value))
	  {
	    //write data
	    for(int k = (slices - 1); k >=0; --k)
	      {
		for(int i = (rows - 1); i >= 0; --i)
		  {
		    for(int j = 0; j < cols; ++j)
		      {
			value_type tmp = slip::swap_endian(field[k][i][j]);
			output.write(reinterpret_cast<const char*>(&tmp),
				     sizeof(value_type));
		      }
		  }
	      }
	  }
	else
	  {
	    for(int k = (slices - 1); k >=0; --k)
	      {
		for(int i = (rows - 1); i >= 0; --i)
		  {
		    for(int j = 0; j < cols; ++j)
		      {
			output.write(reinterpret_cast<const char*>(&field[k][i][j]),
				     sizeof(value_type));
		      }
		  }
	      }
	  }
	output.close();
      }          

     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }




//Writing vector field into vtk format
template <typename MultiContainer3d>
  inline
  void write_vtk_vect3d(const MultiContainer3d& field,
			const std::string& file_path_name)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename MultiContainer3d::block_value_type value_type;
	int slices = static_cast<int>(field.slices()); //k direction
	int rows = static_cast<int>(field.rows()); //i direction
	int cols = static_cast<int>(field.cols()); //j direction
	//write file informations
	output<<"# vtk DataFile Version 3.0"<<std::endl;
	output<<"Volume of vectors"<<std::endl;
	output<<"ASCII"<<std::endl;
	output<<"DATASET STRUCTURED_POINTS"<<std::endl;
	output<<"DIMENSIONS "<<cols<<" "<<rows<<" "<<slices<<std::endl;
	output<<"SPACING "<<1<<" "<<1<<" "<<1<<std::endl;
	output<<"ORIGIN "<<0<<" "<<0<<" "<<0<<std::endl;
	output<<"POINT_DATA "<<slices*rows*cols<<std::endl;
	//output<<"VECTORS volume_vectors double"<<std::endl;	
	if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"VECTORS volume_vectors double"<<std::endl;
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"VECTORS volume_vectors float"<<std::endl;
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"VECTORS volume_vectors int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"VECTORS volume_vectors unsigned_int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"VECTORS volume_vectors char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"VECTORS volume_vectors unsigned_char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"VECTORS volume_vectors long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"VECTORS volume_vectors unsigned_long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"VECTORS volume_vectors short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"VECTORS volume_vectors unsigned_short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"VECTORS volume_vectors bit"<<std::endl;
	  }
	else
	  {
	    output<<"VECTORS volume_vectors float"<<std::endl;
	  }

	//write data
	for(int k=slices-1; k>=0; --k)
	  {
	    for(int i = rows-1; i >=0; --i)
	      {
		for(int j = 0; j < cols ; ++j)
		  {
		    output<<field[k][i][j][0]<<" "<<field[k][i][j][1]<<" "<<field[k][i][j][2]<<"   ";
		  }
	      }
	    output<<std::endl;
	  }          

	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }


template <typename MultiContainer3d>
  inline
  void write_vtk_vect3d_binary(const MultiContainer3d& field,
			       const std::string& file_path_name)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename MultiContainer3d::block_value_type value_type;
	int slices = static_cast<int>(field.slices()); //k direction
	int rows = static_cast<int>(field.rows()); //i direction
	int cols = static_cast<int>(field.cols()); //j direction
	//write file informations
	output<<"# vtk DataFile Version 3.0"<<std::endl;
	output<<"Volume of vectors"<<std::endl;
	output<<"BINARY"<<std::endl;
	output<<"DATASET STRUCTURED_POINTS"<<std::endl;
	output<<"DIMENSIONS "<<cols<<" "<<rows<<" "<<slices<<std::endl;
	output<<"SPACING "<<1<<" "<<1<<" "<<1<<std::endl;
	output<<"ORIGIN "<<0<<" "<<0<<" "<<0<<std::endl;
	output<<"POINT_DATA "<<slices*rows*cols<<std::endl;
	//output<<"VECTORS volume_vectors double"<<std::endl;	
	if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"VECTORS volume_vectors double"<<std::endl;
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"VECTORS volume_vectors float"<<std::endl;
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"VECTORS volume_vectors int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"VECTORS volume_vectors unsigned_int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"VECTORS volume_vectors char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"VECTORS volume_vectors unsigned_char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"VECTORS volume_vectors long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"VECTORS volume_vectors unsigned_long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"VECTORS volume_vectors short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"VECTORS volume_vectors unsigned_short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"VECTORS volume_vectors bit"<<std::endl;
	  }
	else
	  {
	    output<<"VECTORS volume_vectors float"<<std::endl;
	  }
	if(slip::is_little_endian() 
	   && 
	   (!slip::__are_same<value_type,unsigned char>::__value)
	   &&
	   (!slip::__are_same<value_type,char>::__value))
	  {
	    for(int k=slices-1; k>=0; --k)
	      {
		for(int i = (rows - 1); i >= 0; --i)
		  {
		    for(int j = 0; j < cols; ++j)
		      {
			value_type tmp = slip::swap_endian(field[k][i][j][0]);
			output.write(reinterpret_cast<const char*>(&tmp),
				     sizeof(value_type));
			tmp = slip::swap_endian(field[k][i][j][1]);
			output.write(reinterpret_cast<const char*>(&tmp),
				 sizeof(value_type));
			tmp = slip::swap_endian(field[k][i][j][2]);
			output.write(reinterpret_cast<const char*>(&tmp),
				     sizeof(value_type));
		      }
		  }
	      }
	  }
	else
	  {
	    for(int k=slices-1; k>=0; --k)
	      {
		for(int i = (rows - 1); i >= 0; --i)
		  {
		    for(int j = 0; j < cols; ++j)
		      {
			output.write(reinterpret_cast<const char*>(&field[k][i][j][0]),
				     sizeof(value_type));
			output.write(reinterpret_cast<const char*>(&field[k][i][j][1]),
				     sizeof(value_type));
			output.write(reinterpret_cast<const char*>(&field[k][i][j][2]),
				     sizeof(value_type));
		      }
		  }
	      }
	  }


	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }


template <typename MultiContainer3d>
  inline
void write_vtk_vect3d_grid(const MultiContainer3d& field,
			   const std::string& file_path_name)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename MultiContainer3d::block_value_type value_type;

	int slices = static_cast<int>(field.slices()); //k direction
	int rows = static_cast<int>(field.rows()); //i direction
	int cols = static_cast<int>(field.cols()); //j direction
	slip::Point3d<value_type> init_point = field.get_init_point();
	slip::Point3d<value_type> step = field.get_grid_step();
	//write file informations
	output<<"# vtk DataFile Version 3.0"<<std::endl;
	output<<"Image of vectors"<<std::endl;
	output<<"ASCII"<<std::endl;
	output<<"DATASET STRUCTURED_POINTS"<<std::endl;
	output<<"DIMENSIONS "<<cols<<" "<<rows<<" "<<slices<<std::endl;
	output<<"SPACING "<<step[0]<<" "<<step[1]<<" "<<step[2]<<std::endl;
	output<<"ORIGIN "<<init_point[0]<<" "<<init_point[1]<<" "<<init_point[2]<<std::endl;
	output<<"POINT_DATA "<<slices*rows*cols<<std::endl;

	if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"VECTORS image_vectors double"<<std::endl;
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"VECTORS image_vectors float"<<std::endl;
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"VECTORS image_vectors int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"VECTORS image_vectors char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"VECTORS image_vectors long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"VECTORS image_vectors short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"VECTORS image_vectors bit"<<std::endl;
	  }
	else
	  {
	    output<<"VECTORS image_vectors float"<<std::endl;
	  }

	//write data
	for(int k = slices-1; k >=0; --k)
	  {
	    for(int i = rows-1; i >=0; --i)
	      {
		for(int j = 0; j < cols ; ++j)
		  {
		    output<<field[k][i][j][0]<<" "<<field[k][i][j][1]<<" "<<field[k][i][j][2]<<" "<<std::endl;
		  }
	      }
	  }
	output<<std::endl;

	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }


template <typename MultiContainer3d>
  inline
void write_vtk_vect3d_grid_binary(const MultiContainer3d& field,
				  const std::string& file_path_name)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename MultiContainer3d::block_value_type value_type;

	int slices = static_cast<int>(field.slices()); //k direction
	int rows = static_cast<int>(field.rows()); //i direction
	int cols = static_cast<int>(field.cols()); //j direction
	slip::Point3d<value_type> init_point = field.get_init_point();
	slip::Point3d<value_type> step = field.get_grid_step();
	//write file informations
	output<<"# vtk DataFile Version 3.0"<<std::endl;
	output<<"Image of vectors"<<std::endl;
	output<<"BINARY"<<std::endl;
	output<<"DATASET STRUCTURED_POINTS"<<std::endl;
	output<<"DIMENSIONS "<<cols<<" "<<rows<<" "<<slices<<std::endl;
	output<<"SPACING "<<step[0]<<" "<<step[1]<<" "<<step[2]<<std::endl;
	output<<"ORIGIN "<<init_point[0]<<" "<<init_point[1]<<" "<<init_point[2]<<std::endl;
	output<<"POINT_DATA "<<slices*rows*cols<<std::endl;

	if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"VECTORS image_vectors double"<<std::endl;
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"VECTORS image_vectors float"<<std::endl;
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"VECTORS image_vectors int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"VECTORS image_vectors char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"VECTORS image_vectors long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"VECTORS image_vectors short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"VECTORS image_vectors bit"<<std::endl;
	  }
	else
	  {
	    output<<"VECTORS image_vectors float"<<std::endl;
	  }
	if(slip::is_little_endian() 
	   && 
	   (!slip::__are_same<value_type,unsigned char>::__value)
	   &&
	   (!slip::__are_same<value_type,char>::__value))
	  {
	    for(int k=slices-1; k>=0; --k)
	      {
		for(int i = (rows - 1); i >= 0; --i)
		  {
		    for(int j = 0; j < cols; ++j)
		      {
			value_type tmp = slip::swap_endian(field[k][i][j][0]);
			output.write(reinterpret_cast<const char*>(&tmp),
				     sizeof(value_type));
			tmp = slip::swap_endian(field[k][i][j][1]);
			output.write(reinterpret_cast<const char*>(&tmp),
				 sizeof(value_type));
			tmp = slip::swap_endian(field[k][i][j][2]);
			output.write(reinterpret_cast<const char*>(&tmp),
				     sizeof(value_type));
		      }
		  }
	      }
	  }
	else
	  {
	    for(int k=slices-1; k>=0; --k)
	      {
		for(int i = (rows - 1); i >= 0; --i)
		  {
		    for(int j = 0; j < cols; ++j)
		      {
			output.write(reinterpret_cast<const char*>(&field[k][i][j][0]),
				     sizeof(value_type));
			output.write(reinterpret_cast<const char*>(&field[k][i][j][1]),
				     sizeof(value_type));
			output.write(reinterpret_cast<const char*>(&field[k][i][j][2]),
				     sizeof(value_type));
		      }
		  }
	      }
	  }



	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }

//Writing 2d vector field into vtk format
template <typename MultiContainer2d>
  inline
  void write_vtk_vect2d(const MultiContainer2d& field,
			const std::string& file_path_name)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename MultiContainer2d::block_value_type value_type;
	value_type zero = value_type(0);
	int slices = 1; //k direction
	int rows = static_cast<int>(field.rows()); //i direction
	int cols = static_cast<int>(field.cols()); //j direction
	//write file informations
	output<<"# vtk DataFile Version 3.0"<<std::endl;
	output<<"Image of vectors"<<std::endl;
	output<<"ASCII"<<std::endl;
	output<<"DATASET STRUCTURED_POINTS"<<std::endl;
	output<<"DIMENSIONS "<<cols<<" "<<rows<<" "<<slices<<std::endl;
	output<<"SPACING "<<1<<" "<<1<<" "<<1<<std::endl;
	output<<"ORIGIN "<<0<<" "<<0<<" "<<0<<std::endl;
	output<<"POINT_DATA "<<rows*cols<<std::endl;

	if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"VECTORS image_vectors double"<<std::endl;
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"VECTORS image_vectors float"<<std::endl;
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"VECTORS image_vectors int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"VECTORS image_vectors char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"VECTORS image_vectors long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"VECTORS image_vectors short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"VECTORS image_vectors bit"<<std::endl;
	  }
	else
	  {
	    output<<"VECTORS image_vectors float"<<std::endl;
	  }

	//write data
	for(int i = rows-1; i >=0; --i)
	  {
	    for(int j = 0; j < cols ; ++j)
	      {
		output<<field[i][j][0]<<" "<<field[i][j][1]<<" "<<zero<<" "<<std::endl;
	      }
	  }
	output<<std::endl;

	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }

template <typename MultiContainer2d>
  inline
  void write_vtk_vect2d_binary(const MultiContainer2d& field,
			       const std::string& file_path_name)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename MultiContainer2d::block_value_type value_type;
	value_type zero = value_type(0);
	int slices = 1; //k direction
	int rows = static_cast<int>(field.rows()); //i direction
	int cols = static_cast<int>(field.cols()); //j direction
	//write file informations
	output<<"# vtk DataFile Version 3.0"<<std::endl;
	output<<"Image of vectors"<<std::endl;
	output<<"BINARY"<<std::endl;
	output<<"DATASET STRUCTURED_POINTS"<<std::endl;
	output<<"DIMENSIONS "<<cols<<" "<<rows<<" "<<slices<<std::endl;
	output<<"SPACING "<<1<<" "<<1<<" "<<1<<std::endl;
	output<<"ORIGIN "<<0<<" "<<0<<" "<<0<<std::endl;
	output<<"POINT_DATA "<<rows*cols<<std::endl;
	//output<<"VECTORS image_vectors double"<<std::endl;	
	if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"VECTORS image_vectors double"<<std::endl;
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"VECTORS image_vectors float"<<std::endl;
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"VECTORS image_vectors int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"VECTORS image_vectors char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"VECTORS image_vectors long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"VECTORS image_vectors short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"VECTORS image_vectors bit"<<std::endl;
	  }
	else
	  {
	    output<<"VECTORS image_vectors float"<<std::endl;
	  }
	if(slip::is_little_endian() 
	   && 
	   (!slip::__are_same<value_type,unsigned char>::__value)
	   &&
	   (!slip::__are_same<value_type,char>::__value))
	  {
	    //write data
	   
	    for(int i = (rows - 1); i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    value_type tmp = slip::swap_endian(field[i][j][0]);
		    output.write(reinterpret_cast<const char*>(&tmp),
				 sizeof(value_type));
		    tmp = slip::swap_endian(field[i][j][1]);
		    output.write(reinterpret_cast<const char*>(&tmp),
				 sizeof(value_type));
		    output.write(reinterpret_cast<const char*>(&zero),
				 sizeof(value_type));
		  }
	      }
	     
	  }
	else
	  {
	 
	    for(int i = (rows - 1); i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output.write(reinterpret_cast<const char*>(&field[i][j][0]),
				 sizeof(value_type));
		    output.write(reinterpret_cast<const char*>(&field[i][j][1]),
				 sizeof(value_type));
		    output.write(reinterpret_cast<const char*>(&zero),
				 sizeof(value_type));
		  }
	      }
	      
	  }


	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }

template <typename MultiContainer2d>
  inline
void write_vtk_vect2d_grid(const MultiContainer2d& field,
			   const std::string& file_path_name)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename MultiContainer2d::block_value_type value_type;
	value_type zero = value_type(0);
	int slices = 1; //k direction
	int rows = static_cast<int>(field.rows()); //i direction
	int cols = static_cast<int>(field.cols()); //j direction
	slip::Point2d<value_type> init_point = field.get_init_point();
	slip::Point2d<value_type> step = field.get_grid_step();
	//write file informations
	output<<"# vtk DataFile Version 3.0"<<std::endl;
	output<<"Image of vectors"<<std::endl;
	output<<"ASCII"<<std::endl;
	output<<"DATASET STRUCTURED_POINTS"<<std::endl;
	output<<"DIMENSIONS "<<cols<<" "<<rows<<" "<<slices<<std::endl;
	output<<"SPACING "<<step[0]<<" "<<step[1]<<" "<<1<<std::endl;
	output<<"ORIGIN "<<init_point[0]<<" "<<init_point[1]<<" "<<0<<std::endl;
	output<<"POINT_DATA "<<rows*cols<<std::endl;

	if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"VECTORS image_vectors double"<<std::endl;
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"VECTORS image_vectors float"<<std::endl;
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"VECTORS image_vectors int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"VECTORS image_vectors char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"VECTORS image_vectors long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"VECTORS image_vectors short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"VECTORS image_vectors bit"<<std::endl;
	  }
	else
	  {
	    output<<"VECTORS image_vectors float"<<std::endl;
	  }

	//write data
	for(int i = rows-1; i >=0; --i)
	  {
	    for(int j = 0; j < cols ; ++j)
	      {
		output<<field[i][j][0]<<" "<<field[i][j][1]<<" "<<zero<<" "<<std::endl;
	      }
	  }
	output<<std::endl;

	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }


template <typename MultiContainer2d>
  inline
void write_vtk_vect2d_grid_binary(const MultiContainer2d& field,
				  const std::string& file_path_name)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename MultiContainer2d::block_value_type value_type;
	value_type zero = value_type(0);
	int slices = 1; //k direction
	int rows = static_cast<int>(field.rows()); //i direction
	int cols = static_cast<int>(field.cols()); //j direction
	slip::Point2d<value_type> init_point = field.get_init_point();
	slip::Point2d<value_type> step = field.get_grid_step();
	//write file informations
	output<<"# vtk DataFile Version 3.0"<<std::endl;
	output<<"Image of vectors"<<std::endl;
	output<<"BINARY"<<std::endl;
	output<<"DATASET STRUCTURED_POINTS"<<std::endl;
	output<<"DIMENSIONS "<<cols<<" "<<rows<<" "<<slices<<std::endl;
	output<<"SPACING "<<step[0]<<" "<<step[1]<<" "<<1<<std::endl;
	output<<"ORIGIN "<<init_point[0]<<" "<<init_point[1]<<" "<<0<<std::endl;
	output<<"POINT_DATA "<<rows*cols<<std::endl;

	if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"VECTORS image_vectors double"<<std::endl;
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"VECTORS image_vectors float"<<std::endl;
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"VECTORS image_vectors int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"VECTORS image_vectors char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"VECTORS image_vectors long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"VECTORS image_vectors short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"VECTORS image_vectors unsigned_short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"VECTORS image_vectors bit"<<std::endl;
	  }
	else
	  {
	    output<<"VECTORS image_vectors float"<<std::endl;
	  }

	if(slip::is_little_endian() 
	   && 
	   (!slip::__are_same<value_type,unsigned char>::__value)
	   &&
	   (!slip::__are_same<value_type,char>::__value))
	  {
	   
	   for(int i = (rows - 1); i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    value_type tmp = slip::swap_endian(field[i][j][0]);
		    output.write(reinterpret_cast<const char*>(&tmp),
				 sizeof(value_type));
		    tmp = slip::swap_endian(field[i][j][1]);
		    output.write(reinterpret_cast<const char*>(&tmp),
				 sizeof(value_type));
		    output.write(reinterpret_cast<const char*>(&zero),
				 sizeof(value_type));
		  }
	      }
	     
	  }
	else
	  {
	 
	    for(int i = (rows - 1); i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output.write(reinterpret_cast<const char*>(&field[i][j][0]),
				 sizeof(value_type));
		    output.write(reinterpret_cast<const char*>(&field[i][j][1]),
				 sizeof(value_type));
		    output.write(reinterpret_cast<const char*>(&zero),
				 sizeof(value_type));
		  }
	      }
	      
	  }
	

	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }


//------------------------
// container 2d
//------------------------
template <typename Container2d>
  inline
  void write_vtk_2d(const Container2d& field,
		    const std::string& file_path_name)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename Container2d::value_type value_type;
	int slices = 1; //k direction
	int rows = static_cast<int>(field.rows()); //i direction
	int cols = static_cast<int>(field.cols()); //j direction
	//write file informations
	output<<"# vtk DataFile Version 3.0"<<std::endl;
	output<<"Image"<<std::endl;
	output<<"ASCII"<<std::endl;
	output<<"DATASET STRUCTURED_POINTS"<<std::endl;
	output<<"DIMENSIONS "<<cols<<" "<<rows<<" "<<slices<<std::endl;
	output<<"SPACING "<<1<<" "<<1<<" "<<1<<std::endl;
	output<<"ORIGIN "<<0<<" "<<0<<" "<<0<<std::endl;
	output<<"POINT_DATA "<<rows*cols<<std::endl;
	//output<<"VECTORS image double"<<std::endl;	
	if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"SCALARS image double"<<std::endl;
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"SCALARS image float"<<std::endl;
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"SCALARS image int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"SCALARS image unsigned_int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"SCALARS image char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"SCALARS image unsigned_char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"SCALARS image long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"SCALARS image unsigned_long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"SCALARS image short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"SCALARS image unsigned_short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"SCALARS image bit"<<std::endl;
	  }
	else
	  {
	    output<<"SCALARS image float"<<std::endl;
	  }
	output<<"LOOKUP_TABLE default"<<std::endl;
	//write data
	// for(int k=slices-1; k>=0; --k)
	//   {
	 for(int i = rows-1; i >=0; --i)
	   {
	     for(int j = 0; j < cols ; ++j)
	       {
		 output<<field[i][j]<<" ";
	      }
	   }
	output<<std::endl;
	//}          

	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }


template <typename Container2d>
  inline
  void write_vtk_2d_binary(const Container2d& field,
			   const std::string& file_path_name)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename Container2d::value_type value_type;
	int slices = 1; //k direction
	int rows = static_cast<int>(field.rows()); //i direction
	int cols = static_cast<int>(field.cols()); //j direction
	//write file informations
	output<<"# vtk DataFile Version 3.0"<<std::endl;
	output<<"Image"<<std::endl;
	output<<"BINARY"<<std::endl;
	output<<"DATASET STRUCTURED_POINTS"<<std::endl;
	output<<"DIMENSIONS "<<cols<<" "<<rows<<" "<<slices<<std::endl;
	output<<"SPACING "<<1<<" "<<1<<" "<<1<<std::endl;
	output<<"ORIGIN "<<0<<" "<<0<<" "<<0<<std::endl;
	output<<"POINT_DATA "<<rows*cols<<std::endl;
	if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"SCALARS image double"<<std::endl;
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"SCALARS image float"<<std::endl;
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"SCALARS image int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"SCALARS image unsigned_int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"SCALARS image char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"SCALARS image unsigned_char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"SCALARS image long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"SCALARS image unsigned_long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"SCALARS image short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"SCALARS image unsigned_short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"SCALARS image bit"<<std::endl;
	  }
	else
	  {
	    output<<"SCALARS image float"<<std::endl;
	  }
	output<<"LOOKUP_TABLE default"<<std::endl;
	if(slip::is_little_endian() 
	   && 
	   (!slip::__are_same<value_type,unsigned char>::__value)
	   &&
	   (!slip::__are_same<value_type,char>::__value))
	  {
	    //write data
	   
	    for(int i = (rows - 1); i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    value_type tmp = slip::swap_endian(field[i][j]);
		    output.write(reinterpret_cast<const char*>(&tmp),
				 sizeof(value_type));
		  }
	      }
	     
	  }
	else
	  {
	 
	    for(int i = (rows - 1); i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output.write(reinterpret_cast<const char*>(&field[i][j]),
				 sizeof(value_type));
		  }
	      }
	      
	  }
         
      
	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }


template <typename Container2d>
  inline
  void write_image_surface_vtk(const Container2d& field,
			   const std::string& file_path_name)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename Container2d::value_type value_type;
	//	int slices = 1; //k direction
	int rows = static_cast<int>(field.rows()); //i direction
	int cols = static_cast<int>(field.cols()); //j direction
	//write file informations
	output<<"# vtk DataFile Version 3.0"<<std::endl;
	output<<"Image surface"<<std::endl;
	output<<"ASCII"<<std::endl;
	output<<"DATASET UNSTRUCTURED_GRID"<<std::endl;
	if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"POINTS "<<rows*cols<<" double"<<std::endl;
	    for(int i = rows-1; i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output<<static_cast<double>(j)/static_cast<double>(cols-1)<<" "<<static_cast<double>(rows-1-i)/static_cast<double>(rows-1)<<" "<<field[i][j]<<std::endl;
		  } 
	      }
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"POINTS "<<rows*cols<<" float"<<std::endl;
	    for(int i = rows-1; i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		     output<<static_cast<float>(j)/static_cast<float>(cols-1)<<" "<<static_cast<float>(rows-1-i)/static_cast<float>(rows-1)<<" "<<field[i][j]<<std::endl;
		  } 
	      }
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"POINTS "<<rows*cols<<" int"<<std::endl;
	    for(int i = rows-1; i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output<<j<<" "<<(rows-1-i)<<" "<<field[i][j]<<std::endl;
		  } 
	      }
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"POINTS "<<rows*cols<<" unsigned_int"<<std::endl;
	    for(int i = rows-1; i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output<<j<<" "<<(rows-1-i)<<" "<<field[i][j]<<std::endl;
		  } 
	      }
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"POINTS "<<rows*cols<<" char"<<std::endl;
	    for(int i = rows-1; i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output<<j<<" "<<(rows-1-i)<<" "<<int(field[i][j])<<std::endl;
		  } 
	      }
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"POINTS "<<rows*cols<<" unsigned_char"<<std::endl;
	    for(int i = rows-1; i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output<<j<<" "<<(rows-1-i)<<" "<<int(field[i][j])<<std::endl;
		  } 
	      }
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"POINTS "<<rows*cols<<" long"<<std::endl;
	    for(int i = rows-1; i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output<<j<<" "<<(rows-1-i)<<" "<<field[i][j]<<std::endl;
		  } 
	      }
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"POINTS "<<rows*cols<<" unsigned_long"<<std::endl;
	    for(int i = rows-1; i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output<<j<<" "<<(rows-1-i)<<" "<<field[i][j]<<std::endl;
		  } 
	      }
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"POINTS "<<rows*cols<<" short"<<std::endl;
	    for(int i = rows-1; i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output<<j<<" "<<(rows-1-i)<<" "<<field[i][j]<<std::endl;
		  } 
	      }
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"POINTS "<<rows*cols<<" unsigned_short"<<std::endl;
	    for(int i = rows-1; i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output<<j<<" "<<(rows-1-i)<<" "<<field[i][j]<<std::endl;
		  } 
	      }
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"POINTS "<<rows*cols<<" bit"<<std::endl;
	    for(int i = rows-1; i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output<<j<<" "<<(rows-1-i)<<" "<<field[i][j]<<std::endl;
		  } 
	      }
	  }
	else
	  {
	    output<<"POINTS "<<rows*cols<<" float"<<std::endl;
	    for(int i = rows-1; i >= 0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output<<j<<" "<<(rows-1-i)<<" "<<field[i][j]<<std::endl;
		  } 
	      }
	  }
	
	int nb_cells = (rows-1)*(cols-1)*2;
	output<<"CELLS "<<nb_cells<<" "<<4*nb_cells<<std::endl;
	for(int i = rows-1; i >= 1; --i)
	  {
	    for(int j = 0; j < (cols-1); ++j)
	      {
		output<<3<<" "<<(i*cols+j)<<" "<<((i-1)*cols + (j+1))<<" "<<(i*cols + (j+1))<<std::endl;
		output<<3<<" "<<(i*cols+j)<<" "<<((i-1)*cols + j)<<" "<<((i-1)*cols + (j+1))<<std::endl;
	
	      }
	  }
	output<<"CELL_TYPES "<<nb_cells<<std::endl;
	for(int k = 0; k < nb_cells; ++k)
	  {
	    output<<5<<" ";
	  }
	output<<std::endl;
	output<<"POINT_DATA "<<rows*cols<<std::endl;
		if(slip::__are_same<value_type,double>::__value)
	  {
	    output<<"SCALARS greylevels double"<<std::endl;
	  }
	else if(slip::__are_same<value_type,float>::__value)
	  {
	    output<<"SCALARS greylevels float"<<std::endl;
	  }
	else if(slip::__are_same<value_type,int>::__value)
	  {
	    output<<"SCALARS greylevels int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned int>::__value)
	  {
	    output<<"SCALARS greylevels unsigned_int"<<std::endl;
	  }
	else if(slip::__are_same<value_type,char>::__value)
	  {
	    output<<"SCALARS greylevels char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned char>::__value)
	  {
	    output<<"SCALARS greylevels unsigned_char"<<std::endl;
	  }
	else if(slip::__are_same<value_type,long>::__value)
	  {
	    output<<"SCALARS greylevels long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned long>::__value)
	  {
	    output<<"SCALARS greylevels unsigned_long"<<std::endl;
	  }
	else if(slip::__are_same<value_type,short>::__value)
	  {
	    output<<"SCALARS greylevels short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,unsigned short>::__value)
	  {
	    output<<"SCALARS greylevels unsigned_short"<<std::endl;
	  }
	else if(slip::__are_same<value_type,bool>::__value)
	  {
	    output<<"SCALARS greylevels bit"<<std::endl;
	  }
	else
	  {
	    output<<"SCALARS greylevels float"<<std::endl;
	  }


	output<<"LOOKUP_TABLE default"<<std::endl;
	if((slip::__are_same<value_type,unsigned char>::__value)
	   ||
	   (slip::__are_same<value_type,char>::__value))
	  {
	     for(int i = rows-1; i >=0; --i)
	       {
		 for(int j = 0; j < cols; ++j)
		   {
		     output<<int(field[i][j])<<" ";
		  } 
	      }
	  }
	else
	  {
	    for(int i = (rows-1); i >=0; --i)
	      {
		for(int j = 0; j < cols; ++j)
		  {
		    output<<field[i][j]<<" ";
		  } 
	      }
	  }
	output<<std::endl;
	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }




}//::slip
#endif //SLIP_VTK_IO_HPP
