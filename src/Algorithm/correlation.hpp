/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file correlation.hpp
 * 
 * \brief Provides some auto and cross correlation algorithms.
 * \since 1.0.0 
 */

#ifndef SLIP_CORRELATION_HPP
#define SLIP_CORRELATION_HPP


#include <algorithm>
#include <numeric>
#include <cassert>
#include <cmath>
#include <complex>
#include "statistics.hpp"
#include "FFT.hpp"
#include "Array.hpp"
#include "error.hpp"
#include "Array3d.hpp"
#include "convolution.hpp"
#include "linear_algebra.hpp"
#include "linear_algebra_traits.hpp"
#include "arithmetic_op.hpp"
#include "complex_cast.hpp"


namespace slip
{
  

  
  /** 
  **   \enum CROSSCORRELATION_TYPE
  **   \brief Choose between different types of crosscorrelation 
  **   \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
  **   \date 2008/01/31
  **   \since 1.0.0
  **    \code
  **   enum  
  **   {
  **   // Standard algorithm
  **   CC, 
  **   // Centered crosscorrelation algorithm 
  **   CCC,
  **   // Pseudo normalized crosscorrelation algorithm
  **   PNCC,
  **   //Normalized crosscorrelation algorithm 
  **   NCC
  **    };
  **    \endcode
  */

// NCC already defined in '/usr/include/x86_64-linux-gnu/bits/ioctl-types.h'
#ifndef NCC
#undef SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
#pragma message("deprecated ! use SLIP_XXX instead")
  enum CROSSCORRELATION_TYPE
    {
      CC,
      CCC,
      PNCC,
      NCC
    };

#else
#define SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
  enum class CROSSCORRELATION_TYPE : std::uint8_t
    {
      SLIP_CC,
      SLIP_CCC,
      SLIP_PNCC,
      SLIP_NCC
    };

#endif

  /** \name crosscorrelations between two ranges */
  /* @{ */
  
  /*!
   ** \brief Computes the standard crosscorrelation between two sequences: 
   ** \f$ \sum_i x_i y_i \f$.  
   ** Multiplies successive elements from the two ranges and adds 
   ** each product into the accumulated value using operator+().  
   ** The values in the ranges are processed in order.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    An InputIterator.
   ** \param last     An InputIterator.
   ** \param first2   An InputIterator.
   ** \return A value equals to the standard crosscorrelation of the two 
   **  sequences.
   **
   ** \pre [first,last) must be valid.
   ** \pre [first2,first2 + (last -  first)) is a valid range.
   ** \par Example:
   ** \code
   ** float f[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M(2,3,f);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::std_crosscorrelation<float>(M.begin(),M.end(),M2.begin())<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** //finding a motif by crosscorrelation
   ** slip::Matrix<float> Motif(8,8,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),8.0,1.0);
   ** slip::Matrix<float> I(128,128,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),0.0,1.0);
   ** slip::Matrix<float> Result(16,16,0.0);
   ** for(std::size_t i = 0; i < I.dim1(); i+=8)
   **  {
   **   for(std::size_t j = 0; j < I.dim2(); j+=8)
   **	 {
   **	  slip::Box2d<int> box(i,j,i+7,j+7);
   **	  Result[i/8][j/8] = slip::std_crosscorrelation<float>(I.upper_left(box),I.bottom_right(box),Motif.begin());
   **	 }
   **	}
   ** \endcode
   **
   */   
  template<typename T, typename InputIterator1, typename InputIterator2>
  inline
  T std_crosscorrelation(InputIterator1 first, InputIterator1 last, 
			 InputIterator2 first2)
  {
    return std::inner_product(first,last,first2,T());
  }

  /*!
   ** \brief Computes the standard crosscorrelation between two sequences according to a mask sequence \f$ \sum_i x_i y_i \f$.
   ** Multiplies successive elements accordind to a mask sequence 
   ** from the two ranges and adds each product into the accumulated value 
   ** using operator+().  
   ** The values in the ranges are processed in order.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/15
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An input iterator.
   ** \param first2 An InputIterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return A value equals to the standard crosscorrelation of the two 
   **  sequences
   ** \pre [first1,last1) must be valid.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 -  first1)) is a valid range.
   ** \par Example:
   ** \code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** int f3[] = {1,0,1,0,0,0};
   ** slip::Matrix<int> Mask(2,3,f3);
   ** std::cout<<slip::std_crosscorrelation_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   **
   */   
  template<typename T, 
	   typename InputIterator1,
	   typename InputIterator2,
	   typename MaskIterator>
  inline
  T std_crosscorrelation_mask(InputIterator1 first1,
			      InputIterator1 last1,
			      MaskIterator mask_first,
			      InputIterator2 first2,
			      typename std::iterator_traits<MaskIterator>::value_type value=typename
			      std::iterator_traits<MaskIterator>::value_type(1))
{
    assert(first1 != last1);
    T init = T(0);
    for (; first1 != last1; ++first1,++first2,++mask_first)
     {
        if(*mask_first == value)
         {
           init +=(*first1)*(*first2);
         }
     }
    return init;
}

  /**  \brief Computes the standard crosscorrelation between two sequences
   **  according to a Predicate. 
   ** \f$ \sum_i x_i y_i \f$
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/15
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An InputIterator.
   **  \param  last1      An InputIterator.
   **  \param  first2     An InputIterator.
   **  \param  pred       A predicate.
   **  \return  A value equals to the standard crosscorrelation of the two 
   **  sequences
   ** \pre [first1,last1) must be valid.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::std_crosscorrelation_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode
*/

template<typename T,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>

 T std_crosscorrelation_if(InputIterator1 first1, 
                           InputIterator1 last1,
                           InputIterator2 first2, 
                           Predicate pred)
 {
    assert(first1 != last1);
    T init = T(0);
    for (; first1 != last1; ++first1, ++first2)
      {
         if(pred(*first1))
             { 
               init +=(*first1)*(*first2);
             }
      }
  return init;
 }

  /*!
   ** \brief Computes the standard crosscorrelation between two sequences: 
   ** \f$ \sum_i x_i y_i \f$.
   ** Multiplies successive elements from the two ranges and adds 
   ** each product into the accumulated value using operator+().  
   ** The values in the ranges are processed in order.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    An InputIterator.
   ** \param last     An InputIterator.
   ** \param first2   An InputIterator.
   ** \return A value equals to the standard crosscorrelation of the two 
   **  sequences
   ** \par Example:
   ** \code
   ** float f[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M(2,3,f);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::cc<float>(M.begin(),M.end(),M2.begin())<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** //finding a motif by crosscorrelation
   ** slip::Matrix<float> Motif(8,8,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),8.0,1.0);
   ** slip::Matrix<float> I(128,128,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),0.0,1.0);
   ** slip::Matrix<float> Result(16,16,0.0);
   ** for(std::size_t i = 0; i < I.dim1(); i+=8)
   **  {
   **   for(std::size_t j = 0; j < I.dim2(); j+=8)
   **	 {
   **	  slip::Box2d<int> box(i,j,i+7,j+7);
   **	  Result[i/8][j/8] = slip::cc<float>(I.upper_left(box),I.bottom_right(box),Motif.begin());
   **	 }
   **	}
   ** \endcode
   **
   */   
  template<typename T, typename InputIterator1, typename InputIterator2>
  inline
  T cc(InputIterator1 first, InputIterator1 last, 
       InputIterator2 first2)
  {
    return slip::std_crosscorrelation<T>(first,last,first2);
  }
  
/*!
   ** \brief Computes the standard crosscorrelation between two sequences according to a mask sequence \f$ \sum_i x_i y_i \f$.
   ** Multiplies successive elements from the two ranges accordind to a mask 
   ** sequence and adds each product according to the mask range into the 
   ** accumulated value using operator+(). The values in the ranges are processed in order.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/15
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An InputIterator.
   ** \param first2 An InputIterator.
   ** \param  value true value of the mask range. Default is 1.
   ** \return A value equals to the standard crosscorrelation of the two 
   **  sequences
   **
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 -  first1)) is a valid range.
   ** \par Example:
   ** \code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** int f3[] = {1,0,1,0,0,0};
   ** slip::Matrix<int> Mask(2,3,f3);
   ** std::cout<<slip::cc_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),1)<<std::endl;
   ** \endcode
   **
   */   
template<typename T, 
	 typename InputIterator1,
	 typename InputIterator2,
	 typename MaskIterator>
  inline
   T cc_mask(InputIterator1 first1,
	     InputIterator1 last1,
	     MaskIterator mask_first,                
	     InputIterator2 first2,
                             
	     typename std::iterator_traits<MaskIterator>::value_type value=typename
	     std::iterator_traits<MaskIterator>::value_type(1))
{
  return slip::std_crosscorrelation_mask<T>(first1,last1,mask_first,first2,value);
}

  /**  \brief Computes the standard crosscorrelation between two sequences
   **  according to a Predicate. 
   ** \f$ \sum_i x_i y_i \f$
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/15
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An InputIterator.
   **  \param  last1      An InputIterator.
   **  \param  first2     An InputIterator.
   **  \param  pred       A predicate.
   **  \return  A value equals to the standard crosscorrelation of the two 
   **  sequences
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   **  \endcode
   ** \code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::cc_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>)<<std::endl;
   ** \endcode
*/

template<typename T,
           typename InputIterator1, 
           typename InputIterator2,
           typename Predicate>

 T cc_if(InputIterator1 first1, 
	 InputIterator1 last1,
	 InputIterator2 first2, 
	 Predicate pred)
{
  return slip::std_crosscorrelation_if<T>(first1,last1,first2,pred);
}

  

 
  /**
   ** \brief Computes the centered crosscorrelation between 
   ** two sequences: \f$ \sum_i (x_i-mean(x))(y_i-mean(y)) \f$.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    An InputIterator.
   ** \param last     An InputIterator.
   ** \param first2   An InputIterator.
   ** \param mean1 mean value of the first range [first,last). 
   ** \param mean2 mean value of the second range [first2, first2 + (last-first)).
   ** \return A value equals to the centered crosscorrelation of the two 
   **  sequences
   **
   ** \pre [first,last) is a valid range.
   ** \pre [first2,first2 + (last -  first)) is a valid range.
   ** \par Example:
   ** \code
   ** float f[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M(2,3,f);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::centered_crosscorrelation<float>(M.begin(),M.end(),M2.begin())<<std::endl;
   ** float mean1 = slip::mean<float>(M.begin(),M.end());
   ** float mean2 = slip::mean<float>(M2.begin(),M2.end());
   ** std::cout<<slip::centered_crosscorrelation<float>(M.begin(),M.end(),M2.begin(),mean1,mean2)<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** //finding a motif by crosscorrelation
   ** slip::Matrix<float> Motif(8,8,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),8.0,1.0);
   ** slip::Matrix<float> I(128,128,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),0.0,1.0);
   ** slip::Matrix<float> Result(16,16,0.0);
   ** for(std::size_t i = 0; i < I.dim1(); i+=8)
   **  {
   **   for(std::size_t j = 0; j < I.dim2(); j+=8)
   **	 {
   **	  slip::Box2d<int> box(i,j,i+7,j+7);
   **	  Result[i/8][j/8] = slip::centered_crosscorrelation<float>(I.upper_left(box),I.bottom_right(box),Motif.begin());
   **	 }
   **	}
   ** \endcode
   */ 
  template<typename Real, typename InputIterator1, typename InputIterator2>
  inline
  Real centered_crosscorrelation(InputIterator1 first, InputIterator1 last, 
				 InputIterator2 first2,
				 typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
				 typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type())
  {
  
    Real __init1 = Real();

    for (; first != last; ++first, ++first2)
       {
	 __init1 = __init1 + (*first  - mean1) * (*first2  - mean2);
       }

    return __init1;
  }

/*!
   ** \brief Computes the centered crosscorrelation between two sequences according to a mask sequence 
   ** \f$ \sum_i (x_i-mean(x))(y_i-mean(y)) \f$
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/15
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An InputIterator.
   ** \param first2 An InputIterator.
   ** \param mean1 mean value of the first range [first,last). 
   ** \param mean2 mean value of the second range [first2, first2 + (last-first)).
   ** \param  value true value of the mask range. Default is 1.
   ** \return A value equals to the centered crosscorrelation of the two 
   **  sequences
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 -  first1)) is a valid range.
   ** \par Example:
   ** \code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** int f3[] = {1,0,1,0,0,0};
   ** slip::Matrix<int> Mask(2,3,f3);
   ** float mean1 = slip::mean_mask<float>(M1.begin(),M1.end(),Mask.begin(),1);
   ** float mean2 = slip::mean_mask<float>(M2.begin(),M2.end(),Mask.begin(),1);
   ** std::cout<<slip::centered_crosscorrelation_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),mean1,mean2,1)<<std::endl;
   ** \endcode
   **
*/
template<typename Real, 
	 typename InputIterator1, 
	 typename InputIterator2,
	 typename MaskIterator>
  inline
  Real centered_crosscorrelation_mask(InputIterator1 first1,InputIterator1 last1, 
				      MaskIterator mask_first,
				      InputIterator2 first2,
                                 
				 typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
				 typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type(),
                                 typename std::iterator_traits<MaskIterator>::value_type value=typename
                                 std::iterator_traits<MaskIterator>::value_type(1))
{
    Real __init1 = Real();
    for (; first1 != last1; ++first1, ++first2,++mask_first)
       {
           if(*mask_first == value)
          {
	    __init1 = __init1 + (*first1  - mean1) * (*first2  - mean2);
          }
      }
    return __init1;
}

/**  \brief Computes the standard crosscorrelation between two sequences
   **  according to a Predicate: \f$ \sum_i (x_i-mean(x))(y_i-mean(y)) \f$
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/15
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An InputIterator.
   **  \param  last1      An InputIterator.
   **  \param  first2     An InputIterator.
   **  \param  pred       A predicate.
   **  \param mean1 mean value of the first range [first,last) (0 by default). 
   **  \param mean2 mean value of the second range [first2, first2 + (last-first)) (0 by default).
   **  \return  A value equals to the centered crosscorrelation of the two 
   **  sequences
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 -  first1)) is a valid range.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** float mean1 = slip::mean_if<float>(M1.begin(),M1.end(),lt5Predicate<float>);
   ** float mean2 = slip::mean_if<float>(M2.begin(),M2.end(),lt5Predicate<float>);
   ** std::cout<<slip::centered_crosscorrelation_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>,mean1,mean2)<<std::endl;
   ** \endcode
*/

template<typename Real, 
	 typename InputIterator1, 
	 typename InputIterator2,
	 typename Predicate>
inline
Real centered_crosscorrelation_if(InputIterator1 first1,InputIterator1 last1, 
				  InputIterator2 first2,
				  Predicate pred,
				  typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
				  typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type())
{


    Real __init1 = Real();
    for (; first1 != last1; ++first1, ++first2)
       {
           if(pred(*first1))
           {
	     __init1 = __init1 + (*first1  - mean1) * (*first2  - mean2);
           }
      }
    return __init1;
}

  /**
   ** \brief Computes the centered crosscorrelation between 
   ** two sequences: \f$ \sum_i (x_i-mean(x))(y_i-mean(y)) \f$.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    An InputIterator.
   ** \param last     An InputIterator.
   ** \param first2   An InputIterator.
   ** \param mean1 mean value of the first range [first,last). 
   ** \param mean2 mean value of the second range [first2, first2 + (last-first)).
   ** \return A value equals to the normalized crosscorrelation of the two 
   **  sequences
   ** \pre [first,last) is a valid range.
   ** \pre [first2,first2 + (last -  first)) is a valid range.
   ** \par Example:
   ** \code
   ** float f[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M(2,3,f);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::ccc<float>(M.begin(),M.end(),M2.begin())<<std::endl;
   ** float mean1 = slip::mean<float>(M.begin(),M.end());
   ** float mean2 = slip::mean<float>(M2.begin(),M2.end());
   ** std::cout<<slip::ccc<float>(M.begin(),M.end(),M2.begin(),mean1,mean2)<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** //finding a motif by crosscorrelation
   ** slip::Matrix<float> Motif(8,8,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),8.0,1.0);
   ** slip::Matrix<float> I(128,128,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),0.0,1.0);
   ** slip::Matrix<float> Result(16,16,0.0);
   ** for(std::size_t i = 0; i < I.dim1(); i+=8)
   **  {
   **   for(std::size_t j = 0; j < I.dim2(); j+=8)
   **	 {
   **	  slip::Box2d<int> box(i,j,i+7,j+7);
   **	  Result[i/8][j/8] = slip::ccc<float>(I.upper_left(box),I.bottom_right(box),Motif.begin());
   **	 }
   **	}
   ** \endcode
   */ 
  template<typename Real, 
	   typename InputIterator1, 
	   typename InputIterator2>
  inline
  Real ccc(InputIterator1 first, InputIterator1 last, 
	   InputIterator2 first2,
	   typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
	   typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type())
  {
    return slip::centered_crosscorrelation<Real>(first,last,first2,mean1,mean2);
  }

/*!
** \brief Computes the centered crosscorrelation between two sequences according to a mask sequence 
** \f$ \sum_i (x_i-mean(x))(y_i-mean(y)) \f$
** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
** \date 2008/12/15
** \since 1.0.0
** \version 0.0.1
** \param first1 An InputIterator.
** \param last1  An InputIterator.
** \param  mask_first An InputIterator.
** \param first2 An InputIterator.
** \param mean1 mean value of the first range [first,last). 
** \param mean2 mean value of the second range [first2, first2 + (last-first)).
** \param  value true value of the mask range. Default is 1.
** \return A value equals to the centered crosscorrelation of the two 
**  sequences
** \pre [first1,last1) is a valid range.
** \pre [first2,first2 + (last1 -  first1)) is a valid range.
** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
** \par Example:
** \code
** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
** slip::Matrix<float> M1(2,3,f1);
** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
** slip::Matrix<float> M2(2,3,f2);
** int f3[] = {1,0,1,0,0,0};
** slip::Matrix<int> Mask(2,3,f3);
** float mean1 = slip::mean_mask<float>(M1.begin(),M1.end(),Mask.begin(),1);
** float mean2 = slip::mean_mask<float>(M2.begin(),M2.end(),Mask.begin(),1);
** std::cout<<slip::ccc_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),mean1,mean2,1)<<std::endl;
** \endcode
   **
*/
template<typename Real, 
	 typename InputIterator1, 
	 typename InputIterator2,
	 typename MaskIterator>
  inline
  Real ccc_mask(InputIterator1 first1,InputIterator1 last1, 
		MaskIterator mask_first,
		InputIterator2 first2,
                               
				 typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
				 typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type(),
                                 typename std::iterator_traits<MaskIterator>::value_type value=typename
                                 std::iterator_traits<MaskIterator>::value_type(1))

{
  return slip::centered_crosscorrelation_mask<Real>(first1,last1,mask_first,first2,mean1,mean2,value);
}

/**  \brief Computes the standard crosscorrelation between two sequences
   **  according to a Predicate: \f$ \sum_i (x_i-mean(x))(y_i-mean(y)) \f$
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/15
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An InputIterator.
   **  \param  last1      An InputIterator.
   **  \param  first2     An InputIterator.
   **  \param  pred       A predicate.
   **  \param mean1 mean value of the first range [first,last). 
   **  \param mean2 mean value of the second range [first2, first2 + (last-first)).
   **  \return  A value equals to the centered crosscorrelation of the two 
   **  sequences
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 - first1)) is a valid range.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** float mean_if1 = slip::mean_if<float>(M1.begin(),M1.end(),lt5Predicate<float>);
   ** float mean_if2 = slip::mean_if<float>(M2.begin(),M2.end(),lt5Predicate<float>);

   ** std::cout<<slip::ccc_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>,mean_if1,mean_if2)<<std::endl; 
   ** \endcode
   */

template<typename Real, 
	 typename InputIterator1, 
	 typename InputIterator2,
	 typename Predicate>
  inline
  Real ccc_if(InputIterator1 first1,InputIterator1 last1, 
				 InputIterator2 first2,Predicate pred,
                                 typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
                                 typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type())
{
  return slip::centered_crosscorrelation_if<Real>(first1,last1,first2,pred,mean1,mean2);
}


/**
   ** \brief Computes the pseudo normalized crosscorrelation between 
   ** two sequences: 
   ** \f$ \frac{\sum_i x_i y_i}{\sum_i (x_i-mean(x))(y_i-mean(y))} \f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0   
   ** \version 0.0.1
   ** \param first    An InputIterator.
   ** \param last     An InputIterator.
   ** \param first2   An InputIterator.
   ** \param mean1 mean value of the first range [first,last). 
   ** \param mean2 mean value of the second range [first2, first2 + (last-first)).
   ** \return A value equals to the centered crosscorrelation of the two 
   **  sequences
   **
   ** \pre [first,last) is a valid range.
   ** \pre [first2,first2 + (last - first)) is a valid range.
   ** \par Example:
   ** \code
   ** float f[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M(2,3,f);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::pseudo_normalized_crosscorrelation<float>(M.begin(),M.end(),M2.begin())<<std::endl;
   ** float mean1 = slip::mean<float>(M.begin(),M.end());
   ** float mean2 = slip::mean<float>(M2.begin(),M2.end());
   ** std::cout<<slip::pseudo_normalized_crosscorrelation<float>(M.begin(),M.end(),M2.begin(),mean1,mean2)<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** //finding a motif by crosscorrelation
   ** slip::Matrix<float> Motif(8,8,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),8.0,1.0);
   ** slip::Matrix<float> I(128,128,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),0.0,1.0);
   ** slip::Matrix<float> Result(16,16,0.0);
   ** for(std::size_t i = 0; i < I.dim1(); i+=8)
   **  {
   **   for(std::size_t j = 0; j < I.dim2(); j+=8)
   **	 {
   **	  slip::Box2d<int> box(i,j,i+7,j+7);
   **	  Result[i/8][j/8] = slip::pseudo_normalized_correlation<float>(I.upper_left(box),I.bottom_right(box),Motif.begin());
   **	 }
   **	}
   ** \endcode
   */ 
  template<typename Real, 
	   typename InputIterator1, 
	   typename InputIterator2>
  inline
  Real pseudo_normalized_crosscorrelation(InputIterator1 first, InputIterator1 last, 
					  InputIterator2 first2,
					  typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
					  typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type())
  {
  
    Real __init1 = Real();
    Real __init2 = Real();
    Real __cc = Real();
    for (; first != last; ++first, ++first2)
       {
	__init1 = __init1 + (*first  - mean1) * (*first  - mean1);
	__init2 = __init2 + (*first2 - mean2) * (*first2 - mean2);
	__cc = __cc + *first * *first2;
       }

    return  __cc / std::sqrt(__init1 * __init2);
  }
  /*!
     ** \brief Computes the pseudo normalized crosscorrelation between 
   ** two sequences according to a mask sequence : 
   ** \f$ \frac{\sum_i x_i y_i}{\sum_i (x_i-mean(x))(y_i-mean(y))} \f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/01/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An InputIterator.
   ** \param first2 An InputIterator.
   ** \param mean1 mean value of the first range [first,last). 
   ** \param mean2 mean value of the second range [first2, first2 + (last-first)).
   ** \param  value true value of the mask range. Default is 1.
   ** \return A value equals to the normalized crosscorrelation of the two 
   **  sequences
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 - first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \par Example:
   ** \code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** int f3[] = {1,0,1,0,0,0};
   ** slip::Matrix<int> Mask(2,3,f3);
   ** float mean1 = slip::mean_mask<float>(M1.begin(),M1.end(),Mask.begin(),1);
   ** float mean2 = slip::mean_mask<float>(M2.begin(),M2.end(),Mask.begin(),1);
 
   ** std::cout<<slip::pseudo_normalized_crosscorrelation_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),mean1,mean2,1)<<std::endl;
   ** \endcode
   **
*/
template<typename Real, typename InputIterator1, typename InputIterator2,typename MaskIterator >
  inline
  Real pseudo_normalized_crosscorrelation_mask(InputIterator1 first1, 
					       InputIterator1 last1, 
					       MaskIterator mask_first,
					       InputIterator2 first2,
					       typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
				 typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type(),typename std::iterator_traits<MaskIterator>::value_type value=typename
                                 std::iterator_traits<MaskIterator>::value_type(1))
  {
    Real __init1 = Real();
    Real __init2 = Real();
    Real __cc = Real();
   for (; first1 != last1; ++first1, ++first2,++mask_first)
       {
           if(*mask_first == value) 
        {
           
	  Real __diff1 = (*first1  - mean1);
	  Real __diff2 = (*first2 - mean2);
	  __init1 = __init1 + (__diff1 * __diff1);
	  __init2 = __init2 + (__diff2 * __diff2);
	  __cc = __cc + (*first1 * *first2);
         }
        }
   return  __cc / std::sqrt(__init1 * __init2);
  }


/**  \brief Computes the standard pseudo normalized crosscorrelation between two sequences 
   **  according to a Predicate. 
   ** \f$ \frac{\sum_i x_i y_i}{\sum_i (x_i-mean(x))(y_i-mean(y))} \f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/01/03
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An InputIterator.
   **  \param  last1      An InputIterator.
   **  \param  first2     An InputIterator.
   **  \param  pred       A predicate.
   **  \param mean1 mean value of the first range [first,last). 
   **  \param mean2 mean value of the second range [first2, first2 + (last-first)).
   **  \return  A value equals to the centered crosscorrelation of the two 
   **  sequences
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 - first1)) is a valid range.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   **\code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** float mean1 = slip::mean_if<float>(M1.begin(),M1.end(),lt5Predicate<float>);
   ** float mean2 = slip::mean_if<float>(M2.begin(),M2.end(),lt5Predicate<float>);

  ** std::cout<<slip::pseudo_normalized_crosscorrelation_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>,mean1,mean2)<<std::endl;
   ** \endcode
*/
template<typename Real, typename InputIterator1, typename InputIterator2,typename Predicate>
  inline
  Real pseudo_normalized_crosscorrelation_if(InputIterator1 first1,InputIterator1 last1, 
				 InputIterator2 first2,Predicate pred,
                                 typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
                                 typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type())

  {

    Real __init1 = Real();
    Real __init2 = Real();
    Real __cc = Real();
    
   for (; first1 != last1; ++first1, ++first2)
       {
           if(pred(*first1))
              {
		 Real __diff1 = (*first1  - mean1);
	  Real __diff2 = (*first2 - mean2);
	  __init1 = __init1 + (__diff1 * __diff1);
	  __init2 = __init2 + (__diff2 * __diff2);
	  __cc = __cc + (*first1 * *first2);
            }
       }
    return  __cc / std::sqrt(__init1 * __init2);
  }


  /**
   ** \brief Computes the pseudo normalized crosscorrelation between 
   ** two sequences: \f$ \frac{\sum_i x_i y_i}{\sum_i (x_i-mean(x))(y_i-mean(y))} \f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    An InputIterator.
   ** \param last     An InputIterator.
   ** \param first2   An InputIterator.
   ** \param mean1 mean value of the first range [first,last). 
   ** \param mean2 mean value of the second range [first2, first2 + (last-first)).
   ** \return A value equals to the centered crosscorrelation of the two 
   **  sequences
   **
   ** \pre [first,last) is a valid range.
   ** \pre [first2,first2 + (last - first)) is a valid range.
   ** \par Example:
   ** \code
   ** float f[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M(2,3,f);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** float mean1 = slip::mean<float>(M.begin(),M.end());
   ** float mean2 = slip::mean<float>(M2.begin(),M2.end());
   ** std::cout<<slip::pncc<float>(M.begin(),M.end(),M2.begin(),mean1,mean2)<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** //finding a motif by crosscorrelation
   ** slip::Matrix<float> Motif(8,8,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),8.0,1.0);
   ** slip::Matrix<float> I(128,128,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),0.0,1.0);
   ** float mean_motif = slip::mean<float>(Motif.begin(),Motif.end());
   ** slip::Matrix<float> Result(16,16,0.0);
   ** for(std::size_t i = 0; i < I.dim1(); i+=8)
   **  {
   **   for(std::size_t j = 0; j < I.dim2(); j+=8)
   **	 {
   **	  slip::Box2d<int> box(i,j,i+7,j+7);
   **     float meanI = slip::mean<float>(I.upper_left(box),I.bottom_right(box)); 
   **	  Result[i/8][j/8] = slip::pncc<float>(I.upper_left(box),I.bottom_right(box),Motif.begin(),meanI,mean_motif);
   **	 }
   **	}
   ** \endcode
   */ 
  template<typename Real, typename InputIterator1, typename InputIterator2>
  inline
  Real pncc(InputIterator1 first, InputIterator1 last, 
	    InputIterator2 first2,
	    typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
	    typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type())
  {
  
   return slip::pseudo_normalized_crosscorrelation<Real>(first,last,first2,mean1,mean2);
  }
  
/*!
** \brief Computes the pseudo normalized crosscorrelation between 
** two sequences according to a mask sequence : \f$ \frac{\sum_i x_i y_i}{\sum_i (x_i-mean(x))(y_i-mean(y))} \f$
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \date 2009/01/03
** \since 1.0.0
** \version 0.0.1
** \param first1 An InputIterator.
** \param last1  An InputIterator.
** \param  mask_first An InputIterator.
** \param first2 An InputIterator.
** \param mean1 mean value of the first range [first,last). 
** \param mean2 mean value of the second range [first2, first2 + (last-first)).
** \param  value true value of the mask range. Default is 1.
** \return A value equals to the normalized crosscorrelation of the two 
**  sequences
** \pre [first1,last1) is a valid range.
** \pre [first2,first2 + (last1 - first1)) is a valid range.
** \par Example:
** \code
** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
** slip::Matrix<float> M1(2,3,f1);
** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
** slip::Matrix<float> M2(2,3,f2);
** int f3[] = {1,0,1,0,0,0};
** slip::Matrix<int> Mask(2,3,f3);
** float mean1 = slip::mean_mask<float>(M1.begin(),M1.end(),Mask.begin(),1);
** float mean2 = slip::mean_mask<float>(M2.begin(),M2.end(),Mask.begin(),1);
 
** std::cout<<slip::pncc_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),mean1,mean2,1)<<std::endl;
   ** \endcode
   **
*/
template<typename Real, 
	 typename InputIterator1, 
	 typename InputIterator2,
	 typename MaskIterator >
  inline
  Real pncc_mask(InputIterator1 first1, 
		 InputIterator1 last1, 
		 MaskIterator mask_first,
		 InputIterator2 first2,
		 typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
		 typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type(),typename std::iterator_traits<MaskIterator>::value_type value=typename
		 std::iterator_traits<MaskIterator>::value_type(1))
  {
    return slip::pseudo_normalized_crosscorrelation_mask<Real>(first1,last1,mask_first,first2,mean1,mean2,value);
  }


/**  \brief Computes the standard pseudo normalized crosscorrelation between two sequences 
   **  according to a Predicate. 
   ** \f$ \frac{\sum_i x_i y_i}{\sum_i (x_i-mean(x))(y_i-mean(y))} \f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/01/03
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An InputIterator.
   **  \param  last1      An InputIterator.
   **  \param  first2     An InputIterator.
   **  \param  pred       A predicate.
   **  \param mean1 mean value of the first range [first,last). 
   **  \param mean2 mean value of the second range [first2, first2 + (last-first)).
   **  \return  A value equals to the centered crosscorrelation of the two 
   **  sequences
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 - first1)) is a valid range.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \endcode
   ** \code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** float mean_if1 = slip::mean_if<float>(M1.begin(),M1.end(),lt5Predicate<float>);
   ** float mean_if2 = slip::mean_if<float>(M2.begin(),M2.end(),lt5Predicate<float>);
   ** std::cout<<slip::pncc_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>,mean_if1,mean_if2)<<std::endl;
   ** \endcode
*/
template<typename Real, 
	 typename InputIterator1, 
	 typename InputIterator2,
	 typename Predicate>
inline
Real pncc_if(InputIterator1 first1,InputIterator1 last1, 
	     InputIterator2 first2,Predicate pred,
	     typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
	     typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type())
  
  {
    return slip::pseudo_normalized_crosscorrelation_if<Real>(first1,last1,first2,pred,mean1,mean2);
  }
  /**
   ** \brief Computes the standard normalized crosscorrelation between 
   ** two sequences: 
   ** \f$ \frac{\sum_i (x_i-mean(x))(y_i-mean(y))}{\sqrt{\sum_i (x_i-mean(x))^2 \sum_i (y_i-mean(y))^2}} \f$.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/09
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    An InputIterator.
   ** \param last     An InputIterator.
   ** \param first2   An InputIterator.
   ** \param mean1 mean value of the first range [first,last). 
   ** \param mean2 mean value of the second range [first2, first2 + (last-first)).
 
   ** \return A value equals to the normalized crosscorrelation of the two 
   **  sequences
   ** \pre [first,last) is a valid range.
   ** \pre [first2,first2 + (last - first)) is a valid range.
   ** \post The result value must be in the range [-1.0,1.0]
   ** \par Example:
   ** \code
   ** float f[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M(2,3,f);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** std::cout<<slip::normalized_crosscorrelation<float>(M.begin(),M.end(),M2.begin())<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** //finding a motif by crosscorrelation
   ** slip::Matrix<float> Motif(8,8,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),8.0,1.0);
   ** slip::Matrix<float> I(128,128,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),0.0,1.0);
   ** float mean_motif = slip::mean<float>(Motif.begin(),Motif.end());
   ** slip::Matrix<float> Result(16,16,0.0);
   ** for(std::size_t i = 0; i < I.dim1(); i+=8)
   **  {
   **   for(std::size_t j = 0; j < I.dim2(); j+=8)
   **	 {
   **	  slip::Box2d<int> box(i,j,i+7,j+7);
   **     float meanI = slip::mean<float>(I.upper_left(box),I.bottom_right(box));
   **	  Result[i/8][j/8] = slip::normalized_crosscorrelation<float>(I.upper_left(box),I.bottom_right(box),Motif.begin(),meanI,mean_motif);
   **	 }
   **	}
   ** \endcode
   */ 
  template<typename Real, 
	   typename InputIterator1, 
	   typename InputIterator2>
  inline
  Real normalized_crosscorrelation(InputIterator1 first, 
				   InputIterator1 last, 
			           InputIterator2 first2,
			           typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
				 typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type())
  {

    //computation of the normalization term
    Real __init0 = Real();
    Real __init1 = Real();
    Real __init2 = Real();
    
    for (; first != last; ++first, ++first2)
       {
	 Real __diff1 = (*first  - mean1);
	 Real __diff2 = (*first2 - mean2);
	 __init0 = __init0 + __diff1 * __diff2;
	 __init1 = __init1 + __diff1 * __diff1;
	 __init2 = __init2 + __diff2 * __diff2;
       }
       
    return __init0 / std::sqrt(__init1 * __init2);
  }

  /*!
   ** \brief Computes the standard normalized crosscorrelation between two sequences according to a mask sequence 
   ** \f$ \frac{\sum_i (x_i-mean(x))(y_i-mean(y))}{\sqrt{\sum_i (x_i-mean(x))^2 \sum_i (y_i-mean(y))^2}} \f$.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/15
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param mask_first An InputIterator.
   ** \param first2 An InputIterator.
   ** \param mean1 mean value of the first range [first,last). 
   ** \param mean2 mean value of the second range [first2, first2 + (last-first)).
   ** \param  value true value of the mask range. Default is 1.
   ** \return A value equals to the normalized crosscorrelation of the two 
   **  sequences
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 - first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.
   ** \par Example:
   ** \code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** int f3[] = {1,0,1,0,0,0};
   ** slip::Matrix<int> Mask(2,3,f3);
   ** float mean1 = slip::mean_mask<float>(M1.begin(),M1.end(),Mask.begin(),1);
   ** float mean2 = slip::mean_mask<float>(M2.begin(),M2.end(),Mask.begin(),1);
   ** std::cout<<slip::normalized_crosscorrelation_mask<float>(M1.begin(),M1.end(),Mask.begin(),M2.begin(),mean1,mean2,1)<<std::endl;
   ** \endcode
   **
*/
template<typename Real, typename InputIterator1, typename InputIterator2,typename MaskIterator >
  inline
  Real normalized_crosscorrelation_mask(InputIterator1 first1, 
					InputIterator1 last1, 
					MaskIterator mask_first,
					InputIterator2 first2,
                                   
			           typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
				 typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type(),typename std::iterator_traits<MaskIterator>::value_type value=typename
                                 std::iterator_traits<MaskIterator>::value_type(1))
  {

    //computation of the normalization term
    Real __init0 = Real();
    Real __init1 = Real();
    Real __init2 = Real();
    
   for (; first1 != last1; ++first1, ++first2,++mask_first)
       {
           if(*mask_first == value) 
        {
           Real __diff1 = (*first1  - mean1);
	   Real __diff2 = (*first2 - mean2);
	   __init0 = __init0 + __diff1 * __diff2;
	   __init1 = __init1 + __diff1 * __diff1;
	   __init2 = __init2 + __diff2 * __diff2;
         }
        }
    return __init0 / std::sqrt(__init1 * __init2);
  }

/**  \brief Computes the standard normalized crosscorrelation between two sequences 
   **  according to a Predicate: ** \f$ \frac{\sum_i (x_i-mean(x))(y_i-mean(y))}{\sqrt{\sum_i (x_i-mean(x))^2 \sum_i (y_i-mean(y))^2}} \f$.
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/15
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An InputIterator.
   **  \param  last1      An InputIterator.
   **  \param  first2     An InputIterator.
   **  \param  pred       A predicate.
   **  \param mean1 mean value of the first range [first,last). 
   **  \param mean2 mean value of the second range [first2, first2 + (last-first)).
   **  \return  A value equals to the centered crosscorrelation of the two 
   **  sequences
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 - first1)) is a valid range.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \encode
   ** \code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** float mean_if1 = slip::mean_if<float>(M1.begin(),M1.end(),lt5Predicate<float>);
   ** float mean_if2 = slip::mean_if<float>(M2.begin(),M2.end(),lt5Predicate<float>);
   ** std::cout<<slip::normalized_crosscorrelation_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>,mean_if1,mean_if2)<<std::endl;
   ** \endcode
*/
template<typename Real, 
	 typename InputIterator1, 
	 typename InputIterator2,
	 typename Predicate>
  inline
  Real normalized_crosscorrelation_if(InputIterator1 first1,InputIterator1 last1, 
				      InputIterator2 first2,Predicate pred,
				      typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
				      typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type())

  {

    //computation of the normalization term
    Real __init0 = Real();
    Real __init1 = Real();
    Real __init2 = Real();
    
   for (; first1 != last1; ++first1, ++first2)
       {
           if(pred(*first1))
              {
                Real __diff1 = (*first1  - mean1);
	        Real __diff2 = (*first2 - mean2);
	        __init0 = __init0 + __diff1 * __diff2;
	        __init1 = __init1 + __diff1 * __diff1;
	        __init2 = __init2 + __diff2 * __diff2;
            }
       }
    return __init0 / std::sqrt(__init1 * __init2);
  }

/**
 ** \brief Computes the standard normalized crosscorrelation between 
 ** two sequences: 
 ** \f$ \frac{\sum_i (x_i-mean(x))(y_i-mean(y))}{\sqrt{\sum_i (x_i-mean(x))^2 \sum_i (y_i-mean(y))^2}} \f$.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2006/12/09
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param first    An InputIterator.
 ** \param last     An InputIterator.
 ** \param first2   An InputIterator.
 ** \param mean1 mean value of the first range [first,last). 
 ** \param mean2 mean value of the second range [first2, first2 + (last-first)).
 
 ** \return A value equals to the normalized crosscorrelation of the two 
 **  sequences
 ** \pre [first,last) is a valid range.
 ** \pre [first2,first2 + (last - first)) is a valid range.
 ** \post The result value must be in the range [-1.0,1.0]
 ** \par Example:
 ** \code
 ** float f[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
 ** slip::Matrix<float> M(2,3,f);
 ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
 ** slip::Matrix<float> M2(2,3,f2);
 ** std::cout<<slip::normalized_crosscorrelation<float>(M.begin(),M.end(),M2.begin())<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** //finding a motif by crosscorrelation
   ** slip::Matrix<float> Motif(8,8,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),8.0,1.0);
   ** slip::Matrix<float> I(128,128,0.0);
   ** slip::iota(Motif.begin(),Motif.end(),0.0,1.0);
   ** slip::Matrix<float> Result(16,16,0.0);
   ** for(std::size_t i = 0; i < I.dim1(); i+=8)
   **  {
   **   for(std::size_t j = 0; j < I.dim2(); j+=8)
   **	 {
   **	  slip::Box2d<int> box(i,j,i+7,j+7);
   **	  Result[i/8][j/8] = slip::normalized_crosscorrelation<float>(I.upper_left(box),I.bottom_right(box),Motif.begin());
   **	 }
   **	}
   ** \endcode
   */ 
  template<typename Real, typename InputIterator1, typename InputIterator2>
  inline
  Real ncc(InputIterator1 first, 
           InputIterator1 last, 
	   InputIterator2 first2,
	   typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
	   typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type())
  {
    return slip::normalized_crosscorrelation<Real>(first,last,first2,mean1,mean2);
  }

/*!
   ** \brief Computes the standard normalized crosscorrelation between two sequences according to a mask sequence 
   ** \f$ \frac{\sum_i (x_i-mean(x))(y_i-mean(y))}{\sqrt{\sum_i x_i^2 \sum_i y_i^2}} \f$
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/12/15
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 An InputIterator.
   ** \param last1  An InputIterator.
   ** \param  mask_first An InputIterator.
   ** \param first2 An InputIterator.
   ** \param mean1 mean value of the first range [first,last). 
   ** \param mean2 mean value of the second range [first2, first2 + (last-first)).
   ** \param  value true value of the mask range. Default is 1.
   ** \return A value equals to the normalized crosscorrelation of the two 
   **  sequences
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 - first1)) is a valid range.
   ** \pre [mask_first,mask_first + (last1 - first1)) is a valid range.

   ** \pre [first,last) must be valid.
   ** \pre The two sequences must of the same sizes.
   ** \par Example:
   ** \code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** int f3[] = {1,0,1,0,0,0};
   ** slip::Matrix<int> Mask(2,3,f3);
   ** float mean1 = slip::mean_mask<float>(M1.begin(),M1.end(),Mask.begin(),1);
   ** float mean2 = slip::mean_mask<float>(M2.begin(),M2.end(),Mask.begin(),1);
  
   ** std::cout<<slip::ncc_mask<float>(M1.begin(),M1.end(),M2.begin(),Mask.begin(),mean1,mean2,1)<<std::endl;
   ** \endcode
   **
*/
template<typename Real, typename InputIterator1, typename InputIterator2,typename MaskIterator >
  inline
  Real ncc_mask(InputIterator1 first1, 
		InputIterator1 last1, 
		MaskIterator mask_first,
		InputIterator2 first2,
                
		typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
		typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type(),typename std::iterator_traits<MaskIterator>::value_type value=typename
		std::iterator_traits<MaskIterator>::value_type(1))

{
  return slip::normalized_crosscorrelation_mask<Real>(first1,last1,mask_first,first2,mean1,mean2,value);
}

/**  \brief Computes the standard normalized crosscorrelation between two sequences according to a Predicate: \f$ \frac{\sum_i (x_i-mean(x))(y_i-mean(y))}{\sqrt{\sum_i x_i^2 \sum_i y_i^2}} \f$
   ** \author Hammoud Mouhamed<hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2008/12/15
   ** \since 1.0.0
   ** \version 0.0.1
   **  \param  first1     An InputIterator.
   **  \param  last1      An InputIterator.
   **  \param  first2     An InputIterator.
   **  \param  pred       A predicate.
   **  \param mean1 mean value of the first range [first,last). 
   **  \param mean2 mean value of the second range [first2, first2 + (last-first)).
   **  \return  A value equals to the centered crosscorrelation of the two 
   **  sequences
   ** \pre [first1,last1) is a valid range.
   ** \pre [first2,first2 + (last1 - first1)) is a valid range.
   **  \par Example:
   **  \code
   **  bool lt5Predicate (const T& val)
   **   {
   **     return (val < T(5));
   **   };
   ** \encode
   ** \code
   ** float f1[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M1(2,3,f1);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** float mean_if1 = slip::mean_if<float>(M1.begin(),M1.end(),lt5Predicate<float>);
   ** float mean_if2 = slip::mean_if<float>(M2.begin(),M2.end(),lt5Predicate<float>);
   ** std::cout<<slip::ncc_if<float>(M1.begin(),M1.end(),M2.begin(),lt5Predicate<float>,mean_if1,mean_if2)<<std::endl;
   ** \endcode
*/
template<typename Real, typename InputIterator1, typename InputIterator2,typename Predicate>
  inline
  Real ncc_if(InputIterator1 first1,InputIterator1 last1, 
				 InputIterator2 first2,Predicate pred,
                                 typename std::iterator_traits<InputIterator1>::value_type mean1 = typename std::iterator_traits<InputIterator1>::value_type(),
                                 typename std::iterator_traits<InputIterator1>::value_type mean2 = typename std::iterator_traits<InputIterator2>::value_type())

  {
         return slip::centered_crosscorrelation_if<Real>(first1,last1,first2,pred,mean1,mean2);
 
  }

 /* @} */
  
  /** \name autocorrelations and autocovariance algorithms */
  /* @{ */
  /**
   ** \brief Computes the autocorrelations from lag -(last-first) to (last-first) of a range [first,last).
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/05/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    A RandomAccessIterator to the data.
   ** \param last     A RandomAccessIterator to the data.
   ** \param auto_first  A RandomAccessIterator to the autocorrelations range.
   ** \param auto_last  A RandomAccessIterator to the autocorrelations range.
   ** \pre [first,last) must be valid.
   ** \pre [auto_first, auto_last) must be valid.
   ** \pre (auto_last - auto_first) == 2*(last-first) - 1
   ** \pre (last-first) > 0
   ** \par Description:
   **  Computes the autocorrelations from lag -(last-first) to (last-first) of a range [first,last).
   **   \li \f$ R(j) = \sum_n first_n first_{n-j} \f$
   **   \li \f$ R(0) \f$ corresponds the energy of the data
 
   ** \par Example:
   ** \code
   ** slip::Array<float> V(5);
   ** slip::iota(V.begin(),V.end(),1.0f,1.0f);
   ** std::cout<<V<<std::endl;
   ** slip::Array<float> Auto(2*V.size()-1);
   ** slip::autocorrelation_full(V.begin(),V.end(),Auto.begin(),Auto.end());
   ** std::cout<<Auto<<std::endl;
   ** \endcode
   */ 
  template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  inline
  void autocorrelation_full(RandomAccessIterator1 first, 
			    RandomAccessIterator1 last,
			    RandomAccessIterator2 auto_first,
			    RandomAccessIterator2 auto_last)

  {

    //computation of the means
    typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
    typedef typename std::iterator_traits<RandomAccessIterator2>::difference_type _Difference;
 
    assert(last - first > 0);
    assert((auto_last - auto_first) == (last-first)*2 - 1); 
    _Difference med = _Difference(last - first) - 1;
    for(_Difference i = 0; i <= med; ++i)
      {
	value_type __init0 = value_type();
	RandomAccessIterator1 first_tmp  = first;
	RandomAccessIterator2 first_tmp2 = first + i;
	for (; first_tmp2 != last; ++first_tmp, ++first_tmp2)
	  {
	    value_type __diff1 = slip::conj(*first_tmp);
	    value_type __diff2 = *first_tmp2;
	    __init0 = __init0 + __diff1 * __diff2;
	  }
	value_type val = __init0;
       
	*(auto_first + med + i) = val;
	*(auto_first + med - i) = val;
      }
  }
  
   /**
   ** \brief Computes the autocorrelations from lag -(last-first) to (last-first) of a range [first,last).
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/05/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    A RandomAccessIterator to the data.
   ** \param last     A RandomAccessIterator to the data.
   ** \param auto_first  A RandomAccessIterator to the autocorrelations range.
   ** \param auto_last  A RandomAccessIterator to the autocorrelations range.
   ** \pre [first,last) must be valid.
   ** \pre [auto_first, auto_last) must be valid.
   ** \pre (auto_last - auto_first) == (last-first) - 1
   ** \pre (last-first) > 0
   ** \par Description:
   ** Computes the autocorrelations from lag -(last-first) to (last-first) of a range [first,last).
   **   \li \f$ R(j) = \sum_n first_n first_{n-j} \f$
   **   \li \f$ R(0) \f$ corresponds the energy of the data
 
   ** \par Example:
   ** \code
   ** slip::Array<float> V(5);
   ** slip::iota(V.begin(),V.end(),1.0f,1.0f);
   ** std::cout<<V<<std::endl;
   ** slip::Array<float> Auto(V.size());
   ** slip::autocorrelation(V.begin(),V.end(),Auto.begin(),Auto.end());
   ** std::cout<<Auto<<std::endl;
   ** \endcode
   */ 
  template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  inline
  void autocorrelation(RandomAccessIterator1 first, 
		       RandomAccessIterator1 last,
		       RandomAccessIterator2 auto_first,
		       RandomAccessIterator2 auto_last)

  {

    typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
    typedef typename std::iterator_traits<RandomAccessIterator2>::difference_type _Difference;
 
    assert(last - first > 0);
    assert((auto_last - auto_first) == (last-first)); 
    _Difference med = _Difference(last - first);
    for(_Difference i = 0; i < med; ++i)
      {
	value_type __init0 = value_type();
	RandomAccessIterator1 first_tmp  = first;
	RandomAccessIterator2 first_tmp2 = first + i;
	for (; first_tmp2 != last; ++first_tmp, ++first_tmp2)
	  {
	    value_type __diff1 = slip::conj(*first_tmp);
	    value_type __diff2 = *first_tmp2;
	    __init0 = __init0 + __diff1 * __diff2;
	  }
	value_type val = __init0;
       
	*(auto_first + i) = val;
      }
  }
  
 /**
   ** \brief Computes the biased autocorrelations from lag -(last-first) to (last-first) of a range [first,last).
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/05/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    A RandomAccessIterator to the data.
   ** \param last     A RandomAccessIterator to the data.
   ** \param auto_first  A RandomAccessIterator to the autocorrelations range.
   ** \param auto_last  A RandomAccessIterator to the autocorrelations range.
   ** \pre [first,last) must be valid.
   ** \pre [auto_first, auto_last) must be valid.
   ** \pre (auto_last - auto_first) == (last-first) - 1
   ** \pre (last-first) > 0
   ** \par Description:
   ** Computes the autocorrelations from lag -(last-first) to (last-first) of a range [first,last).
   **   \li \f$ R(j) = \frac{1}{N}\sum_n first_n first_{n-j} \f$
   **   \li \f$ R(0) \f$ corresponds the energy of the data
 
   ** \par Example:
   ** \code
   ** slip::Array<float> V(5);
   ** slip::iota(V.begin(),V.end(),1.0f,1.0f);
   ** std::cout<<V<<std::endl;
   ** slip::Array<float> Auto(V.size());
   ** slip::biased_autocorrelation(V.begin(),V.end(),Auto.begin(),Auto.end());
   ** std::cout<<Auto<<std::endl;
   ** \endcode
   */ 
  template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  inline
  void biased_autocorrelation(RandomAccessIterator1 first, 
			      RandomAccessIterator1 last,
			      RandomAccessIterator2 auto_first,
			      RandomAccessIterator2 auto_last)

  {
    typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
    typedef typename std::iterator_traits<RandomAccessIterator2>::difference_type _Difference;
    slip::autocorrelation(first,last,auto_first,auto_last);
    
    _Difference N = (auto_last - auto_first);
    value_type one_over_N = value_type(1)/value_type(N);
    std::transform(auto_first,auto_last,auto_first,std::bind2nd(std::multiplies<value_type>(),one_over_N));
    
  }

   /**
   ** \brief Computes the unbiased autocorrelations from lag -(last-first) to (last-first) of a range [first,last).
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/05/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    A RandomAccessIterator to the data.
   ** \param last     A RandomAccessIterator to the data.
   ** \param auto_first  A RandomAccessIterator to the autocorrelations range.
   ** \param auto_last  A RandomAccessIterator to the autocorrelations range.
   ** \pre [first,last) must be valid.
   ** \pre [auto_first, auto_last) must be valid.
   ** \pre (auto_last - auto_first) == (last-first) - 1
   ** \pre (last-first) > 0
   ** \par Description:
   ** Computes the autocorrelations from lag -(last-first) to (last-first) of a range [first,last).
   **   \li \f$ R(j) = \frac{1}{N-j}\sum_n first_n first_{n-j} \f$
   **   \li \f$ R(0) \f$ corresponds the energy of the data
 
   ** \par Example:
   ** \code
   ** slip::Array<float> V(5);
   ** slip::iota(V.begin(),V.end(),1.0f,1.0f);
   ** std::cout<<V<<std::endl;
   ** slip::Array<float> Auto(V.size());
   ** slip::unbiased_autocorrelation(V.begin(),V.end(),Auto.begin(),Auto.end());
   ** std::cout<<Auto<<std::endl;
   ** \endcode
   */ 
  template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  inline
  void unbiased_autocorrelation(RandomAccessIterator1 first, 
				RandomAccessIterator1 last,
				RandomAccessIterator2 auto_first,
				RandomAccessIterator2 auto_last)

  {

    typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
    typedef typename std::iterator_traits<RandomAccessIterator2>::difference_type _Difference;
 
    assert(last - first > 0);
    assert((auto_last - auto_first) == (last-first)); 
    _Difference med = _Difference(last - first);
    for(_Difference i = 0; i < med; ++i)
      {
	value_type __init0 = value_type();
	RandomAccessIterator1 first_tmp  = first;
	RandomAccessIterator2 first_tmp2 = first + i;
	for (; first_tmp2 != last; ++first_tmp, ++first_tmp2)
	  {
	    value_type __diff1 = slip::conj(*first_tmp);
	    value_type __diff2 = *first_tmp2;
	    __init0 = __init0 + __diff1 * __diff2;
	  }
	value_type val = __init0;
       
	*(auto_first + i) = val / value_type(med-i);
      }
  }

   /**
   ** \brief Computes the autocovariance from lag -(last-first) to 0 of a range [first,last).
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/05/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    A RandomAccessIterator to the data.
   ** \param last     A RandomAccessIterator to the data.
   ** \param auto_first  A RandomAccessIterator to the autocorrelations range.
   ** \param auto_last  A RandomAccessIterator to the autocorrelations range.
   ** \pre [first,last) must be valid.
   ** \pre [auto_first, auto_last) must be valid.
   ** \pre (auto_last - auto_first) == (last-first) - 1
   ** \pre (last-first) > 0
   ** \par Description:
   ** Computes the autocorrelations from lag -(last-first) to (last-first) of a range [first,last).
   **   \li \f$ R(j) = \sum_n first_n first_{n-j} \f$
   **   \li \f$ R(0) \f$ corresponds the energy of the data
 
   ** \par Example:
   ** \code
   ** slip::Array<float> V(5);
   ** slip::iota(V.begin(),V.end(),1.0f,1.0f);
   ** std::cout<<V<<std::endl;
   ** slip::Array<float> Auto(V.size());
   ** slip::autocovariance(V.begin(),V.end(),Auto.begin(),Auto.end());
   ** std::cout<<Auto<<std::endl;
   ** \endcode
   */ 
  template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  inline
  void autocovariance(RandomAccessIterator1 first, 
		      RandomAccessIterator1 last,
		      RandomAccessIterator2 auto_first,
		      RandomAccessIterator2 auto_last)
    
  {

     typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
    typedef typename std::iterator_traits<RandomAccessIterator2>::difference_type _Difference;
 
    assert(last - first > 0);
    assert((auto_last - auto_first) == (last-first)); 
    //coputes the mean
    value_type mean = slip::mean<value_type>(first,last);
    _Difference med = _Difference(last - first);
    for(_Difference i = 0; i < med; ++i)
      {
	value_type __init0 = value_type();
	RandomAccessIterator1 first_tmp  = first;
	RandomAccessIterator2 first_tmp2 = first + i;
	for (; first_tmp2 != last; ++first_tmp, ++first_tmp2)
	  {
	    value_type __diff1 = (slip::conj(*first_tmp) - mean);
	    value_type __diff2 = (*first_tmp2 - mean);
	    __init0 = __init0 + __diff1 * __diff2;
	  }
	value_type val = __init0;
       
	*(auto_first + i) = val;
      }
  }

   /**
   ** \brief Computes the biased autocovariance from lag -(last-first) to 0 of a range [first,last).
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/05/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    A RandomAccessIterator to the data.
   ** \param last     A RandomAccessIterator to the data.
   ** \param auto_first  A RandomAccessIterator to the autocorrelations range.
   ** \param auto_last  A RandomAccessIterator to the autocorrelations range.
   ** \pre [first,last) must be valid.
   ** \pre [auto_first, auto_last) must be valid.
   ** \pre (auto_last - auto_first) == (last-first) - 1
   ** \pre (last-first) > 0
   ** \par Description:
   ** Computes the autocorrelations from lag -(last-first) to (last-first) of a range [first,last).
   **   \li \f$ R(j) = \sum_n first_n first_{n-j} \f$
   **   \li \f$ R(0) \f$ corresponds the energy of the data
 
   ** \par Example:
   ** \code
   ** slip::Array<float> V(5);
   ** slip::iota(V.begin(),V.end(),1.0f,1.0f);
   ** std::cout<<V<<std::endl;
   ** slip::Array<float> Auto(V.size());
   ** slip::biased_autocovariance(V.begin(),V.end(),Auto.begin(),Auto.end());
   ** std::cout<<Auto<<std::endl;
   ** \endcode
   */ 
  template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  inline
  void biased_autocovariance(RandomAccessIterator1 first, 
			     RandomAccessIterator1 last,
			     RandomAccessIterator2 auto_first,
			     RandomAccessIterator2 auto_last)
    
  {
    typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
    typedef typename std::iterator_traits<RandomAccessIterator2>::difference_type _Difference;
    slip::autocovariance(first,last,auto_first,auto_last);
    
    _Difference N = (auto_last - auto_first);
    value_type one_over_N = value_type(1)/value_type(N);
    std::transform(auto_first,auto_last,auto_first,std::bind2nd(std::multiplies<value_type>(),one_over_N));
   typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
   
  }
   /**
   ** \brief Computes the unbiased autocovariance from lag -(last-first) to 0 of a range [first,last).
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/05/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first    A RandomAccessIterator to the data.
   ** \param last     A RandomAccessIterator to the data.
   ** \param auto_first  A RandomAccessIterator to the autocorrelations range.
   ** \param auto_last  A RandomAccessIterator to the autocorrelations range.
   ** \pre [first,last) must be valid.
   ** \pre [auto_first, auto_last) must be valid.
   ** \pre (auto_last - auto_first) == (last-first) - 1
   ** \pre (last-first) > 0
   ** \par Description:
   ** Computes the autocorrelations from lag -(last-first) to (last-first) of a range [first,last).
   **   \li \f$ R(j) = \frac{1}{N-j}\sum_n first_n first_{n-j} \f$
   **   \li \f$ R(0) \f$ corresponds the energy of the data
 
   ** \par Example:
   ** \code
   ** slip::Array<float> V(5);
   ** slip::iota(V.begin(),V.end(),1.0f,1.0f);
   ** std::cout<<V<<std::endl;
   ** slip::Array<float> Auto(V.size());
   ** slip::unbiased_autocovariance(V.begin(),V.end(),Auto.begin(),Auto.end());
   ** std::cout<<Auto<<std::endl;
   ** \endcode
   */ 
  template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  inline
  void unbiased_autocovariance(RandomAccessIterator1 first, 
			       RandomAccessIterator1 last,
			       RandomAccessIterator2 auto_first,
			       RandomAccessIterator2 auto_last)
    
  {
    typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
    typedef typename std::iterator_traits<RandomAccessIterator2>::difference_type _Difference;
 
    assert(last - first > 0);
    assert((auto_last - auto_first) == (last-first)); 
    //coputes the mean
    value_type mean = slip::mean<value_type>(first,last);
    _Difference med = _Difference(last - first);
    for(_Difference i = 0; i < med; ++i)
      {
	value_type __init0 = value_type();
	RandomAccessIterator1 first_tmp  = first;
	RandomAccessIterator2 first_tmp2 = first + i;
	for (; first_tmp2 != last; ++first_tmp, ++first_tmp2)
	  {
	    value_type __diff1 = (slip::conj(*first_tmp) - mean);
	    value_type __diff2 = (*first_tmp2 - mean);
	    __init0 = __init0 + __diff1 * __diff2;
	  }
	value_type val = __init0;
       
	*(auto_first + i) = val  / value_type(med-i);
      }
  }

   /**
   ** \brief Constructs the biased autocorrelation matrix from given a range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first RandomAccessIterator to the first element of the data.
   ** \param  last RandomAccessIterator to one past-the-end element of the data.
   ** \param A The resulting biased autocorrelation matrix.
   ** \pre (A.cols() == A.rows())
   ** \pre A.rows == last - first
   ** \par Example:
   ** \code
   ** slip::Array<float> Vtoep(5);
   ** slip::iota(Vtoep.begin(),Vtoep.end(),1.0);
   ** std::cout<<"Vtoeplitz = \n"<<Vtoep<<std::endl;
   ** slip::Matrix<float> Atoep(Vtoep.size(),Vtoep.size());
   ** slip::biased_autocorrelation_matrix(Vtoep.begin(),Vtoep.end(),Atoep);
   ** std::cout<<"Atoeplitz = \n"<<Atoep<<std::endl;
   ** \endcode
   */
  template <typename RandomAccessIterator, typename Matrix>
  inline
  void biased_autocorrelation_matrix(RandomAccessIterator first, 
				     RandomAccessIterator last, 
				     Matrix& A)
  {
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type value_type;
    
    assert(A.rows() == A.cols());
    assert(int(last -first) == int(A.rows()));
    slip::Array<value_type> Auto(A.rows());
    slip::biased_autocorrelation(first,last,
				 Auto.begin(),Auto.end());
    slip::toeplitz(Auto.begin(),Auto.end(),A);
    
  }
  

    /**
   ** \brief Constructs the biased autocorrelation matrix from given a range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first RandomAccessIterator to the first element of the data.
   ** \param last RandomAccessIterator to one past-the-end element of the data.
   ** \param A The resulting biased autocovariance matrix.
   ** \pre (A.cols() == A.rows())
   ** \pre A.rows == last - first
   ** \par Example:
   ** \code
   ** slip::Array<float> Vtoep(5);
   ** slip::iota(Vtoep.begin(),Vtoep.end(),1.0);
   ** std::cout<<"Vtoeplitz = \n"<<Vtoep<<std::endl;
   ** slip::Matrix<float> Atoep(Vtoep.size(),Vtoep.size());
   ** slip::biased_autocovariance_matrix(Vtoep.begin(),Vtoep.end(),Atoep);
   ** std::cout<<"Atoeplitz = \n"<<Atoep<<std::endl;
   ** \endcode
   */
  template <typename RandomAccessIterator, typename Matrix>
  inline
  void biased_autocovariance_matrix(RandomAccessIterator first, 
				    RandomAccessIterator last, 
				    Matrix& A)
  {
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type value_type;
    
     //    _Difference size = last -first;
    assert(A.rows() == A.cols());
    assert(int(last -first) == int(A.rows()));
    slip::Array<value_type> Auto(A.rows());
    slip::biased_autocovariance(first,last,
				Auto.begin(),Auto.end());
    slip::toeplitz(Auto.begin(),Auto.end(),A);
    
  }

  /* @} */

  /** \name crosscorrelation map algorithms */
  /* @{ */

  /**
  ** \brief Computes the crosscorrelation between two Images.
  ** \author Denis Arrivault <denis.arrivault_AT_sic.univ-poitiers.fr>
  ** \date 2008/01/30
  ** \since 1.0.0
  ** \version 0.0.1
  **
  ** \param in1_upper_left : A 2d input iterator (image).
  ** \param in1_bottom_right : A 2d input iterator (image).
  ** \param in2_upper_left : A 2d input iterator (mask).
  ** \param in2_bottom_right : A 2d input iterator (mask).
  ** \param out_upper_left : A 2d output iterator (result).
  ** \param out_bottom_right : A 2d output iterator (result).
  ** \param t : type of algorithm CC, CCC, NCC, PNCC
  **
  ** \pre [in1_upper_left,in1_bottom_right) is valid
  ** \pre [in2_upper_left,in2_upper_left + (in1_bottom_right - in1_upper_left)) must be valid.
   ** \pre (out_bottom_right - out_upper_left) == (in1_bottom_right - in1_upper_left)
   ** \pre the value_type of the input sequences have to be real (not complex)
   ** \par Example:
   ** \code
   **  slip::GrayscaleImage<double> I1;
   **  I1.read("image1.jpg");
   **  slip::GrayscaleImage<double> I2;
   **  I2.read("imag2.jpg");
   **  slip::DPoint2d<int> size1 = I1.bottom_right() - I1.upper_left();
   **  slip::DPoint2d<int> size2 = I2.bottom_right() - I2.upper_left();
   **  slip::GrayscaleImage<double> OutSTD(size1[0],size1[1],1.0);
   **  // STD crosscorrelation calculation - CC algorithm
   **   slip::crosscorrelation2d<double>(I1.upper_left(),I1.bottom_right(),I2.upper_left(),I2.bottom_right()
   **                                     ,OutSTD.upper_left(),OutSTD.bottom_right(),slip::CC);
   **  slip::change_dynamic_01(OutSTD.upper_left(),OutSTD.bottom_right(),OutSTD.upper_left(),
   **                          slip::AFFINE_FUNCTION);
   **  OutSTD.write("STD_CC_crosscorrelation_2d.jpg");
   **  \endcode
  */   
  template<typename Real, typename InputIterator2d1, typename InputIterator2d2, typename OutputIterator2d>
  inline
  void crosscorrelation2d(InputIterator2d1 in1_upper_left, InputIterator2d1 in1_bottom_right, 
			   InputIterator2d2 in2_upper_left, InputIterator2d2 in2_bottom_right, 
			   OutputIterator2d out_upper_left, OutputIterator2d out_bottom_right,
			   CROSSCORRELATION_TYPE t)
  {					
    typename InputIterator2d1::difference_type size2din1 = in1_bottom_right - in1_upper_left;
    typename InputIterator2d2::difference_type size2din2 = in2_bottom_right - in2_upper_left;
    typename OutputIterator2d::difference_type size2dout = out_bottom_right - out_upper_left;
    assert(size2din1 == size2dout);
    typename OutputIterator2d::difference_type size2d(size2din1[0] + size2din2[0] - 1, size2din1[1] + size2din2[1] - 1);
    //Input containers resized 
    slip::Array2d<Real> I(size2d[0],size2d[1],Real());
    slip::Box2d<int> box1(size2din2[0]/2,size2din2[1]/2,size2din2[0]/2 + size2din1[0]-1,size2din2[1]/2 + size2din1[1]-1);
    std::copy(in1_upper_left,in1_bottom_right,I.upper_left(box1));
#ifndef SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
    if(t==NCC)
#else
    if(t==slip::CROSSCORRELATION_TYPE::SLIP_NCC)
#endif
      {
      	slip::Box2d<int> box2;
      	slip::DPoint2d<int> d;
      	typename InputIterator2d2::value_type mean2 =  slip::mean<typename InputIterator2d2::value_type>(in2_upper_left,in2_bottom_right);
      	for(size_t i=0; i<(size_t)size2dout[0]; i++) 
	  for(size_t j=0; j<(size_t)size2dout[1]; j++) 
	    {
	      box2.set_coord(i,j,size2din2[0]-1+i,size2din2[1]-1+j);
	      typename InputIterator2d1::value_type mean1 =  slip::mean<typename InputIterator2d1::value_type>(I.upper_left(box2),I.bottom_right(box2));
	      d.set_coord(i,j);
	      *(out_upper_left + d) = normalized_crosscorrelation<Real>(in2_upper_left,in2_bottom_right,I.upper_left(box2),mean2,mean1);
	    }
      }
#ifndef SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
    else if(t==PNCC)
#else
    else if(t==slip::CROSSCORRELATION_TYPE::SLIP_PNCC)
#endif
      {
      	slip::Box2d<int> box2;
      	slip::DPoint2d<int> d;
      	typename InputIterator2d2::value_type mean2 =  slip::mean<typename InputIterator2d2::value_type>(in2_upper_left,in2_bottom_right);
      	for(size_t i=0; i<(size_t)size2dout[0]; i++) 
	  for(size_t j=0; j<(size_t)size2dout[1]; j++) 
	    {
	      box2.set_coord(i,j,size2din2[0]-1+i,size2din2[1]-1+j);
	      typename InputIterator2d1::value_type mean1 =  slip::mean<typename InputIterator2d1::value_type>(I.upper_left(box2),I.bottom_right(box2));
	      d.set_coord(i,j);
	      *(out_upper_left + d) = pseudo_normalized_crosscorrelation<Real>(in2_upper_left,in2_bottom_right,I.upper_left(box2),mean2,mean1);
	    }
      }
#ifndef SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
    else if(t==CCC)
#else
    else if(t==slip::CROSSCORRELATION_TYPE::SLIP_CCC)
#endif
      {
      	slip::Box2d<int> box2;
      	slip::DPoint2d<int> d;
      	typename InputIterator2d2::value_type mean2 =  slip::mean<typename InputIterator2d2::value_type>(in2_upper_left,in2_bottom_right);
	for(size_t i=0; i<(size_t)size2dout[0]; i++) 
	  for(size_t j=0; j<(size_t)size2dout[1]; j++) 
	    {
	      box2.set_coord(i,j,size2din2[0]-1+i,size2din2[1]-1+j);
	      typename InputIterator2d1::value_type mean1 =  slip::mean<typename InputIterator2d1::value_type>(I.upper_left(box2),I.bottom_right(box2));
	      d.set_coord(i,j);
	      *(out_upper_left + d) = ccc<Real>(in2_upper_left,in2_bottom_right,I.upper_left(box2),mean2,mean1);
	    }
      }
#ifndef SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
    else if(t==CC)
#else
    else if(t==slip::CROSSCORRELATION_TYPE::SLIP_CC)
#endif
      {
      	slip::Box2d<int> box2;
      	slip::DPoint2d<int> d;
 	for(size_t i=0; i<(size_t)size2dout[0]; i++) 
 	  for(size_t j=0; j<(size_t)size2dout[1]; j++) 
 	    {
 	      box2.set_coord(i,j,size2din2[0]-1+i,size2din2[1]-1+j);
 	      d.set_coord(i,j);
 	      *(out_upper_left + d) = cc<Real>(in2_upper_left,in2_bottom_right,I.upper_left(box2));
 	    }
      }
    else
      {
	std::cerr<< slip::CROSSCORRELATION_BAD_ALGO << std::endl;
	slip::Box2d<int> box2;
	slip::DPoint2d<int> d;
	for(size_t i=0; i<(size_t)size2dout[0]; i++) 
	  for(size_t j=0; j<(size_t)size2dout[1]; j++) 
	    {
	      box2.set_coord(i,j,size2din2[0]-1+i,size2din2[1]-1+j);
	      d.set_coord(i,j);
	      *(out_upper_left + d) = cc<Real>(in2_upper_left,in2_bottom_right,I.upper_left(box2));
	    }
      }
  }
  
  /**
  ** \brief Computes the standard crosscorrelation between two Images.
  ** \author Denis Arrivault <denis.arrivault_AT_sic.univ-poitiers.fr>
  ** \date 2008/01/30
  ** \since 1.0.0
  ** \version 0.0.1
  **
  ** \param in1_upper_left : A 2d input iterator (image).
  ** \param in1_bottom_right : A 2d input iterator (image).
  ** \param in2_upper_left : A 2d input iterator (mask).
  ** \param in2_bottom_right : A 2d input iterator (mask).
  ** \param out_upper_left : A 2d output iterator (result).
  ** \param out_bottom_right : A 2d output iterator (result).
  **
    ** \pre [in1_upper_left,in1_bottom_right) is valid
  ** \pre [in2_upper_left,in2_upper_left + (in1_bottom_right - in1_upper_left)) must be valid.
   ** \pre (out_bottom_right - out_upper_left) == (in1_bottom_right - in1_upper_left)
   ** \pre the value_type of the input sequences have to be real (not complex)
 
  */   
  template<typename Real, typename InputIterator2d1, typename InputIterator2d2, typename OutputIterator2d>
  inline
  void std_crosscorrelation2d(InputIterator2d1 in1_upper_left, InputIterator2d1 in1_bottom_right, 
			       InputIterator2d2 in2_upper_left, InputIterator2d2 in2_bottom_right, 
			       OutputIterator2d out_upper_left,
			       OutputIterator2d out_bottom_right)
  {
#ifndef SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
    slip::crosscorrelation2d<Real>(in1_upper_left,in1_bottom_right,in2_upper_left,in2_bottom_right,out_upper_left,out_bottom_right,CC);
#else
    slip::crosscorrelation2d<Real>(in1_upper_left,in1_bottom_right,in2_upper_left,in2_bottom_right,out_upper_left,out_bottom_right,slip::CROSSCORRELATION_TYPE::SLIP_CC);
#endif
  }
  
  /**
  ** \brief Computes the centered crosscorrelation between two Images.
  ** \author Denis Arrivault <denis.arrivault_AT_sic.univ-poitiers.fr>
  ** \date 2008/01/30
  ** \since 1.0.0
  ** \version 0.0.1
  **
  ** \param in1_upper_left : A 2d input iterator (image).
  ** \param in1_bottom_right : A 2d input iterator (image).
  ** \param in2_upper_left : A 2d input iterator (mask).
  ** \param in2_bottom_right : A 2d input iterator (mask).
  ** \param out_upper_left : A 2d output iterator (result).
  ** \param out_bottom_right : A 2d output iterator (result).
  **
  ** \pre [in1_upper_left,in1_bottom_right) is valid
  ** \pre [in2_upper_left,in2_upper_left + (in1_bottom_right - in1_upper_left)) must be valid.
   ** \pre (out_bottom_right - out_upper_left) == (in1_bottom_right - in1_upper_left)
  ** \pre the value_type of the input sequences have to be real (not complex)

  */   
  template<typename Real, typename InputIterator2d1, typename InputIterator2d2, typename OutputIterator2d>
  inline
  void centered_crosscorrelation2d(InputIterator2d1 in1_upper_left, InputIterator2d1 in1_bottom_right, 
				    InputIterator2d2 in2_upper_left, InputIterator2d2 in2_bottom_right, 
				    OutputIterator2d out_upper_left,
				    OutputIterator2d out_bottom_right)
  {
#ifndef SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
    slip::crosscorrelation2d<Real>(in1_upper_left,in1_bottom_right,in2_upper_left,in2_bottom_right,out_upper_left,out_bottom_right,CCC);
#else
    slip::crosscorrelation2d<Real>(in1_upper_left,in1_bottom_right,in2_upper_left,in2_bottom_right,out_upper_left,out_bottom_right,slip::CROSSCORRELATION_TYPE::SLIP_CCC);
#endif
  }
  
  
  /**
  ** \brief Computes the pseudo normalized crosscorrelation between two Images.
  ** \author Denis Arrivault <denis.arrivault_AT_sic.univ-poitiers.fr>
  ** \date 2008/01/30
  ** \since 1.0.0
  ** \version 0.0.1
  **
  ** \param in1_upper_left : A 2d input iterator (image).
  ** \param in1_bottom_right : A 2d input iterator (image).
  ** \param in2_upper_left : A 2d input iterator (mask).
  ** \param in2_bottom_right : A 2d input iterator (mask).
  ** \param out_upper_left : A 2d output iterator (result).
  ** \param out_bottom_right : A 2d output iterator (result).
  **
  ** \pre [in1_upper_left,in1_bottom_right) is valid
  ** \pre [in2_upper_left,in2_upper_left + (in1_bottom_right - in1_upper_left)) must be valid.
   ** \pre (out_bottom_right - out_upper_left) == (in1_bottom_right - in1_upper_left)
  ** \pre the value_type of the input sequences have to be real (not complex)

  */   
  template<typename Real, typename InputIterator2d1, typename InputIterator2d2, typename OutputIterator2d>
  inline
  void pseudo_normalized_crosscorrelation2d(InputIterator2d1 in1_upper_left, InputIterator2d1 in1_bottom_right, 
			       InputIterator2d2 in2_upper_left, InputIterator2d2 in2_bottom_right, 
			       OutputIterator2d out_upper_left,
			       OutputIterator2d out_bottom_right)
  {
#ifndef SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
    slip::crosscorrelation2d<Real>(in1_upper_left,in1_bottom_right,in2_upper_left,in2_bottom_right,out_upper_left,out_bottom_right,PNCC);
#else
    slip::crosscorrelation2d<Real>(in1_upper_left,in1_bottom_right,in2_upper_left,in2_bottom_right,out_upper_left,out_bottom_right,slip::CROSSCORRELATION_TYPE::SLIP_PNCC);
#endif
  }
  
  /**
   ** \brief Computes the normalized crosscorrelation between two Images.
   ** \author Denis Arrivault <denis.arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/01/30
   ** \since 1.0.0
   ** \version 0.0.1
   **
   ** \param in1_upper_left : A 2d input iterator (image).
   ** \param in1_bottom_right : A 2d input iterator (image).
   ** \param in2_upper_left : A 2d input iterator (mask).
   ** \param in2_bottom_right : A 2d input iterator (mask).
   ** \param out_upper_left : A 2d output iterator (result).
   ** \param out_bottom_right : A 2d output iterator (result).
   **
   ** \pre [in1_upper_left,in1_bottom_right) is valid
   ** \pre [in2_upper_left,in2_upper_left + (in1_bottom_right - in1_upper_left)) must be valid.
   ** \pre (out_bottom_right - out_upper_left) == (in1_bottom_right - in1_upper_left)
   ** \pre the value_type of the input sequences have to be real (not complex)
   */   
  template<typename Real, typename InputIterator2d1, typename InputIterator2d2, typename OutputIterator2d>
  inline
  void normalized_crosscorrelation2d(InputIterator2d1 in1_upper_left, InputIterator2d1 in1_bottom_right, 
				      InputIterator2d2 in2_upper_left, InputIterator2d2 in2_bottom_right, 
				      OutputIterator2d out_upper_left,
				      OutputIterator2d out_bottom_right)
  {
#ifndef SLIP_ENUM_CROSSCORRELATION_TYPE_PREFIXED
    slip::crosscorrelation2d<Real>(in1_upper_left,in1_bottom_right,in2_upper_left,in2_bottom_right,out_upper_left,out_bottom_right,NCC);
#else
    slip::crosscorrelation2d<Real>(in1_upper_left,in1_bottom_right,in2_upper_left,in2_bottom_right,out_upper_left,out_bottom_right,slip::CROSSCORRELATION_TYPE::SLIP_NCC);
#endif
  }
/* @} */

  /** \name fft crosscorrelation algorithms */
  /* @{ */

/**
   ** \brief Computes the standard crosscorrelation between two sequences using fft.
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr> (2007/14/04)
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : change the fft algorithm used
   ** \date 2008/10/04
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first    An InputIterator.
   ** \param last     An InputIterator.
   ** \param first2   An InputIterator.
   ** \param result_first   An Output iterator.
   ** \param result_last An Output iterator.
   ** \return A value equals to the standard crosscorrelation of the two 
   **  sequences
   **
   ** \pre [first,last) must be valid.
   ** \pre [first2,first2 + (last-first)) must be valid.
   ** \pre [result_first,result_first + (last-first)) must be valid.
   ** \par Example:
   ** \code
   ** float f[] = {-1.0,0.0,4.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M(2,3,f);
   ** float f2[] = {-2.0,12.0,5.0,-7.0,6.0,-18.0};
   ** slip::Matrix<float> M2(2,3,f2);
   ** slip::fft_crosscorrelation(M.begin(),M.end(),M2.begin(),M2.end()); 
  ** \endcode
   */   
template<typename InputIterator1, 
	 typename InputIterator2,
	 typename OutputIterator>
  inline
  void fft_crosscorrelation(InputIterator1 first, InputIterator1 last, 
			    InputIterator2 first2,InputIterator2 last2,
			    OutputIterator result_first,
			    OutputIterator result_last)
  {    
    assert((last - first) != 0);
    assert((last2 - first2) != 0);
    assert((result_last-result_first) >= (last - first) + (last2 - first2) -1);

    std::size_t size= static_cast<std::size_t>((last-first) +(last2-first2)-1);
    
    typedef typename std::iterator_traits<InputIterator1>::value_type value_type;
    typedef typename slip::lin_alg_traits<value_type>::value_type Real;

   
    slip::Array<value_type> signal(size,first,last);
    slip::Array<value_type> kernel(size,first2,last2);
      
    slip::Array<std::complex<Real> > fft1(size,std::complex<Real>());
    slip::Array<std::complex<Real> > fft2(size,std::complex<Real>());
     
    slip::real_fft(signal.begin(), signal.end(), fft1.begin());
    slip::real_fft(kernel.begin(),kernel.end(), fft2.begin());
    
     for (std::size_t i = 0; i < size; ++i)
       {
	 fft1[i] *= std::conj(fft2[i]);
       }
     slip::ifft(fft1.begin(), fft1.end(), fft2.begin());
     slip::fftshift(fft2.begin(),fft2.end());
     std::transform(fft2.begin(),fft2.end(),result_first,
		    slip::un_real<std::complex<Real>,Real>());
   }

/**
 ** \brief Computes the FFT central part of the crosscorrelation.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2009/10/08
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param first    A RandomAccessIterator iterator to the data.
 ** \param last     A RandomAccessIterator iterator to the data.
 ** \param first2   A RandomAccessIterator iterator to the filter.
 ** \param last2    A RandomAccessIterator iterator to the filter.
 ** \param result_first   A RandomAccessIterator iterator to the correlation.
 ** \param result_last   A RandomAccessIterator iterator to the correlation.
 ** \pre data value type must be the same for the 3 ranges
 ** \pre [first,last) must be valid.
 ** \pre [first,last) and [first2,last2) are assumed to contain data of the same type
 ** \pre [first,last) and [first2,last2) are supposed to be (last-first) periodic
 ** \pre (result_last - result_first) == (last-first)
 ** \pre (last2-first2) == (last-first)
   ** \par Complexity: 3*K*2*N*log_2(2*N) + N = 6KNlog_2(N) + (4K + 2)N with N =  (last-first) + (last2-first2) + 1
   ** \par Example:
   ** \code
   ** slip::Array<double> signal1(16);
   ** slip::iota(signal1.begin(),signal1.end(),1.0);
   ** slip::Array<double> signal2(16);
   ** slip::iota(signal2.begin(),signal2.end(),3.0);
   ** slip::Array<double> resultc(16);
   ** std::cout<<"signal1 = \n"<<signal1<<std::endl;
   ** std::cout<<"signal2 = \n"<<signal2<<std::endl;
   ** slip::fft_crosscorrelation_same(signal1.begin(),signal1.end(),
   **				      signal2.begin(),signal2.end(),
   **			              resultc.begin(),resultc.end());
   ** std::cout<<"fft crosscorrelation_same(signal1,signal2) = \n"<<resultc<<std::endl;
   ** \endcode
   */
template<typename InputIterator1, 
	 typename InputIterator2,
	 typename OutputIterator>
  inline
  void fft_crosscorrelation_same(InputIterator1 first, InputIterator1 last, 
				 InputIterator2 first2,InputIterator2 last2,
				 OutputIterator result_first,
				 OutputIterator result_last)
  {  
    assert((last - first) == (last2 - first2));
    assert((last - first) == (result_last - result_first));
    assert((last - first) == (last2 - first2));
    
    std::size_t size= static_cast<std::size_t>((last-first) +(last2-first2)-1);
    
    typedef typename std::iterator_traits<InputIterator1>::value_type value_type;
    typedef typename slip::lin_alg_traits<value_type>::value_type Real;

   
    slip::Array<value_type> signal(size,first,last);
    slip::Array<value_type> kernel(size,first2,last2);
      
    slip::Array<std::complex<Real> > fft1(size,std::complex<Real>());
    slip::Array<std::complex<Real> > fft2(size,std::complex<Real>());
     
    slip::real_fft(signal.begin(), signal.end(), fft1.begin());
    slip::real_fft(kernel.begin(),kernel.end(), fft2.begin());
    
     for (std::size_t i = 0; i < size; ++i)
       {
	 fft1[i] *= std::conj(fft2[i]);
       }
     slip::ifft(fft1.begin(), fft1.end(), fft2.begin());
     slip::fftshift(fft2.begin(),fft2.end());
     std::transform(fft2.begin()+((last2-first2)/2),
		    fft2.begin()+((last2-first2)/2) + (last-first),result_first,
		    slip::un_real<std::complex<Real>,Real>());
  }



/**
   ** \brief Computes the FFT circular crosscorrelation.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2014/03/13
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first    A RandomAccessIterator iterator to the data.
   ** \param last     A RandomAccessIterator iterator to the data.
   ** \param first2   A RandomAccessIterator iterator to the filter.
   ** \param last2    A RandomAccessIterator iterator to the filter.
   ** \param result_first   A RandomAccessIterator iterator to the crosscorrelation.
   ** \param result_last    A RandomAccessIterator iterator to the crosscorrelation.
   ** \pre data value type must be the same for the 3 ranges
   ** \pre [first,last) must be valid.
   ** \pre [first,last) and [first2,last2) are assumed to contain data of the same type
   ** \pre [first,last) and [first2,last2) are supposed to be (last-first) periodic
   ** \pre (result_last-result_first) == (last-first)
   ** \pre (last2-first2) == (last-first)
   ** \par Complexity: 3*K*2*N*log_2(2*N) + N = 6KNlog_2(N) + (4K + 2)N with N =  (last-first)
   ** \par Example:
   ** \code
   ** slip::Array<double> signal1(16);
   ** slip::iota(signal1.begin(),signal1.end(),1.0);
   ** slip::Array<double> signal2(16);
   ** slip::iota(signal2.begin(),signal2.end(),3.0);
   ** slip::Array<double> resultc(16);
   ** std::cout<<"signal1 = \n"<<signal1<<std::endl;
   ** std::cout<<"signal2 = \n"<<signal2<<std::endl;
   ** slip::fft_circular_crosscorrelation(signal1.begin(),signal1.end(),
   **				          signal2.begin(),signal2.end(),
   **			                  resultc.begin(),resultc.end());
   ** std::cout<<"circular fft crosscorrelation (signal1,signal2) = \n"<<resultc<<std::endl;
   ** \endcode
   */
   template<typename RandomAccessIterator1, 
	    typename RandomAccessIterator2, 
	    typename RandomAccessIterator3>
  inline
  void fft_circular_crosscorrelation(RandomAccessIterator1 first , RandomAccessIterator1 last, 
				     RandomAccessIterator2 first2, RandomAccessIterator2 last2,
				     RandomAccessIterator3 result_first, RandomAccessIterator3 result_last )
  {
    // assert((last - first) == (last2 - first2));
    // assert((last - first) == (result_last - result_first));
    // assert((last - first) == (last2 - first2));

    // typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type;
    // typedef typename slip::lin_alg_traits<value_type>::value_type T;

   
    // std::size_t N = static_cast<std::size_t>(last-first);
    // slip::Array<std::complex<T> > fft1(N);
    // slip::Array<std::complex<T> > fft2(N);
    // slip::real_fft(first,last,fft1.begin());
    // slip::real_fft(first2,last2,fft2.begin());
    // typename slip::Array2d<std::complex<T> >::iterator it_fft1 = fft1.begin();
    // typename slip::Array2d<std::complex<T> >::iterator it_fft1_last = fft1.end();
    
    // typename slip::Array2d<std::complex<T> >::iterator it_fft2 = fft2.begin();
    // for(; it_fft1 != it_fft1_last; ++it_fft1, ++it_fft2)
    //   {
    // 	*it_fft1 *= std::conj(*it_fft2);
    //   }
    // slip::ifft(fft1.begin(),fft1.end(),fft2.begin());
    // slip::fftshift(fft2.begin(),fft2.end());
    // std::transform(fft2.begin(),fft2.end(),result_first,
    // 		   slip::un_real<std::complex<T>,T>());  
    
    
     slip::fft_circular_convolution(first,last,
				    std::reverse_iterator<RandomAccessIterator2>(last2),
				    std::reverse_iterator<RandomAccessIterator2>(first2),
				    result_first,result_last);
  }
/**
   ** \brief Computes the standard complex crosscorrelation between two sequences using fft.
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr> (2007/14/04)
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> : change the fft algorithm used
   ** \date 2008/10/04
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param first    An InputIterator.
   ** \param last     An InputIterator.
   ** \param first2   An InputIterator.
   ** \param last2 An InputIterator.
   ** \param result_first   An Output iterator.
   ** \param result_last An Output iterator.
   ** \return A value equals to the standard crosscorrelation of the two 
   **  sequences
   **
   ** \pre [first,last) must be valid.
   ** \pre [first2,first2 + (last-first)) must be valid.
   ** \pre [result_first,result_first + (last-first)) must be valid.
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** std::complex<double> dxc[] = {TC(1.0,3.0), TC(2.0,0.0), TC(3.0,-1.0), TC(4.0,1.0)};
   ** std::complex<double> dyc[] = {TC(-1.0,1.0), TC(2.0,-1.0), TC(1.0,0.0),TC(-1.0,-1.0)};
   ** slip::Array<TC> xc(4,dxc);
   ** slip::Array<TC> yc(4,dyc);
   ** slip::Array<TC> convxyc(x.size()+y.size() - 1);
   ** std::cout<<"xc = "<<xc<<std::endl;
   ** std::cout<<"yc = "<<yc<<std::endl;
   ** slip::complex_xcorr(xc.begin(),xc.end(),
   **	                  yc.begin(),yc.end(),
   **	                  convxyc.begin(),convxyc.end());
   ** std::cout<<"fft xcorr(xc,yc) = "<<convxyc<<std::endl;
   **
   ** \endcode
   */
template<typename InputIterator1, 
	 typename InputIterator2,
	 typename OutputIterator>
  inline
  void complex_fft_crosscorrelation(InputIterator1 first, InputIterator1 last, 
				    InputIterator2 first2,InputIterator2 last2,
				    OutputIterator result_first, 
				    OutputIterator result_last)
  {
    assert((last - first) != 0);
    assert((last2 - first2) != 0);
    assert((result_last-result_first) >= (last - first) + (last2 - first2) -1);


    std::size_t size= static_cast<std::size_t>((last-first) +(last2-first2)-1);
    
    typedef typename std::iterator_traits<InputIterator1>::value_type value_type;
    typedef typename slip::lin_alg_traits<value_type>::value_type Real;

   
    slip::Array<value_type> signal(size,first,last);
    slip::Array<value_type> kernel(size,first2,last2);
      
    slip::Array<std::complex<Real> > fft1(size,std::complex<Real>());
    slip::Array<std::complex<Real> > fft2(size,std::complex<Real>());
       
    slip::complex_fft(signal.begin(), signal.end(), fft1.begin());
    slip::complex_fft(kernel.begin(),kernel.end(), fft2.begin());
    
     for (std::size_t i = 0; i < size; ++i)
       {
	 fft1[i] *= std::conj(fft2[i]);
       }
     slip::ifft(fft1.begin(), fft1.end(), result_first);
     slip::fftshift(result_first,result_first+size);
  }

/**
   ** \brief Computes the circular crosscorrelation between two 2D sequences using fft2d.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr> (2007/12/05)
   ** \date 2009/23/01
   ** \since 1.0.0
   ** \version 0.0.3
   ** \param in1_upper_left  A 2d input iterator (image).
   ** \param in1_bottom_right  A 2d input iterator (image).
   ** \param in2_upper_left  A 2d input iterator (mask).
   ** \param in2_bottom_right  A 2d input iterator (mask).
   ** \param out_upper_left  A 2d output iterator (result).
   ** \param out_bottom_right  A 2d output iterator (result).
   **
   ** \pre the value_type of the input sequences have to be real (not complex)
   ** \pre (in1_bottom_right - in1_upper_left) == (out_bottom_right - out_upper_left)
   ** \pre (in2_bottom_right - in2_upper_left) == (out_bottom_right - out_upper_left)
   ** \par Example:
   ** \code
   **  slip::GrayscaleImage<double> I1;
   **  I1.read("image1.jpg");
   **  slip::GrayscaleImage<double> I2;
   **  I2.read("imag2.jpg");
   **  slip::DPoint2d<int> size1 = I1.bottom_right() - I1.upper_left();
   **  slip::DPoint2d<int> size2 = I2.bottom_right() - I2.upper_left();
   **  slip::DPoint2d<int> size(size1[0] + size2[0] - 1, size1[1] + size2[1] - 1);
   **  slip::Array2d<double> A1(size[0],size[1],0.0);
   **  slip::Array2d<double> A2(size[0],size[1],0.0);
   **  slip::Box2d<int> box1(size2[0]/2,size2[1]/2,size2[0]/2 + size1[0]-1,size2[1]/2 + size1[1]-1);
   **  slip::Box2d<int> box2(size1[0]/2,size1[1]/2,size1[0]/2 + size2[0]-1,size1[1]/2 + size2[1]-1);
   **  std::copy(I1.upper_left(),I1.bottom_right(),A1.upper_left(box1));
   **  std::copy(I2.upper_left(),I2.bottom_right(),A2.upper_left(box2));
   **  slip::GrayscaleImage<double> OutFFT(size[0],size[1],1.0);
   **  // FFT crosscorrelation calculation
   **  slip::fft_circular_crosscorrelation2d(A1.upper_left(),A1.bottom_right()
   **        ,A2.upper_left(),A2.bottom_right(),OutFFT.upper_left(),OutFFT.bottom_right());
   **  slip::change_dynamic_01(OutFFT.upper_left(),OutFFT.bottom_right(),OutFFT.upper_left(),
   **                          slip::AFFINE_FUNCTION);
   **  OutFFT.write("fft_crosscorrelation_2d.jpg");
   **  \endcode
   */   
  template<typename InputIterator2d1, typename InputIterator2d2, typename OutputIterator2d>
  inline
  void fft_circular_crosscorrelation2d(InputIterator2d1 in1_upper_left, 
				       InputIterator2d1 in1_bottom_right, 
				       InputIterator2d2 in2_upper_left,
				       InputIterator2d2 in2_bottom_right, 
				       OutputIterator2d out_upper_left, 
				       OutputIterator2d out_bottom_right)
  {
  
    assert((in1_bottom_right - in1_upper_left) == (out_bottom_right - out_upper_left));
    
    assert((in2_bottom_right - in2_upper_left) == (out_bottom_right - out_upper_left));
    typename InputIterator2d1::difference_type size2din1 = in1_bottom_right - in1_upper_left;
    typename InputIterator2d2::difference_type size2din2 = in2_bottom_right - in2_upper_left;
    typename OutputIterator2d::difference_type size2dout = out_bottom_right - out_upper_left;
    typedef  typename slip::lin_alg_traits<typename std::iterator_traits<InputIterator2d1>::value_type>::value_type Real;
    
    slip::Array2d<std::complex<Real> > fft1(size2din1[0],size2din1[1]);
    slip::Array2d<std::complex<Real> > fft2(size2din1[0],size2din1[1]);

    slip::Array2d<Real> Kernel(size2din1[0],size2din1[1]);
      typedef std::reverse_iterator<InputIterator2d2> reverse_iterator;
    std::copy(reverse_iterator(in2_bottom_right- slip::DPoint2d<int>(1,0)),
	      reverse_iterator(in2_upper_left),
	      Kernel.upper_left());
     
    slip::real_fft2d(in1_upper_left, in1_bottom_right, fft1.upper_left());
    slip::real_fft2d(Kernel.upper_left(),Kernel.bottom_right(), 
 		     fft2.upper_left());
    slip::multiplies(fft1.begin(),fft1.end(),fft2.begin(),fft1.begin());
  
   
  
    slip::ifft2d(fft1.upper_left(), fft1.bottom_right(), fft2.upper_left());
    slip::fftshift2d(fft2.upper_left(),fft2.bottom_right());
    std::transform(fft2.upper_left(),fft2.bottom_right(),out_upper_left,slip::un_real<std::complex<Real>,Real>());  
   
  }

/**
   ** \brief Computes the standard crosscorrelation between two 2D sequences using fft2d.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2009/09/17
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in1_upper_left  A 2d input iterator (image).
   ** \param in1_bottom_right  A 2d input iterator (image).
   ** \param in2_upper_left  A 2d input iterator (mask).
   ** \param in2_bottom_right  A 2d input iterator (mask).
   ** \param out_upper_left  A 2d output iterator (result).
   ** \param out_bottom_right  A 2d output iterator (result).
   **
   ** \pre the value_type of the input sequences have to be real (not complex)
   ** \pre (in1_bottom_right - in1_upper_left) + (in2_bottom_right - in2_upper_left) - (1,1) = (out_bottom_right - out_upper_left)
   ** \par Example:
   ** \code
   **  slip::GrayscaleImage<double> I1;
   **  I1.read("image1.jpg");
   **  slip::GrayscaleImage<double> I2;
   **  I2.read("imag2.jpg");
   **  slip::DPoint2d<int> size1 = I1.bottom_right() - I1.upper_left();
   **  slip::DPoint2d<int> size2 = I2.bottom_right() - I2.upper_left();
   **  slip::DPoint2d<int> size(size1[0] + size2[0] - 1, size1[1] + size2[1] - 1);
   **  slip::Array2d<double> A1(size[0],size[1],0.0);
   **  slip::Array2d<double> A2(size[0],size[1],0.0);
   **  slip::Box2d<int> box1(size2[0]/2,size2[1]/2,size2[0]/2 + size1[0]-1,size2[1]/2 + size1[1]-1);
   **  slip::Box2d<int> box2(size1[0]/2,size1[1]/2,size1[0]/2 + size2[0]-1,size1[1]/2 + size2[1]-1);
   **  std::copy(I1.upper_left(),I1.bottom_right(),A1.upper_left(box1));
   **  std::copy(I2.upper_left(),I2.bottom_right(),A2.upper_left(box2));
   **  slip::GrayscaleImage<double> OutFFT(size[0],size[1],1.0);
   **  // FFT crosscorrelation calculation
   **  slip::fft_crosscorrelation2d(A1.upper_left(),A1.bottom_right()
   **        ,A2.upper_left(),A2.bottom_right(),OutFFT.upper_left(),OutFFT.bottom_right());
   **  slip::change_dynamic_01(OutFFT.upper_left(),OutFFT.bottom_right(),OutFFT.upper_left(),
   **                          slip::AFFINE_FUNCTION);
   **  OutFFT.write("fft_crosscorrelation_2d.jpg");
   **  \endcode
   */   
  template<typename InputIterator2d1, typename InputIterator2d2, typename OutputIterator2d>
  inline
  void fft_crosscorrelation2d(InputIterator2d1 in1_upper_left, 
			       InputIterator2d1 in1_bottom_right, 
			       InputIterator2d2 in2_upper_left, 
			       InputIterator2d2 in2_bottom_right, 
			       OutputIterator2d out_upper_left, 
			       OutputIterator2d out_bottom_right)
  {
    assert(((in1_bottom_right - in1_upper_left)[0]+(in2_bottom_right - in2_upper_left)[0] - 1) == (out_bottom_right - out_upper_left)[0]);
    assert(((in1_bottom_right - in1_upper_left)[1]+(in2_bottom_right - in2_upper_left)[1] - 1) == (out_bottom_right - out_upper_left)[1]);

    typename InputIterator2d1::difference_type size2din1 = in1_bottom_right - in1_upper_left;
    typename InputIterator2d2::difference_type size2din2 = in2_bottom_right - in2_upper_left;
    typename OutputIterator2d::difference_type size2dout = out_bottom_right - out_upper_left;
    
    typedef typename std::iterator_traits<InputIterator2d1>::value_type value_type;
    typedef  typename slip::lin_alg_traits<value_type>::value_type Real;
     
    std::size_t rows =  size2din1[0] + size2din2[0] - 1; 
    std::size_t cols =  size2din1[1] + size2din2[1] - 1; 
    //zero-padd Signal and Kernel data
    slip::Array2d<Real> Signal_zero_padded(rows,cols,Real());
    slip::Array2d<Real> Kernel_zero_padded(rows,cols,Real());
    slip::Box2d<int> signal_box(0,0,
				static_cast<int>(size2din1[0]-1),
				static_cast<int>(size2din1[1]-1));
    slip::Box2d<int> kernel_box(0,0,
				static_cast<int>(size2din2[0]-1),
				static_cast<int>(size2din2[1]-1));
    std::copy(in1_upper_left,in1_bottom_right,Signal_zero_padded.upper_left(signal_box));
    //rotate the kernel by 180°
    typedef std::reverse_iterator<InputIterator2d2> reverse_iterator;
    std::copy(reverse_iterator(in2_bottom_right- slip::DPoint2d<int>(1,0)),
	      reverse_iterator(in2_upper_left),
	      Kernel_zero_padded.upper_left(kernel_box));
   

    slip::Array2d<std::complex<Real> > fft1(rows,cols,std::complex<Real>());
    slip::Array2d<std::complex<Real> > fft2(rows,cols,std::complex<Real>());
   
    //computes the two fft
    slip::real_fft2d(Signal_zero_padded,fft1);
    slip::real_fft2d(Kernel_zero_padded,fft2);
     //computes fft1*fft2
    slip::multiplies(fft1.begin(),fft1.end(),fft2.begin(),fft1.begin());
    //ifft2d
    slip::ifft2d(fft1.upper_left(), fft1.bottom_right(), fft2.upper_left());
    
    //the result should be real
     std::transform(fft2.begin(),fft2.end(),out_upper_left,slip::un_real<std::complex<Real>,Real>()); 
   
   
  } 



/**
 ** \brief Computes the standard crosscorrelation between two 2D sequences using fft2d.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
 ** \date 2009/09/17
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param in1_upper_left  A 2d input iterator (image).
 ** \param in1_bottom_right  A 2d input iterator (image).
 ** \param in2_upper_left  A 2d input iterator (mask).
 ** \param in2_bottom_right  A 2d input iterator (mask).
 ** \param out_upper_left  A 2d output iterator (result).
 ** \param out_bottom_right  A 2d output iterator (result).
 ** \pre (in1_bottom_right - in1_upper_left)[0] == (out_bottom_right - out_upper_left)[0]
 ** \pre (in1_bottom_right - in1_upper_left)[1] == (out_bottom_right - out_upper_left)[1]
 ** \par Example:
 ** \code
 ** slip::Matrix<float> Axcorr(8,10);
 ** slip::iota(Axcorr.begin(),Axcorr.end(),1.0f);
 ** slip::Matrix<float> Bxcorr(4,4);
 ** slip::iota(Bxcorr.begin(),Bxcorr.end(),10.0f);
 ** std::cout<<"Axcorr = \n"<<Axcorr<<std::endl;
 ** std::cout<<"Bxcorr = \n"<<Bxcorr<<std::endl;
 ** slip::Matrix<double> Cxcorr_s(Axcorr.rows(),Axcorr.cols());
 ** slip::fft_crosscorrelation2d_same(Axcorr.upper_left(),
 **			       Axcorr.bottom_right(),
 **			       Bxcorr.upper_left(),
 **			       Bxcorr.bottom_right(),
 **			       Cxcorr_s.upper_left(),
 **			       Cxcorr_s.bottom_right());
 ** std::cout<<"Cxcorr_s = slip::fft_crosscorrelation2d_same(Axcorr,Bxcorr,Cxcorr_s) = \n"<<Cxcorr_s<<std::endl;
   **\endcode
   */
template<typename InputIterator2d1, 
	 typename InputIterator2d2, 
	 typename OutputIterator2d>
inline
void fft_crosscorrelation2d_same(InputIterator2d1 in1_upper_left, 
				 InputIterator2d1 in1_bottom_right, 
				 InputIterator2d2 in2_upper_left, 
				 InputIterator2d2 in2_bottom_right, 
				 OutputIterator2d out_upper_left, 
				 OutputIterator2d out_bottom_right)
  {
    assert((in1_bottom_right - in1_upper_left)[0] == (out_bottom_right - out_upper_left)[0]);
    assert((in1_bottom_right - in1_upper_left)[1] == (out_bottom_right - out_upper_left)[1]);
    
    typename InputIterator2d1::difference_type size2din1 = in1_bottom_right - in1_upper_left;
    typename InputIterator2d2::difference_type size2din2 = in2_bottom_right - in2_upper_left;
    typename OutputIterator2d::difference_type size2dout = out_bottom_right - out_upper_left;
    
    typedef typename std::iterator_traits<InputIterator2d1>::value_type value_type;
    typedef  typename slip::lin_alg_traits<value_type>::value_type Real;
     
    std::size_t rows =  size2din1[0] + size2din2[0] - 1; 
    std::size_t cols =  size2din1[1] + size2din2[1] - 1; 
    //zero-padd Signal and Kernel data
    slip::Array2d<Real> Signal_zero_padded(rows,cols,Real());
    slip::Array2d<Real> Kernel_zero_padded(rows,cols,Real());
    slip::Box2d<int> signal_box(0,0,
				static_cast<int>(size2din1[0]-1),
				static_cast<int>(size2din1[1]-1));
    slip::Box2d<int> kernel_box(0,0,
				static_cast<int>(size2din2[0]-1),
				static_cast<int>(size2din2[1]-1));
    std::copy(in1_upper_left,in1_bottom_right,Signal_zero_padded.upper_left(signal_box));
    //rotate the kernel by 180°
    typedef std::reverse_iterator<InputIterator2d2> reverse_iterator;
    std::copy(reverse_iterator(in2_bottom_right- slip::DPoint2d<int>(1,0)),
	      reverse_iterator(in2_upper_left),
	      Kernel_zero_padded.upper_left(kernel_box));

    slip::Array2d<std::complex<Real> > fft1(rows,cols,std::complex<Real>());
    slip::Array2d<std::complex<Real> > fft2(rows,cols,std::complex<Real>());
   
    //computes the two fft
    slip::real_fft2d(Signal_zero_padded,fft1);
    slip::real_fft2d(Kernel_zero_padded,fft2);
     //computes fft1*fft2
    slip::multiplies(fft1.begin(),fft1.end(),fft2.begin(),fft1.begin());
    //ifft2d
    slip::ifft2d(fft1.upper_left(), fft1.bottom_right(), fft2.upper_left());
    
    //the result should be real
    
    slip::Box2d<int> out_box(size2din2[0]/2,size2din2[1]/2,
			     static_cast<int>(size2din2[0]/2 + size2din1[0] - 1),
			     static_cast<int>(size2din2[1]/2 + size2din1[1] - 1));
    
    std::transform(fft2.upper_left(out_box),
		   fft2.bottom_right(out_box),
		   out_upper_left,slip::un_real<std::complex<Real>,Real>());  
        
  } 


  /**
   ** \brief Computes the standard crosscorrelation between two 3D sequences using fft3d.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2009/01/23
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param in1_front_upper_left  A 3d input iterator (volume).
   ** \param in1_back_bottom_right  A 3d input iterator (volume).
   ** \param in2_front_upper_left  A 3d input iterator (mask).
   ** \param in2_back_bottom_right  A 3d input iterator (mask).
   ** \param out_front_upper_left  A 3d output iterator (result).
   ** \param out_back_bottom_right  A32d output iterator (result).
   ** \pre the value_type of the input ranges should be the same
   ** \pre the value_type of the input ranges have to be real (not complex)
   ** \pre (in1_back_bottom_right - in1_front_upper_left) == (out_back_bottom_right - out_front_upper_left)
   ** \pre (in2_back_bottom_right - in2_front_upper_left) == (out_back_bottom_right - out_front_upper_left)
   ** \par Example:
   ** \code
   **  slip::Matrix3d<double> Axcorr3(4,4,4);
   **  slip::iota(Axcorr3.begin(),Axcorr3.end(),1.0);
   **  slip::Matrix3d<double> Bxcorr3(4,4,4);
   **  slip::iota(Bxcorr3.begin(),Bxcorr3.end(),10.0);
   **  slip::Matrix3d<double> Cxcorr3(Axcorr3.slices(),Axcorr3.rows(),Axcorr3.cols());
   **  slip::fft_circular_crosscorrelation3d(Axcorr3.front_upper_left(),
   ** 				             Axcorr3.back_bottom_right(),
   ** 				             Bxcorr3.front_upper_left(),
   ** 				             Bxcorr3.back_bottom_right(),
   ** 				             Cxcorr3.front_upper_left(),
   ** 				             Cxcorr3.back_bottom_right());
   **  std::cout<<"fft_circular_crosscorrelation3d(Axcorr3,Bxcorr3,Cxcorr3) = \n "<<Cxcorr3<<std::endl;
   ** \endcode
   */   
  template<typename InputIterator3d1, 
	   typename InputIterator3d2, 
	   typename OutputIterator3d>
  inline
  void fft_circular_crosscorrelation3d(InputIterator3d1 in1_front_upper_left, 
				       InputIterator3d1 in1_back_bottom_right, 
				       InputIterator3d2 in2_front_upper_left, 
				       InputIterator3d2 in2_back_bottom_right, 
				       OutputIterator3d out_front_upper_left, 
				       OutputIterator3d out_back_bottom_right)
  {
    assert((in1_back_bottom_right - in1_front_upper_left) == (out_back_bottom_right - out_front_upper_left));
    assert((in2_back_bottom_right - in2_front_upper_left) == (out_back_bottom_right - out_front_upper_left));

    typename InputIterator3d1::difference_type size3din1 = in1_back_bottom_right - in1_front_upper_left;
    typename InputIterator3d2::difference_type size3din2 = in2_back_bottom_right - in2_front_upper_left;
    typename OutputIterator3d::difference_type size3dout = out_back_bottom_right - out_front_upper_left;
     typedef  typename slip::lin_alg_traits<typename std::iterator_traits<InputIterator3d1>::value_type>::value_type Real;

    slip::Array3d<std::complex<Real> > fft1(size3din1[0],size3din1[1],size3din1[2]);
    slip::Array3d<std::complex<Real> > fft2(size3din1[0],size3din1[1],size3din1[2]);

    slip::real_fft3d(in1_front_upper_left, in1_back_bottom_right, fft1.front_upper_left());
    slip::real_fft3d(in2_front_upper_left, in2_back_bottom_right, fft2.front_upper_left());
    
    for (std::size_t k=0;k<(size_t)size3din1[0];k++)
      for (std::size_t i=0;i<(size_t)size3din1[1];i++)
	for (std::size_t j=0;j<(size_t)size3din1[2];j++)
	{
	  fft1[k][i][j] *= std::conj(fft2[k][i][j]);
	}
    slip::ifft3d(fft1.front_upper_left(), fft1.back_bottom_right(), fft2.front_upper_left());
    slip::fftshift3d(fft2.front_upper_left(),fft2.back_bottom_right());
    std::transform(fft2.front_upper_left(),fft2.back_bottom_right(),out_front_upper_left,slip::un_real<std::complex<Real>,Real>()); 
  } 


/**
 ** \brief Computes the standard crosscorrelation between two 3D sequences using fft3d. 
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2009/09/17
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param in1_front_upper_left  A 3d input iterator (volume).
 ** \param in1_back_bottom_right  A 3d input iterator (volume).
 ** \param in2_front_upper_left  A 3d input iterator (mask).
 ** \param in2_back_bottom_right  A 3d input iterator (mask).
 ** \param out_front_upper_left  A 3d output iterator (result).
 ** \param out_back_bottom_right  A 3d output iterator (result).
 **
 ** \pre the value_type of the input sequences have to be real (not complex)
 ** \pre ((in1_back_bottom_right - in1_front_upper_left)[0]+(in2_back_bottom_right - in2_front_upper_left)[0] - 1) == (out_back_bottom_right - out_front_upper_left)[0]
 ** \pre ((in1_back_bottom_right - in1_front_upper_left)[1]+(in2_back_bottom_right - in2_front_upper_left)[1] - 1) == (out_back_bottom_right - out_front_upper_left)[1]
 ** \pre ((in1_back_bottom_right - in1_front_upper_left)[2]+(in2_back_bottom_right - in2_front_upper_left)[2] - 1) == (out_back_bottom_right - out_front_upper_left)[2]
 ** \par Example:
 ** \code
 ** slip::Matrix3d<double> Axcorr3d(4,8,10);
 ** slip::iota(Axcorr3d.begin(),Axcorr3d.end(),1.0f);
 ** slip::Matrix3d<double> Bxcorr3d(2,2,2);
 ** slip::iota(Bxcorr3d.begin(),Bxcorr3d.end(),10.0f);
 ** std::cout<<"Axcorr3d = \n"<<Axcorr3d<<std::endl;
 ** std::cout<<"Bxcorr3d = \n"<<Bxcorr3d<<std::endl;
 ** slip::Matrix3d<double> Cxcorr3d(Axcorr3d.slices()+Bxcorr3d.slices()-1,
 ** Axcorr3d.rows()+Bxcorr3d.rows()-1,
 ** Axcorr3d.cols()+Bxcorr3d.cols()-1);
 ** slip::fft_crosscorrelation3d(Axcorr3d.front_upper_left(),
 ** Axcorr3d.back_bottom_right(),
 ** Bxcorr3d.front_upper_left(),
 ** Bxcorr3d.back_bottom_right(),
 ** Cxcorr3d.front_upper_left(),
 ** Cxcorr3d.back_bottom_right());
 ** std::cout<<"Cxcorr3d = slip::fft_crosscorrelation3d(Axcorr3d,Bxcorr3d,Cxcorr3d) = \n"<<Cxcorr3d<<std::endl;
 ** \endcode
 */   
  template<typename InputIterator3d1, typename InputIterator3d2, typename OutputIterator3d>
  inline
  void 
  fft_crosscorrelation3d(InputIterator3d1 in1_front_upper_left, 
			  InputIterator3d1 in1_back_bottom_right, 
			  InputIterator3d2 in2_front_upper_left, 
			  InputIterator3d2 in2_back_bottom_right, 
			  OutputIterator3d out_front_upper_left, 
			  OutputIterator3d out_back_bottom_right)
  {
    assert(((in1_back_bottom_right - in1_front_upper_left)[0]+(in2_back_bottom_right - in2_front_upper_left)[0] - 1) == (out_back_bottom_right - out_front_upper_left)[0]);
    assert(((in1_back_bottom_right - in1_front_upper_left)[1]+(in2_back_bottom_right - in2_front_upper_left)[1] - 1) == (out_back_bottom_right - out_front_upper_left)[1]);
assert(((in1_back_bottom_right - in1_front_upper_left)[2]+(in2_back_bottom_right - in2_front_upper_left)[2] - 1) == (out_back_bottom_right - out_front_upper_left)[2]);
 
    typename InputIterator3d1::difference_type size3din1 = in1_back_bottom_right - in1_front_upper_left;
    typename InputIterator3d2::difference_type size3din2 = in2_back_bottom_right - in2_front_upper_left;
    typename OutputIterator3d::difference_type size3dout = out_back_bottom_right - out_front_upper_left;

     typedef  typename slip::lin_alg_traits<typename std::iterator_traits<InputIterator3d1>::value_type>::value_type Real;
 
    std::size_t slices =  size3din1[0] + size3din2[0] - 1; 
    std::size_t rows   =  size3din1[1] + size3din2[1] - 1; 
    std::size_t cols   =  size3din1[2] + size3din2[2] - 1; 

     //zero-padd Signal and Kernel data
    slip::Array3d<Real> Signal_zero_padded(slices,rows,cols,Real());
    slip::Array3d<Real> Kernel_zero_padded(slices,rows,cols,Real());
    slip::Box3d<int> signal_box(0,0,0,
				static_cast<int>(size3din1[0]-1),
				static_cast<int>(size3din1[1]-1),
				static_cast<int>(size3din1[2]-1));
    slip::Box3d<int> kernel_box(0,0,0,
				static_cast<int>(size3din2[0]-1),
				static_cast<int>(size3din2[1]-1),
				static_cast<int>(size3din2[2]-1));
    std::copy(in1_front_upper_left,
	      in1_back_bottom_right,
	      Signal_zero_padded.front_upper_left(signal_box));
    //rotate the kernel 
    typedef std::reverse_iterator<InputIterator3d2> reverse_iterator;
    std::copy(reverse_iterator(in2_back_bottom_right- slip::DPoint3d<int>(1,1,0)),
	      reverse_iterator(in2_front_upper_left),
	      Kernel_zero_padded.front_upper_left(kernel_box));


    slip::Array3d<std::complex<Real> > fft1(slices,rows,cols,std::complex<Real>());
    slip::Array3d<std::complex<Real> > fft2(slices,rows,cols,std::complex<Real>());
    //computes the two fft
    slip::real_fft3d(Signal_zero_padded.front_upper_left(),
		     Signal_zero_padded.back_bottom_right(),
		     fft1.front_upper_left());
    slip::real_fft3d(Kernel_zero_padded.front_upper_left(),
		     Kernel_zero_padded.back_bottom_right(),
		     fft2.front_upper_left());
    //computes fft1*fft2
    slip::multiplies(fft1.begin(),fft1.end(),fft2.begin(),fft1.begin());
   
    //ifft3d
    slip::ifft3d(fft1.front_upper_left(), fft1.back_bottom_right(), 
		 fft2.front_upper_left());
    //fftshift3d
    slip::fftshift3d(fft2.front_upper_left(),fft2.back_bottom_right());
    //the result should be real
    std::transform(fft2.front_upper_left(),fft2.back_bottom_right(),
		   out_front_upper_left,
		   slip::un_real<std::complex<Real>,Real>()); 
  } 


/**
 ** \brief Computes the standard crosscorrelation between two 3D sequences using fft3d. 
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2009/09/17
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param in1_front_upper_left  A 3d input iterator (volume).
 ** \param in1_back_bottom_right  A 3d input iterator (volume).
 ** \param in2_front_upper_left  A 3d input iterator (mask).
 ** \param in2_back_bottom_right  A 3d input iterator (mask).
 ** \param out_front_upper_left  A 3d output iterator (result).
 ** \param out_back_bottom_right  A32d output iterator (result).
 **
 ** \pre the value_type of the input sequences have to be real (not complex)
 ** \pre (in1_back_bottom_right - in1_front_upper_left) == (out_back_bottom_right - out_front_upper_left)
 ** \par Example:
 ** \code
 ** slip::Matrix3d<double> Axcorr3d(4,8,10);
 **  slip::iota(Axcorr3d.begin(),Axcorr3d.end(),1.0f);
 **  slip::Matrix3d<double> Bxcorr3d(2,2,2);
 **  slip::iota(Bxcorr3d.begin(),Bxcorr3d.end(),10.0f);
 **  std::cout<<"Axcorr3d = \n"<<Axcorr3d<<std::endl;
 **  std::cout<<"Bxcorr3d = \n"<<Bxcorr3d<<std::endl;
 **  slip::Matrix3d<double> Cxcorr3d_s(Axcorr3d.slices(),
 **				       Axcorr3d.rows(),
 **			               Axcorr3d.cols());
 **  slip::fft_crosscorrelation3d_same(Axcorr3d.front_upper_left(),
 **			               Axcorr3d.back_bottom_right(),
 **			               Bxcorr3d.front_upper_left(),
 **			               Bxcorr3d.back_bottom_right(),
 **			               Cxcorr3d_s.front_upper_left(),
 **			               Cxcorr3d_s.back_bottom_right());
 ** std::cout<<"Cxcorr3d_s = slip::fft_crosscorrelation3d_same(Axcorr3d,Bxcorr3d,Cxcorr3d_s) = \n"<<Cxcorr3d<<std::endl;
   
 ** \endcode
 */   
 template<typename InputIterator3d1, 
	  typename InputIterator3d2, 
	  typename OutputIterator3d>
  inline
  void 
  fft_crosscorrelation3d_same(InputIterator3d1 in1_front_upper_left, 
			      InputIterator3d1 in1_back_bottom_right, 
			      InputIterator3d2 in2_front_upper_left, 
			      InputIterator3d2 in2_back_bottom_right, 
			      OutputIterator3d out_front_upper_left, 
			      OutputIterator3d out_back_bottom_right)
  {
    assert(   (in1_back_bottom_right - in1_front_upper_left) 
	   == (out_back_bottom_right - out_front_upper_left));

   typename InputIterator3d1::difference_type size3din1 = in1_back_bottom_right - in1_front_upper_left;
    typename InputIterator3d2::difference_type size3din2 = in2_back_bottom_right - in2_front_upper_left;
    typename OutputIterator3d::difference_type size3dout = out_back_bottom_right - out_front_upper_left;

     typedef  typename slip::lin_alg_traits<typename std::iterator_traits<InputIterator3d1>::value_type>::value_type Real;
 
    std::size_t slices =  size3din1[0] + size3din2[0] - 1; 
    std::size_t rows   =  size3din1[1] + size3din2[1] - 1; 
    std::size_t cols   =  size3din1[2] + size3din2[2] - 1; 

     //zero-padd Signal and Kernel data
    slip::Array3d<Real> Signal_zero_padded(slices,rows,cols,Real());
    slip::Array3d<Real> Kernel_zero_padded(slices,rows,cols,Real());
    slip::Box3d<int> signal_box(0,0,0,
				static_cast<int>(size3din1[0]-1),
				static_cast<int>(size3din1[1]-1),
				static_cast<int>(size3din1[2]-1));
    slip::Box3d<int> kernel_box(0,0,0,
				static_cast<int>(size3din2[0]-1),
				static_cast<int>(size3din2[1]-1),
				static_cast<int>(size3din2[2]-1));
    std::copy(in1_front_upper_left,
	      in1_back_bottom_right,
	      Signal_zero_padded.front_upper_left(signal_box));
    //rotate the kernel 
    typedef std::reverse_iterator<InputIterator3d2> reverse_iterator;
    std::copy(reverse_iterator(in2_back_bottom_right- slip::DPoint3d<int>(1,1,0)),
	      reverse_iterator(in2_front_upper_left),
	      Kernel_zero_padded.front_upper_left(kernel_box));


    slip::Array3d<std::complex<Real> > fft1(slices,rows,cols,std::complex<Real>());
    slip::Array3d<std::complex<Real> > fft2(slices,rows,cols,std::complex<Real>());
    //computes the two fft
     slip::real_fft3d(Signal_zero_padded.front_upper_left(),
		     Signal_zero_padded.back_bottom_right(),
		     fft1.front_upper_left());
    slip::real_fft3d(Kernel_zero_padded.front_upper_left(),
		     Kernel_zero_padded.back_bottom_right(),
		     fft2.front_upper_left());
   
    //computes fft1*fft2
    slip::multiplies(fft1.begin(),fft1.end(),fft2.begin(),fft1.begin());
   
    //ifft3d
    slip::ifft3d(fft1.front_upper_left(), fft1.back_bottom_right(), 
		 fft2.front_upper_left());
    //fftshift3d
    slip::fftshift3d(fft2.front_upper_left(),fft2.back_bottom_right());
    //the result should be real
    slip::Box3d<int> out_box(size3din2[0]/2,size3din2[1]/2,size3din2[2]/2,
			     static_cast<int>(size3din2[0]/2 + size3din1[0] - 1),
			     static_cast<int>(size3din2[1]/2 + size3din1[1] - 1),
			     static_cast<int>(size3din2[2]/2 + size3din1[2] - 1));
    
    std::transform(fft2.front_upper_left(out_box),
		   fft2.back_bottom_right(out_box),
		   out_front_upper_left,slip::un_real<std::complex<Real>,Real>());  
        
  }

  /* @} */

}//slip::

#endif //SLIP_CORRELATION_HPP
