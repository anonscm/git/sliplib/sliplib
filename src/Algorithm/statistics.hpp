/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file statistics.hpp
 * 
 * \brief Provides some statistics algorithms.
 * 
 */
#ifndef SLIP_STATISTICS_HPP
#define SLIP_STATISTICS_HPP

#include <iostream>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cassert>
#include <cmath>
#include <set>
#include <vector>
#include "Array.hpp"
#include "macros.hpp"
#include "iterator_types.hpp"
#include "Block.hpp"

namespace slip
{


/*! \struct Statistics
** 
** \brief This is a structure to store descriptive statistics.
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.1
** \date 2009/11/25
** \since 1.0.0
** \param T Type of the elements in the %Statistics
**
*/

template <typename T>
struct Statistics
{
  /*!
  ** \brief Returns the minimum value.
  ** 
  */

  T& min() 
  { 
    return stat_[0];
  }
 
  /*!
  ** \brief Returns the first quartile value.
  ** 
  */
  T& first_quartile()
  {
    return stat_[1];
  }

  /*!
  ** \brief Returns the median value.
  ** 
  */
  T& median()
  {
    return stat_[2];
  }

  /*!
  ** \brief Returns the third quartile value.
  ** 
  */
  T& third_quartile()
  {
    return stat_[3];
  }

  /*!
  ** \brief Returns the first maximal value.
  ** 
  */
  T& max() 
  { 
    return stat_[4];
  }

  /*!
  ** \brief Returns the mean value.
  ** 
  */
  T& mean()
  { 
    return stat_[5];
  }

  /*!
  ** \brief Returns the standard deviation.
  ** 
  */
  T& std_dev()
  { 
    return stat_[6];
  }

  /*!
  ** \brief Returns the skewness.
  ** 
  */
  T& skewness()
  { 
    return stat_[7];
  }

  /*!
  ** \brief Returns the kurtosis.
  ** 
  */
  T& kurtosis()
  {
    return stat_[8];
  }

  /*!
  ** \brief Returns the cardinal.
  ** 
  */
  T& cardinal()
  {
    return stat_[9];
  }
  
  /*!
  ** \brief Returns a slip::block<T,10> with all the statistics.
  ** 
  */
  slip::block<T,10> all()
  {
    return stat_;
  }

  slip::block<T,10> stat_;
};  

  template<typename Integer, typename>
  struct __cardinal
  {
    template <typename _II>
    static Integer
    cardinal(_II first, _II last)
    {
      return static_cast<Integer>(last-first);
    
    }
  };

  template<typename Integer>
  struct __cardinal<Integer,std::random_access_iterator_tag>
  {
    template <typename _II>
    static Integer
    cardinal(_II first, _II last)
    {
      return static_cast<Integer>(last-first);
    }
  };

  template<typename Integer>
  struct __cardinal<Integer,std::random_access_iterator2d_tag>
  {
    template <typename _II>
    static Integer
    cardinal(_II first, _II last)
    {
      Integer count = 0;
      while(first != last)
      {
	count++;
	first++;
      }
      return count;

    }
  };


   template<typename Integer>
  struct __cardinal<Integer,std::random_access_iterator3d_tag>
  {
    template <typename _II>
    static Integer
    cardinal(_II first, _II last)
    {
      return __cardinal<Integer,std::random_access_iterator2d_tag>::cardinal(first,last);
    }
  };

  /**
   ** \brief Computes the cardinal or the number of elements of a range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \return The number of elements of the container.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** std::cout<<"cardinal = "<<slip::cardinal<int>(M.begin(),M.end())<<std::endl;
   ** \endcode
   */
  template<typename Integer,typename InputIterator>
  inline
  Integer cardinal(InputIterator first, 
		   InputIterator last)
  {
    typedef typename std::iterator_traits<InputIterator>::iterator_category _Category;
  
    return __cardinal<Integer,_Category>::cardinal(first,last);
  }

  /**
   ** \brief Computes the mean value of a range
   ** \f[ \frac{1}{n}\sum_i x_i\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator. 
   ** \return The mean value of the container.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** std::cout<<slip::mean<float>(M.begin(),M.end())<<std::endl;
   ** \endcode
   */
  template<typename Value_T,typename InputIterator>
  inline
  Value_T mean(InputIterator first, 
	       InputIterator last)
  {
    assert(first != last);
    Value_T sum = Value_T(0);
    long int count = 0;
    while(first != last)
      {
	sum += *first++;
	++count;
      }
    return Value_T(sum) / Value_T(count);
  }

  /**
   ** \brief Computes the weighted mean value of a range: 
   ** \f[\frac{\sum_i w_ix_i}{\sum_i w_i}\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/04/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator. 
   ** \param w_first An InputIterator to the weight container.
   ** \param w_last An InputIterator to the weight container.
   ** \return The weighted mean value of the container.
   **
   ** \pre [first,last) must be valid.
   ** \pre The two container must have the same size.
   ** \par Example:
   ** \code
   ** slip::block<float,3> weight = {1.0,2.0,1.0};
   ** slip::block<float,3> values = {5.0,4.0,12.0};
   ** std::cout<<slip::weighted_mean<float>(values.begin(),value.end(),weight.begin(),weight.end())<<std::endl;
   ** \endcode
   */
  template<typename Value_T,typename InputIterator>
  inline
  Value_T weighted_mean(InputIterator first, 
			InputIterator last,
			InputIterator w_first,
			InputIterator w_last)
  {
    assert(first != last);
    assert(slip::cardinal<std::size_t>(first,last) == 
	   slip::cardinal<std::size_t>(w_first,w_last));

    Value_T proj = Value_T(0);
    Value_T sum  = Value_T(0);
    for (; first != last; ++first, ++w_first)
       {
	 proj = proj + (*first) * (*w_first);
	 sum  = sum  + *w_first;
       }
    return Value_T(proj) / Value_T(sum);
  }

 

 

  
  /**
   ** \brief Computes the nth moment of a range
   ** \f[\frac{1}{n}\sum_i (x_i-\overline{x})^N\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the container itself.
   ** \return The value of the nth moment.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** double mean = slip::mean<double>(M.begin(),M.end());
   ** std::cout<<slip::nth_moment(M.begin(),M.end(),mean)<<std::endl;
   ** \endcode
   */
  template<typename T, int N, typename InputIterator>
  inline
  T nth_moment(InputIterator first, InputIterator last, T mean)
  {
    assert(last != first);
    T __init = T();
    int count = 0;
     for (; first != last; ++first)
       {
	__init = __init + slip::nth_power<N>(*first - mean);
	count++;
       }
     return __init / count;
  }

 
   /**
   ** \brief Computes the covariance of a two sequences
   ** \f[\frac{1}{n}\sum_i (x_i-\overline{x})(y_i-\overline{y})\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first  An InputIterator.
   ** \param last   An InputIterator.
   ** \param first2 An InputIterator.
   ** \param mean1  The mean of the first container.
   ** \param mean2  The mean of the second container.
   ** \return The value of the covariance of the two containers.
   **
   ** \pre [first,last) must be valid.
   ** \pre The two conainers must have the same sizes.
   **
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M1(4,5);
   ** slip::iota(M1.begin(),M1.end(),1,1);
   ** double mean1 = slip::mean<double>(M1.begin(),M1.end());
   ** slip::Array2d<int> M2(4,5);
   ** slip::iota(M2.begin(),M2.end(),4,1);
   ** double mean2 = slip::mean<double>(M2.begin(),M2.end());
   ** std::cout<<slip::covariance<float>(M1.begin(),M1.end(),M2.begin(),mean1,mean2)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename InputIterator2>
  inline
  T covariance(InputIterator first, InputIterator last, 
	       InputIterator2 first2, 
	       T mean1, T mean2)
  {
    assert(last != first);
    T __init = T();
    long int count = 0;
     for (; first != last; ++first, ++first2)
       {
	__init = __init + (*first - mean1) * (*first2 - mean2);
	count++;
       }
     return __init / count;
  }

  /**
   ** \brief Computes the unbiased covariance of a two sequences
   ** \f[\frac{1}{n-1}\sum_i (x_i-\overline{x})(y_i-\overline{y})\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first  An InputIterator.
   ** \param last   An InputIterator.
   ** \param first2 An InputIterator.
   ** \param mean1  The mean of the first container.
   ** \param mean2  The mean of the second container.
   ** \return The value of the covariance of the two containers.
   **
   ** \pre [first,last) must be valid.
   ** \pre The two conainers must have the same sizes.
   **
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M1(4,5);
   ** slip::iota(M1.begin(),M1.end(),1,1);
   ** double mean1 = slip::mean<double>(M1.begin(),M1.end());
   ** slip::Array2d<int> M2(4,5);
   ** slip::iota(M2.begin(),M2.end(),4,1);
   ** double mean2 = slip::mean<double>(M2.begin(),M2.end());
   ** std::cout<<slip::unbiased_covariance<float>(M1.begin(),M1.end(),M2.begin(),mean1,mean2)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename InputIterator2>
  inline
  T unbiased_covariance(InputIterator first, InputIterator last, 
			InputIterator2 first2, 
			T mean1, T mean2)
  {
    assert(last != first);
    T __init = T();
    long int count = 0;
     for (; first != last; ++first, ++first2)
       {
	__init = __init + (*first - mean1) * (*first2 - mean2);
	count++;
       }
     T result = T(); 
     if(count != 1)
       {
	 result = __init / (count - 1);
       }
   
     return result;
  }
  /**
   **
   ** \brief Computes the variance of a range
   ** \f[\frac{1}{n}\sum_i (x_i-\overline{x})^2\f]
  
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the container itself.
   ** \return The value of the variance of the container.
   **
   ** \note Calls slip::covariance(first,last,first,mean,mean).
   **
   ** \pre [first,last) must be valid.
    ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** double mean = slip::mean<double>(M.begin(),M.end());
   ** std::cout<<slip::variance(M.begin(),M.end(),mean)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator>
  inline
  T variance(InputIterator first, InputIterator last, T mean)
  {
    return slip::covariance(first,last,first,mean,mean);
  }

  /**
   **
   ** \brief Computes the unbiased variance of a range
   ** \f[\frac{1}{n-1}\sum_i (x_i-\overline{x})^2\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the container itself.
   ** \return The value of the variance of the container.
   **
   ** \note Calls slip::unbiased_covariance(first,last,first,mean,mean).
   **
   ** \pre [first,last) must be valid.
    ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** double mean = slip::mean<double>(M.begin(),M.end());
   ** std::cout<<slip::unbiased_variance(M.begin(),M.end(),mean)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator>
  inline
  T unbiased_variance(InputIterator first, InputIterator last, T mean)
  {
    return slip::unbiased_covariance(first,last,first,mean,mean);
  }

  /**
   ** \brief Computes the standard deviation of a range
   ** \f[\sqrt{\frac{1}{n}\sum_i (x_i-\overline{x})^2}\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the container itself.
   ** \return The value of standard deviation of the container.
   **
   ** \note Calls std::sqrt(slip::variance(first,last,first,mean)).
   **
   ** \pre [first,last) must be valid.
    ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** double mean = slip::mean<float>(M.begin(),M.end());
   ** std::cout<<slip::std_dev(M.begin(),M.end(),mean)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator>
  inline
  T std_dev(InputIterator first, InputIterator last, T mean)
  {
    return std::sqrt(slip::variance(first,last,mean));
  }

   /**
   ** \brief Computes the unbiased standard deviation of a range
   ** \f[\sqrt{\frac{1}{n-1}\sum_i (x_i-\overline{x})^2}\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the container itself.
   ** \return The value of standard deviation of the container.
   **
   ** \note Calls std::sqrt(slip::unbiased_variance(first,last,first,mean)).
   **
   ** \pre [first,last) must be valid.
    ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** double mean = slip::mean<float>(M.begin(),M.end());
   ** std::cout<<slip::unbiased_std_dev(M.begin(),M.end(),mean)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator>
  inline
  T unbiased_std_dev(InputIterator first, InputIterator last, T mean)
  {
    return std::sqrt(slip::unbiased_variance(first,last,mean));
  }

 
  /**
   ** \brief Computes the kurtosis of a range
   ** \f[\frac{n \sum_i (x_i-\overline{x})^4}{\left(\sum_i (x_i-\overline{x})^2\right)^2}\f]
   ** A high kurtosis distribution has a sharper peak and longer, 
   ** fatter tails, while a low kurtosis distribution has a more rounded 
   ** peak and shorter thinner tails. Here are some common value of kurtosis
   ** for unimodal and symmetric densities:
   ** \li Laplace distribution: 6
   ** \li Hyperbolic secant distribution: 5
   ** \li Logistic distribution: 4.2
   ** \li Normal distribution: 3
   ** \li Raised cosine distribution: 2.406258...
   ** \li Wigner semi circled distribution: 2
   ** \li Uniform distribution: 1.8
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the container itself.
   ** \return The value of the kurtosis of the container.
   **
   ** \pre [first,last) must be valid.
    ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** double mean = slip::mean<double>(M.begin(),M.end());
   ** std::cout<<slip::kurtosis(M.begin(),M.end(),mean)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator>
  inline
  T kurtosis(InputIterator first, InputIterator last, T mean)
  {
    assert(first != last);
    T __init  = T(0);
    T __init2 = T(0);
    long int count = 0;
     for (; first != last; ++first)
       {
	 T p2 = slip::nth_power<2>(*first - mean);
	 __init  = __init  + p2;
	 __init2 = __init2 + (p2 * p2);
	 count++;
       }
     T result = T(0);
     if(__init != T(0))
       {
	 result = (__init2 * T(count)) / (__init * __init);
       }
     return result;
  }
 

  /**
   ** \brief Computes the unbiased kurtosis of a range
   ** \f[\frac{(n+1)n(n-1)}{(n-2)(n-3)}\frac{\sum_i (x_i-\overline{x})^4}{\left[\sum_i (x_i-\overline{x})^2\right]^2}- \frac{3(n-1)^2}{(n-2)(n-3)} + 3\f]
   ** A high kurtosis distribution has a sharper peak and longer, 
   ** fatter tails, while a low kurtosis distribution has a more rounded 
   ** peak and shorter thinner tails.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the container itself.
   ** \return The value of the kurtosis of the container.
   **
   ** \pre [first,last) must be valid.
   ** \pre slip::cardinal<int>(last-first) > 3
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** double mean = slip::mean<double>(M.begin(),M.end());
   ** std::cout<<slip::unbiased_kurtosis(M.begin(),M.end(),mean)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator>
  inline
  T unbiased_kurtosis(InputIterator first, InputIterator last, T mean)
  {
    assert(slip::cardinal<int>(first,last) > 3);
    T __init  = T(0);
    T __init2 = T(0);
    long int count = 0;
    for (; first != last; ++first)
       {
	 T p2 = slip::nth_power<2>(*first - mean);
	 __init  = __init  + p2;
	 __init2 = __init2 + (p2 * p2);
	 count++;
       }
    T count_m1 = T(count - 1);
    T count_m2 = T(count - 2);
    T count_m3 = T(count - 3);
  
    T result = T(0);
    if(__init != T(0))
      {
    
	result = (((T(count+1)*T(count)* count_m1) * __init2 ) 
		  / ( (count_m2 * count_m3) * (__init * __init) ))
	  - ( (T(3)* (count_m1*count_m1)) / (count_m2 * count_m3)) + T(3);
      }
  return result;
  }

  /**
   ** \brief Computes the skewness of a range
   ** \f[\frac{\frac{1}{n}\sum_i (x_i-\overline{x})^3}{\left(\sqrt{\frac{1}{n}\sum_i (x_i-\overline{x})^2}\right)^3}\f]
   ** skewness is a measure of the asymmetry of the probability distribution of a real-valued random variable
   ** \li negative skew: The left tail is longer; the mass of the distribution is concentrated on the right
   ** \li positive skew: The right tail is longer; the mass of the distribution is concentrated on the left
   ** \li 0 skew: the distribution is symmetric (Normal distribution for example)
   ** \author Mouhamed Hammoud <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/09/09
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param std_dev  The standard deviation of the container itself.
   ** \return The value of the skewness of the container.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** double mean = slip::mean<double>(M.begin(),M.end());
   ** std::cout<<slip::skewness(M.begin(),M.end(),mean)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator>
  inline
  T skewness(InputIterator first, InputIterator last, T mean)
  {
    assert(first != last);
    T __init  = T();
    T __init2 = T();
    long int count = 0;
     for (; first != last; ++first)
       {
	 T p1 = (*first - mean);
	 T p2 =  p1 * p1;
	 __init  = __init  + p2;
	 __init2 = __init2 + (p2 * p1);
	 count++;
       }
     T result = T(0);
     if(__init != T(0))
       {
	 result = __init2 / (T(count)*slip::nth_power<3>(std::sqrt(__init/T(count))));
       }
     return result;
  }

  /**
   ** \brief Computes the unbiased skewness of a range
   ** \f[\frac{n}{(n-1)(n-2)}\frac{\sum_i (x_i-\overline{x})^3}{\left(\sqrt{\frac{1}{n-1}\sum_i (x_i-\overline{x})^2}\right)^3}\f]
   ** skewness is a measure of the asymmetry of the probability distribution of a real-valued random variable
   ** \li negative skew: The left tail is longer; the mass of the distribution is concentrated on the right
   ** \li positive skew: The right tail is longer; the mass of the distribution is concentrated on the left
   ** \li 0 skew: the distribution is symmetric (Normal distribution for example)
   ** \author Mouhamed Hammoud <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/09/09
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param std_dev  The standard deviation of the container itself.
   ** \return The value of the skewness of the container.
   **
   ** \pre [first,last) must be valid.
   ** \pre (last-first) > 2
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** double mean = slip::mean<double>(M.begin(),M.end());
   ** std::cout<<slip::skewness(M.begin(),M.end(),mean)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator>
  inline
  T unbiased_skewness(InputIterator first, InputIterator last, T mean)
  {
    assert(slip::cardinal<int>(first,last) > 2);
    T __init  = T();
    T __init2 = T();
    long int count = 0;
     for (; first != last; ++first)
       {
	 T p1 = (*first - mean);
	 T p2 =  p1 * p1;
	 __init  = __init  + p2;
	 __init2 = __init2 + (p2 * p1);
	 count++;
       }
    T result = T(0);
    if(__init != T(0))
      {
	result = (__init2 * T(count)) / (T(count-1)*T(count-2)*slip::nth_power<3>(std::sqrt(__init/T(count-1))));
      }
    return result;
}
 
 
  /**
   ** \brief Computes the root mean square (rms) value of a range
   ** \f[ \sqrt{\frac{1}{n}\sum_i x_i^2}\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator. 
   ** \return The rms value of the container.
   **
   ** \pre [first,last) must be valid.
   **
   ** \note Calls std::sqrt(slip::covariance(first,last,first,T(0),T(0)));
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** std::cout<<slip::rms<double>(M.begin(),M.end())<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator>
  inline
  T rms(InputIterator first, InputIterator last)
  {
    return std::sqrt(slip::covariance(first,last,first,T(0),T(0)));
  }


  /**
   ** \brief Computes the median value from a sorted range:
   ** returns *(first + (last - first) / 2)
   ** \li (last - first) is even 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \return The median value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \pre The range must be sorted.
   ** \pre (last - first) must return the number of element in the container
   */
  template<typename InputIterator>
  inline
  typename std::iterator_traits<InputIterator>::value_type
  median_from_sorted_data(InputIterator first, InputIterator last)
  {
    return *(first + (last - first) / 2); 
  }
  

  /**
   ** \brief Computes the median value from a sorted range:
   ** returns *(first + n/2).
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param n The number of element in the container.
   ** \return The median value of the range.
   **
   ** \pre The range must be sorted.
   ** \pre The iterator must have the opertor+(std::size_t n) defined
   */
  template<typename RandomAccessIterator, typename Size>
  inline
  typename std::iterator_traits<RandomAccessIterator>::value_type
  median_from_sorted_data_n(RandomAccessIterator first, Size n)
  {
    return *(first + n/2); 
  }

  

 

   /**
   ** \brief Computes the median value from a non sorted range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \param n The number of element in the range.
   ** \return The median value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M2(3,3);
   ** std::generate(M2.begin(),M2.end(),std::rand);
   **  std::cout<<slip::median_from_data_n(M2.begin(),M2.end(),9)<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator, typename Size>
  inline
  typename std::iterator_traits<RandomAccessIterator>::value_type
  median_from_data_n(RandomAccessIterator first, RandomAccessIterator last, 
		     Size n)
  {
    assert(last != first);
    slip::Array<typename std::iterator_traits<RandomAccessIterator>::value_type> 
      tmp(n,first,last);
    std::sort(tmp.begin(),tmp.end());
    return slip::median_from_sorted_data_n(tmp.begin(),n); 
  }

  /**
   ** \brief Computes the median value from a non sorted range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \param n The number of element in the range.
   ** \param comp is a model of StrictWeakOrdering. 
   ** \return The median value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M2(3,3);
   ** std::generate(M2.begin(),M2.end(),std::rand);
   **  std::cout<<slip::median_from_data_n(M2.begin(),M2.end(),9,std::less<float>())<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator, 
	   typename Size, 
	   typename StrictWeakOrdering>
  inline
  typename std::iterator_traits<RandomAccessIterator>::value_type
  median_from_data_n(RandomAccessIterator first, RandomAccessIterator last, 
		     Size n, 
		     StrictWeakOrdering comp)
  {
    assert(last != first);
    slip::Array<typename std::iterator_traits<RandomAccessIterator>::value_type> 
      tmp(n,first,last);
    std::sort(tmp.begin(),tmp.end(),comp);
    return slip::median_from_sorted_data_n(tmp.begin(),n); 
  }


  template<typename>
  struct __median
  {
    template <typename _II>
    static typename std::iterator_traits<_II>::value_type
    median(_II first, _II last)
    {
      return slip::median_from_data_n(first,last,slip::cardinal<int>(first,last));
    }
    template <typename _II, typename StrictWeakOrdering>
    static typename std::iterator_traits<_II>::value_type
    median(_II first, _II last, StrictWeakOrdering comp)
    {
      return slip::median_from_data_n(first,last,slip::cardinal<int>(first,last),comp);
    }
    template <typename _II,typename SizeType>
    static typename std::iterator_traits<_II>::value_type
    median_n(_II first, _II last, SizeType n)
    {
      return slip::median_from_data_n(first,last,n);
    }
    template <typename _II,typename SizeType, typename StrictWeakOrdering>
    static typename std::iterator_traits<_II>::value_type
    median_n(_II first, _II last, SizeType n, StrictWeakOrdering comp)
    {
      return slip::median_from_data_n(first,last,n,comp);
    }
  };

 
  template<>
  struct __median<std::random_access_iterator_tag>
  {
    template <typename _II>
    static typename std::iterator_traits<_II>::value_type
    median(_II first, _II last)
    {
      return slip::median_from_data_n(first,last,(last-first));
    }
    template <typename _II, typename StrictWeakOrdering>
    static typename std::iterator_traits<_II>::value_type
    median(_II first, _II last, StrictWeakOrdering comp)
    {
      return slip::median_from_data_n(first,last,(last-first),comp);
    }
     template <typename _II,typename SizeType>
    static typename std::iterator_traits<_II>::value_type
    median_n(_II first, _II last, SizeType n)
    {
      return slip::median_from_data_n(first,last,n);
    }

    template <typename _II,typename SizeType, typename StrictWeakOrdering>
    static typename std::iterator_traits<_II>::value_type
    median_n(_II first, _II last, SizeType n, StrictWeakOrdering comp)
    {
      return slip::median_from_data_n(first,last,n,comp);
    }
  };
  
  /**
   ** \brief Computes the median value from a range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/11/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \return The median value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M2(3,3);
   ** std::generate(M2.begin(),M2.end(),std::rand);
   **  std::cout<<slip::median(M2.begin(),M2.end())<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator>
  inline
  typename std::iterator_traits<RandomAccessIterator>::value_type
  median(RandomAccessIterator first, RandomAccessIterator last)
  {
     typedef typename std::iterator_traits<RandomAccessIterator>::iterator_category _Category;
  
     return __median<_Category>::median(first,last);
  }


   /**
   ** \brief Computes the median value from a range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/11/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \param comp is a model of StrictWeakOrdering. 
   ** \return The median value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M2(3,3);
   ** std::generate(M2.begin(),M2.end(),std::rand);
   **  std::cout<<slip::median(M2.begin(),M2.end(),std::less<float>())<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator, typename StrictWeakOrdering>
  inline
  typename std::iterator_traits<RandomAccessIterator>::value_type
  median(RandomAccessIterator first, RandomAccessIterator last, StrictWeakOrdering comp)
  {
     typedef typename std::iterator_traits<RandomAccessIterator>::iterator_category _Category;
  
     return __median<_Category>::median(first,last,comp);
  }


  /**
   ** \brief Computes the median value from a range of size n.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/11/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \param n Size of the range.
   ** \return The median value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M2(3,3);
   ** std::generate(M2.begin(),M2.end(),std::rand);
   **  std::cout<<slip::median_n(M2.begin(),M2.end(),9)<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator, typename SizeType>
  inline
  typename std::iterator_traits<RandomAccessIterator>::value_type
  median_n(RandomAccessIterator first, RandomAccessIterator last, SizeType n)
  {
     typedef typename std::iterator_traits<RandomAccessIterator>::iterator_category _Category;
  
     return __median<_Category>::median_n(first,last,n);
  }
  
   /**
   ** \brief Computes the median value from a range of size n.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/11/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \param n Size of the range.
   ** \param comp is a model of StrictWeakOrdering. 
   ** \return The median value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M2(3,3);
   ** std::generate(M2.begin(),M2.end(),std::rand);
   **  std::cout<<slip::median_n(M2.begin(),M2.end(),9,std::less<float>())<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator, typename SizeType, typename StrictWeakOrdering>
  inline
  typename std::iterator_traits<RandomAccessIterator>::value_type
  median_n(RandomAccessIterator first, RandomAccessIterator last, SizeType n, StrictWeakOrdering comp)
  {
     typedef typename std::iterator_traits<RandomAccessIterator>::iterator_category _Category;
  
     return __median<_Category>::median_n(first,last,n,comp);
  }
  

 /**
   ** \brief Computes the first quartile value from a non sorted range.
   ** \author Mouhamed Hammoud <hammoud_AT_sic.univ-poitiers.fr>
   ** \since 1.0.0
   ** \date 2009/08/26
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
    ** \return The first quartile value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M2(3,3);
   ** std::generate(M2.begin(),M2.end(),std::rand);
   **  std::cout<<slip::first_quartile(M2.begin(),M2.end())<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator>
  inline
  typename std::iterator_traits<RandomAccessIterator>::value_type
  first_quartile(RandomAccessIterator first, RandomAccessIterator last)
  {
    assert(last != first);
    std::size_t n = slip::cardinal<std::size_t>(first,last);
    slip::Array<typename std::iterator_traits<RandomAccessIterator>::value_type> 
      tmp(n,first,last);
    std::sort(tmp.begin(),tmp.end());
    return *(tmp.begin()+n/4);
  }

   /**
   ** \brief Computes the third quartile value from a non sorted range.
   ** \author Mouhamed Hammoud <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2009/08/26
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
    ** \return The third quartile value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** slip::Array2d<float> M2(3,3);
   ** std::generate(M2.begin(),M2.end(),std::rand);
   **  std::cout<<slip::third_quartile(M2.begin(),M2.end())<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator>
  inline
  typename std::iterator_traits<RandomAccessIterator>::value_type
  third_quartile(RandomAccessIterator first, RandomAccessIterator last)
  {
    assert(last != first);
    std::size_t n=slip::cardinal<std::size_t>(first,last);
    slip::Array<typename std::iterator_traits<RandomAccessIterator>::value_type> 
      tmp(n,first,last);
    std::sort(tmp.begin(),tmp.end());
    return *((tmp.end()-1)-n/4);
  }
  


  /*!
  ** \brief finds the \a n largest elements in the range [first, last).
  ** \since 1.0.0
  ** \param first InputIterator to the first element of the range.
  ** \param last InputIterator to one past-the-end element of the range.
  ** \param max std::vector which contains the first iterators i in [first, last) such that the *i are the greatest one in [first, last). The elements are ordered by decreasing order.
  ** \param n Number of largest elements to find. Default value is 1.
  ** \pre n <= slip::cardinal<std::size_t>(first,last)
  ** \pre n == max.size()
  ** \pre n > 0
  ** \remarks Call std::max_element(first,last) if n = 1.
  ** \par Example:
  ** \code
  **  int tab[] = {12,6,6,9,10,1,5,0,-1};
  **  std::vector<int*> max(2);
  **  slip::n_max_elements(tab,tab+9,max,2);
  **  std::cout<<"---------------------"<<std::endl;
  ** for(std::size_t i = 0; i < max.size();++i)
  **  {
  **    std::cout<<"max["<<i<<"] = "<<*(max[i])<<" ";
  **  }
  ** std::cout<<std::endl;
  ** std::cout<<"---------------------"<<std::endl;
  ** \endcode
  */ 
  template<typename ForwardIterator>
  void n_max_elements(ForwardIterator first, ForwardIterator last,
		      std::vector<ForwardIterator>& max, 
		      const std::size_t n = 1)
  {
    
    assert(n <= slip::cardinal<std::size_t>(first,last));
    assert(max.size() == n);
    assert(n > 0);
    if(n == 1)
      {
	max[0] = std::max_element(first,last);
      }
    else
      {
	std::multiset<ForwardIterator,slip::gt_it<ForwardIterator> > S;
	
	while(first != last)
	  {
	    S.insert(first);
	    first++;
	  }
	
	typename std::multiset<ForwardIterator,gt_it<ForwardIterator> >::iterator it = S.begin();
	for(std::size_t i = 0; i < n; ++i)
	  {
	    max[i] = *it++;
	  }
	
      }
}


  /*!
  ** \brief finds the \a n smallest elements in the range [first, last).
  ** \since 1.0.0
  ** \param first InputIterator to the first element of the range.
  ** \param last InputIterator to one past-the-end element of the range.
  ** \param min std::vector which contains the first iterators i in [first, last) such that the *i are the smallest one in [first, last). The elements are ordered by increasing order.
  ** \param n Number of smallest elements to find. Default value is 1.
  ** \pre n <= slip::cardinal<std::size_t>(first,last)
  ** \pre n == min.size()
  ** \pre n > 0
  ** \remarks Call std::min_element(first,last) if n = 1.
  ** \par Example:
  ** \code
  **  int tab[] = {12,6,6,9,10,1,5,0,-1};
  **  std::vector<int*> min(2);
  **  slip::n_min_elements(tab,tab+9,min,2);
  **  std::cout<<"---------------------"<<std::endl;
  ** for(std::size_t i = 0; i < min.size();++i)
  **  {
  **    std::cout<<"min["<<i<<"] = "<<*(min[i])<<" ";
  **  }
  ** std::cout<<std::endl;
  ** std::cout<<"---------------------"<<std::endl;
  ** \endcode
  */ 
  template<typename ForwardIterator>
  void n_min_elements(ForwardIterator first, ForwardIterator last,
		      std::vector<ForwardIterator>& min, 
		      const std::size_t n = 1)
  {
    assert(n <= slip::cardinal<std::size_t>(first,last));
    assert(min.size() == n);
    assert(n > 0);
    if(n == 1)
      {
	min[0] = std::min_element(first,last);
      }
    else
      {
	std::multiset<ForwardIterator,slip::lt_it<ForwardIterator> > S;
	
	while(first != last)
	  {
	    S.insert(first);
	    first++;
	  }
	
	typename std::multiset<ForwardIterator,lt_it<ForwardIterator> >::iterator it = S.begin();
	for(std::size_t i = 0; i < n; ++i)
	  {
	    min[i] = *it++;
	  }
	
      }
  }

 /*!
  ** \brief Substracts its mean to a range.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2008/10/21
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first RandomAccessIterator to the first element of the range.
  ** \param last RandomAccessIterator to one past-the-end element of the range.
  ** \param out_first RandomAccessIterator to the first element of the range.


  ** \pre The two range must have the same size.
  ** \pre The value type of the range should be compatible with those of the mean value of the range.
  ** \par Example:
  ** \code
  ** slip::Array2d<float> Mc(3,3);
  ** slip::iota(Mc.begin(),Mc.end(),1.0);
  ** std::cout<<"Mc = \n"<<Mc<<std::endl;
  ** slip::Array2d<float> Mc2(3,3);
  ** std::cout<<"center(Mc) = "<<std::endl;
  ** slip::center(Mc.begin(),Mc.end(),Mc2.begin());
  ** std::cout<<"Mc2 = \n"<<Mc2<<std::endl;
  ** \endcode
  */ 
  template<typename RandomAccessIterator, typename RandomAccessIterator2>
  void center(RandomAccessIterator first, RandomAccessIterator last,
	      RandomAccessIterator2 out_first)
  { 
    typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
    value_type mean  = slip::mean<value_type>(first,last);
    
    for (; first != last; ++first, ++out_first)
      {
	*out_first = (*first - mean);
      }
  }

  /*!
  ** \brief Substracts its mean to a range and divide it by its standard deviation.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2008/10/21
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first RandomAccessIterator to the first element of the range.
  ** \param last RandomAccessIterator to one past-the-end element of the range.
  ** \param out_first RandomAccessIterator to the first element of the range.


  ** \pre The two range must have the same size.
  ** \pre The value type of the range should be compatible with those of the mean value of the range.
  ** \par Example:
  ** \code
  ** slip::Array2d<float> Mc(3,3);
  ** slip::iota(Mc.begin(),Mc.end(),1.0);
  ** std::cout<<"Mc = \n"<<Mc<<std::endl;
  ** slip::Array2d<float> Mc2(3,3);
  ** std::cout<<"studentize(Mc) = "<<std::endl;
  ** slip::studentize(Mc.begin(),Mc.end(),Mc2.begin());
  ** std::cout<<"Mc2 = \n"<<Mc2<<std::endl;
  ** \endcode
  */ 
  template<typename RandomAccessIterator, typename RandomAccessIterator2>
  void studentize(RandomAccessIterator first, RandomAccessIterator last,
		  RandomAccessIterator2 out_first)
  { 
    typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
    value_type mean  = slip::mean<value_type>(first,last);
    value_type std_dev = slip::std_dev<value_type>(first,last,mean);
    if(std_dev != value_type(0))
      {
	for (; first != last; ++first, ++out_first)
	  {
	    *out_first = (*first - mean) / std_dev;
	  }
      }
    else
      {
	for (; first != last; ++first, ++out_first)
	  {
	    *out_first = (*first - mean);
	  }
      }
  }
  

  ////// Statistics over a mask


  /**
   ** \brief Computes the mean value of a range over a mask
   ** \f[ \frac{1}{n}\sum_i x_i\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator 
   ** \param mask_first  An InputIterator on the mask 
   ** \param value Mask value.
   ** \return The mean value of the range over the mask.
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
   ** \endcode
   */
   template<typename Value_T,
	    typename InputIterator, 
	    typename MaskIterator>
   inline
   Value_T mean_mask(InputIterator first, InputIterator last, 
		     MaskIterator mask_first, 
		     typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
   {
    assert(first != last); 
    Value_T sum = Value_T(0); 
    long int count = 0; 
    while(first != last)
        {
            if(*mask_first == value)
            { 
		sum += *first; 
		++count; 
	    }

        mask_first++; 
        first++;
        }
    Value_T result = Value_T(0);
    if (count != 0)
      {
	result = Value_T(sum) / Value_T(count);
      }
    return result;
   }

 /**
   ** \brief Computes the nth moment of a range over a mask
   ** \f[\frac{1}{n}\sum_i (x_i-\overline{x})^N\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mask_first  An InputIterator on the mask 
   ** \param mean  The mean of the range itself over the mask.
   ** \param value Mask value.
   ** \return The value of the nth moment over the mask.
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
    ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
   ** std::cout<<slip::nth_moment_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
   ** \endcode
   */
  template<typename T, int N, typename InputIterator, typename MaskIterator>
  inline
  T nth_moment_mask(InputIterator first, InputIterator last, MaskIterator mask_first, T mean, typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    T __init = T();
    int count = 0;
     for (; first != last; ++first,++mask_first)
       {
	if(*mask_first == value)
            { 
		__init = __init + slip::nth_power<N>(*first - mean);
		count++;
	     }
       }
     T result = T(0);
     if(count != 0)
       {
	 result = __init / static_cast<T>(count);
       }
     return result;
  }

 
    /**
   ** \brief Computes the covariance of a two sequences over a mask
   ** \f[\frac{1}{n}\sum_i (x_i-\overline{x})(y_i-\overline{y})\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first  An InputIterator.
   ** \param last   An InputIterator.
   ** \param first2 An InputIterator.
   ** \param mask_first  An InputIterator on the mask 
   ** \param mean1  The mean of the first range over the mask.
   ** \param mean2  The mean of the second range over the mask.
   ** \param value Mask value.
   ** \return The value of the covariance of the two ranges over the mask.
   **
   ** \pre [first,last) must be valid.
   ** \pre The two conainers must have the same sizes.
   ** \pre Range and the Mask must have the same dimensions
   **
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
   ** std::cout<<slip::covariance_mask(M.begin(),M.end(),M.begin(),ValuedMask.begin(),mean_mask2,mean_mask2,2)<<std::endl;
   ** \endcode
   **
   */
  template<typename T, typename InputIterator, typename InputIterator2, typename MaskIterator>
  inline
  T covariance_mask(InputIterator first, InputIterator last, 
	       InputIterator2 first2, MaskIterator mask_first, 
		    T mean1, T mean2, typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    assert(first != last);
    T __init = T();
    long int count = 0;
     for (; first != last; ++first, ++first2,++mask_first)
       {
	if(*mask_first == value)
            { 
		__init = __init + (*first - mean1) * (*first2 - mean2);
		count++;
	    }
       }
     T result = T(0);
     if(count != 0)
       {
	 result = __init / static_cast<T>(count);
       }
     return result;
  }

    /**
   ** \brief Computes the unbiased covariance of a two sequences over a mask
   ** \f[\frac{1}{n-1}\sum_i (x_i-\overline{x})(y_i-\overline{y})\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first  An InputIterator.
   ** \param last   An InputIterator.
   ** \param first2 An InputIterator.
   ** \param mask_first  An InputIterator on the mask 
   ** \param mean1  The mean of the first range over the mask.
   ** \param mean2  The mean of the second range over the mask.
   ** \param value Mask value.
   ** \return The value of the covariance of the two ranges over the mask.
   **
   ** \pre [first,last) must be valid.
   ** \pre The two conainers must have the same sizes.
   ** \pre Range and the Mask must have the same dimensions
   **
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
   ** std::cout<<slip::unbiased_covariance_mask(M.begin(),M.end(),,M.begin(),ValuedMask.begin(),mean_mask2,mean_mask2,2)<<std::endl;
   ** \endcode
   **
   */
  template<typename T, typename InputIterator, typename InputIterator2, typename MaskIterator>
  inline
  T unbiased_covariance_mask(InputIterator first, InputIterator last, 
	       InputIterator2 first2, MaskIterator mask_first, 
		    T mean1, T mean2, typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    T __init = T();
    long int count = 0;
     for (; first != last; ++first, ++first2,++mask_first)
       {
	if(*mask_first == value)
            { 
		__init = __init + (*first - mean1) * (*first2 - mean2);
		count++;
	    }
       }

     T result = T(0);
     if(count != 0)
       {
	 result = __init / static_cast<T>(count-1);
       }
     return result;
  }


  /**
   **
   ** \brief Computes the variance of a range over a mask
   ** \f[\frac{1}{n}\sum_i (x_i-\overline{x})^2\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mask_first  An InputIterator on the mask 
   ** \param mean  The mean of the range itself over the mask.
   ** \param value Mask value.
   ** \return The value of the variance of the range over the mask.
   **
   ** \note Calls slip::covariance_mask(first,last,first,mask_first, mean,mean).
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
   ** std::cout<<slip::variance_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename MaskIterator>
  inline
  T variance_mask(InputIterator first, InputIterator last, MaskIterator mask_first, T mean, typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    return slip::covariance_mask(first,last,first,mask_first, mean,mean,value);
  }


   /**
   **
   ** \brief Computes the unbiased variance of a range over a mask
   ** \f[\frac{1}{n-1}\sum_i (x_i-\overline{x})^2\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mask_first  An InputIterator on the mask 
   ** \param mean  The mean of the range itself over the mask.
   ** \param value Mask value.
   ** \return The value of the variance of the range over the mask.
   **
   ** \note Calls slip::covariance_mask(first,last,first,mask_first, mean,mean).
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
   ** std::cout<<slip::unbiased_variance_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename MaskIterator>
  inline
  T unbiased_variance_mask(InputIterator first, InputIterator last, MaskIterator mask_first, T mean, typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    return slip::unbiased_covariance_mask(first,last,first,mask_first, mean,mean,value);
  }

  /**
   ** \brief Computes the standard deviation of a range over a mask
   ** \f[\sqrt{\frac{1}{n}\sum_i (x_i-\overline{x})^2}\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mask_first  An InputIterator on the mask 
   ** \param mean  The mean of the range itself over the mask.
   ** \param value Mask value.
   ** \return The value of standard deviation of the range over the mask.
   **
   ** \note Calls std::sqrt(slip::variance_mask(first,last,first,mask_first,mean)).
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
   ** std::cout<<slip::std_dev_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename MaskIterator>
  inline
  T std_dev_mask(InputIterator first, InputIterator last, MaskIterator mask_first, T mean, typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    return std::sqrt(slip::variance_mask(first,last,mask_first,mean,value));
  }


  /**
   ** \brief Computes the unbiased standard deviation of a range over a mask
   ** \f[\sqrt{\frac{1}{n-1}\sum_i (x_i-\overline{x})^2}\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mask_first  An InputIterator on the mask 
   ** \param mean  The mean of the range itself over the mask.
   ** \param value Mask value.
   ** \return The value of standard deviation of the range over the mask.
   **
   ** \note Calls std::sqrt(slip::variance_mask(first,last,first,mask_first,mean)).
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
     ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
   ** std::cout<<slip::unbiased_std_dev_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename MaskIterator>
  inline
  T unbiased_std_dev_mask(InputIterator first, InputIterator last, MaskIterator mask_first, T mean, typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    return std::sqrt(slip::unbiased_variance_mask(first,last,mask_first,mean,value));
  }

 
  /**
   ** \brief Computes the kurtosis of a range over a mask
    ** \f[\frac{n \sum_i (x_i-\overline{x})^4}{\left(\sum_i (x_i-\overline{x})^2\right)^2}\f]
   ** A high kurtosis distribution has a sharper peak and longer, 
   ** fatter tails, while a low kurtosis distribution has a more rounded 
   ** peak and shorter thinner tails. Here are some common value of kurtosis
   ** for unimodal and symmetric densities:
   ** \li Laplace distribution: 6
   ** \li Hyperbolic secant distribution: 5
   ** \li Logistic distribution: 4.2
   ** \li Normal distribution: 3
   ** \li Raised cosine distribution: 2.406258...
   ** \li Wigner semi circled distribution: 2
   ** \li Uniform distribution: 1.8
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mask_first  An InputIterator on the mask 
   ** \param mean  The mean of the range itself over the mask.
   ** \param value Mask value.
   ** \return The value of the kurtosis of the range over the mask.
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
     ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
   ** std::cout<<slip::kurtosis_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename MaskIterator>
  inline
  T kurtosis_mask(InputIterator first, InputIterator last, MaskIterator mask_first, T mean, typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    T __init  = T(0);
    T __init2 = T(0);
    long int count = 0;
     for (; first != last; ++first,++mask_first)
       {
	if (*mask_first == value)
	  {
	    T p2 = slip::nth_power<2>(*first - mean);
	    __init  = __init  + p2;
	    __init2 = __init2 + (p2 * p2);
	    count++;
	  }
       }
     T result = T(0);
     if( __init != T(0))
       {
	 result =  (__init2 * count) / (__init * __init);
       }
     return result;
  }
 

  /**
   ** \brief Computes the unbiased kurtosis of a range over a mask
   ** \f[\frac{(n+1)n(n-1)}{(n-2)(n-3)}\frac{\sum_i (x_i-\overline{x})^4}{\left[\sum_i (x_i-\overline{x})^2\right]^2}- \frac{3(n-1)^2}{(n-2)(n-3)} + 3\f]
   ** A high kurtosis distribution has a sharper peak and longer, 
   ** fatter tails, while a low kurtosis distribution has a more rounded 
   ** peak and shorter thinner tails.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mask_first  An InputIterator on the mask 
   ** \param mean  The mean of the range itself over the mask.
   ** \param value Mask value.
   ** \return The value of the kurtosis of the range over the mask.
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
     ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
   ** std::cout<<slip::unbiased_kurtosis_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
   ** \endcode
   */
  template<typename T, 
	   typename InputIterator, 
	   typename MaskIterator>
  inline
  T unbiased_kurtosis_mask(InputIterator first, InputIterator last, 
			   MaskIterator mask_first, 
			   T mean, 
			   typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    T __init  = T(0);
    T __init2 = T(0);
    long int count = 0;
     for (; first != last; ++first,++mask_first)
       {
	if (*mask_first == value)
	  {
	    T p2 = slip::nth_power<2>(*first - mean);
	    __init  = __init  + p2;
	    __init2 = __init2 + (p2 * p2);
	    count++;
	  }
       }

     T count_m1 = T(count - 1);
     T count_m2 = T(count - 2);
     T count_m3 = T(count - 3);
     assert(count > 3);
     T result = T(0);
     if(__init != T(0))
       {
	 result = (((T(count+1)*T(count)* count_m1) * __init2 ) 
		   / ( (count_m2 * count_m3) * (__init * __init) ))
	   - ( (T(3)* (count_m1*count_m1)) / (count_m2 * count_m3)) + T(3);
       }
     return result;
     
  }
 

  /**
   ** \brief Computes the skewness of a range over a mask
   ** \f[\frac{\frac{1}{n}\sum_i (x_i-\overline{x})^3}{\left(\sqrt{\frac{1}{n}\sum_i (x_i-\overline{x})^2}\right)^3}\f]
   ** skewness is a measure of the asymmetry of the probability distribution of a real-valued random variable
   ** \li negative skew: The left tail is longer; the mass of the distribution is concentrated on the right
   ** \li positive skew: The right tail is longer; the mass of the distribution is concentrated on the left
   ** \li 0 skew: the distribution is symmetric (Normal distribution for example)
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mask_first  An InputIterator on the mask 
   ** \param mean  The mean of the range itself over the mask.
   ** \param value Mask value.
   ** \return The value of the skewness of the range over the mask.
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
   ** std::cout<<slip::skewness_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
   ** \endcode
   */
  template<typename T, 
	   typename InputIterator, 
	   typename MaskIterator>
  inline
  T skewness_mask(InputIterator first, InputIterator last, 
		  MaskIterator mask_first, 
		  T mean, 
		  typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    T __init  = T(0);
    T __init2 = T(0);
    long int count = 0;
     for (; first != last; ++first,++mask_first)
       {
	if (*mask_first == value)
	  {
	    T p1 = (*first - mean);
	    T p2 =  p1 * p1;
	    __init  = __init  + p2;
	    __init2 = __init2 + (p2 * p1);
	    count++;
	 
	  }
       }
     assert(count > 0);
     T result = T(0);
     if(__init != T(0))
       {
	 result = __init2 / (T(count)*slip::nth_power<3>(std::sqrt(__init/T(count))));
       }
     return result;
  }
 

  /**
   ** \brief Computes the unbiased skewness of a range over a mask
   ** \f[\frac{n}{(n-1)(n-2)}\frac{\sum_i (x_i-\overline{x})^3}{\left(\sqrt{\frac{1}{n-1}\sum_i (x_i-\overline{x})^2}\right)^3}\f]
   ** skewness is a measure of the asymmetry of the probability distribution of a real-valued random variable
   ** \li negative skew: The left tail is longer; the mass of the distribution is concentrated on the right
   ** \li positive skew: The right tail is longer; the mass of the distribution is concentrated on the left
   ** \li 0 skew: the distribution is symmetric (Normal distribution for example)
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mask_first  An InputIterator on the mask 
   ** \param mean  The mean of the range itself over the mask.
   ** \param value Mask value.
   ** \return The value of the skewness of the range over the mask.
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
   ** std::cout<<slip::unbiased_skewness_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
   ** \endcode
   */
  template<typename T, 
	   typename InputIterator, 
	   typename MaskIterator>
  inline
  T unbiased_skewness_mask(InputIterator first, InputIterator last, 
			   MaskIterator mask_first, 
			   T mean, 
			   typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    T __init  = T(0);
    T __init2 = T(0);
    long int count = 0;
     for (; first != last; ++first,++mask_first)
       {
	if (*mask_first == value)
	  {
	    T p1 = (*first - mean);
	    T p2 =  p1 * p1;
	    __init  = __init  + p2;
	    __init2 = __init2 + (p2 * p1);
	    count++;
	 
	  }
       }
     assert(count > 2);
     T result = T(0);
     if(__init != T(0))
       {
	 result = (__init2 * T(count)) / (T(count-1)*T(count-2)*slip::nth_power<3>(std::sqrt(__init/T(count-1))));
       }
     return result;
}

  /**
   ** \brief Computes the root mean square (rms) value of a range over a mask
   ** \f[ \sqrt{\frac{1}{n}\sum_i x_i^2}\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator. 
   ** \param mask_first  An InputIterator on the mask
   ** \param value Mask value.
   ** \return The rms value of the range over the mask.
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
   **
   ** \note Calls std::sqrt(slip::covariance_mask(first,last,first,mask_first,T(0),T(0)));
     ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  double mean_mask2=slip::mean_mask<double>(M.begin(),M.end(),ValuedMask.begin(),2);
   ** std::cout<<slip::rms_mask(M.begin(),M.end(),ValuedMask.begin(),mean_mask2,2)<<std::endl;
   ** \endcode
   */
  template<typename T, 
	   typename InputIterator, 
	   typename MaskIterator>
  inline
  T rms_mask(InputIterator first, InputIterator last, 
	     MaskIterator mask_first,
	     typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    return std::sqrt(slip::covariance_mask(first,last,first,mask_first,	T(0),T(0),value));
  }

  /**
   ** \brief Computes the cardinal or the number of elements of a mask range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param mask_first An InputIterator of a mask Iterator.
   ** \param mask_last  An InputIterator of a mask Iterator.
   ** \param value Mask value.
   ** \return The number of elements of the range.
   **
   ** \pre [mask_first,mask_last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
   ** \par Example:
   ** \code
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   **  std::cout<<slip::cardinal_mask<int>(ValuedMask.begin(),ValuedMask.end(),2)<<std::endl;
   ** \endcode
   */
  template<typename Integer,typename MaskIterator>
  inline
  Integer cardinal_mask(MaskIterator mask_first, 
			MaskIterator mask_last,
			typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    Integer count = 0;
    while(mask_first != mask_last)
      {
	if (*mask_first == value)
	  {
	    count++;
	  }
	mask_first++;
      }
    return count;
  }

  /**
   ** \brief Computes the median value from a sorted range over a mask.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \param mask_first  An InputIterator on the mask
   ** \param value Mask value.
   ** \return The Iterator to the median value of the range over a mask iterator.
   **
   ** \pre [first,last) must be valid.
   ** \pre The range must be sorted.
   ** \pre (last - first) must return the number of element in the range
   ** \pre Range and the Mask must have the same dimensions
   */
  template<typename InputIterator, 
	   typename MaskIterator>
  inline
  typename std::iterator_traits<InputIterator>::value_type
  median_from_sorted_data_mask(InputIterator first, InputIterator last, 
			       MaskIterator mask_first, 
			       typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    MaskIterator mask_last=mask_first+(last - first);
    std::size_t mask_count=slip::cardinal_mask<std::size_t>(mask_first,mask_last,value);
    assert(mask_count != 0);
    std::size_t count = 0;
    while(first != last)
      {
	if (*mask_first == value)
	  {
		count++;
	  }
	if (count>mask_count/2)
	  {
		return *first;
	  }
	first++;
        mask_first++;
      }    
    std::cerr << "Error computing median_from_sorted_data_mask"<<std::endl;
    return *first; 
  }
  
   /**
   ** \brief Computes the median value from a non sorted range over a mask.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \param mask_first  An InputIterator of the mask
   ** \param value Mask value.
   ** \return The median value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   ** std::cout<<slip::median_mask(M.begin(),M.end(),ValuedMask.begin(),2)<<std::endl;
   ** \endcode
   **
   */
  template<typename InputIterator, typename MaskIterator>
  inline
  typename std::iterator_traits<InputIterator>::value_type
  median_mask(InputIterator first, InputIterator last, 
	      MaskIterator mask_first, 
	      typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    std::size_t mask_count = 
      slip::cardinal_mask<std::size_t>(mask_first,mask_first+(last-first),
				       value);

    assert(mask_count != 0);
    slip::Array<typename std::iterator_traits<InputIterator>::value_type> tmp(mask_count); 
    std::size_t count = 0;
    while(first != last)
      {
	if (*mask_first == value)
	  {
	    tmp[count]=*first;
	    count++;
	  }
	first++;
        mask_first++;
      }    
    
    std::sort(tmp.begin(),tmp.end());
    return slip::median_from_sorted_data_n(tmp.begin(),mask_count); 

  }

 /**
   ** \brief Computes the first quartile value from a non sorted range over a mask.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \param mask_first  An InputIterator of the mask
   ** \param value Mask value.
   ** \return The first quartile value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   ** std::cout<<slip::first_quartile_mask(M.begin(),M.end(),ValuedMask.begin(),2)<<std::endl;
   ** \endcode
   **
   */
  template<typename InputIterator, 
	   typename MaskIterator>
  inline
  typename std::iterator_traits<InputIterator>::value_type
  first_quartile_mask(InputIterator first, InputIterator last, 
		      MaskIterator mask_first, 
		      typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    std::size_t mask_count = slip::cardinal_mask<std::size_t>(mask_first,mask_first+(last-first),value);
    assert(mask_count != 0);
    slip::Array<typename std::iterator_traits<InputIterator>::value_type> tmp(mask_count);
    std::size_t count = 0;
    while(first != last)
      {
	if (*mask_first == value)
	  {
	    tmp[count]=*first;
	    count++;
	  }
	first++;
        mask_first++;

      }    

    std::sort(tmp.begin(),tmp.end());
    return slip::first_quartile(tmp.begin(),tmp.end()); 

  }

   /**
   ** \brief Computes the third quartile value from a non sorted range over a mask.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \param mask_first  An InputIterator of the mask
   ** \param value Mask value.
   ** \return The first quartile value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \pre Range and the Mask must have the same dimensions
   ** \par Example:
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** slip::Array2d<int> ValuedMask(4,5,2);
   ** for(std::size_t i = 0; i < M.dim1(); ++i)
   ** {
   **	ValuedMask[i][0]= 0;
   ** }  
   ** for(std::size_t j = 0; j < M.dim2(); ++j)
   ** {
   **	ValuedMask[0][j]=0;
   ** }  
   ** std::cout<<slip::third_quartile_mask(M.begin(),M.end(),ValuedMask.begin(),2)<<std::endl;
   ** \endcode
   **
   */
  template<typename InputIterator, typename MaskIterator>
  inline
  typename std::iterator_traits<InputIterator>::value_type
  third_quartile_mask(InputIterator first, InputIterator last, MaskIterator mask_first, typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
  {
    std::size_t mask_count=slip::cardinal_mask<std::size_t>(mask_first,mask_first+(last-first),value);
    assert(mask_count != 0);
    slip::Array<typename std::iterator_traits<InputIterator>::value_type> tmp(mask_count);

    std::size_t count = 0;
    while(first != last)
      {
	if (*mask_first == value)
	  {
	    tmp[count]=*first;
	    count++;
	  }
	first++;
        mask_first++;
      }    

    std::sort(tmp.begin(),tmp.end());
    return slip::third_quartile(tmp.begin(),tmp.end()); 

  }

  /**
   ** \brief Substracts its mean to a range according a mask.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator 
   ** \param mask_first  An InputIterator on the mask 
   ** \param out_first An OuputIterator. 
   ** \param value Mask value.
   **
   ** \pre [first,last) must be valid.
   ** \pre The two ranges and the Mask must have the same dimensions
   ** \pre The value type of the range should be compatible with those of the mean value of the range.
   ** \par Example:
   ** \code
   ** slip::Array2d<float> Mcmask(4,5);
   ** slip::iota(Mcmask.begin(),Mcmask.end(),1.0);
   ** std::cout<<"Mcmask = \n"<<Mcmask<<std::endl;
   ** slip::Array2d<float> Mcmask2(4,5);
   ** slip::center_mask(Mcmask.begin(),Mcmask.end(),
   **		        ValuedMask.begin(),
   **	                Mcmask2.begin(),2);
   ** std::cout<<"Mcmask2 = \n"<<Mcmask2<<std::endl;
   ** \endcode
   */
  template<typename InputIterator, typename MaskIterator, typename OutputIterator>
   inline
   void center_mask(InputIterator first, InputIterator last, 
		    MaskIterator mask_first, 
		    OutputIterator out_first, 
		    typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
   {
    assert(first != last); 
    typedef typename std::iterator_traits<OutputIterator>::value_type value_type;
 
    value_type mean = slip::mean_mask<value_type>(first,last,mask_first,value);
    
    for (; first != last; ++first, ++out_first, ++mask_first)
      { 
	if(*mask_first == value)
	  {
	    *out_first = (*first - mean);
	  }
      }
                 
   }


  /**
   ** \brief Substracts its mean to a range and divide it by its standard deviation according a mask.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator 
   ** \param mask_first  An InputIterator on the mask 
   ** \param out_first An OuputIterator. 
   ** \param value Mask value.
   **
   ** \pre [first,last) must be valid.
   ** \pre The two ranges and the Mask must have the same dimensions
   ** \pre The value type of the range should be compatible with those of the mean value of the range.
   ** \par Example:
   ** \code
   ** slip::Array2d<float> Mcmask(4,5);
   ** slip::iota(Mcmask.begin(),Mcmask.end(),1.0);
   ** std::cout<<"Mcmask = \n"<<Mcmask<<std::endl;
   ** slip::Array2d<float> Mcmask2(4,5);
   ** slip::studentize_mask(Mcmask.begin(),Mcmask.end(),
   **		        ValuedMask.begin(),
   **	                Mcmask2.begin(),2);
   ** std::cout<<"Mcmask2 = \n"<<Mcmask2<<std::endl;
   ** \endcode
   */
  template<typename InputIterator, typename MaskIterator, typename OutputIterator>
   inline
   void studentize_mask(InputIterator first, InputIterator last, 
		    MaskIterator mask_first, 
		    OutputIterator out_first, 
		    typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
   {
    assert(first != last); 
    typedef typename std::iterator_traits<OutputIterator>::value_type value_type;
 
    value_type mean = slip::mean_mask<value_type>(first,last,mask_first,value);
    value_type std_dev = slip::std_dev_mask<value_type>(first,last,mask_first,mean,value);
    
    if(std_dev != value_type(0))
      {
	for (; first != last; ++first, ++out_first, ++mask_first)
	  { 
	    if(*mask_first == value)
	      {
		*out_first = (*first - mean) / std_dev;
	      }
	  }
      }
    else
      {
	for (; first != last; ++first, ++out_first, ++mask_first)
	  { 
	    if(*mask_first == value)
	      {
		*out_first = (*first - mean);
	      }
	  }
      }
                 
   }


  ////// Statistics using a predicate
  /**
   ** \brief Computes the cardinal or the number of elements of a range using a predicate.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param pred a predicate function
   ** \return The number of elements of the range.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** std::cout<<slip::cardinal_if<int>(M.begin(),M.end(),myPredicate)<<std::endl;
   ** \endcode
   */
  template<typename Integer,typename InputIterator, typename Predicate>
  inline
  Integer cardinal_if(InputIterator first, 
		   InputIterator last,
		Predicate pred)
  {
    Integer count = 0;
    while(first != last)
      {
	if (pred(*first))
	  {
	    count++;
	  }
	first++;
      }
    return count;
  }

  /**
   ** \brief Computes the mean value of a range using a predicate
   ** \f[ \frac{1}{n}\sum_i x_i\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator 
   ** \param pred  a predicate function
   ** \return The mean value of the range using a predicate.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** 
   ** std::cout<<slip::mean_if<double>(M.begin(),M.end(),myPredicate)<<std::endl;
   ** \endcode
   */
   template<typename Value_T,typename InputIterator, typename Predicate>
   inline
   Value_T mean_if(InputIterator first, InputIterator last, Predicate pred)
   {
    assert(first != last); 
    Value_T sum = Value_T(0); 
    long int count = 0; 
    while(first != last)
        {
            if(pred(*first))
            { 
		sum += *first; ++count; 
	    }

        first++;
        }
    Value_T result = Value_T(0);
    if(count != 0)
      {
	result =  Value_T(sum) / Value_T(count);
      }
    return result;
   }

 /**
   ** \brief Computes the nth moment of a range using a predicate
   ** \f[\frac{1}{n}\sum_i (x_i-\overline{x})^N\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the range itself using the predicate.
   ** \param pred  a predicate function
   ** \return The value of the nth moment using the predicate.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** 
   ** double mean_if = slip::mean_if<double>(M.begin(),M.end(),myPredicate);
   ** std::cout<<slip::nth_moment_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
   ** \endcode
   */
  template<typename T, int N, typename InputIterator, typename Predicate>
  inline
  T nth_moment_if(InputIterator first, InputIterator last, T mean, Predicate pred)
  {
    T __init = T();
    long int count = 0;
     for (; first != last; ++first)
       {
	if(pred(*first))
            { 
		__init = __init + slip::nth_power<N>(*first - mean);
		count++;
	     }
       }
     T result = T(0);
     if(count != 0)
       {
	 result = __init / count;
       }
     return result;
  }

 
    /**
     ** \brief Computes the covariance of a two sequences using a predicate on the first one
     ** \f[\frac{1}{n}\sum_i (x_i-\overline{x})(y_i-\overline{y})\f]
     ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
     ** \date 2007/05/25
     ** \since 1.0.0
     ** \version 0.0.1
     ** \param first  An InputIterator.
     ** \param last   An InputIterator.
     ** \param first2 An InputIterator.
     ** \param mean1  The mean of the first range using the predicate.
     ** \param mean2  The mean of the second range using the predicate.
     ** \param pred  a predicate function
     ** \return The value of the covariance of the two ranges using the predicate.
     **
     ** \pre [first,last) must be valid.
     ** \pre The two conainers must have the same sizes.
     ** \par Example:
     ** \code
     ** //definition of the predicate less than 10
     ** bool myPredicate (const double& val)
     ** {
     **   return (val<10);
     ** }
     ** \endcode
     ** \code
     ** //construction of the array
     ** slip::Array2d<int> M(4,5);
     ** slip::iota(M.begin(),M.end(),1,1);
     ** 
     ** double mean_if = slip::mean_if<double>(M.begin(),M.end(),myPredicate);
     ** std::cout<<slip::covariance_if(M.begin(),M.end(),M.begin(),mean_if,mean_if,myPredicate)<<std::endl;
     ** \endcode
     **
     */
  template<typename T, 
	   typename InputIterator, 
	   typename InputIterator2, 
	   typename Predicate>
  inline
  T covariance_if(InputIterator first, InputIterator last, 
		  InputIterator2 first2, 
		  T mean1, T mean2, Predicate pred)
  {
    T __init = T();
    long int count = 0;
     for (; first != last; ++first, ++first2)
       {
	if(pred(*first))
            { 
		__init = __init + (*first - mean1) * (*first2 - mean2);
		count++;
	    }
       }
     T result = T(0);
     if(count != 0)
       {
	 result = __init / count;
       }
     return result;
  }

    /**
   ** \brief Computes the unbiased covariance of a two sequences using a predicate on the first one
   ** \f[\frac{1}{n-1}\sum_i (x_i-\overline{x})(y_i-\overline{y})\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first  An InputIterator.
   ** \param last   An InputIterator.
   ** \param first2 An InputIterator.
   ** \param mean1  The mean of the first range using the predicate.
   ** \param mean2  The mean of the second range using the predicate.
   ** \param pred  a predicate function
   ** \return The value of the unbiased covariance of the two ranges using the predicate.
   **
   ** \pre [first,last) must be valid.
   ** \pre The two conainers must have the same sizes.
   ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** 
   ** double mean_if = slip::mean_if<double>(M.begin(),M.end(),myPredicate);
   ** std::cout<<slip::unbiased_covariance_if(M.begin(),M.end(),M.begin(),mean_if,mean_if,myPredicate)<<std::endl;
   ** \endcode
   **
   */
  template<typename T, typename InputIterator, typename InputIterator2, typename Predicate>
  inline
  T unbiased_covariance_if(InputIterator first, InputIterator last, 
		  InputIterator2 first2, 
		  T mean1, T mean2,Predicate pred)
  {
    T __init = T();
    long int count = 0;
     for (; first != last; ++first, ++first2)
       {
	if(pred(*first))
            { 
		__init = __init + (*first - mean1) * (*first2 - mean2);
		count++;
	    }
       }
     T result = T(0);
     if(count != 0)
       {
	 result = __init / static_cast<T>(count - 1);
       }
     return result;
  }


  /**
   **
   ** \brief Computes the variance of a range using a predicate
   ** \f[\frac{1}{n}\sum_i (x_i-\overline{x})^2\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the range itself using the predicate.
   ** \param pred  a predicate function
   ** \return The value of the variance of the range using the predicate.
   **
   ** \note Calls slip::covariance_if(first,last,first,pred, mean,mean).
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** 
   ** double mean_if = slip::mean_if<double>(M.begin(),M.end(),myPredicate);
   ** std::cout<<slip::variance_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename Predicate>
  inline
  T variance_if(InputIterator first, InputIterator last, T mean, Predicate pred)
  {
    return slip::covariance_if(first,last,first, mean,mean,pred);
  }


   /**
   **
   ** \brief Computes the unbiased variance of a range using a predicate
   ** \f[\frac{1}{n-1}\sum_i (x_i-\overline{x})^2\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the range itself using the predicate.
   ** \param pred  a predicate function
   ** \return The value of the unbiased variance of the range using the predicate.
   **
   ** \note Calls slip::unbiased_covariance_if(first,last,first,pred, mean,mean).
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** 
   ** double mean_if = slip::mean_if<double>(M.begin(),M.end(),myPredicate);
   ** std::cout<<slip::unbiased_variance_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename Predicate>
  inline
  T unbiased_variance_if(InputIterator first, InputIterator last, T mean, Predicate pred)
  {
    return slip::unbiased_covariance_if(first,last,first, mean,mean,pred);
  }

  /**
   ** \brief Computes the standard deviation of a range using a predicate
   ** \f[\sqrt{\frac{1}{n}\sum_i (x_i-\overline{x})^2}\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the range itself using the predicate.
   ** \param pred a predicate function
   ** \return The value of standard deviation of the range using the predicate.
   **
   ** \note Calls std::sqrt(slip::variance_if(first,last,first,pred,mean)).
   **
   ** \pre [first,last) must be valid.
   
     ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** 
   ** double mean_if = slip::mean_if<double>(M.begin(),M.end(),myPredicate);
   ** std::cout<<slip::std_dev_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename Predicate>
  inline
  T std_dev_if(InputIterator first, InputIterator last, T mean, Predicate pred)
  {
    return std::sqrt(slip::variance_if(first,last,mean,pred));
  }


   /**
   ** \brief Computes the unbiased standard deviation of a range using a predicate
   ** \f[\sqrt{\frac{1}{n-1}\sum_i (x_i-\overline{x})^2}\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the range itself using the predicate.
   ** \param pred a predicate function
   ** \return The value of unbiased standard deviation of the range using the predicate.
   **
   ** \note Calls std::sqrt(slip::unbiased_variance_if(first,last,first,pred,mean)).
   **
   ** \pre [first,last) must be valid.
   
     ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** 
   ** double mean_if = slip::mean_if<double>(M.begin(),M.end(),myPredicate);
   ** std::cout<<slip::unbiased_std_dev_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename Predicate>
  inline
  T unbiased_std_dev_if(InputIterator first, InputIterator last, T mean, Predicate pred)
  {
    return std::sqrt(slip::unbiased_variance_if(first,last,mean,pred));
  }

 
  /**
   ** \brief Computes the kurtosis of a range using a predicate
   ** \f[\frac{n \sum_i (x_i-\overline{x})^4}{\left(\sum_i (x_i-\overline{x})^2\right)^2}\f]
   ** A high kurtosis distribution has a sharper peak and longer, 
   ** fatter tails, while a low kurtosis distribution has a more rounded 
   ** peak and shorter thinner tails. Here are some common value of kurtosis
   ** for unimodal and symmetric densities:
   ** \li Laplace distribution: 6
   ** \li Hyperbolic secant distribution: 5
   ** \li Logistic distribution: 4.2
   ** \li Normal distribution: 3
   ** \li Raised cosine distribution: 2.406258...
   ** \li Wigner semi circled distribution: 2
   ** \li Uniform distribution: 1.8
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the range itself using the predicate.
   ** \param pred a predicate function
   ** \return The value of the kurtosis of the range using the predicate.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** 
   ** double mean_if = slip::mean_if<double>(M.begin(),M.end(),myPredicate);
   ** std::cout<<slip::kurtosis_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename Predicate>
  inline
  T kurtosis_if(InputIterator first, InputIterator last, T mean, Predicate pred)
  {
    T __init  = T();
    T __init2 = T();
    long int count = 0;
     for (; first != last; ++first)
       {
	if (pred(*first))
	  {
	 	T p2 = slip::nth_power<2>(*first - mean);
	 	__init  = __init  + p2;
	 	__init2 = __init2 + (p2 * p2);
	 	count++;
	  }
       }
     T result = T(0);
     if( __init != T(0))
       {
	 result =  (__init2 * T(count)) / (__init * __init);
       }
     return result;
  }
 

  /**
   ** \brief Computes the unbiased kurtosis of a range using a predicate
   ** \f[\frac{(n+1)n(n-1)}{(n-2)(n-3)}\frac{\sum_i (x_i-\overline{x})^4}{\left[\sum_i (x_i-\overline{x})^2\right]^2}- \frac{3(n-1)^2}{(n-2)(n-3)} + 3\f]
   ** A high kurtosis distribution has a sharper peak and longer, 
   ** fatter tails, while a low kurtosis distribution has a more rounded 
   ** peak and shorter thinner tails.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the range itself using the predicate.
   ** \param pred a predicate function
   ** \return The value of the unbiased kurtosis of the range using the predicate.
   **
   ** \pre [first,last) must be valid.
      ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** 
   ** double mean_if = slip::mean_if<double>(M.begin(),M.end(),myPredicate);
   ** std::cout<<slip::unbiased_kurtosis_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename Predicate>
  inline
  T unbiased_kurtosis_if(InputIterator first, InputIterator last, T mean, Predicate pred)
  {
    T __init  = T();
    T __init2 = T();
    long int count = 0;
     for (; first != last; ++first)
       {
	if (pred(*first)){
	 	T p2 = slip::nth_power<2>(*first - mean);
	 	__init  = __init  + p2;
	 	__init2 = __init2 + (p2 * p2);
	 	count++;
	}
       }
       assert(count > 3);
       T count_m1 = T(count - 1);
       T count_m2 = T(count - 2);
       T count_m3 = T(count - 3);

       T result = T(0);
       if( __init != T(0))
	 {
	   result =   (((T(count+1)*T(count)* count_m1) * __init2 ) 
		/ ( (count_m2 * count_m3) * (__init * __init) ))
	 - ( (T(3)* (count_m1*count_m1)) / (count_m2 * count_m3)) + T(3);
	 }
       return result;
  }


   /**
    ** \brief Computes the skewness of a range using a predicate
    ** \f[\frac{\frac{1}{n}\sum_i (x_i-\overline{x})^3}{\left(\sqrt{\frac{1}{n}\sum_i (x_i-\overline{x})^2}\right)^3}\f]
    ** skewness is a measure of the asymmetry of the probability distribution of a real-valued random variable
    ** \li negative skew: The left tail is longer; the mass of the distribution is concentrated on the right
    ** \li positive skew: The right tail is longer; the mass of the distribution is concentrated on the left
    ** \li 0 skew: the distribution is symmetric (Normal distribution for example)
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the range itself using the predicate.
   ** \param pred a predicate function
   ** \return The value of the skewness of the range using the predicate.
   **
   ** \pre [first,last) must be valid.
      ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** 
   ** double mean_if = slip::mean_if<double>(M.begin(),M.end(),myPredicate);
   ** std::cout<<slip::skewness_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename Predicate>
  inline
  T skewness_if(InputIterator first, InputIterator last, T mean, Predicate pred)
  {
    T __init  = T();
    T __init2 = T();
    long int count = 0;
     for (; first != last; ++first)
       {
	if (pred(*first))
	  {
	    T p1 = (*first - mean);
	    T p2 =  p1 * p1;
	    __init  = __init  + p2;
	    __init2 = __init2 + (p2 * p1);
	    count++;
	  }
       }
     assert(count > 0);
     T result = T(0);
     if( __init != T(0))
       {
	 result = __init2 / (T(count)*slip::nth_power<3>(std::sqrt(__init/T(count))));
       }
     return result;
  }

  /**
   ** \brief Computes the unbiased skewness of a range using a predicate
    ** \f[\frac{n}{(n-1)(n-2)}\frac{\sum_i (x_i-\overline{x})^3}{\left(\sqrt{\frac{1}{n-1}\sum_i (x_i-\overline{x})^2}\right)^3}\f]
    ** skewness is a measure of the asymmetry of the probability distribution of a real-valued random variable
    ** \li negative skew: The left tail is longer; the mass of the distribution is concentrated on the right
    ** \li positive skew: The right tail is longer; the mass of the distribution is concentrated on the left
    ** \li 0 skew: the distribution is symmetric (Normal distribution for example)
    ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator.
   ** \param mean  The mean of the range itself using the predicate.
   ** \param pred a predicate function
   ** \return The value of the skewness of the range using the predicate.
   **
   ** \pre [first,last) must be valid.
      ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** 
   ** double mean_if = slip::mean_if<double>(M.begin(),M.end(),myPredicate);
   ** std::cout<<slip::unbiased_skewness_if(M.begin(),M.end(),mean_if,myPredicate)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename Predicate>
  inline
  T unbiased_skewness_if(InputIterator first, InputIterator last, T mean, Predicate pred)
  {
    T __init  = T();
    T __init2 = T();
    long int count = 0;
     for (; first != last; ++first)
       {
	if (pred(*first))
	  {
	    T p1 = (*first - mean);
	    T p2 =  p1 * p1;
	    __init  = __init  + p2;
	    __init2 = __init2 + (p2 * p1);
	    count++;
	  }
       }
     assert(count > 2);
     T result = T(0);
     if( __init != T(0))
       {
	 result = (__init2 * T(count)) / (T(count-1)*T(count-2)*slip::nth_power<3>(std::sqrt(__init/T(count-1))));
       }
     return result;
  }

  /**
   ** \brief Computes the root mean square (rms) value of a range using a predicate
   ** \f[ \sqrt{\frac{1}{n}\sum_i x_i^2}\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator. 
   ** \param pred a predicate function
   ** \return The rms value of the range using the predicate.
   **
   ** \pre [first,last) must be valid.
   **
   ** \note Calls
   ** std::sqrt(slip::covariance_if(first,last,first,T(0),T(0),pred);
        ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   ** 
   ** std::cout<<slip::rms_if(M.begin(),M.end(),myPredicate)<<std::endl;
   ** \endcode
   */
  template<typename T, typename InputIterator, typename Predicate>
  inline
  T rms_if(InputIterator first, InputIterator last, Predicate pred)
  {
    return std::sqrt(slip::covariance_if(first,last,first,T(0),T(0),pred));
  }



  /**
   ** \brief Computes the median value from a sorted range using a predicate.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2007/05/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \param pred  a predicate function
   ** \return The Iterator to the median value of the range over a mask iterator.
   **
   ** \pre [first,last) must be valid.
   ** \pre The range must be sorted.
   ** \pre (last - first) must return the number of element in the range
   */
  template<typename InputIterator, typename Predicate>
  inline
  typename std::iterator_traits<InputIterator>::value_type
  median_from_sorted_data_if(InputIterator first, InputIterator last, 
			     Predicate pred)
  {
    std::size_t mask_count=slip::cardinal_if<std::size_t>(first,last,pred);
    assert(mask_count != 0);
    std::size_t count = 0;
    while(first != last)
      {
	if (pred(*first))
	  {
	    count++;
	  }
	if (count > mask_count/2)
	  {
	    return *first;
	  }
	first++;
      }    
    std::cerr << "Error computing median_from_sorted_data_if"<<std::endl;
    return *first; 
  }
  
   /**
   ** \brief Computes the median value from a non sorted range using a predicate.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \param pred a predicate function
   ** \return The median value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   **  std::cout<<slip::median_if(M.begin(),M.end(),myPredicate)<<std::endl;
   ** \endcode
   **
   */
  template<typename InputIterator, typename Predicate>
  inline
  typename std::iterator_traits<InputIterator>::value_type
  median_if(InputIterator first, InputIterator last, 
	    Predicate pred)
  {
    std::size_t mask_count=slip::cardinal_if<std::size_t>(first,last,pred);
    assert(mask_count != 0);
    slip::Array<typename std::iterator_traits<InputIterator>::value_type> tmp(mask_count);
    std::size_t count = 0;

    while(first != last)
      {
	if (pred(*first))
	  {
	    tmp[count]=*first;
	    count++;
	  }
	first++;
      }    
    std::sort(tmp.begin(),tmp.end());
    return slip::median_from_sorted_data_n(tmp.begin(),mask_count); 

  }

   /**
   ** \brief Computes the first quartile value from a non sorted range using a predicate.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \param pred a predicate function
   ** \return The first quartile value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   **  std::cout<<slip::first_quartile_if(M.begin(),M.end(),myPredicate)<<std::endl;
   ** \endcode
   **
   */
  template<typename InputIterator, typename Predicate>
  inline
  typename std::iterator_traits<InputIterator>::value_type
  first_quartile_if(InputIterator first, InputIterator last, Predicate pred)
  {
    std::size_t mask_count=slip::cardinal_if<std::size_t>(first,last,pred);
    assert(mask_count != 0);
    slip::Array<typename std::iterator_traits<InputIterator>::value_type> tmp(mask_count);
    std::size_t count = 0;

    while(first != last)
      {
	if (pred(*first))
	  {
	    tmp[count] = *first;
	    count++;
	  }
	first++;
      }    

    std::sort(tmp.begin(),tmp.end());
    return slip::first_quartile(tmp.begin(),tmp.end()); 

  }


   /**
   ** \brief Computes the third quartile value from a non sorted range using a predicate.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first A RandomAccessIterator.
   ** \param last  A RandomAccessIterator. 
   ** \param pred a predicate function
   ** \return The third quartile value of the range.
   **
   ** \pre [first,last) must be valid.
   ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** //construction of the array
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),1,1);
   **  std::cout<<slip::third_quartile_if(M.begin(),M.end(),myPredicate)<<std::endl;
   ** \endcode
   **
   */
  template<typename InputIterator, typename Predicate>
  inline
  typename std::iterator_traits<InputIterator>::value_type
  third_quartile_if(InputIterator first, InputIterator last, Predicate pred)
  {
    std::size_t mask_count=slip::cardinal_if<std::size_t>(first,last,pred);
    assert(mask_count != 0);
    slip::Array<typename std::iterator_traits<InputIterator>::value_type> tmp(mask_count);
    std::size_t count = 0;

    while(first != last)
      {
	if (pred(*first))
	  {
	    tmp[count] = *first;
	    count++;
	  }
	first++;
      }    

    std::sort(tmp.begin(),tmp.end());
    return slip::third_quartile(tmp.begin(),tmp.end()); 

  }

  
 /**
   ** \brief Substracts its mean to a range using a predicate.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator 
   ** \param pred  a predicate function
   ** \param out_first An OutputIterator
   **
   ** \pre [first,last) must be valid.
   ** \pre the two range must have the same size.
   ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** slip::Array2d<float> Mcif2(4,5);
   ** slip::center_if(M.begin(),M.end(),myPredicate,Mcif2.begin());
   ** std::cout<<"Mcif2 = \n"<<Mcif2<<std::endl;
   ** \endcode
   */
  template<typename InputIterator, typename Predicate, typename OutputIterator>
  inline
  void center_if(InputIterator first, InputIterator last, 
		 Predicate pred,
		 OutputIterator out_first)
  {
    assert(first != last); 

    typedef typename std::iterator_traits<OutputIterator>::value_type value_type;
    value_type mean = slip::mean_if<value_type>(first,last,pred);
   
    while(first != last)
        {
	  if(pred(*first))
            { 
	      *out_first = (*first - mean);
	    }
	  ++out_first;
	  ++first;
        }
   }


  /**
   ** \brief Substracts its mean to a range and divide it by its standard deviation using a predicate.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first An InputIterator.
   ** \param last  An InputIterator 
   ** \param pred  a predicate function
   ** \param out_first An OutputIterator
   **
   ** \pre [first,last) must be valid.
   ** \pre the two range must have the same size.
   ** \par Example:
   ** \code
   ** //definition of the predicate less than 10
   ** bool myPredicate (const double& val)
   ** {
   **   return (val<10);
   ** }
   ** \endcode
   ** \code
   ** slip::Array2d<int> M(4,5);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** slip::Array2d<float> Mcif2(4,5);
   ** slip::studentize_if(M.begin(),M.end(),myPredicate,Mcif2.begin());
   ** std::cout<<"Mcif2 = \n"<<Mcif2<<std::endl;
   ** \endcode
   */
  template<typename InputIterator, typename Predicate, typename OutputIterator>
  inline
  void studentize_if(InputIterator first, InputIterator last, 
		 Predicate pred,
		 OutputIterator out_first)
  {
    assert(first != last); 

    typedef typename std::iterator_traits<OutputIterator>::value_type value_type;
    value_type mean = slip::mean_if<value_type>(first,last,pred);
    value_type std_dev = slip::std_dev_if<value_type>(first,last,mean,pred);
   
    if(std_dev != value_type(0))
      {
	while(first != last)
	  {
	    if(pred(*first))
	      { 
		*out_first = (*first - mean) / std_dev;
	      }
	    ++out_first;
	    ++first;
	  }
      }
    else
      {
	while(first != last)
	  {
	    if(pred(*first))
	      { 
		*out_first = (*first - mean);
	      }
	    ++out_first;
	    ++first;
	  }
      }
   }






/*!
  ** \brief compute all statistics of a range
  ** \author Mouhamed Hammoud <hammoud_AT_sic.univ-poitiers.fr>
  ** \date 2009/08/25
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first  An InputIterator.
  ** \param last   An InputIterator.
  ** \param statistic  slip::statistics
  ** \return a range which contains all its statistics
  ** \par Example:
  ** \code
  ** int f[]={6, 47, 49, 15, 43, 41, 7, 39, 43, 41, 36};
  ** slip::Vector<int> M(11,f);
  ** std::cout<<"M=\n"<<M<<std::endl;
  ** slip::Statistics<double> stat;
  ** slip::statistics(M.begin(),M.end(),stat);
  **std::cout<<"min, first quartile, median, third quartile, maximum,
  ** mean, standard deviation, skewness, kurtosis and cardinal"<<std::endl;
  ** std::cout<<stat.all()<<std::endl;
  ** \endcode
  */

template <typename InputIterator, typename T>
 void statistics(InputIterator first,
                 InputIterator last,
                 slip::Statistics<T>& statistic)
 
    {   
      std::size_t n = slip::cardinal<std::size_t>(first,last);
      slip::Array<typename std::iterator_traits<InputIterator>::value_type> 
	tmp(n,first,last);
      std::sort(tmp.begin(),tmp.end());

      
      statistic.min() = *tmp.begin();
      statistic.first_quartile() = *(tmp.begin()+n/4);
      statistic.median() = *(tmp.begin()+n/2);
      statistic.third_quartile()= *((tmp.end()-1)-n/4);
      statistic.max() = *(tmp.end() - 1);
      statistic.mean() = slip::mean<T>(first,last);
      statistic.std_dev()= slip::std_dev(first,last,statistic.mean());
      statistic.skewness() = slip::skewness(first,last,statistic.mean());
      statistic.kurtosis() = slip::kurtosis(first,last,statistic.mean());
      statistic.cardinal() = n; 

     }
  

/*!
  ** \brief compute all unbiased statistics of a range
  ** \author Mouhamed Hammoud <hammoud_AT_sic.univ-poitiers.fr>
  ** \date 2009/08/25
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first  An InputIterator.
  ** \param last   An InputIterator.
  ** \param statistic  slip::statistics
  ** \return a range which contains all its statistics
  ** \par Example:
  ** \code
  ** int f[]={6, 47, 49, 15, 43, 41, 7, 39, 43, 41, 36};
  ** slip::Vector<int> M(11,f);
  ** std::cout<<"M=\n"<<M<<std::endl;
  ** slip::Statistics<double> stat;
  ** slip::unbiased_statistics(M.begin(),M.end(),stat);
  **std::cout<<"min, first quartile, median, third quartile, maximum,
  ** mean, unbiased standard deviation,  unbiased skewness,  unbiased kurtosis and cardinal"<<std::endl;
  ** std::cout<<stat.all()<<std::endl;
  ** \endcode
  */

template <typename InputIterator, typename T>
 void unbiased_statistics(InputIterator first,
			  InputIterator last,
			  slip::Statistics<T>& statistic)
 
    {   
      std::size_t n = slip::cardinal<std::size_t>(first,last);
      slip::Array<typename std::iterator_traits<InputIterator>::value_type> 
	tmp(n,first,last);
      std::sort(tmp.begin(),tmp.end());

      
      statistic.min() = *tmp.begin();
      statistic.first_quartile() = *(tmp.begin()+n/4);
      statistic.median() = *(tmp.begin()+n/2);
      statistic.third_quartile()= *((tmp.end()-1)-n/4);
      statistic.max() = *(tmp.end() - 1);
      statistic.mean() = slip::mean<T>(first,last);
      statistic.std_dev()= slip::unbiased_std_dev(first,last,statistic.mean());
      statistic.skewness() = slip::unbiased_skewness(first,last,statistic.mean());
      statistic.kurtosis() = slip::unbiased_kurtosis(first,last,statistic.mean());
      statistic.cardinal() = n; 

     }
  


/*!
  ** \brief compute all statistics of a range according to a mask range.
  ** \author Mouhamed Hammoud <hammoud_AT_sic.univ-poitiers.fr>
  ** \date 2009/08/25
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first  An InputIterator.
  ** \param last   An InputIterator.
  ** \param mask_first  An InputIterator of the mask
  ** \param statistic slip::Statistics which contains all the statistics of the range.
  ** \param value Mask value.
  ** \par Example1:
  ** \code
  ** slip::Array2d<bool> Mask(4,5,true);
  ** for(std::size_t i = 0; i < M.dim1(); ++i)
  **   {
  ** 	Mask[i][0]=false;
  **   }  
  ** for(std::size_t j = 0; j < M.dim2(); ++j)
  **   {
  ** 	Mask[0][j]=false;
  **   }  
  **  std::cout << "Data : "<<std::endl<<M<<std::endl;
  **  std::cout << "Mask : "<< std::endl<<Mask<<std::endl;
  **  slip::Statistics<double> S;
  **  slip::statistics_mask(M.begin(),M.end(),Mask.begin(),S);
  **  std::cout<<"statistics mask"<<std::endl;
  **  std::cout<<S.all()<<std::endl;
  ** \endcode
  ** \par Example2:
  ** \code
  ** slip::Array2d<int> ValuedMask(4,5,2);
  ** for(std::size_t i = 0; i < M.dim1(); ++i)
  **   {
  ** 	ValuedMask[i][0]= 0;
  **  }  
  ** for(std::size_t j = 0; j < M.dim2(); ++j)
  **  {
  **	ValuedMask[0][j]=0;
  **  }  
  **
  **  std::cout << "Data : "<<std::endl<<M<<std::endl;
  **  std::cout << "ValuedMask : "<< std::endl<<ValuedMask<<std::endl;
  **  slip::Statistics<double> S;
  **  slip::statistics_mask(M.begin(),M.end(),ValuedMask.begin(),S,2);
  **  std::cout<<"statistics mask"<<std::endl;
  **  std::cout<<S.all()<<std::endl;
  ** \endcode
  */

  template <typename InputIterator, typename T, typename MaskIterator>
 void statistics_mask(InputIterator first,
		      InputIterator last,
		      MaskIterator mask_first, 
		      slip::Statistics<T>& statistic,
		      typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
 
    {   

       std::size_t n = slip::cardinal_mask<std::size_t>(mask_first,mask_first+(last-first),value);
       assert(n != 0);
    slip::Array<typename std::iterator_traits<InputIterator>::value_type> tmp(n);
    std::size_t count = 0;
    while(first != last)
      {
	if (*mask_first == value)
	  {
	    tmp[count] = *first;
	    count++;
	  }
	first++;
        mask_first++;

      }    

    std::sort(tmp.begin(),tmp.end());
     

      
    statistic.min() = *(tmp.begin());
      statistic.first_quartile() = *(tmp.begin()+n/4);
      statistic.median() = *(tmp.begin()+n/2);
      statistic.third_quartile()= *((tmp.end()-1)-n/4);
      statistic.max() = *(tmp.end() - 1);
      statistic.mean() = slip::mean<T>(tmp.begin(),tmp.end());
      statistic.std_dev()= slip::std_dev(tmp.begin(),tmp.end(),statistic.mean());
      statistic.skewness() = slip::skewness(tmp.begin(),tmp.end(),statistic.mean());
      statistic.kurtosis() = slip::kurtosis(tmp.begin(),tmp.end(),statistic.mean());
      statistic.cardinal() = n; 

     }


/*!
  ** \brief compute all unbiased statistics of a range according to a mask range.
  ** \author Mouhamed Hammoud <hammoud_AT_sic.univ-poitiers.fr>
  ** \date 2009/08/25
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first  An InputIterator.
  ** \param last   An InputIterator.
  ** \param mask_first  An InputIterator of the mask
  ** \param statistic slip::Statistics which contains all the unbiased statistics of the range.
  ** \param value Mask value.
   ** \par Example1:
  ** \code
  ** slip::Array2d<bool> Mask(4,5,true);
  ** for(std::size_t i = 0; i < M.dim1(); ++i)
  **   {
  ** 	Mask[i][0]=false;
  **   }  
  ** for(std::size_t j = 0; j < M.dim2(); ++j)
  **   {
  ** 	Mask[0][j]=false;
  **   }  
  **  std::cout << "Data : "<<std::endl<<M<<std::endl;
  **  std::cout << "Mask : "<< std::endl<<Mask<<std::endl;
  **  slip::Statistics<double> S;
  **  slip::unbiased_statistics_mask(M.begin(),M.end(),Mask.begin(),S);
  **  std::cout<<"unbiased statistics mask"<<std::endl;
  **  std::cout<<S.all()<<std::endl;
  ** \endcode
  ** \par Example2:
  ** \code
  ** slip::Array2d<int> ValuedMask(4,5,2);
  ** for(std::size_t i = 0; i < M.dim1(); ++i)
  **   {
  ** 	ValuedMask[i][0]= 0;
  **  }  
  ** for(std::size_t j = 0; j < M.dim2(); ++j)
  **  {
  **	ValuedMask[0][j]=0;
  **  }  
  **
  **  std::cout << "Data : "<<std::endl<<M<<std::endl;
  **  std::cout << "ValuedMask : "<< std::endl<<ValuedMask<<std::endl;
  **  slip::Statistics<double> S;
  **  slip::unbiased_statistics_mask(M.begin(),M.end(),ValuedMask.begin(),S,2);
  **  std::cout<<"unbiased statistics mask"<<std::endl;
  **  std::cout<<S.all()<<std::endl;
  ** \endcode
  */

  template <typename InputIterator, typename T, typename MaskIterator>
 void unbiased_statistics_mask(InputIterator first,
		      InputIterator last,
		      MaskIterator mask_first, 
		      slip::Statistics<T>& statistic,
		      typename std::iterator_traits<MaskIterator>::value_type value=typename std::iterator_traits<MaskIterator>::value_type(1))
 
    {   

       std::size_t n = slip::cardinal_mask<std::size_t>(mask_first,mask_first+(last-first),value);
       assert(n != 0);
    slip::Array<typename std::iterator_traits<InputIterator>::value_type> tmp(n);
    std::size_t count = 0;
    while(first != last)
      {
	if (*mask_first == value)
	  {
	    tmp[count] = *first;
	    count++;
	  }
	first++;
        mask_first++;

      }    

    std::sort(tmp.begin(),tmp.end());
     

      
    statistic.min() = *(tmp.begin());
      statistic.first_quartile() = *(tmp.begin()+n/4);
      statistic.median() = *(tmp.begin()+n/2);
      statistic.third_quartile()= *((tmp.end()-1)-n/4);
      statistic.max() = *(tmp.end() - 1);
      statistic.mean() = slip::mean<T>(tmp.begin(),tmp.end());
      statistic.std_dev()= slip::unbiased_std_dev(tmp.begin(),tmp.end(),statistic.mean());
      statistic.skewness() = slip::unbiased_skewness(tmp.begin(),tmp.end(),statistic.mean());
      statistic.kurtosis() = slip::unbiased_kurtosis(tmp.begin(),tmp.end(),statistic.mean());
      statistic.cardinal() = n; 

     }


/*!
  ** \brief compute all statistics of a range according to a preidcate.
  ** \author Mouhamed Hammoud <hammoud_AT_sic.univ-poitiers.fr>
  ** \date 2009/08/25
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first  An InputIterator.
  ** \param last   An InputIterator.
  ** \param statistic  slip::statistics
  ** \param pred a predicate function.
  ** \return a range which contains all its statistics
  ** \par Example:
  ** \code
  ** //definition of the predicate less than 10
  ** bool myPredicate (const double& val)
  ** {
  **   return (val<10);
  ** }
  ** \endcode
  ** \code
  ** slip::Statistics<double> S;
  ** slip::statistics_if(M.begin(),M.end(),S,myPredicate);
  ** std::cout<<"statistics using a predicate"<<std::endl;
  ** std::cout<<S.all()<<std::endl;
  ** \endcode
  */

  template <typename InputIterator, typename T, typename Predicate>
 void statistics_if(InputIterator first,
		    InputIterator last,
		    slip::Statistics<T>& statistic,
		    Predicate pred)
		    
    {   

       std::size_t n = slip::cardinal_if<std::size_t>(first,last,pred);
       assert(n != 0);
    slip::Array<typename std::iterator_traits<InputIterator>::value_type> tmp(n);
    std::size_t count = 0;
    while(first != last)
      {
	if (pred(*first))
	  {
	    tmp[count]=*first;
	    count++;
	}
	first++;
      }    

  
    std::sort(tmp.begin(),tmp.end());
     

      
    statistic.min() = *tmp.begin();
    statistic.first_quartile() = *(tmp.begin()+n/4);
    statistic.median() = *(tmp.begin()+n/2);
    statistic.third_quartile()= *((tmp.end()-1)-n/4);
    statistic.max() = *(tmp.end() - 1);
    statistic.mean() = slip::mean<T>(tmp.begin(),tmp.end());
    statistic.std_dev()= slip::std_dev(tmp.begin(),tmp.end(),statistic.mean());
    statistic.skewness() = slip::skewness(tmp.begin(),tmp.end(),statistic.mean());
    statistic.kurtosis() = slip::kurtosis(tmp.begin(),tmp.end(),statistic.mean());
    statistic.cardinal() = n; 

     }


/*!
  ** \brief compute all unbiased statistics of a range according to a preidcate.
  ** \author Mouhamed Hammoud <hammoud_AT_sic.univ-poitiers.fr>
  ** \date 2009/08/25
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first  An InputIterator.
  ** \param last   An InputIterator.
  ** \param statistic  slip::statistics
  ** \param pred a predicate function.
  ** \return a range which contains all its unbiased statistics
  ** \par Example:
  ** \code
  ** //definition of the predicate less than 10
  ** bool myPredicate (const double& val)
  ** {
  **   return (val<10);
  ** }
  ** \endcode
  ** \code
  ** slip::Statistics<double> S;
  ** slip::unbiased_statistics_if(M.begin(),M.end(),S,myPredicate);
  ** std::cout<<"unbiased_statistics using a predicate"<<std::endl;
  ** std::cout<<S.all()<<std::endl;
  ** \endcode
  */

  template <typename InputIterator, typename T, typename Predicate>
 void unbiased_statistics_if(InputIterator first,
		    InputIterator last,
		    slip::Statistics<T>& statistic,
		    Predicate pred)
		    
    {   

       std::size_t n = slip::cardinal_if<std::size_t>(first,last,pred);
       assert(n != 0);
    slip::Array<typename std::iterator_traits<InputIterator>::value_type> tmp(n);
    std::size_t count = 0;
    while(first != last)
      {
	if (pred(*first))
	  {
	    tmp[count]=*first;
	    count++;
	}
	first++;
      }    

  
    std::sort(tmp.begin(),tmp.end());
     

      
    statistic.min() = *tmp.begin();
    statistic.first_quartile() = *(tmp.begin()+n/4);
    statistic.median() = *(tmp.begin()+n/2);
    statistic.third_quartile()= *((tmp.end()-1)-n/4);
    statistic.max() = *(tmp.end() - 1);
    statistic.mean() = slip::mean<T>(tmp.begin(),tmp.end());
    statistic.std_dev()= slip::unbiased_std_dev(tmp.begin(),tmp.end(),statistic.mean());
    statistic.skewness() = slip::unbiased_skewness(tmp.begin(),tmp.end(),statistic.mean());
    statistic.kurtosis() = slip::unbiased_kurtosis(tmp.begin(),tmp.end(),statistic.mean());
    statistic.cardinal() = n; 

     }


/*!
   ** \brief Computes the covariance matrix Kxx of a data matrix X.
   ** \f[
   
      \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2016/04/24
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param Data A 2d Container (Matrix for example) of the data.
   ** \param CovMatrix The Covariance matrix of the data.
   ** \par Example:
   ** \code
   ** double data[] = {1,    7,    3,    4,
   **                  5,    4,    7,    8,
   **		       9,   20,    1,   12};
   ** slip::Matrix<double> A(3,4,data,data+12);
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Matrix<double> CovA(A.cols(),A.cols());
   ** slip::covariance_matrix(A,CovA);
   ** std::cout<<"CovA = \n"<<CovA<<std::endl;
   ** \endcode
   */
  template <typename Container2d1,
	    typename Container2d2>
  void covariance_matrix(const Container2d1& Data,
			 Container2d2& CovMatrix)
  {
    typedef typename Container2d1::value_type value_type1;
    typedef typename Container2d2::value_type value_type2;

    const typename Container2d1::size_type rows = Data.rows();
    const typename Container2d1::size_type cols = Data.cols();
    Container2d1 Centerdata(rows,cols);
    CovMatrix.resize(cols,cols);
    //center Data
    for (std::size_t j = 0; j < cols; ++j)
      {
	slip::center(Data.col_begin(j),Data.col_end(j),Centerdata.col_begin(j));
		
      }
    for(std::size_t i = 0; i < cols; ++i)
      {
	for(std::size_t j = 0; j < i; ++j)
	  {
	    CovMatrix[i][j] = static_cast<value_type2>(slip::covariance(Centerdata.col_begin(i),Centerdata.col_end(i),Centerdata.col_begin(j),value_type1(),value_type2()));
	    CovMatrix[j][i] = CovMatrix[i][j];
	  }
      }
    for(std::size_t i = 0; i < cols; ++i)
      {
	CovMatrix[i][i] = static_cast<value_type2>(slip::covariance(Centerdata.col_begin(i),Centerdata.col_end(i),Centerdata.col_begin(i),value_type1(),value_type2()));
      }
  }

 /*!
   ** \brief Computes the unbiased covariance matrix of data matrix.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2016/04/24
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param Data A 2d Container (Matrix for example) of the data.
   ** \param CovMatrix The unbiased Covariance matrix of the data.
   ** \par Example:
   ** \code
   ** double data[] = {1,    7,    3,    4,
   **                  5,    4,    7,    8,
   **		       9,   20,    1,   12};
   ** slip::Matrix<double> A(3,4,data,data+12);
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Matrix<double> UnCovA(A.cols(),A.cols());
   ** slip::unbiased_covariance_matrix(A,UnCovA);
   ** std::cout<<"unbiased CovA = \n"<<UnCovA<<std::endl;
   ** \endcode
   */

   template <typename Container2d1,
	    typename Container2d2>
  void unbiased_covariance_matrix(const Container2d1& Data,
                                  Container2d2& CovMatrix)
  {
    typedef typename Container2d1::value_type value_type1;
    typedef typename Container2d2::value_type value_type2;

    const typename Container2d1::size_type rows = Data.rows();
    const typename Container2d1::size_type cols = Data.cols();
    Container2d1 Centerdata(rows,cols);
    CovMatrix.resize(cols,cols);
    //center Data
    for (std::size_t j = 0; j < cols; ++j)
      {
	slip::center(Data.col_begin(j),Data.col_end(j),Centerdata.col_begin(j));
		
      }
    for(std::size_t i = 0; i < cols; ++i)
      {
	for(std::size_t j = 0; j < i; ++j)
	  {
	    CovMatrix[i][j] = static_cast<value_type2>(slip::unbiased_covariance(Centerdata.col_begin(i),Centerdata.col_end(i),Centerdata.col_begin(j),value_type1(),value_type2()));
	    CovMatrix[j][i] = CovMatrix[i][j];
	  }
      }
    for(std::size_t i = 0; i < cols; ++i)
      {
	CovMatrix[i][i] = static_cast<value_type2>(slip::unbiased_covariance(Centerdata.col_begin(i),Centerdata.col_end(i),Centerdata.col_begin(i),value_type1(),value_type2()));
      }
  }
  
  

  
 /** \name recursive statistics algorithms */
  /* @{ */
 /*!
   ** \brief Recursive mean algorithm.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2012/04/24
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param n Current element index.
   ** \param prev_mean Previous mean value.
   ** \param xnp1 Next value of the serie.
   ** \return the next mean value.
   ** \par Example:
   ** \code
   ** slip::Array<int> x(8);
   ** slip::iota(x.begin(),x.end(),1,1);
   ** double mu = static_cast<double>(x[0]);
   ** std::cout<<"mu = "<<mu<<std::endl;
   ** for(int i = 1; i < 8; ++i)
   ** {
   **  mu = slip::mean_next(i,mu,x[i]);
   **  std::cout<<"mu = "<<mu<<std::endl;
   ** }
   ** \endcode
   */
  template<typename DataT,
	   typename MeanT>
  MeanT mean_next(const int n, 
		  const MeanT& prev_mean,
		  const DataT& xnp1)
  {
    MeanT an = static_cast<MeanT>(1)/static_cast<MeanT>(n+1);
    MeanT bn = static_cast<MeanT>(n)*an;
    return bn * prev_mean + an * xnp1;
  }
  

   /*!
   ** \brief Recursive variance algorithm.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2012/04/24
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param n Current element index.
   ** \param prev_mean Previous mean value.
   ** \param xnp1 Next value of the serie.
   ** \return the next mean value.
   ** \par Example:
   ** \code
   ** slip::Array<int> x(8);
   ** slip::iota(x.begin(),x.end(),1,1);
   ** double mu = static_cast<double>(x[0]);
   ** double var = 0.0;
   ** std::cout<<"mu = "<<mu<<" var = "<<var<<std::endl;
   ** for(int i = 1; i < 8; ++i)
   ** {
   **  slip::var_next(i,mu,var,x[i],mu,var);
   **  std::cout<<"mu = "<<mu<<" var = "<<var<<std::endl;
   ** }
   ** std::cout<<"slip::mean = "<<slip::mean<double>(x.begin(),x.end())<<std::endl;
   ** std::cout<<"slip::variance = "<<slip::variance(x.begin(),x.end(),slip::mean<double>(x.begin(),x.end()))<<std::endl;
   ** \endcode
   */
  template<typename DataT,
	   typename VarT>
  void var_next(const int n, 
		const VarT& prev_mean,
		const VarT& prev_var,
		const DataT& xnp1,
		VarT& next_mean,
		VarT& next_var)
  {
    VarT an = static_cast<VarT>(1)/static_cast<VarT>(n+1);
    VarT bn = static_cast<VarT>(n)*an;
    VarT cn = bn * an;
    VarT s  = (static_cast<VarT>(xnp1) - prev_mean);
    next_mean = bn * prev_mean + an * static_cast<VarT>(xnp1);
    next_var =  bn * prev_var + cn * (s * s);
  }
/* @} */
  
}//slip::

#endif //SLIP_STATISTICS_HPP
