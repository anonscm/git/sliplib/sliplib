/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file window.hpp
 * 
 * \brief Provides some algorithms to generate filtering windows.
 * \since 1.5.0
 * 
 */

#ifndef SLIP_WINDOW_HPP
#define SLIP_WINDOW_HPP

#include <iostream>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cassert>
#include <cmath>
#include <set>
#include <vector>
#include "Array.hpp"
#include "Matrix.hpp"
#include "Matrix3d.hpp"
#include "macros.hpp"
#include "arithmetic_op.hpp"
#include "linear_algebra.hpp"


namespace slip
{

  /**
   ** \brief functor to compute the symmetric Hamming window  
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2010/10/07
   ** \version 0.0.2
   ** \since 1.5.0
   ** \return Hamming window value
   ** \par Description
   ** The value is computed from the following equation:
   ** \f$ 0.54-0.46\cos(2\pi\frac{t}{T}), 0 \leq t \leq T, T=m-1 \f$ 
   ** \par Example:
   **\code
   ** slip::Array<double> hamming(10);
   ** slip::Hamming<double> hamming_fun(10);
   ** slip::window(hamming.begin(),hamming.end(),
   **		   hamming_fun);
   **
   ** \endcode
   */
  template<typename T = double>
  struct Hamming
  {
    Hamming(const std::size_t& m):m_(m)
    {}
    
    T operator()(const std::size_t& i) const 
    {
      return (static_cast<T>(0.54)-(static_cast<T>(0.46)*std::cos((static_cast<T>(2.0)*slip::constants<T>::pi())*(static_cast<T>(i)/static_cast<T>(m_-1)))));
    }
    
    void set_size(const std::size_t& size)
    {
      m_ = size;
    }
    std::size_t m_;
  };


  /**
   ** \brief functor to compute the symmetric Hanning window  
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2010/10/07
   ** \version 0.0.2
   ** \since 1.5.0
   ** \return Hanning window value
   ** \par Description
   ** The value is computed from the following equation:
   ** \f$ 0.5(1-cos(2\pi\frac{t}{T})), 0 \leq t \leq T, T=m-1 \f$ 
   ** \par Example:
   **\code
   ** slip::Array<double> hanning(10);
   ** slip::Hamming<double> hanning_fun(hanning.size());
   ** slip::window(hanning.begin(),hanning.end(),hanning_fun);
   ** std::cout<<"hanning(10) = \n"<<hanning<<std::endl;
   ** \endcode
   */
  template<typename T=double>
  struct Hanning
  {
    Hanning(const std::size_t& m):m_(m)
    {}
    T operator()(const std::size_t& i) const
    {
      return static_cast<T>(0.5)*(static_cast<T>(1.0)-std::cos((static_cast<T>(2.0)*slip::constants<T>::pi())*(static_cast<T>(i)/static_cast<T>(m_-1))));
    
  
    }

    void set_size(const std::size_t& size)
    {
      m_ = size;
    }
    
    std::size_t m_;
  };

/**
   ** \brief functor to compute the Bartlett window  
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr> 
   ** \date 2010/10/07
   ** \version 0.0.2
   ** \since 1.5.0
   ** \return Bartlett window value.
   ** \par Description
   ** The value is computed from the following equation:
   \f$ \left\{
        \begin{array}{ll}
        \frac{2t}{T} & 0\leq t \leq \frac{T}{2} \\
        2-\frac{2t}{T} & \frac{T}{2}\leq t \leq T
    \end{array}
   \right.\f$ 
   ** \par Example:
   **\code
   ** slip::Array<double> bartlett(10);
   ** slip::Bartlett<double> bartlett_fun(bartlett.size());
   ** slip::window(bartlett.begin(),bartlett.end(),bartlett_fun);
   ** std::cout<<"bartlett(10) = \n"<<bartlett<<std::endl;
   ** \endcode
   */

template<typename T=double>
struct Bartlett
{
  Bartlett(const std::size_t& m):m_(m)
     {}
  T operator()(const std::size_t& i) const
  {
    if(i <= ((m_-1)/2))
      {
	return static_cast<T>(2.0)*(static_cast<T>(i)/static_cast<T>(m_-1));
      }
    return static_cast<T>(2.0)- static_cast<T>(2.0)*(static_cast<T>(i)/static_cast<T>(m_-1));
  
  }

   void set_size(const std::size_t& size)
    {
      m_ = size;
    }
  std::size_t m_;
};

/**
   ** \brief functor to compute the Blackman window  
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr> : conceptor
   ** \date 2010/10/07
   ** \version 0.0.2
   ** \return Blackman window value 
   ** \par Description
   ** The value is computed from the following equation:
   ** \f$ 0.42-0.5\cos(2\pi\frac{t}{T})+0.08\cos(4\pi\frac{t}{T}), 0 \leq t\leq T, T=m-1 \f$ 
   ** \par Example:
   ** \code
   ** slip::Array<double> blackman(10);
   ** slip::Blackman<double> blackman_fun(blackman.size());
   ** slip::window(blackman.begin(),blackman.end(),blackman_fun);
   ** std::cout<<"blackman(10) = \n"<<blackman<<std::endl;
   ** \endcode
   */
template<typename T=double>
struct Blackman
{
  Blackman(const std::size_t& m):m_(m)
     {}
  T operator()(const std::size_t& i) const
  {
    //    assert((int)i>=0);
    T result = T(0);
    if(m_ == 1)
      {
	result = static_cast<T>(1);
      }
    else
      {
	result = static_cast<T>(0.42) 
      - static_cast<T>(0.5) *std::cos((static_cast<T>(2.0)*slip::constants<T>::pi())*(static_cast<T>(i)/static_cast<T>(m_-1)))
      + static_cast<T>(0.08)*std::cos((static_cast<T>(4.0)*slip::constants<T>::pi())*(static_cast<T>(i)/static_cast<T>(m_-1)));
      }
    return result;
  }
  
  void set_size(const std::size_t& size)
  {
    m_ = size;
  }
  
  std::size_t m_;
};

 

  /**
   ** \brief compute the window  
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2010/10/07
   ** \version 0.0.2
   ** \param fisrt  inputIterator
   ** \param last  inputIterator
   ** \param func  functor window 
   ** \par Example:
   ** \code
   ** slip::Array<double> hamming(10);
   ** slip::Hamming<double> hamming_fun(10);
   ** slip::window(hamming.begin(),hamming.end(),hamming_fun);
   ** std::cout<<"hamming(10) = \n"<<hamming<<std::endl;
   ** \endcode
   */
template<typename RandomAccessIterator,
	 typename window_functor>
void  window1d(RandomAccessIterator first,
	       RandomAccessIterator last,
	       window_functor func)
{
  
  std::size_t size = static_cast<std::size_t>(last - first);
  for(std::size_t i = 0; i < size; ++i)
    {
     *(first+i) = func(i);
    }
 }


  
/**
 ** \brief Return the filter coefficients of a Hamming window of length (last-first).
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2010/10/07
 ** \version 0.0.1
 ** \param first  RandomAccessIterator
 ** \param last  RandomAccessIterator
 ** \par Example:
 ** \code
 ** slip::Array<double> hamming(10);
 ** slip::hamming(hamming.begin(),hamming.end());
 ** std::cout<<"hamming(10) = \n"<<hamming<<std::endl;
 ** \endcode
 */
template<typename RandomAccessIterator>

void  hamming(RandomAccessIterator first,
	      RandomAccessIterator last)
{
  typedef typename std::iterator_traits<RandomAccessIterator>::value_type T;
  slip::Hamming<T> hamming_fun(static_cast<size_t>(last-first));
  slip::window1d(first,last,hamming_fun);
}

/**
 ** \brief Return the filter coefficients of a Hanning window of length (last-first).
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2010/10/07
 ** \version 0.0.1
 ** \param first  RandomAccessIterator
 ** \param last  RandomAccessIterator
 ** \par Example:
 ** \code
 ** slip::Array<double> hanning(10);
 ** slip::hanning(hanning.begin(),hanning.end());
 ** std::cout<<"hanning(10) = \n"<<hanning<<std::endl;
 ** \endcode
 */
template<typename RandomAccessIterator>

void  hanning(RandomAccessIterator first,
	      RandomAccessIterator last)
{
  typedef typename std::iterator_traits<RandomAccessIterator>::value_type T;
  slip::Hanning<T> hanning_fun(static_cast<size_t>(last-first));
  slip::window1d(first,last,hanning_fun);
}

/**
 ** \brief Return the filter coefficients of a Bartlett window of length (last-first).
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2010/10/07
 ** \version 0.0.1
 ** \param first  RandomAccessIterator
 ** \param last  RandomAccessIterator
 ** \par Example:
 ** \code
 ** slip::Array<double> bartlett(10);
 ** slip::bartlett(hamming.begin(),bartlett.end());
 ** std::cout<<"bartlett(10) = \n"<<bartlett<<std::endl;
 ** \endcode
 */
template<typename RandomAccessIterator>

void  bartlett(RandomAccessIterator first,
	       RandomAccessIterator last)
{
  typedef typename std::iterator_traits<RandomAccessIterator>::value_type T;
  slip::Bartlett<T> bartlett_fun(static_cast<size_t>(last-first));
  slip::window1d(first,last,bartlett_fun);
}

/**
 ** \brief Return the filter coefficients of a Blackman window of length (last-first).
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2010/10/07
 ** \version 0.0.1
 ** \param first  RandomAccessIterator
 ** \param last  RandomAccessIterator
 ** \par Example:
 ** \code
 ** slip::Array<double> blackman(10);
 ** slip::blackman(hamming.begin(),blackman.end());
 ** std::cout<<"blackman(10) = \n"<<blackman<<std::endl;
 ** \endcode
 */
template<typename RandomAccessIterator>

void  blackman(RandomAccessIterator first,
	       RandomAccessIterator last)
{
  typedef typename std::iterator_traits<RandomAccessIterator>::value_type T;
  slip::Blackman<T> blackman_fun(static_cast<size_t>(last-first));
  slip::window1d(first,last,blackman_fun);

}


/**
 ** \brief Return the filter coefficients of a sinc window of length (last-first).
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2010/10/07
 ** \version 0.0.1
 ** \param first  RandomAccessIterator
 ** \param last  RandomAccessIterator
 ** \par Example:
 ** \code
 ** slip::Array<double> sincc(7);
 ** slip::sinc_window(sincc.begin(),sincc.end());
 ** std::cout<<"sinc_window = \n"<<sincc<<std::endl;
 ** \endcode
 */
template<typename RandomAccessIterator>

void  sinc_window(RandomAccessIterator first,
		  RandomAccessIterator last)
{
  typedef typename std::iterator_traits<RandomAccessIterator>::value_type T;
  slip::Array<double> x(static_cast<std::size_t>(last-first));
  slip::iota(x.begin(),x.end(),-static_cast<double>(x.size())/static_cast<double>(2),static_cast<double>(x.size())/static_cast<double>(x.size()-1));
  std::transform(x.begin(),x.end(),first,slip::sinc<T>());
}


  /**
   ** \brief compute a 2d window
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2010/10/07
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param fisrt  RandomAccessIterator2d.
   ** \param last  RandomAccessIterator2d.
   ** \param func  window functor.
   ** \par Example:
   ** \code
   ** slip::Array2d<double> hamming2d(10,10);
   ** slip::Hamming<double> hamming_fun(10);
   ** slip::window2d(hamming2d.upper_left(),hamming2d.bottom_right(),
   **		         hamming_fun);
   ** std::cout<<"hamming2d = \n"<<hamming2d<<std::endl;
   ** \endcode
   */
template<typename RandomAccessIterator2d,
	 typename window_functor>
void  window2d(RandomAccessIterator2d up,
	       RandomAccessIterator2d bot,
	       window_functor func)
{
  typedef typename std::iterator_traits<RandomAccessIterator2d>::value_type T;
  const std::size_t rows = static_cast<std::size_t>((bot-up)[0]);
  const std::size_t cols = static_cast<std::size_t>((bot-up)[1]);

  func.set_size(rows);
  slip::Array<T> A_rows(rows);
  slip::window1d(A_rows.begin(),A_rows.end(),func);
  func.set_size(cols);
  slip::Array<T> A_cols(cols);
  slip::window1d(A_cols.begin(),A_cols.end(),func);
  
  //Tensor product
  slip::outer_product(A_rows.begin(),A_rows.end(),
		      A_cols.begin(),A_cols.end(),
		      up,bot);
 }

 /**
   ** \brief compute a 3d window
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2010/10/07
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param fup  RandomAccessIterator3d.
   ** \param bbr  RandomAccessIterator3d.
   ** \param func  window functor.
   ** \par Example:
   ** \code
   ** slip::Array3d<double> hamming3d(6,6,6);
   ** slip::Hamming<double> hamming_fun(6);
   ** slip::window3d(hamming3d.front_upper_left(),hamming3d.back_bottom_right(), hamming_fun);
   ** std::cout<<"hamming3d = \n"<<hamming3d<<std::endl;
   ** \endcode
   */
template<typename RandomAccessIterator3d,
	 typename window_functor>
void  window3d(RandomAccessIterator3d fup,
	       RandomAccessIterator3d bbr,
	       window_functor func)
{
  typedef typename std::iterator_traits<RandomAccessIterator3d>::value_type T;
  const std::size_t slices = static_cast<std::size_t>((bbr-fup)[0]);
  const std::size_t rows   = static_cast<std::size_t>((bbr-fup)[1]);
  const std::size_t cols   = static_cast<std::size_t>((bbr-fup)[2]);

  func.set_size(slices);
  slip::Array<T> A_slices(slices);
  slip::window1d(A_slices.begin(),A_slices.end(),func);
  func.set_size(rows);
  slip::Array<T> A_rows(rows);
  slip::window1d(A_rows.begin(),A_rows.end(),func);
  func.set_size(cols);
  slip::Array<T> A_cols(cols);
  slip::window1d(A_cols.begin(),A_cols.end(),func);
  
  //Tensor product
  slip::rank1_tensorial_product(A_slices.begin(),A_slices.end(),
				A_rows.begin(),A_rows.end(),
				A_cols.begin(),A_cols.end(),
				fup,bbr);
 }



  template<typename>
  struct __window
       {
    template<typename RandomAccessIterator1, 
	     typename WindowFunctor>
    static void 
   window(RandomAccessIterator1 first,
	  RandomAccessIterator1 last,
	  WindowFunctor win_func)
    {
      std::clog<<"generic window"<<std::endl;
    }

};


  template<>
  struct __window<std::random_access_iterator_tag>
  {
  
    template<typename RandomAccessIterator1, 
	     typename WindowFunctor>
    static void 
    window(RandomAccessIterator1 in_first,
	   RandomAccessIterator1 in_last,
	   WindowFunctor win_func)
    {
      std::clog<<"window 1d"<<std::endl;
      slip::window1d(in_first,in_last,win_func);
    }
  };

   template<>
  struct __window<std::random_access_iterator2d_tag>
  {
  
    template<typename RandomAccessIterator1, 
	     typename WindowFunctor>
    static void 
    window(RandomAccessIterator1 in_first,
	   RandomAccessIterator1 in_last,
	   WindowFunctor win_func)
    {
      std::clog<<"window 2d"<<std::endl;
      slip::window2d(in_first,in_last,win_func);
    }
  };

  template<>
  struct __window<std::random_access_iterator3d_tag>
  {
  
    template<typename RandomAccessIterator1, 
	     typename WindowFunctor>
    static void 
    window(RandomAccessIterator1 in_first,
	   RandomAccessIterator1 in_last,
	   WindowFunctor win_func)
    {
      std::clog<<"window 3d"<<std::endl;
      slip::window3d(in_first,in_last,win_func);
    }
  };

 /**
  ** \brief Computes a generic nd window according to the iterator category (dimension) 1d, 2d or 3d.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2024/04/10
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param in_first  RandomAccessIterator.
   ** \param in_last  RandomAccessIterator.
   ** \param func  window functor.
   ** \par Example:
   ** \code
   ** slip::Array3d<double> hamming3d(6,6,6);
   ** slip::Hamming<double> hamming_fun(6);
   ** slip::window(hamming3d.front_upper_left(),hamming3d.back_bottom_right(), hamming_fun);
   ** std::cout<<"hamming3d = \n"<<hamming3d<<std::endl;
   ** \endcode
   */
 template<typename RandomAccessIterator1, 
	  typename WindowFunctor>
    void window(RandomAccessIterator1 in_first,
		RandomAccessIterator1 in_last,
		WindowFunctor win_func)
   {
     typedef typename std::iterator_traits<RandomAccessIterator1>::iterator_category _Category;
     slip::__window<_Category>::window(in_first,in_last,win_func);
   }

}//::slip
#endif// SLIP_WINDOW
