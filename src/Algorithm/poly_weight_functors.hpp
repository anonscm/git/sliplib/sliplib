/** 
 * \file poly_weight_functors.hpp
 * 
 * \brief Provides some polynomial weight functors.
 * 
 */

#ifndef SLIP_POLY_WEIGHT_FUNCTORS_HPP
#define SLIP_POLY_WEIGHT_FUNCTORS_HPP

#include <iostream>
#include <cmath>
#include "macros.hpp"
#include "utils_compilation.hpp"

namespace slip
{
  /**
   ** \brief functor to compute the Legendre weight.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2010/11/22
   ** \version 0.0.1
   ** \return 1
   ** \par Example:
   **\code
   ** slip::Array<double> legendre_w(10);
   ** slip::Array<double> x(10);
   ** slip::uniform_collocations(x.begin(),x.end(),-1.0,1.0);
   ** std::transform(x.begin(),x.end(),
   **	             legendre_w.begin(),
   **	             slip::LegendreWeight<double>());
   ** std::cout<<"legendre_w = \n"<<legendre_w<<std::endl;
   ** \endcode
   */
  template<typename T>
  struct LegendreWeight:public std::unary_function<T,T>
  {

    LegendreWeight():w_(static_cast<T>(1))
    {}

    T operator()(const T& UNUSED(x)) const
    {
      return w_;
    }
    
    std::string name() const
    {
      return "Legendre weight";
    }

    T w_;
  };
 /**
   ** \brief functor to compute the Gram or Discrete Chebychev weight.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2017/09/12
   ** \version 0.0.1
   ** \return 1
   ** \par Example:
   **\code
   ** slip::Array<double> gram_w(10);
   ** slip::Array<double> x(10);
   ** slip::gram_uniform_collocations(x.begin(),x.end(),-1.0,1.0);
   ** std::transform(x.begin(),x.end(),
   **	             gram_w.begin(),
   **	             slip::GramWeight<double>(x.size()));
   ** std::cout<<"gram_w = \n"<<gram_w<<std::endl;
   ** \endcode
   */
  template<typename T>
  struct GramWeight:public std::unary_function<T,T>
  {

    GramWeight():w_(static_cast<T>(1))
    {}

    GramWeight(const std::size_t& range_size):w_(slip::constants<T>::one()/static_cast<T>(range_size))
    {}

    T operator()(const T& UNUSED(x)) const
    {
      return w_;
    }
    
    std::string name() const
    {
      return "Gram weight";
    }

    T w_;
  };


   /**
   ** \brief functor to compute the Chebyshev (first kind) weight:
   **  \f[ w(x) = \frac{1}{\sqrt{1-x^2}}\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2010/11/22
   ** \version 0.0.1
   ** \return \f[ w(x) = \frac{1}{\sqrt{1-x^2}}\f]
   ** \par Example:
   **\code
    ** slip::Array<double> chebyshev_w(10);
   ** slip::Array<double> x(10);
   ** slip::uniform_collocations(x.begin(),x.end(),-1.0,1.0);
   ** std::transform(x.begin(),x.end(),
   **	             chebyshev_w.begin(),
   **	             slip::ChebyshevWeight<double>());
   ** std::cout<<"chebyshev_w = \n"<<chebyshev_w<<std::endl;
   ** \endcode
   */
  template<typename T>
  struct ChebyshevWeight:public std::unary_function<T,T>
  {

    T operator()(const T& x) const
    {
      return slip::constants<T>::one()/
	std::sqrt(static_cast<T>(1.00001) - x*x);
    }

    std::string name() const
    {
      return "Chebyshev weight";
    }

  };


  
   /**
   ** \brief functor to compute the Chebyshev (second kind) weight:
   **  \f[ w(x) = \sqrt{1-x^2}\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2010/11/22
   ** \version 0.0.1
   ** \return \f[ w(x) = \sqrt{1-x^2}\f]
   ** \par Example:
   **\code
   ** slip::Array<double> chebyshevII_w(10);
   ** slip::Array<double> x(10);
   ** slip::uniform_collocations(x.begin(),x.end(),-1.0,1.0);
   ** std::transform(x.begin(),x.end(),
   **	             chebyshevII_w.begin(),
   **	             slip::ChebyshevIIWeight<double>());
   ** std::cout<<"chebyshevII_w = \n"<<chebyshevII_w<<std::endl;
   ** \endcode
   */
  template<typename T>
  struct ChebyshevIIWeight:public std::unary_function<T,T>
  {

    T operator()(const T& x) const
    {
      return std::sqrt(slip::constants<T>::one() - x*x);
    }
 
    std::string name() const
    {
      return "Chebyshev second kind weight";
    }
  };

  /**
   ** \brief functor to compute the Laguerre weight:
   **  \f[ w(x) = \exp(-x)\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2010/11/22
   ** \version 0.0.1
   ** \return \f[ w(x) = \exp(-x)\f]
   ** \par Example:
   **\code
    ** slip::Array<double> laguerre_w(10);
   ** slip::Array<double> x(10);
   ** slip::uniform_collocations(x.begin(),x.end(),-1.0,1.0);
   ** std::transform(x.begin(),x.end(),
   **	             laguerre_w.begin(),
   **	             slip::LaguerreWeight<double>());
   ** std::cout<<"laguerre_w = \n"<<laguerre_w<<std::endl;
   ** \endcode
   */
  template<typename T>
  struct LaguerreWeight:public std::unary_function<T,T>
  {

    T operator()(const T& x) const
    {
      return std::exp(-x);
    }

    std::string name() const
    {
      return "Laguerre weight";
    }

  };

  /**
   ** \brief functor to compute the Hermite weight:
   **  \f[ w(x) = \exp(-x)\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2010/11/22
   ** \version 0.0.1
   ** \return \f[ w(x) = \exp(-x)\f]
   ** \par Example:
   **\code
    ** slip::Array<double> hermite_w(10);
   ** slip::Array<double> x(10);
   ** slip::uniform_collocations(x.begin(),x.end(),-1.0,1.0);
   ** std::transform(x.begin(),x.end(),
   **	             hermite_w.begin(),
   **	             slip::HermiteWeight<double>());
   ** std::cout<<"hermite_w = \n"<<hermite_w<<std::endl;
   ** \endcode
   */
  template<typename T>
  struct HermiteWeight:public std::unary_function<T,T>
  {

    T operator()(const T& x) const
    {
      return std::exp(-slip::constants<T>::half()*(x*x));
    }

    std::string name() const
    {
      return "Hermite weight";
    }

  };

}//::slip

#endif //SLIP_POLY_WEIGHT_FUNCTORS_HPP
