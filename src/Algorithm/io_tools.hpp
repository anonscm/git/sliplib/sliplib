/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file io_tools.hpp
 * 
 * \brief Provides some input/ouput algorithms.
 * \since 1.0.0
 */
#ifndef SLIP_IO_TOOLS_HPP
#define SLIP_IO_TOOLS_HPP

#include <ios>
#include <stdexcept>
#include <iostream>
#include <iterator>
#include <iomanip>
#include <sstream>
#include <string>
#include <fstream>
#include <algorithm>
#include "error.hpp"
//read/write image with ImageMagick
//
// -> Adding the macro condition HAVE_MAGICK for
// breaking the ImageMagick dependency
// Author : denis.arrivault\AT\inria.fr
// Date : 2013/02/06
// \since 1.4.0
#ifdef HAVE_MAGICK
#include <wand/magick_wand.h> 
#include <magick/api.h>
#endif

#include <cstdio>
#include <cstdlib>
#include "Array.hpp"
#include "Matrix.hpp"
#include "Point2d.hpp"
#include "Point3d.hpp"
#include "stride_iterator.hpp"
#include <boost/tokenizer.hpp>


#ifdef HAVE_MAGICK
//Macro for ImageMagick Input/Output exceptions
#define ThrowWandException(wand) \
{ \
  char \
    *description; \
 \
  ExceptionType \
    severity; \
 \
  description=MagickGetException(wand,&severity); \
  (void) fprintf(stderr,"%s %s %lu %s\n",GetMagickModule(),description); \
  description=(char *) MagickRelinquishMemory(description); \
  exit(-1); \
}
#endif//HAVE_MAGICK

namespace slip
{

  /** \name Miscellaneous algorithms */
  /* @{ */
 
  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2009/03/27
  ** \since 1.0.0
  ** \brief Split a file_name in two substrings: the file_path_name string
  ** without the extension and the extension string.
  ** \param file_path_name String of the file path name
  ** \param file_path_name_cut String of the file path name cut
  ** \param ext extension string.
  ** \return size_t indicating the position of the . of the extension.
  ** \par Example1:
  ** \code
  ** std::string ext;
  ** std::string file_path_cut;
  ** std::size_t pos = slip::split_extension("/toto/titi/tutu000_a.tiff",file_path_cut,ext);
  ** std::cout<<"file_path_cut = "<<file_path_cut<<std::endl;
  ** std::cout<<"ext = "<<ext<<std::endl;
  ** std::cout<<"pos = "<<pos<<std::endl;
  ** //will print:
  ** //file_path_cut = /toto/titi/tutu000_a
  ** //ext = .tiff
  ** //pos = 20
  ** \endcode
   */
  inline
  std::size_t split_extension(const std::string& file_path_name, 
		       std::string& file_path_name_cut,
		       std::string& ext)
  {
    std::size_t pos_point = file_path_name.find_last_of(".");
    ext = file_path_name.substr(pos_point,file_path_name.length() - 1);
    file_path_name_cut = file_path_name.substr(0,pos_point);
    return pos_point;
  }
 
  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2009/03/27
  ** \since 1.0.0
  ** \brief Compose a file_name from the file name format 
  **        basename%nd*.ext
  ** \param file_path_name String of the file path name
  ** \param img Number to format.
  ** \return the resulting string
  ** \remarks This function substitute the last 3 characters length
  ** beginning by '%' by img formated according to n. 
  **
  ** 
  ** \par Example1:
  ** \code
  ** //print toto001.bmp, toto002.bmp..., toto005.bmp
  ** for(std::size_t img = 1; img < 5; ++img)
  **  {
  **    std::cout<<slip::compose_file_name("toto%3d.bmp",img)<<std::endl;
  **  }
  **
  ** \endcode
  ** \par Example2:
  ** \code
  ** //print toto001_a.bmp, toto002_a.bmp..., toto005_a.bmp
  ** for(std::size_t img = 1; img < 5; ++img)
  **  {
  **    std::cout<<slip::compose_file_name("toto%3d_a.bmp",img)<<std::endl;
  **  }
  **
  ** \endcode
  */
  inline
  std::string compose_file_name(const std::string& file_path_name,
				const std::size_t img)
  {
    
    std::size_t pos_purcent = file_path_name.find_last_of("%");
    std::size_t pos_after_purcent = pos_purcent + 3;
    std::size_t format_lenght = (std::size_t)atoi(&file_path_name[pos_purcent+1]);
    std::string base_name = file_path_name.substr(0,pos_purcent);
    std::string extension = file_path_name.substr(pos_after_purcent,file_path_name.length()-1);
       
    std::ostringstream ost;
    ost.width(format_lenght);
    ost.fill('0');
    ost<<img;
    return (base_name+ost.str())+extension;
  }

 

 
  
  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2009/03/26
  ** \since 1.0.0
  ** \brief Print the name of the container follow by the values of 
  ** the container in a terminal.
  ** \param C Container to print.
  ** \param name Name of the container to print
  ** \par Example:
  ** \code
  ** slip::ColorImage<double> I(4,5);
  ** slip::iota(I.begin(),I.end(),1.0);
  ** slip::print(I,"I");
  ** \endcode
  ** \par Example:
  ** \code
  ** float f = 1.2f
  ** slip::print(f,"I");
  ** \endcode
  */
  template <typename Container>
  void print(const Container& C, const std::string& name = "")
  {
    if(name != "")
      {
	std::cout<<name<<" = "<<std::endl;
      }
    std::cout<<C<<std::endl;
  }

 



 /* @} */

   /** \name Raw input/output algorithms */
  /* @{ */
  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/14
  ** \since 1.0.0
  ** \brief Write a Container to an raw file.
  ** \param first InputIterator. 
  ** \param last  InputIterator.
  ** \param file_path_name String of the file path name
  **
  ** \par Example:
  ** \code
  ** int tabi[] = {1,2,3,4,5,6};
  ** slip::Matrix<int> Mi(2,3,tabi);
  ** std::cout<<Mi<<std::endl;
  ** slip::write_raw(Mi.begin(),Mi.end(),"Mi.raw");
  **
  **  slip::GrayscaleImage<int> Mi2(2,3);
  ** slip::read_raw("Mi.raw",Mi2.begin(),Mi2.end());
  ** std::cout<<Mi2<<std::endl;
  ** slip::Array<int> AA(6);
  ** slip::read_raw("Mi.raw",AA.begin(),AA.end());
  ** std::cout<<AA<<std::endl;
  ** \endcode
  */
  template <typename InputIterator>
  inline
  void write_raw(InputIterator first,
		 InputIterator last,
		 const std::string& file_path_name)
  {
    std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }

	typedef typename std::iterator_traits<InputIterator>::value_type value_type;
	
	int size = last - first;
	output.write((const char*)first,size * sizeof(value_type));
	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
   
  }

 /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/14
  ** \since 1.0.0
  ** \brief Write a Container to an raw file.
  ** \param file_path_name String of the file path name
  ** \param first InputIterator. 
  ** \param last  InputIterator.
  ** \pre container size must be greater or egal to (last-first)  
  **
  ** \par Example:
  ** \code
  ** int tabi[] = {1,2,3,4,5,6};
  ** slip::Matrix<int> Mi(2,3,tabi);
  ** std::cout<<Mi<<std::endl;
  ** slip::write_raw(Mi.begin(),Mi.end(),"Mi.raw");
  **
  **  slip::GrayscaleImage<int> Mi2(2,3);
  ** slip::read_raw("Mi.raw",Mi2.begin(),Mi2.end());
  ** std::cout<<Mi2<<std::endl;
  ** slip::Array<int> AA(6);
  ** slip::read_raw("Mi.raw",AA.begin(),AA.end());
  ** std::cout<<AA<<std::endl;
  ** \endcode
  */
  template <typename InputIterator>
  inline
  void read_raw(const std::string& file_path_name,
		InputIterator first,
		InputIterator last)
  {
    std::ifstream input(file_path_name.c_str(),std::ios::in);
    try
      {
	if (!input) 
	  { 
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename std::iterator_traits<InputIterator>::value_type value_type;
	input.read((char*)first,(last-first)*sizeof(value_type));
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	input.close();
	exit(1);
      }
   
  }

  /* @} */

/** \name 2d containers input/output */
  /* @{ */

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2007/12/19
  ** \since 1.0.0
  ** \brief Write a Container2d to an ASCII file.
  ** \param upper_left RandomAccessIterator2d.
  ** \param bottom_right RandomAccessIterator2d.
  ** \param file_path_name String of the file path name.
  **
  ** \remarks This file format is compatible with matlab, octave and gnuplot
  **  ASCII files
  ** \remarks Write is useful to write char or unsigned char value
  ** \par Example:
  ** \code
  ** double d[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,
  **	 15.0,16.0,17.0,18.0};
  **
  ** slip::Array2d<double> A(3,6,d);
  ** std::cout<<A<<std::endl;
  **
  ** slip::write_ascii_2d<slip::Array2d<double>::iterator2d,double>(A.upper_left(),A.bottom_right(),"A.dat");
  **
  ** slip::Range<int> r1(0,2,1);
  ** slip::Range<int> r2(1,5,2);
  ** slip::write_ascii_2d<slip::Array2d<double>::iterator2d_range,double>(A.upper_left(r1,r2),A.bottom_right(r1,r2),"Arange.dat");
  **
  ** slip::Array2d<double> AAA;
  ** slip::read_ascii_2d(AAA,"Arange.dat");
  ** std::cout<<"read ascii2d "<<std::endl;
  ** std::cout<<AAA<<std::endl;
  ** \endcode
  */
  template <typename RandomAccessIterator2d, typename WriteType>
  inline
  void write_ascii_2d(RandomAccessIterator2d upper_left,
		      RandomAccessIterator2d bottom_right,
		       const std::string& file_path_name)
  {

    std::ofstream output(file_path_name.c_str(),std::ios::out);
    try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	
 
	typename RandomAccessIterator2d::difference_type size2d 
	  = bottom_right - upper_left;
	 
	std::size_t nb_lig = size2d[0];
	std::size_t nb_col = size2d[1];
	
	RandomAccessIterator2d it2d = upper_left;
	for(std::size_t i = 0; i < nb_lig; i++)
	  {
	    for(std::size_t j = 0; j < nb_col; j++)
	      {
		output<<*it2d<<" ";
		++it2d;
	      }
	    output<<std::endl;
	  }
	output.close();
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
     
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2007/12/19
  ** \since 1.0.0
  ** \brief Write a Container2d to an ASCII file.
  ** \param cont 2d container to write.
  ** \param file_path_name String of the file path name.
  **
  ** \remarks This file format is compatible with matlab, octave and gnuplot
  **  ASCII files
  ** \remarks Write is useful to write char or unsigned char value
 ** \par Example:
  ** \code
  ** double d[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,
  **	 15.0,16.0,17.0,18.0};
  **
  ** slip::Array2d<double> A(3,6,d);
  ** std::cout<<A<<std::endl;
  **
  ** slip::write_ascii_2d<slip::Array2d<double>::iterator2d,double>(A,"A.dat");
  **
  ** \endcode
  */
  template <typename Container2d, typename WriteType>
  inline
  void write_ascii_2d(const Container2d& cont,
		      const std::string& file_path_name)
  {
    std::ofstream output(file_path_name.c_str(),std::ios::out);
    try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);         
	  }
	
	
	typedef typename  Container2d::size_type size_type;
	size_type nb_lig = cont.rows();
	size_type nb_col = cont.cols();
	
	for(size_type i = 0; i < nb_lig; i++)
	  {
	    for(size_type j = 0; j < nb_col; j++)
	      {
		output<<(WriteType)cont[i][j]<<" ";
	      }
	    output<<std::endl;
	  }
	output.close();
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.4
  ** \date 2010/10/30
  ** \since 1.0.0
  ** \brief Read a Container2d from an ASCII file.
  ** \param container 2d container to read.
  ** \param file_path_name String of the file path name
  **
  ** \remarks This file format is compatible with dml matlab, octave and gnuplot
  **  ASCII files. This function skip comment lines beginning by #, /,
  **  ! or % " or a letter of the alphabet.
  ** \remarks Write is useful to write char or unsigned char value
  ** \par Example:
  ** \code
  ** double d[] = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,
  **	 15.0,16.0,17.0,18.0};
  **
  ** slip::Array2d<double> A(3,6,d);
  ** std::cout<<A<<std::endl;
  **
  ** slip::write_ascii_2d<slip::Array2d<double>::iterator2d,double>(A.upper_left(),A.bottom_right(),"A.dat");
  **
  ** slip::Range<int> r1(0,2,1);
  ** slip::Range<int> r2(1,5,2);
  ** slip::write_ascii_2d<slip::Array2d<double>::iterator2d_range,double>(A.upper_left(r1,r2),A.bottom_right(r1,r2),"Arange.dat");
  **
  ** slip::Array2d<double> AAA;
  ** slip::read_ascii_2d(AAA,"Arange.dat");
  ** std::cout<<"read ascii2d "<<std::endl;
  ** std::cout<<AAA<<std::endl;
  ** \endcode
  */
  template <typename Container2d>
  inline
  void read_ascii_2d(Container2d& container,
		     const std::string& file_path_name)
  {
    std::ifstream input;

    try
      {
	input.open(file_path_name.c_str());
	if(!input)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);         
	  }

	//computes the number of rows
	int nr = 0;
	int nhead = 0; //number of head (comment) lines
	std::string line;
	while ( std::getline(input, line ) && line.length()!=0)
	  {
	    ++nr;
	     //check for comment lines (starting with either # % ! //
	     //or ")
	    std::size_t first_non_space = line.find_first_not_of(' ');
	   
	    if(line[first_non_space] =='#' || line[first_non_space]=='%' || line[first_non_space]=='!' || line[first_non_space]=='/' || line[first_non_space]=='"' || isalpha(line[first_non_space]))
	      {
		nhead++;
	      }
	    
	   
	  }
	nr = nr - nhead;

	//nr-=1;
	//	nr = line_number_ascii_file(input);
	//computes the number of columns
	int nc = 0;
	input.seekg(0,std::ios::beg);
	input.close();
	std::ifstream input;
	input.open(file_path_name.c_str());
	if(!input)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);         
	  }
	//skip head lines
	for(int n = 0; n < nhead; ++n)
	  {
	    std::getline(input, line);
	  }
	std::getline(input, line );
	boost::char_separator<char> sep(" \t");
	boost::tokenizer<boost::char_separator<char> > tok(line,sep);
	for(boost::tokenizer<boost::char_separator<char> >::iterator beg=tok.begin(); beg!=tok.end();++beg)
	  {
	    ++nc;
	  }
		
	container.resize((std::size_t)nr,(std::size_t)nc);
	//reading datas
	input.seekg(0,std::ios::beg);
	//skip head lines
	for(int n = 0; n < nhead; ++n)
	  {
	    std::getline(input, line);
	  }

	  
	for(int i = 0; i < nr; ++i)
	  {
	    for(int j = 0;j < nc; ++j)
	      {
		typename Container2d::value_type tmp;
		input>>tmp;
		container[i][j] = tmp;
	      }
	  }
	input.close();

      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	input.close();
	exit(1);
      }

  }


 /*!
 ** \brief Write a Container2d to an ASCII file.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.2
 ** \date 2014/04/10
 ** \since 1.0.0
 ** \param upper_left RandomAccessIterator2d.
  ** \param bottom_right RandomAccessIterator2d.
  ** \param file_path_name String of the file path name.
  ** \param title tecplot title string.
  ** \param zone tecplot zone string.
  ** \param precision Print precision of float values (6 by default).
  ** \par Data format:
  ** \code
  ** TITLE= title 
  ** VARIABLES= X Y U V 
  ** ZONE T= zone, I= rows(), J= << cols()
  ** x y U(x,y) V(x,y)
  ** \endcode
  ** \remarks x = j and y = (dim1 - 1) - i
  ** \remarks Write is useful to write char or unsigned char value
  */
  // template <typename RandomAccessIterator2d, typename WriteType>
  // inline
  // void write_tecplot_2d(RandomAccessIterator2d upper_left,
  // 			RandomAccessIterator2d bottom_right,
  // 			const std::string& file_path_name,
  // 			const std::string& title,
  // 			const std::string& zone)
  // {
  //   std::ofstream output(file_path_name.c_str(),std::ios::out);
  //   try
  //     {
  // 	if(!output)
  // 	  {
  // 	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  }
	
  // 	typename RandomAccessIterator2d::difference_type size2d 
  // 	  = bottom_right - upper_left;
  // 	std::size_t nb_lig = size2d[0];
  // 	std::size_t nb_col = size2d[1];
  // 	output<<"TITLE=\""<<title<<"\""<<std::endl;
  // 	output<<"VARIABLES=\"x\" \"y\" \"I\"" << std::endl;
  // 	output<<"ZONE T=\""<<zone<<"\", I="<<nb_col<<", J="<<nb_lig<<"  F=POINT"<<std::endl;
	
  // 	RandomAccessIterator2d it2d = upper_left;
  // 	for(std::size_t i = 0; i < nb_lig; i++)
  // 	  {
  // 	    for(std::size_t j = 0; j < nb_col; j++)
  // 	      {
  // 		//output<<*it2d<<" ";
  // 		output << j << " " << i << " " << *it2d << std::endl;
  // 		++it2d;
  // 	      }
  // 	    output<<std::endl;
  // 	  }
  // 	output<<std::endl;
  // 	output.close();
  //     }
  //   catch(std::exception& e)
  //     {
  // 	std::cerr<<e.what()<<std::endl;
  // 	output.close();
  // 	exit(1);
  //     }
  // }
   template <typename RandomAccessIterator2d, typename WriteType>
  inline
  void write_tecplot_2d(RandomAccessIterator2d upper_left,
			RandomAccessIterator2d bottom_right,
			const std::string& file_path_name,
			const std::string& title,
			const std::string& zone,
			const int precision = 6)
  {
    std::ofstream output(file_path_name.c_str(),std::ios::out);
    try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  }
	int default_precision = output.precision();
	output.setf(std::ios::scientific);
	output.precision(precision);
	typename RandomAccessIterator2d::difference_type size2d 
	  = bottom_right - upper_left;
	const std::size_t nb_lig = size2d[0];
	const std::size_t nb_col = size2d[1];
	output<<"TITLE=\""<<title<<"\""<<std::endl;
	output<<"VARIABLES=\"x\" \"y\" \"I\"" << std::endl;
	output<<"ZONE T=\""<<zone<<"\", I="<<nb_col<<", J="<<nb_lig<<"  F=POINT"<<std::endl;
	
	RandomAccessIterator2d it2d = upper_left;
	for(std::size_t i = 0; i < nb_lig; i++)
	  {
	    for(std::size_t j = 0; j < nb_col; j++)
	      {
		//output<<*it2d<<" ";
		output << j << " " << i << " " << *it2d << std::endl;
		++it2d;
	      }
	    output<<std::endl;
	  }
	output<<std::endl;
	output.precision(default_precision);
	output.close();
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }
 /* @} */

  /** \name 2d vector fields input/output */
  /* @{ */

 /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2010/04/27
  ** \since 1.0.0
  ** \brief Write a DenseVector2dField2D to a gnuplot file.
  ** \param field DenseVector2dField2D.
  ** \param file_path_name String of the file path name
  ** \param precision Print precision of float values (6 by default).
  ** The data format is the following :
  ** \code
  ** x y Vx(x,y) Vy(x,y)
  ** x vary first then y. y is written according to the increase order
  ** \endcode
  ** \remarks x = j and y = (dim1 - 1) - i
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::DenseVector2dField2d<double> Field(4,5);
  ** slip::write_gnuplot_vect2d(Field,"field.gnu");
  ** \endcode
  */
  template <typename MultiContainer2d>
  inline
  void write_gnuplot_vect2d(const MultiContainer2d& field,
			    const std::string& file_path_name,
			    const int precision = 6)
  {
    std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename MultiContainer2d::block_value_type T;
	int default_precision = output.precision();
	output.setf(std::ios::scientific);
	output.precision(precision);
	int dim1 = static_cast<int>(field.dim1());
	int dim2 = static_cast<int>(field.dim2());
	T y = T(0);
	for(int i = dim1 - 1; i >= 0 ; --i, y+=T(1))
	  {
	    T x = T(0);
	    for(int j = 0; j < dim2; ++j,x+=T(1))
	      {
		output<<static_cast<T>(x)<<" "<<static_cast<T>(y)<<" "<<field[i][j][0]<<" "<<field[i][j][1]<<"\n";
	      }
	  }
	output.precision(default_precision);
	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
   
  }

 
/*!
  ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2010/04/27
  ** \since 1.0.0
  ** \brief Write a DenseVector2dField2D to a gnuplot file handle real regular grid 2d.
  ** \param field DenseVector2dField2D.
  ** \param init_point Point2d corresponding to the point having the smallest x and y coordinates.
  ** \param step Point2d corresponding to the steps of the regular grid.
  ** \param file_path_name String of the file path name
  ** \param precision Print precision of float values (6 by default).
  ** The data format is the following :
  ** \code
  ** x y Vx(x,y) Vy(x,y)
  ** x vary first then y. y is written according to the increase order
  ** \endcode
  ** \remarks x = j and y = (dim1 - 1) - i
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::Point2d<double> init_point(1.1,1.1);
  ** slip::Point2d<double> step(0.2,0.1);
  ** slip::DenseVector2dField2d<double> Field(4,5);
  ** slip::write_gnuplot_vect2d(Field,init_point,step,"field.gnu");
  ** \endcode
  */
  template <typename MultiContainer2d,typename T>
  inline
  void write_gnuplot_vect2d(const MultiContainer2d& field,
                            const slip::Point2d<T>& init_point,
                            const slip::Point2d<T>& step,  
			    const std::string& file_path_name,
			    const int precision = 6)
  {
    std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	int default_precision = output.precision();
	output.setf(std::ios::scientific);
	output.precision(precision);
	int dim1 = static_cast<int>(field.dim1());
	int dim2 = static_cast<int>(field.dim2());
       
	T y=init_point[1];
	for(int i=dim1-1; i >= 0;--i,y+=step[1])
	  {
	    T x=init_point[0];
	    for(int j=0; j < dim2; ++j,x+=step[0])
	      {
		output<<static_cast<T>(x)<<" "<<static_cast<T>(y)<<" "<<field[i][j][0]<<" "<<field[i][j][1]<<"\n";
	      }
	  }
	output.precision(default_precision);
	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
   
  }


 /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2009/04/27
  ** \since 1.0.0
  ** \brief Write a DenseVector2dField2D to a tecplot file.
  ** \param field DenseVector2dField2D.
  ** \param file_path_name String of the file path name
  ** \param title Title of the data.
  ** \param zone Name of the zone.
  ** \param precision Print precision of float values (6 by default).
  ** The data format is the following :
  ** \code
  ** TITLE= title 
  ** VARIABLES= X Y U V 
  ** ZONE T= zone, I= rows(), J=  cols()
  ** x y U(x,y) V(x,y)
  ** x vary first then y. y is written according to the increase order
  ** \endcode
  ** \remarks x = j and y = (dim1 - 1) - i
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::DenseVector2dField2d<double> Field(4,5);
  ** slip::write_tecplot_vect2d(Field,"field.gnu");
  ** \endcode
  */
  template <typename MultiContainer2d>
  inline
  void write_tecplot_vect2d(const MultiContainer2d& field,
			    const std::string& file_path_name,
			    const std::string& title,
			    const std::string& zone,
			    const int precision = 6)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	typedef typename MultiContainer2d::block_value_type T;
	int default_precision = output.precision();
	output.setf(std::ios::scientific);
	output.precision(precision);
	int dim1 = static_cast<int>(field.dim1());
	int dim2 = static_cast<int>(field.dim2());
	//write file informations
	output<<"TITLE=\"" <<title<<"\""<<std::endl;
	output<<"VARIABLES=\"X\",\"Y\",\"U\",\"V\"" <<std::endl;
	output<<"ZONE T=\""<<zone<<"\", I="<<dim2<<", J="<<dim1<<std::endl;
	//write data
	T y= T(0);
	for(int i=dim1-1; i >= 0;--i,y+=T(1))
	  {
	    T x= T(0);
	    for(int j=0; j < dim2; ++j,x+=T(1))
	      {
		output<<static_cast<T>(x)<<" "<<static_cast<T>(y)<<" "<<field[i][j][0]<<" "<<field[i][j][1]<<"\n";
	      }
	  }

	output.precision(default_precision);
	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }


/*!
  ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2009/04/27
  ** \since 1.0.0
  ** \brief Write a DenseVector2dField2D to a tecplot file  handle real regular grid 2d
  ** \param field DenseVector2dField2D.
  ** \param init_point Point2d corresponding to the point having the smallest x and y coordinates.
  ** \param step Point2d corresponding to the steps of the regular grid.
  ** \param file_path_name String of the file path name
  ** \param title Title of the data.
  ** \param zone Name of the zone.
  ** The data format is the following :
  ** \code
  ** TITLE= title 
  ** VARIABLES= X Y U V 
  ** ZONE T= zone, I= rows(), J=  cols()
  ** x y U(x,y) V(x,y)
  ** x vary first then y. y is written according to the increase order
  ** \endcode
  ** \remarks x = j and y = (dim1 - 1) - i
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::Point2d<double> init_point(0.5,0.5);
  ** slip::Point2d<double> step(0.3,0.1)
  ** slip::DenseVector2dField2d<double> Field(4,5);
  ** slip::write_tecplot_vect2d(Field,init_point,step,"field.gnu");
  ** \endcode
  */
  template <typename MultiContainer2d, typename T>
  inline void write_tecplot_vect2d(const MultiContainer2d& field,
				   const slip::Point2d<T>& init_point, 
				   const slip::Point2d<T>& step, 
				   const std::string& file_path_name, 
				   const std::string& title, 
				   const std::string& zone,
				   const int precision = 6)
  {
    std::ofstream output(file_path_name.c_str(),std::ios::out);
    try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	//	typedef typename MultiContainer2d::block_value_type T;
	int default_precision = output.precision();
	output.setf(std::ios::scientific);
	output.precision(precision);

	int dim1 = static_cast<int>(field.dim1());
	int dim2 = static_cast<int>(field.dim2());
        
	//write file informations
	output<<"TITLE=\"" <<title<<"\""<<std::endl;
	output<<"VARIABLES=\"X\",\"Y\",\"U\",\"V\"" <<std::endl;
	output<<"ZONE T=\""<<zone<<"\", I="<<dim2<<", J="<<dim1<<std::endl;
	//write data
	T y=init_point[1];
	for(int i=dim1-1; i >= 0;--i,y+=step[1])
	  {
	    T x=init_point[0];
	    for(int j=0; j < dim2; ++j,x+=step[0])
	      {
		output<<static_cast<T>(x)<<" "<<static_cast<T>(y)<<" "<<field[i][j][0]<<" "<<field[i][j][1]<<"\n";
	      }
	  }


	
	output.precision(default_precision);
	output.close();
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }
   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \author Ludovic Chatellier <ludovic.chatellier@univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2009/04/27
  ** \since 1.0.0
  ** \brief Create a DenseVector2dField2D from a tecplot file.
  ** \param file_path_name String of the file path name
  ** \param field DenseVector2dField2D.
  ** \par The data format is the following:
  ** \code
  ** TITLE= title 
  ** VARIABLES= X Y U V 
  ** ZONE T= zone, I= rows(), J= cols()
  ** x y U(x,y) V(x,y)
  ** \endcode
  ** handled reading cases:
  ** \li x vary first, y written according to the increasing order 
  ** \li y vary first, y written according to the increasing order 
  ** \li x vary first, y written according to the decreasing order 
  ** \li y vary first, y written according to the decreasing order 
  
  ** \remarks x = j and y = (dim1 - 1) - i
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::DenseVector2dField2d<double> XY;
  ** slip::DenseVector2dField2d<double> field;
  ** slip::read_tecplot_vect2d("xyuv.dat",field);
  ** slip::read_tecplot_XY("xyuv.dat",XY);
  ** std::cout<<"read_tecplot_vect2d xyuv.dat"<<std::endl;
  ** std::cout<<"XY : "<<std::endl;
  ** std::cout<<XY<<std::endl;
  ** std::cout<<"Field : "<<std::endl;
  ** std::cout<<field<<std::endl;
  ** \endcode
  */
  template <class MultiContainer2d>
  inline
  void read_tecplot_vect2d(const std::string& file_path_name,
			   MultiContainer2d& field)
  {
    slip::Array2d<double> data;
    slip::read_ascii_2d(data,file_path_name);
    std::size_t size_y = std::count(data.col_begin(0),data.col_end(0),
				    data[0][0]); 
    std::size_t size_x = data.rows() / size_y;

    slip::Point2d<double> init_point;
    init_point[0]=std::min(data[0][0],data[data.rows()-1][0]);
    init_point[1]=std::min(data[0][1],data[data.rows()-1][1]);
    bool y_direct = (init_point[1] == data[0][1]);

    field.resize(size_y,size_x);
    if(data[0][0] == data[1][0])
      {
	//cas x fixe, y varie
	std::size_t stepy = 0;
       
	for(std::size_t j = 0; j < size_x; ++j)
	  {
	     if(y_direct)
	      {
		std::copy(data.col_begin(3) + stepy,
			  data.col_begin(3) + (stepy + size_y),
			  field.col_rbegin(1,j));
		std::copy(data.col_begin(2) + stepy,
			  data.col_begin(2) + (stepy + size_y),
			  field.col_rbegin(0,j));
		stepy += size_y;
	      }
	     else
	       {
		 std::copy(data.col_begin(3) + stepy,
			   data.col_begin(3) + (stepy + size_y),
			   field.col_begin(1,j));
		 std::copy(data.col_begin(2) + stepy,
			   data.col_begin(2) + (stepy + size_y),
			   field.col_begin(0,j));
		 stepy += size_y;
	       }
	  }
      }
    else
      {
	//cas x varie, y fixe
	std::size_t stepx = 0;
	for(std::size_t j = 0; j < size_y; ++j)
	  {
	    if(y_direct)
	      {
		std::copy(data.col_begin(2) + stepx,
			  data.col_begin(2) + (stepx + size_x),
			  field.row_begin(0,(size_y - 1) - j));
		std::copy(data.col_begin(3) + stepx, 
			  data.col_begin(3) + (stepx + size_x),
			  field.row_begin(1,(size_y - 1) - j));
		
	      }
	    else
	      {
		std::copy(data.col_begin(2) + stepx,
			  data.col_begin(2) + (stepx + size_x),
			  field.row_begin(0,j));
		std::copy(data.col_begin(3) + stepx, 
			  data.col_begin(3) + (stepx + size_x),
			  field.row_begin(1,j));
	      }
	    stepx += size_x;
       }
      }
  }

/*!
  ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2009/03/10
  ** \since 1.0.0
  ** \brief Create a DenseVector2dField2D from a tecplot file handle 2d real regular grid.
  ** \param file_path_name String of the file path name
  ** \param field DenseVector2dField2D.
  ** \param init_point Point2d corresponding to the point having the smallest x and y coordinates.
  ** \param step step between element of the grid.
  ** \par The data format is the following:
  ** \code
  ** TITLE= title 
  ** VARIABLES= X Y U V 
  ** ZONE T= zone, I= rows(), J= cols()
  ** x y U(x,y) V(x,y)
  ** \endcode
  ** handled reading cases:
  ** \li x vary first, y written according to the increasing order 
  ** \li y vary first, y written according to the increasing order 
  ** \li x vary first, y written according to the decreasing order 
  ** \li y vary first, y written according to the decreasing order 
  ** \remarks x = j and y = (dim1 - 1) - i
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::Point2d<double> init_point;
  ** slip::Point2d<double> step;
  ** slip::DenseVector2dField2d<double> field;
  ** slip::read_tecplot_vect2d("xyuv.dat",field,init_point,step);
  ** std::cout<<"read_tecplot_vect2d xyuv.dat"<<std::endl;
  ** std::cout<<"init_point : "<<init_point<<std::endl;
  ** std::cout<<"step : "<< step<<std::endl;
  ** std::cout<<"Field : "<<std::endl;
  ** \endcode
  */
  template <class MultiContainer2d,typename T>
  inline
  void read_tecplot_vect2d(const std::string& file_path_name,
                            MultiContainer2d& field,
                            slip::Point2d<T>& init_point,
                            slip::Point2d<T>& step)
  {
    slip::Array2d<double> data;
    slip::read_ascii_2d(data,file_path_name);
    std::size_t size_y = std::count(data.col_begin(0),data.col_end(0),
				    data[0][0]); 
    std::size_t size_x = data.rows() / size_y;

    init_point[0]=std::min(data[0][0],data[data.rows()-1][0]);
    init_point[1]=std::min(data[0][1],data[data.rows()-1][1]);

    bool y_direct = (init_point[1] == data[0][1]);

    field.resize(size_y,size_x);
    if(data[0][0] == data[1][0])
      {
	//cas x fixe, y varie
	step[0]=std::abs(*(data.col_begin(0)+size_y)-(*(data.col_begin(0))));
	step[1]=std::abs(*(data.col_begin(1)+1)-*(data.col_begin(1)));

	std::size_t stepy = 0;
       
	for(std::size_t j = 0; j < size_x; ++j)
	  {
	     if(y_direct)
	      {
		std::copy(data.col_begin(3) + stepy,
			  data.col_begin(3) + (stepy + size_y),
			  field.col_rbegin(1,j));
		std::copy(data.col_begin(2) + stepy,
			  data.col_begin(2) + (stepy + size_y),
			  field.col_rbegin(0,j));
		stepy += size_y;
	      }
	     else
	       {
		 std::copy(data.col_begin(3) + stepy,
			   data.col_begin(3) + (stepy + size_y),
			   field.col_begin(1,j));
		 std::copy(data.col_begin(2) + stepy,
			   data.col_begin(2) + (stepy + size_y),
			   field.col_begin(0,j));
		 stepy += size_y;
	       }
	  }
      }
    else
      {
	//cas x varie, y fixe
	step[0]=std::abs(*(data.col_begin(0)+1)-(*(data.col_begin(0))));
	step[1]=std::abs(*(data.col_begin(1)+size_x) - *(data.col_begin(1)));

	std::size_t stepx = 0;
	for(std::size_t j = 0; j < size_y; ++j)
	  {
	    if(y_direct)
	      {
		std::copy(data.col_begin(2) + stepx,
			  data.col_begin(2) + (stepx + size_x),
			  field.row_begin(0,(size_y - 1) - j));
		std::copy(data.col_begin(3) + stepx, 
			  data.col_begin(3) + (stepx + size_x),
			  field.row_begin(1,(size_y - 1) - j));
		
	      }
	    else
	      {
		std::copy(data.col_begin(2) + stepx,
			  data.col_begin(2) + (stepx + size_x),
			  field.row_begin(0,j));
		std::copy(data.col_begin(3) + stepx, 
			  data.col_begin(3) + (stepx + size_x),
			  field.row_begin(1,j));
	      }
	    stepx += size_x;
       }
      }
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \author Ludovic Chatellier <ludovic.chatellier@univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2010/10/12
  ** \since 1.0.0
  ** \brief Create a XY DenseVector2dField2D from a tecplot file.
  ** \param file_path_name String of the file path name
  ** \param XY DenseVector2dField2D which store the XY locations.
  ** \par The data format is the following:
  ** \code
  ** TITLE= title 
  ** VARIABLES= X Y U V 
  ** ZONE T= zone, I= rows(), J=  cols()
  ** x y U(x,y) V(x,y)
  ** \endcode
   ** handled reading cases:
  ** \li x vary first, y written according to the increasing order 
  ** \li y vary first, y written according to the increasing order 
  ** \li x vary first, y written according to the decreasing order 
  ** \li y vary first, y written according to the decreasing order 
  ** \remarks x = j and y = (dim1 - 1) - i
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::DenseVector2dField2d<double> XY;
  ** slip::DenseVector2dField2d<double> field;
  ** slip::read_tecplot_vect2d("xyuv.dat",field);
  ** slip::read_tecplot_XY("xyuv.dat",XY);
  ** std::cout<<"read_tecplot_vect2d xyuv.dat"<<std::endl;
  ** std::cout<<"XY : "<<std::endl;
  ** std::cout<<XY<<std::endl;
  ** std::cout<<"Field : "<<std::endl;
  ** std::cout<<field<<std::endl;
  ** \endcode
  */
  template <typename MultiContainer2d>
  inline
  void read_tecplot_XY(const std::string& file_path_name,
		       MultiContainer2d& XY)
  {
    slip::Array2d<double> data;
    slip::read_ascii_2d(data,file_path_name);
    std::size_t size_y = std::count(data.col_begin(0),data.col_end(0),
				    data[0][0]); 
    std::size_t size_x = data.rows() / size_y;

    slip::Point2d<double> init_point;
    init_point[0]=std::min(data[0][0],data[data.rows()-1][0]);
    init_point[1]=std::min(data[0][1],data[data.rows()-1][1]);
    bool y_direct = (init_point[1] == data[0][1]);
    XY.resize(size_y,size_x);
   
    if(data[0][0] == data[1][0])
      {
	//cas x fixe, y varie
	std::size_t stepy = 0;
       
	for(std::size_t j = 0; j < size_x; ++j)
	  {
	     if(y_direct)
	      {
		std::copy(data.col_begin(1) + stepy,
			  data.col_begin(1) + (stepy + size_y),
			  XY.col_rbegin(1,j));
		std::copy(data.col_begin(0) + stepy,
			  data.col_begin(0) + (stepy + size_y),
			  XY.col_rbegin(0,j));
		stepy += size_y;
	      }
	     else
	       {
		 std::copy(data.col_begin(1) + stepy,
			   data.col_begin(1) + (stepy + size_y),
			   XY.col_begin(1,j));
		 std::copy(data.col_begin(0) + stepy,
			   data.col_begin(0) + (stepy + size_y),
			   XY.col_begin(0,j));
		 stepy += size_y;
	       }
	  }
      }
    else
      {
	//cas x varie, y fixe
	std::size_t stepx = 0;
	for(std::size_t j = 0; j < size_y; ++j)
	  {
	    if(y_direct)
	      {
		std::copy(data.col_begin(0) + stepx,
			  data.col_begin(0) + (stepx + size_x),
			  XY.row_begin(0,(size_y - 1) - j));
		std::copy(data.col_begin(1) + stepx, 
			  data.col_begin(1) + (stepx + size_x),
			  XY.row_begin(1,(size_y - 1) - j));
		
	      }
	    else
	      {
		std::copy(data.col_begin(0) + stepx,
			  data.col_begin(0) + (stepx + size_x),
			  XY.row_begin(0,j));
		std::copy(data.col_begin(1) + stepx, 
			  data.col_begin(1) + (stepx + size_x),
			  XY.row_begin(1,j));
	      }
	    stepx += size_x;
       }
      }
  }

    /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \author Ludovic Chatellier <ludovic.chatellier@univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2010/10/12
  ** \since 1.0.0
  ** \brief Reads X and Y column of an ASCII 2d tecplot file.
  ** \param file_path_name String of the file path name
  ** \param X Container 1d containing X part of tecplot file.
  ** \param Y Container 1d containing Y part of tecplot file.
  ** \par The data format is the following:
  ** \code
  ** TITLE= title 
  ** VARIABLES= X Y U V 
  ** ZONE T= zone, I= rows(), J= cols()
  ** x y U(x,y) V(x,y)
  ** \endcode
   ** handled reading cases:
  ** \li x vary first, y written according to the increasing order 
  ** \li y vary first, y written according to the increasing order 
  ** \li x vary first, y written according to the decreasing order 
  ** \li y vary first, y written according to the decreasing order 
  ** \remarks x = j and y = (dim1 - 1) - i
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::Array<double> X2(5);
  ** slip::Array<double> Y2(4);
  ** slip::read_tecplot_XY("test_tec2.txt",X2,Y2);
  ** std::cout<<"X2 = \n"<<X2<<std::endl;
  ** std::cout<<"Y2 = \n"<<Y2<<std::endl;
  ** \endcode
  */
  template <typename Vector1,
	    typename Vector2>
  inline
  void read_tecplot_XY(const std::string& file_path_name,
		       Vector1& X,
		       Vector2& Y)
  {
    typedef typename slip::Array2d<double>::col_iterator col_iterator;
    slip::Array2d<double> data;
    slip::read_ascii_2d(data,file_path_name);
    std::size_t size_y = std::count(data.col_begin(0),data.col_end(0),
				    data[0][0]); 
    std::size_t size_x = data.rows() / size_y;
    X.resize(size_x);
    Y.resize(size_y);

    slip::Point2d<double> init_point;
    init_point[0]=std::min(data[0][0],data[data.rows()-1][0]);
    init_point[1]=std::min(data[0][1],data[data.rows()-1][1]);
    bool y_direct = (init_point[1] == data[0][1]);
    if(data[0][0] == data[1][0])
      {
	//cas x fixe, y varie
	if(y_direct)
	  {
	    //copy Y
	    std::copy(data.col_begin(1),data.col_begin(1)+size_y,Y.begin());
	    //copy X    
	    std::copy(slip::stride_iterator<col_iterator>(data.col_begin(0),size_y),
		      slip::stride_iterator<col_iterator>(data.col_begin(0),size_y)+size_x,
		      X.begin());
	  }
	else
	  {
	    //copy Y
	    std::copy(data.col_begin(1),data.col_begin(1)+size_y,Y.rbegin());
	    //copy X    
	    std::copy(slip::stride_iterator<col_iterator>(data.col_begin(0),size_y),
		      slip::stride_iterator<col_iterator>(data.col_begin(0),size_y)+size_x,
		      X.begin());
	  }
      }
    else
      {
	//cas x varie, y fixe
	if(y_direct)
	  {
	    //copy X
	    std::copy(data.col_begin(0),data.col_begin(0)+size_x,X.begin());
	    //copy Y    
	    std::copy(slip::stride_iterator<col_iterator>(data.col_begin(1),size_x),
		      slip::stride_iterator<col_iterator>(data.col_begin(1),size_x)+size_y,
		      Y.begin());
	  }
	else
	  {
	    //copy X
	    std::copy(data.col_begin(0),data.col_begin(0)+size_x,X.begin());
	    //copy Y    
	    std::copy(slip::stride_iterator<col_iterator>(data.col_begin(1),size_x),
		      slip::stride_iterator<col_iterator>(data.col_begin(1),size_x)+size_y,
		      Y.rbegin());
	  }
      }
    
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \author Ludovic Chatellier <ludovic.chatellier@univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/05/24
  ** \since 1.0.0
  ** \brief Create a DenseVector2dField2D from a gnuplot file.
  ** \param file_path_name String of the file path name
  ** \param field DenseVector2dField2D.
  ** \par The data format is the following:
  ** \code
  ** x y U(x,y) V(x,y)
  ** \endcode
  ** handled reading cases:
  ** \li x vary first, y written according to the increasing order 
  ** \li y vary first, y written according to the increasing order 
  ** \li x vary first, y written according to the decreasing order 
  ** \li y vary first, y written according to the decreasing order 
  ** \remarks x = j and y = (dim1 - 1) - i
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::DenseVector2dField2d<double> XY;
  ** slip::DenseVector2dField2d<double> field;
  ** slip::read_gnuplot_vect2d("xyuv.dat",field);
  ** slip::read_gnuplot_XY("xyuv.dat",XY);
  ** std::cout<<"read_gnuplot_vect2d xyuv.dat"<<std::endl;
  ** std::cout<<"XY : "<<std::endl;
  ** std::cout<<XY<<std::endl;
  ** std::cout<<"Field : "<<std::endl;
  ** std::cout<<field<<std::endl;
  ** \endcode
  */
  template <class MultiContainer2d>
  inline
  void read_gnuplot_vect2d(const std::string& file_path_name,
			   MultiContainer2d& field)
  {
    slip::read_tecplot_vect2d(file_path_name,field);
  }

/*!
** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
  * \version 0.0.1
  ** \date 2009/03/10
  ** \since 1.0.0
  ** \brief Create a DenseVector2dField2D from a gnuplot file handle 2d real regular grid.
  ** \param file_path_name String of the file path name
  ** \param field DenseVector2dField2D.
  ** \param init_point Point2d corresponding to the point having the smallest x and y coordinates.
  ** \param step Steps of the regular grid.
  ** \par The data format is the following:
  ** \code
  ** x y U(x,y) V(x,y)
  ** \endcode
  ** handled reading cases:
  ** \li x vary first, y written according to the increasing order 
  ** \li y vary first, y written according to the increasing order 
  ** \li x vary first, y written according to the decreasing order 
  ** \li y vary first, y written according to the decreasing order 
  ** \par The data format is the following:
  ** \remarks x = j and y = (dim1 - 1) - i
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::Point2d<double> init_point;
  ** slip::Point2d<double> step;
  ** slip::DenseVector2dField2d<double> field;
  ** slip::read_gnuplot_vect2d("xyuv.dat",field,init_point,step);
  ** std::cout<<"read_tecplot_vect2d xyuv.dat"<<std::endl;
  ** std::cout<<"init_point : "<<init_point<<std::endl;
  ** std::cout<<"step : "<< step<<std::endl;
  ** std::cout<<"Field : "<<std::endl;
  ** \endcode   
  */
  template <class MultiContainer2d,typename T>
  inline
  void read_gnuplot_vect2d(const std::string& file_path_name,
			   MultiContainer2d& field,
                           slip::Point2d<T>& init_point,
                           slip::Point2d<T>& step)
  {
    slip::read_tecplot_vect2d(file_path_name,field,init_point,step);
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \author Ludovic Chatellier <ludovic.chatellier@univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/05/24
  ** \since 1.0.0
  ** \brief Create a XY DenseVector2dField2D from a gnuplot file.
  ** \param file_path_name String of the file path name
  ** \param XY DenseVector2dField2D which store the XY locations.
  ** \par Data format:
  ** x y U(x,y) V(x,y)
  ** handled reading cases:
  ** \li x vary first, y written according to the increasing order 
  ** \li y vary first, y written according to the increasing order 
  ** \li x vary first, y written according to the decreasing order 
  ** \li y vary first, y written according to the decreasing order 
  ** \remarks x = j and y = (dim1 - 1) - i
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::DenseVector2dField2d<double> XY;
  ** slip::DenseVector2dField2d<double> field;
  ** slip::read_gnuplot_vect2d("xyuv.dat",field);
  ** slip::read_gnuplot_XY("xyuv.dat",XY);
  ** std::cout<<"read_gnuplot_vect2d xyuv.dat"<<std::endl;
  ** std::cout<<"XY : "<<std::endl;
  ** std::cout<<XY<<std::endl;
  ** std::cout<<"Field : "<<std::endl;
  ** std::cout<<field<<std::endl;
  ** \endcode
  */
  template <typename MultiContainer2d>
  inline
  void read_gnuplot_XY(const std::string& file_path_name,
		       MultiContainer2d& XY)
  {
   slip::read_tecplot_XY(file_path_name,XY);
  }


     /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \author Ludovic Chatellier <ludovic.chatellier@univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/05/24
  ** \since 1.0.0
  ** \brief Read  X and Y column of a 2d gnuplot ASCII file.
  ** \param file_path_name String of the file path name
  ** \param X Container 1d containing X part of tecplot file.
  ** \param Y Container 1d containing Y part of tecplot file.
  ** \par The data format is the following:
  ** x y U(x,y) V(x,y)
   ** handled reading cases:
  ** \li x vary first, y written according to the increasing order 
  ** \li y vary first, y written according to the increasing order 
  ** \li x vary first, y written according to the decreasing order 
  ** \li y vary first, y written according to the decreasing order 
  ** \remarks x = j and y = (dim1 - 1) - i
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::Array<double> X2(5);
  ** slip::Array<double> Y2(4);
  ** slip::read_gnuplot_XY("test_tec2.txt",X2,Y2);
  ** std::cout<<"X2 = \n"<<X2<<std::endl;
  ** std::cout<<"Y2 = \n"<<Y2<<std::endl;
  ** \endcode
  */
  template <typename Vector1,
	    typename Vector2>
  inline
  void read_gnuplot_XY(const std::string& file_path_name,
		       Vector1& X,
		       Vector2& Y)
  {
    slip::read_tecplot_XY(file_path_name,X,Y);
  }
 

 

/* @} */

   /** \name 3d vector fields input/output */
  /* @{ */

 /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.3
  ** \date 2010/04/20
  ** \since 1.0.0
  ** \brief Write a DenseVector3dField3D to a tecplot file.
  ** \param field DenseVector3dField3D.
  ** \param file_path_name String of the file path name
  ** \param title
  ** \param zone
  ** \param precision Print precision of float values (6 by default).
  ** The data format is the following :
  ** \code
  ** TITLE= title 
  ** VARIABLES= X Y Z U V W
  ** ZONE T= zone, I= rows(), J= cols(), K = slices()
  ** x y z U(x,y,z) V(x,y,z) W(x,y,z)
  ** \endcode
  ** \remarks x = j and y = (rows() - 1) - i and z = (slices() - 1) - k
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::DenseVector3dField3d<double> Field(4,5,6);
  ** slip::write_tecplot_vect3d(Field,"field.gnu","title","zone");
  ** \endcode
  */
  template <typename DenseVector3dField3d>
  inline
  void write_tecplot_vect3d(const DenseVector3dField3d& field,
			    const std::string& file_path_name,
			    const std::string& title,
			    const std::string& zone,
			    const int precision = 6)
  {
    std::ofstream output(file_path_name.c_str(),std::ios::out);
    try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);
	  }
  
	typedef typename DenseVector3dField3d::block_value_type T;
	int default_precision = output.precision();
	output.setf(std::ios::scientific);
	output.precision(precision);
	int dim1 = static_cast<int>(field.dim1()); //z
	int dim2 = static_cast<int>(field.dim2()); //y
	int dim3 = static_cast<int>(field.dim3());//x

	//write file informations
    output<<"TITLE=\"" <<title<<"\""<<std::endl;
    output<<"VARIABLES=\"X\",\"Y\",\"Z\",\"U\",\"V\",\"W\"" <<std::endl; 
    output<<"ZONE T=\""<<zone<<"\", I="<<dim3<<", J="<<dim2<<", K="<<dim1<<std::endl;
    //write data
    T z = T(0);
    for(int k = dim1-1; k >= 0;--k,z+=T(1))
      {
	T y = T(0);
	for(int i = dim2-1; i >= 0;--i,y+=T(1))
	  {
	    T x = T(0);
	    for(int j = 0; j < dim3; ++j,x+=T(1))
	      {
		output<<static_cast<T>(x)<<" "<<static_cast<T>(y)<<" "<<static_cast<T>(z)<<" "<<field[k][i][j][0]<<" "<<field[k][i][j][1]<<" "<<field[k][i][j][2]<<"\n";
	      }
	  }
      }
     output.precision(default_precision);
    output.close();
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }


  /*!
  ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2010/04/20
  ** \since 1.0.0
  ** \brief Write a DenseVector3dField3D to a tecplot file handle 3d real regular grid. 
  ** \param field DenseVector3dField3D.
  ** \param init_point Point3d corresponding to the point having the smallest x, y and z coordinates.
  ** \param step Point3d corresponding to the steps of the regular grid.
  ** \param file_path_name String of the file path name
  ** \param title
  ** \param zone
  ** \param precision Print precision of float values (6 by default).
  ** The data format is the following : 
  ** \code
  ** TITLE= title 
  ** VARIABLES= X Y Z U V W
  ** ZONE T= zone, I= rows(), J= cols(), K = slices()
  ** x y z U(x,y,z) V(x,y,z) W(x,y,z)
  ** \endcode
  ** \remarks x = j and y = (rows() - 1) - i and z = (slices() - 1) - k
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::Point3d<double> init_point(0.5,0.5,0.5);
  ** slip::Point3d<double> step(0.1,0.2,0.1);
  ** slip::DenseVector3dField3d<double> Field(4,5,6);
  ** slip::write_tecplot_vect3d(Field,init_point,step"field.gnu","title","zone");
  ** \endcode
  */
 template <typename DenseVector3dField3d,typename T>
  inline
  void write_tecplot_vect3d(const DenseVector3dField3d& field,
                            const slip::Point3d<T>& init_point,
                            const slip::Point3d<T>& step,
                            const std::string& file_path_name,
			    const std::string& title,
			    const std::string& zone,
			    const int precision = 6)
  {
  
    std::ofstream output(file_path_name.c_str(),std::ios::out);
    try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);
	  }

	int default_precision = output.precision();
	output.setf(std::ios::scientific);
	output.precision(precision);
	int dim1 = static_cast<int>(field.dim1()); //z
	int dim2 = static_cast<int>(field.dim2()); //y
	int dim3 = static_cast<int>(field.dim3());//x

	//write file informations
    output<<"TITLE=\"" <<title<<"\""<<std::endl;
    output<<"VARIABLES=\"X\",\"Y\",\"Z\",\"U\",\"V\",\"W\"" <<std::endl;
    output<<"ZONE T=\""<<zone<<"\", I="<<dim3<<", J="<<dim2<<", K="<<dim1<<std::endl;
    //write data
    T z = init_point[2];
    for(int k = dim1-1; k >= 0;--k,z+=step[2])
    {
      T y = init_point[1];
      for(int i = dim2-1; i >= 0;--i,y+=step[1])
	{
	  T x = init_point[0];
	  for(int j = 0; j < dim3; ++j,x+=step[0])
	    {
	      output<<static_cast<T>(x)<<" "<<static_cast<T>(y)<<" "<<static_cast<T>(z)<<" "<<field[k][i][j][0]<<" "<<field[k][i][j][1]<<" "<<field[k][i][j][2]<<"\n";
	    }
	}
    }

    output.precision(default_precision);
    output.close();
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }
/*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \author Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>
  ** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr
  ** \version 0.0.2
  ** \date 2014/12/08
  ** \since 1.0.0
  ** \brief Create a DenseVector3dField3D from a tecplot file.
  ** \param file_path_name String of the file path name
  ** \param field DenseVector3dField3D.
  ** \par The data format is the following:
  ** \code
  ** TITLE= title 
  ** VARIABLES= X Y Z U V W
  ** ZONE T= zone, I= rows(), J= cols(), K = slices()
  ** x y z U(x,y,z) V(x,y,z) W(x,y,z)
  ** \endcode
  ** handled reading cases:
  ** \li x vary first, y written according to the increasing order, z written according to the increasing order 
  ** \li y vary first, y written according to the increasing order , z written according to the increasing order 
  ** \li x vary first, y written according to the decreasing order , z written according to the increasing order 
  ** \li y vary first, y written according to the decreasing order , z written according to the increasing order 
  ** \remarks x = j and y = (rows() - 1 - i) - i and z = (slices() - 1 - k)
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::DenseVector3dField3d<double> XYZ;
  ** slip::DenseVector3dField3d<double> field;
  ** slip::read_tecplot_vect3d("xyzuvw.dat",field);
  ** slip::read_tecplot_XYZ("xyzuvw.dat",XY2);
  ** std::cout<<"read_tecplot_vect3d xyzuvw.dat"<<std::endl;
  ** std::cout<<"XYZ : "<<std::endl;
  ** std::cout<<XYZ<<std::endl;
  ** std::cout<<"Field : "<<std::endl;
  ** std::cout<<field<<std::endl;
  ** \endcode
  */
  template <class MultiContainer3d>
  inline
  void read_tecplot_vect3d(const std::string& file_path_name,
			   MultiContainer3d& field)
  {
    slip::Array2d<double> data;
    slip::read_ascii_2d(data,file_path_name);
    std::size_t size =  data.rows();
    std::size_t size_xy = std::count(data.col_begin(2),data.col_end(2),data[0][2]); 
    if (size_xy == 0)
      {
	throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);
      }
    std::size_t size_z = size / size_xy;
    std::size_t size_y = std::count(data.col_begin(0),data.col_end(0),data[0][0]);
    if (size_z == 0)
      {
	throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);
      }
    size_y = size_y / size_z;
     if (size_y * size_z == 0)
       {
	 throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);
       }
    std::size_t size_x = size / (size_y * size_z);
    if (size_x * size_y * size_z == 0 || size_x * size_y * size_z != data.dim1())
      {
	throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);
      }

    slip::Point3d<double> init_point;
    init_point[0]=std::min(data[0][0],data[data.rows()-1][0]);
    init_point[1]=std::min(data[0][1],data[data.rows()-1][1]);
    init_point[2]=std::min(data[0][2],data[data.rows()-1][2]);

   
    bool y_direct = (init_point[1] == data[0][1]);
   
    field.resize(size_z,size_y,size_x);

    if(data[0][0] == data[1][0])
      {
	//cas x fixe, y varie

	std::size_t stepz = 0;
	for(std::size_t k = 0 ; k < size_z; ++k)
	  {
	    std::size_t stepy = 0;
	    
	    for(std::size_t j = 0; j < size_x; ++j)
	      {
		if(y_direct)
		  {
		    std::copy(data.col_begin(4) + stepz + stepy,
			      data.col_begin(4) + (stepz + stepy + size_y),
			      field.col_rbegin(1,(size_z-1)-k,j));
		    std::copy(data.col_begin(3) + stepz + stepy,
			      data.col_begin(3) + (stepz + stepy + size_y),
			      field.col_rbegin(0,(size_z-1)-k,j));
		    std::copy(data.col_begin(5) + stepz + stepy,
			      data.col_begin(5) + (stepz + stepy + size_y),
			      field.col_rbegin(2,(size_z-1)-k,j));
		    stepy += size_y;
		  }
		else
		  {
		    std::copy(data.col_begin(4) + stepz + stepy,
			      data.col_begin(4) + (stepz + stepy + size_y),
			      field.col_begin(1,(size_z-1)-k,j));
		    std::copy(data.col_begin(3) + stepz + stepy,
			      data.col_begin(3) + (stepz + stepy + size_y),
			      field.col_begin(0,(size_z-1)-k,j));
		    std::copy(data.col_begin(5) + stepz + stepy,
			      data.col_begin(5) + (stepz + stepy + size_y),
			      field.col_begin(2,(size_z-1)-k,j));
		    stepy += size_y;
		  }
	      }
	    stepz+=size_xy;
	  }
      }
    else
      {
	//cas x varie, y fixe
	std::size_t stepz = 0;
	for(std::size_t k = 0 ; k < size_z; ++k)
	  {
	    std::size_t stepx = 0;
	    for(std::size_t j = 0; j < size_y; ++j)
	      {
		if(y_direct)
		  {
		    std::copy(data.col_begin(3) + stepz + stepx,
			      data.col_begin(3) + (stepz + stepx + size_x),
			      field.row_begin(0,(size_z-1)-k,(size_y - 1) - j));
		    std::copy(data.col_begin(4) + stepz + stepx, 
			      data.col_begin(4) + (stepz + stepx + size_x),
			      field.row_begin(1,(size_z-1)-k,(size_y - 1) - j));
		    std::copy(data.col_begin(5) + stepz + stepx, 
			      data.col_begin(5) + (stepz + stepx + size_x),
			      field.row_begin(2,(size_z-1)-k,(size_y - 1) - j));
		
		  }
		else
		  {
		    std::copy(data.col_begin(3) + stepz + stepx,
			      data.col_begin(3) + stepz + (stepx + size_x),
			      field.row_begin(0,(size_z-1)-k,j));
		    std::copy(data.col_begin(4) + stepz + stepx, 
			      data.col_begin(4) + (stepz + stepx + size_x),
			      field.row_begin(1,(size_z-1)-k,j));
		    std::copy(data.col_begin(5) + stepz + stepx, 
			      data.col_begin(5) + (stepz + stepx + size_x),
			      field.row_begin(2,(size_z-1)-k,j));
		  }
		stepx += size_x;
	      }
	    stepz+=size_xy;
	  }
      }
   }

   /*!
   ** \brief Create a DenseVector3dField3D from a tecplot file handle 3d real regular grid. 
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \version 0.0.1
   ** \date 2009/03/10
   ** \since 1.0.0
   ** \param file_path_name String of the file path name
   ** \param field DenseVector3dField3D.
   ** \param init_point Point3d corresponding to the point having the smallest x, y and z coordinates.
   ** \param step Point3d corresponding to the steps of the regular grid.
   ** \code
   ** TITLE= title 
   ** VARIABLES= X Y Z U V W
   ** ZONE T= zone, I= rows(), J= cols(), K = slices()
   ** x y z U(x,y,z) V(x,y,z) W(x,y,z)
   ** \endcode
   ** handled reading cases:
   ** \li x vary first, y written according to the increasing order, z written according to the increasing order 
   ** \li y vary first, y written according to the increasing order , z written according to the increasing order 
   ** \li x vary first, y written according to the decreasing order , z written according to the increasing order 
   ** \li y vary first, y written according to the decreasing order , z written according to the increasing order 
   ** \remarks x = j and y = (rows() - 1 - i) - i and z = (slices() - 1 - k)
   ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
   ** \par Example:
   ** \code
   ** slip::DenseVector3dField3d<double> field;
   ** slip::Point3d<double> init_point;
   ** slip::Point3d<double> step;
   ** slip::read_tecplot_vect3d("xyzuvw.dat",field,init_point,step);
   ** std::cout<<"field : \n"<<field<<std::endl;
   ** std::cout<<"init_point : "<< init_point<<std::endl;
   ** std::cout<<"step : "<< step<<std::endl;
   ** \endcode
   */
  template <class MultiContainer3d,typename T>
  inline
  void read_tecplot_vect3d(const std::string& file_path_name,
			   MultiContainer3d& field,
                           slip::Point3d<T>& init_point,
                           slip::Point3d<T>& step)
  {

    slip::Array2d<double> data;
    slip::read_ascii_2d(data,file_path_name);
    std::size_t size =  data.rows();
    std::size_t size_xy = std::count(data.col_begin(2),data.col_end(2),data[0][2]);
    std::size_t size_z = size / size_xy;
    std::size_t size_y = std::count(data.col_begin(0),data.col_end(0),data[0][0]);
    size_y = size_y / size_z;
    std::size_t size_x = size / (size_y * size_z);

    
    init_point[0]=std::min(data[0][0],data[data.rows()-1][0]);
    init_point[1]=std::min(data[0][1],data[data.rows()-1][1]);
    init_point[2]=std::min(data[0][2],data[data.rows()-1][2]);

   
    bool y_direct = (init_point[1] == data[0][1]);
   
    field.resize(size_z,size_y,size_x);

    if(data[0][0] == data[1][0])
      {
	//cas x fixe, y varie
	step[0]=std::abs(*(data.col_begin(0)+size_y)-(*(data.col_begin(0))));
	step[1]=std::abs(*(data.col_begin(1)+1)-*(data.col_begin(1)));
 	step[2]=std::abs(*(data.col_begin(2)+size_xy)-*(data.col_begin(2)));
	std::size_t stepz = 0;
	for(std::size_t k = 0 ; k < size_z; ++k)
	  {
	    std::size_t stepy = 0;
	    
	    for(std::size_t j = 0; j < size_x; ++j)
	      {
		if(y_direct)
		  {
		    std::copy(data.col_begin(4) + stepz + stepy,
			      data.col_begin(4) + (stepz + stepy + size_y),
			      field.col_rbegin(1,(size_z-1)-k,j));
		    std::copy(data.col_begin(3) + stepz + stepy,
			      data.col_begin(3) + (stepz + stepy + size_y),
			      field.col_rbegin(0,(size_z-1)-k,j));
		    std::copy(data.col_begin(5) + stepz + stepy,
			      data.col_begin(5) + (stepz + stepy + size_y),
			      field.col_rbegin(2,(size_z-1)-k,j));
		    stepy += size_y;
		  }
		else
		  {
		    std::copy(data.col_begin(4) + stepz + stepy,
			      data.col_begin(4) + (stepz + stepy + size_y),
			      field.col_begin(1,(size_z-1)-k,j));
		    std::copy(data.col_begin(3) + stepz + stepy,
			      data.col_begin(3) + (stepz + stepy + size_y),
			      field.col_begin(0,(size_z-1)-k,j));
		    std::copy(data.col_begin(5) + stepz + stepy,
			      data.col_begin(5) + (stepz + stepy + size_y),
			      field.col_begin(2,(size_z-1)-k,j));
		    stepy += size_y;
		  }
	      }
	    stepz+=size_xy;
	  }
      }
    else
      {
	//cas x varie, y fixe
	step[0]=std::abs(*(data.col_begin(0)+1)-(*(data.col_begin(0))));
	step[1]=std::abs(*(data.col_begin(1)+size_x) - *(data.col_begin(1)));
	step[2]=std::abs(*(data.col_begin(2)+size_xy) - *(data.col_begin(2)));

	std::size_t stepz = 0;
	for(std::size_t k = 0 ; k < size_z; ++k)
	  {
	    std::size_t stepx = 0;
	    for(std::size_t j = 0; j < size_y; ++j)
	      {
		if(y_direct)
		  {
		    std::copy(data.col_begin(3) + stepz + stepx,
			      data.col_begin(3) + (stepz + stepx + size_x),
			      field.row_begin(0,(size_z-1)-k,(size_y - 1) - j));
		    std::copy(data.col_begin(4) + stepz + stepx, 
			      data.col_begin(4) + (stepz + stepx + size_x),
			      field.row_begin(1,(size_z-1)-k,(size_y - 1) - j));
		    std::copy(data.col_begin(5) + stepz + stepx, 
			      data.col_begin(5) + (stepz + stepx + size_x),
			      field.row_begin(2,(size_z-1)-k,(size_y - 1) - j));
		
		  }
		else
		  {
		    std::copy(data.col_begin(3) + stepz + stepx,
			      data.col_begin(3) + stepz + (stepx + size_x),
			      field.row_begin(0,(size_z-1)-k,j));
		    std::copy(data.col_begin(4) + stepz + stepx, 
			      data.col_begin(4) + (stepz + stepx + size_x),
			      field.row_begin(1,(size_z-1)-k,j));
		    std::copy(data.col_begin(5) + stepz + stepx, 
			      data.col_begin(5) + (stepz + stepx + size_x),
			      field.row_begin(2,(size_z-1)-k,j));
		  }
		stepx += size_x;
	      }
	    stepz+=size_xy;
	  }
      }

  
  }

 /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \author Ludovic Chatellier <ludovic.chatellier@univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2010/10/12
  ** \since 1.0.0
  ** \brief Create a XYZ DenseVector3dField3D from a tecplot file.
  ** \param file_path_name String of the file path name
  ** \param XYZ DenseVector3dField3D.
  ** \par The data format is the following:
  ** \code
  ** TITLE= title 
  ** VARIABLES= X Y Z U V W 
  ** ZONE T= zone, I= rows(), J= cols(), K= slices()
  ** x y z U(x,y,z) V(x,y,z) W(x,y,z)
  ** \endcode
  ** handled reading cases:
   ** \li x vary first, y written according to the increasing order, z written according to the increasing order 
   ** \li y vary first, y written according to the increasing order , z written according to the increasing order 
   ** \li x vary first, y written according to the decreasing order , z written according to the increasing order 
   ** \li y vary first, y written according to the decreasing order , z written according to the increasing order 
  ** \remarks x = j and y = (dim2 - 1) - i and z = (dim1 - 1) - k
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::DenseVector3dField3d<double> XYZ;
  ** slip::DenseVector3dField3d<double> field;
  ** slip::read_tecplot_vect3d("xyzuvw.dat",field);
  ** slip::read_tecplot_XYZ("xyzuvw.dat",XY2);
  ** std::cout<<"read_tecplot_vect3d xyzuvw.dat"<<std::endl;
  ** std::cout<<"XYZ : "<<std::endl;
  ** std::cout<<XYZ<<std::endl;
  ** std::cout<<"Field : "<<std::endl;
  ** std::cout<<field<<std::endl;
  ** \endcode
  */
  template <class MultiContainer3d>
  inline
  void read_tecplot_XYZ(const std::string& file_path_name,
			MultiContainer3d& XYZ)
  {
    slip::Array2d<double> data;
    slip::read_ascii_2d(data,file_path_name);
    std::size_t size =  data.rows();
    std::size_t size_xy = std::count(data.col_begin(2),data.col_end(2),data[0][2]);
    std::size_t size_z = size / size_xy;
    std::size_t size_y = std::count(data.col_begin(0),data.col_end(0),data[0][0]);
    size_y = size_y / size_z;
    std::size_t size_x = size / (size_y * size_z);

    slip::Point3d<double> init_point;
    init_point[0]=std::min(data[0][0],data[data.rows()-1][0]);
    init_point[1]=std::min(data[0][1],data[data.rows()-1][1]);
    init_point[2]=std::min(data[0][2],data[data.rows()-1][2]);

   
    bool y_direct = (init_point[1] == data[0][1]);
   
    XYZ.resize(size_z,size_y,size_x);

    if(data[0][0] == data[1][0])
      {
	//cas x fixe, y varie

	std::size_t stepz = 0;
	for(std::size_t k = 0 ; k < size_z; ++k)
	  {
	    std::size_t stepy = 0;
	    
	    for(std::size_t j = 0; j < size_x; ++j)
	      {
		if(y_direct)
		  {
		    std::copy(data.col_begin(1) + stepz + stepy,
			      data.col_begin(1) + (stepz + stepy + size_y),
			      XYZ.col_rbegin(1,(size_z-1)-k,j));
		    std::copy(data.col_begin(0) + stepz + stepy,
			      data.col_begin(0) + (stepz + stepy + size_y),
			      XYZ.col_rbegin(0,(size_z-1)-k,j));
		    std::copy(data.col_begin(2) + stepz + stepy,
			      data.col_begin(2) + (stepz + stepy + size_y),
			      XYZ.col_rbegin(2,(size_z-1)-k,j));
		    stepy += size_y;
		  }
		else
		  {
		    std::copy(data.col_begin(1) + stepz + stepy,
			      data.col_begin(1) + (stepz + stepy + size_y),
			      XYZ.col_begin(1,(size_z-1)-k,j));
		    std::copy(data.col_begin(0) + stepz + stepy,
			      data.col_begin(0) + (stepz + stepy + size_y),
			      XYZ.col_begin(0,(size_z-1)-k,j));
		    std::copy(data.col_begin(2) + stepz + stepy,
			      data.col_begin(2) + (stepz + stepy + size_y),
			      XYZ.col_begin(2,(size_z-1)-k,j));
		    stepy += size_y;
		  }
	      }
	    stepz+=size_xy;
	  }
      }
    else
      {
	//cas x varie, y fixe
	std::size_t stepz = 0;
	for(std::size_t k = 0 ; k < size_z; ++k)
	  {
	    std::size_t stepx = 0;
	    for(std::size_t j = 0; j < size_y; ++j)
	      {
		if(y_direct)
		  {
		    std::copy(data.col_begin(0) + stepz + stepx,
			      data.col_begin(0) + (stepz + stepx + size_x),
			      XYZ.row_begin(0,(size_z-1)-k,(size_y - 1) - j));
		    std::copy(data.col_begin(1) + stepz + stepx, 
			      data.col_begin(1) + (stepz + stepx + size_x),
			      XYZ.row_begin(1,(size_z-1)-k,(size_y - 1) - j));
		    std::copy(data.col_begin(2) + stepz + stepx, 
			      data.col_begin(2) + (stepz + stepx + size_x),
			      XYZ.row_begin(2,(size_z-1)-k,(size_y - 1) - j));
		
		  }
		else
		  {
		    std::copy(data.col_begin(0) + stepz + stepx,
			      data.col_begin(0) + stepz + (stepx + size_x),
			      XYZ.row_begin(0,(size_z-1)-k,j));
		    std::copy(data.col_begin(1) + stepz + stepx, 
			      data.col_begin(1) + (stepz + stepx + size_x),
			      XYZ.row_begin(1,(size_z-1)-k,j));
		    std::copy(data.col_begin(2) + stepz + stepx, 
			      data.col_begin(2) + (stepz + stepx + size_x),
			      XYZ.row_begin(2,(size_z-1)-k,j));
		  }
		stepx += size_x;
	      }
	    stepz+=size_xy;
	  }
      }
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \author Ludovic Chatellier <ludovic.chatellier@univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2009/03/27
  ** \since 1.0.0
  ** \brief Create a XYZ DenseVector3dField3D from a tecplot file.
  ** \param file_path_name String of the file path name
  ** \param X Container1d containing the X locations.
  ** \param Y Container1d containing the Y locations.
  ** \param Z Container1d containing the Z locations.
  ** \par The data format is the following:
  ** \code
  ** TITLE= title 
  ** VARIABLES= X Y Z U V W 
  ** ZONE T= zone, I= rows(), J= cols(), K= slices()
  ** x y z U(x,y,z) V(x,y,z) W(x,y,z)
  ** \endcode
     ** handled reading cases:
   ** \li x vary first, y written according to the increasing order, z written according to the increasing order 
   ** \li y vary first, y written according to the increasing order , z written according to the increasing order 
   ** \li x vary first, y written according to the decreasing order , z written according to the increasing order 
   ** \li y vary first, y written according to the decreasing order , z written according to the increasing order 
  ** \remarks x = j and y = (dim2 - 1) - i and z = (dim1 - 1) - k
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::Array<double> X3;
  ** slip::Array<double> Y3;
  ** slip::Array<double> Z3;
  ** slip::read_tecplot_XYZ("xyzuvw.dat",X3,Y3,Z3);
  ** std::cout<<"X : \n"<<X3<<std::endl;
  ** std::cout<<"Y : \n"<<Y3<<std::endl;
  ** std::cout<<"Z : \n"<<Z3<<std::endl;
  ** \endcode
  */
  template <class Vector1, class Vector2, class Vector3>
  inline
  void read_tecplot_XYZ(const std::string& file_path_name,
			Vector1& X,
			Vector2& Y,
			Vector3& Z)
  {
 
    typedef typename slip::Array2d<double>::col_iterator col_iterator;
    slip::Array2d<double> data;
    slip::read_ascii_2d(data,file_path_name);
    std::size_t size =  data.rows();
    std::size_t size_xy = std::count(data.col_begin(2),data.col_end(2),data[0][2]);
    std::size_t size_z = size / size_xy;
    std::size_t size_y = std::count(data.col_begin(0),data.col_end(0),data[0][0]);
    size_y = size_y / size_z;
    std::size_t size_x = size / (size_y * size_z);

    slip::Point3d<double> init_point;
    init_point[0]=std::min(data[0][0],data[data.rows()-1][0]);
    init_point[1]=std::min(data[0][1],data[data.rows()-1][1]);
    init_point[2]=std::min(data[0][2],data[data.rows()-1][2]);

   
    bool y_direct = (init_point[1] == data[0][1]);
   
    X.resize(size_x);
    Y.resize(size_y);
    Z.resize(size_z);
    

    if(data[0][0] == data[1][0])
      {

	//cas x fixe, y varie
		if(y_direct)
	  {
	    //copy Y
	    std::copy(data.col_begin(1),data.col_begin(1)+size_y,Y.begin());
	    //copy X
	    std::copy(slip::stride_iterator<col_iterator>(data.col_begin(0),size_y),
		      slip::stride_iterator<col_iterator>(data.col_begin(0),size_y) +size_x,
		      X.begin());
	    //copy Z
	    std::copy(slip::stride_iterator<col_iterator>(data.col_begin(2),size_xy),
		      slip::stride_iterator<col_iterator>(data.col_begin(2),size_xy) + size_z,
		      Z.begin());  
	    
	  }
	else
	  {
	    //copy Y
	    std::copy(data.col_begin(1),data.col_begin(1)+size_y,Y.rbegin());
	    //copy X
	    std::copy(slip::stride_iterator<col_iterator>(data.col_begin(0),size_y),
		      slip::stride_iterator<col_iterator>(data.col_begin(0),size_y) +size_x,
		      X.begin());
	    //copy Z
	    std::copy(slip::stride_iterator<col_iterator>(data.col_begin(2),size_xy),
		      slip::stride_iterator<col_iterator>(data.col_begin(2),size_xy) + size_z,
		      Z.begin());  
	    
	  }
	


	  
      }
    else
      {
	//cas x varie, y fixe
	if(y_direct)
	  {
	    //copy X
	    std::copy(data.col_begin(0),data.col_begin(0)+size_x,X.begin());
	    //copy Y
	    std::copy(slip::stride_iterator<col_iterator>(data.col_begin(1),size_x),
		      slip::stride_iterator<col_iterator>(data.col_begin(1),size_x) +size_y,
		      Y.begin());
	    //copy Z
	    std::copy(slip::stride_iterator<col_iterator>(data.col_begin(2),size_xy),
		      slip::stride_iterator<col_iterator>(data.col_begin(2),size_xy) + size_z,
		      Z.begin());  
	  }
	else
	  {
	    //copy X
	    std::copy(data.col_begin(0),data.col_begin(0)+size_x,X.begin());
	    //copy Y
	    std::copy(slip::stride_iterator<col_iterator>(data.col_begin(1),size_x),
		      slip::stride_iterator<col_iterator>(data.col_begin(1),size_x) +size_y,
		      Y.rbegin());
	    //copy Z
	    std::copy(slip::stride_iterator<col_iterator>(data.col_begin(2),size_xy),
		      slip::stride_iterator<col_iterator>(data.col_begin(2),size_xy) + size_z,
		      Z.begin());   
	  }

	
      }

  }

 



  /*!  \brief Create a XYZ DenseVector3dField3D from a gnuplot file.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \author Ludovic Chatellier <ludovic.chatellier@univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/05/24
  ** \since 1.0.0
  ** \param file_path_name String of the file path name.
  ** \param XYZ DenseVector3dField3D.
  ** \par The data format is the following:
  ** \code
  ** TITLE= title 
  ** VARIABLES= X Y Z U V W 
  ** ZONE T= zone, I= rows(), J= cols(), K= slices()
  ** x y z U(x,y,z) V(x,y,z) W(x,y,z)
  ** \endcode
     ** handled reading cases:
   ** \li x vary first, y written according to the increasing order, z written according to the increasing order 
   ** \li y vary first, y written according to the increasing order , z written according to the increasing order 
   ** \li x vary first, y written according to the decreasing order , z written according to the increasing order 
   ** \li y vary first, y written according to the decreasing order , z written according to the increasing order 
  ** \remarks x = j and y = (dim2 - 1) - i and z = (dim1 - 1) - k
  ** \remarks This function is provided in the hope to be usefull but WITHOUT ANY WARRANTY
  ** \par Example:
  ** \code
  ** slip::DenseVector3dField3d<double> XYZ;
  ** slip::DenseVector3dField3d<double> field;
  ** slip::read_gnuplot_vect3d("xyzuvw.dat",field);
  ** slip::read_gnuplot_XYZ("xyzuvw.dat",XY2);
  ** std::cout<<"read_gnuplot_vect3d xyzuvw.dat"<<std::endl;
  ** std::cout<<"XYZ : "<<std::endl;
  ** std::cout<<XYZ<<std::endl;
  ** std::cout<<"Field : "<<std::endl;
  ** std::cout<<field<<std::endl;
  ** \endcode
  */
  template <class MultiContainer3d>
  inline
  void read_gnuplot_XYZ(const std::string& file_path_name,
			MultiContainer3d& XYZ)
  {
    slip::read_tecplot_XYZ(file_path_name,XYZ);
  }

/* @} */



  
 

 /** \name 1d ASCII containers input/output */
  /* @{ */
  
 /*!
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.2
 ** \date 2008/02/12
 ** \since 1.0.0
 ** \brief Write a Container1d to an ASCII file.
 ** \param first InputIterator to the first element of the range.
 ** \param last InputIterator to one past-the-end element of the range.
 ** \param file_path_name String of the file path name
 **
 ** \remarks This file format is compatible with matlab, octave and gnuplot
 **  ASCII files
 ** \remarks Write is useful to write char or unsigned char value
 ** \par Example:
 ** \code
 ** float tab[] = {1.0,2.0,3.0,4.0,5.0,6.0};
 **  slip::Array<float> Tab(6,tab);
 **  slip::write_ascii_1d<slip::Array<float>::iterator,float>(Tab.begin(),Tab.end(),"test.dat");
 ** \endcode
 */
  template <typename InputIterator, typename WriteType>
  inline
  void write_ascii_1d(InputIterator first,
		      InputIterator last,
		      const std::string& file_path_name)
  {
    std::ofstream output(file_path_name.c_str(),std::ios::out);
    try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  }
	
 	std::copy(first,last,std::ostream_iterator<WriteType>(output,"\n"));
	output.close();
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
     
  }

/*!
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.1
 ** \date 2008/05/24
 ** \since 1.0.0
 ** \brief Read a Container1d from an ASCII file.
 ** \param file_path_name String of the file path name
 ** \param container Container 1d.
 ** \remarks This file format is compatible with matlab, octave and gnuplot ASCII files
 ** \par Example:
 ** \code
 ** float tab[] = {1.0,2.0,3.0,4.0,5.0,6.0};
 ** slip::Array<float> Tab(6,tab);
 ** slip::write_ascii_1d<slip::Array<float>::iterator,float>(Tab.begin(),Tab.end(),"test.dat");
 ** slip::Array<float> Tab2;
 ** slip::read_ascii_1d("test.dat",Tab2);
 ** \endcode
 */
 template <typename Container1d>
 inline
 void read_ascii_1d(const std::string& file_path_name,
		    Container1d& container)
  {
    std::ifstream input;

    try
      {
	input.open(file_path_name.c_str());
	if(!input)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);         
	  }

	//computes the number of rows
	int nr = 0;
	std::string line;
	while ( std::getline(input, line ) && line.length()!=0)
	  {
	    ++nr;
	  }

	input.close();
	container.resize((std::size_t)nr);
	input.open(file_path_name.c_str());
	if(!input)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);         
	  }

	//reading datas
	for(std::size_t i = 0; i < container.size(); ++i)
	  {
	    typename Container1d::value_type tmp;
	    input>>tmp;
	    container[i] = tmp;
	  }
	input.close();

      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	input.close();
	exit(1);
      }

  }
/* @} */

   /** \name grayscale images input/output */
  /* @{ */

  #ifdef HAVE_MAGICK
  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a RandomAccessIterator range to an image file.
  ** \param first RandomAccessIterator.
  ** \param w Width of the image.
  ** \param h Height of the image.
  ** \param storage_t ImageMagick storage type : CharPixel, IntegerPixel,
  **                   ShortPixel, LongPixel, FloatPixel or DoublePixel.
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator>
  inline
  void write_magick_gray(RandomAccessIterator first,
			 const std::size_t w,
			 const std::size_t h,
			 const StorageType storage_t,
			 const std::string& file_path_name)
  {
    //Create the wand 
    MagickWandGenesis();
    MagickWand* wand = NewMagickWand();
   
    //Write the image located at file_path_name
    MagickSetImageColorspace(wand,GRAYColorspace);
    unsigned long width  = (unsigned long)w;
    unsigned long height = (unsigned long)h;
    MagickConstituteImage(wand,width,height,"I",storage_t,first);
    MagickBooleanType status = MagickWriteImage(wand,file_path_name.c_str());
    if(status == MagickFalse)
      {
	ThrowWandException(wand);
      }
    wand = DestroyMagickWand(wand); 
    MagickWandTerminus();
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a char RandomAccessIterator range to an image file.
  ** \param first RandomAccessIterator.
  ** \param w Width of the image.
  ** \param h Height of the image.
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator>
  inline
  void write_magick_gray_char(RandomAccessIterator first,
			      const std::size_t w,
			      const std::size_t h,
			      const std::string& file_path_name)
  {
    slip::write_magick_gray(first,w,h,CharPixel,file_path_name);
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a short RandomAccessIterator range to an image
  ** file.
  ** \param first RandomAccessIterator.
  ** \param w Width of the image.
  ** \param h Height of the image.
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator>
  inline
  void write_magick_gray_short(RandomAccessIterator first,
			       const std::size_t w,
			       const std::size_t h,
			       const std::string& file_path_name)
    {
      slip::write_magick_gray(first,w,h,ShortPixel,file_path_name);
    }


  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes an integer RandomAccessIterator range to an image
  ** file.
  ** \param first RandomAccessIterator.
  ** \param w Width of the image.
  ** \param h Height of the image.
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator>
  inline
  void write_magick_gray_int(RandomAccessIterator first,
			     const std::size_t w,
			     const std::size_t h,
			     const std::string& file_path_name)
  {
    slip::write_magick_gray(first,w,h,IntegerPixel,file_path_name);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \brief Writes a long RandomAccessIterator range to an image file.
  ** \param first RandomAccessIterator.
  ** \param w Width of the image.
  ** \param h Height of the image.
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator>
  inline
  void write_magick_gray_long(RandomAccessIterator first,
			      const std::size_t w,
			      const std::size_t h,
			      const std::string& file_path_name)
  {
    slip::write_magick_gray(first,w,h,LongPixel,file_path_name);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a float RandomAccessIterator range to an image
  ** file.
  ** \param first RandomAccessIterator.
  ** \param w Width of the image.
  ** \param h Height of the image.
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator>
  inline
  void write_magick_gray_float(RandomAccessIterator first,
			       const std::size_t w,
			       const std::size_t h,
			       const std::string& file_path_name)
  {
    slip::write_magick_gray(first,w,h,FloatPixel,file_path_name);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a double RandomAccessIterator range to an image file.
  ** \param first RandomAccessIterator.
  ** \param w Width of the image.
  ** \param h Height of the image.
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator>
  inline
  void write_magick_gray_double(RandomAccessIterator first,
				const std::size_t w,
				const std::size_t h,
				const std::string& file_path_name)
  {
    slip::write_magick_gray(first,w,h,DoublePixel,file_path_name);
  }


   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a grayscale image from RandomAccessIterator2d range 
  **        to a file.
  ** \param upper_left RandomAccessIterator2d to the upper_left element
  ** \param bottom_right RandomAccessIterator2d to bottom_right element.
  ** \param storage_t ImageMagick storage type : CharPixel, IntegerPixel,
  **                   ShortPixel, LongPixel, FloatPixel or DoublePixel 
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator2d>
  inline
  void write_magick_gray(RandomAccessIterator2d upper_left,
			 RandomAccessIterator2d bottom_right,
			 const StorageType storage_t,
			 const std::string& file_path_name)
  {
    
    typename RandomAccessIterator2d::difference_type size2d 
      = bottom_right - upper_left;
    
    slip::Array<typename RandomAccessIterator2d::value_type> array(size2d[0] * size2d[1]);
    array.fill(upper_left,bottom_right);
    slip::write_magick_gray(array.begin(),
			    (std::size_t)size2d[1],
			    (std::size_t)size2d[0],
			    storage_t,
			    file_path_name);
  }


   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a char grayscale image from RandomAccessIterator2d range 
  **        to a file.
  ** \param upper_left RandomAccessIterator2d to the upper_left element
  ** \param bottom_right RandomAccessIterator2d to bottom_right element.
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator2d>
  inline
  void write_magick_gray_char(RandomAccessIterator2d upper_left,
			      RandomAccessIterator2d bottom_right,
			      const std::string& file_path_name)
  {
    slip::write_magick_gray(upper_left,bottom_right,CharPixel,file_path_name);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a short grayscale image from RandomAccessIterator2d range 
  **        to a file.
  ** \param upper_left RandomAccessIterator2d to the upper_left element
  ** \param bottom_right RandomAccessIterator2d to bottom_right element.
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator2d>
  inline
  void write_magick_gray_short(RandomAccessIterator2d upper_left,
			       RandomAccessIterator2d bottom_right,
			       const std::string& file_path_name)
  {
    slip::write_magick_gray(upper_left,bottom_right,ShortPixel,file_path_name);
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes an integer grayscale image from RandomAccessIterator2d 
  **        range to a file.
  ** \param upper_left RandomAccessIterator2d to the upper_left element
  ** \param bottom_right RandomAccessIterator2d to bottom_right element.
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator2d>
  inline
  void write_magick_gray_int(RandomAccessIterator2d upper_left,
			     RandomAccessIterator2d bottom_right,
			     const std::string& file_path_name)
  {
    slip::write_magick_gray(upper_left,bottom_right,IntegerPixel,file_path_name);
  }

    /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes an long grayscale image from RandomAccessIterator2d 
  **        range to a file.
  ** \param upper_left RandomAccessIterator2d to the upper_left element
  ** \param bottom_right RandomAccessIterator2d to bottom_right element.
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator2d>
  inline
  void write_magick_gray_long(RandomAccessIterator2d upper_left,
			      RandomAccessIterator2d bottom_right,
			      const std::string& file_path_name)
  {
    slip::write_magick_gray(upper_left,bottom_right,LongPixel,file_path_name);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes an float grayscale image from RandomAccessIterator2d 
  **        range to a file.
  ** \param upper_left RandomAccessIterator2d to the upper_left element
  ** \param bottom_right RandomAccessIterator2d to bottom_right element.
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator2d>
  inline
  void write_magick_gray_float(RandomAccessIterator2d upper_left,
			       RandomAccessIterator2d bottom_right,
			       const std::string& file_path_name)
  {
    slip::write_magick_gray(upper_left,bottom_right,FloatPixel,file_path_name);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes an double grayscale image from RandomAccessIterator2d 
  **        range to a file.
  ** \param upper_left RandomAccessIterator2d to the upper_left element.
  ** \param bottom_right RandomAccessIterator2d to bottom_right element.
  ** \param file_path_name String of the file path name
  */
  template<typename RandomAccessIterator2d>
  inline
  void write_magick_gray_double(RandomAccessIterator2d upper_left,
				RandomAccessIterator2d bottom_right,
				const std::string& file_path_name)
  {
    slip::write_magick_gray(upper_left,bottom_right,DoublePixel,file_path_name);
  }


  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Reads a grayscale image from file.
  ** \param file_path_name String of the file path name
  ** \param container A Container2d where to store the image.
  ** \param storage_t ImageMagick storage type : CharPixel, IntegerPixel,
  **                   ShortPixel, LongPixel, FloatPixel or DoublePixel 
  
  */
  template<typename Container2d>
  inline
  void read_magick_gray(const std::string& file_path_name,
			Container2d& container,
			const StorageType storage_t)
  {
    //Create the wand
    MagickWandGenesis();
    MagickWand* wand = NewMagickWand();
    //Read the image located at file_path_name
    MagickBooleanType status= MagickReadImage(wand,file_path_name.c_str());
    if (status == MagickFalse)
      {
	ThrowWandException(wand);
      }
    unsigned long width  = MagickGetImageWidth(wand);
    unsigned long height = MagickGetImageHeight(wand);
    container.resize(std::size_t(height),
		     std::size_t(width),
		     0);
   //  // In 6.4.5.4 MagickGetImagePixels changed to MagickGetAuthenticPixels
//     #if MagickLibVersion == 0x645
//     #define MagickGetImagePixels MagickGetAuthenticPixels
//     #endif
    // In 6.4.6 MagickGetImagePixels changed to MagickExportImagePixels
    #if MagickLibVersion >= 0x646
    #define MagickGetImagePixels MagickExportImagePixels
    #endif
   
    MagickGetImagePixels(wand,0,0,width,height,"I",storage_t,container.begin());
    
    wand = DestroyMagickWand(wand);
    MagickWandTerminus();
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Reads a char grayscale image from file.
  ** \param file_path_name String of the file path name
  ** \param container A Container2d where to store the image.
  
  */
  template<typename Container2d>
  inline
  void read_magick_gray_char(const std::string& file_path_name,
			     Container2d& container)
  {
    read_magick_gray(file_path_name,container,CharPixel);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Reads a short grayscale image from file.
  ** \param file_path_name String of the file path name
  ** \param container A Container2d where to store the image.
  
  */
  template<typename Container2d>
  inline
  void read_magick_gray_short(const std::string& file_path_name,
			      Container2d& container)
  {
    read_magick_gray(file_path_name,container,ShortPixel);
  }

     /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Reads a integer grayscale image from file.
  ** \param file_path_name String of the file path name
  ** \param container A Container2d where to store the image.
  
  */
  template<typename Container2d>
  inline
  void read_magick_gray_int(const std::string& file_path_name,
			     Container2d& container)
  {
    read_magick_gray(file_path_name,container,IntegerPixel);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Reads a long grayscale image from file.
  ** \param file_path_name String of the file path name
  ** \param container A Container2d where to store the image.
  
  */
  template<typename Container2d>
  inline
  void read_magick_gray_long(const std::string& file_path_name,
			     Container2d& container)
  {
    read_magick_gray(file_path_name,container,LongPixel);
  }

     /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Reads a float grayscale image from file.
  ** \param file_path_name String of the file path name
  ** \param container A Container2d where to store the image.
  
  */
  template<typename Container2d>
  inline
  void read_magick_gray_float(const std::string& file_path_name,
			      Container2d& container)
  {
    read_magick_gray(file_path_name,container,FloatPixel);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Reads a double grayscale image from file.
  ** \param file_path_name String of the file path name
  ** \param container A Container2d where to store the image.
  
  */
  template<typename Container2d>
  inline
  void read_magick_gray_double(const std::string& file_path_name,
			       Container2d& container)
  {
    read_magick_gray(file_path_name,container,DoublePixel);
  }

  /* @} */

   /** \name colorimage input/output */
  /* @{ */
  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a char color image from RandomAccessIterator range
  ** to a file.
  ** \param first RandomAccessIterator.
  ** \param w Width of the image.
  ** \param h Height of the image.
  ** \param file_path_name String of the file path name
  
  */
  template<typename RandomAccessIterator>
  inline
  void write_magick_color_char(RandomAccessIterator first,
			       const std::size_t w,
			       const std::size_t h,
			       const std::string& file_path_name)
  {
    //Create the wand
    MagickWandGenesis();
    MagickWand* wand = NewMagickWand();
    
    //Write the image located at file_path_name
    MagickSetImageColorspace(wand,RGBColorspace);
    unsigned long width  = (unsigned long)w;
    unsigned long height = (unsigned long)h;
    MagickConstituteImage(wand,width,height,"RGB",CharPixel,(unsigned char*)first);
    MagickBooleanType status = MagickWriteImage(wand,file_path_name.c_str());
    if(status == MagickFalse)
      {
	ThrowWandException(wand);
      }
    wand = DestroyMagickWand(wand); 
    MagickWandTerminus();
  }  

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a short color image from RandomAccessIterator range
  ** to a file.
  ** \param first RandomAccessIterator.
  ** \param  w Width of the image.
  ** \param  h Height of the image.
  ** \param file_path_name String of the file path name
  
  */
  template<typename RandomAccessIterator>
  inline
  void write_magick_color_short(RandomAccessIterator first,
				const std::size_t w,
				const std::size_t h,
				const std::string& file_path_name)
  {
    //Create the wand
    MagickWandGenesis();
    MagickWand* wand = NewMagickWand();
    
    //Write the image located at file_path_name
    MagickSetImageColorspace(wand,RGBColorspace);
    unsigned long width  = (unsigned long)w;
    unsigned long height = (unsigned long)h;
    MagickConstituteImage(wand,width,height,"RGB",ShortPixel,(short*)first);
     MagickBooleanType status = MagickWriteImage(wand,file_path_name.c_str());
    if(status == MagickFalse)
      {
	ThrowWandException(wand);
      }
    wand = DestroyMagickWand(wand); 
    MagickWandTerminus();
  }  

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes an integer color image from RandomAccessIterator range 
  **        to a file.
  ** \param first RandomAccessIterator.
  ** \param  w Width of the image.
  ** \param  h Height of the image.
  ** \param file_path_name String of the file path name
  
  */
  template<typename RandomAccessIterator>
  inline
  void write_magick_color_int(RandomAccessIterator first,
			      const std::size_t w,
			      const std::size_t h,
			      const std::string& file_path_name)
  {
    //Create the wand
    MagickWandGenesis();
    MagickWand* wand = NewMagickWand();
    
    //Write the image located at file_path_name
    MagickSetImageColorspace(wand,RGBColorspace);
    unsigned long width  = (unsigned long)w;
    unsigned long height = (unsigned long)h;
    MagickConstituteImage(wand,width,height,"RGB",IntegerPixel,(int*)first);
    MagickBooleanType status = MagickWriteImage(wand,file_path_name.c_str());
    if(status == MagickFalse)
      {
	ThrowWandException(wand);
      }
    wand = DestroyMagickWand(wand); 
    MagickWandTerminus();
  } 

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a long color image from RandomAccessIterator range
  ** to a file.
  ** \param first RandomAccessIterator.
  ** \param w Width of the image.
  ** \param h Height of the image.
  ** \param file_path_name String of the file path name
  
  */
  template<typename RandomAccessIterator>
  inline
  void write_magick_color_long(RandomAccessIterator first,
			       const std::size_t w,
			       const std::size_t h,
			       const std::string& file_path_name)
  {
    //Create the wand
    MagickWandGenesis();
    MagickWand* wand = NewMagickWand();
    
    //Write the image located at file_path_name
    MagickSetImageColorspace(wand,RGBColorspace);
    unsigned long width  = (unsigned long)w;
    unsigned long height = (unsigned long)h;
    MagickConstituteImage(wand,width,height,"RGB",LongPixel,(long*)first);
    MagickBooleanType status = MagickWriteImage(wand,file_path_name.c_str());
    if(status == MagickFalse)
      {
	ThrowWandException(wand);
      }
    wand = DestroyMagickWand(wand); 
    MagickWandTerminus();
  } 

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a float color image from RandomAccessIterator range to a file.
  ** \param first RandomAccessIterator.
  ** \param  w Width of the image.
  ** \param  h Height of the image.
  ** \param file_path_name String of the file path name
  
  */
  template<typename RandomAccessIterator>
  inline
  void write_magick_color_float(RandomAccessIterator first,
				const std::size_t w,
				const std::size_t h,
				const std::string& file_path_name)
  {
    //Create the wand
    MagickWandGenesis();
    MagickWand* wand = NewMagickWand();
    
    //Write the image located at file_path_name
    MagickSetImageColorspace(wand,RGBColorspace);
    unsigned long width  = (unsigned long)w;
    unsigned long height = (unsigned long)h;
    MagickConstituteImage(wand,width,height,"RGB",FloatPixel,(float*)first);
    MagickBooleanType status = MagickWriteImage(wand,file_path_name.c_str());
    if(status == MagickFalse)
      {
	ThrowWandException(wand);
      }
    wand = DestroyMagickWand(wand); 
    MagickWandTerminus();
  } 

 
  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a double color image from RandomAccessIterator range to a file.
  ** \param first RandomAccessIterator.
  ** \param  w Width of the image.
  ** \param  h Height of the image.
  ** \param file_path_name String of the file path name
  
  */
  template<typename RandomAccessIterator>
  inline
  void write_magick_color_double(RandomAccessIterator first,
				 const std::size_t w,
				 const std::size_t h,
				 const std::string& file_path_name)
  {
    //Create the wand
    MagickWandGenesis();
    MagickWand* wand = NewMagickWand();
    
    //Write the image located at file_path_name
    MagickSetImageColorspace(wand,RGBColorspace);
    unsigned long width  = (unsigned long)w;
    unsigned long height = (unsigned long)h;
    MagickConstituteImage(wand,width,height,"RGB",DoublePixel,(float*)first);
    MagickBooleanType status = MagickWriteImage(wand,file_path_name.c_str());
    if(status == MagickFalse)
      {
	ThrowWandException(wand);
      }
    wand = DestroyMagickWand(wand); 
    MagickWandTerminus();
  } 

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes a color image from RandomAccessIterator2d range to a file.
  ** \param upper_left RandomAccessIterator2d to the upper_left element
  ** \param bottom_right RandomAccessIterator2d to bottom_right element.
  ** \param storage_t ImageMagick storage type : CharPixel, IntegerPixel,
  **                   ShortPixel, LongPixel, FloatPixel or DoublePixel 
  ** \param file_path_name String of the file path name
  
  */
  template<typename RandomAccessIterator2d>
  inline
  void write_magick_color(RandomAccessIterator2d upper_left,
			  RandomAccessIterator2d bottom_right,
			  const StorageType storage_t,
			  const std::string& file_path_name)
  {
    
    typename RandomAccessIterator2d::difference_type size2d 
      = bottom_right - upper_left;
    
    slip::Array<typename RandomAccessIterator2d::value_type> array(size2d[0] * size2d[1]);
    array.fill(upper_left,bottom_right);
    if(storage_t == CharPixel)
      {
	slip::write_magick_color_char(array.begin(),
				      (std::size_t)size2d[1],
				      (std::size_t)size2d[0],
				      file_path_name);

      }
    else if(storage_t == ShortPixel)
      {
	slip::write_magick_color_short(array.begin(),
				       (std::size_t)size2d[1],
				       (std::size_t)size2d[0],
				       file_path_name);
      }
	
    else if(storage_t == IntegerPixel)
      {
	slip::write_magick_color_int(array.begin(),
				     (std::size_t)size2d[1],
				     (std::size_t)size2d[0],
				     file_path_name);
      }
    else if(storage_t == LongPixel)
      {
	slip::write_magick_color_long(array.begin(),
				      (std::size_t)size2d[1],
				      (std::size_t)size2d[0],
				      file_path_name);
      }
    else if(storage_t == FloatPixel)
      {
	slip::write_magick_color_float(array.begin(),
				       (std::size_t)size2d[1],
				       (std::size_t)size2d[0],
				       file_path_name);
      }
    else if(storage_t == DoublePixel)
      {
     	slip::write_magick_color_double(array.begin(),
					(std::size_t)size2d[1],
					(std::size_t)size2d[0],
					file_path_name);
      }
    else
      {
	std::cerr<<"Wrong storage type"<<std::endl;
      }
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Read a color image from file.
  ** \param file_path_name String of the file path name
  ** \param container GenericMultiComponent2d (3 components) where to 
  **                   store the image.
  ** \param storage_t ImageMagick storage type : CharPixel, IntegerPixel,
  **                   ShortPixel, LongPixel, FloatPixel or DoublePixel 
  
  */
  template<typename Container2d>
  inline
  void read_magick_color(const std::string& file_path_name,
			 Container2d& container,
			 const StorageType storage_t)
  {
    //Create the wand
    MagickWandGenesis();
    MagickWand* wand = NewMagickWand();
    //Read the image located at file_path_name
    MagickBooleanType status = MagickReadImage(wand,file_path_name.c_str());
    if (status == MagickFalse)
      {
	ThrowWandException(wand);
      }
    unsigned long width  = MagickGetImageWidth(wand);
    unsigned long height = MagickGetImageHeight(wand);
    container.resize(std::size_t(height),std::size_t(width));

    MagickGetImagePixels(wand,0,0,width,height,"RGB",storage_t,&(container[0][0][0]));
    wand = DestroyMagickWand(wand);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Read a color image from file.
  ** \param file_path_name String of the file path name
  ** \param container char GenericMultiComponent2d (3 components) where to 
  **                   store the image.
  
  */
  template<typename Container2d>
  inline
  void read_magick_color_char(const std::string& file_path_name,
			     Container2d& container)
  {
    read_magick_color(file_path_name,container,CharPixel);
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Read a color image from file.
  ** \param file_path_name String of the file path name
  ** \param container short GenericMultiComponent2d (3 components) where to 
  **                   store the image.
  
  */
  template<typename Container2d>
  inline
  void read_magick_color_short(const std::string& file_path_name,
			      Container2d& container)
  {
    read_magick_color(file_path_name,container,ShortPixel);
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Read a color image from file.
  ** \param file_path_name String of the file path name
  ** \param container int GenericMultiComponent2d (3 components) where to 
  **                   store the image.
  
  */
  template<typename Container2d>
  inline
  void read_magick_color_int(const std::string& file_path_name,
			     Container2d& container)
  {
    read_magick_color(file_path_name,container,IntegerPixel);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \brief Read a color image from file.
  ** \param file_path_name String of the file path name
  ** \param container long GenericMultiComponent2d (3 components) where to 
  **                   store the image.
  
  */
  template<typename Container2d>
  inline
  void read_magick_color_long(const std::string& file_path_name,
			     Container2d& container)
  {
    read_magick_color(file_path_name,container,LongPixel);
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Read a color image from file.
  ** \param file_path_name String of the file path name
  ** \param container float GenericMultiComponent2d (3 components) where to 
  **                   store the image.
  
  */
  template<typename Container2d>
  inline
  void read_magick_color_float(const std::string& file_path_name,
			      Container2d& container)
  {
    read_magick_color(file_path_name,container,FloatPixel);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Read a color image from file.
  ** \param file_path_name String of the file path name
  ** \param container double GenericMultiComponent2d (3 components) where to 
  **                   store the image.
  
  */
  template<typename Container2d>
  inline
  void read_magick_color_double(const std::string& file_path_name,
			       Container2d& container)
  {
    read_magick_color(file_path_name,container,DoublePixel);
  }


/* @} */
#endif //HAVE_MAGICK

   /** \name container3d (volume) input/output */
  /* @{ */
   /*!
 ** \brief Write a Container3D to a tecplot file.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.1
 ** \date 2008/02/14
 ** \since 1.0.0
 ** \param container Container3d.
  ** \param file_path_name String of the file path name
  ** \param title
  ** The data format is the following :
  ** TITLE= title 
  ** VARIABLES= X Y Z I
  ** ZONE T= data, I= rows() J=  cols() K= slices() F=POINT
  ** x y z I(x,y,z)
  ** \remarks x = j and y = (dim1 - 1) - i and z = k
  
  */
  template <typename Container3d>
  inline
  void write_tecplot_vol(const Container3d& container,
			 const std::string& file_path_name,
			 const std::string& title)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }

	int dim1 = int(container.dim1());
	int dim2 = int(container.dim2());
	int dim3 = int(container.dim3());
	//write file informations
	output<<"TITLE=\"" <<title<<"\""<<std::endl;
	output<<"VARIABLES=\"X\",\"Y\",\"Z\",\"I\"" <<std::endl;
	output<<"ZONE T=\"data\" I="<<dim3<<" J="<<dim2<<" K="<<dim1<< "  F=POINT"<<std::endl;
	//write data
	std::size_t dim2_1 = dim2 - 1;
	for(int z = 0; z < dim1; ++z)
	  {
	    for(int x = 0; x < dim3; ++x)
	      {
		for(int y = 0; y < dim2 ; ++y)
		  {
		    output<<x<<" "<<y<<" "<<z<<" "<<container[z][dim2_1-y][x]<<"\n";
		  }
	      }
	  }
	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }


  /*!
 ** \brief Write a Container3D to a tecplot file handle 3d real regular grid.
 ** \author Adrien Berchet <adrien.berchet_AT_univ-poitiers.fr>
 ** \version 0.0.1
 ** \date 2012/02/21
 ** \since 1.4.1
 ** \param container Container3d.
 ** \param file_path_name String of the file path name
 ** \param title
 ** The data format is the following :
 ** TITLE= title 
 ** VARIABLES= X Y Z I
 ** ZONE T= data, I= rows() J=  cols() K= slices() F=POINT
 ** x y z I(x,y,z)
 ** \remarks x = j and y = (dim1 - 1) - i and z = k
 */
  template <typename Container3d, typename T>
  inline
  void write_tecplot_vol(const Container3d& container,
			 const slip::Point3d<T>& init_point,
			 const slip::Point3d<T>& step,
			 const std::string file_path_name,
			 const std::string title="title",
			 const std::string zone="data",
			 const int precision = 6)
  {
   std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }

	int default_precision = output.precision();
	output.precision(precision);
	int dim1 = int(container.dim1());
	int dim2 = int(container.dim2());
	int dim3 = int(container.dim3());
	//write file informations
	output<<"TITLE=\"" <<title<<"\""<<std::endl;
	output<<"VARIABLES=\"X\",\"Y\",\"Z\",\"I\"" <<std::endl;
	output<<"ZONE T=\""<<zone<<"\" I="<<dim3<<" J="<<dim2<<" K="<<dim1<< "  F=POINT"<<std::endl;
	//write data
    T z = init_point[2];
    for(int k = dim1-1; k >= 0;--k,z+=step[2])
    {
      T y = init_point[1];
      for(int i = dim2-1; i >= 0;--i,y+=step[1])
	{
	  T x = init_point[0];
	  for(int j = 0; j < dim3; ++j,x+=step[0])
	    {
	      output<<static_cast<T>(x)<<" "<<static_cast<T>(y)<<" "<<static_cast<T>(z)<<" "<<container[k][i][j]<<"\n";
	    }
	}
    }
    output.precision(default_precision);
	output.close();
      }
     catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/14
  ** \since 1.0.0
  ** \brief Write slices of a Container3D to a tecplot file.
  ** \param container Container3d.
  ** \param file_path_name String of the file path name
  ** \param title Title of the file.
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \par The data format is the following:
  ** \code 
  ** TITLE= title 
  ** VARIABLES= X Y I
  ** ZONE T= image to I= rows() J=  cols() F=POINT
  ** <data of the first slice>
  ** ZONE T= image (to+1) I= rows() J=  cols() F=POINT
  ** <data of the second slice>
  ** ...
  ** \endcode 
  ** \remarks x = j and y = (dim1 - 1) - i
  
  */
  template <typename Container3d>
  void write_to_images_tecplot(Container3d& container,
			       const std::string& file_path_name,
			       const std::string& title,
			       const std::size_t from,
			       const std::size_t to)			
  {
    assert(((to - from) + 1) > 0);
     std::ofstream output(file_path_name.c_str(),std::ios::out);
     try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }

	int dim2 = int(container.dim2());
	int dim3 = int(container.dim3());
	//write file informations
	output<<"TITLE=\"" <<title<<"\""<<std::endl;
	output<<"VARIABLES=\"X\" \"Y\" \"Z\" \"I\"" <<std::endl;
	for(std::size_t img = from; img <= to; ++img)
	  {
	    std::string file_name_img = slip::compose_file_name(file_path_name,img);
	   	    
	    output<<"ZONE T=\"image "<<img<<"\" I="<<dim3<<" J="<<dim2<<"  F=POINT"<<std::endl;
	    //write data
	    std::size_t dim2_1 = dim2 - 1;
	    for(int x = 0; x < dim3; ++x)
		  {
		    for(int y = 0; y < dim2 ; ++y)
		      {
			output<<x<<" "<<y<<" "<<img<<" "<<container[img][dim2_1-y][x]<<"\n";
		      }
		  }
	  }
	output.close();
      }
     catch(std::exception& e)
       {
	 std::cerr<<e.what()<<std::endl;
	 output.close();
	 exit(1);
       }
     
  }
#ifdef HAVE_MAGICK  
  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes slices of a Container3d to image files.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param w Width of the images.
  ** \param h Height of the images.
  ** \param storage_t ImageMagick storage type : CharPixel, IntegerPixel,
  **                   ShortPixel, LongPixel, FloatPixel or DoublePixel 
  ** \param plane_begin A RandomAccessIterator to the container containing
  **                     the first iterators of the slices of the Container3d.
  **                   
  **
  ** \pre (to - from) + 1 > 0
  
  ** \par Example:
  ** \code
  ** slip::Volume<double> Vol;
  ** std::string file_base_name;
  ** std::cout<<"file base name ? (example : toto%3d.png)"<<std::endl;
  ** std::cin>>file_base_name;
  ** std::cout<<"first image number ?"<<std::endl;
  ** std::size_t img_from = 1;
  ** std::cin>>img_from;
  ** std::cout<<"last image number ?"<<std::endl;
  ** std::size_t img_to = 1;
  ** std::cin>>img_to;
  ** slip::read_from_images(file_base_name,img_from,img_to,Vol,DoublePixel);
  ** std::cout<<"dim1 = "<<Vol.dim1()<<" dim2 = "<<Vol.dim2()<<" dim3 = "<<Vol.dim3()<<" size = "<<Vol.size()<<" slice_size = "<<Vol.slice_size()<<" max_size = "<<Vol.max_size()<<" empty = "<<Vol.empty()<<std::endl; 
    
  **  Vol.write_to_images("toto-%4d.bmp",1,img_to-img_from);
  ** \endcode
  */
  template <typename RandomAccessIterator>
  void write_to_images(const std::string& file_path_name,
		       const std::size_t from,
		       const std::size_t to,
		       const std::size_t w,
		       const std::size_t h,
		       const StorageType storage_t,
		       RandomAccessIterator plane_begin)
  {
    assert(((to - from) + 1) > 0);
    for(std::size_t img = from; img <= to; ++img)
      {
	std::string file_name_img = slip::compose_file_name(file_path_name,img);
	slip::write_magick_gray(plane_begin[img-from],w,h,storage_t,file_name_img);
      }
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes slices of a char Container3d to image files.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param w Width of the images.
  ** \param h Height of the images.
  ** \param plane_begin A RandomAccessIterator to the container containing
  **                     the first iterators of the slices of the Container3d.
  **                   
  **
  ** \pre (to - from) + 1 > 0
  
  */
  template <typename RandomAccessIterator>
  void write_to_images_char(const std::string& file_path_name,
			    const std::size_t from,
			    const std::size_t to,
			    const std::size_t w,
			    const std::size_t h,
			    RandomAccessIterator plane_begin)
  {
    slip::write_to_images(file_path_name,from,to,w,h,CharPixel,plane_begin);
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes slices of a short Container3d to image files.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param w Width of the images.
  ** \param h Height of the images.
  ** \param plane_begin A RandomAccessIterator to the container containing
  **                     the first iterators of the slices of the Container3d.
  **                   
  **
  ** \pre (to - from) + 1 > 0
  
  */
  template <typename RandomAccessIterator>
  void write_to_images_short(const std::string& file_path_name,
			     const std::size_t from,
			     const std::size_t to,
			     const std::size_t w,
			     const std::size_t h,
			     RandomAccessIterator plane_begin)
  {
    slip::write_to_images(file_path_name,from,to,w,h,ShortPixel,plane_begin);
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes slices of a int Container3d to image files.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param w Width of the images.
  ** \param h Height of the images.
  ** \param plane_begin A RandomAccessIterator to the container containing
  **                     the first iterators of the slices of the Container3d.
  **                   
  **
  ** \pre (to - from) + 1 > 0
  
  */
  template <typename RandomAccessIterator>
  void write_to_images_int(const std::string& file_path_name,
			   const std::size_t from,
			   const std::size_t to,
			   const std::size_t w,
			   const std::size_t h,
			   RandomAccessIterator plane_begin)
  {
    slip::write_to_images(file_path_name,from,to,w,h,IntegerPixel,plane_begin);
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes slices of a long Container3d to image files.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param w Width of the images.
  ** \param h Height of the images.
  ** \param plane_begin A RandomAccessIterator to the container containing
  **                     the first iterators of the slices of the Container3d.
  **                   
  **
  ** \pre (to - from) + 1 > 0
  
  */
  template <typename RandomAccessIterator>
  void write_to_images_long(const std::string& file_path_name,
			    const std::size_t from,
			    const std::size_t to,
			    const std::size_t w,
			    const std::size_t h,
			    RandomAccessIterator plane_begin)
  {
    slip::write_to_images(file_path_name,from,to,w,h,LongPixel,plane_begin);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes slices of a float Container3d to image files.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param w Width of the images.
  ** \param h Height of the images.
  ** \param plane_begin A RandomAccessIterator to the container containing
  **                     the first iterators of the slices of the Container3d.
  **                   
  **
  ** \pre (to - from) + 1 > 0
  
  */
  template <typename RandomAccessIterator>
  void write_to_images_float(const std::string& file_path_name,
			     const std::size_t from,
			     const std::size_t to,
			     const std::size_t w,
			     const std::size_t h,
			      RandomAccessIterator plane_begin)
  {
    slip::write_to_images(file_path_name,from,to,w,h,FloatPixel,plane_begin);
  }


   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Writes slices of a double Container3d to image files.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param w Width of the images.
  ** \param h Height of the images.
  ** \param plane_begin A RandomAccessIterator to the container containing
  **                     the first iterators of the slices of the Container3d.
  **                   
  **
  ** \pre (to - from) + 1 > 0
  
  */
  template <typename RandomAccessIterator>
  void write_to_images_double(const std::string& file_path_name,
			      const std::size_t from,
			      const std::size_t to,
			      const std::size_t w,
			      const std::size_t h,
			      RandomAccessIterator plane_begin)
  {
    slip::write_to_images(file_path_name,from,to,w,h,DoublePixel,plane_begin);
  }


  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Read slices of a Container3D from image file.
  ** \param container Container3d.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param container A Container3d.
  ** \param storage_t ImageMagick storage type : CharPixel, IntegerPixel,
  **                   ShortPixel, LongPixel, FloatPixel or DoublePixel 
  **                   
  **
  ** \pre (to - from) + 1 > 0
  
    ** \par Example:
  ** \code
  ** slip::Volume<double> Vol;
  ** std::string file_base_name;
  ** std::cout<<"file base name ? (example : toto%3d.png)"<<std::endl;
  ** std::cin>>file_base_name;
  ** std::cout<<"first image number ?"<<std::endl;
  ** std::size_t img_from = 1;
  ** std::cin>>img_from;
  ** std::cout<<"last image number ?"<<std::endl;
  ** std::size_t img_to = 1;
  ** std::cin>>img_to;
  ** slip::read_from_images(file_base_name,img_from,img_to,Vol,DoublePixel);
  ** std::cout<<"dim1 = "<<Vol.dim1()<<" dim2 = "<<Vol.dim2()<<" dim3 = "<<Vol.dim3()<<" size = "<<Vol.size()<<" slice_size = "<<Vol.slice_size()<<" max_size = "<<Vol.max_size()<<" empty = "<<Vol.empty()<<std::endl; 
    
  **  Vol.write_to_images("toto-%4d.bmp",1,img_to-img_from);
  ** \endcode
  */
  template<typename Container3d>
  inline
  void read_from_images(const std::string& file_path_name,
			const std::size_t from,
			const std::size_t to,
			Container3d& container,
			const StorageType storage_t)
  {
    assert(((to - from) + 1) > 0);
    
    std::ifstream input;
      try
      {
	//read first slice
	std::string file_name_img = slip::compose_file_name(file_path_name,from);
	input.open(file_name_img.c_str(),std::ios::in);
	 if (!input) 
	  { 
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	  }
	slip::Matrix<typename Container3d::value_type> Mat;
	read_magick_gray(file_name_img,Mat,storage_t);
	//resize the container
	container.resize((to-from) + 1,Mat.dim1(),Mat.dim2(),0);
	//copy the first slice
	std::copy(Mat.begin(),Mat.end(),container.plane_begin(0));
	input.close();
    
	for(std::size_t img = (from + 1); img <= to; ++img)
	  {
	    file_name_img = slip::compose_file_name(file_path_name,img);
	    input.open(file_name_img.c_str(),std::ios::in);
	    if (!input) 
	      {
		throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_name_img);	
	      }
	    slip::Matrix<typename Container3d::value_type> Mat;
	    read_magick_gray(file_name_img,Mat,storage_t);
	    assert(Mat.dim1()  == container.dim2());
	    assert(Mat.dim2()  == container.dim3());
	    std::copy(Mat.begin(),Mat.end(),container.plane_begin(img-from));
	    input.close();
	  }
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	input.close();
	exit(1);
      }
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Read slices of a char Container3D from image file.
  ** \param container Container3d.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param container A Container3d.
  **
  ** \pre (to - from) + 1 > 0
  
  */
  template<typename Container3d>
  inline
  void read_from_images_char(const std::string& file_path_name,
			     const std::size_t from,
			     const std::size_t to,
			     Container3d& container)
  {
    slip::read_from_images(file_path_name,from,to,container,CharPixel);
  }


   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Read slices of an integer Container3D from image file.
  ** \param container Container3d.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param container A Container3d.
  **
  ** \pre (to - from) + 1 > 0
  
  */
  template<typename Container3d>
  inline
   void read_from_images_int(const std::string& file_path_name,
			     const std::size_t from,
			     const std::size_t to,
			     Container3d& container)
  {
    slip::read_from_images(file_path_name,from,to,container,IntegerPixel);
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \brief Read slices of a short Container3D from image file.
  ** \param container Container3d.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param container A Container3d.
  **
  ** \pre (to - from) + 1 > 0
  
  */
  template<typename Container3d>
  inline
  void read_from_images_short(const std::string& file_path_name,
			      const std::size_t from,
			      const std::size_t to,
			      Container3d& container)
  {
    slip::read_from_images(file_path_name,from,to,container,ShortPixel);
  }

   /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Read slices of a long Container3D from image file.
  ** \param container Container3d.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param container A Container3d.
  **
  ** \pre (to - from) + 1 > 0
  
  */
  template<typename Container3d>
  inline
  void read_from_images_long(const std::string& file_path_name,
			     const std::size_t from,
			     const std::size_t to,
			     Container3d& container)
  {
    slip::read_from_images(file_path_name,from,to,container,LongPixel);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Read slices of a float Container3D from image file.
  ** \param container Container3d.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param container A Container3d.
  **
  ** \pre (to - from) + 1 > 0
  
  */
  template<typename Container3d>
  inline
  void read_from_images_float(const std::string& file_path_name,
			      const std::size_t from,
			      const std::size_t to,
			      Container3d& container)
  {
    slip::read_from_images(file_path_name,from,to,container,FloatPixel);
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/02/28
  ** \since 1.0.0
  ** \brief Read slices of a double Container3D from image file.
  ** \param container Container3d.
  ** \param file_path_name String of the file path name
  ** \param from Index of the first slice.
  ** \param to Index of the last slice.
  ** \param container A Container3d.
  **
  ** \pre (to - from) + 1 > 0
  
  */
  template<typename Container3d>
  inline
  void read_from_images_double(const std::string& file_path_name,
			       const std::size_t from,
			       const std::size_t to,
			       Container3d& container)
  {
    slip::read_from_images(file_path_name,from,to,container,DoublePixel);
  }
 #endif //HAVE_MAGICK
/* @} */

}//::slip

#endif //SLIP_IO_TOOLS_HPP
