/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file dst.hpp
 * \brief Provides some Discrete Sinus Transform algorithms.
 * \since 1.5.0
 * 
 */

#ifndef SLIP_DST_HPP
#define SLIP_DST_HPP

#include <algorithm>
#include <numeric>
#include <cassert>
#include <iterator>
#include "Array.hpp"
#include "Array2d.hpp"
#include "dct.hpp"
#include "arithmetic_op.hpp"

#ifdef HAVE_FFTW
#include <fftw3.h>
#endif



namespace slip
{
  template<typename RandomAccessIterator1,
	   typename RandomAccessIterator2>
  void odd_sign_changing(RandomAccessIterator1 first,
			 RandomAccessIterator1 last,
			 RandomAccessIterator2 out_first)
  {
    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
  
    T power_minus_one = slip::constants<T>::one();
    for(;first != last; ++first, ++out_first)
       {
	 *(out_first) = power_minus_one* *first;
	 power_minus_one = - power_minus_one;
       }
  }
  
  template<typename RandomAccessIterator1,
	   typename RandomAccessIterator2>
  void even_sign_changing(RandomAccessIterator1 first,
			 RandomAccessIterator1 last,
			 RandomAccessIterator2 out_first)
  {
    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
  
    T power_minus_one = -slip::constants<T>::one();
    for(;first != last; ++first, ++out_first)
       {
	 *(out_first) = power_minus_one* *first;
	 power_minus_one = - power_minus_one;
       }
  }


/**
   ** \brief Computes the normalized split radix Discrete Sinus Transform II of a container (from FFTPACK.hpp).
   ** \author Benoit Tremblais <tremblais_AT_univ-poitiers.fr> : conceptor
   ** \date 2024/04/01
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \pre size(b) >= size(a).) 
   ** slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** slip::split_radix_dst(val1,val1+8,A.begin());
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Vector<double> IdstA(8);
   ** slip::split_radix_idst(A.begin(),A.end(),IdstA.begin());
   ** std::cout<<"IdstA = \n"<<IdstA<<std::endl;
   */

  template<class InputIter,class OutputIter>
  inline
  void split_radix_dst(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    typedef typename std::iterator_traits<InputIter>::value_type real;
    size_t length = in_end - in_begin;
    assert(length > 1);

    // data copy
    std::copy(in_begin,in_end,out_begin);

    // DST initialization
    slip::Array<real> r(5*length,0.0);
    slip::Array<size_t> ifac(5*length,(size_t)0);
    slip::sinqi(&length,r.begin(),ifac.begin());
    
    //  DST
    slip::sinqb(&length,out_begin,r.begin(),ifac.begin());
    std::transform(out_begin, out_begin+length, out_begin,std::bind2nd(std::divides<real>(),slip::constants<real>::two()*std::sqrt(real(2*length))));
   
    
    *(out_begin+(length-1)) *= slip::constants<real>::sqrt1_2();
  }


  /**
   ** \brief Computes the normalized split radix Discrete Sinus Transform III (inverse Discrete Sinus Transform) of a container (from FFTPACK.hpp).
   ** \author Benoit Tremblais <tremblais_AT_univ-poitiers.fr> : conceptor
   ** \date 2024/04/01
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \pre size(b) >= size(a).) 
   ** slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** slip::split_radix_dst(val1,val1+8,A.begin());
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Vector<double> IdstA(8);
   ** slip::split_radix_idst(A.begin(),A.end(),IdstA.begin());
   ** std::cout<<"IdstA = \n"<<IdstA<<std::endl;
   */
  template<class InputIter,class OutputIter>
  inline
  void split_radix_idst(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    typedef typename std::iterator_traits<InputIter>::value_type real;
    size_t length = in_end - in_begin;
    assert(length > 1);
    *(in_end-1) *= slip::constants<real>::sqrt2();
    // data copy
    std::copy(in_begin,in_end,out_begin);
    
    //DST initialization
    slip::Array<real> r(8*length+15,0.0);
    slip::Array<size_t> ifac(8*length+15,(size_t)0);
    slip::sinqi(&length,r.begin(),ifac.begin());

    //DST
    slip::sinqf(&length,out_begin,r.begin(),ifac.begin());
    std::transform(out_begin, out_begin+length, out_begin,std::bind2nd(std::divides<real>(),std::sqrt(real(2*length))));
    
  }


   /**
   ** \brief Computes the normalized Discrete Sinus Transform II (forward) of a container (from fftw).
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr> : conceptor
   ** \date 2024/04/01
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   ** \post ouput data are normalized by \f$\frac{1}{\sqrt{2\times N}}\f$
   ** \par Example:
   ** \code
   **  slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** slip::fftw_dst(val1,val1+8,A.begin());
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Vector<double> IdstA(8);
   ** slip::fftw_idst(A.begin(),A.end(),IdstA.begin());
   ** std::cout<<"IdstA = \n"<<IdstA<<std::endl;
   ** \endcode
   */
#ifdef HAVE_FFTW  
  template<class InputIter,class OutputIter>
  inline
  void fftw_dst(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    size_t length = in_end - in_begin;
    assert(length > 1);

    double *in = (double*) fftw_malloc(sizeof(double) * int(length));
    double *out = (double*) fftw_malloc(sizeof(double) * int(length));
    fftw_plan plan;
    fftw_r2r_kind kind = FFTW_RODFT10;//DST-II

    std::copy(in_begin,in_end,in);
    plan  = fftw_plan_r2r_1d(int(length),in,out,kind,FFTW_ESTIMATE);
    fftw_execute(plan);
    std::transform(out,out+length,out_begin,std::bind2nd(std::divides<double>(),std::sqrt(2*length)));
    *(out_begin+(length-1)) *= slip::constants<double>::sqrt1_2(); 
    fftw_destroy_plan(plan);
    fftw_free(in);
    fftw_free(out);
  }

    /**
   ** \brief Computes the normalized Discrete Sinus Transform III (inverse Discrete Sinus II Transform) of a container (from fftw).
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr> : conceptor
   ** \date 2024/04/01
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \pre the HAVE_FFTW macro must be defined (--enable-fftw3 option of configuration) 
   ** \post ouput data are normalized by \f$\frac{1}{\sqrt{2\times N}}\f$
   ** \par Example:
   ** \code
   **  slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** slip::fftw_dst(val1,val1+8,A.begin());
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Vector<double> IdstA(8);
   ** slip::fftw_idst(A.begin(),A.end(),IdstA.begin());
   ** std::cout<<"IdstA = \n"<<IdstA<<std::endl;
   ** \endcode
   */
  template<class InputIter,class OutputIter>
  inline
  void fftw_idst(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
    size_t length = in_end - in_begin;
    assert(length > 1);

    
    
    double *in = (double*) fftw_malloc(sizeof(double) * int(length));
    double *out = (double*) fftw_malloc(sizeof(double) * int(length));
    fftw_plan plan;
    fftw_r2r_kind kind = FFTW_RODFT01;//DST-III

    std::copy(in_begin,in_end,in);
    *(in+(length-1)) *= slip::constants<double>::sqrt2();
    plan  = fftw_plan_r2r_1d(int(length),in,out,kind,FFTW_ESTIMATE);
    fftw_execute(plan);
    std::transform(out,out+length,out_begin,std::bind2nd(std::divides<double>(),std::sqrt(2*length)));

    fftw_destroy_plan(plan);
    fftw_free(in);
    fftw_free(out);
  }
  #endif
  
  /**
   ** \brief Computes the normalized Discrete Sinus Transform II (forward) of a container.  
   ** The fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one else.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr> : conceptor
   ** \date 2024/04/01
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
 
   ** \post ouput data are normalized by \f$\frac{1}{\sqrt{2\times N}}\f$
   ** \par Example:
   ** \code  
   ** slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** slip::dst(val1,val1+8,A.begin());
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Vector<double> IdstA(8);
   ** slip::idst(A.begin(),A.end(),IdstA.begin());
   ** std::cout<<"IdstA = \n"<<IdstA<<std::endl;
   ** \endcode
   */
  template<class InputIter,
	   class OutputIter>
  inline
  void dst(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
#ifdef HAVE_FFTW
    slip::fftw_dst(in_begin,in_end,out_begin);
#else
    slip::split_radix_dst(in_begin,in_end,out_begin);
#endif
  }
  
/**
   ** \brief Computes the normalized Discrete Sinus Transform III (backward DST-II) of a container.
   ** The fft algorithm used is the fftw if enabled 
   ** (--enable-fftw3 option of configuration) or the split radix one else.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr> : conceptor
   ** \date 2024/04/01
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param in_begin A BidirectionalIterator to iterate from the beginning of the input data
   ** \param in_end A BidirectionalIterator to iterate from the end of the input data
   ** \param out_begin A BidirectionalIterator to iterate from the beginning of the output data
   **
   ** \post ouput data are normalized by \f$\frac{1}{\sqrt{2\times N}}\f$
   ** \par Example:
   ** \code  
   ** slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** slip::dst(val1,val1+8,A.begin());
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Vector<double> IdstA(8);
   ** slip::idst(A.begin(),A.end(),IdstA.begin());
   ** std::cout<<"IdstA = \n"<<IdstA<<std::endl;
   ** \endcode
   */
    template<class InputIter,class OutputIter>
  inline
  void idst(InputIter in_begin, InputIter in_end, OutputIter out_begin)
  {
#ifdef HAVE_FFTW
    slip::fftw_idst(in_begin,in_end,out_begin);
#else
    slip::split_radix_idst(in_begin,in_end,out_begin);
#endif
  }

 /**
   ** \brief Computes the 1d DST-II.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param datain 1d container containing the data.
   ** \param dataout 1d container containing the DCT-II.
   ** \pre data type should be double float or long double
   ** \pre datain.size() == dataout.size()
   ** \par Example:
   ** \code
   ** slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** std::cout<<"dst = \n"<<A<<std::endl;
   ** slip::Vector<double> IdstA(8);
   ** slip::idst(A.begin(),A.end(),IdstA.begin());
   ** std::cout<<"IdstA = \n"<<IdstA<<std::endl;
   ** \endcode
   */
template<typename Container1d1,
	 typename Container1d2>
inline
void dst(const Container1d1 &datain, Container1d2 &dataout)
{
  assert(datain.size() == dataout.size());
  slip::dst(datain.begin(),datain.end(), dataout.begin());
}
   /**
   ** \brief Computes the 1d DST-III (inverse DST-II).
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param datain 1d container containing the data.
   ** \param dataout 1d container containing the inverse DST-II.
   ** \pre data type should be double float or long double
   ** \pre datain.size() == dataout.size()
   ** \par Example:
   ** \code
   ** slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** std::cout<<"dst = \n"<<A<<std::endl;
   ** slip::Vector<double> IdstA(8);
   ** slip::idst(A.begin(),A.end(),IdstA.begin());
   ** std::cout<<"IdstA = \n"<<IdstA<<std::endl;
   ** \endcode
   */  
template<typename Container1d1,
	 typename Container1d2>
inline
void idst(const Container1d1 &datain, Container1d2 &dataout)
{
  assert(datain.size() == dataout.size());
  slip::idst(datain.begin(),datain.end(), dataout.begin());
}  
}//::slip   


namespace slip
{

/**
   ** \brief functor to compute DST-I matrix coefficients
   ** \f[ C^I_{N-1}[i][j] = \sqrt{\frac{2}{N}}\left[\sin\left(\frac{\pi (i+1)(j+1)}{N}\right) \right],\, \forall i,j\in \{0,1,\cdots,N-2\}\f]
   ** where
   **  \f[\epsilon_p = \frac{1}{\sqrt{2}},\, \textrm{if}\, p=0\, \textrm{or}\, p=N\,\textrm{and}\, 1\,\textrm{else}\f] 
   ** \f[DST-I]^{-1} = [DST-I]^T = DST-I\f] 
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \par Example:
   **\code
   ** slip::Matrix<double> DSTI_8_norm;
   ** slip::dstI_coeff<double> dst_coeffI_norm(8);
   ** dst_matrix(dst_coeffI_norm,DSTI_8_norm);
   ** std::cout<<"DSTI_8_norm = \n"<<DSTI_8_norm<<std::endl;
   ** \endcode
   */
template<typename Real=double>
struct dstI_coeff
{
  /**
   ** \brief Construct DST-I coefficient functor with N=7.
   */
  dstI_coeff():
    N_(8-1),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_+1))),
    pi_o_N_(slip::constants<Real>::pi()/static_cast<Real>(N_+1))
    
  {}
   /**
   ** \brief Construct DST-I coefficient functor 
   ** \param N. size of the DST-I matrix.
   */
  dstI_coeff(const std::size_t N):
    N_(N-1),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_+1))),
    pi_o_N_(slip::constants<Real>::pi()/static_cast<Real>(N_+1))
  {}

  /**
   ** \brief Computes the DST-I coefficient at indices (i,j).
   ** \param i row index of the matrix.
   ** \param j column index of the matrix. 
   ** \return a real value
   */
  Real operator()(const std::size_t i,
		  const std::size_t j) const
  {
    const Real xij = std::sin(pi_o_N_*(Real((j+1)*(i+1))));
    Real result = coeff_*xij;
 

    return result;
  }
  std::size_t N_;
  Real coeff_;
  Real pi_o_N_;
};


  /**
   ** \brief functor to compute DST-II matrix coefficients
   ** \f[ C^{II}_{N}[i][j] = \sqrt{\frac{2}{N}}\left[\epsilon_i\sin\left(\frac{\pi (i+1)(2j+1)}{2N}\right) \right],\, \forall i,j\in \{0,1,\cdots,N-1\}\f]
   ** where
   **  \f[\epsilon_p = \frac{1}{\sqrt{2}},\, \textrm{if}\, p=0\,\textrm{and}\, 1\,\textrm{else}\f] 
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
    ** \par Example:
   ** \code
   ** slip::Matrix<double> DSTII_8_norm;
   ** slip::dstII_coeff<double> dst_coeffII(8,true);
   ** dst_matrix(dst_coeffII,DSTII_8_norm);
   ** std::cout<<"DSTII_8_norm = \n"<<DSTII_8_norm<<std::endl;
   ** \endcode
   */  
template<typename Real=double>
struct dstII_coeff
{
  /**
   ** \brief Construct DST-II coefficient functor with N=8 and normalized = true.
   */
  dstII_coeff():
    N_(8),
    normalized_(true),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_))),
    pi_o_2N_(slip::constants<Real>::pi()/(slip::constants<Real>::two()*static_cast<Real>(N_)))
    
  {}
  
   /**
   ** \brief Construct DST-II coefficient functor 
   ** \param N. size of the DST-II matrix.
   ** \param normalized. If true, the matrix coefficients are normalized.
   */
  dstII_coeff(const std::size_t N, bool normalized):
    N_(N),
    normalized_(normalized),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_))),
    pi_o_2N_(slip::constants<Real>::pi()/(slip::constants<Real>::two()*static_cast<Real>(N_)))
  {}

  /**
   ** \brief Computes the DST-II coefficient at indices (i,j).
   ** \param i row index of the matrix.
   ** \param j column index of the matrix. 
   ** \return a real value
   */
  Real operator()(const std::size_t i,
		  const std::size_t j) const
  {
    const Real xij = std::sin(pi_o_2N_*(Real((2*j+1)*(i+1))));
    Real result = coeff_*xij;
    if(normalized_)
      {
	if(i==N_-1)
	  {
	    result *= slip::constants<Real>::sqrt1_2();
	  }
      }

    return result;
  }
  std::size_t N_;
  bool normalized_;
  Real coeff_;
  Real pi_o_2N_;
};

 /**
   ** \brief functor to compute DST-III matrix coefficients
   ** \f[ C^{III}_{N}[i][j] = \sqrt{\frac{2}{N}}\left[\epsilon_j\sin\left(\frac{\pi (2i+1)(j+1)}{2N}\right) \right],\, \forall i,j\in \{0,1,\cdots,N-1\}\f]
   ** where
   **  \f[\epsilon_p = \frac{1}{\sqrt{2}},\, \textrm{if}\, p=0\,\textrm{and}\, 1\,\textrm{else}\f] 
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \par Example:
   ** \code
   ** slip::Matrix<double> DSTIII_8_norm;
   ** slip::dstIII_coeff<double> dst_coeffIII(8,true);
   ** dst_matrix(dst_coeffIII,DSTIII_8_norm);
   ** std::cout<<"DSTIII_8_norm = \n"<<DSTIII_8_norm<<std::endl;
   ** \endcode
   */ 
template<typename Real=double>
struct dstIII_coeff
{
  /**
   ** \brief Construct DST-III coefficient functor with N=8 and normalized = true.
   */
  dstIII_coeff():
    N_(8),
    normalized_(true),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_))),
    pi_o_2N_(slip::constants<Real>::pi()/(slip::constants<Real>::two()*static_cast<Real>(N_)))
    
  {}
  
  /**
   ** \brief Construct DST-III coefficient functor 
   ** \param N. size of the DCT-III matrix.
   ** \param normalized. If true, the matrix coefficients are normalized.
   */
  dstIII_coeff(const std::size_t N, bool normalized):
    N_(N),
    normalized_(normalized),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_))),
    pi_o_2N_(slip::constants<Real>::pi()/(slip::constants<Real>::two()*static_cast<Real>(N_)))
  {}

  /**
   ** \brief Computes the DST-III coefficient at indices (i,j).
   ** \param i row index of the matrix.
   ** \param j column index of the matrix.
   ** \return a real value
   */
  Real operator()(const std::size_t i,
		  const std::size_t j) const
  {
    const Real xij = std::sin(pi_o_2N_*Real((2*i+1)*(j+1)));
    Real result = coeff_*xij;
    if(normalized_)
      {
	if(j==N_-1)
	  {
	    result *= slip::constants<Real>::sqrt1_2();
	  }
      }

    return result;
  }
  std::size_t N_;
  bool normalized_;
  Real coeff_;
  Real pi_o_2N_;
};

 /**
   ** \brief functor to compute DST-IV matrix coefficients
   ** \f[ C^{IV}_{N}[i][j] = \sqrt{\frac{2}{N}}\left[\sin\left(\frac{\pi (2i+1)(2j+1)}{4N}\right) \right],\, \forall i,j\in \{0,1,\cdots,N-1\}\f].
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \par Example:
   **\code
   ** slip::Matrix<double> DSTIV_8;
   ** slip::dstIV_coeff<double> dst_coeffIV(8);
   ** dst_matrix(dst_coeffIV,DSTIV_8);
   ** std::cout<<"DSTIV_8 = \n"<<DSTIV_8<<std::endl;
   ** \endcode
   */  
template<typename Real=double>
struct dstIV_coeff
{
  /**
   ** \brief Construct DST-IV coefficient functor with N=8.
   */
  dstIV_coeff():
    N_(8),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_))),
    pi_o_4N_(slip::constants<Real>::pi()/(static_cast<Real>(4.0)*static_cast<Real>(N_)))
    
  {}
  
   /**
   ** \brief Construct DST-IV coefficient functor 
   ** \param N. size of the DCT-IV matrix.
   */
  dstIV_coeff(const std::size_t N):
    N_(N),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_))),
    pi_o_4N_(slip::constants<Real>::pi()/(static_cast<Real>(4.0)*static_cast<Real>(N_)))
  {}
  /**
   ** \brief Computes the DST-IV coefficient at indices (i,j).
   ** \param i row index of the matrix.
   ** \param j column index of the matrix.
   ** \return a real value
   */
  
  Real operator()(const std::size_t i,
		  const std::size_t j) const
  {
    const Real xij = std::sin(pi_o_4N_*Real((2*i+1)*(2*j+1)));

    return coeff_*xij;
  }
  std::size_t N_;
  //bool normalized_;
  Real coeff_;
  Real pi_o_4N_;
};

 /**
   ** \brief Computes a DST matrix according to a given DST coefficient functor.
   ** \param func a DST coefficient functor (slip::dstI_coeff, slip::dstII_coeff, slip::dstIII_coeff, slip::dstIV_coeff).
   ** The size of the matrix is computed according to the size N provided to the DST functor.
   ** \param DST. The DST matrix.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> DSTII_8_norm;
   ** slip::dctII_coeff<double> dct_coeffII(8,true);
   ** dct_matrix(dct_coeffII,DSTII_8_norm);
   ** std::cout<<"DSTII_8_norm = \n"<<DSTII_8_norm<<std::endl;
   ** \endcode
   */
template<typename Matrix1,
	 class DSTFunctor>
void dst_matrix(const DSTFunctor& func,
		Matrix1& DST)
{
  const std::size_t N = func.N_;
  DST.resize(N,N);
  for(std::size_t i = 0; i < N; ++i)
    {
      for(std::size_t j = 0; j < N; ++j)
	{
	  DST[i][j] = func(i,j);
	}
    }
}

  
 /**
   ** \brief Computes the DST-II of data size 8.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param first RandomAccessIterator to the first element of the data
   ** \param  last RandomAccessIterator to one past-the-end element of the 
   ** data.
   ** \param dst_first RandomAccessIterator to the first element of the
   ** dst.
   ** \par Complexity: 18 multiplications, 20 additions
   ** \par Description: 
   ** Computes the Discrete Sinus Transform (DST)-II of data size 8 using the algorihtm of:
   ** W. Chen, C.H. Smith, and S.C. Fralick, "A fast computational
   ** algorithm for the discrete cosine transform ", IEEE Trans. Commun., Vol.
   ** COM-25, p. 1004-1009, Sep. 1977.
   ** \pre data type should be double float or long double
   ** \pre (last - first == 8)
   ** \pre [dst_first,dst_first + (last-first) must be valid
   ** \todo optimize eliminating some operations it is possible to have only 16 multiplications
   ** \par Example:
   ** \code
   ** slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** 
   ** slip::dst_8(val1,val1+8,A.begin());
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Vector<double> IdstA(8);
   ** slip::idst_8(A.begin(),A.end(),IdstA.begin());
   ** std::cout<<"IdstA = \n"<<IdstA<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator1,
	   typename RandomAccessIterator2>
  void dst_8(RandomAccessIterator1 first,
	     RandomAccessIterator1 last,
	     RandomAccessIterator2 dst_first)
   {
     assert((last -first) == 8);
     typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
     slip::odd_sign_changing(first,last,dst_first);
     slip::dct_8(std::reverse_iterator<RandomAccessIterator1>(dst_first+8),
      		     std::reverse_iterator<RandomAccessIterator1>(dst_first),
      		     std::reverse_iterator<RandomAccessIterator1>(dst_first+8));
     slip::even_sign_changing(dst_first,dst_first+8,dst_first);

   }


/**
   ** \brief Computes the DST-III (Inverse DST) of data size 8.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param first RandomAccessIterator to the first element of the data
   ** \param  last RandomAccessIterator to one past-the-end element of the 
   ** data.
   ** \param idst_first RandomAccessIterator to the first element of the
   ** idst.
   ** \pre data type should be double float or long double
   ** \pre (last - first == 8)
   ** \pre [dst_first,dst_first + (last-first) must be valid
   ** \par Complexity: 20 multiplications, 22 additions
   ** \par Description: 
   ** Computes the Discrete Cosinus Transform (DST)-III (Inverse DST-II) of data size 8 using
   ** the algorithm of:
   ** W. Chen, C.H. Smith, and S.C. Fralick, "A fast computational
   ** algorithm for the discrete cosine transform ", IEEE Trans. Commun., Vol.
   ** COM-25, p. 1004-1009, Sep. 1977.
   ** \todo Optimize: it is possible to have only 12 multiplications
   ** \par Example:
   ** \code
   ** slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** 
   ** slip::dst_8(val1,val1+8,A.begin());
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Vector<double> IdstA(8);
   ** slip::idst_8(A.begin(),A.end(),IdstA.begin());
   ** std::cout<<"IdstA = \n"<<IdstA<<std::endl;
   ** \endcode
   */
template<typename RandomAccessIterator1,
	 typename RandomAccessIterator2>
  void idst_8(RandomAccessIterator1 first,
	      RandomAccessIterator1 last,
	      
	      RandomAccessIterator2 idst_first)
   {
     assert((last -first) == 8);
     typedef typename std::iterator_traits<RandomAccessIterator1>::value_type Real;
     
     slip::idct_8(std::reverse_iterator<RandomAccessIterator1>(last),
		      std::reverse_iterator<RandomAccessIterator1>(first),
       		      idst_first);
     slip::odd_sign_changing(idst_first,idst_first+8,idst_first);

   }

  
/**
   ** \brief Computes the DST-II 2d of data size 8x8.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left RandomAccessIterator2d to the first element of the data
   ** \param bottom_right RandomAccessIterator2d to one past-the-end element of the 
   ** data.
   ** \param dst_upper_left RandomAccessIterator2d to the first element of the
   ** dst.
   ** \par Complexity: 16 * Complexity(idst_8)
   ** \pre (bottom_right - upper_left)[0] == 8
   ** \pre (bottom_right - upper_left)[1] == 8
   ** \pre [dst_upper_left, dst_upper_left + (bottom_right -
   ** upper_left)) must be valid
   ** \pre data type should be double float or long double
   ** \par Example:
   ** \code
   ** double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
   **		      63, 59, 66, 90, 109, 85, 69, 72,
   **		      62, 59, 68, 113, 144, 104, 66, 73,
   **		      63, 58, 71, 122, 154, 106, 70, 69,
   **		      67, 61, 68, 104, 126,  88, 68, 70,
   **		      79, 65, 60,  70,  77,  68, 58, 75,
   **		      85, 71, 64,  59,  55,  61, 65, 83,
   **		      87, 79, 69,  68,  65,  76, 78, 94};
   ** slip::GrayscaleImage<double> I(8,8,val);
   ** std::cout<<"I = \n"<<I<<std::endl;
   **
   ** I-=128;
   ** std::cout<<"I-128 = \n"<<I<<std::endl;
   **
   ** slip::GrayscaleImage<double> DST(I.rows(),I.cols());
   ** slip::dst_8x8(I.upper_left(),I.bottom_right(),DST.upper_left());
   ** std::cout<<"DST(I) = \n"<<DST<<std::endl;
   ** slip::GrayscaleImage<double> IDST(I.rows(),I.cols());
   ** slip::idst_8x8(DST.upper_left(),DST.bottom_right(),
   ** 		     IDST.upper_left());
   ** std::cout<<"IDST(DST(I)) = \n"<<IDST<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator2d1,
	   typename RandomAccessIterator2d2>
  void dst_8x8(RandomAccessIterator2d1 upper_left,
	       RandomAccessIterator2d1 bottom_right,
	       RandomAccessIterator2d2 dst_upper_left)
  {
    assert((bottom_right - upper_left)[0] == 8);
    assert((bottom_right - upper_left)[1] == 8);
    typename RandomAccessIterator2d1::difference_type size2d = bottom_right - upper_left;
    typedef typename std::iterator_traits<RandomAccessIterator2d1>::value_type Real;
    std::size_t rows = size2d[0];
    std::size_t cols = size2d[1];
    slip::Array2d<Real> tmp(rows,cols,Real(0));
    for(size_t i = 0; i < rows; ++i)
      {
	slip::dst_8(upper_left.row_begin(i),upper_left.row_end(i),
		    tmp.row_begin(i));
      } 
    for(size_t j = 0; j < cols; ++j)
      {
	slip::dst_8(tmp.col_begin(j),tmp.col_end(j),
		    dst_upper_left.col_begin(j));
      } 
  }

/**
   ** \brief Computes the DST-III 2d (Inverse DST-II) of data size 8x8.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left RandomAccessIterator2d to the first element of the data
   ** \param bottom_right RandomAccessIterator2d to one past-the-end element of the 
   ** data.
   ** \param idst_upper_left RandomAccessIterator2d to the first element of the
   ** dst.
  
   ** \par Complexity: 16 * Complexity(idst_8)
   ** \pre (bottom_right - upper_left)[0] == 8
   ** \pre (bottom_right - upper_left)[1] == 8
   ** \pre [idst_upper_left, idst_upper_left + (bottom_right -
   ** upper_left)) must be valid
   ** \pre data type should be double float or long double
   ** \par Example:
   ** \code
   ** double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
   **		      63, 59, 66, 90, 109, 85, 69, 72,
   **		      62, 59, 68, 113, 144, 104, 66, 73,
   **		      63, 58, 71, 122, 154, 106, 70, 69,
   **		      67, 61, 68, 104, 126,  88, 68, 70,
   **		      79, 65, 60,  70,  77,  68, 58, 75,
   **		      85, 71, 64,  59,  55,  61, 65, 83,
   **		      87, 79, 69,  68,  65,  76, 78, 94};
   ** slip::GrayscaleImage<double> I(8,8,val);
   ** std::cout<<"I = \n"<<I<<std::endl;
   **
   ** I-=128;
   ** std::cout<<"I-128 = \n"<<I<<std::endl;
   **
   ** slip::GrayscaleImage<double> DST(I.rows(),I.cols());
   ** slip::dst_8x8(I.upper_left(),I.bottom_right(),DST.upper_left());
   ** std::cout<<"DST(I) = \n"<<DST<<std::endl;
   ** slip::GrayscaleImage<double> IDST(I.rows(),I.cols());
   ** slip::idst_8x8(DST.upper_left(),DST.bottom_right(),
   ** 		     IDST.upper_left());
   ** std::cout<<"IDST(DST(I)) = \n"<<IDST<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator2d1,
	   typename RandomAccessIterator2d2>
  void idst_8x8(RandomAccessIterator2d1 upper_left,
		RandomAccessIterator2d1 bottom_right,
		
		RandomAccessIterator2d2 idst_upper_left)
  {
     assert((bottom_right - upper_left)[0] == 8);
     assert((bottom_right - upper_left)[1] == 8);
    typename RandomAccessIterator2d1::difference_type size2d = bottom_right - upper_left;
    typedef typename std::iterator_traits<RandomAccessIterator2d1>::value_type Real;
    std::size_t rows = size2d[0];
    std::size_t cols = size2d[1];
    slip::Array2d<Real> tmp(rows,cols,Real(0));
    for(size_t i = 0; i < rows; ++i)
      {
	slip::idst_8(upper_left.row_begin(i),upper_left.row_end(i),
		     tmp.row_begin(i));
      } 
    for(size_t j = 0; j < cols; ++j)
      {
	slip::idst_8(tmp.col_begin(j),tmp.col_end(j),
		     idst_upper_left.col_begin(j));
      } 
  }


   /**
   ** \brief Computes the DST-II 2d with 2 1d DST.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left RandomAccessIterator2d to the first element of the data
   ** \param bottom_right RandomAccessIterator2d to one past-the-end element of the 
   ** data.
   ** \param dst_upper_left RandomAccessIterator2d to the first element of the
   ** dst.
   ** \pre data type should be double float or long double
   ** \par Example:
   ** \code
   ** double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
   **		  63, 59, 66, 90, 109, 85, 69, 72,
   **		  62, 59, 68, 113, 144, 104, 66, 73,
   **		  63, 58, 71, 122, 154, 106, 70, 69,
   **		  67, 61, 68, 104, 126,  88, 68, 70,
   **		  79, 65, 60,  70,  77,  68, 58, 75,
   **		  85, 71, 64,  59,  55,  61, 65, 83,
   **		  87, 79, 69,  68,  65,  76, 78, 94};
   ** slip::GrayscaleImage<double> I(8,8,val);
   ** std::cout<<"I = \n"<<I<<std::endl;
   **
   ** I-=128;
   ** std::cout<<"I-128 = \n"<<I<<std::endl;
   ** slip::GrayscaleImage<double> DST(I.rows(),I.cols());
   ** slip::dst2d(I.upper_left(),I.bottom_right(),DST.upper_left());
   ** std::cout<<"DST(I) = \n"<<DST<<std::endl;
   ** slip::GrayscaleImage<double> IDST(I.rows(),I.cols());
   ** slip::idst2d(DST.upper_left(),DST.bottom_right(),
   **		   IDST.upper_left());
   ** std::cout<<"IDST(DST(I)) = \n"<<IDST<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator2d1,
	   typename RandomAccessIterator2d2>
  void dst2d(RandomAccessIterator2d1 upper_left,
	     RandomAccessIterator2d1 bottom_right,
	     
	     RandomAccessIterator2d2 dst_upper_left)
  {
     typename RandomAccessIterator2d1::difference_type size2d = bottom_right - upper_left;
     typedef typename std::iterator_traits<RandomAccessIterator2d1>::value_type Real;
    const std::size_t rows = size2d[0];
    const std::size_t cols = size2d[1];
 
    std::fill_n(dst_upper_left,rows*cols,Real(0));    

    for(size_t i = 0; i < rows; ++i)
      {
	slip::dst(upper_left.row_begin(i),upper_left.row_end(i),
		  dst_upper_left.row_begin(i));
	//normalization
	*(dst_upper_left.row_begin(i)) /= slip::constants<Real>::sqrt2();
      } 


    slip::Array<Real> tmp_col(rows);
    slip::Array<Real> tmp(rows);
    for(size_t j = 0; j < cols; ++j)
      {
	//copy 
	std::copy(dst_upper_left.col_begin(j),dst_upper_left.col_end(j),
		  tmp_col.begin());
	slip::dst(tmp_col.begin(),tmp_col.end(),
	 	  tmp.begin());
	tmp[0] /= slip::constants<Real>::sqrt2();
	std::copy(tmp.begin(),tmp.end(),dst_upper_left.col_begin(j));

      } 
   
  }

   /**
   ** \brief Computes the normalized DST-III 2d (inverse DST-II 2d) with 2 1d DST.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left RandomAccessIterator2d to the first element of the data
   ** \param bottom_right RandomAccessIterator2d to one past-the-end element of the 
   ** data.
   ** \param dst_upper_left RandomAccessIterator2d to the first element of the
   ** dst.
   ** \pre data type should be double float or long double
   ** \par Example:
   ** \code
   ** double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
   **		  63, 59, 66, 90, 109, 85, 69, 72,
   **		  62, 59, 68, 113, 144, 104, 66, 73,
   **		  63, 58, 71, 122, 154, 106, 70, 69,
   **		  67, 61, 68, 104, 126,  88, 68, 70,
   **		  79, 65, 60,  70,  77,  68, 58, 75,
   **		  85, 71, 64,  59,  55,  61, 65, 83,
   **		  87, 79, 69,  68,  65,  76, 78, 94};
   ** slip::GrayscaleImage<double> I(8,8,val);
   ** std::cout<<"I = \n"<<I<<std::endl;
   **
   ** I-=128;
   ** std::cout<<"I-128 = \n"<<I<<std::endl;
   ** slip::GrayscaleImage<double> DST(I.rows(),I.cols());
   ** slip::dst2d(I.upper_left(),I.bottom_right(),DST.upper_left());
   ** std::cout<<"DST(I) = \n"<<DST<<std::endl;
   ** slip::GrayscaleImage<double> IDST(I.rows(),I.cols());
   ** slip::idst2d(DST.upper_left(),DST.bottom_right(),
   **		   IDST.upper_left());
   ** std::cout<<"IDST(DST(I)) = \n"<<IDST<<std::endl;
   ** \endcode
   */
template<typename RandomAccessIterator2d1,
	 typename RandomAccessIterator2d2>
void idst2d(RandomAccessIterator2d1 upper_left,
	    RandomAccessIterator2d1 bottom_right,
	    
	    RandomAccessIterator2d2 idst_upper_left)
{
  typename RandomAccessIterator2d1::difference_type size2d = bottom_right - upper_left;
  typedef typename std::iterator_traits<RandomAccessIterator2d1>::value_type Real;
  const std::size_t rows = size2d[0];
  const std::size_t cols = size2d[1];
    
  std::fill_n(idst_upper_left,rows*cols,Real(0));    

  slip::Array<Real> row_tmp(cols);
  for(size_t i = 0; i < rows; ++i)
    {
      //copy 
      std::copy(upper_left.row_begin(i),upper_left.row_end(i),row_tmp.begin());
      row_tmp[0] *=  slip::constants<Real>::sqrt2();
      slip::idst(row_tmp.begin(),row_tmp.end(),idst_upper_left.row_begin(i));
    } 

  slip::Array<Real> col_tmp(rows);
  slip::Array<Real> tmp(rows);
  for(size_t j = 0; j < cols; ++j)
      {
	//copy 
	std::copy(idst_upper_left.col_begin(j),idst_upper_left.col_end(j),
		  col_tmp.begin());
	col_tmp[0] *= slip::constants<Real>::sqrt2();
	//  iDST
	slip::idst(col_tmp.begin(),col_tmp.end(),
		   tmp.begin());
	
	std::copy(tmp.begin(),tmp.end(),idst_upper_left.col_begin(j));
	
      } 
     
   
  }

 /**
 ** \brief Computes the normalized DST-II with 2 1d DST-II.
 ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
 ** \date 2024/04/01
 ** \version 0.0.1
 ** \since 1.5.0
 ** \param datain Container2d containing the data.
 ** \param dataout Container2d containing the 2d DST-II.
 ** \pre data type of the Containers should be double float or long double.
 ** \par Example:
 ** \code
 ** double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
 **		    63, 59, 66, 90, 109, 85, 69, 72,
 **		    62, 59, 68, 113, 144, 104, 66, 73,
 **		    63, 58, 71, 122, 154, 106, 70, 69,
 **		    67, 61, 68, 104, 126,  88, 68, 70,
 **		    79, 65, 60,  70,  77,  68, 58, 75,
 **		    85, 71, 64,  59,  55,  61, 65, 83,
 **		    87, 79, 69,  68,  65,  76, 78, 94};
 ** slip::GrayscaleImage<double> I(8,8,val);
 ** std::cout<<"I = \n"<<I<<std::endl;
 ** 
 ** I-=128;
 ** std::cout<<"I-128 = \n"<<I<<std::endl;
 ** slip::GrayscaleImage<double> DST(I.rows(),I.cols());
 ** slip::dst2d(I,DST);
 ** std::cout<<"DST(I) = \n"<<DST<<std::endl;
 ** slip::GrayscaleImage<double> IDST(I.rows(),I.cols());
 ** slip::idst2d(DST,IDST);
 ** std::cout<<"IDST(DST) = \n"<<IDST<<std::endl;
 ** \endcode
 */
  template<typename Matrix1,
	   typename Matrix2>
  inline
  void dst2d(const Matrix1 &datain, Matrix2 &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    slip::dst2d(datain.upper_left(),datain.bottom_right(),
		dataout.upper_left());
  }
  
  /**
 ** \brief Computes the normalized DST-III (inverse DST-II) with 2 1d DST-III.
 ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
 ** \date 2024/04/01
 ** \version 0.0.1
 ** \since 1.5.0
 ** \param datain Container2d containing the data.
 ** \param dataout Container2d containing the 2d DST-III.
 ** \pre data type of the Containers should be double float or long double.
 ** \par Example:
 ** \code
 ** double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
 **		    63, 59, 66, 90, 109, 85, 69, 72,
 **		    62, 59, 68, 113, 144, 104, 66, 73,
 **		    63, 58, 71, 122, 154, 106, 70, 69,
 **		    67, 61, 68, 104, 126,  88, 68, 70,
 **		    79, 65, 60,  70,  77,  68, 58, 75,
 **		    85, 71, 64,  59,  55,  61, 65, 83,
 **		    87, 79, 69,  68,  65,  76, 78, 94};
 ** slip::GrayscaleImage<double> I(8,8,val);
 ** std::cout<<"I = \n"<<I<<std::endl;
 ** 
 ** I-=128;
 ** std::cout<<"I-128 = \n"<<I<<std::endl;
 ** slip::GrayscaleImage<double> DST(I.rows(),I.cols());
 ** slip::dst2d(I,DST);
 ** std::cout<<"DST(I) = \n"<<DST<<std::endl;
 ** slip::GrayscaleImage<double> IDST(I.rows(),I.cols());
 ** slip::idst2d(DST,IDST);
 ** std::cout<<"IDST(DST) = \n"<<IDST<<std::endl;
 ** \endcode
 */
   template<typename Matrix1,
	   typename Matrix2>
  inline
  void idst2d(const Matrix1 &datain, Matrix2 &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    slip::idst2d(datain.upper_left(),datain.bottom_right(),
		 dataout.upper_left());
  }


  /**
   ** \brief Computes the normalized DST-II 3d with 3 1d DST.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left RandomAccessIterator3d to the first element of the data
   ** \param bottom_right RandomAccessIterator3d to one past-the-end element of the 
   ** data.
   ** \param dct_upper_left RandomAccessIterator3d to the first element of the
   ** dst.
   ** \pre data type should be double float or long double
   ** \par Example:
   ** \code
   ** slip::Volume<double> Vol(8,8,8);
   ** slip::iota(Vol.begin(),Vol.end(),0.0,1.0);
   **
   ** slip::Volume<double> DSTVol(8,8,8);
   ** slip::dst3d(Vol.front_upper_left(),
   **	          Vol.back_bottom_right(),
   **	          DSTVol.front_upper_left());
   ** std::cout<<"DSTVol = \n"<<DSTVol<<std::endl;
   ** slip::Volume<double> IDSTVol(8,8,8);
   ** slip::idst3d(DSTVol.front_upper_left(),
   **		   DSTVol.back_bottom_right(),
   **		   IDSTVol.front_upper_left());
   ** std::cout<<"IDSTVol = \n"<<IDSTVol<<std::endl;
   ** \endcode
   */
 template<typename RandomAccessIterator3d1,
	   typename RandomAccessIterator3d2>
  void dst3d(RandomAccessIterator3d1 front_upper_left,
	     RandomAccessIterator3d1 back_bottom_right,
	     
	     RandomAccessIterator3d2 dst_front_upper_left)
  {
     typename RandomAccessIterator3d1::difference_type size3d = back_bottom_right - front_upper_left;
     typedef typename std::iterator_traits<RandomAccessIterator3d1>::value_type Real;
     const std::size_t slices = size3d[0];
     const std::size_t rows   = size3d[1];
     const std::size_t cols   = size3d[2];
     const std::size_t S = slices * rows * cols;
     std::fill_n(dst_front_upper_left,S,Real(0));    
     for(size_t k = 0; k < slices; ++k)
       {
	 for(size_t i = 0; i < rows; ++i)
	   {
	     slip::dst(front_upper_left.row_begin(k,i),
		       front_upper_left.row_end(k,i),
		       dst_front_upper_left.row_begin(k,i));
	     //normalization
	     *(dst_front_upper_left.row_begin(k,i)) /= slip::constants<Real>::sqrt2();
	   } 
       }


    slip::Array<Real> tmp_col(rows);
    slip::Array<Real> tmp(rows);
    for(size_t k = 0; k < slices; ++k)
      {
    	for(size_t j = 0; j < cols; ++j)
    	  {
    	    //copy 
    	    std::copy(dst_front_upper_left.col_begin(k,j),
    		      dst_front_upper_left.col_end(k,j),
    		      tmp_col.begin());
    	    slip::dst(tmp_col.begin(),tmp_col.end(),
    		      tmp.begin());
    	    tmp[0] /= slip::constants<Real>::sqrt2();
    	    std::copy(tmp.begin(),tmp.end(),dst_front_upper_left.col_begin(k,j));
    	  }
      } 

    slip::Array<Real> tmp_slice(slices);
    slip::Array<Real> tmp2(slices);
    for(size_t i= 0; i < rows; ++i)
      {
    	for(size_t j = 0; j < cols; ++j)
    	  {
    	    //copy 
    	    std::copy(dst_front_upper_left.slice_begin(i,j),
    		      dst_front_upper_left.slice_end(i,j),
    		      tmp_slice.begin());
    	    slip::dst(tmp_slice.begin(),tmp_slice.end(),
    		      tmp2.begin());
    	    tmp2[0] /= slip::constants<Real>::sqrt2();
    	    std::copy(tmp2.begin(),tmp2.end(),
    		      dst_front_upper_left.slice_begin(i,j));
    	  }
      } 
   
  }

/**
   ** \brief Computes the normalized DST-III 3d (inverse DST-II 3d) with 3 1d DST.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left RandomAccessIterator3d to the first element of the data
   ** \param bottom_right RandomAccessIterator3d to one past-the-end element of the 
   ** data.
   ** \param dct_upper_left RandomAccessIterator3d to the first element of the
   ** dst.
   ** \pre data type should be double float or long double
   ** \par Example:
   ** \code
   ** slip::Volume<double> Vol(8,8,8);
   ** slip::iota(Vol.begin(),Vol.end(),0.0,1.0);
   **
   ** slip::Volume<double> DSTVol(8,8,8);
   ** slip::dst3d(Vol.front_upper_left(),
   **	          Vol.back_bottom_right(),
   **	          DSTVol.front_upper_left());
   ** std::cout<<"DSTVol = \n"<<DSTVol<<std::endl;
   ** slip::Volume<double> IDSTVol(8,8,8);
   ** slip::idst3d(DSTVol.front_upper_left(),
   **		   DSTVol.back_bottom_right(),
   **		   IDSTVol.front_upper_left());
   ** std::cout<<"IDSTVol = \n"<<IDSTVol<<std::endl;
   ** \endcode
   */
template<typename RandomAccessIterator3d1,
	 typename RandomAccessIterator3d2>
void idst3d(RandomAccessIterator3d1 front_upper_left,
	    RandomAccessIterator3d1 back_bottom_right,
	    
	    RandomAccessIterator3d2 idst_front_upper_left)
{
  typename RandomAccessIterator3d1::difference_type size3d = back_bottom_right - front_upper_left;
  typedef typename std::iterator_traits<RandomAccessIterator3d1>::value_type Real;

  const std::size_t slices = size3d[0];
  const std::size_t rows   = size3d[1];
  const std::size_t cols   = size3d[2];
  const std::size_t S = slices * rows * cols;
  
    
  std::fill_n(idst_front_upper_left,S,Real(0));    

  slip::Array<Real> row_tmp(cols);
  for(size_t k = 0; k < slices; ++k)
    {
      for(size_t i = 0; i < rows; ++i)
	{
	  //copy 
	  std::copy(front_upper_left.row_begin(k,i),
		    front_upper_left.row_end(k,i),
		    row_tmp.begin());
	  row_tmp[0] *=  slip::constants<Real>::sqrt2();
	  slip::idst(row_tmp.begin(),row_tmp.end(),
		     idst_front_upper_left.row_begin(k,i));
	} 
    }

  slip::Array<Real> col_tmp(rows);
  slip::Array<Real> tmp(rows);
  for(size_t k = 0; k < slices; ++k)
    {
      for(size_t j = 0; j < cols; ++j)
  	{
  	  //copy 
  	  std::copy(idst_front_upper_left.col_begin(k,j),
  		    idst_front_upper_left.col_end(k,j),
  		    col_tmp.begin());
  	  col_tmp[0] *= slip::constants<Real>::sqrt2();
  	  //  iDST
  	  slip::idst(col_tmp.begin(),col_tmp.end(),
  		     tmp.begin());
	
  	  std::copy(tmp.begin(),tmp.end(),
  		    idst_front_upper_left.col_begin(k,j));
	  
  	} 
    }
     
  slip::Array<Real> slice_tmp(slices);
  slip::Array<Real> tmp2(slices);
  for(size_t i = 0; i < rows; ++i)
    {
      for(size_t j = 0; j < cols; ++j)
  	{
  	  //copy 
  	  std::copy(idst_front_upper_left.slice_begin(i,j),
  		    idst_front_upper_left.slice_end(i,j),
  		    slice_tmp.begin());
  	  slice_tmp[0] *= slip::constants<Real>::sqrt2();
  	  //  iDST
  	  slip::idst(slice_tmp.begin(),slice_tmp.end(),
  		     tmp2.begin());
	
  	  std::copy(tmp2.begin(),tmp2.end(),
  		    idst_front_upper_left.slice_begin(i,j));
	  
  	} 
    }
  }

/**
 ** \brief Computes the normalized 3d DST-II with 3 1d DST.
 ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
 ** \date 2024/04/01
 ** \version 0.0.1
 ** \since 1.5.0
 ** \param datain Container3d containing the data.
 ** \param dataout Container3d containing the 3d DST-II.
 ** \pre data type of the Containers should be double float or long double.
 ** \par Example:
 ** \code
 ** slip::Volume<double> Vol(8,8,8);
 ** slip::iota(Vol.begin(),Vol.end(),0.0,1.0);
 ** slip::Volume<double> DSTVol(8,8,8);
 ** slip::dst3d(Vol,DSTVol);
 ** std::cout<<"DST(Vol) = \n"<<DSTVol<<std::endl;
 ** slip::Volume<double> IDSTVol(8,8,8);
 ** slip::idst3d(DSTVol,IDSTVol);
 ** std::cout<<"IDSTVol = \n"<<IDSTVol<<std::endl;
 ** \endcode
 */
template<typename Volume1,
	 typename Volume2>
inline
void dst3d(const Volume1 &datain, Volume2 &dataout)
{
  assert(datain.cols() == dataout.cols());
  assert(datain.rows() == dataout.rows());
  assert(datain.slices() == dataout.slices());
  slip::dst3d(datain.front_upper_left(),datain.back_bottom_right(),
	      dataout.front_upper_left());
  
}
/**
 ** \brief Computes the normalized 3d DST-III (inverse DST-II)  with 3 1d DST.
 ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
 ** \date 2024/01/01
 ** \version 0.0.1
 ** \since 1.5.0
 ** \param datain Container3d containing the data.
 ** \param dataout Container3d containing the 3d inverse DST-II.
 ** \pre data type of the Containers should be double float or long double.
 ** \par Example:
 ** \code
 ** slip::Volume<double> Vol(8,8,8);
 ** slip::iota(Vol.begin(),Vol.end(),0.0,1.0);
 ** slip::Volume<double> DSTVol(8,8,8);
 ** slip::dst3d(Vol,DSTVol);
 ** std::cout<<"DST(Vol) = \n"<<DSTVol<<std::endl;
 ** slip::Volume<double> IDSTVol(8,8,8);
 ** slip::idst3d(DSTVol,IDSTVol);
 ** std::cout<<"IDSTVol = \n"<<IDSTVol<<std::endl;
 ** \endcode
 */
template<typename Volume1,
	 typename Volume2>
inline
void idst3d(const Volume1 &datain, Volume2 &dataout)
{
  assert(datain.cols() == dataout.cols());
  assert(datain.rows() == dataout.rows());
  assert(datain.slices() == dataout.slices());
  slip::idst3d(datain.front_upper_left(),datain.back_bottom_right(),
	       dataout.front_upper_left());
  
}

}//slip::


#endif //SLIP_DST_HPP
