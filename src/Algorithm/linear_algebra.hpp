/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file linear_algebra.hpp
 * 
 * \brief Provides common linear algebra algorithms.
 * \since 1.0.0
 */
#ifndef SLIP_LINEAR_ALGEBRA_HPP
#define SLIP_LINEAR_ALGEBRA_HPP

#include <iterator>
#include <algorithm>
#include <numeric>
#include <cassert>
#include <cmath>
#include <limits>
#include <iostream>
#include <complex>
#include <stdexcept>
#include "complex_cast.hpp"
#include "Array2d.hpp"
//#include "Vector.hpp"
#include "Array.hpp"
#include "macros.hpp"
#include "arithmetic_op.hpp"
#include "linear_algebra_traits.hpp"
#include "error.hpp"
#include "apply.hpp"
#include "norms.hpp"
#include "utils_compilation.hpp"

namespace slip
{

template <class RandomAccessIterator2d, 
	  class RandomAccessIterator1, 
	  class RandomAccessIterator2>
void matrix_vector_multiplies(RandomAccessIterator2d M_upper_left,
				RandomAccessIterator2d M_bottom_right,
				RandomAccessIterator1 first1,
				RandomAccessIterator1 last1,
				RandomAccessIterator2 result_first,
				RandomAccessIterator2 result_last);
 /** 
    \enum DENSE_MATRIX_TYPE
    \brief Indicate the type of a dense matrix
    \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
    \date 2009/03/02
    \since 1.0.0
    \code
    enum DENSE_MATRIX_TYPE
    {
    DENSE_MATRIX_FULL,
    DENSE_MATRIX_DIAGONAL,
    DENSE_MATRIX_UPPER_BIDIAGONAL,
    DENSE_MATRIX_LOWER_BIDIAGONAL,
    DENSE_MATRIX_TRIDIAGONAL,
    DENSE_MATRIX_UPPER_TRIANGULAR,
    DENSE_MATRIX_LOWER_TRIANGULAR,
    DENSE_MATRIX_BANDED,
    DENSE_MATRIX_UPPER_HESSENBERG,
    DENSE_MATRIX_LOWER_HESSENBERG,
    DENSE_MATRIX_NULL
    };
    \endcode
*/


enum DENSE_MATRIX_TYPE
{
  DENSE_MATRIX_FULL,
  DENSE_MATRIX_DIAGONAL,
  DENSE_MATRIX_UPPER_BIDIAGONAL,
  DENSE_MATRIX_LOWER_BIDIAGONAL,
  DENSE_MATRIX_TRIDIAGONAL,
  DENSE_MATRIX_UPPER_TRIANGULAR,
  DENSE_MATRIX_LOWER_TRIANGULAR,
  DENSE_MATRIX_BANDED,
  DENSE_MATRIX_UPPER_HESSENBERG,
  DENSE_MATRIX_LOWER_HESSENBERG,
  DENSE_MATRIX_NULL
};


   /**
   ** \brief Computes the saxpy ("scalar a x plus b") operation: (\f$a*x + y\f$) between two values.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param __x The first value.
   ** \param __y The second value.
   ** \return The saxpy value.
   ** \par Complexity: 2 flops
   */
  template <typename F, typename S, typename RT, typename AT>
  struct saxpy : public std::binary_function<F, S, RT>
  {
    saxpy():a_(AT(1))
    {}
    saxpy(const AT& a):a_(a)
    {}

    inline
    RT
    operator()(const F& __x, const S& __y) const
    { 
      return a_ * __x + __y;
    }

    AT a_;
  };


   /**
   ** \brief Computes the complex operation: (\f$z1\overline{z2}\f$)
   ** between two complex values z1 and z2.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2014/03/22   
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param z1 The first value.
   ** \param z2 The second value.
   ** \return The saxpy value.
   ** \remarks Used to compute complex inner_product
   ** \par Complexity: 2 flops
   */
  template <class _Tp>
  struct z1conjz2 : public std::binary_function<_Tp, _Tp, _Tp>
  {
     inline
    _Tp
    operator()(const _Tp& z1, const _Tp& z2) const
    { 
      return z1 * slip::conj(z2);
    }
  };

   /**
   ** \brief Computes the complex operation: (\f$\overline{z1}z2\f$)
   ** between two complex values z1 and z2.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param z1 The first value.
   ** \param z2 The second value.
   ** \return The saxpy value.
   ** \remarks Used to compute complex inner_product
   ** \par Complexity: 2 flops
   */
  template <class _Tp>
  struct conjz1z2 : public std::binary_function<_Tp, _Tp, _Tp>
  {
     inline
    _Tp
    operator()(const _Tp& z1, const _Tp& z2) const
    { 
      return slip::conj(z1) * z2;
    }
  };


 


/** \name Inner-poduct algorithms */
  /* @{ */
  /**
   ** \brief Computes the hermitian inner-product of two ranges X and
   ** Y: \f[ \overline{X}^TY = X^HY\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 iterator to the beginning of the first sequence (X)
   ** \param last1 iterator to the end of the first sequence (X)
   ** \param first2 iterator to the beginning of the second sequence (Y)
   ** \param init initial value.
   ** \return hermitian_inner_product of two ranges.
   **
   ** \pre [first1, last1) must be valid.
   ** \pre [first2, first2 + (last1-first1)) must be valid.
   ** \par Complexity: 
   **  Exactly last1 - first1 applications of each binary operation. 
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** TC xic[] = {TC(1,1),TC(2,1),TC(2,3)};
   ** slip::Array<TC> Xic(Mic.cols(),xic); 
   ** std::cout<<"Xic = "<<std::endl;
   ** std::cout<<Xic<<std::endl;
   ** TC yic[] = {TC(2,1),TC(1,3),TC(2,4)};
   ** slip::Array<TC> Yic(Mic.cols(),yic);
   ** std::cout<<"Yic = "<<std::endl;
   ** std::cout<<Yic<<std::endl;
   ** std::cout<<"Xic^H Yic = "<<std::endl;
   ** std::cout<<slip::hermitian_inner_product(Xic.begin(),Xic.end(),
   **                                          Yic.begin(),TC(0,0))<<std::endl;
   ** \endcode
   */  
  template <typename RandomAccessIterator1, 
	    typename RandomAccessIterator2>
  inline
  typename std::iterator_traits<RandomAccessIterator1>::value_type 
  hermitian_inner_product(RandomAccessIterator1 first1, 
			  RandomAccessIterator1 last1, 
			  RandomAccessIterator2 first2, 
			  typename std::iterator_traits<RandomAccessIterator1>::value_type init)
  {
    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type;
    return std::inner_product(first1,
			      last1,
			      first2,
			      init,
			      std::plus<value_type>(),
			      slip::conjz1z2<value_type>());
  }
  

   /**
   ** \brief Computes the inner_product of two ranges X and Y: \f[ \overline{X}^TY = X^HY\f].
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 iterator to the beginning of the first sequence (X)
   ** \param last1 iterator to the end of the first sequence (X)
   ** \param first2 iterator to the beginning of the second sequence (Y)
   ** \param init initial value (zero by default).
   ** \return inner_product of two ranges.
   **
   ** \pre [first1, last1) must be valid.
   ** \pre [first2, first2 + (last1-first1)) must be valid.
   ** \remarks Calls hermitian_inner_product if data are complex
   ** \par Complexity: 
   ** Exactly last1 - first1 applications of each binary operation. 
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** TC xic[] = {TC(1,1),TC(2,1),TC(2,3)};
   ** slip::Array<TC> Xic(Mic.cols(),xic); 
   ** std::cout<<"Xic = "<<std::endl;
   ** std::cout<<Xic<<std::endl;
   ** TC yic[] = {TC(2,1),TC(1,3),TC(2,4)};
   ** slip::Array<TC> Yic(Mic.cols(),yic);
   ** std::cout<<"Yic = "<<std::endl;
   ** std::cout<<Yic<<std::endl;
   ** std::cout<<"Xic^H Yic = "<<std::endl;
   ** std::cout<<slip::inner_product(Xic.begin(),Xic.end(),
   **                                Yic.begin(),TC(0,0))<<std::endl;
   ** \endcode
   */  
  template <typename RandomAccessIterator1, 
	    typename RandomAccessIterator2>
  inline
  typename std::iterator_traits<RandomAccessIterator1>::value_type 
  inner_product(RandomAccessIterator1 first1, 
		RandomAccessIterator1 last1, 
		RandomAccessIterator2 first2, 
		typename std::iterator_traits<RandomAccessIterator1>::value_type init = typename std::iterator_traits<RandomAccessIterator1>::value_type())
  {
    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type;


    if(slip::is_complex(value_type()))
      {
	return slip::hermitian_inner_product(first1,
					    last1,
					    first2,
					    init);
      }
    else
      {
	return std::inner_product(first1,
				  last1,
				  first2,
				  init);
      }
  }
  

   /**
   ** \brief Computes the inner_product of two ranges X and Y:
   **  \f[ \overline{X}^TY = X^HY\f].
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param V1 First Vector.
   ** \param V2 Second Vector.
   ** \return inner_product of the two Vectors.
   ** \pre V1.size() >= V2.size()
   ** \remarks Calls hermitian_inner_product if data are complex
   ** \par Complexity: 
   ** Exactly last1 - first1 applications of each binary operation. 
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** TC xic[] = {TC(1,1),TC(2,1),TC(2,3)};
   ** slip::Array<TC> Xic(Mic.cols(),xic); 
   ** std::cout<<"Xic = "<<std::endl;
   ** std::cout<<Xic<<std::endl;
   ** TC yic[] = {TC(2,1),TC(1,3),TC(2,4)};
   ** slip::Array<TC> Yic(Mic.cols(),yic);
   ** std::cout<<"Yic = "<<std::endl;
   ** std::cout<<Yic<<std::endl;
   ** std::cout<<"Xic^H Yic = "<<std::endl;
   ** std::cout<<slip::inner_product(Xic,Yic)<<std::endl;
   ** \endcode
   */  
  template <typename Vector1, 
	    typename Vector2>
  inline
  typename Vector1::value_type 
  inner_product(const Vector1& V1,
		const Vector2& V2)
  {
    assert(V1.size() >= V2.size());
    return slip::inner_product(V1.begin(),V1.end(),V2.begin(),typename Vector1::value_type());
  }

 


  
  

     /**
   ** \brief Computes the generalized inner_product of two ranges: 
   **  \f[ (x,y)_A = \overline{x}^T A y = x^H A y \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>  
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 RandomAccessIterator to the first element of the vector x.
   ** \param last1 RandomAccessIterator to one-past-the-end element of the vector x.
   ** \param A_upper_left RandomAccessIterator2d to the upper_left matrix element.
   ** \param A_bottom_right RandomAccessIterator2d to the bottom_right matrix element.
   ** \param first2 RandomAccessIterator to the first element of the vector y.
   ** \param last2 RandomAccessIterator to one-past-the-end element of
   **  the vector y.
   ** \param init initial value (0 by default)
   ** \return inner_product value.
   ** \pre (last2 - first2) == (A_bottom_right - A_upper_left)[1]
   ** \pre (last1 - first1) >= (last2 - first2)
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** TC dci[] = {TC(1,1),TC(0,1),TC(0,1),
   **	          TC(1,0),TC(1,1),TC(0,1),
   **	          TC(1,2),TC(1,2),TC(1,2)};
   ** slip::Array2d<TC> Mic(3,3,dci);
   ** std::cout<<"Mic = "<<std::endl;
   ** std::cout<<Mic<<std::endl;
   ** TC xic[] = {TC(1,1),TC(2,1),TC(2,3)};
   ** slip::Array<TC> Xic(Mic.cols(),xic); 
   ** std::cout<<"Xic = "<<std::endl;
   ** std::cout<<Xic<<std::endl;
   ** TC yic[] = {TC(2,1),TC(1,3),TC(2,4)};
   ** slip::Array<TC> Yic(Mic.cols(),yic);
   ** std::cout<<"Yic = "<<std::endl;
   ** std::cout<<Yic<<std::endl;
   ** std::cout<<"Xic^H Mic Yic = "<<std::endl;
   ** std::cout<<slip::gen_inner_product(Xic.begin(),Xic.end(),
   **				         Mic.upper_left(),
   **			                 Mic.bottom_right(),
   **			                 Yic.begin(),Yic.end())<<std::endl;
   ** \endcode
   */  
  template <typename RandomAccessIterator1,
	    typename RandomAccessIterator2d,
	    typename RandomAccessIterator2>
  inline 
  typename std::iterator_traits<RandomAccessIterator2>::value_type
  gen_inner_product(RandomAccessIterator1 first1, 
		    RandomAccessIterator1 last1, 
		    RandomAccessIterator2d A_upper_left,
		    RandomAccessIterator2d A_bottom_right,
		    RandomAccessIterator2 first2, 
		    RandomAccessIterator2 last2, 
		    typename std::iterator_traits<RandomAccessIterator2>::value_type init = typename std::iterator_traits<RandomAccessIterator2>::value_type())
  {
    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type;
    slip::Array<value_type> tmp(last1-first1);
    slip::matrix_vector_multiplies(A_upper_left,A_bottom_right,first2,last2,tmp.begin(),tmp.end());
    
    if(slip::is_complex(value_type()))
      {
	return slip::hermitian_inner_product(first1,last1,
					 tmp.begin(),
					 init);
      }
    else
      {
	return std::inner_product(first1,last1,
				  tmp.begin(),
				  init);
      }
  }


   /**
   ** \brief Computes the generalized inner_product of two ranges: 
   **  \f[ (x,y)_A = \overline{x}^T A y = x^H A y \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>  
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param X The vector x.
   ** \param M The matrix M.
   ** \param Y The vector y.
   ** \return generalized inner_product value.
   ** \pre Y.size() == A.cols()
   ** \pre X.size() >= Y.size()
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** TC dci[] = {TC(1,1),TC(0,1),TC(0,1),
   **	          TC(1,0),TC(1,1),TC(0,1),
   **	          TC(1,2),TC(1,2),TC(1,2)};
   ** slip::Array2d<TC> Mic(3,3,dci);
   ** std::cout<<"Mic = "<<std::endl;
   ** std::cout<<Mic<<std::endl;
   ** TC xic[] = {TC(1,1),TC(2,1),TC(2,3)};
   ** slip::Array<TC> Xic(Mic.cols(),xic); 
   ** std::cout<<"Xic = "<<std::endl;
   ** std::cout<<Xic<<std::endl;
   ** TC yic[] = {TC(2,1),TC(1,3),TC(2,4)};
   ** slip::Array<TC> Yic(Mic.cols(),yic);
   ** std::cout<<"Yic = "<<std::endl;
   ** std::cout<<Yic<<std::endl;
   ** std::cout<<"Xic^H Mic Yic = "<<std::endl;
   ** std::cout<<slip::gen_inner_product(Xic,Mic,Yic)<<std::endl;
   ** \endcode
   */  
  template <typename Vector1,
	    typename Matrix,
	    typename Vector2>
  inline 
  typename Vector2::value_type
  gen_inner_product(const Vector1& X, 
		    const Matrix& M,
		    const Vector2& Y)

  {
    return slip::gen_inner_product(X.begin(),X.end(),
				   M.upper_left(),M.bottom_right(),
				   Y.begin(),Y.end(),
				   typename Vector2::value_type());
				   
  }


  /**
   ** \brief Computes the rayleigh coefficient of a vector x: 
   **  \f[ r(x) = \frac{x^H A x}{x^Hx} \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>  
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first1 RandomAccessIterator to the first element of the vector x.
   ** \param last1 RandomAccessIterator to one-past-the-end element of the vector x.
   ** \param A_upper_left RandomAccessIterator2d to the upper_left matrix element.
   ** \param A_bottom_right RandomAccessIterator2d to the bottom_right matrix element.
  
   ** \return rayleigh coefficient.
   ** \pre x must be different of the null vector
   ** \pre (last1 - first1) == (A_bottom_right - A_upper_left)[1]
   ** \pre (last1 - first1) >= (A_bottom_right - A_upper_left)[0]
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   **  slip::Array2d<float> Ai(3,3);
   ** slip::iota(Ai.begin(),Ai.end(),1.0f);
   ** std::cout<<"Ai = "<<std::endl;
   ** std::cout<<Ai<<std::endl;
   ** slip::Array<float> Xi(Ai.cols());
   ** slip::iota(Xi.begin(),Xi.end(),2.0f);
   ** std::cout<<"Xi = "<<std::endl;
   ** std::cout<<"rayleigh(Xi) = "<<std::endl;
   ** std::cout<<slip::rayleigh(Xi.begin(),Xi.end(),
   **	 		        Ai.upper_left(),Ai.bottom_right())<<std::endl;
   ** \endcode
   */  
  template <typename RandomAccessIterator1,
	    typename RandomAccessIterator2d>
  inline 
  typename std::iterator_traits<RandomAccessIterator1>::value_type
  rayleigh(RandomAccessIterator1 first1, 
	   RandomAccessIterator1 last1, 
	   RandomAccessIterator2d A_upper_left,
	   RandomAccessIterator2d A_bottom_right)
  {
    
    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type;
    return slip::gen_inner_product(first1,last1,
				   A_upper_left,A_bottom_right,
				   first1,last1) /
      slip::inner_product(first1,last1,
			  first1,
			  value_type());
  }

  /**
   ** \brief Computes the rayleigh coefficient o f a vector x: 
   **  \f[ r(x) = \frac{x^H A x}{x^Hx} \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>  
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param X The vector x.
   ** \param A The matrix A.
   ** \return rayleigh coefficient.
   ** \pre x must be different of the null vector
   ** \pre x.size() == A.cols()
   ** \pre x.size() >= A.rows()
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   **  slip::Array2d<float> Ai(3,3);
   ** slip::iota(Ai.begin(),Ai.end(),1.0f);
   ** std::cout<<"Ai = "<<std::endl;
   ** std::cout<<Ai<<std::endl;
   ** slip::Array<float> Xi(Ai.cols());
   ** slip::iota(Xi.begin(),Xi.end(),2.0f);
   ** std::cout<<"Xi = "<<std::endl;
   ** std::cout<<"rayleigh(Xi) = "<<std::endl;
   ** std::cout<<slip::rayleigh(Xi,Ai)<<std::endl;
   ** \endcode
   */  
  template <typename Vector,
	    typename Matrix>
  inline 
  typename Vector::value_type
  rayleigh(const Vector& X,
	   const Matrix& A)
  {
    return slip::rayleigh(X.begin(),X.end(),
			  A.upper_left(),A.bottom_right());
  }

/* @} */
  /** \name Matrix vector algorithms */
  /* @{ */
  
 /**
   ** \brief Computes the saxpy ("scalar a x plus b") for each element of two ranges x and y.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param a A scalar value 
   ** \param first1 iterator to the beginning of the first sequence (X)
   ** \param last1 iterator to the end of the first sequence (X)
   ** \param first2 iterator to the beginning of the second sequence (Y)
   ** \pre [first1, last1) must be valid.
   ** \pre The two range must have the same size
   ** \par Complexity: 2n flops
   */  
  template <typename AT,
	    typename RandomAccessIterator1, 
	    typename RandomAccessIterator2>
  inline
  void aXpY(const AT& a,
	    RandomAccessIterator1 first1, 
	    RandomAccessIterator1 last1, 
	    RandomAccessIterator2 first2)
  {
    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type1;
    typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type2;
    std::transform(first1,last1,
		   first2,
		   first2,
		   slip::saxpy<value_type1,value_type2,value_type2,AT>(a));
  }

   /**
   ** \brief Computes the multiplication of a vector V by a scalar scal.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param V_first RandomAccessIterator to the first element of the Vector.
   ** \param V_last RandomAccessIterator to one-past-the-end element of the vector.
   ** \param scal A scalar value.
   ** \param result_first RandomAccessIterator to the first element of the result vector.
   ** \pre [V_first,V_last) must be valid 
   ** \pre [result_first, result_first + (V_last-V_first) must be valid
   ** \pre The result range must be larger than the vector range.
   ** \pre result should be of scal type
   ** \par Example1:
   ** \code
   ** slip::Array<float> V1(4);
   ** slip::iota(V1.begin(),V1.end(),1.0);
   ** std::cout<<"V = "<<std::endl;
   ** std::cout<<V1<<std::endl;
   ** float scal = 3.3;
   ** slip::Array<float> Vscal1(4);
   ** slip::vector_scalar_multiplies(V1.begin(),V1.end(),scal,Vscal1.begin());
   ** std::cout<<"Vscal1 = "<<std::endl;
   ** std::cout<<Vscal1<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code 
   ** slip::Array<float> V1(4);
   ** slip::iota(V1.begin(),V1.end(),1.0);
   ** std::cout<<"V = "<<std::endl;
   ** std::cout<<V1<<std::endl;
   ** std::complex<float> scalc(2.2,2.2);
   ** slip::Array<std::complex<float> > Vscalc1(4);
   ** slip::vector_scalar_multiplies(V1.begin(),V1.end(),scalc,Vscalc1.begin());
   ** std::cout<<"Vscalc1 = "<<std::endl;
   ** std::cout<<Vscalc1<<std::endl;
   ** \endcode
   */  
  template <typename RandomAccessIterator1, 
	    typename T,
	    typename RandomAccessIterator2>
  inline
  void vector_scalar_multiplies(RandomAccessIterator1 V_first,
				RandomAccessIterator1 V_last,
				const T& scal,
				RandomAccessIterator2 result_first)
  { 
     for ( ; V_first != V_last; ++V_first, ++result_first)
       {
	 *result_first = *V_first * scal;
       }
  }


   /**
   ** \brief Computes the multiplication of a vector V by a scalar scal.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param V The Vector.
   ** \param scal A scalar value.
   ** \param Result The result of the scalar multplication.
   ** \pre V.size() <= Result.size()
   ** \pre result should be of scal type
   ** \par Example1:
   ** \code
   ** slip::Array<float> V1(4);
   ** slip::iota(V1.begin(),V1.end(),1.0);
   ** std::cout<<"V = "<<std::endl;
   ** std::cout<<V1<<std::endl;
   ** float scal = 3.3;
   ** slip::Array<float> Vscal1(4);
   ** slip::vector_scalar_multiplies(V1,scal,Vscal1);
   ** std::cout<<"Vscal1 = "<<std::endl;
   ** std::cout<<Vscal1<<std::endl;
   ** \endcode
   **
   */  
  template <typename Vector1, 
	    typename T,
	    typename Vector2>
  inline
  void vector_scalar_multiplies(const Vector1& V,
				const T& scal,
				Vector2& Result)
  { 
    assert(Result.size() <= V.size());
    slip::vector_scalar_multiplies(V.begin(),V.end(),scal,Result.begin());
  }

  /**
   ** \brief Computes the multiplication of a vector \f$\overline{V}\f$ by a scalar scal.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param V_first RandomAccessIterator to the first element of the Vector.
   ** \param V_last RandomAccessIterator to one-past-the-end element of the vector.
   ** \param scal A scalar value.
   ** \param result_first RandomAccessIterator to the first element of the result vector.
   ** \pre [V_first,V_last) must be valid 
   ** \pre [result_first, result_first + (V_last-V_first) must be valid
   ** \pre The result range must be larger than the vector range.
   ** \pre result should be of scal type
   ** \par Complexity: n flops for real data, 2n  flops for complex data
   ** \par Example:
   ** \code
   ** slip::Array<float> V1(4);
   ** slip::iota(V1.begin(),V1.end(),1.0);
   ** std::cout<<"V = "<<std::endl;
   ** std::cout<<V1<<std::endl;
   ** float scal = 3.3;
   ** slip::Array<float> Vscal1(4);
   ** slip::conj_vector_scalar_multiplies(V1.begin(),V1.end(),scalc,Vscalc1.begin());
 
   ** std::cout<<"Vscal1 = "<<std::endl;
   ** std::cout<<Vscal1<<std::endl;
   ** \endcode
   */  
  template <typename RandomAccessIterator1, 
	    typename T,
	    typename RandomAccessIterator2>
  inline
  void conj_vector_scalar_multiplies(RandomAccessIterator1 V_first,
				     RandomAccessIterator1 V_last,
				     const T& scal,
				     RandomAccessIterator2 result_first)
  { 
    for ( ; V_first != V_last; ++V_first, ++result_first)
       {
	 *result_first = slip::conj(*V_first) * scal;
       }
  }

   /**
   ** \brief Computes the multiplication of a vector \f$\overline{V}\f$ by a scalar scal.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param V The vector.
   ** \param scal A scalar value.
   ** \param Result Resulting vector.
   ** \pre V.size() <= Result.size()
   ** \pre result should be of scal type
   ** \par Complexity: n flops for real data, 2n  flops for complex data
   ** \par Example:
   ** \code
   ** slip::Array<float> V1(4);
   ** slip::iota(V1.begin(),V1.end(),1.0);
   ** std::cout<<"V = "<<std::endl;
   ** std::cout<<V1<<std::endl;
   ** float scal = 3.3;
   ** slip::Array<float> Vscal1(4);
   ** slip::conj_vector_scalar_multiplies(V1,scalc,Vscalc1);
 
   ** std::cout<<"Vscal1 = "<<std::endl;
   ** std::cout<<Vscal1<<std::endl;
   ** \endcode
   */  
  template <typename Vector1, 
	    typename T,
	    typename Vector2>
  inline
  void conj_vector_scalar_multiplies(const Vector1& V,
				     const T& scal,
				     Vector2& Result)
  { 
    slip::conj_vector_scalar_multiplies(V.begin(),V.end(),scal,Result.begin());
  }

 /**
   ** \brief Computes the multiplication of a Matrix by a scalar R = M*scal.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M_upper_left RandomAccessIterator2d to the upper_left matrix element.
   ** \param M_bottom_right RandomAccessIterator2d to the bottom_right matrix element.
 
   ** \param R_upper_left RandomAccessIterator2d to the upper_left matrix element.
   ** \param R_bottom_right RandomAccessIterator2d to the bottom_right matrix element.
   ** \param scal Scalar value.
   **
   ** \pre (M_bottom_right - M_upper_left)[0] == (R_bottom_right - R_upper_left)[0]
   ** \pre (M_bottom_right - M_upper_left)[1] == (R_bottom_right - R_upper_left)[1]
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** std::cout<<"M = "<<std::endl;
   ** std::cout<<M<<std::endl;
   ** slip::Matrix<float> Mscal1(3,4);
   ** float scal = 3.3;
   ** slip::matrix_scalar_multiplies(M.upper_left(),M.bottom_right(),
   **				  scal,
   **			  Mscal1.upper_left(), Mscal1.bottom_right());
   ** std::cout<<"Mscal1 = M * "<<scal<<std::endl;
   ** std::cout<<Mscal1<<std::endl;
   ** \endcode
   */  
  template <class RandomAccessIterator2d1, 
	    class T, 
	    class RandomAccessIterator2d2>
  inline
  void matrix_scalar_multiplies(RandomAccessIterator2d1 M_upper_left,
				RandomAccessIterator2d1 M_bottom_right,
				const T& scal,
				RandomAccessIterator2d2 R_upper_left,
				RandomAccessIterator2d2 R_bottom_right)

  { 
   
    typedef typename RandomAccessIterator2d1::size_type size_type;
    size_type M_rows = (M_bottom_right - M_upper_left)[0];
       
    assert((M_bottom_right - M_upper_left)[0] == (R_bottom_right - R_upper_left)[0]);
    assert((M_bottom_right - M_upper_left)[1] == (R_bottom_right - R_upper_left)[1]);
   
   
    for(std::size_t i = 0; i < M_rows; ++i)
      {
	slip::vector_scalar_multiplies(M_upper_left.row_begin(i),
				      M_upper_left.row_end(i),
				      scal,
				      R_upper_left.row_begin(i));
      }
    
  }
 
  
 /**
   ** \brief Computes the multiplication of a Matrix by a scalar R = M*scal.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M Matrix.
   ** \param scal Scalar value.
   ** \param R Resulting matrix
   ** \pre M.rows() == R.rows()
   ** \pre M.cols() == R.cols()
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** std::cout<<"M = "<<std::endl;
   ** std::cout<<M<<std::endl;
   ** slip::Matrix<float> Mscal1(3,4);
   ** float scal = 3.3;
   ** slip::matrix_scalar_multiplies(M,scal,Mscal1);
   ** std::cout<<"Mscal1 = M * "<<scal<<std::endl;
   ** std::cout<<Mscal1<<std::endl;
   ** \endcode
   */  
  template <class Matrix1, 
	    class T, 
	    class Matrix2>
  inline
  void matrix_scalar_multiplies(const Matrix1& M,
				const T& scal,
				Matrix2& R)

  { 
   
    slip::matrix_scalar_multiplies(M.upper_left(),M.bottom_right(),
				   scal,
				   R.upper_left(),R.bottom_right());
    
  }
 
  

  /**
   ** \brief Computes the multiplication of a Matrix and a Vector: Result=MV.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M_upper_left RandomAccessIterator2d to the upper_left matrix element.
   ** \param M_bottom_right RandomAccessIterator2d to the bottom_right matrix element.
   ** \param first1 RandomAccessIterator to the first element of the Vector.
   ** \param last1 RandomAccessIterator to one-past-the-end element of the vector.
   ** \param result_first RandomAccessIterator to the first element of the result vector.
   ** \param result_last RandomAccessIterator to one-past-the-end element of the result vector.
 
   **
   ** \pre (M_bottom_right - M_upper_left)[0] == (last1 - first1)
   ** \pre (M_bottom_right - M_upper_left)[1] == (result_last - result_first)
   ** \pre Matrix and Vector must have the same data type.
   ** \par Complexity: 2*M.rows()*M.cols() flops
   ** \remarks Works with real and complex matrix.
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** slip::Array<float> V(4);
   ** slip::iota(V.begin(),V.end(),1.0);
   ** std::cout<<"V = "<<std::endl;
   ** std::cout<<V<<std::endl;
   ** slip::Array<float> MxV(3);
   ** slip::matrix_vector_multiplies(M.upper_left(),M.bottom_right(),
   **                                V.begin(),V.end(),
   **                                MxV.begin(),MxV.end());
   ** std::cout<<"MxV = "<<std::endl;
   ** std::cout<<MxV<<std::endl;
   ** \endcode
   */  
  template <class RandomAccessIterator2d, 
	    class RandomAccessIterator1, 
	    class RandomAccessIterator2>
  inline
  void matrix_vector_multiplies(RandomAccessIterator2d M_upper_left,
				RandomAccessIterator2d M_bottom_right,
				RandomAccessIterator1 first1,
				RandomAccessIterator1 last1,
				RandomAccessIterator2 result_first,
				RandomAccessIterator2 result_last)
  { 
    assert((M_bottom_right - M_upper_left)[1] == (last1 - first1));
    assert((M_bottom_right - M_upper_left)[0] == (result_last - result_first));
   //  typename RandomAccessIterator2d::difference_type size2d = 
//       M_bottom_right - M_upper_left;
    typedef typename RandomAccessIterator2d::size_type size_type;
    //size_type nb_rows = size2d[0];
    //size_type nb_cols = size2d[1];
    //typename std::iterator_traits<RandomAccessIterator1>::difference_type size1d =    last1 - first1;
    typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
    
    
    
    size_type i = 0;
    for(; result_first!=result_last; ++result_first, ++i)
      {
	*result_first = std::inner_product(M_upper_left.row_begin(i),
					   M_upper_left.row_end(i),
					   first1,
					   value_type());
      }
    
  }


 /**
   ** \brief Computes the multiplication of a Matrix and a Vector: Result=MV.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M The Matrix container.
   ** \param V The Vector container.
   ** \param Result The container which contains the result 
   **        of the multiplication
   **
   ** \pre M must have row_begin iterators.
   ** \pre V must be compatible with the Container concept of the STL.
   ** \pre M.cols() == V.size()
   ** \pre Result.size() == M.rows()
   ** \pre Matrix and Vector must have the same data type.
   ** \remarks Works with real and complex matrix.
   ** \par Complexity: 2*M.rows()*M.cols() flops
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** slip::Array<float> V(4);
   ** slip::iota(V.begin(),V.end(),1.0);
   ** std::cout<<"V = "<<std::endl;
   ** std::cout<<V<<std::endl;
   ** slip::Array<float> MxV(3);
   ** slip::matrix_vector_multiplies(M,V,MxV);
   ** std::cout<<"MxV = "<<std::endl;
   ** std::cout<<MxV<<std::endl;
   ** \endcode
   */  
  template <class Matrix, 
	    class Vector1, 
	    class Vector2>
  inline
  void matrix_vector_multiplies(const Matrix& M,
				const Vector1& V,
				Vector2& Result)
  { 
    slip::matrix_vector_multiplies(M.upper_left(),M.bottom_right(),
				   V.begin(),V.end(),
				   Result.begin(), Result.end());
  }

   /**
   ** \brief Computes the hermitian left multiplication: \f[R = V^H M\f].
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param V_first RandomAccessIterator to the first element of the Vector.
   ** \param V_last RandomAccessIterator to one-past-the-end element of the vector.
   ** \param M_up RandomAccessIterator2d to the upper_left matrix element.
   ** \param M_bot RandomAccessIterator2d to the bottom_right matrix element.
   ** \param result_first RandomAccessIterator to the first element of the Vector R.
   ** \param result_last RandomAccessIterator to one-past-the-end element of the vector R.

   ** \pre (M_bottom_right - M_upper_left)[0] == (V_last - V_first)
   ** \pre (M_bottom_right - M_upper_left)[1] == (result_last - result_first)
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   **  typedef std::complex<double> TC;
   ** TC dc[] = {TC(1,1),TC(0,1),TC(0,1),TC(0,1),
   **	     TC(1,0),TC(1,1),TC(0,1),TC(0,1),
   **	     TC(1,2),TC(1,2),TC(1,2),TC(1,2)};
   ** slip::Matrix<TC> Mcc(3,4,dc);
   ** slip::Vector<std::complex<double> > Vcc(3);
   ** slip::iota(Vcc.begin(),Vcc.end(),std::complex<double>(1.0,1.0));
   ** std::cout<<"Vcc = "<<std::endl;
   ** std::cout<<Vcc<<std::endl;
   ** slip::Vector<std::complex<double> > Vrcc(4);
   ** slip::hvector_matrix_multiplies(Vcc.begin(),Vcc.end(),
   ** 				   Mcc.upper_left(),Mcc.bottom_right(),
   **				   Vrcc.begin(),Vrcc.end());
   ** std::cout<<"Vrcc = "<<std::endl;
   ** std::cout<<Vrcc<<std::endl;
   ** \endcode
   */  
  template <class RandomAccessIterator2d, 
	    class RandomAccessIterator1, 
	    class RandomAccessIterator2>
  inline
  void hvector_matrix_multiplies(RandomAccessIterator1 V_first,
				 RandomAccessIterator1 V_last,
				 RandomAccessIterator2d M_up,
				 RandomAccessIterator2d M_bot,
				 RandomAccessIterator2 result_first,
				 RandomAccessIterator2 result_last)
  { 
    typename RandomAccessIterator2d::difference_type size2d = 
      M_bot - M_up;
    typedef typename RandomAccessIterator2d::size_type size_type;
    size_type nb_rows = size2d[0];
    size_type nb_cols = size2d[1];
    typename std::iterator_traits<RandomAccessIterator1>::difference_type size1d = 
      V_last - V_first;

    typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
    
    assert(nb_rows == (std::size_t)size1d);
    assert(nb_cols == (std::size_t)(result_last - result_first));
   
    size_type j = 0;
    for(; result_first!=result_last; ++result_first, ++j)
      {
	*result_first = slip::hermitian_inner_product(V_first,
						      V_last,
						      M_up.col_begin(j),
						      value_type());
      }
    
  }


   /**
   ** \brief Computes the hermitian left multiplication: \f[R = V^H M\f].
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param V Vector V.
   ** \param M Matrix M.
   ** \param R Resulting vector R.
   ** \pre M.rows() == V.size()
   ** \pre M.cols() == R.size()
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   **  typedef std::complex<double> TC;
   ** TC dc[] = {TC(1,1),TC(0,1),TC(0,1),TC(0,1),
   **	     TC(1,0),TC(1,1),TC(0,1),TC(0,1),
   **	     TC(1,2),TC(1,2),TC(1,2),TC(1,2)};
   ** slip::Matrix<TC> Mcc(3,4,dc);
   ** slip::Vector<std::complex<double> > Vcc(3);
   ** slip::iota(Vcc.begin(),Vcc.end(),std::complex<double>(1.0,1.0));
   ** std::cout<<"Vcc = "<<std::endl;
   ** std::cout<<Vcc<<std::endl;
   ** slip::Vector<std::complex<double> > Vrcc(4);
   ** slip::hvector_matrix_multiplies(Vcc,Mcc,Vrcc);
   ** std::cout<<"Vrcc = "<<std::endl;
   ** std::cout<<Vrcc<<std::endl;
   ** \endcode
   */  
  template <class Vector1, 
	    class Matrix, 
	    class Vector2>
  inline
  void hvector_matrix_multiplies(const Vector1& V,
				 const Matrix& M,
				 Vector2& R)
  { 
    slip::hvector_matrix_multiplies(V.begin(),V.end(),
				    M.upper_left(),M.bottom_right(),
				    R.begin(),R.end());
  }
  /**
   ** \brief Computes the transpose left multiplication: \f[R = V^T M\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param V_first RandomAccessIterator to the first element of the Vector.
   ** \param V_last RandomAccessIterator to one-past-the-end element of the vector.
   ** \param M_up RandomAccessIterator2d to the upper_left matrix element.
   ** \param M_bot RandomAccessIterator2d to the bottom_right matrix element.
   ** \param result_first RandomAccessIterator to the first element of the Vector R.
   ** \param result_last RandomAccessIterator to one-past-the-end element of the vector R.

   ** \pre (M_bottom_right - M_upper_left)[0] == (V_last - V_first)
   ** \pre (M_bottom_right - M_upper_left)[1] == (result_last - result_first)
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   **  slip::Matrix<float> M(3,4);
   **  slip::iota(M.begin(),M.end(),0.0);
   ** std::cout<<"M = "<<std::endl;
   ** std::cout<<M<<std::endl;
   ** slip::Vector<float> Vtt(3);
   ** slip::iota(Vtt.begin(),Vtt.end(),1.0);
   ** std::cout<<"Vtt = "<<std::endl;
   ** std::cout<<Vtt<<std::endl;
   ** slip::Vector<float> Vttr(4);
   ** slip::tvector_matrix_multiplies(Vtt.begin(),Vtt.end(),
   **			   M.upper_left(),M.bottom_right(),
   **			   Vttr.begin(),Vttr.end());
   ** std::cout<<"Vttr = "<<std::endl;
   ** std::cout<<Vttr<<std::endl;
   ** \endcode
   */  
  template <class RandomAccessIterator2d, 
	    class RandomAccessIterator1, 
	    class RandomAccessIterator2>
  inline
  void tvector_matrix_multiplies(RandomAccessIterator1 V_first,
				 RandomAccessIterator1 V_last,
				 RandomAccessIterator2d M_up,
				 RandomAccessIterator2d M_bot,
				 RandomAccessIterator2 result_first,
				 RandomAccessIterator2 result_last)
  { 
    
    slip::hvector_matrix_multiplies(V_first,V_last,
				    M_up,M_bot,
				    result_first,result_last);
    
  }


  /**
   ** \brief Computes the transpose left multiplication: \f[R = V^T M\f].
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param V Vector V.
   ** \param M Matrix M.
   ** \param R Resulting vector R.
   ** \pre M.rows() == V.size()
   ** \pre M.cols() == R.size()
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   **  typedef std::complex<double> TC;
   ** TC dc[] = {TC(1,1),TC(0,1),TC(0,1),TC(0,1),
   **	     TC(1,0),TC(1,1),TC(0,1),TC(0,1),
   **	     TC(1,2),TC(1,2),TC(1,2),TC(1,2)};
   ** slip::Matrix<TC> Mcc(3,4,dc);
   ** slip::Vector<std::complex<double> > Vcc(3);
   ** slip::iota(Vcc.begin(),Vcc.end(),std::complex<double>(1.0,1.0));
   ** std::cout<<"Vcc = "<<std::endl;
   ** std::cout<<Vcc<<std::endl;
   ** slip::Vector<std::complex<double> > Vrcc(4);
   ** slip::tvector_matrix_multiplies(Vcc,Mcc,Vrcc);
   ** std::cout<<"Vrcc = "<<std::endl;
   ** std::cout<<Vrcc<<std::endl;
   ** \endcode
   */  
  template <class Vector1, 
	    class Matrix, 
	    class Vector2>
  inline
  void tvector_matrix_multiplies(const Vector1& V,
				 const Matrix& M,
				 Vector2& R)
  { 
    slip::tvector_matrix_multiplies(V.begin(),V.end(),
				    M.upper_left(),M.bottom_right(),
				    R.begin(),R.end());
  }



 /**
   ** \brief Computes the hermitian left multiplication of a matrix: \f[R = M1^H M2\f].
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M1_up RandomAccessIterator2d to the upper_left matrix M1 element.
   ** \param M1_bot RandomAccessIterator2d to the bottom_right matrix M1 element.
   ** \param M2_up RandomAccessIterator2d to the upper_left matrix M2 element.
   ** \param M2_bot RandomAccessIterator2d to the bottom_right matrix M2 element.
   ** \param result_up RandomAccessIterator to the upper_left result matrix element.
   ** \param result_up RandomAccessIterator to the bottom_right result matrix element.

   ** \pre (M1_bot-M1_up)[0] == (M2_bot-M2_up)[0]
   ** \pre (result_bot-result_up)[0] == (M1_bot-M1_up)[1]
   ** \pre (result_bot-result_up)[1] == (M2_bot-M2_up)[1]
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   **  typedef std::complex<double> TC;
   ** slip::Matrix<std::complex<double> > M1c(2,3);
   ** slip::iota(M1c.begin(),M1c.end(),
   ** std::complex<double>(1.0,1.0),
   ** std::complex<double>(1.0,1.0));
   ** slip::Matrix<std::complex<double> > M2c(2,4);
   ** slip::iota(M2c.begin(),M2c.end(),
   ** std::complex<double>(1.0,1.0),
   ** std::complex<double>(1.0,1.0));
   ** slip::Matrix<std::complex<double> > M3c(3,4);
   ** slip::hmatrix_matrix_multiplies(M1c.upper_left(),M1c.bottom_right(),
   **			              M2c.upper_left(),M2c.bottom_right(),
   **			              M3c.upper_left(),M3c.bottom_right());
   ** std::cout<<"M1c = \n"<<M1c<<std::endl;
   ** std::cout<<"M2c = \n"<<M2c<<std::endl;
   ** std::cout<<"M3c = \n"<<M3c<<std::endl;
   ** \endcode
   */  
  template <class RandomAccessIterator2d1, 
	    class RandomAccessIterator2d2, 
	    class RandomAccessIterator2d3>
  inline
  void hmatrix_matrix_multiplies(RandomAccessIterator2d1 M1_up,
				 RandomAccessIterator2d1 M1_bot,
				 RandomAccessIterator2d2 M2_up,
				 RandomAccessIterator2d2 M2_bot,
				 RandomAccessIterator2d3 result_up,
				 RandomAccessIterator2d3 result_bot)
  { 
    assert((M1_bot-M1_up)[0] == (M2_bot-M2_up)[0]);
    assert((result_bot-result_up)[0] == (M1_bot-M1_up)[1]);
    assert((result_bot-result_up)[1] == (M2_bot-M2_up)[1]);
    
   
    typedef typename RandomAccessIterator2d1::size_type size_type;
    size_type rows = static_cast<size_type>((M1_bot-M1_up)[1]);
    size_type cols = static_cast<size_type>((M2_bot-M2_up)[1]);
   

    typedef typename std::iterator_traits<RandomAccessIterator2d3>::value_type value_type;
    
    slip::DPoint2d<int> d(0,0);
    for(size_type i = 0; i < rows; ++i)
      {
	d[0] = i;
	for(size_type j = 0; j < cols; ++j)
	  {
	    d[1] = j;
	    result_up[d] = 
	      slip::hermitian_inner_product(M1_up.col_begin(i),M1_up.col_end(i),
					    M2_up.col_begin(j),
					    value_type());
	  }
      }
  }

 /**
   ** \brief Computes the hermitian left multiplication of a matrix: \f[R = M1^H M2\f].
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M1 The first Matrix container.
   ** \param M2 The second Matrix container.
   ** \param Result The container which contains the result 
   **        of the multiplication
   ** \pre M1.rows == M2.rows()
   ** \pre M1.cols == Result.rows()
   ** \pre M2.cols == Result.cols()
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   **  typedef std::complex<double> TC;
   ** slip::Matrix<std::complex<double> > M1c(2,3);
   ** slip::iota(M1c.begin(),M1c.end(),
   ** std::complex<double>(1.0,1.0),
   ** std::complex<double>(1.0,1.0));
   ** slip::Matrix<std::complex<double> > M2c(2,4);
   ** slip::iota(M2c.begin(),M2c.end(),
   ** std::complex<double>(1.0,1.0),
   ** std::complex<double>(1.0,1.0));
   ** slip::Matrix<std::complex<double> > M3c(3,4);
   ** slip::hmatrix_matrix_multiplies(M1c,M2c,M3c);
   ** std::cout<<"M1c = \n"<<M1c<<std::endl;
   ** std::cout<<"M2c = \n"<<M2c<<std::endl;
   ** std::cout<<"M3c = \n"<<M3c<<std::endl;
   ** \endcode
   */  
  template <class Matrix1, 
	    class Matrix2, 
	    class Matrix3>
  inline
  void hmatrix_matrix_multiplies(const Matrix1& M1,
				 const Matrix2& M2,
				 Matrix3& Result)
  { 
    slip::hmatrix_matrix_multiplies(M1.upper_left(),M1.bottom_right(),
				    M2.upper_left(),M2.bottom_right(),
				    Result.upper_left(),Result.bottom_right());
  }


  /**
   ** \brief Computes the hermitian matrix left multiplication of a vector: \f[R = M^H V\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M_up RandomAccessIterator2d to the upper_left matrix element.
   ** \param M_bot RandomAccessIterator2d to the bottom_right matrix element.
   ** \param V_first RandomAccessIterator to the first element of the Vector.
   ** \param V_last RandomAccessIterator to one-past-the-end element of the vector.
   ** \param R_first RandomAccessIterator to the first element of the Vector R.
   ** \param R_last RandomAccessIterator to one-past-the-end element of the vector R.

   ** \pre (M_bot-M_up)[0] == (V_last-V_first)
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   **  slip::Matrix<double> M1r(2,3);
   ** slip::iota(M1r.begin(),M1r.end(),1.0);
   ** slip::Vector<double> V1r(M1r.rows());
   ** slip::iota(V1r.begin(),V1r.end(),1.0);
   ** slip::Vector<double> V2r(M1r.cols());
   ** slip::hmatrix_vector_multiplies(M1r.upper_left(),M1r.bottom_right(),
   **                                 V1r.begin(),V1r.end(),
   **			              V2r.begin(),V2r.end());
   ** std::cout<<"M1r = \n"<<M1r<<std::endl;
   ** std::cout<<"V1r = \n"<<V1r<<std::endl;
   ** std::cout<<"V2r = M^TV1r\n"<<V2r<<std::endl;
   ** \endcode
   */
 template <class RandomAccessIterator2d, 
	    class RandomAccessIterator1, 
	    class RandomAccessIterator2>
  inline
  void hmatrix_vector_multiplies(RandomAccessIterator2d M_up,
				 RandomAccessIterator2d M_bot,
				 RandomAccessIterator1 V_first,
				 RandomAccessIterator1 V_last,
				 RandomAccessIterator2 R_first,
				 RandomAccessIterator2 UNUSED(R_last))
  { 
    assert((M_bot-M_up)[0] == (V_last-V_first));

    typedef typename RandomAccessIterator2d::size_type size_type;
    size_type cols = static_cast<size_type>((M_bot-M_up)[1]);
   

    typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
    
    for(size_type j = 0; j < cols; ++j)
      {

	*R_first++ = 
	  slip::hermitian_inner_product(M_up.col_begin(j),
					M_up.col_end(j),
					V_first,
					value_type());
	
	 
      }
  }


  /**
   ** \brief Computes the hermitian matrix left multiplication of a vector: \f[R = M^H V\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M Matrix M.
   ** \param V Vector V.
   ** \param R Resulting vector R.
   ** \pre M.rows() == V.size()
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   **  slip::Matrix<double> M1r(2,3);
   ** slip::iota(M1r.begin(),M1r.end(),1.0);
   ** slip::Vector<double> V1r(M1r.rows());
   ** slip::iota(V1r.begin(),V1r.end(),1.0);
   ** slip::Vector<double> V2r(M1r.cols());
   ** slip::hmatrix_vector_multiplies(M1r,V1r,V2r);
   ** std::cout<<"M1r = \n"<<M1r<<std::endl;
   ** std::cout<<"V1r = \n"<<V1r<<std::endl;
   ** std::cout<<"V2r = M^TV1r\n"<<V2r<<std::endl;
   ** \endcode
   */
  template <class Matrix, 
	    class Vector1, 
	    class Vector2>
  inline
  void hmatrix_vector_multiplies(const Matrix& M,
				 const Vector1& V,
				 Vector2& R)
  { 
    slip::hmatrix_vector_multiplies(M.upper_left(),M.bottom_right(),
				    V.begin(),V.end(),
				    R.begin(),R.end());
  }


  /**
   ** \brief Computes the hermitian right multiplication of a matrix: \f[R = M1M2^H\f].
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param M1_up RandomAccessIterator2d to the upper_left matrix M1 element.
   ** \param M1_bot RandomAccessIterator2d to the bottom_right matrix M1 element.
   ** \param M2_up RandomAccessIterator2d to the upper_left matrix M2 element.
   ** \param M2_bot RandomAccessIterator2d to the bottom_right matrix M2 element.
   ** \param result_up RandomAccessIterator to the upper_left result matrix element.
   ** \param result_up RandomAccessIterator to the bottom_right result matrix element.

   ** \pre (M1_bot-M1_up)[1] == (M2_bot-M2_up)[1]
   ** \pre (result_bot-result_up)[0] == (M1_bot-M1_up)[0]
   ** \pre (result_bot-result_up)[1] == (M2_bot-M2_up)[0]
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** slip::Matrix<TC> M1c(2,3);
   ** slip::iota(M1c.begin(),M1c.end(),TC(1.0,1.0),TC(1.0,1.0));
   ** slip::Matrix<TC> M2c(4,3);
   ** slip::iota(M2c.begin(),M2c.end(),TC(1.0,1.0),TC(1.0,1.0));
   ** slip::Matrix<TC> M3c(2,4);
   ** slip::matrix_hmatrix_multiplies(M1c.upper_left(),M1c.bottom_right(),
   **                                 M2c.upper_left(),M2c.bottom_right(),
   **                                 M3c.upper_left(),M3c.bottom_right());

   ** std::cout<<"M1c = \n"<<M1c<<std::endl;
   ** std::cout<<"M2c = \n"<<M2c<<std::endl;
   ** std::cout<<"M3c = \n"<<M3c<<std::endl;
   ** \endcode
   */  
  template <class RandomAccessIterator2d1, 
	    class RandomAccessIterator2d2, 
	    class RandomAccessIterator2d3>
  inline
  void matrix_hmatrix_multiplies(RandomAccessIterator2d1 M1_up,
				 RandomAccessIterator2d1 M1_bot,
				 RandomAccessIterator2d2 M2_up,
				 RandomAccessIterator2d2 M2_bot,
				 RandomAccessIterator2d3 result_up,
				 RandomAccessIterator2d3 result_bot)
  { 
    assert((M1_bot-M1_up)[1] == (M2_bot-M2_up)[1]);
    assert((result_bot-result_up)[0] == (M1_bot-M1_up)[0]);
    assert((result_bot-result_up)[1] == (M2_bot-M2_up)[0]);
    
   
    typedef typename RandomAccessIterator2d1::size_type size_type;
    size_type rows = static_cast<size_type>((M1_bot-M1_up)[0]);
    size_type cols = static_cast<size_type>((M2_bot-M2_up)[0]);
   

    typedef typename std::iterator_traits<RandomAccessIterator2d3>::value_type value_type;
    
    slip::DPoint2d<int> d(0,0);
    for(size_type i = 0; i < rows; ++i)
      {
	d[0] = i;
	for(size_type j = 0; j < cols; ++j)
	  {
	    d[1] = j;
	    result_up[d] = 
	      slip::hermitian_inner_product(M1_up.row_begin(i),M1_up.row_end(i),
					    M2_up.row_begin(j),
					    value_type());
	  }
      }
  }


 /**
   ** \brief Computes the hermitian left multiplication of a matrix: \f[R = M1M2^H\f].
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param M1 The first Matrix container.
   ** \param M2 The second Matrix container.
   ** \param Result The container which contains the result 
   **        of the multiplication
   ** \pre M1.cols() == M2.cols()
   ** \pre M1.rows == Result.rows()
   ** \pre M2.rows == Result.cols()
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** slip::Matrix<TC> M1c(2,3);
   ** slip::iota(M1c.begin(),M1c.end(),TC(1.0,1.0),TC(1.0,1.0));
   ** slip::Matrix<TC> M2c(4,3);
   ** slip::iota(M2c.begin(),M2c.end(),TC(1.0,1.0),TC(1.0,1.0));
   ** slip::Matrix<TC> M3c(2,4);
   ** slip::matrix_hmatrix_multiplies(M1c,M2c,M3c);
   ** std::cout<<"M1c = \n"<<M1c<<std::endl;
   ** std::cout<<"M2c = \n"<<M2c<<std::endl;
   ** std::cout<<"M3c = \n"<<M3c<<std::endl;
   ** \endcode
   */  
  template <class Matrix1, 
	    class Matrix2, 
	    class Matrix3>
  inline
  void matrix_hmatrix_multiplies(const Matrix1& M1,
				 const Matrix2& M2,
				 Matrix3& Result)
  { 
    slip::matrix_hmatrix_multiplies(M1.upper_left(),M1.bottom_right(),
				    M2.upper_left(),M2.bottom_right(),
				    Result.upper_left(),Result.bottom_right());
  }

  
   /**
   ** \brief Computes the generalised multiplication of a Matrix and a Vector:
   ** computes the update \f[Y = MV + Y \f]. 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M_up RandomAccessIterator2d to the upper_left matrix element.
   ** \param M_bot RandomAccessIterator2d to the bottom_right matrix element.
   ** \param V_first RandomAccessIterator to the first element of the Vector V.
   ** \param V_last RandomAccessIterator to one-past-the-end element of the vector V.
   ** \param Y_first RandomAccessIterator to the first element of the Vector Y.
   ** \param Y_last RandomAccessIterator to one-past-the-end element of the vector Y.
   ** \pre (M_bot - M_up)[1] == (V_last - V_first)
   ** \pre (Y_last - Y_first) == (M_bot - M_up)[0]
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** std::cout<<"M = "<<std::endl;
   ** std::cout<<M<<std::endl;
   ** slip::Array<float> V1(4);
   ** slip::iota(V1.begin(),V1.end(),1.0);
   ** std::cout<<"V = "<<std::endl;
   ** std::cout<<V1<<std::endl;
   ** slip::Array<float> Y(3);
   ** slip::iota(Y.begin(),Y.end(),2.0);
   ** std::cout<<"Y = "<<std::endl;
   ** std::cout<<Y<<std::endl;
   ** slip::gen_matrix_vector_multiplies(M.upper_left(),M.bottom_right(),
   **			                 V1.begin(),V1.end(),
   **			                 Y.begin(),Y.end());
   ** std::cout << "Y = MV + Y " <<std::endl;
   ** std::cout<<Y<<std::endl;
   ** \endcode
   */  
  template <typename RandomAccessIterator2d,
	    typename RandomAccessIterator1, 
	    typename RandomAccessIterator2>
  inline
  void gen_matrix_vector_multiplies(RandomAccessIterator2d M_up,
				    RandomAccessIterator2d M_bot,
				    RandomAccessIterator1 V_first,
				    RandomAccessIterator1 V_last,
				    RandomAccessIterator2 Y_first,
				    RandomAccessIterator2 Y_last)
  { 
    assert((M_bot - M_up)[1] == (V_last - V_first));
    assert((Y_last - Y_first) == (M_bot - M_up)[0]);
   
    typedef typename std::iterator_traits<RandomAccessIterator2d>::value_type value_type;
    typedef typename RandomAccessIterator2d::size_type size_type;
    typedef RandomAccessIterator2 iterator;
 
    iterator it = Y_first;
    iterator ite = Y_last;
    size_type i = 0;
    for(; it!=ite; ++it, ++i)
      {
	*it = *it + std::inner_product(M_up.row_begin(i),M_up.row_end(i),
				       V_first,
				       value_type());
      }
    
  }

  /**
   ** \brief Computes the generalised multiplication of a Matrix and a Vector:
   ** computes the update \f[Y = MV + Y\f]. 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M The Matrix container.
   ** \param V The Vector container.
   ** \param Y The Vector container.
   ** \pre M must have row_begin iterators.
   ** \pre V and Y must be compatible with the Container concept of the STL.
   ** \pre M.cols() == V.size()
   ** \pre Y.size() == M.rows()
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** std::cout<<"M = "<<std::endl;
   ** std::cout<<M<<std::endl;
   ** slip::Array<float> V1(4);
   ** slip::iota(V1.begin(),V1.end(),1.0);
   ** std::cout<<"V = "<<std::endl;
   ** std::cout<<V1<<std::endl;
   ** slip::Array<float> Y(3);
   ** slip::iota(Y.begin(),Y.end(),2.0);
   ** std::cout<<"Y = "<<std::endl;
   ** std::cout<<Y<<std::endl;
   ** slip::gen_matrix_vector_multiplies(M,V1,Y);
   ** std::cout << "Y = MV + Y " <<std::endl;
   ** std::cout<<Y<<std::endl;
   ** \endcode
   */  
  template <class Matrix, 
	    class Vector1, 
	    class Vector2>
  inline
  void gen_matrix_vector_multiplies(const Matrix& M,
				    const Vector1& V,
				    Vector2& Y)
  { 
    slip::gen_matrix_vector_multiplies(M.upper_left(),M.bottom_right(),
				       V.begin(),V.end(),
				       Y.begin(),Y.end());
  }

  

   /**
   ** \brief Computes the matrix matrix multiplication of 2 2d ranges.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/19
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M1_up 2D iterator on the upper_left element of M1 container
   ** \param M1_bot 2D iterator on the bottom_right element of M1 container
   ** \param M2_up 2D iterator on the upper_left element of M2 container
   ** \param M2_bot 2D iterator on the bottom_right element of M2 container
   ** \param R_up 2D iterator on the upper_left element of R container
   ** \param R_bot 2D iterator on the bottom_right element of R container
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** std::cout<<"M = "<<std::endl;
   ** std::cout<<M<<std::endl;
   ** slip::Matrix<float> M2(4,2);
   ** slip::iota(M2.begin(),M2.end(),0.0);
   ** std::cout<<"M2 = "<<std::endl;
   ** std::cout<<M2<<std::endl;
   ** slip::Matrix<float> MxM2(3,2);
   ** slip::matrix_matrix_multiplies(M.upper_left(),M.bottom_right(),
   **			             M2.upper_left(),M2.bottom_right(),
   **			             MxM2.upper_left(),MxM2.bottom_right());
   ** std::cout<<"M x M2 = "<<std::endl;
   ** std::cout<<MxM2<<std::endl;
   ** \endcode
   */
  template <typename MatrixIterator1,
	    typename MatrixIterator2,
	    typename MatrixIterator3>
  inline
  void
  matrix_matrix_multiplies(MatrixIterator1 M1_up, MatrixIterator1 M1_bot, 
			   MatrixIterator2 M2_up, MatrixIterator2 M2_bot, 
			   MatrixIterator3 R_up, MatrixIterator3 R_bot)

  {
    assert((M1_bot - M1_up)[1] == (M2_bot - M2_up)[0]);
    assert((M1_bot - M1_up)[0] == (R_bot - R_up)[0]);
    assert((M2_bot - M2_up)[1] == (R_bot - R_up)[1]);
    typedef typename MatrixIterator1::value_type value_type;
    typedef  typename MatrixIterator1::size_type size_type;
    size_type M1_rows = static_cast<size_type>((M1_bot - M1_up)[0]);
    size_type M2_cols =  static_cast<size_type>((M2_bot - M2_up)[1]);
   
    for(size_type i = 0; i < M1_rows; ++i)
      {
	for(size_type j = 0; j < M2_cols; ++j)
	  {
	    
	    *R_up = std::inner_product(M1_up.row_begin(i),M1_up.row_end(i),
				       M2_up.col_begin(j),
				       value_type());
	    ++R_up;
	  }
      }
    
  }


   /**
   ** \brief Computes the multiplication of a two Matrix.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M1 The first Matrix container.
   ** \param M2 The second Matrix container.
   ** \param Result The container which contains the result 
   **        of the multiplication
   **
   ** \pre M1 must have row_begin iterators.
   ** \pre M2 must have col_begin iterators.
   ** \pre M1, M2 and Result must have the double bracket accessor
   ** \pre M1, M2 and Result should contain data of the same value type
   ** \pre M1.cols == M2.rows()
   ** \pre M1.rows == Result.rows()
   ** \pre M2.cols == Result.cols()
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** std::cout<<"M = "<<std::endl;
   ** std::cout<<M<<std::endl;
   ** slip::Matrix<float> M2(4,2);
   ** slip::iota(M2.begin(),M2.end(),0.0);
   ** std::cout<<"M2 = "<<std::endl;
   ** std::cout<<M2<<std::endl;
   ** slip::Matrix<float> MxM2(3,2);
   ** slip::matrix_matrix_multiplies(M,M2,MxM2);
   ** std::cout<<"M x M2 = "<<std::endl;
   ** std::cout<<MxM2<<std::endl;
   ** \endcode
   */  
  template <class Matrix1, 
	    class Matrix2, 
	    class Matrix3>
  inline
  void matrix_matrix_multiplies(const Matrix1& M1,
				const Matrix2& M2,
				Matrix3& Result)
  { 
    slip::matrix_matrix_multiplies(M1.upper_left(),M1.bottom_right(),
				   M2.upper_left(),M2.bottom_right(),
				   Result.upper_left(),Result.bottom_right());
  }


  /**
   ** \brief Computes the generalized multiplication of a two Matrix:
   ** \f[C = AB + C\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \param B_up 2D iterator on the upper_left element of B container
   ** \param B_bot 2D iterator on the bottom_right element of B container
   ** \param C_up 2D iterator on the upper_left element of C container
   ** \param C_bot 2D iterator on the bottom_right element of C container
   ** \pre A, B and C should contain data of the same value type
   ** \pre (A_bot-A_up)[1] == (B_bot-B_up)[0]
   ** \pre (A_bot-A_up)[0] == (C_bot-C_up)[0]
   ** \pre (B_bot-B_up)[1] == (C_bot-C_up)[1]
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<float> A(3,4);
   ** slip::iota(A.begin(),A.end(),0.0);
   ** std::cout<<"A = "<<std::endl;
   ** std::cout<<A<<std::endl;
   ** slip::Matrix<float> B(4,2);
   ** slip::iota(B.begin(),B.end(),0.0);
   ** std::cout<<"B = "<<std::endl;
   ** std::cout<<B<<std::endl;
   ** slip::Matrix<float> C(3,2);
   ** slip::iota(C.begin(),C.end(),2.0);
   ** std::cout<<"C = "<<std::endl;
   ** std::cout<<C<<std::endl;
   ** std::cout << "C = AB + C " << std::endl;
   **  slip::gen_matrix_matrix_multiplies(M.upper_left(),M.bottom_right(),
   **				          M2.upper_left(),M2.bottom_right(),
   **			                  C.upper_left(),C.bottom_right());
   ** std::cout<<C<<std::endl;
   ** \endcode
   */  
  template <typename RandomAccessIterator2d1, 
	    typename RandomAccessIterator2d2, 
	    typename RandomAccessIterator2d3>
  inline
  void gen_matrix_matrix_multiplies(RandomAccessIterator2d1 A_up,
				    RandomAccessIterator2d1 A_bot,
				    RandomAccessIterator2d2 B_up,
				    RandomAccessIterator2d2 B_bot,
				    RandomAccessIterator2d3 C_up,
				    RandomAccessIterator2d3 C_bot)
  { 
    assert((A_bot-A_up)[1] == (B_bot-B_up)[0]);
    assert((A_bot-A_up)[0] == (C_bot-C_up)[0]);
    assert((B_bot-B_up)[1] == (C_bot-C_up)[1]);
  
   
    typedef typename std::iterator_traits<RandomAccessIterator2d1>::value_type value_type;
    typedef typename RandomAccessIterator2d1::size_type size_type;
    size_type A_rows = (A_bot-A_up)[0];
    size_type B_cols = (B_bot-B_up)[1];
    for(size_type i = 0; i < A_rows; ++i)
      {
	for(size_type j = 0; j < B_cols; ++j)
	  {
	    *C_up  += std::inner_product(A_up.row_begin(i),A_up.row_end(i),
					 B_up.col_begin(j),
					 value_type());
	    ++C_up;
	  }
      }
  }

  /**
   ** \brief Computes the generalized multiplication of a two Matrix:
   ** \f[C = AB + C\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A The first Matrix container.
   ** \param B The second Matrix container.
   ** \param C The third input/output Matrix
   **
   ** \pre A must have row_begin iterators.
   ** \pre B must have col_begin iterators.
   ** \pre A, B and C must have the double bracket accessor
   ** \pre A, B and C should contain data of the same value type
   ** \pre A.cols == B.rows()
   ** \pre A.rows == C.rows()
   ** \pre B.cols == C.cols()
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<float> A(3,4);
   ** slip::iota(A.begin(),A.end(),0.0);
   ** std::cout<<"A = "<<std::endl;
   ** std::cout<<A<<std::endl;
   ** slip::Matrix<float> B(4,2);
   ** slip::iota(B.begin(),B.end(),0.0);
   ** std::cout<<"B = "<<std::endl;
   ** std::cout<<B<<std::endl;
   ** slip::Matrix<float> C(3,2);
   ** slip::iota(C.begin(),C.end(),2.0);
   ** std::cout<<"C = "<<std::endl;
   ** std::cout<<C<<std::endl;
   ** std::cout << "C = AB + C " << std::endl;
   ** slip::gen_matrix_matrix_multiplies(A,B,C);
   ** std::cout<<C<<std::endl;
   ** \endcode
   */  
  template <class Matrix1, 
	    class Matrix2, 
	    class Matrix3>
  inline
  void gen_matrix_matrix_multiplies(const Matrix1& A,
				    const Matrix2& B,
				    Matrix3& C)
  { 
    slip::gen_matrix_matrix_multiplies(A.upper_left(),A.bottom_right(),
				       B.upper_left(),B.bottom_right(),
				       C.upper_left(),C.bottom_right());
  }


  /**
   ** \brief Computes the multiplication of two Matrix.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/10/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M1 The first Matrix container
   ** \param nr1 The number of rows of M1
   ** \param nc1 The number of columns of M1
   ** \param M2 The second Matrix container
   ** \param nr2 The number of rows of M2
   ** \param nc2 The number of columns of M2
   ** \param Result The container which contains the result 
   **        of the multiplication
   ** \param nr3 The number of rows of Result
   ** \param nc3 The number of columns of Result
   **
   ** \pre M1, M2 and M3 must have the double bracket element accessor 
   ** \pre nc1 == nr2
   ** \pre nr1 == nr3
   ** \pre nc1 == nc3
   ** \deprecated You should use slip::matrix_matrix_multiplies instead
   */  
  template <class Matrix1, class Matrix2, class Matrix3>
  inline
  void multiplies(const Matrix1 & M1,
		  const std::size_t nr1,
		  const std::size_t nc1,
		  const Matrix2 & M2,
		  const std::size_t nr2,
		  const std::size_t nc2,
		  Matrix3 & Result,
		  const std::size_t nr3,
		  const std::size_t nc3)
  {
    assert(nc1 == nr2);
    assert(nr1 == nr3);
    assert(nc2 == nc3);

    for(std::size_t i = 0; i < nr1; ++i)
      {
	for(std::size_t j = 0; j < nc2; ++j)
	  {
	    Result[i][j] = 0;
	    for(std::size_t k = 0; k < nc1; ++k)
	      {
		Result[i][j] += M1[i][k] * M2[k][j];
	      }
	  }
      }
  }

  /**
   ** \brief Computes the multiplication of a Matrix with a Vector.
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/23
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M The Matrix container
   ** \param nrm The number of rows of M
   ** \param ncm The number of columns of M
   ** \param V The Vector container
   ** \param nrv The number of rows of V
   ** \param Result The container which contains the result 
   **        of the multiplication
   ** \param nrr The number of columns of Result
   **
   ** \pre M must have double bracket element accessor
   ** \pre V and Result must have simple bracket element accessor
   ** \pre ncm == nrv
   ** \pre nrv == nrr
   ** \deprecated You should use slip::matrix_vector_multiplies instead
     */  
  template <class Matrix1, class Matrix2, class Matrix3>
  inline
  void multiplies(const Matrix1 & M,
		  const std::size_t nrm,
		  const std::size_t ncm,
		  const Matrix2 & V,
		  const std::size_t nrv,
		  Matrix3 & Result,
		  const std::size_t nrr)
  {
    assert(ncm == nrv);
    assert(nrm == nrr);

    for(std::size_t i = 0; i < nrm; ++i)
      {
        Result[i] = 0;
	for(std::size_t k = 0; k < ncm; ++k)
	  {
		Result[i] += M[i][k] * V[k];
	  }
      }
  }


  /**
   ** \fn  void multiplies(const Matrix1 & M1, 
   **                      const Matrix2 & M2,
   **                      Matrix3 & Result)
   ** \brief Computes the multiplication of two Matrix.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/10/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M1 first Matrix container
   ** \param M2 second Matrix container
   ** \param Result result of the multiplication
   **
   ** \pre M1, M2 and M3 must have cols() and rows() methods
   ** \pre M1, M2 and M3 must have the double bracket element accessor 
   ** \pre Matrix are supposed to be allocated first
   ** \pre M1.cols() == M2.rows()
   ** \pre M1.rows() == M3.rows()
   ** \pre M2.cols() == M3.cols()
   ** \deprecated You should use slip::matrix_matrix_multiplies instead
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0,1.0);
   ** slip::Array2d<float> M2(4,3);
   ** slip::iota(M2.rbegin(),M2.rend(),0.0,1.0);
   ** slip::block2d<float,3,3> Result;
   ** slip::multiplies(M,M2,Result);
   ** \endcode
   */  
  template <class Matrix1, class Matrix2, class Matrix3>
  inline
  void multiplies(const Matrix1 & M1, 
		  const Matrix2 & M2,
		  Matrix3 & Result)
  {
    assert(M1.cols() == M2.rows());
    assert(M1.rows() == Result.rows());
    assert(M2.cols() == Result.cols());
    std::size_t nr1 = M1.rows();
    std::size_t nc1 = M1.cols();
    std::size_t nc2 = M2.cols();
    slip::multiplies(M1,nr1,nc1,M2,nc1,nc2,Result,nr1,nc2);
  }

  /**
   ** \brief Computes the multiplication of two matrices
   ** \f$ Res = A_1 \times A_2 \f$
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/07/21
   ** \since 1.0.0
   ** \version 0.0.1
   **
   ** \param A1_up 2d iterator on the upper_left element of the A1 matrix 
   ** \param A1_bot 2d iterator on the bottom_right element of the A1 matrix 
   ** \param A2_up 2d iterator on the upper_left element of the A2 matrix 
   ** \param A2_bot 2d iterator on the bottom_right element of the A2 matrix 
   ** \param Res_up 2d iterator on the upper_left element of the Res matrix 
   ** \param Res_bot 2d iterator on the bottom_right element of the Res matrix 
   **
   ** \pre NbRows(Res) == NbRows(A1)
   ** \pre NbCols(Res) == NbCols(A2)
   ** \pre NbRows(A2) == NbCols(A1)
   ** \deprecated You should use slip::matrix_matrix_multiplies instead
   */   
  template <typename MatrixIterator1, 
	    typename MatrixIterator2, 
	    typename MatrixIterator3>
  inline
  void multiplies(MatrixIterator1 A1_up, MatrixIterator1 A1_bot, 
		  MatrixIterator2 A2_up, MatrixIterator2 A2_bot, 
		  MatrixIterator3 Res_up, MatrixIterator3 Res_bot)
  {
    typedef typename std::iterator_traits<MatrixIterator1>::value_type _T;
    typename MatrixIterator1::difference_type sizeA1= A1_bot - A1_up;
    typename MatrixIterator2::difference_type sizeA2= A2_bot - A2_up;
    typename MatrixIterator3::difference_type sizeRes= Res_bot - Res_up;
    assert(sizeRes[0] == sizeA1[0]);
    assert(sizeRes[1] == sizeA2[1]);
    assert(sizeA1[1] == sizeA2[0]);
    
    for(int i = 0; i < sizeRes[0]; ++i)
      {
	for(int j = 0; j < sizeRes[1]; ++j)
	  {
	    slip::DPoint2d<int> ij(i,j);
	    *(Res_up + ij) = _T(0);
	    for(int k = 0; k < sizeA1[1]; ++k)
	      {
		slip::DPoint2d<int> ik(i,k);
		slip::DPoint2d<int> kj(k,j);
		*(Res_up + ij) += (*(A1_up + ik)) * (*(A2_up + kj));
	      }
	  }
      }
  }

  /**
   ** \brief Computes the multiplication of a matrix with its transposate A^T
   ** \f$ Res = A \times A^T \f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2013/02/14
   ** \since 1.2.0
   ** \version 0.0.1
   **
   ** \param M1_up 2d iterator on the upper_left element of the M1 matrix 
   ** \param M1_bot 2d iterator on the bottom_right element of the M1 matrix 
   ** \param Res_up 2d iterator on the upper_left element of the Res matrix 
   ** \param Res_bot 2d iterator on the bottom_right element of the Res matrix 
   ** \pre (Res_bot-Res_up)[0] == (M1_bot-M1_up)[0])
   ** \pre (Res_bot-Res_up)[1] == (M1_bot-M1_up)[0])
   ** \par Example:
   ** \code
   ** const std::size_t rows = 5;
   ** const std::size_t cols = 6;
   ** slip::Matrix<double> A(rows,cols);
   ** slip::iota(A.begin(),A.end(),1.0);
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Matrix<double> AAT(rows,rows);
   ** slip::matrix_matrixt_multiplies(A.upper_left(),
   **                                 A.bottom_right(),
   **                                 AAT.upper_left(),
   **                                 AAT.bottom_right());
   ** std::cout<<"AAT = \n"<<AAT<<std::endl;
   ** \endcode
   */   
  template <class RandomAccessIterator2d1, 
	   class RandomAccessIterator2d2>
  inline
  void matrix_matrixt_multiplies(RandomAccessIterator2d1 M1_up,
				 RandomAccessIterator2d1 M1_bot,
				 RandomAccessIterator2d2 Res_up,
				 RandomAccessIterator2d2 Res_bot)
  { 
    assert((Res_bot-Res_up)[0] == (M1_bot-M1_up)[0]);
    assert((Res_bot-Res_up)[1] == (M1_bot-M1_up)[0]);
    
   
    typedef typename RandomAccessIterator2d1::size_type size_type;
    size_type rows = static_cast<size_type>((M1_bot-M1_up)[0]);

    typedef typename std::iterator_traits<RandomAccessIterator2d2>::value_type value_type;
    
    slip::DPoint2d<int> d(0,0);
    for(size_type i = 0; i < rows; ++i)
      {
	d[0] = i;
	for(size_type j = 0; j < rows; ++j)
	  {
	    d[1] = j;
	    Res_up[d] = 
	      std::inner_product(M1_up.row_begin(i),M1_up.row_end(i),
				 M1_up.row_begin(j),
				 value_type());
	  }
      }
  }


  /**
   ** \brief Computes the multiplication of A and its transposate AT
   ** \f$ Res = A \times A^T \f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2013/02/14
   ** \since 1.2.0
   ** \version 0.0.1
   ** \param M1 Matrix
   ** \param result result = AA^T
   ** \pre result.rows()==M1.rows()
   ** \pre result.cols()==M1.rows()
   ** \par Example:
   ** \code
   ** const std::size_t rows = 5;
   ** const std::size_t cols = 6;
   ** slip::Matrix<double> A(rows,cols);
   ** slip::iota(A.begin(),A.end(),1.0);
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Matrix<double> AAT(rows,rows);
   ** slip::matrix_matrixt_multiplies(A,AAT);
   ** std::cout<<"AAT = \n"<<AAT<<std::endl;
   ** \endcode
   */
  template <class Matrix1, 
	    class Matrix2>
  inline
  void matrix_matrixt_multiplies(const Matrix1& M1,
				 Matrix2& result)
  {
    slip::matrix_matrixt_multiplies(M1.upper_left(),M1.bottom_right(),
				    result.upper_left(),result.bottom_right());
  }

   /**
   ** \brief Computes \f[ R = A - \lambda I_n\f] with In the identity matrix.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/20
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M_upper_left RandomAccessIterator2d to the upper_left matrix element.
   ** \param M_bottom_right RandomAccessIterator2d to the bottom_right matrix element.
   ** \param lambda Scalar.
   ** \param R_upper_left RandomAccessIterator2d to the upper_left matrix element.
   ** \param R_bottom_right RandomAccessIterator2d to the bottom_right matrix element.
  
   **
   ** \pre (M_bottom_right - M_upper_left)[0] == (M_bottom_right - M_upper_left)[1]
   ** \pre (M_bottom_right - M_upper_left)[0] == (R_bottom_right - R_upper_left)[0]
   ** \pre (M_bottom_right - M_upper_left)[1] == (R_bottom_right - R_upper_left)[1]
     ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Mshift(4,4);
   ** slip::iota(Mshift.begin(),Mshift.end(),1.0);
   ** std::cout<<"Mshift = "<<std::endl;
   ** std::cout<<Mshift<<std::endl;
   ** slip::Matrix<double> Mshift2(4,4);
   ** slip::matrix_shift(Mshift.upper_left(),Mshift.bottom_right(),
   **	                 2.2,
   **	                 Mshift2.upper_left(),Mshift2.bottom_right());
   ** std::cout<<"Mshift - 2.2 I4= "<<std::endl;
   ** std::cout<<Mshift2<<std::endl;
   ** \endcode
  
   */  
  template <class RandomAccessIterator2d1,
	    class T,
	    class RandomAccessIterator2d2>
  inline
  void matrix_shift(RandomAccessIterator2d1 M_upper_left,
		    RandomAccessIterator2d1 M_bottom_right,
		    const T& lambda,
		    RandomAccessIterator2d2 R_upper_left,
		    RandomAccessIterator2d2 R_bottom_right)
  {
    assert((M_bottom_right - M_upper_left)[0] == (M_bottom_right - M_upper_left)[1]);
    assert((M_bottom_right - M_upper_left)[0] == (R_bottom_right - R_upper_left)[0]);
    assert((M_bottom_right - M_upper_left)[1] == (R_bottom_right - R_upper_left)[1]);
    typename RandomAccessIterator2d1::difference_type size2d_M = 
      M_bottom_right - M_upper_left;
    typename RandomAccessIterator2d2::difference_type size2d_R = 
      R_bottom_right - R_upper_left;
    typedef typename RandomAccessIterator2d1::size_type size_type;
    size_type M_rows = size2d_M[0];
 
   
    
    typedef typename std::iterator_traits<RandomAccessIterator2d2>::value_type value_type;
    
    std::copy(M_upper_left,M_bottom_right,R_upper_left);
    slip::DPoint2d<int> d(1,1);
    for(size_type i = 0; i < M_rows; ++i)
      {
	*R_upper_left = value_type(*M_upper_left) - value_type(lambda);
	R_upper_left += d;
	M_upper_left += d;
      }

  }

   /**
   ** \brief Computes \f[ A  = A - \lambda I_n\f] with In the identity
   ** matrix.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/20
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M_upper_left RandomAccessIterator2d to the upper_left matrix element.
   ** \param M_bottom_right RandomAccessIterator2d to the bottom_right matrix element.
   ** \param lambda Scalar.
   **
   ** \pre (M_bottom_right - M_upper_left)[0] == (M_bottom_right - M_upper_left)[1]
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Mshift(4,4);
   ** slip::iota(Mshift.begin(),Mshift.end(),1.0);
   ** std::cout<<"Mshift = "<<std::endl;
   ** std::cout<<Mshift<<std::endl;
   ** slip::Matrix<double> Mshift2(4,4);
   ** slip::matrix_shift(Mshift.upper_left(),Mshift.bottom_right(),
   **	                 2.2);
   ** std::cout<<"Mshift - 2.2 I4= "<<std::endl;
   ** std::cout<<Mshift2<<std::endl;
   ** \endcode
   */  
  template <class RandomAccessIterator2d1,
	    class T>
  inline
  void matrix_shift(RandomAccessIterator2d1 M_upper_left,
		    RandomAccessIterator2d1 M_bottom_right,
		    const T& lambda)
  { 
    typename RandomAccessIterator2d1::difference_type size2d_M = 
      M_bottom_right - M_upper_left;
    typedef typename RandomAccessIterator2d1::size_type size_type;
    size_type M_rows = size2d_M[0];
    size_type M_cols = size2d_M[1];
    assert(M_rows == M_cols);
    
    typedef typename std::iterator_traits<RandomAccessIterator2d1>::value_type value_type;
    
    slip::DPoint2d<int> d(1,1);
    for(size_type i = 0; i < M_rows; ++i)
      {
	*M_upper_left -= value_type(lambda);
	M_upper_left += d;
      }

  }

   /**
   ** \brief Computes \f[ R  = A - \lambda I_n\f] with In the identity
   ** matrix.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/20
   ** \version 0.0.1
   ** \since 1.0.0
   ** \param A The input matrix.
   ** \param lambda scalar.
   ** \param R output matrix.
   **
   ** \pre (M.rows() == M.cols())
   ** \pre (M.rows() == R.rows())
   ** \pre (M.cols() == R.cols())
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Mshift(4,4);
   ** slip::iota(Mshift.begin(),Mshift.end(),1.0);
   ** std::cout<<"Mshift = "<<std::endl;
   ** std::cout<<Mshift<<std::endl;
   ** slip::Matrix<double> Mshift2(4,4);
   ** slip::matrix_shift(Mshift,2.2,Mshift2);
   ** std::cout<<"Mshift - 2.2 I4= "<<std::endl;
   ** std::cout<<Mshift2<<std::endl;
   ** \endcode
   */  
  template <class Matrix1,
	    class T,
	    class Matrix2>
  inline
  void matrix_shift(const Matrix1& A,
		    const T& lambda,
		    Matrix2& R)
  {
    slip::matrix_shift(A.upper_left(),A.bottom_right(),
		       lambda,
		       R.upper_left(),R.bottom_right());
    
  }
  
   /**
   ** \brief Computes \f[ A  = A - \lambda I_n\f] with In the identity
   ** matrix.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/20
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A The input/output matrix.
   ** \param lambda scalar.
   **
   ** \pre (M.row() == M.cols())
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Mshift(4,4);
   ** slip::iota(Mshift.begin(),Mshift.end(),1.0);
   ** std::cout<<"Mshift = "<<std::endl;
   ** std::cout<<Mshift<<std::endl;
   ** slip::matrix_shift(Mshift,2.2);
   ** std::cout<<"Mshift - 2.2 I4= "<<std::endl;
   ** std::cout<<Mshift<<std::endl;
   ** \endcode
   */  
  template <class Matrix,
	    class T>
  inline
  void matrix_shift(Matrix& A,
		    const T& lambda)
  {
    slip::matrix_shift(A.upper_left(),A.bottom_right(),lambda);
    
  }

 
 /**
   ** \brief Computes \f[ A = A + \alpha X Y^H\f] 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up RandomAccessIterator2d to the upper_left matrix element.
   ** \param A_bot RandomAccessIterator2d to the bottom_right matrix element.
   ** \param alpha Scalar.
   ** \param X_first RandomAccessIterator to the first element of the vector X.
   ** \param X_last RandomAccessIterator to one-past-the-end element of the vector X.
   ** \param Y_first RandomAccessIterator to the first element of the vector Y.
   ** \param Y_last RandomAccessIterator to one-past-the-end element of the vector Y.
   ** \par Complexity: 3*A.cols()*A.rows() flops for real data 4*A.cols()*A.rows() flops for complex data
   ** \pre (A_bot - A_up)[0] == (X_last - X_first)
   ** \pre (A_bot - A_up)[1] == (Y_last - Y_first)
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code 
   ** typedef std::complex<double> TC;
   ** TC dc[] = {TC(1,1),TC(0,1),TC(0,1),TC(0,1),
   **	         TC(1,0),TC(1,1),TC(0,1),TC(0,1),
   **            TC(1,2),TC(1,2),TC(1,2),TC(1,2)};
   ** slip::Matrix<TC> Mrank1(4,3,dc);
   ** std::cout<<"Mrank1 = "<<std::endl;
   ** std::cout<<Mrank1<<std::endl;
   ** std::cout<<"Vout1 = "<<std::endl;
   ** std::cout<<Vout1<<std::endl;
   ** std::cout<<"Vout2 = "<<std::endl;
   ** std::cout<<Vout2<<std::endl;
   ** slip::rank1_update(Mrank1.upper_left(),Mrank1.bottom_right(),
   **	                 3.0,
   **                    Vout1.begin(),Vout1.end(),
   **	                 Vout2.begin(),Vout2.end());
   ** std::cout<<"Mrank1 = Mrank1 + 3xXxY^*"<<std::endl;
   ** std::cout<<Mrank1<<std::endl;
   ** \endcode
   */  
  template <typename RandomAccessIterator2d1,
	    typename T,
	    typename RandomAccessIterator1,
	    typename RandomAccessIterator2>
  inline
  void rank1_update(RandomAccessIterator2d1 A_up,
		    RandomAccessIterator2d1 A_bot,
		    const T& alpha,
		    RandomAccessIterator1 X_first,
		    RandomAccessIterator1 X_last,
		    RandomAccessIterator2 Y_first,
		    RandomAccessIterator2 Y_last)
  {
   
    
    assert((A_bot - A_up)[0] == (X_last - X_first));
    assert((A_bot - A_up)[1] == (Y_last - Y_first));

    typename RandomAccessIterator2d1::difference_type size2d_A = 
      A_bot - A_up;
    typedef typename RandomAccessIterator2d1::size_type size_type;
    size_type A_rows = size2d_A[0];
   
    typedef typename std::iterator_traits<RandomAccessIterator1>::difference_type _Difference;
    _Difference y_size = Y_last - Y_first;
   
    
    typedef typename std::iterator_traits<RandomAccessIterator2d1>::value_type value_type;

   

    slip::Array<value_type> tmp((std::size_t)(y_size));
    //computes tmp = Y^HA
     for(size_type i = 0; i < A_rows; ++i, ++X_first)
      {
	slip::conj_vector_scalar_multiplies(Y_first,Y_last,
					    *X_first,
					    tmp.begin());
	slip::aXpY(alpha,tmp.begin(),tmp.end(),A_up.row_begin(i));
      }
  }


  /**
   ** \brief Computes \f[ A = A + \alpha X Y^H\f] 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix A.
   ** \param alpha Scalar.
   ** \param X The vector X.
   ** \param Y The vector Y.
   ** \par Complexity: 3*A.cols()*A.rows() flops for real data 4*A.cols()*A.rows() flops for complex data
   ** \pre A.rows() == X.size()
   ** \pre A.cols() == Y.size()
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code 
   ** typedef std::complex<double> TC;
   ** TC dc[] = {TC(1,1),TC(0,1),TC(0,1),TC(0,1),
   **	         TC(1,0),TC(1,1),TC(0,1),TC(0,1),
   **            TC(1,2),TC(1,2),TC(1,2),TC(1,2)};
   ** slip::Matrix<TC> Mrank1(4,3,dc);
   ** std::cout<<"Mrank1 = "<<std::endl;
   ** std::cout<<Mrank1<<std::endl;
   ** std::cout<<"Vout1 = "<<std::endl;
   ** std::cout<<Vout1<<std::endl;
   ** std::cout<<"Vout2 = "<<std::endl;
   ** std::cout<<Vout2<<std::endl;
   ** slip::rank1_update(Mrank1,3.0,Vout1,Vout2);
   ** std::cout<<"Mrank1 = Mrank1 + 3xXxY^*"<<std::endl;
   ** std::cout<<Mrank1<<std::endl;
   ** \endcode
   */  
  template <typename Matrix,
	    typename T,
	    typename Vector1,
	    typename Vector2>
  inline
  void rank1_update(Matrix& A,
		    const T& alpha,
		    const Vector1& X,
		    const Vector2& Y)
  {
    slip::rank1_update(A.upper_left(),A.bottom_right(),
		       alpha,
		       X.begin(),X.end(),
		       Y.begin(),Y.end());
  }

 /**
   ** \brief Add u times the second sequence to the first one: 
   **  seq1 = seq1 + u * seq2
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/08/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param seq1_beg iterator to the beginning of the first sequence
   ** \param seq1_end iterator to the end of the first sequence
   ** \param seq2_beg iterator to the beginning of the second sequence
   ** \param u reduction constant
   **
   ** \pre seq1 and seq2 must have the same size
   */  
  template <typename Iterator1, typename Iterator2, typename U>
  inline
  void reduction(Iterator1 seq1_beg, Iterator1 seq1_end, 
		 Iterator2 seq2_beg, U u)
  {
    typedef typename std::iterator_traits<Iterator1>::value_type _T1;
    typedef typename std::iterator_traits<Iterator2>::value_type _T2;
    std::transform(seq2_beg,seq2_beg + (seq1_end - seq1_beg),
		   seq1_beg,
		   seq1_beg,
		   slip::saxpy<_T2,_T1,_T1,U>(u));
  }

 /**
   ** \brief Computes the hermitian transpose of a matrix
   ** \f$ \overline{M}^{T} \f$ which is the transpose of the conjugate 
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/06/24
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M Matrix container
   ** \param TM result of the transposition
   **
   ** \pre M, TM must have the double bracket element accessor 
   ** \pre Matrix are supposed to be allocated first
   ** \pre M.rows() == TM.cols()
   ** \pre M.cols() == TM.rows()
   */  
  template <class Matrix1, class Matrix2>
  inline
  void hermitian_transpose(const Matrix1 & M, Matrix2 & TM)
  {
    assert(M.rows() == TM.cols());
    assert(M.cols() == TM.rows());
   
    const std::size_t M_rows = M.rows();
    const std::size_t M_cols = M.cols();
    for (std::size_t i = 0; i < M_rows; ++i)
      {
	for (std::size_t j = 0; j < M_cols; ++j)
	  {
	    TM[j][i] = slip::conj(M[i][j]);
	  }
      }
   
  }

 

  /**
   ** \brief Computes the transpose of a matrix \f$M^T\f$.
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/14
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M Matrix container
   ** \param nr1 The number of rows of M
   ** \param nc1 The number of columns of M
   ** \param TM result of the transposition
   ** \param nr2 The number of rows of TM
   ** \param nc2 The number of columns of TM
   **
   ** \pre M, TM must have the double bracket element accessor 
   ** \pre Matrix are supposed to be allocated first
   ** \pre nr1 == nc2
   ** \pre nc1 == nr2
   ** \internal
   */  
  template <class Matrix1, class Matrix2>
  inline
  void transpose(const Matrix1 & M, 
		 const std::size_t nr1,
		 const std::size_t nc1,
		 Matrix2 & TM,
		 const std::size_t nr2,
		 const std::size_t nc2)
  {
	assert(nr1==nc2);
        assert(nc1==nr2);

	for (std::size_t i = 0; i < nr1; ++i)
	  {
	    for (std::size_t j = 0; j < nc1; ++j)
	      {
		TM[j][i] = M[i][j];
	      }
	  }

  }

  /**
   ** \brief Computes the transpose of a matrix \f$M^T\f$.
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/14
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M Matrix container
   ** \param TM result of the transposition
   **
   ** \pre M, TM must have cols() and rows() methods
   ** \pre M, TM must have the double bracket element accessor 
   ** \pre Matrix are supposed to be allocated first
   ** \pre Matrix shoud have the same data types.
   ** \pre M.cols() == TM.rows()
   ** \pre M.rows() == TM.cols()
   ** \remarks Works with real and complex matrix.
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0,1.0);
   ** slip::Array2d<float> M2(4,3);
   ** slip::transpose(M,M2);
   ** \endcode
   */  
  template <class Matrix1, class Matrix2>
  inline
  void transpose(const Matrix1 & M, Matrix2 & TM)
  {
   
    slip::transpose(M,M.rows(),M.cols(),TM,TM.rows(),TM.cols());
  }


   /**
   ** \brief Computes the conjugate of a matrix
   ** \f$ CM = \overline{M} \f$ 
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/06/24
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M Matrix container
   ** \param CM result of the conjugate
   **
   ** \pre M, CM must have the double bracket element accessor 
   ** \pre Matrix are supposed to be allocated first
   ** \pre M.rows() == CM.rows()
   ** \pre M.cols() == CM.cols()
   ** \par Example:
   ** \code
   ** slip::Matrix<std::complex<double> > Sshc(3,3);
   ** Sshc(0,0) = std::complex<double>(0.0,1.0);
   ** Sshc(0,1) = std::complex<double>(0.5,-0.5);
   ** Sshc(0,2) = std::complex<double>(1.0,-1.2);
   ** Sshc(1,0) = std::complex<double>(-0.5,-0.5);  
   ** Sshc(1,1) = std::complex<double>(0.0,2.0);
   ** Sshc(1,2) = std::complex<double>(0.2,-0.3);
   ** Sshc(2,0) = std::complex<double>(-1.0,-1.2);  
   ** Sshc(2,1) = std::complex<double>(-0.2,-0.3);
   ** Sshc(2,2) = std::complex<double>(0.0,3.0);
   ** std::cout<<"Sshc = \n"<<Sshc<<std::endl;
   ** slip::Matrix<std::complex<double> > conjSshc(3,3);
   ** slip::conj(Sshc,conjSshc);
   ** std::cout<<"conjSshc = \n"<<conjSshc<<std::endl;
   ** \endcode
   */  
  template <class Matrix1, class Matrix2>
  inline
  void conj(const Matrix1 & M, Matrix2 & CM)
  {
    assert(M.rows() == CM.rows());
    assert(M.cols() == CM.cols());
    slip::apply(M.begin(),M.end(),CM.begin(),slip::conj);
  }

   /**
   ** \brief Extract the real Matrix of a complex Matrix.
   ** \f$ R_{ij} = \mathcal{R}(C_{ij}) \f$ 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/06
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param C Complex Matrix container
   ** \param R result of the real
   **
   ** \pre Matrix are supposed to be allocated first
   ** \pre R.rows() == C.rows()
   ** \pre R.cols() == C.cols()
   ** \par Example:
   ** \code
   ** slip::Matrix<std::complex<double> > Sshc(3,3);
   ** Sshc(0,0) = std::complex<double>(0.0,1.0);
   ** Sshc(0,1) = std::complex<double>(0.5,-0.5);
   ** Sshc(0,2) = std::complex<double>(1.0,-1.2);
   ** Sshc(1,0) = std::complex<double>(-0.5,-0.5);  
   ** Sshc(1,1) = std::complex<double>(0.0,2.0);
   ** Sshc(1,2) = std::complex<double>(0.2,-0.3);
   ** Sshc(2,0) = std::complex<double>(-1.0,-1.2);  
   ** Sshc(2,1) = std::complex<double>(-0.2,-0.3);
   ** Sshc(2,2) = std::complex<double>(0.0,3.0);
   ** std::cout<<"Sshc = \n"<<Sshc<<std::endl;
   ** slip::Matrix<double> realSshc(3,3);
   ** slip::real(Sshc,realSshc);
   ** std::cout<<"realSshc = \n"<<realSshc<<std::endl;
   ** \endcode
   */  
  template <class Matrix1, class Matrix2>
  inline
  void real(const Matrix1 & C, Matrix2 & R)
  {
    assert(R.rows() == C.rows());
    assert(R.cols() == C.cols());
    slip::apply(C.begin(),C.end(),R.begin(),std::real);
  }


   /**
   ** \brief Extract the imaginary Matrix of a complex Matrix.
   ** \f$ I_{ij} = \mathcal{I}(C_{ij}) \f$ 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/06
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param C Complex Matrix container
   ** \param I result of the imag
   **
   ** \pre Matrix are supposed to be allocated first
   ** \pre I.rows() == C.rows()
   ** \pre I.cols() == C.cols()
    ** \par Example:
   ** \code
   ** slip::Matrix<std::complex<double> > Sshc(3,3);
   ** Sshc(0,0) = std::complex<double>(0.0,1.0);
   ** Sshc(0,1) = std::complex<double>(0.5,-0.5);
   ** Sshc(0,2) = std::complex<double>(1.0,-1.2);
   ** Sshc(1,0) = std::complex<double>(-0.5,-0.5);  
   ** Sshc(1,1) = std::complex<double>(0.0,2.0);
   ** Sshc(1,2) = std::complex<double>(0.2,-0.3);
   ** Sshc(2,0) = std::complex<double>(-1.0,-1.2);  
   ** Sshc(2,1) = std::complex<double>(-0.2,-0.3);
   ** Sshc(2,2) = std::complex<double>(0.0,3.0);
   ** std::cout<<"Sshc = \n"<<Sshc<<std::endl;
   ** slip::Matrix<double> imagSshc(3,3);
   ** slip::imag(Sshc,realSshc);
   ** std::cout<<"imagSshc = \n"<<imagSshc<<std::endl;
   ** \endcode
   */  
  template <class Matrix1, class Matrix2>
  inline
  void imag(const Matrix1 & C, Matrix2 & I)
  {
    assert(I.rows() == C.rows());
    assert(I.cols() == C.cols());
    slip::apply(C.begin(),C.end(),I.begin(),std::imag);
  }

 
 


   /**
   ** \brief Computes the hermitian outer product of the vector V and
   **  the transpose of the conjugate of the vector W: 
   ** \f[ R = V \times W^{H} \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param V_begin iterator on the first element of V
   ** \param V_end iterator on the last element of V
   ** \param W_begin iterator on the first element of W
   ** \param W_end iterator on the last element of W
   ** \param R_up 2d iterator on the upper_left element of the result matrix 
   ** \param R_bot 2d iterator on the bottom_right element of the result matrix 
   **
   ** \pre Matrix and Vector are supposed to be allocated first
   ** \pre R.rows() == V.size()
   ** \pre R.cols() == W.size()
   ** \par Example:
   ** \code
   ** slip::Vector<std::complex<double> > Vout1(4);
   ** slip::iota(Vout1.begin(),Vout1.end(),
   ** std::complex<double>(1.0,1.0),
   ** std::complex<double>(1.0,1.0));
   ** std::cout<<"Vout1 = "<<std::endl;
   ** std::cout<<Vout1<<std::endl;
   ** slip::Vector<std::complex<double> > Vout2(3);
   ** slip::iota(Vout2.begin(),Vout2.end(),
   ** std::complex<double>(2.0,1.0),
   ** std::complex<double>(1.0,1.0));
   ** std::cout<<"Vout2 = "<<std::endl;
   ** std::cout<<Vout2<<std::endl;
   ** slip::Matrix<std::complex<double> > Aout(Vout1.size(),Vout2.size());
   ** slip::outer_product(Vout1.begin(),Vout1.end(),
   **	                  Vout2.begin(),Vout2.end(),
   ** 		          Aout.upper_left(),Aout.bottom_right());
   ** std::cout<<"Aout = "<<std::endl;
   ** std::cout<<Aout<<std::endl;
   ** \endcode
   */  
  template <typename VectorIterator1, 
	    typename VectorIterator2, 
	    typename MatrixIterator>
  inline
  void outer_product(VectorIterator1 V_begin, VectorIterator1 V_end, 
		     VectorIterator2 W_begin, VectorIterator2 W_end, 
		     MatrixIterator R_up, MatrixIterator R_bot)
  {
    
    typename MatrixIterator::difference_type sizeR = R_bot - R_up;
    std::size_t R_rows = sizeR[0];
    
    assert(sizeR[0] == (V_end - V_begin));
    assert(sizeR[1] == (W_end - W_begin));
    
    for(std::size_t i = 0; i < R_rows; ++i, ++V_begin)
      {
	slip::conj_vector_scalar_multiplies(W_begin,W_end,
					    *V_begin,
					    R_up.row_begin(i));
      }
  }
  
 /**
   ** \brief Computes the tensorial product of two rank one tensors
   ** \f$ R = V \times W^{T} \f$
   ** it provides a rank2 tensor (Container2d) of size (MxN) if M and N are respectivly the size of V and W.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2010/11/07
   ** \since 1.4.0
   ** \version 0.0.1
   ** \param base1_first iterator on the first element of the first rank one tensor
   ** \param base1_end iterator on one-past-the-end element of the first rank one tensor
   ** \param base2_first iterator on the first element of the second rank one tensor
   ** \param base2_end iterator on one-past-the-end element of the second rank one tensor
   ** \param matrix_upper_left 2d iterator on the first element of the rank two tensor
   ** \param matrix_bottom_right 2d iterator on the one-past-the-end element of the rank two tensor
   ** \pre Containers are supposed to be allocated first
   ** \pre (matrix_bottom_right - matrix_upper_left)[0] == (base1_end - base1_first)
   ** \pre (matrix_bottom_right - matrix_upper_left)[1] == (base2_end - base2_first)
   ** \par Example1:
   ** \code
   ** slip::Vector<double> Vbase1(4);
   ** slip::iota(Vbase1.begin(),Vbase1.end(),1.0);
   ** std::cout<<"Vbase1 = \n"<<Vbase1<<std::endl;
   ** slip::Vector<double> Vbase2(5);
   ** slip::iota(Vbase2.begin(),Vbase2.end(),2.0);
   ** std::cout<<"Vbase2 = \n"<<Vbase2<<std::endl;
   **
   ** slip::Matrix<double> Tpbase1base2(Vbase1.size(),Vbase2.size());
   ** slip::rank1_tensorial_product(Vbase1.begin(),Vbase1.end(),
   **	  			    Vbase2.begin(),Vbase2.end(),
   **				    Tpbase1base2.upper_left(),Tpbase1base2.bottom_right());
   ** std::cout<<"Tpbase1base2 = \n"<<Tpbase1base2<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** //generate x1 2d chebyshev polynomial base
   ** slip::MultivariatePolynomial<double,2> p0x1;
   ** slip::Monomial<2> m01x1;
   ** m01x1.powers[0] = 0;
   ** m01x1.powers[1] = 0;
   ** p0x1.insert(m01x1,1.0);
   **
   ** slip::MultivariatePolynomial<double,2> p1x1;
   ** slip::Monomial<2> m11x1;
   ** m11x1.powers[0] = 1;
   ** m11x1.powers[1] = 0;
   ** p1x1.insert(m11x1,1.0);
   **
   ** std::vector<slip::MultivariatePolynomial<double,2> > chebyshev_basis_2d_x1;
   ** chebyshev_basis_2d_x1.push_back(p0x1);
   ** chebyshev_basis_2d_x1.push_back(p1x1);
   ** for(unsigned l = 1; l < 3; ++l)
   ** {
   **	chebyshev_basis_2d_x1.push_back(slip::chebyshev_nd_next<slip::MultivariatePolynomial<double,2> >(l, p1x1, chebyshev_basis_2d_x1[l], chebyshev_basis_2d_x1[l-1]));
   **   }
   ** //print base
   ** for(unsigned l = 0; l < chebyshev_basis_2d_x1.size(); ++l)
   ** {
   **   std::cout<<chebyshev_basis_2d_x1[l]<<std::endl;
   ** }
   ** 
   ** //Generate 2d x2 Chebyshev basis 
   ** slip::MultivariatePolynomial<double,2> p0x2;
   ** slip::Monomial<2> m01x2;
   ** m01x2.powers[0] = 0;
   ** m01x2.powers[1] = 0;
   ** p0x2.insert(m01x2,1.0);
   ** slip::MultivariatePolynomial<double,2> p1x2;
   ** slip::Monomial<2> m11x2;
   ** m11x2.powers[0] = 0;
   ** m11x2.powers[1] = 1;
   ** p1x2.insert(m11x2,1.0);
   ** std::vector<slip::MultivariatePolynomial<double,2> > chebyshev_basis_2d_x2;
   ** chebyshev_basis_2d_x2.push_back(p0x2);
   ** chebyshev_basis_2d_x2.push_back(p1x2);
   ** for(unsigned l = 1; l < 3; ++l)
   ** {
   **	chebyshev_basis_2d_x2.push_back(slip::chebyshev_nd_next<slip::MultivariatePolynomial<double,2> >(l, p1x2, chebyshev_basis_2d_x2[l], chebyshev_basis_2d_x2[l-1]));
   ** }
   ** 
   ** for(unsigned l = 0; l < chebyshev_basis_2d_x2.size(); ++l)
   ** {
   **  std::cout<<chebyshev_basis_2d_x2[l]<<std::endl;
   ** }
   ** 
   ** 
   ** slip::Matrix<slip::MultivariatePolynomial<double,2> > Chebyshev2dBasis(chebyshev_basis_2d_x1.size(),chebyshev_basis_2d_x1.size());
   ** 
   **
   ** slip:rank1_tensorial_product(chebyshev_basis_2d_x2.begin(),
   **                              chebyshev_basis_2d_x2.end(),
   **			           chebyshev_basis_2d_x1.begin(),
   **			           chebyshev_basis_2d_x1.end(),
   **			           Chebyshev2dBasis.upper_left(),
   **			           Chebyshev2dBasis.bottom_right());
   **
   ** std::cout<<"Chebyshev2dBasis \n"<<Chebyshev2dBasis<<std::endl;
   ** \endcode
   */  
template<class RandomAccessIterator1, 
	   class RandomAccessIterator2,
	   class RandomAccessIterator2d>
  void rank1_tensorial_product(RandomAccessIterator1 base1_first,
			       RandomAccessIterator1 base1_end,
			       RandomAccessIterator2 base2_first,
			       RandomAccessIterator2 base2_end,
			       RandomAccessIterator2d matrix_upper_left,
			       RandomAccessIterator2d matrix_bottom_right)
{
  assert((matrix_bottom_right - matrix_upper_left)[0] == (base1_end - base1_first));
  assert((matrix_bottom_right - matrix_upper_left)[1] == (base2_end - base2_first));
 
  typedef typename std::iterator_traits<RandomAccessIterator1>::difference_type _Distance1;
 typedef typename std::iterator_traits<RandomAccessIterator2>::difference_type _Distance2;

  typedef typename std::iterator_traits<RandomAccessIterator2d>::difference_type _Distance2d;

  _Distance1 base1_first_size = (base1_end - base1_first);  
  _Distance2 base2_first_size = (base2_end - base2_first);  
 
  _Distance2d matrix_size2d = (matrix_bottom_right - matrix_upper_left);
  
  for(_Distance2 i = 0 ; i < base1_first_size; ++i)
    {
      for(_Distance1 j = 0 ; j < base2_first_size; ++j)
	{
	  *(matrix_upper_left++) = base1_first[i] * base2_first[j];
	}
    }

}


 /**
   ** \brief Computes the tensorial product of three rank one tensors
   ** it provides a rank3 tensor (Container3d) of size (PxMxN) if P, M and N are respectivly the size of V, W and X.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2010/11/07
   ** \since 1.4.0
   ** \version 0.0.1
   ** \param base1_first iterator on the first element of the first rank one tensor
   ** \param base1_end iterator on one-past-the-end element of the first rank one tensor
   ** \param base2_first iterator on the first element of the second rank one tensor
   ** \param base2_end iterator on one-past-the-end element of the second rank one tensor
   ** \param base3_first iterator on the first element of the third rank one tensor
   ** \param base3_end iterator on one-past-the-end element of the third rank one tensor
   ** \param base_front_upper_left 3d iterator on the first element of the rank three tensor
   ** \param base_back_bottom_right 3d iterator on the one-past-the-end element of the rank three tensor
   ** \pre Containers are supposed to be allocated first
   ** \pre (base_back_bottom_right - base_front_upper_left)[0] == (base1_end - base1_first)
   ** \pre (base_back_bottom_right - base_front_upper_left)[1] == (base2_end - base2_first)
   ** \pre (base_back_bottom_right - base_front_upper_left)[2] == (base3_end - base3_first)
   ** \par Example1:
   ** \code
   ** slip::Vector<double> Vbase12(4);
   ** slip::iota(Vbase12.begin(),Vbase12.end(),1.0);
   ** std::cout<<"Vbase12 = \n"<<Vbase12<<std::endl;
   ** slip::Vector<double> Vbase22(5);
   ** slip::iota(Vbase22.begin(),Vbase22.end(),2.0);
   ** std::cout<<"Vbase22 = \n"<<Vbase22<<std::endl;
   ** slip::Vector<double> Vbase32(3);
   ** slip::iota(Vbase32.begin(),Vbase32.end(),3.0);
   ** std::cout<<"Vbase32 = \n"<<Vbase32<<std::endl;

   ** slip::Matrix3d<double> Tpbase1base2base3(Vbase12.size(),Vbase22.size(),Vbase32.size());
   ** slip::rank1_tensorial_product(Vbase12.begin(),Vbase12.end(),
   **				    Vbase22.begin(),Vbase22.end(),
   **				    Vbase32.begin(),Vbase32.end(),
   **				 
   **				 Tpbase1base2base3.front_upper_left(),Tpbase1base2base3.back_bottom_right());
   ** std::cout<<"Tpbase1base2base3 = \n"<<Tpbase1base2base3<<std::endl;
   ** \endcode
   ** \par Example2:
   ** \code
   ** std::cout << "Generate 3d Chebyshev basis " << std::endl;
   ** std::cout << "3d x1 polynomial "<< std::endl;
   ** slip::MultivariatePolynomial<double,3> p0x1_3d;
   ** slip::Monomial<3> m01x1_3d;
   ** m01x1_3d.powers[0] = 0;
   ** m01x1_3d.powers[1] = 0;
   ** m01x1_3d.powers[2] = 0;
   ** p0x1_3d.insert(m01x1_3d,1.0);
   **
   ** slip::MultivariatePolynomial<double,3> p1x1_3d;
   ** slip::Monomial<3> m11x1_3d;
   ** m11x1_3d.powers[0] = 1;
   ** m11x1_3d.powers[1] = 0;
   ** m11x1_3d.powers[2] = 0;
   ** p1x1_3d.insert(m11x1_3d,1.0);
   ** std::vector<slip::MultivariatePolynomial<double,3> > chebyshev_basis_3d_x1;
   ** chebyshev_basis_3d_x1.push_back(p0x1_3d);
   ** chebyshev_basis_3d_x1.push_back(p1x1_3d);
   ** for(unsigned l = 1; l < 3; ++l)
   **   {
   **	chebyshev_basis_3d_x1.push_back(slip::chebyshev_nd_next<slip::MultivariatePolynomial<double,3> >(l, p1x1_3d, chebyshev_basis_3d_x1[l], chebyshev_basis_3d_x1[l-1]));
   **   }
   **
   **  for(unsigned l = 0; l < chebyshev_basis_3d_x1.size(); ++l)
   **   {
   **	std::cout<<chebyshev_basis_3d_x1[l]<<std::endl;
   **   }
   ** std::cout << std::setw(50) << '_' << std::endl;
   ** std::cout << "3d x2 polynomial "<< std::endl;
   ** std::cout << std::setw(50) << '_' << std::endl;
   ** slip::MultivariatePolynomial<double,3> p0x2_3d;
   ** slip::Monomial<3> m01x2_3d;
   ** m01x2_3d.powers[0] = 0;
   ** m01x2_3d.powers[1] = 0;
   ** m01x2_3d.powers[2] = 0;
   ** p0x2_3d.insert(m01x2_3d,1.0);
   **
   ** slip::MultivariatePolynomial<double,3> p1x2_3d;
   ** slip::Monomial<3> m11x2_3d;
   ** m11x2_3d.powers[0] = 0;
   ** m11x2_3d.powers[1] = 1;
   ** m11x2_3d.powers[2] = 0;
   ** p1x2_3d.insert(m11x2_3d,1.0);
   ** std::vector<slip::MultivariatePolynomial<double,3> > chebyshev_basis_3d_x2;
   ** chebyshev_basis_3d_x2.push_back(p0x2_3d);
   ** chebyshev_basis_3d_x2.push_back(p1x2_3d);
   ** for(unsigned l = 1; l < 3; ++l)
   **   {
   **	chebyshev_basis_3d_x2.push_back(slip::chebyshev_nd_next<slip::MultivariatePolynomial<double,3> >(l, p1x2_3d, chebyshev_basis_3d_x2[l], chebyshev_basis_3d_x2[l-1]));
   **   }
   **
   ** for(unsigned l = 0; l < chebyshev_basis_3d_x2.size(); ++l)
   **   {
   **     std::cout<<chebyshev_basis_3d_x2[l]<<std::endl;
   **   }
   ** std::cout << std::setw(50) << '_' << std::endl;
   ** std::cout << "3d x1, x2 and x3 polynomial tensorial product"<< std::endl;
   ** std::cout << std::setw(50) << '_' << std::endl;
   ** 
   ** std::cout << std::setw(50) << '_' << std::endl;
   ** std::cout << "3d x3 polynomial "<< std::endl;
   ** std::cout << std::setw(50) << '_' << std::endl;
   ** slip::MultivariatePolynomial<double,3> p0x3_3d;
   ** slip::Monomial<3> m01x3_3d;
   ** m01x3_3d.powers[0] = 0;
   ** m01x3_3d.powers[1] = 0;
   ** m01x3_3d.powers[2] = 0;
   ** p0x3_3d.insert(m01x3_3d,1.0);
   **
   ** slip::MultivariatePolynomial<double,3> p1x3_3d;
   ** slip::Monomial<3> m11x3_3d;
   ** m11x3_3d.powers[0] = 0;
   ** m11x3_3d.powers[1] = 0;
   ** m11x3_3d.powers[2] = 1;
   ** p1x3_3d.insert(m11x3_3d,1.0);
   ** std::vector<slip::MultivariatePolynomial<double,3> > chebyshev_basis_3d_x3;
   ** chebyshev_basis_3d_x3.push_back(p0x3_3d);
   ** chebyshev_basis_3d_x3.push_back(p1x3_3d);
   ** for(unsigned l = 1; l < 3; ++l)
   **   {
   **	chebyshev_basis_3d_x3.push_back(slip::chebyshev_nd_next<slip::MultivariatePolynomial<double,3> >(l, p1x3_3d, chebyshev_basis_3d_x3[l], chebyshev_basis_3d_x3[l-1]));
   **   }
   **
   ** for(unsigned l = 0; l < chebyshev_basis_3d_x3.size(); ++l)
   **   {
   **   std::cout<<chebyshev_basis_3d_x3[l]<<std::endl;
   **   }
   **     
   ** slip::Matrix3d<slip::MultivariatePolynomial<double,3> > Chebyshev3dBasis(chebyshev_basis_3d_x3.size(),chebyshev_basis_3d_x2.size(),chebyshev_basis_3d_x1.size());
   ** 
   ** slip::rank1_tensorial_product(chebyshev_basis_3d_x3.begin(),
   **			            chebyshev_basis_3d_x3.end(),
   **			            chebyshev_basis_3d_x2.begin(),
   **			            chebyshev_basis_3d_x2.end(),
   **			            chebyshev_basis_3d_x1.begin(),
   **			            chebyshev_basis_3d_x1.end(),
   **			            Chebyshev3dBasis.front_upper_left(),
   **			            Chebyshev3dBasis.back_bottom_right());
   ** std::cout<<"Chebyshev3dBasis \n"<<Chebyshev3dBasis<<std::endl;
   **
   ** \endcode
   */  
template<class RandomAccessIterator1, 
	   class RandomAccessIterator2,
	   class RandomAccessIterator3,
	   class RandomAccessIterator3d>
  void rank1_tensorial_product(RandomAccessIterator1 base1_first,
			       RandomAccessIterator1 base1_end,
			       RandomAccessIterator2 base2_first,
			       RandomAccessIterator2 base2_end,
			       RandomAccessIterator3 base3_first,
			       RandomAccessIterator3 base3_end,
			       RandomAccessIterator3d base_front_upper_left,
			       RandomAccessIterator3d base_back_bottom_right)
{
  assert((base_back_bottom_right - base_front_upper_left)[0] == (base1_end - base1_first));
  assert((base_back_bottom_right - base_front_upper_left)[1] == (base2_end - base2_first));
  assert((base_back_bottom_right - base_front_upper_left)[2] == (base3_end - base3_first));
  
  typedef typename std::iterator_traits<RandomAccessIterator1>::difference_type _Distance1;
  typedef typename std::iterator_traits<RandomAccessIterator2>::difference_type _Distance2;
  typedef typename std::iterator_traits<RandomAccessIterator3>::difference_type _Distance3;

  typedef typename std::iterator_traits<RandomAccessIterator3d>::difference_type _Distance3d;

  _Distance1 base1_first_size = (base1_end - base1_first);  
  _Distance2 base2_first_size = (base2_end - base2_first); 
  _Distance2 base3_first_size = (base3_end - base3_first); 
 
  _Distance3d base_size3d = (base_back_bottom_right - base_front_upper_left);
  for(_Distance3 k = 0 ; k < base1_first_size; ++k)
    {
      for(_Distance2 i = 0 ; i < base2_first_size; ++i)
	{
	  for(_Distance1 j = 0 ; j < base3_first_size; ++j)
	    {
	      *(base_front_upper_left++) = base1_first[k]*base2_first[i] * base3_first[j];
	    }
	}
    }

}


  /* @} */

  

  /** \name Marix norms */
  /* @{ */

/**
 ** \brief Computes the L22 norm \f$ \sum_i \bar{z_i}z_i\f$ of a complex container.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2009/02/28
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param first An InputIterator.
 ** \param last  An InputIterator.
 ** \return a real value corresponding to the L22 norm.
 ** \pre [first, last) is a valid range
 ** \par Example:
 ** \code
 ** slip::Array2d<std::complex<float> > M(2,3);
 ** slip::iota(M.begin(),M.end(),std::complex<float>(1.0,1.0),
 **            std::complex<float>(1.0,0.5));
 ** std::cout<<M<<std::endl;
 ** std::cout<<slip::L22_norm_cplx(M.begin(),M.end())<<std::endl
 ** \endcode
 **
 */
  template<typename InputIterator>
  inline
  typename slip::lin_alg_traits<typename std::iterator_traits<InputIterator>::value_type>::value_type
  L22_norm_cplx(InputIterator first,
		InputIterator last)
    
{
  typedef typename std::iterator_traits<InputIterator>::value_type _T; 
  _T __init = _T();
    for (; first != last; ++first)
     {
       __init +=slip::conj(*first)*(*first);
     }
    return std::real(__init);
}


/**
   ** \brief Computes the row norm (\f$\max_i\sum_j |a_{ij}|\f$)  of a 2d range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param upper_left RandomAccessIterator2d: first element of the 2d range.
   ** \param bottom_right RandomAccessIterator2d: one past-the-end of the 2d range.
   ** \return The row norm of the 2d range.
   ** \pre bottom_right - upper_left != slip::Dpoint2d<int>(0,0)
   ** \remarks It corresponds to the infinite norm of a Matrix.
   ** \remarks Works with real and complex matrix.
   ** \par Complexity: rows*cols flops
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** std::cout<<"row norm of M = "<<slip::row_norm(M.upper_left(),M.bottom_right())<<std::endl;
   ** \endcode
   */  
  template <typename RandomAccessIterator2d>
  inline
  typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator2d>::value_type>::value_type row_norm(RandomAccessIterator2d upper_left, RandomAccessIterator2d bottom_right)
  {
    assert(((bottom_right - upper_left)[0] != 0) && ((bottom_right - upper_left)[1] != 0));
   
    typedef  typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator2d>::value_type>::value_type value_type;
    typedef typename RandomAccessIterator2d::size_type size_type;
    size_type rows = (bottom_right - upper_left)[0];
    value_type max = slip::L1_norm<value_type>(upper_left.row_begin(0),
					       upper_left.row_end(0));

    for(std::size_t i = 1; i < rows; ++i)
      {
	value_type tmp = slip::L1_norm<value_type>(upper_left.row_begin(i),
						   upper_left.row_end(i));
	if(tmp > max)
	  {
	    max = tmp;
	  }
      }
    return max;
  }
  


 /**
   ** \brief Computes the row norm (\f$\max_i\sum_j |a_{ij}|\f$)  of a Container2d.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param container Container2d.
   ** \return The row norm of the 2d container.
   ** \pre container.size() != 0
   ** \remarks It corresponds to the infinite norm of a Matrix.
   ** \par Complexity: rows*cols flops
   ** \remarks Works with real and complex matrix.
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** std::cout<<"row norm of M = "<<slip::row_norm(M)<<std::endl;
   ** \endcode
   */  
  template <typename Container2d>
  inline
  typename slip::lin_alg_traits<typename Container2d::value_type>::value_type row_norm(const Container2d& container)
  {
    return slip::row_norm(container.upper_left(),container.bottom_right());
  }


/**
   ** \brief Computes the column norm  (\f$\max_i\sum_j |a_{ij}|\f$) of a 2d range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param upper_left RandomAccessIterator2d: first element of the 2d range.
   ** \param bottom_right RandomAccessIterator2d: one past-the-end of the 2d range.
   ** \return The column norm of the 2d range.
   ** \pre bottom_right - upper_left != slip::Dpoint2d<int>(0,0)
   ** \remarks It corresponds to the infinite norm of a Matrix.
   ** \remarks Works with real and complex matrix.
   ** \par Complexity: rows*cols flops
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** std::cout<<"col norm of M = "<<slip::col_norm(M.upper_left(),M.bottom_right())<<std::endl;
   ** \endcode
   */  
  template <typename RandomAccessIterator2d>
  inline
  typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator2d>::value_type>::value_type col_norm(RandomAccessIterator2d upper_left, RandomAccessIterator2d bottom_right)
  {
    assert(((bottom_right - upper_left)[0] != 0) && ((bottom_right - upper_left)[1] != 0));
   
    typedef  typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator2d>::value_type>::value_type value_type;
    typedef typename RandomAccessIterator2d::size_type size_type;
    size_type cols = (bottom_right - upper_left)[1];
    value_type max = slip::L1_norm<value_type>(upper_left.col_begin(0),
					       upper_left.col_end(0));

    for(std::size_t j = 1; j < cols; ++j)
      {
	value_type tmp = slip::L1_norm<value_type>(upper_left.col_begin(j),
						   upper_left.col_end(j));
	if(tmp > max)
	  {
	    max = tmp;
	  }
      }
    return max;
  }
  

  /**
   ** \brief Computes the column norm (\f$\max_i\sum_j |a_{ij}|\f$)  of a Container2d.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param container Container2d.
   ** \return The column norm of the 2d container.
   ** \pre container.size() != 0
   ** \remarks It corresponds to the L1 norm of a Matrix.
   ** \remarks Works with real and complex matrix.
   ** \par Complexity: rows*cols flops
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** std::cout<<"col norm of M = "<<slip::col_norm(M)<<std::endl;
   ** \endcode
   */  
  template <typename Container2d>
  inline
  typename slip::lin_alg_traits<typename Container2d::value_type>::value_type col_norm(const Container2d& container)
  {
    return slip::col_norm(container.upper_left(),container.bottom_right());
  }


/**
   ** \brief Computes the Frobenius norm (\f$\sum_{i,j} \bar{a_{ij}}a_{i,j}\f$)  of a 2d range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/04/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param upper_left RandomAccessIterator2d: first element of the 2d range.
   ** \param bottom_right RandomAccessIterator2d: one past-the-end of the 2d range.
   ** \return The Frobenius norm of the 2d range.
   ** \pre bottom_right - upper_left != slip::Dpoint2d<int>(0,0)
   ** \remarks It corresponds to the infinite norm of a Matrix.
   ** \remarks Works with real and complex matrix.
   ** \par Complexity: rows*cols flops
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** std::cout<<"Frobenius norm of M = "<<slip::frobenius_norm(M.upper_left(),M.bottom_right())<<std::endl;
   ** \endcode
   */  
  template <typename RandomAccessIterator2d>
  inline
  typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator2d>::value_type>::value_type frobenius_norm(RandomAccessIterator2d upper_left, RandomAccessIterator2d bottom_right)
  {
    assert(((bottom_right - upper_left)[0] != 0) && ((bottom_right - upper_left)[1] != 0));
    
    return std::sqrt(slip::L22_norm_cplx(upper_left,bottom_right));
  }  

/**
   ** \brief Computes the Frobenius norm   (\f$\sum_{i,j}
   ** \bar{a_{ij}}a_{i,j}\f$) of a Container2d.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param container Container2d.
   ** \return The column norm of the 2d container.
   ** \pre container.size() != 0
   ** \remarks It corresponds to the L1 norm of a Matrix.
   ** \remarks Works with real and complex matrix.
   ** \par Complexity: rows*cols flops
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::iota(M.begin(),M.end(),0.0);
   ** std::cout<<"Frobenius norm of M = "<<slip::frobenius_norm(M)<<std::endl;
   ** \endcode
   */  
  template <typename Container2d>
  inline
  typename slip::lin_alg_traits<typename Container2d::value_type>::value_type frobenius_norm(const Container2d& container)
  {
    return slip::frobenius_norm(container.upper_left(),container.bottom_right());
  }

   /* @} */


  

  
/** \name Diagonal algorithms */
  /* @{ */

/**
 ** \brief Get the diagonal \a diag_number of a 2d container.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2008/10/28
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param container A 2d container
 ** \param diag_first RandomAccessIterator to the first element of the diagonal.
 ** \param diag_last RandomAccessIterator to one past-the-end element of the diagonal.
 ** \param diag_number number of the diagonal:
 **          \li 0 the diagonal (default value)
 **          \li a negative number corresponds to a lower diagonal
 **          \li a positive number corresponds to an upper diagonal 
 ** \pre container.dim1() == container.dim2()
 ** \pre std::abs(diag_number) < container.dim1()
 ** \pre (diag_last - diag_first) <= container.dim1()
 ** \par Example:
 ** \code
 ** slip::Array2d<float> AA(5,5);
 ** slip::iota(AA.begin(),AA.end(),1.0);
 ** std::cout<<"AA ="<<std::endl;
 ** std::cout<<AA<<std::endl;
 ** slip::Array<float> diag0(5);
 ** slip::get_diagonal(AA,diag0.begin(),diag0.end());
 ** std::cout<<"main diagonal:\n"<<diag0<<std::endl;
 ** slip::Array<float> diag1(4);
 ** slip::get_diagonal(AA,diag1.begin(),diag1.end(),1);
 ** std::cout<<"first upper diagonal:\n"<<diag1<<std::endl;
 ** slip::Array<float> diagm1(4);
 ** slip::get_diagonal(AA,diagm1.begin(),diagm1.end(),-1);
 ** std::cout<<"first lower diagonal:\n"<<diagm1<<std::endl;
 ** slip::Array<float> diag2(3);
 ** slip::get_diagonal(AA,diag2.begin(),diag2.end(),2);
 ** std::cout<<"second upper diagonal:\n"<<diag2<<std::endl;
 ** slip::Array<float> diagm2(3);
 ** slip::get_diagonal(AA,diagm2.begin(),diagm2.end(),-2);
 ** std::cout<<"second lower diagonal:\n"<<diagm2<<std::endl;
 ** \endcode
 */
  template<typename Container2d,
	   typename RandomAccessIterator1>
  void get_diagonal(const Container2d& container,
		    RandomAccessIterator1 diag_first, RandomAccessIterator1 diag_last,
		    const int diag_number = 0)

  {
    assert(container.dim1() == container.dim2());
    assert(std::abs(diag_number) < int(container.dim1()));
    assert((diag_last - diag_first) <= int(container.dim1())); 
    int i = 0;
    int j = 0;
    if(diag_number >= 0)
      {
	i = 0;
	j = diag_number;
      }
    else
      {
	i = -diag_number;
	j = 0;
      }
    
    for(; diag_first != diag_last; ++diag_first)
      {
	*diag_first = container[i][j];
	i = i + 1;
	j = j + 1;
      }
  }

/**
 ** \brief Set the diagonal [diag_first,diag_last) in the diagonal \a diag_number of a 2d container.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2008/10/28
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param diag_first RandomAccessIterator to the first element of the diagonal.
 ** \param diag_last RandomAccessIterator to one past-the-end element of the diagonal.
 ** \param container A 2d container
 ** \param diag_number number of the diagonal:
 **          \li 0 the diagonal (default value)
 **          \li a negative number corresponds to a lower diagonal
 **          \li a positive number corresponds to an upper diagonal 
 ** \pre container.dim1() == container.dim2()
 ** \pre std::abs(diag_number) < container.dim1()
 ** \pre (diag_last - diag_first) <= container.dim1()
 ** \par Example:
 ** \code
 ** slip::Array2d<float> AA(5,5);
 ** slip::Array<float> diag0(5);
 ** diag0.fill(0.0);
 ** diag1.fill(1.0);
 ** diagm1.fill(-1.0);
 ** diag2.fill(2.0);
 ** diagm2.fill(-2.0);
 ** std::cout<<"AA ="<<std::endl;
 ** std::cout<<AA<<std::endl;
 ** std::cout<<"set main diagonal with : "<<diag0<<std::endl;
 ** slip::set_diagonal(diag0.begin(),diag0.end(),AA);
 ** std::cout<<"set first upper diagonal with : "<<diag1<<std::endl;
 ** slip::set_diagonal(diag1.begin(),diag1.end(),AA,1);
 ** std::cout<<"set first lower diagonal with : "<<diagm1<<std::endl;
 ** slip::set_diagonal(diagm1.begin(),diagm1.end(),AA,-1);
 ** std::cout<<"set second upper diagonal with : "<<diag2<<std::endl;
 ** slip::set_diagonal(diag2.begin(),diag2.end(),AA,2);
 ** std::cout<<"set second lower diagonal with : "<<diagm2<<std::endl;
 ** slip::set_diagonal(diagm2.begin(),diagm2.end(),AA,-2);
 ** std::cout<<"new AA ="<<std::endl;
 ** std::cout<<AA<<std::endl;
 ** \endcode
 */
  template<typename Container2d,
	   typename RandomAccessIterator1>
  void set_diagonal(RandomAccessIterator1 diag_first, RandomAccessIterator1 diag_last,
		    Container2d& container,
		    const int diag_number = 0)

  {
    assert(container.dim1() == container.dim2());
    assert(std::abs(diag_number) < int(container.dim1()));
    assert((diag_last - diag_first) <= int(container.dim1())); 
    int i = 0;
    int j = 0;
    if(diag_number >= 0)
      {
	i = 0;
	j = diag_number;
      }
    else
      {
	i = -diag_number;
	j = 0;
      }
    
    for(; diag_first != diag_last; ++diag_first)
      {
	container[i][j] = *diag_first;
	i = i + 1;
	j = j + 1;
      }
  }

/**
 ** \brief Fill the diagonal \a diag_number of a 2d container with the value \a val.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2008/10/28
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param container A 2d container
 ** \param val value
 ** \param diag_number number of the diagonal:
 **          \li 0 the diagonal (default value)
 **          \li a negative number corresponds to a lower diagonal
 **          \li a positive number corresponds to an upper diagonal 
 ** \pre container.dim1() == container.dim2()
 ** \pre std::abs(diag_number) < container.dim1()
 ** \par Example:
 ** \code
 ** slip::Array2d<float> AA(5,5);
 ** std::cout<<"fill main diagonal with 1.0 :"<<std::endl;
 ** slip::fill_diagonal(AA,1.0);
 ** std::cout<<"fill first upper diagonal with 2.0 :"<<std::endl;
 ** slip::fill_diagonal(AA,2.0,1);
 ** std::cout<<"fill first lower diagonal with -2.0 :"<<std::endl;
 ** slip::fill_diagonal(AA,-2.0,-1);
 ** std::cout<<"fill second upper diagonal with 3.0 :"<<std::endl;
 ** slip::fill_diagonal(AA,3.0,2);
 ** std::cout<<"fill second lower diagonal with -3.0 :"<<std::endl;
 ** slip::fill_diagonal(AA,-3.0,-2);
 ** std::cout<<"new AA ="<<std::endl;
 ** std::cout<<AA<<std::endl;
 ** \endcode
 */
  template<typename Container2d>
  void fill_diagonal(Container2d& container,
		     const typename Container2d::value_type& val,
		     const int diag_number = 0)

  {
    assert(container.dim1() == container.dim2());
    assert(std::abs(diag_number) < int(container.dim1()));
   
    int i = 0;
    int j = 0;
    if(diag_number >= 0)
      {
	i = 0;
	j = diag_number;
      }
    else
      {
	i = -diag_number;
	j = 0;
      }
    
    int n = int(container.dim1()) - std::abs(diag_number);
    for(int k = 0; k < n; ++k)
      {
	container[i][j] = val;
	i = i + 1;
	j = j + 1;
      }
    }

/**
 ** \brief Fill the diagonal \a diag_number of a 2d range with the value \a val.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2008/10/28
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param M_up iterator2d to the upper_left element of the range.
 ** \param M_bot iterator2d to the bottom_right element og the range.
 ** \param val value
 ** \param diag_number number of the diagonal:
 **          \li 0 the diagonal (default value)
 **          \li a negative number corresponds to a lower diagonal
 **          \li a positive number corresponds to an upper diagonal 
 ** \pre std::abs(diag_number) < (M_bot-M_up)[0]
 ** \par Example:
 ** \code
 ** slip::Array2d<float> AA(5,5);
 ** slip::fill_diagonal(AA.upper_left(),AA.bottom_right(),1.5,-2);
 ** std::cout<<"new AA ="<<std::endl;
 ** std::cout<<AA<<std::endl;
 ** \endcode
 */
 template<typename MatrixIterator>
 void fill_diagonal(MatrixIterator M_up,
		    MatrixIterator M_bot,
		    const typename MatrixIterator::value_type& val,
		    const int diag_number = 0)

  {
    //assert((M_bot-M_up)[0] == (M_bot-M_up)[1]);
    assert(std::abs(diag_number) < int((M_bot-M_up)[0]));
   
    int i = 0;
    int j = 0;
    if(diag_number >= 0)
      {
	i = 0;
	j = diag_number;
      }
    else
      {
	i = -diag_number;
	j = 0;
      }
    
    int n = int((M_bot-M_up)[0]) - std::abs(diag_number);
    slip::DPoint2d<int> di(i,j);
    slip::DPoint2d<int> d(1,1);
    for(int k = 0; k < n; ++k)
      {
	M_up[di] = val;
	di+= d;
      }
    }

/* @} */

/** \name Matrix generators */
/* @{ */
   /**
   ** \brief Returns an identity matrix which dimensions are nr*nc
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/07/15
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param nr Number of rows.
   ** \param nc Number of columns.
   **
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(slip::identity<slip::Matrix<float> >(3,4));
   ** \endcode
   */
  template <typename Matrix>
  inline
  Matrix identity(const std::size_t nr, const std::size_t nc)
  {
    typedef typename Matrix::value_type _T;
    Matrix Res(nr,nc,_T(0));
    for(std::size_t i = 0; i < nr; ++i)
      {
	if(i < nc)
	  {
	    Res[i][i] = _T(1);
	  }
      }
    return Res;
  }

 /**
   ** \brief Set a 2d range to the identity matrix.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/07/15
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up
   ** \param A_bot
   **
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::identity(M.upper_left(),M.bottom_right());
   ** \endcode
   */
 template <typename MatrixIterator>
 inline
 void identity(MatrixIterator A_up,
	       MatrixIterator A_bot)
  {
    typedef typename MatrixIterator::value_type value_type;
    int A_rows = int((A_bot - A_up)[0]);
    int A_cols = int((A_bot - A_up)[1]);
    int n = std::min(A_rows,A_cols);
    std::fill(A_up,A_bot,value_type(0));
    slip::DPoint2d<int> d(1,1);
    for (int i = 0; i < n; ++i)
      {
	*A_up = value_type(1);
	A_up += d;
      }
  }

 /**
   ** \brief Replaces A with the identity matrix
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/07/15
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   **
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::identity(M);
   ** \endcode
   */
  
  template <typename Matrix>
  inline
  void identity(Matrix & A)
  {
    typedef typename Matrix::value_type _T;
    std::size_t A_rows = A.rows();
    std::size_t A_cols = A.cols();
    for(std::size_t i = 0; i < A_rows; ++i)
      {
	if(i < A_cols)
	  {
	    for(std::size_t j = 0; j < i; ++j)
	      {
		A[i][j] = _T(0);
	      }
	    A[i][i] = _T(1);
	    for(std::size_t j = i+1; j < A_cols; ++j)
	      {
		A[i][j] = _T(0);
	      }
	  }
	else
	  {
	    for(std::size_t j = 0; j < A_cols; ++j)
	      {
		A[i][j] = _T(0);
	      }
	  }
      }
  }

 /**
   ** \brief Replaces A with the Hilbert matrix: \f[h_{ij} = \frac{1}{i+j+1}\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \remarks Fill the matrix A according to its size
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::hilbert(M);
   ** \endcode
   */
  template <typename Matrix>
  inline
  void hilbert(Matrix & A)
  {
    typedef typename Matrix::value_type value_type;
    std::size_t A_rows = A.rows();
    std::size_t A_cols = A.cols();
    for(std::size_t i = 0; i < A_rows; ++i)
      { 
	for(std::size_t j = 0; j < A_cols; ++j)
	  {
	    A(i,j) = value_type(1) / value_type((i + j) + 1);
	  }
      }

  }

  /**
   ** \brief Constructs the Toeplitz matrix from given a range.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first RandomAccessIterator to the first element of the
   ** Toeplitz vector
   ** \param  last RandomAccessIterator to one past-the-end element of the
   ** Toeplitz vector
   ** \param A Toplitz matrix.
   ** \pre (A.cols() == A.rows())
   ** \pre A.rows == last - first
   ** \par Example:
   ** \code
   ** slip::Array<float> Vtoep(5);
   ** slip::iota(Vtoep.begin(),Vtoep.end(),1.0);
   ** std::cout<<"Vtoeplitz = \n"<<Vtoep<<std::endl;
   ** slip::Matrix<float> Atoep(Vtoep.size(),Vtoep.size());
   ** slip::toeplitz(Vtoep.begin(),Vtoep.end(),Atoep);
   ** std::cout<<"Atoeplitz = \n"<<Atoep<<std::endl;
   ** \endcode
   */
  
  template <typename RandomAccessIterator, typename Matrix>
  inline
  void toeplitz(RandomAccessIterator first, RandomAccessIterator last, 
		Matrix& A)
  {
    typedef typename std::iterator_traits<RandomAccessIterator>::difference_type _Difference;
    
    const _Difference size = last -first;
    const int n = int(size);

    assert(A.rows() == A.cols());
    assert(int(size) == int(A.rows()));
    
    slip::fill_diagonal(A,*first++);
    
    for(int i = 1; i < n; ++i, ++first)
      { 
	slip::fill_diagonal(A,*first,i);
	slip::fill_diagonal(A,*first,-i);
      }
  }

/* @} */

/** \name Matrix tests  */
  /* @{ */
  /**
   ** \brief Test if a matrix is squared
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \return true if the matrix is squared, false else.
   **
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** std::cout<<slip::is_squared(M)<<std::endl;
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_squared(const Matrix & A)
  {
    return (A.dim1() == A.dim2());
  }

  /**
   ** \brief Test if a matrix is squared
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/02/22
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \return true if the matrix is squared, false else.
   **
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(3,4);
   ** slip::Box2d<int> box(1,1,3,3);
   ** std::cout<<slip::is_squared(M.upper_left(box),M.bottom_right(box))<<std::endl;
   ** \endcode
   */
  template <typename MatrixIterator1>
  inline
  bool is_squared(MatrixIterator1 A_up,
		  MatrixIterator1 A_bot)
  {
    return ((A_bot - A_up)[0] == (A_bot - A_up)[1]);
  }

   /**
   ** \brief Test if a matrix is identity
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is identity, false else.
   **
   ** \par Example:
   ** \code
   ** slip::Matrix<float> M(4,4);
   ** slip::identity(M);
   ** std::cout<<slip::is_identity(M,1.0e-6)<<std::endl;
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_identity(const Matrix & A, 
		   const typename slip::lin_alg_traits<typename Matrix::value_type>::value_type tol 
		   = slip::epsilon<typename Matrix::value_type>())
  {
    if(!is_squared(A))
      {
	return false;
      }
    
    typedef typename Matrix::value_type value_type;
    for(std::size_t i = 0; i < A.rows(); ++i)
      { 
	if(std::abs(A[i][i] - value_type(1)) > tol)
	  {
	    return false;
	  }
	for(std::size_t j = 0; j < i; ++j)
	  {
	    if((std::abs(A[i][j]) > tol) || (std::abs(A[j][i]) > tol))
	      {
		return false;
	      }
	  }
      }
      return true;
      
  }

 

   /**
   ** \brief Test if a matrix is diagonal
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is diagonal, false else.
   **
   ** \par Example:
   ** \code
   ** slip::Array2d<double> AA(5,5);
   ** std::cout<<"fill main diagonal with 1.0 :"<<std::endl;
   ** slip::fill_diagonal(AA,1.0);
   ** std::cout<<slip::is_diagonal(AA.upper_left(),AA.bottom_right())<<std::endl;
   ** \endcode
   */
  template <typename MatrixIterator1>
  inline
  bool is_diagonal(MatrixIterator1 A_up,
		   MatrixIterator1 A_bot,
		   const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
		   = slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
  {
    if(!is_squared(A_up,A_bot))
      {
	return false;
      }
    
    const std::size_t A_rows = (std::size_t)(A_bot - A_up)[0];
    slip::DPoint2d<int> d1;
    slip::DPoint2d<int> d2;
    
    for(std::size_t i = 0; i < A_rows; ++i)
      { 
	for(std::size_t j = 0; j < i; ++j)
	  {
	    d1.set_coord(i,j);
	    d2.set_coord(j,i);
	    if((std::abs(A_up[d1]) > tol) || (std::abs(A_up[d2]) > tol))
	      {
		return false;
	      }
	  }
      }
      return true;
      
  }


  
   /**
   ** \brief Test if a matrix is diagonal
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is diagonal, false else.
   **
   ** \par Example:
   ** \code
   ** slip::Array2d<double> AA(5,5);
   ** std::cout<<"fill main diagonal with 1.0 :"<<std::endl;
   ** slip::fill_diagonal(AA,1.0);
   ** std::cout<<slip::is_diagonal(AA)<<std::endl;
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_diagonal(const Matrix & A, 
		   const typename slip::lin_alg_traits<typename Matrix::value_type>::value_type tol 
		   = slip::epsilon<typename Matrix::value_type>())
  {
   
    return slip::is_diagonal(A.upper_left(),A.bottom_right(),tol);
      
  }


  /**
   ** \brief Test if a matrix is hermitian
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is hermitian, false else.
   **
   ** \par Example:
   ** \code
   ** slip::Matrix<double> H(4,4);
   ** slip::hilbert(H);
   ** std::cout<<"Hilbert matrix = "<<std::endl;
   ** std::cout<<H<<std::endl;
   ** std::cout<<slip::is_hermitian(H.upper_left(),H.bottom_right())<<std::endl;
   ** std::cout<<slip::is_hermitian(H.upper_left(),H.bottom_right(),1.0E-6)<<std::endl;
    
   ** \endcode
   */
  template <typename MatrixIterator1>
  inline
  bool is_hermitian(MatrixIterator1 A_up,
		    MatrixIterator1 A_bot,
		    const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
		    = slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
  {
    if(!is_squared(A_up,A_bot))
      {
	return false;
      }
    
    const std::size_t A_rows = (std::size_t)(A_bot - A_up)[0];
    slip::DPoint2d<int> d1;
    slip::DPoint2d<int> d2;
    
    for(std::size_t i = 0; i < A_rows; ++i)
      { 
	for(std::size_t j = 0; j <= i; ++j)
	  {
	    d1.set_coord(i,j);
	    d2.set_coord(j,i);
	    if(std::abs(A_up[d1] - slip::conj(A_up[d2])) > tol)
	      {
		return false;
	      }
	  }
      }
      return true;
      
  }

 /**
   ** \brief Test if a matrix is hermitian
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is hermitian, false else.
   **
   ** \par Example:
   ** \code
   ** slip::Matrix<double> H(4,4);
   ** slip::hilbert(H);
   ** std::cout<<"Hilbert matrix = "<<std::endl;
   ** std::cout<<H<<std::endl;
   ** std::cout<<slip::is_hermitian(H)<<std::endl;
   ** std::cout<<slip::is_hermitian(H,1.0E-6)<<std::endl;
    
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_hermitian(const Matrix & A, 
		    const typename slip::lin_alg_traits<typename Matrix::value_type>::value_type tol 
		   = slip::epsilon<typename Matrix::value_type>())
  {
   
    return slip::is_hermitian(A.upper_left(),A.bottom_right(),tol);
      
  }
 /**
   ** \brief Test if a matrix is skew hermitian
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is skew hermitian, false else.
   **
   ** \par Example:
   ** \code
   ** slip::Matrix<std::complex<double> > Sshc(3,3);
   ** Sshc(0,0) = std::complex<double>(0.0,1.0);
   ** Sshc(0,1) = std::complex<double>(0.5,-0.5);
   ** Sshc(0,2) = std::complex<double>(1.0,-1.2);
   ** Sshc(1,0) = std::complex<double>(-0.5,-0.5);  
   ** Sshc(1,1) = std::complex<double>(0.0,2.0);
   ** Sshc(1,2) = std::complex<double>(0.2,-0.3);
   ** Sshc(2,0) = std::complex<double>(-1.0,-1.2);  
   ** Sshc(2,1) = std::complex<double>(-0.2,-0.3);
   ** Sshc(2,2) = std::complex<double>(0.0,3.0);
   ** std::cout<<"Sshc = \n"<<Sshc<<std::endl;
   ** if(slip::is_skew_hermitian(Sshc.upper_left(),Sshc.bottom_right()))
   ** {
   **   std::cout<<"Sshc is skew hermitian"<<std::endl;
   ** }
   ** else
   ** {
   **   std::cout<<"Sshc is not skew hermitian"<<std::endl;
   ** }
   ** \endcode
   */
  template <typename MatrixIterator1>
  inline
  bool is_skew_hermitian(MatrixIterator1 A_up,
		    MatrixIterator1 A_bot,
		    const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
		    = slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
  {
    if(!is_squared(A_up,A_bot))
      {
	return false;
      }
    
    const std::size_t A_rows = (std::size_t)(A_bot - A_up)[0];
    slip::DPoint2d<int> d1;
    slip::DPoint2d<int> d2;
    
    for(std::size_t i = 0; i < A_rows; ++i)
      { 
	for(std::size_t j = 0; j <= i; ++j)
	  {
	    d1.set_coord(i,j);
	    d2.set_coord(j,i);
	    if(std::abs(A_up[d1] + slip::conj(A_up[d2])) > tol)
	      {
		return false;
	      }
	  }
      }
      return true;
      
  }

 /**
   ** \brief Test if a matrix is skew hermitian
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is skew hermitian, false else.
   **
   ** \par Example:
   ** \code
  ** slip::Matrix<std::complex<double> > Sshc(3,3);
   ** Sshc(0,0) = std::complex<double>(0.0,1.0);
   ** Sshc(0,1) = std::complex<double>(0.5,-0.5);
   ** Sshc(0,2) = std::complex<double>(1.0,-1.2);
   ** Sshc(1,0) = std::complex<double>(-0.5,-0.5);  
   ** Sshc(1,1) = std::complex<double>(0.0,2.0);
   ** Sshc(1,2) = std::complex<double>(0.2,-0.3);
   ** Sshc(2,0) = std::complex<double>(-1.0,-1.2);  
   ** Sshc(2,1) = std::complex<double>(-0.2,-0.3);
   ** Sshc(2,2) = std::complex<double>(0.0,3.0);
   ** std::cout<<"Sshc = \n"<<Sshc<<std::endl;
   ** if(slip::is_skew_hermitian(Sshc)
   ** {
   **   std::cout<<"Sshc is skew hermitian"<<std::endl;
   ** }
   ** else
   ** {
   **   std::cout<<"Sshc is not skew hermitian"<<std::endl;
   ** }
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_skew_hermitian(const Matrix & A, 
		    const typename slip::lin_alg_traits<typename Matrix::value_type>::value_type tol 
		   = slip::epsilon<typename Matrix::value_type>())
  {
   
    return slip::is_hermitian(A.upper_left(),A.bottom_right(),tol);
      
  }


  /**
   ** \brief Test if a matrix is symmetric
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is symmetric, false else.
   **
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Sh(3,3);
   ** Sh(0,0) =  1.0;  Sh(0,1) =  0.5;  Sh(0,2) = -1.2;
   ** Sh(1,0) =  0.5;  Sh(1,1) =  2.0;  Sh(1,2) = -0.3;
   ** Sh(2,0) = -1.2;  Sh(2,1) = -0.3;  Sh(2,2) =  3.0;
   ** if(slip::is_symmetric(Sh.upper_left(),Sh.bottom_right()))
   **  {
   **    std::cout<<"Sh is symmetric"<<std::endl;
   **  }
   ** else
   ** {
   **    std::cout<<"Sh is not symmetric"<<std::endl;
   ** }
   ** \endcode
   */
  template <typename MatrixIterator1>
  inline
  bool is_symmetric(MatrixIterator1 A_up,
		    MatrixIterator1 A_bot,
		    const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
		    = slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
  {
    if(slip::is_complex(typename std::iterator_traits<MatrixIterator1>::value_type()))
      {
	return false;
      }
    return slip::is_hermitian(A_up,A_bot,tol);      
  }

 /**
   ** \brief Test if a matrix is symmetric
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is symmetric, false else.
   **
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Sh(3,3);
   ** Sh(0,0) =  1.0;  Sh(0,1) =  0.5;  Sh(0,2) = -1.2;
   ** Sh(1,0) =  0.5;  Sh(1,1) =  2.0;  Sh(1,2) = -0.3;
   ** Sh(2,0) = -1.2;  Sh(2,1) = -0.3;  Sh(2,2) =  3.0;
   ** if(slip::is_symmetric(Sh))
   **  {
   **    std::cout<<"Sh is symmetric"<<std::endl;
   **  }
   ** else
   ** {
   **    std::cout<<"Sh is not symmetric"<<std::endl;
   ** }
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_symmetric(const Matrix & A, 
		    const typename slip::lin_alg_traits<typename Matrix::value_type>::value_type tol 
		   = slip::epsilon<typename Matrix::value_type>())
  {
   
    return slip::is_symmetric(A.upper_left(),A.bottom_right(),tol);
      
  }




  /**
   ** \brief Test if a matrix is a band matrix
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \param lower_band_width Lower band width.
   ** \param upper_band_width Upper band width.

   ** \param tol
   ** \return true if the matrix i > j + lower_band_width and j > i - upper_band_width=> std::abs(Aij) < tol, false else.
   **
   ** \pre lower_band_width >= 0
   ** \pre upper_band_width >= 0
   ** \pre upper_band_width < (A_bot - A_up)[0]
   ** \pre lower_band_width < (A_bot - A_up)[0]
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename MatrixIterator1>
  inline
  bool is_band_matrix(MatrixIterator1 A_up,
		      MatrixIterator1 A_bot,
		      const typename MatrixIterator1::size_type lower_band_width,
		      const typename MatrixIterator1::size_type upper_band_width,
		      const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
		      = slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
  {

    assert(typename MatrixIterator1::size_type(lower_band_width) < typename MatrixIterator1::size_type((A_bot - A_up)[0]));
    assert(typename MatrixIterator1::size_type(upper_band_width) < typename MatrixIterator1::size_type((A_bot - A_up)[0]));
    assert(lower_band_width >= typename MatrixIterator1::size_type(0));
    assert(upper_band_width >= typename MatrixIterator1::size_type(0));

    if(!is_squared(A_up,A_bot))
      {
	return false;
      }
    const std::size_t A_rows = (std::size_t)(A_bot - A_up)[0];

    slip::DPoint2d<int> d1;
       
    for(std::size_t i = 0; i < (A_rows - upper_band_width); ++i)
      { 
	for(std::size_t j = (i + upper_band_width + 1); j < A_rows; ++j)
	  {
	    d1.set_coord(i,j);
	    if(std::abs(A_up[d1]) > tol)
	      {
		return false;
	      }
	  }
      }

    for(std::size_t i = (lower_band_width + 1); i < A_rows; ++i)
      { 
	for(std::size_t j = 0; j < (i-lower_band_width); ++j)
	  {
	    d1.set_coord(i,j);
	    if(std::abs(A_up[d1]) > tol)
	      {
		return false;
	      }
	  }
      }
      return true;
  }

 /**
   ** \brief Test if a matrix is symmetric
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param lower_band_width Lower band width.
   ** \param upper_band_width Upper band width.
   ** \param tol
   ** \return true if the matrix is symmetric, false else.
   **
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_band_matrix(const Matrix & A, 
		      const typename Matrix::size_type lower_band_width,
		      const typename Matrix::size_type upper_band_width,
		      const typename slip::lin_alg_traits<typename Matrix::value_type>::value_type tol 
		   = slip::epsilon<typename Matrix::value_type>())
  {
   
    return slip::is_band_matrix(A.upper_left(),A.bottom_right(),
				lower_band_width,upper_band_width,
				tol);
      
  }
  /**
   ** \brief Test if a matrix is tridiagonal
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \param tol
   ** \return true if the matrix is tridiagonal
   **
 
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename MatrixIterator1>
  inline
  bool is_tridiagonal(MatrixIterator1 A_up,
		      MatrixIterator1 A_bot,
		      const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
		      = slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
  {
    return slip::is_band_matrix(A_up,A_bot,1,1,tol);
  }


   /**
   ** \brief Test if a matrix is tridiagonal
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param tol
   ** \return true if the matrix is tridiagonal, false else.
   **
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_tridiagonal(const Matrix & A, 
		   const typename slip::lin_alg_traits<typename Matrix::value_type>::value_type tol 
		   = slip::epsilon<typename Matrix::value_type>())
  {
   
    return slip::is_tridiagonal(A.upper_left(),A.bottom_right(),tol);
      
  }

/**
   ** \brief Test if a matrix is upper_bidiagonal
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \param tol
   ** \return true if the matrix is upper_bidiagonal
   **
 
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename MatrixIterator1>
  inline
  bool is_upper_bidiagonal(MatrixIterator1 A_up,
			   MatrixIterator1 A_bot,
			   const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
		      = slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
  {
    return slip::is_band_matrix(A_up,A_bot,0,1,tol);
  }
 /**
   ** \brief Test if a matrix is upper bidiagonal
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is upper bidiagonal, false else.
   **
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_upper_bidiagonal(const Matrix & A, 
		   const typename slip::lin_alg_traits<typename Matrix::value_type>::value_type tol 
		   = slip::epsilon<typename Matrix::value_type>())
  {
   
    return slip::is_upper_bidiagonal(A.upper_left(),A.bottom_right(),tol);
      
  }

  /**
   ** \brief Test if a matrix is lower_bidiagonal
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is lower_bidiagonal
   **
 
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename MatrixIterator1>
  inline
  bool is_lower_bidiagonal(MatrixIterator1 A_up,
			   MatrixIterator1 A_bot,
			   const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
		      = slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
  {
    return slip::is_band_matrix(A_up,A_bot,1,0,tol);
  }

  /**
   ** \brief Test if a matrix is lower bidiagonal
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is lower bidiagonal, false else.
   **
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_lower_bidiagonal(const Matrix & A, 
		   const typename slip::lin_alg_traits<typename Matrix::value_type>::value_type tol 
		   = slip::epsilon<typename Matrix::value_type>())
  {
   
    return slip::is_lower_bidiagonal(A.upper_left(),A.bottom_right(),tol);
      
  }
/**
   ** \brief Test if a matrix is upper_hessenberg
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is upper_hessenberg
   **
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename MatrixIterator1>
  inline
  bool is_upper_hessenberg(MatrixIterator1 A_up,
			   MatrixIterator1 A_bot,
			   const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
		      = slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
  {
    return slip::is_band_matrix(A_up,A_bot,1,((A_bot-A_up)[0]-1),tol);
  }


   /**
   ** \brief Test if a matrix is upper hessenber
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is upper hessenber, false else.
   **
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_upper_hessenberg(const Matrix & A, 
		   const typename slip::lin_alg_traits<typename Matrix::value_type>::value_type tol 
		   = slip::epsilon<typename Matrix::value_type>())
  {
   
    return slip::is_upper_hessenberg(A.upper_left(),A.bottom_right(),tol);
      
  }

/**
   ** \brief Test if a matrix is lower_hessenberg
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is lower_hessenberg
   **
 
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename MatrixIterator1>
  inline
  bool is_lower_hessenberg(MatrixIterator1 A_up,
			   MatrixIterator1 A_bot,
			   const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
		      = slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
  {
    return slip::is_band_matrix(A_up,A_bot,((A_bot-A_up)[0]-1),1,tol);
  }


  /**
   ** \brief Test if a matrix is lower hessenber
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is lower hessenber, false else.
   **
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_lower_hessenberg(const Matrix & A, 
		   const typename slip::lin_alg_traits<typename Matrix::value_type>::value_type tol 
		   = slip::epsilon<typename Matrix::value_type>())
  {
   
    return slip::is_lower_hessenberg(A.upper_left(),A.bottom_right(),tol);
      
  }

    /**
   ** \brief Test if a matrix is upper_triangular
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is upper_triangular, false else.
   **
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename MatrixIterator1>
  inline
  bool is_upper_triangular(MatrixIterator1 A_up,
			   MatrixIterator1 A_bot,
  const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
  = slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
  {
    return slip::is_band_matrix(A_up,A_bot,0,((A_bot-A_up)[0]-1),tol);
  }

  /**
   ** \brief Test if a matrix is upper triangular
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is upper triangular, false else.
   **
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_upper_triangular(const Matrix & A, 
		   const typename slip::lin_alg_traits<typename Matrix::value_type>::value_type tol 
		   = slip::epsilon<typename Matrix::value_type>())
  {
   
    return slip::is_upper_triangular(A.upper_left(),A.bottom_right(),tol);
      
  }
   /**
   ** \brief Test if a matrix is lower_triangular
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/02/22
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container
   ** \param A_bot 2D iterator on the bottom_right element of A container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is lower triangular, false else.
   **
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename MatrixIterator1>
  inline
  bool is_lower_triangular(MatrixIterator1 A_up,
			   MatrixIterator1 A_bot,
			   const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
  = slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
  {
     return slip::is_band_matrix(A_up,A_bot,((A_bot-A_up)[0]-1),0,tol);
  }
/**
   ** \brief Test if a matrix is lower triangular
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is lower triangular, false else.
   **
   ** \par Example:
   ** \code
 
   ** \endcode
   */
  template <typename Matrix>
  inline
  bool is_lower_triangular(const Matrix & A, 
		   const typename slip::lin_alg_traits<typename Matrix::value_type>::value_type tol 
		   = slip::epsilon<typename Matrix::value_type>())
  {
   
    return slip::is_lower_triangular(A.upper_left(),A.bottom_right(),tol);
      
  }
  /**
   ** \brief Test if a matrix has a nul diagonal
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container.
   ** \param A_bot 2D iterator on the bottom_right element of A container.
   ** \param diag_number digonal number.
   ** \param tol Tolerance (epsilon by default).
   ** \return true if the matrix is nul diagonal, false else.
   **
   ** \par Example:
   ** \code
 
   ** \endcode
   */
 template <typename MatrixIterator1>
 inline
 bool is_null_diagonal(MatrixIterator1 A_up,
		       MatrixIterator1 A_bot,
		       int diag_number,
		       const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
		       = slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
{

  int rows = int((A_bot - A_up)[0]);
  int cols = int((A_bot - A_up)[1]);
  int iterations = std::min(rows,cols) - std::abs(diag_number);
 
  slip::DPoint2d<int> d(1,1);
  int i = 0;
  int j = 0;
  if(diag_number >= 0)
    {
      i = 0;
      j = diag_number;
    }
  else
    {
      i = -diag_number;
      j = 0;
    }
  slip::DPoint2d<int> dstart(i,j);
  MatrixIterator1 A_up_start = A_up + dstart;
  for(int k = 0; k < iterations; ++k)
    {
      if(std::abs(*A_up_start) > tol)
	{
	  return false;
	}
      A_up_start += d;
    }
  return true;
}

 /**
   ** \brief Analyse the type of a matrix.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/10
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator on the upper_left element of A container.
   ** \param A_bot 2D iterator on the bottom_right element of A container.
   ** \param tol Tolerance (epsilon by default).
   ** \return slip::DENSE_MATRIX_TYPE type of the dense matrix.
   **
   ** \par Example:
   ** \code
 
   ** \endcode
   */
 template <typename MatrixIterator1>
 inline
 slip::DENSE_MATRIX_TYPE analyse_matrix(MatrixIterator1 A_up,
					MatrixIterator1 A_bot,
					const typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator1>::value_type>::value_type tol 
					= slip::epsilon<typename std::iterator_traits<MatrixIterator1>::value_type>())
  {
    int rows = int((A_bot - A_up)[0]);
    int cols = int((A_bot - A_up)[1]);
    
    int up_band_number  = (cols - 1);
    int low_band_number = (rows - 1);
     
    int k = -low_band_number; 
    while((k < 0) && slip::is_null_diagonal(A_up,A_bot,k,tol))
	{
	  low_band_number--;
	  ++k;
	}
    k = up_band_number;
    while((k > 0) && slip::is_null_diagonal(A_up,A_bot,k,tol))
	{
	  up_band_number--;
	  --k;
	}

    if((up_band_number == low_band_number) && (up_band_number = 0))
      {
	if(slip::is_null_diagonal(A_up,A_bot,0,tol))
	  {
	    return slip::DENSE_MATRIX_NULL;
	  }
	else
	  {
	    return slip::DENSE_MATRIX_DIAGONAL;
	  }
      }
    else if((up_band_number == (cols-1)) && (low_band_number == rows - 1))
      {
	return slip::DENSE_MATRIX_FULL;
      }
    else if((up_band_number == 1) && (low_band_number == 0))
      {
	return slip::DENSE_MATRIX_UPPER_BIDIAGONAL;
      }
    else if((up_band_number == 0) && (low_band_number == 1))
      {
	return slip::DENSE_MATRIX_LOWER_BIDIAGONAL;
      }
    else if((up_band_number == 1) && (low_band_number == 1))
      {
	return slip::DENSE_MATRIX_TRIDIAGONAL;
      }
    else if((up_band_number == (cols-1)) && (low_band_number == 0))
      {
	return slip::DENSE_MATRIX_UPPER_TRIANGULAR;
      }
    else if((up_band_number == 0) && (low_band_number == (rows-1)))
      {
	return slip::DENSE_MATRIX_LOWER_TRIANGULAR;
      }
    else if((up_band_number == (cols-1)) && (low_band_number == 1))
      {
	return slip::DENSE_MATRIX_UPPER_HESSENBERG;
      }
    else if((up_band_number == 1) && (low_band_number == (rows-1)))
      {
	return slip::DENSE_MATRIX_LOWER_HESSENBERG;
      }
    else
      {
	return slip::DENSE_MATRIX_BANDED;
      }
    
  }
/* @} */


  /** \name Pivot algorithms */
  /* @{ */
  

 /**
   ** \brief Returns the row position of the maximum partial pivot of a 2d range.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/08/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M_up 2D iterator on the upper_left element of M container
   ** \param M_bot 2D iterator on the bottom_right element of M container
   ** \return row position of maximum pivot (-1 if all pivot are null)
   */  
  template <typename MatrixIterator>
  inline
  int pivot_max(MatrixIterator M_up,MatrixIterator M_bot)
  {
    typedef typename std::iterator_traits<MatrixIterator>::value_type _T;
    typedef typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator>::value_type>::value_type norm_type;
    typename MatrixIterator::difference_type sizeM= M_bot - M_up;

    norm_type max = std::abs(_T());
    int ind = -1;
    
    while(M_up.x1() < M_bot.x1())
      {
	if(std::abs(*M_up) > max)
	  {
	    max = std::abs(*M_up);
	    ind = M_up.x1();
	  }
	M_up++;
      }
    return ind;
  }
/**
   ** \brief Returns the partial pivot of a 2d range.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/08/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M_up 2D iterator on the upper_left element of M container
   ** \param M_bot 2D iterator on the bottom_right element of M container
   ** \return row position of partial pivot (-1 if all pivot are null)
   */  
template <typename MatrixIterator>
inline
int partial_pivot(MatrixIterator M_up, MatrixIterator M_bot)
{
  typedef typename std::iterator_traits<MatrixIterator>::value_type _T;
  typedef typename slip::lin_alg_traits<typename std::iterator_traits<MatrixIterator>::value_type>::value_type norm_type;
   
  norm_type max = std::abs(*M_up);
  int ind = 0;
  slip::DPoint2d<int> d10(1,0);
  MatrixIterator it = M_up + d10;
  int istop = int((M_bot - M_up)[0]);
  for(int i = 1; i < istop; ++i, it+=d10)
      {
	if(std::abs(*it) > max)
	  {
	    ind = i;
	    max = std::abs(*it);
	  }
      }
    if(max < std::abs(_T()))
      {
	ind = -1;
      }
    else
      {
	ind = ind + M_up.x1();
      }
    return ind;
    
}
/* @} */

/** \name General inverse algorithm */
  /* @{ */
  /**
   ** \brief Computes the inverse of a matrix using gaussian elimination
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/14
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M Matrix container
   ** \param nr1 The number of rows of M
   ** \param nc1 The number of columns of M
   ** \param IM result of the inversion
   ** \param nr2 The number of rows of IM
   ** \param nc2 The number of columns of IM
   **
   ** \pre M, IM must have the double bracket element accessor 
   ** \pre Matrix are supposed to be allocated first
   ** \pre nr1 == nc1
   ** \pre nr2 == nc2
   ** \pre nr1 == nr2
   */  
  template <class Matrix1,class Matrix2>
  inline
  void inverse(const Matrix1 & M, 
		 const std::size_t nr1,
		 const std::size_t nc1,
		 Matrix2 & IM,
		 const std::size_t nr2,
		 const std::size_t nc2)
  {
	assert(nr1==nc1);
        assert(nr2==nc2);
	assert(nr1==nr2);

	size_t i,j,k;
	
	typedef typename Matrix2::value_type _T;
	_T tmpval;
	

	Array2d<typename Matrix2::value_type> Mcopy(nr1,nc1);
	

	// Copy M to the first half of the 
	for (i=0;i<nr1;++i)
	  {
	    for (j=0;j<nc1;++j)
	      {
		Mcopy[i][j]=M[i][j];
	      }
	  }

	// Identity matrix to the right
	std::fill(IM.begin(),IM.end(),_T(0));
	for (i=0;i<nr2;++i)
	  {
	    IM[i][i]=_T(1.0);
	  }

	
	for (i=0;i<nr1;++i)
	  {
	    // M[i][i]==0 : find another row to add...
	    if (Mcopy[i][i]==_T(0))
	      {
		k=i+1;
		while ( k<nr1 && Mcopy[k][i]==_T(0) ) 
		  {
		    k++;
		  }
		if ( k==nr1 )
		  {
		    // TODO : Need to raise exception
		    std::cerr<< "Singular Matrix - can not invert it"<<std::endl;
		    exit(1);
		  }
		for ( j = i ; j < nr1;++j)
		  {
		    Mcopy[i][j]+= Mcopy[k][j] / Mcopy[k][i];
		  }
		for ( j = 0 ; j < nr2;++j)
		  {
		    IM[i][j]+= IM[k][j] / Mcopy[k][i];
		  }
		
	      }
	    // Scale the row to 1
	    tmpval=Mcopy[i][i];
	    for ( j = i ; j < nr1;++j)
	      {
		Mcopy[i][j]/=tmpval;
	      }
	    for ( j = 0 ; j < nr1;++j)
	      {
		IM[i][j]/=tmpval;
	      }
	    // Do elimination
	    for ( k = 0 ; k < nr1;++k)
	      {
		if (k != i)
		  {
		    tmpval=Mcopy[k][i];
		    for ( j = i ; j < nr1;++j)
		      {
			Mcopy[k][j]-=tmpval/Mcopy[i][i]*Mcopy[i][j];
		      }
		    for ( j = 0 ; j < nr2;++j)
		      {
			IM[k][j]-=tmpval/Mcopy[i][i]*IM[i][j];
		      }
		  }
	      }
	  }	
  }
  
  /**
   ** \brief Computes the inverse of a matrix using gaussian elimination
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/14
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M Matrix container
   ** \param IM result of the inversion
   **
   ** \pre M, IM must have cols() and rows() methods
   ** \pre M, IM must have the double bracket element accessor 
   ** \pre Matrix are supposed to be allocated first
   ** \pre M.cols() == M.rows()
   ** \pre IM.rows() == IM.cols()
   ** \pre M.rows() == IM.rows()
   ** \par Example:
   ** \code
   ** std::size_t size2 = 500;  
   ** slip::Matrix<double> M3(size2,size2);
   ** slip::Matrix<double> IM3(size2,size2);
   ** slip::Matrix<double> testinv(size2,size2);
   ** std::generate(M3.begin(),M3.end(),std::rand);
   ** slip::inverse(M3,IM3);	
   ** slip::multiplies(M3,IM3,testinv);
   ** \endcode
   */  
  template <class Matrix1, class Matrix2>
  inline
  void inverse(const Matrix1 & M, Matrix2 & IM)
  {
    slip::inverse(M,M.rows(),M.cols(),IM,IM.rows(),IM.cols());
  }
/* @} */


/** \name LU algorithms */
/* @{ */
  /**
   ** \brief Computes the LU decomposition according to rows permutations of a matrix using Crout method
   ** \f[
   ** \left(
   ** \begin{array}{cccc}
   ** m_{1,1} & \cdots&\cdots&m_{1,n}\\
   ** \vdots&&&\vdots\\
   ** \vdots&&&\vdots\\
   ** m_{n,1}&\cdots&\cdots&m_{n,n}\\
   ** \end{array}\right) 
   ** = \left(
   ** \begin{array}{cccc}
   ** 1&0&\cdots&0\\
   ** l_{2,1}&\ddots&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&0\\
   ** l_{n,1}&\cdots&l_{n,n-1}&1\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{cccc}
   ** u_{1,1}&\cdots&\cdots&u_{1,n}\\
   ** 0&\ddots&&\vdots\\
   ** \vdots&\ddots&\ddots&\vdots\\
   ** 0&\cdots&0&u_{n,n}\\
   ** \end{array}\right) 
   ** \f]
   ** LU is composed of L and U as following:
   ** \f[
   ** LU = 
   ** \left(
   ** \begin{array}{cccc}
   ** u_{1,1}&\cdots&\cdots&u_{1,n}\\
   ** l_{2,1}&\ddots&&\vdots\\
   ** \vdots&\ddots&\ddots&\vdots\\
   ** l_{n,1}&\cdots&l_{n,n-1}&u_{n,n}\\
   ** \end{array}\right) 
   ** \f]
   ** Indx is a vector that records  the row permutations effected by the partial pivoting.
   **
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr: adapt to complex
   ** \date 2009/03/02
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param M Matrix container
   ** \param LU Matrix container lower and upper matrix in the same container
   ** \param Indx Vector container for permutation matrix (integer)
   ** \return +1 if row interchanges was even, -1 if odd
   **
   ** \pre M.rows() == M.cols()
   ** \pre LU.rows() == LU.cols()
   ** \pre M.rows() == LU.rows()
   ** \pre Indx.size() == M.rows()
   ** \remarks Works with real and complex data.
   ** \remarks Raise an exception if M is a singular matrix.
   ** \par Complexity:
   ** \par Example:
   ** \code
   ** std::size_t size=5;  
   ** slip::Matrix<double> M(size,size);
   ** slip::Matrix<double> LU(size,size);
   ** slip::Vector<double> P(size,size);
   **
   ** std::generate(M.begin(),M.end(),std::rand);
   ** std::cout<<std::endl;
   ** std::cout <<"M = \n"<<M<<std::endl;
   ** slip::lu(M,LU,P);	
   ** std::cout <<"LU =\n"<<LU<<std::endl;
   ** std::cout <<"P =\n"<<P<<std::endl;
   ** \endcode
   */  
  template <class Matrix1, class Matrix2, class Vector>
  inline
  int lu(const Matrix1 & M, 
	 Matrix2 & LU,
	 Vector & Indx)
  {
	assert(M.rows()==M.cols());
        assert(LU.rows()==LU.cols());
	assert(M.rows()==LU.rows());
	assert(M.rows()==Indx.size());
	
	std::size_t nr = M.rows();
	std::size_t nc = M.cols();
	

	    typedef typename Matrix1::value_type value_type;
	    typedef typename slip::lin_alg_traits<value_type>::value_type norm_type;

	    slip::Array<norm_type> Vv(nr);
	    int npivot = 1;

	    for(std::size_t i = 0; i < nr; ++i)
	      {
		for(std::size_t j = 0; j < nc; ++j)
		  {
		    LU[i][j] = M[i][j];
		  }
		Indx[i] = i;
	      }
	    // Crout's algorithm without pivoting : working on LU
	    for(std::size_t i = 0; i < nr; ++i)
	      {
		norm_type max = std::abs(LU[i][0]);
		for(std::size_t j = 1; j < nc; ++j)
		  {	
		    norm_type tmp2 = 
		      std::abs(LU[i][j]);
		    if (tmp2 > max) 
		      {
			max = tmp2;
		      }
		  }
		if (max == typename slip::lin_alg_traits<value_type>::value_type(0)) 
		  {
		    throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
		  }
		Vv[i] = norm_type(1) / max;
	      }

	    for (std::size_t j = 0; j < nr; ++j)
	      {
		for (std::size_t i = 0; i < j; ++i)
		  {
		    value_type sum = LU[i][j];
		    for (std::size_t k = 0; k < i; ++k)
		      {
			sum -= LU[i][k] * LU[k][j];
		      }
		    LU[i][j] = sum;
		  }
		norm_type max = 0;
		std::size_t imax = 0;
		for (std::size_t i = j; i < nr; ++i)
		  {
		    value_type sum = LU[i][j];
		    for (std::size_t k = 0; k < j; ++k)
		      {
			sum -= LU[i][k] * LU[k][j];
		      }
		    LU[i][j] = sum;
		    norm_type tmp = Vv[i] * std::abs(sum);
		    if (tmp >= max)
		      {
			max = tmp;
			imax = i;
		      }
		  }
		// Invert columns
		if (j != imax)
		  {
		    for (std::size_t k = 0; k < nr; ++k)
		      {
			std::swap(LU[imax][k],LU[j][k]);
		      }
		    Vv[imax] = Vv[j];

		    std::swap(Indx[j],Indx[imax]);
		    npivot = - npivot;
		  }
	
		//replace 0 values by epsilon
		if (LU[j][j] == value_type(0))
		  {
		    LU[j][j] = std::numeric_limits<value_type>::epsilon();
		  }
		if (j != (nr-1))
		  {
		    value_type scal = value_type(1) / LU[j][j];
		    for (std::size_t i =j+1; i < nr; ++i)
		      {
			LU[i][j]*=scal;
		      }
		  }
	      }
	    return npivot;

  }


  /**
     Computes the LU decomposition according to rows permutations of a matrix using Crout method
   ** \f[
   ** \left(
   ** \begin{array}{cccc}
   ** m_{1,1} & \cdots&\cdots&m_{1,n}\\
   ** \vdots&&&\vdots\\
   ** \vdots&&&\vdots\\
   ** m_{n,1}&\cdots&\cdots&m_{n,n}\\
   ** \end{array}\right) 
   ** = P \left(
   ** \begin{array}{cccc}
   ** 1&0&\cdots&0\\
   ** l_{2,1}&\ddots&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&0\\
   ** l_{n,1}&\cdots&l_{n,n-1}&1\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{cccc}
   ** u_{1,1}&\cdots&\cdots&u_{1,n}\\
   ** 0&\ddots&&\vdots\\
   ** \vdots&\ddots&\ddots&\vdots\\
   ** 0&\cdots&0&u_{n,n}\\
   ** \end{array}\right) 
   ** \f]
   ** LU is composed of L and U as following:
   ** \f[
   ** LU = 
   ** \left(
   ** \begin{array}{cccc}
   ** u_{1,1}&\cdots&\cdots&u_{1,n}\\
   ** l_{2,1}&\ddots&&\vdots\\
   ** \vdots&\ddots&\ddots&\vdots\\
   ** l_{n,1}&\cdots&l_{n,n-1}&u_{n,n}\\
   ** \end{array}\right) 
   ** \f]
   ** P is a permutation matrix needed in case of partial pivoting during the algorithm.
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2007/11/16
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param M Matrix container
   ** \param L Matrix container for lower triangular
   ** \param U Matrix container for upper triangular
   ** \param P Matrix container for permutation matrix
   ** \return +1 if row interchanges was even, -1 if odd
   ** \pre M.rows() == M.cols()
   ** \pre L.rows() == L.cols()
   ** \pre U.rows() == U.cols()
   ** \pre M.rows() == L.rows()
   ** \pre M.rows() == U.rows()
   ** \pre P.rows() == P.cols()
   ** \pre P.cols() == L.rows()
   ** \remarks Works with real and complex data.
   ** \remarks Raise an exception if M is a singular matrix.
   ** \par Complexity:
   ** \par Example:
   ** \code
   ** std::size_t size=5;  
   ** slip::Matrix<double> M(size,size);
   ** slip::Matrix<double> L(size,size);
   ** slip::Matrix<double> U(size,size);
   ** slip::Matrix<double> P(size,size);
   ** slip::Matrix<double> LU(size,size);
   ** slip::Matrix<double> PLU(size,size);
   **
   ** std::generate(M.begin(),M.end(),std::rand);
   ** std::cout<<std::endl;
   ** std::cout <<"M = \n"<<M<<std::endl;
   ** slip::lu(M,L,U,P);	
   ** std::cout <<"L =\n"<<L<<std::endl;
   ** std::cout <<"U =\n"<<U<<std::endl;
   ** std::cout <<"P =\n"<<P<<std::endl;
   ** slip::matrix_matrix_multiplies(L,U,LU);
   ** slip::matrix_matrix_multiplies(P,LU,PLU);
   ** std::cout <<"P =\n"<<P<<std::endl;
   ** std::cout <<"PLU =\n"<<PLU<<std::endl;
   ** \endcode
   */  
  template <class Matrix1, class Matrix2>
  inline
  int lu(const Matrix1 & M, 
	 Matrix2 & L,
	 Matrix2 & U,
	 Matrix2 & P)
  {
    assert(M.rows() == M.cols());
    assert(L.rows() == L.cols());
    assert(U.rows() == U.cols());
    assert(P.rows() == P.cols());
    assert(M.rows() == L.rows());
    assert(M.rows() == U.rows());
    assert(M.rows() == P.cols());
	
    std::size_t nr = M.rows();
    slip::Array<std::size_t> Indx(nr);
    //decompostion LU
    int sign = slip::lu(M,U,Indx);
    // Generate L and P
    for (std::size_t i = 0; i < nr; ++i)
      {
	for (std::size_t j=0; j < i; ++j)
	  {
	    L[i][j] = U[i][j];
	    U[i][j] = 0;
	    P[i][j] = 0;
	  }
      }
    for (std::size_t i = 0; i < nr; ++i)
      {
	L[i][i] = 1;
	P[Indx[i]][i] = 1;
      }
    return sign;
  }


  /**
   ** \brief Solve Linear Equation using LU decomposition
   **
   ** Solve : A.X=B
   ** This function need results from lu method to works
   **
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param LU Matrix container comes from lu method (A in the equation)
   ** \param nr The number of rows of LU
   ** \param nc The number of columns of LU 
   ** \param Indx Permutation vector comes from UDecomposition method
   ** \param nv1 he number of rows of Indx
   ** \param B Vector to match
   ** \param nv2 he number of rows of B
   ** \param X Result Vector
   ** \param nv3 he number of rows of X
   **
   ** \pre Matrix must have the double bracket element accessor 
   ** \pre Matrix are supposed to be allocated first
   ** \pre Vector must have the simple bracket element accessor 
   ** \pre Vector are supposed to be allocated first
   ** \pre nr == nc
   ** \pre nv1 == nv2
   ** \pre nv2 == nv3
   ** \pre nr == nv1
   ** \internal
   */  
  template <class Matrix, class Vector1, class Vector2, class Vector3>
  inline
  void lu_solve(const Matrix LU, 
		const std::size_t nr,
		const std::size_t nc,
		const Vector1 & Indx,
		const std::size_t nv1,
		const Vector2 & B,
		const std::size_t nv2,
		Vector3 & X,
		const std::size_t nv3)
  {
	assert(nr==nc);
	assert(nv1==nv2);
	assert(nv2==nv3);
	assert(nr==nv1); 
	
	// Solving LY=B
	for (std::size_t i = 0; i < nr; ++i)
	  {
	    // Permut value
	    X[i] = B[Indx[i]];

	    for (std::size_t j = 0; j < i; ++j)
	      {
		X[i] -= LU[i][j] * X[j];
			
	      }
	  }
	// Solving UX=Y : std::size_t always positive so check if <nr for loop ending
	for (std::size_t i = nr-1; i < nr; i--)
	  {
	    for (std::size_t j=i+1;j<nr;++j)
	      {
		X[i] -= LU[i][j] * X[j];
	      }
	    X[i] /= LU[i][i];
	}

  }

  /**
   ** \brief Solve Linear Equation AX = B using LU decomposition
   **
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A The Matrix of the linear system.
   ** \param X Result Vector.
   ** \param B The vector B of the linear system.
   ** \pre A.rows() == A.cols()
   ** \pre A.cols() == X.size()
   ** \pre B.size() == X.size()
   ** \remarks Works with real and complex data.
   ** \remarks Raise an exception if the matrix A is singular.
   ** \par Complexity:
   ** \par Example:
   ** \code
   ** double ddet[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0};
   ** slip::Matrix<double> Mdet(3,3,ddet);
   ** std::cout<<"Mdet \n"<<Mdet<<std::endl;
   ** double bdet[] ={1.0, 2.0, 3.0};
   ** slip::Vector<double> Bdet(3,bdet);
   ** slip::Vector<double> Xdet(3);
   ** std::cout<<"Bdet \n"<<Bdet<<std::endl;
   ** slip::lu_solve(Mdet,Xdet,Bdet);
   ** std::cout<<"Xdet \n"<<Xdet<<std::endl;
   ** std::cout<<"Check: "<<std::endl;
   ** slip::Vector<double> Bdet2(3);
   ** slip::matrix_vector_multiplies(Mdet,Xdet,Bdet2);
   ** std::cout<<"Bdet2 \n"<<Bdet2<<std::endl;
   ** \endcode
   */  
  template <class Matrix, class Vector1, class Vector2>
  inline
  void lu_solve(const Matrix& A, 
		Vector1& X,
		const Vector2& B)
  {
    std::size_t nr = A.rows();
    std::size_t nc = A.cols();
    
    Matrix LU(nr,nc);
    slip::Array<std::size_t> Indx(nr);
    slip::lu(A,LU,Indx);
    // Solving LY=B
    for (std::size_t i = 0; i < nr; ++i)
      {
	// Permut value
	X[i] = B[Indx[i]];
	
	for (std::size_t j = 0; j < i; ++j)
	  {
	    X[i] -= LU[i][j] * X[j];
	  }
      }
    // Solving UX=Y:
    for (std::size_t i = nr-1; i < nr; i--)
      {
	for (std::size_t j = i+1;j < nr; ++j)
	  {
	    X[i] -= LU[i][j] * X[j];
	  }
	X[i] /= LU[i][i];
      }
  }


  /**
   ** \brief Computes the inverse of a matrix using LU decomposition.
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/11/14
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M Matrix container
   ** \param IM result of the inversion
   ** \pre M.rows() == M.cols()
   ** \pre IM.rows() == IM.cols()
   ** \pre M.rows() == IM.rows()
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is singular.
   ** \par Complexity:
   ** \par Example:
   ** \code
   ** std::size_t size = 5;  
   ** slip::Matrix<double> M(size,size);
   ** slip::Matrix<double> IM(size,size);
   ** slip::Matrix<double> MxIM(size,size);
   ** std::generate(M.begin(),M.end(),std::rand);
   ** std::cout<<"M = \n"<<M<<std::endl;
   ** slip::lu_inv(M,IM);	
   ** std::cout<<"IM = \n"<<IM<<std::endl;
   ** slip::matrix_matrix_multiplies(M,IM,MxIM);
   ** std::cout <<"M x IM = \n"<<std::endl<< MxIM<<std::endl;
   ** \endcode
   */  
  template <class Matrix1,class Matrix2>
  inline
  void lu_inv(const Matrix1 & M, 
		  Matrix2 & IM)
  {
    assert(M.rows() == M.cols());
    assert(IM.rows() == IM.cols());
    assert(M.rows() == IM.rows());

    std::size_t nr1 = M.rows();
    std::size_t nc1 = M.cols();

  typedef typename Matrix1::value_type value_type;

  slip::Array<std::size_t> permut(nr1);
  slip::Array2d<value_type> LU(nr1,nc1);
  slip::Array<value_type> B(nr1);
  slip::Array<value_type> X(nr1);

 

   lu(M,LU,permut);

   for (std::size_t j = 0; j < nc1; ++j)
     {
	for (std::size_t i = 0; i < nr1; ++i)
	  {
	    B[i] = value_type(0.0);
	  }
	B[j] = value_type(1.0);
	lu_solve(LU,nr1,nc1,permut,nr1,B,nr1,X,nr1);
	for (std::size_t i = 0;i < nr1; ++i) 
	  {
	    IM[i][j] = X[i];
	  }
     }
  }



  /**
   ** \brief Computes the determinant of a matrix using LU decomposition
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \date 2006/12/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M Matrix container
   ** \return T determinant of the matrix M.
   **
   ** \pre M.rows() == M.cols()
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if M is singular.
   ** \par Example:
   ** \code
   ** double ddet[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0};
   ** slip::Matrix<double> Mdet(3,3,ddet);
   ** std::cout<<"Mdet =\n"<<Mdet<<std::endl;
   ** std::cout <<"lu_det(Mdet) = "<<lu_det(Mdet)<<std::endl;
   ** \endcode
   */  
  template <class Matrix1>
  inline
  typename Matrix1::value_type 
  lu_det(const Matrix1& M)
  {
    assert(M.rows() == M.cols());
    std::size_t nr1 = M.rows();
    std::size_t nc1 = M.cols();
     
    typedef typename Matrix1::value_type value_type;
    value_type det = 1.0;
    int sign = 0;
    slip::Array2d<value_type> LU(nr1,nc1);
    slip::Array<std::size_t> permut(nr1);
    sign = lu(M,LU,permut);
    for (std::size_t i=0;i<nr1;++i)
      {
	det *= LU[i][i];
      }

    if(sign < 0)
      {
	det = -det;
      }
    return det;
  }

 
/* @} */

 /** \name Triangular matrix algorithms */
  /* @{ */
 
  /**
   ** \brief Solve the linear system U*X=B when U is upper triangular
   **  \f[
   ** \left(
   ** \begin{array}{cccc}
   ** u_{1,1} & u_{1,2}&\cdots&u_{1,n}\\
   ** 0 & u_{2,2} & \ddots&\vdots\\
   ** \vdots&\ddots&\ddots&u_{n-1,n}\\
   ** 0&\cdots&0&u_{n,n}\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** x_{2}\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** b_{2}\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptor
   ** \date 2009/02/27
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param U_up 2D iterator corresponding to the upper_left element
   ** of the matrix U
   ** \param U_bot 2D iterator corresponding to one past-the-end bottom_right of the
   ** matrix U
   ** \param X_first RandomAccessIterator point to the first element of X.
   ** \param X_last RandomAccessIterator point  one-past-the-end element of X.
   ** \param B_first RandomAccessIterator point to the first element of B.
   ** \param B_last RandomAccessIterator to one-past-the-end element of B.
   ** \param precision
   ** \pre U must be upper triangular
   ** \pre (U_bot - U_up)[0] == (U_bot - U_up)[1]
   ** \pre (U_bot - U_up)[1] == (X_last - X_first)
   ** \pre (X_last - X_first) == (B_last - B_first)
   ** \par Complexity: n^2 flops
   ** \remarks If U is singular an exception will be raised.
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** std::complex<double> dup2c[] = {TC(2.0,0.5), TC(1.0,0.3), TC(5.0,1.2),
   **                              TC(0.0,0.0), TC(1.0,2.1), TC(3.0,-0.1),
   **	                   TC(0.0,0.0), TC(0.0,0.0), TC(2.0,4.1)};
   ** slip::Array2d<std::complex<double> > Aupc(3,3,dup2c);
   ** std::cout<<"Aupc ="<<std::endl;
   ** std::cout<<Aupc<<std::endl;
   ** slip::Array<std::complex<double> > Xupc(3);
   ** std::complex<double> bupc[] = {TC(6.0,0.5),TC(2.0,1.1),TC(5.0,4.7)};
   ** slip::Array<std::complex<double> > Bupc(3,bupc);
   ** std::cout<<"Bupc ="<<std::endl;
   ** std::cout<<Bupc<<std::endl;
   ** slip::upper_triangular_solve(Aupc.upper_left(),Aupc.bottom_right(),
   **			           Xupc.begin(),Xupc.end(),
   **			           Bupc.begin(),Bupc.end(),10E-6);
   ** std::cout<<"Xupc = "<<Xupc<<std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Array<std::complex<double> > Bup2c(3);
   ** slip::matrix_vector_multiplies(Aupc,Xupc,Bup2c);
   ** std::cout<<"Aupc * Xupc = "<<std::endl;
   ** std::cout<<Bup2c<<std::endl;
   ** \endcode
   */  
   template <typename MatrixIterator,typename RandomAccessIterator1,typename RandomAccessIterator2>
  inline
  void upper_triangular_solve(MatrixIterator U_up,
			      MatrixIterator U_bot,
			      RandomAccessIterator1 X_first,
			      RandomAccessIterator1 X_last,
			      RandomAccessIterator2 B_first,
			      RandomAccessIterator2 B_last,
			      typename slip::lin_alg_traits<typename MatrixIterator::value_type>::value_type precision)
  {
    assert((U_bot - U_up)[0] == (U_bot - U_up)[1]);
    assert((U_bot - U_up)[1] == (X_last - X_first));
    assert((X_last - X_first) == (B_last - B_first));
    
   
	typedef typename MatrixIterator::value_type _T;

	int U_rows_m1 =  int((U_bot - U_up)[0]) - 1;
	int U_rows_m2 =  U_rows_m1 - 1;
	slip::DPoint2d<int> d(U_rows_m1,U_rows_m1);

	//bottom right value inside the box
	_T botr = U_up[d];
	if(std::abs(botr) < precision)
	  {
	    
	    throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	  }
	//computes the last value of X
	*(--X_last) = *(--B_last) / botr;
	--X_last;
	--B_last;
       	for(int i =  U_rows_m2; i >= 0 ; i--, X_last--, B_last--)
	  {
	    d.set_coord(i,i);
	    _T uii = U_up[d];
	    _T inv_uii = _T(1.0)/uii;
	    if(std::abs(uii) < precision)
	      {
		throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	      }
	    int istart = (i+1);
	    _T sum = std::inner_product(U_up.row_begin(i) + istart,U_up.row_end(i),
					X_first + istart,_T());
	    *X_last = (*B_last - sum) * inv_uii;
	  }
    

  }


  /**
   ** \brief Solve the linear system U*X=B when U is upper triangular
   **  \f[
   ** \left(
   ** \begin{array}{cccc}
   ** u_{1,1} & u_{1,2}&\cdots&u_{1,n}\\
   ** 0 & u_{2,2} & \ddots&\vdots\\
   ** \vdots&\ddots&\ddots&u_{n-1,n}\\
   ** 0&\cdots&0&u_{n,n}\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** x_{2}\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** b_{2}\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/08/25
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param U 2D container
   ** \param X 1D container
   ** \param B 1D container
   ** \param precision 
   ** \pre U must be upper triangular
   ** \pre U.row() == U.cols()
   ** \pre U.cols() == X.size()
   ** \pre X.size() == B.size()
   ** \remarks If U is singular an exception will be raised.
   ** \remarks Works with real and complex data.
   ** \par Complexity: n^2 flops
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** std::complex<double> dup2c[] = {TC(2.0,0.5), TC(1.0,0.3), TC(5.0,1.2),
   **                              TC(0.0,0.0), TC(1.0,2.1), TC(3.0,-0.1),
   **	                   TC(0.0,0.0), TC(0.0,0.0), TC(2.0,4.1)};
   ** slip::Array2d<std::complex<double> > Aupc(3,3,dup2c);
   ** std::cout<<"Aupc ="<<std::endl;
   ** std::cout<<Aupc<<std::endl;
   ** slip::Array<std::complex<double> > Xupc(3);
   ** std::complex<double> bupc[] = {TC(6.0,0.5),TC(2.0,1.1),TC(5.0,4.7)};
   ** slip::Array<std::complex<double> > Bupc(3,bupc);
   ** std::cout<<"Bupc ="<<std::endl;
   ** std::cout<<Bupc<<std::endl;
   ** slip::upper_triangular_solve(Aupc,Xupc,Bupc,10E-6);
   ** std::cout<<"Xupc = "<<Xupc<<std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Array<std::complex<double> > Bup2c(3);
   ** slip::matrix_vector_multiplies(Aupc,Xupc,Bup2c);
   ** std::cout<<"Aupc * Xupc = "<<std::endl;
   ** std::cout<<Bup2c<<std::endl;
   ** \endcode
   */  
  template <class Matrix,class Vector1,class Vector2>
  inline
  void upper_triangular_solve(const Matrix & U, 
			      Vector1 & X, 
			      const Vector2 & B,
			      typename slip::lin_alg_traits<typename Matrix::value_type>::value_type precision)
  {
     slip::upper_triangular_solve(U.upper_left(),U.bottom_right(),
				 X.begin(),X.end(),
				 B.begin(),B.end(),
				 precision);
  }

  /**
   ** \brief Invert an upper triangular Matrix
    **  \f[
   ** \left(
   ** \begin{array}{cccc}
   ** u_{1,1} & u_{1,2}&\cdots&u_{1,n}\\
   ** 0 & u_{2,2} & \ddots&\vdots\\
   ** \vdots&\ddots&\ddots&u_{n-1,n}\\
   ** 0&\cdots&0&u_{n,n}\\
   ** \end{array}\right)^{-1} 
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator corresponding to the upper_left element
   ** of the matrix A
   ** \param A_bot 2D iterator corresponding to one past-the-end bottom_right of the  matrix A
   ** \param Ainv_up 2D iterator corresponding to the upper_left element
   ** of the inverse matrix.
   ** \param Ainv_bot 2D iterator corresponding to one past-the-end bottom_right of the  inverse matrix.
   ** \param precision 1.0E-6 by default.
   ** \pre (A_bot - A_up)[0]   == (A_bot - A_up)[1];
   ** \pre (Ainv_bot - Ainv_up)[0]   == (Ainv_bot - Ainv_up)[1];
   ** \pre (A_bot - A_up)[0] == (Ainv_bot - Ainv_up)[0]
   ** \pre (A_bot - A_up)[1] == (Ainv_bot - Ainv_up)[1]
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is singular
   ** \par Complexity: n^3 flops
   ** \par Example:
   ** \code
   ** double dup2[] = {2.0, 3.0, 4.5, 0.0, 1.0, 3.0, 0.0, 0.0, 2.0};
   ** slip::Array2d<double> Aup(3,3,dup2);
   ** std::cout<<"Aup =\n "<<Aup<<std::endl;
   ** slip::Array2d<double> invAup(3,3);
   ** slip::upper_triangular_inv(Aup.upper_left(),Aup.bottom_right(),
   **			      invAup.upper_left(),invAup.bottom_right(),
   **		      1.0e-8);
   ** std::cout<<"invAup = \n"<<invAup<<std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Array2d<float> IAup(3,3);
   ** slip::matrix_matrix_multiplies(Aup,invAup,IAup);
   ** std::cout<<"Aup * invAup = \n"<<IAup<<std::endl;
   ** \endcode
   */
  template <typename MatrixIterator1,
	    typename MatrixIterator2>
  inline
  void upper_triangular_inv(MatrixIterator1 A_up,
		    MatrixIterator1 A_bot,
		    MatrixIterator2 Ainv_up,
		    MatrixIterator2 Ainv_bot,
		    typename slip::lin_alg_traits<typename MatrixIterator1::value_type>::value_type precision = typename slip::lin_alg_traits<typename MatrixIterator1::value_type>::value_type(1.0E-6))
  {
    assert((A_bot - A_up)[0]   == (A_bot - A_up)[1]);
    assert((Ainv_bot - Ainv_up)[0]   == (Ainv_bot - Ainv_up)[1]);
    assert((A_bot - A_up)[0] == (Ainv_bot - Ainv_up)[0]);
    assert((A_bot - A_up)[1] == (Ainv_bot - Ainv_up)[1]);

   
	const std::size_t n = std::size_t((A_bot - A_up)[0]);
	//init Ainv with the identity matrix
	slip::identity(Ainv_up,Ainv_bot);
	
	
	//solve the n systems 
	for(std::size_t k = 0; k < n; ++k)
	  {
	    //resolution of LY = B
	    slip::upper_triangular_solve(A_up,
					 A_bot,
					 Ainv_up.col_begin(k),
					 Ainv_up.col_end(k),
					 Ainv_up.col_begin(k),
					 Ainv_up.col_end(k),
					 precision);
	    
	  }
    

  }
/**
 ** \brief Invert an upper triangular Matrix
    **  \f[
   ** \left(
   ** \begin{array}{cccc}
   ** u_{1,1} & u_{1,2}&\cdots&u_{1,n}\\
   ** 0 & u_{2,2} & \ddots&\vdots\\
   ** \vdots&\ddots&\ddots&u_{n-1,n}\\
   ** 0&\cdots&0&u_{n,n}\\
   ** \end{array}\right)^{-1} 
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A The matrix to invert.
   ** \param Ainv The inverted matrix.
   ** \param precision 1.0E-6 by default.
   ** \pre A.rows() == A.cols()
   ** \pre Ainv.rows() == Ainv.cols()
   ** \pre A.rows() == Ainv.rows()
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is singular
   ** \par Complexity: n^3 flops
   ** \par Example:
   ** \code
   ** double dup2[] = {2.0, 3.0, 4.5, 0.0, 1.0, 3.0, 0.0, 0.0, 2.0};
   ** slip::Array2d<double> Aup(3,3,dup2);
   ** std::cout<<"Aup =\n "<<Aup<<std::endl;
   ** slip::Array2d<double> invAup(3,3);
   ** slip::upper_triangular_inv(Aup,invAup,1.0e-8);
   ** std::cout<<"invAup = \n"<<invAup<<std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Array2d<float> IAup(3,3);
   ** slip::matrix_matrix_multiplies(Aup,invAup,IAup);
   ** std::cout<<"Aup * invAup = \n"<<IAup<<std::endl;
   ** \endcode
   */
  template <typename Matrix1,
	    typename Matrix2>
  inline
  void upper_triangular_inv(Matrix1 A,
			    Matrix2 Ainv,
			    typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type precision = typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type(1.0E-6))
  {
    slip::upper_triangular_inv(A.upper_left(),A.bottom_right(),
			       Ainv.upper_left(),Ainv.bottom_right(),
			       precision);
  }
   /**
   ** \brief Solve the linear system L*X=B when L is lower triangular
   **  \f[
   ** \left(
   ** \begin{array}{cccc}
   ** l_{1,1} & 0&\cdots&0\\
   ** l_{2,1} & l_{2,2} & \ddots&\vdots\\
   ** \vdots&\ddots&\ddots&0\\
   ** l_{n,1}&\cdots&l_{n,n-1}&l_{n,n}\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** x_{2}\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** b_{2}\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptor
   ** \date 2009/02/27
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param L_up 2D iterator corresponding to the upper_left element
   ** of the matrix L
   ** \param L_bot 2D iterator corresponding to one past-the-end bottom_right of the
   ** matrix L
   ** \param X_first RandomAccessIterator point to the first element of X.
   ** \param X_last RandomAccessIterator point  one-past-the-end element of X.
   ** \param B_first RandomAccessIterator point to the first element of B.
   ** \param B_last RandomAccessIterator to one-past-the-end element of B.
   ** \param precision
   ** \pre L must be lower triangular
   ** \pre (L_bot - L_up)[0] == (L_bot - L_up)[1]
   ** \pre (L_bot - L_up)[1] == (X_last - X_first)
   ** \pre (X_last - X_first) == (B_last - B_first)
   ** \remarks If L is singular an exception will be raised.
   ** \remarks Works with real and complex data.
   ** \par Complexity: n^2 flops
   ** \par Example:
   ** \code
   ** double dlow[] = {2.0, 0.0, 0.0, 1.0, 5.0, 0.0, 7.0, 9.0, 8.0};
   ** slip::Array2d<double> Alow(3,3,dlow);
   ** std::cout<<"Alow ="<<std::endl;
   ** std::cout<<Alow<<std::endl;
   ** slip::Array<double> Xlow(3);
   ** double blow[] = {6.0,2.0,5.0};
   ** slip::Array<double> Blow(3,blow);
   ** std::cout<<"Blow ="<<std::endl;
   ** std::cout<<Blow<<std::endl;
   ** slip::lower_triangular_solve(Alow.upper_left(),Alow.bottom_right(),
   **             		   Xlow.begin(),Xlow.end(),
   **				   Blow.begin(),Blow.end(),10E-6);
   ** std::cout<<"Xlow = "<<Xlow<<std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Array<float> Blow2(3);
   ** slip::matrix_vector_multiplies(Alow,Xlow,Blow2);
   ** std::cout<<"Alow * Xlow = "<<std::endl;
   ** std::cout<<Blow2<<std::endl;
   ** \endcode
   */  
  template <typename MatrixIterator,typename RandomAccessIterator1,typename RandomAccessIterator2>
  inline
  void lower_triangular_solve(MatrixIterator L_up,
			      MatrixIterator L_bot,
			      RandomAccessIterator1 X_first,
			      RandomAccessIterator1 X_last,
			      RandomAccessIterator2 B_first,
			      RandomAccessIterator2 B_last,
			      typename slip::lin_alg_traits<typename MatrixIterator::value_type>::value_type precision)
  {
    assert((L_bot - L_up)[0] == (L_bot - L_up)[1]);
    assert((L_bot - L_up)[1] == (X_last - X_first));
    assert((X_last - X_first) == (B_last - B_first));
    
   
	typedef typename MatrixIterator::value_type _T;

	RandomAccessIterator1 X_first_tmp = X_first;
	int rows = int((L_bot - L_up)[0]);
	_T L00 = *L_up;
	if(std::abs(L00) < precision)
	  {
	    throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	  }
	slip::DPoint2d<int> d(1,1);
	*(X_first++) = *(B_first++) / L00;
	for(int i = 1; i < rows ; ++i, ++X_first, ++B_first)
	  { 
	    d.set_coord(i,i);
	    _T Lii = L_up[d];
	    _T inv_Lii = _T(1.0)/Lii;
	    if(std::abs(Lii) < precision)
	      {
		throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	      }
	    _T sum = std::inner_product(L_up.row_begin(i),L_up.row_begin(i)+i,X_first_tmp,_T());

	    *X_first = (*B_first - sum) * inv_Lii;
	  }

     
  }

   /**
   ** \brief Solve the linear system L*X=B when L is lower triangular
   **  \f[
   ** \left(
   ** \begin{array}{cccc}
   ** l_{1,1} & 0&\cdots&0\\
   ** l_{2,1} & l_{2,2} & \ddots&\vdots\\
   ** \vdots&\ddots&\ddots&0\\
   ** l_{n,1}&\cdots&l_{n,n-1}&l_{n,n}\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** x_{2}\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** b_{2}\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param L 2D container
   ** \param X 1D container
   ** \param B 1D container
   ** \param precision 
   ** \par Complexity: n^2 flops
   ** \par Remarks: This algorihtm is also called forward substitution
   ** algorithm.
    ** \pre L must be lower triangular
   ** \pre L.rows() == L.cols()
   ** \pre L.cols() == X.size()
   ** \pre X.size() == B.size()
   ** \remarks If L is singular an exception will be raised.
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** double dlow[] = {2.0, 0.0, 0.0, 1.0, 5.0, 0.0, 7.0, 9.0, 8.0};
   ** slip::Array2d<double> Alow(3,3,dlow);
   ** std::cout<<"Alow ="<<std::endl;
   ** std::cout<<Alow<<std::endl;
   ** slip::Array<double> Xlow(3);
   ** double blow[] = {6.0,2.0,5.0};
   ** slip::Array<double> Blow(3,blow);
   ** std::cout<<"Blow ="<<std::endl;
   ** std::cout<<Blow<<std::endl;
   ** slip::lower_triangular_solve(Alow,Xlow,Blow,10E-6);
   ** std::cout<<"Xlow = "<<Xlow<<std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Array<float> Blow2(3);
   ** slip::matrix_vector_multiplies(Alow,Xlow,Blow2);
   ** std::cout<<"Alow * Xlow = "<<std::endl;
   ** std::cout<<Blow2<<std::endl;
   ** \endcode
   */  
  template <class Matrix,class Vector1,class Vector2>
  inline
  void lower_triangular_solve(const Matrix & L, 
			      Vector1 & X, 
			      const Vector2 & B, 
			      typename slip::lin_alg_traits<typename Matrix::value_type>::value_type precision)
  {
    slip::lower_triangular_solve(L.upper_left(),L.bottom_right(),
				 X.begin(),X.end(),
				 B.begin(),B.end(),
				 precision);
  }
  

/**
   ** \brief Invert a lower triangular Matrix
    **  \f[
   ** \left(
   ** \begin{array}{cccc}
   ** l_{1,1} & 0&\cdots&0\\
   ** l_{2,1} & l_{2,2} & \ddots&\vdots\\
   ** \vdots&\ddots&\ddots&0\\
   ** l_{n,1}&\cdots&l_{n,n-1}&l_{n,n}\\
   ** \end{array}\right)^{-1}
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator corresponding to the upper_left element
   ** of the matrix A
   ** \param A_bot 2D iterator corresponding to one past-the-end bottom_right of the  matrix A
   ** \param Ainv_up 2D iterator corresponding to the upper_left element
   ** of the inverse matrix.
   ** \param Ainv_bot 2D iterator corresponding to one past-the-end bottom_right of the  inverse matrix.
   ** \param precision 1.0E-6 by default.
   ** \pre (A_bot - A_up)[0]   == (A_bot - A_up)[1];
   ** \pre (Ainv_bot - Ainv_up)[0]   == (Ainv_bot - Ainv_up)[1];
   ** \pre (A_bot - A_up)[0] == (Ainv_bot - Ainv_up)[0]
   ** \pre (A_bot - A_up)[1] == (Ainv_bot - Ainv_up)[1]
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is singular
   ** \par Complexity: n^3 flops
   ** \par Example:
   ** \code
   ** double dlow[] = {2.0, 0.0, 0.0, 1.0, 5.0, 0.0, 7.0, 9.0, 8.0};
   ** slip::Array2d<double> Alow(3,3,dlow);
   ** slip::Array2d<double> invAlow(3,3);
   ** std::cout<<"Alow \n="<<Alow<<std::endl;
   ** slip::lower_triangular_inv(Alow.upper_left(),Alow.bottom_right(),
   **		      invAlow.upper_left(),invAlow.bottom_right(),
   **		      1.0e-6);
   ** std::cout<<"invAlow \n="<<invAlow<<std::endl;
   ** slip::Array2d<double> IAlow(3,3);
   ** slip::matrix_matrix_multiplies(Alow,invAlow,IAlow);
   ** std::cout<<"Alow invAlow = \n"<<IAlow<<std::endl;
   ** \endcode
   */
  template <typename MatrixIterator1,
	    typename MatrixIterator2>
  inline
  void lower_triangular_inv(MatrixIterator1 A_up,
		    MatrixIterator1 A_bot,
		    MatrixIterator2 Ainv_up,
		    MatrixIterator2 Ainv_bot,
		    typename slip::lin_alg_traits<typename MatrixIterator1::value_type>::value_type precision = typename slip::lin_alg_traits<typename MatrixIterator1::value_type>::value_type(1.0E-6))
  {
    assert((A_bot - A_up)[0]   == (A_bot - A_up)[1]);
    assert((Ainv_bot - Ainv_up)[0]   == (Ainv_bot - Ainv_up)[1]);
    assert((A_bot - A_up)[0] == (Ainv_bot - Ainv_up)[0]);
    assert((A_bot - A_up)[1] == (Ainv_bot - Ainv_up)[1]);

   	const std::size_t n = std::size_t((A_bot - A_up)[0]);
	//init Ainv with the identity matrix
	slip::identity(Ainv_up,Ainv_bot);
	
	
	//solve the n systems 
	for(std::size_t k = 0; k < n; ++k)
	  {
	    //resolution of LY = B
	    slip::lower_triangular_solve(A_up,
					 A_bot,
					 Ainv_up.col_begin(k),
					 Ainv_up.col_end(k),
					 Ainv_up.col_begin(k),
					 Ainv_up.col_end(k),
					 precision);
	    
	  }
  }

/**
 ** \brief Invert a lower triangular Matrix
   **  \f[
   ** \left(
   ** \begin{array}{cccc}
   ** l_{1,1} & 0&\cdots&0\\
   ** l_{2,1} & l_{2,2} & \ddots&\vdots\\
   ** \vdots&\ddots&\ddots&0\\
   ** l_{n,1}&\cdots&l_{n,n-1}&l_{n,n}\\
   ** \end{array}\right)^{-1}
   ** \f]   
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A The matrix to invert.
   ** \param Ainv The inverted matrix.
   ** \param precision 1.0E-6 by default.
   ** \pre A.rows() == A.cols()
   ** \pre Ainv.rows() == Ainv.cols()
   ** \pre A.rows() == Ainv.rows()
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is singular
   ** \par Complexity: n^3 flops
   ** \par Example:
   ** \code
   ** double dlow[] = {2.0, 0.0, 0.0, 1.0, 5.0, 0.0, 7.0, 9.0, 8.0};
   ** slip::Array2d<double> Alow(3,3,dlow);
   ** slip::Array2d<double> invAlow(3,3);
   ** std::cout<<"Alow \n="<<Alow<<std::endl;
   ** slip::lower_triangular_inv(Alow,invAlow,1.0e-6);
   ** std::cout<<"invAlow \n="<<invAlow<<std::endl;
   ** slip::Array2d<double> IAlow(3,3);
   ** slip::matrix_matrix_multiplies(Alow,invAlow,IAlow);
   ** std::cout<<"Alow invAlow = \n"<<IAlow<<std::endl;
   ** \endcode
   */
  template <typename Matrix1,
	    typename Matrix2>
  inline
  void lower_triangular_inv(const Matrix1& A,
			    Matrix2& Ainv,
			    typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type precision = typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type(1.0E-6))
  {
    slip::lower_triangular_inv(A.upper_left(),A.bottom_right(),
			       Ainv.upper_left(),Ainv.bottom_right(),
			       precision);
  }
   /**
   ** \brief Solve the linear system U*X=B when U is unit upper triangular
   **  \f[
   ** \left(
   ** \begin{array}{cccc}
   ** 1 & u_{1,2}&\cdots&u_{1,n}\\
   ** 0 & 1 & \ddots&\vdots\\
   ** \vdots&\ddots&\ddots&u_{n-1,n}\\
   ** 0&\cdots&0&1\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** x_{2}\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** b_{2}\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptor
   ** \date 2009/02/27
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param U_up 2D iterator corresponding to the upper_left element
   ** of the matrix U
   ** \param U_bot 2D iterator corresponding to one past-the-end bottom_right of the
   ** matrix U
   ** \param X_first RandomAccessIterator point to the first element of X.
   ** \param X_last RandomAccessIterator point  one-past-the-end element of X.
   ** \param B_first RandomAccessIterator point to the first element of B.
   ** \param B_last RandomAccessIterator to one-past-the-end element of B.
   ** \pre U should be unit upper triangular
   ** \pre (U_bot - U_up)[0] == (U_bot - U_up)[1]
   ** \pre (U_bot - U_up)[1] == (X_last - X_first)
   ** \pre (X_last - X_first) == (B_last - B_first)
   ** \par Complexity: n^2 flops
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** std::complex<double> dup2c[] = {TC(1.0,0.0), TC(1.0,0.3), TC(5.0,1.2),
   **                              TC(0.0,0.0), TC(1.0,0.0), TC(3.0,-0.1),
   **	                   TC(0.0,0.0), TC(0.0,0.0), TC(1.0,1.0)};
   ** slip::Array2d<std::complex<double> > Aupc(3,3,dup2c);
   ** std::cout<<"Aupc ="<<std::endl;
   ** std::cout<<Aupc<<std::endl;
   ** slip::Array<std::complex<double> > Xupc(3);
   ** std::complex<double> bupc[] = {TC(6.0,0.5),TC(2.0,1.1),TC(5.0,4.7)};
   ** slip::Array<std::complex<double> > Bupc(3,bupc);
   ** std::cout<<"Bupc ="<<std::endl;
   ** std::cout<<Bupc<<std::endl;
   ** slip::unit_upper_triangular_solve(Aupc.upper_left(),Aupc.bottom_right(),
   **			           Xupc.begin(),Xupc.end(),
   **			           Bupc.begin(),Bupc.end());
   ** std::cout<<"Xupc = "<<Xupc<<std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Array<std::complex<double> > Bup2c(3);
   ** slip::matrix_vector_multiplies(Aupc,Xupc,Bup2c);
   ** std::cout<<"Aupc * Xupc = "<<std::endl;
   ** std::cout<<Bup2c<<std::endl;
   ** \endcode
   */  
   template <typename MatrixIterator,typename RandomAccessIterator1,typename RandomAccessIterator2>
  inline
  void unit_upper_triangular_solve(MatrixIterator U_up,
				   MatrixIterator U_bot,
				   RandomAccessIterator1 X_first,
				   RandomAccessIterator1 X_last,
				   RandomAccessIterator2 B_first,
				   RandomAccessIterator2 B_last)
  {
    assert((U_bot - U_up)[0] == (U_bot - U_up)[1]);
    assert((U_bot - U_up)[1] == (X_last - X_first));
    assert((X_last - X_first) == (B_last - B_first));
    
   
	typedef typename MatrixIterator::value_type _T;

	int U_rows_m1 =  int((U_bot - U_up)[0]) - 1;
	int U_rows_m2 =  U_rows_m1 - 1;
	slip::DPoint2d<int> d(U_rows_m1,U_rows_m1);

	
	//computes the last value of X
	*(--X_last) = *(--B_last);
	--X_last;
	--B_last;
       	for(int i =  U_rows_m2; i >= 0 ; i--, X_last--, B_last--)
	  {
	    d.set_coord(i,i);

	    int istart = (i+1);
	    _T sum = std::inner_product(U_up.row_begin(i) + istart,
					U_up.row_end(i),
					X_first + istart,_T());
	    *X_last = (*B_last - sum) ;
	  }
  }

  /**
   ** \brief Solve the linear system U*X=B when U is unit upper triangular
   **  \f[
   ** \left(
   ** \begin{array}{cccc}
   ** 1 & u_{1,2}&\cdots&u_{1,n}\\
   ** 0 & 1 & \ddots&\vdots\\
   ** \vdots&\ddots&\ddots&u_{n-1,n}\\
   ** 0&\cdots&0&1\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** x_{2}\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** b_{2}\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/01
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param U 2D container
   ** \param X 1D container
   ** \param B 1D container
   ** \pre U should be unit upper triangular
   ** \pre U.rows() == U.cols()
   ** \pre U.cols() == X.size()
   ** \pre X.size() == B.size()
   ** \par Complexity: n^2 flops
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** std::complex<double> dup2c[] = {TC(1.0,0.0), TC(1.0,0.3), TC(5.0,1.2),
   **                              TC(0.0,0.0), TC(1.0,0.0), TC(3.0,-0.1),
   **	                   TC(0.0,0.0), TC(0.0,0.0), TC(1.0,1.0)};
   ** slip::Array2d<std::complex<double> > Aupc(3,3,dup2c);
   ** std::cout<<"Aupc ="<<std::endl;
   ** std::cout<<Aupc<<std::endl;
   ** slip::Array<std::complex<double> > Xupc(3);
   ** std::complex<double> bupc[] = {TC(6.0,0.5),TC(2.0,1.1),TC(5.0,4.7)};
   ** slip::Array<std::complex<double> > Bupc(3,bupc);
   ** std::cout<<"Bupc ="<<std::endl;
   ** std::cout<<Bupc<<std::endl;
   ** slip::unit_upper_triangular_solve(Aupc,Xupc,Bupc);
   ** std::cout<<"Xupc = "<<Xupc<<std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Array<std::complex<double> > Bup2c(3);
   ** slip::matrix_vector_multiplies(Aupc,Xupc,Bup2c);
   ** std::cout<<"Aupc * Xupc = "<<std::endl;
   ** std::cout<<Bup2c<<std::endl;
   ** \endcode
   */  
  template <class Matrix,class Vector1,class Vector2>
  inline
  void unit_upper_triangular_solve(const Matrix & U, 
				   Vector1 & X, 
				   const Vector2 & B)
  {
 
    slip::unit_upper_triangular_solve(U.upper_left(),U.bottom_right(),
				      X.begin(),X.end(),
				      B.begin(),B.end());
  }


   /**
   ** \brief Solve the linear system L*X=B when L is unit lower triangular
   **  \f[
   ** \left(
   ** \begin{array}{cccc}
   ** 1 & 0&\cdots&0\\
   ** l_{2,1} & 1 & \ddots&\vdots\\
   ** \vdots&\ddots&\ddots&0\\
   ** l_{n,1}&\cdots&l_{n,n-1}&1\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** x_{2}\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** b_{2}\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptor
   ** \date 2009/02/27
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param L_up 2D iterator corresponding to the upper_left element
   ** of the matrix L
   ** \param L_bot 2D iterator corresponding to one past-the-end bottom_right of the
   ** matrix L
   ** \param X_first RandomAccessIterator point to the first element of X.
   ** \param X_last RandomAccessIterator point  one-past-the-end element of X.
   ** \param B_first RandomAccessIterator point to the first element of B.
   ** \param B_last RandomAccessIterator to one-past-the-end element of B.
   ** \pre L must be lower triangular
   ** \pre (L_bot - L_up)[0] == (L_bot - L_up)[1]
   ** \pre (L_bot - L_up)[1] == (X_last - X_first)
   ** \pre (X_last - X_first) == (B_last - B_first)
    ** \remarks Works with real and complex data.
   ** \par Complexity: n^2 flops
   ** \par Example:
   ** \code
   ** double dlow[] = {2.0, 0.0, 0.0, 1.0, 5.0, 0.0, 7.0, 9.0, 8.0};
   ** slip::Array2d<double> Alow(3,3,dlow);
   ** std::cout<<"Alow ="<<std::endl;
   ** std::cout<<Alow<<std::endl;
   ** slip::Array<double> Xlow(3);
   ** double blow[] = {6.0,2.0,5.0};
   ** slip::Array<double> Blow(3,blow);
   ** std::cout<<"Blow ="<<std::endl;
   ** std::cout<<Blow<<std::endl;
   ** slip::unit_lower_triangular_solve(Alow.upper_left(),Alow.bottom_right(),
   **             		   Xlow.begin(),Xlow.end(),
   **				   Blow.begin(),Blow.end(),10E-6);
   ** std::cout<<"Xlow = "<<Xlow<<std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Array<float> Blow2(3);
   ** slip::matrix_vector_multiplies(Alow,Xlow,Blow2);
   ** std::cout<<"Alow * Xlow = "<<std::endl;
   ** std::cout<<Blow2<<std::endl;
   ** \endcode
   */  
  template <typename MatrixIterator,typename RandomAccessIterator1,typename RandomAccessIterator2>
  inline
  void unit_lower_triangular_solve(MatrixIterator L_up,
			      MatrixIterator L_bot,
			      RandomAccessIterator1 X_first,
			      RandomAccessIterator1 X_last,
			      RandomAccessIterator2 B_first,
			      RandomAccessIterator2 B_last)
  {
    assert((L_bot - L_up)[0] == (L_bot - L_up)[1]);
    assert((L_bot - L_up)[1] == (X_last - X_first));
    assert((X_last - X_first) == (B_last - B_first));
    
   
    typedef typename MatrixIterator::value_type _T;
    
    RandomAccessIterator1 X_first_tmp = X_first;
    int rows = int((L_bot - L_up)[0]);
    
    slip::DPoint2d<int> d(1,1);
    *(X_first++) = *(B_first++);
    for(int i = 1; i < rows ; ++i, ++X_first, ++B_first)
      { 
	d.set_coord(i,i);
	
	_T sum = std::inner_product(L_up.row_begin(i),L_up.row_begin(i)+i,X_first_tmp,_T());
	
	*X_first = (*B_first - sum);
      }
  }


  /**
   ** \brief Solve the linear system L*X=B when L is unit lower triangular
   **  \f[
   ** \left(
   ** \begin{array}{cccc}
   ** 1 & 0&\cdots&0\\
   ** l_{2,1} & 1 & \ddots&\vdots\\
   ** \vdots&\ddots&\ddots&0\\
   ** l_{n,1}&\cdots&l_{n,n-1}&1\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** x_{2}\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** b_{2}\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param L 2D container
   ** \param X 1D container
   ** \param B 1D container
    ** \pre L should be unit lower triangular
   ** \pre L.rows() == L.cols()
   ** \pre L.cols() == X.size()
   ** \pre X.size() == B.size()
   ** \par Complexity: n^2 flops
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** double dllow[] = {1.0, 0.0, 0.0, 2.0, 1.0, 0.0, 3.0, 4.0, 1.0};
   ** slip::Array2d<double> Allow(3,3,dllow);
   ** std::cout<<"Allow ="<<std::endl;
   ** std::cout<<Allow<<std::endl;
   ** slip::Array<double> Xllow(3);
   ** double bllow[] = {6.0,2.0,5.0};
   ** slip::Array<double> Bllow(3,bllow);
   ** std::cout<<"Bllow ="<<std::endl;
   ** std::cout<<Bllow<<std::endl;
   ** slip::unit_lower_triangular_solve(Allow,Xllow,Bllow);
   ** std::cout<<"Xllow = "<<Xllow<<std::endl;
   ** slip::unit_lower_triangular_solve(Allow,Xllow,Bllow);
   ** std::cout<<"Xllow = "<<Xllow<<std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Array<float> Bllow2(3);
   ** slip::matrix_vector_multiplies(Allow,Xllow,Bllow2);
   ** std::cout<<"Allow * Xllow = "<<std::endl;
   ** std::cout<<Bllow2<<std::endl;
   ** \endcode
   */  
  template <class Matrix,class Vector1,class Vector2>
  inline
  void unit_lower_triangular_solve(const Matrix & L, 
			      Vector1 & X, 
			      const Vector2 & B)
  {
    slip::unit_lower_triangular_solve(L.upper_left(),L.bottom_right(),
				      X.begin(),X.end(),
				      B.begin(),B.end());
  }

/* @} */

/** \name Diagonal matrix algorithms */
  /* @{ */
  /**
   ** \brief Solve the linear system D*X=B when D a diagonal matrix
   **  \f[
   ** \left(
   ** \begin{array}{cccc}
   ** d_{1,1} & 0&\cdots&0\\
   ** 0 & d_{2,2} & \ddots&\vdots\\
   ** \vdots&\ddots&\ddots&0\\
   ** 0&\cdots&0&d_{n,n}\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** x_{2}\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** b_{2}\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/06
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param first RandomAccessIterator to the first element of D.
   ** \param last RandomAccessIterator to one-past-the-end element of D.
   ** \param X_first RandomAccessIterator to the first element of X.
   ** \param X_last RandomAccessIterator to one-past-the-end element of X.
   ** \param B_first RandomAccessIterator to the first element of B.
   ** \param B_last RandomAccessIterator to one-past-the-end element of B.
   ** \param precision
   ** \pre (last - first) == (X_last - X_first)
   ** \pre (B_last - B_first) == (X_last - X_first)
   ** \remarks Raise an exception if the matrix is singular
   ** \remarks Works with real and complex data.
   ** \par Complexity: n flops
   ** \par Example:
   ** \code
   ** double diagonal[] = {1.0,2.0,4.0};
   ** slip::Array<double> Ddiag(3,diagonal);
   ** std::cout<<"Ddiag ="<<std::endl;
   ** std::cout<<Ddiag<<std::endl;
   ** slip::Array<double> Xdiag(3);
   ** double bdiag[] = {6.0,2.0,5.0};
   ** slip::Array<double> Bdiag(3,bdiag);
   ** std::cout<<"Bdiag ="<<std::endl;
   ** std::cout<<Bdiag<<std::endl;
   ** slip::diagonal_solve(Ddiag.begin(),Ddiag.end(),
   **			Xdiag.begin(),Xdiag.end(),
   **			Bdiag.begin(),Bdiag.end(),1.0E-6);
   ** std::cout<<"Xdiag = "<<Xdiag<<std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Array<float> Bdiag2(3);
   ** slip::Array2d<float> Adiag(3,3);
   ** slip::set_diagonal(Ddiag.begin(),Ddiag.end(),Adiag);
   ** std::cout<<"Adiag ="<<std::endl;
   ** std::cout<<Adiag<<std::endl;
   ** slip::matrix_vector_multiplies(Adiag,Xdiag,Bdiag2);
   ** std::cout<<"Adiag * Xdiag = "<<std::endl;
   ** std::cout<<Bdiag2<<std::endl;
   ** \endcode
   **
   */  
  template <typename RandomAccessIterator1,
	    typename RandomAccessIterator2,
	    typename RandomAccessIterator3>
  inline
  void diagonal_solve(RandomAccessIterator1 first,
		      RandomAccessIterator1 last,
		      RandomAccessIterator2 X_first,
		      RandomAccessIterator2 X_last,
		      RandomAccessIterator3 B_first,
		      RandomAccessIterator3 B_last,
		      typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type precision)
  {

    assert((last - first) == (X_last - X_first));
    assert((B_last - B_first) == (X_last - X_first));
    
   
	//Computes X
	for(; first != last; ++first, ++X_first, ++B_first)
	  {
	    if(std::abs(*first) < precision)
	      {
		throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
		
	      }
	    *X_first = *B_first / *first;
	  }
  }
/* @} */

/** \name Band matrix algorithms */
  /* @{ */

/**
   ** \brief  in place Band LU decomposition a square band Matrix A
   **  \f$ A = LU\f$ with L a p lower band matrix and U a q upper band matrix
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : concepteur
   ** \date 2009/03/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator corresponding to the upper_left element
   ** of the matrix A
   ** \param A_bot 2D iterator corresponding to one past-the-end bottom_right of the  matrix A
   ** \param p Width of the lower band of A.
   ** \param q Width of the upper_band of A
   ** \param Ind_first RandomAccessIterator to the first element of the permutation vector.
 ** \param Ind_last RandomAccessIterator to one past-the-end element of the permutation vector.
   ** \param precision
   ** \pre (A_bot - A_up)[0]   == (A_bot - A_up)[1];
   ** \pre (A_bot - A_up)[1]   == (Ind_last - Ind_first)
   ** \pre p < (A_bot - A_up)[0]
   ** \pre q < (A_bot - A_up)[0]
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is singular.
   ** \remarks a partial pivoting strategy is used.
   ** \par Complexity: npq - pq^2/2 - p^3/6 + pn if p <= q, npq -qp^2/2 - q^3/6 + qn if p > q
   
   */  
template <typename MatrixIterator1,
	  typename RandomAccessIterator>
  inline
  int band_lu(MatrixIterator1 A_up,
	       MatrixIterator1 A_bot,
	       const int p,
	       const int q,
	       RandomAccessIterator Ind_first,
	       RandomAccessIterator Ind_last,
	       typename slip::lin_alg_traits<typename MatrixIterator1::value_type>::value_type precision )
  {
    assert((A_bot - A_up)[0]   == (A_bot - A_up)[1]);
    assert((A_bot - A_up)[1]   == (Ind_last - Ind_first));
    assert(p < int((A_bot - A_up)[0]));
    assert(q < int((A_bot - A_up)[0]));
  
	typedef typename MatrixIterator1::value_type value_type;
	//typedef typename slip::lin_alg_traits<typename MatrixIterator1::value_type>::value_type real_type;
	const int A_rows =  int((A_bot - A_up)[0]);
	const int A_rows_m1 = A_rows - 1;
	slip::DPoint2d<int> d(0,0);
	slip::DPoint2d<int> d2(0,0);
	slip::DPoint2d<int> dstart(0,0);
	slip::DPoint2d<int> dstop(0,0);
	int nb_pivot = 1;
	slip::iota(Ind_first,Ind_last,0);
	
	//value_type proj = value_type();
	for(int k = 0; k < A_rows_m1; ++k)
	  {
	    d.set_coord(k,k);
	    int istart =  k + 1;
	    int istop  = std::min(k+p,A_rows_m1) + 1;
	    int jstart = istart;
	    int jstop  = std::min(k+p+q,A_rows_m1) + 1;
	    dstart.set_coord(k,k);
	    dstop.set_coord(istop,jstop);
	    
	    int pivot = slip::partial_pivot(A_up + dstart,A_up + dstop);
	    if(pivot == -1)
	      {
		throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
		
	      }

	    if((pivot > 0) && (pivot != k))
	      {
		std::swap_ranges(A_up.row_begin(k),A_up.row_end(k),
				 A_up.row_begin(pivot));
		nb_pivot -= 1;
		std::swap(Ind_first[k],Ind_first[pivot]);
	      }
	    value_type Akk = A_up[d];
	    if(std::abs(Akk) < precision)
	      {
		throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	      }
	    value_type inv_Akk = value_type(1) / Akk;
	    slip::vector_scalar_multiplies(A_up.col_begin(k)+istart,
					   A_up.col_begin(k)+istop,
					   inv_Akk,
					   A_up.col_begin(k)+istart);
	   
	    for(int i = istart; i < istop; ++i)
	      {
		d2.set_coord(i,k);
		slip::reduction(A_up.row_begin(i)+jstart,
				A_up.row_begin(i)+jstop,
				A_up.row_begin(k)+jstart,
				-A_up[d2]);
	      }
	  }
	
	d.set_coord(A_rows_m1,A_rows_m1);
	if(std::abs(A_up[d]) < precision)
	  {
	    throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	
	  }
	return nb_pivot;   
   
  }
	   
/**
   ** \brief  in place Band LU decomposition a square band Matrix A
   **  \f$ A = PLU\f$ with L a p lower band matrix and U a q upper band matrix and P the permutation matrix.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/05
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M The matrix M.
   ** \param p Width of the lower band of A.
   ** \param q Width of the upper_band of A.
   ** \param L The matrix L.
   ** \param U The matrix U.
   ** \param P The matrix P.
   ** \param precision
   ** \pre A must be square 
   ** \pre (A_bot - A_up)[0]   == (A_bot - A_up)[1];
   ** \pre (A_bot - A_up)[1]   == (Ind_last - Ind_first)
   ** \pre p < (A_bot - A_up)[0]
   ** \pre q < (A_bot - A_up)[0]
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is singular.
   ** \remarks a partial pivoting strategy is used.
   ** \par Complexity: npq - pq^2/2 - p^3/6 + pn if p <= q, npq -qp^2/2 - q^3/6 + qn if p > q
   ** \par Example:
   ** \code
   ** slip::Array2d<double> Aband(5,5);
   ** slip::fill_diagonal(Aband,1.0,0);
   ** slip::fill_diagonal(Aband,2.0,1);
   ** slip::fill_diagonal(Aband,-2.0,-1);
   ** slip::fill_diagonal(Aband,3.0,2);
   ** slip::fill_diagonal(Aband,-3.0,-2);
   ** std::cout<<"Aband = \n"<<Aband<<std::endl;
   ** slip::Array2d<double> AbandL(5,5);
   ** slip::Array2d<double> AbandU(5,5);
   ** slip::Array2d<double> Pband(5,5);
   ** slip::Array<int> Indxband(5);
   ** slip::band_lu(Aband,2,2,AbandL,AbandU,Pband,1.0E-6);
   ** std::cout<<"Pband = \n"<<Pband<<std::endl;
   ** std::cout<<"AbandL = \n"<<AbandL<<std::endl;
   ** std::cout<<"AbandU = \n"<<AbandU<<std::endl;
   ** slip::Array2d<double> PxAbandL(5,5);
   ** slip::matrix_matrix_multiplies(Pband,AbandL,PxAbandL);
   ** slip::Array2d<double> PxAbandLxAbandU(5,5);
   ** slip::matrix_matrix_multiplies(PxAbandL,AbandU, PxAbandLxAbandU);
   ** std::cout<<"PxAbandLxAbandU = \n"<<PxAbandLxAbandU<<std::endl;
   **\endcode
   */  
template <class Matrix1, class Matrix2, class Matrix3, class Matrix4>
  inline
  int band_lu(const Matrix1 & M, 
	      const int p,
	      const int q,
	      Matrix2 & L,
	      Matrix3 & U,
	      Matrix4 & P,
	      typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type precision )
      {
    assert(M.rows() == M.cols());
    assert(L.rows() == L.cols());
    assert(U.rows() == U.cols());
    assert(P.rows() == P.cols());
    assert(M.rows() == L.rows());
    assert(M.rows() == U.rows());
    assert(M.rows() == P.cols());
	
    std::size_t nr = M.rows();
    slip::Array<std::size_t> Indx(nr);

    std::copy(M.begin(),M.end(),U.begin());
    //decompostion LU
    int sign = slip::band_lu(U.upper_left(),
			     U.bottom_right(),
			     p,q,
			     Indx.begin(),
			     Indx.end(),
			     precision);
    // Generate L and P
    for (std::size_t i = 0; i < nr; ++i)
      {
	for (std::size_t j=0; j < i; ++j)
	  {
	    L[i][j] = U[i][j];
	    U[i][j] = 0;
	    P[i][j] = 0;
	  }
      }
    for (std::size_t i = 0; i < nr; ++i)
      {
	L[i][i] = 1;
	P[Indx[i]][i] = 1;
      }
    return sign;
  }
 /**
   ** \brief Solve the linear system U*X=B when U is q width upper banded
   **  \f[
   ** \left(
   ** \begin{array}{cccccc}
   ** u_{1,1} & \cdots&u_{1,q+1}&0&\cdots&0\\
   ** 0 & u_{2,2} &\ddots&\ddots&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&\ddots&\ddots&0\\
   ** \vdots&&\ddots&\ddots&\ddots&u_{n-q,n}\\
   ** \vdots&&&\ddots&\ddots&\vdots\\
   ** 0&\cdots&\cdots&\cdots&0&u_{n,n}\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** x_{2}\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** b_{2}\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param U_up 2D iterator corresponding to the upper_left element
   ** of the matrix U
   ** \param U_bot 2D iterator corresponding to one past-the-end bottom_right of the
   ** matrix U
   ** \param q Size of the upper band.
   ** \param X_first RandomAccessIterator point to the first element of X.
   ** \param X_last RandomAccessIterator point  one-past-the-end element of X.
   ** \param B_first RandomAccessIterator point to the first element of B.
   ** \param B_last RandomAccessIterator to one-past-the-end element of B.
   ** \param precision
   ** \pre U must be upper triangular
   ** \pre (U_bot - U_up)[0] == (U_bot - U_up)[1]
   ** \pre (U_bot - U_up)[1] == (X_last - X_first)
   ** \pre (X_last - X_first) == (B_last - B_first)
   ** \pre q < int((U_bot - U_up)[0])
   ** \par Complexity: n(q+1) - q^2/2 flops
   ** \remarks If U is singular an exception will be raised.
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Guband(5,5);
   ** slip::hilbert(Guband);
   ** slip::fill_diagonal(Guband,0.0,-1);
   ** slip::fill_diagonal(Guband,0.0,-2);
   ** slip::fill_diagonal(Guband,0.0,-3);
   ** slip::fill_diagonal(Guband,0.0,-4);
   ** slip::fill_diagonal(Guband,0.0,4);
   ** slip::fill_diagonal(Guband,0.0,3);
   ** double buband[] = {22.0,7.0,4.5,1.0,2.0};
   ** slip::Vector<double> Buband(5,buband);
   ** slip::Vector<double> Xuband(5);
   ** std::cout<< "Guband : " <<std::endl;
   ** std::cout<< Guband <<std::endl;
   ** std::cout<< "Buband : " <<std::endl;
   ** std::cout<< Buband <<std::endl;
   ** slip::upper_band_solve(Guband.upper_left(),Guband.bottom_right(),
   **		 2,
   **		 Xuband.begin(),Xuband.end(),
   **		 Buband.begin(),Buband.end(),1.0e-06);
   ** std::cout<< "Xuband : \n" <<Xuband<<std::endl;
   ** slip::Vector<double> Buband2(5);
   ** std::cout<< "Check: "<<std::endl;
   ** slip::matrix_vector_multiplies(Guband,Xuband,Buband2);
   ** std::cout<< "Buband2 : \n" <<Buband2<<std::endl;
   ** \endcode
   */  
template <typename MatrixIterator,typename RandomAccessIterator1,typename RandomAccessIterator2>
  inline
  void upper_band_solve(MatrixIterator U_up,
			MatrixIterator U_bot,
			int q,
			RandomAccessIterator1 X_first,
			RandomAccessIterator1 X_last,
			RandomAccessIterator2 B_first,
			RandomAccessIterator2 B_last,
			typename slip::lin_alg_traits<typename MatrixIterator::value_type>::value_type precision)
  {
    assert((U_bot - U_up)[0] == (U_bot - U_up)[1]);
    assert((U_bot - U_up)[1] == (X_last - X_first));
    assert((X_last - X_first) == (B_last - B_first));
    assert( q < int((U_bot - U_up)[0]));

    
	typedef typename MatrixIterator::value_type _T;

	int U_rows_m1 =  int((U_bot - U_up)[0]) - 1;
	int U_rows_m2 =  U_rows_m1 - 1;
	slip::DPoint2d<int> d(U_rows_m1,U_rows_m1);

	//bottom right value inside the box
	_T botr = U_up[d];
	if(std::abs(botr) < precision)
	  {
	    
	    throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	  }
	//computes the last value of X
	*(--X_last) = *(--B_last) / botr;
	--X_last;
	--B_last;
       	for(int i =  U_rows_m2; i >= 0 ; i--, X_last--, B_last--)
	  {
	    d.set_coord(i,i);
	    _T uii = U_up[d];
	    _T inv_uii = _T(1.0)/uii;
	    if(std::abs(uii) < precision)
	      {
		throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	      }
	    int istart = (i+1);
	    int istop  = std::min(i+q,U_rows_m1) + 1; 
	    _T sum = std::inner_product(U_up.row_begin(i) + istart,U_up.row_begin(i) + istop,X_first + istart,_T());
	    *X_last = (*B_last - sum) * inv_uii;
	  }
   

  }
  /**
   ** \brief Solve the linear system U*X=B when U is unit q width upper banded
   **  \f[
   ** \left(
   ** \begin{array}{cccccc}
   ** 1 & \cdots&u_{1,q+1}&0&\cdots&0\\
   ** 0 & 1 &\ddots&\ddots&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&\ddots&\ddots&0\\
   ** \vdots&&\ddots&\ddots&\ddots&u_{n-q,n}\\
   ** \vdots&&&\ddots&\ddots&\vdots\\
   ** 0&\cdots&\cdots&\cdots&0&1\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** x_{2}\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** b_{2}\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param U_up 2D iterator corresponding to the upper_left element
   ** of the matrix U
   ** \param U_bot 2D iterator corresponding to one past-the-end bottom_right of the
   ** matrix U
   ** \param q Size of the upper band.
   ** \param X_first RandomAccessIterator point to the first element of X.
   ** \param X_last RandomAccessIterator point  one-past-the-end element of X.
   ** \param B_first RandomAccessIterator point to the first element of B.
   ** \param B_last RandomAccessIterator to one-past-the-end element of B.
   ** \param precision
   ** \pre U must be upper triangular
   ** \pre (U_bot - U_up)[0] == (U_bot - U_up)[1]
   ** \pre (U_bot - U_up)[1] == (X_last - X_first)
   ** \pre (X_last - X_first) == (B_last - B_first)
   ** \pre q < int((U_bot - U_up)[0])
   ** \par Complexity: n(q+1) - q^2/2 flops
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Guuband(5,5);
   ** slip::hilbert(Guuband);
   ** slip::fill_diagonal(Guuband,0.0,-1);
   ** slip::fill_diagonal(Guuband,0.0,-2);
   ** slip::fill_diagonal(Guuband,0.0,-3);
   ** slip::fill_diagonal(Guuband,0.0,-4);
   ** slip::fill_diagonal(Guuband,0.0,4);
   ** slip::fill_diagonal(Guuband,0.0,3);
   ** slip::fill_diagonal(Guuband,1.0);
   **
   ** double buuband[] = {22.0,7.0,4.5,1.0,2.0};
   ** slip::Vector<double> Buuband(5,buuband);
   ** slip::Vector<double> Xuuband(5);
   ** std::cout<< "Guuband : " <<std::endl;
   ** std::cout<< Guuband <<std::endl;
   ** std::cout<< "Buuband : " <<std::endl;
   ** std::cout<< Buuband <<std::endl;
   ** slip::upper_band_solve(Guuband.upper_left(),Guuband.bottom_right(),
   **		 2,
   **		 Xuuband.begin(),Xuuband.end(),
   **		 Buuband.begin(),Buuband.end(),1.0e-06);
   ** std::cout<< "Xuuband : \n" <<Xuuband<<std::endl;
   ** slip::Vector<double> Buuband2(5);
   ** std::cout<< "Check: "<<std::endl;
   ** slip::matrix_vector_multiplies(Guuband,Xuuband,Buuband2);
   ** std::cout<< "Buuband2 : \n" <<Buuband2<<std::endl;
   ** \endcode
   */  
 template <typename MatrixIterator,typename RandomAccessIterator1,typename RandomAccessIterator2>
  inline
  void unit_upper_band_solve(MatrixIterator U_up,
			MatrixIterator U_bot,
			int q,
			RandomAccessIterator1 X_first,
			RandomAccessIterator1 X_last,
			RandomAccessIterator2 B_first,
			RandomAccessIterator2 B_last,
			typename slip::lin_alg_traits<typename MatrixIterator::value_type>::value_type precision)
  {
    assert((U_bot - U_up)[0] == (U_bot - U_up)[1]);
    assert((U_bot - U_up)[1] == (X_last - X_first));
    assert((X_last - X_first) == (B_last - B_first));
    assert( q < int((U_bot - U_up)[0]));

   
	typedef typename MatrixIterator::value_type _T;

	int U_rows_m1 =  int((U_bot - U_up)[0]) - 1;
	int U_rows_m2 =  U_rows_m1 - 1;
	slip::DPoint2d<int> d(U_rows_m1,U_rows_m1);

	
	//computes the last value of X
	*(--X_last) = *(--B_last);
	--X_last;
	--B_last;
       	for(int i =  U_rows_m2; i >= 0 ; i--, X_last--, B_last--)
	  {
	    d.set_coord(i,i);
	    int istart = (i+1);
	    int istop  = std::min(i+q,U_rows_m1) + 1; 
	    _T sum = std::inner_product(U_up.row_begin(i) + istart,U_up.row_begin(i) + istop,X_first + istart,_T());
	    *X_last = (*B_last - sum);
	  }
   

  }


 /**
   ** \brief Solve the linear system L*X=B when L is p lower banded
   **  \f[
   ** \left(
   ** \begin{array}{ccccccc}
   ** l_{1,1} & 0&\cdots&\cdots&\cdots&\cdots&0\\
   ** l_{2,1} & l_{2,2} & \ddots&&&&\vdots\\
   ** \vdots&&\ddots&\ddots&&&\vdots\\
   ** l_{p+1,1}&&&\ddots&\ddots&&\vdots\\
   ** 0&\ddots&&&\ddots&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&&&l_{n-1,n-1}&0\\
   ** 0&\cdots&0&l_{n,n-q}&\cdots&l_{n,n-1}&l_{n,n}\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** x_{2}\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** b_{2}\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param L_up 2D iterator corresponding to the upper_left element
   ** of the matrix L
   ** \param L_bot 2D iterator corresponding to one past-the-end bottom_right of the
   ** matrix L
   ** \param p Width of the lower band.
   ** \param X_first RandomAccessIterator point to the first element of X.
   ** \param X_last RandomAccessIterator point  one-past-the-end element of X.
   ** \param B_first RandomAccessIterator point to the first element of B.
   ** \param B_last RandomAccessIterator to one-past-the-end element of B.
   ** \param precision
   ** \pre L must be lower triangular
   ** \pre (L_bot - L_up)[0] == (L_bot - L_up)[1]
   ** \pre (L_bot - L_up)[1] == (X_last - X_first)
   ** \pre (X_last - X_first) == (B_last - B_first)
   ** \pre p < int((L_bot - L_up)[0])
   ** \remarks If L is singular an exception will be raised.
   ** \remarks Works with real and complex data.
   ** \par Complexity: np - p^2/2 flops
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Glband(5,5);
   ** slip::hilbert(Glband);
   ** slip::fill_diagonal(Glband,0.0,1);
   ** slip::fill_diagonal(Glband,0.0,2);
   ** slip::fill_diagonal(Glband,0.0,3);
   ** slip::fill_diagonal(Glband,0.0,4);
   ** slip::fill_diagonal(Glband,0.0,-4);
   ** slip::fill_diagonal(Glband,0.0,-3);
   ** double blband[] = {22.0,7.0,4.5,1.0,2.0};
   ** slip::Vector<double> Blband(5,blband);
   ** slip::Vector<double> Xlband(5);
   ** std::cout<< "Glband : " <<std::endl;
   ** std::cout<< Glband <<std::endl;
   ** std::cout<< "Blband : " <<std::endl;
   ** std::cout<< Blband <<std::endl;
   ** slip::lower_band_solve(Glband.upper_left(),Glband.bottom_right(),
   **			     2,
   **		 	     Xlband.begin(),Xlband.end(),
   **		             Blband.begin(),Blband.end(),1.0e-06);
   ** std::cout<< "Xlband : \n" <<Xlband<<std::endl;
   ** slip::Vector<double> Blband2(5);
   ** std::cout<< "Check: "<<std::endl;
   ** slip::matrix_vector_multiplies(Glband,Xlband,Blband2);
   ** std::cout<< "Blband2 : \n" <<Blband2<<std::endl;
   ** \endcode
   */  
  template <typename MatrixIterator,typename RandomAccessIterator1,typename RandomAccessIterator2>
  inline
  void lower_band_solve(MatrixIterator L_up,
			MatrixIterator L_bot,
			int p,
			RandomAccessIterator1 X_first,
			RandomAccessIterator1 X_last,
			RandomAccessIterator2 B_first,
			RandomAccessIterator2 B_last,
			typename slip::lin_alg_traits<typename MatrixIterator::value_type>::value_type precision)
  {
    assert((L_bot - L_up)[0] == (L_bot - L_up)[1]);
    assert((L_bot - L_up)[1] == (X_last - X_first));
    assert((X_last - X_first) == (B_last - B_first));
    assert( p < int((L_bot - L_up)[0]));

   
	typedef typename MatrixIterator::value_type _T;

	RandomAccessIterator1 X_first_tmp = X_first;
	int rows = int((L_bot - L_up)[0]);
	_T L00 = *L_up;
	if(std::abs(L00) < precision)
	  {
	    throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	  }
	slip::DPoint2d<int> d(1,1);
	*(X_first++) = *(B_first++) / L00;
	for(int i = 1; i < rows ; ++i, ++X_first, ++B_first)
	  { 
	    d.set_coord(i,i);
	    _T Lii = L_up[d];
	    _T inv_Lii = _T(1.0)/Lii;
	    int istart = std::max(0,i-p);
	    if(std::abs(Lii) < precision)
	      {
		throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	      }

	    _T sum = std::inner_product(L_up.row_begin(i)+istart,L_up.row_begin(i)+i,X_first_tmp+istart,_T());

	    *X_first = (*B_first - sum) * inv_Lii;
	  }

   
    
  }


/**
   ** \brief Solve the linear system L*X=B when L is unit p lower banded
    **  \f[
   ** \left(
   ** \begin{array}{ccccccc}
   ** 1 & 0&\cdots&\cdots&\cdots&\cdots&0\\
   ** l_{2,1} & 1 & \ddots&&&&\vdots\\
   ** \vdots&\ddots&\ddots&\ddots&&&\vdots\\
   ** l_{p+1,1}&&\ddots&\ddots&\ddots&&\vdots\\
   ** 0&\ddots&&\ddots&\ddots&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&&\ddots&1&0\\
   ** 0&\cdots&0&l_{n,n-q}&\cdots&l_{n,n-1}&1\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** x_{2}\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** b_{2}\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/03
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param L_up 2D iterator corresponding to the upper_left element
   ** of the matrix L
   ** \param L_bot 2D iterator corresponding to one past-the-end bottom_right of the
   ** matrix L
   ** \param p Width of the lower band.
   ** \param X_first RandomAccessIterator point to the first element of X.
   ** \param X_last RandomAccessIterator point  one-past-the-end element of X.
   ** \param B_first RandomAccessIterator point to the first element of B.
   ** \param B_last RandomAccessIterator to one-past-the-end element of B.
   ** \pre L must be lower triangular
   ** \pre (L_bot - L_up)[0] == (L_bot - L_up)[1]
   ** \pre (L_bot - L_up)[1] == (X_last - X_first)
   ** \pre (X_last - X_first) == (B_last - B_first)
   ** \pre p < int((L_bot - L_up)[0])
   ** \remarks If L is singular an exception will be raised.
   ** \remarks Works with real and complex data.
   ** \par Complexity: np - p^2/2 flops
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Gulband(5,5);
   ** slip::hilbert(Gulband);
   ** slip::fill_diagonal(Gulband,0.0,1);
   ** slip::fill_diagonal(Gulband,0.0,2);
   ** slip::fill_diagonal(Gulband,0.0,3);
   ** slip::fill_diagonal(Gulband,0.0,4);
   ** slip::fill_diagonal(Gulband,0.0,-4);
   ** slip::fill_diagonal(Gulband,0.0,-3);
   ** slip::fill_diagonal(Gulband,1.0);
   ** double bulband[] = {22.0,7.0,4.5,1.0,2.0};
   ** slip::Vector<double> Bulband(5,bulband);
   ** slip::Vector<double> Xulband(5);
   ** std::cout<< "Gulband : " <<std::endl;
   ** std::cout<< Gulband <<std::endl;
   ** std::cout<< "Bulband : " <<std::endl;
   ** std::cout<< Bulband <<std::endl;
   ** slip::unit_lower_band_solve(Gulband.upper_left(),Gulband.bottom_right(),
   **		 2,
   **		 Xulband.begin(),Xulband.end(),
   **		 Bulband.begin(),Bulband.end(),1.0e-06);
   ** std::cout<< "Xulband : \n" <<Xulband<<std::endl;
   ** slip::Vector<double> Bulband2(5);
   ** std::cout<< "Check: "<<std::endl;
   ** slip::matrix_vector_multiplies(Gulband,Xulband,Bulband2);
   ** std::cout<< "Bulband2 : \n" <<Bulband2<<std::endl;
   ** \endcode
   */  
 template <typename MatrixIterator,typename RandomAccessIterator1,typename RandomAccessIterator2>
  inline
  void unit_lower_band_solve(MatrixIterator L_up,
			MatrixIterator L_bot,
			int p,
			RandomAccessIterator1 X_first,
			RandomAccessIterator1 X_last,
			RandomAccessIterator2 B_first,
			RandomAccessIterator2 B_last)
  {
    assert((L_bot - L_up)[0] == (L_bot - L_up)[1]);
    assert((L_bot - L_up)[1] == (X_last - X_first));
    assert((X_last - X_first) == (B_last - B_first));
    assert( p < int((L_bot - L_up)[0]));

   
	typedef typename MatrixIterator::value_type _T;

	RandomAccessIterator1 X_first_tmp = X_first;
	int rows = int((L_bot - L_up)[0]);

	slip::DPoint2d<int> d(1,1);
	*(X_first++) = *(B_first++);
	for(int i = 1; i < rows ; ++i, ++X_first, ++B_first)
	  { 
	    d.set_coord(i,i);
	   
	    int istart = std::max(0,i-p);
	   
	    _T sum = std::inner_product(L_up.row_begin(i)+istart,L_up.row_begin(i)+i,X_first_tmp+istart,_T());

	    *X_first = (*B_first - sum);
	  }

    
  }


//TO FIX!!!
/* 
 template <typename MatrixIterator,
	     typename RandomAccessIterator1,
	     typename RandomAccessIterator2>
  inline
  void band_lu_solve(MatrixIterator A_up,
		      MatrixIterator A_bot,
		     const int p,
		     const int q,
		      RandomAccessIterator1 X_first,
		      RandomAccessIterator1 X_last,
		      RandomAccessIterator2 B_first,
		      RandomAccessIterator2 B_last,
		      typename slip::lin_alg_traits<typename MatrixIterator::value_type>::value_type precision = typename slip::lin_alg_traits<typename MatrixIterator::value_type>::value_type(1.0E-6))
  {
    assert((A_bot - A_up)[0]   == (A_bot - A_up)[1]);
    assert((A_bot - A_up)[1]   == (X_last - X_first));
    assert((X_last - X_first)  == (B_last - B_first));
 
    typedef typename std::iterator_traits<MatrixIterator>::value_type value_type;
    typedef typename MatrixIterator::size_type size_type;
    size_type n = size_type((A_bot - A_up)[0]);
   
    //decomposition
    slip::Array2d<value_type> LU(n,n);
    slip::Array<int> Indx(n);
    std::copy(A_up,A_bot,LU.begin());
    int sign = slip::band_lu(LU.upper_left(),
			     LU.bottom_right(),
			     p,q,
			     Indx.begin(),
			     Indx.end(),
			     precision);
    std::cout<<"Indx = "<<Indx<<std::endl;
    slip::Array<typename std::iterator_traits<RandomAccessIterator2>::value_type > B2(n);
    //swap B according to Indx
    for(int i = 0; i < n; ++i)
      {
	//std::swap(B_first[i],B_first[Indx[i]]);
	//value_type tmp = B_first[i];
	B2[i] = B_first[Indx[i]];
	//B_first[Indx[i]] = tmp;
      }
   //  std::copy(B_first,B_last,std::ostream_iterator<value_type>(std::cout," "));
//     std::cout<<std::endl;
    std::cout<<"B2 = "<<B2<<std::endl;
    slip::Array<value_type> Y(n);
    //resolution of LY = B
   //  slip::unit_lower_band_solve(LU.upper_left(),LU.bottom_right(),
// 				p+q,
// 				Y.begin(),Y.end(),
// 				B_first,B_last,precision);
  //   slip::lower_triangular_solve(LU.upper_left(),LU.bottom_right(),
// 				      Y.begin(),Y.end(),
// 				      B_first,B_last,precision);
    slip::lower_triangular_solve(LU.upper_left(),LU.bottom_right(),
				 Y.begin(),Y.end(),
				 B2.begin(),B2.end(),precision);
    std::cout<<"Y = "<<Y<<std::endl;
    //resolution of UX = Y
    // slip::upper_band_solve(LU.upper_left(),LU.bottom_right(),
// 				 p+q,
// 				 X_first,X_last,
// 				 Y.begin(),Y.end(),precision);
    slip::upper_triangular_solve(LU.upper_left(),LU.bottom_right(),
				 X_first,X_last,
				 Y.begin(),Y.end(),precision);
    
  }

*/

/* @} */

/** \name Gauss system solve algorithms */
  /* @{ */

  /**
   ** \brief Solve the linear system M*X=B using the Gauss elemination partial pivoting.
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adapt for complex matrix
   ** \date 2009/03/01
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param M 2D container
   ** \param X 1D container
   ** \param B 1D container
   ** \param precision 
   ** \return false if the system can't be solved
   ** \pre M.rows() == M.cols()
   ** \pre M.cols() == X.size()
   ** \pre X.size() == B.size()
   ** \remarks If M is singular an exception will be raised.
   ** \remarks Works with real and complex data
   ** \remarks This method is numerically instable. That is to say you should
   ** use it for small and well conditioned matrices. For other matrices, 
   ** you should prefer eucidean methods like QR, Cholesky...
   ** This method is well apdated to tridiagonal or Hessenberg matrices.
   ** \par Complexity almost n^3/3 + O(n^2) comparisons
   ** \par Example:
   ** \code
   ** slip::Matrix<double> G(4,4);
   ** slip::hilbert(G);
   ** //  G(0,0) = 0.0;
   ** double bg[] = {22.0,7.0,4.5,1.0};
   ** slip::Vector<double> Bg(4,bg);
   ** slip::Vector<double> Xg(4);
   ** std::cout<< "G : " <<std::endl;
   ** std::cout<< G <<std::endl;
   ** std::cout<< "B : " <<std::endl;
   ** std::cout<< Bg <<std::endl;
   ** if(slip::gauss_solve(G,Xg,Bg,10E-6))
   ** std::cout << "Gauss ok!\n";
   ** else
   **  {
   **   std::cout << "Gauss not ok!\n";
   **  }
   ** std::cout<< "X : " <<std::endl;
   ** std::cout<< Xg <<std::endl;
   ** std::cout<< "Check : "<<std::endl;
   ** slip::Vector<double> Bg2(4);
   ** slip::matrix_vector_multiplies(G,Xg,Bg2);
   ** std::cout<< "Bg2  = G Xg: " <<std::endl;
   ** std::cout<< Bg2 <<std::endl;
   ** \endcode
   ** 
   */  
  template <class Matrix,class Vector1,class Vector2>
  inline
  bool gauss_solve(const Matrix & M, 
		   Vector1 & X, 
		   const Vector2 & B, 
		   typename slip::lin_alg_traits<typename Matrix::value_type>::value_type precision)
  {
  
	typedef typename Matrix::value_type _T;
	int M_rows = int(M.rows());
	int M_cols = int(M.cols());
	int M_rows_m1 = M_rows - 1;
	int M_cols_m1 = M_cols - 1;
	//int M_rows_p1 = M_rows + 1;
	int M_cols_p1 = M_cols + 1;
    
	//We first build S = [M|B]
	Matrix S(M_rows, M_cols_p1);
	slip::Box2d<int> bm(0,0,M_rows_m1,M_cols_m1);
	std::copy(M.upper_left(),M.bottom_right(),S.upper_left(bm));
	std::copy(B.begin(),B.end(),S.col_begin(M.cols()));
	int nbr = 0;

	//triangularization of S
	while(nbr < M_rows)
	  {
	    bm.set_coord(nbr,nbr,M_rows_m1,M_cols);
	    int nbpivot = slip::pivot_max(S.upper_left(bm),S.bottom_right(bm));
	    if(nbpivot == -1)
	      {
		throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
		
	      }
	    if(std::abs(S[nbpivot][nbr]) < precision)
	      {
		throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	
	      }
	    if(nbpivot > 0)
	      {
		std::swap_ranges(S.row_begin(nbr),S.row_end(nbr),S.row_begin(nbpivot));
	      }
	    for(int i = nbr+1; i < M_rows; ++i)
	      {
		_T u = - S[i][nbr] / S[nbr][nbr];
		slip::reduction(S.row_begin(i),S.row_end(i),S.row_begin(nbr),u);
	      }
	    nbr++;
	  }
	
	//Calculation of X
	if(std::abs(S[M_rows_m1][M_cols_m1]) < precision)
	  {
	    throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	   
	  }

	 bm.set_coord(0,0,M_rows_m1,M_cols_m1);
	 slip::upper_triangular_solve(S.upper_left(bm),S.bottom_right(bm),
				      X.begin(),X.end(),
				      S.col_begin(M_cols),S.col_end(M_cols),
				      precision);
	 return true;
    
  }



  /**
   ** \brief Solve the linear system M*X=B with the Gauss pivot method, 
   **  the null pivot are replaced by precision
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adapt for complex matrix
   ** \date 2009/03/01
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param M 2D container
   ** \param X 1D container
   ** \param B 1D container
   ** \param precision 
   ** \return false if the system has been filled.
   ** \pre M.rows() == M.cols()
   ** \pre M.cols() == X.size()
   ** \pre X.size() == B.size()
   ** \remarks If M is singular an exception will be raised.
   ** \remarks Works with real and complex data
   ** \remarks This method is numerically instable. That is to say you should
   ** use it for small and well conditioned matrices. For other matrices, 
   ** you should prefer eucidean methods like QR, Cholesky...
   ** This method is well apdated to tridiagonal or Hessenberg matrices.
   ** \par Complexity almost n^3/3 + O(n^2) comparisons
   ** \par Example:
   ** \code
   ** slip::Matrix<double> G(4,4);
   ** slip::hilbert(G);
   ** //  G(0,0) = 0.0;
   ** double bg[] = {22.0,7.0,4.5,1.0};
   ** slip::Vector<double> Bg(4,bg);
   ** slip::Vector<double> Xg(4);
   ** std::cout<< "G : " <<std::endl;
   ** std::cout<< G <<std::endl;
   ** std::cout<< "B : " <<std::endl;
   ** std::cout<< Bg <<std::endl;
   ** if(slip::gauss_solve_with_filling(G,Xg,Bg,10E-6))
   ** std::cout << "Gauss ok!\n";
   ** else
   **  {
   **   std::cout << "Gauss not ok!\n";
   **  }
   ** std::cout<< "X : " <<std::endl;
   ** std::cout<< Xg <<std::endl;
   ** std::cout<< "Check : "<<std::endl;
   ** slip::Vector<double> Bg2(4);
   ** slip::matrix_vector_multiplies(G,Xg,Bg2);
   ** std::cout<< "Bg2  = G Xg: " <<std::endl;
   ** std::cout<< Bg2 <<std::endl;
   ** \endcode
   ** 
   */  
  template <class Matrix,
	    class Vector1,
	    class Vector2>
  inline
  bool gauss_solve_with_filling(const Matrix & M, 
				Vector1 & X, 
				const Vector2 & B, 
				typename slip::lin_alg_traits<typename Matrix::value_type>::value_type precision)
  {
  	typedef typename Matrix::value_type _T;
	int M_rows = int(M.rows());
	int M_cols = int(M.cols());
	int M_rows_m1 = M_rows - 1;
	int M_cols_m1 = M_cols - 1;
	//int M_rows_p1 = M_rows + 1;
	int M_cols_p1 = M_cols + 1;
    
	//We first build S = [M|B]
	Matrix S(M_rows, M_cols_p1);
	slip::Box2d<int> bm(0,0,M_rows_m1,M_cols_m1);
	std::copy(M.upper_left(),M.bottom_right(),S.upper_left(bm));
	std::copy(B.begin(),B.end(),S.col_begin(M.cols()));
	int nbr = 0;
	bool ret = true;

	//triangularization of S
	while(nbr < M_rows)
	  {
	    bm.set_coord(nbr,nbr,M_rows_m1,M_cols);
	    int nbpivot = slip::pivot_max(S.upper_left(bm),S.bottom_right(bm));
	    if(nbpivot == -1)
	      {
		ret = false;
		*(S.upper_left(bm)) = precision;
		nbpivot = S.upper_left(bm).x1();
			
	      }
	    if(std::abs(S[nbpivot][nbr]) < precision)
	      {
		ret = false;
		S[nbpivot][nbr] = precision;
		
	      }
	    if(nbpivot > 0)
	      {
		std::swap_ranges(S.row_begin(nbr),S.row_end(nbr),S.row_begin(nbpivot));
	      }
	    for(int i = nbr+1; i < M_rows; ++i)
	      {
		_T u = - S[i][nbr] / S[nbr][nbr];
		slip::reduction(S.row_begin(i),S.row_end(i),S.row_begin(nbr),u);
	      }
	    nbr++;
	  }
	
	//Calculation of X
	if(std::abs(S[M_rows_m1][M_cols_m1]) < precision)
	  {
	    ret = false;
	    S[M.rows()-1][M.cols()-1] = precision;
	  }

	 bm.set_coord(0,0,M_rows_m1,M_cols_m1);
	 slip::upper_triangular_solve(S.upper_left(bm),S.bottom_right(bm),
				      X.begin(),X.end(),
				      S.col_begin(M_cols),S.col_end(M_cols),
				      precision);
	return ret;
     
  }

/* @} */
 
/** \name Tridiagonal matrix algorithms */
  /* @{ */
  
/** \brief Computes the LU decomposition for a tridiagonal matrix
 **  \f[
 ** \left(
 ** \begin{array}{ccccc}
 ** d_{1} & u_{1}&0&\cdots&0\\
 ** l_{1} & d_{2} & u_{2}&\ddots&\vdots\\
 ** 0&l_{2}&\ddots&\ddots&0\\
 ** \vdots&\ddots&\ddots&\ddots&u_{n-1}\\
 ** 0&\cdots&0&l_{n-1}&d_{n}\\
 ** \end{array}\right) =
 ** \left(
 ** \begin{array}{ccccc}
 ** 1 & 0&\cdots&\cdots&0\\
 ** ll_1 & 1 & \ddots&&\vdots\\
 ** 0&ll_2&\ddots&\ddots&\vdots\\
 ** \vdots&\ddots&\ddots&\ddots&0\\
 ** 0&\cdots&0&ll_{n-1}&1\\
 ** \end{array}\right)
 ** \left(
 ** \begin{array}{ccccc}
 ** du_{1} & uu_{1}&0&\cdots&0\\
 ** 0 & du_{2} & uu_{2}&\ddots&\vdots\\
 ** \vdots&\ddots&\ddots&\ddots&0\\
 ** \vdots&&\ddots&\ddots&uu_{n-1}\\
 ** 0&\cdots&\cdots&0&du_{n}\\
 ** \end{array}\right)
 ** \f]
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2009/03/04
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param diag_first RandomAccessIterator to the beginning of the main diagonal.
 ** \param diag_last RandomAccessIterator to one-past-the-end of the main diagonal.
 ** \param up_diag_first RandomAccessIterator to the beginning of the first upper diagonal.
 ** \param up_diag_last RandomAccessIterator to one-past-the-end of the first upper diagonal. 
 ** \param low_diag_first RandomAccessIterator to the beginning of the first lower diagonal.
 ** \param low_diag_last RandomAccessIterator to one-past-the-end of the first lower diagonal. 
 ** \param L_low_first RandomAccessIterator to the beginning of the first element of the lower diagonal of L.
 ** \param L_low_last RandomAccessIterator to one-past-the-end element of the lower diagonal of L. 
 ** \param U_up_first RandomAccessIterator to the beginning of the first element of the upper diagonal of U.
 ** \param U_up_last RandomAccessIterator to one-past-the-end of the first element of the upper diagonal of U. 
 ** \param precision 1.0E-6 by default.
 ** \pre diag_last != diag_first
 ** \pre (diag_last - diag_first) == (up_diag_last - up_diag_first + 1)
 ** \pre (diag_last - diag_first) == (low_diag_last - low_diag_first + 1)
 ** \pre (L_low_last-L_low_first) == (diag_last - diag_first - 1)
 ** \pre (U_up_last-U_up_first) == (diag_last - diag_first)
 ** \remarks Works with real and complex data.
 ** \remarks Will raise an exception if the matrix is singular
 ** \par Complexity: 3(n-1) flops
 ** \par Example:
 ** \code
 ** double dtlu_diag[]     = {2.0, 1.0, 5.0, -0.2, 7.0, 9.0, 8.0};
 ** double dtlu_up_diag[]  = {1.0, 1.0, 1.0, 1.2, 1.0, 1.0};
 **  double dtlu_low_diag[] = {1.0, 1.0, 1.0, 1.2, 1.0, 1.0};
 **  slip::Array<double> Dtlu_diag(7,dtlu_diag);
 **  slip::Array<double> Dtlu_up_diag(6,dtlu_up_diag);
 **  slip::Array<double> Dtlu_low_diag(6,dtlu_low_diag);
 **  std::cout<<"Dtlu_diag = "<<Dtlu_diag<<std::endl;
 **  std::cout<<"Dtlu_up_diag = "<<Dtlu_up_diag<<std::endl;
 **  std::cout<<"Dtlu_low_diag = "<<Dtlu_low_diag<<std::endl;
 **
 **  slip::Array<double> dtlu_l(6);
 **  slip::Array<double> dtlu_u(7);
 **  slip::tridiagonal_lu(dtlu_diag,dtlu_diag+7,
 **			dtlu_up_diag,dtlu_up_diag+6,
 **			dtlu_low_diag,dtlu_low_diag+6,
 **			dtlu_l.begin(),dtlu_l.end(),
 **			dtlu_u.begin(),dtlu_u.end());
 ** slip::Array2d<double> dtlu_L(7,7);
 ** slip::set_diagonal(dtlu_l.begin(),dtlu_l.end(),dtlu_L,-1);
 ** slip::fill_diagonal(dtlu_L,1.0);
 ** slip::Array2d<double> dtlu_U(7,7);
 ** slip::set_diagonal(dtlu_u.begin(),dtlu_u.end(),dtlu_U,0);
 ** slip::set_diagonal(Dtlu_up_diag.begin(),Dtlu_up_diag.end(),dtlu_U,1);
 ** slip::Array2d<double> dtlu_Lxdtlu_U(7,7);
 ** slip::matrix_matrix_multiplies(dtlu_L,dtlu_U,dtlu_Lxdtlu_U);
 ** std::cout<<"dtlu_l = \n"<<dtlu_l<<std::endl;
 ** std::cout<<"dtlu_u = \n"<<dtlu_u<<std::endl;
 ** std::cout<<"dtlu_L = \n"<<dtlu_L<<std::endl;
 ** std::cout<<"dtlu_U = \n"<<dtlu_U<<std::endl;
 ** std::cout<<"dtlu_Lxdtlu_U = \n"<<dtlu_Lxdtlu_U<<std::endl;
 ** \endcode
 */
 template<typename RandomAccessIterator1, 
	  typename RandomAccessIterator2,
	  typename RandomAccessIterator3>
 void tridiagonal_lu(RandomAccessIterator1 diag_first, RandomAccessIterator1 diag_last,
		     RandomAccessIterator1 up_diag_first, RandomAccessIterator1 up_diag_last, 
		     RandomAccessIterator1 low_diag_first, RandomAccessIterator1 low_diag_last, 
		     RandomAccessIterator2 L_low_first, RandomAccessIterator2 L_low_last, 
		     RandomAccessIterator3 U_up_first, RandomAccessIterator3 U_up_last,
		     typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type precision = typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type(1.0E-8))
 {
    assert(diag_last != diag_first);
    assert((diag_last - diag_first) == (up_diag_last - up_diag_first + 1));
    assert((diag_last - diag_first) == (low_diag_last - low_diag_first + 1));
    assert((L_low_last-L_low_first) == (diag_last - diag_first - 1));
    assert((U_up_last-U_up_first) == (diag_last - diag_first));

   
	*U_up_first = *diag_first;
	for(; L_low_first != L_low_last; ++L_low_first, ++U_up_first, ++diag_first, ++up_diag_first, ++low_diag_first)
	  {
	    if(std::abs(*U_up_first) < precision)
	      {
		throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	      }
	    *L_low_first = *low_diag_first / *U_up_first;
	    *(U_up_first + 1) = *(diag_first + 1) - (*L_low_first * *up_diag_first);
	  }
    
 }

/* @} */


/** \name Bidiagonal matrix algorithms */
  /* @{ */
/** \brief solve Ax = B with A unit lower bidiagonal
 **  \f[
 ** \left(
 ** \begin{array}{ccccc}
 ** 1 & 0&\cdots&\cdots&0\\
 ** ll_1 & 1 & \ddots&&\vdots\\
 ** 0&ll_2&\ddots&\ddots&\vdots\\
 ** \vdots&\ddots&\ddots&\ddots&0\\
 ** 0&\cdots&0&ll_{n-1}&1\\
 ** \end{array}\right)
 ** \left(
 ** \begin{array}{c}
 ** x_{1}\\
 ** x_{2}\\
 ** \vdots\\
 ** x_n\\
 ** \end{array}\right)
 ** = 
 ** \left(
 ** \begin{array}{c}
 ** b_{1}\\
 ** b_{2}\\
 ** \vdots\\
 ** b_n\\
 ** \end{array}\right)
 ** \f]
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2009/03/04
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param low_diag_first RandomAccessIterator to the first element of the lower diagonal.
 ** \param low_diag_last RandomAccessIterator to one-past-the-end element of the lower diagonal. 
 ** \param X_first RandomAccessIterator to the begenning of the result range.
 ** \param X_last RandomAccessIterator to one-past-the-end of the result range.
 ** \param B_first RandomAccessIterator to the begenning of the B range.
 ** \param B_last RandomAccessIterator to one-past-the-end of the B range.
 ** \pre low_diag_last != low_diag_first
 ** \pre (low_diag_last - low_diag_first) == (X_last - X_first - 1)
 ** \pre (low_diag_last - low_diag_first) == (B_last - B_first - 1)
 ** \par Complexity: 2(n-1) flops
 ** \remarks Works with real and complex data
 ** \par Example:
 ** \code
 ** double lb_low[]  = {2.0,2.0,1.0};
 ** double b_lb[]  = {3.0,2.0,1.0};
 **
 ** slip::Vector<double> LB_diag(4,lb_diag);
 ** slip::Vector<double> LB_low(3,lb_low);
 ** slip::Matrix<double> LB(LB_diag.size(),LB_diag.size());
 ** slip::fill_diagonal(LB,1.0);
 ** slip::set_diagonal(LB_low.begin(),LB_low.end(),LB,-1);
 ** std::cout<<"LB = \n"<<LB<<std::endl;
 ** slip::Vector<double> B_lb(4,b_lb);
 ** slip::Vector<double> X_lb(4);
 ** slip::unit_lower_bidiagonal_inv(LB_diag.begin(),LB_diag.end(),
 **			       LB_low.begin(),LB_low.end(),
 **                            X_lb.begin(),X_lb.end(),
 **			       B_lb.begin(),B_lb.end());
 ** std::cout<<"X_lb = \n"<<X_lb<< std::endl;
 ** std::cout<<"Check:"<<std::endl;
 ** slip::Vector<double> B_lb2(4);
 ** slip::matrix_vector_multiplies(LB,X_lb,B_lb2);
 ** std::cout<<"X_lb2 = \n"<<X_lb2<< std::endl;
 ** \endcode
 */
  template<typename RandomAccessIterator1, 
	   typename RandomAccessIterator2,
	   typename RandomAccessIterator3>
  void unit_lower_bidiagonal_solve(RandomAccessIterator1 low_diag_first, RandomAccessIterator1 low_diag_last, 
				     RandomAccessIterator2 X_first, RandomAccessIterator2 X_last, 
				     RandomAccessIterator3 B_first, RandomAccessIterator3 B_last)
  {
    assert(low_diag_last != low_diag_first);
    assert((low_diag_last - low_diag_first) == (X_last - X_first - 1));
    assert((low_diag_last - low_diag_first) == (B_last - B_first - 1));

    *X_first++ = *B_first++;
    for(; X_first != X_last; ++X_first, ++B_first, ++low_diag_first)
      {
	*X_first = (*B_first - (*low_diag_first * *(X_first - 1)) );
      }
  }

/**
 ** \brief Invert a lower unit bidiagonal matrix:
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** 1 & 0&\cdots&\cdots&0\\
   ** l_{1} & 1 & \ddots&&\vdots\\
   ** 0&l_{2}&\ddots&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&\ddots&0\\
   ** 0&\cdots&0&l_{n-1}&1\\
   ** \end{array}\right)^{-1}
   ** \f]
 
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2009/05/17
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param low_diag_first RandomAccessIterator to the beginning of the first lower diagonal.
 ** \param low_diag_last RandomAccessIterator to one-past-the-end of the first lower diagonal.
 ** \param Ainv_up  RandomAccessIterator2d to the upper_left element of the 2d range of the inverse matrix.
 ** \param Ainv_bot RandomAccessIterator2d to the one past the bottom_right element of the 2d range of the inverse matrix.
 ** \pre (Ainv_bot - Ainv_up)[0] == (Ainv_bot - Ainv_up)[1])
 ** \pre (Ainv_bot - Ainv_up)[0] == (low_diag_last - low_diag_first + 1)
 ** \par Complexity: 2n^2 - n flops
 ** \remarks Works with real and complex data
 ** \par Example:
 ** \code
 ** double lb_low[]  = {2.0,2.0,1.0};
 **
 ** slip::Vector<double> LB_low(3,lb_low);
 ** slip::Matrix<double> LB(LB_diag.size(),LB_diag.size());
 ** slip::fill_diagonal(LB,1.0);
 ** slip::set_diagonal(LB_low.begin(),LB_low.end(),LB,-1);
 ** std::cout<<"LB = \n"<<LB<<std::endl;
 ** slip::Matrix<double> LBinv(LB.rows(),LB.cols());
 ** slip::unit_lower_bidiagonal_inv(LB_low.begin(),LB_low.end(),
 **			            LBinv.upper_left(),LBinv.bottom_right());
 ** std::cout<<"LBinv = \n"<<LBinv<< std::endl;
 ** std::cout<<"Check:"<<std::endl;
 ** slip::Matrix<double> LBLBinv(LB.rows(),LB.cols());
 ** slip::matrix_matrix_multiplies(LB,LBinv,LBLBinv);
 ** std::cout<<"LBLBinv = \n"<<LBLBinv<< std::endl;
 ** \endcode
 */
template<typename RandomAccessIterator1, 
	 typename RandomAccessIterator2d>
void unit_lower_bidiagonal_inv(RandomAccessIterator1 low_diag_first, 
			       RandomAccessIterator1 low_diag_last, 
			       RandomAccessIterator2d Ainv_up, 
			       RandomAccessIterator2d Ainv_bot)
{

   assert((Ainv_bot - Ainv_up)[0] == (Ainv_bot - Ainv_up)[1]);
   assert((Ainv_bot - Ainv_up)[0] == (low_diag_last - low_diag_first + 1));
 
   const  int n = static_cast<int>((Ainv_bot - Ainv_up)[0]);
   //init Ainv with the identity matrix
   slip::identity(Ainv_up,Ainv_bot);
 
   for(int i = 0; i < n; ++i)
     {
       slip::unit_lower_bidiagonal_solve(low_diag_first,low_diag_last,
					 Ainv_up.col_begin(i),Ainv_up.col_end(i),
					 Ainv_up.col_begin(i),Ainv_up.col_end(i));
     }
}

/** \brief solve Ax = B with A lower bidiagonal
 **  \f[
 ** \left(
 ** \begin{array}{ccccc}
 ** dl_1 & 0&\cdots&\cdots&0\\
 ** ll_1 & dl_2 & \ddots&&\vdots\\
 ** 0&ll_2&\ddots&\ddots&\vdots\\
 ** \vdots&\ddots&\ddots&\ddots&0\\
 ** 0&\cdots&0&ll_{n-1}&dl_{n}\\
 ** \end{array}\right)
 ** \left(
 ** \begin{array}{c}
 ** x_{1}\\
 ** x_{2}\\
 ** \vdots\\
 ** x_n\\
 ** \end{array}\right)
 ** = 
 ** \left(
 ** \begin{array}{c}
 ** b_{1}\\
 ** b_{2}\\
 ** \vdots\\
 ** b_n\\
 ** \end{array}\right)
 ** \f]
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2009/03/04
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param diag_first RandomAccessIterator to the first element of thediagonal.
 ** \param diag_last RandomAccessIterator to one-past-the-end element of the diagonal. 
 ** \param low_diag_first RandomAccessIterator to the first element of the lower diagonal.
 ** \param low_diag_last RandomAccessIterator to one-past-the-end element of the lower diagonal. 
 ** \param X_first RandomAccessIterator to the begenning of the result range.
 ** \param X_last RandomAccessIterator to one-past-the-end of the result range.
 ** \param B_first RandomAccessIterator to the begenning of the B range.
 ** \param B_last RandomAccessIterator to one-past-the-end of the B range.
 ** \param precision 1.0E-6 by default.
 ** \pre low_diag_last != low_diag_first
 ** \pre (low_diag_last - low_diag_first) == (X_last - X_first - 1)
 ** \pre (low_diag_last - low_diag_first) == (B_last - B_first - 1)
 ** \par Complexity: 3n - 2 flops
 ** \remarks Works with real and complex data
 ** \remarks Will raise an exception if the matrix is singular.
 ** \par Example:
 ** \code
 ** double lb_diag[] = {1.0,2.0,3.0,4.0};
 ** double lb_low[]  = {2.0,2.0,1.0};
 ** double b_lb[]  = {3.0,2.0,1.0};
 **
 ** slip::Vector<double> LB_diag(4,lb_diag);
 ** slip::Vector<double> LB_low(3,lb_low);
 ** slip::Matrix<double> LB(LB_diag.size(),LB_diag.size());
 ** slip::set_diagonal(LB_diag.begin(),LB_diag.end(),LB);
 ** slip::set_diagonal(LB_low.begin(),LB_low.end(),LB,-1);
 ** std::cout<<"LB = \n"<<LB<<std::endl;
 ** slip::Vector<double> B_lb(4,b_lb);
 ** slip::Vector<double> X_lb(4);
 ** slip::lower_bidiagonal_inv(LB_diag.begin(),LB_diag.end(),
 **			       LB_low.begin(),LB_low.end(),
 **                            X_lb.begin(),X_lb.end(),
 **			       B_lb.begin(),B_lb.end());
 ** std::cout<<"X_lb = \n"<<X_lb<< std::endl;
 ** std::cout<<"Check:"<<std::endl;
 ** slip::Vector<double> B_lb2(4);
 ** slip::matrix_vector_multiplies(LB,X_lb,B_lb2);
 ** std::cout<<"X_lb2 = \n"<<X_lb2<< std::endl;
 ** \endcode
 */
  template<typename RandomAccessIterator1, typename RandomAccessIterator2,
	   typename RandomAccessIterator3>
  void lower_bidiagonal_solve(RandomAccessIterator1 diag_first, RandomAccessIterator1 diag_last,
			      RandomAccessIterator1 low_diag_first, RandomAccessIterator1 low_diag_last, 
			      RandomAccessIterator2 X_first, RandomAccessIterator2 X_last, 
			      RandomAccessIterator3 B_first, RandomAccessIterator3 B_last,
			      typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type precision = typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type(1.0E-8))
  {
    assert(diag_last != diag_first);
    assert((diag_last - diag_first) == (X_last - X_first));
    assert((diag_last - diag_first) == (B_last - B_first));
    assert((diag_last - diag_first) == (low_diag_last - low_diag_first + 1));
  
    if(std::abs(*diag_first) < precision)
      {
	throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
      }
    *X_first++ = *B_first++ / *diag_first++;
    for(; X_first != X_last; ++X_first, ++B_first, ++diag_first, ++low_diag_first)
      { 
	if(std::abs(*diag_first) < precision)
	  {
	    throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	  }
	*X_first = (*B_first - (*low_diag_first * *(X_first - 1)) ) / *diag_first;
      }
  }


  /**
   ** \brief Invert a lower bidiagonal matrix:
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & 0&\cdots&\cdots&0\\
   ** l_{1} & d_{2} & \ddots&&\vdots\\
   ** 0&l_{2}&\ddots&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&\ddots&0\\
   ** 0&\cdots&0&l_{n-1}&d_{n}\\
   ** \end{array}\right)^{-1}
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/17
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param diag_first RandomAccessIterator to the beginning of the main diagonal.
   ** \param diag_last RandomAccessIterator to one-past-the-end of the main diagonal.
   ** \param low_diag_first RandomAccessIterator to the beginning of the first lower diagonal.
   ** \param low_diag_last RandomAccessIterator to one-past-the-end of the first lower diagonal.
   ** \param Ainv_up  RandomAccessIterator2d to the upper_left element of the 2d range of the inverse matrix.
   ** \param Ainv_bot RandomAccessIterator2d to the one past the bottom_right element of the 2d range of the inverse matrix.
   ** \param precision 1.0E-6 by default.
   ** \pre diag_last != diag_first
   ** \pre (diag_last - diag_first) == (low_diag_last - low_diag_first + 1)
   ** \pre (Ainv_bot - Ainv_up)[0] == (Ainv_bot - Ainv_up)[1])
   ** \pre (Ainv_bot - Ainv_up)[0] == (diag_last - diag_first)
   ** \par Complexity: 3n^2 - 2n flops
   ** \remarks Works with real and complex data
   ** \remarks Will raise an exception if the matrix is singular
   ** \par Example:
   ** \code
   ** double lb_diag[] = {1.0,2.0,3.0,4.0};
   ** double lb_low[]  = {2.0,2.0,1.0};
   **
   ** slip::Vector<double> LB_diag(4,lb_diag);
   ** slip::Vector<double> LB_low(3,lb_low);
   ** slip::Matrix<double> LB(LB_diag.size(),LB_diag.size());
   ** slip::set_diagonal(LB_diag.begin(),LB_diag.end(),LB);
   ** slip::set_diagonal(LB_low.begin(),LB_low.end(),LB,-1);
   ** std::cout<<"LB = \n"<<LB<<std::endl;
   ** slip::Matrix<double> LBinv(LB.rows(),LB.cols());
   ** slip::lower_bidiagonal_inv(LB_diag.begin(),LB_diag.end(),
   **			LB_low.begin(),LB_low.end(),
   **			LBinv.upper_left(),LBinv.bottom_right());
   ** std::cout<<"LBinv = \n"<<LBinv<< std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Matrix<double> LBLBinv(LB.rows(),LB.cols());
   ** slip::matrix_matrix_multiplies(LB,LBinv,LBLBinv);
   ** std::cout<<"LBLBinv = \n"<<LBLBinv<< std::endl;
   ** \endcode
   */
template<typename RandomAccessIterator1, 
	 typename RandomAccessIterator2d>
void lower_bidiagonal_inv(RandomAccessIterator1 diag_first, 
			  RandomAccessIterator1 diag_last,
			  RandomAccessIterator1 low_diag_first, 
			  RandomAccessIterator1 low_diag_last, 
			  RandomAccessIterator2d Ainv_up, 
			  RandomAccessIterator2d Ainv_bot, 
			  typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type precision = typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type(1.0E-8))
{
   assert(diag_last != diag_first);
   assert((diag_last - diag_first) == (low_diag_last - low_diag_first + 1));
   assert((Ainv_bot - Ainv_up)[0] == (Ainv_bot - Ainv_up)[1]);
   assert((Ainv_bot - Ainv_up)[0] == (diag_last - diag_first));

   const int n = static_cast<int>(diag_last - diag_first);
   //init Ainv with the identity matrix
   slip::identity(Ainv_up,Ainv_bot);
 
   for(int i = 0; i < n; ++i)
     {
       slip::lower_bidiagonal_solve(diag_first,diag_last,
				    low_diag_first,low_diag_last,
				    Ainv_up.col_begin(i),Ainv_up.col_end(i),
				    Ainv_up.col_begin(i),Ainv_up.col_end(i),
				    precision);
     }
   

}  

 /**
   ** \brief Invert a lower bidiagonal matrix:
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & 0&\cdots&\cdots&0\\
   ** l_{1} & d_{2} & \ddots&&\vdots\\
   ** 0&l_{2}&\ddots&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&\ddots&0\\
   ** 0&\cdots&0&l_{n-1}&d_{n}\\
   ** \end{array}\right)^{-1}
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/17
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A 2d container of the matrix to invert.
   ** \param Ainv 2d container of the matrix the inverse matrix.
   ** \param precision 1.0E-6 by default.
   ** \pre A.dim1() == A.dim2()
   ** \pre Ainv.dim1() == Ainv.dim2()
   ** 
   ** \par Complexity: 3n^2 - 2n flops + (2n - 1) flops
   ** \remarks Works with real and complex data
   ** \remarks Will raise an exception if the matrix is singular
   ** \par Example:
   ** \code
   ** double lb_diag[] = {1.0,2.0,3.0,4.0};
   ** double lb_low[]  = {2.0,2.0,1.0};
   **
   ** slip::Vector<double> LB_diag(4,lb_diag);
   ** slip::Vector<double> LB_low(3,lb_low);
   ** slip::Matrix<double> LB(LB_diag.size(),LB_diag.size());
   ** slip::set_diagonal(LB_diag.begin(),LB_diag.end(),LB);
   ** slip::set_diagonal(LB_low.begin(),LB_low.end(),LB,-1);
   ** std::cout<<"LB = \n"<<LB<<std::endl;
   ** slip::Matrix<double> LBinv(LB.rows(),LB.cols());
   ** slip::lower_bidiagonal_inv(LB,LBinv);
   ** std::cout<<"LBinv = \n"<<LBinv<< std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Matrix<double> LBLBinv(LB.rows(),LB.cols());
   ** slip::matrix_matrix_multiplies(LB,LBinv,LBLBinv);
   ** std::cout<<"LBLBinv = \n"<<LBLBinv<< std::endl;
   ** \endcode
   */
 template<typename Container2d1,
	  typename Container2d2>
 void lower_bidiagonal_inv(const Container2d1& A,
			   Container2d2& Ainv,
			   typename slip::lin_alg_traits<typename Container2d1::value_type>::value_type precision = typename slip::lin_alg_traits<typename Container2d1::value_type>::value_type(1.0E-8))
  {
    
    assert(A.dim1() == A.dim2());
    assert(Ainv.dim1() == Ainv.dim2());

    
    typedef typename Container2d1::value_type value_type;
    typedef typename Container2d1::size_type size_type;

    //get the 3 main diagonals
    size_type n = A.dim1();
    slip::Array<value_type> diag(n);
    slip::Array<value_type> low_diag(n-1);
    slip::get_diagonal(A,diag.begin(),diag.end());
    slip::get_diagonal(A,low_diag.begin(),low_diag.end(),-1);
    slip::lower_bidiagonal_inv(diag.begin(),diag.end(),
			       low_diag.begin(),low_diag.end(),
			       Ainv.upper_left(),
			       Ainv.bottom_right(),
			       precision);

  }

   

/** \brief solve Ax = B with A upper bidiagonal
 **  \f[
 ** \left(
 ** \begin{array}{ccccc}
 ** du_1 & uu_1&0&\cdots&0\\
 ** 0 & \ddots & \ddots&\ddots&\vdots\\
 ** \vdots&\ddots&\ddots&\ddots&0\\
 ** \vdots&&\ddots&\ddots&uu_{n-1}\\
 ** 0&\cdots&\cdots&0&du_{n}\\
 ** \end{array}\right)
 ** \left(
 ** \begin{array}{c}
 ** x_{1}\\
 ** x_{2}\\
 ** \vdots\\
 ** x_n\\
 ** \end{array}\right)
 ** = 
 ** \left(
 ** \begin{array}{c}
 ** b_{1}\\
 ** b_{2}\\
 ** \vdots\\
 ** b_n\\
 ** \end{array}\right)
 ** \f]
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2009/03/04
 ** \since 1.0.0
 ** \version 0.0.1
  ** \param diag_first RandomAccessIterator to the first element of thediagonal.
 ** \param diag_last RandomAccessIterator to one-past-the-end element of the diagonal. 
 ** \param up_diag_first RandomAccessIterator to the first element of the upper diagonal.
 ** \param up_diag_last RandomAccessIterator to one-past-the-end element of the upper diagonal. 
 ** \param X_first RandomAccessIterator to the begenning of the result range.
 ** \param X_last RandomAccessIterator to one-past-the-end of the result range.
 ** \param B_first RandomAccessIterator to the begenning of the B range.
 ** \param B_last RandomAccessIterator to one-past-the-end of the B range.
 ** \param precision 1.0E-6 by default.
 ** \pre diag_last != diag_first
 ** \pre (diag_last - diag_first) == (X_last - X_first)
 ** \pre (diag_last - diag_first) == (B_last - B_first)
 ** \pre (diag_last - diag_first) == (up_diag_last - up_diag_first + 1)
 ** \par Complexity: 3n - 2 flops
 ** \remarks Works with real and complex data
 ** \remarks Will raise an exception if the matrix is singular.
 ** \par Example:
 ** \code
 ** double ub_diag[] = {1.0,2.0,3.0,4.0};
 ** double ub_up[]  = {2.0,2.0,1.0};
 ** double b_ub[] = {2.0,1.0,4.0,3.0};
 **
 ** slip::Vector<double> UB_diag(4,ub_diag);
 ** slip::Vector<double> UB_up(3,ub_up);
 ** slip::Matrix<double> UB(UB_diag.size(),UB_diag.size());
 ** slip::set_diagonal(UB_diag.begin(),UB_diag.end(),UB);
 ** slip::set_diagonal(UB_up.begin(),UB_up.end(),UB,1);
 ** std::cout<<"UB = \n"<<UB<<std::endl;
 ** slip::Vector<double> B_ub(4,b_ub);
 ** slip::Matrix<double> X_ub(B_ub.size());
 ** slip::upper_bidiagonal_solve(UB_diag.begin(),UB_diag.end(),
 **			UB_up.begin(),UB_up.end(),
 **                     X_ub.begin(),X_ub.end(), 
 **			B_ub.begin(),B_ub.bottom_right());
 ** std::cout<<"X_ub = \n"<<X_ub<< std::endl;
 ** std::cout<<"Check:"<<std::endl;
 ** slip::Vector<double> B_ub2(4);
 ** slip::matrix_vector_multiplies(UB,X_ub,B_ub2);
 ** std::cout<<"B_ub2 = \n"<<B_ub2<< std::endl;
 ** \endcode
 */
  template<typename RandomAccessIterator1, typename RandomAccessIterator2,
	   typename RandomAccessIterator3>
  void upper_bidiagonal_solve(RandomAccessIterator1 diag_first, RandomAccessIterator1 diag_last,
			      RandomAccessIterator1 up_diag_first, RandomAccessIterator1 up_diag_last, 
			      RandomAccessIterator2 X_first, RandomAccessIterator2 X_last, 
			      RandomAccessIterator3 B_first, RandomAccessIterator3 B_last,
			      typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type precision = typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type(1.0E-8))
  {

    assert(diag_last != diag_first);
    assert((diag_last - diag_first) == (X_last - X_first));
    assert((diag_last - diag_first) == (B_last - B_first));
    assert((diag_last - diag_first) == (up_diag_last - up_diag_first + 1));

  
	typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type;
	value_type d = *(--diag_last);
	if(std::abs(d) < precision)
	      {
		throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	      }
	*(--X_last) = *(--B_last) / d;
	//
	--X_last;
	--B_last;
	--diag_last;
	--up_diag_last;
	
	for(; X_last >= X_first; --X_last, --B_last, --up_diag_last, --diag_last)
	  {
	    if(std::abs(*diag_last) < precision)
	      {
		throw std::runtime_error(slip::SINGULAR_MATRIX_ERROR);
	      }
	    *X_last = (*B_last - (*(X_last + 1) * *up_diag_last)) / *diag_last;
	  }   
  
  }

 /**
   ** \brief Invert an upper bidiagonal squared matrix
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & u_{1}&0&\cdots&0\\
   ** 0 & d_{2} & u_{2}&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&\ddots&0\\
   ** \vdots&\ddots&\ddots&\ddots&u_{n-1}\\
   ** 0&\cdots&\cdots&0&d_{n}\\
   ** \end{array}\right)^{-1}
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/17
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param diag_first RandomAccessIterator to the beginning of the main diagonal.
   ** \param diag_last RandomAccessIterator to one-past-the-end of the main diagonal.
   ** \param up_diag_first RandomAccessIterator to the beginning of the first upper diagonal.
   ** \param up_diag_last RandomAccessIterator to one-past-the-end of the first upper diagonal.  
   ** \param Ainv_up  RandomAccessIterator2d to the upper_left element of the 2d range of the inverse matrix.
   ** \param Ainv_bot RandomAccessIterator2d to the one past the bottom_right element of the 2d range of the inverse matrix.
   ** \param precision 1.0E-6 by default.
   ** \pre diag_last != diag_first
   ** \pre (diag_last - diag_first) == (up_diag_last - up_diag_first + 1)
   ** \pre (Ainv_bot - Ainv_up)[0] == (Ainv_bot - Ainv_up)[1])
   ** \pre (Ainv_bot - Ainv_up)[0] == (diag_last - diag_first)
   ** \par Complexity: 3n^2 - 2n flops
   ** \remarks Works with real and complex data
   ** \remarks Will raise an exception if the matrix is singular
   ** \par Example:
   ** \code
   ** double ub_diag[] = {1.0,2.0,3.0,4.0};
   ** double ub_up[]  = {2.0,2.0,1.0};
   **
   ** slip::Vector<double> UB_diag(4,ub_diag);
   ** slip::Vector<double> UB_up(3,ub_up);
   ** slip::Matrix<double> UB(UB_diag.size(),UB_diag.size());
   ** slip::set_diagonal(UB_diag.begin(),UB_diag.end(),UB);
   ** slip::set_diagonal(UB_up.begin(),UB_up.end(),UB,1);
   ** std::cout<<"UB = \n"<<UB<<std::endl;
   ** slip::Matrix<double> UBinv(UB.rows(),UB.cols());
   ** slip::upper_bidiagonal_inv(UB_diag.begin(),UB_diag.end(),
   **			UB_up.begin(),UB_up.end(),
   **			UBinv.upper_left(),UBinv.bottom_right());
   ** std::cout<<"UBinv = \n"<<UBinv<< std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Matrix<double> UBUBinv(UB.rows(),UB.cols());
   ** slip::matrix_matrix_multiplies(UB,UBinv,UBUBinv);
   ** std::cout<<"UBUBinv = \n"<<UBUBinv<< std::endl;
   ** \endcode
   */
template<typename RandomAccessIterator1, 
	 typename RandomAccessIterator2d>
void upper_bidiagonal_inv(RandomAccessIterator1 diag_first, 
			  RandomAccessIterator1 diag_last,
			  RandomAccessIterator1 up_diag_first, 
			  RandomAccessIterator1 up_diag_last, 
			  RandomAccessIterator2d Ainv_up, 
			  RandomAccessIterator2d Ainv_bot, 
			  typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type precision = typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type(1.0E-8))
{
   assert(diag_last != diag_first);
   assert((diag_last - diag_first) == (up_diag_last - up_diag_first + 1));
   assert((Ainv_bot - Ainv_up)[0] == (Ainv_bot - Ainv_up)[1]);
   assert((Ainv_bot - Ainv_up)[0] == (diag_last - diag_first));

   const int n = static_cast<int>(diag_last - diag_first);
   //init Ainv with the identity matrix
   slip::identity(Ainv_up,Ainv_bot);
 
   for(int i = 0; i < n; ++i)
     {
       slip::upper_bidiagonal_solve(diag_first,diag_last,
				    up_diag_first,up_diag_last,
				    Ainv_up.col_begin(i),Ainv_up.col_end(i),
				    Ainv_up.col_begin(i),Ainv_up.col_end(i),
				    precision);
     }
   

}
 /**
   ** \brief Invert an upper bidiagonal squared matrix
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & u_{1}&0&\cdots&0\\
   ** 0 & d_{2} & u_{2}&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&\ddots&0\\
   ** \vdots&\ddots&\ddots&\ddots&u_{n-1}\\
   ** 0&\cdots&\cdots&0&d_{n}\\
   ** \end{array}\right)^{-1}
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/17
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A 2d container of the matrix to invert.
   ** \param Ainv 2d container of the matrix the inverse matrix.
   ** \param precision 1.0E-6 by default.
   ** \pre A.dim1() == A.dim2()
   ** \pre Ainv.dim1() == Ainv.dim2()
   ** \par Complexity: 3n^2 - 2n flops + (2n - 1) flops
   ** \remarks Works with real and complex data
   ** \remarks Will raise an exception if the matrix is singular
   ** \par Example:
   ** \code
   ** double ub_diag[] = {1.0,2.0,3.0,4.0};
   ** double ub_up[]  = {2.0,2.0,1.0};
   **
   ** slip::Vector<double> UB_diag(4,ub_diag);
   ** slip::Vector<double> UB_up(3,ub_up);
   ** slip::Matrix<double> UB(UB_diag.size(),UB_diag.size());
   ** slip::set_diagonal(UB_diag.begin(),UB_diag.end(),UB);
   ** slip::set_diagonal(UB_up.begin(),UB_up.end(),UB,1);
   ** std::cout<<"UB = \n"<<UB<<std::endl;
   ** slip::Matrix<double> UBinv(UB.rows(),UB.cols());
   ** slip::upper_bidiagonal_inv(UB,UBinv);
   ** std::cout<<"UBinv = \n"<<UBinv<< std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Matrix<double> UBUBinv(UB.rows(),UB.cols());
   ** slip::matrix_matrix_multiplies(UB,UBinv,UBUBinv);
   ** std::cout<<"UBUBinv = \n"<<UBUBinv<< std::endl;
   ** \endcode
   */
 template<typename Container2d1,
	   typename Container2d2>
  void upper_bidiagonal_inv(const Container2d1& A,
			    Container2d2& Ainv,
			    typename slip::lin_alg_traits<typename Container2d1::value_type>::value_type precision = typename slip::lin_alg_traits<typename Container2d1::value_type>::value_type(1.0E-8))
  {
    
    assert(A.dim1() == A.dim2());
    assert(Ainv.dim1() == Ainv.dim2());

    
    typedef typename Container2d1::value_type value_type;
    typedef typename Container2d1::size_type size_type;

    //get the 3 main diagonals
    size_type n = A.dim1();
    slip::Array<value_type> diag(n);
    slip::Array<value_type> up_diag(n-1);

    slip::get_diagonal(A,diag.begin(),diag.end());
    slip::get_diagonal(A,up_diag.begin(),up_diag.end(),1);

    slip::upper_bidiagonal_inv(diag.begin(),diag.end(),
			       up_diag.begin(),up_diag.end(),
			       Ainv.upper_left(),
			       Ainv.bottom_right(),
			       precision);

  }

/** \brief solve Ax = B with A unit upper bidiagonal
 **  \f[
 ** \left(
 ** \begin{array}{ccccc}
 ** 1 & uu_1&0&\cdots&0\\
 ** 0 & \ddots & \ddots&\ddots&\vdots\\
 ** \vdots&\ddots&\ddots&\ddots&0\\
 ** \vdots&&\ddots&\ddots&uu_{n-1}\\
 ** 0&\cdots&\cdots&0&1\\
 ** \end{array}\right)
 ** \left(
 ** \begin{array}{c}
 ** x_{1}\\
 ** x_{2}\\
 ** \vdots\\
 ** x_n\\
 ** \end{array}\right)
 ** = 
 ** \left(
 ** \begin{array}{c}
 ** b_{1}\\
 ** b_{2}\\
 ** \vdots\\
 ** b_n\\
 ** \end{array}\right)
 ** \f]
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2009/03/04
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param up_diag_first RandomAccessIterator to the first element of the upper diagonal.
 ** \param up_diag_last RandomAccessIterator to one-past-the-end element of the upper diagonal. 
 ** \param X_first RandomAccessIterator to the begenning of the result range.
 ** \param X_last RandomAccessIterator to one-past-the-end of the result range.
 ** \param B_first RandomAccessIterator to the begenning of the B range.
 ** \param B_last RandomAccessIterator to one-past-the-end of the B range.
 ** \pre X_last != X_first
 ** \pre (X_last - X_first) == (B_last - B_first)
 ** \pre (diag_last - diag_first) == (B_last - B_first)
 ** \pre (X_last - X_first) == (up_diag_last - up_diag_first + 1)
 ** \par Complexity: 2n - 1 flops
 ** \remarks Works with real and complex data
 ** \par Example:
 ** \code
 ** double ub_up[]  = {2.0,2.0,1.0};
 ** double b_ub[] = {2.0,1.0,4.0,3.0};
 **
 ** slip::Vector<double> UB_up(3,ub_up);
 ** slip::Matrix<double> UB(UB_diag.size(),UB_diag.size());
 ** slip::fill_diagonal(UB,1.0);
 ** slip::set_diagonal(UB_up.begin(),UB_up.end(),UB,1);
 ** std::cout<<"UB = \n"<<UB<<std::endl;
 ** slip::Vector<double> B_ub(4,b_ub);
 ** slip::Matrix<double> X_ub(B_ub.size());
 ** slip::unit_upper_bidiagonal_solve(UB_up.begin(),UB_up.end(),
 **                                   X_ub.begin(),X_ub.end(), 
 **			              B_ub.begin(),B_ub.bottom_right());
 ** std::cout<<"X_ub = \n"<<X_ub<< std::endl;
 ** std::cout<<"Check:"<<std::endl;
 ** slip::Vector<double> B_ub2(4);
 ** slip::matrix_vector_multiplies(UB,X_ub,B_ub2);
 ** std::cout<<"B_ub2 = \n"<<B_ub2<< std::endl;
 ** \endcode
 */
  template<typename RandomAccessIterator1, typename RandomAccessIterator2,
	   typename RandomAccessIterator3>
  void unit_upper_bidiagonal_solve(RandomAccessIterator1 up_diag_first, RandomAccessIterator1 up_diag_last, 
				   RandomAccessIterator2 X_first, RandomAccessIterator2 X_last, 
				   RandomAccessIterator3 B_first, RandomAccessIterator3 B_last
			      )
  {

    assert(X_last != X_first);
    assert((X_last - X_first) == (B_last - B_first));
    assert((X_last - X_first) == (up_diag_last - up_diag_first + 1));

    *(--X_last) = *(--B_last);
    //
    --X_last;
    --B_last;
    --up_diag_last;
    
    for(; X_last >= X_first; --X_last, --B_last, --up_diag_last)
      {
	*X_last = (*B_last - (*(X_last + 1) * *up_diag_last));
      }   
  }


 /**
   ** \brief Invert an unit upper bidiagonal matrix:
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** 1 & u_{1}&0&\cdots&0\\
   ** 0 & 1 & u_{2}&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&\ddots&0\\
   ** \vdots&&\ddots&\ddots&u_{n-1}\\
   ** 0&\cdots&\cdots&0&1\\
   ** \end{array}\right)^{-1}
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/05/17
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param up_diag_first RandomAccessIterator to the beginning of the first upper diagonal.
   ** \param up_diag_last RandomAccessIterator to one-past-the-end of the first upper diagonal.  
   ** \param Ainv_up  RandomAccessIterator2d to the upper_left element of the 2d range of the inverse matrix.
   ** \param Ainv_bot RandomAccessIterator2d to the one past the bottom_right element of the 2d range of the inverse matrix.
   ** \pre (Ainv_bot - Ainv_up)[0] == (Ainv_bot - Ainv_up)[1])
   ** \pre (Ainv_bot - Ainv_up)[0] == (up_diag_last - up_diag_first + 1)
   ** \par Complexity: 2n^2 - n flops
   ** \remarks Works with real and complex data
   ** \par Example:
   ** \code
   ** double ub_up[]  = {2.0,2.0,1.0};
   **
   ** slip::Vector<double> UB_up(3,ub_up);
   ** slip::Matrix<double> UB(UB_diag.size(),UB_diag.size());
   ** slip::fill_diagonal(UB,1.0);
   ** slip::set_diagonal(UB_up.begin(),UB_up.end(),UB,1);
   ** std::cout<<"UB = \n"<<UB<<std::endl;
   ** slip::Matrix<double> UBinv(UB.rows(),UB.cols());
   ** slip::unit_upper_bidiagonal_inv(UB_up.begin(),UB_up.end(),
   **			              UBinv.upper_left(),UBinv.bottom_right());
   ** std::cout<<"UBinv = \n"<<UBinv<< std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Matrix<double> UBUBinv(UB.rows(),UB.cols());
   ** slip::matrix_matrix_multiplies(UB,UBinv,UBUBinv);
   ** std::cout<<"UBUBinv = \n"<<UBUBinv<< std::endl;
   ** \endcode
   */
template<typename RandomAccessIterator1, 
	 typename RandomAccessIterator2d>
void unit_upper_bidiagonal_inv(RandomAccessIterator1 up_diag_first, 
			       RandomAccessIterator1 up_diag_last, 
			       RandomAccessIterator2d Ainv_up, 
			       RandomAccessIterator2d Ainv_bot)
{
  assert((Ainv_bot - Ainv_up)[0] == (Ainv_bot - Ainv_up)[1]);
  assert((Ainv_bot - Ainv_up)[0] == (up_diag_last - up_diag_first + 1));

   const int n = static_cast<int>((Ainv_bot - Ainv_up)[0]);
   //init Ainv with the identity matrix
   slip::identity(Ainv_up,Ainv_bot);
 
   for(int i = 0; i < n; ++i)
     {
       slip::unit_upper_bidiagonal_solve(up_diag_first,up_diag_last,
					 Ainv_up.col_begin(i),Ainv_up.col_end(i),
					 Ainv_up.col_begin(i),Ainv_up.col_end(i));
     }
   

}
/* @} */

/** \name Tridiagonal system solve algorithms */
  /* @{ */
/**
   ** \brief Solve the tridiagonal system A*X=B with the Thomas method
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & u_{1}&0&\cdots&0\\
   ** l_{1} & d_{2} & u_{2}&\ddots&\vdots\\
   ** 0&l_{2}&\ddots&\ddots&0\\
   ** \vdots&\ddots&\ddots&\ddots&u_{n-1}\\
   ** 0&\cdots&0&l_{n-1}&d_{n}\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** \vdots\\
   ** \vdots\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** \vdots\\
   ** \vdots\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/04
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param diag_first RandomAccessIterator to the beginning of the main diagonal.
   ** \param diag_last RandomAccessIterator to one-past-the-end of the main diagonal.
   ** \param up_diag_first RandomAccessIterator to the beginning of the first upper diagonal.
   ** \param up_diag_last RandomAccessIterator to one-past-the-end of the first upper diagonal.  
   ** \param low_diag_first RandomAccessIterator to the beginning of the first lower diagonal.
   ** \param low_diag_last RandomAccessIterator to one-past-the-end of the first lower diagonal.
   ** \param X_first RandomAccessIterator to the begenning of the result range.
   ** \param X_last RandomAccessIterator to one-past-the-end of the result range.
   ** \param B_first RandomAccessIterator to the begenning of the B range.
   ** \param B_last RandomAccessIterator to one-past-the-end of the B range.
   ** \param precision 1.0E-6 by default.
   ** \pre diag_last != diag_first
   ** \pre (diag_last - diag_first) == (X_last - X_first)
   ** \pre (diag_last - diag_first) == (B_last - B_first)
   ** \pre (diag_last - diag_first) == (up_diag_last - up_diag_first + 1)
   ** \pre (low_diag_last - low_diag_first) == (up_diag_last - up_diag_first)
   ** \par Complexity: 8n - 7 flops
   ** \remarks Works with real and complex data
   ** \remarks Will raise an exception if the matrix is singular
   ** \par Example:
   ** \code
   ** double d[]= {1.0,2.0,3.0,4.0};
   ** slip::Array<double> diag(4,d);
   ** double dup[]= {3.0,1.0,1.0};
   ** slip::Array<double> diag_up(3,dup); 
   ** double ddown[]= {2.0,2.0,2.0};
   ** slip::Array<double> diag_down(3,ddown);
   ** slip::Array<double> X(4);
   ** double b[]= {4.0,2.0,3.0,1.0};
   ** slip::Array<double> B(4,b);
   ** std::cout<<B<<std::endl;
   ** slip::thomas_solve(diag.begin(),diag.end(),
   **		     diag_up.begin(),diag_up.end(),
   **	     diag_down.begin(),diag_down.end(),
   **		     X.begin(),X.end(),
   **		     B.begin(),B.end());
   ** std::cout<<X<<std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Array<double> B2(4);
   ** slip::matrix_vector_multiplies(D3,X,B2);
   ** std::cout<<"D3 * X = "<<std::endl;
   ** std::cout<<B2<<std::endl;
   ** \endcode
   */  
  template<typename RandomAccessIterator1, 
	   typename RandomAccessIterator2,
	   typename RandomAccessIterator3>
  void thomas_solve(RandomAccessIterator1 diag_first, RandomAccessIterator1 diag_last,
		    RandomAccessIterator1 up_diag_first, RandomAccessIterator1 up_diag_last, 
		    RandomAccessIterator1 low_diag_first, RandomAccessIterator1 low_diag_last, 
		    RandomAccessIterator2 X_first, RandomAccessIterator2 X_last, 
		    RandomAccessIterator3 B_first, RandomAccessIterator3 B_last,
		    typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type precision = typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type(1.0E-8))
  {
    assert(diag_last != diag_first);
    assert((diag_last - diag_first) == (X_last - X_first));
    assert((diag_last - diag_first) == (B_last - B_first));
    assert((diag_last - diag_first) == (up_diag_last - up_diag_first + 1));
    assert((low_diag_last - low_diag_first) == (up_diag_last - up_diag_first));

    typedef typename std::iterator_traits<RandomAccessIterator1>::difference_type _Difference;
    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type;
    _Difference n = (diag_last - diag_first);

    //tridiagonal LU decomposition
    slip::Array<value_type> M(n);
    slip::Array<value_type> L(n-1);
    tridiagonal_lu(diag_first,diag_last,
		   up_diag_first,up_diag_last,
		   low_diag_first,low_diag_last,
		   L.begin(),L.end(),
		   M.begin(),M.end(),precision);
		   
    //First step: resolution of the lower bidiagonal system LY = B
    //L main diagonal are ones
    //L lower diagonal is L vector
    slip::Array<value_type> Y(n);
    unit_lower_bidiagonal_solve(L.begin(),L.end(),Y.begin(),Y.end(),B_first,B_last);
    //Second step: resolution of the upper bidiagonal system UX = Y
    //U main diagonal is M vector
    //U upper diagonal is up_diag range
    upper_bidiagonal_solve(M.begin(),M.end(),up_diag_first,up_diag_last,X_first,X_last,Y.begin(),Y.end(),precision);
  
  }


 /**
  ** \brief Solve the tridiagonal system Tx=B
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & u_{1}&0&\cdots&0\\
   ** l_{1} & d_{2} & u_{2}&\ddots&\vdots\\
   ** 0&l_{2}&\ddots&\ddots&0\\
   ** \vdots&\ddots&\ddots&\ddots&u_{n-1}\\
   ** 0&\cdots&0&l_{n-1}&d_{n}\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** \vdots\\
   ** \vdots\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** \vdots\\
   ** \vdots\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2008/10/04
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param A A matrix.
  ** \param X X Vector.
  ** \param B B Vector.
  ** \param precision 1.0E-6 by default.

  ** \pre A.dim1() == A.dim2()
  ** \pre X.size() == A.dim1()
  ** \pre B.size() == A.dim1()
  ** \par Complexity: 8n - 7 flops
  ** \remarks Works with real and complex data
  ** \remarks Will raise an exception if the matrix is singular
   ** \par Example:
  ** \code
  ** double d[]= {1.0,2.0,3.0,4.0};
  ** slip::Array<double> diag(4,d);
  ** double dup[]= {3.0,1.0,1.0};
  ** slip::Array<double> diag_up(3,dup); 
  ** double ddown[]= {2.0,2.0,2.0};
  ** slip::Array<double> diag_down(3,ddown);
  ** slip::Array<double> X(4);
  ** double b[]= {4.0,2.0,3.0,1.0};
  ** slip::Array<double> B(4,b);
  ** slip::Array2d<double> D3(4,4,0.0);
  ** slip::set_diagonal(diag.begin(),diag.end(),D3);
  ** slip::set_diagonal(diag_up.begin(),diag_up.end(),D3,1);
  ** slip::set_diagonal(diag_down.begin(),diag_down.end(),D3,-1);
  ** std::cout<<"D3 = "<<std::endl;
  ** std::cout<<D3<<std::endl;
  ** std::cout<<"B = "<<std::endl;
  ** std::cout<<B<<std::endl;
  ** std::cout<<std::endl;
  ** slip::thomas_solve(D3,X,B);
  ** std::cout<<"X = "<<std::endl;
  ** std::cout<<X<<std::endl;
  ** std::cout<<std::endl;
  ** std::cout<<"Check:"<<std::endl;
  ** slip::Array<double> B2(4);
  ** slip::matrix_vector_multiplies(D3,X,B2);
  ** std::cout<<"D3 * X = "<<std::endl;
  ** std::cout<<B2<<std::endl;
  ** \endcode
  */  
  template<typename Container2d,
	   typename Vector1, 
	   typename Vector2>
  void thomas_solve(const Container2d& A,
		    Vector1& X,
		    const Vector2& B,
		    typename slip::lin_alg_traits<typename Container2d::value_type>::value_type precision = typename slip::lin_alg_traits<typename Container2d::value_type>::value_type(1.0E-8))
  {
    
    assert(A.dim1() == A.dim2());
    assert(X.size() == A.dim1());
    assert(B.size() == A.dim1());
    
    typedef typename Container2d::value_type value_type;
    typedef typename Container2d::size_type size_type;

    //get the 3 main diagonals
    size_type n = A.dim1();
    slip::Array<value_type> diag(n);
    slip::Array<value_type> up_diag(n-1);
    slip::Array<value_type> low_diag(n-1);
    slip::get_diagonal(A,diag.begin(),diag.end());
    slip::get_diagonal(A,up_diag.begin(),up_diag.end(),1);
    slip::get_diagonal(A,low_diag.begin(),low_diag.end(),-1);
    //tridiagonal solve
    thomas_solve(diag.begin(),diag.end(),
		 up_diag.begin(),up_diag.end(),
		 low_diag.begin(),low_diag.end(),
		 X.begin(),X.end(),
		 B.begin(),B.end(),precision);
  }
 
  /**
   ** \brief Invert a tridiagonal squared matrix A with the Thomas method
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & u_{1}&0&\cdots&0\\
   ** l_{1} & d_{2} & u_{2}&\ddots&\vdots\\
   ** 0&l_{2}&\ddots&\ddots&0\\
   ** \vdots&\ddots&\ddots&\ddots&u_{n-1}\\
   ** 0&\cdots&0&l_{n-1}&d_{n}\\
   ** \end{array}\right)^{-1}
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/04/12
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param diag_first RandomAccessIterator to the beginning of the main diagonal.
   ** \param diag_last RandomAccessIterator to one-past-the-end of the main diagonal.
   ** \param up_diag_first RandomAccessIterator to the beginning of the first upper diagonal.
   ** \param up_diag_last RandomAccessIterator to one-past-the-end of the first upper diagonal.  
   ** \param low_diag_first RandomAccessIterator to the beginning of the first lower diagonal.
   ** \param low_diag_last RandomAccessIterator to one-past-the-end of the first lower diagonal.
   ** \param Ainv_up  RandomAccessIterator2d to the upper_left element of the 2d range of the inverse matrix.
   ** \param Ainv_bot RandomAccessIterator2d to the one past the bottom_right element of the 2d range of the inverse matrix.
   ** \param precision 1.0E-6 by default.
   ** \pre diag_last != diag_first
   ** \pre (diag_last - diag_first) == (up_diag_last - up_diag_first + 1)
   ** \pre (low_diag_last - low_diag_first) == (up_diag_last - up_diag_first)
   ** \pre (Ainv_bot - Ainv_up)[0] == (Ainv_bot - Ainv_up)[1])
   ** \pre (Ainv_bot - Ainv_up)[0] == (diag_last - diag_first)
   ** \par Complexity: 8n^2 - 7n flops
   ** \remarks Works with real and complex data
   ** \remarks Will raise an exception if the matrix is singular
   ** \par Example:
   ** \code
   ** double d[]= {1.0,2.0,3.0,4.0};
   ** slip::Array<double> diag(4,d);
   ** double dup[]= {3.0,1.0,1.0};
   ** slip::Array<double> diag_up(3,dup); 
   ** double ddown[]= {2.0,2.0,2.0};
   ** slip::Array<double> diag_down(3,ddown);
   ** slip::Array2d<double> D3(4,4,0.0);
   ** slip::set_diagonal(diag.begin(),diag.end(),D3);
   ** slip::set_diagonal(diag_up.begin(),diag_up.end(),D3,1);
   ** slip::set_diagonal(diag_down.begin(),diag_down.end(),D3,-1);
   ** std::cout<<"D3 = \n"<<D3<<std::endl;
   ** slip::Matrix<double> D3inv(D3.rows(),D3.cols());
   ** slip::thomas_inv(diag.begin(),diag.end(),
   **	    diag_up.begin(),diag_up.end(),
   **	    diag_down.begin(),diag_down.end(),
   **	    D3inv.upper_left(),D3inv.bottom_right());
   ** std::cout<<"D3inv = \n "<<D3inv<< std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Matrix<double> D3D3inv(D3.rows(),D3.cols());
   ** slip::matrix_matrix_multiplies(D3,D3inv,D3D3inv);
   ** std::cout<<"D3D3inv = \n "<<D3D3inv<< std::endl;
   ** \endcode
   */
template<typename RandomAccessIterator1, 
	 typename RandomAccessIterator2d>
void thomas_inv(RandomAccessIterator1 diag_first, RandomAccessIterator1 diag_last,
		RandomAccessIterator1 up_diag_first, RandomAccessIterator1 up_diag_last, 
		RandomAccessIterator1 low_diag_first, RandomAccessIterator1 low_diag_last, 
		RandomAccessIterator2d Ainv_up, RandomAccessIterator2d Ainv_bot, 
		typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type precision = typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type(1.0E-8))
{
   assert(diag_last != diag_first);
   assert((diag_last - diag_first) == (up_diag_last - up_diag_first + 1));
   assert((low_diag_last - low_diag_first) == (up_diag_last - up_diag_first));
   assert((Ainv_bot - Ainv_up)[0] == (Ainv_bot - Ainv_up)[1]);
   assert((Ainv_bot - Ainv_up)[0] == (diag_last - diag_first));
   typedef typename std::iterator_traits<RandomAccessIterator1>::difference_type _Difference;

   const _Difference n = (diag_last - diag_first);
   //init Ainv with the identity matrix
   slip::identity(Ainv_up,Ainv_bot);
 
   for(_Difference i = 0; i < n; ++i)
     {
       slip::thomas_solve(diag_first,diag_last,
			  up_diag_first,up_diag_last,
			  low_diag_first,low_diag_last,
			  Ainv_up.col_begin(i),Ainv_up.col_end(i),
			  Ainv_up.col_begin(i),Ainv_up.col_end(i),
			  precision);
     }
   

}
/**
   ** \brief Invert a tridiagonal squared matrix A with the Thomas method
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & u_{1}&0&\cdots&0\\
   ** l_{1} & d_{2} & u_{2}&\ddots&\vdots\\
   ** 0&l_{2}&\ddots&\ddots&0\\
   ** \vdots&\ddots&\ddots&\ddots&u_{n-1}\\
   ** 0&\cdots&0&l_{n-1}&d_{n}\\
   ** \end{array}\right)^{-1}
   ** \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/04/12
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A 2d container of the matrix to invert.
   ** \param Ainv 2d container of the matrix the inverse matrix.
   ** \param precision 1.0E-6 by default.
   ** \pre A.dim1() == A.dim2()
   ** \pre Ainv.dim1() == Ainv.dim2()
   ** 
   ** \par Complexity: 8n^2 - 7n flops + (3n - 2) flops
   ** \remarks Works with real and complex data
   ** \remarks Will raise an exception if the matrix is singular
   ** \par Example:
   ** \code
   ** double d[]= {1.0,2.0,3.0,4.0};
   ** slip::Array<double> diag(4,d);
   ** double dup[]= {3.0,1.0,1.0};
   ** slip::Array<double> diag_up(3,dup); 
   ** double ddown[]= {2.0,2.0,2.0};
   ** slip::Array<double> diag_down(3,ddown);
   ** slip::Array2d<double> D3(4,4,0.0);
   ** slip::set_diagonal(diag.begin(),diag.end(),D3);
   ** slip::set_diagonal(diag_up.begin(),diag_up.end(),D3,1);
   ** slip::set_diagonal(diag_down.begin(),diag_down.end(),D3,-1);
   ** std::cout<<"D3 = \n"<<D3<<std::endl;
   ** slip::Matrix<double> D3inv(D3.rows(),D3.cols());
   ** slip::thomas_inv(D3,D3inv);
   ** std::cout<<"D3inv = \n "<<D3inv<< std::endl;
   ** std::cout<<"Check:"<<std::endl;
   ** slip::Matrix<double> D3D3inv(D3.rows(),D3.cols());
   ** slip::matrix_matrix_multiplies(D3,D3inv,D3D3inv);
   ** std::cout<<"D3D3inv = \n "<<D3D3inv<< std::endl;
   ** \endcode
   */
 template<typename Container2d1,
	   typename Container2d2>
  void thomas_inv(const Container2d1& A,
		  Container2d2& Ainv,
		  typename slip::lin_alg_traits<typename Container2d1::value_type>::value_type precision = typename slip::lin_alg_traits<typename Container2d1::value_type>::value_type(1.0E-8))
  {
    
    assert(A.dim1() == A.dim2());
    assert(Ainv.dim1() == Ainv.dim2());

    
    typedef typename Container2d1::value_type value_type;
    typedef typename Container2d1::size_type size_type;

    //get the 3 main diagonals
    const size_type n = A.dim1();
    slip::Array<value_type> diag(n);
    slip::Array<value_type> up_diag(n-1);
    slip::Array<value_type> low_diag(n-1);
    slip::get_diagonal(A,diag.begin(),diag.end());
    slip::get_diagonal(A,up_diag.begin(),up_diag.end(),1);
    slip::get_diagonal(A,low_diag.begin(),low_diag.end(),-1);
    slip::thomas_inv(diag.begin(),diag.end(),
		     up_diag.begin(),up_diag.end(),
		     low_diag.begin(),low_diag.end(),
		     Ainv.upper_left(),
		     Ainv.bottom_right(),
		     precision);

  }
/* @} */

/** \name Tridiagonal symmetric positive matrix algorithms */
  /* @{ */

/**
 ** \brief Tridiagonal symmetric positive decomposition 
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & u_{1}&0&\cdots&0\\
   ** \bar{u}_{1} & d_{2} & u_{2}&\ddots&\vdots\\
   ** 0&\bar{u}_{2}&\ddots&\ddots&0\\
   ** \vdots&\ddots&\ddots&\ddots&u_{n-1}\\
   ** 0&\cdots&0&\bar{u}_{n-1}&d_{n}\\
   ** \end{array}\right) =
  ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & 0&\cdots&\cdots&0\\
   ** r_1 & d_{2} & \ddots&&\vdots\\
   ** 0&r_2&\ddots&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&\ddots&0\\
   ** 0&\cdots&0&r_{n-1}&d_{n}\\
   ** \end{array}\right)
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & \bar{r}_{1}&0&\cdots&0\\
   ** 0 & d_{2} & \bar{r}_{2}&\ddots&\vdots\\
   ** \vdots&\ddots&\ddots&\ddots&0\\
   ** \vdots&&\ddots&\ddots&\bar{r}_{n-1}\\
   ** 0&\cdots&\cdots&0&d_{n}\\
   ** \end{array}\right)
   ** \f]
   **  where: \f$\bar{u}_{i}\f$ is the conjugate of  \f$u_{i}\f$ and \f$d_{i} = \bar{d}_{i}\f$.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/04
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param diag_first RandomAccessIterator to the beginning of the main diagonal.
   ** \param diag_last RandomAccessIterator to one-past-the-end of the main diagonal.
   ** \param up_diag_first RandomAccessIterator to the beginning of the first upper diagonal.
   ** \param up_diag_last RandomAccessIterator to one-past-the-end of the first upper diagonal. 
   ** \param R_diag_first RandomAccessIterator to the beginning of the first diagonal of R.
   ** \param R_diag_last RandomAccessIterator to one-past-the-end of the first  diagonal of R. 
   ** \param R_low_first RandomAccessIterator to the beginning of the first lower diagonal.
   ** \param R_low_last RandomAccessIterator to one-past-the-end of the first lower diagonal. 
   ** \pre diag_last != diag_first
   ** \pre (diag_last - diag_first) == (up_diag_last - up_diag_first + 1)
   ** \pre (R_diag_last-R_diag_first) == (diag_last - diag_first)
   ** \pre (R_diag_last-R_diag_first) == (R_low_last - R_low_first + 1)
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is not hermitian positive definite
   ** \par Example:
   ** \code
   ** slip::Array2d<float> TriChol(4,4);
   ** slip::fill_diagonal(TriChol,2.0);
   ** slip::fill_diagonal(TriChol,-1.0,-1);
   ** slip::fill_diagonal(TriChol,-1.0,1);
   ** std::cout<<"TriChol = "<<std::endl;
   ** std::cout<<TriChol<<std::endl;
   ** slip::Array<float> TriChol_diag(4);
   ** slip::get_diagonal(TriChol,TriChol_diag.begin(),TriChol_diag.end());
   ** slip::Array<float> TriChol_low_diag(3);
   ** slip::get_diagonal(TriChol,TriChol_low_diag.begin(),TriChol_low_diag.end(),1);
   ** std::cout<<"TriChol_diag = "<<std::endl;
   ** std::cout<<TriChol_diag <<std::endl;
   ** std::cout<<"TriChol_low_diag = "<<std::endl;
   ** std::cout<<TriChol_low_diag<<std::endl;
   ** slip::Array<float> R_TriChol_diag(4);
   ** slip::Array<float> R_TriChol_low_diag(3);
   ** slip::tridiagonal_cholesky(TriChol_diag.begin(),TriChol_diag.end(),
   **		      TriChol_low_diag.begin(),TriChol_low_diag.end(),
   **		      R_TriChol_diag.begin(),R_TriChol_diag.end(),
   **		      R_TriChol_low_diag.begin(),R_TriChol_low_diag.end());
   ** std::cout<<"R_TriChol_diag = "<<std::endl;
   ** std::cout<<R_TriChol_diag <<std::endl;
   ** std::cout<<"R_TriChol_low_diag = "<<std::endl;
   ** std::cout<<R_TriChol_low_diag<<std::endl;
   **
   ** slip::Array2d<float> LTriChol(4,4);
   ** slip::Array2d<float> LTriCholT(4,4);
   ** slip::set_diagonal(R_TriChol_diag.begin(),R_TriChol_diag.end(),LTriChol);
   ** slip::set_diagonal(R_TriChol_low_diag.begin(),R_TriChol_low_diag.end(),LTriChol,-1);
   ** slip::transpose(LTriChol,LTriCholT);
   ** std::cout<<"LTriChol = "<<std::endl;
   ** std::cout<<LTriChol <<std::endl;
   ** std::cout<<"LTriCholT = "<<std::endl;
   ** std::cout<<LTriCholT<<std::endl;
   ** slip::Array2d<float> LTriCholxLTriCholT(4,4);
   ** slip::matrix_matrix_multiplies(LTriChol,LTriCholT,LTriCholxLTriCholT);
   ** std::cout<<"LTriCholxLTriChol^T  = "<<std::endl;
   ** std::cout<<LTriCholxLTriCholT<<std::endl;
   ** \endcode
   */
template<typename RandomAccessIterator1, 
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator3>
void tridiagonal_cholesky(RandomAccessIterator1 diag_first, RandomAccessIterator1 diag_last,
			  RandomAccessIterator1 up_diag_first, RandomAccessIterator1 up_diag_last, 

			  RandomAccessIterator2 R_diag_first, RandomAccessIterator2 R_diag_last, 
			  RandomAccessIterator3 R_low_first, RandomAccessIterator3 R_low_last)
 {
    assert(diag_last != diag_first);
    assert((diag_last - diag_first) == (up_diag_last - up_diag_first + 1));
    assert((R_diag_last-R_diag_first) == (diag_last - diag_first));
    assert((R_diag_last-R_diag_first) == (R_low_last - R_low_first + 1));
  
	typedef typename slip::lin_alg_traits<typename std::iterator_traits<RandomAccessIterator1>::value_type>::value_type real_type;
	typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type;
	if(std::real(*diag_first) <= real_type(0))
	  {
	    throw std::runtime_error(slip::POSITIVE_DEFINITE_MATRIX_ERROR);
	  }
	*R_diag_first = std::sqrt(*diag_first);
	
	for(; R_low_first != R_low_last; ++R_low_first, ++up_diag_first, ++diag_first, ++R_diag_first)
	  {
	   
	    
	    *R_low_first  = slip::conj(*up_diag_first / *R_diag_first);
	    value_type proj = *(diag_first + 1) - slip::conj(*R_low_first) * *R_low_first;
	    if(std::real(proj) <= real_type(0))
	      {
		throw std::runtime_error(slip::POSITIVE_DEFINITE_MATRIX_ERROR);
	      }
	    *(R_diag_first + 1) = std::sqrt(proj);
	  }
   
 }
 /**
   ** \brief Solve the tridiagonal symmetric positive definite system T*X=B with the Cholesky method
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & u_{1}&0&\cdots&0\\
   ** \bar{u}_{1} & d_{2} & u_{2}&\ddots&\vdots\\
   ** 0&\bar{u}_{2}&\ddots&\ddots&0\\
   ** \vdots&\ddots&\ddots&\ddots&u_{n-1}\\
   ** 0&\cdots&0&\bar{u}_{n-1}&d_{n}\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** \vdots\\
   ** \vdots\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** \vdots\\
   ** \vdots\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   **  where: \f$\bar{u}_{i}\f$ is the conjugate of  \f$u_{i}\f$ and \f$d_{i} = \bar{d}_{i}\f$.
   ** 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/04
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param diag_first RandomAccessIterator to the beginning of the main diagonal.
   ** \param diag_last RandomAccessIterator to one-past-the-end of the main diagonal.
   ** \param up_diag_first RandomAccessIterator to the beginning of the first upper diagonal.
   ** \param up_diag_last RandomAccessIterator to one-past-the-end of the first upper diagonal.  
   ** \param X_first RandomAccessIterator to the begenning of the result range.
   ** \param X_last RandomAccessIterator to one-past-the-end of the result range.
   ** \param B_first RandomAccessIterator to the begenning of the B range.
   ** \param B_last RandomAccessIterator to one-past-the-end of the B range.
   ** \pre T must be symmetric positive definite matrix.
   ** \pre (X_last - X_first) == (B_last - B_first)
   ** \pre (diag_last - diag_first) == (X_last - X_first)
   ** \pre (diag_last - diag_first) == (up_diag_last - up_diag_first + 1)
   ** \par Complexity: 3n + n sqrt
   ** \remarks Works with real and complex data
   ** \remarks Raise an exception if T is not a symmetric positive definite matrix.
   **\par Example:
   **\code
   ** slip::Array2d<float> TriChol(4,4);
   ** slip::fill_diagonal(TriChol,2.0);
   ** slip::fill_diagonal(TriChol,-1.0,-1);
   ** slip::fill_diagonal(TriChol,-1.0,1);
   ** std::cout<<"TriChol = "<<std::endl;
   ** std::cout<<TriChol<<std::endl;
   ** slip::Array<float> TriChol_diag(4);
   ** slip::get_diagonal(TriChol,TriChol_diag.begin(),TriChol_diag.end());
   ** slip::Array<float> TriChol_low_diag(3);
   ** slip::get_diagonal(TriChol,TriChol_low_diag.begin(),TriChol_low_diag.end(),1);
   ** std::cout<<"TriChol_diag = "<<std::endl;
   ** std::cout<<TriChol_diag <<std::endl;
   ** std::cout<<"TriChol_low_diag = "<<std::endl;
   ** std::cout<<TriChol_low_diag<<std::endl;
   ** std::cout<<"TriChol = "<<std::endl;
   ** std::cout<<TriChol<<std::endl;
   ** float btricol[] = {1.0,2.0,3.0,4.0};
   ** slip::Array<float> BTriChol(4,btricol);
   ** std::cout<<"BTriChol = \n"<<BTriChol<<std::endl;
   ** slip::Array<float> XTriChol(4);
   ** slip::tridiagonal_cholesky_solve(TriChol_diag.begin(),TriChol_diag.end(),
   **				    TriChol_low_diag.begin(),TriChol_low_diag.end(),
   **			    XTriChol.begin(),XTriChol.end(),
   **			    BTriChol.begin(),BTriChol.end());
   ** std::cout<<"XTriChol = \n"<<XTriChol<<std::endl;
   ** std::cout<<"Check: "<<std::endl;
   ** slip::Array<float> BTriChol2(4);
   ** slip::matrix_vector_multiplies(TriChol,XTriChol,BTriChol2);
   ** std::cout<<"BTriChol2 = \n"<<BTriChol2<<std::endl;
   ** \endcode
   */  
template<typename RandomAccessIterator1, 
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator3>
  void tridiagonal_cholesky_solve(RandomAccessIterator1 diag_first, RandomAccessIterator1 diag_last,
				  RandomAccessIterator1 up_diag_first, RandomAccessIterator1 up_diag_last, 
				  RandomAccessIterator2 X_first, RandomAccessIterator2 X_last, 
				  RandomAccessIterator3 B_first, RandomAccessIterator3 B_last)
  {
    assert((X_last - X_first) == (B_last - B_first));
    assert((diag_last - diag_first) == (X_last - X_first));
    assert((diag_last - diag_first) == (up_diag_last - up_diag_first + 1));
    typedef typename std::iterator_traits<RandomAccessIterator1>::difference_type _Difference;
    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type;
    const _Difference n = (diag_last - diag_first);

    //tridiagonal LU decomposition
    slip::Array<value_type> R_diag(n);
    slip::Array<value_type> R_low(n-1);
    slip::tridiagonal_cholesky(diag_first,diag_last,
			up_diag_first,up_diag_last,
			R_diag.begin(),R_diag.end(),
			R_low.begin(),R_low.end());
		   
    //First step: resolution of the lower bidiagonal system LY = B
    //L main diagonal are ones
    //L lower diagonal is L vector
    slip::Array<value_type> Y(n);
    slip::lower_bidiagonal_solve(R_diag.begin(),R_diag.end(),
				 R_low.begin(),R_low.end(),
				 Y.begin(),Y.end(),
				 B_first,B_last);
    //Second step: resolution of the upper bidiagonal system UX = Y
    //U main diagonal is M vector
    //U upper diagonal is up_diag range
    slip::apply(R_low.begin(),R_low.end(),R_low.begin(),slip::conj);
    slip::upper_bidiagonal_solve(R_diag.begin(),R_diag.end(),
				 R_low.begin(),R_low.end(),
				 X_first,X_last,
				 Y.begin(),Y.end());
  
  }

 /**
   ** \brief Solve the tridiagonal symmetric positive definite system T*X=B with the Cholesky method
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & u_{1}&0&\cdots&0\\
   ** \bar{u}_{1} & d_{2} & u_{2}&\ddots&\vdots\\
   ** 0&\bar{u}_{2}&\ddots&\ddots&0\\
   ** \vdots&\ddots&\ddots&\ddots&u_{n-1}\\
   ** 0&\cdots&0&\bar{u}_{n-1}&d_{n}\\
   ** \end{array}\right) 
   ** \left(
   ** \begin{array}{c}
   ** x_{1}\\
   ** \vdots\\
   ** \vdots\\
   ** \vdots\\
   ** x_{n}\\
   ** \end{array}\right) =
   ** \left(
   ** \begin{array}{c}
   ** b_{1}\\
   ** \vdots\\
   ** \vdots\\
   ** \vdots\\
   ** b_{n}\\
   ** \end{array}\right) 
   ** \f]
   **  where: \f$\bar{u}_{i}\f$ is the conjugate of  \f$u_{i}\f$ and \f$d_{i} = \bar{d}_{i}\f$.
   ** 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/04
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A The matrix A.
   ** \param X The vector X.
   ** \param B The vector to match B.
   ** \pre T must be symmetric positive definite matrix.
   ** \pre X.size() == B.size()
   ** \pre T.cols() == X.size()
   ** \pre T.rows() == T.cols()
   ** \par Complexity: 5n - 1 + n sqrt
   ** \remarks Works with real and complex data
   ** \remarks Raise an exception if T is not a symmetric positive definite matrix.
   **\par Example:
   **\code
   ** slip::Array2d<float> TriChol(4,4);
   ** slip::fill_diagonal(TriChol,2.0);
   ** slip::fill_diagonal(TriChol,-1.0,-1);
   ** slip::fill_diagonal(TriChol,-1.0,1);
   ** std::cout<<"TriChol = "<<std::endl;
   ** std::cout<<TriChol<<std::endl;
   ** slip::Array<float> TriChol_diag(4);
   ** slip::get_diagonal(TriChol,TriChol_diag.begin(),TriChol_diag.end());
   ** slip::Array<float> TriChol_low_diag(3);
   ** slip::get_diagonal(TriChol,TriChol_low_diag.begin(),TriChol_low_diag.end(),1);
   ** std::cout<<"TriChol_diag = "<<std::endl;
   ** std::cout<<TriChol_diag <<std::endl;
   ** std::cout<<"TriChol_low_diag = "<<std::endl;
   ** std::cout<<TriChol_low_diag<<std::endl;
   ** std::cout<<"TriChol = "<<std::endl;
   ** std::cout<<TriChol<<std::endl;
   ** float btricol[] = {1.0,2.0,3.0,4.0};
   ** slip::Array<float> BTriChol(4,btricol);
   ** std::cout<<"BTriChol = \n"<<BTriChol<<std::endl;
   ** slip::Array<float> XTriChol(4);
   ** slip::tridiagonal_cholesky_solve(TriChol,XTriChol,BTriChol);
   ** std::cout<<"XTriChol = \n"<<XTriChol<<std::endl;
   ** std::cout<<"Check: "<<std::endl;
   ** slip::Array<float> BTriChol2(4);
   ** slip::matrix_vector_multiplies(TriChol,XTriChol,BTriChol2);
   ** std::cout<<"BTriChol2 = \n"<<BTriChol2<<std::endl;
   ** \endcode
   */  
template<typename Container2d,
	   typename Vector1, 
	   typename Vector2>
  void tridiagonal_cholesky_solve(const Container2d& A,
				  Vector1& X,
				  const Vector2& B)
  {
    
    assert(A.dim1() == A.dim2());
    assert(X.size() == A.dim1());
    assert(B.size() == A.dim1());
    
    typedef typename Container2d::value_type value_type;
    typedef typename Container2d::size_type size_type;

    //get the 2 main diagonals
    size_type n = A.dim1();
    slip::Array<value_type> diag(n);
    slip::Array<value_type> up_diag(n-1);
    slip::get_diagonal(A,diag.begin(),diag.end());
    slip::get_diagonal(A,up_diag.begin(),up_diag.end(),1);
    //tridiagonal solve
    slip::tridiagonal_cholesky_solve(diag.begin(),diag.end(),
			       up_diag.begin(),up_diag.end(),
			       X.begin(),X.end(),
			       B.begin(),B.end());
  }
 
/**
 ** \brief Invert the tridiagonal symmetric positive definite system T:
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & u_{1}&0&\cdots&0\\
   ** \bar{u}_{1} & d_{2} & u_{2}&\ddots&\vdots\\
   ** 0&\bar{u}_{2}&\ddots&\ddots&0\\
   ** \vdots&\ddots&\ddots&\ddots&u_{n-1}\\
   ** 0&\cdots&0&\bar{u}_{n-1}&d_{n}\\
   ** \end{array}\right)^{-1} 
   ** \f]
   **  where: \f$\bar{u}_{i}\f$ is the conjugate of  \f$u_{i}\f$ and \f$d_{i} = \bar{d}_{i}\f$.
   ** 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/04/12
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param diag_first RandomAccessIterator to the beginning of the main diagonal.
   ** \param diag_last RandomAccessIterator to one-past-the-end of the main diagonal.
   ** \param up_diag_first RandomAccessIterator to the beginning of the first upper diagonal.
   ** \param up_diag_last RandomAccessIterator to one-past-the-end of the first upper diagonal.  
   ** \param Ainv_up  RandomAccessIterator2d to the upper_left element of the 2d range of the inverse matrix.
   ** \param Ainv_bot RandomAccessIterator2d to the one past the bottom_right element of the 2d range of the inverse matrix.
   ** \pre diag_last != diag_first
   ** \pre (diag_last - diag_first) == (up_diag_last - up_diag_first + 1)
   ** \pre (Ainv_bot - Ainv_up)[0] == (Ainv_bot - Ainv_up)[1])
   ** \pre (Ainv_bot - Ainv_up)[0] == (diag_last - diag_first)
   ** \par Complexity: 5n^2 - n flops + n^2 sqrt
   ** \remarks Works with real and complex data
   ** \remarks Will raise an exception if the matrix is singular
   ** \par Example:
   ** \code
   ** slip::Array2d<std::complex<float> > TriCholc(4,4);
   ** slip::fill_diagonal(TriCholc,std::complex<float>(2.0,0.0));
   ** slip::fill_diagonal(TriCholc,std::complex<float>(-1.0,0.5),-1);
   ** slip::fill_diagonal(TriCholc,std::complex<float>(-1.0,-0.5),1);
   ** std::cout<<"TriCholc = \n"<<TriCholc<<std::endl;
   ** slip::Array<std::complex<float> > TriChol_diagc(4);
   ** slip::get_diagonal(TriCholc,TriChol_diagc.begin(),TriChol_diagc.end());
   ** slip::Array<std::complex<float> > TriChol_low_diagc(3);
   ** slip::get_diagonal(TriCholc,TriChol_low_diagc.begin(),TriChol_low_diagc.end(),1);
   ** slip::Matrix<std::complex<float> > TriCholinvc (TriCholc.rows(),TriCholc.cols());
   ** slip::tridiagonal_cholesky_inv(TriChol_diagc.begin(),TriChol_diagc.end(),
   **			  TriChol_low_diagc.begin(),TriChol_low_diagc.end(),TriCholinvc.upper_left(),TriCholinvc.bottom_right());
   ** std::cout<<"TriCholinvc  = \n"<<TriCholinvc<<std::endl;
   ** slip::Matrix<std::complex<float> > TriCholcTriCholinvc(TriCholc.rows(),TriCholc.cols());
   ** std::cout<<"Check "<<std::endl;
   ** slip::matrix_matrix_multiplies(TriCholc,TriCholinvc,TriCholcTriCholinvc);
   ** std::cout<<"TriCholcTriCholinvc  = \n"<<TriCholcTriCholinvc<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator1, 
	   typename RandomAccessIterator2d>
void tridiagonal_cholesky_inv(RandomAccessIterator1 diag_first, RandomAccessIterator1 diag_last,
			      RandomAccessIterator1 up_diag_first, RandomAccessIterator1 up_diag_last, 
			      RandomAccessIterator2d Ainv_up, RandomAccessIterator2d Ainv_bot)
{
   assert(diag_last != diag_first);
   assert((diag_last - diag_first) == (up_diag_last - up_diag_first + 1));
   assert((Ainv_bot - Ainv_up)[0] == (Ainv_bot - Ainv_up)[1]);
   assert((Ainv_bot - Ainv_up)[0] == (diag_last - diag_first));
   typedef typename std::iterator_traits<RandomAccessIterator1>::difference_type _Difference;

   const _Difference n = (diag_last - diag_first);
   //init Ainv with the identity matrix
   slip::identity(Ainv_up,Ainv_bot);
   for(_Difference i = 0; i < n; ++i)
     {
       slip::tridiagonal_cholesky_solve(diag_first,diag_last,
					up_diag_first,up_diag_last,
					Ainv_up.col_begin(i),Ainv_up.col_end(i),
					Ainv_up.col_begin(i),Ainv_up.col_end(i));
     }
   

}

/**
 ** \brief Invert the tridiagonal symmetric positive definite system T:
   **  \f[
   ** \left(
   ** \begin{array}{ccccc}
   ** d_{1} & u_{1}&0&\cdots&0\\
   ** \bar{u}_{1} & d_{2} & u_{2}&\ddots&\vdots\\
   ** 0&\bar{u}_{2}&\ddots&\ddots&0\\
   ** \vdots&\ddots&\ddots&\ddots&u_{n-1}\\
   ** 0&\cdots&0&\bar{u}_{n-1}&d_{n}\\
   ** \end{array}\right)^{-1} 
   ** \f]
   **  where: \f$\bar{u}_{i}\f$ is the conjugate of  \f$u_{i}\f$ and \f$d_{i} = \bar{d}_{i}\f$.
   ** 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/04/12
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A 2d container of the matrix to invert.
   ** \param Ainv 2d container of the matrix the inverse matrix.
   ** \pre A.dim1() == A.dim2()
   ** \pre Ainv.dim1() == Ainv.dim2()
   ** 
   ** \par Complexity: 5n^2 - n flops + n^2 sqrt + (2n - 1) flops
   ** \remarks Works with real and complex data
   ** \remarks Will raise an exception if the matrix is singular
   ** \par Example:
   ** \code
    ** slip::Array2d<std::complex<float> > TriCholc(4,4);
   ** slip::fill_diagonal(TriCholc,std::complex<float>(2.0,0.0));
   ** slip::fill_diagonal(TriCholc,std::complex<float>(-1.0,0.5),-1);
   ** slip::fill_diagonal(TriCholc,std::complex<float>(-1.0,-0.5),1);
   ** std::cout<<"TriCholc = \n"<<TriCholc<<std::endl;
   ** slip::Array<std::complex<float> > TriChol_diagc(4);
   ** slip::get_diagonal(TriCholc,TriChol_diagc.begin(),TriChol_diagc.end());
   ** slip::Array<std::complex<float> > TriChol_low_diagc(3);
   ** slip::get_diagonal(TriCholc,TriChol_low_diagc.begin(),TriChol_low_diagc.end(),1);
   ** slip::Matrix<std::complex<float> > TriCholinvc (TriCholc.rows(),TriCholc.cols());
   ** slip::tridiagonal_cholesky_inv(TriCholc,TriCholinvc);
   ** std::cout<<"TriCholinvc  = \n"<<TriCholinvc<<std::endl;
   ** slip::Matrix<std::complex<float> > TriCholcTriCholinvc(TriCholc.rows(),TriCholc.cols());
   ** std::cout<<"Check "<<std::endl;
   ** slip::matrix_matrix_multiplies(TriCholc,TriCholinvc,TriCholcTriCholinvc);
   ** std::cout<<"TriCholcTriCholinvc  = \n"<<TriCholcTriCholinvc<<std::endl;
  
   ** \endcode
   */
template<typename Container2d1,
	 typename Container2d2>
void tridiagonal_cholesky_inv(const Container2d1& A,
				Container2d2& Ainv)
  {
    
    assert(A.dim1() == A.dim2());
    assert(Ainv.dim1() == Ainv.dim2());

    
    typedef typename Container2d1::value_type value_type;
    typedef typename Container2d1::size_type size_type;

    //get the 3 main diagonals
    size_type n = A.dim1();
    slip::Array<value_type> diag(n);
    slip::Array<value_type> up_diag(n-1);
  
    slip::get_diagonal(A,diag.begin(),diag.end());
    slip::get_diagonal(A,up_diag.begin(),up_diag.end(),1);
  
    slip::tridiagonal_cholesky_inv(diag.begin(),diag.end(),
				   up_diag.begin(),up_diag.end(),
				   Ainv.upper_left(),
				   Ainv.bottom_right());

  }

/* @} */

  /** \name LDLT  algorithms */
  /* @{ */

  /** \brief in place LU decomposition for symmetric matrix \f[A = LDL^T\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A a unit lower triangular matrix.
   ** \par Description:
   **  \li L is a lower triangular matrix
   **  \li L is stored in the lower triangular part of A
   **  \li D is stored in the diagonal of A
   ** \par Complexity: n^3/6 flops
   ** \note Works only on real matrices.
   */
  template<typename Container2d>
  void LDLT_decomposition(Container2d& A)
  {
    typedef typename Container2d::value_type value_type;

    const int n = int(A.dim1());
    slip::Array<value_type> R(n);
    value_type proj = value_type();
    for(int i = 0; i < n; ++i)
      {
	//computes R
	for(int j = 0; j < i; ++j)
	  {
	    R[j] = A[i][j] * A[j][j];
	  }
	
	proj = std::inner_product(A.row_begin(i),A.row_begin(i)+i,
				  R.begin(),
				  value_type());
	R[i] = A[i][i] - proj;
	//store D
	A[i][i] = R[i];
	//computes L
	for(int k = (i+1); k < n; ++k)
	  {
	    proj = std::inner_product(A.row_begin(k),A.row_begin(k)+i,
				      R.begin(),
				      value_type());
	    A[k][i] = (A[k][i] -  proj) / R[i];
	  }
      }
  
  }

 
/** \brief in place LU decomposition for symmetric matrix \f[A = LDL^T\f]
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param A The matrix to decompose.
 ** \param L The lower triangular matrix.
 ** \param D The diagonal matrix.
 ** \param LT The transposed matrix of L.
 ** \par Description:
 **  \li L is a lower triangular matrix
 ** \note Works only on real matrices.
 */
  template<typename Matrix1,
	   typename Matrix2,
	   typename Vector1,
	   typename Matrix3>
  void LDLT_decomposition(const Matrix1& A,
			  Matrix2& L,
			  Vector1& D,
			  Matrix3& LT)
  {
    typedef typename Matrix1::value_type value_type;
    //typedef typename Matrix1::size_type size_type;
    const int n = int(A.dim1());
    slip::Array<value_type> R(n);
    value_type proj = value_type();
    for(int i = 0; i < n; ++i)
      {
	//computes R
	for(int j = 0; j < i; ++j)
	  {
	    R[j] = L[i][j] * D[j];
	  }
	
	proj = std::inner_product(L.row_begin(i),L.row_begin(i)+i,
				  R.begin(),
				  value_type());
	R[i] = A[i][i] - proj;
	D[i] = R[i];
	L[i][i] = value_type(1);
	LT[i][i] = value_type(1);
	//store D
	//A[i][i] = R[i];
	//computes L
	for(int k = (i+1); k < n; ++k)
	  {
	    proj = std::inner_product(L.row_begin(k),L.row_begin(k)+i,
				      R.begin(),
				      value_type());
	    L[k][i] = (A[k][i] -  proj) / R[i];
	    LT[i][k] = L[k][i];
	  }
      }
  
  }

   /**
   ** \brief LDLT solve of system AX = B with A a square symmetric Matrix.
   **  \f$ A = LDL^T\f$ with L a lower triangular matrix
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/06
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A 2D container.
   ** \param X 1D container.
   ** \param B 1D container.
   ** \par Complexity: 
   */  
  template<typename Matrix1,
	   typename Vector1,
	   typename Vector2>
  void LDLT_solve(const Matrix1& A,
		  Vector1& X,
		  const Vector2& B)
  {
    typedef typename Matrix1::value_type value_type;
    typedef typename Matrix1::size_type size_type;
    const size_type n = A.dim1();
    //decomposition
    slip::Array2d<value_type> L(n,n);
    slip::Array<value_type> D(n);
    slip::Array2d<value_type> LT(n,n);
    slip::LDLT_decomposition(A,L,D,LT);
   
    //resolution of LY = B
    slip::Array<value_type> Y(n);
    slip::unit_lower_triangular_solve(L,Y,B);
    //resolution of DZ = Y
    slip::Array<value_type> Z(n);
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!REPLACE 1E-06 by precision
    slip::diagonal_solve(D.begin(),D.end(),
			 Z.begin(),Z.end(),
			 Y.begin(),Y.end(),1E-06);
    
    //resolution of LTX = Z
    slip::unit_upper_triangular_solve(LT,X,Z);
  }
/* @} */

  /** \name Square hermitian or symmetric matrix algorithms */
  /* @{ */
  /**
   ** \brief  cholesky decomposition of a square hermitian positive definite Matrix.
   **  \f$ A = LL^H\f$ with L a lower triangular matrix
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptor
   ** \date 2009/02/27
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param A_up 2D iterator corresponding to the upper_left element
   ** of the matrix A
   ** \param A_bot 2D iterator corresponding to one past-the-end bottom_right of the  matrix A
   ** \param L_up 2D iterator corresponding to the upper_left element
   ** of the matrix L
   ** \param L_bot 2D iterator corresponding to one past-the-end bottom_right of the  matrix L
   ** \param LH_up 2D iterator corresponding to the upper_left element
   ** of the matrix LH
   ** \param LH_bot 2D iterator corresponding to one past-the-end bottom_right of the  matrix LH
   ** \pre A must be square and positive definite
   ** \pre (A_bot - A_up)[0]   == (A_bot - A_up)[1];
   ** \pre (L_bot - L_up)[0]   == (L_bot - L_up)[1];
   ** \pre (LH_bot - LH_up)[0] == (LH_bot - LH_up)[1];
   ** \pre (A_bot - A_up)[0]   == (L_bot - L_up)[0];
   ** \pre (A_bot - A_up)[0]   == (LH_bot - LH_up)[0];
   ** \pre (L_bot - L_up)[0]   == (LH_bot - LH_up)[0];
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is not positive definite
   ** \par Complexity: n^3/6 flops + n std::sqrt 
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** std::complex<double> dcholc[] = {TC(2.0,0.0),TC(1.0,1.0),TC(0.0,2.0),
   **			   TC(1.0,-1.0),TC(5.0,0.0),TC(-3.0,0.0),
   **			   TC(0.0,-2.0),TC(-3.0,0.0),TC(10.0,0.0)};
   ** slip::Array2d<std::complex<double> > Cholc(3,3,dcholc);
   ** std::cout<<"Cholc = "<<std::endl;
   ** std::cout<<Cholc<<std::endl;
   ** slip::Array2d<std::complex<double> > LCholc(3,3);
   ** slip::Array2d<std::complex<double> > LCholTc(3,3);
   ** slip::cholesky_cplx(Cholc.upper_left(),Cholc.bottom_right(),
   **			     LCholc.upper_left(),LCholc.bottom_right(),
   **			     LCholTc.upper_left(),LCholTc.bottom_right());
   ** std::cout<<"LCholc = "<<std::endl;
   ** std::cout<<LCholc<<std::endl;
   ** std::cout<<"LChol^Tc = "<<std::endl;
   ** std::cout<<LCholTc<<std::endl;
   ** slip::Array2d<std::complex<double> > LCholxLCholTc(3,3);
   ** slip::matrix_matrix_multiplies(LCholc,LCholTc,LCholxLCholTc);
   ** std::cout<<"LCholxLChol^Tc  = "<<std::endl;
   ** std::cout<<LCholxLCholTc<<std::endl;
   ** \endcode
   */  
  template <typename MatrixIterator1,
	    typename MatrixIterator2,
	    typename MatrixIterator3>
  inline
  void cholesky_cplx(MatrixIterator1 A_up,
				   MatrixIterator1 A_bot,
				   MatrixIterator2 L_up,
				   MatrixIterator2 L_bot,
				   MatrixIterator3 LH_up,
				   MatrixIterator3 LH_bot)
  {
     assert((A_bot - A_up)[0]   == (A_bot - A_up)[1]);
    assert((L_bot - L_up)[0]   == (L_bot - L_up)[1]);
    assert((LH_bot - LH_up)[0] == (LH_bot - LH_up)[1]);
    assert((A_bot - A_up)[0]   == (L_bot - L_up)[0]);
    assert((A_bot - A_up)[0]   == (LH_bot - LH_up)[0]);
    assert((L_bot - L_up)[0]   == (LH_bot - LH_up)[0]);
   
	typedef typename MatrixIterator1::value_type value_type;
	typedef typename slip::lin_alg_traits<typename MatrixIterator1::value_type>::value_type real_type;
	const std::size_t A_rows =  std::size_t((A_bot - A_up)[0]);
     
	slip::DPoint2d<int> d(0,0);
	slip::DPoint2d<int> d2(0,0);
	slip::DPoint2d<int> d3(0,0);
	value_type proj = value_type();
	for(std::size_t i = 0; i < A_rows; ++i)
	  {
	    d.set_coord(i,i);
	    proj = slip::hermitian_inner_product(L_up.row_begin(i),
						 L_up.row_begin(i)+i,
						 L_up.row_begin(i),
						 value_type());
	    value_type Aup_m_proj = A_up[d] - proj;
	    if(std::real(Aup_m_proj) <= real_type())
	      {
		throw std::runtime_error(slip::POSITIVE_DEFINITE_MATRIX_ERROR);
	      }
	    value_type Lii = std::sqrt(Aup_m_proj);
	    L_up[d] = Lii;
	    LH_up[d] = Lii;
	    for(std::size_t j = (i+1); j < A_rows; ++j)
	      {
		proj = slip::hermitian_inner_product(L_up.row_begin(j),
						     L_up.row_begin(j)+i,
						     L_up.row_begin(i),
						     value_type());
		d2.set_coord(j,i);
		d3.set_coord(i,j);
		L_up[d2] = (A_up[d2] - slip::conj(proj))/Lii;
		L_up[d3] = value_type();
		LH_up[d3] = slip::conj(L_up[d2]);
		LH_up[d2] = value_type();
	      }
	  }
  
    
  }


  /**
   ** \brief  cholesky decomposition of a square hermitian positive definite Matrix.
   **  \f$ A = LL^H\f$ with L a lower triangular matrix.
   **  L is stored in the lower triangular part of A, \f$L^H\f$ is stored in the
   **  upper triangular part of A.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptor
   ** \date 2009/02/27
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param A_up 2D iterator corresponding to the upper_left element
   ** of the matrix A
   ** \param A_bot 2D iterator corresponding to one past-the-end bottom_right of the  matrix A
   ** \pre A must be square and positive definite
   ** \pre (A_bot - A_up)[0]   == (A_bot - A_up)[1];
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is not positive definite
   ** \par Complexity: n^3/6 flops + n std::sqrt 
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** std::complex<double> dcholc[] = {TC(2.0,0.0),TC(1.0,1.0),TC(0.0,2.0),
   **			   TC(1.0,-1.0),TC(5.0,0.0),TC(-3.0,0.0),
   **			   TC(0.0,-2.0),TC(-3.0,0.0),TC(10.0,0.0)};
   ** slip::Array2d<std::complex<double> > Cholc(3,3,dcholc);
   ** std::cout<<"Cholc = "<<std::endl;
   ** std::cout<<Cholc<<std::endl;
   ** slip::Array2d<std::complex<double> > LCholc(3,3);
   ** slip::Array2d<std::complex<double> > LCholTc(3,3);
   ** slip::cholesky_cplx(Cholc.upper_left(),Cholc.bottom_right()); 
   ** std::cout<<"Cholc = "<<std::endl;
   ** std::cout<<Cholc<<std::endl;
   ** \endcode
   */  
  template <typename MatrixIterator1>
  inline
  void cholesky_cplx(MatrixIterator1 A_up,
		     MatrixIterator1 A_bot)
			
  {
    assert((A_bot - A_up)[0]   == (A_bot - A_up)[1]);
 
	typedef typename MatrixIterator1::value_type value_type;
	typedef typename slip::lin_alg_traits<typename MatrixIterator1::value_type>::value_type real_type;
	const std::size_t A_rows =  std::size_t((A_bot - A_up)[0]);
     
	slip::DPoint2d<int> d(0,0);
	slip::DPoint2d<int> d2(0,0);
	slip::DPoint2d<int> d3(0,0);
	value_type proj = value_type();
	for(std::size_t i = 0; i < A_rows; ++i)
	  {
	    d.set_coord(i,i);
	    proj = slip::hermitian_inner_product(A_up.row_begin(i),
						 A_up.row_begin(i)+i,
						 A_up.row_begin(i),
						 value_type());
	    value_type Aup_m_proj = A_up[d] - proj;
	    if(std::real(Aup_m_proj) <= real_type())
	      {
		throw std::runtime_error(slip::POSITIVE_DEFINITE_MATRIX_ERROR);
	      }
	    value_type Aii = std::sqrt(Aup_m_proj);
	
	    A_up[d] = Aii;
	    for(std::size_t j = (i+1); j < A_rows; ++j)
	      {
		proj = slip::hermitian_inner_product(A_up.row_begin(j),
						     A_up.row_begin(j)+i,
						     A_up.row_begin(i),
						     value_type());
		d2.set_coord(j,i);
		d3.set_coord(i,j);
		A_up[d2] = (A_up[d2] - slip::conj(proj))/Aii;
		A_up[d3] = slip::conj(A_up[d2]);
	    
	      }
	  }
    
  }

  /**
   ** \brief  cholesky decomposition of a square real symmetric and positive definite Matrix.
   **  \f$ A = LL^T\f$ with L a lower triangular matrix.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptor
   ** \date 2009/02/27
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param A_up 2D iterator corresponding to the upper_left element
   ** of the matrix A
   ** \param A_bot 2D iterator corresponding to one past-the-end bottom_right of the  matrix A
   ** \param L_up 2D iterator corresponding to the upper_left element
   ** of the matrix L
   ** \param L_bot 2D iterator corresponding to one past-the-end bottom_right of the  matrix L
   ** \param LH_up 2D iterator corresponding to the upper_left element
   ** of the matrix LH
   ** \param LH_bot 2D iterator corresponding to one past-the-end bottom_right of the  matrix LH
   ** \pre A must be a square, real  and positive definite matrix
   ** \pre (A_bot - A_up)[0]   == (A_bot - A_up)[1];
   ** \pre (L_bot - L_up)[0]   == (L_bot - L_up)[1];
   ** \pre (LH_bot - LH_up)[0] == (LH_bot - LH_up)[1];
   ** \pre (A_bot - A_up)[0]   == (L_bot - L_up)[0];
   ** \pre (A_bot - A_up)[0]   == (LH_bot - LH_up)[0];
   ** \pre (L_bot - L_up)[0]   == (LH_bot - LH_up)[0];
   ** \remarks Will raise an exception if the matrix is not positive definite
   ** \par Complexity: n^3/6 flops + n std::sqrt 
   ** \par Example:
   ** \code
   ** slip::Array2d<float> Chol(4,4);
   ** slip::hilbert(Chol);
   ** std::cout<<"Chol = "<<std::endl;
   ** std::cout<<Chol<<std::endl;
   ** slip::Array2d<float> LChol(4,4);
   ** slip::Array2d<float> LCholT(4,4);
   ** slip::cholesky_real(Chol.upper_left(),Chol.bottom_right(),
   **			     LChol.upper_left(),LChol.bottom_right(),
   **			     LCholT.upper_left(),LCholT.bottom_right());
   ** std::cout<<"LChol = "<<std::endl;
   ** std::cout<<LChol<<std::endl;
   ** std::cout<<"LChol^T = "<<std::endl;
   ** std::cout<<LCholT<<std::endl;
   ** slip::Array2d<float> LCholxLCholT(4,4);
   ** slip::matrix_matrix_multiplies(LChol,LCholT,LCholxLCholT);
   ** std::cout<<"LCholxLChol^T  = "<<std::endl;
   ** std::cout<<LCholxLCholT<<std::endl;
   ** \endcode
   */  
   template <typename MatrixIterator1,
	    typename MatrixIterator2,
	    typename MatrixIterator3>
  inline
  void cholesky_real(MatrixIterator1 A_up,
				   MatrixIterator1 A_bot,
				   MatrixIterator2 L_up,
				   MatrixIterator2 L_bot,
				   MatrixIterator3 LH_up,
				   MatrixIterator3 LH_bot)
  {
    assert((A_bot - A_up)[0]   == (A_bot - A_up)[1]);
    assert((L_bot - L_up)[0]   == (L_bot - L_up)[1]);
    assert((LH_bot - LH_up)[0] == (LH_bot - LH_up)[1]);
    assert((A_bot - A_up)[0]   == (L_bot - L_up)[0]);
    assert((A_bot - A_up)[0]   == (LH_bot - LH_up)[0]);
    assert((L_bot - L_up)[0]   == (LH_bot - LH_up)[0]);
  
	typedef typename MatrixIterator1::value_type value_type;
	typedef typename slip::lin_alg_traits<typename MatrixIterator1::value_type>::value_type real_type;
	const std::size_t A_rows =  std::size_t((A_bot - A_up)[0]);
   
	slip::DPoint2d<int> d(0,0);
	slip::DPoint2d<int> d2(0,0);
	slip::DPoint2d<int> d3(0,0);
	value_type proj = value_type();
	for(std::size_t i = 0; i < A_rows; ++i)
	  {
	    d.set_coord(i,i);
	    proj = slip::inner_product(L_up.row_begin(i),
				       L_up.row_begin(i)+i,
				       L_up.row_begin(i),
				       value_type());
	    value_type Aup_m_proj = A_up[d] - proj;
	    if(Aup_m_proj <= real_type())
	      {
		throw std::runtime_error(slip::POSITIVE_DEFINITE_MATRIX_ERROR);
	      }
	    value_type Lii = std::sqrt(Aup_m_proj);

	    L_up[d] = Lii;
	    LH_up[d] = Lii;
	    for(std::size_t j = (i+1); j < A_rows; ++j)
	      {
		proj = slip::inner_product(L_up.row_begin(j),
					   L_up.row_begin(j)+i,
					   L_up.row_begin(i),
					   value_type());
		d2.set_coord(j,i);
		d3.set_coord(i,j);
		L_up[d2] = (A_up[d2] - proj)/Lii;
		L_up[d3] = value_type();
		LH_up[d3] = L_up[d2];
		LH_up[d2] = value_type();
	      }
	  }

  }
 /**
   ** \brief  cholesky decomposition of a square real symmetric and positive definite Matrix.
   **  \f$ A = LL^T\f$ with L a lower triangular matrix.
   ** L is stored in the lower triangular part of A, \f$L^T\f$ is stored in the
   ** upper triangular part of A.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptor
   ** \date 2009/02/27
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param A_up 2D iterator corresponding to the upper_left element
   ** of the matrix A
   ** \param A_bot 2D iterator corresponding to one past-the-end bottom_right of the  matrix A
   ** \pre A must be a square, real  and positive definite matrix
   ** \pre (A_bot - A_up)[0]   == (A_bot - A_up)[1];
   ** \remarks Will raise an exception if the matrix is not positive definite
   ** \par Complexity: n^3/6 flops + n std::sqrt 
   ** \par Example:
   ** \code
   ** slip::Array2d<float> Chol(4,4);
   ** slip::hilbert(Chol);
   ** std::cout<<"Chol = "<<std::endl;
   ** std::cout<<Chol<<std::endl;
   ** slip::Array2d<float> LChol(4,4);
   ** slip::Array2d<float> LCholT(4,4);
   ** slip::cholesky_real(Chol.upper_left(),Chol.bottom_right());
   ** std::cout<<"Chol = "<<std::endl;
   ** std::cout<<Chol<<std::endl;
   ** \endcode
   */  
   template <typename MatrixIterator1>
  inline
  void cholesky_real(MatrixIterator1 A_up,
		     MatrixIterator1 A_bot)
  {
    assert((A_bot - A_up)[0]   == (A_bot - A_up)[1]);
   
	typedef typename MatrixIterator1::value_type value_type;
	typedef typename slip::lin_alg_traits<typename MatrixIterator1::value_type>::value_type real_type;
	const std::size_t A_rows =  std::size_t((A_bot - A_up)[0]);

	//init L and LH
	slip::DPoint2d<int> d(0,0);
	slip::DPoint2d<int> d2(0,0);
	slip::DPoint2d<int> d3(0,0);
	value_type proj = value_type();
	for(std::size_t i = 0; i < A_rows; ++i)
	  {
	    d.set_coord(i,i);
	    proj = slip::inner_product(A_up.row_begin(i),
				       A_up.row_begin(i)+i,
				       A_up.row_begin(i),
				       value_type());
	    value_type Aup_m_proj = A_up[d] - proj;
	    if(std::real(Aup_m_proj) <= real_type(0))
	      {
		throw std::runtime_error(slip::POSITIVE_DEFINITE_MATRIX_ERROR);
	      }
	    value_type Aii = std::sqrt(Aup_m_proj);

	    A_up[d] = Aii;
	    for(std::size_t j = (i+1); j < A_rows; ++j)
	      {
		proj = slip::inner_product(A_up.row_begin(j),
					   A_up.row_begin(j)+i,
					   A_up.row_begin(i),
					   value_type());
		d2.set_coord(j,i);
		d3.set_coord(i,j);
		A_up[d2] = (A_up[d2] - proj)/Aii;
		A_up[d3] = A_up[d2];
	      }
	  }
     
    
  }

  
  /**
   ** \brief  cholesky decomposition of a square real symmetric positive definite Matrix.
   **  \f$ A = LL^T\f$ with L a lower triangular matrix
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptor
   ** \date 2009/02/27
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param A 2D container.
   ** \param L 2D container.
   ** \param LT 2D container.
   ** \pre A must be square,real and positive definite.
   ** \pre A.rows() == A.cols()
   ** \pre L.rows() == L.cols()
   ** \pre LH.rows() == LH.cols()
   ** \pre A.rows() == L.rows()
   ** \pre A.rows() == LH.rows()
   ** \remarks Will raise an exception if the matrix is not positive definite
   ** \par Complexity: n^3/6 flops + n std::sqrt 
   ** \par Example:
   ** \code
   ** slip::Array2d<float> Chol(4,4);
   ** slip::hilbert(Chol);
   ** std::cout<<"Chol = "<<std::endl;
   ** std::cout<<Chol<<std::endl;
   ** slip::Array2d<float> LChol(4,4);
   ** slip::Array2d<float> LCholT(4,4);
   ** slip::cholesky_real(Chol,LChol,LCholT);
   ** std::cout<<"LChol = "<<std::endl;
   ** std::cout<<LChol<<std::endl;
   ** std::cout<<"LChol^T = "<<std::endl;
   ** std::cout<<LCholT<<std::endl;
   ** slip::Array2d<float> LCholxLCholT(4,4);
   ** slip::matrix_matrix_multiplies(LChol,LCholT,LCholxLCholT);
   ** std::cout<<"LCholxLChol^T  = "<<std::endl;
   ** std::cout<<LCholxLCholT<<std::endl;
   ** \endcode
   */  
  template <class Matrix1,
	    class Matrix2,
	    class Matrix3>
  inline
  void cholesky_real(const Matrix1 & A, 
				   Matrix2 & L,
				   Matrix3& LT)
  {
    slip::cholesky_real(A.upper_left(),A.bottom_right(),
				      L.upper_left(),L.bottom_right(),
				      LT.upper_left(),LT.bottom_right());
  }

   
   /**
   ** \brief  cholesky decomposition of a square hermitian positive definite Matrix.
   **  \f$ A = LL^H\f$ with L a lower triangular matrix
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptor
   ** \date 2009/02/27
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param A 2D container.
   ** \param L 2D container.
   ** \param LT 2D container.
   ** \pre A must be square and positive definite
   ** \pre A.rows() == A.cols()
   ** \pre L.rows() == L.cols()
   ** \pre LH.rows() == LH.cols()
   ** \pre A.rows() == L.rows()
   ** \pre A.rows() == LH.rows()
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is not positive definite
   ** \par Complexity: n^3/6 flops + n std::sqrt 
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** std::complex<double> dcholc[] = {TC(2.0,0.0),TC(1.0,1.0),TC(0.0,2.0),
   **			   TC(1.0,-1.0),TC(5.0,0.0),TC(-3.0,0.0),
   **			   TC(0.0,-2.0),TC(-3.0,0.0),TC(10.0,0.0)};
   ** slip::Array2d<std::complex<double> > Cholc(3,3,dcholc);
   ** std::cout<<"Cholc = "<<std::endl;
   ** std::cout<<Cholc<<std::endl;
   ** slip::Array2d<std::complex<double> > LCholc(3,3);
   ** slip::Array2d<std::complex<double> > LCholTc(3,3);
   ** slip::cholesky_cplx(Cholc,LCholc,LCholTc);
   ** std::cout<<"LCholc = "<<std::endl;
   ** std::cout<<LCholc<<std::endl;
   ** std::cout<<"LChol^Tc = "<<std::endl;
   ** std::cout<<LCholTc<<std::endl;
   ** slip::Array2d<std::complex<double> > LCholxLCholTc(3,3);
   ** slip::matrix_matrix_multiplies(LCholc,LCholTc,LCholxLCholTc);
   ** std::cout<<"LCholxLChol^Tc  = "<<std::endl;
   ** std::cout<<LCholxLCholTc<<std::endl;
   ** \endcode
   */  
  template <class Matrix1,
	    class Matrix2,
	    class Matrix3>
  inline
  void cholesky_cplx(const Matrix1 & A, 
			      Matrix2 & L,
			      Matrix3& LT)
  {
    slip::cholesky_cplx(A.upper_left(),A.bottom_right(),
					  L.upper_left(),L.bottom_right(),
					  LT.upper_left(),LT.bottom_right());
  }


   /**
   ** \brief  cholesky decomposition of a square hermitian positive definite Matrix.
   **  \f$ A = LL^H\f$ with L a lower triangular matrix
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptor
   ** \date 2009/02/27
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param A 2D container.
   ** \param L 2D container.
   ** \param LT 2D container.
   ** \pre A must be square and positive definite
   ** \pre A.rows() == A.cols()
   ** \pre L.rows() == L.cols()
   ** \pre LH.rows() == LH.cols()
   ** \pre A.rows() == L.rows()
   ** \pre A.rows() == LH.rows()
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is not positive definite
   ** \par Complexity: n^3/6 flops + n std::sqrt 
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** std::complex<double> dcholc[] = {TC(2.0,0.0),TC(1.0,1.0),TC(0.0,2.0),
   **			   TC(1.0,-1.0),TC(5.0,0.0),TC(-3.0,0.0),
   **			   TC(0.0,-2.0),TC(-3.0,0.0),TC(10.0,0.0)};
   ** slip::Array2d<std::complex<double> > Cholc(3,3,dcholc);
   ** std::cout<<"Cholc = "<<std::endl;
   ** std::cout<<Cholc<<std::endl;
   ** slip::Array2d<std::complex<double> > LCholc(3,3);
   ** slip::Array2d<std::complex<double> > LCholTc(3,3);
   ** slip::cholesky(Cholc,LCholc,LCholTc);
   ** std::cout<<"LCholc = "<<std::endl;
   ** std::cout<<LCholc<<std::endl;
   ** std::cout<<"LChol^Tc = "<<std::endl;
   ** std::cout<<LCholTc<<std::endl;
   ** slip::Array2d<std::complex<double> > LCholxLCholTc(3,3);
   ** slip::matrix_matrix_multiplies(LCholc,LCholTc,LCholxLCholTc);
   ** std::cout<<"LCholxLChol^Tc  = "<<std::endl;
   ** std::cout<<LCholxLCholTc<<std::endl;
   ** \endcode
   */  
  template <class Matrix1,
	    class Matrix2,
	    class Matrix3>
  inline
  void cholesky(const Matrix1 & A, 
			      Matrix2 & L,
			      Matrix3& LT)
  {
    if(slip::is_complex(typename Matrix1::value_type()))
      {
	slip::cholesky_cplx(A.upper_left(),A.bottom_right(),
					  L.upper_left(),L.bottom_right(),
					  LT.upper_left(),LT.bottom_right());
      }
    else
      {
	slip::cholesky_real(A.upper_left(),A.bottom_right(),
					  L.upper_left(),L.bottom_right(),
					  LT.upper_left(),LT.bottom_right());
      }
  }
 
   /**
   ** \brief  cholesky decomposition of a square hermitian positive definite Matrix.
   **  \f$ A = LL^H\f$ with L a lower triangular matrix.
   **  L is stored in the lower triangular part of A, \f$L^H\f$ is stored in the
   **  upper triangular part of A.
 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptor
   ** \date 2009/02/27
    ** \since 1.0.0
   ** \version 0.0.2
   ** \param A 2D container.
   ** \pre A must be square and positive definite
   ** \pre A.rows() == A.cols()
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is not positive definite
   ** \par Complexity: n^3/6 flops + n std::sqrt 
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   ** std::complex<double> dcholc[] = {TC(2.0,0.0),TC(1.0,1.0),TC(0.0,2.0),
   **			   TC(1.0,-1.0),TC(5.0,0.0),TC(-3.0,0.0),
   **			   TC(0.0,-2.0),TC(-3.0,0.0),TC(10.0,0.0)};
   ** slip::Array2d<std::complex<double> > Cholc(3,3,dcholc);
   ** std::cout<<"Cholc = "<<std::endl;
   ** std::cout<<Cholc<<std::endl;
   ** slip::Array2d<std::complex<double> > LCholc(3,3);
   ** slip::Array2d<std::complex<double> > LCholTc(3,3);
   ** slip::cholesky(Cholc,LCholc,LCholTc);
   ** std::cout<<"LCholc = "<<std::endl;
   ** std::cout<<LCholc<<std::endl;
   ** std::cout<<"LChol^Tc = "<<std::endl;
   ** std::cout<<LCholTc<<std::endl;
   ** slip::Array2d<std::complex<double> > LCholxLCholTc(3,3);
   ** slip::matrix_matrix_multiplies(LCholc,LCholTc,LCholxLCholTc);
   ** std::cout<<"LCholxLChol^Tc  = "<<std::endl;
   ** std::cout<<LCholxLCholTc<<std::endl;
   ** \endcode
   */  
  template <class Matrix1>
  inline
  void cholesky(Matrix1 & A)

  {
    if(slip::is_complex(typename Matrix1::value_type()))
      {
	slip::cholesky_cplx(A.upper_left(),A.bottom_right());
      }
    else
      {
	slip::cholesky_real(A.upper_left(),A.bottom_right());
      }
  }

  /**
   ** \brief cholesky solve of system AX = B with A a square hermitian positive definite Matrix.
   
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/02/27
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param A_up 2D iterator corresponding to the upper_left element
   ** of the matrix A
   ** \param A_bot 2D iterator corresponding to one past-the-end bottom_right of the  matrix A
   ** \param X_first RandomAccessIterator (points to the first element of X)
   ** \param X_last RandomAccessIterator (points to the one past the end element of X)
   ** \param B_first RandomAccessIterator (points to the first element of B)
   ** \param B_last RandomAccessIterator (points to the one past the end element of B)
   ** \param precision 1.0E-6 by default.
   ** \pre A must be a square, real  and positive definite matrix
   ** \pre (A_bot - A_up)[0]   == (A_bot - A_up)[1];
   ** \pre (A_bot - A_up)[1]   == (X_last - X_first);
   ** \pre (X_last - X_first)  == (B_last - B_first);
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is not positive definite
   ** \par Complexity: n^3/6 flops + n std::sqrt + 2 n^2 flops
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   **  std::complex<double> dcholc[] = {TC(2.0,0.0),TC(1.0,1.0),TC(0.0,2.0),
   **			   TC(1.0,-1.0),TC(5.0,0.0),TC(-3.0,0.0),
   **			   TC(0.0,-2.0),TC(-3.0,0.0),TC(10.0,0.0)};
   ** slip::Array2d<std::complex<double> > Cholc(3,3,dcholc);
   ** std::cout<<"Cholc = "<<std::endl;
   ** std::cout<<Cholc<<std::endl;
   ** std::complex<double> bcholc[] = {TC(1.0,0.2),TC(2.0,0.5),TC(3.0,0.5)};
   ** slip::Array<std::complex<double> > BCholc(3,bcholc);
   ** std::cout<<"BCholc = "<<std::endl;
   ** std::cout<<BCholc<<std::endl;
   ** slip::Array<std::complex<double> > XCholc(3);
   ** slip::cholesky_solve(Cholc.upper_left(),Cholc.bottom_right(),
   **			   XCholc.begin(),XCholc.end(),
   **		           BCholc.begin(),BCholc.end());
   ** std::cout<<"XCholc = "<<std::endl;
   ** std::cout<<XCholc<<std::endl;
   ** slip::Array<std::complex<double> > BChol2c(3);
   ** slip::matrix_vector_multiplies(Cholc,XCholc,BChol2c);
   ** std::cout<<"Cholc XCholc = "<<std::endl;
   ** std::cout<<BChol2c<<std::endl;
   ** \endcode
   */
  template <typename MatrixIterator,
	     typename RandomAccessIterator1,
	     typename RandomAccessIterator2>
  inline
  void cholesky_solve(MatrixIterator A_up,
		      MatrixIterator A_bot,
		      RandomAccessIterator1 X_first,
		      RandomAccessIterator1 X_last,
		      RandomAccessIterator2 B_first,
		      RandomAccessIterator2 B_last,
		      typename slip::lin_alg_traits<typename MatrixIterator::value_type>::value_type precision = typename slip::lin_alg_traits<typename MatrixIterator::value_type>::value_type(1.0E-6))
  {
    assert((A_bot - A_up)[0]   == (A_bot - A_up)[1]);
    assert((A_bot - A_up)[1]   == (X_last - X_first));
    assert((X_last - X_first)  == (B_last - B_first));
 
    typedef typename std::iterator_traits<MatrixIterator>::value_type value_type;
    typedef typename MatrixIterator::size_type size_type;
    const size_type n = size_type((A_bot - A_up)[0]);
   
    //decomposition
    slip::Array2d<value_type> LLT(n,n);
    std::copy(A_up,A_bot,LLT.begin());
    slip::cholesky(LLT);
    slip::Array<value_type> Y(n);
    //resolution of LY = B
    slip::lower_triangular_solve(LLT.upper_left(),LLT.bottom_right(),
				 Y.begin(),Y.end(),
				 B_first,B_last,precision);
    //resolution of LTX = Y
    slip::upper_triangular_solve(LLT.upper_left(),LLT.bottom_right(),
				 X_first,X_last,
				 Y.begin(),Y.end(),precision);
    
  }

  
  /**
   ** \brief cholesky solve of system AX = B with A a square hermitian positive definite Matrix.
   
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/02/27
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param A 2D container.
   ** \param X 1D container.
   ** \param B 1D container.
   ** \param precision 1.0E-6 by default.
   ** \pre A must be a square, real  and positive definite matrix
   ** \pre A.rows() == A.cols();
   ** \pre A.cols() == X.size()
   ** \pre X.size() == B.size()
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is not positive definite
   ** \par Complexity: n^3/6 flops + n std::sqrt + 2 n^2 flops
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   **  std::complex<double> dcholc[] = {TC(2.0,0.0),TC(1.0,1.0),TC(0.0,2.0),
   **			   TC(1.0,-1.0),TC(5.0,0.0),TC(-3.0,0.0),
   **			   TC(0.0,-2.0),TC(-3.0,0.0),TC(10.0,0.0)};
   ** slip::Array2d<std::complex<double> > Cholc(3,3,dcholc);
   ** std::cout<<"Cholc = "<<std::endl;
   ** std::cout<<Cholc<<std::endl;
   ** std::complex<double> bcholc[] = {TC(1.0,0.2),TC(2.0,0.5),TC(3.0,0.5)};
   ** slip::Array<std::complex<double> > BCholc(3,bcholc);
   ** std::cout<<"BCholc = "<<std::endl;
   ** std::cout<<BCholc<<std::endl;
   ** slip::Array<std::complex<double> > XCholc(3);
   ** slip::cholesky_solve(Cholc,XCholc,BCholc);
   ** std::cout<<"XCholc = "<<std::endl;
   ** std::cout<<XCholc<<std::endl;
   ** slip::Array<std::complex<double> > BChol2c(3);
   ** slip::matrix_vector_multiplies(Cholc,XCholc,BChol2c);
   ** std::cout<<"Cholc XCholc = "<<std::endl;
   ** std::cout<<BChol2c<<std::endl;
   ** \endcode
   */
  template <class Matrix1,
	    class Vector1,
	    class Vector2>
  inline
  void cholesky_solve(const Matrix1 & A, 
		      Vector1 & X,
		      const Vector2& B,
		      typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type precision =  
		      typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type(1.0E-6))
  {
    slip::cholesky_solve(A.upper_left(),A.bottom_right(),
			 X.begin(),X.end(),
			 B.begin(),B.end(),
			 precision);
    
  }


 /**
   ** \brief cholesky inverse of a square hermitian positive definite Matrix A.
   
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A_up 2D iterator corresponding to the upper_left element
   ** of the matrix A
   ** \param A_bot 2D iterator corresponding to one past-the-end bottom_right of the  matrix A
   ** \param Ainv_up 2D iterator corresponding to the upper_left element
   ** of the inverse matrix.
   ** \param Ainv_bot 2D iterator corresponding to one past-the-end bottom_right of the  inverse matrix.
   ** \param precision 1.0E-6 by default.
   ** \pre A must be a square, real  and positive definite matrix
   ** \pre (A_bot - A_up)[0]   == (A_bot - A_up)[1];
   ** \pre (Ainv_bot - Ainv_up)[0]   == (Ainv_bot - Ainv_up)[1];
    
   ** \pre (A_bot - A_up)[0] == (Ainv_bot - Ainv_up)[0]
   ** \pre (A_bot - A_up)[1] == (Ainv_bot - Ainv_up)[1]
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is not positive definite
   ** \par Complexity: (13/6)n^3 flops + n std::sqrt 
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   **  std::complex<double> dcholc[] = {TC(2.0,0.0),TC(1.0,1.0),TC(0.0,2.0),
   **			   TC(1.0,-1.0),TC(5.0,0.0),TC(-3.0,0.0),
   **			   TC(0.0,-2.0),TC(-3.0,0.0),TC(10.0,0.0)};
   ** slip::Array2d<std::complex<double> > Cholc(3,3,dcholc);
   ** std::cout<<"Cholc = "<<std::endl;
   ** std::cout<<Cholc<<std::endl;
   ** slip::Array2d<std::complex<double> > invCholc(3,3);
   ** slip::cholesky_inv(Cholc.upper_left(),Cholc.bottom_right(),
   **			 invCholc.upper_left(),invCholc.bottom_right());
   ** std::cout<<"invCholc = "<<std::endl;
   ** std::cout<<invCholc<<std::endl;
   ** slip::Array2d<std::complex<double> > ICholc(3,3);
   ** slip::matrix_matrix_multiplies(Cholc,invCholc,ICholc);
   ** std::cout<<"Cholc invCholc = "<<std::endl;
   ** std::cout<<IChol2<<std::endl;
   ** \endcode
   */
  template <typename MatrixIterator1,
	    typename MatrixIterator2>
  inline
  void cholesky_inv(MatrixIterator1 A_up,
		    MatrixIterator1 A_bot,
		    MatrixIterator2 Ainv_up,
		    MatrixIterator2 Ainv_bot,
		    typename slip::lin_alg_traits<typename MatrixIterator1::value_type>::value_type precision = typename slip::lin_alg_traits<typename MatrixIterator1::value_type>::value_type(1.0E-6))
  {
    assert((A_bot - A_up)[0]   == (A_bot - A_up)[1]);
    assert((Ainv_bot - Ainv_up)[0]   == (Ainv_bot - Ainv_up)[1]);
    assert((A_bot - A_up)[0] == (Ainv_bot - Ainv_up)[0]);
    assert((A_bot - A_up)[1] == (Ainv_bot - Ainv_up)[1]);

   
	typedef typename MatrixIterator1::value_type value_type;
	const std::size_t n = std::size_t((A_bot - A_up)[0]);
	//init Ainv with the identity matrix
	slip::identity(Ainv_up,Ainv_bot);
	
	slip::Array2d<value_type> LLT(n,n);
	std::copy(A_up,A_bot,LLT.upper_left());
	//cholesky decomposition
	if(slip::is_complex(value_type()))
	  {
	    slip::cholesky_cplx(LLT.upper_left(),LLT.bottom_right());
	  }
	else
	  {
	    slip::cholesky_real(LLT.upper_left(),LLT.bottom_right());
	  }
	
	//solve the n systems 
	slip::Array<value_type> Y(n);
	for(std::size_t k = 0; k < n; ++k)
	  {
	//resolution of LY = B
	    slip::lower_triangular_solve(LLT.upper_left(),LLT.bottom_right(),
					 Y.begin(),Y.end(),
					 Ainv_up.col_begin(k),Ainv_up.col_end(k),
					 precision);
	    //resolution of LTX = Y
	    slip::upper_triangular_solve(LLT.upper_left(),LLT.bottom_right(),
					 Ainv_up.col_begin(k),Ainv_up.col_end(k),
					 Y.begin(),Y.end(),precision);
	  }
   
  }

/**
   ** \brief cholesky inverse of a square hermitian positive definite Matrix A.
   
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/08
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A The matrix to invert.
   ** \param Ainv The inverted matrix.
   ** \param precision 1.0E-6 by default.
   ** \pre A must be a square, real  and positive definite matrix
   ** \pre A.rows()   ==  A.cols()
   ** \pre Ainv.rows() == Ainv.cols()
   ** \pre A.rows() == Ainv.rows()
   ** \pre (A_bot - A_up)[0] == (Ainv_bot - Ainv_up)[0]
   ** \pre (A_bot - A_up)[1] == (Ainv_bot - Ainv_up)[1]
   ** \remarks Works with real and complex data.
   ** \remarks Will raise an exception if the matrix is not positive definite
   ** \par Complexity: (13/6)n^3 flops + n std::sqrt 
   ** \par Example:
   ** \code
   ** typedef std::complex<double> TC;
   **  std::complex<double> dcholc[] = {TC(2.0,0.0),TC(1.0,1.0),TC(0.0,2.0),
   **			   TC(1.0,-1.0),TC(5.0,0.0),TC(-3.0,0.0),
   **			   TC(0.0,-2.0),TC(-3.0,0.0),TC(10.0,0.0)};
   ** slip::Array2d<std::complex<double> > Cholc(3,3,dcholc);
   ** std::cout<<"Cholc = "<<std::endl;
   ** std::cout<<Cholc<<std::endl;
   ** slip::Array2d<std::complex<double> > invCholc(3,3);
   ** slip::cholesky_inv(Cholc,invCholc,1.0e-8);
   ** std::cout<<"invCholc = "<<std::endl;
   ** std::cout<<invCholc<<std::endl;
   ** slip::Array2d<std::complex<double> > ICholc(3,3);
   ** slip::matrix_matrix_multiplies(Cholc,invCholc,ICholc);
   ** std::cout<<"Cholc invCholc = "<<std::endl;
   ** std::cout<<IChol2<<std::endl;
   ** \endcode
   */
template <typename Matrix1,
	  typename Matrix2>
  inline
  void cholesky_inv(Matrix1 A,
		    Matrix2 Ainv,
		    typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type precision = typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type(1.0E-6))
  {
    slip::cholesky_inv(A.upper_left(),A.bottom_right(),
		       Ainv.upper_left(),Ainv.bottom_right(),
		       precision);
  }
 /* @} */


/** \name Givens rotations algorithms */
/* @{ */
/**
   ** \brief Computes the Givens sinus and cosinus.
   ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/13
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param a value  
   ** \param b value 
   ** \param sin Givens sinus
   ** \param cos Givens cosinus.
   **
   */
template <typename Real >
  inline
  void rotgen(Real& a,
	      Real& b,
	      Real& cos, 
	      Real& sin)
  {
    if(b == Real(0.0))
      {
        cos = Real(1.0);
        sin = Real(0.0);
        return;
      }
    if(a == Real(0.0))
      {
        cos = Real(0.0);
        sin = Real(1.0);
        a = b;
        b = Real(0.0);
        return;
      }
    Real mu = a/(std::abs(a));
    Real tau = std::abs(a)+std::abs(b);
    Real nu = tau*(std::sqrt(std::abs(a/tau)*std::abs(a/tau)+std::abs(b/tau)*std::abs(b/tau)));
    cos = std::abs(a)/nu;
    sin = mu * ((slip::conj(b))/nu);
    a = nu * mu;
    b = Real(0.0);
  }

/**
   ** \brief Computes the Givens sinus and cosinus.
   **  \f[
   R = \left(
   \begin{array}{cc}
   \cos \theta & -\sin \theta \\
   \sin \theta & \cos \theta\\
   \end{array}\right)
   \f]
   \f[
   \cos \theta = \frac{xi}{\sqrt{xi^2+xk^2}}
   \f]
   \f[
    \sin \theta = \frac{-xk}{\sqrt{xi^2+xk^2}}
    \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/13
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param xi value at index i. 
   ** \param xk value at index k.
   ** \param sin Givens sinus
   ** \param cos Givens cosinus.
   **
   ** \par Complexity: 5 flops + 1 std::sqrt 
   */
  template <typename Real>
  inline
  void givens_sin_cos(const Real& xi, const Real& xk,
		      Real& sin, Real& cos)
  {
    Real n = std::sqrt(xi*xi + xk * xk);
    cos = xi /n;
    sin = -xk/n;
  }


 /**
   ** \brief Computes the complex Givens sinus and cosinus.
   **  \f[
   R = \left(
   \begin{array}{cc}
   \cos \theta & -\overline{\sin \theta} \\
   \sin \theta & \cos \theta\\
   \end{array}\right)
   \f]
   \f[
   \cos \theta = \frac{|xi|}{\epsilon\sqrt{xi\overline{xi}+xk\overline{xk}}}
   \f]
   \f[
    \sin \theta = \frac{-xk}{\epsilon sign(xi)\sqrt{xi\overline{xi}+xk\overline{xk}}}
    \f]
    with 
    \f[
    \epsilon = sign{\mathcal{R}(xi)}
    \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/10/13
   ** \version 0.0.1
   ** \param xi value at index i. 
   ** \param xk value at index k.
   ** \param sin Givens sinus
   ** \param cos Givens cosinus.
   **
   ** 
   ** \par Complexity: 10 flops + 1 std::sqrt
   */
  template <typename Complex>
  inline
  void complex_givens_sin_cos(const Complex& xi, const Complex& xk,
			      Complex& sin, typename Complex::value_type& cos)
  {
    typedef typename Complex::value_type Real;
    Real epsilon = slip::sign(xi.real());
    Real n = std::sqrt(std::norm(xi) + std::norm(xk));
    cos = std::abs(xi) / (epsilon * n);
    sin = - xk / (epsilon * (slip::complex_sign<Complex>()(xi) * n));
  }


  
   /**
   **  \brief Apply left Givens rotation multiplication.
   ** 
   **  Perform a left multiplication of a matrix M with the transpose Givens matrix 
   **  on the row1 and row2 between the indices col1 and col2.
   **  \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptator
   **  \date 2008/10/14
   ** \since 1.0.0
   **  \version 0.0.1
   **
   **  \param M the Container to multiply
   **  \param row1 the first row involved in the computation.
   **  \param row2 the second row involved in the computation.
   **  \param sinus Givens sinus.
   **  \param cosinus Givens cosinus.
   **  \param col1 the first column index.
   **  \param col2 the second column index.
   **  \pre row1 and row2 must be within the Matrix indices range.
   **  \pre col1 and col2 must be within the Matrix indices range.
   **  \pre  row1 should be less than row2.
   **  \pre  col1 should be less than col2.
   ** \par Complexity: 6 (col2-col1 + 1) flops
   **/
  template <typename Matrix, typename SizeType, typename Real>
  inline
  void left_givens(Matrix & M, 
		   const SizeType& row1, 
		   const SizeType& row2, 
		   const Real& sinus,
		   const Real& cosinus, 
		   const SizeType& col1,
		   const SizeType& col2)
  {
    typedef typename Matrix::value_type value_type;
    for(SizeType j = col1; j <= col2; ++j)
      {
	value_type t1 = M(row1,j);
	value_type t2 = M(row2,j);
	M(row1,j) = cosinus * t1 + sinus * t2;
	M(row2,j) = - sinus * t1 + cosinus * t2;
      }
  }

   /**
   **  \brief  Apply right Givens rotation multiplication.
   ** 
   **  Perform a right multiplication of a matrix M with the Givens matrix 
   **  on the col1 and col2 between the indices row1 and row2.
   **  \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \since 1.0.0
   **  \date 2008/10/14
   **  \version 0.0.1
   **
   **  \param M The Container to multiply
   **  \param col1 The first column involved in the computation.
   **  \param col2 The second column involved in the computation.
   **  \param sinus Givens sinus.
   **  \param cosinus Givens cosinus.
   **  \param row1 the first row index.
   **  \param row2 the second row index.
   **  \pre row1 and row2 must be within the Matrix indices range.
   **  \pre col1 and col2 must be within the Matrix indices range.
   **  \pre  row1 should be less than row2.
   **  \pre  col1 should be less than col2.
   **  \par Complexity: 6 (row2-row1 + 1) flops
   **/
  template <typename Matrix, typename SizeType, typename Real>
  inline
  void right_givens(Matrix & M, 
		    const SizeType& col1, 
		    const SizeType& col2, 
		    const Real& sinus,
		    const Real& cosinus, 
		    const SizeType& row1,
		    const SizeType& row2)
  {
    typedef typename Matrix::value_type value_type;
    for(SizeType i = row1; i <= row2; ++i)
      {
	value_type t1 = M(i,col1);
	value_type t2 = M(i,col2);
	M(i,col1) = cosinus * t1 + sinus * t2;
	M(i,col2) = - sinus * t1 + cosinus * t2;
      }
  }


   /**
   **  \brief Apply complex left Givens rotation multiplication.
   ** 
   **  Perform a left multiplication of a matrix M with the hermitian transpose Givens matrix 
   **  on the row1 and row2 between the indices col1 and col2.
   **  \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> : adaptator
   **  \date 2008/10/14
   ** \since 1.0.0
   **  \version 0.0.1
   **
   **  \param M the Container to multiply
   **  \param row1 the first row involved in the computation.
   **  \param row2 the second row involved in the computation.
   **  \param sinus Givens sinus.
   **  \param cosinus Givens cosinus.
   **  \param col1 the first column index.
   **  \param col2 the second column index.
   **  \pre row1 and row2 must be within the Matrix indices range.
   **  \pre col1 and col2 must be within the Matrix indices range.
   **  \pre  row1 should be less than row2.
   **  \pre  col1 should be less than col2.
   ** \par Complexity: 7 (col2-col1 + 1) flops
   **/
  template <typename Matrix, typename SizeType, typename Complex>
  inline
  void complex_left_givens(Matrix & M, 
		   const SizeType& row1, 
		   const SizeType& row2, 
		   const Complex& sinus,
		   const typename Complex::value_type& cosinus, 
		   const SizeType& col1,
		   const SizeType& col2)
  {
    for(SizeType j = col1; j <= col2; ++j)
      {
	Complex t1 = M(row1,j);
	Complex t2 = M(row2,j);
	M(row1,j) = cosinus * t1 + slip::conj(sinus) * t2;
	M(row2,j) = -sinus * t1  + cosinus * t2;
      }
  }


   /**
   **  \brief  Apply complex right Givens rotation multiplication.
   ** 
   **  Perform a right multiplication of a matrix M with the Givens matrix 
   **  on the col1 and col2 between the indices row1 and row2.
   **  \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   **  \date 2008/10/14
   ** \since 1.0.0
   **  \version 0.0.1
   **
   **  \param M The Container to multiply
   **  \param col1 The first column involved in the computation.
   **  \param col2 The second column involved in the computation.
   **  \param sinus Givens sinus.
   **  \param cosinus Givens cosinus.
   **  \param row1 the first row index.
   **  \param row2 the second row index.
   **  \pre row1 and row2 must be within the Matrix indices range.
   **  \pre col1 and col2 must be within the Matrix indices range.
   **  \pre  row1 should be less than row2.
   **  \pre  col1 should be less than col2.
   **  \par Complexity: 7 (row2-row1 + 1) flops
   **/
  template <typename Matrix, typename SizeType, typename Complex>
  inline
  void complex_right_givens(Matrix & M, 
			    const SizeType& col1, 
			    const SizeType& col2, 
			    const Complex& sinus,
			    const typename Complex::value_type& cosinus, 
			    const SizeType& row1,
			    const SizeType& row2)
  {
    for(SizeType i = row1; i <= row2; ++i)
      {
	Complex t1 = M(i,col1);
	Complex t2 = M(i,col2);
	M(i,col1) = cosinus * t1 + sinus * t2;
	M(i,col2) = -slip::conj(sinus) * t1 + cosinus * t2;
      }
  }


/** \name Householder algorithms */
/* @{ */

/** 
 **  \brief Compute the Householder vector u of a vector a.
 ** 
 **  Given a vector a, compute the vector u of the matrix of Householder
 **  \f$ H=I-2uu^H \f$ such that \f$ (I -u*u^H)a=\nu * e1 \f$. 
 **  \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr> : 
 **  \date 2009/01/21
 ** \since 1.0.0
 **  \version 0.0.1
 **  \param a_begin begin iterator of the vector to rotate
 **  \param a_end  end iterator of the vector to rotate
 **  \param u_begin begin iterator of the householder vector 
 **  \param u_end  end iterator of the the householder vector
 **  \param nu \f$(I -u*u^H)a=\nu *e1 \f$
 **  \pre (a_end - a_begin) == (u_end - u_begin)
 **  \remarks Algorithm 2.1 from "Matrix Algorithms" Vol II G. W. Stewart
 **  \remarks Works with real and complex data.
 **  \par Example:
 **  \code
 **  slip::Vector<double> Xh(5);
 **  slip::iota(Xh.begin(),Xh.end(),5.0,-1.0);
 **  std::cout<<"Xh = \n"<<Xh<<std::endl;
 **  slip::Vector<double> Vh(Xh.size());
 **  double nu  = 0.0;
 **  slip::housegen(Xh.begin(),Xh.end(),Vh.begin(),Vh.end(),nu);
 **  std::cout<<"Vh = \n"<<Vh<<std::endl;
 **  std::cout<<"nu = \n"<<nu<<std::endl;
 **  \endcode
 **/

template <typename VectorIterator1,typename VectorIterator2>
inline
void housegen(VectorIterator1 a_begin, VectorIterator1 a_end,
	      VectorIterator2 u_begin,VectorIterator2 u_end,
	      typename std::iterator_traits<VectorIterator1>::value_type &nu)

 {
       
   assert((a_end - a_begin) == (u_end - u_begin));
   typedef typename std::iterator_traits<VectorIterator1>::value_type value_type; 
   //typedef typename slip::lin_alg_traits<typename std::iterator_traits<VectorIterator1>::value_type>::value_type norm_type; 
   value_type rho = value_type(0);
   
   std::copy(a_begin,a_end,u_begin);
   nu = std::sqrt(slip::L22_norm_cplx(a_begin,a_end));
   if(std::abs(nu) == value_type(0.0))
     {
       *(u_begin) = value_type(std::sqrt(2.0));
       return;
     }
   if(*(u_begin) != value_type(0.0))
     {
       rho = slip::conj(*u_begin) / std::abs(*u_begin);
     }
   else
     { 
       rho = value_type(1.0);
     }
   value_type scal = (rho/nu);
   slip::vector_scalar_multiplies(u_begin,u_end,scal,u_begin);
   *u_begin = value_type(1.0) + *u_begin;
   scal = value_type(1.0) / std::sqrt(std::real(*u_begin));
   slip::vector_scalar_multiplies(u_begin,u_end,scal,u_begin);
   *u_begin = std::real(*u_begin);
   nu = -(slip::conj(rho)) * nu;

 }


  /**
   ** \brief right multiplies the matrix M with the Householder matrix P:
   ** \f[ M = M(I - vv^H) \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   **  
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param V_first RandomAccessIterator to the beginning of the sequence (V)
   ** \param V_last RandomAccessIterator to one-past-the end of the sequence (V)
   ** \param M_up 2D iterator on the upper_left element of M. 
   ** \param M_bot 2D iterator on the bottom_right_left element of M.
   ** \par Complexity: 5*M.rows()*M.cols() for real data, 6*M.rows()*M.cols() for complex data
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Mh(5,4);
   ** slip::iota(Mh.begin(),Mh.end(),1.0);
   ** std::cout<<"Mh = \n"<<Mh<<std::endl;
   ** slip::Vector<double> Vh3(4);
   ** double nu  = 0.0;
   ** slip::housegen(Mh.row_begin(0),Mh.row_end(0),Vh3.begin(),Vh3.end(),nu);
   ** std::cout<<"Vh3 = \n"<<Vh3<<std::endl;
   ** std::cout<<"nu = "<<nu<<std::endl;
   ** slip::right_householder_update(Vh3.begin(),Vh3.end(),
   **			 Mh.upper_left(),Mh.bottom_right());
   ** std::cout<<"Mh = \n"<<Mh<<std::endl;
  \endcode
   */
   template <typename RandomAccessIterator, 
	    typename MatrixIterator1>
  inline
  void
  right_householder_update(RandomAccessIterator V_first, 
			   RandomAccessIterator V_last,
			   MatrixIterator1 M_up,
			   MatrixIterator1 M_bot)
		   
  {
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type value_type;

     typename MatrixIterator1::difference_type sizeM = M_bot - M_up;
    const std::size_t M_rows = sizeM[0];

    
    assert(sizeM[1] == (V_last - V_first));
    
    //computes w = MV
    slip::Array<value_type> w(M_rows);
    slip::matrix_vector_multiplies(M_up,M_bot,V_first,V_last,w.begin(),w.end());
    
    //computes M = M - beta w V^T
    slip::rank1_update(M_up,M_bot,
		       value_type(-1.0),
		       w.begin(),w.end(),
		       V_first,V_last);

  }


   /**
   ** \brief Left multiplies the Householder matrix P with the matrix M:
   ** \f[ M = (I - vv^H)M \f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   **  
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param V_first RandomAccessIterator to the beginning of the sequence (V)
   ** \param V_last RandomAccessIterator to one-past-the end of the sequence (V)
   ** \param M_up 2D iterator on the upper_left element of M. 
   ** \param M_bot 2D iterator on the bottom_right_left element of M.
   ** \par Complexity: 5*M.rows()*M.cols() for real data, 6*M.rows()*M.cols() for complex data
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Mh(5,4);
   ** slip::iota(Mh.begin(),Mh.end(),1.0);
   ** std::cout<<"Mh = \n"<<Mh<<std::endl;
   ** slip::Vector<double> Vh2(5);
   ** double nu  = 0.0;
   ** slip::housegen(Mh.col_begin(0),Mh.col_end(0),Vh2.begin(),Vh2.end(),nu);
   ** std::cout<<"Vh2 = \n"<<Vh2<<std::endl;
   ** std::cout<<"nu = "<<nu<<std::endl;
   ** slip::left_householder_update(Vh2.begin(),Vh2.end(),
   **				Mh.upper_left(),Mh.bottom_right());
   ** std::cout<<"Mh = \n"<<Mh<<std::endl;
   ** \endcode
   */
  template <typename RandomAccessIterator, 
	    typename MatrixIterator1>
  inline
  void
  left_householder_update(RandomAccessIterator V_first, 
			  RandomAccessIterator V_last,
			  MatrixIterator1 M_up,
			  MatrixIterator1 M_bot)
		   
  {
     typedef typename std::iterator_traits<RandomAccessIterator>::value_type value_type;

     typename MatrixIterator1::difference_type sizeM = M_bot - M_up;

    const std::size_t M_cols = sizeM[1];
    
    assert(sizeM[0] == (V_last - V_first));
    
    //computes w = M^Hv 
    slip::Array<value_type> w((std::size_t)sizeM[1]);
    for(std::size_t j = 0; j < M_cols; ++j)
      {
	w[j] = slip::hermitian_inner_product(M_up.col_begin(j),M_up.col_end(j), 
					     V_first,
					     value_type(0));
      }
    //computes M = M - Vw^H
    
    slip::rank1_update(M_up,M_bot,
		       value_type(-1.0),
		       V_first,V_last,
		       w.begin(),w.end());
  }

 /**
   ** \brief Computes Q = Q1Q2...Qn from the inplace Householder QR decomposition. 
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>  
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M 2D Container. 
   ** \param V0 The Vector of the initial value of householder vectors
   ** \param Q The accumulated matrix Q.
   */
  template <typename Matrix1,  typename Vector1,typename Matrix2>
  inline
  void
  left_householder_accumulate(const Matrix1& M,
			      const Vector1& V0,
			      Matrix2& Q)
		   
  {
    typedef typename Matrix1::value_type value_type;

    const std::size_t M_rows = M.rows();
    const std::size_t M_cols = M.cols();
    const int M_rows_m1 = int(M_rows) - 1;
    const int M_cols_m1 = int(M_cols) - 1;
   
    slip::Array<value_type> V(M_rows);
    //init Q with the identity matrix
    slip::identity(Q);
    slip::Box2d<int> box;
    for(int j = M_rows-1; j > -1; --j)
      {
	
	V[j] = value_type(V0[j]);
	std::copy(M.col_begin(j)+(j+1),M.col_end(j),V.begin()+(j+1));
	box.set_coord(j,j,M_rows_m1,M_cols_m1);
	slip::left_householder_update(V.begin()+j,V.end(),
				      Q.upper_left(box),Q.bottom_right(box));

      }
  }

/* @} */

  /** \name Hessenberg reduction */
  /* @{ */
  /**
   ** \brief Householder Hessenberg reduction of the square matrix M. 
   ** The result is overwritten in M. 
   ** The Hessenberg decomposition is \f$Q * H * Q^H = M\f$ 
   ** \li where Q is a square unitary matrix: \f$Q^H * Q = I\f$
   ** \li H is upper Hessenberg: \f$ i \ge j+1 \Rightarrow H_{i,j} = 0\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>  
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M 2D Container. 
   ** \pre M.rows() == M.cols()
   ** \par Complexity: (M.cols()-1)*(10*M.rows()*M.cols()) + (M.rows()*M.rows())/2
   ** \remarks Works with real and complex data.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> HH(4,4);
   ** slip::hilbert(HH);
   ** std::cout<<"HH = \n"<<HH<<std::endl;
   ** slip::householder_hessenberg(HH);
   ** std::cout<<"HH = \n"<<HH<<std::endl;
   ** \endcode
   */
  template <typename Matrix1>
  inline
  void
  householder_hessenberg(Matrix1& M)
		   
  {
    assert(M.rows() == M.cols());
    typedef typename Matrix1::value_type value_type;
   
    const std::size_t M_rows = M.rows();
    const std::size_t M_cols = M.cols();
   
    slip::Array<value_type> V(M_rows);
   
    slip::Box2d<int> box;
    slip::Box2d<int> box2;
    for(std::size_t j = 0; j < (M_cols-1); ++j)
      {
	//computes the householder vector from jth column of M
	box.set_coord(int(j+1),int(j),int(M_rows-1),int(M_cols-1));
	value_type beta = value_type(0);

	slip::housegen(M.upper_left(box).col_begin(0),
		       M.upper_left(box).col_end(0),
		       V.begin()+(j+1),V.end(),
		       beta);

	//computes M = (I-VV^*)M(j+1:M_rows-1,j:M_cols-1)
	slip::left_householder_update(V.begin()+(j+1),V.end(),
				      M.upper_left(box),M.bottom_right(box));
	box2.set_coord(int(0),int(j+1),int(M_rows-1),int(M_cols-1));
	//computes M = M(0:M_rows-1,j+1:M_cols-1)(I-betaVV^*)
	slip::right_householder_update(V.begin()+(j+1),V.end(),
				       M.upper_left(box2),M.bottom_right(box2));

      }
  }


   /**
   ** \brief Householder Hessenberg reduction of the square matrix M.
    ** The Hessenberg decomposition is \f$Q * H * Q^H = M\f$ 
   ** \li where Q is a square unitary matrix: \f$Q^H * Q = I\f$
   ** \li H is upper Hessenberg: \f$ i \ge j+1 \Rightarrow H_{i,j} = 0\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   **  
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M 2D Container of the input Matrix. 
   ** \param H 2D Container of the Hessenberg matrix.
   ** \pre M.rows() == M.cols()
   ** \pre M.rows() == H.rows()
   ** \pre M.cols() == H.cols()
   ** \remarks Works with real and complex data.
   ** \par Complexity: (M.cols()-1)*(10*M.rows()*M.cols()) + (M.rows()*M.rows())/2
   ** \par Example:
   ** \code
   ** slip::Matrix<double> HH(4,4);
   ** slip::hilbert(HH);
   ** std::cout<<"HH = \n"<<HH<<std::endl;
   ** slip::Matrix<double> Hh(4,4);
   ** slip::householder_hessenberg(HH,Hh);
   ** std::cout<<"HH = \n"<<HH<<std::endl;
   ** std::cout<<"Hh = \n"<<Hh<<std::endl;
   ** \endcode
   */
  template <typename Matrix1, typename Matrix2>
  inline
  void
  householder_hessenberg(const Matrix1& M, Matrix2& H)
		   
  {
    assert(M.rows() == M.cols());
    assert(M.rows() == H.rows());
    assert(M.cols() == H.cols());
    
    //init H with M
    H = M;
    slip::householder_hessenberg(H);
 
  }


  /**
   ** \brief Householder Hessenberg reduction of the square matrix M. 
   ** The Hessenberg decomposition is \f$Q * H * Q^H = M\f$ 
   ** \li where Q is a square unitary matrix: \f$Q^H * Q = I\f$
   ** \li H is upper Hessenberg: \f$ i \ge j+1 \Rightarrow H_{i,j} = 0\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   **  
   ** \date 2008/10/28
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param M 2D Container of the input Matrix. 
   ** \param H 2D Container of the Hessenberg matrix.
   ** \param Q 2D Container of the Q matrix.
   ** \pre M.rows() == M.cols()
   ** \pre H.rows() == H.cols()
   ** \pre Q.rows() == Q.cols()
   ** \pre M.rows() == H.rows()
   ** \pre M.rows() == Q.cols()
   ** \remarks Works with real and complex data.
   ** \par Complexity: (M.cols()-1)*(10*M.rows()*M.cols()) + (M.rows()*M.rows())/2
   ** \par Example:
   ** \code
   ** slip::Matrix<double> HH(4,4);
   ** slip::hilbert(HH);
   ** std::cout<<"HH = \n"<<HH<<std::endl;
   ** slip::Matrix<double> Hhh(4,4);
   ** slip::Matrix<double> Phh(4,4);
   ** slip::householder_hessenberg(HH,Hhh,Phh);
   ** std::cout<<"Hhh = \n"<<Hhh<<std::endl;
   ** std::cout<<"Phh = \n"<<Phh<<std::endl;
   ** slip::Matrix<double> PhhT(4,4);
   ** slip::transpose(Phh,PhhT);
   ** slip::Matrix<double> PhhHhh(4,4);
   ** slip::matrix_matrix_multiplies(Phh,Hhh,PhhHhh);
   ** slip::Matrix<double> PhhHhhPhhT(4,4);
   ** slip::matrix_matrix_multiplies(PhhHhh,PhhT,PhhHhhPhhT);
   ** std::cout<<"Phh Hhh PhhT = \n"<<PhhHhhPhhT<<std::endl;
   ** \endcode
   */
  template <typename Matrix1, typename Matrix2, typename Matrix3>
  inline
  void
  householder_hessenberg(const Matrix1& M, Matrix2& H, Matrix3& Q)
		   
  {
    assert(M.rows() == M.cols());
    assert(H.rows() == H.cols());
    assert(Q.rows() == Q.cols());
    assert(M.rows() == H.rows());
    assert(M.rows() == Q.cols());
    
    typedef typename Matrix2::value_type value_type;
 
    //init Q with identity matrix
    slip::identity(Q);
    
    const std::size_t M_rows = M.rows();
    const std::size_t M_cols = M.cols();
    const std::size_t M_cols_m_1 = M_cols-1;
    const std::size_t M_rows_m_1 = M_rows-1;
    slip::Array<value_type> V(M_rows);
   
    slip::Box2d<int> box;
    slip::Box2d<int> box2;
    //init H with M
    std::copy(M.begin(),M.end(),H.begin());
    for(std::size_t j = 0; j < M_cols_m_1; ++j)
      {
	//computes the householder vector from jth column of H
	box.set_coord(int(j+1),int(j),int(M_rows_m_1),int(M_cols_m_1));
	value_type beta = value_type(0);
	
	slip::housegen(H.upper_left(box).col_begin(0),
		       H.upper_left(box).col_end(0),
		       V.begin()+(j+1),
		       V.end(),beta);
	//computes H = (I-VV^*)H(j+1:H_rows-1,j:H_cols-1)
	slip::left_householder_update(V.begin()+(j+1),V.end(),
				      H.upper_left(box),H.bottom_right(box));
	std::fill(H.col_begin(j)+(j+2),H.col_end(j),value_type(0));
	box2.set_coord(int(0),int(j+1),int(M_rows_m_1),int(M_cols_m_1));
	//computes H = H(0:H_rows-1,j+1:H_cols-1)(I-VV^*)
	slip::right_householder_update(V.begin()+(j+1),V.end(),
				       H.upper_left(box2),H.bottom_right(box2));
	//computes Q
	slip::right_householder_update(V.begin()+(j+1),V.end(),
				       Q.upper_left(box2),Q.bottom_right(box2));
	
      }
  }

/* @} */ 
 

}//slip::


#endif //SLIP_LINEAR_ALGEBRA_HPP
