/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file linear_algebra_eigen.hpp
 * 
 * \brief Provides some algorithms to computes eigenvalues and eigenvectors.
 * \since 1.0.0
 * 
 */
#ifndef SLIP_LINEAR_ALGEBRA_EIGEN_HPP
#define SLIP_LINEAR_ALGEBRA_EIGEN_HPP

#include <cmath>
#include <complex>
#include <algorithm>
#include <map>
#include "Vector.hpp"
#include "Array2d.hpp"
#include "linear_algebra.hpp"
#include "linear_algebra_qr.hpp"
#include "linear_algebra_svd.hpp"
#include "linear_algebra_traits.hpp"
#include "macros.hpp"
#include "norms.hpp"
#include "noise.hpp"
#include "apply.hpp"
#include "complex_cast.hpp"

namespace slip
{

  
  /**
   ** \brief Eigen Values Computation of an hermitian semi-definite positive matrix.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/03/23
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param A Matrix container
   ** \param EigenValues Vector containing the real eigenvalue
   ** \param EigenVectors Container2d containing the eigenvectors
   ** \remarks The eigen vectors may be negated as SVD is not unique
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Meig(4,4);
   ** slip::hilbert(Meig);
   ** std::cout<<"Meig = \n"<<Meig<<std::endl;
   ** slip::Vector<double> MEigVal(4);
   ** slip::Matrix<double> MEigVect;
   ** slip::hermitian_eigen(Meig,MEigVal,MEigVect);
   ** std::cout<<"MEigVal = \n"<<MEigVal<<std::endl;
   ** std::cout<<"MEigVect = \n"<<MEigVect<<std::endl;
   ** \endcode
   */
  template <class Matrix1,
	    class Vector1, 
	    class Matrix2>
  inline
  void hermitian_eigen(const Matrix1 & A, 
		       Vector1& EigenValues, 
		       Matrix2& EigenVectors)
  {
    typedef typename Matrix1::value_type value_type;
    std::size_t A_rows = A.rows();
    std::size_t A_cols = A.cols();
    
    Matrix1 U;
    Matrix1 V;
    Matrix1 AA;
    if(A_rows >= A_cols)
      {
	AA.resize(A_cols,A_cols);
	U.resize(A_cols,A_cols);
	EigenValues.resize(A_cols);
	EigenVectors.resize(A_cols,A_cols);
	//compute A^HA
	for(std::size_t i = 0; i < A_cols; ++i)
	  {
	    for(std::size_t j = 0; j < A_cols; ++j)
	      {
		AA(i,j) = slip::hermitian_inner_product(A.col_begin(i),A.col_end(i),A.col_begin(j),value_type(0));
	      }
	  }
	//compute the svd
	slip::svd(AA,U,EigenValues.begin(),EigenValues.end(),EigenVectors);
    
      }
    else
      {
	AA.resize(A_rows,A_rows);
	V.resize(A_rows,A_rows);
	EigenValues.resize(A_rows);
	EigenVectors.resize(A_rows,A_rows);
	//compute AA^H
	for(std::size_t i = 0; i < A_rows; ++i)
	  {
	    for(std::size_t j = 0; j < A_rows; ++j)
	      {
		AA(i,j) = slip::hermitian_inner_product(A.row_begin(i),A.row_end(i),A.row_begin(j),value_type(0)); 
	      }
	  }
	//compute the svd
	slip::svd(AA,EigenVectors,EigenValues.begin(),EigenValues.end(),V);
      }
    
    slip::apply(EigenValues.begin(),EigenValues.end(),EigenValues.begin(),std::sqrt);
  }


  

/**
   ** \brief Eigen Values Computation for non symmetric  matrix.
   ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
   ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2014/03/13
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param A Matrix container
   ** \param E Vector containing the real eigenvalue
   ** \param sort a boolean, if sort == true eigen will be sorted by decressing order
   ** \pre A must be real
   ** \pre E must be complex
   ** \todo Add the eigen vectors
   ** \todo optimize schur transform
   ** \par Example:
   ** \code
   ** double db[] ={3.0, 7.0, 5.0, 10.0, 7.0, 8.0, 15.0, 11.0, 12.0};
   ** slip::Matrix<double> Mb(3,3,db);
   ** slip::Vector<std::complex<double> > Eigb(3);
   ** std::cout<<" Mb \n"<<Mb<<std::endl;
   ** slip::eigen(Mb,Eigb,true);
   ** std::cout<<" Eigenvalues\n"<<Eigb<<std::endl;
   ** \endcode
   */
  template <class Matrix1,class Vector1>
  inline
  void eigen(const Matrix1 & A, Vector1 & E, bool sort=false)
  {
    typedef typename Matrix1::value_type value_type;
    slip::Array2d<value_type> H(A.rows(),A.cols());
    slip::Array2d<value_type> Z(A.rows(),A.cols());
    slip::Array2d<value_type> D(A.rows(),A.cols());
    Matrix1 Atmp(A);
    slip::balance(Atmp,D);
    slip::schur(A,H,Z,E,false);
    //check is E is a real vector
    bool real = true;
    for(std::size_t i = 0 ; i < E.size() ; ++i)
      {
	if(std::abs(std::imag(E[i])) > slip::epsilon<typename slip::lin_alg_traits<typename Vector1::value_type>::value_type >())
	  {
	    real = false;
	    break;
	  }
      }
    if(sort && real)
      {
	typedef typename slip::lin_alg_traits<typename Vector1::value_type>::value_type real;
	typedef typename Vector1::value_type complex_type;
	slip::Vector<real> Er(E.size());
	for(std::size_t i = 0; i < E.size(); ++i)
	  {
	    Er[i]  = std::real(E[i]);
	  }
	std::sort(Er.begin(),Er.end(),std::greater<real>());
	E.fill(typename Vector1::value_type(0));


	for(std::size_t i = 0; i < E.size(); ++i)
	  {
	    //	    E[i].real()  = Er[i];
	    //E[i].imag()  = real(0); 
	    E[i] = complex_type(Er[i],real(0));
	  }
      }
    else
      {
	std::sort(E.begin(),E.end(),slip::greater_abs<typename Vector1::value_type>());
      }
  }


   /**
   ** \brief Spectral radius of a matrix \f$ \rho(A) =  \max_i \{ |\lambda_i|\}\f$.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2014/03/13
   ** \since 1.0.0
   ** \version 0.0.2
   ** \param A Matrix container
   ** \par Example:
   ** \code
   ** slip::Matrix<double> Meig(4,4);
   ** slip::hilbert(Meig);
   ** std::cout<<"Meig = \n"<<Meig<<std::endl;
   ** std::cout<<"spectral radius = \n"<<slip::spectral_radius(Meig)<<std::endl;
   ** \endcode
   */
  template <class Matrix1>
  inline
  typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type 
  spectral_radius(const Matrix1 & A)
  {
 
    typedef typename slip::lin_alg_traits<typename Matrix1::value_type>::value_type value_type;
    slip::Vector<std::complex<value_type> > MEigVal(A.rows());
    slip::eigen(A,MEigVal,true);
    return std::abs(MEigVal[0]);
  }

 
 /**
** \brief Computes the sinus and cosinus of the orientation of a symmetric 2x2 real matrix A.
** \since 1.5.0
** \date 2021/01/09
** \param a00 a00 term of the matrix.
** \param a01 a01 term of the matrix.
** \param a11 a11 term of the matrix.
** \param c cosinus of the orientation.
** \param s sinus of the orientation.
** \par Example:
** \code
** typedef double T;
** slip::Matrix<T> M(2,2);
** M[0][0] = 3.0; M[0][1] = 2.0;
** M[1][0] = 2.0; M[1][1] = 1.0;
**
**  T s = T();
**  T c = T();
** slip::computes_sin_cos(M[0][0],M[0][1],M[1][1],s,c);
** std::cout<<"M = \n"<<M<<std::endl;
** std::cout<<"cos(theta) = "<<c<<std::endl;
** std::cout<<"sin(theta) = "<<s<<std::endl;
** \endcode
**/
template<typename Real>
void computes_sin_cos(const Real& a00,
		 const Real& a01,
		 const Real& a11,
		 Real& s, 
		 Real& c)
{
// c2 = cos (2*theta) , s2 = sin(2*theta)
  const Real zero = Real();
  Real c2 = ( a00 - a11 )*slip::constants<Real>::half();
  Real s2 = a01;
  const Real maxAbsT = std::max( std::abs(c2),std::abs(s2)) ;
  if (maxAbsT > zero)
  {
    // A is not a multiple of the identity. One of c2 or s2 will be 1 .
    // This block is a robust computation of the argument of sqrt().
    c2 /= maxAbsT;
    s2 /= maxAbsT;
    const Real length = std::sqrt( c2 * c2 + s2 * s2 ) ;
    c2 /=length ;
    s2 /=length ; // not zero because a01 is not zero
    if ( c2 > zero)
      {
	// Choose c2 < 0 for robust computation of sin ( theta ) and cos ( theta) .
	c2 = -c2 ;
	s2 = -s2 ;
      }
  }
  else
    {
      // A is a multiple of the identity . Choose c2 < 0 for robust
      // computation of sin(theta) and cos(theta) .
      c2 = -slip::constants<Real>::one();
      s2 = zero;
    }
  // s = sin (theta) , c = cos (theta)
  s = std::sqrt(slip::constants<Real>::half() * ( slip::constants<Real>::one() - c2 ) ) ; // >= 1/ sqrt(2)
  c = slip::constants<Real>::half() * s2 / s ;
}

/**
** \brief Computes the eigenvalues and eigenvectors of a symmetric 2x2 real matrix A = eigenvectors*diag(eigenvalues)*eigenvectors^T
** \since 1.5.0
** \date 2021/01/09
** \param a00 a00 term of the matrix.
** \param a01 a01 term of the matrix.
** \param a11 a11 term of the matrix.
** \param eigenvalues vector of size 2 containing eigen values.
** \param eigenvectors matrix of size 2x2 containing on the first column the first eigen vector and on the second column the second eigenvector.
** \param sorted if true the first eigenvalue is greater than the second if false the eigenvalues are not sorted.
** \par Example:
** \code
** typedef double T;
** slip::Matrix<T> M(2,2);
** M[0][0] = 3.0; M[0][1] = 2.0;
** M[1][0] = 2.0; M[1][1] = 1.0;
** slip::Vector<T> eigenvalues(2);
** slip::Matrix<T> eigenvectors(2,2);
** slip::eigen_symmetric_2x2(M[0][0],M[0][1],M[1][1],
**                           eigenvalues,
**                           eigenvectors,
**                           true);
** std::cout<<"eigenvalues  = \n"<<eigenvalues<<std::endl;
** std::cout<<"eigenvectors = \n"<<eigenvectors<<std::endl;
** \endcode
**/
template<typename Real,
	 class Vector2,
	 class Matrix2x2>
void eigen_symmetric_2x2(const Real& a00,
			   const Real& a01,
			   const Real& a11,
			   Vector2& eigenvalues, 
			   Matrix2x2& eigenvectors,
			   const bool sorted = false)
{
  eigenvalues.resize(2);
  eigenvectors.resize(2,2);
// c2 = cos (2*theta) , s2 = sin(2*theta)
  const Real zero = Real();
  Real c2 = ( a00 - a11 )*slip::constants<Real>::half();
  Real s2 = a01;
  const Real maxAbsT = std::max( std::abs(c2),std::abs(s2)) ;
  if (maxAbsT > zero)
  {
    // A is not a multiple of the identity. One of c2 or s2 will be 1 .
    // This block is a robust computation of the argument of sqrt().
    c2 /= maxAbsT;
    s2 /= maxAbsT;
    const Real length = std::sqrt( c2 * c2 + s2 * s2 ) ;
    c2 /=length ;
    s2 /=length ; // not zero because a01 is not zero
    if ( c2 > zero)
      {
	// Choose c2 < 0 for robust computation of sin ( theta ) and cos ( theta) .
	c2 = -c2 ;
	s2 = -s2 ;
      }
  }
  else
    {
      // A is a multiple of the identity . Choose c2 < 0 for robust
      // computation of sin(theta) and cos(theta) .
      c2 = -slip::constants<Real>::one();
      s2 = zero;
    }
  // s = sin (theta) , c = cos (theta)
  const Real s = std::sqrt(slip::constants<Real>::half() * ( slip::constants<Real>::one() - c2 ) ) ; // >= 1/ sqrt(2)
  const Real c = slip::constants<Real>::half() * s2 / s ;

  const Real csquare = c*c;
  const Real ssquare = s*s;
  const Real mid = s2 * a01;
  eigenvalues[0] = (csquare*a00) + mid + (ssquare*a11);
  eigenvalues[1] = (csquare*a11) - mid + (ssquare*a00);
  
  eigenvectors[0][0] = c;
  eigenvectors[1][0] = s;
  eigenvectors[0][1] = -s;
  eigenvectors[1][1] = c;
  
if(sorted)
  {
if(eigenvalues[0] < eigenvalues[1])
  { 
std::cout<<"swap eigen values"<<std::endl;
   std::swap(eigenvalues[0],eigenvalues[1]);
   eigenvectors[0][0] = -s;
   eigenvectors[1][0] =  c;
   eigenvectors[0][1] =  -c;
   eigenvectors[1][1] =  -s;

  } 
  }

}
/**
** \brief Computes the eigenvalues and eigenvectors of a symmetric 2x2 real matrix A = eigenvectors*diag(eigenvalues)*eigenvectors^T
** \since 1.5.0
** \date 2021/01/09
** \param M a 2x2 real symmetric matrix.
** \param a01 a01 term of the matrix.
** \param a11 a11 term of the matrix.
** \param eigenvalues vector of size 2 containing eigen values.
** \param eigenvectors matrix of size 2x2 containing on the first column the first eigen vector and on the second column the second eigenvector.
** \param sorted if true the first eigenvalue is greater than the second if false the eigenvalues are not sorted.
** \pre M shoud be a real symmetric 2x2 matrix.
** \par Example:
** \code
** typedef double T;
** slip::Matrix<T> M(2,2);
** M[0][0] = 3.0; M[0][1] = 2.0;
** M[1][0] = 2.0; M[1][1] = 1.0;
** slip::Vector<T> eigenvalues(2);
** slip::Matrix<T> eigenvectors(2,2);
** slip::eigen_symmetric_2x2(M,
**                           eigenvalues,
**                           eigenvectors,
**                           true);
** std::cout<<"eigenvalues  = \n"<<eigenvalues<<std::endl;
** std::cout<<"eigenvectors = \n"<<eigenvectors<<std::endl;
** \endcode
**/
template<class Vector2,
	 class Matrix2x2>
void eigen_symmetric_2x2(const Matrix2x2& A,
			 Vector2& eigenvalues, 
			 Matrix2x2& eigenvectors,
			 const bool sorted = false)
{
assert(A.rows() == 2);
assert(A.rows() == A.cols());
assert(A[0][1] == A[1][0]);
slip::eigen_symmetric_2x2(A[0][0],A[0][1],A[1][1],eigenvalues,eigenvectors,sorted);
}

 
 
} // end slip

#endif //SLIP_LINEAR_ALGEBRA_EIGEN_HPP
