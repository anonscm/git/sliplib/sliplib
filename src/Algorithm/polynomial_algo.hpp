/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file polynomial_algo.hpp
 * 
 * \brief Provides some polynomial algorithms.
 * 
 */
#ifndef SLIP_POLYNOMIAL_ALGO_HPP
#define SLIP_POLYNOMIAL_ALGO_HPP

#include "statistics.hpp"
#include "Array2d.hpp"
#include "arithmetic_op.hpp"
#include "utils_compilation.hpp"

#include "Point3d.hpp" //for eval_poly_* et fit_poly
#include "Vector.hpp" //for eval_poly_* et fit_poly
#include "Matrix.hpp"
//#include "MultivariatePolynomial.hpp"
#include <iterator>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <cmath>


namespace slip
{
 /** \name Collocations algorithms */
  /* @{ */
  /**
 ** \brief For a given number of points n, computes the Chebyshev  
 ** collocations within the range [a,b]:
 ** \f[ x_i = \frac{a+b}{2} + \frac{a-b}{2}\cos\left(\frac{2i-1}{2N}\pi\right),\quad \forall i \in \{1,\cdots,N \}\f]
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2010/11/07
 ** \since 1.5.0
 ** \version 0.0.1
 ** \param first RandomAccessIterator to the result.
 ** \param last RandomAccessIterator to the result.
 ** \param a first value of the range [a,b] (-1.0 by default).
 ** \param b second value of the range [a,b] (1.0 by default).
 ** \pre a < b
 ** \pre a, b and the range value type should be real: float, double or long double
 ** \par Description:
 ** For a given number of points n, computes the Gauss Lobatto Chebyshev
 ** collocations within the range [a,b]:
 ** \f[ x_i = \frac{a+b}{2} + \frac{a-b}{2}\cos\left(\frac{2i-1}{2N}\pi\right),\quad \forall i \in \{1,\cdots,N \}\f]
 ** This points corresponds to the n roots of a Chebyshev polynomial of the first kind 
 ** \f[ T_n(x) = \cos(n acos(x))\f]
 ** \par Example:
 ** \code
 ** slip::Vector<double> V(21);
 ** slipalgo::chebyshev_collocations(V.begin(),V.end(),-1.0,1.0);
 ** std::cout<<V<<std::endl;
 ** \endcode
 */
template<typename RandomAccessIterator, typename Real>
void chebyshev_collocations(RandomAccessIterator first, 
			    RandomAccessIterator last,
			    const Real& a = static_cast<Real>(-1),
			    const Real& b = static_cast<Real>(1))
{
  Real n = static_cast<Real>(last-first);
  Real one = slip::constants<Real>::one();
  Real two = slip::constants<Real>::two();
  Real mean_a_b  = (a+b)/two;
  Real a_m_b_o_2 = (a-b)/two;
  Real pi_o_2n  = slip::constants<Real>::pi()/(two*n);
  Real i = 1;
  for(; first != last; ++first, ++i)
    {
      *first = mean_a_b + a_m_b_o_2 * std::cos((two*i-one)*pi_o_2n);
    }
  
}

/**
 ** \brief For a given number of points n, computes the uniform  
 ** collocations within the range [a,b].
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2010/11/07
 ** \version 0.0.1 
 ** \since 1.5.0
 ** \param first RandomAccessIterator to the result.
 ** \param last RandomAccessIterator to the result.
 ** \param a first value of the range [a,b] (-1.0 by default).
 ** \param b second value of the range [a,b] (1.0 by default).
 ** \pre a < b
 ** \pre a, b and the range value type should be real: float, double or long double
 ** \par Example:
 ** \code
 ** slip::Vector<double> V(21);
 ** slipalgo::uniform_collocations(V.begin(),V.end(),-1.0,1.0);
 ** std::cout<<V<<std::endl;
 ** \endcode
 */  
template<typename RandomAccessIterator, typename Real>
void uniform_collocations(RandomAccessIterator first, 
			  RandomAccessIterator last,
			  const Real& a = static_cast<Real>(-1),
			  const Real& b = static_cast<Real>(1))
{

  Real step = (b-a)/ static_cast<Real>(last-first-1); 
  slip::iota(first,last,a,step);
}


/**
 ** \brief For a given number of points n, computes the uniform  
 ** Gram (discrete Chebyshev collocations) within the range [a,b].
 ** \f[ x_k = (a+\frac{(b-a)}{2N} + \frac{(b-a)}{N}k, \quad 1 \le k \le N\f]
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2016/04/11
 ** \version 0.0.1
 ** \since 1.5.0
 ** \param first RandomAccessIterator to the result.
 ** \param last RandomAccessIterator to the result.
 ** \param a first value of the range [a,b] (-1.0 by default).
 ** \param b second value of the range [a,b] (1.0 by default).
 ** \pre a < b
 ** \pre a, b and the range value type should be real: float, double or long double
 ** \par Example:
 ** \code
 ** slip::Vector<double> V(21);
 **  slipalgo::gram_uniform_collocations(V.begin(),V.end(),-1.0,1.0);
 ** std::cout<<V<<std::endl;
 ** \endcode
 */  
template<typename RandomAccessIterator, typename Real>
void gram_uniform_collocations(RandomAccessIterator first, 
			       RandomAccessIterator last,
			       const Real& a = static_cast<Real>(-1),
			       const Real& b = static_cast<Real>(1))
{
  Real m = static_cast<Real>(last-first);
  Real step = (b-a)/m; 
  slip::iota(first,last,a+(step)/2,step);
}

/**
 ** \brief For a given number of points n, computes the Fejer
 ** collocations (equivalent to chebyshev collocations on the range [-1,1]) 
 ** \f[ x_i = \cos\left(\frac{2i-1}{2N}\pi\right),\quad \forall i \in \{1,\cdots,N \}\f]
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2010/11/07
 ** \version 0.0.1
 ** \since 1.5.0
 ** \param first RandomAccessIterator to the result.
 ** \param last RandomAccessIterator to the result.
 ** \par Example:
 ** \code
 ** slip::Vector<double> V(21);
 ** slipalgo::fejer_collocations(V.begin(),V.end(),-1.0,1.0);
 ** std::cout<<V<<std::endl;
 ** \endcode
 */
template<typename RandomAccessIterator>
void fejer_collocations(RandomAccessIterator first, 
			RandomAccessIterator last)
{
  slip::chebyshev_collocations(first,last,
				   typename std::iterator_traits<RandomAccessIterator>::value_type(-1),
				   typename std::iterator_traits<RandomAccessIterator>::value_type(1));
   
}

/**
 ** \brief For a given number of points N, computes the Fejer
 ** weights
 ** \f[ w_i = \frac{2}{N}\left(1-2\sum_{n=1}^{N/2}\cos\left(2n\frac{2i-1}{2N*(4n^2-1)}\pi\right)\right),\quad \forall i \in \{1,\cdots,N \}\f]
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2010/11/07
 ** \version 0.0.1
 ** \since 1.5.0
 ** \param first RandomAccessIterator to the result.
 ** \param last RandomAccessIterator to the result.
 ** \par Example:
 ** \code
 ** slip::Vector<double> V(21);
 ** slip::fejer_weights(V.begin(),V.end());
 ** std::cout<<V<<std::endl;
 ** slip::Vector<double> V2(20);
 ** slip::fejer_weights(V2.begin(),V2.end());
 ** \endcode
 */
template<typename RandomAccessIterator>
void fejer_weights(RandomAccessIterator first, 
		   RandomAccessIterator last)
{
  typedef typename std::iterator_traits<RandomAccessIterator>::value_type Real;
  Real N = static_cast<Real>(last-first);
  Real one = slip::constants<Real>::one();
  Real two = slip::constants<Real>::two();
  Real four = two * two;
  Real pi_o_2N  = slip::constants<Real>::pi()/(two*N);

  Real two_o_N = two/N;
  
  Real i = Real(1);
  for(; first != last; ++first, i+=Real(1))
    {
      Real theta_i = (two*i-one)*pi_o_2N;
      Real s = Real(0);
      for(std::size_t n = 1; n < N/2; ++n)
	{
	  s += std::cos((two*static_cast<Real>(n))*theta_i)/(four*static_cast<Real>(n*n)-one); 
	}
      *first = two_o_N*(one - two*s);
    }
  
}

/**
   ** \brief functor to compute uniform collocations.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2011/03/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \par Example:
   **\code
   ** slip::Array<double> x(10);
   ** slipalgo::UniformCollocations<double> UC(-1.0,1.0);
   ** UC(x.begin(),x.end());
   ** std::cout<<"x = \n"<<x<<std::endl;
   ** \endcode
   */
  template<typename Real>
  struct UniformCollocations
  {
    /**
     ** \brief Construct the collocation range [-1,1]
     */
    UniformCollocations():a_(static_cast<Real>(-1)),b_(static_cast<Real>(1))
    {}

    /**
     ** \brief Construct the collocation range [a,b]
     ** \param a lower limit of the range.
     ** \param b upper limit of the range.
     */
    UniformCollocations(const Real a,
			const Real b):
      a_(static_cast<Real>(-1)),b_(static_cast<Real>(1))
    {}

    /**
     ** \brief Computes the uniform collocations
     ** \param first RandomAccessIterator on the collocations range.
     ** \param last RandomAccessIterator on the collocations range.
     */
    template <typename RandomAccessIterator>
    void operator()(RandomAccessIterator first, RandomAccessIterator last)
    {
      slip::uniform_collocations(first,last,a_,b_);
    }
    
    /**
     ** \brief Return the name of the collocation.
     ** \return std::string 
     */
    std::string name()
    {
      return "Uniform collocations";
    }

    Real a_;
    Real b_;

  };

/**
   ** \brief functor to compute Gram (discrete Chebychev) uniform collocations.
   ** \f[ x_k = (a+\frac{(b-a)}{2N} + \frac{(b-a)}{N}k, \quad 1 \le k \le N\f]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2016/04/11
   ** \version 0.0.1
   ** \since 1.5.0
   ** \par Example:
   **\code
   ** slip::Array<double> x(10);
   ** slipalgo::GramUniformCollocations<double> GUC(-1.0,1.0);
   ** GUC(x.begin(),x.end());
   ** std::cout<<"x = \n"<<x<<std::endl;
   ** \endcode
   */
  template<typename Real>
  struct GramUniformCollocations
  {
    /**
     ** \brief Construct the collocation range [-1,1]
     */
    GramUniformCollocations():a_(static_cast<Real>(-1)),b_(static_cast<Real>(1))
    {}

    /**
     ** \brief Construct the collocation range [a,b]
     ** \param a lower limit of the range.
     ** \param b upper limit of the range.
     */
    GramUniformCollocations(const Real a,
			const Real b):
      a_(static_cast<Real>(-1)),b_(static_cast<Real>(1))
    {}

    /**
     ** \brief Computes the Gram uniform collocations
     ** \param first RandomAccessIterator on the collocations range.
     ** \param last RandomAccessIterator on the collocations range.
     */
    template <typename RandomAccessIterator>
    void operator()(RandomAccessIterator first, RandomAccessIterator last)
    {
      slip::gram_uniform_collocations(first,last,a_,b_);
    }
    
    /**
     ** \brief Return the name of the collocation.
     ** \return std::string 
     */
    std::string name()
    {
      return "Gram Uniform collocations";
    }

    Real a_;
    Real b_;

  };


  /**
   ** \brief functor to compute Chebyshev collocations.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2011/03/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \par Example:
   **\code
   ** slip::Vector<double> V(21);
   ** slipalgo::ChebyshevCollocations<double> CC(-1.0,1.0);
   ** CC(V.begin(),V.end());
   ** std::cout<<V<<std::endl;
   ** \endcode
   */
  template<typename Real>
  struct ChebyshevCollocations
  {
    /**
     ** \brief Construct the collocation range [-1,1]
     */
    ChebyshevCollocations():a_(static_cast<Real>(-1)),b_(static_cast<Real>(1))
    {}

    /**
     ** \brief Construct the collocation range [a,b]
     ** \param a lower limit of the range.
     ** \param b upper limit of the range.
     */
    ChebyshevCollocations(const Real a,
			const Real b):
      a_(a),b_(b)
    {}

    /**
     ** \brief Computes the Chebyshev collocations
     ** \param first RandomAccessIterator on the collocations range.
     ** \param last RandomAccessIterator on the collocations range.
     */
    template <typename RandomAccessIterator>
    void operator()(RandomAccessIterator first, RandomAccessIterator last)
    {
      slip::chebyshev_collocations(first,last,a_,b_);
    }
    

     /**
     ** \brief Return the name of the collocation.
     ** \return std::string 
     */
    std::string name()
    {
      return "Chebyshev collocations";
    }

    Real a_;
    Real b_;
  };


/* @} */  
  
  /** \name Legendre polynomial algorithms */
  /* @{ */
  /*!
  ** \brief Implements the three term recurrence relation for the 
  ** Legendre multivariate polynomials, this function can be used to create 
  ** a sequence of orthogonal polynomial, and for rising k. 
  ** This recurrence relation holds for Legendre Polynomials of both 
  ** the first and second kinds:
  ** \f[ \frac{(2k+1)xPk(x)-kP_{k-1}}{k+1}\f]
  ** \since 1.0.0
  ** \param k The degree of the last polynomial calculated.
  ** \param x The x polynomial.
  ** \param Pk The value of the polynomial evaluated at degree k.
  ** \param Pkm1 The value of the polynomial evaluated at degree k-1.
  ** \remarks This code is inspired from the file 
  ** boost/math/special_functions/legendre.hpp (http://www.boost.org/)
  ** \par Example:
  ** \code
  ** //creation of the two initial polynomial
  ** slip::MultivariatePolynomial<double,2> p0;
  ** slip::Monomial<2> m01;
  ** m01.powers[0] = 0;
  ** m01.powers[1] = 0;
  ** p0.insert(m01,1.0);
  ** slip::MultivariatePolynomial<double,2> p1;
  ** slip::Monomial<2> m11;
  ** m11.powers[0] = 1;
  ** m11.powers[1] = 0;
  ** p1.insert(m11,1.0);
  ** std::vector<slip::MultivariatePolynomial<double,2> > basis_nd;
  ** basis_nd.push_back(p0);
  ** basis_nd.push_back(p1);
  ** for(unsigned l = 1; l < 10; ++l)
  **    basis_nd.push_back(slip::legendre_nd_next<slip::MultivariatePolynomial<double,2> >(l, p1, basis_nd[l], basis_nd[l-1]));
  **
  **
  ** \endcode
  */
  template <typename Poly, typename Poly1, typename Poly2, typename Poly3>
  inline Poly
  legendre_nd_next(std::size_t k, Poly1 x, Poly2 Pk, Poly3 Pkm1)
  {
    typedef typename Poly::mapped_type T;
   
    return ((((Poly(x) * Poly(Pk))* T(2 * k + 1)) - (Poly(Pkm1)* T(k))) / T(k + 1));
    
  }

   /*!
  ** \brief Implements the three term recurrence relation for the 
  ** Legendre polynomials, this function can be used to create 
  ** a sequence of orthogonal polynomial, and for rising k. 
  ** This recurrence relation holds for Legendre Polynomials of both 
  ** the first and second kinds:
  ** \f[ \frac{(2k+1)xPk(x)-kP_{k-1}}{k+1}\f]
  ** \since 1.0.0
  ** \param k The degree of the last polynomial calculated.
  ** \param x The x polynomial.
  ** \param Pk The value of the polynomial evaluated at degree k.
  ** \param Pkm1 The value of the polynomial evaluated at degree k-1.
  ** \remarks This code is inspired from the file 
  ** boost/math/special_functions/legendre.hpp (http://www.boost.org/)
  ** \par Example:
  ** \code
  ** //creation of the two initial polynomial
  ** slip::Polynomial<double> P0(0,1.0);
  ** slip::Polynomial<double> P1(1,1.0);
  ** P1[0] = 0.0;
  ** std::vector<slip::Polynomial<double> > basis;
  ** //creation of Legendre polynomial basis
  ** basis.push_back(P0);
  ** basis.push_back(P1);
  ** for(unsigned k = 1; k < 10; ++k)
  **    basis.push_back(my_legendre_next<slip::Polynomial<double> >(k, P1, basis[k], basis[k-1]));
  **
  ** \endcode
  */
  template <typename Poly, typename Poly1, typename Poly2, typename Poly3>
  inline Poly
  legendre_next(std::size_t k, Poly1 x, Poly2 Pk, Poly3 Pkm1)
  {
    typedef typename Poly::value_type T;
   
    return ((((Poly(x) * Poly(Pk))* T(2 * k + 1)) - (Poly(Pkm1)* T(k))) / T(k + 1));
    
  }

  /* @} */


 /** \name Chebyshev polynomial algorithms */
  /* @{ */

  /*!
  ** \brief Implements the three term recurrence relation for the 
  ** Chebyshev polynomials, this function can be used to create 
  ** a sequence of orthogonal polynomial, and for rising k. 
  ** This recurrence relation holds for Chebyshev Polynomials of both 
  ** the first and second kinds:
  ** \f[ P_{k+1}(x) = 2xP_k(x)-P_{k-1}(x)\f]
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2010/11/07
  ** \version 0.0.1
  ** \since 1.4.0
  ** \param k The degree of the last polynomial calculated.
  ** \param x The x polynomial.
  ** \param Pk The value of the polynomial evaluated at degree k.
  ** \param Pkm1 The value of the polynomial evaluated at degree k-1.
  ** \return Pk+1
  ** \par Example 1:
  ** \code
  ** std::vector<slip::Polynomial<double> > chebyshev_basis;
  ** //creation of Chebyshev polynomial of the first kind basis
  ** slip::Polynomial<double> P0(0,1.0);
  ** slip::Polynomial<double> P1(1,1.0);
  ** P1[0] = 0.0;
  ** chebyshev_basis.push_back(P0);
  ** chebyshev_basis.push_back(P1);
  ** for(unsigned k = 1; k < 10; ++k)
  **  {
  **  chebyshev_basis.push_back(slip::chebyshev_next<slip::Polynomial<double> >(k, P1, chebyshev_basis[k], chebyshev_basis[k-1]));
  **  }
  ** for(std::size_t k = 0; k < chebyshev_basis.size(); ++k)
  **  {
  **   std::cout<<chebyshev_basis[k]<<std::endl;
  **  }
  ** \endcode
  ** \par Example 2:
  ** \code
  ** std::vector<slip::Polynomial<double> > chebyshev_basis;
  ** //creation of Chebyshev polynomial of the second kind basis
  ** slip::Polynomial<double> P0(0,1.0);
  ** slip::Polynomial<double> P1(1,2.0);
  ** P1[0] = 0.0;
  ** chebyshev_basis.push_back(P0);
  ** chebyshev_basis.push_back(P1);
  ** for(unsigned k = 1; k < 10; ++k)
  **  {
  **  chebyshev_basis.push_back(slip::chebyshev_next<slip::Polynomial<double> >(k, P1, chebyshev_basis[k], chebyshev_basis[k-1]));
  **  }
  ** for(std::size_t k = 0; k < chebyshev_basis.size(); ++k)
  **  {
  **   std::cout<<chebyshev_basis[k]<<std::endl;
  **  }
  ** \endcode
  */
 template <typename Poly, 
	   typename Poly1, 
	   typename Poly2, 
	   typename Poly3>
 inline Poly
 chebyshev_next(std::size_t UNUSED(k), Poly1 x, Poly2 Pk, Poly3 Pkm1)
  {
    typedef typename Poly::value_type T;
   
    return ((Poly(x) * Poly(Pk))* T(2)) - Poly(Pkm1);
    
  }

   /*!
  ** \brief Implements the three term recurrence relation for the 
  ** Chebyshev multivariate polynomials, this function can be used to create 
  ** a sequence of orthogonal polynomial, and for rising k. 
  ** This recurrence relation holds for Chebychev Polynomials of both 
  ** the first and second kinds:
  ** \f[ 2xPk(x)-P_{k-1}\f]
  ** \since 1.4.0
  ** \param k The degree of the last polynomial calculated.
  ** \param x The x polynomial.
  ** \param Pk The value of the polynomial evaluated at degree k.
  ** \param Pkm1 The value of the polynomial evaluated at degree k-1.
  ** \par Example:
  ** \code
  ** slip::MultivariatePolynomial<double,2> p0;
  ** slip::Monomial<2> m01;
  ** m01.powers[0] = 0;
  ** m01.powers[1] = 0;
  ** p0.insert(m01,1.0);
  ** slip::MultivariatePolynomial<double,2> p1;
  ** slip::Monomial<2> m11;
  ** m11.powers[0] = 1;
  ** m11.powers[1] = 0;
  ** p1.insert(m11,1.0);
  ** std::vector<slip::MultivariatePolynomial<double,2> > chebyshev_basis_2d;
  ** chebyshev_basis_2d.push_back(p0);
  ** chebyshev_basis_2d.push_back(p1);
  ** for(unsigned l = 1; l < 3; ++l)
  ** {
  **	chebyshev_basis_2d.push_back(slip::chebyshev_nd_next<slip::MultivariatePolynomial<double,2> >(l, p1, chebyshev_basis_2d[l], chebyshev_basis_2d[l-1]));
  ** }
  **
  **  for(unsigned l = 0; l < chebyshev_basis_2d.size(); ++l)
  **  {
  **    std::cout<<chebyshev_basis_2d[l]<<std::endl;
  **  }
  **
  **
  **
  ** \endcode
  */
  template <typename Poly, typename Poly1, typename Poly2, typename Poly3>
  inline Poly
  chebyshev_nd_next(std::size_t UNUSED(k), Poly1 x, Poly2 Pk, Poly3 Pkm1)
  {
    typedef typename Poly::mapped_type T;
   
    return ((Poly(x) * Poly(Pk))* T(2)) - Poly(Pkm1);
    
  }

  /*!
  ** \brief Returns the evaluations of x until order \a n to 
  **  the range [first,last) using the ChebyshevII scheme.
  ** The order of the resulting values is the following
  ** (1,2x,4x^2-1,8x^3-4x,...).
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2010/11/21
  ** \version 0.0.1
  ** \since 1.4.0
  ** \param x Location to be evaluated.
  ** \param n last order to compute.
  ** \param first RandomAccessIterator.
  ** \param last RandomAccessIterator.
  ** \pre [first,last) is a valid range
  ** \pre (last - first) = n + 1
  ** \par Complexity: n multiplications + (n-1) substraction
  ** \par Example:
  ** \code
  ** slip::Array<double> chIIx(8);
  ** double x = 2.1;
  ** slip::eval_chebyshevII_basis(2.1,
  **                              chIIx.size()-1,
  **                              chIIx.begin(),chIIx.end());
  ** std::cout<<"chebyshevII_basis at x = "<<x<<"\n"<<chIIx<<std::endl;
  ** \endcode
  */
  template<typename T, typename RandomAccessIterator>
  inline
  void eval_chebyshevII_basis(const T& x, 
			   const std::size_t n,
			   RandomAccessIterator first, RandomAccessIterator last)
  {
    assert((last - first) == typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    if(n == 0)
      {
	first[0] = T(1);
      }
    else if (n == 1)
      {
	 first[0] = T(1);
	 first[1] = 2*x;
      }
    else
      {

	T twox = T(2) * x;
	first[0] = T(1);
	first[1] = twox;

	
	for (std::size_t j = 2; j <= n; ++j)
	  {
	    first[j] = (twox * first[j-1]) - first[j-2];
	  }
	 
      }
   
  }

  
  /*!
  ** \brief Returns the evaluations of x until order \a n to 
  **  the range [first,last) using the Discrete Chebyshev scheme.
  ** The order of the resulting values is the following
  ** (1,2x-N+1,...).
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2010/11/21
  ** \version 0.0.1
  ** \since 1.4.0
  ** \param x Location to be evaluated.
  ** \param n last order to compute.
  ** \param first RandomAccessIterator.
  ** \param last RandomAccessIterator.
  ** \pre [first,last) is a valid range
  ** \pre (last - first) = n + 1
  ** \par Complexity: 
  ** \par Example:
  ** \code
  ** slip::Array<double> chIIx(8);
  ** double x = 2.1;
  ** slip::eval_discrete_chebyshev_basis(2.1,
  **                                     chIIx.size()-1,
  **                                     chIIx.begin(),chIIx.end());
  ** std::cout<<"chebyshevII_basis at x = "<<x<<"\n"<<chIIx<<std::endl;
  ** \endcode
  */
  template<typename T, typename RandomAccessIterator>
  inline
  void eval_discrete_chebyshev_basis(const T& x, 
				     const std::size_t n,
				     RandomAccessIterator first, RandomAccessIterator last)
  {
    assert((last - first) == typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    T N =  static_cast<T>(last-first);
    if(n == 0)
      {
	first[0] = T(1);
      }
    else if (n == 1)
      {
	 first[0] = T(1);
	 first[1] = T(2)*x - N + T(1);
      }
    else
      {

	T twox = T(2)*x - N + T(1);
	first[0] = T(1);
	first[1] = twox;
	T N2 = N * N;
	
	for (std::size_t j = 2; j <= n; ++j)
	  {
	    T jf = static_cast<T>(j);
	    T jf_m_1 = jf - static_cast<T>(1);
	    T alpha = (jf_m_1 + jf_m_1 + static_cast<T>(1)) / jf;
	    T jf_m_12 = static_cast<T>(jf_m_1 * jf_m_1);
	    T beta = (static_cast<T>(jf_m_1)*(N2-jf_m_12))/jf;
	    
	    first[j] = alpha*(twox * first[j-1]) - beta*first[j-2];
	  }
	 
      }
   
  }

 /* @} */  

 /** \name Hermite polynomial algorithms */
  /* @{ */
 /*!
  ** \brief Implements the three term recurrence relation for the 
  ** Hermite polynomials, this function can be used to create 
  ** a sequence of orthogonal polynomial, and for rising k. 
  ** This recurrence relation holds for Hermite Polynomials:
  ** \f[ P_{k+1}(x) = 2xP_k(x)-2kP_{k-1}(x)\f]
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2010/11/07
  ** \version 0.0.1
  ** \since 1.4.0
  ** \param k The degree of the last polynomial calculated.
  ** \param x The x polynomial.
  ** \param Pk The value of the polynomial evaluated at degree k.
  ** \param Pkm1 The value of the polynomial evaluated at degree k-1.
  ** \return Pk+1
  ** \remarks This code is inspired from the file 
  ** boost/math/special_functions/legendre.hpp (http://www.boost.org/)
 
  ** \par Example:
  ** \code
  
  ** std::vector<slip::Polynomial<double> > hermite_basis;
  ** //creation of hermite polynomial basis
  ** slip::Polynomial<double> P02(0,1.0);
  ** slip::Polynomial<double> P12(1,2.0);
  ** P12[0] = 0.0;
  ** hermite_basis.push_back(P02);
  ** hermite_basis.push_back(P12);
  ** for(unsigned k = 1; k < 10; ++k)
  **  {
  **  hermite_basis.push_back(slip::hermite_next<slip::Polynomial<double> >(k, P1, hermite_basis[k], hermite_basis[k-1]));
  ** }
  ** for(std::size_t k = 0; k < hermite_basis.size(); ++k)
  **  {
  **    std::cout<<hermite_basis[k]<<std::endl;
  **  }
  ** \endcode
  */
 template <typename Poly, 
	   typename Poly1, 
	   typename Poly2, 
	   typename Poly3>
 inline Poly
 hermite_next(std::size_t k, Poly1 x, Poly2 Pk, Poly3 Pkm1)
  {
    typedef typename Poly::value_type T;
   
    return ((Poly(x) * Poly(Pk))* T(2)) - Poly(Pkm1)* T(2*k);
    
  }

   /*!
  ** \brief Implements the three term recurrence relation for the 
  ** Hermite multivariate polynomials, this function can be used to create 
  ** a sequence of orthogonal polynomial, and for rising k. 
  ** This recurrence relation holds for Hermite Polynomials:
  ** \f[ P_{k+1}(x) = 2xP_k(x)-2kP_{k-1}(x)\f]
  ** \since 1.4.0
  ** \param k The degree of the last polynomial calculated.
  ** \param x The x polynomial.
  ** \param Pk The value of the polynomial evaluated at degree k.
  ** \param Pkm1 The value of the polynomial evaluated at degree k-1.
  ** \remarks This code is inspired from the file 
  ** boost/math/special_functions/legendre.hpp (http://www.boost.org/)
  ** \par Example:
  ** \code
  ** //Generate x1 2d Hermite basis
  ** slip::MultivariatePolynomial<double,2> ph0;
  ** slip::Monomial<2> mh00;
  ** mh00.powers[0] = 0;
  ** mh00.powers[1] = 0;
  ** ph0.insert(mh00,1.0);
  ** slip::MultivariatePolynomial<double,2> ph1;
  ** slip::Monomial<2> mh10;
  ** mh10.powers[0] = 1;
  ** mh10.powers[1] = 0;
  ** ph1.insert(mh10,1.0);
  ** std::vector<slip::MultivariatePolynomial<double,2> > hermite_x1_basis_2d;
  ** hermite_x1_basis_2d.push_back(ph0);
  ** hermite_x1_basis_2d.push_back(ph1);
  ** for(unsigned l = 1; l < 10; ++l)
  **    {
  **	hermite_x1_basis_2d.push_back(slip::hermite_nd_next<slip::MultivariatePolynomial<double,2> >(l,ph1,hermite_x1_basis_2d[l],hermite_x1_basis_2d[l-1]));
  **    }
  **  
  **  for(unsigned l = 0; l < hermite_x1_basis_2d.size(); ++l)
  **    {
  **	std::cout<<hermite_x1_basis_2d[l]<<std::endl;
  **    }
  ** \endcode
  */
  template <typename Poly, typename Poly1, typename Poly2, typename Poly3>
  inline Poly
  hermite_nd_next(std::size_t k, Poly1 x, Poly2 Pk, Poly3 Pkm1)
  {
    typedef typename Poly::mapped_type T;
   
    return ((Poly(x) * Poly(Pk))* T(2)) - Poly(Pkm1)* T(2*k);
    
  }

/*!
  ** \brief Returns the evaluations of x until order \a n to 
  **  the range [first,last) using the Hermite scheme.
  ** The order of the resulting values is the following
  ** (1,2x,4x^2-2,8x^3-12x,...).
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2010/11/21
  ** \version 0.0.1
  ** \since 1.4.0
  ** \param x Location to be evaluated.
  ** \param n last order to compute.
  ** \param first RandomAccessIterator.
  ** \param last RandomAccessIterator.
  ** \pre [first,last) is a valid range
  ** \pre (last - first) = n + 1
  ** \par Complexity: 2*n multiplications + (n-1) substraction
  ** \par Example:
  ** \code
  ** slip::Array<double> hermitex(8);
  ** double x = 2.1;
  ** slip::eval_hermite_basis(2.1,
  **		     hermitex.size()-1,
  **		     hermitex.begin(),hermitex.end());
  ** std::cout<<"hermite_basis at x = "<<x<<"\n"<<hermitex<<std::endl;
  ** \endcode
  */
  template<typename T, typename RandomAccessIterator>
  inline
  void eval_hermite_basis(const T& x, 
			  const std::size_t n,
			  RandomAccessIterator first, RandomAccessIterator last)
  {
    assert((last - first) == typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    if(n == 0)
      {
	first[0] = T(1);
      }
    else if (n == 1)
      {
	 first[0] = T(1);
	 first[1] = 2*x;
      }
    else
      {

	T twox = T(2) * x;
	first[0] = T(1);
	first[1] = twox;

	
	for (std::size_t j = 2; j <= n; ++j)
	  {
	    first[j] = (twox * first[j-1]) - T(2*j-2)*first[j-2];
	  }
	 
      }
   
  }
/* @} */  

   /** \name Laguerre polynomial algorithms */
  /* @{ */
  /*!
  ** \brief Implements the three term recurrence relation for the 
  ** Laguerre polynomials, this function can be used to create 
  ** a sequence of orthogonal polynomial, and for rising k. 
  ** This recurrence relation holds for Laguerre Polynomials:
  ** \f[ P_{k+1}(x) = \frac{(2k+1 - x)P_k(x)-kP_{k-1}(x)}{k+1}\f]
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2010/11/07
  ** \version 0.0.1
  ** \since 1.4.0
  ** \param k The degree of the last polynomial calculated.
  ** \param x The x polynomial.
  ** \param Pk The value of the polynomial evaluated at degree k.
  ** \param Pkm1 The value of the polynomial evaluated at degree k-1.
  ** \return Pk+1
  ** \remarks This code is inspired from the file boost/math/special_functions/laguerre.hpp (http://www.boost.org/)
  ** \par Example:
  ** \code
  ** slip::MultivariatePolynomial<double,2> pla0;
  ** slip::Monomial<2> mla00;
  ** mla00.powers[0] = 0;
  ** mla00.powers[1] = 0;
  ** pla0.insert(mla00,1.0);
  ** slip::MultivariatePolynomial<double,2> pla1;
  ** pla1.insert(mla00,1.0);
  ** slip::Monomial<2> mla10;
  ** mla10.powers[0] = 1;
  ** mla10.powers[1] = 0;
  ** pla1.insert(mla10,-1.0);
  **
  ** slip::MultivariatePolynomial<double,2> plax;
  ** plax.insert(mla10,1.0);
  ** std::cout<<"plax = "<<plax<<std::endl;
  ** std::vector<slip::MultivariatePolynomial<double,2> > laguerre_x1_basis_2d;
  ** laguerre_x1_basis_2d.push_back(pla0);
  ** laguerre_x1_basis_2d.push_back(pla1);
  ** for(unsigned l = 1; l < 10; ++l)
  **    {
  **	laguerre_x1_basis_2d.push_back(slip::laguerre_nd_next<slip::MultivariatePolynomial<double,2> >(l,plax,laguerre_x1_basis_2d[l],laguerre_x1_basis_2d[l-1]));
  **      }
  **  
  ** for(unsigned l = 0; l < laguerre_x1_basis_2d.size(); ++l)
  **    {
  **	std::cout<<laguerre_x1_basis_2d[l]<<std::endl;
  **    }
  **
  ** \endcode
  */
 template <typename Poly, 
	   typename Poly1, 
	   typename Poly2, 
	   typename Poly3>
 inline Poly
 laguerre_next(std::size_t k, Poly1 x, Poly2 Pk, Poly3 Pkm1)
  {
    typedef typename Poly::value_type T;
   
    return (((T(2*k+1) - Poly(x)) * Poly(Pk)) - Poly(Pkm1)* T(k))/T(k+1);
    
  }



  /*!
  ** \brief Implements the three term recurrence relation for the 
  ** Laguerre multivariate polynomials, this function can be used to create 
  ** a sequence of orthogonal polynomial, and for rising k. 
  ** This recurrence relation holds for Laguere Polynomials:
  ** \f[ P_{k+1}(x) = \frac{(2k+1 - x)P_k(x)-kP_{k-1}(x)}{k+1}\f]
  ** \since 1.4.0
  ** \param k The degree of the last polynomial calculated.
  ** \param x The x polynomial.
  ** \param Pk The value of the polynomial evaluated at degree k.
  ** \param Pkm1 The value of the polynomial evaluated at degree k-1.
  ** \remarks This code is inspired from the file 
  ** boost/math/special_functions/legendre.hpp (http://www.boost.org/)
  ** \par Example:
  ** \code
  ** //Generate x1 2d Laguerre basis
  ** slip::MultivariatePolynomial<double,2> ph0;
  ** slip::Monomial<2> mh00;
  ** mh00.powers[0] = 0;
  ** mh00.powers[1] = 0;
  ** ph0.insert(mh00,1.0);
  ** slip::MultivariatePolynomial<double,2> ph1;
  ** slip::Monomial<2> mh10;
  ** mh10.powers[0] = 1;
  ** mh10.powers[1] = 0;
  ** ph1.insert(mh10,1.0);
  ** std::vector<slip::MultivariatePolynomial<double,2> > hermite_x1_basis_2d;
  ** hermite_x1_basis_2d.push_back(ph0);
  ** hermite_x1_basis_2d.push_back(ph1);
  ** for(unsigned l = 1; l < 10; ++l)
  **    {
  **	hermite_x1_basis_2d.push_back(slip::hermite_nd_next<slip::MultivariatePolynomial<double,2> >(l,ph1,hermite_x1_basis_2d[l],hermite_x1_basis_2d[l-1]));
  **    }
  **  
  **  for(unsigned l = 0; l < hermite_x1_basis_2d.size(); ++l)
  **    {
  **	std::cout<<hermite_x1_basis_2d[l]<<std::endl;
  **    }
  ** \endcode
  */
template <typename Poly, typename Poly1, typename Poly2, typename Poly3>
inline Poly
laguerre_nd_next(std::size_t k, Poly1 x, Poly2 Pk, Poly3 Pkm1)
  {
    typedef typename Poly::mapped_type T;
   
    return (((T(2*k+1) - Poly(x)) * Poly(Pk)) - Poly(Pkm1)* T(k))/T(k+1);
    
  }


  /*!
  ** \brief Returns the evaluations of x until order \a n to 
  **  the range [first,last) using the Laguerre scheme.
  ** The order of the resulting values is the following
  ** (1,-x+1,0.5x^2-2x+1,-(1/6)x^3-(3/2)x^2-3x+1,...).
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2010/11/21
  ** \version 0.0.1
  ** \since 1.4.0
  ** \param x Location to be evaluated.
  ** \param n last order to compute.
  ** \param first RandomAccessIterator.
  ** \param last RandomAccessIterator.
  ** \pre [first,last) is a valid range
  ** \pre (last - first) = n + 1
  ** \par Complexity: 3*n multiplications + (n-1) substractions + (n-1) additions
  ** \par Example:
  ** \code
  ** slip::Array<double> laguerrex(8);
  ** double x = 2.1;
  ** slip::eval_laguerre_basis(2.1,
  **		               laguerrex.size()-1,
  **		               laguerrex.begin(),laguerrex.end());
  ** std::cout<<"laguerre_basis at x = "<<x<<"\n"<<laguerrex<<std::endl;
  ** \endcode
  */
  template<typename T, typename RandomAccessIterator>
  inline
  void eval_laguerre_basis(const T& x, 
			   const std::size_t n,
			   RandomAccessIterator first, RandomAccessIterator last)
  {
    assert((last - first) == typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    if(n == 0)
      {
	first[0] = T(1);
      }
    else if (n == 1)
      {
	 first[0] = T(1);
	 first[1] = -x + T(1);
      }
    else
      {

	first[0] = T(1);
	first[1] = -x + T(1);

	
	for (std::size_t j = 2; j <= n; ++j)
	  {
	    first[j] = -(((x - T(2*j-1))* first[j-1]) + T(j-1)*first[j-2])/T(j);
	  }
	 
      }
   
  }
  /* @} */ 

  /** \name Polynome evaluation algorithms */
  /* @{ */
  /*!
  ** \brief Returns the evaluation of the polynomial given throw 
  **  the range [first,last) at x using the horner scheme.
  ** The coefficients of the polynomial a0 + a1x + a2x^2 +... anx^n 
  ** are supposed ordered as follows : (a0,a1,...,an).
  **  
  ** \since 1.0.0
  ** \param first RandomAccessIterator.
  ** \param last RandomAccessIterator.
  ** \param x Location to be evaluated. 
  ** \return The evaluated value.
  ** \pre [first,last) is a valid range
  ** \remarks Complexity : (last - first) Mult + (last - first) Add
  */
  template<typename T, typename RandomAccessIterator>
  inline
  T eval_horner(RandomAccessIterator first, RandomAccessIterator last, 
		const T& x)
  {
    assert(last != first);
    T s = T(0);
    if(x == T(0))
      {
	s = *first;
      }
    else if(x == T(1))
      {
	s = std::accumulate(first,last,T(0));
      }
    else if(x == T(-1))
      {
	typedef typename std::iterator_traits<RandomAccessIterator>::difference_type _Distance;
	_Distance size = (last - first);
	s = *first;
	for(_Distance i = 1; i < size; ++i)
	  {
	    if(i%2 == 0)
	      {
		s = s + first[i];
	      }
	    else
	      {
		s = s - first[i];
	      }
	  }
      }
    else
      {
	typedef typename std::iterator_traits<RandomAccessIterator>::difference_type _Distance;
	
	_Distance size = last - first;
	s = first[size - 1];
	for(_Distance i = (size - 2); i >= 0; --i)
	  {
	    s = first[i] + x * s;
	  }
      }
    return s;
  }  

  /*!
  ** \brief Returns the evaluation of the power of x until order \a n to 
  **  the range [first,last) using the horner scheme.
  **  
  ** The order of the resulting values is the following
  ** (1,x,x^2,...,x^n).
  **  
  ** \since 1.0.0
  ** \param x Location to be evaluated.
  ** \param n last order to compute.
  ** \param first RandomAccessIterator.
  ** \param last RandomAccessIterator.
  
  ** \pre [first,last) is a valid range
  ** \pre (last - first) = n + 1
  */
  template<typename T, typename RandomAccessIterator>
  inline
  void eval_power_basis(const T& x, 
			const std::size_t n,
			RandomAccessIterator first, RandomAccessIterator last)
  {
    assert((last - first) == typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    if(n == 0)
      {
	*first = T(1);
      }
    else if (n == 1)
      {
	 *first++ = T(1);
	 *first++ = x;
      }
    else
      {
	*first++ = T(1);

	if(x == T(0))
	  {
	    std::fill(first,last,T(0));
	  }
	else if (x == T(1))
	  {
	    std::fill(first,last,T(1));
	  }
	else if (x == T(-1))
	  {
	    
	    *first = T(-1);
	    std::size_t i = 0;
	    while(++first != last)
	      {
		if(i%2 == 0)
		  {
		    *first = T(1);
		  }
		else
		  {
		    *first = T(-1);
		  }
		i++;
	      }
	  }
	else
	  {
	    T pow = x;
	    *first = pow;
	    
	    while(++first != last)
	      {
		pow = pow * x;
		*first = pow;
	      }
	  }
      }
   
  }
  
 /*!
  ** \brief Returns the evaluations of x until order \a n to 
  **  the range [first,last) using the legendre scheme.
  **  
  ** The order of the resulting values is the following
  ** (1,x,0.5*(3x^2-1),...).
  **  
  ** \since 1.0.0
  ** \param x Location to be evaluated.
  ** \param n last order to compute.
  ** \param first RandomAccessIterator.
  ** \param last RandomAccessIterator.
  
  ** \pre [first,last) is a valid range
  ** \pre (last - first) = n + 1
  */
  template<typename T, typename RandomAccessIterator>
  inline
  void eval_legendre_basis(const T& x, 
			   const std::size_t n,
			   RandomAccessIterator first, RandomAccessIterator last)
  {
    assert((last - first) == typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    if(n == 0)
      {
	first[0] = T(1);
      }
    else if (n == 1)
      {
	 first[0] = T(1);
	 first[1] = x;
      }
    else
      {
	first[0] = T(1);
	first[1] = x;

	T twox = T(2) * x;
	T f2 = x;
	T d = T(1);
	T f1 = T(0);

	for (std::size_t j = 2; j <= n; ++j)
	  {
	    f1 = d++;
	    f2 += twox;
	    first[j] = (f2 * first[j-1] - f1 * first[j-2]) / d;
	  }
	
      }
   
  }

/*!
  ** \brief Returns the evaluations of x until order \a n to 
  **  the range [first,last) using the Chebyshev scheme.
  **  
  ** The order of the resulting values is the following
  ** (1,x,2x^2-1,4x^3-3x,...).
  **  
  ** \since 1.0.0
  ** \param x Location to be evaluated.
  ** \param n last order to compute.
  ** \param first RandomAccessIterator.
  ** \param last RandomAccessIterator.
  
  ** \pre [first,last) is a valid range
  ** \pre (last - first) = n + 1
  */
  template<typename T, typename RandomAccessIterator>
  inline
  void eval_chebyshev_basis(const T& x, 
			   const std::size_t n,
			   RandomAccessIterator first, RandomAccessIterator last)
  {
    assert((last - first) == typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    if(n == 0)
      {
	first[0] = T(1);
      }
    else if (n == 1)
      {
	 first[0] = T(1);
	 first[1] = x;
      }
    else
      {
	first[0] = T(1);
	first[1] = x;

	T twox = T(2) * x;
	for (std::size_t j = 2; j <= n; ++j)
	  {
	    first[j] = (twox * first[j-1]) - first[j-2];
	  }
	
      }
   
  }


 /*!
  ** \brief Returns the evaluation of the powers of X = (x1,x2) 
  **  such that \f$i_1 + i_2 \le n\f$ with  \f$i_1\f$ and
  **  \f$i_2\f$ the power of x1 and x2.
  **  the range [first,last) using the horner scheme.
  **  
  ** The order of the resulting values is the following 
  ** (1,x1,...,x1^n,x2,x2x1,...,x2^2,..).
  **  
  ** \since 1.0.0
  ** \param x Location (multivariate) to be evaluated.
  ** \param n Polynomial order to compute.
  ** \param first RandomAccessIterator.
  ** \param last RandomAccessIterator. 
  ** \pre [first,last) is a valid range
  ** \pre (last - first) = (n + 1)(n + 2)/2
  */
  template<class Vector, typename RandomAccessIterator>
  inline
  void eval_power_2d_basis(const Vector& x, 
			   const std::size_t n,
			   RandomAccessIterator first, RandomAccessIterator last)
  {
    assert(x.size() == 2);
    assert((last - first) == typename std::iterator_traits<RandomAccessIterator>::difference_type(((n + 1)*(n+2))/2)); 
    slip::Array2d<typename std::iterator_traits<RandomAccessIterator>::value_type> Eval(x.size(),n+1);
   
    for(std::size_t i = 0; i < Eval.rows(); ++i)
      {
	slip::eval_power_basis(x[i],n,Eval.row_begin(i),Eval.row_end(i));
      }

    for(std::size_t i = 0; i < (n+1); ++i)
      {
	for(std::size_t j = 0; j < ((n+1) - i); ++j)
	  {
	    *first++ = Eval[0][j] * Eval[1][i];
	  }
      }
  }


 
/*!
  ** \brief Returns the evaluation of the powers of \f$X = (x_1,...,x_{dim})\f$ 
  **  such that \f$ sum_{k=1}^{dim} i_k \le order\f$ with  \f$i_k\f$ 
  **  the power of the xk to the range [first,last) using the horner 
  **  scheme.
  **  
  ** The order of the resulting values is the lexicographical one 
  ** according to the powers of the monomial 
  ** \f$(1,x1,...,x1^{order},x2,x2x1,...,x2xk,x2^2,..x_{dim}^{order})\f$.
  **  
  ** \since 1.0.0
  ** \param x Location (multivariate) to be evaluated.
  ** \param order Polynomial order to compute.
  ** \param first RandomAccessIterator.
  ** \param last RandomAccessIterator.
  
  ** \pre [first,last) is a valid range
  ** \pre (last - first) = \f$ \frac{\prod_{i=1}^{dim} (order + i)}{dim!} \f$
  */
  template<class Vector, typename RandomAccessIterator>
  inline
  void eval_power_nd_basis(const Vector& x, 
			   const std::size_t order,
			   RandomAccessIterator first, RandomAccessIterator last)
  {
    std::size_t dim = x.size(); 
    slip::Array<std::size_t> V(dim+order);
    slip::iota(V.begin(),V.end(),1,1);
    std::size_t result_size = (std::accumulate(V.begin()+order,V.end(),1,std::multiplies<int>()))/(std::accumulate(V.begin(),V.begin()+dim,1,std::multiplies<int>()));

    assert((last - first) == typename std::iterator_traits<RandomAccessIterator>::difference_type(result_size));
    
    
    //array of all the powers combinations sort by lexicographical order
     slip::Array2d<std::size_t> powers(slip::power(order+1,dim),dim);
    for(std::size_t d = 0; d < powers.dim2(); ++d)
      {
	std::size_t step = slip::power((order+1),d);
	for(std::size_t ord = 0, ord2 = 0; ord <= (powers.dim1() - step); ord+=step,++ord2)
	  {
	    std::size_t tmp = ord2%(order+1);
	    for(std::size_t k = 0; k < step; ++k)
	      {
		powers[ord+k][d] = tmp;
	      }
	    
	  }
      }
   
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type value_type;
    //Pre-computation of the powers of x
    slip::Array2d<value_type> Eval(x.size(),order+1);
    for(std::size_t i = 0; i < Eval.rows(); ++i)
      {
	slip::eval_power_basis(x[i],order,Eval.row_begin(i),Eval.row_end(i));
      }
   
    //Evaluation of the monomials
    for(std::size_t ord = 0; ord < powers.dim1(); ++ord)
      {
	//	if(std::accumulate(powers.row_begin(ord),powers.row_end(ord),value_type()) <= order)
	if(std::accumulate(powers.row_begin(ord),powers.row_end(ord),static_cast<std::size_t>(0)) <= order)
	  {
	   
	    value_type res = value_type(1);
	    for(std::size_t d = 0; d < dim; ++d)
	      {
		res = res * Eval[d][powers[ord][d]];
	      }
	    *first++ = res;
	  }
      }


  }

 /*!
  ** \brief Returns the evaluation of the power of a slip::MultivariatePolynomial<T,DIM> P until order \a n to the range [first,last) using the horner scheme.
  **  
  ** The order of the resulting values is the following
  ** (1,P,P^2,...,P^n).
  **  
  ** \since 1.0.0
  ** \param x  slip::MultivariatePolynomial<T,DIM> to be evaluated.
  ** \param n last order to compute.
  ** \param first RandomAccessIterator.
  ** \param last RandomAccessIterator.
  ** \pre [first,last) is a valid range
  ** \pre (last - first) = n + 1
  ** \par Example:
  ** \code
  ** typedef double T;
  **  std::vector<T> coefP(6);
  ** slip::iota(coefP.begin(),coefP.end(),1.0);
  **  slip::MultivariatePolynomial<T,2> P(2,coefP.begin(),coefP.end());
  **  std::cout<<"P = \n"<<P<<std::endl;
  ** 
  **  std::vector<T> coefQ(6);
  **  slip::iota(coefQ.begin(),coefQ.end(),2.0);
  **  slip::MultivariatePolynomial<T,2> Q(2,coefQ.begin(),coefQ.end());
  **  std::cout<<"Q = \n"<<Q<<std::endl;
  ** 
  ** std::vector<slip::MultivariatePolynomial<T,2> > VP(2);
  ** VP[0] = P;
  ** VP[1] = Q;
  ** 
  **  std::vector<T> coefS(6);
  **  slip::iota(coefS.begin(),coefS.end(),3.0);
  ** slip::MultivariatePolynomial<T,2> S(2,coefS.begin(),coefS.end());
  **  std::cout<<"S = \n"<<S<<std::endl;
  **  slip::Array2d<slip::MultivariatePolynomial<T,2> > A(VP.size(),S.degree()+1);
  **  for(std::size_t i = 0; i < A.rows(); ++i)
  **  {
  **    slip::eval_multi_poly_power_basis(VP[i],S.degree(),A.row_begin(i),A.row_end(i));
  **  }
  **  std::cout<<"P^0 = \n"<<A[0][0]<<std::endl;
  ** std::cout<<"P^1 = \n"<<A[0][1]<<std::endl;
  ** std::cout<<"P^2 = \n"<<A[0][2]<<std::endl;
  ** std::cout<<"Q^0 = \n"<<A[1][0]<<std::endl;
  ** std::cout<<"Q^1  = \n"<<A[1][1]<<std::endl;
  ** std::cout<<"Q^2 = \n"<<A[1][2]<<std::endl;
  ** std::cout<<"P*Q = \n"<<A[0][1]*A[1][1]<<std::endl;
  ** \endcode
  */
  template<typename MultivariatePolynomial, 
	   typename RandomAccessIterator>
  inline
void eval_multi_poly_power_basis(const MultivariatePolynomial& x, 
				 const std::size_t n,
				 RandomAccessIterator first, 
				 RandomAccessIterator last)
  {
    assert((last - first) == typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    if(n == 0)
      {
	*first = MultivariatePolynomial(0);
      }
    else if (n == 1)
      {
	 *first++ = MultivariatePolynomial(0);
	 *first++ = x;
      }
    else
      {
	*first++ = MultivariatePolynomial(0);

	MultivariatePolynomial pow = x;
	*first = pow;
	
	while(++first != last)
	  {
	    pow = pow * x;
	    *first = pow;
	  }
	
      }
   
  }

/*!
  ** \brief Returns the evaluation of the powers of slip::MultivariatePolynomial<T,DIM> \f$Q = (P_1,...,P_{dim})\f$ 
  **  such that \f$ sum_{k=1}^{dim} i_k \le order\f$ with  \f$i_k\f$ 
  **  the power of the qk to the range [first,last) using the horner 
  **  scheme.
  **  
  ** The order of the resulting values is the lexicographical one 
  ** according to the powers of the monomial 
  ** \f$(1,P_1,...,P_1^{order},P_2,P_2P_1,...,P_2P_k,P_2^2,..P_{dim}^{order})\f$.
  **  
  ** \since 1.0.0
  ** \param x to be evaluated.
  ** \param order Polynomial order to compute.
  ** \param first RandomAccessIterator.
  ** \param last RandomAccessIterator.
  
  ** \pre [first,last) is a valid range
  ** \pre (last - first) = \f$ \frac{\prod_{i=1}^{dim} (order + i)}{dim!} \f$
  ** \par Example:
  ** \code
  ** typedef double T;
  ** std::vector<T> coefP(6);
  ** slip::iota(coefP.begin(),coefP.end(),1.0);
  ** slip::MultivariatePolynomial<T,2> P(2,coefP.begin(),coefP.end());
  ** std::cout<<"P = \n"<<P<<std::endl;
  ** std::vector<T> coefQ(6);
  ** slip::iota(coefQ.begin(),coefQ.end(),2.0);
  ** slip::MultivariatePolynomial<T,2> Q(2,coefQ.begin(),coefQ.end());
  ** std::cout<<"Q = \n"<<Q<<std::endl;
  **
  ** std::vector<slip::MultivariatePolynomial<T,2> > VP(2);
  ** VP[0] = P;
  ** VP[1] = Q;
  ** std::vector<T> coefS(6);
  ** slip::iota(coefS.begin(),coefS.end(),3.0);
  ** slip::MultivariatePolynomial<T,2> S(2,coefS.begin(),coefS.end());
  ** std::cout<<"S = \n"<<S<<std::endl;
  ** slip::MultivariatePolynomial<T,2> SrondPQ(2);
  ** SrondPQ = PoVP(S,VP);
  ** std::cout<<"S(P,Q) = \n"<<SrondPQ<<std::endl;
  ** \endcode
  */
template<class Vector, typename RandomAccessIterator>
inline
void eval_multi_poly_power_nd_basis(const Vector& x, 
				    const std::size_t order,
				    RandomAccessIterator first, RandomAccessIterator last)
  {
    std::size_t dim = x.size(); 
    slip::Array<std::size_t> V(dim+order);
    slip::iota(V.begin(),V.end(),1,1);
    std::size_t result_size = (std::accumulate(V.begin()+order,V.end(),1,std::multiplies<int>()))/(std::accumulate(V.begin(),V.begin()+dim,1,std::multiplies<int>()));

    assert((last - first) == typename std::iterator_traits<RandomAccessIterator>::difference_type(result_size));
    
    
    //array of all the powers combinations sort by lexicographical order
     slip::Array2d<std::size_t> powers(slip::power(order+1,dim),dim);
    for(std::size_t d = 0; d < powers.dim2(); ++d)
      {
	std::size_t step = slip::power((order+1),d);
	for(std::size_t ord = 0, ord2 = 0; ord <= (powers.dim1() - step); ord+=step,++ord2)
	  {
	    std::size_t tmp = ord2%(order+1);
	    for(std::size_t k = 0; k < step; ++k)
	      {
		powers[ord+k][d] = tmp;
	      }
	    
	  }
      }
   
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type value_type;
    //Pre-computation of the powers of x
    slip::Array2d<value_type> Eval(x.size(),order+1);
    for(std::size_t i = 0; i < Eval.rows(); ++i)
      {
	slip::eval_multi_poly_power_basis(x[i],order,Eval.row_begin(i),Eval.row_end(i));
      }
   
    //Evaluation of the monomials
    for(std::size_t ord = 0; ord < powers.dim1(); ++ord)
      {
	if(std::accumulate(powers.row_begin(ord),powers.row_end(ord),static_cast<std::size_t>(0)) <= order)
	  {
	   
	    value_type res = value_type(0);
	    for(std::size_t d = 0; d < dim; ++d)
	      {
		res = res * Eval[d][powers[ord][d]];
	      }
	    *first++ = res;
	  }
      }


  }


  
  template<typename T,typename RandomAccessIterator>
  struct EvalBasis 
  {
   virtual void operator()(const T& x, 
	    const std::size_t n,
	    RandomAccessIterator first, 
            RandomAccessIterator last ) const = 0;
    virtual ~EvalBasis(){} 
    
   };

 template<typename T,typename RandomAccessIterator>
 struct EvalPowerBasis: public EvalBasis<T,RandomAccessIterator>
 {
   void operator()(const T& x, 
	     const std::size_t n,
	     RandomAccessIterator first, 
	     RandomAccessIterator last ) const
    {
      slip::eval_power_basis(x,n,first,last);
    }
  
 };
   
  template<typename T,typename RandomAccessIterator>
  struct EvalLegendreBasis: public EvalBasis<T,RandomAccessIterator>
  {
    void operator()(const T& x, 
	     const std::size_t n,
	     RandomAccessIterator first, 
	     RandomAccessIterator last ) const
    {
      slip::eval_legendre_basis(x,n,first,last);
    }
  };

 template<typename T,typename RandomAccessIterator>
  struct EvalChebyshevBasis: public EvalBasis<T,RandomAccessIterator>
  {
    void operator()(const T& x, 
	     const std::size_t n,
	     RandomAccessIterator first, 
	     RandomAccessIterator last ) const
    {
      slip::eval_chebyshev_basis(x,n,first,last);
    }
  };

  template<typename T,typename RandomAccessIterator>
  struct EvalPower2dBasis: public EvalBasis<T,RandomAccessIterator>
 {
   void operator()(const T& x, 
	     const std::size_t n,
	     RandomAccessIterator first, 
	     RandomAccessIterator last ) const
    {
      slip::eval_power_2d_basis(x,n,first,last);
    }
  
 };

 template<typename T,typename RandomAccessIterator>
  struct EvalPowerNdBasis: public EvalBasis<T,RandomAccessIterator>
 {

   void operator()(const T& x, 
		   const std::size_t n,	     
		   RandomAccessIterator first, 
		   RandomAccessIterator last ) const
    {
      slip::eval_power_nd_basis(x,n,first,last);
    }

   std::size_t dim_;
  
 };

/** \name Multivariate polynomial algorithms */
/* @{ */
 /**
   ** \brief Computes the number of multivariate polynomials in polynomial base of degree less than degree.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param dim Dimension of the multivariate polynomial
   ** \param degree maximal degree of the multivariate polynomials
   ** \return the number of multivariate polynomials in the base.
   ** \par Example:
   ** \code
   ** std::cout<<"number of polynomial less than degree "<<4<<" for multivariate polynomials of dimension "<<2<<" = "<<slip::base_poly_nb_lt_degree(2,4)<<std::endl;
   **
   ** std::cout<<"number of polynomial less than degree "<<4<<" for multivariate polynomials of dimension "<<3<<" = "<<slip::base_poly_nb_lt_degree(3,4)<<std::endl;
   ** \endcode
   */
  std::size_t base_poly_nb_lt_degree(const std::size_t dim,
				     const std::size_t degree)
  {
    std::size_t prod = 1;
    for(std::size_t d = 1; d <= dim; ++d)
      {
	prod*=(degree+d);
      }
    std::size_t factDIM = 1;
    for(std::size_t n = 2; n <= dim; ++n)
      {
	factDIM*=n;
      }
       
    return prod/factDIM;
  }

  /**
   ** \brief Computes the all the powers indices of a multivariate polynomial of dimension \a dim which maximal degree of each variable is of maximal degree \a degree sorted by lexicographical order.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param dim dimension of the multivariate polynomial.
   ** \param degree maximal degree of each variable of the multivariate polynomial.
   ** \param powers Container2d containing all the powers indices
   ** \par Example:
   ** \code
   ** const std::size_t dim_powers = 3;
   ** const std::size_t degree_powers = 4;
   ** slip::Matrix<std::size_t> powers_matrix;
   ** slip::lexicographic_powers_matrix(dim_powers,degree_powers,powers_matrix);
   ** std::cout<<"powers_matrix = \n"<<powers_matrix<<std::endl;
   ** const std::size_t degree_powers2 = 3;
   ** slip::Matrix<std::size_t> powers_matrix2;
   ** slip::lexicographic_powers_matrix(dim_powers,degree_powers2,powers_matrix2);
   ** std::cout<<"powers_matrix2 = \n"<<powers_matrix2<<std::endl;
   ** \endcode
   */
  template <typename Container2d>
  void lexicographic_powers_matrix(const std::size_t dim,
				   const std::size_t order,
				   Container2d& powers)
  {
    powers.resize(slip::power(order+1,dim),dim);
    for(std::size_t d = 0; d < powers.dim2(); ++d)
      {
	std::size_t step = slip::power((order+1),d);
	for(std::size_t ord = 0, ord2 = 0; ord <= (powers.dim1() - step); ord+=step,++ord2)
	  {
	    std::size_t tmp = ord2%(order+1);
	    for(std::size_t k = 0; k < step; ++k)
	      {
		powers[ord+k][d] = tmp;
	      }
	    
	  }
      }
  }

/**
 ** \brief Computes the all the powers indices of a multivariate polynomial of dimension \a dim which maximal degree of each variable is given by a range [order_first, order_last) sorted by lexicographical order.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
 ** \date 2023/11/22
 ** \since 1.5.0
 ** \version 0.0.1
 ** \param dim dimension of the multivariate polynomial.
 ** \param order_first RandomAccessIterator to the first variables' maximal degrees range.
 ** \param order_last RandomAccessIterator to one past the end variables' maximal degrees range.
 ** \param powers Container2d containing all the powers indices
 ** \par Example:
 ** \code
 ** const std::size_t dim_powers = 3;
 ** slip::Matrix<std::size_t> powers_degree_matrix_ind;
 ** slip::Vector<std::size_t> orders(3);
 ** orders[0] = 3;
 ** orders[1] = 3;
 ** orders[2] = 2;
 ** std::cout<<"orders = "<<orders<<std::endl;
 ** slip::lexicographic_powers_matrix(dim_powers, orders.begin(), orders.end(),powers_degree_matrix_ind);
 ** std::cout<<"powers_degree_matrix_ind = \n"<<powers_degree_matrix_ind<<std::endl;
 ** \endcode
 */
template <typename Container2d,
	  typename RandomAccessIterator>
void lexicographic_powers_matrix(const std::size_t dim,
				 RandomAccessIterator order_first,
				 RandomAccessIterator order_last,
				 Container2d& powers)
{

    assert(static_cast<std::size_t>(order_last- order_first) == dim);
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type value_type;
    //matrix of dimension (product of all degrees) x dim
    slip::Array<value_type> degrees(dim,order_first,order_last);
    slip::plus_scalar(degrees.begin(),degrees.end(),slip::constants<value_type>::one(),degrees.begin());
    powers.resize(std::accumulate(degrees.begin(), degrees.end(), static_cast<value_type>(1), std::multiplies<value_type>()),dim);
    for(std::size_t d = 0, step = 1; d < powers.dim2(); ++d,++order_first)
      {
	//std::size_t step = slip::power((order+1),d);
	
	for(std::size_t ord = 0, ord2 = 0; ord <= (powers.dim1() - step); ord+=step,++ord2)
	  {
	    std::size_t tmp = ord2%(*order_first+1);
	    for(std::size_t k = 0; k < step; ++k)
	      {
		powers[ord+k][d] = tmp;
	      }
	    
	  }
	step *= (*order_first + 1);
      }
  }

/**
 ** \brief Computes the all the powers indices of a multivariate polynomial of dimension \a dim which maximal degree of each variable is given by a range [order_first, order_last) according to a maximal total order \a max_order sorted by lexicographical order.
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2023/11/22
 ** \since 1.5.0
 ** \version 0.0.1
 ** \param dim dimension of the multivariate polynomial.
 ** \param order_first RandomAccessIterator to the first variables' maximal degrees range.
 ** \param order_last RandomAccessIterator to one past the end variables' maximal degrees range.
 ** \param max_order total maximal order of the multivariate polynomial.
 ** \param powers Container2d containing all the powers indices
 ** \par Example:
 ** \code
 ** const std::size_t dim_powers = 3;
 ** slip::Vector<std::size_t> orders(3);
 ** orders[0] = 3;
 ** orders[1] = 3;
 ** orders[2] = 2;
 ** std::cout<<"orders = "<<orders<<std::endl;
 ** std::size_t max_order = 3;
 ** slip::Matrix<std::size_t> powers_degree_matrix_ind_lt;
 ** slip::lexicographic_powers_matrix(dim_powers, orders.begin(), orders.end(),
 **				      max_order,
 **				      powers_degree_matrix_ind_lt);
 ** std::cout<<"powers_degree_matrix_ind_lt = \n"<<powers_degree_matrix_ind_lt<<std::endl;
 ** \endcode
 */
  template <typename Container2d,
	    typename RandomAccessIterator>
  void lexicographic_powers_matrix(const std::size_t dim,
				   RandomAccessIterator order_first,
				   RandomAccessIterator order_last,
				   const std::size_t max_order,
				   Container2d& powers)
  {

    assert(static_cast<std::size_t>(order_last- order_first) == dim);
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type value_type;
    //matrix of dimension (product of all degrees) x dim
    slip::Array<value_type> degrees(dim,order_first,order_last);
    slip::plus_scalar(degrees.begin(),degrees.end(),slip::constants<value_type>::one(),degrees.begin());
    Container2d powers_tmp(std::accumulate(degrees.begin(), degrees.end(), static_cast<value_type>(1), std::multiplies<value_type>()),dim);
    for(std::size_t d = 0, step = 1; d < powers_tmp.dim2(); ++d,++order_first)
      {
	//std::size_t step = slip::power((order+1),d);
	
	for(std::size_t ord = 0, ord2 = 0; ord <= (powers_tmp.dim1() - step); ord+=step,++ord2)
	  {
	    std::size_t tmp = ord2%(*order_first+1);
	    for(std::size_t k = 0; k < step; ++k)
	      {
		powers_tmp[ord+k][d] = tmp;
	      }
	    
	  }
	step *= (*order_first + 1);
      }
    std::size_t nb_monomes = 0;
    for(std::size_t i = 0; i < powers_tmp.rows(); ++i)
      {
	std::size_t s = std::accumulate(powers_tmp.row_begin(i),
					powers_tmp.row_end(i),0);
	if(s <= max_order)
	  {
	    nb_monomes +=1;
	  }
      }
    powers.resize(nb_monomes,dim);
    for(std::size_t i = 0, k = 0; i < powers_tmp.rows(); ++i)
      {
	std::size_t s = std::accumulate(powers_tmp.row_begin(i),
					powers_tmp.row_end(i),0);
	if(s <= max_order)
	  {
	    std::copy(powers_tmp.row_begin(i),powers_tmp.row_end(i),
		      powers.row_begin(k));
	    k+=1;
	  }

      }
    
  }

/**
   ** \brief Computes the all the powers indices of a multivariate polynomial of degree less than \a degree and dimension \a dim sorted by lexicographical order.
   ** \param dim dimension of the multivariate polynomial.
   ** \param degree maximal degree of the multivariate polynomial.
   ** \param powers_lt_degree Container2d containing all the powers indices
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr> 
   ** \date 2023/11/22
   ** \since 1.5.0
   ** \version 0.0.1
   ** \par Example:
   ** \code
   ** const std::size_t dim_powers = 3;
   ** const std::size_t degree_powers = 4;
   ** slip::Matrix<std::size_t> powers_lt_degree_matrix;
   ** slip::lexicographic_powers_matrix_lt_degree(dim_powers,degree_powers,powers_lt_degree_matrix);
   ** std::cout<<"powers_lt_degree_matrix = \n"<<powers_lt_degree_matrix<<std::endl;
   ** \endcode
   */
  template <typename Container2d>
  void lexicographic_powers_matrix_lt_degree(const std::size_t dim,
					     const std::size_t degree,
					     Container2d& powers_lt_degree)
  {
    Container2d powers;
    slip::lexicographic_powers_matrix(dim,degree,powers);
    powers_lt_degree.resize(slip::base_poly_nb_lt_degree(dim,degree),dim);
    for(std::size_t i = 0, ii = 0; i < powers.rows(); ++i)
      {
    	std::size_t sum_degree = std::accumulate(powers.row_begin(i),powers.row_end(i),std::size_t(0));
    	if(sum_degree <= degree)
    	  {
	    std::copy(powers.row_begin(i),powers.row_end(i),
	    	      powers_lt_degree.row_begin(ii));
    	    ++ii;
    	  }
      }
  }

/* @} */

/** \name Vandermonde matrices algorithms */
/* @{ */

/**
   ** \brief Computes Vandermonde matrix \f$V_k\f$ of order k from a Data sequence \f$(x_0,\cdots,x_{n-1})\f$ where each element provides the coordinates x to evaluate
   ** \f[ 
   ** V_k = 
   ** \left(
   ** \begin{array}{ccccc}  
   ** 1&x_0&x_0^2&\cdots&x_0^{k}\\
   ** \vdots&\vdots&\vdots&\vdots\\
   ** 1&x_i&x_i^2&\cdots&x_0^{k}\\
   ** \vdots&\vdots&\vdots&\vdots\\
   ** 1&x_{n-1}&x_{n-1}^2&\cdots&x_{n-1}^{k}\\
   ** \end{array}\right) 
   ** \f].
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2023/11/22
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param x_first RandomAccessIterator to the first data.
   ** \param x_last RandomAccessIterator to one-past-the-end data.
   ** \param order Order of the polynomial.
   ** \param Vander The nd Vandermonde matrix.
   ** \param transpose If true transpose de Vandermonde matrix.
   ** \par Example:
   ** \code
   ** typedef double T;
   ** slip::Vector<T> X(3);
   ** X[0] = 2.0;
   ** X[1] = 3.0;
   ** X[2] = 4.0; 
   ** slip::Matrix<T> Vander3;
   ** slip::vandermonde(X.begin(), X.end(), order, Vander3);
   ** std::cout<<"Vander3 = \n" <<Vander3<<std::endl;
   ** slip::Matrix<T> Vander4;
   ** slip::vandermonde(X.begin(), X.end(), order, Vander4, true);
   ** std::cout<<"Vander4 = \n" <<Vander4<<std::endl;
   ** \endcode
   */
template<typename RandomAccessIterator,
	 typename Matrix1>
void vandermonde(RandomAccessIterator x_first,
		 RandomAccessIterator x_last,
		 const std::size_t order,
		 Matrix1& Vander,
		 const bool transpose = false)
{
  
  const std::size_t data_size = static_cast<std::size_t>(x_last-x_first);
  
  if(transpose)
    {
      Vander.resize(order+1,data_size);
      RandomAccessIterator it_first = x_first;
      for(std::size_t i = 0; it_first != x_last; ++it_first, ++i)
	{
	  slip::eval_power_basis(*it_first,order,Vander.col_begin(i),Vander.col_end(i));
	}
    }
 else
   {
     Vander.resize(data_size,order+1);
     RandomAccessIterator it_first = x_first;
     for(std::size_t i = 0; it_first != x_last; ++it_first, ++i)
       {
	 slip::eval_power_basis(*it_first,order,Vander.row_begin(i),Vander.row_end(i));
       }
   }
}



/**
   ** \brief Computes nd Vandermonde matrix from a Data matrix where
   ** each row provide the coordinates \f$(x^1_i x^2_i ... x^dim_i)\f$ to evaluate
   ** \f[ 
   ** Data = 
   ** \left(
   ** \begin{array}{cccc}  
   ** x^1_0&x^2_0&\cdots&x^{dim}_0\\
   ** \vdots&\vdots&\vdots&\vdots\\
   ** x^1_n&x^2_n&\cdots&x^{dim}_n\\
   ** \end{array}\right) 
   ** \f].
   ** each row i of \f$V_k\f$ contains the evaluation of the lexicographic ordered cononical polynomial base at \f$(x^1_i x^2_i ... x^dim_i)\f$.
   ** \param Data data matrix.
   ** \param dim Dimention of the data.
   ** \param order Order of the lexicographic ordered cononical polynomial base.
   ** \param Vander The nd Vandermonde matrix.
   ** \param transpose If true transpose de Vandermonde matrix.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2023/11/22
   ** \version 0.0.1
   ** \since 1.5.0
   ** \par Example:
   ** \code 
   ** typedef double T;
   ** slip::Matrix<T> Data(3, 3);
   ** Data[0][0] = 2.0;  Data[0][1] = 3.0; Data[0][2] = 4.0;
   ** Data[1][0] = 3.0;  Data[1][1] = 4.0; Data[1][2] = 5.0;
   ** Data[2][0] = 4.0;  Data[2][1] = 5.0; Data[2][2] = 6.0;
   ** slip::Matrix<T> Vander;
   ** const std::size_t order = 3;
   ** const std::size_t dim = 3;
   ** slip::vandermonde_nd(Data,dim,order,Vander);
   ** //evaluate 1 x x^2 x^3 y xy x^2y y^2 xy^2 y^3 z xz x^2z yz xyz y^2z z^2 xz^2 z^3 
   ** //for each row of Data : x_i y_i z_i
   ** std::cout<<"Data = \n"<<Data<<std::endl;
   ** std::cout<<"dim = "<<dim<<std::endl;
   ** std::cout<<"order = "<<order<<std::endl;
   ** std::cout<<"Vander = \n" <<Vander<<std::endl;
   ** slip::Matrix<T> Vander2;
   ** slip::vandermonde_nd(Data,dim,order,Vander2, true);
   ** std::cout<<"Vander2 = \n" <<Vander2<<std::endl;
   ** \endcode
   */

template<typename Matrix1,
	 typename Matrix2>
void vandermonde_nd(const Matrix1& Data,
		    const std::size_t dim,
		    const std::size_t order,
		    
		    Matrix2& Vander,
		    const bool transpose = false)
{
  
  std::size_t Vcols = order + 1;
  std::size_t fac_dim = 1;
  for(std::size_t k = 2; k <= dim; ++k)
    {
      fac_dim *=k;
      Vcols *= (order + k);
    }
  Vcols = Vcols/fac_dim;
   
  const std::size_t Data_rows = Data.rows();
 
  slip::Vector<typename Matrix1::value_type> Data_row(dim);
  if(transpose)
    {
      Vander.resize(Vcols,Data_rows);
      for(std::size_t i = 0; i < Data_rows; ++i)
	{
	  std::copy(Data.row_begin(i), Data.row_end(i), Data_row.begin());
	  slip::eval_power_nd_basis(Data_row,order,Vander.col_begin(i),Vander.col_end(i));
	}
    }
  else
    {
      Vander.resize(Data_rows, Vcols);
      for(std::size_t i = 0; i < Data_rows; ++i)
	{
	  std::copy(Data.row_begin(i), Data.row_end(i), Data_row.begin());
	  slip::eval_power_nd_basis(Data_row,order,Vander.row_begin(i),Vander.row_end(i));
	}
    }
}
/**
   ** \brief Computes nd Vandermonde matrix from a Data matrix where
   ** each row provide the coordinates \f$(x^1_i x^2_i ... x^dim_i)\f$ to evaluate
   ** \f[ 
   ** Data = 
   ** \left(
   ** \begin{array}{cccc}  
   ** x^1_0&x^2_0&\cdots&x^{dim}_0\\
   ** \vdots&\vdots&\vdots&\vdots\\
   ** x^1_n&x^2_n&\cdots&x^{dim}_n\\
   ** \end{array}\right) 
   ** \f].
   ** each row i of \f$V_k\f$ contains the evaluation of the lexicographic ordered cononical polynomial base at \f$(x^1_i x^2_i ... x^dim_i)\f$.
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2023/11/22
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param Data data matrix.
   ** \param dim Dimention of the data.
   ** \param order_first RandomAccessIterator to the first variables' maximal degrees range.
   ** \param order_last RandomAccessIterator to one past the end variables' maximal degrees range.
   ** \param Vander The nd Vandermonde matrix.
   ** \param transpose If true transpose de Vandermonde matrix.
   ** \par Example:
   ** \code 
   ** slip::Vector<std::size_t> orders(3);
   ** orders[0] = 3;
   ** orders[1] = 3;
   ** orders[2] = 2;
   ** std::cout<<"orders = "<<orders<<std::endl;
   ** std::size_t order = 3;
   ** std::cout<<"order = "<<order<<std::endl;
   ** std::size_t dim = 3;
   ** std::cout<<"dim = "<<dim<<std::endl;
   ** slip::Matrix<T> Vander5;
   ** slip::Matrix<T> Data(3, 3);
   ** Data[0][0] = 2.0;  Data[0][1] = 3.0; Data[0][2] = 4.0;
   ** Data[1][0] = 3.0;  Data[1][1] = 4.0; Data[1][2] = 5.0;
   ** Data[2][0] = 4.0;  Data[2][1] = 5.0; Data[2][2] = 6.0;
   ** std::cout<<"Data = \n"<<Data<<std::endl;
   ** slip::vandermonde_nd(Data,dim,orders.begin(),orders.end(),order,Vander5);
   ** std::cout<<"Vander5 = \n" <<Vander5<<std::endl;
   ** \endcode
   */
template<typename Matrix1,
	 typename RandomAccessIterator,
	 typename Matrix2>
void vandermonde_nd(const Matrix1& Data,
		    const std::size_t dim,
		    RandomAccessIterator order_first,
		    RandomAccessIterator order_last,
		    const std::size_t max_order,
		    Matrix2& Vander,
		    const bool transpose = false)
{
 const std::size_t Data_rows = Data.rows();
 slip::Array2d<std::size_t> powers_degree_matrix;
 slip::lexicographic_powers_matrix(dim,
				   order_first, order_last,
				   max_order,
				   powers_degree_matrix);

 std::size_t max_orders = *std::max_element(order_first, order_last);
 //pre-computation of the variables' powers
 typedef typename Matrix1::value_type T;
 Matrix1 values(dim,max_orders+1, T());
 
 const std::size_t Vcols = powers_degree_matrix.rows();
 //std::cout<<values[0][powers_degree_matrix[0]]<<std::endl;
 //std::cout<<values[0][0]<<std::endl;
 slip::Vector<T> Data_row(dim); 
  if(transpose)
    {
      Vander.resize(Vcols,Data_rows);
       for(std::size_t i = 0; i < Data_rows; ++i)
	{
	  std::copy(Data.row_begin(i), Data.row_end(i), Data_row.begin());
	  //pre-computation of the variables' powers
	  //1 Data_row[d] Data_row[d]^2 ... Data_row[d]^orderd
	  //with orderd the order of the coordinate d 
	  for(std::size_t d = 0; d < dim; ++d)
	    {
	      slip::eval_power_basis(Data_row[d],max_orders,values.row_begin(d),values.row_end(d));
	    }
	  typename Matrix2::col_iterator monomials_first = Vander.col_begin(i);
	  typename Matrix2::col_iterator monomials_last = Vander.col_end(i);
	  //for all monomials in lexicographical order
	  for(std::size_t o = 0; o < powers_degree_matrix.rows(); ++o, ++monomials_first)
	    {
	      //evaluate a monomial
	      T val_monomial = values[0][powers_degree_matrix[o][0]];
	      for(std::size_t d = 1; d < dim; ++d)
		{
		  val_monomial *= values[d][powers_degree_matrix[o][d]];
		}
	      //copy monomial in row i and column o of the Vandermonde matrix
	      *monomials_first = val_monomial;
	    }
	}
    }
  else
    {
      Vander.resize(Data_rows, Vcols);
      for(std::size_t i = 0; i < Data_rows; ++i)
	{
	  std::copy(Data.row_begin(i), Data.row_end(i), Data_row.begin());
	  //pre-computation of the variables' powers
	  //1 Data_row[d] Data_row[d]^2 ... Data_row[d]^orderd
	  //with orderd the order of the coordinate d 
	  for(std::size_t d = 0; d < dim; ++d)
	    {
	      slip::eval_power_basis(Data_row[d],max_orders,values.row_begin(d),values.row_end(d));
	    }
	  typename Matrix2::row_iterator monomials_first = Vander.row_begin(i);
	  typename Matrix2::row_iterator monomials_last = Vander.row_end(i);
	  //for all monomials in lexicographical order
	  for(std::size_t o = 0; o < powers_degree_matrix.rows(); ++o, ++monomials_first)
	    {
	      //evaluate a monomial
	      T val_monomial = values[0][powers_degree_matrix[o][0]];
	      for(std::size_t d = 1; d < dim; ++d)
		{
		  val_monomial *= values[d][powers_degree_matrix[o][d]];
		}
	      //copy monomial in row i and column o of the Vandermonde matrix
	      *monomials_first = val_monomial;
	    }
	}
    }
  
  
}

 template<typename Type>
  inline Type eval_poly(const slip::Vector<Type> &coeffs,
			const std::size_t &Xdeg,
			const std::size_t &Ydeg,
			const std::size_t &Zdeg,
			const slip::Point3d<Type> &pt)
  {
    int i=(Xdeg+1)*(Ydeg+1)*(Zdeg+1)-1;
    Type res=static_cast<Type>(0);
    for (int m=Xdeg; m>=0; --m)
      {
        res*=pt[0];
        Type c=static_cast<Type>(0);
        for (int n=Ydeg; n>=0; --n)
          {
            c*=pt[1];
            Type b=static_cast<Type>(0);
            for (int q=Zdeg; q>=0; --q)
              {
                b*=pt[2];
                b+=coeffs(i);
                --i;
              }
            c+=b;
          }
        res+=c;
      }
    return res;
  }


 template<typename Type>
  inline Type eval_poly_Xderivative(const slip::Vector<Type> &coeffs,
				    const std::size_t &Xdeg,
				    const std::size_t &Ydeg,
				    const std::size_t &Zdeg,
				    const slip::Point3d<Type> &pt)
  {
    int i=(Xdeg+1)*(Ydeg+1)*(Zdeg+1)-1;
    Type res=static_cast<Type>(0);
    for (int m=Xdeg; m>=0; --m)
      {
        if (m>0)
          {
            res*=pt[0];
          }
        Type c=static_cast<Type>(0);
        for (int n=Ydeg; n>=0; --n)
          {
            c*=pt[1];
            Type b=static_cast<Type>(0);
            for (int q=Zdeg; q>=0; --q)
              {
                b*=pt[2];
                b+=coeffs(i);
                --i;
              }
            c+=b;
          }
        res+=static_cast<Type>(m)*c;
      }
    return res;
  }

  template<typename Type>
  inline Type eval_poly_Yderivative(const slip::Vector<Type> &coeffs,
				    const std::size_t &Xdeg,
				    const std::size_t &Ydeg,
				    const std::size_t &Zdeg,
				    const slip::Point3d<Type> &pt)
  {
    int i=(Xdeg+1)*(Ydeg+1)*(Zdeg+1)-1;
    Type res=static_cast<Type>(0);
    for (int m=Xdeg; m>=0; --m)
      {
        res*=pt[0];
        Type c=static_cast<Type>(0);
        for (int n=Ydeg; n>=0; --n)
          {
            if (n>0)
              {
                c*=pt[1];
              }
            Type b=static_cast<Type>(0);
            for (int q=Zdeg; q>=0; --q)
              {
                b*=pt[2];
                b+=coeffs(i);
                --i;
              }
            c+=static_cast<Type>(n)*b;
          }
        res+=c;
      }
    return res;
  }

  template<typename Type>
  inline Type eval_poly_Zderivative(const slip::Vector<Type> &coeffs,
				    const std::size_t &Xdeg,
				    const std::size_t &Ydeg,
				    const std::size_t &Zdeg,
				    const slip::Point3d<Type> &pt)
  {
    int i=(Xdeg+1)*(Ydeg+1)*(Zdeg+1)-1;
    Type res=static_cast<Type>(0);
    for (int m=Xdeg; m>=0; --m)
      {
            res*=pt[0];
            Type c=static_cast<Type>(0);
            for (int n=Ydeg; n>=0; --n)
              {
                c*=pt[1];
                Type b=static_cast<Type>(0);
                for (int q=Zdeg; q>=0; --q)
                  {
                    if (q>0)
                      {
                        b*=pt[2];
                        b+=static_cast<Type>(q)*coeffs(i);
                      }
                    --i;
                  }
                c+=b;
              }
            res+=c;
      }
    return res;
  }


  template<typename Type>
  void fit_poly(slip::Vector<Type> &coeffs,
		const std::size_t &Xdeg,
		const std::size_t &Ydeg,
		const std::size_t &Zdeg,
		const slip::Matrix<Type> &data,
		const slip::Vector<std::size_t> &datavars)
  {
    std::size_t nc=(Xdeg+1)*(Ydeg+1)*(Zdeg+1);
    coeffs.resize(nc);
    slip::Matrix<long double> A(nc,nc,0.);
    slip::Vector<long double> B(nc,0.);

    //std::size_t nd=data.size();
    std::size_t nd=data.dim1();
    for (std::size_t d=0; d<nd; ++d)
      {
        Type X=data[d][datavars[0]];
        Type Y=data[d][datavars[1]];
        Type Z=data[d][datavars[2]];
        Type varval;
        varval=data[d][datavars[3]];

        // computing stats
        std::size_t i=0;
        long double val1X=static_cast<long double>(1);
        for (std::size_t m=0; m<=Xdeg; ++m)
          {
            long double val1Y=static_cast<long double>(1);
            for (std::size_t n=0; n<=Ydeg; ++n)
              {
                long double val1Z=static_cast<long double>(1);
                for (std::size_t q=0; q<=Zdeg; ++q)
                  {
                    long double val1=val1X*val1Y*val1Z;
                    B(i)+=varval*val1/static_cast<long double>(nd);
                    // ------
                    std::size_t j=0;
                    long double val2X=val1X;
                    for (std::size_t mm=0; mm<=Xdeg; ++mm)
                      {
                        long double val2Y=val1Y;
                        for (std::size_t nn=0; nn<=Ydeg; ++nn)
                          {
                            long double val2Z=val1Z;
                            for (std::size_t qq=0; qq<=Zdeg; ++qq)
                              {
                                long double val2=val2X*val2Y*val2Z;
                                A(i,j)+=val2/static_cast<long double>(nd);
                                ++j;
                                val2Z*=Z;
                              }
                            val2Y*=Y;
                          }
                        val2X*=X;
                      }
                    // ------
                    ++i;
                    val1Z*=Z;
                  }
                val1Y*=Y;
              }
            val1X*=X;
          }
      }

    //std::cerr << "A: " << A << std::endl;
    //std::cerr << "B: " << B << std::endl;

    // choleski method
    slip::cholesky_solve(A,coeffs,B);
    // long double coeff;
    // for (std::size_t k=0; k<nc; ++k)
    //   {
    //     for (std::size_t l=k+1; l<nc; ++l)
    //       {
    //         coeff=A(l,k);
    //         for (std::size_t p=0; p<k+1; ++p)
    //           A(l,p)=0.;
    //         for (std::size_t p=k+1; p<nc; ++p)
    //           A(l,p)-=A(k,p)*coeff/A(k,k);
    //         B(l)-=B(k)*coeff/A(k,k);
    //       }
    //   }
    // for (int l=nc-1; l>=0; l--)
    //   {
    //     coeffs(l)=B(l);
    //     for (std::size_t k=l+1; k<nc; ++k)
    //       coeffs(l)-=coeffs(k)*A(l,k);
    //     coeffs(l)/=A(l,l);
    //   }
    //std::cerr << "coeffs: " << coeffs << std::endl;
  }


/* @} */

//OK !
template<typename RandomAccessIterator1,
	 typename RandomAccessIterator2>
void legendre_jacobi_coefficients(const std::size_t order,
				  RandomAccessIterator1 alpha_first,     
				  RandomAccessIterator1 alpha_last,
				  RandomAccessIterator2 beta_first,     
				  RandomAccessIterator2 beta_last)				 
{
  assert(static_cast<std::size_t>(alpha_last - alpha_first) == (order + 1));
  assert(static_cast<std::size_t>(beta_last - beta_first) == (order + 1));
  typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
  std::fill(alpha_first,alpha_last,T());
  *beta_first++ = static_cast<T>(2.0);
  const T one = static_cast<T>(1.0);
  const T four = static_cast<T>(4.0);
  for(int k = 1;beta_first != beta_last; ++beta_first, ++k)
    {
      *beta_first = one / (four - one/static_cast<T>(k*k));
    }
  
}

//OK !
template<typename RandomAccessIterator1,
	 typename RandomAccessIterator2>
void chebyshev_jacobi_coefficients(const std::size_t order,
				   RandomAccessIterator1 alpha_first,     
				   RandomAccessIterator1 alpha_last,
				   RandomAccessIterator2 beta_first,     
				   RandomAccessIterator2 beta_last)				 
{
  assert(static_cast<std::size_t>(alpha_last - alpha_first) == (order + 1));
  assert(static_cast<std::size_t>(beta_last - beta_first) == (order + 1));
  typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
  std::fill(alpha_first,alpha_last,T());
  *beta_first++ = slip::constants<T>::pi();
  *beta_first++ = slip::constants<T>::half();
  std::fill(beta_first,beta_last,static_cast<T>(0.25L));
  
}

//OK !
template<typename RandomAccessIterator1,
	 typename RandomAccessIterator2>
void laguerre_jacobi_coefficients(const std::size_t order,
				   RandomAccessIterator1 alpha_first,     
				   RandomAccessIterator1 alpha_last,
				   RandomAccessIterator2 beta_first,     
				   RandomAccessIterator2 beta_last)				 
{
  assert(static_cast<std::size_t>(alpha_last - alpha_first) == (order + 1));
  assert(static_cast<std::size_t>(beta_last - beta_first) == (order + 1));
  typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
  *alpha_first++ = slip::constants<T>::one();
  *beta_first++  = slip::constants<T>::one();
  for(std::size_t k = 1; k <= order; ++k, ++alpha_first, ++beta_first)
    {
      *alpha_first = static_cast<T>(2*k+1);
      *beta_first  = static_cast<T>(k*k);
    }
  
}

template<typename RandomAccessIterator1,
	 typename RandomAccessIterator2>
void hermite_jacobi_coefficients(const std::size_t order,
				 RandomAccessIterator1 alpha_first,     
				 RandomAccessIterator1 alpha_last,
				 RandomAccessIterator2 beta_first,     
				 RandomAccessIterator2 beta_last)				 
{
  assert(static_cast<std::size_t>(alpha_last - alpha_first) == (order + 1));
  assert(static_cast<std::size_t>(beta_last - beta_first) == (order + 1));
  typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
  std::fill(alpha_first,alpha_last,T());
  *beta_first++ = std::sqrt(slip::constants<T>::pi());
  T half =  slip::constants<T>::half();
  for(int k = 1;beta_first!=beta_last; ++beta_first,++k)
    {
      *beta_first =  half * static_cast<T>(k);
    }
}


  //equivalent to Legendre inner-product
  //OK !
template<typename RandomAccessIterator1,
	 typename RandomAccessIterator2>
void discrete_chebychev_jacobi_coefficients(const std::size_t order,
					    const std::size_t N,
					    RandomAccessIterator1 alpha_first,     
					    RandomAccessIterator1 alpha_last,
					    RandomAccessIterator2 beta_first,     
					    RandomAccessIterator2 beta_last)				 
{
   assert(static_cast<std::size_t>(alpha_last - alpha_first) == (order + 1));
   assert(static_cast<std::size_t>(beta_last - beta_first) == (order + 1));
   assert((order+1) < N);
   typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
   T alphan = static_cast<T>((N-1))/static_cast<T>(2);
   std::fill(alpha_first,alpha_last,alphan);
   *beta_first++ = static_cast<T>(N);
   T N2 = static_cast<T>(N)*static_cast<T>(N);
   T N2_o_4 = N2/static_cast<T>(4);
   T four = static_cast<T>(4);
   T one = slip::constants<T>::one();
   for(int k = 1;beta_first!=beta_last; ++beta_first,++k)
    {
      T k2 = static_cast<T>(k)*static_cast<T>(k);
      *beta_first = N2_o_4 * ((one-(k2/N2))/ (four - (one/k2)));
    }
}


template<typename RandomAccessIterator1,
	 typename RandomAccessIterator2>
void discrete_krawtchouk_jacobi_coefficients(const std::size_t order,
					     const std::size_t N,
					     const typename std::iterator_traits<RandomAccessIterator1>::value_type& p,
					    RandomAccessIterator1 alpha_first,     
					    RandomAccessIterator1 alpha_last,
					    RandomAccessIterator2 beta_first,     
					    RandomAccessIterator2 beta_last)				 
{
   assert(static_cast<std::size_t>(alpha_last - alpha_first) == (order + 1));
   assert(static_cast<std::size_t>(beta_last - beta_first) == (order + 1));
   assert((order+1) < N);
   typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
   *alpha_first++ = p*static_cast<T>(N);
   *beta_first++ = slip::constants<T>::one();
  
   T p_1_m_p = p*(1-p);
   for(int k = 1;beta_first!=beta_last; ++beta_first,++k)
    {
      T n = static_cast<T>(k);
      *alpha_first = n+p*(static_cast<T>(N)-slip::constants<T>::two()*n);
      *beta_first = p_1_m_p*k*(N-n+slip::constants<T>::one());
    }
}

template<typename RandomAccessIterator1, 
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator3,
	 typename T>
inline
T
discrete_poly_inner_product(RandomAccessIterator1 poly1_first,
			    RandomAccessIterator1 poly1_last,
			    RandomAccessIterator2 poly2_first,
			    RandomAccessIterator2 poly2_last,
			    RandomAccessIterator3 weight_first,
			    RandomAccessIterator3 weight_last,
			    T init)
{
  assert((poly1_last - poly1_first) == (poly2_last - poly2_first));
  assert((poly1_last - poly1_first) == (weight_last - weight_first));

  for (; poly1_first != poly1_last; ++poly1_first,++poly2_first, ++weight_first)
	init = init + (*poly1_first * *poly2_first * *weight_first);
   return init;
}

template <typename Poly, 
	  typename Poly1, 
	  typename Poly2, 
	  typename Poly3>
 inline Poly
stieltjes_next(const typename Poly::value_type& alpha, 
	       const typename Poly::value_type& beta, 
	       Poly1 x, Poly2 Pk, Poly3 Pkm1)
  {
    typedef typename Poly::value_type T;

    return ((Poly(x) * Poly(Pk)) - T(alpha) * Poly(Pk)) - (T(beta) * Poly(Pkm1));
  }

  template <typename Poly, 
	    typename Poly1, 
	    typename Poly2, 
	    typename Poly3>
  inline Poly
  stieltjes_nd_next(const typename Poly::mapped_type& alpha, 
		    const typename Poly::mapped_type& beta, 
		    Poly1 x, 
		    Poly2 Pk, 
		    Poly3 Pkm1)
  {
    typedef typename Poly::mapped_type T;
   
    return ((Poly(x) * Poly(Pk)) - T(alpha) * Poly(Pk)) - (T(beta) * Poly(Pkm1));
    
  }



template<typename T, 
	 typename RandomAccessIterator>
inline
void eval_stieltjes_basis(const T& x, 
			  const T& alpha,
			  const T& beta,
			  const std::size_t n,
			  RandomAccessIterator first, 
			  RandomAccessIterator last)
  {
    assert((last - first) == typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    if(n == 0)
      {
	first[0] = T(1);
      }
    else if (n == 1)
      {
	 first[0] = T(1);
	 first[1] = x - alpha;
      }
    else
      {

	T x_m_alpha = x - alpha;
	first[0] = T(1);
	first[1] = x_m_alpha;

	
	for (std::size_t j = 2; j <= n; ++j)
	  {
	    first[j] = (x_m_alpha * first[j-1]) - beta*first[j-2];
	  }
	 
      }
   
  }

template<typename T, 
	 typename RandomAccessIterator1,
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator>
inline
void eval_stieltjes_basis(const T& x, 
			  RandomAccessIterator1 alpha_first,
			  RandomAccessIterator1 alpha_last,
			  RandomAccessIterator2 beta_first,
			  RandomAccessIterator2 beta_last,
			  const std::size_t n,
			  RandomAccessIterator first, 
			  RandomAccessIterator last)
  {
    assert((last - first) >= typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    if(n == 0)
      {
	first[0] = T(1);
      }
    else if (n == 1)
      {
	 first[0] = T(1);
	 first[1] = x - *alpha_first;
      }
    else
      {

	T x_m_alpha = x - *alpha_first;
	first[0] = T(1);
	first[1] = x_m_alpha;

	//std::cout<<"first = \n";
// 	std::cout<<first[0]<<std::endl;
// 	std::cout<<first[1]<<std::endl;
	
	RandomAccessIterator1 it_alpha = alpha_first + 1;
	RandomAccessIterator2 it_beta  = beta_first;
	for (std::size_t j = 2; j <= n; ++j, ++it_alpha,++it_beta)
	  {
	    x_m_alpha = x - *it_alpha;
	    first[j] = (x_m_alpha * first[j-1]) - *it_beta*first[j-2];
	    //std::cout<<first[j]<<std::endl;
	  }
	 
      }
   
  }

template<typename T, 
	 typename RandomAccessIterator1,
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator>
inline
void eval_stieltjes_basis_normalized(const T& x, 
				     RandomAccessIterator1 alpha_first,
				     RandomAccessIterator1 alpha_last,
				     RandomAccessIterator2 beta_first,
				     RandomAccessIterator2 beta_last,
				     const std::size_t n,
				     RandomAccessIterator first, 
				     RandomAccessIterator last)
  {
    assert((last - first) >= typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    if(n == 0)
      {
	first[0] = T(1) / std::sqrt(*beta_first);
      }
    else if (n == 1)
      {
	 first[0] = T(1) / std::sqrt(*beta_first);
	 first[1] = (x - *alpha_first) * (first[0] / std::sqrt(beta_first[1]));
      }
    else
      {

	T x_m_alpha = x - *alpha_first;
	first[0] = T(1) / std::sqrt(*beta_first);
	first[1] = x_m_alpha * (first[0] / std::sqrt(beta_first[1]));

	RandomAccessIterator1 it_alpha = alpha_first + 1;
	RandomAccessIterator2 it_beta  = beta_first;
	for (std::size_t j = 2; j <= n; ++j, ++it_alpha,++it_beta)
	  {
	    x_m_alpha = x - *it_alpha;
	    first[j] = ((x_m_alpha * first[j-1]) - std::sqrt(*it_beta)*first[j-2])/std::sqrt(*(it_beta+1));
	  }
	 
      }
   
  }



template<typename T, 
	 typename RandomAccessIterator1,
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator>
inline
void derivative_stieltjes_basis(const T& x, 
				RandomAccessIterator1 alpha_first,
				RandomAccessIterator1 alpha_last,
				RandomAccessIterator2 beta_first,
				RandomAccessIterator2 beta_last,
				const std::size_t n,
				RandomAccessIterator first, 
				RandomAccessIterator last)
  {
    assert((last - first) >= typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    switch(n)
      {
      case 0:
	{
	  first[0] = T(0.0);
	}
	break;
  case 1:
      {
	 first[0] = T(0.0);
	 first[1] = T(1.0);
      }
    break;
  case 2:
      {
	first[0] = T(0.0);
	first[1] = T(1.0);
	first[2] = (x - *alpha_first) + (x - *(alpha_first+1));
      }
    break;
      default:
      {
	T x_m_alpha = (x - *alpha_first);
	first[0] = T(0.0);
	first[1] = T(1.0);
	first[2] = x_m_alpha + (x - *(alpha_first+1));

	T ykm1 = T(1.0);
	T yk   = x_m_alpha;

	RandomAccessIterator1 it_alpha = alpha_first + 2;
	RandomAccessIterator2 it_beta  = beta_first + 1;
	for (std::size_t j = 3; j <= n; ++j, ++it_alpha,++it_beta)
	  {
	    T x_m_alpha = x - *it_alpha;
	    T ykp1 = (x_m_alpha * yk) - (*it_beta*ykm1);
	    first[j] = ykp1 + (x - *(it_alpha+1))*first[j-1] - *(it_beta+1)* first[j-2];
	    //rotate yk
	    ykm1 = yk;
	    yk   = ykp1;
	    	    
	  }
	 
      }
      };
   
  }

//OK
template<typename T, 
	 typename RandomAccessIterator1,
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator>
inline
void derivative_stieltjes_basis_norm(const T& x, 
				     RandomAccessIterator1 alpha_first,
				     RandomAccessIterator1 alpha_last,
				     RandomAccessIterator2 beta_first,
				     RandomAccessIterator2 beta_last,
				     const std::size_t n,
				     RandomAccessIterator first, 
				     RandomAccessIterator last)
  {
    assert((last - first) >= typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    switch(n)
      {
      case 0:
	{
	  first[0] = static_cast<T>(0.0);
	}
	break;
  case 1:
      {
	 first[0] = static_cast<T>(0.0);
	 first[1] = static_cast<T>(1.0)/std::sqrt(*beta_first * *(beta_first+1));
      }
    break;
      default:
	{
	  T inv_sqrt_beta0 =  static_cast<T>(1.0)/std::sqrt(*beta_first);
	  T inv_sqrt_beta1 =  static_cast<T>(1.0)/std::sqrt(*(beta_first+1));
	  first[0]  = static_cast<T>(0.0);
	  first[1]  = inv_sqrt_beta0 * inv_sqrt_beta1;

	  T ykm1 = inv_sqrt_beta0;
	  T yk   = (x - *alpha_first) * first[1];

	  RandomAccessIterator1 it_alpha = alpha_first + 1;
	  RandomAccessIterator2 it_beta  = beta_first + 1;

	  for (std::size_t j = 2; j <= n; ++j,++it_alpha,++it_beta)
	  {
	    T sqrt_betak   = std::sqrt(*it_beta);
	    T inv_sqrt_betakp1 = static_cast<T>(1.0)/std::sqrt(*(it_beta+1));
	    T x_m_alpha = ( x - *it_alpha);
	  
	    T ykp1 = (x_m_alpha * yk)*inv_sqrt_betakp1 - (sqrt_betak*inv_sqrt_betakp1) * ykm1;
	   
	    first[j] = (yk + (x_m_alpha * first[j-1]))*inv_sqrt_betakp1 - (sqrt_betak*inv_sqrt_betakp1) * first[j-2];

	    //update yk
	    ykm1 = yk;
	    yk   = ykp1;
	  }

	}
      };
   
  }


//OK
template<typename T, 
	 typename RandomAccessIterator1,
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator3>
inline
T derivative_poly(const T& x, 
		  RandomAccessIterator1 alpha_first,
		  RandomAccessIterator1 alpha_last,
		  RandomAccessIterator2 beta_first,
		  RandomAccessIterator2 beta_last,
		  RandomAccessIterator3 coef_first,
		  RandomAccessIterator3 coef_last,
		  const std::size_t n)
{
  
  assert((coef_last - coef_first) >= typename std::iterator_traits<RandomAccessIterator3>::difference_type(n + 1));

 
  T result = static_cast<T>(0.0);

    switch(n)
      {
      case 0:
	{

	}
	break;
      case 1:
	{
	  result = *(coef_first+1);
	}
	break;
      default:
      {
 	T dykm1  = static_cast<T>(0.0);
 	T dyk    = static_cast<T>(1.0);

	result = *(coef_first+1);
	
	T ykm1 = static_cast<T>(1.0);
	T yk   = (x - *alpha_first);

	RandomAccessIterator1 it_alpha = alpha_first + 1;
	RandomAccessIterator2 it_beta  = beta_first + 1;
	RandomAccessIterator3 it_coef = coef_first + 2;

	for (;it_coef!=coef_last;++it_alpha,++it_beta,++it_coef)
	  {
	    T x_m_alpha = ( x - *it_alpha);
	    T ykp1 = (x_m_alpha * yk) - (*it_beta * ykm1);
	    
	   
	    T dykp1 = yk + (x_m_alpha * dyk) - (*(it_beta) * dykm1);
 	    result = result + (*it_coef * dykp1);

	    //update yk
	    ykm1 = yk;
	    yk   = ykp1;
	    //update dyk
	    dykm1 = dyk;
	    dyk   = dykp1;
	    	    
	  }
      }
      };

    return result;
 }

//OK
template<typename T, 
	 typename RandomAccessIterator1,
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator3>
inline
T derivative_poly_norm(const T& x, 
		       RandomAccessIterator1 alpha_first,
		       RandomAccessIterator1 alpha_last,
		       RandomAccessIterator2 beta_first,
		       RandomAccessIterator2 beta_last,
		       RandomAccessIterator3 coef_first,
		       RandomAccessIterator3 coef_last,
		       const std::size_t n)
{
  
  assert((coef_last - coef_first) >= typename std::iterator_traits<RandomAccessIterator3>::difference_type(n + 1));

 
  T result = static_cast<T>(0.0);

    switch(n)
      {
      case 0:
	{

	}
	break;
      case 1:
	{
	  T dyk    = static_cast<T>(1.0)/std::sqrt(*beta_first * *(beta_first+1));
	  result = *(coef_first+1) * dyk;

	}
	break;
      default:
      {
	T inv_sqrt_beta0 =  static_cast<T>(1.0)/std::sqrt(*beta_first);
	T inv_sqrt_beta1 =  static_cast<T>(1.0)/std::sqrt(*(beta_first+1));
 	T dykm1  = static_cast<T>(0.0);
 	T dyk    = inv_sqrt_beta0 * inv_sqrt_beta1;

	result = *(coef_first+1) * dyk;
	
	T ykm1 = inv_sqrt_beta0;
	T yk   = ((x - *alpha_first)*ykm1)* inv_sqrt_beta1;

	RandomAccessIterator1 it_alpha = alpha_first + 1;
	RandomAccessIterator2 it_beta  = beta_first + 1;
	RandomAccessIterator3 it_coef = coef_first + 2;

	for (;it_coef!=coef_last;++it_alpha,++it_beta,++it_coef)
	  {
	    T sqrt_betak   = std::sqrt(*it_beta);
	    T inv_sqrt_betakp1 = static_cast<T>(1.0)/std::sqrt(*(it_beta+1));
	    T x_m_alpha = ( x - *it_alpha);
	  
	    T ykp1 = (x_m_alpha * yk)*inv_sqrt_betakp1 - (sqrt_betak*inv_sqrt_betakp1) * ykm1;
	   
	    T dykp1 = (yk + (x_m_alpha * dyk))*inv_sqrt_betakp1 - (sqrt_betak*inv_sqrt_betakp1) * dykm1;
 	    result = result + (*it_coef * dykp1);

	    //update yk
	    ykm1 = yk;
	    yk   = ykp1;
	    //update dyk
	    dykm1 = dyk;
	    dyk   = dykp1;
	    	    
	  }
      }
      };

    return result;
 }


//OK
template<typename T, 
	 typename RandomAccessIterator1,
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator3>
inline
T eval_poly_stieltjes(const T& x, 
		      RandomAccessIterator1 alpha_first,
		      RandomAccessIterator1 alpha_last,
		      RandomAccessIterator2 beta_first,
		      RandomAccessIterator2 beta_last,
		      RandomAccessIterator3 coef_first,
		      RandomAccessIterator3 coef_last,
		      const std::size_t n)
{
  
  assert((coef_last - coef_first) >= typename std::iterator_traits<RandomAccessIterator3>::difference_type(n + 1));

 
  T result = static_cast<T>(0.0);
    switch(n)
      {
      case 0:
	{
	   result = *coef_first;
	}
	break;
      case 1:
	{
	  result = *(coef_first) +  *(coef_first+1)* (x - *alpha_first);
	}
	break;
      default:
      {
	
	T ykm1 = static_cast<T>(1.0);
	T yk   = (x - *alpha_first);
	T ykp1 = static_cast<T>(0.0);
	result = *(coef_first) + *(coef_first+1)*yk;
	RandomAccessIterator1 it_alpha = alpha_first + 1;
	RandomAccessIterator2 it_beta  = beta_first + 1;
	RandomAccessIterator3 it_coef = coef_first + 2;

	for (;it_coef!=coef_last;++it_alpha,++it_beta,++it_coef)
	  {
	    ykp1 = (( x - *it_alpha) * yk) - (*it_beta*ykm1);
	    result = result + ykp1 * *it_coef;
	    //update yk
	    ykm1 = yk;
	    yk   = ykp1;
	  }
      }
      };
    return result;
 }

//OK
template<typename T, 
	 typename RandomAccessIterator1,
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator3>
inline
T eval_poly_stieltjes_norm(const T& x, 
		      RandomAccessIterator1 alpha_first,
		      RandomAccessIterator1 alpha_last,
		      RandomAccessIterator2 beta_first,
		      RandomAccessIterator2 beta_last,
		      RandomAccessIterator3 coef_first,
		      RandomAccessIterator3 coef_last,
		      const std::size_t n)
{
  
  assert((coef_last - coef_first) >= typename std::iterator_traits<RandomAccessIterator3>::difference_type(n + 1));

 
  T result = static_cast<T>(0.0);
    switch(n)
      {
      case 0:
	{
	  result = *coef_first/std::sqrt(*beta_first);
	}
	break;
      case 1:
	{
	  result = (*coef_first/std::sqrt(*beta_first)) +  *(coef_first+1)* (x - *alpha_first)/std::sqrt(*(beta_first+1));
	}
	break;
      default:
      {
	
	T ykm1 = static_cast<T>(1.0)/std::sqrt(*beta_first);
	T yk   = ((x - *alpha_first)*ykm1)/std::sqrt(*(beta_first+1));
	result = (*coef_first * ykm1) + *(coef_first+1)*yk;
	RandomAccessIterator1 it_alpha = alpha_first + 1;
	RandomAccessIterator2 it_beta  = beta_first + 1;
	RandomAccessIterator3 it_coef = coef_first + 2;

	for (;it_coef!=coef_last;++it_alpha,++it_beta,++it_coef)
	  {
	    T ykp1 = (( x - *it_alpha) * yk)/std::sqrt(*(it_beta+1)) - std::sqrt((*it_beta)/(*(it_beta+1)) ) * ykm1;
	    result = result + ykp1 * *it_coef;
	    //update yk
	    ykm1 = yk;
	    yk   = ykp1;
	  }
      }
      };
    return result;
 }

//n = nb polynomes
//same results as GAutchi
//TODO : n = 1
template<typename T, 
	 typename RandomAccessIterator1,
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator3>
inline
T clenshaw(const T& x, 
	   const T& y0,
	   const T& ym1,
	   RandomAccessIterator1 alpha_first,
	   RandomAccessIterator1 alpha_last,
	   RandomAccessIterator2 beta_first,
	   RandomAccessIterator2 beta_last,
	   RandomAccessIterator3 coef_first,
	   RandomAccessIterator3 coef_last,
	   const std::size_t n)
  {
    //assert((last - first) >= typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    if(n == 0)
      {
	return *coef_first;
      }
    else if (n == 1)
      {
	return *coef_first + *(coef_first +1)* (x - *alpha_first);
      }
    else
      {
	const int nm2 = n - 2;
	const int nm3 = n - 3;
	T Unp1 = *(coef_first + (n - 1));
	T Un   = *(coef_first + nm2) + (x - *(alpha_first + nm2))*Unp1;

	RandomAccessIterator1 it_alpha = alpha_first + nm3;
	RandomAccessIterator2 it_beta  = beta_first  + nm2;
	RandomAccessIterator3 it_coef  = coef_first  + nm3;
	for (int k = nm3; k >=0; --k,--it_alpha,--it_beta,--it_coef)
	  {
	    T tmp = Un ;
	    Un = ((x - *it_alpha) * Un) - (*it_beta*Unp1) + *it_coef;
	    Unp1 = tmp;
	  }
	return Un*y0 - *beta_first*Unp1*ym1; 
      }
   
  }

template<typename T, 
	 typename RandomAccessIterator1,
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator3>
inline
T clenshaw_poly(const T& x, 
		RandomAccessIterator1 alpha_first,
		RandomAccessIterator1 alpha_last,
		RandomAccessIterator2 beta_first,
		RandomAccessIterator2 beta_last,
		RandomAccessIterator3 coef_first,
		RandomAccessIterator3 coef_last,
		const std::size_t n)
  {
    //assert((last - first) >= typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    if(n == 0)
      {
	return *coef_first;
      }
    else if (n == 1)
      {
	return *coef_first + *(coef_first +1)* (x - *alpha_first);
      }
    else
      {
	const int nm2 = n - 2;
	const int nm3 = n - 3;
	T Unp1 = *(coef_first + (n - 1));
	T Un   = *(coef_first + nm2) + (x - *(alpha_first + nm2))*Unp1;

	RandomAccessIterator1 it_alpha = alpha_first + nm3;
	RandomAccessIterator2 it_beta  = beta_first  + nm2;
	RandomAccessIterator3 it_coef  = coef_first  + nm3;
	for (int k = nm3; k >=0; --k,--it_alpha,--it_beta,--it_coef)
	  {
	    T tmp = Un ;
	    Un = ((x - *it_alpha) * Un) - (*it_beta*Unp1) + *it_coef;
	    Unp1 = tmp;
	  }
	return Un;
      }
   
  }

//OK, to test for n = 0 and n = 1
template<typename T, 
	 typename RandomAccessIterator1,
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator3>
inline
T clenshaw_poly_norm(const T& x, 
		     RandomAccessIterator1 alpha_first,
		     RandomAccessIterator1 alpha_last,
		     RandomAccessIterator2 beta_first,
		     RandomAccessIterator2 beta_last,
		     RandomAccessIterator3 coef_first,
		     RandomAccessIterator3 coef_last,
		     const std::size_t n)
{
    //assert((last - first) >= typename std::iterator_traits<RandomAccessIterator>::difference_type(n + 1));
    if(n == 0)
      {
	return *coef_first/std::sqrt(*beta_first);
      }
    else if (n == 1)
      {
	return *coef_first/std::sqrt(*beta_first) + *(coef_first +1)/std::sqrt(*(beta_first+1))* (x - *alpha_first);
      }
    else
      {
	const int nm1 = n - 1;
	const int nm2 = n - 2;
	const int nm3 = n - 3;
	T Unp1 = *(coef_first + nm1);
	T Un   = *(coef_first + nm2) + ((x - *(alpha_first + nm2))/std::sqrt(*(beta_first+nm1)))*Unp1;

	RandomAccessIterator1 it_alpha = alpha_first + nm3;
	RandomAccessIterator2 it_beta  = beta_first  + nm2;
	RandomAccessIterator3 it_coef  = coef_first  + nm3;
	for (int k = nm3; k >=0; --k,--it_alpha,--it_beta,--it_coef)
	  {
	    T tmp = Un ;
	    Un = (((x - *it_alpha) * Un)/std::sqrt(*it_beta)) - std::sqrt((*it_beta)/ *(it_beta+1))*Unp1 + *it_coef;
	    Unp1 = tmp;
	  }
	return Un/std::sqrt(*beta_first);
      }
   
  }



 /*!
  ** \brief Implements the stieltjes procedure to get discrete
  ** orthogonal polynomials.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2010/11/07
  ** \version 0.0.1
  ** \param x_first RandomAccessIterator to the collocations range.
  ** \param x_last  RandomAccessIterator to the collocations range.
  ** \param weight_first RandomAccessIterator to the weights range.
  ** \param weight_last  RandomAccessIterator to the weights range.
  ** \param order Maximail order of the polynomials.
  ** \param alpha_first RandomAccessIterator to the three terms
  ** recurrence alpha coefficients range.
  ** \param alpha_last  RandomAccessIterator to the three terms
  ** recurrence alpha coefficients range.
  ** \param beta_first RandomAccessIterator to the three terms
  ** recurrence beta coefficients range.
  ** \param beta_last  RandomAccessIterator to the three terms
  ** recurrence beta coefficients range.
  ** \param pkpk_first RandomAccessIterator to the (Pk,Pk) discrete inner products.
  ** \param pkpk_last  RandomAccessIterator to the (Pk,Pk) discrete inner products.
 
  ** \param poly (order+1)x(x_last-x_first) 2d Container which k-th row contains the evaluations of
  ** the k-th polynomial at the collocations of the range [x_first, x_last).
  ** \param poly_w (order+1)x(x_last-x_first) 2d Container which k-th row contains the evaluations of
  ** the k-th polynomial at the collocations of the range [x_first,
  ** x_last) multiplies by the weights of the range [weight_first, weight_last).
  ** \pre (x_last-x_first) == (weight_last-weight_first)
  ** \pre order <= (x_last-x_first)
  ** \pre (alpha_last-alpha_first) 
  ** \pre (beta_last-beta_first) == order + 1
  ** \pre (pkk_last-pkk_first)  == order + 1
  
  ** \par Example:
  ** \code
  ** std::size_t fejer_size = 6;
  ** slip::Array<double> fejer_coll(fejer_size);
  ** slip::Array<double> fejer_weights(fejer_size);
  ** slipalgo::fejer_collocations(fejer_coll.begin(),fejer_coll.end());
  ** slipalgo::fejer_weights(fejer_weights.begin(),fejer_weights.end());
  ** std::cout<<"fejer_collocations = \n"<<fejer_coll<<std::endl;
  ** std::cout<<"fejer_weights      = \n"<<fejer_weights<<std::endl;
  **
  ** std::size_t fejer_order = 5;
  ** slip::Array<double> alpha(fejer_order+1);
  ** slip::Array<double> beta(fejer_order+1);
  ** 
  ** slip::Array2d<double> poly(fejer_order+1,fejer_coll.size());
  ** slip::Array2d<double> poly_w(fejer_order+1,fejer_coll.size());
  ** slip::Array<double> pk_pk(fejer_order+1);
  ** slip::stieltjes(fejer_coll.begin(),fejer_coll.end(),
  **		     fejer_weights.begin(),fejer_weights.end(),
  **	             fejer_order,
  **	             alpha.begin(),alpha.end(),
  **	             beta.begin(),beta.end(),
  **                 pk_pk.begin(),pk_pk.end(),
  **	             poly,
  **	             poly_w);
  ** std::cout<<"alpha   = \n"<<alpha<<std::endl;
  ** std::cout<<"beta    = \n"<<beta<<std::endl;
  ** std::cout<<"poly    = \n"<<poly<<std::endl;
  ** std::cout<<"poly_w  = \n"<<poly_w<<std::endl;
  ** std::cout<<"pk_pk   = \n"<<pk_pk<<std::endl;
  ** \endcode
  */
template<typename RandomAccessIterator1, 
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator3,
	 typename RandomAccessIterator4,
	 typename RandomAccessIterator5,
	 typename Container2d1,
	 typename Container2d2>
  void stieltjes(RandomAccessIterator1 x_first,
		 RandomAccessIterator1 x_last,
		 RandomAccessIterator2 weight_first,
		 RandomAccessIterator2 weight_last,
		 const std::size_t order,
		 RandomAccessIterator3 alpha_first,
		 RandomAccessIterator3 alpha_last,
		 RandomAccessIterator4 beta_first,
		 RandomAccessIterator4 beta_last,
		 RandomAccessIterator5 pkk_first,
		 RandomAccessIterator5 pkk_last,
		 Container2d1& poly,
		 Container2d2& poly_w)
{
  assert((x_last-x_first) == (weight_last-weight_first));
  assert(order <= static_cast<std::size_t>(x_last-x_first));
  assert(static_cast<std::size_t>(alpha_last-alpha_first) == (order +1));
  assert(static_cast<std::size_t>(beta_last-beta_first) == (order +1));
 

  typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
  
  std::size_t N = static_cast<std::size_t>(x_last-x_first);
  poly.resize(order+1,N);
  poly_w.resize(order+1,N);
  T sw = std::accumulate(weight_first,weight_last,T());
  //compute alpha0 and beta0
  *alpha_first = slip::inner_product(x_first,x_last,weight_first) / sw;
  *beta_first  = sw;
  
  //first polynomial
  std::fill(poly.row_begin(0),poly.row_end(0),T(1));
  std::copy(weight_first, weight_last,poly_w.row_begin(0));
  //second polynomial
  typename Container2d1::row_iterator poly_itk = poly.row_begin(0);
  typename Container2d1::row_iterator poly_itkp1 = poly.row_begin(1);
  typename Container2d2::row_iterator poly_w_it = poly_w.row_begin(1);
  slip::Array<T> t_pkp1(N);
  typename slip::Array<T>::iterator t_pkp1_it = t_pkp1.begin();
  RandomAccessIterator1 x_it = x_first;
  RandomAccessIterator2 w_it = weight_first;
  for(; x_first != x_last; ++x_first, 
	++poly_itk, ++poly_itkp1, 
	++poly_w_it,++weight_first,
	++t_pkp1_it)
    {
      *poly_itkp1 = (*x_first - *alpha_first) * *poly_itk;
      *poly_w_it = *poly_itkp1 * *weight_first;
      *t_pkp1_it = *x_first * *poly_itkp1;
    }
 
  T pkm1_pkm1 = slip::inner_product(poly.row_begin(0),
				    poly.row_end(0),
				    poly_w.row_begin(0));
  pkk_first[0] = pkm1_pkm1;

  T pk_pk = slip::inner_product(poly.row_begin(1),
				    poly.row_end(1),
				    poly_w.row_begin(1));
  pkk_first[1] = pk_pk;
 
				
  *(alpha_first+1) = slip::inner_product(t_pkp1.begin(),
				   t_pkp1.end(),
				   poly_w.row_begin(1))
              / pk_pk;

  *(beta_first+1) = pk_pk / pkm1_pkm1;
  

  //other polynomials
  RandomAccessIterator5 it_pkpk = pkk_first + 2;
  for(std::size_t k = 2; k <= order; ++k,++it_pkpk)
    {
      //computes pk
      typename Container2d1::row_iterator poly_itkm1 = poly.row_begin(k-2);
      poly_itk = poly.row_begin(k-1);
      poly_itkp1 = poly.row_begin(k);
      poly_w_it = poly_w.row_begin(k);
      t_pkp1_it = t_pkp1.begin();
      x_first = x_it;
      weight_first = w_it;
    
     
      for(; x_first != x_last; 
	    ++x_first, 
	    ++poly_itkm1,++poly_itk, ++poly_itkp1, 
	    ++poly_w_it,++weight_first,
	    ++t_pkp1_it)
	{
	  
	  *poly_itkp1 = (*x_first - *(alpha_first+(k-1))) * *poly_itk - *(beta_first+(k-1))* *poly_itkm1;
	  *poly_w_it = *poly_itkp1 * *weight_first;
	  *t_pkp1_it = *x_first * *poly_itkp1;
	}

      
      pkm1_pkm1 = *(it_pkpk - 1); 
      pk_pk = slip::inner_product(poly.row_begin(k),
				  poly.row_end(k),
				  poly_w.row_begin(k));
      *it_pkpk = pk_pk;
   
     
      //computes alpha_k
      *(alpha_first+k) = slip::inner_product(t_pkp1.begin(),
					     t_pkp1.end(),
					     poly_w.row_begin(k))
	/ pk_pk;
      //computes beta_k
      *(beta_first+k) = pk_pk / pkm1_pkm1;
     
    }

}

  //computes a polynomial basis using the jacobi matrix coefficients
template <typename RandomAccessIterator1,
	  typename RandomAccessIterator2,
          typename Poly>
void stieltjes(RandomAccessIterator1 alpha_first,     
	       RandomAccessIterator1 alpha_last,
	       RandomAccessIterator2 beta_first,     
	       RandomAccessIterator2 beta_last,
	       const Poly& x,
	       std::vector<Poly>& base)
{
   typedef typename Poly::mapped_type T;
   base.resize(alpha_last-alpha_first);
   Poly Pm1 = Poly(T(0));
   slip::Array<T> p0(1,T(1));
   base[0]  = Poly(0,p0.begin(),p0.end());
   base[1]  = (x - *alpha_first) * base[0];
   alpha_first++;
   beta_first++;
   for(std::size_t k = 2; k < base.size(); ++k, ++alpha_first,++beta_first)
     {
       base[k] = (x - *alpha_first) * base[k-1] - base[k-2] * *beta_first;
     }
}


  //computes an orthonormal polynomial basis using the jacobi matrix coefficients
template <typename RandomAccessIterator1,
	  typename RandomAccessIterator2,
          typename Poly>
void stieltjes_normalized(RandomAccessIterator1 alpha_first,     
			  RandomAccessIterator1 alpha_last,
			  RandomAccessIterator2 beta_first,     
			  RandomAccessIterator2 beta_last,
			  const Poly& x,
			  std::vector<Poly>& base)
{
   typedef typename Poly::mapped_type T;
   base.resize(alpha_last-alpha_first);
   Poly Pm1 = Poly(T(0));
   slip::Array<T> p0(1,T(1)/std::sqrt(*beta_first));
   base[0]  = Poly(0,p0.begin(),p0.end());
   base[1]  = (x - *alpha_first) * (base[0] / std::sqrt(*(beta_first+1)));
   alpha_first++;
   beta_first++;
   for(std::size_t k = 2; k < base.size(); ++k, ++alpha_first,++beta_first)
     {
       base[k] = (x - *alpha_first) * (base[k-1]/std::sqrt(*(beta_first+1))) - base[k-2] * (std::sqrt(*beta_first) / std::sqrt(*(beta_first+1)));
     }
}


}//::slip

#endif //SLIP_POLYNOMIAL_ALGO_HPP
