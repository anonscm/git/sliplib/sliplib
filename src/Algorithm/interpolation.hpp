/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file interpolation.hpp
 * 
 * \brief Provides some interpolation algorithms.
 * \since 1.0.0
 * 
 */
#ifndef SLIP_INTERPOLATION_HPP
#define SLIP_INTERPOLATION_HPP

#include <cmath>
#include <limits>
#include "Array.hpp"
#include "Array2d.hpp"
#include "Vector.hpp"
#include "Matrix.hpp"
#include "macros.hpp"
#include "bspline_interpolation.hpp"
#include "linear_algebra.hpp"

namespace slip
{

   /**
   ** \brief Lanczos functor 
   ** Computes the Lanczos kernel function for  \f$ x \f$, this routine returns a sinus cardinal value: \f$ y(x)=\left\{\begin{array}{cc}1&x=0\\ \frac{r \sin(\pi x)\sin(\frac{\pi x}{r})}{(\pi x)^2}& 0 < |x| < r \\ 0 &\textrm{else}\\ \end{array}\right.\f$
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr
   ** \date 2009/06/23
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param r integer radius of the kernel (r= 2 or 3 in general).
   ** \return Lanczos kernel value
   ** \par Example:
   ** \code
   **  slip::Array<double> lanczos(7);
   ** slip::Array<double> x(7);
   ** slip::iota(x.begin(),x.end(),-static_cast<double>(x.size())/static_cast<double>(2),static_cast<double>(x.size())/static_cast<double>(x.size()-1));
   ** std::transform(x.begin(),x.end(),lanczos.begin(),slip::Lanczos<double>(3));
   ** std::cout<<"lanczos = \n"<<lanczos<<std::endl;

   ** \endcode 
   ** \remarks Will be delocated in macros.hpp in the future
   */
template <class _Tp>
struct Lanczos : public std::unary_function<_Tp, _Tp>
  {
    Lanczos():
      radius_(static_cast<_Tp>(3))
    {}
    Lanczos(const _Tp& radius):
      radius_(radius)
    {}

    _Tp
    operator()(const _Tp& x) const
    { 
      if (x == static_cast<_Tp>(0.0)) 
	{
	  return slip::constants<_Tp>::one();
	}
      if( (x <= - radius_) || (x >= radius_))
	{
	  return _Tp();
	} 
      
      _Tp tmp = x * slip::constants<_Tp>::pi();
      return radius_ * std::sin(tmp) * std::sin(tmp / radius_) / (tmp * tmp);
      
     }

      _Tp radius_;
  };



 /*!
  ** \brief nearest neighbor functor 
  ** \f$ y(x)= \left\{\begin{array}{cc}1& -0.5 \le x < 0.5\\ 0 &\textrm{else}\\ \end{array}\right.\f$
  ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr
  ** \date 2015/01/28
  ** \since 1.5.0
  ** \version 0.0.1
  ** \param point 1d point 
  ** \return sinus cardinal value
  ** \remarks Will be delocated in macros.hpp in the future
  ** \par Example:
  ** \code 
  ** slip::Array<double> nearest_neigh(7);
  ** slip::Array<double> x2(7);
  ** slip::iota(x2.begin(),x2.end(),-1.0,2.0/static_cast<double>(x2.size()-1));
  ** std::cout<<"x2 = \n"<<x2<<std::endl;
  ** std::transform(x2.begin(),x2.end(),nearest_neigh.begin(),slip::NearestNeighbor<double>());
  ** std::cout<<"nearest_neigh = \n"<<nearest_neigh<<std::endl;
  ** \endcode
  **
 */
template <class _Tp>
struct NearestNeighbor : public std::unary_function<_Tp, _Tp>
  {
    _Tp
    operator()(const _Tp& point) const
    { 
      if(-slip::constants<_Tp>::half() <= point && point < slip::constants<_Tp>::half())
	{
	  return slip::constants<_Tp>::one();
	}
      else
	{
	  return _Tp();
	}
    }
  };


  //----------------------------------------------------------------
  // Polynomial interpolation functions
  //----------------------------------------------------------------
 
/*!
** \brief Computes the coefficients of a Newton polynomial of order \a order
** 
** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
** \date 2012/02/02
** \version 0.0.1
** \since 1.5.0
** \param x_first RandomAccessIterator to abscissas xi.
** \param x_last RandomAccessIterator to abscissas xi.
** \param y_first RandomAccessIterator to yi = f(xi). 
** \param y_last RandomAccessIterator to yi = f(xi). 
** \param coeff_first RandomAccessIterator to the Newton coefficients. 
** \param coeff_last RandomAccessIterator to the Newton coefficients.
** \param order Order of the interpolation polynomial.
** \pre (x_last - x_first) == (y_last - y_first)
** \pre (x_last - x_first) == (coeff_last - coeff_first)
** \pre  (x_last - x_first) == (order + 1)
** \pre All the values within [x_first,x_last) must be disctinct.
** \par Complexity order*(order+1) substraction + ((order*(order+1))/2) divisions 
** \par Example:
**\code
** slip::Array<T> XN(4);
**   XN[0] = T(0);
**   XN[1] = T(2);
**   XN[2] = T(4);
**   XN[3] = T(6);
**  
**   slip::Array<T> YN(4);
**   YN[0] = T(0);
**   YN[1] = T(4);
**   YN[2] = T(0);
**   YN[3] = T(4);
** 
**   slip::Array<T> Ncoeff(4);
**   slip::newton_polynomial_coeff(XN.begin(),XN.end(),
** 				YN.begin(),YN.end(),
** 				Ncoeff.begin(),Ncoeff.end(),3);
**   std::cout<<"XN =\n"<<XN<<std::endl;
**   std::cout<<"YN =\n"<<YN<<std::endl;
**   std::cout<<"Ncoeff =\n"<<Ncoeff<<std::endl;
** \endcode
*/
template <typename RandomAccessIterator1,
	  typename RandomAccessIterator2,
	  typename RandomAccessIterator3>
void newton_polynomial_coeff(RandomAccessIterator1 x_first,
			     RandomAccessIterator1 x_last,
			     RandomAccessIterator2 y_first,
			     RandomAccessIterator2 y_last,
			     RandomAccessIterator3 coeff_first,
			     RandomAccessIterator3 coeff_last,
			     const std::size_t order)
{
  assert((x_last - x_first) == (y_last - y_first));
  assert((x_last - x_first) == (coeff_last - coeff_first));
  assert(static_cast<std::size_t>(x_last - x_first) == (order + 1));
  std::copy(y_first,y_last,coeff_first);
  for(std::size_t i = 1; i <= order; ++i)
    {
      for(std::size_t j = i; j <= order; ++j)
	{
	  coeff_first[j] = (coeff_first[j] - coeff_first[i-1]) / (x_first[j] - x_first[i-1]);
	}
    }

}

/*!
** \brief Computes the Newton polynomial interpolation from Newton polynomial coefficients.
** 
** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
** \date 2012/02/02
** \version 0.0.1
** \since 1.5.0
** \param coeff_first RandomAccessIterator to the Newton coefficients. 
** \param coeff_last RandomAccessIterator to the Newton coefficients.
** \param x_first RandomAccessIterator to abscissas xi.
** \param x_last RandomAccessIterator to abscissas xi.
** \pre (x_last - x_first) == (coeff_last - coeff_first)
** \pre All the values within [x_first,x_last) must be disctinct.
** \note Should be used with the slip::newton_polynomial_coeff function.
** \par Complexity order multiplications  + 2*order additions 
** \par Example:
**\code
** slip::Array<T> XN(4);
**   XN[0] = T(0);
**   XN[1] = T(2);
**   XN[2] = T(4);
**   XN[3] = T(6);
**  
**   slip::Array<T> YN(4);
**   YN[0] = T(0);
**   YN[1] = T(4);
**   YN[2] = T(0);
**   YN[3] = T(4);
** 
**   slip::Array<T> Ncoeff(4);
**   slip::newton_polynomial_coeff(XN.begin(),XN.end(),
** 				YN.begin(),YN.end(),
** 				Ncoeff.begin(),Ncoeff.end(),3);
**   std::cout<<"XN =\n"<<XN<<std::endl;
**   std::cout<<"YN =\n"<<YN<<std::endl;
**   std::cout<<"Ncoeff =\n"<<Ncoeff<<std::endl;
**   T valN = T(2.2);
**   std::cout<<"Evaluate P("<<valN<<") = "<<slip::newton_interpolation(Ncoeff.begin(),Ncoeff.end(),XN.begin(),XN.end(),valN)<<std::endl;
** \endcode
*/
template <typename RandomAccessIterator1,
	  typename RandomAccessIterator2,
	  typename Type>
Type newton_interpolation(RandomAccessIterator1 coeff_first,
			  RandomAccessIterator1 coeff_last,
			  RandomAccessIterator2 x_first,
			  RandomAccessIterator2 x_last,
			  const Type& val)
{
   assert((x_last - x_first) == (coeff_last - coeff_first));
  const std::size_t N = static_cast<std::size_t>(coeff_last-coeff_first);
  Type p = *(coeff_last - 1);
  for(std::size_t i = 1, j = (N-2); i < N; ++i,--j)
    {
      p = coeff_first[j] + (val - x_first[j]) * p;
    }
  return p;
  
}

  /*!
  ** \brief polynomial interpolation of order \a order such that
  ** P(*(x_first+i)) = *(y_first + i) using the Neville-Aitken algorithm.
  ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
  ** \date 2010/05/08
  ** \version 0.0.1
  ** \since 1.5.0
  ** \param x_first RandomAccessIterator to abscissas xi.
  ** \param x_last RandomAccessIterator to abscissas xi.
  ** \param y_first RandomAccessIterator to yi = f(xi). 
  ** \param y_last RandomAccessIterator to yi = f(xi). 
  ** \param order Order of the interpolation polynomial.
  ** \param x Value at which interpolation is done.
  ** \return interpolated value
  ** \pre (x_last - x_first) == (y_last - y_first)
  ** \pre (x_last - x_first) == (order + 1)
  ** \pre All the values within [x_first,x_last) must be disctinct.
  ** \par Complexity (order+1) + (1/2)(order)(order+1)*(2 substractions + 2 multiplications + 1 division)
  ** \par Example1:
  ** \code
  ** typedef float T;
  ** slip::Array<T> X(2);
  ** slip::Array<T> Y(2);
  ** X[0] = T(0.0);
  ** X[1] = T(1.0);
  ** Y[0] = T(3.2);
  ** Y[1] = T(5.1);
  ** T mu = T(0.2);
  ** //linear interpolation at mu = 0.2
  ** T y = slip::polynomial_interpolation(X.begin(),X.end(),
  **    				  Y.begin(),Y.end(),
  ** 				          1,
  ** 				          mu);
  ** std::cout<<"X = "<<X<<std::endl;
  ** std::cout<<"Y = "<<Y<<std::endl;
  ** std::cout<<"mu = "<<mu<<" y = "<<y<<std::endl;
  ** \endcode
  ** \par Example2:
  ** \code
  ** typedef float T;
  ** slip::Array<T> X(2);
  ** slip::Array<slip::Vector2d<T> > Y(2);
  ** X[0] = T(0.0);
  ** X[1] = T(1.0);
  ** Y[0] = slip::Vector2d<T>(T(1.2),T(1.3));
  ** Y[1] = slip::Vector2d<T>(T(2.2),T(3.3));
  ** T mu = T(0.3);
  ** //linear interpolation between 2 slip::Vector2d<T> at mu = 0.3
  ** slip::Vector2d<T> yp = slip::polynomial_interpolation(X.begin(),X.end(),
  **                                                       Y.begin(),Y.end(),
  **                                                       1,
  **                                                       mu);
  ** std::cout<<"X = "<<X<<std::endl;
  ** std::cout<<"Y = "<<Y<<std::endl;
  ** std::cout<<"mu = "<<mu<<" yp = "<<yp<<std::endl;
  ** 
  */
  template <typename RandomAccessIterator1,
	    typename RandomAccessIterator2,
	    typename T>
  inline
  typename std::iterator_traits<RandomAccessIterator2>::value_type
  polynomial_interpolation(RandomAccessIterator1 x_first, 
			   RandomAccessIterator1 x_last,
			   RandomAccessIterator2 y_first, 
			   RandomAccessIterator2 y_last,
			   const std::size_t& order,
			   const T& x)
  {
    typedef typename std::iterator_traits<RandomAccessIterator2>::value_type value_type;
    assert((x_last - x_first) == (y_last - y_first));
    assert(static_cast<std::size_t>(x_last - x_first) == (order + 1));
    std::size_t n = static_cast<std::size_t>(x_last - x_first);
    slip::Array<T> t(n);
    slip::Array2d<value_type> P(n,n);

    for(std::size_t i = 0; i < n; ++i, ++y_first, ++x_first)
      {
	P[i][0] = *y_first;
	t[i] = (x - *x_first);
      }

    std::size_t kend = n - 1;
    for(std::size_t k = 0; k < kend; ++k)
      {
	std::size_t iend = (n-k-1);
	for(std::size_t i = 0; i < iend; ++i)
	  {
	    T tik1 = t[i+k+1];
	    T ti = t[i];
	    P[i][k+1] = (ti * P[i+1][k] - tik1 * P[i][k]) / 
	                (ti - tik1); 
	  } 
      }
    return P[0][n-1];
  }

   /*!
  ** \brief polynomial interpolation on a regular grid (xi = i)
  ** and xi+1 - xi = 1) of order \a order such that
  ** P(i) = *(y_first + i) using the Neville-Aitken algorithm.
  ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
  ** \date 2010/05/08
  ** \version 0.0.1
  ** \since 1.5.0
  ** \param y_first RandomAccessIterator to yi = fi. 
  ** \param y_last RandomAccessIterator to yi = fi. 
  ** \param order Order of the interpolation polynomial.
  ** \param x Value at which interpolation is done.
  ** \return interpolated value
  ** \pre (y_last - y_first) == (order + 1)
  ** \par Example1:
  ** \code
  ** typedef float T;
  ** Y[0] = T(3.2);
  ** Y[1] = T(5.1);
  ** T mu = T(0.2);
  ** //linear interpolation at mu = 0.2
  ** T y = slip::polynomial_interpolation(Y.begin(),Y.end(),
  ** 				          1,
  ** 				          mu);
  ** std::cout<<"Y = "<<Y<<std::endl;
  ** std::cout<<"mu = "<<mu<<" y = "<<y<<std::endl;
  ** \endcode
  ** \par Example2:
  ** \code
  ** typedef float T;
  ** slip::Array<slip::Vector2d<T> > Y(2);
  ** Y[0] = slip::Vector2d<T>(T(1.2),T(1.3));
  ** Y[1] = slip::Vector2d<T>(T(2.2),T(3.3));
  ** T mu = T(0.3);
  ** //linear interpolation between 2 slip::Vector2d<T> at mu = 0.3
  ** slip::Vector2d<T> yp = slip::polynomial_interpolation(Y.begin(),Y.end(),
  **                                                       1,
  **                                                       mu);
  ** std::cout<<"Y = "<<Y<<std::endl;
  ** std::cout<<"mu = "<<mu<<" yp = "<<yp<<std::endl;
  ** 
  */
  template <typename RandomAccessIterator1,
	   typename T>
  inline
  typename std::iterator_traits<RandomAccessIterator1>::value_type
  polynomial_interpolation(RandomAccessIterator1 y_first, 
			   RandomAccessIterator1 y_last,
			   const std::size_t& order,
			   const T& x)
  {
    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type;
    assert(static_cast<std::size_t>(y_last - y_first) == (order + 1));
    std::size_t n = static_cast<std::size_t>(y_last - y_first);
    slip::Array<T> t(n);
    slip::Array2d<value_type> P(n,n);

    for(std::size_t i = 0; i < n; ++i, ++y_first)
      {
	P[i][0] = *y_first;
	t[i] = (x - static_cast<T>(i));
      }

    std::size_t kend = n - 1;
   
    for(std::size_t k = 0; k < kend; ++k)
      {
	T den = static_cast<T>(k+1);
	std::size_t iend = (n-k-1);
	for(std::size_t i = 0; i < iend; ++i)
	  {
	    
	    P[i][k+1] = (t[i] * (P[i+1][k] - P[i][k]) + (den * P[i][k]))/ 
	                den; 
	  } 
      }
    return P[0][n-1];
  }

  template <typename RandomAccessIterator2d1,
	    typename T>
  inline
  typename std::iterator_traits<RandomAccessIterator2d1>::value_type
  polynomial_interpolation2d(RandomAccessIterator2d1 y_first, 
			     RandomAccessIterator2d1 y_last,
			     const std::size_t& order,
			     const T& p)
  {
    typedef typename std::iterator_traits<RandomAccessIterator2d1>::value_type value_type;
   
    std::size_t rows = static_cast<std::size_t>((y_last-y_first)[0]);
    slip::Array<value_type> I(rows);
    for(std::size_t i = 0; i < rows; ++i)
      {
	I[i] = slip::polynomial_interpolation(y_first.row_begin(i),
					      y_first.row_end(i),
					      order,
					      p[1]);
      }
    
    return slip::polynomial_interpolation(I.begin(),I.end(),order,p[0]);
  }

  template <typename RandomAccessIterator3d1,
	    typename T>
  inline
  typename std::iterator_traits<RandomAccessIterator3d1>::value_type
  polynomial_interpolation3d(RandomAccessIterator3d1 y_first, 
			     RandomAccessIterator3d1 y_last,
			     const std::size_t& order,
			     const T& p)
  {
    typedef typename std::iterator_traits<RandomAccessIterator3d1>::value_type value_type;
   
    std::size_t slices = static_cast<std::size_t>((y_last-y_first)[0]);
    std::size_t rows   = static_cast<std::size_t>((y_last-y_first)[1]);

    slip::Array<value_type> I(rows);
    slip::Array<value_type> K(slices);
    
    for(std::size_t k = 0; k < slices; ++k)
      {
	for(std::size_t i = 0; i < rows; ++i)
	  {
	    I[i] = slip::polynomial_interpolation(y_first.row_begin(k,i),
						  y_first.row_end(k,i),
						  order,
						  p[2]);
	  }
    
	K[k] = slip::polynomial_interpolation(I.begin(),I.end(),order,p[1]);
      }
    return slip::polynomial_interpolation(K.begin(),K.end(),order,p[0]);
  }

  //------------------------------------------------------------------------
  //  Linear interpolation
  //------------------------------------------------------------------------

  /*!
  ** \brief 1d Linear interpolation 
  ** \since 1.5.0
  ** \param ya
  ** \param yb
  ** \param mu [0,1]
  ** \return interpolated value
  ** \par Complexity 2 multiplications + 2 additions
  */
  template <typename T,
	    typename R>
  T linear_interpolation(const T& ya, const T& yb, const R& mu)
  {
    return ya * (static_cast<R>(1) - mu) + yb * mu;
  }

 
   /*!
  ** \brief 2d Linear interpolation
  ** \since 1.5.0
  ** \param p00
  ** \param p01
  ** \param p10
  ** \param p11
  ** \param mui [0,1]
  ** \param muj [0,1]
  ** \return interpolated value
  ** \code
  **  p00-----p01
  **  |    mui  |
  **  | muj x   |
  **  |         |
  **  p10-----p11
  ** \endcode
  ** \par Complexity 6 additions + 6 multiplications
  */
  template <typename T,
	    typename R>
  T bilinear_interpolation(const T& p00, const T& p01, 
			   const T& p10, const T& p11, 
			   const R& mui, 
			   const R& muj)
  {
    T x1 = slip::linear_interpolation(p00,p01,muj);
    T x2 = slip::linear_interpolation(p10,p11,muj);
    return  linear_interpolation(x1,x2,mui);
  }

  template <typename T,
	    typename R>
  T trilinear_interpolation(const T& p000, const T& p001, 
			    const T& p010, const T& p011,
			    const T& p100, const T& p101, 
			    const T& p110, const T& p111,
			    const R& muk,
			    const R& mui, 
			    const R& muj)
  {
    T p1 = slip::bilinear_interpolation(p000,p001,
					p010,p011,
					mui,
					muj);
    T p2 = slip::bilinear_interpolation(p100,p101,
					p110,p111,
					mui,
					muj);
    
    return slip::linear_interpolation(p1,p2,muk);
  }

 //------------------------------------------------------------------------
//  Cubic interpolation
//------------------------------------------------------------------------
 /*!
  ** \brief 1d cubic interpolation 
  ** \since 1.5.0
  ** \param y0
  ** \param y1
  ** \param y2
  ** \param y3
  ** \param mu [0,1]
  ** \return interpolated value
  ** \par Complexity 4 multiplications + 10 additions
  */
template <typename T,
	  typename R>
T cubic_interpolation(const T& y0, 
		      const T& y1,
		      const T& y2, 
		      const T& y3,
		      const R& mu)
{
   R mu2 = mu*mu;
   T a0 = y3 - y2 - y0 + y1;
   T a1 = y0 - y1 - a0;
   T a2 = y2 - y0;
   T a3 = y1;

   return (a0*static_cast<T>(mu*mu2)+a1*static_cast<T>(mu2)+a2*static_cast<T>(mu)+a3);
}

 template <typename T,
	    typename R>
  T cubic_interpolation_2d(const T& p00, const T& p01, const T& p02, const T& p03, 
			   const T& p10, const T& p11, const T& p12, const T& p13, 
			   const T& p20, const T& p21, const T& p22, const T& p23,
			   const T& p30, const T& p31, const T& p32, const T& p33,
			   const R& mui, 
			   const R& muj)
  {
    T x0 = slip::cubic_interpolation(p00,p01,p02,p03,muj);
    T x1 = slip::cubic_interpolation(p10,p11,p12,p13,muj);
    T x2 = slip::cubic_interpolation(p20,p21,p22,p23,muj);
    T x3 = slip::cubic_interpolation(p30,p31,p32,p33,muj);
    
    return  linear_interpolation(x0,x1,x2,x3,mui);
  }

//------------------------------------------------------------------------
//  Cubic spline interpolation (natural spline)
//------------------------------------------------------------------------
 /** 
    \enum CUBIC_SPLINE_BOUNDARY_TYPE
    \brief Indicate the type of boundary cubic spline interpolation condition.
    \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
    \date 2024/05/24
    \since 1.5.0
    \code
    enum CUBIC_SPLINE_BOUNDARY_TYPE
    {
    CUBIC_SPLINE_BOUNDARY_NATURAL,
    CUBIC_SPLINE_BOUNDARY_NOT_A_KNOT
    };
    \endcode
*/

 enum CUBIC_SPLINE_BOUNDARY_TYPE
    {
    CUBIC_SPLINE_BOUNDARY_NATURAL,
    CUBIC_SPLINE_BOUNDARY_NOT_A_KNOT
    };

/*!
  ** \brief Computes cubic spline system without boundary conditions.
  ** \f[ \left(\begin{array}{cccccc}
  ** d_0&u_0&0&\cdots&\cdots&0\\
  ** l_0&d_1&u_1&\ddots&&\vdots\\
  ** 0&l_1&d_2&u_2&\ddots&\vdots\\
  ** \vdots&\ddots&\ddots&\ddots&\ddots&0\\
  ** \vdots&&\ddots&l_{n-2}&d_{n-1}&u_{n-1}\\
  ** 0&\cdots&\cdots&0&l_{n-1}&d_n\\
  ** \end{array}\right)
  ** \left(
  ** \begin{array}{c}
  ** c_0\\
  ** c_1\\
  ** \vdots\\
  ** \\
  ** \vdots\\
  ** c_{n-2}\\
  ** c_{n-1}\\
  ** \end{array}\right)
  ** =
  ** \left(
  ** \begin{array}{c}
  ** b_0\\
  ** b_1\\
  ** \vdots\\
  ** \\
  ** \vdots\\
  ** b_{n-2}\\
  ** b_{n-1}\\
  ** \end{array}\right)
  ** \f]
  ** That is to say \f$d_0, u_0, l_{n-1}\f$ and \f$d_n\f$ are not computed.
  ** \f[  h_{i+1}c_{i-1}+2(h_i+h_{i+1})c_i+h_ic_{i+1} = 3(h_id_{i+1}+h_{i+1}d_i),\quad, 1 \le i \le n-1\f]
  ** with 
  ** \f[h_i = x_i - x_{i-1}\f]
  ** and
  ** \f[d_i = \frac{y_i - y_{i-1}}{h_i} \f]
  ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
  ** \date 2010/05/08
  ** \version 0.0.2
  ** \since 1.5.0
  ** \param x_first RandomAccessIterator to abscissas xi.
  ** \param x_last RandomAccessIterator to abscissas xi.
  ** \param y_first RandomAccessIterator to yi = f(xi). 
  ** \param y_last RandomAccessIterator to yi = f(xi). 
  ** \param diag_first RandomAccessIterator to the main diagonal of the matrix system.
  ** \param diag_last RandomAccessIterator  to the main diagonal of the matrix system. 
  ** \param up_diag_first RandomAccessIterator to the upper diagonal of the matrix system.
  ** \param up_diag_last RandomAccessIterator  to the upper diagonal of the matrix system.
  ** \param low_diag_first RandomAccessIterator to the lower diagonal of the matrix system.
  ** \param low_diag_last RandomAccessIterator  to the lower diagonal of the matrix system.
  ** \param b_first RandomAccessIterator to the vector of the matrix system.
  ** \param b_last RandomAccessIterator  to the vector of the matrix system.
  ** \pre (x_last - x_first) == (y_last - y_first)
  ** \pre (x_last - x_first) == (diag_last - diag_first)
  ** \pre (x_last - x_first) == (b_last - b_first)
  ** \pre (x_last - x_first) == (up_diag_last - up_diag_first) + 1
  ** \pre (x_last - x_first) == (low_diag_last - low_diag_first) + 1
  ** \pre all the range should have the same real data type
  ** \pre  (x_last - x_first) > 2
  ** \par Example:
  ** \code
  ** slip::Vector<double> xsp3(8);
  ** slip::iota(xsp3.begin(),xsp3.end(),0.0,1.0);
  ** slip::Vector<double> ysp3 = slip::sin(xsp3);
  ** 
  ** slip::Vector<double> diagsp3(xsp3.size());
  ** slip::Vector<double> up_diagsp3(xsp3.size()-1);
  ** slip::Vector<double> low_diagsp3(xsp3.size()-1);
  **
  ** slip::Vector<double> bsp3(xsp3.size());
  ** slip::cubic_spline_system_main(xsp3.begin(),xsp3.end(),
  **			            ysp3.begin(),ysp3.end(),
  **				    diagsp3.begin(),diagsp3.end(),
  ** 			            up_diagsp3.begin(),up_diagsp3.end(),
  **                                low_diagsp3.begin(),low_diagsp3.end(),
  ** 			            bsp3.begin(),bsp3.end());
  ** slip::Matrix<double> Msp3(diagsp3.size(),diagsp3.size());
  ** slip::set_diagonal(diagsp3.begin(),diagsp3.end(),Msp3);
  ** 
  ** slip::set_diagonal(up_diagsp3.begin(),up_diagsp3.end(),Msp3,1);
  ** slip::set_diagonal(low_diagsp3.begin(),low_diagsp3.end(),Msp3,-1);
  ** std::cout<<"Msp3 = \n"<<Msp3<<std::endl;
  ** slip::Vector<double> Vbsp3(bsp3.begin(),bsp3.end());
  ** std::cout<<"Vbsp3 = \n"<<Vbsp3<<std::endl;			 
  ** \endcode
  */
template <typename RandomAccessIterator1,
	  typename RandomAccessIterator2,
	  typename RandomAccessIterator3,
	  typename RandomAccessIterator4,
	  typename RandomAccessIterator5,
	  typename RandomAccessIterator6>
void cubic_spline_system_main(RandomAccessIterator1 x_first,
			      RandomAccessIterator1 x_last,
			      RandomAccessIterator2 y_first,
			      RandomAccessIterator2 y_last,
			      RandomAccessIterator3 diag_first,
			      RandomAccessIterator3 diag_last,
			      RandomAccessIterator4 up_diag_first,
			      RandomAccessIterator4 up_diag_last,
			      RandomAccessIterator5 low_diag_first,
			      RandomAccessIterator5 low_diag_last,
			      RandomAccessIterator6 b_first,
			      RandomAccessIterator6 b_last)
{

  typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
  const T three = static_cast<T>(3.0);
  
  RandomAccessIterator1 it_x = x_first + 1;
  RandomAccessIterator2 it_y = y_first + 1;
  RandomAccessIterator3 it_diag = diag_first + 1;
  RandomAccessIterator3 it_diag_end = diag_last - 1;
  RandomAccessIterator4 it_diag_up = up_diag_first + 1;
  RandomAccessIterator5 it_diag_low = low_diag_first;
  RandomAccessIterator6 it_b = b_first+1;
  //diagonal
  for(; it_diag!=it_diag_end; 
      ++it_x,++it_diag,++it_diag_up,++it_diag_low,++it_b,++it_y)
    {
      //cubic spline matrix
      
      const T hi =  (*it_x - *(it_x-1));
      const T hip1 = (*(it_x+1) - *it_x);
      *it_diag  = (hi + hip1) + (hi + hip1);
      *it_diag_up = hi;
      *it_diag_low = hip1;
      //cubic spline vector
      *it_b = three * (  (*(it_y + 1) - *it_y) *(hi / hip1)
  			+(*it_y - *(it_y - 1)) *(hip1 / hi) ) ;
    
    }

  //wikipedia https://en.wikipedia.org/wiki/Spline_interpolation
  //diagonal
  // for(; it_diag!=it_diag_end; 
  //     ++it_x,++it_diag,++it_diag_up,++it_diag_low,++it_b,++it_y)
  //   {
  //     //cubic spline matrix
      
  //     const T one_o_hi =  slip::constants<T>::one()/(*it_x - *(it_x-1));
  //     const T one_o_hi2 =  one_o_hi*one_o_hi;
  //     const T one_o_hip1 = slip::constants<T>::one()/(*(it_x+1) - *it_x);
  //     const T one_o_hip12 = one_o_hip1*one_o_hip1;
  //     *it_diag  = (one_o_hi+one_o_hip1) + (one_o_hi+one_o_hip1);
  //     *it_diag_up = one_o_hip1;
  //     *it_diag_low = one_o_hi;
  //     //cubic spline vector
  //     *it_b = three * (  (*(it_y + 1) - *it_y)*one_o_hip12
  // 			+(*it_y - *(it_y - 1))*one_o_hi2 ) ;
  // }
  
}

/*!
  ** \brief Computes the first and last row of the matrix and rhs vector of the cubic spline matrix system for natural boundary condition:
  ** \f[ \left(\begin{array}{cccccc}
  ** d_0&u_0&0&\cdots&\cdots&0\\
  ** l_0&d_1&u_1&\ddots&&\vdots\\
  ** 0&l_1&d_2&u_2&\ddots&\vdots\\
  ** \vdots&\ddots&\ddots&\ddots&\ddots&0\\
  ** \vdots&&\ddots&l_{n-2}&d_{n-1}&u_{n-1}\\
  ** 0&\cdots&\cdots&0&l_{n-1}&d_n\\
  ** \end{array}\right)
  ** \left(
  ** \begin{array}{c}
  ** c_0\\
  ** c_1\\
  ** \vdots\\
  ** \\
  ** \vdots\\
  ** c_{n-2}\\
  ** c_{n-1}\\
  ** \end{array}\right)
  ** =
  ** \left(
  ** \begin{array}{c}
  ** b_0\\
  ** b_1\\
  ** \vdots\\
  ** \\
  ** \vdots\\
  ** b_{n-2}\\
  ** b_{n-1}\\
  ** \end{array}\right)
  ** \f]
  ** with
  ** \f[
  ** \begin{array}{cc}
  **  2c_0+c_1&=3d_1\\
  **  c_{n-1}+2c_{n}&=3d_n\\
  ** \end{array} 
  ** \f]
  ** with 
  ** \f[h_i = x_i - x_{i-1}\f]
  ** and
  ** \f[d_i = \frac{y_i - y_{i-1}}{h_i} \f]
  ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
  ** \date 2010/05/08
  ** \version 0.0.2
  ** \since 1.5.0
  ** \param x_first RandomAccessIterator to abscissas xi.
  ** \param x_last RandomAccessIterator to abscissas xi.
  ** \param y_first RandomAccessIterator to yi = f(xi). 
  ** \param y_last RandomAccessIterator to yi = f(xi). 
  ** \param diag_first RandomAccessIterator to the main diagonal of the matrix system.
  ** \param diag_last RandomAccessIterator  to the main diagonal of the matrix system. 
  ** \param up_diag_first RandomAccessIterator to the upper diagonal of the matrix system.
  ** \param up_diag_last RandomAccessIterator  to the upper diagonal of the matrix system.
  ** \param low_diag_first RandomAccessIterator to the lower diagonal of the matrix system.
  ** \param low_diag_last RandomAccessIterator  to the lower diagonal of the matrix system.
  ** \param b_first RandomAccessIterator to the vector of the matrix system.
  ** \param b_last RandomAccessIterator  to the vector of the matrix system.
  ** \pre (x_last - x_first) == (y_last - y_first)
  ** \pre (x_last - x_first) == (diag_last - diag_first)
  ** \pre (x_last - x_first) == (b_last - b_first)
  ** \pre (x_last - x_first) == (up_diag_last - up_diag_first) + 1
  ** \pre (x_last - x_first) == (low_diag_last - low_diag_first) + 1
  ** \pre all the range should have the same real data type
  ** \pre  (x_last - x_first) > 2
  ** \par Example:
  ** \code
  ** slip::Vector<double> xsp3(8);
  ** slip::iota(xsp3.begin(),xsp3.end(),0.0,1.0);
  ** slip::Vector<double> ysp3 = slip::sin(xsp3);
  ** 
  ** slip::Vector<double> diagsp3(xsp3.size());
  ** slip::Vector<double> up_diagsp3(xsp3.size()-1);
  ** slip::Vector<double> low_diagsp3(xsp3.size()-1);
  **
  ** slip::Vector<double> bsp3(xsp3.size());
  ** slip::cubic_spline_system_main(xsp3.begin(),xsp3.end(),
  **  		                    ysp3.begin(),ysp3.end(),
  **				    diagsp3.begin(),diagsp3.end(),
  ** 			            up_diagsp3.begin(),up_diagsp3.end(),
  **                                low_diagsp3.begin(),low_diagsp3.end(),
  **      			    bsp3.begin(),bsp3.end());
  ** cubic_spline_system_boundary_natural(xsp3.begin(),xsp3.end(),
  **	   		                  ysp3.begin(),ysp3.end(),
  **				          diagsp3.begin(),diagsp3.end(),
  ** 			                  up_diagsp3.begin(),up_diagsp3.end(),
  **                                      low_diagsp3.begin(),low_diagsp3.end(),
  ** 			                  bsp3.begin(),bsp3.end());
  ** slip::Matrix<double> Msp3(diagsp3.size(),diagsp3.size());
  ** slip::set_diagonal(diagsp3.begin(),diagsp3.end(),Msp3);
  ** 
  ** slip::set_diagonal(up_diagsp3.begin(),up_diagsp3.end(),Msp3,1);
  ** slip::set_diagonal(low_diagsp3.begin(),low_diagsp3.end(),Msp3,-1);
  ** std::cout<<"Msp3 = \n"<<Msp3<<std::endl;
  ** slip::Vector<double> Vbsp3(bsp3.begin(),bsp3.end());
  ** std::cout<<"Vbsp3 = \n"<<Vbsp3<<std::endl;
  ** \endcode
  */
template <typename RandomAccessIterator1,
	  typename RandomAccessIterator2,
	  typename RandomAccessIterator3,
	  typename RandomAccessIterator4,
	  typename RandomAccessIterator5,
	  typename RandomAccessIterator6>
void cubic_spline_system_boundary_natural(RandomAccessIterator1 x_first,
					  RandomAccessIterator1 x_last,
					  RandomAccessIterator2 y_first,
					  RandomAccessIterator2 y_last,
					  RandomAccessIterator3 diag_first,
					  RandomAccessIterator3 diag_last,
					  RandomAccessIterator4 up_diag_first,
					  RandomAccessIterator4 up_diag_last,
					  RandomAccessIterator5 low_diag_first,
					  RandomAccessIterator5 low_diag_last,
					  RandomAccessIterator6 b_first,
					  RandomAccessIterator6 b_last)
{
  typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
  const T three = static_cast<T>(3.0);
  //x0 natural spline border
  const T x1_m_x0 = (*(x_first+1) - *x_first);
  *diag_first = slip::constants<T>::two();
  *up_diag_first = slip::constants<T>::one();
  *b_first = (three/x1_m_x0)*(*(y_first+1)- *y_first);

  //xn natural spline border
  const T xn_m_xnm1 = (*(x_last-1)- *(x_last-2));
  *(diag_last - 1) = slip::constants<T>::two();
  *(low_diag_last - 1) = slip::constants<T>::one();
  *(b_last-1) = (three/xn_m_xnm1)*(*(y_last-1) - *(y_last-2));
}


/*!
  ** \brief Computes the first and last row of the matrix and rhs vector of the cubic spline matrix system for not a knot boundary condition.
  ** \f[ \left(\begin{array}{cccccc}
  ** d_0&u_0&0&\cdots&\cdots&0\\
  ** l_0&d_1&u_1&\ddots&&\vdots\\
  ** 0&l_1&d_2&u_2&\ddots&\vdots\\
  ** \vdots&\ddots&\ddots&\ddots&\ddots&0\\
  ** \vdots&&\ddots&l_{n-2}&d_{n-1}&u_{n-1}\\
  ** 0&\cdots&\cdots&0&l_{n-1}&d_n\\
  ** \end{array}\right)
  ** \left(
  ** \begin{array}{c}
  ** c_0\\
  ** c_1\\
  ** \vdots\\
  ** \\
  ** \vdots\\
  ** c_{n-2}\\
  ** c_{n-1}\\
  ** \end{array}\right)
  ** =
  ** \left(
  ** \begin{array}{c}
  ** b_0\\
  ** b_1\\
  ** \vdots\\
  ** \\
  ** \vdots\\
  ** b_{n-2}\\
  ** b_{n-1}\\
  ** \end{array}\right)
  ** \f]
  ** with
  ** \f[ 
  ** \begin{array}{cc}
  ** h_2c_0 + (h_1+h_2)c_1&= 2h_2d_1+\frac{h_1(h_1d_2+h_2d_1)}{h_1+h_2}\\
  ** (h_{n-1}+h_n)c_{n-1}+h_{n-1}c_n &= 2h_{n-1}d_n+\frac{h_n(h_{n-1}d_n+h_nd_{n-1}}{h_{n-1}+h_n}\\
  ** \end{array}
  ** \f]
  ** with 
  ** \f[h_i = x_i - x_{i-1}\f]
  ** and
  ** \f[d_i = \frac{y_i - y_{i-1}}{h_i} \f]
  ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
  ** \date 2010/05/08
  ** \version 0.0.2
  ** \since 1.5.0
  ** \param x_first RandomAccessIterator to abscissas xi.
  ** \param x_last RandomAccessIterator to abscissas xi.
  ** \param y_first RandomAccessIterator to yi = f(xi). 
  ** \param y_last RandomAccessIterator to yi = f(xi). 
  ** \param diag_first RandomAccessIterator to the main diagonal of the matrix system.
  ** \param diag_last RandomAccessIterator  to the main diagonal of the matrix system. 
  ** \param up_diag_first RandomAccessIterator to the upper diagonal of the matrix system.
  ** \param up_diag_last RandomAccessIterator  to the upper diagonal of the matrix system.
  ** \param low_diag_first RandomAccessIterator to the lower diagonal of the matrix system.
  ** \param low_diag_last RandomAccessIterator  to the lower diagonal of the matrix system.
  ** \param b_first RandomAccessIterator to the vector of the matrix system.
  ** \param b_last RandomAccessIterator  to the vector of the matrix system.
  ** \pre (x_last - x_first) == (y_last - y_first)
  ** \pre (x_last - x_first) == (diag_last - diag_first)
  ** \pre (x_last - x_first) == (b_last - b_first)
  ** \pre (x_last - x_first) == (up_diag_last - up_diag_first) + 1
  ** \pre (x_last - x_first) == (low_diag_last - low_diag_first) + 1
  ** \pre all the range should have the same real data type
  ** \pre  (x_last - x_first) > 2
  ** \par Example:
  ** \code
  ** slip::Vector<double> xsp3(8);
  ** slip::iota(xsp3.begin(),xsp3.end(),0.0,1.0);
  ** slip::Vector<double> ysp3 = slip::sin(xsp3);
  ** 
  ** slip::Vector<double> diagsp3(xsp3.size());
  ** slip::Vector<double> up_diagsp3(xsp3.size()-1);
  ** slip::Vector<double> low_diagsp3(xsp3.size()-1);
  **
  ** slip::Vector<double> bsp3(xsp3.size());
  ** slip::cubic_spline_system_main(xsp3.begin(),xsp3.end(),
  **			            ysp3.begin(),ysp3.end(),
  **				    diagsp3.begin(),diagsp3.end(),
  ** 			            up_diagsp3.begin(),up_diagsp3.end(),
  **                                low_diagsp3.begin(),low_diagsp3.end(),
  ** 			            bsp3.begin(),bsp3.end());
  ** cubic_spline_system_boundary_not_a_knot(xsp3.begin(),xsp3.end(),
  **			                     ysp3.begin(),ysp3.end(),
  **				             diagsp3.begin(),diagsp3.end(),
  ** 			                     up_diagsp3.begin(),up_diagsp3.end(),
  **                                        low_diagsp3.begin(),low_diagsp3.end(),
  ** 			                    bsp3.begin(),bsp3.end());
  ** slip::Matrix<double> Msp3(diagsp3.size(),diagsp3.size());
  ** slip::set_diagonal(diagsp3.begin(),diagsp3.end(),Msp3);
  ** 
  ** slip::set_diagonal(up_diagsp3.begin(),up_diagsp3.end(),Msp3,1);
  ** slip::set_diagonal(low_diagsp3.begin(),low_diagsp3.end(),Msp3,-1);
  ** std::cout<<"Msp3 = \n"<<Msp3<<std::endl;
  ** slip::Vector<double> Vbsp3(bsp3.begin(),bsp3.end());
  ** std::cout<<"Vbsp3 = \n"<<Vbsp3<<std::endl;
  ** \endcode
  */

template <typename RandomAccessIterator1,
	  typename RandomAccessIterator2,
	  typename RandomAccessIterator3,
	  typename RandomAccessIterator4,
	  typename RandomAccessIterator5,
	  typename RandomAccessIterator6>
void cubic_spline_system_boundary_not_a_knot(RandomAccessIterator1 x_first,
					     RandomAccessIterator1 x_last,
					     RandomAccessIterator2 y_first,
					     RandomAccessIterator2 y_last,
					     RandomAccessIterator3 diag_first,
					     RandomAccessIterator3 diag_last,
					     RandomAccessIterator4 up_diag_first,
					     RandomAccessIterator4 up_diag_last,
					     RandomAccessIterator5 low_diag_first,
					     RandomAccessIterator5 low_diag_last,
					     RandomAccessIterator6 b_first,
					     RandomAccessIterator6 b_last)
{
  typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
  
  //x0 natural spline border
  const T h1 = (*(x_first+1) - *x_first);
  const T h2 = (*(x_first+2) - *(x_first+1));
  const T d1 = (*(y_first+1) - *y_first)/h1;
  const T d2 = (*(y_first+2) - *(y_first+1))/h2;
  const T h2d1 = h2*d1;
  *diag_first = h2;
  *up_diag_first = h1 + h2;
  *b_first = (h2d1 + h2d1) + (h1*(h2d1+h1*d2))/(*up_diag_first);

  //xn natural spline border
  const T hn = (*(x_last-1)- *(x_last-2));
  const T hnm1 = (*(x_last-2)- *(x_last-3));
  const T dn = (*(y_last-1) - *(y_last-2))/hn;
  const T dnm1 = (*(y_last-2)- *(y_last-3))/hnm1;
  const T hnm1dn = hnm1 * dn;
  *(diag_last - 1) = hnm1;
  *(low_diag_last - 1) = hn + hnm1;
  *(b_last-1) = (hnm1dn +hnm1dn) + (hn*(hnm1dn+hn*dnm1))/(*(low_diag_last - 1));
}

/*!
  ** \brief Computes the cubic spline coefficients \f$c_i\f$ solving the cubic spline system using the Thomas's algorithm.
  ** \f[ \left(\begin{array}{cccccc}
  ** d_0&u_0&0&\cdots&\cdots&0\\
  ** l_0&d_1&u_1&\ddots&&\vdots\\
  ** 0&l_1&d_2&u_2&\ddots&\vdots\\
  ** \vdots&\ddots&\ddots&\ddots&\ddots&0\\
  ** \vdots&&\ddots&l_{n-2}&d_{n-1}&u_{n-1}\\
  ** 0&\cdots&\cdots&0&l_{n-1}&d_n\\
  ** \end{array}\right)
  ** \left(
  ** \begin{array}{c}
  ** c_0\\
  ** c_1\\
  ** \vdots\\
  ** \\
  ** \vdots\\
  ** c_{n-2}\\
  ** c_{n-1}\\
  ** \end{array}\right)
  ** =
  ** \left(
  ** \begin{array}{c}
  ** b_0\\
  ** b_1\\
  ** \vdots\\
  ** \\
  ** \vdots\\
  ** b_{n-2}\\
  ** b_{n-1}\\
  ** \end{array}\right)
  ** \f]
  ** whith
  ** \f[  h_{i+1}c_{i-1}+2(h_i+h_{i+1})c_i+h_ic_{i+1} = 3(h_id_{i+1}+h_{i+1}d_i),\quad, 1 \le i \le n-1\f]
  ** with 
  ** \f[h_i = x_i - x_{i-1}\f]
  ** and
  ** \f[d_i = \frac{y_i - y_{i-1}}{h_i} \f]
  ** if bc_type == slip::CUBIC_SPLINE_BOUNDARY_NATURAL then
  ** \f[
  ** \begin{array}{cc}
  **  2c_0+c_1&=3d_1\\
  **  c_{n-1}+2c_{n}&=3d_n\\
  ** \end{array} 
  ** \f]
  ** if bc_type == slip::CUBIC_SPLINE_BOUNDARY_NOT_A_KNOT then
  ** \f[ 
  ** \begin{array}{cc}
  ** h_2c_0 + (h_1+h_2)c_1&= 2h_2d_1+\frac{h_1(h_1d_2+h_2d_1)}{h_1+h_2}\\
  ** (h_{n-1}+h_n)c_{n-1}+h_{n-1}c_n &= 2h_{n-1}d_n+\frac{h_n(h_{n-1}d_n+h_nd_{n-1}}{h_{n-1}+h_n}\\
  ** \end{array}
  ** \f]
  ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
  ** \date 2010/05/08
  ** \version 0.0.2
  ** \since 1.5.0
  ** \param x_first RandomAccessIterator to abscissas xi.
  ** \param x_last RandomAccessIterator to abscissas xi.
  ** \param y_first RandomAccessIterator to yi = f(xi). 
  ** \param y_last RandomAccessIterator to yi = f(xi). 
  ** \param coeff_first RandomAccessIterator to the coefficients of the cubic spline interpolation.
  ** \param coeff_last RandomAccessIterator  to the coefficients of the cubic spline interpolation.
  ** \param bc_type cubic spline boundary condition slip::CUBIC_SPLINE_BOUNDARY_NOT_A_KNOT or slip::CUBIC_SPLINE_BOUNDARY_NATURAL
  ** \pre (x_last - x_first) == (y_last - y_first)
  ** \pre (x_last - x_first) == (coeff_last - coeff_first)
  ** \pre all the range should have the same real data type
  ** \pre  (x_last - x_first) > 2
  ** \par Example:
  ** \code
  ** slip::Vector<double> xsp3(8);
  ** slip::iota(xsp3.begin(),xsp3.end(),0.0,1.0);
  ** slip::Vector<double> ysp3 = slip::sin(xsp3);
  ** 
  ** slip::Vector<double> coeffsp3(xsp3.size());
  ** slip::cubic_spline_coefficients(xsp3.begin(),xsp3.end(),
  **			             ysp3.begin(),ysp3.end(),
  **				     coeffsp3.begin(),coeffsp3.end(),
  **                                 slip::CUBIC_SPLINE_BOUNDARY_NATURAL);
  ** std::cout<<"coeffsp3 = \n"<<coeffsp3<<std::endl;
  ** \endcode
  */
template <typename RandomAccessIterator1,
	  typename RandomAccessIterator2,
	  typename RandomAccessIterator3>
void cubic_spline_coefficients(RandomAccessIterator1 x_first,
			       RandomAccessIterator1 x_last,
			       RandomAccessIterator2 y_first,
			       RandomAccessIterator2 y_last,
			       RandomAccessIterator3 coeff_first,
			       RandomAccessIterator3 coeff_last,
			       const slip::CUBIC_SPLINE_BOUNDARY_TYPE& bc_type= slip::CUBIC_SPLINE_BOUNDARY_NOT_A_KNOT)
{
  typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
  std::size_t N = static_cast<std::size_t>(x_last-x_first);
  slip::Array<T> up_diagsp(N-1);
  slip::Array<T> diagsp(N);
  slip::Array<T> low_diagsp(N-1);
  slip::Array<T> bsp(N);
  slip::cubic_spline_system_main(x_first,x_last,
				 y_first,y_last,
				 diagsp.begin(),diagsp.end(),
				 up_diagsp.begin(),up_diagsp.end(),
				 low_diagsp.begin(),low_diagsp.end(),
				 bsp.begin(),bsp.end());
  switch(bc_type)
    {
    case slip::CUBIC_SPLINE_BOUNDARY_NATURAL:
      slip::cubic_spline_system_boundary_natural(x_first,x_last,
						 y_first,y_last,
						 diagsp.begin(),diagsp.end(),
						 up_diagsp.begin(),up_diagsp.end(),
						 low_diagsp.begin(),low_diagsp.end(),
						 bsp.begin(),bsp.end());
      break;
    case slip::CUBIC_SPLINE_BOUNDARY_NOT_A_KNOT:
      slip::cubic_spline_system_boundary_not_a_knot(x_first,x_last,
  						y_first,y_last,
  						diagsp.begin(),diagsp.end(),
  						up_diagsp.begin(),up_diagsp.end(),
  						low_diagsp.begin(),low_diagsp.end(),
  						bsp.begin(),bsp.end());
      break;
    default:
        slip::cubic_spline_system_boundary_not_a_knot(x_first,x_last,
  						y_first,y_last,
  						diagsp.begin(),diagsp.end(),
  						up_diagsp.begin(),up_diagsp.end(),
  						low_diagsp.begin(),low_diagsp.end(),
  						bsp.begin(),bsp.end());
    };
 
  
  // slip::Matrix<double> Msp(diagsp.size(),diagsp.size());
  // slip::set_diagonal(diagsp.begin(),diagsp.end(),Msp);
  // slip::set_diagonal(up_diagsp.begin(),up_diagsp.end(),Msp,1);
  // slip::set_diagonal(low_diagsp.begin(),low_diagsp.end(),Msp,-1);
  // std::cout<<"Msp = \n"<<Msp<<std::endl;
  // slip::Vector<double> Vbsp(bsp.begin(),bsp.end());
  // std::cout<<"Vbsp = \n"<<Vbsp<<std::endl;
  slip::thomas_solve( diagsp.begin(),diagsp.end(),
  		      up_diagsp.begin(),up_diagsp.end(),
  		      low_diagsp.begin(),low_diagsp.end(),
  		      coeff_first,coeff_last,
  		      bsp.begin(),bsp.end());
   
}

// template <typename RandomAccessIterator1,
// 	  typename RandomAccessIterator2,
// 	  typename RandomAccessIterator3,
// 	  typename  = typename std::iterator_traits<RandomAccessIterator1>::value_type>
// void cubic_spline_polynomials(RandomAccessIterator1 x_first,
// 			      RandomAccessIterator1 x_last,
// 			      RandomAccessIterator2 y_first,
// 			      RandomAccessIterator2 y_last,
// 			      RandomAccessIterator3 coeff_first,
// 			      RandomAccessIterator3 coeff_last,
// 			      std::vector<slip::Polynomial<T>>& spline_polynomials)
// {
//    const std::size_t N = std::distance(x_first,x_last);
//    spline_polynomials.resize(N-1);
// }

/*!
  ** \brief Interpolates the value of \f$f(x)\f$ according to previously computed cubic spline coefficients range [coeff_first, coeff_last].
    ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
  ** \date 2010/05/08
  ** \version 0.0.2
  ** \since 1.5.0
  ** \param x_first RandomAccessIterator to abscissas xi.
  ** \param x_last RandomAccessIterator to abscissas xi.
  ** \param y_first RandomAccessIterator to yi = f(xi). 
  ** \param y_last RandomAccessIterator to yi = f(xi). 
  ** \param coeff_first RandomAccessIterator to the coefficients of the cubic spline interpolation.
  ** \param coeff_last RandomAccessIterator  to the coefficients of the cubic spline interpolation.
  ** \param x x value.
  ** \pre (x_last - x_first) == (y_last - y_first)
  ** \pre (x_last - x_first) == (coeff_last - coeff_first)
  ** \pre all the range should have the same real data type
  ** \pre  (x_last - x_first) > 2
  ** \par Example:
  ** \code
  ** slip::Vector<double> xsp3(8);
  ** slip::iota(xsp3.begin(),xsp3.end(),0.0,1.0);
  ** slip::Vector<double> ysp3 = slip::sin(xsp3);
  ** 
  ** slip::Vector<double> coeffsp3(xsp3.size());
  ** slip::cubic_spline_coefficients(xsp3.begin(),xsp3.end(),
  **			             ysp3.begin(),ysp3.end(),
  **				     coeffsp3.begin(),coeffsp3.end(),
  **                                 slip::CUBIC_SPLINE_BOUNDARY_NATURAL);
  ** std::cout<<"coeffsp3 = \n"<<coeffsp3<<std::endl; 
  ** y335 = slip::cubic_spline_eval(xsp3.begin(),xsp3.end(),
  **	 		            ysp3.begin(),ysp3.end(),
  **			            coeffsp3.begin(),coeffsp3.end(),
  **                                3.35);
  ** std::cout<<"f(3.35) = "<<y335<<std::endl;
  ** \endcode
  ** 
  */
template <typename RandomAccessIterator1,
	  typename RandomAccessIterator2,
	  typename RandomAccessIterator3,
	  typename T,
	  typename R = double>
T cubic_spline_eval(RandomAccessIterator1 x_first,
		    RandomAccessIterator1 x_last,
		    RandomAccessIterator2 y_first,
		    RandomAccessIterator2 y_last,
		    RandomAccessIterator3 coeff_first,
		    RandomAccessIterator3 coeff_last,
		    const T& x)
 {
   const std::size_t N = std::distance(x_first,x_last);
   assert(std::distance(x_first,x_last) == N);
   assert(std::distance(coeff_first,coeff_last) == N);
   std::size_t ilow = 0;
   std::size_t ihigh  = 0;
   R result = R();
   if((x < *x_first) || (x > *(x_last-1)))
     {
       //out of range
       throw std::range_error("x out of range");
     }
   else if(x == *x_first)
     {
       result = *y_first;
     }
   else if(x == *(x_last-1))
     {
       typename std::iterator_traits<RandomAccessIterator1>::difference_type N = std::distance(x_first,x_last);
       ilow =  N - 1;
       ihigh  = ilow;
       result = *(y_last-1);
     }
   else
     {
     
       //find x range
       RandomAccessIterator1 lower = std::lower_bound(x_first,x_last,x,std::less_equal<T>());
       ihigh  = std::distance(x_first,lower);
       ilow = ihigh - 1;
       const T xi = *(x_first + ilow);
       const T xip1 = *(x_first + ihigh);
       const T yi = *(y_first + ilow);
       const T yip1 = *(y_first + ihigh);
       const T ki = *(coeff_first + ilow);
       const T kip1 = *(coeff_first + ihigh);
       const T hi = (xip1 - xi);
       const T hi2 = hi*hi;
       const T di = (yip1 - yi)/hi;
       const T mu = (x - xi);
       //std::cout<<"x = "<<x<<" - ["<<ilow<<","<<ihigh<<"] - ["<<xi<<","<<xip1<<"] - (yi,yi+1) = ["<<yi<<","<<yip1<<"]"<<std::endl;
       //std::cout<<"(ki,ki+1) = ["<<ki<<","<<kip1<<"]"<<std::endl;
       // const T one_m_mu = slip::constants<T>::one() - mu;
       // const T ai = ki*hi    - yip1_m_yi;
       // const T bi = -kip1*hi + yip1_m_yi;
       // //std::cout<<"ai = "<<ai<<" bi = "<<bi<<std::endl;
     

       const T a0 = yi;
       const T a1 = ki;
      
       const T a3 = (ki + kip1 - slip::constants<T>::two()*di)/hi2;
       //const T a2 = (di - ki)/hi - a3*hi;
       const T a2 = (static_cast<T>(3.0)*di-slip::constants<T>::two()*ki-kip1)/hi;
       //const T a2 = (yip1_m_yi/hi - ki - a3)/hi;
       std::cout<<"poly: "<<a0<<" "<<a1<<" "<<a2<<" "<<a3<<std::endl;
       result = a0 + mu *(a1 + mu*(a2 + mu*a3));
     }
   return result;
 }
/*!
  ** \brief Interpolates the value of \f$f(x)\f$ in a range \f$a = x_0 < \cdots < x_n\f$ at the \f$u_j\f$ of the range [xx_first,xx_last).
  ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
  ** \date 2010/05/08
  ** \version 0.0.2
  ** \since 1.5.0
  ** \param x_first RandomAccessIterator to abscissas xi.
  ** \param x_last RandomAccessIterator to abscissas xi.
  ** \param y_first RandomAccessIterator to yi = f(xi). 
  ** \param y_last RandomAccessIterator to yi = f(xi). 
  ** \param xx_first RandomAccessIterator to abscissas xxi.
  ** \param xx_last RandomAccessIterator to abscissas xxi.
  ** \param yy_first RandomAccessIterator to yyi = f(xxi). 
  ** \param yy_last RandomAccessIterator to yyi = f(xxi).
  ** \pre (x_last - x_first) == (y_last - y_first)
  ** \pre (xx_last - xx_first) == (yy_last - yy_first)
  ** \par Example:
  ** \code 
  ** slip::Vector<double> xsp3(8);
  ** slip::iota(xsp3.begin(),xsp3.end(),0.0,1.0);
  ** slip::Vector<double> ysp3 = slip::sin(xsp3);
  ** 
  ** slip::Vector<T> xxsp3(29);
  ** slip::iota(xxsp3.begin(),xxsp3.end(),xsp3[0],0.25);
  ** std::cout<<"xxsp3 = \n"<<xxsp3<<std::endl;
  ** slip::Vector<T> yysp3(xxsp3.size());
  ** slip::cubic_spline(xsp3.begin(),xsp3.end(),
  **		        ysp3.begin(),ysp3.end(),
  **		        xxsp3.begin(),xxsp3.end(),
  **		        yysp3.begin(),yysp3.end());
  **
  ** std::cout<<"yysp3 = \n"<<yysp3<<std::endl;
  ** \endcode
  */
template <typename RandomAccessIterator1,
	  typename RandomAccessIterator2,
	  typename RandomAccessIterator3,
	  typename RandomAccessIterator4>
void cubic_spline(RandomAccessIterator1 x_first,
		  RandomAccessIterator1 x_last,
		  RandomAccessIterator2 y_first,
		  RandomAccessIterator2 y_last,
		  RandomAccessIterator3 xx_first,
		  RandomAccessIterator3 xx_last,
		  RandomAccessIterator4 yy_first,
		  RandomAccessIterator4 yy_last)
 {
   assert(std::distance(x_first,x_last) == std::distance(y_first,y_last));
   assert(std::distance(xx_first,xx_last) == std::distance(yy_first,yy_last));
   
   typedef typename std::iterator_traits<RandomAccessIterator1>::value_type T;
   slip::Array<T> spline_coeff(std::distance(x_first,x_last));
   
   slip::cubic_spline_coefficients(x_first,x_last,
				   y_first,y_last,
				   spline_coeff.begin(),spline_coeff.end());
   std::cout<<"spline_coeff = \n"<<spline_coeff<<std::endl;
   for(; xx_first != xx_last; ++xx_first, ++yy_first)
     {
       *yy_first = slip::cubic_spline_eval(x_first,x_last,
					   y_first,y_last,
					   spline_coeff.begin(),spline_coeff.end(),*xx_first);
     }
   
 }


// template <typename R,
// 	  typename RandomAccessIterator2,
// 	  typename RandomAccessIterator3,
// 	  typename RandomAccessIterator4,
// 	  typename RandomAccessIterator5>
// void regular_cubic_spline_system(const R& x_step,
// 				 RandomAccessIterator2 y_first,
// 				 RandomAccessIterator2 y_last,
// 				 RandomAccessIterator3 diag_first,
// 				 RandomAccessIterator3 diag_last,
// 				 RandomAccessIterator4 diag2_first,
// 				 RandomAccessIterator4 diag2_last,
// 				 RandomAccessIterator5 b_first,
// 				 RandomAccessIterator5 b_last)
// {
//   // typedef typename std::iterator_traits<RandomAccessIterator3>::value_type T;
//   // std::fill(diag_first,diag_last,static_cast<T>(4.0) * static_cast<T>(x_step));
//   // std::fill(diag2_first,diag2_last,static_cast<T>(2.0)*static_cast<T>(x_step));

//   // RandomAccessIterator2 it_y = y_first + 1;
//   // const T six = static_cast<T>(6.0);
//   // const T c = six / static_cast<T>(x_step);
//   // for(;b_first != b_last; 
//   //      ++b_first,
//   //      ++it_y)
//   //   {
//   //     *b_first = c * (*(it_y + 1) - slip::constants<T>::two()* *it_y +  *(it_y - 1)) ;
//   //   }
//   // typedef typename std::iterator_traits<RandomAccessIterator3>::value_type T;
//   // std::fill(diag_first,diag_last,static_cast<T>(4.0) * static_cast<T>(x_step));
//   // std::fill(diag2_first,diag2_last,static_cast<T>(2.0)*static_cast<T>(x_step));

//   // RandomAccessIterator2 it_y = y_first + 1;
//   // const T c = static_cast<T>(3.0);
//   // for(;b_first != b_last; 
//   //      ++b_first,
//   //      ++it_y)
//   //   {
//   //     *b_first = c * (*(it_y + 1) -  *(it_y - 1)) ;
//   //   }
//   typedef typename std::iterator_traits<RandomAccessIterator2>::value_type T;
//    const T three_o_x_step = static_cast<T>(3.0)/x_step;
//   //x0 natural spline border

//   *diag_first = slip::constants<T>::two();
//   *diag2_first = slip::constants<T>::one();
//   *b_first = three_o_x_step*(*(y_first+1)- *y_first);
  
//   //RandomAccessIterator1 it_x = x_first + 1;
//   RandomAccessIterator2 it_y = y_first + 1;
//   RandomAccessIterator3 it_diag = diag_first + 1;
//   RandomAccessIterator3 it_diag_end = diag_last - 1;
//   RandomAccessIterator4 it_diag2 = diag2_first + 1;
//   RandomAccessIterator5 it_b = b_first+1;
//   //diagonal
//   for(; it_diag!=it_diag_end; 
//       ++it_diag,++it_diag2,++it_b)
//     {
//       //cubic spline matrix
//       *it_diag  = (x_step + x_step) + (x_step + x_step);
//       *it_diag2 = x_step;
//       //cubic spline vector
//       *it_b = three_o_x_step * (*(it_y + 1) - *(it_y - 1)) ;
    
//     }
//   //xn natural spline border
//   *(diag_last - 1) = slip::constants<T>::two();
//   *(diag2_last - 1) = slip::constants<T>::one();
//   *(b_last-1) = three_o_x_step*(*(y_last-1)- *(y_last-2));
// }

// template <typename R,
// 	  typename RandomAccessIterator2,
// 	  typename RandomAccessIterator3>
// void cubic_spline_coefficients(const R& x_step,
// 			       RandomAccessIterator2 y_first,
// 			       RandomAccessIterator2 y_last,
// 			       RandomAccessIterator3 coeff_first,
// 			       RandomAccessIterator3 coeff_last)
// {
//   typedef typename std::iterator_traits<RandomAccessIterator2>::value_type T;
//   std::size_t N = static_cast<std::size_t>(y_last-y_first);
//   slip::Array<T> diagsp(N-1);
//   slip::Array<T> diag2sp(N);
//   slip::Array<T> bsp(N-1);
//   slip::cubic_spline_system(x_step,
// 				 y_first,y_last,
// 				 diagsp.begin(),diagsp.end(),
// 				 diag2sp.begin(),diag2sp.end(),
// 				 bsp.begin(),bsp.end());
//   // slip::cubic_spline_system_boundary_natural(x_step,
//   // 					     y_first,y_last,
//   // 					     diagsp.begin(),diagsp.end(),
//   // 					     diag2sp.begin(),diag2sp.end(),
//   // 					     bsp.begin(),bsp.end());
//    // slip::cubic_spline_system_boundary_not_a_knot(x_step,
//    // 						 y_first,y_last,
//    // 						 diagsp.begin(),diagsp.end(),
//    // 						 diag2sp.begin(),diag2sp.end(),
//    // 						 bsp.begin(),bsp.end());

 
  
//   slip::thomas_solve( diagsp.begin(),diagsp.end(),
// 		      diag2sp.begin(),diag2sp.end(),
// 		      diag2sp.begin(),diag2sp.end(),
// 		      coeff_first,coeff_last,
// 		      bsp.begin(),bsp.end());
   
// }



}//slip::


#endif //SLIP_INTERPOLATION_HPP
