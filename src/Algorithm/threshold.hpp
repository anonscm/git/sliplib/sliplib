/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file threshold.hpp
 * 
 * \brief Provides some threshold and binarization algorithms.
 * 
 */
#ifndef SLIP_THRESHOLD_HPP
#define SLIP_THRESHOLD_HPP

#include <iterator>
#include <algorithm>
#include <numeric>
#include <cassert>
#include <cmath>

namespace slip
{

   /**
   ** \brief Functor object used to binarize a value
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/09/13
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param t_value Threshold value 
   ** \param false_value False return value 
   ** \param true_value True return value 
   **
   */
  template <typename InType, typename  OutType>
  struct binarize_fun
  {
    binarize_fun(const InType& t_value, 
		 const OutType& false_value,
		 const OutType& true_value
		 ):
      t_value_(t_value),false_value_(false_value),true_value_(true_value)
    {}


    OutType operator()(const InType& val)
    {
      return (val <= t_value_) ? false_value_ : true_value_;
    }

    InType t_value_;
    OutType false_value_;
    OutType true_value_;
    
  };

   /**
   ** \brief Functor object used to threshold a value
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/09/13
   ** \version 0.0.1
   ** \param t_value Threshold value 
   ** \param false_value False return value 
   **
   */
  template <typename InType, typename  OutType>
  struct threshold_fun
  {
    threshold_fun(const InType& t_value, 
		  const OutType& false_value):
      t_value_(t_value),false_value_(false_value)
    {}
    OutType operator()(const InType& val)
    {
      return (val <= t_value_) ?  false_value_ : OutType(val);
    }

    InType t_value_;
    OutType false_value_;
  };

  /**
   ** \brief Functor object used to double-threshold a value
   ** \author Denis Arivault <arrivault_AT_sic.univ-poitiers.fr>
   ** \date 2008/07/21
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param min_t_value Minimum threshold value 
   ** \param max_t_value Maximum threshold value
   ** \param false_value False return value 
   **
   */
  template <typename InType, typename  OutType>
  struct db_threshold_fun
  {
    db_threshold_fun(const InType& min_t_value,const InType& max_t_value,
		     const OutType& false_value):
      min_t_value_(min_t_value),max_t_value_(max_t_value),false_value_(false_value)
    {}
    OutType operator()(const InType& val)
    {
      return ((val >= min_t_value_)&&(val <= max_t_value_)) ?  false_value_ : OutType(val);
    }

    InType min_t_value_;
    InType max_t_value_;
    OutType false_value_;
  };

  /*!
  ** \brief binarize algorithm
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2006/09/14
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first begin InputIterator of the range to binarize
  ** \param last past-the-end InputIterator of the range to binarize
  ** \param result begin OutputIterator of the resulting range
  ** \param t_value binarize value
  ** \param false_value result value when *first <= t_value
  ** \param true_value result value when *first > t_value
  ** \pre [first, last) must be valid.
  ** \pre [result, result + (last-first)) must be valid.
  ** \par Example:
  ** \code
  **  //values less than 8 are set to 2 other are set to 266
  **  slip::Matrix<float> M(4,5);
  **  slip::iota(M.begin(),M.end(),0.0,1.0);
  **  slip::Matrix<int> Result(4,5);
  **  slip::binarize(M.begin(),M.end(),Result.begin(),8.0f,2,255);
  ** \endcode
  */
  template<typename InputIterator, typename OutputIterator>
  inline
  void binarize(InputIterator first,
		InputIterator last,
		OutputIterator result,
		const typename std::iterator_traits<InputIterator>::value_type& t_value,
		const typename std::iterator_traits<OutputIterator>::value_type& false_value,
		const typename std::iterator_traits<OutputIterator>::value_type& true_value)	 
  {
    assert(first != last);
    
    slip::binarize_fun<
      typename std::iterator_traits<InputIterator>::value_type, 
      typename std::iterator_traits<OutputIterator>::value_type>  
      t_fun(t_value,false_value,true_value);
    
    std::transform(first,last,result,t_fun);
  }

 /*!
  ** \brief threshold algorithm
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2006/12/10
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first begin InputIterator of the range to threshold
  ** \param last past-the-end InputIterator of the range to threshold
  ** \param result begin OutputIterator of the resulting range
  ** \param t_value threshold value
  ** \param false_value result value when *first <= t_value
  ** \pre [first, last) must be valid.
  ** \pre [result, result + (last-first)) must be valid.
  ** \par Example:
  ** \code
  **  //values less than 120 are set to 0
  ** slip::GrayscaleImage<unsigned char> I1;
  ** I1.read("lena.gif");
  ** slip::GrayscaleImage<unsigned char> I2(I1.dim1(),I1.dim2());
  ** slip::threshold(I1.begin(),I1.end(),I2.begin(),120,0);
  ** I2.write("lena_threshold.gif");
  ** \endcode
  */
  template<typename InputIterator, typename OutputIterator>
  inline
  void threshold(InputIterator first,
		 InputIterator last,
		 OutputIterator result,
		 const typename std::iterator_traits<InputIterator>::value_type& t_value,
		 const typename std::iterator_traits<OutputIterator>::value_type& false_value)	 
  {
    assert(first != last);
    
    slip::threshold_fun<
      typename std::iterator_traits<InputIterator>::value_type, 
      typename std::iterator_traits<OutputIterator>::value_type>  
      t_fun(t_value,false_value);
    
    std::transform(first,last,result,t_fun);
  }

  /*!
  ** \brief double-threshold algorithm
  ** \author Denis Arrivault<arrivault_AT_sic.univ-poitiers.fr>
  ** \date 2008/07/21
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first begin InputIterator of the range to threshold
  ** \param last past-the-end InputIterator of the range to threshold
  ** \param result begin OutputIterator of the resulting range
  ** \param min_t_value minimum threshold value
  ** \param max_t_value maximum threshold value
  ** \param false_value result value when min_t_value <= *first <= max_t_value
  ** \pre [first, last) must be valid.
  ** \pre [result, result + (last-first)) must be valid.
  ** \par Example:
  ** \code
  **  //values less than 120 and more than 40 are set to 0
  ** slip::GrayscaleImage<unsigned char> I1;
  ** I1.read("lena.gif");
  ** slip::GrayscaleImage<unsigned char> I2(I1.dim1(),I1.dim2());
  ** slip::db_threshold(I1.begin(),I1.end(),I2.begin(),40,120,0);
  ** I2.write("lena_threshold.gif");
  ** \endcode
  */
  template<typename InputIterator, typename OutputIterator>
  inline
  void db_threshold(InputIterator first,
		    InputIterator last,
		    OutputIterator result,
		    const typename std::iterator_traits<InputIterator>::value_type& min_t_value,
		    const typename std::iterator_traits<InputIterator>::value_type& max_t_value,
		    const typename std::iterator_traits<OutputIterator>::value_type& false_value)	 
  {
    assert(first != last);
    
    slip::db_threshold_fun<
    typename std::iterator_traits<InputIterator>::value_type, 
      typename std::iterator_traits<OutputIterator>::value_type>  
    t_fun(min_t_value,max_t_value,false_value);
  
  std::transform(first,last,result,t_fun);
}


 /*!
  ** \brief multi_threshold algorithm
  ** It is equivalent to a quantification  J[i] = Level[i] if T[i-1] < I[i] <= T[i] 
  ** \author Mouhamed Hammoud <hammoud_AT_sic.univ-poitiers.fr>
  ** \date 2010/11/02
  ** \since 1.0.0
  ** \version 0.0.2
  ** \param first InputIterator to the input data. 
  ** \param last  InputIterator  to the input data. 
  ** \param first_th RandomAccessIterator to the threshold data.
  ** \param last_th  RandomAccessIterator to the threshold data.
  ** \param first_level InputIterator to the level data.
  ** \param last_level  InputIterator to the level data.
  ** \param result_first OutputIterator to the output data. 
  ** \param result_last  OutputIterator to the output data. 
  
  ** \pre (last_th - first_th) == (last_level - first_level)
  ** \pre (last - first) == (result_last - result_first)
  ** \par Example:
  ** \code
  ** slip::Array2d<double> M(4,5);
  ** slip::iota(M.begin(),M.end(),1.0,1.0);
  ** M[0][4] = 18.0;
  ** M[3][3] = 2.0;
  ** M[3][0] = 5.0;
  ** M[1][2] = 20.0;
  ** std::cout<<"M=\n"<<M<<std::endl;
  ** int f1[]={5,10,15,20};
  ** int f2[]={1,2,3,4};
  ** slip::Array<int> threshold(4,f1);
  ** slip::Array<int> level(4,f2);
  ** std::cout<<"threshold = \n"<<threshold<<std::endl;
  ** std::cout<<"level = \n"<<level<<std::endl;
  ** slip::Array2d<double> M_res(M.rows(),M.cols());
  ** slip::multi_threshold(M.begin(),M.end(),
  **                       threshold.begin(),threshold.end(),
  **			   level.begin(),level.end(),
  **		           M_res.begin(),M_res.end());
  ** std::cout<<"M_res=\n"<<M_res<<std::endl;
  ** 
  ** 
  ** \endcode
  ** \todo optimize
  */
  template<typename InputIterator1,
	   typename RandomAccessIterator1,
	   typename RandomAccessIterator2,
	   typename OutputIterator>
  void multi_threshold(InputIterator1 first, 
		       InputIterator1 last,
		       RandomAccessIterator1 first_th, 
		       RandomAccessIterator1 last_th, 
		       RandomAccessIterator2 first_level,
		       RandomAccessIterator2 last_level,
		       OutputIterator result_first,
		       OutputIterator result_last)
  {

    assert((last_th-first_th) == (last_level-first_level));
    assert((last - first) == (result_last - result_first));

    RandomAccessIterator1 tmp_t = first_th;
    RandomAccessIterator2 tmp_l = first_level;

    for(;first!=last; ++first, ++result_first)
      { 
	for ( ; first_th != last_th ; ++first_th, ++first_level)
	  {
	    if (*first <= *tmp_t) 
              { 
                *result_first = *tmp_l;
              }
           
	    else if((*(first_th-1) < *first) && (*first <= *first_th))  
              {
                *result_first = *first_level;
              }
	    else if( *first >  *(last_th - 1))
	      {
		*result_first = *(last_level - 1);
	      }
	  }
	first_th = tmp_t;
	first_level = tmp_l;
      }
  }

 
}//slip::


#endif //SLIP_THRESHOLD_HPP
