/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



#ifndef DYNAMIC_HPP
#define DYNAMIC_HPP

/** 
 * \file dynamic.hpp
 * 
 * \brief Provides algorithms and functors to change the dynamic of ranges.
 * \since 1.0.0
 */
#include <algorithm>
#include <cassert>
#include <cmath>
#include "stl_algo_ext.hpp"
#include "statistics.hpp"

namespace slip
{

  /** 
   ** \enum NORMALIZATION_FUNCTION
   ** \brief Choose between different border treatment modes for convolution 
   ** algorithms 
   ** \since 1.0.0   
   ** \code
    enum NORMALIZATION_FUNTION 
    {
    // T([a,b]) = [0,1], with T an affine transformation
    AFFINE_FUNCTION, 

    //
    NORMAL_FUNCTION,

    //
    SIGMOID_FUNCTION

    };
    \endcode
*/
enum NORMALIZATION_FUNCTION
{
   AFFINE_FUNCTION, 
   NORMAL_FUNCTION, 
   SIGMOID_FUNCTION
};



  /** \struct range_fun_interab
   ** \brief Functor object uses to change the range of container according to an affine transformation
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \version 0.0.1
   ** \since 1.0.0
   ** \param in_min minimum value of the input
   ** \param in_max maximum value of the input
   ** \param out_min minimum value of the output
   ** \param out_max maximum value of the output
   **
   */ 
 template <typename InType, typename  OutType>
  struct range_fun_interab
  {
    range_fun_interab(const InType& in_min,
		      const InType& in_max,
		      const OutType& out_min,
		      const OutType& out_max):
      in_min_(in_min),in_max_(in_max),out_min_(out_min),out_max_(out_max)
    {}

    OutType operator()(const InType& val)
    {
      double d = in_max_ - in_min_;
      double a = out_max_ - out_min_;
      double b = (in_max_ * out_min_) -  (in_min_ * out_max_); 
      return OutType((a * val + b) / d);
    }

    InType in_min_;
    InType in_max_;
    OutType out_min_;
    OutType out_max_;
  };

  
  /** \struct range_fun_inter01
   ** \brief Functor object uses to change the range of container into [0,1]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_min minimum value of the input
   ** \param in_max maximum value of the input
   **
   */ 
  template <typename InType, typename  Real>
  struct range_fun_inter01
  {
    range_fun_inter01(const InType& in_min,
		      const InType& in_max):
      in_min_(in_min),in_max_(in_max)
    {}

    Real operator()(const InType& val)
    {
      Real d = Real(in_max_ - in_min_);
      return Real(val - in_min_) / d;
    }
    InType in_min_;
    InType in_max_;
  };


 /** \struct range_fun_inter0255
   ** \brief Functor object uses to change the range of container into [0,255]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_min minimum value of the input
   ** \param in_max maximum value of the input
   **
   */ 
  template <typename InType, typename  OutType>
  struct range_fun_inter0255
  {
    range_fun_inter0255(const InType& in_min,
			const InType& in_max):
      in_min_(in_min),in_max_(in_max)
    {}

    OutType operator()(const InType& val)
    {
      return OutType(255 * ((val - in_min_) / (in_max_ - in_min_)));
    }
    InType in_min_;
    InType in_max_;
  };

 /**  \struct range_fun_inter0b
   ** \brief Functor object uses to change the range of container into [0,b]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_min minimum value of the input
   ** \param in_max maximum value of the input
   ** \param out_max maximum value of the output
   **
   */ 
  template <typename InType, typename  OutType>
  struct range_fun_inter0b
  {
    range_fun_inter0b(const InType& in_min,
		      const InType& in_max,
		      const OutType& out_max):
      in_min_(in_min),in_max_(in_max),out_max_(out_max)
    {}

    OutType operator()(const InType& val)
    {
      double d = in_max_ - in_min_;
      return OutType(out_max_ * (double(val) - in_min_) / d);
    }
    InType in_min_;
    InType in_max_;
    OutType out_max_;
  };

 /** \struct range_fun_normal
   ** \brief Functor object uses to change the range of container applying normal distribution
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_mean mean of the input
   ** \param in_sigma std_dev of the input
   ** \note result values may be outside [0.0,1.0]
   **
   */ 
  template <typename InType, typename Real, typename  RealOut>
  struct range_fun_normal
  {
    range_fun_normal(const Real& in_mean,
		     const Real& in_sigma):
      in_mean_(in_mean),in_sigma_(in_sigma)
    {}

    RealOut operator()(const InType& val)
    {
      double d = (double(val) - double(in_mean_)) / double(in_sigma_);
      return RealOut(d);
    }
    Real in_mean_;
    Real in_sigma_;
  };
  
 
 /** \struct range_fun_sigmoide
   ** \brief Functor object uses to change the range of container applying sigmoide distribution
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_mean mean of the input
   ** \param in_sigma std_dev of the input
   **
   */ 
  template <typename InType, typename  Real>
  struct range_fun_sigmoide
  {
    range_fun_sigmoide(const Real& in_mean,
		       const Real& in_sigma):
      in_mean_(in_mean),in_sigma_(in_sigma)
    {}

    Real operator()(const InType& val)
    {
      return 1.0 / (1.0 + std::exp((in_mean_ - val) / in_sigma_ ));
    }
    Real in_mean_;
    Real in_sigma_;
  };

 /** \struct range_fun_sigmoideb
   ** \brief Functor object uses to change the range of container applying sigmoide distribution between [0,b]
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2006/07/29
   ** \since 1.0.0
   ** \version 0.0.1
   ** \param in_mean mean of the input
   ** \param in_sigma std_dev of the input
   ** \param b maximum value
   **
   */ 
  template <typename InType, typename  Real, typename OutType>
  struct range_fun_sigmoideb
  {
    range_fun_sigmoideb(const Real& in_mean,
			const Real& in_sigma,
			const OutType& b):
      in_mean_(in_mean),in_sigma_(in_sigma),b_(b)
    {}

    OutType operator()(const InType& val)
    {
      return OutType(b_ / (1.0 + std::exp((in_mean_ - val) / in_sigma_)));
    }
    Real in_mean_;
    Real in_sigma_;
    OutType b_;
  };


  
  /*!
  ** \brief Changes the dynamic of a container
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \date 2006/10/05
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first begin iterator of the input container 
  ** \param last past-the-end iterator of the input container
  ** \param result begin iterator of the result container
  ** \param dynamic_fun Unary function 
  ** \pre containers must have the same sizes. 
  ** \par Example 1:
  ** \code
  ** slip::Matrix<float> M(10,10);
  ** slip::iota(M.begin(,M.end(),1.0f,1.0f);
  ** slip::Matrix<int> Result(10,10);
  ** slip::range_fun_inter0255<float,int> fun(M.min(),M.max());
  ** slip::change_dynamic(M.begin(),M.end(),Result.begin(),fun);
  ** std::cout<<Result<<std::endl;
  ** \endcode
  ** \par Example2:
  ** \code
  ** slip::Matrix<double> M(10,10);
  ** slip::iota(M.begin(,M.end(),1.0,1.0);
  ** slip::Matrix<int> Result(10,10);
  ** slip::range_fun_interab<double,int> funab(M.min(),M.max(),0,200);
  ** slip::change_dynamic(M.begin(),M.end(),Result.begin(),funab);
  ** std::cout<<Result<<std::endl;
  ** \endcode
  */
  template<typename InputIterator, 
	   typename OutputIterator, 
	   typename UnaryOperation>
  inline
  void change_dynamic(InputIterator first,
		     InputIterator last,
		     OutputIterator result,
		     UnaryOperation dynamic_fun)
  {
    std::transform(first,last,result,dynamic_fun);  
  }
  
/*!
  ** \brief Changes the dynamic of a container according to a mask sequence
  ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
  ** \date 2008/12/16
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first An InputIterator.
  ** \param last  An InputIterator.
  ** \param result   An output iterator.
  ** \param mask_first An input iterator.
  ** \param dynamic_fun Unary function 
  ** \param  value true value of the mask range. Default is 1.
  ** \pre containers must have the same sizes. 
  ** \par Example 1:
  ** \code
  ** slip::Matrix<float> M(10,10);
  ** slip::iota(M.begin(,M.end(),1.0f,1.0f);
  ** slip::Matrix<int> Result(10,10);
  ** slip::Matrix<float> Mask(10,10,1);
  ** 
  ** slip::range_fun_inter0255<float,int> fun(M.min(),M.max());
  ** slip::change_dynamic_mask(M.begin(),M.end(),Mask.begin(),Result.begin(),fun,1);
  ** std::cout<<Result<<std::endl;
  ** \endcode
  ** \par Example2:
  ** \code
  ** slip::Matrix<double> M(10,10);
  ** slip::iota(M.begin(,M.end(),1.0,1.0);
  ** slip::Matrix<int> Result(10,10);
  ** slip::Matrix<float> Mask(10,10,1);
  ** 
  ** slip::range_fun_interab<double,int> funab(M.min(),M.max(),0,200);
  ** slip::change_dynamic_mask(M.begin(),M.end(),Mask.begin(),Result.begin(),funab,1);
  ** std::cout<<Result<<std::endl;
  ** \endcode
  */
template<typename InputIterator, 
	   typename OutputIterator,
           typename MaskIterator,
	   typename UnaryOperation>
inline
  void change_dynamic_mask(InputIterator first,
		           InputIterator last,
		           MaskIterator mask_first,
                           OutputIterator result,
		           UnaryOperation dynamic_fun,
                           typename std::iterator_traits<MaskIterator>::value_type value=typename            std::iterator_traits<MaskIterator>::value_type(1))
  {
    slip::transform_mask_un(first,last,mask_first,result,dynamic_fun,value);  
  }
/*!
  ** \brief Changes the dynamic of a container according to a predicate
  ** \author Hammoud Mouhamed <hammoud_AT_sic.univ-poitiers.fr>
  ** \date 2008/12/16
  ** \since 1.0.0
  ** \version 0.0.1
  ** \param first An InputIterator.
  ** \param last  An InputIterator.
  ** \param result   An output iterator.
  ** \param pred predicate.
  ** \param dynamic_fun Unary function 
  ** \pre containers must have the same sizes. 
  ** \par Example1:
  ** \code
  ** template<typename T>
  **    bool lt5Predicate (const T& val)
  **    {
  **      return (val < T(250));
  **    }
  ** \endcode
  ** \code
  ** slip::Matrix<float> M(10,10);
  ** slip::iota(M.begin(,M.end(),1.0f,1.0f);
  ** slip::Matrix<int> Result(10,10);
  ** slip::range_fun_inter0255<float,int> fun(M.min(),M.max());
  ** slip::change_dynamic_if(M.begin(),M.end(),Result.begin(),lt5Predicate<float>,fun);
  ** std::cout<<Result<<std::endl;
  ** \endcode
  ** \par Example2:
  ** \code
  ** template<typename T>
  **    bool lt5Predicate (const T& val)
  **    {
  **      return (val < T(250));
  **    }
  ** \endcode
  ** \code
  ** slip::Matrix<float> M(10,10);
  ** slip::iota(M.begin(,M.end(),1.0f,1.0f);
  ** slip::Matrix<int> Result(10,10);
  ** slip::range_fun_interab<double,int> funab(M.min(),M.max(),0,200);
  ** slip::change_dynamic_if(M.begin(),M.end(),Result.begin(),lt5Predicate<float>,funab);
  ** std::cout<<Result<<std::endl;
  ** \endcode
  */
template<typename InputIterator, 
	   typename OutputIterator,
	   typename UnaryOperation,typename Predicate>
inline
  void change_dynamic_if(InputIterator first,
		     InputIterator last,
		     OutputIterator result,
                     Predicate pred,
		     UnaryOperation dynamic_fun)
  {
    slip::transform_if(first,last,result,dynamic_fun,pred);  
  } 

 /*!
 ** \brief Changes the dynamic of a container
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \date 2006/12/10
 ** \since 1.0.0
 ** \version 0.0.1
 ** \param first InputIterator on the container.
 ** \param last  InputIterator on the container.
 ** \param result OutputIterator on the result container.
 ** \param fun NORMALIZATION_FUNCTION : AFFINE_FUNCTION,SIGMOID_FUNCTION   
 ** \pre containers must have the same sizes 
 ** \par Example:
 ** \code
 ** slip::GrayscaleImage<unsigned char> I;
 ** I.read("lena.gif");
 ** slip::GrayscaleImage<float> I2(I.rows(),I.cols());
 ** slip::change_dynamic_01(I.begin(),I.end(),I2.begin(),SIGMOID_FUNCTION);
 ** \endcode
 */
  template<typename InputIterator, 
	   typename OutputIterator>
  inline
  void change_dynamic_01(InputIterator first,
			 InputIterator last,
			 OutputIterator result,
			 slip::NORMALIZATION_FUNCTION fun)

  {
    typedef typename std::iterator_traits<InputIterator>::value_type in_type;
    typedef typename std::iterator_traits<OutputIterator>::value_type out_type;
    
    switch (fun)
      {
      case AFFINE_FUNCTION: 
	slip::change_dynamic(first,last,
			     result,
			     slip::range_fun_inter01<in_type,out_type> 
			     (*std::min_element(first,last),
			      *std::max_element(first,last)));
	break;
      case SIGMOID_FUNCTION:
	{ 
	double mean = slip::mean<double>(first,last);
	double sigma = slip::std_dev<double>(first,last,mean);
	slip::change_dynamic(first,last,
			     result,
			     slip::range_fun_sigmoide<in_type,out_type>
			     (mean,sigma));
	
	break;
	}
      default : 
	slip::change_dynamic(first,last,
			     result,
			     slip::range_fun_inter01<in_type,out_type> 
			     (*std::min_element(first,last),
			      *std::max_element(first,last)));
      }
  }
  


}//slip::


#endif //DYNAMIC_HPP
