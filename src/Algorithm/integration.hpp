#ifndef SLIP_INTEGRATION_HPP
#define SLIP_INTEGRATION_HPP

/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file integration.hpp
 * \brief Provides numerical integration algorithms.
 * \since 1.5.0
 * 
 */
#include <iostream>
#include "Array.hpp"
#include "arithmetic_op.hpp"
#include "Polynomial.hpp"

namespace slip
{

  /**
   ** \brief Closed newton cotes integration algorithm on the real range [a,b].
   ** It is assumed that the value of a function f defined on \f$[a,b]\f$ is known at \f$n+1\f$ equally spaces points: \f$a=x_0 < x_1 < \cdots<x_n=b \f$. 
   ** The Newton-Cotes formulas of order n is defined as:
   ** \f[\int_a^b f(x)\,dx = \sum_{i=0}^n f(x_i)\f] where
   ** \f$ x_i = a + ih\f$ with \f$h = \frac{b-a}{n} \f$ the step size.
   ** The weight \f$w_i\f$ can be computed as the Lagrange basis polynomials:
   ** \f[\int_a^b f(x)\,dx \simeq \int_a^b L(x)\,dx = \int_a^b\left(\sum_{i=0}^n f(x_i)l_i(x) \right)\,dx = \sum_{i=0}^n f(x_i)\int_a^b l_i(x)\,dx\f] where
   ** \f[w_i = \int_a^b l_i(x)\]
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr> : conceptor
   ** \date 2024/04/02
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param a. First value of the interval of integration.
   ** \param b. Second value of the interval of integration.
   ** \param order. Order or degree of the integration method.
   ** \param fun. Unary functor of the function f to integrate.
   ** \pre a < b
   ** \pre 0 <= order <= 10
   ** \par Functor example:
   ** \code
   ** template<typename T = double>
   ** struct Fun2:public std::unary_function<T,T>
   ** {
   **
   ** T operator()(const T& x)
   ** {
   **   return (x*x)*std::exp(-static_cast<T>(2.0)*x);
   ** }
   ** 
   **};
   ** \endcode
   ** \par Example:
   ** \code
   ** typedef double T;
   ** T a = -1.0;
   ** T b = 3.0;
   ** T good_value_fun2 = static_cast<T>(1.83177182362850);
   ** std::cout<<"Integration of x^2exp(-2x) on [-1.0,3.0]:"<<std::endl;
   ** std::cout<<"Exact result = "<<good_value_fun2<<std::endl;
   ** std::cout<<std::endl;
   ** std::cout<<"rectangle        = "<<slip::newton_cotes(a,b,N,0,Fun2<T>())<<std::endl;
   ** std::cout<<"trapeze          = "<<slip::newton_cotes(a,b,N,1,Fun2<T>())<<std::endl;
   ** std::cout<<"Simpson          = "<<slip::newton_cotes(a,b,N,2,Fun2<T>())<<std::endl;
   ** std::cout<<"Newton-Cotes 3   = "<<slip::newton_cotes(a,b,N,3,Fun2<T>())<<std::endl;
   ** std::cout<<"Boole-Villarceau = "<<slip::newton_cotes(a,b,N,4,Fun2<T>())<<std::endl;
   ** std::cout<<"Newton-Cotes 5   = "<<slip::newton_cotes(a,b,N,5,Fun2<T>())<<std::endl;
   ** std::cout<<"Newton-Cotes 6   = "<<slip::newton_cotes(a,b,N,6,Fun2<T>())<<std::endl;
   ** std::cout<<"Newton-Cotes 7   = "<<slip::newton_cotes(a,b,N,7,Fun2<T>())<<std::endl;
   ** std::cout<<"Newton-Cotes 8   = "<<slip::newton_cotes(a,b,N,8,Fun2<T>())<<std::endl;
   ** std::cout<<"Newton-Cotes 9   = "<<slip::newton_cotes(a,b,N,9,Fun2<T>())<<std::endl;
   ** std::cout<<"Newton-Cotes 10  = "<<slip::newton_cotes(a,b,N,10,Fun2<T>())<<std::endl;

   ** \endcode
   */
  template <typename Real,
	    typename Fun>
  Real newton_cotes(const Real& a, const Real& b,
		    const std::size_t N,
		    const std::size_t order,
		    const Fun& fun)
  {
    assert(a < b);
    assert(order <= 10);
    const Real delta = (b-a)/static_cast<Real>(N);
    //std::cout<<"delta = "<<delta<<std::endl;
    slip::Array<Real> xi(N+1);
    slip::iota(xi.begin(),xi.end(),a,delta);
    //std::cout<<xi<<std::endl;
    slip::Array<Real> yi(N+1);
    std::transform(xi.begin(),xi.end(),yi.begin(),fun);
    const std::size_t size = order+1;
    slip::Array<Real> support(size);
    switch(order)
      {
      case 0:
	{
	  support[0] = delta;
	}
	break;
      case 1: 
	{
	  // std::cerr<<"trapezium"<<std::endl;
	  support[0] = support[1] = delta*static_cast<Real>(0.5); 
	}
	break;
      case 2: 
	{
	  // std::cerr<<"Simpson"<<std::endl;
	  support[0] = delta*(static_cast<Real>(1.0)/static_cast<Real>(3.0)); 
	  support[1] = delta*(static_cast<Real>(4.0)/static_cast<Real>(3.0));
	  support[2] = support[0];
	}
	break;
      case 3:
	{
	  // std::cerr<<"Newton-Cotes 3 or Simpson 3/8"<<std::endl;
	  const Real coef = delta*(static_cast<Real>(3.0)/static_cast<Real>(8.0));
	  support[0] = coef;
	  support[1] = coef * static_cast<Real>(3.0);
	  support[2] = support[1];
	  support[3] = coef;
	} 
	break;
      case 4:
	{
	  // std::cerr<<"Boole-Villarceau"<<std::endl;
	  const Real coef = delta*(static_cast<Real>(2.0)/static_cast<Real>(45.0));
	  support[0] = coef * static_cast<Real>(7.0);
	  support[1] = coef * static_cast<Real>(32.0);
	  support[2] = coef * static_cast<Real>(12.0);
	  support[3] = support[1];
	  support[4] = support[0];
	}
	break;
      case 5:
	{
	  //Boole
	  const Real coef = delta*(static_cast<Real>(5.0)/static_cast<Real>(288.0));
	  support[0] = coef * static_cast<Real>(19.0);
	  support[1] = coef * static_cast<Real>(75.0);
	  support[2] = coef * static_cast<Real>(50.0);
	  support[3] = support[2];
	  support[4] = support[1];
	  support[5] = support[0];
	}
	break;
      case 6:
	{
	  const Real coef = delta*(static_cast<Real>(1.0)/static_cast<Real>(140.0));
	  support[0] = coef * static_cast<Real>(41.0);
	  support[1] = coef * static_cast<Real>(216.0);
	  support[2] = coef * static_cast<Real>(27.0);
	  support[3] = coef * static_cast<Real>(272.0);
	  support[4] = support[2];
	  support[5] = support[1];
	  support[6] = support[0];
	}
	break;
      case 7:
	{
	  const Real coef = delta*(static_cast<Real>(7.0)/static_cast<Real>(17280.0));
	  support[0] = coef * static_cast<Real>(751.0);
	  support[1] = coef * static_cast<Real>(3577.0);
	  support[2] = coef * static_cast<Real>(1323.0);
	  support[3] = coef * static_cast<Real>(2989.0);
	  support[4] = support[3];
	  support[5] = support[2];
	  support[6] = support[1];
	  support[7] = support[0];
	  
	}
	break;
      case 8:
	{
	  const Real coef = delta*(static_cast<Real>(4.0)/static_cast<Real>(14175.0));
	  support[0] = coef * static_cast<Real>(989.0);
	  support[1] = coef * static_cast<Real>(5888.0);
	  support[2] = -coef * static_cast<Real>(928.0);
	  support[3] = coef * static_cast<Real>(10496.0);
	  support[4] = -coef * static_cast<Real>(4540.0);
	  support[5] = support[3];
	  support[6] = support[2];
	  support[7] = support[1];
	  support[8] = support[0];
	}
	break;
	 case 9:
	{
	  const Real coef = delta*(static_cast<Real>(9.0)/static_cast<Real>(89600.0));
	  support[0] = coef * static_cast<Real>(2857.0);
	  support[1] = coef * static_cast<Real>(15741.0);
	  support[2] = coef * static_cast<Real>(1080.0);
	  support[3] = coef * static_cast<Real>(19344.0);
	  support[4] = coef * static_cast<Real>(5778.0);
	  support[5] = support[4];
	  support[6] = support[3];
	  support[7] = support[2];
	  support[8] = support[1];
	  support[9] = support[0];

	}
	break;
      case 10:
	{
	  const Real coef = delta*(static_cast<Real>(5.0)/static_cast<Real>(299376.0));
	  support[0] = coef * static_cast<Real>(16067.0);
	  support[1] = coef * static_cast<Real>(106300.0);
	  support[2] = -coef * static_cast<Real>(48525.0);
	  support[3] = coef * static_cast<Real>(272400.0);
	  support[4] = -coef * static_cast<Real>(260550.0);
	  support[5] = coef * static_cast<Real>(427368.0);
	  support[6] = support[4];
	  support[7] = support[3];
	  support[8] = support[2];
	  support[9] = support[1];
	  support[10] = support[0];
	  

	}
	break;
  };
 
    //std::cout<<"support =\n"<<support<<std::endl;

    Real result = Real();
   if(order != 0)
    {
      for(std::size_t i = 0; i < N; i+=order)
	{
	  result = std::inner_product(yi.begin()+i,
				      yi.begin()+i+size,
				      support.begin(),
				      result);
	}
    }
   else
     {
       for(std::size_t i = 0; i <= N; ++i)
	{
	  result = std::inner_product(yi.begin()+i,
				      yi.begin()+i+1,
				      support.begin(),
				      result);
	}
     }
    return result;
  }


    /**
   ** \brief Newton-Cotes abstract class.
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */
 template <typename Real=double>
  struct NewtonCotesMethod
  {
    typedef NewtonCotesMethod<Real> self;

    /**
     ** \brief Default constructor.
     */
    NewtonCotesMethod():
      order_(static_cast<std::size_t>(0)),
      support_(new slip::Array<Real>(1))
    {}
    /**
     ** \brief Constructor.
     ** \param order Order of the Newton-Cotes integration method.
     */
    NewtonCotesMethod(const std::size_t order):
      order_(order),
      support_(new slip::Array<Real>(order+1))
    {}
    /**
     ** \brief Constructs a copy of the %NewtonCotesMethod \a other.
     ** \param other. rhs 
     */
    NewtonCotesMethod(const self& other):
      order_(other.order_),
      support_(new slip::Array<Real>(*(other.support_)))
    {
    }
    /**
     ** \brief Assigns a %NewtonCotesMethod in \a other
     ** \param other. Other %NewtonCotesMethod.
     */
    self& operator=(const self& other)
    {
      if(this != &other)
	{
	  this->order_ = other.order_;
	  if(this->support_ != nullptr)
	    {
	      delete this->support_;
	    }
	  this->support_ = new slip::Array<Real>(*(other.support_));
	}
      return *this;
    }
    /*!
    ** \brief Destructor of the %NewtonCotesMethod
    */
    virtual ~NewtonCotesMethod()
    {
       if(this->support_ != nullptr)
	{
	  delete support_;
	}
    }
    /*!
    ** \brief init integration support.
    ** \param h. Interval integration step.
    */
    virtual void init(const Real& h) = 0;

    /*!
    ** \brief init integration support.
    ** \param a First value of the interval of integration.
    ** \param b Second value of the interval of integration.
    ** \param N. Number of subintervals
    */
    void init(const Real& a,
	      const Real& b,
	      const std::size_t N)
    {
      const Real h = (b-a)/static_cast<Real>(N);
      this->init(h);
    }
    /*!
    ** \brief RandomAccessIterator to the beginning of integration weights array.
    ** \return slip::Array<Real>::const_iterator
    */
    typename slip::Array<Real>::const_iterator cbegin() const
    {
      return (this->support_)->begin();
    }
     /*!
    ** \brief RandomAccessIterator to one past-the-end of integration weights array.
    ** \return slip::Array<Real>::const_iterator
    */
    typename slip::Array<Real>::const_iterator cend() const
    {
      return (this->support_)->end();
    }

    /*!
    ** \brief Returns the order of integration.
    */
    const std::size_t& order() const
    {
      return this->order_;
    }

  protected:
    std::size_t order_;
    slip::Array<Real>* support_;
  };

  /**
   ** \brief Newton-Cotes Rectangular integration class (order 0 integration).
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */
  template <typename Real=double>
  struct Rectangular: public NewtonCotesMethod<Real>
  {
    typedef NewtonCotesMethod<Real> base;
    
    /*!
    ** \brief Default %Rectangular constructor.
    */
    Rectangular():
      base(static_cast<std::size_t>(0))
    {}

    /*!
    ** \brief init integration weights array.
    ** \param h. Interval integration step.
    */
    void init(const Real& h)
    {
      (*(this->support_))[0] = h;
    }

};

  /**
   ** \brief Newton-Cotes Tapezium integration class (order 1 integration).
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */
  template <typename Real=double>
  struct Trapezium: public NewtonCotesMethod<Real>
  {
    typedef NewtonCotesMethod<Real> base;

     /*!
    ** \brief Default %Trapezium constructor.
    */
    Trapezium():
      base(static_cast<std::size_t>(1))
    {}

    /*!
    ** \brief init integration weights array.
    ** \param h. Interval integration step.
    */
    void init(const Real& h)
    {
      (*(this->support_))[0] = slip::constants<Real>::half()*h;
      (*(this->support_))[1] = (*(this->support_))[0];
    }

   
    
  };
  /**
   ** \brief Newton-Cotes Simpson integration class (order 2 integration).
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */
  template <typename Real=double>
  struct Simpson: public NewtonCotesMethod<Real>
  {
    typedef NewtonCotesMethod<Real> base;
      /*!
    ** \brief Default %Simpson constructor.
    */
    Simpson():
      base(static_cast<std::size_t>(2))
    {}

    /*!
    ** \brief init integration weights array.
    ** \param h. Interval integration step.
    */
    void init(const Real& h)
    {
      
       (*(this->support_))[0] = h*(static_cast<Real>(1.0)/static_cast<Real>(3.0)); 
       (*(this->support_))[1] = h*(static_cast<Real>(4.0)/static_cast<Real>(3.0));
       (*(this->support_))[2] =  (*(this->support_))[0];
    }

   
    
  };
   
  /**
   ** \brief Newton-Cotes of order 3 integration.
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */
 template <typename Real=double>
  struct NewtonCotes3: public NewtonCotesMethod<Real>
  {
    typedef NewtonCotesMethod<Real> base;
    /*!
    ** \brief Default %NewtonCotes3 constructor.
    */
    NewtonCotes3():
      base(static_cast<std::size_t>(3))
    {}

     /*!
    ** \brief init integration weights array.
    ** \param h. Interval integration step.
    */
    void init(const Real& h)
    {
       const Real coef = h*(static_cast<Real>(3.0)/static_cast<Real>(8.0));
       (*(this->support_))[0] = coef;
       (*(this->support_))[1] = coef * static_cast<Real>(3.0);
       (*(this->support_))[2] = (*(this->support_))[1];
       (*(this->support_))[3] = coef;
    }

   
    
  };
  /**
   ** \brief BooleVillarceau integration class (order 4).
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */
  template <typename Real=double>
  struct BooleVillarceau: public NewtonCotesMethod<Real>
  {
    typedef NewtonCotesMethod<Real> base;
    /*!
    ** \brief Default %BooleVillarceau constructor.
    */
    BooleVillarceau():
      base(static_cast<std::size_t>(4))
    {}

     /*!
    ** \brief init integration weights array.
    ** \param h. Interval integration step.
    */
    void init(const Real& h)
    {
       const Real coef = h*(static_cast<Real>(2.0)/static_cast<Real>(45.0));
       (*(this->support_))[0] = coef * static_cast<Real>(7.0);
       (*(this->support_))[1] = coef * static_cast<Real>(32.0);
       (*(this->support_))[2] = coef * static_cast<Real>(12.0);
       (*(this->support_))[3] = (*(this->support_))[1];
       (*(this->support_))[4] = (*(this->support_))[0];
    }

   
    
  };
/**
   ** \brief Newton-Cotes of order 5 integration class.
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */
  template <typename Real=double>
  struct NewtonCotes5: public NewtonCotesMethod<Real>
  {
    typedef NewtonCotesMethod<Real> base;
    /*!
    ** \brief Default %NewtonCotes5 constructor.
    */
    NewtonCotes5():
      base(static_cast<std::size_t>(5))
    {}

     /*!
    ** \brief init integration weights array.
    ** \param h. Interval integration step.
    */
    void init(const Real& h)
    {
       const Real coef = h*(static_cast<Real>(5.0)/static_cast<Real>(288.0));
       (*(this->support_))[0] = coef * static_cast<Real>(19.0);
       (*(this->support_))[1] = coef * static_cast<Real>(75.0);
       (*(this->support_))[2] = coef * static_cast<Real>(50.0);
       (*(this->support_))[3] =  (*(this->support_))[2];
       (*(this->support_))[4] =  (*(this->support_))[1];
       (*(this->support_))[5] =  (*(this->support_))[0];
       
       
    }

   
    
  };
/**
   ** \brief Newton-Cotes of order 6 integration class.
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */
template <typename Real=double>
  struct NewtonCotes6: public NewtonCotesMethod<Real>
  {
    typedef NewtonCotesMethod<Real> base;
    /*!
    ** \brief Default %NewtonCotes6 constructor.
    */
    NewtonCotes6():
      base(static_cast<std::size_t>(6))
    {}

     /*!
    ** \brief init integration weights array.
    ** \param h. Interval integration step.
    */
    void init(const Real& h)
    {
      const Real coef = h*(static_cast<Real>(1.0)/static_cast<Real>(140.0));
	  (*(this->support_))[0] = coef * static_cast<Real>(41.0);
	  (*(this->support_))[1] = coef * static_cast<Real>(216.0);
	  (*(this->support_))[2] = coef * static_cast<Real>(27.0);
	  (*(this->support_))[3] = coef * static_cast<Real>(272.0);
	  (*(this->support_))[4] = (*(this->support_))[2];
	  (*(this->support_))[5] = (*(this->support_))[1];
	  (*(this->support_))[6] = (*(this->support_))[0];
    }

   
    
  };


/**
   ** \brief Newton-Cotes Weddle integration class (order 6).
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */

template <typename Real=double>
  struct Weddle: public NewtonCotesMethod<Real>
  {
    typedef NewtonCotesMethod<Real> base;
    typedef Weddle<Real> self;

     /*!
    ** \brief Default %Weddle constructor.
    */
    Weddle():
      base(static_cast<std::size_t>(6))
    {}

  

    /*!
    ** \brief init integration weights array.
    ** \param h. Interval integration step.
    */ 
    void init(const Real& h)
    {
        const Real coef = h*(static_cast<Real>(3.0)/static_cast<Real>(10.0));
	(*(this->support_))[0] = coef * slip::constants<Real>::one();
	(*(this->support_))[1] = coef * static_cast<Real>(5.0);
	(*(this->support_))[2] = coef * slip::constants<Real>::one();
	(*(this->support_))[3] = coef * static_cast<Real>(6.0);
	(*(this->support_))[4] = (*(this->support_))[2];
	(*(this->support_))[5] = (*(this->support_))[1];
	(*(this->support_))[6] = (*(this->support_))[0];
    }

   
    
  };
/**
   ** \brief Newton-Cotes of order 7 integration class.
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */
template <typename Real=double>
  struct NewtonCotes7: public NewtonCotesMethod<Real>
  {
    typedef NewtonCotesMethod<Real> base;
     /*!
    ** \brief Default %NewtonCotes7 constructor.
    */
    NewtonCotes7():
      base(static_cast<std::size_t>(7))
    {}

    /*!
    ** \brief init integration weights array.
    ** \param h. Interval integration step.
    */
    void init(const Real& h)
    {
       const Real coef = h*(static_cast<Real>(7.0)/static_cast<Real>(17280.0));
	  (*(this->support_))[0] = coef * static_cast<Real>(751.0);
	  (*(this->support_))[1] = coef * static_cast<Real>(3577.0);
	  (*(this->support_))[2] = coef * static_cast<Real>(1323.0);
	  (*(this->support_))[3] = coef * static_cast<Real>(2989.0);
	  (*(this->support_))[4] = (*(this->support_))[3];
	  (*(this->support_))[5] = (*(this->support_))[2];
	  (*(this->support_))[6] = (*(this->support_))[1];
	  (*(this->support_))[7] = (*(this->support_))[0];
    }

   
    
  };
/**
   ** \brief Newton-Cotes of order 8 integration class.
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */
template <typename Real=double>
  struct NewtonCotes8: public NewtonCotesMethod<Real>
  {
    typedef NewtonCotesMethod<Real> base;

    /*!
    ** \brief Default %NewtonCotes8 constructor.
    */
    NewtonCotes8():
      base(static_cast<std::size_t>(8))
    {}

     /*!
    ** \brief init integration weights array.
    ** \param h. Interval integration step.
    */
    void init(const Real& h)
    {
        const Real coef = h*(static_cast<Real>(4.0)/static_cast<Real>(14175.0));
	  (*(this->support_))[0] = coef * static_cast<Real>(989.0);
	  (*(this->support_))[1] = coef * static_cast<Real>(5888.0);
	  (*(this->support_))[2] = -coef * static_cast<Real>(928.0);
	  (*(this->support_))[3] = coef * static_cast<Real>(10496.0);
	  (*(this->support_))[4] = -coef * static_cast<Real>(4540.0);
	  (*(this->support_))[5] = (*(this->support_))[3];
	  (*(this->support_))[6] = (*(this->support_))[2];
	  (*(this->support_))[7] = (*(this->support_))[1];
	  (*(this->support_))[8] = (*(this->support_))[0];
    }

   
    
  };
  /**
   ** \brief Newton-Cotes of order 9 integration class.
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */
template <typename Real=double>
  struct NewtonCotes9: public NewtonCotesMethod<Real>
  {
    typedef NewtonCotesMethod<Real> base;
     /*!
    ** \brief Default %NewtonCotes9 constructor.
    */
    NewtonCotes9():
      base(static_cast<std::size_t>(9))
    {}

     /*!
    ** \brief init integration weights array.
    ** \param h. Interval integration step.
    */
    void init(const Real& h)
    {
      const Real coef = h*(static_cast<Real>(9.0)/static_cast<Real>(89600.0));
	  (*(this->support_))[0] = coef * static_cast<Real>(2857.0);
	  (*(this->support_))[1] = coef * static_cast<Real>(15741.0);
	  (*(this->support_))[2] = coef * static_cast<Real>(1080.0);
	  (*(this->support_))[3] = coef * static_cast<Real>(19344.0);
	  (*(this->support_))[4] = coef * static_cast<Real>(5778.0);
	  (*(this->support_))[5] = (*(this->support_))[4];
	  (*(this->support_))[6] = (*(this->support_))[3];
	  (*(this->support_))[7] = (*(this->support_))[2];
	  (*(this->support_))[8] = (*(this->support_))[1];
	  (*(this->support_))[9] = (*(this->support_))[0];
       
    }

   
    
  };
  /**
   ** \brief Newton-Cotes of order 10 integration class.
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */
template <typename Real=double>
  struct NewtonCotes10: public NewtonCotesMethod<Real>
  {
    typedef NewtonCotesMethod<Real> base;
     /*!
    ** \brief Default %NewtonCotes10 constructor.
    */
    NewtonCotes10():
      base(static_cast<std::size_t>(10))
    {}

     /*!
    ** \brief init integration weights array.
    ** \param h. Interval integration step.
    */
    void init(const Real& h)
    {
       const Real coef = h*(static_cast<Real>(5.0)/static_cast<Real>(299376.0));
	  (*(this->support_))[0] = coef * static_cast<Real>(16067.0);
	  (*(this->support_))[1] = coef * static_cast<Real>(106300.0);
	  (*(this->support_))[2] = -coef * static_cast<Real>(48525.0);
	  (*(this->support_))[3] = coef * static_cast<Real>(272400.0);
	  (*(this->support_))[4] = -coef * static_cast<Real>(260550.0);
	  (*(this->support_))[5] = coef * static_cast<Real>(427368.0);
	  (*(this->support_))[6] = (*(this->support_))[4];
	  (*(this->support_))[7] = (*(this->support_))[3];
	  (*(this->support_))[8] = (*(this->support_))[2];
	  (*(this->support_))[9] = (*(this->support_))[1];
	  (*(this->support_))[10] = (*(this->support_))[0];
    }

   
    
  };
/**
   ** \brief Shovelton integration class (order 10).
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/02
   ** \version 0.0.1
   ** \since 1.5.0
   */
template <typename Real=double>
  struct Shovelton: public NewtonCotesMethod<Real>
  {
    typedef NewtonCotesMethod<Real> base;
    /*!
    ** \brief Default %Shovelton constructor.
    */
    Shovelton():
      base(static_cast<std::size_t>(10))
    {}

     /*!
    ** \brief init integration weights array.
    ** \param h. Interval integration step.
    */
    void init(const Real& h)
    {
       const Real coef = h*(static_cast<Real>(5.0)/static_cast<Real>(126.0));
	  (*(this->support_))[0] = coef * static_cast<Real>(8.0);
	  (*(this->support_))[1] = coef * static_cast<Real>(35.0);
	  (*(this->support_))[2] = coef * static_cast<Real>(15.0);
	  (*(this->support_))[3] = (*(this->support_))[1];
          (*(this->support_))[4] = (*(this->support_))[2];
	  (*(this->support_))[5] =  coef * static_cast<Real>(36.0);
	  (*(this->support_))[6] = (*(this->support_))[4];
	  (*(this->support_))[7] = (*(this->support_))[3];
	  (*(this->support_))[8] = (*(this->support_))[2];
	  (*(this->support_))[9] = (*(this->support_))[1];
	  (*(this->support_))[10] = (*(this->support_))[0];
    }

   
    
  };

/**
   ** \brief Newton cotes integration algorithm on the real range [a,b].
   ** It is assumed that the value of a function f defined on \f$[a,b]\f$ is known at \f$n+1\f$ equally spaces points: \f$a\le x_0 < x_1 < \cdots<x_n \le b \f$. 
   ** The Newton-Cotes formulas of order n is defined as:
   ** \f[\int_a^b f(x)\,dx = \sum_{i=0}^n f(x_i)\f] where
   ** \f$ x_i = a + ih\f$ with \f$h = \frac{b-a}{n} \f$ the step size.
   ** The weight \f$w_i\f$ can be computed as the Lagrange basis polynomials:
   ** \f[\int_a^b f(x)\,dx \simeq \int_a^b L(x)\,dx = \int_a^b\left(\sum_{i=0}^n f(x_i)l_i(x) \right)\,dx = \sum_{i=0}^n f(x_i)\int_a^b l_i(x)\,dx\f] where
   ** \f[w_i = \int_a^b l_i(x)\]
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr> : conceptor
   ** \date 2024/04/02
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param a. First value of the interval of integration.
   ** \param b. Second value of the interval of integration.
   ** \param method. slip::NewtonCotesMethod integration method.
   ** \param fun_first. RandomAccessIterator to the first value of \f$f(x_i)\f$.
   ** \param fun_first. RandomAccessIterator to one-past-the-end value of \f$f(x_i)\f$.
   ** \pre a < b
   ** \par Functor example:
   ** \code
   ** template<typename T = double>
   ** struct Fun2:public std::unary_function<T,T>
   ** {
   **
   ** T operator()(const T& x)
   ** {
   **   return (x*x)*std::exp(-static_cast<T>(2.0)*x);
   ** }
   ** 
   **};
   ** \endcode
   ** \par Example:
   ** \code
   ** typedef double T;
   ** T a = -1.0;
   ** T b = 3.0;
   ** T good_value_fun2 = static_cast<T>(1.83177182362850);
   ** std::cout<<"Integration of x^2exp(-2x) on [-1.0,3.0]:"<<std::endl;
   ** std::cout<<"Exact result = "<<good_value_fun2<<std::endl;
   ** std::cout<<std::endl;
   ** slip::Rectangular<T> rect;
   ** slip::Trapezium<T> trapezium;
   ** slip::Simpson<T> simpson;
   ** slip::NewtonCotes3<T> newtoncotes3;
   ** slip::BooleVillarceau<T> boolevillarceau;
   ** slip::NewtonCotes5<T> newtoncotes5;
   ** slip::NewtonCotes6<T> newtoncotes6;
   ** slip::NewtonCotesMethod<T>* ptrnc6 = &newtoncotes6;
   ** T h = (b-a)/static_cast<T>(N);
   ** slip::Array<T> xi(N+1);
   ** slip::iota(xi.begin(),xi.end(),a,h);
   ** slip::Array<T> yi(N+1);
   ** std::transform(xi.begin(),xi.end(),yi.begin(),Fun2<T>());
   ** std::cout<<"rectangular      = "<<slip::newton_cotes(a,b,rect,yi.begin(),yi.end())<<std::endl;
   ** std::cout<<"trapeze          = "<<slip::newton_cotes(a,b,trapezium,yi.begin(),yi.end())<<std::endl;
   ** std::cout<<"Simpson          = "<<slip::newton_cotes(a,b,simpson,yi.begin(),yi.end())<<std::endl;
   ** std::cout<<"Newton-Cotes 3   = "<<slip::newton_cotes(a,b,newtoncotes3,yi.begin(),yi.end())<<std::endl;
   ** std::cout<<"Boole-Villarceau = "<<slip::newton_cotes(a,b,boolevillarceau,yi.begin(),yi.end())<<std::endl;
   ** std::cout<<"Newton-Cotes 5   = "<<slip::newton_cotes(a,b,newtoncotes5,yi.begin(),yi.end())<<std::endl;
   ** std::cout<<"Newton-Cotes 6   = "<<slip::newton_cotes(a,b,*ptrnc6,yi.begin(),yi.end())<<std::endl;
   ** \endcode
   */
  template <typename Real,
	    typename RandomAccessIterator>
  Real newton_cotes(const Real& a, const Real& b,
		    slip::NewtonCotesMethod<Real>& method,
		    RandomAccessIterator fun_first,
		    RandomAccessIterator fun_last)
  {
    const std::size_t N = static_cast<std::size_t>(fun_last-fun_first - 1);
    //Real h = (b-a)/static_cast<Real>(N);
    method.init(a,b,N);

    Real result = Real();
    const std::size_t order = method.order();
    const std::size_t size = order + 1;
    if(order != 0)
      {
	for(std::size_t i = 0, ii = i+size; i < N; i+=order, ii+=order)
	  {
	    result = std::inner_product(fun_first+i,
					fun_first+ii,
					method.cbegin(),
					result);
	}
    }
   else
     {
       for(std::size_t i = 0, ii =1; i <= N; ++i, ++ii)
	{
	  result = std::inner_product(fun_first+i,
				      fun_first+ii,
				      method.cbegin(),
				      result);
	}
     }
    return result;
    
  }
  
/**
   ** \brief Newton cotes integration algorithm on the real range [a,b].
   ** It is assumed that the value of a function f defined on \f$[a,b]\f$ is known at \f$n+1\f$ equally spaces points: \f$a\le x_0 < x_1 < \cdots<x_n \le b \f$. 
   ** The Newton-Cotes formulas of order n is defined as:
   ** \f[\int_a^b f(x)\,dx = \sum_{i=0}^n f(x_i)\f] where
   ** \f$ x_i = a + ih\f$ with \f$h = \frac{b-a}{n} \f$ the step size.
   ** The weight \f$w_i\f$ can be computed as the Lagrange basis polynomials:
   ** \f[\int_a^b f(x)\,dx \simeq \int_a^b L(x)\,dx = \int_a^b\left(\sum_{i=0}^n f(x_i)l_i(x) \right)\,dx = \sum_{i=0}^n f(x_i)\int_a^b l_i(x)\,dx\f] where
   ** \f[w_i = \int_a^b l_i(x)\]
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr> : conceptor
   ** \date 2024/04/02
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param a. First value of the interval of integration.
   ** \param b. Second value of the interval of integration.
   ** \param N. Number of subintervals of [a,b].
   ** \param method. slip::NewtonCotesMethod integration method.
   ** \param fun. Unary functor of the function f to integrate.
   ** \pre a < b
   ** \pre 0 <= order <= 10
   ** \par Functor example:
   ** \code
   ** template<typename T = double>
   ** struct Fun2:public std::unary_function<T,T>
   ** {
   **
   ** T operator()(const T& x)
   ** {
   **   return (x*x)*std::exp(-static_cast<T>(2.0)*x);
   ** }
   ** 
   **};
   ** \endcode
   ** \par Example:
   ** \code
   ** typedef double T;
   ** T a = -1.0;
   ** T b = 3.0;
   ** T good_value_fun2 = static_cast<T>(1.83177182362850);
   ** std::cout<<"Integration of x^2exp(-2x) on [-1.0,3.0]:"<<std::endl;
   ** std::cout<<"Exact result = "<<good_value_fun2<<std::endl;
   ** std::cout<<std::endl;
   ** slip::Rectangular<T> rect;
   ** slip::Trapezium<T> trapezium;
   ** slip::Simpson<T> simpson;
   ** slip::NewtonCotes3<T> newtoncotes3;
   ** slip::BooleVillarceau<T> boolevillarceau;
   ** slip::NewtonCotes5<T> newtoncotes5;
   ** slip::NewtonCotes6<T> newtoncotes6;
   ** slip::NewtonCotes7<T> newtoncotes7;
   ** slip::NewtonCotes8<T> newtoncotes8;
   ** slip::NewtonCotes9<T> newtoncotes9;
   ** slip::NewtonCotes10<T> newtoncotes10;
   ** slip::Weddle<T> weddle;
   ** slip::Shovelton<T> shovelton;
   **
   ** std::cout<<"rectangular      = "<<slip::newton_cotes(a,b,N,rect,Fun2<T>())<<std::endl;
  ** std::cout<<"trapeze          = "<<slip::newton_cotes(a,b,N,trapezium,Fun2<T>())<<std::endl;
  ** std::cout<<"Simpson          = "<<slip::newton_cotes(a,b,N,simpson,Fun2<T>())<<std::endl;
  ** std::cout<<"Newton-Cotes 3   = "<<slip::newton_cotes(a,b,N,newtoncotes3,Fun2<T>())<<std::endl;
  ** std::cout<<"Boole-Villarceau = "<<slip::newton_cotes(a,b,N,boolevillarceau,Fun2<T>())<<std::endl;
  ** std::cout<<"Newton-Cotes 5   = "<<slip::newton_cotes(a,b,N,newtoncotes5,Fun2<T>())<<std::endl;
  ** std::cout<<"Weddle           = "<<slip::newton_cotes(a,b,N,weddle,Fun2<T>())<<std::endl;
  ** std::cout<<"Newton-Cotes 6   = "<<slip::newton_cotes(a,b,N,newtoncotes6,Fun2<T>())<<std::endl;
  ** std::cout<<"Newton-Cotes 7   = "<<slip::newton_cotes(a,b,N,newtoncotes7,Fun2<T>())<<std::endl;
  ** std::cout<<"Newton-Cotes 8   = "<<slip::newton_cotes(a,b,N,newtoncotes8,Fun2<T>())<<std::endl;
  ** std::cout<<"Newton-Cotes 9   = "<<slip::newton_cotes(a,b,N,newtoncotes9,Fun2<T>())<<std::endl;
  ** std::cout<<"Newton-Cotes 10  = "<<slip::newton_cotes(a,b,N,newtoncotes10,Fun2<T>())<<std::endl;
  ** std::cout<<"Shovelton        = "<<slip::newton_cotes(a,b,N,shovelton,Fun2<T>())<<std::endl;
   ** \endcode
   */
  template <typename Real,
	    typename Fun>
  Real newton_cotes(const Real& a, const Real& b,
		    const std::size_t N,
		    slip::NewtonCotesMethod<Real>& method,
		    const Fun& fun)
  {
    const Real h = (b-a)/static_cast<Real>(N);
    method.init(a,b,N);
    
    slip::Array<Real> xi(N+1);
    slip::iota(xi.begin(),xi.end(),a,h);
    
    slip::Array<Real> yi(N+1);
    std::transform(xi.begin(),xi.end(),yi.begin(),fun);

    return slip::newton_cotes(a,b,method,yi.begin(),yi.end());
  }

  /**
   ** \brief Newton cotes integration algorithm on the real range [a,b].
   ** It is assumed that the value of a function f defined on \f$[a,b]\f$ is known at \f$n+1\f$ equally spaces points: \f$a\le x_0 < x_1 < \cdots<x_n \le b \f$. 
   ** The Newton-Cotes formulas of order n is defined as:
   ** \f[\int_a^b f(x)\,dx = \sum_{i=0}^n f(x_i)\f] where
   ** \f$ x_i = a + ih\f$ with \f$h = \frac{b-a}{n} \f$ the step size.
   ** The weight \f$w_i\f$ can be computed as the Lagrange basis polynomials:
   ** \f[\int_a^b f(x)\,dx \simeq \int_a^b L(x)\,dx = \int_a^b\left(\sum_{i=0}^n f(x_i)l_i(x) \right)\,dx = \sum_{i=0}^n f(x_i)\int_a^b l_i(x)\,dx\f] where
   ** \f[w_i = \int_a^b l_i(x)\]
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr> : conceptor
   ** \date 2024/04/02
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param a. First value of the interval of integration.
   ** \param b. Second value of the interval of integration.
   ** \param N. Number of subintervals of [a,b].
   ** \param method. slip::NewtonCotesMethod integration method.
   ** \param p. slip::Polynomial<Real> function.
   ** \pre a < b
   ** \pre 0 <= order <= 10
   ** \par Functor example:
   ** \code
   ** template<typename T = double>
   ** struct Fun2:public std::unary_function<T,T>
   ** {
   **
   ** T operator()(const T& x)
   ** {
   **   return (x*x)*std::exp(-static_cast<T>(2.0)*x);
   ** }
   ** 
   **};
   ** \endcode
   ** \par Example:
   ** \code
   ** \endcode
   */
  
  template <typename Real=double>
  Real newton_cotes(const Real& a, const Real& b,
		    const std::size_t N,
		    slip::NewtonCotesMethod<Real>& method,
		    const slip::Polynomial<Real>& p)
  {
    const Real h = (b-a)/static_cast<Real>(N);
    method.init(a,b,N);
    //std::cout<<"h = "<<h<<std::endl;
    slip::Array<Real> xi(N+1);
    slip::iota(xi.begin(),xi.end(),a,h);
    //std::cout<<xi<<std::endl;
    slip::Array<Real> yi(N+1);
    p.multi_evaluate(xi.begin(),xi.end(),yi.begin());
    // std::cout<<"yi = "<<yi<<std::endl;

    return slip::newton_cotes(a,b,method,yi.begin(),yi.end());
  }

  template <typename Real>
  struct AlternativeExtendedSimpsonsRule
  {
    typedef AlternativeExtendedSimpsonsRule<Real> self;

    AlternativeExtendedSimpsonsRule():
      points_number_(static_cast<std::size_t>(8)),
      support_(new slip::Array<Real>(8))
    {}

    AlternativeExtendedSimpsonsRule(const std::size_t points_number):
      points_number_(points_number),
      support_(new slip::Array<Real>(points_number))
    {}

    AlternativeExtendedSimpsonsRule(const self& other):
      points_number_(other.points_number_),
      support_(new slip::Array<Real>(*(other.support_)))
    {
    }

    self& operator=(const self& other)
    {
      if(this != &other)
	{
	  this->points_number_ = other.points_number_;
	  if(this->support_ != nullptr)
	    {
	      delete this->support_;
	    }
	  this->support_ = new slip::Array<Real>(*(other.support_));
	}
      return *this;
    }
    virtual ~AlternativeExtendedSimpsonsRule()
    {
       if(this->support_ != nullptr)
	{
	  delete support_;
	}
    }
   
    void init(const Real& a,
	      const Real& b,
	      const std::size_t N)
    {
      const Real h = (b-a)/static_cast<Real>(N);
      this->init(h);
    }

    typename slip::Array<Real>::const_iterator cbegin() const
    {
      return (this->support_)->begin();
    }
    typename slip::Array<Real>::const_iterator cend() const
    {
      return (this->support_)->end();
    }

    const std::size_t& points_number() const
    {
      return this->points_number_;
    }

  protected:
    std::size_t points_number_;
    slip::Array<Real>* support_;

  private:
    void init(const Real& h)
    {
      const Real forty_eight = static_cast<Real>(48.0);
      const Real c1 = h*(static_cast<Real>(17.0)/forty_eight);
      const Real c2 = h*(static_cast<Real>(59.0)/forty_eight);
      const Real c3 = h*(static_cast<Real>(43.0)/forty_eight);
      const Real c4 = h*(static_cast<Real>(49.0)/forty_eight);
      
      if(this->points_number_ == 8)
	{
	  (*(this->support_))[0] = c1;
	  (*(this->support_))[1] = c2;
	  (*(this->support_))[2] = c3;
	  (*(this->support_))[3] = c4;
	  (*(this->support_))[4] = c4;
	  (*(this->support_))[5] = c3;
	  (*(this->support_))[6] = c2;
	  (*(this->support_))[7] = c1;
	}
      else if(this->points_number_ > 8)
	{
	  const std::size_t N = this->points_number_;
	  (*(this->support_))[0] = c1;
	  (*(this->support_))[1] = c2;
	  (*(this->support_))[2] = c3;
	  (*(this->support_))[3] = c4;
	  (*(this->support_))[N-1] = c1;
	  (*(this->support_))[N-2] = c2;
	  (*(this->support_))[N-3] = c3;
	  (*(this->support_))[N-4] = c4;
	  std::fill_n((this->support_)->begin()+4,N-8,h);
	  
	}
      else
	{
	  
	}
    }

  };

 template <typename Real,
	   typename RandomAccessIterator>
  Real alternative_extended_simpsons_rule(const Real& a, const Real& b,
		    slip::AlternativeExtendedSimpsonsRule<Real>& aesr,
		    RandomAccessIterator fun_first,
		    RandomAccessIterator fun_last)
  {
    const std::size_t N = static_cast<std::size_t>(fun_last-fun_first-1);
    //Real h = (b-a)/static_cast<Real>(N);
    aesr.init(a,b,N);
    Real result = Real();
    if((N+1) >=8)
      {
	
	result = std::inner_product(fun_first,
				    fun_last,
				    aesr.cbegin(),
				    Real());
	
    }
   else
     {
       std::cerr<<"the number of points must be greater or equal to 8"<<std::endl;
     }
    return result;
    
  }

template <typename Real,
	  typename Fun>
  Real alternative_extended_simpsons_rule(const Real& a, const Real& b,
					  const std::size_t N,
					  slip::AlternativeExtendedSimpsonsRule<Real>& aesr,
					  const Fun& fun)
  {
    const Real h = (b-a)/static_cast<Real>(N);
    slip::Array<Real> xi(N+1);
    slip::iota(xi.begin(),xi.end(),a,h);
    slip::Array<Real> yi(N+1);
    std::transform(xi.begin(),xi.end(),yi.begin(),fun);

    return slip::alternative_extended_simpsons_rule(a,b,aesr,yi.begin(),yi.end());
  }

 template <typename Real>
 Real alternative_extended_simpsons_rule(const Real& a, const Real& b,
					 const std::size_t N,
					 slip::AlternativeExtendedSimpsonsRule<Real>& aesr,
					 const slip::Polynomial<Real>& p)
  {
    const Real h = (b-a)/static_cast<Real>(N);
    aesr.init(a,b,N);
    slip::Array<Real> xi(N+1);
    slip::iota(xi.begin(),xi.end(),a,h);
    slip::Array<Real> yi(N+1);
    p.multi_evaluate(xi.begin(),xi.end(),yi.begin());
    return slip::alternative_extended_simpsons_rule(a,b,aesr,yi.begin(),yi.end());
  }


}//::slip

#endif //SLIP_INTEGRATION_HPP
