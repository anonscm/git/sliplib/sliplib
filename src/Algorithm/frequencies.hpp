/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file frequencies.hpp
 * 
 * \brief Provides some frequencies computations algorithms.
 * 
 */
#ifndef SLIP_FREQUENCIES_HPP
#define SLIP_FREQUENCIES_HPP

#include "macros.hpp"
#include "FFT.hpp"
#include "arithmetic_op.hpp"

namespace slip
{

  /*!
   ** \brief Computes the normalized frequencies (in the range [-1/2, 1/2]).
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/12/13
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param freq_first. RandomAccesIterator to the input frequencies range.
   ** \param freq_last. RandomAccesIterator to the input frequencies range.
   ** \param shift. If true shift the frequencies range using ifftshift.
   ** \pre data type should be a real type (float, double...).
   ** \par Example:
   ** \code
   ** const std::size_t N_odd = 7;
   ** const std::size_t N_even = 8;
   ** slip::Array<double> Freqodd(N_odd);
   ** slip::Array<double> Freqeven(N_even);
   ** slip::normalized_frequencies(Freqodd.begin(),Freqodd.end());
   ** slip::normalized_frequencies(Freqeven.begin(),Freqeven.end());
   ** std::cout<<"Freqodd = \n"<<Freqodd<<std::endl;
   ** std::cout<<"Freqeven = \n"<<Freqeven<<std::endl;
   ** std::cout<<"Shifted normalized frequencies"<<std::endl;
   ** Freqodd = 0.0;
   ** Freqeven = 0.0;
   ** slip::normalized_frequencies(Freqodd.begin(),Freqodd.end(),true);
   ** slip::normalized_frequencies(Freqeven.begin(),Freqeven.end(),true);
   ** std::cout<<"Freqodd = \n"<<Freqodd<<std::endl;
   ** std::cout<<"Freqeven = \n"<<Freqeven<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator>
  void normalized_frequencies(RandomAccessIterator freq_first,
			      RandomAccessIterator freq_last,
			      const bool shift = false)
  {
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type Real;
    const std::size_t N = static_cast<std::size_t>(freq_last-freq_first);
    
    slip::iota(freq_first,freq_last,-std::floor(N/2)/Real(N),
	       slip::constants<Real>::one()/Real(N));
    if(shift)
      {
	if(N%2 == 0)
	  {
	    slip::fftshift(freq_first,freq_last);
	  }
	else
	  {
	    slip::ifftshift(freq_first,freq_last);
	  }
      }
  }

 
   /*!
   ** \brief Computes the angular frequencies (in the range [-pi, pi]).
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/12/13
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param freq_first. RandomAccesIterator to the input frequencies range.
   ** \param freq_last. RandomAccesIterator to the input frequencies range.
   ** \param shift. If true shift the frequencies range using ifftshift.
   ** \pre data type should be a real type (float, double...).
   ** \par Example:
   ** \code
   ** const std::size_t N_odd = 7;
   ** const std::size_t N_even = 8;
   ** slip::Array<double> Freqodd(N_odd);
   ** slip::Array<double> Freqeven(N_even);
   ** slip::angular_frequencies(Freqodd.begin(),Freqodd.end());
   ** slip::angular_frequencies(Freqeven.begin(),Freqeven.end());
   ** std::cout<<"Freqodd = \n"<<Freqodd<<std::endl;
   ** std::cout<<"Freqeven = \n"<<Freqeven<<std::endl;
   ** std::cout<<"Shifted angular frequencies"<<std::endl;
   ** Freqodd = 0.0;
   ** Freqeven = 0.0;
   ** slip::angular_frequencies(Freqodd.begin(),Freqodd.end(),true);
   ** slip::angular_frequencies(Freqeven.begin(),Freqeven.end(),true);
   ** std::cout<<"Freqodd = \n"<<Freqodd<<std::endl;
   ** std::cout<<"Freqeven = \n"<<Freqeven<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator>
  void angular_frequencies(RandomAccessIterator freq_first,
			   RandomAccessIterator freq_last,
			   const bool shift = false)
  {
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type Real;
    slip::normalized_frequencies(freq_first,freq_last,shift);
    slip::multiplies_scalar(freq_first,freq_last,slip::constants<Real>::two()*slip::constants<Real>::pi(),freq_first);
  }

   /*!
   ** \brief Computes the frequencies in the range [-Fs/2, Fs/2] with Fs the sampling frequency.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2024/12/13
   ** \since 1.5.0
   ** \version 0.0.1
   ** \param freq_first. RandomAccesIterator to the input frequencies range.
   ** \param freq_last. RandomAccesIterator to the input frequencies range.
   ** \param Fs. Sampling frequency.
   ** \param shift. If true shift the frequencies range using ifftshift.
   ** \pre data type should be a real type (float, double...).
   ** \par Example:
   ** \code
   ** const std::size_t N_odd = 7;
   ** const std::size_t N_even = 8;
   ** slip::Array<double> Freqodd(N_odd);
   ** slip::Array<double> Freqeven(N_even);
   ** double Fs = 4000.0;
   ** slip::frequencies(Freqodd.begin(),Freqodd.end(),Fs);
   ** slip::frequencies(Freqeven.begin(),Freqeven.end(),Fs);
   ** std::cout<<"Freqodd = \n"<<Freqodd<<std::endl;
   ** std::cout<<"Freqeven = \n"<<Freqeven<<std::endl;
   ** std::cout<<"Shifted frequencies"<<std::endl;
   ** Freqodd = 0.0;
   ** Freqeven = 0.0;
   ** slip::frequencies(Freqodd.begin(),Freqodd.end(),Fs,true);
   ** slip::frequencies(Freqeven.begin(),Freqeven.end(),Fs,true);
   ** std::cout<<"Freqodd = \n"<<Freqodd<<std::endl;
   ** std::cout<<"Freqeven = \n"<<Freqeven<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator>
  void frequencies(RandomAccessIterator freq_first,
		   RandomAccessIterator freq_last,
		   const typename std::iterator_traits<RandomAccessIterator>::value_type& Fs,
		   const bool shift = false)
  {
    slip::normalized_frequencies(freq_first,freq_last,shift);
    slip::multiplies_scalar(freq_first,freq_last,Fs,freq_first);
  }

  
}//::slip

#endif //SLIP_FREQUENCIES_HPP
