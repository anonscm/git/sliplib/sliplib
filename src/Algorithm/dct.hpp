/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file dct.hpp
 * 
 * \brief Provides some dct algorithms.
 * \since 1.5.0
 */

#ifndef SLIP_DCT_HPP
#define SLIP_DCT_HPP

#include <algorithm>
#include <numeric>
#include <cassert>
#include <iterator>
#include "macros.hpp"
#include "Array2d.hpp"
#include "FFT.hpp"
#include "arithmetic_op.hpp"

namespace slip
{

  /**
   ** \brief functor to compute DCT-I matrix coefficients
   ** \f[ C^I_{N+1}[i][j] = \sqrt{\frac{2}{N}}\left[\epsilon_i\epsilon_j\cos\left(\frac{\pi ij}{N}\right) \right],\, \forall i,j\in \{0,1,\cdots,N\}\f]
   ** where
   **  \f[\epsilon_p = \frac{1}{\sqrt{2}},\, \textrm{if}\, p=0\, \textrm{or}\, p=N\,\textrm{and}\, 1\,\textrm{else}\f] 
   ** \f[DCT-I]^{-1} = [DCT-I]^T = DCT-I\f] 
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \par Example:
   **\code
   ** slip::Matrix<double> DCTI_8_norm;
   ** slip::dctI_coeff<double> dct_coeffI_norm(8,true);
   ** dct_matrix(dct_coeffI_norm,DCTI_8_norm);
   ** std::cout<<"DCTI_8_norm = \n"<<DCTI_8_norm<<std::endl;
   ** \endcode
   */
 template<typename Real=double>
struct dctI_coeff
{
  /**
   ** \brief Construct DCT-I coefficient functor with N=9 and normalized = true.
   */
  dctI_coeff():
    N_(8+1),
    normalized_(true),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_-1))),
    pi_o_N_(slip::constants<Real>::pi()/static_cast<Real>(N_-1))
    
  {}
  /**
   ** \brief Construct DCT-I coefficient functor 
   ** \param N. size of the DCT-I matrix.
   ** \param normalized. If true, the matrix coefficient are normalized.
   */
  dctI_coeff(const std::size_t N, bool normalized):
    N_(N+1),
    normalized_(normalized),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_-1))),
    pi_o_N_(slip::constants<Real>::pi()/static_cast<Real>(N_-1))
  {}

  /**
   ** \brief Computes the DCT-I coefficient at indices (i,j).
   ** \param i row index of the matrix.
   ** \param j column index of the matrix. 
   ** \return a real value
   */
  Real operator()(const std::size_t i,
		  const std::size_t j) const
  {
    const Real xij = std::cos(pi_o_N_*(Real(j*i)));
    Real result = coeff_*xij;
    if(normalized_)
      {
	if((i==0) or (i==N_-1))
	  {
	    result *= slip::constants<Real>::sqrt1_2();
	  }
	if((j==0) or (j==N_-1))
	  {
	    result *= slip::constants<Real>::sqrt1_2();
	  }
      }

    return result;
  }
  std::size_t N_;
  bool normalized_;
  Real coeff_;
  Real pi_o_N_;
};


 /**
   ** \brief functor to compute DCT-II matrix coefficients
   ** \f[ C^{II}_{N}[i][j] = \sqrt{\frac{2}{N}}\left[\epsilon_i\cos\left(\frac{\pi i(2j+1)}{2N}\right) \right],\, \forall i,j\in \{0,1,\cdots,N-1\}\f]
   ** where
   **  \f[\epsilon_p = \frac{1}{\sqrt{2}},\, \textrm{if}\, p=0\,\textrm{and}\, 1\,\textrm{else}\f] 
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
    ** \par Example:
   ** \code
   ** slip::Matrix<double> DCTII_8_norm;
   ** slip::dctII_coeff<double> dct_coeffII(8,true);
   ** dct_matrix(dct_coeffII,DCTII_8_norm);
   ** std::cout<<"DCTII_8_norm = \n"<<DCTII_8_norm<<std::endl;
   ** \endcode
   */  
template<typename Real=double>
struct dctII_coeff
{
  /**
   ** \brief Construct DCT-II coefficient functor with N=8 and normalized = true.
   */
  dctII_coeff():
    N_(8),
    normalized_(true),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_))),
    pi_o_2N_(slip::constants<Real>::pi()/(slip::constants<Real>::two()*static_cast<Real>(N_)))
    
  {}
  /**
   ** \brief Construct DCT-II coefficient functor 
   ** \param N. size of the DCT-II matrix.
   ** \param normalized. If true, the matrix coefficients are normalized.
   */
  dctII_coeff(const std::size_t N, bool normalized):
    N_(N),
    normalized_(normalized),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_))),
    pi_o_2N_(slip::constants<Real>::pi()/(slip::constants<Real>::two()*static_cast<Real>(N_)))
  {}
  
   /**
   ** \brief Computes the DCT-II coefficient at indices (i,j).
   ** \param i row index of the matrix.
   ** \param j column index of the matrix. 
   ** \return a real value
   */
  Real operator()(const std::size_t i,
		  const std::size_t j) const
  {
    const Real xij = std::cos(pi_o_2N_*(Real((2*j+1)*i)));
    Real result = coeff_*xij;
    if(normalized_)
      {
	if(i==0)
	  {
	    result *= slip::constants<Real>::sqrt1_2();
	  }
      }

    return result;
  }
  std::size_t N_;
  bool normalized_;
  Real coeff_;
  Real pi_o_2N_;
};

 /**
   ** \brief functor to compute DCT-III matrix coefficients
   ** \f[ C^{III}_{N}[i][j] = \sqrt{\frac{2}{N}}\left[\epsilon_j\cos\left(\frac{\pi (2i+1)j}{2N}\right) \right],\, \forall i,j\in \{0,1,\cdots,N-1\}\f]
   ** where
   **  \f[\epsilon_p = \frac{1}{\sqrt{2}},\, \textrm{if}\, p=0\,\textrm{and}\, 1\,\textrm{else}\f] 
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \par Example:
   ** \code
   ** slip::Matrix<double> DCTIII_8_norm;
   ** slip::dctIII_coeff<double> dct_coeffIII(8,true);
   ** dct_matrix(dct_coeffIII,DCTIII_8_norm);
   ** std::cout<<"DCTIII_8_norm = \n"<<DCTIII_8_norm<<std::endl;
   ** \endcode
   */  
template<typename Real=double>
struct dctIII_coeff
{
   /**
   ** \brief Construct DCT-III coefficient functor with N=8 and normalized = true.
   */
  dctIII_coeff():
    N_(8),
    normalized_(true),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_))),
    pi_o_2N_(slip::constants<Real>::pi()/(slip::constants<Real>::two()*static_cast<Real>(N_)))
    
  {}
  /**
   ** \brief Construct DCT-III coefficient functor 
   ** \param N. size of the DCT-III matrix.
   ** \param normalized. If true, the matrix coefficients are normalized.
   */
  dctIII_coeff(const std::size_t N, bool normalized):
    N_(N),
    normalized_(normalized),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_))),
    pi_o_2N_(slip::constants<Real>::pi()/(slip::constants<Real>::two()*static_cast<Real>(N_)))
  {}
    /**
   ** \brief Computes the DCT-III coefficient at indices (i,j).
   ** \param i row index of the matrix.
   ** \param j column index of the matrix.
   ** \return a real value
   */
  Real operator()(const std::size_t i,
		  const std::size_t j) const
  {
    const Real xij = std::cos(pi_o_2N_*Real((2*i+1)*j));
    Real result = coeff_*xij;
    if(normalized_)
      {
	if(j==0)
	  {
	    result *= slip::constants<Real>::sqrt1_2();
	  }
      }

    return result;
  }
  std::size_t N_;
  bool normalized_;
  Real coeff_;
  Real pi_o_2N_;
};


  /**
   ** \brief functor to compute DCT-IV matrix coefficients
   ** \f[ C^{IV}_{N}[i][j] = \sqrt{\frac{2}{N}}\left[\cos\left(\frac{\pi (2i+1)(2j+1)}{4N}\right) \right],\, \forall i,j\in \{0,1,\cdots,N-1\}\f].
   ** \author Benoit Tremblais <benoit.tremblais_AT.univ-poitiers.fr>
   ** \date 2024/04/01
   ** \version 0.0.1
   ** \since 1.5.0
   ** \par Example:
   **\code
   ** slip::Matrix<double> DCTIV_8;
   ** slip::dctIV_coeff<double> dct_coeffIV(8);
   ** dct_matrix(dct_coeffIV,DCTIV_8);
   ** std::cout<<"DCTIV_8 = \n"<<DCTIV_8<<std::endl;
   ** \endcode
   */  
template<typename Real=double>
struct dctIV_coeff
{
  /**
   ** \brief Construct DCT-IV coefficient functor with N=8.
   */
  dctIV_coeff():
    N_(8),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_))),
    pi_o_4N_(slip::constants<Real>::pi()/(static_cast<Real>(4.0)*static_cast<Real>(N_)))
    
  {}
  
  /**
   ** \brief Construct DCT-IV coefficient functor 
   ** \param N. size of the DCT-IV matrix.
   */
  dctIV_coeff(const std::size_t N):
    N_(N),
    coeff_(std::sqrt(slip::constants<Real>::two()/static_cast<Real>(N_))),
    pi_o_4N_(slip::constants<Real>::pi()/(static_cast<Real>(4.0)*static_cast<Real>(N_)))
  {}
  
  /**
   ** \brief Computes the DCT-IV coefficient at indices (i,j).
   ** \param i row index of the matrix.
   ** \param j column index of the matrix.
   ** \return a real value
   */
  Real operator()(const std::size_t i,
		  const std::size_t j) const
  {
    const Real xij = std::cos(pi_o_4N_*Real((2*i+1)*(2*j+1)));

    return coeff_*xij;
  }
  std::size_t N_;
  //bool normalized_;
  Real coeff_;
  Real pi_o_4N_;
};

  /**
   ** \brief Computes a DCT matrix according to a given DCT coefficient functor.
   ** \param func a DCT coefficient functor (slip::dctI_coeff, slip::dctII_coeff, slip::dctIII_coeff, slip::dctIV_coeff).
   ** The size of the matrix is computed according to the size N provided to the DCT functor.
   ** \param DCT. The DCT matrix.
   ** \par Example:
   ** \code
   ** slip::Matrix<double> DCTII_8_norm;
   ** slip::dctII_coeff<double> dct_coeffII(8,true);
   ** dct_matrix(dct_coeffII,DCTII_8_norm);
   ** std::cout<<"DCTII_8_norm = \n"<<DCTII_8_norm<<std::endl;
   ** \endcode
   */

template<typename Matrix1,
	 class DCTFunctor>
void dct_matrix(const DCTFunctor& func,
		Matrix1& DCT)
{
  const std::size_t N = func.N_;
  DCT.resize(N,N);
  for(std::size_t i = 0; i < N; ++i)
    {
      for(std::size_t j = 0; j < N; ++j)
	{
	  DCT[i][j] = func(i,j);
	}
    }
}

template<typename _Tp>
struct dct8_cos
{
  ///  Constant @f$ c_1 = 0.5\frac{\pi}{16} @f$.
  static _Tp c1() throw()
  { return static_cast<_Tp>(0.4903926402016152152896211191546172L); }
  ///  Constant @f$ c_2 = 0.5\frac{2\pi}{16} @f$.
  static _Tp c2() throw()
  { return static_cast<_Tp>(0.4619397662556433692415680525300559L); }
  ///  Constant @f$ c_3 = 0.5\frac{3\pi}{16} @f$.
  static _Tp c3() throw()
  { return static_cast<_Tp>(0.4157348061512726178357013395725517L); }
 ///  Constant @f$ c_4 = 0.5\frac{4\pi}{16} @f$.
  static _Tp c4() throw()
  { return static_cast<_Tp>(0.3535533905932737863686554646847071L); }
  ///  Constant @f$ c_5 = 0.5\frac{5\pi}{16} @f$.
  static _Tp c5() throw()
  { return static_cast<_Tp>(0.2777851165098011443355119354237104L); }
  ///  Constant @f$ c_6 = 0.5\frac{6\pi}{16} @f$.
  static _Tp c6() throw()
  { return static_cast<_Tp>(0.1913417161825449186451919558749069L); }
  ///  Constant @f$ c_7 = 0.5\frac{7\pi}{16} @f$.
  static _Tp c7() throw()
  { return static_cast<_Tp>(0.0975451610080641656752575840982900L); }
 
};
  /**
   ** \brief Computes the normalized 1d DCT-II.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2010/11/05
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param datain 1d container containing the data.
   ** \param dataout 1d container containing the DCT-II.
   ** \pre data type should be double float or long double
   ** \pre datain.size() == dataout.size()
   ** \par Example:
   ** \code
   ** slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** std::cout<<"dct = \n"<<A<<std::endl;
   ** slip::Vector<double> IdctA(8);
   ** slip::idct(A.begin(),A.end(),IdctA.begin());
   ** std::cout<<"IdctA = \n"<<IdctA<<std::endl;
   ** \endcode
   */
template<typename Container1d1,
	 typename Container1d2>
inline
void dct(const Container1d1 &datain, Container1d2 &dataout)
{
  assert(datain.size() == dataout.size());
  slip::dct(datain.begin(),datain.end(), dataout.begin());
}


 /**
   ** \brief Computes the normalized 1d DCT-III (inverse DCT-II).
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2010/11/05
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param datain 1d container containing the data.
   ** \param dataout 1d container containing the inverse DCT-II.
   ** \pre data type should be double float or long double
   ** \pre datain.size() == dataout.size()
   ** \par Example:
   ** \code
   ** slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** std::cout<<"dct = \n"<<A<<std::endl;
   ** slip::Vector<double> IdctA(8);
   ** slip::idct(A.begin(),A.end(),IdctA.begin());
   ** std::cout<<"IdctA = \n"<<IdctA<<std::endl;
   ** \endcode
   */  
template<typename Container1d1,
	 typename Container1d2>
inline
void idct(const Container1d1 &datain, Container1d2 &dataout)
{
  assert(datain.size() == dataout.size());
  slip::idct(datain.begin(),datain.end(), dataout.begin());
}  

 /**
   ** \brief Computes the normalized DCT-II of data size 8.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2010/11/05
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param first RandomAccessIterator to the first element of the data
   ** \param  last RandomAccessIterator to one past-the-end element of the 
   ** data.
   ** \param dct_first RandomAccessIterator to the first element of the
   ** dct.
   ** \par Complexity: 18 multiplications, 20 additions
   ** \par Description: 
   ** Computes the Discrete Cosinus Transform (DCT)-II of data size 8 using the algorihtm of:
   ** W. Chen, C.H. Smith, and S.C. Fralick, "A fast computational
   ** algorithm for the discrete cosine transform ", IEEE Trans. Commun., Vol.
   ** COM-25, p. 1004-1009, Sep. 1977.
   ** \pre data type should be double float or long double
   ** \pre (last - first == 8)
   ** \pre [dct_first,dct_first + (last-first) must be valid
   ** \todo optimize eliminating some operations it is possible to have only 16 multiplications
   ** \par Example:
   ** \code
   ** slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** 
   ** slip::dct_8(val1,val1+8,A.begin());
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Vector<double> IdctA(8);
   ** slip::idct_8(A.begin(),A.end(),IdctA.begin());
   ** std::cout<<"IdctA = \n"<<IdctA<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator1,
	   typename RandomAccessIterator2>
  void dct_8(RandomAccessIterator1 first,
	     RandomAccessIterator1 last,
	     RandomAccessIterator2 dct_first)
   {
     assert((last -first) == 8);
     typedef typename std::iterator_traits<RandomAccessIterator1>::value_type Real;
      
     const Real x0 = *first;
     const Real x1 = *(first+1);
     const Real x2 = *(first+2);
     const Real x3 = *(first+3);
     const Real x4 = *(first+4);
     const Real x5 = *(first+5);
     const Real x6 = *(first+6);
     const Real x7 = *(first+7);
     
        
     const Real e0 = x0 + x7;
     const Real e1 = x1 + x6;
     const Real e2 = x2 + x5;
     const Real e3 = x3 + x4;
    
     const Real o0 = x0 - x7;
     const Real o1 = x1 - x6;
     const Real o2 = x2 - x5;
     const Real o3 = x3 - x4;

  
     const Real c4_e0_p_e3 = dct8_cos<Real>::c4() * (e0 + e3);
     const Real e0_m_e3 = e0 - e3;
     const Real c4_e1_p_e2 = dct8_cos<Real>::c4() * (e1 + e2);
     const Real e1_m_e2 = e1 - e2;
     
     
     *(dct_first)     = (c4_e0_p_e3 + c4_e1_p_e2);
     *(dct_first + 2) = dct8_cos<Real>::c2() * e0_m_e3 + dct8_cos<Real>::c6() * e1_m_e2;
     *(dct_first + 4) = (c4_e0_p_e3  - c4_e1_p_e2); 
     *(dct_first + 6) = dct8_cos<Real>::c6() * e0_m_e3 - dct8_cos<Real>::c2() * e1_m_e2;
     *(dct_first + 1) = dct8_cos<Real>::c1() * o0 + dct8_cos<Real>::c3() * o1 + dct8_cos<Real>::c5() * o2 + dct8_cos<Real>::c7() * o3;
     *(dct_first + 3) = dct8_cos<Real>::c3() * o0 - dct8_cos<Real>::c7() * o1 - dct8_cos<Real>::c1() * o2 - dct8_cos<Real>::c5() * o3;
     *(dct_first + 5) = dct8_cos<Real>::c5() * o0 - dct8_cos<Real>::c1() * o1 + dct8_cos<Real>::c7() * o2 + dct8_cos<Real>::c3() * o3;
     *(dct_first + 7) = dct8_cos<Real>::c7() * o0 - dct8_cos<Real>::c5() * o1 + dct8_cos<Real>::c3() * o2 - dct8_cos<Real>::c1() * o3;
     
     
   }


/**
   ** \brief Computes the normalized DCT-III (Inverse DCT) of data size 8.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2010/11/05
   ** \version 0.0.1
   ** \param first RandomAccessIterator to the first element of the data
   ** \param  last RandomAccessIterator to one past-the-end element of the 
   ** data.
   ** \param idct_first RandomAccessIterator to the first element of the
   ** idct.
   ** \pre data type should be double float or long double
   ** \pre (last - first == 8)
   ** \pre [dct_first,dct_first + (last-first) must be valid
   ** \par Complexity: 20 multiplications, 22 additions
   ** \par Description: 
   ** Computes the Discrete Cosinus Transform (DCT)-III (Inverse DCT-II) of data size 8 using
   ** the algorithm of:
   ** W. Chen, C.H. Smith, and S.C. Fralick, "A fast computational
   ** algorithm for the discrete cosine transform ", IEEE Trans. Commun., Vol.
   ** COM-25, p. 1004-1009, Sep. 1977.
   ** \todo Optimize: it is possible to have only 12 multiplications
   ** \par Example:
   ** \code
   ** slip::Vector<double> A(8);
   ** double val1[] = {8,16,24,32,40,48,56,64};
   ** 
   ** slip::dct_8(val1,val1+8,A.begin());
   ** std::cout<<"A = \n"<<A<<std::endl;
   ** slip::Vector<double> IdctA(8);
   ** slip::idct_8(A.begin(),A.end(),IdctA.begin());
   ** std::cout<<"IdctA = \n"<<IdctA<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator1,
	   typename RandomAccessIterator2>
  void idct_8(RandomAccessIterator1 first,
	      RandomAccessIterator1 last,
	      RandomAccessIterator2 idct_first)
   {
     assert((last -first) == 8);
     typedef typename std::iterator_traits<RandomAccessIterator1>::value_type Real;
    
     const Real x0 = *first;
     const Real x1 = *(first+1);
     const Real x2 = *(first+2);
     const Real x3 = *(first+3);
     const Real x4 = *(first+4);
     const Real x5 = *(first+5);
     const Real x6 = *(first+6);
     const Real x7 = *(first+7);

     //Complexity: 22 multiplications, 22 additions
     const Real c4_x0_p_x4 = dct8_cos<Real>::c4() * (x0 + x4);
     const Real c4_x0_m_x4 = dct8_cos<Real>::c4() * (x0 - x4);
     const Real c2_x2 = dct8_cos<Real>::c2() * x2;
     const Real c6_x6 = dct8_cos<Real>::c6() * x6;
     const Real c2_x6 = dct8_cos<Real>::c2() * x6;
     const Real c6_x2 = dct8_cos<Real>::c6() * x2;

     const Real A0 = c4_x0_p_x4 + c2_x2 + c6_x6;
     const Real A1 = c4_x0_m_x4 + c6_x2 - c2_x6;
     const Real A2 = c4_x0_m_x4 - c6_x2 + c2_x6;
     const Real A3 = c4_x0_p_x4 - c2_x2 - c6_x6;

     const Real B0 = dct8_cos<Real>::c1() * x1 + dct8_cos<Real>::c3() * x3 + dct8_cos<Real>::c5() * x5 + dct8_cos<Real>::c7() * x7;
     const Real B1 = dct8_cos<Real>::c3() * x1 - dct8_cos<Real>::c7() * x3 - dct8_cos<Real>::c1() * x5 - dct8_cos<Real>::c5() * x7;
     const Real B2 = dct8_cos<Real>::c5() * x1 - dct8_cos<Real>::c1() * x3 + dct8_cos<Real>::c7() * x5 + dct8_cos<Real>::c3() * x7;
     const Real B3 = dct8_cos<Real>::c7() * x1 - dct8_cos<Real>::c5() * x3 + dct8_cos<Real>::c3() * x5 - dct8_cos<Real>::c1() * x7;

    
     *(idct_first)     =  (A0 + B0);
     *(idct_first + 1) =  (A1 + B1);
     *(idct_first + 2) =  (A2 + B2);
     *(idct_first + 3) =  (A3 + B3);
     *(idct_first + 4) =  (A3 - B3);
     *(idct_first + 5) =  (A2 - B2);
     *(idct_first + 6) =  (A1 - B1);
     *(idct_first + 7) =  (A0 - B0);
     
          
   }

  
/**
   ** \brief Computes the normalized DCT-II 2d of data size 8x8.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2010/11/05
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left RandomAccessIterator2d to the first element of the data
   ** \param bottom_right RandomAccessIterator2d to one past-the-end element of the 
   ** data.
   ** \param dct_upper_left RandomAccessIterator2d to the first element of the
   ** dct.
   ** \par Complexity: 16 * Complexity(idct_8)
   ** \pre (bottom_right - upper_left)[0] == 8
   ** \pre (bottom_right - upper_left)[1] == 8
   ** \pre [dct_upper_left, dct_upper_left + (bottom_right -
   ** upper_left)) must be valid
   ** \pre data type should be double float or long double
   ** \par Example:
   ** \code
   ** double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
   **		      63, 59, 66, 90, 109, 85, 69, 72,
   **		      62, 59, 68, 113, 144, 104, 66, 73,
   **		      63, 58, 71, 122, 154, 106, 70, 69,
   **		      67, 61, 68, 104, 126,  88, 68, 70,
   **		      79, 65, 60,  70,  77,  68, 58, 75,
   **		      85, 71, 64,  59,  55,  61, 65, 83,
   **		      87, 79, 69,  68,  65,  76, 78, 94};
   ** slip::GrayscaleImage<double> I(8,8,val);
   ** std::cout<<"I = \n"<<I<<std::endl;
   **
   ** I-=128;
   ** std::cout<<"I-128 = \n"<<I<<std::endl;
   **
   ** slip::GrayscaleImage<double> DCT(I.rows(),I.cols());
   ** slip::dct_8x8(I.upper_left(),I.bottom_right(),DCT.upper_left());
   ** std::cout<<"DCT(I) = \n"<<DCT<<std::endl;
   ** slip::GrayscaleImage<double> IDCT(I.rows(),I.cols());
   ** slip::idct_8x8(DCT.upper_left(),DCT.bottom_right(),
   ** 		     IDCT.upper_left());
   ** std::cout<<"IDCT(DCT(I)) = \n"<<IDCT<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator2d1,
	   typename RandomAccessIterator2d2>
  void dct_8x8(RandomAccessIterator2d1 upper_left,
	       RandomAccessIterator2d1 bottom_right,
	       RandomAccessIterator2d2 dct_upper_left)
  {
    assert((bottom_right - upper_left)[0] == 8);
    assert((bottom_right - upper_left)[1] == 8);
    typename RandomAccessIterator2d1::difference_type size2d = bottom_right - upper_left;
    typedef typename std::iterator_traits<RandomAccessIterator2d1>::value_type Real;
    const std::size_t rows = size2d[0];
    const std::size_t cols = size2d[1];
    slip::Array2d<Real> tmp(rows,cols,Real());
    for(size_t i = 0; i < rows; ++i)
      {
	slip::dct_8(upper_left.row_begin(i),upper_left.row_end(i),
		    tmp.row_begin(i));
      } 
    for(size_t j = 0; j < cols; ++j)
      {
	slip::dct_8(tmp.col_begin(j),tmp.col_end(j),
		    dct_upper_left.col_begin(j));
      } 
  }

  /**
   ** \brief Computes the normalized DCT-III 2d (Inverse DCT-II) of data size 8x8.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2010/11/05
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left RandomAccessIterator2d to the first element of the data
   ** \param bottom_right RandomAccessIterator2d to one past-the-end element of the 
   ** data.
   ** \param idct_upper_left RandomAccessIterator2d to the first element of the
   ** dct.
  
   ** \par Complexity: 16 * Complexity(idct_8)
   ** \pre (bottom_right - upper_left)[0] == 8
   ** \pre (bottom_right - upper_left)[1] == 8
   ** \pre [idct_upper_left, idct_upper_left + (bottom_right -
   ** upper_left)) must be valid
   ** \pre data type should be double float or long double
   ** \par Example:
   ** \code
   ** double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
   **		      63, 59, 66, 90, 109, 85, 69, 72,
   **		      62, 59, 68, 113, 144, 104, 66, 73,
   **		      63, 58, 71, 122, 154, 106, 70, 69,
   **		      67, 61, 68, 104, 126,  88, 68, 70,
   **		      79, 65, 60,  70,  77,  68, 58, 75,
   **		      85, 71, 64,  59,  55,  61, 65, 83,
   **		      87, 79, 69,  68,  65,  76, 78, 94};
   ** slip::GrayscaleImage<double> I(8,8,val);
   ** std::cout<<"I = \n"<<I<<std::endl;
   **
   ** I-=128;
   ** std::cout<<"I-128 = \n"<<I<<std::endl;
   **
   ** slip::GrayscaleImage<double> DCT(I.rows(),I.cols());
   ** slip::dct_8x8(I.upper_left(),I.bottom_right(),DCT.upper_left());
   ** std::cout<<"DCT(I) = \n"<<DCT<<std::endl;
   ** slip::GrayscaleImage<double> IDCT(I.rows(),I.cols());
   ** slip::idct_8x8(DCT.upper_left(),DCT.bottom_right(),
   ** 		     IDCT.upper_left());
   ** std::cout<<"IDCT(DCT(I)) = \n"<<IDCT<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator2d1,
	   typename RandomAccessIterator2d2>
  void idct_8x8(RandomAccessIterator2d1 upper_left,
		RandomAccessIterator2d1 bottom_right,
		
		RandomAccessIterator2d2 idct_upper_left)
  {
     assert((bottom_right - upper_left)[0] == 8);
     assert((bottom_right - upper_left)[1] == 8);
    typename RandomAccessIterator2d1::difference_type size2d = bottom_right - upper_left;
    typedef typename std::iterator_traits<RandomAccessIterator2d1>::value_type Real;
    const std::size_t rows = size2d[0];
    const std::size_t cols = size2d[1];
    slip::Array2d<Real> tmp(rows,cols,Real(0));
    for(size_t i = 0; i < rows; ++i)
      {
	slip::idct_8(upper_left.row_begin(i),upper_left.row_end(i),
		     tmp.row_begin(i));
      } 
    for(size_t j = 0; j < cols; ++j)
      {
	slip::idct_8(tmp.col_begin(j),tmp.col_end(j),
		     idct_upper_left.col_begin(j));
      } 
  }



 


  /**
   ** \brief Computes the normalized DCT-II 2d with 2 1d DCT.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2010/11/05
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left RandomAccessIterator2d to the first element of the data
   ** \param bottom_right RandomAccessIterator2d to one past-the-end element of the 
   ** data.
   ** \param dct_upper_left RandomAccessIterator2d to the first element of the
   ** dct.
   ** \pre data type should be double float or long double
   ** \par Example:
   ** \code
   ** double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
   **		  63, 59, 66, 90, 109, 85, 69, 72,
   **		  62, 59, 68, 113, 144, 104, 66, 73,
   **		  63, 58, 71, 122, 154, 106, 70, 69,
   **		  67, 61, 68, 104, 126,  88, 68, 70,
   **		  79, 65, 60,  70,  77,  68, 58, 75,
   **		  85, 71, 64,  59,  55,  61, 65, 83,
   **		  87, 79, 69,  68,  65,  76, 78, 94};
   ** slip::GrayscaleImage<double> I(8,8,val);
   ** std::cout<<"I = \n"<<I<<std::endl;
   **
   ** I-=128;
   ** std::cout<<"I-128 = \n"<<I<<std::endl;
   ** slip::GrayscaleImage<double> DST(I.rows(),I.cols());
   ** slip::dct2d(I.upper_left(),I.bottom_right(),DCT.upper_left());
   ** std::cout<<"DCT(I) = \n"<<DCT<<std::endl;
   ** slip::GrayscaleImage<double> IDCT(I.rows(),I.cols());
   ** slip::idct2d(DCT.upper_left(),DCT.bottom_right(),
   **		   IDCT.upper_left());
   ** std::cout<<"IDCT(DCT(I)) = \n"<<IDCT<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator2d1,
	   typename RandomAccessIterator2d2>
  void dct2d(RandomAccessIterator2d1 upper_left,
	     RandomAccessIterator2d1 bottom_right,
	     RandomAccessIterator2d2 dct_upper_left)
  {
     typename RandomAccessIterator2d1::difference_type size2d = bottom_right - upper_left;
     typedef typename std::iterator_traits<RandomAccessIterator2d1>::value_type Real;
    const std::size_t rows = size2d[0];
    const std::size_t cols = size2d[1];
 
    std::fill_n(dct_upper_left,rows*cols,Real());    

    for(size_t i = 0; i < rows; ++i)
      {
	slip::dct(upper_left.row_begin(i),upper_left.row_end(i),
		  dct_upper_left.row_begin(i));
	//normalization
	*(dct_upper_left.row_begin(i)) /= slip::constants<Real>::sqrt2();
      } 


    slip::Array<Real> tmp_col(rows);
    slip::Array<Real> tmp(rows);
    for(size_t j = 0; j < cols; ++j)
      {
	//copy 
	std::copy(dct_upper_left.col_begin(j),dct_upper_left.col_end(j),
		  tmp_col.begin());
	slip::dct(tmp_col.begin(),tmp_col.end(),
	 	  tmp.begin());
	tmp[0] /= slip::constants<Real>::sqrt2();
	std::copy(tmp.begin(),tmp.end(),dct_upper_left.col_begin(j));

      } 
   
  }

  /**
   ** \brief Computes the normalized DCT-III 2d (Inverse DCT-II).
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2010/11/05
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left RandomAccessIterator2d to the first element of the data
   ** \param bottom_right RandomAccessIterator2d to one past-the-end element of the 
   ** data.
   ** \param idct_upper_left RandomAccessIterator2d to the first element of the
   ** dct.
   ** \par Example:
   ** \code
   ** double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
   **		  63, 59, 66, 90, 109, 85, 69, 72,
   **		  62, 59, 68, 113, 144, 104, 66, 73,
   **		  63, 58, 71, 122, 154, 106, 70, 69,
   **		  67, 61, 68, 104, 126,  88, 68, 70,
   **		  79, 65, 60,  70,  77,  68, 58, 75,
   **		  85, 71, 64,  59,  55,  61, 65, 83,
   **		  87, 79, 69,  68,  65,  76, 78, 94};
   ** slip::GrayscaleImage<double> I(8,8,val);
   ** std::cout<<"I = \n"<<I<<std::endl;
   **
   ** I-=128;
   ** std::cout<<"I-128 = \n"<<I<<std::endl;
   ** slip::GrayscaleImage<double> DST(I.rows(),I.cols());
   ** slip::dct2d(I.upper_left(),I.bottom_right(),DCT.upper_left());
   ** std::cout<<"DCT(I) = \n"<<DCT<<std::endl;
   ** slip::GrayscaleImage<double> IDCT(I.rows(),I.cols());
   ** slip::idct2d(DCT.upper_left(),DCT.bottom_right(),
   **		   IDCT.upper_left());
   ** std::cout<<"IDCT(DCT(I)) = \n"<<IDCT<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator2d1,
	   typename RandomAccessIterator2d2>
  void idct2d(RandomAccessIterator2d1 upper_left,
	      RandomAccessIterator2d1 bottom_right,
	      RandomAccessIterator2d2 idct_upper_left)
  {
    typename RandomAccessIterator2d1::difference_type size2d = bottom_right - upper_left;
    typedef typename std::iterator_traits<RandomAccessIterator2d1>::value_type Real;
    const std::size_t rows = size2d[0];
    const std::size_t cols = size2d[1];
    
    std::fill_n(idct_upper_left,rows*cols,Real());    

    slip::Array<Real> row_tmp(cols);
    for(size_t i = 0; i < rows; ++i)
      {
	//copy 
	std::copy(upper_left.row_begin(i),upper_left.row_end(i),row_tmp.begin());
	row_tmp[0] *=  slip::constants<Real>::sqrt2();
	slip::idct(row_tmp.begin(),row_tmp.end(),idct_upper_left.row_begin(i));
      } 

    slip::Array<Real> col_tmp(rows);
    slip::Array<Real> tmp(rows);
    for(size_t j = 0; j < cols; ++j)
      {
	//copy 
	std::copy(idct_upper_left.col_begin(j),idct_upper_left.col_end(j),
		  col_tmp.begin());
	col_tmp[0] *= slip::constants<Real>::sqrt2();
	//  iDCT
	slip::idct(col_tmp.begin(),col_tmp.end(),
		   tmp.begin());
	
	std::copy(tmp.begin(),tmp.end(),idct_upper_left.col_begin(j));
	
      } 
     
   
  }
  /**
 ** \brief Computes the normalized 2d DCT-II with 2 1d DCT-II.
 ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
 ** \date 2010/11/05
 ** \version 0.0.1
 ** \since 1.5.0
 ** \param datain Container2d containing the data.
 ** \param dataout Container2d containing the 2d DCT-II.
 ** \pre data type of the Containers should be double float or long double.
 ** \par Example:
 ** \code
 ** double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
 **		    63, 59, 66, 90, 109, 85, 69, 72,
 **		    62, 59, 68, 113, 144, 104, 66, 73,
 **		    63, 58, 71, 122, 154, 106, 70, 69,
 **		    67, 61, 68, 104, 126,  88, 68, 70,
 **		    79, 65, 60,  70,  77,  68, 58, 75,
 **		    85, 71, 64,  59,  55,  61, 65, 83,
 **		    87, 79, 69,  68,  65,  76, 78, 94};
 ** slip::GrayscaleImage<double> I(8,8,val);
 ** std::cout<<"I = \n"<<I<<std::endl;
 ** 
 ** I-=128;
 ** std::cout<<"I-128 = \n"<<I<<std::endl;
 ** slip::GrayscaleImage<double> DCT(I.rows(),I.cols());
 ** slip::dct2d(I,DCT);
 ** std::cout<<"DCT(I) = \n"<<DCT<<std::endl;
 ** slip::GrayscaleImage<double> IDCT(I.rows(),I.cols());
 ** slip::idct2d(DCT,IDCT);
 ** std::cout<<"IDCT(DCT) = \n"<<IDCT<<std::endl;
 ** \endcode
 */
  template<typename Matrix1,
	   typename Matrix2>
  inline
  void dct2d(const Matrix1 &datain, Matrix2 &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    slip::dct2d(datain.upper_left(),datain.bottom_right(),
	       dataout.upper_left());
  }

    /**
 ** \brief Computes the  normalized2d DCT-III (inverse DCT-II)  with 2 1d IDCT-II.
 ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
 ** \date 2010/11/05
 ** \version 0.0.1
 ** \since 1.5.0
 ** \param datain Container2d containing the data.
 ** \param dataout Container2d containing the 2d inverse DCT-II.
 ** \pre data type of the Containers should be double float or long double.
 ** \par Example:
 ** \code
 ** double val[] = {52, 55, 61, 66, 70, 61, 64, 73,
 **		   63, 59, 66, 90, 109, 85, 69, 72,
 **		   62, 59, 68, 113, 144, 104, 66, 73,
 **		   63, 58, 71, 122, 154, 106, 70, 69,
 **		   67, 61, 68, 104, 126,  88, 68, 70,
 **		   79, 65, 60,  70,  77,  68, 58, 75,
 **		   85, 71, 64,  59,  55,  61, 65, 83,
 **		   87, 79, 69,  68,  65,  76, 78, 94};
 ** slip::GrayscaleImage<double> I(8,8,val);
 ** std::cout<<"I = \n"<<I<<std::endl;
 ** 
 ** I-=128;
 ** std::cout<<"I-128 = \n"<<I<<std::endl;
 ** slip::GrayscaleImage<double> DCT(I.rows(),I.cols());
 ** slip::dct2d(I,DCT);
 ** std::cout<<"DCT(I) = \n"<<DCT<<std::endl;
 ** slip::GrayscaleImage<double> IDCT(I.rows(),I.cols());
 ** slip::idct2d(DCT,IDCT);
 ** std::cout<<"IDCT(DCT) = \n"<<IDCT<<std::endl;
 ** \endcode
 */
  
  template<typename Matrix1,
	   typename Matrix2>
  inline
  void idct2d(const Matrix1 &datain, Matrix2 &dataout)
  {
    assert(datain.cols() == dataout.cols());
    assert(datain.rows() == dataout.rows());
    slip::idct2d(datain.upper_left(),datain.bottom_right(),
		 dataout.upper_left());
  }
  
/**
   ** \brief Computes the normalized DCT-II 3d with 3 1d DCT.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2010/11/05
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left RandomAccessIterator3d to the first element of the data
   ** \param bottom_right RandomAccessIterator3d to one past-the-end element of the 
   ** data.
   ** \param dct_upper_left RandomAccessIterator3d to the first element of the
   ** dct.
   ** \pre data type should be double float or long double
   ** \par Example:
   ** \code
   ** slip::Volume<double> Vol(8,8,8);
   ** slip::iota(Vol.begin(),Vol.end(),0.0,1.0);
   **
   ** slip::Volume<double> DCTVol(8,8,8);
   ** slip::dct3d(Vol.front_upper_left(),
   **	          Vol.back_bottom_right(),
   **	          DCTVol.front_upper_left());
   ** std::cout<<"DCTVol = \n"<<DCTVol<<std::endl;
   ** slip::Volume<double> IDCTVol(8,8,8);
   ** slip::idct3d(DCTVol.front_upper_left(),
   **		   DCTVol.back_bottom_right(),
   **		   IDCTVol.front_upper_left());
   ** std::cout<<"IDCTVol = \n"<<IDCTVol<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator3d1,
	   typename RandomAccessIterator3d2>
  void dct3d(RandomAccessIterator3d1 front_upper_left,
	     RandomAccessIterator3d1 back_bottom_right,
	     RandomAccessIterator3d2 dct_front_upper_left)
  {
     typename RandomAccessIterator3d1::difference_type size3d = back_bottom_right - front_upper_left;
     typedef typename std::iterator_traits<RandomAccessIterator3d1>::value_type Real;
     const std::size_t slices = size3d[0];
     const std::size_t rows   = size3d[1];
     const std::size_t cols   = size3d[2];
     const std::size_t S = slices * rows * cols;
     std::fill_n(dct_front_upper_left,S,Real());    
     for(size_t k = 0; k < slices; ++k)
       {
	 for(size_t i = 0; i < rows; ++i)
	   {
	     slip::dct(front_upper_left.row_begin(k,i),
		       front_upper_left.row_end(k,i),
		       dct_front_upper_left.row_begin(k,i));
	     //normalization
	     *(dct_front_upper_left.row_begin(k,i)) /= slip::constants<Real>::sqrt2();
	   } 
       }


    slip::Array<Real> tmp_col(rows);
    slip::Array<Real> tmp(rows);
    for(size_t k = 0; k < slices; ++k)
      {
    	for(size_t j = 0; j < cols; ++j)
    	  {
    	    //copy 
    	    std::copy(dct_front_upper_left.col_begin(k,j),
    		      dct_front_upper_left.col_end(k,j),
    		      tmp_col.begin());
    	    slip::dct(tmp_col.begin(),tmp_col.end(),
    		      tmp.begin());
    	    tmp[0] /= slip::constants<Real>::sqrt2();
    	    std::copy(tmp.begin(),tmp.end(),dct_front_upper_left.col_begin(k,j));
    	  }
      } 

    slip::Array<Real> tmp_slice(slices);
    slip::Array<Real> tmp2(slices);
    for(size_t i= 0; i < rows; ++i)
      {
    	for(size_t j = 0; j < cols; ++j)
    	  {
    	    //copy 
    	    std::copy(dct_front_upper_left.slice_begin(i,j),
    		      dct_front_upper_left.slice_end(i,j),
    		      tmp_slice.begin());
    	    slip::dct(tmp_slice.begin(),tmp_slice.end(),
    		      tmp2.begin());
    	    tmp2[0] /= slip::constants<Real>::sqrt2();
    	    std::copy(tmp2.begin(),tmp2.end(),
    		      dct_front_upper_left.slice_begin(i,j));
    	  }
      } 
   
  }

/**
   ** \brief Computes the normalized inverse DCT-III 3d with 3 1d IDCT.
   ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
   ** \date 2010/11/05
   ** \version 0.0.1
   ** \since 1.5.0
   ** \param upper_left RandomAccessIterator3d to the first element of the data
   ** \param bottom_right RandomAccessIterator3d to one past-the-end element of the 
   ** data.
   ** \param idct_upper_left RandomAccessIterator3d to the first element of the
   ** idct.
   ** \pre data type should be double float or long double
   ** \par Example:
   ** \code
   ** slip::Volume<double> Vol(8,8,8);
   ** slip::iota(Vol.begin(),Vol.end(),0.0,1.0);
   **
   ** slip::Volume<double> DCTVol(8,8,8);
   ** slip::dct3d(Vol.front_upper_left(),
   **	          Vol.back_bottom_right(),
   **	          DCTVol.front_upper_left());
   ** std::cout<<"DCTVol = \n"<<DCTVol<<std::endl;
   ** slip::Volume<double> IDCTVol(8,8,8);
   ** slip::idct3d(DCTVol.front_upper_left(),
   **		   DCTVol.back_bottom_right(),
   **		   IDCTVol.front_upper_left());
   ** std::cout<<"IDCTVol = \n"<<IDCTVol<<std::endl;
   ** \endcode
   */
  template<typename RandomAccessIterator3d1,
	   typename RandomAccessIterator3d2>
  void idct3d(RandomAccessIterator3d1 front_upper_left,
	      RandomAccessIterator3d1 back_bottom_right,
	      RandomAccessIterator3d2 idct_front_upper_left)
{
  typename RandomAccessIterator3d1::difference_type size3d = back_bottom_right - front_upper_left;
  typedef typename std::iterator_traits<RandomAccessIterator3d1>::value_type Real;

  const std::size_t slices = size3d[0];
  const std::size_t rows   = size3d[1];
  const std::size_t cols   = size3d[2];
  const std::size_t S = slices * rows * cols;
  
    
  std::fill_n(idct_front_upper_left,S,Real(0));    

  slip::Array<Real> row_tmp(cols);
  for(size_t k = 0; k < slices; ++k)
    {
      for(size_t i = 0; i < rows; ++i)
	{
	  //copy 
	  std::copy(front_upper_left.row_begin(k,i),
		    front_upper_left.row_end(k,i),
		    row_tmp.begin());
	  row_tmp[0] *=  slip::constants<Real>::sqrt2();
	  slip::idct(row_tmp.begin(),row_tmp.end(),
		     idct_front_upper_left.row_begin(k,i));
	} 
    }

  slip::Array<Real> col_tmp(rows);
  slip::Array<Real> tmp(rows);
  for(size_t k = 0; k < slices; ++k)
    {
      for(size_t j = 0; j < cols; ++j)
  	{
  	  //copy 
  	  std::copy(idct_front_upper_left.col_begin(k,j),
  		    idct_front_upper_left.col_end(k,j),
  		    col_tmp.begin());
  	  col_tmp[0] *= slip::constants<Real>::sqrt2();
  	  //  iDCT
  	  slip::idct(col_tmp.begin(),col_tmp.end(),
  		     tmp.begin());
	
  	  std::copy(tmp.begin(),tmp.end(),
  		    idct_front_upper_left.col_begin(k,j));
	  
  	} 
    }
     
  slip::Array<Real> slice_tmp(slices);
  slip::Array<Real> tmp2(slices);
  for(size_t i = 0; i < rows; ++i)
    {
      for(size_t j = 0; j < cols; ++j)
  	{
  	  //copy 
  	  std::copy(idct_front_upper_left.slice_begin(i,j),
  		    idct_front_upper_left.slice_end(i,j),
  		    slice_tmp.begin());
  	  slice_tmp[0] *= slip::constants<Real>::sqrt2();
  	  //  iDCT
  	  slip::idct(slice_tmp.begin(),slice_tmp.end(),
  		     tmp2.begin());
	
  	  std::copy(tmp2.begin(),tmp2.end(),
  		    idct_front_upper_left.slice_begin(i,j));
	  
  	} 
    }
  }

/**
 ** \brief Computes the normalized 3d DCT-II with 3 1d DCT.
 ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
 ** \date 2010/11/05
 ** \version 0.0.1
 ** \since 1.5.0
 ** \param datain Container3d containing the data.
 ** \param dataout Container3d containing the 3d DCT-II.
 ** \pre data type of the Containers should be double float or long double.
 ** \par Example:
 ** \code
 ** slip::Volume<double> Vol(8,8,8);
 ** slip::iota(Vol.begin(),Vol.end(),0.0,1.0);
 ** slip::Volume<double> DCTVol(8,8,8);
 ** slip::dct3d(Vol,DCTVol);
 ** std::cout<<"DCT(Vol) = \n"<<DCTVol<<std::endl;
 ** slip::Volume<double> IDCTVol(8,8,8);
 ** slip::idct3d(DCTVol,IDCTVol);
 ** std::cout<<"IDCTVol = \n"<<IDCTVol<<std::endl;
 ** \endcode
 */
template<typename Volume1,
	 typename Volume2>
inline
void dct3d(const Volume1 &datain, Volume2 &dataout)
{
  assert(datain.cols() == dataout.cols());
  assert(datain.rows() == dataout.rows());
  assert(datain.slices() == dataout.slices());
  slip::dct3d(datain.front_upper_left(),datain.back_bottom_right(),
	      dataout.front_upper_left());
  
}
/**
 ** \brief Computes the normalized 3d DCT-III (inverse DCT-II)  with 3 1d DCT.
 ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
 ** \date 2010/11/05
 ** \version 0.0.1
 ** \since 1.5.0
 ** \param datain Container3d containing the data.
 ** \param dataout Container3d containing the 3d inverse DCT-II.
 ** \pre data type of the Containers should be double float or long double.
 ** \par Example:
 ** \code
 ** slip::Volume<double> Vol(8,8,8);
 ** slip::iota(Vol.begin(),Vol.end(),0.0,1.0);
 ** slip::Volume<double> DCTVol(8,8,8);
 ** slip::dct3d(Vol,DCTVol);
 ** std::cout<<"DCT(Vol) = \n"<<DCTVol<<std::endl;
 ** slip::Volume<double> IDCTVol(8,8,8);
 ** slip::idct3d(DCTVol,IDCTVol);
 ** std::cout<<"IDCTVol = \n"<<IDCTVol<<std::endl;
 ** \endcode
 */
template<typename Volume1,
	 typename Volume2>
inline
void idct3d(const Volume1 &datain, Volume2 &dataout)
{
  assert(datain.cols() == dataout.cols());
  assert(datain.rows() == dataout.rows());
  assert(datain.slices() == dataout.slices());
  slip::idct3d(datain.front_upper_left(),datain.back_bottom_right(),
	       dataout.front_upper_left());
  
}

}//slip::


#endif //SLIP_DCT_HPP
