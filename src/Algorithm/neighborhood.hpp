/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file neighborhood.hpp
 * 
 * \brief Provides functors getting the classical image processing neighborhood.
 * 
 */

#ifndef SLIP_NEIGHBORHOOD_HPP
#define SLIP_NEIGHBORHOOD_HPP

#include <vector>
#include "neighbors.hpp"
#include "iterator_types.hpp"


namespace slip
{

 /*!
 ** @defgroup nbors Group of neighborhood 
 ** @brief Group of the neighborhoods, successors and predecessors
 *  @{
 ** 
 */
 /*@} End Neighborhood */
  

 /*!
 ** @defgroup nbors1d 1d neighborhood 
 ** \ingroup nbors
 ** @brief Group of the 1d neighborhood, successors and predecessors
 *  @{
 ** 
 */
template<typename Container>
struct SafeN2C
{
  SafeN2C(const Container& cont):
    cont_(&cont),
    size_(static_cast<int>(cont.size())),
    size_max_( slip::n_2c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(0);
    int it_i = static_cast<int>(it - cont_->begin());
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::n_2c[n];
	if(i >= 0)
	  {
	    if(i < size_)
	      {
		neigh.push_back(it + slip::n_2c[n]);
	      }
	  }
      }
  }

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(0);
    int it_i = static_cast<int>(it - cont_->begin());
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::n_2c[n];
	if(i >= 0)
	  {
	    if(i < size_)
	      {
		neigh.push_back(it[slip::n_2c[n]]);
	      }
	  }
      }
  }
  const Container* cont_;
  int size_;
  int size_max_;
 };

struct N2C 
{
  
  N2C():size_max_(2)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it + slip::n_2c[n];
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it[slip::n_2c[n]];
      }
  }

  int size_max_ ;
};

  template<typename Container>
struct SafePrev2C
{
  SafePrev2C(const Container& cont):
    cont_(&cont),
    size_(static_cast<int>(cont.size())),
    size_max_( slip::prev_2c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(0);
    int it_i = static_cast<int>(it - cont_->begin());
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::prev_2c[n];
	if(i >= 0)
	  {
	    if(i < size_)
	      {
		neigh.push_back(it + slip::prev_2c[n]);
	      }
	  }
      }
  }

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type> & neigh) const
  {
    neigh.resize(0);
    int it_i = static_cast<int>(it - cont_->begin());
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::prev_2c[n];
	if(i >= 0)
	  {
	    if(i < size_)
	      {
		neigh.push_back(it[slip::prev_2c[n]]);
	      }
	  }
      }
  }

  const Container* cont_;
  int size_;
  int size_max_;
 };


struct Prev2C 
{
  
  Prev2C():size_max_(1)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it + slip::prev_2c[n];
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it[slip::prev_2c[n]];
      }
  }

  int size_max_ ;
};


template<typename Container>
struct SafeNext2C
{
  SafeNext2C(const Container& cont):
    cont_(&cont),
    size_(static_cast<int>(cont.size())),
    size_max_( slip::next_2c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(0);
    int it_i = static_cast<int>(it - cont_->begin());
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::next_2c[n];
	if(i >= 0)
	  {
	    if(i < size_)
	      {
		neigh.push_back(it + slip::next_2c[n]);
	      }
	  }
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type> & neigh) const
  {
    neigh.resize(0);
    int it_i = static_cast<int>(it - cont_->begin());
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::next_2c[n];
	if(i >= 0)
	  {
	    if(i < size_)
	      {
		neigh.push_back(it[slip::next_2c[n]]);
	      }
	  }
      }
  }

  const Container* cont_;
  int size_;
  int size_max_;
 };


struct Next2C 
{
  
  Next2C():size_max_(1)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it + slip::next_2c[n];
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it[slip::next_2c[n]];
      }
  }

  int size_max_ ;
};
/*@} End 1D Neighborhood */

  
  
 /*!
 ** @defgroup nbors2d 2d neighborhood 
 ** \ingroup nbors
 ** @brief Group of the 2d neighborhood, successors and predecessors
 *  @{
 ** 
 */
template<typename Container>
struct SafeN8C
{
  SafeN8C(const Container& cont):
    cont_(&cont),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::n_8c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::n_8c[n][0];
	if(i >= 0)
	  {
	    if(i < rows_)
	      {
		int j = it_j + slip::n_8c[n][1];
		if(j >= 0)
		  {
		    if(j < cols_)
		      {
			neigh.push_back(it + slip::n_8c[n]);
		      }
		  }
	      }
	  }
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::n_8c[n][0];
	if(i >= 0)
	  {
	    if(i < rows_)
	      {
		int j = it_j + slip::n_8c[n][1];
		if(j >= 0)
		  {
		    if(j < cols_)
		      {
			neigh.push_back(it[slip::n_8c[n]]);
		      }
		  }
	      }
	  }
      }
  }

  const Container* cont_;
  int rows_;
  int cols_;
  int size_max_;
 };



struct N8C 
{
  
  N8C():size_max_(8)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it + slip::n_8c[n];
      }
  }

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it[slip::n_8c[n]];
      }
  }

  int size_max_ ;
 };



template<typename Container>
struct SafePrev8C
{
  SafePrev8C(const Container& cont):
    cont_(&cont),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::prev_8c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::prev_8c[n][0];
	if(i >= 0)
	  {
	    if(i < rows_)
	      {
		int j = it_j + slip::prev_8c[n][1];
		if(j >= 0)
		  {
		    if(j < cols_)
		      {
			neigh.push_back(it + slip::prev_8c[n]);
		      }
		  }
	      }
	  }
      }
  }
   
  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::prev_8c[n][0];
	if(i >= 0)
	  {
	    if(i < rows_)
	      {
		int j = it_j + slip::prev_8c[n][1];
		if(j >= 0)
		  {
		    if(j < cols_)
		      {
			neigh.push_back(it[slip::prev_8c[n]]);
		      }
		  }
	      }
	  }
      }
  }

  const Container* cont_;
  int rows_;
  int cols_;
  int size_max_;
 };

struct Prev8C 
{
  
  Prev8C():size_max_(4)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it + slip::prev_8c[n];
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it[slip::prev_8c[n]];
      }
  }

  int size_max_ ;
 };



template<typename Container>
struct SafeNext8C
{
  SafeNext8C(const Container& cont):
    cont_(&cont),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::next_8c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::next_8c[n][0];
	if(i >= 0)
	  {
	    if(i < rows_)
	      {
		int j = it_j + slip::next_8c[n][1];
		if(j >= 0)
		  {
		    if(j < cols_)
		      {
			neigh.push_back(it + slip::next_8c[n]);
		      }
		  }
	      }
	  }
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::next_8c[n][0];
	if(i >= 0)
	  {
	    if(i < rows_)
	      {
		int j = it_j + slip::next_8c[n][1];
		if(j >= 0)
		  {
		    if(j < cols_)
		      {
			neigh.push_back(it[slip::next_8c[n]]);
		      }
		  }
	      }
	  }
      }
  }

  const Container* cont_;
  int rows_;
  int cols_;
  int size_max_;
 };

struct Next8C 
{
  
  Next8C():size_max_(4)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
     neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it + slip::next_8c[n];
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
     neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it[slip::next_8c[n]];
      }
  }

  int size_max_ ;
 };


template<typename Container>
struct SafeN4C
{
  SafeN4C(const Container& cont):
    cont_(&cont),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::n_4c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::n_4c[n][0];
	if(i >= 0)
	  {
	    if(i < rows_)
	      {
		int j = it_j + slip::n_4c[n][1];
		if(j >= 0)
		  {
		    if(j < cols_)
		      {
			neigh.push_back(it + slip::n_4c[n]);
		      }
		  }
	      }
	  }
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::n_4c[n][0];
	if(i >= 0)
	  {
	    if(i < rows_)
	      {
		int j = it_j + slip::n_4c[n][1];
		if(j >= 0)
		  {
		    if(j < cols_)
		      {
			neigh.push_back(it[slip::n_4c[n]]);
		      }
		  }
	      }
	  }
      }
  }

  const Container* cont_;
  int rows_;
  int cols_;
  int size_max_;
 };


struct N4C 
{
  
  N4C():size_max_(4)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
	neigh[n] = it + slip::n_4c[n];
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
	neigh[n] = it[slip::n_4c[n]];
      }
  }

  int size_max_ ;
 };


template<typename Container>
struct SafeNext4C
{
  SafeNext4C(const Container& cont):
    cont_(&cont),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::next_4c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
     neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::next_4c[n][0];
	if(i >= 0)
	  {
	    if(i < rows_)
	      {
		int j = it_j + slip::next_4c[n][1];
		if(j >= 0)
		  {
		    if(j < cols_)
		      {
			neigh.push_back(it + slip::next_4c[n]);
		      }
		  }
	      }
	  }
      }
  }

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
     neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::next_4c[n][0];
	if(i >= 0)
	  {
	    if(i < rows_)
	      {
		int j = it_j + slip::next_4c[n][1];
		if(j >= 0)
		  {
		    if(j < cols_)
		      {
			neigh.push_back(it[slip::next_4c[n]]);
		      }
		  }
	      }
	  }
      }
  }

  const Container* cont_;
  int rows_;
  int cols_;
  int size_max_;
 };

template<typename Container>
struct SafePrev4C
{
  SafePrev4C(const Container& cont):
    cont_(&cont),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::prev_4c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
     
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::prev_4c[n][0];
	if(i >= 0)
	  {
	    if(i < rows_)
	      {
		int j = it_j + slip::prev_4c[n][1];
		if(j >= 0)
		  {
		    if(j < cols_)
		      {
			neigh.push_back(it + slip::prev_4c[n]);
		      }
		  }
	      }
	  }
      }
  }

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
     
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    for(int n = 0; n < size_max_; ++n)
      {
	int i = it_i + slip::prev_4c[n][0];
	if(i >= 0)
	  {
	    if(i < rows_)
	      {
		int j = it_j + slip::prev_4c[n][1];
		if(j >= 0)
		  {
		    if(j < cols_)
		      {
			neigh.push_back(it[slip::prev_4c[n]]);
		      }
		  }
	      }
	  }
      }
  }

  const Container* cont_;
  int rows_;
  int cols_;
  int size_max_;
 };

struct Prev4C 
{
  
  Prev4C():size_max_(2)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it + slip::prev_4c[n];
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it[slip::prev_4c[n]];
      }
  }

  int size_max_ ;
 };

struct Next4C 
{
  
  Next4C():size_max_(2)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it + slip::next_4c[n];
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it[slip::next_4c[n]];
      }
  }

  int size_max_ ;
 };


template<typename Container>
struct SafePseudoHexagonal
{
  SafePseudoHexagonal(const Container& cont):
    cont_(&cont),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::n_6co.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    if(it.i()%2 == 0)
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    int i = it_i + slip::n_6co[n][0];
	    if(i >= 0)
	      {
		if(i < rows_)
		  {
		    int j = it_j + slip::n_6co[n][1];
		    if(j >= 0)
		      {
			if(j < cols_)
			  {
			    neigh.push_back(it + slip::n_6co[n]);
			  }
		      }
		  }
	      }
	  }
      }
    else
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    int i = it_i + slip::n_6ce[n][0];
	    if(i >= 0)
	      {
		if(i < rows_)
		  {
		    int j = it_j + slip::n_6ce[n][1];
		    if(j >= 0)
		      {
			if(j < cols_)
			  {
			    neigh.push_back(it + slip::n_6ce[n]);
			  }
		      }
		  }
	      }
	  }
      }
  }
    

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    if(it.i()%2 == 0)
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    int i = it_i + slip::n_6co[n][0];
	    if(i >= 0)
	      {
		if(i < rows_)
		  {
		    int j = it_j + slip::n_6co[n][1];
		    if(j >= 0)
		      {
			if(j < cols_)
			  {
			    neigh.push_back(it[slip::n_6co[n]]);
			  }
		      }
		  }
	      }
	  }
      }
    else
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    int i = it_i + slip::n_6ce[n][0];
	    if(i >= 0)
	      {
		if(i < rows_)
		  {
		    int j = it_j + slip::n_6ce[n][1];
		    if(j >= 0)
		      {
			if(j < cols_)
			  {
			    neigh.push_back(it[slip::n_6ce[n]]);
			  }
		      }
		  }
	      }
	  }
      }
  }
    

  const Container* cont_;
  int rows_;
  int cols_;
  int size_max_;
 };

struct PseudoHexagonal 
{
  
  PseudoHexagonal():size_max_(6)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    if(it.i() %2 == 0)
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    neigh[n] = it + slip::n_6co[n];
	  }
      }
    else
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    neigh[n] = it + slip::n_6ce[n];
	  }
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    if(it.i() %2 == 0)
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    neigh[n] = it[slip::n_6co[n]];
	  }
      }
    else
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    neigh[n] = it[slip::n_6ce[n]];
	  }
      }
  }


  int size_max_ ;
 };

template<typename Container>
struct SafePrevPseudoHexagonal
{
  SafePrevPseudoHexagonal(const Container& cont):
    cont_(&cont),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::prev_6co.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
     
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    if(it.i()%2 == 0)
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    int i = it_i + slip::prev_6co[n][0];
	    if(i >= 0)
	      {
		if(i < rows_)
		  {
		    int j = it_j + slip::prev_6co[n][1];
		    if(j >= 0)
		      {
			if(j < cols_)
			  {
			    neigh.push_back(it + slip::prev_6co[n]);
			  }
		      }
		  }
	      }
	  }
      }
    else
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    int i = it_i + slip::prev_6ce[n][0];
	    if(i >= 0)
	      {
		if(i < rows_)
		  {
		    int j = it_j + slip::prev_6ce[n][1];
		    if(j >= 0)
		      {
			if(j < cols_)
			  {
			    neigh.push_back(it + slip::prev_6ce[n]);
			  }
		      }
		  }
	      }
	  }
      }
  }
  
 template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
     
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    if(it.i()%2 == 0)
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    int i = it_i + slip::prev_6co[n][0];
	    if(i >= 0)
	      {
		if(i < rows_)
		  {
		    int j = it_j + slip::prev_6co[n][1];
		    if(j >= 0)
		      {
			if(j < cols_)
			  {
			    neigh.push_back(it[slip::prev_6co[n]]);
			  }
		      }
		  }
	      }
	  }
      }
    else
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    int i = it_i + slip::prev_6ce[n][0];
	    if(i >= 0)
	      {
		if(i < rows_)
		  {
		    int j = it_j + slip::prev_6ce[n][1];
		    if(j >= 0)
		      {
			if(j < cols_)
			  {
			    neigh.push_back(it[slip::prev_6ce[n]]);
			  }
		      }
		  }
	      }
	  }
      }
  }
      

  const Container* cont_;
  int rows_;
  int cols_;
  int size_max_;
 };

template<typename Container>
struct SafeNextPseudoHexagonal
{
  SafeNextPseudoHexagonal(const Container& cont):
    cont_(&cont),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::next_6co.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    if(it.i()%2 == 0)
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    int i = it_i + slip::next_6co[n][0];
	    if(i >= 0)
	      {
		if(i < rows_)
		  {
		    int j = it_j + slip::next_6co[n][1];
		    if(j >= 0)
		      {
			if(j < cols_)
			  {
			    neigh.push_back(it + slip::next_6co[n]);
			  }
		      }
		  }
	      }
	  }
      }
    else
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    int i = it_i + slip::next_6ce[n][0];
	    if(i >= 0)
	      {
		if(i < rows_)
		  {
		    int j = it_j + slip::next_6ce[n][1];
		    if(j >= 0)
		      {
			if(j < cols_)
			  {
			    neigh.push_back(it + slip::next_6ce[n]);
			  }
		      }
		  }
	      }
	  }
      }
  }
    

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    if(it.i()%2 == 0)
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    int i = it_i + slip::next_6co[n][0];
	    if(i >= 0)
	      {
		if(i < rows_)
		  {
		    int j = it_j + slip::next_6co[n][1];
		    if(j >= 0)
		      {
			if(j < cols_)
			  {
			    neigh.push_back(it[slip::next_6co[n]]);
			  }
		      }
		  }
	      }
	  }
      }
    else
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    int i = it_i + slip::next_6ce[n][0];
	    if(i >= 0)
	      {
		if(i < rows_)
		  {
		    int j = it_j + slip::next_6ce[n][1];
		    if(j >= 0)
		      {
			if(j < cols_)
			  {
			    neigh.push_back(it[slip::next_6ce[n]]);
			  }
		      }
		  }
	      }
	  }
      }
  }

  const Container* cont_;
  int rows_;
  int cols_;
  int size_max_;
 };



struct PrevPseudoHexagonal
{
  
  PrevPseudoHexagonal():size_max_(3)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    if(it.i() %2 == 0)
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    neigh[n] = it + slip::prev_6co[n];
	  }
      }
    else
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    neigh[n] = it + slip::prev_6ce[n];
	  }
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    if(it.i() %2 == 0)
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    neigh[n] = it[slip::prev_6co[n]];
	  }
      }
    else
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    neigh[n] = it[slip::prev_6ce[n]];
	  }
      }
  }

  int size_max_ ;
 };

struct NextPseudoHexagonal 
{
  
  NextPseudoHexagonal():size_max_(3)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    if(it.i() %2 == 0)
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    neigh[n] = it + slip::next_6co[n];
	  }
      }
    else
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    neigh[n] = it + slip::next_6ce[n];
	  }
      }
  }

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    if(it.i() %2 == 0)
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    neigh[n] = it[slip::next_6co[n]];
	  }
      }
    else
      {
	for(int n = 0; n < size_max_; ++n)
	  {
	    neigh[n] = it[slip::next_6ce[n]];
	  }
      }
  }

  int size_max_ ;
 };
/*@} End 2D Neighborhood */

/*!
 ** @defgroup nbors3d 3d neighborhood 
 ** \ingroup nbors
 ** @brief Group of the 3d neighborhood, successors and predecessors
 *  @{
 ** 
 */
template<typename Container>
struct SafeN6C
{
  SafeN6C(const Container& cont):
    cont_(&cont),
    slices_(static_cast<int>(cont.slices())),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::n_6c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::n_6c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	      {
		int i = it_i + slip::n_6c[n][1];
		if(i >= 0)
		  {
		    if(i < rows_)
		      {
			 int j = it_j + slip::n_6c[n][2];
			 if(j>=0)
			   {
			      if(j<cols_)
				{
				   neigh.push_back(it+slip::n_6c[n]);
				}
			   }
		      }
		  }
	      }
	  }
      }
  }


   template<typename _II>
  void operator() (_II it, 
	    std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::n_6c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	      {
		int i = it_i + slip::n_6c[n][1];
		if(i >= 0)
		  {
		    if(i < rows_)
		      {
			 int j = it_j + slip::n_6c[n][2];
			 if(j>=0)
			   {
			      if(j<cols_)
				{
				   neigh.push_back(it[slip::n_6c[n]]);
				}
			   }
		      }
		  }
	      }
	  }
      }
  }

  const Container* cont_;
  int slices_;
  int rows_;
  int cols_;
  int size_max_;
 };

struct N6C 
{
  
  N6C():size_max_(6)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it + slip::n_6c[n];
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it[slip::n_6c[n]];
      }
  }

  int size_max_ ;
 };


template<typename Container>
struct SafePrev6C
{
  SafePrev6C(const Container& cont):
    cont_(&cont),
    slices_(static_cast<int>(cont.slices())),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::prev_6c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    
      for(int n = 0; n < size_max_; ++n)
        {
	  int k = it_k + slip::prev_6c[n][0];
	  if(k >= 0)
	    {
	      if(k < slices_)
	        {
		  int i = it_i + slip::prev_6c[n][1];
  		  if(i >= 0)
		    {
		      if(i < rows_)
		        {
			   int j = it_j + slip::prev_6c[n][2];
			   if(j>=0)
			     {
			        if(j<cols_)
				  {
				     neigh.push_back(it+slip::prev_6c[n]);
				  }
			     }
		        }
		    }
	        }
	    }
        }
  }

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    
      for(int n = 0; n < size_max_; ++n)
        {
	  int k = it_k + slip::prev_6c[n][0];
	  if(k >= 0)
	    {
	      if(k < slices_)
	        {
		  int i = it_i + slip::prev_6c[n][1];
  		  if(i >= 0)
		    {
		      if(i < rows_)
		        {
			   int j = it_j + slip::prev_6c[n][2];
			   if(j>=0)
			     {
			        if(j<cols_)
				  {
				     neigh.push_back(it[slip::prev_6c[n]]);
				  }
			     }
		        }
		    }
	        }
	    }
        }
  }

  const Container* cont_;
  int slices_;
  int rows_;
  int cols_;
  int size_max_;
 };


struct Prev6C
{
  
  Prev6C():size_max_(3)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
   
    for(int n = 0; n < size_max_; ++n)
      {
	neigh[n] = it + slip::prev_6c[n];
      }
  }

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
   
    for(int n = 0; n < size_max_; ++n)
      {
	neigh[n] = it[slip::prev_6c[n]];
      }
  }

  int size_max_ ;
 };

template<typename Container>
struct SafeNext6C
{
  SafeNext6C(const Container& cont):
    cont_(&cont),
    slices_(static_cast<int>(cont.slices())),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::next_6c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::next_6c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	        {
		  int i = it_i + slip::next_6c[n][1];
  		  if(i >= 0)
		    {
		      if(i < rows_)
		        {
			   int j = it_j + slip::next_6c[n][2];
			   if(j>=0)
			     {
			        if(j<cols_)
				  {
				     neigh.push_back(it+slip::next_6c[n]);
				  }
			     }
		        }
		    }
	        }
	    }
        }
    
  }

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::next_6c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	        {
		  int i = it_i + slip::next_6c[n][1];
  		  if(i >= 0)
		    {
		      if(i < rows_)
		        {
			   int j = it_j + slip::next_6c[n][2];
			   if(j>=0)
			     {
			        if(j<cols_)
				  {
				     neigh.push_back(it[slip::next_6c[n]]);
				  }
			     }
		        }
		    }
	        }
	    }
        }
    
  }

  const Container* cont_;
  int slices_;
  int rows_;
  int cols_;
  int size_max_;
 };

struct Next6C
{
  
  Next6C():size_max_(3)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    
    for(int n = 0; n < size_max_; ++n)
      {
	neigh[n] = it + slip::next_6c[n];
      }
  }

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    
    for(int n = 0; n < size_max_; ++n)
      {
	neigh[n] = it[slip::next_6c[n]];
      }
  }

  int size_max_ ;
 };


template<typename Container>
struct SafeN26C
{
  SafeN26C(const Container& cont):
    cont_(&cont),
    slices_(static_cast<int>(cont.slices())),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::n_26c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::n_26c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	      {
		int i = it_i + slip::n_26c[n][1];
		if(i >= 0)
		  {
		    if(i < rows_)
		      {
			 int j = it_j + slip::n_26c[n][2];
			 if(j>=0)
			   {
			      if(j<cols_)
				{
				   neigh.push_back(it+slip::n_26c[n]);
				}
			   }
		      }
		  }
	      }
	  }
      }
  }

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::n_26c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	      {
		int i = it_i + slip::n_26c[n][1];
		if(i >= 0)
		  {
		    if(i < rows_)
		      {
			 int j = it_j + slip::n_26c[n][2];
			 if(j>=0)
			   {
			      if(j<cols_)
				{
				   neigh.push_back(it[slip::n_26c[n]]);
				}
			   }
		      }
		  }
	      }
	  }
      }
  }

  const Container* cont_;
  int slices_;
  int rows_;
  int cols_;
  int size_max_;
 };

struct N26C
{
  
  N26C():size_max_(26)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
     neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it + slip::n_26c[n];
      }
  }

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
     neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it[slip::n_26c[n]];
      }
  }

  int size_max_ ;
 };



template<typename Container>
struct SafePrev26C
{
  SafePrev26C(const Container& cont):
    cont_(&cont),
    slices_(static_cast<int>(cont.slices())),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::prev_26c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
   
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::prev_26c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	      {
		   int i = it_i + slip::prev_26c[n][1];
		   if(i >= 0)
		     {
		       if(i < rows_)
		         {
			    int j = it_j + slip::prev_26c[n][2];
			    if(j>=0)
			      {
			         if(j<cols_)
				   {
				      neigh.push_back(it+slip::prev_26c[n]);
				   }
			      }
		         }
		     }
	         }
	     }
         }
  }

   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
   
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::prev_26c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	      {
		   int i = it_i + slip::prev_26c[n][1];
		   if(i >= 0)
		     {
		       if(i < rows_)
		         {
			    int j = it_j + slip::prev_26c[n][2];
			    if(j>=0)
			      {
			         if(j<cols_)
				   {
				      neigh.push_back(it[slip::prev_26c[n]]);
				   }
			      }
		         }
		     }
	         }
	     }
         }
  }

  const Container* cont_;
  int slices_;
  int rows_;
  int cols_;
  int size_max_;
 };


struct Prev26C
{
  
  Prev26C():size_max_(13)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
   
    for(int n = 0; n < size_max_; ++n)
      {
	neigh[n] = it + slip::prev_26c[n];
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
   
    for(int n = 0; n < size_max_; ++n)
      {
	neigh[n] = it[slip::prev_26c[n]];
      }
  }

  int size_max_ ;
 };


template<typename Container>
struct SafeNext26C
{
  SafeNext26C(const Container& cont):
    cont_(&cont),
    slices_(static_cast<int>(cont.slices())),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::next_26c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
     
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
   
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::next_26c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	      {
		int i = it_i + slip::next_26c[n][1];
		if(i >= 0)
		  {
		    if(i < rows_)
		      {
			int j = it_j + slip::next_26c[n][2];
			if(j>=0)
			  {
			    if(j<cols_)
			      {
				neigh.push_back(it+slip::next_26c[n]);
			      }
			  }
		      }
		  }
	      }
	  }
      }
     
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
     
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
   
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::next_26c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	      {
		int i = it_i + slip::next_26c[n][1];
		if(i >= 0)
		  {
		    if(i < rows_)
		      {
			int j = it_j + slip::next_26c[n][2];
			if(j>=0)
			  {
			    if(j<cols_)
			      {
				neigh.push_back(it[slip::next_26c[n]]);
			      }
			  }
		      }
		  }
	      }
	  }
      }
     
  }

  const Container* cont_;
  int slices_;
  int rows_;
  int cols_;
  int size_max_;
 };


struct Next26C
{
  
  Next26C():size_max_(13)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
   
    for(int n = 0; n < size_max_; ++n)
      {
	neigh[n] = it + slip::next_26c[n];
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
   
    for(int n = 0; n < size_max_; ++n)
      {
	neigh[n] = it[slip::next_26c[n]];
      }
  }

  int size_max_ ;
 };


template<typename Container>
struct SafeN18C
{
  SafeN18C(const Container& cont):
    cont_(&cont),
    slices_(static_cast<int>(cont.slices())),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::n_18c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
     neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::n_18c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	      {
		int i = it_i + slip::n_18c[n][1];
		if(i >= 0)
		  {
		    if(i < rows_)
		      {
			 int j = it_j + slip::n_18c[n][2];
			 if(j>=0)
			   {
			      if(j<cols_)
				{
				   neigh.push_back(it+slip::n_18c[n]);
				}
			   }
		      }
		  }
	      }
	  }
      }
  }


  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
     neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::n_18c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	      {
		int i = it_i + slip::n_18c[n][1];
		if(i >= 0)
		  {
		    if(i < rows_)
		      {
			 int j = it_j + slip::n_18c[n][2];
			 if(j>=0)
			   {
			      if(j<cols_)
				{
				   neigh.push_back(it[slip::n_18c[n]]);
				}
			   }
		      }
		  }
	      }
	  }
      }
  }

  const Container* cont_;
  int slices_;
  int rows_;
  int cols_;
  int size_max_;
 };

struct N18C
{
  
  N18C():size_max_(18)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it + slip::n_18c[n];
      }
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
    for(int n = 0; n < size_max_; ++n)
      {
    	neigh[n] = it[slip::n_18c[n]];
      }
  }

  int size_max_ ;
 };

template<typename Container>
struct SafePrev18C
{
  SafePrev18C(const Container& cont):
    cont_(&cont),
    slices_(static_cast<int>(cont.slices())),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::prev_18c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
     
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::prev_18c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	      {
		int i = it_i + slip::prev_18c[n][1];
		if(i >= 0)
		  {
		    if(i < rows_)
		      {
			int j = it_j + slip::prev_18c[n][2];
			if(j>=0)
			  {
			    if(j<cols_)
			      {
				neigh.push_back(it+slip::prev_18c[n]);
			      }
			  }
		      }
		  }
	      }
	  }
      }
   
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
     
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    
    for(int n = 0; n < size_max_; ++n)
      {
	int k = it_k + slip::prev_18c[n][0];
	if(k >= 0)
	  {
	    if(k < slices_)
	      {
		int i = it_i + slip::prev_18c[n][1];
		if(i >= 0)
		  {
		    if(i < rows_)
		      {
			int j = it_j + slip::prev_18c[n][2];
			if(j>=0)
			  {
			    if(j<cols_)
			      {
				neigh.push_back(it[slip::prev_18c[n]]);
			      }
			  }
		      }
		  }
	      }
	  }
      }
   
  }

  const Container* cont_;
  int slices_;
  int rows_;
  int cols_;
  int size_max_;
 };

struct Prev18C
{
  
  Prev18C():size_max_(9)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
     neigh.resize(size_max_);
    
     for(int n = 0; n < size_max_; ++n)
       {
	 neigh[n] = it + slip::prev_18c[n];
       }
    
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
     neigh.resize(size_max_);
    
     for(int n = 0; n < size_max_; ++n)
       {
	 neigh[n] = it[slip::prev_18c[n]];
       }
    
  }

  int size_max_ ;
 };



template<typename Container>
struct SafeNext18C
{
  SafeNext18C(const Container& cont):
    cont_(&cont),
    slices_(static_cast<int>(cont.slices())),
    rows_(static_cast<int>(cont.rows())),
    cols_(static_cast<int>(cont.cols())),
    size_max_( slip::next_18c.size())
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    
       for(int n = 0; n < size_max_; ++n)
         {
	   int k = it_k + slip::next_18c[n][0];
	   if(k >= 0)
	     {
	       if(k < slices_)
	         {
		   int i = it_i + slip::next_18c[n][1];
		   if(i >= 0)
		     {
		       if(i < rows_)
		         {
			    int j = it_j + slip::next_18c[n][2];
			    if(j>=0)
			      {
			         if(j<cols_)
				   {
				      neigh.push_back(it+slip::next_18c[n]);
				   }
			      }
		         }
		     }
	         }
	     }
         }
  }


   template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    
    neigh.resize(0);
    int it_i = it.i();
    int it_j = it.j();
    int it_k = it.k();
    
       for(int n = 0; n < size_max_; ++n)
         {
	   int k = it_k + slip::next_18c[n][0];
	   if(k >= 0)
	     {
	       if(k < slices_)
	         {
		   int i = it_i + slip::next_18c[n][1];
		   if(i >= 0)
		     {
		       if(i < rows_)
		         {
			    int j = it_j + slip::next_18c[n][2];
			    if(j>=0)
			      {
			         if(j<cols_)
				   {
				      neigh.push_back(it[slip::next_18c[n]]);
				   }
			      }
		         }
		     }
	         }
	     }
         }
  }

  const Container* cont_;
  int slices_;
  int rows_;
  int cols_;
  int size_max_;
 };


struct Next18C
{
  
  Next18C():size_max_(9)
  {}

  template<typename _II>
  void operator() (_II it, 
		   std::vector<_II>& neigh) const
  {
    neigh.resize(size_max_);
   
    for(int n = 0; n < size_max_; ++n)
      {
	neigh[n] = it + slip::next_18c[n];
      }
    
  }

  template<typename _II>
  void operator() (_II it, 
		   std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
  {
    neigh.resize(size_max_);
   
    for(int n = 0; n < size_max_; ++n)
      {
	neigh[n] = it[slip::next_18c[n]];
      }
    
  }

  int size_max_ ;
 };
/*@} End 3D Neighborhood */

///////////////////////////////////////////////////////////////


/*!
 ** @defgroup nbors4d 4D Neighborhood
 ** \ingroup nbors Fluex
 ** \version Fluex 1.0
 ** \date 2013/06/07
 ** \since 1.4.0
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief 4D neighborhood. Those neighborhoods are 4-connexity extensions of classical slip neighborhood in 4D.
 ** \image html neighbors4d_8c.jpg "4D 4-connexity Neighborhood"
 ** \image latex neighbors4d_8c.eps "4D 4-connexity Neighborhood" width=10cm
 */
 /*@{*/

	/*!
	 ** \version Fluex 1.0
	 ** \date 2013/06/07
	 ** \since 1.4.0
	 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
	 ** \brief safe version of N4D8C neighborhood (only existing neighbors are returned)
	 */
	 template<typename Container>
 struct SafeN4D8C
 {
	 SafeN4D8C(const Container& cont):
		 cont_(&cont),
		 slabs_(static_cast<int>(cont.slabs())),
		 slices_(static_cast<int>(cont.slices())),
		 rows_(static_cast<int>(cont.rows())),
		 cols_(static_cast<int>(cont.cols())),
		 size_max_( slip::n_4d_8c.size())
	 {}

	 template<typename _II>
	 void operator() (_II it,
			 std::vector<_II>& neigh) const
	 {
		 neigh.resize(0);
		 int it_i = it.i();
		 int it_j = it.j();
		 int it_k = it.k();
		 int it_t = it.t();
		 for(int n = 0; n < size_max_; ++n)
		 {
			 int t = it_t + slip::n_4d_8c[n][0];
			 if(t >= 0)
			 {
				 if(t < slabs_)
				 {
					 int k = it_k + slip::n_4d_8c[n][1];
					 if(k >= 0)
					 {
						 if(k < slices_)
						 {
							 int i = it_i + slip::n_4d_8c[n][2];
							 if(i >= 0)
							 {
								 if(i < rows_)
								 {
									 int j = it_j + slip::n_4d_8c[n][3];
									 if(j>=0)
									 {
										 if(j<cols_)
										 {
											 neigh.push_back(it+slip::n_4d_8c[n]);
										 }
									 }
								 }
							 }
						 }
					 }
				 }
			 }
		 }
	 }


	 template<typename _II>
	 void operator() (_II it,
			 std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
	 {
		 neigh.resize(0);
		 int it_i = it.i();
		 int it_j = it.j();
		 int it_k = it.k();
		 int it_t = it.t();
		 for(int n = 0; n < size_max_; ++n)
		 {
			 int t = it_t + slip::n_4d_8c[n][0];
			 if(t >= 0)
			 {
				 if(t < slabs_)
				 {
					 int k = it_k + slip::n_4d_8c[n][1];
					 if(k >= 0)
					 {
						 if(k < slices_)
						 {
							 int i = it_i + slip::n_4d_8c[n][2];
							 if(i >= 0)
							 {
								 if(i < rows_)
								 {
									 int j = it_j + slip::n_4d_8c[n][3];
									 if(j>=0)
									 {
										 if(j<cols_)
										 {
											 neigh.push_back(it[slip::n_4d_8c[n]]);
										 }
									 }
								 }
							 }
						 }
					 }
				 }
			 }
		 }
	 }

	 const Container* cont_;
	 int slabs_;
	 int slices_;
	 int rows_;
	 int cols_;
	 int size_max_;
 };

	 /*!
	  ** \version Fluex 1.0
	  ** \date 2013/06/07
	  ** \since 1.4.0
	  ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
	  ** \brief N4D8C neighborhood
	  */
	 struct N4D8C
	 {

		 N4D8C():size_max_(8)
		 {}

		 template<typename _II>
		 void operator() (_II it,
				 std::vector<_II>& neigh) const
		 {
			 neigh.resize(size_max_);
			 for(int n = 0; n < size_max_; ++n)
			 {
				 neigh[n] = it + slip::n_4d_8c[n];
			 }
		 }

		 template<typename _II>
		 void operator() (_II it,
				 std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
		 {
			 neigh.resize(size_max_);
			 for(int n = 0; n < size_max_; ++n)
			 {
				 neigh[n] = it[slip::n_4d_8c[n]];
			 }
		 }

		 int size_max_ ;
	 };

	 /*!
	  ** \version Fluex 1.0
	  ** \date 2013/06/07
	  ** \since 1.4.0
	  ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
	  ** \brief safe version of previous N4D8C neighborhood (only previous and existing neighbors are returned)
	  */
	 template<typename Container>
	 struct SafePrev4D8C
	 {
		 SafePrev4D8C(const Container& cont):
			 cont_(&cont),
			 slabs_(static_cast<int>(cont.slabs())),
			 slices_(static_cast<int>(cont.slices())),
			 rows_(static_cast<int>(cont.rows())),
			 cols_(static_cast<int>(cont.cols())),
			 size_max_( slip::prev_4d_8c.size())
		 {}

		 template<typename _II>
		 void operator() (_II it,
				 std::vector<_II>& neigh) const
		 {

			 neigh.resize(0);
			 int it_i = it.i();
			 int it_j = it.j();
			 int it_k = it.k();
			 int it_t = it.t();

			 for(int n = 0; n < size_max_; ++n)
			 {
				 int t = it_t + slip::n_4d_8c[n][0];
				 if(t >= 0)
				 {
					 if(t < slabs_)
					 {
						 int k = it_k + slip::n_4d_8c[n][1];
						 if(k >= 0)
						 {
							 if(k < slices_)
							 {
								 int i = it_i + slip::n_4d_8c[n][2];
								 if(i >= 0)
								 {
									 if(i < rows_)
									 {
										 int j = it_j + slip::n_4d_8c[n][3];
										 if(j>=0)
										 {
											 if(j<cols_)
											 {
												 neigh.push_back(it+slip::n_4d_8c[n]);
											 }
										 }
									 }
								 }
							 }
						 }
					 }
				 }
			 }
		 }

		 template<typename _II>
		 void operator() (_II it,
				 std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
		 {

			 neigh.resize(0);
			 int it_i = it.i();
			 int it_j = it.j();
			 int it_k = it.k();
			 int it_t = it.t();

			 for(int n = 0; n < size_max_; ++n)
			 {
				 int t = it_t + slip::n_4d_8c[n][0];
				 if(t >= 0)
				 {
					 if(t < slabs_)
					 {
						 int k = it_k + slip::n_4d_8c[n][1];
						 if(k >= 0)
						 {
							 if(k < slices_)
							 {
								 int i = it_i + slip::n_4d_8c[n][2];
								 if(i >= 0)
								 {
									 if(i < rows_)
									 {
										 int j = it_j + slip::n_4d_8c[n][3];
										 if(j>=0)
										 {
											 if(j<cols_)
											 {
												 neigh.push_back(it[slip::n_4d_8c[n]]);
											 }
										 }
									 }
								 }
							 }
						 }
					 }
				 }
			 }
		 }

		 const Container* cont_;
		 int slabs_;
		 int slices_;
		 int rows_;
		 int cols_;
		 int size_max_;
	 };

	 /*!
	  ** \version Fluex 1.0
	  ** \date 2013/06/07
	  ** \since 1.4.0
	  ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
	  ** \brief previous N4D8C neighborhood (only previous neighbors are returned)
	  */
	 struct Prev4D8C
	 {

		 Prev4D8C():size_max_(4)
		 {}

		 template<typename _II>
		 void operator() (_II it,
				 std::vector<_II>& neigh) const
		 {
			 neigh.resize(size_max_);

			 for(int n = 0; n < size_max_; ++n)
			 {
				 neigh[n] = it + slip::n_4d_8c[n];
			 }
		 }

		 template<typename _II>
		 void operator() (_II it,
				 std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
		 {
			 neigh.resize(size_max_);

			 for(int n = 0; n < size_max_; ++n)
			 {
				 neigh[n] = it[slip::n_4d_8c[n]];
			 }
		 }

		 int size_max_ ;
	 };

	 /*!
	  ** \version Fluex 1.0
	  ** \date 2013/06/07
	  ** \since 1.4.0
	  ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
	  ** \brief safe version of next N4D8C neighborhood (only next and existing neighbors are returned)
	  */
	 template<typename Container>
	 struct SafeNext4D8C
	 {
		 SafeNext4D8C(const Container& cont):
			 cont_(&cont),
			 slabs_(static_cast<int>(cont.slabs())),
			 slices_(static_cast<int>(cont.slices())),
			 rows_(static_cast<int>(cont.rows())),
			 cols_(static_cast<int>(cont.cols())),
			 size_max_( slip::next_4d_8c.size())
		 {}

		 template<typename _II>
		 void operator() (_II it,
				 std::vector<_II>& neigh) const
		 {
			 neigh.resize(0);
			 int it_i = it.i();
			 int it_j = it.j();
			 int it_k = it.k();
			 int it_t = it.t();
			 for(int n = 0; n < size_max_; ++n)
			 {
				 int t = it_t + slip::n_4d_8c[n][0];
				 if(t >= 0)
				 {
					 if(t < slabs_)
					 {
						 int k = it_k + slip::n_4d_8c[n][1];
						 if(k >= 0)
						 {
							 if(k < slices_)
							 {
								 int i = it_i + slip::n_4d_8c[n][2];
								 if(i >= 0)
								 {
									 if(i < rows_)
									 {
										 int j = it_j + slip::n_4d_8c[n][3];
										 if(j>=0)
										 {
											 if(j<cols_)
											 {
												 neigh.push_back(it+slip::n_4d_8c[n]);
											 }
										 }
									 }
								 }
							 }
						 }
					 }
				 }
			 }

		 }

		 template<typename _II>
		 void operator() (_II it,
				 std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
		 {
			 neigh.resize(0);
			 int it_i = it.i();
			 int it_j = it.j();
			 int it_k = it.k();
			 int it_t = it.t();
			 for(int n = 0; n < size_max_; ++n)
			 {
				 int t = it_t + slip::n_4d_8c[n][0];
				 if(t >= 0)
				 {
					 if(t < slabs_)
					 {
						 int k = it_k + slip::n_4d_8c[n][1];
						 if(k >= 0)
						 {
							 if(k < slices_)
							 {
								 int i = it_i + slip::n_4d_8c[n][2];
								 if(i >= 0)
								 {
									 if(i < rows_)
									 {
										 int j = it_j + slip::n_4d_8c[n][3];
										 if(j>=0)
										 {
											 if(j<cols_)
											 {
												 neigh.push_back(it[slip::n_4d_8c[n]]);
											 }
										 }
									 }
								 }
							 }
						 }
					 }
				 }
			 }

		 }

		 const Container* cont_;
		 int slabs_;
		 int slices_;
		 int rows_;
		 int cols_;
		 int size_max_;
	 };

	 /*!
	  ** \version Fluex 1.0
	  ** \date 2013/06/07
	  ** \since 1.4.0
	  ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
	  ** \brief next N4D8C neighborhood (only next neighbors are returned)
	  */
	 struct Next4D8C
	 {

		 Next4D8C():size_max_(4)
		 {}

		 template<typename _II>
		 void operator() (_II it,
				 std::vector<_II>& neigh) const
		 {
			 neigh.resize(size_max_);

			 for(int n = 0; n < size_max_; ++n)
			 {
				 neigh[n] = it + slip::n_4d_8c[n];
			 }
		 }

		 template<typename _II>
		 void operator() (_II it,
				 std::vector<typename std::iterator_traits<_II>::value_type>& neigh) const
		 {
			 neigh.resize(size_max_);

			 for(int n = 0; n < size_max_; ++n)
			 {
				 neigh[n] = it[slip::n_4d_8c[n]];
			 }
		 }

		 int size_max_ ;
	 };



	 /*@} End 4D Neighborhood */


}//::slip

#endif //SLIP_NEIGHBORHOOD_HPP
