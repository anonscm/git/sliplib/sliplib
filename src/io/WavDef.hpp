/*!
 ** \file WavDef.hpp
 ** \brief Some definitions specific to wav files manipulations
 ** \version Fluex 1.0
 ** \date 2013/04/16
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef WAVDEF_HPP_
#define WAVDEF_HPP_

/*!
 * \cond Do not put the following into Doxygen documentation
 */

#include <istream>
#include <ostream>
#include <limits>
#include <algorithm>
/*
 * \todo find a portable solution for int32 and int16 definitions
 */
extern "C"{
#include <stdint.h>
}

#include "error.hpp"

typedef char char8;
typedef int32_t int32;
typedef int16_t int16;

// REVERSE_ENDIANISM must be defined if the endianism of the host platform is not Intel
// (Intel is little-endian)
#define SWAP_32(in) (  \
		(((int32) in) << 24) |  \
		((((int32) in) & 0x0000FF00L) << 8) |  \
		((((int32) in) & 0x00FF0000L) >> 8) |  \
		(((int32) in) >> 24))

#define SWAP_16(in) (  \
		(((int16) in) << 8) |  \
		(((int16) in) >> 8))




/*!
 ** \version Fluex 1.0
 ** \date 2013/04/16
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief Riff chunk header structure of wave files.
 */
struct RIFF_CHUNK_HEADER
{
	/*! Constant should be "RIFF" */
	char8 chunk_id[5];
	/*! file size minus 8 bits */
	int32 chunk_size;
	/*! Constant should be "WAVE" */
	char8 format[5];

	/*!\brief operator << */
	friend std::ostream & operator<<(std::ostream & os, const RIFF_CHUNK_HEADER & s);
	/*!\brief operator >> */
	friend std::istream & operator>>(std::istream & is, RIFF_CHUNK_HEADER & s);
};

std::ostream & operator<<(std::ostream & os, const RIFF_CHUNK_HEADER & s){
	os << "RiffChunkId= " << s.chunk_id << " | ";
	os << "RiffChunkSize= " << s.chunk_size << " | ";
	os << "RiffFormat= " << s.format << '\n';
	return os;
}

std::istream & operator>>(std::istream & is, RIFF_CHUNK_HEADER & s){
	std::string buf;
	is >> buf >> s.chunk_id >> buf;
	is >> buf >> s.chunk_size >> buf;
	is >> buf >> s.format;
	return is;
}

/*!
 ** \version Fluex 1.0
 ** \date 2013/04/16
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief Format chunk header structure of wave files.
 */
struct FORMAT_CHUNK_HEADER
{
	/*! Constant should be "fmt " */
	char8 chunk_id[5];
	/*! Number of bytes of this chunk block */
	int32 chunk_size;
	/*! in file registered format (1:PCM,...) */
	int16 format;
	/*! Number of channels */
	int16 num_channels;
	/*! sample frequency */
	int32 sample_rate;
	/*! Number of bytes to read per second */
	int32 byte_rate;
	/*! Number of bytes per sampling block (num_channels * bits_per_sample/8) */
	int16 align;
	/*! Number of bytes used for a sample coding (8,16,32) */
	int16 bits_per_sample;
	/*!\brief operator << */
	friend std::ostream & operator<<(std::ostream & os, const FORMAT_CHUNK_HEADER & s);
	/*!\brief operator >> */
	friend std::istream & operator>>(std::istream & is, FORMAT_CHUNK_HEADER & s);
};

std::ostream & operator<<(std::ostream & os, const FORMAT_CHUNK_HEADER & s){
	os << "FmtChunkId= " << s.chunk_id << " | ";
	os << "FmtChunkSize= " << s.chunk_size << " | ";
	os << "FmtFormat= " << s.format << " | ";
	os << "FmtNumChannels= " << s.num_channels << " | ";
	os << "FmtSampleRate= " << s.sample_rate << " | ";
	os << "FmtByteRate= " << s.byte_rate << " | ";
	os << "FmtAlign= " << s.align << " | ";
	os << "FmtBitsPerSample= " << s.bits_per_sample << '\n';
	return os;
}

std::istream & operator>>(std::istream & is, FORMAT_CHUNK_HEADER & s){
	std::string buf;
	is >> buf >> s.chunk_id >> buf;
	is >> buf >> s.chunk_size >> buf;
	is >> buf >> s.format >> buf;
	is >> buf >> s.num_channels >> buf;
	is >> buf >> s.sample_rate >> buf;
	is >> buf >> s.byte_rate >> buf;
	is >> buf >> s.align >> buf;
	is >> buf >> s.bits_per_sample;
	return is;
}

/*!
 ** \version Fluex 1.0
 ** \date 2013/04/16
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief Data chunk header structure of wave files.
 */
struct DATA_CHUNK_HEADER
{
	/*! Constant should be "data" */
	char8 chunk_id[5];
	/*! length of data */
	int32 chunk_size;
	/*!\brief operator << */
	friend std::ostream & operator<<(std::ostream & os, const DATA_CHUNK_HEADER & s);
	/*!\brief operator >> */
	friend std::istream & operator>>(std::istream & is, DATA_CHUNK_HEADER & s);
};

std::ostream & operator<<(std::ostream & os, const DATA_CHUNK_HEADER & s){
	os << "DataChunkId= " << s.chunk_id << " | ";
	os << "DataChunkSize= " << s.chunk_size << '\n';
	return os;
}

std::istream & operator>>(std::istream & is, DATA_CHUNK_HEADER & s){
	std::string buf;
	is >> buf >> s.chunk_id >> buf;
	is >> buf >> s.chunk_size;
	return is;
}

/*!
 ** \version Fluex 1.0
 ** \date 2013/04/16
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief Overall structure of a PCM WAVE file header.
 ** <a href="https://ccrma.stanford.edu/courses/422/projects/WaveFormat/">Image extract from here :</a>
 ** \image html wav-sound-format.jpg "Wave format."
 ** \image latex wav-sound-format.eps "Wave format." width=5cm
 */
struct WAVE_HEADER
{
	RIFF_CHUNK_HEADER riff_header;
	FORMAT_CHUNK_HEADER format_header;
	DATA_CHUNK_HEADER data_header;
	//!\brief : operator <<
	friend std::ostream & operator<<(std::ostream & os, const WAVE_HEADER & s);
	//!\brief : operator >>
	friend std::istream & operator>>(std::istream & is, WAVE_HEADER & s);
};

std::ostream & operator<<(std::ostream & os, const WAVE_HEADER & s){
	os << s.riff_header << s.format_header << s.data_header;
	return os;
}

std::istream & operator>>(std::istream & is, WAVE_HEADER & s){
	is >> s.riff_header;
	is >> s.format_header;
	is >> s.data_header;
	return is;
}

/*!
 * \endcond
 */

#endif /* WAVDEF_HPP_ */

