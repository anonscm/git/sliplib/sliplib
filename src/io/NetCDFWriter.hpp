/*!
 ** \file NetCDFWriter.hpp
 ** \brief containers writer into netcdf files
 ** \version Fluex 1.0
 ** \date 2013/04/29
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \todo Adding 4D containers
 ** \todo Adding writing of multicomponents variables (this version can only write one component per variable)
 */

#ifndef NETCDFWRITER_HPP_
#define NETCDFWRITER_HPP_

#include "NetCDFDef.hpp"

#include "ContainerWriter.hpp"

namespace slip {
/*!
 * \cond Do not include the following into documentation
 */
/*!
 * \version Fluex 1.0
 * \date 2013/04/29
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_write structure used by NetCDFReader for writing a container into a NcVar.
 * \param Nb_components : the number of components or netcdf variables
 * \param Dim : container dimension
 * \return the first dimension size that has been filled
 */
template <std::size_t Nb_components, std::size_t Dim>
struct __nc_write_data{
	template <typename MonoContainer, typename T>
	static std::size_t apply(const MonoContainer& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, const std::vector<NcDim*> & nc_dim){
		std::ostringstream err;
		err << "Nb Components or dimension is not valid.";
		slip::slip_exception exc(std::string("slip"), std::string("nc_write_data"), err.str());
		throw (exc);
		return 0;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/04/29
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_write structure used by NetCDFReader for writing a container into a NcVar.
 * \param Nb_components : the number of components or netcdf variables = 1
 * \param Dim : container dimension = 1
 * \return the first dimension size that has been filled
 */
template <>
struct __nc_write_data<1,1>{
	template <typename MonoContainer1D, typename T>
	static std::size_t apply(const MonoContainer1D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, const std::vector<NcDim*> & nc_dim)
	{
		std::size_t size = static_cast<std::size_t>(std::distance(container.begin(),container.end()));
		if (!nc_var[0]->set_cur(first_dim_pos)){
			std::ostringstream err;
			err << "Impossible to set the new corner.";
			slip::slip_exception exc(std::string("slip"), std::string("__nc_write_data_1_1"), err.str());
			throw (exc);
		}
		if (!nc_var[0]->put(container.begin(),static_cast<long>(size)))
		{
			std::ostringstream err;
			err << "Cannot write 1D data.";
			slip::slip_exception exc(std::string("slip"), std::string("__nc_write_data_1_1"), err.str());
			throw (exc);
		}
		return size;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/04/29
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_write structure used by NetCDFReader for writing a container into a NcVar.
 * \param Nb_components : the number of components or netcdf variables = N
 * \param Dim : container dimension = 1
 * \return the first dimension size that has been filled
 */
template <std::size_t N>
struct __nc_write_data<N,1>{
	template <typename MultiContainer1D, typename T>
	static std::size_t apply(const MultiContainer1D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, const std::vector<NcDim*> & nc_dim)
	{
		std::size_t size = static_cast<std::size_t>(std::distance(container.begin(),container.end()));
		slip::Vector<T> tmp(size,T());
		for(std::size_t i = 0; i < N; ++i)
		{
			if (!nc_var[i]->set_cur(first_dim_pos)){
				std::ostringstream err;
				err << "Impossible to set the new corner of component " << i << ".";
				slip::slip_exception exc(std::string("slip"), std::string("__nc_write_data_n_1"), err.str());
				throw (exc);
			}
			for(std::size_t j=0; j<size; j++){
				tmp[j] = container[j][i];
			}
			if (!(nc_var[i]->put(tmp.begin(),static_cast<long>(size))))
			{
				std::ostringstream err;
				err << "Cannot write 1D data.";
				slip::slip_exception exc(std::string("slip"), std::string("__nc_write_data_n_1"), err.str());
				throw (exc);
			}
		}
		return size;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/04/29
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_write structure used by NetCDFReader for writing a container into a NcVar.
 * \param Nb_components : the number of components or netcdf variables = 1
 * \param Dim : container dimension = 2
 * \return the first dimension size that has been filled
 */
template <>
struct __nc_write_data<1,2>{
	template <typename MonoContainer2D, typename T>
	static std::size_t apply(const MonoContainer2D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, const std::vector<NcDim*> & nc_dim)
	{
		std::size_t dim1 = static_cast<std::size_t>((container.bottom_right() - container.upper_left()).dx1());
		if (!nc_var[0]->set_cur(first_dim_pos,0)){
			std::ostringstream err;
			err << "Impossible to set the new corner.";
			slip::slip_exception exc(std::string("slip"), std::string("__nc_write_data_1_2"), err.str());
			throw (exc);
		}
		if (!nc_var[0]->put(container.begin(),static_cast<long>(dim1),nc_dim[1]->size()))
		{
			std::ostringstream err;
			err <<  "Cannot write 2D data.";
			slip::slip_exception exc(std::string("slip"), std::string("__nc_write_data_1_2"), err.str());
			throw (exc);
		}
		return dim1;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/04/29
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_write structure used by NetCDFReader for writing a container into a NcVar.
 * \param Nb_components : the number of components or netcdf variables = N
 * \param Dim : container dimension = 2
 * \return the first dimension size that has been filled
 */
template <std::size_t N>
struct __nc_write_data<N,2>{
	template <typename MultiContainer2D, typename T>
	static std::size_t apply(const MultiContainer2D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, const std::vector<NcDim*> & nc_dim)
	{
		std::size_t dim1 = static_cast<std::size_t>((container.bottom_right() - container.upper_left()).dx1());
		std::size_t dim2 = static_cast<std::size_t>(nc_dim[1]->size());

		for(std::size_t i = 0; i < N; ++i)
		{
			if (!nc_var[i]->set_cur(first_dim_pos,0)){
				std::ostringstream err;
				err << "Impossible to set the new corner of component " << i << ".";
				slip::slip_exception exc(std::string("slip"), std::string("_nc_write_data_n_2"), err.str());
				throw (exc);
			}
			slip::Array2d<T> tmp(dim1,dim2,container.begin(i),container.end(i));
			if (!(nc_var[i]->put(tmp.begin(),static_cast<long>(dim1),static_cast<long>(dim2))))
			{
				std::ostringstream err;
				err << "Cannot write 2D data.";
				slip::slip_exception exc(std::string("slip"), std::string("_nc_write_data_n_2"), err.str());
				throw (exc);
			}

		}
		return dim1;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/04/29
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_write structure used by NetCDFReader for writing a container into a NcVar.
 * \param Nb_components : the number of components or netcdf variables = 1
 * \param Dim : container dimension = 3
 * \return the first dimension size that has been filled
 */
template <>
struct __nc_write_data<1,3>{
	template <typename MonoContainer3D, typename T>
	static std::size_t apply(const MonoContainer3D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, const std::vector<NcDim*> & nc_dim)
	{
		std::size_t dim1 = static_cast<std::size_t>((container.back_bottom_right() - container.front_upper_left()).dx1());
		if (!nc_var[0]->set_cur(first_dim_pos,0,0)){
			std::ostringstream err;
			err << "Impossible to set the new corner.";
			slip::slip_exception exc(std::string("slip"), std::string("__nc_write_data_1_3"), err.str());
			throw (exc);
		}
		if (!nc_var[0]->put(container.begin(),static_cast<long>(dim1),nc_dim[1]->size(),nc_dim[2]->size()))
		{
			std::ostringstream err;
			err << "Cannot write 3D data.";
			slip::slip_exception exc(std::string("slip"), std::string("__nc_write_data_1_3"), err.str());
			throw (exc);
		}
		return dim1;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/04/29
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_write structure used by NetCDFReader for writing a container into a NcVar.
 * \param Nb_components : the number of components or netcdf variables = n
 * \param Dim : container dimension = 3
 * \return the first dimension size that has been filled
 */
template <std::size_t N>
struct __nc_write_data<N,3>{
	template <typename MultiContainer3D, typename T>
	static std::size_t apply(const MultiContainer3D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos,const std::vector<NcDim*> & nc_dim)
	{
		std::size_t dim1  = static_cast<std::size_t>((container.back_bottom_right() -
				container.front_upper_left()).dx1());
		std::size_t dim2 = static_cast<std::size_t>(nc_dim[1]->size());
		std::size_t dim3 = static_cast<std::size_t>(nc_dim[2]->size());

		for(std::size_t i = 0; i < N; ++i)
		{
			if (!nc_var[i]->set_cur(first_dim_pos,0,0)){
				std::ostringstream err;
				err << "Impossible to set the new corner of component " << i << ".";
				slip::slip_exception exc(std::string("slip"), std::string("_nc_write_data_n_3"), err.str());
				throw (exc);
			}
			slip::Array3d<T> tmp(dim1,dim2,dim3,container.begin(i),container.end(i));
			if (!(nc_var[i]->put(tmp.begin(),static_cast<long>(dim1),static_cast<long>(dim2),
					static_cast<long>(dim3))))
			{
				std::ostringstream err;
				err << "Cannot write 3D data.";
				slip::slip_exception exc(std::string("slip"), std::string("_nc_write_data_n_3"), err.str());
				throw (exc);
			}
		}
		return dim1;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/04/29
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_write structure used by NetCDFReader for writing a container into a NcVar.
 * \param Nb_components : the number of components or netcdf variables = 1
 * \param Dim : container dimension = 4
 * \return the first dimension size that has been filled
 */
template <>
struct __nc_write_data<1,4>{
	template <typename MonoContainer4D, typename T>
	static std::size_t apply(const MonoContainer4D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, const std::vector<NcDim*> & nc_dim)
	{
		std::size_t dim1 = static_cast<std::size_t>((container.last_back_bottom_right() -
				container.first_front_upper_left()).dx1());
		if (!nc_var[0]->set_cur(first_dim_pos,0,0,0)){
			std::ostringstream err;
			err << "Impossible to set the new corner.";
			slip::slip_exception exc(std::string("slip"), std::string("__nc_write_data_1_4"), err.str());
			throw (exc);
		}
		if (!nc_var[0]->put(container.begin(),static_cast<long>(dim1),nc_dim[1]->size(),nc_dim[2]->size(),
				nc_dim[3]->size()))
		{
			std::ostringstream err;
			err << "Cannot write 4D data.";
			slip::slip_exception exc(std::string("slip"), std::string("__nc_write_data_1_4"), err.str());
			throw (exc);
		}
		return dim1;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/04/29
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_write structure used by NetCDFReader for writing a container into a NcVar.
 * \param Nb_components : the number of components or netcdf variables = n
 * \param Dim : container dimension = 4
 * \return the first dimension size that has been filled
 */
template <std::size_t N>
struct __nc_write_data<N,4>{
	template <typename MultiContainer4D, typename T>
	static std::size_t apply(const MultiContainer4D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos,const std::vector<NcDim*> & nc_dim)
	{
		std::size_t dim1  = static_cast<std::size_t>((container.last_back_bottom_right() -
				container.first_front_upper_left()).dx1());
		std::size_t dim2 = static_cast<std::size_t>(nc_dim[1]->size());
		std::size_t dim3 = static_cast<std::size_t>(nc_dim[2]->size());
		std::size_t dim4 = static_cast<std::size_t>(nc_dim[3]->size());

		for(std::size_t i = 0; i < N; ++i)
		{
			if (!nc_var[i]->set_cur(first_dim_pos,0,0,0)){
				std::ostringstream err;
				err << "Impossible to set the new corner of component " << i << ".";
				slip::slip_exception exc(std::string("slip"), std::string("_nc_write_data_n_4"), err.str());
				throw (exc);
			}
			slip::Array4d<T> tmp(dim1,dim2,dim3,dim4,container.begin(i),container.end(i));
			if (!(nc_var[i]->put(tmp.begin(),static_cast<long>(dim1),static_cast<long>(dim2),
					static_cast<long>(dim3),static_cast<long>(dim4))))
			{
				std::ostringstream err;
				err << "Cannot write 4D data.";
				slip::slip_exception exc(std::string("slip"), std::string("_nc_write_data_n_4"), err.str());
				throw (exc);
			}
		}
		return dim1;
	}
};

/*!
 * \endcond
 */

/*!
 ** @ingroup IO
 ** \brief NetCDFWriter is a net
 ** \version Fluex 1.0
 ** \date 2013/04/29
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>cdf writer.
 ** \param Container is the type of the containers that will be written in the netcdf file.
 ** It can be a single or multi component container of dimension 1,2,3 (4 will come later).
 ** \param T the type of the container data.
 ** \param Nb_components the number of container components (ex : 3 for a color signal or 1 for a gray scale signal...).
 ** \param Dim is the dimension of the signal
 ** \note Every component will be registered as a separate variable.
 */
template<class Container, typename T, std::size_t Nb_components, std::size_t Dim>
class NetCDFWriter: public ContainerWriter<Container, T> {
public:
	typedef Container container_type;
	typedef T value_type;
	typedef ContainerWriter<Container, T> base;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** Default constructor
	 */
	NetCDFWriter()
	:base(),output_signal_dimensions_(),filemode_(NcFile::Replace),var_names_(),nc_var_(),nc_dim_()
	,dataFile(0),finished(0),total_dim1_size(0),initialized(false){}

	/*!
	 ** Constructor with parameters.
	 ** \param output_filename is the name of the output file
	 ** \param output_signal_dimensions is the vector containing the dimension size of the signal = [dim1,dim2,dim3...]
	 ** \param var_names vector of string which contains the name of the variables (features)
	 ** If this vector is empty, default names will be used ("I1","I2"...)
	 ** \param filemode NcFile::FileMode netcdf filemode (NcFile::Replace by default)
	 */
	NetCDFWriter(std::string output_filename, const std::vector<std::size_t> & output_signal_dimensions,
			const std::vector<std::string> & var_names, NcFile::FileMode filemode = NcFile::Replace)
	:base(output_filename),output_signal_dimensions_(output_signal_dimensions),filemode_(filemode),
	 var_names_(),nc_var_(),nc_dim_(),dataFile(0),finished(0),total_dim1_size(0),initialized(false)
	{
		set_var_names(var_names);
		initialize();
	}

	/*!
	 ** Destructor
	 */
	virtual ~NetCDFWriter(){
		release();
	}

	/*@} End Constructors */

	/*!
	 ** \brief initialized the writing process. Has to be called before the first write() call of a given signal.
	 ** \param output_signal_dimensions is the vector containing the dimension size of the signal = [dim1,dim2,dim3...]
	 ** \param var_names vector of string which contains the name of the variables (features)
	 ** If this vector is empty, default names will be used ("I1","I2"...)
	 ** \param filemode NcFile::FileMode netcdf filemode (NcFile::Replace by default)
	 */
	void initialize(const std::vector<std::size_t> & output_signal_dimensions,
			const std::vector<std::string> & var_names, NcFile::FileMode filemode = NcFile::Replace);


	/*!
	 ** \brief release the writing process. Has to be called when an netcdf has been fully written.
	 */
	void release();

	/*!
	 ** \brief write function. This function write the data within the output
	 ** file after the previous one. When last is set to true, the output file is closed.
	 ** \param data is the container to save.
	 ** \return false if the file has been completely filled.
	 */
	int write(const container_type & data);

private:
	/*! output_signal_dimensions is the vector containing the dimension size of the signal = [dim1,dim2,dim3...] */
	std::vector<std::size_t> output_signal_dimensions_;
	/*! filemode NcFile::FileMode netcdf filemode (NcFile::Replace by default) */
	NcFile::FileMode filemode_;
	/*!vector of string which contains the name of the variables (features)*/
	std::vector<std::string> var_names_;
	/*!vector containing the NcVar adresses (NetCDF objects) */
	std::vector<NcVar*> nc_var_;
	/*!vector containing the NCDim of the NcVar adresses (NetCDF objects) */
	std::vector<NcDim*> nc_dim_;
	/*!netCDF files, providing methods for netCDF file operations.*/
	NcFile * dataFile;


	/*! flag indicating the end of the writing process */
	bool finished;
	/*! total dim1 written counter*/
	std::size_t total_dim1_size;
	/*! bool indicates that initialization has been done */
	bool initialized;

	/*!
	 ** \param var_names is the vector containing the variables names
	 ** \pre var_names.size() == Nb_components (one variable by component)
	 ** if not, no exception is risen but the variables names will be set
	 ** with default values.
	 ** \pre 	- var_names[0] == name of the first variable (component)
	 ** 		- var_names[1] == name of the second variable (component)
	 **			- ...
	 **			.
	 */
	void set_var_names(const std::vector<std::string> & var_names){
		if(var_names.size() == Nb_components){
			var_names_.clear();
			var_names_.assign(var_names.begin(),var_names.end());
		}
	}
	//All components will have the same dimensions.
	void create_dim();
	// Warning create_var() should be launched after create_dim()!
	void create_var();
	void initialize();
};

} /*! namespace slip */

namespace slip {


template<class Container, typename T, std::size_t Nb_components, std::size_t Dim>
inline
void NetCDFWriter<Container,T,Nb_components,Dim>::create_dim(){
	for(std::size_t i = 0; i < Dim; ++i)
	{
		std::ostringstream ost;
		ost<<(i+1);
		std::string dim_name("Dim"+ost.str());
		nc_dim_.push_back(dataFile->add_dim(dim_name.c_str(), static_cast<long>(output_signal_dimensions_[i])));
		if (!(nc_dim_[i]))
		{
			std::ostringstream err;
			err << FILE_WRITE_ERROR << this->output_filename_ << " | Cannot create " << dim_name << ".";
			slip::slip_exception exc(std::string("slip"), std::string("NetCDFWriter::create_dim()"), err.str());
			throw (exc);
		}
	}
}

template<class Container, typename T, std::size_t Nb_components, std::size_t Dim>
inline
void NetCDFWriter<Container,T,Nb_components,Dim>::create_var(){

	//default variables names
	if(var_names_.empty())
	{
		var_names_.resize(Nb_components);
		for(std::size_t i = 0; i < Nb_components; ++i)
		{
			std::ostringstream ost;
			ost<<(i+1);
			var_names_[i] = std::string("I"+ost.str());
		}
	}
	if (var_names_.size() != Nb_components){
		std::ostringstream err;
		err << FILE_WRITE_ERROR << this->output_filename_ << " | The var_name vector should contain "
				<< Nb_components << " names.";
		slip::slip_exception exc(std::string("slip"), std::string("NetCDFWriter::create_var()"), err.str());
		throw (exc);
	}
	// Define the netCDF data variables.
	for(std::size_t i = 0; i < Nb_components; ++i)
	{
		switch (Dim){
		case 1:	nc_var_.push_back(dataFile->add_var(var_names_[i].c_str(),(NcType)(NcTypeInfo<T>::ncId()),
				nc_dim_[0]));break;
		case 2:	nc_var_.push_back(dataFile->add_var(var_names_[i].c_str(),(NcType)(NcTypeInfo<T>::ncId()),
				nc_dim_[0],nc_dim_[1]));break;
		case 3:nc_var_.push_back(dataFile->add_var(var_names_[i].c_str(),(NcType)(NcTypeInfo<T>::ncId()),
				nc_dim_[0],nc_dim_[1],nc_dim_[2]));break;
		case 4:nc_var_.push_back(dataFile->add_var(var_names_[i].c_str(),(NcType)(NcTypeInfo<T>::ncId()),
				nc_dim_[0],nc_dim_[1],nc_dim_[2],nc_dim_[3]));break;
		default:
			std::ostringstream err;
			err << FILE_WRITE_ERROR << this->output_filename_ << " | Dim is not valid.";
			slip::slip_exception exc(std::string("slip"), std::string("NetCDFWriter::create_var()"), err.str());
			throw (exc);
		}
		if(!(nc_var_[i]))
		{
			std::ostringstream err;
			err << FILE_WRITE_ERROR << this->output_filename_ << " | " << var_names_[i] << " is not a valid var name.";
			slip::slip_exception exc(std::string("slip"), std::string("NetCDFWriter::create_var()"), err.str());
			throw (exc);
		}
	}
}


template<class Container, typename T, std::size_t Nb_components, std::size_t Dim>
inline
void NetCDFWriter<Container,T,Nb_components,Dim>::initialize(
		const std::vector<std::size_t> & output_signal_dimensions, const std::vector<std::string> & var_names,
		NcFile::FileMode filemode){
	output_signal_dimensions_.assign(output_signal_dimensions.begin(),output_signal_dimensions.end());
	filemode_ = filemode;
	set_var_names(var_names);
	initialize();
}

template<class Container, typename T, std::size_t Nb_components, std::size_t Dim>
inline
void NetCDFWriter<Container,T,Nb_components,Dim>::initialize(){
	if(!initialized){
		if (Nb_components == 0 || Dim == 0 || Dim > 4){//Dim > 4 with 4D containers
			std::ostringstream err;
			err << FILE_WRITE_ERROR << this->output_filename_ << " | Bad template parameters.";
			slip::slip_exception exc(std::string("slip"), std::string("NetCDFWriter::initialize()"), err.str());
			throw (exc);
		}
		if (output_signal_dimensions_.size() != Dim){
			std::ostringstream err;
			err << FILE_WRITE_ERROR << this->output_filename_ << " | Dimensions does not match.";
			slip::slip_exception exc(std::string("slip"), std::string("NetCDFWriter::initialize()"), err.str());
			throw (exc);
		}
		//open the file
		dataFile = new NcFile(this->output_filename_.c_str(), filemode_,0,0,NcFile::Classic);
		NcError err(NcError::silent_nonfatal);
		if (!dataFile->is_valid())
		{
			delete dataFile;
			std::ostringstream err;
			err << FILE_OPEN_ERROR << this->output_filename_;
			slip::slip_exception exc(std::string("slip"), std::string("NetCDFWriter::initialize()"), err.str());
			throw (exc);
		}
		//Create variables and dimensions
		try{
			create_dim();
		}catch(std::exception &e){
			if(dataFile->is_valid()){
				dataFile->close();
				delete dataFile;
			}
			throw(e);
		}

		try{
			create_var();
		}catch(std::exception &e){
			if(dataFile->is_valid()){
				dataFile->close();
				delete dataFile;
			}
			throw(e);
		}
		initialized = true;
	}
}

template<class Container, typename T, std::size_t Nb_components, std::size_t Dim>
inline
int NetCDFWriter<Container,T,Nb_components,Dim>::write(const container_type & data){
	if(!initialized){
		std::ostringstream err;
		err << FILE_WRITE_ERROR << this->output_filename_ << " | The writer to be initialized before writing.";
		slip::slip_exception exc(std::string("slip"), std::string("NetCDFWriter::write()"), err.str());
		throw (exc);
	}
	if (finished){
		return 0;
	}

	try{
		total_dim1_size += __nc_write_data<Nb_components,Dim>::
				template apply<Container,T> (data,nc_var_,static_cast<long>(total_dim1_size),nc_dim_);
	}catch(std::exception &e){
		if(dataFile->is_valid()){
			dataFile->close();
			delete dataFile;
		}
		std::ostringstream err;
		err << FILE_WRITE_ERROR << this->output_filename_ << " | " << e.what() << ".";
		slip::slip_exception exc(std::string("slip"), std::string("NetCDFWriter::write()"), err.str());
		throw(exc);
	}

	if (total_dim1_size == output_signal_dimensions_[0]){
		finished = true;
	}

	return (!finished);
}

template<class Container, typename T, std::size_t Nb_components, std::size_t Dim>
inline
void NetCDFWriter<Container,T,Nb_components,Dim>::release(){
	if(initialized){
		if(dataFile->is_valid()){
			dataFile->close();
			delete dataFile;
		}
		output_signal_dimensions_.clear();
		filemode_ = NcFile::Replace;
		finished = false;
		total_dim1_size = 0;
		initialized = false;
	}
}




} /*! namespace slip */
#endif /* NETCDFWRITER_HPP_ */

