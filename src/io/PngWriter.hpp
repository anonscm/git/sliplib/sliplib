/*!
 ** \file PngWriter.hpp
 ** \brief containers writer from png files
 ** It includes png.h from libpng
 ** \version Fluex 1.0
 ** \date 2013/04/22
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef PNGWRITER_HPP_
#define PNGWRITER_HPP_

#include <iostream>
#include <sstream>

#include "PngDef.hpp"
#include "error.hpp"
#include "ContainerWriter.hpp"


namespace slip {

template <typename T>
struct png_static_cast_func
{
	template <typename T1> // T1 models type statically convertible to T
	T operator()(const T1& x) const { return static_cast<T>(x); }
};

/*!
 ** @ingroup IO
 ** \version Fluex 1.0
 ** \date 2022/12/09
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief PngWriter is the png image writer.
 ** \param Container2d is the type of the containers that will be written in the png file.
 ** It should be a 2 dimensional containers with [] operator, width() and height() methods defined.
 ** \param T the type of the container data.
 ** \param Nb_components the number of image components (3 for a color image or 1 for a gray scale image).
 ** \note The color space is RGB for color image and GRAYSCALE for grayscale images.
 */
template<class Container2d, typename T, std::size_t Nb_components>
class PngWriter: public ContainerWriter<Container2d, T>{
public:

	typedef Container2d container_type;
	typedef T value_type;
	typedef ContainerWriter<Container2d, T> base;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** Default constructor
	 */
	PngWriter()
	:base(),outfile(NULL),png_ptr(NULL),info_ptr(NULL),row_stride(0),
	 finished(0),total_nb_row(0),initialized(false),output_image_width_(0),
	 output_image_height_(0)
	{}

	/*!
	 ** Constructor with parameters.
	 ** \param output_filename is the name of the output file
	 ** where the data of the container will be saved
	 ** \param output_image_width the total image width
	 ** \param output_image_height the total image height
	 ** \param quality is the compression rate (default is 90%)
	 */
	PngWriter(std::string output_filename, std::size_t output_image_width, std::size_t output_image_height)
	:base(output_filename),outfile(NULL),png_ptr(NULL),info_ptr(NULL),row_stride(0),finished(0),total_nb_row(0),
	 initialized(false),output_image_width_(output_image_width),output_image_height_(output_image_height){
		initialize();
	}

	/*!
	 ** Destructor
	 */
	virtual ~PngWriter(){
		release();
	}
	/*@} End Constructors */

	/*!
	 ** \brief initialized the writing process. Has to be called before the first write() call of a given image.
	 ** \param output_image_width the total image width
	 ** \param output_image_height the total image height
	 */
	void initialize(std::size_t output_image_width, std::size_t output_image_height);


	/*!
	 ** \brief release the writing process. Has to be called when an image has been fully written.
	 */
	void release();

	/*!
	 ** \brief write function. This function write the data within the output
	 ** file after the previous one. When last is set to true, the output file is closed.
	 ** \param data is the image container to save. Warning, the container should be a 2d container
	 ** with [] operator, width() and height() methods defined.
	 ** \return false if the file has been completely filled.
	 ** \note Whatever the type of the data in the container, a static_cast is done for getting unsigned char.
	 ** Be sure that the dynamic of the data is conform with this cast.
	 ** \note The color space is RGB for color image and GRAYSCALE for grayscale images.
	 */
	int write(const container_type & data);

private:

	FILE *outfile;
	png_structp png_ptr;
	png_infop info_ptr;
	//png_colorp palette;

	int row_stride;		/** physical row width in image buffer */
	bool finished; /** flag indicating the end of the writting process */
	std::size_t total_nb_row;
	bool initialized;
	std::size_t output_image_width_;
	std::size_t output_image_height_;

	void initialize();
};

} /*! namespace slip */

namespace slip {

template<class Container2d, typename T, std::size_t Nb_components>
inline
void PngWriter<Container2d,T,Nb_components>::initialize(std::size_t output_image_width,
		std::size_t output_image_height){
	output_image_width_ = output_image_width;
	output_image_height_ = output_image_height;
	initialize();
}


template<class Container2d, typename T, std::size_t Nb_components>
inline
void PngWriter<Container2d,T,Nb_components>::initialize(){
	if(!initialized){
		if (Nb_components != 1 && Nb_components != 3){
			std::ostringstream err;
			err << FILE_WRITE_ERROR << this->output_filename_ << " | Nb_components should be 1 or 3.";
			slip::slip_exception exc(std::string("slip"), std::string("PngWriter::initialize()"), err.str());
			throw (exc);
		}
		/* Open the file */
		if ((outfile = fopen(this->output_filename_.c_str(), "wb")) == NULL) {
			std::ostringstream err;
			err << FILE_OPEN_ERROR << this->output_filename_;
			slip::slip_exception exc(std::string("slip"), std::string("PngWriter::initialize()"), err.str());
			throw (exc);
		}

		/* Create and initialize the png_struct with the desired error handler
		 * functions.  If you want to use the default stderr and longjump method,
		 * you can supply NULL for the last three parameters.  We also check that
		 * the library version is compatible with the one used at compile time,
		 * in case we are using dynamically linked libraries.  REQUIRED.
		 */
		png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,NULL,NULL,NULL);

		if (png_ptr == NULL)
		{
			fclose(outfile);
			std::ostringstream err;
			err << FILE_WRITE_ERROR << this->output_filename_ << " | Error in png_ptr initialization.";
			slip::slip_exception exc(std::string("slip"), std::string("PngWriter::initialize()"), err.str());
			throw (exc);
		}

		/* Allocate/initialize the image information data.  REQUIRED */
		info_ptr = png_create_info_struct(png_ptr);
		if (info_ptr == NULL)
		{
		  png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
			fclose(outfile);
			std::ostringstream err;
			err << FILE_WRITE_ERROR << this->output_filename_ << " | Error in info_ptr allocation.";
			slip::slip_exception exc(std::string("slip"), std::string("PngWriter::initialize()"), err.str());
			throw (exc);
		}

		/* Set error handling.  REQUIRED if you aren't supplying your own
		 * error handling functions in the png_create_write_struct() call.
		 */
		if (setjmp(png_jmpbuf(png_ptr)))
		{
			/* If we get here, we had a problem writing the file */
			png_destroy_write_struct(&png_ptr, &info_ptr);
			fclose(outfile);
			std::ostringstream err;
			err << FILE_WRITE_ERROR << this->output_filename_ << " | PNG code has signaled an error.";
			slip::slip_exception exc(std::string("slip"), std::string("PngWriter::initialize()"), err.str());
			throw (exc);
		}

		/* Set up the output control if you are using standard C streams */
		png_init_io(png_ptr, outfile);

		/* Set the image information here.  Width and height are up to 2^31,
		 * bit_depth is one of 1, 2, 4, 8, or 16, but valid values also depend on
		 * the color_type selected. color_type is one of PNG_COLOR_TYPE_GRAY,
		 * PNG_COLOR_TYPE_GRAY_ALPHA, PNG_COLOR_TYPE_PALETTE, PNG_COLOR_TYPE_RGB,
		 * or PNG_COLOR_TYPE_RGB_ALPHA.  interlace is either PNG_INTERLACE_NONE or
		 * PNG_INTERLACE_ADAM7, and the compression_type and filter_type MUST
		 * currently be PNG_COMPRESSION_TYPE_BASE and PNG_FILTER_TYPE_BASE. REQUIRED
		 */
		if (Nb_components == 1){
			png_set_IHDR(png_ptr, info_ptr, output_image_width_, output_image_height_,
					8, PNG_COLOR_TYPE_GRAY, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
		}else{
			png_set_IHDR(png_ptr, info_ptr, output_image_width_, output_image_height_,
					8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
		}

		/* Write the file header information.  REQUIRED */
		png_write_info(png_ptr, info_ptr);


		row_stride = output_image_width_ * Nb_components;
		initialized = true;
	}
}



template<class Container2d, typename T, std::size_t Nb_components>
inline
int PngWriter<Container2d,T,Nb_components>::write(const container_type & data){
	if(!initialized){
		std::ostringstream err;
		err << FILE_WRITE_ERROR << this->output_filename_ << " | The writer to be initialized before writing.";
		slip::slip_exception exc(std::string("slip"), std::string("PngWriter::write()"), err.str());
		throw (exc);
	}
	if (finished){
		return 0;
	}

	total_nb_row += data.height();

	if(output_image_height_ < total_nb_row){
		std::ostringstream err;
		err << FILE_WRITE_ERROR << this->output_filename_ << " | Allocation problem during png writing.";
		slip::slip_exception exc(std::string("slip"), std::string("PngWriter::write()"), err.str());
		throw (exc);
	}


	png_bytep row_pointer[1];

	if (static_cast<png_uint_32>(output_image_height_) > PNG_UINT_32_MAX/sizeof(png_bytep))
		png_error (png_ptr, "Image is too tall to process in memory");

	for (std::size_t j=0; j<data.height(); ++j){
		row_pointer[0] = new unsigned char[row_stride];
		const T * data_ptr_j  = reinterpret_cast<const T *>(data[j]);
		std::transform(data_ptr_j, data_ptr_j + row_stride,
				row_pointer[0], png_static_cast_func<unsigned char>());
		png_write_rows(png_ptr, &row_pointer[0], 1);
		delete[] row_pointer[0];
	}

	if (total_nb_row == output_image_height_){
		png_write_end(png_ptr, info_ptr);
		finished = true;
	}

	return (!finished);
}

template<class Container2d, typename T, std::size_t Nb_components>
inline
void PngWriter<Container2d,T,Nb_components>::release(){
	if(initialized){
		/* Clean up after the write, and free any memory allocated */
		png_destroy_write_struct(&png_ptr, &info_ptr);

		/* Close the file */
		if (outfile != NULL)
			fclose(outfile);

		row_stride = 0;
		finished = false;
		total_nb_row = 0;
		output_image_width_ = 0;
		output_image_height_ = 0;
		initialized = false;
	}
}

} /*! namespace slip */

#endif /* PNGWRITER_HPP_ */
