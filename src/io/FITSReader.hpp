/*!
 ** \file FITSReader.hpp
 ** \brief containers reader from fits files (read only Single Image FITS file)
 ** \version Fluex 1.0
 ** \date 2013/09/03
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef FITSREADER_HPP_
#define FITSREADER_HPP_

#include <valarray>
#include <memory>
#include "FITSDef.hpp"

#include "ContainerReader.hpp"

namespace slip {

/*!
 * \cond Do not include the following into documentation
 */

/*!
 * \version Fluex 1.0
 * \date 2013/09/03
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_read structure used by FITSReader for reading the data of a Primary HDU.
 * \param Nb_components : the number of components or fits variables
 * \param Dim : container dimension
 */
template <std::size_t Nb_components, std::size_t Dim>
struct __fits_read_data{
	template <typename MonoContainer, typename T>
	static void apply(MonoContainer& container, CCfits::PHDU * phdu,
			long first_dim_pos, long first_dim_size){
		std::ostringstream err;
		err << "Nb Components or dimension is not valid.";
		slip::slip_exception exc(std::string("slip"), std::string("fits_read_data"), err.str());
		throw (exc);
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/03
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_read structure used by FITSReader for reading the data of a Primary HDU.
 * \param Nb_components : the number of components or fits variables = 1
 * \param Dim : container dimension = 1
 */
template <>
struct __fits_read_data<1,1>{
	template <typename MonoContainer1D, typename T>
	static void apply(MonoContainer1D& container, CCfits::PHDU * phdu,
			long first_dim_pos, long first_dim_size)
	{
		std::valarray<T> contents;
		std::size_t dim = first_dim_size;
		try{
			phdu->read(contents,first_dim_pos+1,dim);
		}catch(CCfits::ExtHDU::WrongExtensionType &ef){
			std::ostringstream err;
			err << "HDU is not an image.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_1_1"), err.str());
			throw (exc);
		}catch(...){
			std::ostringstream err;
			err << "1D data not found.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_1_1"), err.str());
			throw (exc);
		}
		std::copy(&contents[0],&contents[0]+dim,container.begin());
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/03
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_read structure used by FITSReader for reading the data of a Primary HDU.
 * \param Nb_components : the number of components or fits variables = N
 * \param Dim : container dimension = 1
 */
template <std::size_t N>
struct __fits_read_data<N,1>{
	template <typename MultiContainer1D, typename T>
	static void apply(MultiContainer1D& container, CCfits::PHDU * phdu,
			long first_dim_pos, long first_dim_size)
	{
		std::valarray<T> contents;
		std::size_t dim = first_dim_size;
		try{
			phdu->read(contents,first_dim_pos+1,dim);
		}catch(CCfits::ExtHDU::WrongExtensionType &ef){
			std::ostringstream err;
			err << "HDU is not an image.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_N_1"), err.str());
			throw (exc);
		}catch(...){
			std::ostringstream err;
			err << "1D data not found.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_N_1"), err.str());
			throw (exc);
		}
		for (int i=0; i<static_cast<int>(N); ++i){
			std::copy(&contents[0],&contents[0]+dim,container.begin(i));
		}
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/03
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_read structure used by FITSReader for reading the data of a Primary HDU.
 * \param Nb_components : the number of components or fits variables = 1
 * \param Dim : container dimension = 2
 */
template <>
struct __fits_read_data<1,2>{
	template <typename MonoContainer2D, typename T>
	static void apply(MonoContainer2D& container, CCfits::PHDU * phdu,
			long first_dim_pos, long first_dim_size)
	{
		std::valarray<T> contents;
		std::size_t dim = first_dim_size * container.dim2();
		try{
			std::vector<long> first(2,1);
			first[1] = first_dim_pos+1;
			phdu->read(contents,first,dim);
		}catch(CCfits::ExtHDU::WrongExtensionType &ef){
			std::ostringstream err;
			err << "HDU is not an image.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_1_2"), err.str());
			throw (exc);
		}catch(...){
			std::ostringstream err;
			err << "2D data not found.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_1_2"), err.str());
			throw (exc);
		}
		std::copy(&contents[0],&contents[0]+dim,container.begin());
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/03
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_read structure used by FITSReader for reading the data of a Primary HDU.
 * \param Nb_components : the number of components or fits variables = N
 * \param Dim : container dimension = 2
 */
template <std::size_t N>
struct __fits_read_data<N,2>{
	template <typename MultiContainer2D, typename T>
	static void apply(MultiContainer2D& container, CCfits::PHDU * phdu,
			long first_dim_pos, long first_dim_size)
	{
		std::valarray<T> contents;
		std::size_t dim = first_dim_size * container.dim2();
		try{
			std::vector<long> first(2,1);
			first[1] = first_dim_pos+1;
			phdu->read(contents,first,dim);
		}catch(CCfits::ExtHDU::WrongExtensionType &ef){
			std::ostringstream err;
			err << "HDU is not an image.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_N_2"), err.str());
			throw (exc);
		}catch(...){
			std::ostringstream err;
			err << "2D data not found.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_N_2"), err.str());
			throw (exc);
		}
		for (int i=0; i<static_cast<int>(N); ++i){
			std::copy(&contents[0],&contents[0]+dim,container.begin(i));
		}
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/03
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_read structure used by FITSReader for reading the data of a Primary HDU.
 * \param Nb_components : the number of components or fits variables = 1
 * \param Dim : container dimension = 3
 */
template <>
struct __fits_read_data<1,3>{
	template <typename MonoContainer3D, typename T>
	static void apply(MonoContainer3D& container, CCfits::PHDU * phdu,
			long first_dim_pos, long first_dim_size)
	{
		std::valarray<T> contents;
		std::size_t dim = first_dim_size * container.dim2() * container.dim3();
		try{
			std::vector<long> first(3,1);
			first[2] = first_dim_pos+1;
			phdu->read(contents,first,dim);
		}catch(CCfits::ExtHDU::WrongExtensionType &ef){
			std::ostringstream err;
			err << "HDU is not an image.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_1_3"), err.str());
			throw (exc);
		}catch(...){
			std::ostringstream err;
			err << "3D data not found.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_1_3"), err.str());
			throw (exc);
		}
		std::copy(&contents[0],&contents[0]+dim,container.begin());
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/03
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_read structure used by FITSReader for reading the data of a Primary HDU.
 * \param Nb_components : the number of components or fits variables = N
 * \param Dim : container dimension = 3
 */
template <std::size_t N>
struct __fits_read_data<N,3>{
	template <typename MultiContainer3D, typename T>
	static void apply(MultiContainer3D& container, CCfits::PHDU * phdu,
			long first_dim_pos, long first_dim_size)
	{
		std::valarray<T> contents;
		std::size_t dim = first_dim_size * container.dim2() * container.dim3();
		try{
			std::vector<long> first(3,1);
			first[2] = first_dim_pos+1;
			phdu->read(contents,first,dim);
		}catch(CCfits::ExtHDU::WrongExtensionType &ef){
			std::ostringstream err;
			err << "HDU is not an image.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_N_3"), err.str());
			throw (exc);
		}catch(...){
			std::ostringstream err;
			err << "3D data not found.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_N_3"), err.str());
			throw (exc);
		}
		for (int i=0; i<static_cast<int>(N); ++i){
			std::copy(&contents[0],&contents[0]+dim,container.begin(i));
		}
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/03
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_read structure used by FITSReader for reading the data of a Primary HDU.
 * \param Nb_components : the number of components or fits variables = 1
 * \param Dim : container dimension = 4
 */
template <>
struct __fits_read_data<1,4>{
	template <typename MonoContainer4D, typename T>
	static void apply(MonoContainer4D& container, CCfits::PHDU * phdu,
			long first_dim_pos, long first_dim_size)
	{
		std::valarray<T> contents;
		std::size_t dim = first_dim_size * container.dim2() * container.dim3() * container.dim4();
		try{
			std::vector<long> first(4,1);
			first[3] = first_dim_pos+1;
			phdu->read(contents,first,dim);
		}catch(CCfits::ExtHDU::WrongExtensionType &ef){
			std::ostringstream err;
			err << "HDU is not an image.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_1_4"), err.str());
			throw (exc);
		}catch(...){
			std::ostringstream err;
			err << "4D data not found.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_1_4"), err.str());
			throw (exc);
		}
		std::copy(&contents[0],&contents[0]+dim,container.begin());
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/03
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_read structure used by FITSReader for reading the data of a Primary HDU.
 * \param Nb_components : the number of components or fits variables = N
 * \param Dim : container dimension = 4
 */
template <std::size_t N>
struct __fits_read_data<N,4>{
	template <typename MultiContainer4D, typename T>
	static void apply(MultiContainer4D& container, CCfits::PHDU * phdu,
			long first_dim_pos, long first_dim_size)
	{
		std::valarray<T> contents;
		std::size_t dim = first_dim_size * container.dim2() * container.dim3() * container.dim4();
		try{
			std::vector<long> first(4,1);
			first[3] = first_dim_pos+1;
			phdu->read(contents,first,dim);
		}catch(CCfits::ExtHDU::WrongExtensionType &ef){
			std::ostringstream err;
			err << "HDU is not an image.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_N_4"), err.str());
			throw (exc);
		}catch(...){
			std::ostringstream err;
			err << "4D data not found.";
			slip::slip_exception exc(std::string("slip"), std::string("fits_read_data_N_4"), err.str());
			throw (exc);
		}
		for (int i=0; i<static_cast<int>(N); ++i){
			std::copy(&contents[0],&contents[0]+dim,container.begin(i));
		}
	}
};

/*!
 * \endcond
 */

/*!
 ** @ingroup IO
 ** \version Fluex 1.0
 ** \date 2013/09/03
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief %FITSReader, inherited from %ContainerReader, is a reader of fits files
 ** (only Single Image FITS file as the reader read only the PHDU)
 ** \param Container should be a Container with Dim dimensions.
 ** \param T the type of the container data.
 ** \param Nb_components the number of components per point (ex : 3 for a color container or 1 for a gray scale container).
 ** \param Nb_block is the number of containers to create (>1 if one want to split the input
 ** data into different containers, default is 1). Notice that the data is always cut according to the
 ** first dimension of the input container.
 ** \param Dim is the dimension of the container.
 ** \pre Container should have Nb_components components with components iterators defined
 ** \pre The fits file should have defined a variable for each component.
 ** \note As FITS format has no widely accepted convention for storing color pictures or volumes,
 ** if Nb_components is more than 1, all components will be the same.
 ** \todo Extensions for being able to read extended HDU (different spectrum images for example)
 ** \see http://fits.gsfc.nasa.gov/
 */
template<class Container, typename T, std::size_t Nb_components, std::size_t Nb_block, std::size_t Dim>
class FITSReader: public ContainerReader<Container,T,Nb_block>{
public:
	typedef Container container_type;
	typedef T value_type;
	typedef ContainerReader<Container,T,Nb_block> base;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** Default constructor
	 */
	FITSReader()
	:base(),pInfile(0),data(0),axis(),block_ind(1),block_size(0),last_block_size(0),
	 last_it(0),finished(0),initialized(0)
	{}

	/*!
	 ** Constructor with parameters.
	 ** \param data_filename is the name of the file containing the data to read
	 ** \note this constructor automatically call the initialize() method.
	 */
	FITSReader(std::string data_filename)
	:base(data_filename),pInfile(0),data(0),axis(),block_ind(1),block_size(0),
	 last_block_size(0),last_it(0),finished(0),initialized(0)
	{ initialize();}

	/*!
	 ** Destructor
	 ** \note the release() method is called.
	 */
	virtual ~FITSReader(){
		if(initialized){
			release();
		}
	}
	/*@} End Constructors */

	/*!
	 ** \brief initialized the reading process
	 ** \note an initialization is called by the %FITSReader(std::string, const std::vector<std::string> &)
	 ** constructor.
	 */
	void initialize();

	/*!
	 ** \brief release the reading process.
	 ** \note this method is called by the destructor.
	 */
	void release();

	/*!
	 ** \brief virtual read function. If Nb_block is more than one,
	 ** it reads only one container, and returns 0. Else it reads the
	 ** container and returns 1. If the file data_filename
	 ** has been completely read (no more block to read), it returns 1.
	 ** \param in is the container to fill.
	 ** \return true if there is still data to read, false if the file has been completely read.
	 */
	int read(Container & in);

private:

	/*!FITS base object pointer*/
	std::auto_ptr<CCfits::FITS> pInfile;
	/*!Pointer on the Primary HDU that must contain the signal to read*/
	CCfits::PHDU * data;
	/*!Axis dimensions. Warning it is in reverse order comparing to container conventions!*/
	std::vector<int> axis;
	/*!current read block indice*/
	int block_ind;
	/*!block size*/
	int block_size;
	/*!last block size*/
	int last_block_size;
	/*!indicates that the current read is the last.*/
	bool last_it;
	/*!indicates that the last read has been done and no more data are available.*/
	bool finished;
	/*!indicates that the reader has been initialized*/
	bool initialized;

};

} /*! namespace slip */


namespace slip {



template<class Container, typename T, std::size_t Nb_components, std::size_t Nb_block, std::size_t Dim>
inline
void FITSReader<Container, T, Nb_components, Nb_block, Dim>::initialize(){
	if (!initialized){
		if (Nb_components == 0 || Dim == 0 || Dim > 4){//Dim > 4 with 4D containers
			std::ostringstream err;
			err << READ_ERROR << this->data_filename_ << " | Bad template parameters.";
			slip::slip_exception exc(std::string("slip"), std::string("FITSReader::initialize()"), err.str());
			throw (exc);
		}
		//open the file
		try{
			pInfile.reset(new CCfits::FITS(this->data_filename_,CCfits::Read,true));
		}catch(CCfits::FITS::CantCreate &ef){
			std::string filename = "!" + this->data_filename_;
			try{
				pInfile.reset(new CCfits::FITS(filename,CCfits::Read,true));
			}catch(CCfits::FITS::CantCreate){
				std::ostringstream err;
				err << FILE_OPEN_ERROR << this->data_filename_;
				slip::slip_exception exc(std::string("slip"), std::string("FITSReader::initialize()"), err.str());
				throw (exc);
			}
		}
		data = &pInfile->pHDU();
		//Get variables and dimensions
		long nbaxes = static_cast<int>(data->axes());
		if(nbaxes != Dim){
			std::ostringstream err;
			err << READ_ERROR << this->data_filename_ << " | Bad signal dimensions.";
			err << std::endl;
			err << "Dim should be " << Dim << " but " << nbaxes << " has been read." << std::endl;
			slip::slip_exception exc(std::string("slip"), std::string("FITSReader::initialize()"), err.str());
			throw (exc);
		}
		for(int i=0; i< nbaxes; ++i){
			axis.push_back(static_cast<int>(data->axis(i)));
		}
		block_size = axis.back() / static_cast<int>(Nb_block);
		last_block_size = axis.back() % static_cast<int>(Nb_block);
		last_block_size  = (last_block_size == 0 ? block_size : block_size+last_block_size);
		last_it = (Nb_block == 1);
		finished = (last_it && last_block_size == 0);
		initialized = true;
	}

}

template<class Container, typename T, std::size_t Nb_components, std::size_t Nb_block, std::size_t Dim>
inline
int FITSReader<Container, T, Nb_components, Nb_block, Dim>::read(Container & in){
	if(!initialized){
		std::ostringstream err;
		err << READ_ERROR << this->data_filename_ << " | The reader needs to be initialized before reading.";
		slip::slip_exception exc(std::string("slip"), std::string("FITSReader::read()"), err.str());
		throw (exc);
	}
	if (finished)
		return 0;

	long first_dim_size = static_cast<long>(last_it ? last_block_size : block_size);
	long first_dim_pos = static_cast<long>((block_ind-1) *  block_size);

	try{
		__fits_resize<Dim>::template apply<Container> (in,first_dim_size,axis);
	}catch(std::exception &e){
		std::ostringstream err;
		err << READ_ERROR << this->data_filename_ << " | Container resize impossible : " << e.what() << ".";
		slip::slip_exception exc(std::string("slip"), std::string("FITSReader::read()"), err.str());
		throw(exc);
	}

	try{
		__fits_read_data<Nb_components,Dim>::template apply<Container,T>(in,data,first_dim_pos,first_dim_size);
	}catch(std::exception &e){
		std::ostringstream err;
		err << READ_ERROR << this->data_filename_ << " | " << e.what() << ".";
		slip::slip_exception exc(std::string("slip"), std::string("FITSReader::read()"), err.str());
		throw(exc);
	}

	block_ind++;
	if (last_it){
		finished = true;
	}
	else
		last_it = (block_ind == Nb_block);
	return (!finished);
}

template<class Container, typename T, std::size_t Nb_components, std::size_t Nb_block, std::size_t Dim>
inline
void FITSReader<Container, T, Nb_components, Nb_block, Dim>::release(){
	if(initialized){

		pInfile.release();
		data = 0;
		axis.clear();
		block_ind = 1;
		block_size = 0;
		last_block_size = 0;
		last_it = 0;
		finished = false;
		initialized = false;
	}
}

} /*! namespace slip */
#endif /* FITSREADER_HPP_ */

