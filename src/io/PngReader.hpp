/*!
 ** \file PngReader.hpp
 ** \brief containers reader from png files
 ** It includes png.h from libpng
 ** \version Fluex 1.0
 ** \date 2013/04/22
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef PNGREADER_HPP_
#define PNGREADER_HPP_

#include <iostream>
#include <sstream>


#include "PngDef.hpp"
#include "error.hpp"
#include "ContainerReader.hpp"

namespace slip {

/*!
 ** @ingroup IO
 ** \version Fluex 1.0
 ** \date 2022/12/09
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief %PngReader, inherited from %ContainerReader, is a reader for png images.
 ** \param Container2d should be a 2d Container with a component number equal to Nb_components.
 ** \param T the type of the container data.
 ** \param Nb_components the number of image components (3 for a color image or 1 for a gray scale image).
 ** \param Nb_block is the number of containers to create (>1 if one want to split the input
 ** data into different containers, default is 1). Notice that the image data is always cut according to the
 ** rows dimension which must be the first dimension of the input container. In other words
 ** each container will contain a set of rows which size depends on the Nb_block.
 ** \pre Container2d should have Nb_components components
 ** \pre Nb_components should be 1 or 3.
 */
template<class Container2d, typename T, std::size_t Nb_components, std::size_t Nb_block>
class PngReader: public ContainerReader<Container2d,T,Nb_block> {
public:
	typedef Container2d container_type;
	typedef T value_type;
	typedef ContainerReader<Container2d,T,Nb_block> base;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** \brief Default constructor
	 */
	PngReader()
	:base(),png_ptr(NULL),info_ptr(NULL),sig_read(0),width(0),height(0),bit_depth(0),color_type(0),
	 interlace_type(0), number_passes(0),infile(NULL),row_stride(0),
	 block_ind(1),block_size(0),last_block_size(0),last_it(0),finished(0),initialized(0)
	{}

	/*!
	 ** Constructor with parameters.
	 ** \param data_filename is the name of the file containing the data to read
	 ** \note this constructor automatically call the initialize() method.
	 */
	PngReader(std::string data_filename)
	:base(data_filename),png_ptr(NULL),info_ptr(NULL),sig_read(0),width(0),height(0),bit_depth(0),color_type(0),
	 interlace_type(0), number_passes(0),infile(NULL),row_stride(0),
	 block_ind(1),block_size(0),last_block_size(0),last_it(0),finished(0),initialized(0)
	{initialize();}

	/*!
	 ** \brief Destructor
	 ** \note the release() method is called.
	 */
	virtual ~PngReader()
	{
		if(initialized){
			release();
		}
	}

	/*@} End Constructors */

	/*!
	 ** \brief initialized the reading process.
	 ** \note an initialization is called by the %PngReader(std::string) constructor.
	 */
	void initialize();

	/*!
	 ** \brief release the reading process.
	 ** \note this method is called by the destructor.
	 */
	void release();

	/*!
	 ** \brief virtual read function. If Nb_block is more than one,
	 ** it reads only one container, and returns 0. Else it reads the
	 ** container and returns 1. If the file data_filename
	 ** has been completely read (no more block to read), it returns 1.
	 ** \param in is the container to fill. Warning, the container should be a 2d container
	 ** with [] operator, resize(), width() and height() methods defined.
	 ** \return true if there is still data to read, false if the file has been completely read.
	 */
	int read(Container2d & in);

private :
	//libpng variables (see example.c provided with png.h)
	png_structp png_ptr;
	png_infop info_ptr;
	unsigned int sig_read;
	png_uint_32 width, height;
	int bit_depth, color_type, interlace_type, number_passes;
	png_bytep row_pointers[1];//buffer
	FILE* infile;//input file stream

	int row_stride;		/* physical row width in output buffer */
	int block_ind;/*current read block indice*/
	int block_size;/*block size*/
	int last_block_size;/*last block size*/
	bool last_it;/*indicates that the current read is the last.*/
	bool finished;/*indicates that the last read has been done and no more data are available.*/
	bool initialized;/*indicates that the reader has been initialized*/

};

} /*! namespace slip */


namespace slip {

template<class Container2d, typename T, std::size_t Nb_components, std::size_t Nb_block>
inline
void PngReader<Container2d, T, Nb_components, Nb_block>::initialize(){
	if (!initialized){
		if (Nb_components != 1 && Nb_components != 3){
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | Nb_components should be 1 or 3.";
			slip::slip_exception exc(std::string("slip"), std::string("PngReader::initialize()"), err.str());
			throw (exc);
		}

		if ((infile = fopen(this->data_filename_.c_str(), "rb")) == NULL) {
			std::ostringstream err;
			err << FILE_OPEN_ERROR << this->data_filename_;
			slip::slip_exception exc(std::string("slip"), std::string("PngReader::initialize()"), err.str());
			throw (exc);
		}

		/* Create and initialize the png_struct with the desired error handler
		 * functions.  If you want to use the default stderr and longjump method,
		 * you can supply NULL for the last three parameters.  We also supply the
		 * the compiler header file version, so that we know if the application
		 * was compiled with a compatible version of the library.  REQUIRED
		 */
		png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,NULL,NULL,NULL);

		if (png_ptr == NULL)
		{
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | png_struct initialization failed.";
			slip::slip_exception exc(std::string("slip"), std::string("PngReader::initialize()"), err.str());
			fclose(infile);
			throw (exc);
		}

		/* Allocate/initialize the memory for image information.  REQUIRED. */
		info_ptr = png_create_info_struct(png_ptr);
		if (info_ptr == NULL)
		{
		  png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
			fclose(infile);
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | info_ptr initialization failed.";
			slip::slip_exception exc(std::string("slip"), std::string("PngReader::initialize()"), err.str());
			throw (exc);
		}

		/* Set error handling if you are using the setjmp/longjmp method (this is
		 * the normal method of doing things with libpng).  REQUIRED unless you
		 * set up your own error handlers in the png_create_read_struct() earlier.
		 */

		if (setjmp(png_jmpbuf(png_ptr)))
		{
			/* Free all of the memory associated with the png_ptr and info_ptr */
		  png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
			fclose(infile);
			/* If we get here, we had a problem reading the file */
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | PNG library has signaled an error";
			slip::slip_exception exc(std::string("slip"), std::string("PngReader::initialize()"), err.str());
			throw (exc);
		}

		/* I/O initialization */
		png_init_io(png_ptr, infile);

		/* If we have already read some of the signature */
		png_set_sig_bytes(png_ptr, sig_read);

		/* The call to png_read_info() gives us all of the information from the
		 * PNG file before the first IDAT (image data chunk).  REQUIRED
		 */
		png_read_info(png_ptr, info_ptr);

		png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
			     &interlace_type, (int*)NULL, (int*)NULL);

		if(png_get_channels(png_ptr, info_ptr) != Nb_components){
			fclose(infile);
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | Nb_components does not match.";
			slip::slip_exception exc(std::string("slip"), std::string("PngReader::initialize()"), err.str());
			throw(exc);
		}

		/* Set up the data transformations you want.  Note that these are all
		 * optional.  Only call them if you want/need them.  Many of the
		 * transformations only work on specific types of images, and many
		 * are mutually exclusive.
		 */

		/* Tell libpng to strip 16 bit/color files down to 8 bits/color */
		png_set_strip_16(png_ptr);

		/* Strip alpha bytes from the input data without combining with the
		 * background (not recommended).
		 */
		//png_set_strip_alpha(png_ptr);

		/* Extract multiple pixels with bit depths of 1, 2, and 4 from a single
		 * byte into separate bytes (useful for paletted and grayscale images).
		 */
		png_set_packing(png_ptr);

		/* Change the order of packed pixels to least significant bit first
		 * (not useful if you are using png_set_packing). */
		png_set_packswap(png_ptr);


		/* Expand paletted colors into true RGB triplets */
		if (color_type == PNG_COLOR_TYPE_PALETTE){
			png_set_palette_to_rgb(png_ptr);
		}

		/* Expand grayscale images to the full 8 bits from 1, 2, or 4 bits/pixel */
		if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8){
			png_set_expand_gray_1_2_4_to_8(png_ptr);
		}

		/* Swap bytes of 16 bit files to least significant byte first */
		png_set_swap(png_ptr);

		/* Turn on interlace handling.  REQUIRED if you are not using
		 * png_read_image().  To see how to handle interlacing passes,
		 * see the png_read_row() method below:
		 */
		number_passes = png_set_interlace_handling(png_ptr);

		/* Clear the pointer array */
		row_pointers[0] = NULL;

		//	for (row = 0; row < height; row++)
		row_pointers[0] = reinterpret_cast<png_bytep>(png_malloc(png_ptr, png_get_rowbytes(png_ptr,info_ptr)));

		/*samples per row in output buffer */
		row_stride = png_get_rowbytes(png_ptr,info_ptr);
		block_size = static_cast<int>(height) / Nb_block;
		last_block_size = static_cast<int>(height) % Nb_block;
		last_block_size  = (last_block_size == 0 ? block_size : block_size+last_block_size);
		last_it = (Nb_block == 1);
		finished = (last_it && last_block_size == 0);
		initialized = true;
	}
}

template<class Container2d, typename T, std::size_t Nb_components, std::size_t Nb_block>
int PngReader<Container2d, T, Nb_components, Nb_block>::read(Container2d & in){
	if(!initialized){
		std::ostringstream err;
		err << FILE_READ_ERROR << this->data_filename_ << " | The reader needs to be initialized before reading.";
		slip::slip_exception exc(std::string("slip"), std::string("PngReader::read()"), err.str());
		throw (exc);
	}
	if (finished)
		return 0;

	if (last_it)
		in.resize(last_block_size,static_cast<std::size_t>(width),T());
	else{
		in.resize(block_size,static_cast<std::size_t>(width),T());
	}
	if(static_cast<std::size_t>(height) - static_cast<std::size_t>(png_get_current_row_number(png_ptr))< in.height()){
		std::ostringstream err;
		err << FILE_READ_ERROR << this->data_filename_ << " | Bad image dimensions.";
		slip::slip_exception exc(std::string("slip"), std::string("PngReader::read()"), err.str());
		throw (exc);
	}

	/* If the image is interlaced,  number_passes readings of the same row has to be done*/
	//std::cerr << "row_nb = " << png_ptr->row_number << "\n";
	for (int pass = 0; pass < number_passes; pass++)
	{
		/* Read the image a single row at a time */
		for(unsigned int i=0; i < in.height(); ++i)
		{
		  png_read_rows(png_ptr, &row_pointers[0], (png_bytepp)NULL, 1);
			T * ptr_i = reinterpret_cast<T *>(in[i]);
			std::copy(row_pointers[0],row_pointers[0]+row_stride,ptr_i);
		}
	}

	block_ind++;
	if (last_it){
		/* Read rest of file, and get additional chunks in info_ptr - REQUIRED */
		png_read_end(png_ptr, info_ptr);
		finished = true;
	}
	else
		last_it = (block_ind == Nb_block);
	return (!finished);
}

template<class Container2d, typename T, std::size_t Nb_components, std::size_t Nb_block>
void  PngReader<Container2d, T, Nb_components, Nb_block>::release(){
	if(initialized){
		/* At this point you have read the entire image */

		/* Clean up after the read, and free any memory allocated - REQUIRED */
	  png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);

		/* Close the file */
		if (infile != NULL)
			fclose(infile);
		/* That's it */
		if (row_pointers[0])
		  free(row_pointers[0]);
		sig_read = 0;
		width = 0;
		height = 0;
		bit_depth = 0;
		color_type = 0;
		interlace_type = 0;
		number_passes = 0;
		row_stride = 0;
		block_ind = 1;
		block_size = 0;
		last_block_size = 0;
		last_it = false;
		finished = false;
		initialized = false;
	}
}

} /*! namespace slip */

#endif /* PNGREADER_HPP_ */
