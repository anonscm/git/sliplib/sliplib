/*!
 ** \file AvWriter.hpp
 ** \brief containers writer from av files
 ** \version Fluex 1.0
 ** \date 2013/04/29
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef AVWRITER_HPP_
#define AVWRITER_HPP_

#include <iostream>
#include <sstream>

extern "C"
{
#include <libswscale/swscale.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavcodec/version.h>
#include <libavutil/pixfmt.h>
#include <libavutil/mathematics.h>
#include <stdio.h>
}

#include "color_spaces.hpp"
#include "error.hpp"
#include "ContainerWriter.hpp"


template <typename T>
struct av_static_cast_func
{
	template <typename T1> // T1 models type statically convertible to T
	T operator()(const T1& x) const { return static_cast<T>(x); }
};


namespace slip
{

/*!
 ** @ingroup IO
 ** \version Fluex 1.0
 ** \date 2013/04/29
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief AvWriter is a video writer.
 ** \param ContainerVideo is the type of the containers that will be written in the avi file.
 ** It has to be a 3D container. The [][] operator and the slices(), the rows() and the cols()
 ** methods should have been defined.
 ** \param T the type of the container data.
 ** \param Nb_components the number of video components (3 for a color video or 1 for a gray scale video).
 ** \note The input container format should be RGB24 or GRAY8 according to the Nb_component value.
 ** \note only the video streams are registered, no sound can be added.
 */
template<class ContainerVideo,
	 typename T,
	 std::size_t Nb_components>
class AvWriter: public ContainerWriter<ContainerVideo, T>
{
public:
  typedef ContainerVideo container_type;
  typedef T value_type;
  typedef ContainerWriter<ContainerVideo, T> base;
  
  /**
   ** \name Constructors & Destructors
   */
  /*@{*/
  
  /*!
  ** Default constructor
  */
  AvWriter():
    base(),
    pFormatCtx(NULL),
    video_stream_idx_(-1),
    pictureIN(NULL),
    pictureOUT(NULL),
    pSwsCtxOUT(NULL),
    codec_id_(AV_CODEC_ID_NONE),
    dimension_optimize_(0),
    bit_rate_(0),
    output_video_width_(0),
    output_video_height_(0),
    output_video_nbframes_(0),
    time_base_((AVRational){0,1}),
    input_pxfmt_(AV_PIX_FMT_GRAY8),
    output_pxfmt_(AV_PIX_FMT_RGB24),
    row_stride(0),
    finished(0),
    total_nb_slice(0),
    initialized(false)
  {}

  /*!
  ** Constructor with parameters.
  ** \param output_filename is the name of the output file
  ** where the data of the container will be saved
  ** \param output_video_width the total video width
  ** \param output_video_height the total video height
  ** \param output_video_nbframes the total video number of video frames
  ** \param bit_rate = average bit rate of the video frame.
  ** \param time_base time base of the video stream = 1/frame_rate. AVRationnal is a structure
  ** used for writing a rational number. AvRational r{1,5} = 1/5.
  ** \param codec_id is the id of the codec according to the CodecID enum defined in avcodec.h.
  ** By default it is guessed from the output filename extension if possible.
  ** \param dimension_optimize true if output frame dimensions should be modified so that they will result
  ** in a memory buffer that is acceptable for the codec (frames will be rescaled). Be careful, some codec
  ** can operate with any kind of dimension. For exemple, every codec that are using Y'UV pixel format
  ** require dimensions that are at least divisible by 2 (mpgX codecs). Default is false.
  */
  AvWriter(std::string output_filename,
	   std::size_t output_video_width,
	   std::size_t output_video_height,
	   std::size_t output_video_nbframes,
	   int bit_rate,
	   AVRational time_base,
	   bool dimension_optimize = 0,
	   AVCodecID codec_id = AV_CODEC_ID_NONE):
    base(output_filename),
    pFormatCtx(NULL),
    video_stream_idx_(-1),
    pictureIN(NULL),
    pictureOUT(NULL),
    pSwsCtxOUT(NULL),
    codec_id_(codec_id),
    dimension_optimize_(dimension_optimize),
    bit_rate_(bit_rate),
    output_video_width_(output_video_width),
    output_video_height_(output_video_height),
    output_video_nbframes_(output_video_nbframes),
    time_base_(time_base),
    input_pxfmt_(AV_PIX_FMT_GRAY8),
    output_pxfmt_(AV_PIX_FMT_RGB24),
    row_stride(0),
    finished(0),
    total_nb_slice(0),
    initialized(false)
  {
    initialize();
  }
  
  /*!
  ** Destructor
  */
  virtual ~AvWriter()
  {
    release();
  }
  
  /*@} End Constructors */

  /*!
  ** \brief initialized the writing process. Has to be called before the first write() call of a given video.
  ** \param bit_rate = average bit rate of the video frame.
  ** \param output_video_width the total video width
  ** \param output_video_height the total video height
  ** \param output_video_nbframes the total video number of video frames
  ** \param duration video stream duration
  ** \param time_base time base of the video stream = 1/frame_rate
  ** \param codec_id is the id of the codec according to the CodecID enum defined in avcodec.h.
  ** By default it is guessed from the output filename extension if possible.
  ** \param dimension_optimize true if output frame dimensions should be modified so that they will result
  ** in a memory buffer that is acceptable for the codec (frames will be rescaled). Be careful, some codec
  ** can operate with any kind of dimension. For exemple, every codec that are using Y'UV pixel format
  ** require dimensions that are at least divisible by 2 (mpgX codecs). Default is false.
  */
  void initialize(const std::size_t& output_video_width,
		  const std::size_t& output_video_height,
		  const std::size_t& output_video_nbframes,
		  const int& bit_rate,
		  const AVRational& time_base,
		  const bool& dimension_optimize = 0,
		  const AVCodecID& codec_id = AV_CODEC_ID_NONE);


  /*!
  ** \brief release the writing process. Has to be called when an video has been fully written.
  */
  void release();

  /*!
  ** \brief write function. This function write the data within the output
  ** file after the previous one. When last is set to true, the output file is closed.
  ** \param data is the video container to save. Warning, the container should be a 3d container
  ** with [][] operator, slices() rows() and cols() methods defined.
  ** \return false if the file has been completely filled.
  ** \note Whatever the type of the data in the container, a static_cast is done for getting unsigned char.
  ** Be sure that the dynamic of the data is conform with this cast.
  ** \note The input container format should be RGB24 or GRAY8 according to the Nb_component value.
  */
  int write(const container_type & data);

private:
  AVFormatContext *pFormatCtx;
  int video_stream_idx_;
  /*!Picture with pixel format = PIX_FMT_RGB24 or PIX_FMT_GRAY8*/
  AVFrame * pictureIN;
  /*!Picture with pixel format = output_pxfmt_*/
  AVFrame * pictureOUT;
  /*!used to convert input_pxfmt_-> output_pxfmt_*/
  struct SwsContext * pSwsCtxOUT;

  /*! codec id (see avcodec.h : enum AVCodecID)*/
  AVCodecID codec_id_;
  /*!true if output frame dimensions should be modified so that
   * they will result in a memory buffer that is acceptable for the codec*/
  bool dimension_optimize_;
  /*! average bit rate*/
  int bit_rate_;
  /*! Output video dimensions*/
  std::size_t output_video_width_;
  /*! Output video dimensions*/
  std::size_t output_video_height_;
  /*! Output video dimensions*/
  std::size_t output_video_nbframes_;
  /*! time base of the video stream = 1/frame_rate */
  AVRational time_base_;
  /*! input Pixel format : RGB24 or GRAY8 only*/
  AVPixelFormat input_pxfmt_;
  /*! output Pixel format */
  AVPixelFormat output_pxfmt_;
  /*! physical row width in image buffer */
  int row_stride;
  /*! flag indicating the end of the writing process */
  bool finished;
  /*! slices written counter*/
  std::size_t total_nb_slice;
  /*! bool indicates that initialization has been done */
  bool initialized;


  void initialize();
  void alloc_picture(AVFrame *&picture,
		     AVPixelFormat pix_fmt,
		     int width, int height);
  void add_video_stream();
  void write_video_frame(const container_type & data,
			 std::size_t nb_slice);
};

} /*! namespace slip */

namespace slip
{

template<class ContainerVideo,
	 typename T,
	 std::size_t Nb_components>
inline
void
AvWriter<ContainerVideo,T,Nb_components>::initialize(const std::size_t& output_video_width,
						     const std::size_t& output_video_height,
						     const std::size_t& output_video_nbframes,
						     const int& bit_rate,
						     const AVRational& time_base,
						     const bool& dimension_optimize,
						     const AVCodecID& codec_id)
{
  bit_rate_ = bit_rate;
  output_video_width_ = output_video_width;
  output_video_height_ = output_video_height;
  output_video_nbframes_ = output_video_nbframes;
  time_base_ = time_base;
  codec_id_ = codec_id;
  dimension_optimize_ = dimension_optimize;
  initialize();
}
  
template<class ContainerVideo,
	 typename T,
	 std::size_t Nb_components>
inline
void
AvWriter<ContainerVideo,T,Nb_components>::alloc_picture(AVFrame * &picture,
							AVPixelFormat pix_fmt,
							int width,
							int height)
{
  //AVFrame *picture;
  uint8_t *picture_buf;
  int size;
  
  //picture = avcodec_alloc_frame();
  picture = av_frame_alloc();
  if (picture)
    {
      size = avpicture_get_size(pix_fmt, width, height);
      picture_buf = reinterpret_cast<uint8_t*>(av_malloc(size));
      if (!picture_buf)
	{
	  av_free(picture);
	}
      else
	{
	  avpicture_fill((AVPicture *)picture,
			 picture_buf,
			 pix_fmt,
			 width,
			 height);
	}
    }
}



template<class ContainerVideo,
	 typename T,
	 std::size_t Nb_components>
inline
void AvWriter<ContainerVideo,T,Nb_components>::add_video_stream()
{
  AVStream * video_st = NULL;
  /* find the video encoder */
  AVCodec * codec = avcodec_find_encoder(codec_id_);
  if (!codec)
    {
      std::ostringstream err;
      err << FILE_WRITE_ERROR << this->output_filename_ << " | Codec not found.";
      slip::slip_exception exc(std::string("slip"), std::string("AvWriter::add_video_stream()"), err.str());
      throw (exc);
    }

  const AVPixelFormat * possible_pix_fmt = codec->pix_fmts;
  if (possible_pix_fmt)
    {
      int loss;
#ifndef FF_API_FIND_BEST_PIX_FMT
      int64_t pix_fmt_mask = (1 << *possible_pix_fmt);
      possible_pix_fmt++;
      while(*possible_pix_fmt != -1)
	{
	  pix_fmt_mask = pix_fmt_mask || (1 << *possible_pix_fmt);
	  possible_pix_fmt++;
	}
      output_pxfmt_ = avcodec_find_best_pix_fmt(pix_fmt_mask,
						input_pxfmt_,
						0,
						&loss);
#else
      output_pxfmt_ = avcodec_find_best_pix_fmt2(const_cast<PixelFormat * >(possible_pix_fmt),input_pxfmt_,0,&loss);
#endif

    }
  else
    {
      std::cerr << "Warning -> pixel format unknown for this codec, "
		<< "PIX_FMT_RGB24 has been chosen by default." << std::endl;
    }

  video_st = avformat_new_stream(pFormatCtx, codec);
  if (!video_st)
    {
      std::ostringstream err;
      err << FILE_WRITE_ERROR << this->output_filename_ << " | Could not alloc stream.";
      slip::slip_exception exc(std::string("slip"), std::string("AvWriter::add_video_stream()"), err.str());
      throw (exc);
    }

  /* frames per second */
  video_st->codec->time_base = time_base_;
  video_st->time_base = time_base_;
  video_st->codec->gop_size = time_base_.den; /* =frame rate */
  video_st->codec->pix_fmt = output_pxfmt_;
  if (video_st->codec->codec_id == AV_CODEC_ID_MPEG2VIDEO)
    {
      /* just for testing, we also add B frames */
      video_st->codec->max_b_frames = 2;
    }
  if (video_st->codec->codec_id == AV_CODEC_ID_MPEG1VIDEO)
    {
      /* Needed to avoid using macroblocks in which some coeffs overflow.
       * This does not happen with normal video, it just happens here as
       * the motion of the chroma plane does not match the luma plane. */
      video_st->codec->mb_decision = 2;
    }

  int width = static_cast<int>(output_video_width_);
  int height = static_cast<int>(output_video_height_);
  //Resize if needed
  if(dimension_optimize_)
    {
      std::cerr << "Old dimensions = (" << width << ", " << height << ")\n";
      int linesize_align[AV_NUM_DATA_POINTERS];
      avcodec_align_dimensions2(video_st->codec,
				&width,&height,
				linesize_align);
      std::cerr << "New dimensions = (" << width << ", " << height << ")\n";
    }
  video_st->codec->codec_type = AVMEDIA_TYPE_VIDEO;
  video_st->codec->bit_rate = bit_rate_;
  video_st->codec->width = width;
  video_st->codec->height = height;

  /* Some formats want stream headers to be separate. */
  if (pFormatCtx->oformat->flags & AVFMT_GLOBALHEADER)
    {
      video_st->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    }

  video_stream_idx_ = video_st->index;

  // open the video codec
  if (avcodec_open2(video_st->codec, codec, NULL) < 0)
    {
      std::ostringstream err;
      err << FILE_WRITE_ERROR << this->output_filename_ << " | Can not open codec.";
      slip::slip_exception exc(std::string("slip"), std::string("AvWritter::add_video_stream()"), err.str());
      throw (exc);
    }
  
  /* Allocate all pictures. */
  //pictureIN
  alloc_picture(pictureIN, input_pxfmt_,
		output_video_width_, output_video_height_);
  if (!pictureIN)
    {
      std::ostringstream err;
      err << FILE_WRITE_ERROR << this->output_filename_ << " | Could not allocate input picture buffer.";
      slip::slip_exception exc(std::string("slip"), std::string("AvWritter::add_video_stream()"), err.str());
      throw (exc);
    }
  //pictureOUT =
  alloc_picture(pictureOUT, video_st->codec->pix_fmt,
		video_st->codec->width, video_st->codec->height);
  if (!pictureOUT)
    {
      std::ostringstream err;
      err << FILE_WRITE_ERROR << this->output_filename_ << " | Could not allocate output picture buffer.";
      slip::slip_exception exc(std::string("slip"), std::string("AvWritter::add_video_stream()"), err.str());
      throw (exc);
    }
}

template<class ContainerVideo,
	 typename T,
	 std::size_t Nb_components>
inline
void AvWriter<ContainerVideo,T,Nb_components>::initialize()
{
  if(!initialized)
    {
      if (Nb_components != 1 && Nb_components != 3)
	{
	  std::ostringstream err;
	  err << FILE_WRITE_ERROR << this->output_filename_ << " | Nb_components should be 1 or 3.";
	  slip::slip_exception exc(std::string("slip"),
				   std::string("AvWriter::initialize()"),
				   err.str());
	  throw (exc);
	}
      if (Nb_components == 3)
	{
	  input_pxfmt_ = AV_PIX_FMT_RGB24;
	}
      
      //avcodec_register_all();//deprecated
      //av_register_all();//deprecated
      //avformat_network_init();
      
      /* Autodetect the output format from the name. default is MPEG. */
      AVOutputFormat *fmt = av_guess_format(NULL,
					    this->output_filename_.c_str(),
					    NULL);
      if (!fmt)
	{
	  std::cerr << "Could not deduce output format from file extension: using MPEG." << std::endl;
	  fmt = av_guess_format("mpeg", NULL, NULL);
	}
      if (!fmt)
	{
	  std::ostringstream err;
	  err << FILE_WRITE_ERROR << this->output_filename_ << " | Could not find suitable output format.";
	  slip::slip_exception exc(std::string("slip"), std::string("AvWriter::initialize()"), err.str());
	  throw (exc);
	}
      /*!
       * \TODO is it possible to deduce the AVOutputFormat from a codec?
       */
      if(codec_id_ != fmt->video_codec && codec_id_ != AV_CODEC_ID_NONE)
	{
	  std::cerr << "The specified codec id does not match the file extension." << std::endl;
	}
      codec_id_ = fmt->video_codec;
      /* Allocate the output media context. */
      pFormatCtx = avformat_alloc_context();
      if (!pFormatCtx)
	{
	  std::ostringstream err;
	  err << FILE_WRITE_ERROR << this->output_filename_ << " | Memory error.";
	  slip::slip_exception exc(std::string("slip"), std::string("AvWriter::initialize()"), err.str());
	  throw (exc);
	}
      pFormatCtx->oformat = fmt;
      strcpy(pFormatCtx->url, this->output_filename_.c_str());

      /* Add the video stream using the default format codecs
       * and initialize the codecs. */
      //CHECK -> is this if condition useful?
      if (pFormatCtx->oformat->video_codec != AV_CODEC_ID_NONE)
	{
	  add_video_stream();
	}

      av_dump_format(pFormatCtx, 0, this->output_filename_.c_str(), 1);
      /* open the output file*/
      if (!(pFormatCtx->oformat->flags & AVFMT_NOFILE))
	{
	  //CHECK -> URL_WRONLY instead of AVIO_FLAG_WRITE
	  if (avio_open(&pFormatCtx->pb,
			this->output_filename_.c_str(),
			AVIO_FLAG_WRITE) < 0)
	    {
	      std::ostringstream err;
	      err << FILE_OPEN_ERROR << this->output_filename_;
	      slip::slip_exception exc(std::string("slip"), std::string("AvWriter::initialize()"), err.str());
	      throw (exc);
	    }
	}

      /* Write the stream header, if any. */
      if (avformat_write_header(pFormatCtx, NULL) < 0)
	{
	  std::ostringstream err;
	  err << FILE_WRITE_ERROR << this->output_filename_ << " | Cannot write a file header.";
	  slip::slip_exception exc(std::string("slip"), std::string("AvWriter::initialize()"), err.str());
	  throw (exc);
	}
      row_stride = output_video_width_ * Nb_components;
      initialized = true;
    }
}

template<class ContainerVideo,
	 typename T,
	 std::size_t Nb_components>
inline
void AvWriter<ContainerVideo,T,Nb_components>::write_video_frame(const container_type & data,
								 std::size_t nb_slice)
{
  int ret = 1;
  AVStream * video_st = pFormatCtx->streams[video_stream_idx_];
  AVCodecContext * pCodecCtx = video_st->codec;
  //CHECK-> replace SWS_FAST_BILINEAR by SWS_BICUBIC
  if (Nb_components == 1)
    {
      pSwsCtxOUT = sws_getContext(data.cols(),data.rows(),
				  AV_PIX_FMT_GRAY8,
				  pCodecCtx->width,pCodecCtx->height,
				  pCodecCtx->pix_fmt,SWS_FAST_BILINEAR,
				  NULL, NULL, NULL);
    }
  else
    {
      pSwsCtxOUT = sws_getContext(data.cols(),data.rows(),
				  AV_PIX_FMT_RGB24,
				  pCodecCtx->width,pCodecCtx->height,
				  pCodecCtx->pix_fmt,SWS_FAST_BILINEAR,
				  NULL, NULL, NULL);
    }
  if (pSwsCtxOUT == NULL)
    {
      std::ostringstream err;
      err << FILE_WRITE_ERROR << this->output_filename_ << " | Cannot initialize the YUV conversion context.";
      slip::slip_exception exc(std::string("slip"),
			       std::string("AvWriter::write_video_frame()"),
			       err.str());
      throw (exc);
    }

  if (total_nb_slice >= output_video_nbframes_)
    {
      /* No more frames to compress. The codec has a latency of a few
       * frames if using B-frames, so we get the last frames by
       * passing the same picture again. */
    }
  else
    {
      for(std::size_t y=0; y < data.rows(); ++y)
	{
	  const T * data_ptr_i = reinterpret_cast<const T *>(data[nb_slice][y]);
	  std::transform(data_ptr_i, data_ptr_i + row_stride,
			 pictureIN->data[0]+y*pictureIN->linesize[0],
			 av_static_cast_func<unsigned char>());
	}
      //Scale into pictureOUT
      sws_scale(pSwsCtxOUT, pictureIN->data, pictureIN->linesize,
		0, data.rows(), pictureOUT->data,
		pictureOUT->linesize);
    }

  pictureOUT->pts = //static_cast<int64_t>((1000.0 * av_q2d(time_base_)) * 90.0 *
    static_cast<double>(total_nb_slice);//);
  
  if (pFormatCtx->oformat->flags & AVFMT_RAWPICTURE)
    {
      /* Raw video case - the API will change slightly in the near
       * future for that. */
      AVPacket pkt;
      av_init_packet(&pkt);
      
      pkt.flags        |= AV_PKT_FLAG_KEY;
      pkt.stream_index  = video_stream_idx_;
      pkt.data          = reinterpret_cast<uint8_t *>(pictureOUT);
      pkt.size          = sizeof(AVPicture);
      
      ret = av_write_frame(pFormatCtx, &pkt);
      av_packet_unref(&pkt);
    }
  else
    {
      AVPacket pkt = { 0 };
      av_init_packet(&pkt);
      /* encode the image */
      uint8_t * buf = NULL; 
#ifndef FF_API_OLD_ENCODE_VIDEO
      int buf_size = static_cast<int>( 9 * video_st->codec->width * video_st->codec->height * Nb_components + 10000);
      //std::cerr << "buf_size = " << buf_size << "\n";
      buf = new uint8_t[buf_size];
      int sizeout = avcodec_encode_video(pCodecCtx, buf, buf_size, pictureOUT);
      if(sizeout > 0)
	{
	  pkt.data = buf;
	  pkt.size = sizeout;
	  if (pCodecCtx->coded_frame->pts != static_cast<int64_t>(AV_NOPTS_VALUE))
	    {
	      pkt.pts = av_rescale_q(pCodecCtx->coded_frame->pts,
				     pCodecCtx->time_base,
				     video_st->time_base);
	    }
	  if(pCodecCtx->coded_frame->key_frame)
	    {
	      pkt.flags |= AV_PKT_FLAG_KEY;
	    }
	  pkt.stream_index = video_st->index;
	  /* Write the compressed frame to the media file. */
	  ret = av_write_frame(pFormatCtx, &pkt);
	}
      else
	{
	  ret = 0;
	}
#else
      int got_packet;
      ret = avcodec_encode_video2(pCodecCtx, &pkt, pictureOUT, &got_packet);
      if(!ret && got_packet && pkt.size)
	{
	  if (pkt.pts != static_cast<int64_t>(AV_NOPTS_VALUE))
	    {
	      pkt.pts = av_rescale_q(pkt.pts,
				     pCodecCtx->time_base, video_st->time_base);
	    }
	  if (pkt.dts != static_cast<int64_t>(AV_NOPTS_VALUE))
	    {
	      pkt.dts = av_rescale_q(pkt.dts,
				     pCodecCtx->time_base, video_st->time_base);
	    }

	  pkt.stream_index = video_st->index;
	  /* Write the compressed frame to the media file. */
	  ret = av_write_frame(pFormatCtx, &pkt);
	}
      else
	{
	  ret = 0;
	}
#endif
      av_packet_unref(&pkt);
      if(buf)
	{
	  delete[] buf;
	}
    }
  if (ret != 0)
    {
      std::ostringstream err;
      err << FILE_WRITE_ERROR << this->output_filename_ << " | Error while writing frame.";
      slip::slip_exception exc(std::string("slip"), std::string("AvWriter::write()"), err.str());
      throw (exc);
      exit(1);
    }
  total_nb_slice++;
}


template<class ContainerVideo,
	 typename T,
	 std::size_t Nb_components>
inline
int AvWriter<ContainerVideo,T,Nb_components>::write(const container_type & data){
  if(!initialized)
    {
      std::ostringstream err;
      err << FILE_WRITE_ERROR << this->output_filename_ << " | The writer to be initialized before writing.";
      slip::slip_exception exc(std::string("slip"), std::string("AvWriter::write()"), err.str());
      throw (exc);
    }
  if (finished)
    {
      return 0;
    }

  std::size_t nb_slices = data.slices();
  if(output_video_nbframes_ < (total_nb_slice + nb_slices))
    {
      std::cerr << "output_video_nbframes_  = " <<  output_video_nbframes_ << std::endl;
      std::cerr << "total_nb_slice  = " << total_nb_slice << std::endl;
      std::cerr << "nb_slices  = " << nb_slices << std::endl;
      std::ostringstream err;
      err << FILE_WRITE_ERROR << this->output_filename_ << " | Allocation problem during writing.";
      slip::slip_exception exc(std::string("slip"), std::string("AvWriter::write()"), err.str());
      throw (exc);
    }

  /* encode the video */
  for(std::size_t i = 0; i < nb_slices; ++i)
    {
      write_video_frame(data,i);
    }

  if (total_nb_slice == output_video_nbframes_)
    {
      if(pFormatCtx->streams[video_stream_idx_])
	{
	  double time_base_dble = static_cast<double>(pFormatCtx->streams[video_stream_idx_]->time_base.num) /
	    static_cast<double>(pFormatCtx->streams[video_stream_idx_]->time_base.den);
	  double video_pts = static_cast<double>(pFormatCtx->streams[video_stream_idx_]->pts.val) * time_base_dble;
	  double duration = static_cast<double>(output_video_nbframes_) * time_base_dble;
	  while(video_pts < duration)
	    {
	      write_video_frame(data,0);
	      video_pts = static_cast<double>(pFormatCtx->streams[video_stream_idx_]->pts.val) * time_base_dble ;
	    }
	}
      av_write_trailer(pFormatCtx);
      finished = true;
    }

  return (!finished);
}

template<class ContainerVideo,
	 typename T,
	 std::size_t Nb_components>
inline
void AvWriter<ContainerVideo,T,Nb_components>::release()
{
  if(initialized)
    {
      av_free(pictureIN->data[0]);
      av_free(pictureIN);
      av_free(pictureOUT->data[0]);
      av_free(pictureOUT);
      if (!(pFormatCtx->oformat->flags & AVFMT_NOFILE))
	{
	/* Close the output file. */
	avio_close(pFormatCtx->pb);
	}

      for (std::size_t i = 0; i < pFormatCtx->nb_streams; ++i)
	{
	  av_freep(&pFormatCtx->streams[i]->codec);
	  av_freep(&pFormatCtx->streams[i]);
	}

      /* free the stream */
      av_free(pFormatCtx);
      
      video_stream_idx_ = -1;
      codec_id_ = AV_CODEC_ID_NONE;
      dimension_optimize_ = false;
      bit_rate_ = 0;
      output_video_width_ = 0;
      output_video_height_ = 0;
      output_video_nbframes_ = 0;
      time_base_ = (AVRational){0,1};
      input_pxfmt_ = AV_PIX_FMT_GRAY8;
      output_pxfmt_ = AV_PIX_FMT_RGB24;
      row_stride = 0;
      finished = false;
      total_nb_slice = 0;
      initialized = false;
    }
}




} /*! namespace slip */
#endif /* AVWRITER_HPP_ */
