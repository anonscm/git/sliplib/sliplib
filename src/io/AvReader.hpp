/*!
 ** \file AvReader.hpp
 ** \brief containers reader from video files (according to libav capability)
 ** It includes avcodec.h, avformat.h, swscale.h and pixfmt.h from libav
 ** \version Fluex 1.0
 ** \date 2013/04/24
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef AVREADER_HPP_
#define AVREADER_HPP_

#include <iostream>
#include <sstream>

extern "C"
{
#include <libswscale/swscale.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/pixfmt.h>
}

#include "ContainerReader.hpp"
#include "error.hpp"

namespace slip
{

/*!
 ** @ingroup IO
 ** \version Fluex 1.0
 ** \date 2013/04/24
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief %AvReader, inherited from %ContainerReader, is a reader for videos.
 ** \param VideoContainer should be a 3d Container with a component number equal to Nb_components.
 ** The [][] operator and the slices(), the rows() and the cols() methods should have been defined.
 ** \param T the type of the container data.
 ** \param Nb_components the number of frames components (3 for a color video or 1 for a gray scale video).
 ** \param Nb_block is the number of containers to create (>1 if one want to split the input
 ** data into different containers, default is 1). Notice that the video data is always cut according to the
 ** time dimension which must be the first dimension of the input container (or slices dimension). In other words
 ** each container will contain a set of frames which size depends on the Nb_block.
 ** \pre VideoContainer should have Nb_components components.
 ** \pre Nb_components should be 1 or 3.
 ** \par Axis conventions (dim1 = slices, dim2 = rows, dim3 = cols):
 ** \image html video_axis_conventions.jpg "axis and notation conventions"
 ** \image latex video_axis_conventions.eps "axis and notation conventions" width=5cm
 */
template<class VideoContainer,
	 typename T,
	 std::size_t Nb_components,
	 std::size_t Nb_block>
class AvReader: public ContainerReader<VideoContainer,T,Nb_block>
{
public:
  typedef VideoContainer container_type;
  typedef T value_type;
  typedef ContainerReader<VideoContainer,T,Nb_block> base;

  /**
   ** \name Constructors & Destructors
   */
  /*@{*/

  /*!
  ** Default constructor
  */
  AvReader()
    :base(),
     pFormatCtx(NULL),
     pCodecCtx(NULL),
     pCodec(NULL),
     pFrame(NULL),
     pFrameScaled(NULL),
     numBytes(0),
     buffer(NULL),
     packet(),
     next_frame_loaded(0),
     videoStream(-1),
     colorspace(AV_PIX_FMT_GRAY8),
     remained_frames(0),
     read_frames(0),
     nbframes_is_approximated(0),
     block_ind(1),
     block_size(0),
     last_block_size(0),
     last_it(0),
     finished(0),
     initialized(0)
  {}

  /*!
  ** Constructor with parameters.
  ** \param data_filename is the name of the file containing the data to read
  ** \note this constructor automatically call the initialize() method.
  */
  AvReader(std::string data_filename):
    base(data_filename),
    pFormatCtx(NULL),
    pCodecCtx(NULL),
    pCodec(NULL),
    pFrame(NULL),
    pFrameScaled(NULL),
    numBytes(0),
    buffer(NULL),
    packet(),
    next_frame_loaded(0),
    videoStream(-1),
    colorspace(AV_PIX_FMT_GRAY8),
    remained_frames(0),
    read_frames(0),
    nbframes_is_approximated(0),
    block_ind(1),
    block_size(0),
    last_block_size(0),
    last_it(0),
    finished(0),
    initialized(0)
  {
    initialize();
  }

  /*!
  ** Destructor
  ** \note the release() method is called.
  */
  virtual ~AvReader()
  {
    if(initialized)
      {
	release();
      }
  }
  /*@} End Constructors */

  /*!
  ** \brief initialized the reading process.
  ** \note an initialization is called by the %AvReader(std::string) constructor.
  */
  void initialize();

  /*!
  ** \brief release the reading process.
  ** \note this method is called by the destructor.
  */
  void release();

  /*!
  ** \brief virtual read function. If Nb_block is more than one,
  ** it reads only one container, and returns 0. Else it reads the
  ** container and returns 1. If the file data_filename
  ** has been completely read (no more block to read), it returns 1.
  ** \param in is the container to fill. Warning, the container should be a video container
  ** with [] operator, resize(), slices(), width(), height() methods defined.
  ** \return true if there is still data to read, false if the file has been completely read.
  */
  int read(VideoContainer & in);


  /**
   ** \name Getters
   */
  /*@{*/

  /*!
   * \brief get the codec pointer
   * \return the AVCodec * of the file
   */
  AVCodec *  get_codec() const
  {
    if(pCodec != NULL)
      {
	return pCodec;
      }
    else
      {
	return NULL;
      }
  }

  /*!
   * \brief get the codec id (see avcodec.h : enum AVCodecID)
   * \return CODEC_ID_NONE if no codec has been defined.
   */
  AVCodecID get_codec_id() const
  {
    if(pCodec != NULL)
      {
	return pCodec->id;
      }
    else
      {
	return AV_CODEC_ID_NONE;
      }
  }

  /*!
   * \brief get the number of read frames
   */
  int get_read_frames() const
  {
    return read_frames;
  }

  /*!
   * \brief get the total number of frames
   */
  int get_nb_frames() const
  {
    return (read_frames + remained_frames);
  }

  /*!
   * \brief get the the average bit rate of the codec
   * \return 0 if no codec has been read
   */
  int get_bit_rate() const
  {
    if(pCodecCtx != NULL)
      {
	return pCodecCtx->bit_rate;
      }
    else
      {
	return 0;
      }
  }

  /*!
   * \brief get the width of a decoded frame picture
   * \return 0 if no codec has been read
   */
  std::size_t get_frame_width() const
  {
    if(pCodecCtx != NULL)
      {
	return static_cast<std::size_t>(pCodecCtx->width);
      }
    else
      {
	return 0;
      }
  }

  /*!
   * \brief get the height of a decoded frame picture
   * \return 0 if no codec has been read
   */
  std::size_t get_frame_height() const
  {
    if(pCodecCtx != NULL)
      {
	return static_cast<std::size_t>(pCodecCtx->height);
      }
    else
      {
	return 0;
      }
  }

  /*!
   * \brief get the time base of the video stream (1/framerate)
   * \return 1/0 if no codec has been read
   */
  AVRational get_time_base() const
  {
    if(pCodecCtx != NULL)
      {
	return pCodecCtx->time_base;
      }
    else
      {
	return (AVRational){0,1};
      }
  }

  /*!
   * \brief get the duration of the video stream
   * \return 0 if not defined
   */
  int64_t get_duration() const
  {
    if(pFormatCtx != NULL)
      {
	return pFormatCtx->duration;
      }
    else
      {
	return 0;
      }
  }

  /*@} End Getters */

private:
  AVFormatContext *pFormatCtx;
  AVCodecContext  *pCodecCtx;
  AVCodec         *pCodec;
  AVFrame         *pFrame;
  AVFrame         *pFrameScaled;
  /*!size of a picture in bytes (3 bytes = 24bits)*/
  int             numBytes;
  uint8_t         *buffer;
  AVPacket 		packet;
  bool 			next_frame_loaded;
  /*!id of the video stream (the first stream in the file)*/
  int 			videoStream;
  AVPixelFormat   colorspace;

  /*!Number of remaining frames to read in the file */
  std::size_t remained_frames;
  std::size_t read_frames;
  bool nbframes_is_approximated;
  /*!current read block indice*/
  int block_ind;
  /*!block size*/
  int block_size;
  /*!last block size*/
  int last_block_size;
  /*!indicates that the current read is the last.*/
  bool last_it;
  /*!indicates that the last read has been done and no more data are available.*/
  bool finished;
  /*!indicates that the reader has been initialized*/
  bool initialized;
};

} /*! namespace slip */


namespace slip
{

template<class VideoContainer,
	 typename T,
	 std::size_t Nb_components,
	 std::size_t Nb_block>
inline
void AvReader<VideoContainer, T, Nb_components, Nb_block>::initialize()
{
  if (!initialized)
    {
      if (Nb_components != 1 && Nb_components != 3)
	{
	  std::ostringstream err;
	  err << FILE_READ_ERROR << this->data_filename_ << " | Nb_components should be 1 or 3.";
	  slip::slip_exception exc(std::string("slip"), std::string("AvReader::initialize()"), err.str());
	  throw (exc);
	}
      if (Nb_components == 3)
	{
	  colorspace = AV_PIX_FMT_RGB24;
	}
      // AVFormatContext holds the header information from the format (Container)
      // Allocating memory for this component
      
      // Register all formats and codecs
      //av_register_all();//deprecated since 2018
      pFormatCtx = avformat_alloc_context();
      if(!pFormatCtx)
	{
	  std::ostringstream err;
	  err << FILE_OPEN_ERROR << this->data_filename_<<" | Couldn't alloc AVFormatContex";
	  slip::slip_exception exc(std::string("slip"), std::string("AvReader::initialize()"), err.str());
	  throw (exc);
	}

      // Open video file
      if(avformat_open_input(&pFormatCtx, this->data_filename_.c_str(), NULL, NULL)!=0)
	{
	  std::ostringstream err;
	  err << FILE_OPEN_ERROR << this->data_filename_;
	  slip::slip_exception exc(std::string("slip"), std::string("AvReader::initialize()"), err.str());
	  throw (exc);
	}

      // Retrieve stream information
      if(avformat_find_stream_info(pFormatCtx,NULL) < 0)
	{
	  std::ostringstream err;
	  err << FILE_READ_ERROR << this->data_filename_ << " | Couldn't find stream information.";
	  slip::slip_exception exc(std::string("slip"), std::string("AvReader::initialize()"), err.str());
	  throw (exc);
	}

      // Dump information about file onto standard error
      av_dump_format(pFormatCtx,0,this->data_filename_.c_str(),false);

      // Find the first video stream
      for(unsigned int i=0; i < pFormatCtx->nb_streams; ++i)
	{
	  if(pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
	    {
	      videoStream = i;
	      break;
	    }
	}
      if(videoStream == -1)
	{
	  std::ostringstream err;
	  err << FILE_READ_ERROR << this->data_filename_ << " | Didn't find a video stream.";
	  slip::slip_exception exc(std::string("slip"), std::string("AvReader::initialize()"), err.str());
	  throw (exc);
	}

      // Get a pointer to the codec context for the video stream
      pCodecCtx = pFormatCtx->streams[videoStream]->codecpar;

      // Find the decoder for the video stream
      pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
      if(pCodec==NULL)
	{
	  std::ostringstream err;
	  err << FILE_READ_ERROR << this->data_filename_ << " |  Codec not found.";
	  slip::slip_exception exc(std::string("slip"), std::string("AvReader::initialize()"), err.str());
	  throw (exc);
	}


      // Open codec
      if(avcodec_open2(pCodecCtx, pCodec, NULL) < 0)
	{
	  std::ostringstream err;
	  err << FILE_READ_ERROR << this->data_filename_ << " |  Could not open codec.";
	  slip::slip_exception exc(std::string("slip"), std::string("AvReader::initialize()"), err.str());
	  throw (exc);
	}

      // Hack to correct wrong frame rates that seem to be generated by some
      // codecs
      //		if(pCodecCtx->frame_rate>1000 && pCodecCtx->frame_rate_base==1)
      //			pCodecCtx->frame_rate_base=1000;

      // Allocate video frame
      //pFrame=avcodec_alloc_frame();
      pFrame = av_frame_alloc();

      // Allocate an AVFrame structure
      //pFrameScaled=avcodec_alloc_frame();
      pFrameScaled = av_frame_alloc();
      
      if(pFrameScaled == NULL)
	{
	  std::ostringstream err;
	  err << FILE_READ_ERROR << this->data_filename_ << " |  Could not allocate AVFrame structure.";
	  slip::slip_exception exc(std::string("slip"), std::string("AvReader::initialize()"), err.str());
	  throw (exc);
	}

      // Determine required buffer size and allocate buffer
      
      //numBytes = avpicture_get_size(colorspace,
      //pCodecCtx->width,pCodecCtx->height); //deprecated
      //int linesize_align[AV_NUM_DATA_POINTERS]; //
      int linesize_align = 1; //16 or 32
      numBytes = av_image_get_buffer_size(colorspace,
					  pCodecCtx->width,
					  pCodecCtx->height,
					  linesize_align);
      buffer = new uint8_t[numBytes];
      // Assign appropriate parts of buffer to image planes in pFrameScaled
      avpicture_fill((AVPicture *)pFrameScaled, buffer, colorspace,
		     pCodecCtx->width, pCodecCtx->height);

      remained_frames = pFormatCtx->streams[videoStream]->nb_frames;
      if (remained_frames == 0)
	{
	  nbframes_is_approximated = true;
	  remained_frames = pFormatCtx->streams[videoStream]->codec_info_nb_frames;
	  //std::cout << "Nb Frames estimated : " << remaining_frames << "\n";
	}

      block_size = remained_frames / Nb_block;
      long int one_frame_size = pCodecCtx->width * pCodecCtx->height * Nb_components;
      double container_size = static_cast<double>(block_size) * static_cast<double>(one_frame_size);
      double limit_1_go = 1073741824.0;
      if ( container_size > limit_1_go)
	{
	  std::ostringstream err;
	  err << FILE_READ_ERROR << this->data_filename_ << " |  Containers size ";
	  err << container_size << " is greater than 1Go = " << limit_1_go;
	  slip::slip_exception exc(std::string("slip"), std::string("AvReader::initialize()"), err.str());
	  throw (exc);
	}

      last_block_size = remained_frames % Nb_block;
      last_block_size  = (last_block_size == 0 ? block_size : block_size+last_block_size);
      last_it = (Nb_block == 1);
      finished = (last_it && last_block_size == 0);
      initialized = true;
    }

}

template<class VideoContainer,
	 typename T,
	 std::size_t Nb_components,
	 std::size_t Nb_block>
inline
int AvReader<VideoContainer, T, Nb_components, Nb_block>::read(VideoContainer & in)
{
	if(!initialized)
	  {
	    std::ostringstream err;
	    err << FILE_READ_ERROR << this->data_filename_ << " | The reader needs to be initialized before reading.";
	    slip::slip_exception exc(std::string("slip"), std::string("AvReader::read()"), err.str());
	    throw (exc);
	  }
	if (finished)
	  {
	    return 0;
	  }
	if (last_it)
	  {
	    in.resize(last_block_size,pCodecCtx->height,pCodecCtx->width,T());
	  }
	else
	  {
	    in.resize(block_size,pCodecCtx->height,pCodecCtx->width,T());
	  }

	if((!nbframes_is_approximated) && (remained_frames < in.slices()))
	  {
	    std::ostringstream err;
	    err << FILE_READ_ERROR << this->data_filename_ << " | Bad image dimensions.";
	    slip::slip_exception exc(std::string("slip"), std::string("JpegReader::read()"), err.str());
	    throw (exc);
	  }

	// Read frames and save first five frames to disk
	std::size_t filled_frames=0;
	int frameFinished = 0;
	std::size_t height = static_cast<std::size_t>(pCodecCtx->height);
	std::size_t width = static_cast<std::size_t>(pCodecCtx->width);
	std::size_t row_stride = static_cast<std::size_t>(pCodecCtx->width * Nb_components);
	std::size_t in_slices = in.slices();
	if (!next_frame_loaded)
	  {
	    next_frame_loaded = (av_read_frame(pFormatCtx, &packet)>=0);
	  }
	while(next_frame_loaded && (filled_frames < in_slices))
	{
	  // Is this a packet from the video stream?
	  if(packet.stream_index==videoStream)
	    {
	      // Decode video frame
	      //if(avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished,&packet) < 0)
	      if(avcodec_send_packet(pCodecCtx, &packet) < 0)
		{
		  std::ostringstream err;
		  err << FILE_READ_ERROR << this->data_filename_ << " | The frame can not be decompressed.";
		  slip::slip_exception exc(std::string("slip"), std::string("AvReader::read()"), err.str());
		  throw (exc);
		}
	      // Did we get a video frame?
	      if(frameFinished)
		{
		  static struct SwsContext *img_convert_ctx = NULL;
		  img_convert_ctx = sws_getCachedContext(img_convert_ctx,
							 width, height,
							 pCodecCtx->pix_fmt,
							 width, height,
							 colorspace,
							 SWS_FAST_BILINEAR,
							 NULL, NULL, NULL);
		  std::size_t h = sws_scale(img_convert_ctx,
					    (const uint8_t* const*)pFrame->data,
					    pFrame->linesize,
					    0,
					    height,
					    pFrameScaled->data,
					    pFrameScaled->linesize);
		  if (h > height)
		    {
		      std::ostringstream err;
		      err << FILE_READ_ERROR << this->data_filename_ << " |  The frame size is too large.";
		      slip::slip_exception exc(std::string("slip"), std::string("AvReader::read()"), err.str());
		      throw (exc);
		    }
		  // Save the frame to the input container
		  for(std::size_t y = 0; y < height; ++y)
		    {
		      T * ptr_i = reinterpret_cast<T *>(in[filled_frames][y]);
		      std::copy(pFrameScaled->data[0]+y*pFrameScaled->linesize[0],
				pFrameScaled->data[0]+y*pFrameScaled->linesize[0] + row_stride,ptr_i);
		    }
		  filled_frames++;
		}
	    }
	  // Free the packet that was allocated by av_read_frame
	  //av_free_packet(&packet);//deprecated since 2015
	  av_packet_unref(&packet);
	  //Read the next frame
	  next_frame_loaded = (av_read_frame(pFormatCtx, &packet)>=0);
	}
	read_frames += filled_frames;
	remained_frames -= filled_frames;
	block_ind++;
	if (last_it)
	  {
	    finished = true;
	    if (nbframes_is_approximated)
	      {
		if (in.slices() > (filled_frames + 1))
		  {
		    std::size_t rs = in.slices() - filled_frames - 1;
		    std::cerr << "slip::AvReader::read | Warning the last container has not been fully filled, the "
			      << rs << " last frames remain unchanged." << std::endl;
		  }
		else if(next_frame_loaded)
		  {
		    std::cerr << "slip::AvReader::read | Warning some end frames of the video could have not been read." << std::endl;
		  }
	      }
	  }
	else
	  {
	    last_it = (block_ind == Nb_block);
	  }
	return (!finished);

}

template<class VideoContainer,
	 typename T,
	 std::size_t Nb_components,
	 std::size_t Nb_block>
inline
void AvReader<VideoContainer, T, Nb_components, Nb_block>::release()
{
  if(initialized)
    {
      
      // Free the RGB image
      delete [] buffer;
      av_free(pFrameScaled);

      // Free the YUV frame
      av_free(pFrame);
      
      // Close the codec
      avcodec_close(pCodecCtx);

      // Close the video file
      avformat_close_input(&pFormatCtx);

      pFormatCtx = NULL;
      pCodecCtx = NULL;
      pCodec = NULL;
      pFrame = NULL;
      pFrameScaled = NULL;
      numBytes = 0;
      buffer = NULL;
      next_frame_loaded = false;
      videoStream = -1;
      colorspace = AV_PIX_FMT_GRAY8;
      remained_frames = 0;
      read_frames = 0;
      nbframes_is_approximated = false;
      block_ind = 1;
      block_size = 0;
      last_block_size = 0;
      last_it = 0;
      finished = false;
      initialized = false;
    }
}

} /*! namespace slip */
#endif /* AVREADER_HPP_ */
