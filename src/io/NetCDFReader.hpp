/*!
 ** \file NetCDFReader.hpp
 ** \brief containers reader from netcdf files
 ** It includes netcdfcpp.h
 ** \version Fluex 1.0
 ** \date 2013/05/13
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \todo Testing 4D containers
 ** \todo Adding reading of multicomponents variables (this version can only read one component per variable)
 */

#ifndef NETCDFREADER_HPP_
#define NETCDFREADER_HPP_

#include "NetCDFDef.hpp"
#include "ContainerReader.hpp"
#include "utils_compilation.hpp"

namespace slip {

/*!
 * \cond Do not include the following into documentation
 */

/*!
 * \version Fluex 1.0
 * \date 2013/05/13
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_read structure used by NetCDFReader for reading the NcVar's data into a container.
 * \param Nb_components : the number of components or netcdf variables
 * \param Dim : container dimension
 */
template <std::size_t Nb_components, std::size_t Dim>
struct __nc_read_data{
	template <typename MonoContainer, typename T>
	static void apply(MonoContainer& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, long first_dim_size, const std::vector<NcDim*> & nc_dim){
		std::ostringstream err;
		err << "Nb Components or dimension is not valid.";
		slip::slip_exception exc(std::string("slip"), std::string("nc_read_data"), err.str());
		throw (exc);
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/05/13
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_read structure used by NetCDFReader for reading the NcVar's data into a container.
 * \param Nb_components : the number of components or netcdf variables = 1
 * \param Dim : container dimension = 1
 */
template <>
struct __nc_read_data<1,1>{
	template <typename MonoContainer1D, typename T>
	static void apply(MonoContainer1D& container, const std::vector<NcVar*> & nc_var,
			  long first_dim_pos, long first_dim_size, const std::vector<NcDim*> & UNUSED(nc_dim))
	{
		if (!nc_var[0]->set_cur(first_dim_pos)){
			std::ostringstream err;
			err << "Impossible to set the new corner.";
			slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_1_1"), err.str());
			throw (exc);
		}
		if (!nc_var[0]->get(container.begin(),first_dim_size))
		{
			std::ostringstream err;
			err << "1D data not found.";
			slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_1_1"), err.str());
			throw (exc);
		}
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/05/13
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_read structure used by NetCDFReader for reading the NcVar's data into a container.
 * \param Nb_components : the number of components or netcdf variables = N
 * \param Dim : container dimension = 1
 */
template <std::size_t N>
struct __nc_read_data<N,1>{
	template <typename MultiContainer1D, typename T>
	static void apply(MultiContainer1D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, long first_dim_size, const std::vector<NcDim*> & nc_dim)
	{

		slip::Vector<T> tmp(first_dim_size,T());
		for(std::size_t i = 0; i < N; ++i)
		{
			if (!nc_var[i]->set_cur(first_dim_pos)){
				std::ostringstream err;
				err << "Impossible to set the new corner of component " << i << ".";
				slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_n_1"), err.str());
				throw (exc);
			}
			if (!(nc_var[i]->get(tmp.begin(),first_dim_size)))
			{
				std::ostringstream err;
				err << "1D data not found.";
				slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_n_1"), err.str());
				throw (exc);
			}
			for(long j=0; j<first_dim_size; j++){
				container[j][i] = tmp[j];
			}
		}
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/05/13
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_read structure used by NetCDFReader for reading the NcVar's data into a container.
 * \param Nb_components : the number of components or netcdf variables = 1
 * \param Dim : container dimension = 2
 */
template <>
struct __nc_read_data<1,2>{
	template <typename MonoContainer2D, typename T>
	static void apply(MonoContainer2D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, long first_dim_size, const std::vector<NcDim*> & nc_dim)
	{
		if (!nc_var[0]->set_cur(first_dim_pos,0)){
			std::ostringstream err;
			err << "Impossible to set the new corner.";
			slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_1_2"), err.str());
			throw (exc);
		}
		if (!nc_var[0]->get(container.begin(),first_dim_size,nc_dim[1]->size()))
		{
			std::ostringstream err;
			err <<  "2D data not found.";
			slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_1_2"), err.str());
			throw (exc);
		}
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/05/13
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_read structure used by NetCDFReader for reading the NcVar's data into a container.
 * \param Nb_components : the number of components or netcdf variables = N
 * \param Dim : container dimension = 2
 */
template <std::size_t N>
struct __nc_read_data<N,2>{
	template <typename MultiContainer2D, typename T>
	static void apply(MultiContainer2D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, long first_dim_size, const std::vector<NcDim*> & nc_dim)
	{
		std::size_t dim1  = static_cast<std::size_t>(first_dim_size);
		std::size_t dim2 = static_cast<std::size_t>(nc_dim[1]->size());

		for(std::size_t i = 0; i < N; ++i)
		{
			if (!nc_var[i]->set_cur(first_dim_pos,0)){
				std::ostringstream err;
				err << "Impossible to set the new corner of component " << i << ".";
				slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_n_2"), err.str());
				throw (exc);
			}
			slip::Array2d<T> tmp(dim1,dim2);
			if (!(nc_var[i]->get(tmp.begin(),dim1,dim2)))
			{
				std::ostringstream err;
				err << "2D data not found.";
				slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_n_2"), err.str());
				throw (exc);
			}
			std::copy(tmp.begin(),tmp.end(),container.begin(i));
		}
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/05/13
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_read structure used by NetCDFReader for reading the NcVar's data into a container.
 * \param Nb_components : the number of components or netcdf variables = 1
 * \param Dim : container dimension = 3
 */
template <>
struct __nc_read_data<1,3>{
	template <typename MonoContainer3D, typename T>
	static void apply(MonoContainer3D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, long first_dim_size, const std::vector<NcDim*> & nc_dim)
	{
		if (!nc_var[0]->set_cur(first_dim_pos,0,0)){
			std::ostringstream err;
			err << "Impossible to set the new corner.";
			slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_1_3"), err.str());
			throw (exc);
		}
		if (!nc_var[0]->get(container.begin(),first_dim_size,nc_dim[1]->size(),nc_dim[2]->size()))
		{
			std::ostringstream err;
			err << "3D data not found.";
			slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_1_3"), err.str());
			throw (exc);
		}
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/05/13
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_read structure used by NetCDFReader for reading the NcVar's data into a container.
 * \param Nb_components : the number of components or netcdf variables = N
 * \param Dim : container dimension = 3
 */
template <std::size_t N>
struct __nc_read_data<N,3>{
	template <typename MultiContainer3D, typename T>
	static void apply(MultiContainer3D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, long first_dim_size, const std::vector<NcDim*> & nc_dim)
	{
		std::size_t dim1  = static_cast<std::size_t>(first_dim_size);
		std::size_t dim2 = static_cast<std::size_t>(nc_dim[1]->size());
		std::size_t dim3 = static_cast<std::size_t>(nc_dim[2]->size());

		for(std::size_t i = 0; i < N; ++i)
		{
			if (!nc_var[i]->set_cur(first_dim_pos,0,0)){
				std::ostringstream err;
				err << "Impossible to set the new corner of component " << i << ".";
				slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_n_3"), err.str());
				throw (exc);
			}
			slip::Array3d<T> tmp(dim1,dim2,dim3);
			if (!(nc_var[i]->get(tmp.begin(),dim1,dim2,dim3)))
			{
				std::ostringstream err;
				err << "3D data not found.";
				slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_n_3"), err.str());
				throw (exc);
			}
			std::copy(tmp.begin(),tmp.end(),container.begin(i));
		}
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/05/13
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_read structure used by NetCDFReader for reading the NcVar's data into a container.
 * \param Nb_components : the number of components or netcdf variables = 1
 * \param Dim : container dimension = 4
 */
template <>
struct __nc_read_data<1,4>{
	template <typename MonoContainer4D, typename T>
	static void apply(MonoContainer4D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, long first_dim_size, const std::vector<NcDim*> & nc_dim)
	{
		if (!nc_var[0]->set_cur(first_dim_pos,0,0,0)){
			std::ostringstream err;
			err << "Impossible to set the new corner.";
			slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_1_4"), err.str());
			throw (exc);
		}
		if (!nc_var[0]->get(container.begin(),first_dim_size,nc_dim[1]->size(),nc_dim[2]->size(),nc_dim[3]->size()))
		{
			std::ostringstream err;
			err << "4D data not found.";
			slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_1_4"), err.str());
			throw (exc);
		}
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/05/13
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief nc_read structure used by NetCDFReader for reading the NcVar's data into a container.
 * \param Nb_components : the number of components or netcdf variables = N
 * \param Dim : container dimension = 4
 */
template <std::size_t N>
struct __nc_read_data<N,4>{
	template <typename MultiContainer4D, typename T>
	static void apply(MultiContainer4D& container, const std::vector<NcVar*> & nc_var,
			long first_dim_pos, long first_dim_size, const std::vector<NcDim*> & nc_dim)
	{
		std::size_t dim1  = static_cast<std::size_t>(first_dim_size);
		std::size_t dim2 = static_cast<std::size_t>(nc_dim[1]->size());
		std::size_t dim3 = static_cast<std::size_t>(nc_dim[2]->size());
		std::size_t dim4 = static_cast<std::size_t>(nc_dim[3]->size());

		for(std::size_t i = 0; i < N; ++i)
		{
			if (!nc_var[i]->set_cur(first_dim_pos,0,0,0)){
				std::ostringstream err;
				err << "Impossible to set the new corner of component " << i << ".";
				slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_n_4"), err.str());
				throw (exc);
			}
			slip::Array4d<T> tmp(dim1,dim2,dim3,dim4);
			if (!(nc_var[i]->get(tmp.begin(),dim1,dim2,dim3,dim4)))
			{
				std::ostringstream err;
				err << "4D data not found.";
				slip::slip_exception exc(std::string("slip"), std::string("nc_read_data_n_n4"), err.str());
				throw (exc);
			}
			std::copy(tmp.begin(),tmp.end(),container.begin(i));
		}
	}
};

/*!
 * \endcond
 */

/*!
 ** @ingroup IO
 ** \version Fluex 1.0
 ** \date 2013/05/13
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief %NetCDFReader, inherited from %ContainerReader, is a reader of netcdf files.
 ** \param Container should be a Container with a component number equal to Nb_components and with Dim dimensions.
 ** \param T the type of the container data.
 ** \param Nb_components the number of components per point (ex : 3 for a color container or 1 for a gray scale container).
 ** \param Nb_block is the number of containers to create (>1 if one want to split the input
 ** data into different containers, default is 1). Notice that the data is always cut according to the
 ** first dimension of the input container.
 ** \param Dim is the dimension of the container.
 ** \pre Container should have Nb_components components with components iterators defined
 ** \pre The netcdf file should have defined a variable for each component.
 ** \note As multi-component 1D container does not exist, such signals should be stored in a vector<vector<T> >
 ** \todo some changes has to be done for including 4D containers.
 ** \note see : http://www.unidata.ucar.edu/software/netcdf/docs/netcdf-cxx/ for NetCDF API help
 */
template<class Container, typename T, std::size_t Nb_components, std::size_t Nb_block, std::size_t Dim>
class NetCDFReader: public ContainerReader<Container,T,Nb_block>{
public:
	typedef Container container_type;
	typedef T value_type;
	typedef ContainerReader<Container,T,Nb_block> base;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** Default constructor
	 */
	NetCDFReader()
	:base(),dataFile(0),var_names_(),nc_var_(),nc_dim_(),block_ind(1),block_size(0),last_block_size(0),
	 last_it(0),finished(0),initialized(0)
	{}

	/*!
	 ** Constructor with parameters.
	 ** \param data_filename is the name of the file containing the data to read
	 ** \param var_names vector of string which contains the name of the variables (features).
	 ** If this vector is empty, variables with the good dimensions will be automatically added.
	 ** This can reach exception if less than Nb_Components variables with the dimension Dim are found.
	 ** \note this constructor automatically call the initialize() method.
	 */
	NetCDFReader(std::string data_filename, const std::vector<std::string> & var_names)
	:base(data_filename),dataFile(0),var_names_(var_names),nc_var_(),nc_dim_(),block_ind(1),block_size(0),
	 last_block_size(0),last_it(0),finished(0),initialized(0)
	{ initialize();}

	/*!
	 ** Destructor
	 ** \note the release() method is called.
	 */
	virtual ~NetCDFReader(){
		if(initialized){
			release();
		}
	}
	/*@} End Constructors */

	/*!
	 ** \brief initialized the reading process.
	 ** \param var_names vector of string which contains the name of the variables (features)
	 ** If this vector is empty, variables with the good dimensions will be automatically added.
	 ** This can reach exception if less than Nb_Components variables with the dimension Dim are found.
	 ** \note an initialization is called by the %NetCDFReader(std::string, const std::vector<std::string> &)
	 ** constructor.
	 */
	void initialize(const std::vector<std::string> & var_names);

	/*!
	 ** \brief release the reading process.
	 ** \note this method is called by the destructor.
	 */
	void release();

	/*!
	 ** \brief virtual read function. If Nb_block is more than one,
	 ** it reads only one container, and returns 0. Else it reads the
	 ** container and returns 1. If the file data_filename
	 ** has been completely read (no more block to read), it returns 1.
	 ** \param in is the container to fill.
	 ** \return true if there is still data to read, false if the file has been completely read.
	 */
	int read(Container & in);

private:

	/*!netCDF files, providing methods for netCDF file operations.*/
	NcFile * dataFile;
	/*!vector of string which contains the name of the variables (features)*/
	std::vector<std::string> var_names_;
	/*!vector containing the NcVar adresses (NetCDF objects) */
	std::vector<NcVar*> nc_var_;
	/*!vector containing the NCDim of the NcVar adresses (NetCDF objects) */
	std::vector<NcDim*> nc_dim_;


	/*!current read block indice*/
	int block_ind;
	/*!block size*/
	int block_size;
	/*!last block size*/
	int last_block_size;
	/*!indicates that the current read is the last.*/
	bool last_it;
	/*!indicates that the last read has been done and no more data are available.*/
	bool finished;
	/*!indicates that the reader has been initialized*/
	bool initialized;

	void find_var();
	void get_var();
	void get_dim(NcVar* Var);

	/*!
	 ** \brief initialized the reading process.
	 */
	void initialize();

};

} /*! namespace slip */

namespace slip {


template<class Container, typename T, std::size_t Nb_components, std::size_t Nb_block, std::size_t Dim>
inline
void NetCDFReader<Container, T, Nb_components, Nb_block, Dim>::find_var(){
	var_names_.clear();
	int num_vars = dataFile->num_vars();
	//	std::cerr << "Nb var = " << num_vars << "\n";
	for(int i=0; i< num_vars; ++i){
		//		std::cerr << "Var #" << i << " = " << dataFile->get_var(i)->name() << "\n";
		//		std::cerr << "dims = " << dataFile->get_var(i)->num_dims() << "\n";
		//		std::cerr << "vals = " << dataFile->get_var(i)->num_vals() << "\n";
		//		std::cerr << "atts = " << dataFile->get_var(i)->num_atts() << "\n";
		if(dataFile->get_var(i)->num_dims() == Dim){
			var_names_.push_back(std::string(dataFile->get_var(i)->name()));
		}
	}
}


template<class Container, typename T, std::size_t Nb_components, std::size_t Nb_block, std::size_t Dim>
inline
void NetCDFReader<Container, T, Nb_components, Nb_block, Dim>::get_var(){
	//default variables names
	if (var_names_.empty())
		find_var();
	//	if (var_names_.empty())
	//	{
	//		var_names_.resize(Nb_components);
	//		for(std::size_t i = 0; i < Nb_components; ++i)
	//		{
	//			std::ostringstream ost;
	//			ost<<(i+1);
	//			var_names_[i] = std::string("I"+ost.str());
	//		}
	//	}

	if (var_names_.size() < Nb_components){
		std::ostringstream err;
		err << FILE_READ_ERROR << this->data_filename_ << " | The var_name vector should contain " << Nb_components << " names.";
		err << "Available variables with the correct dimensions are ";
		std::copy(var_names_.begin(),var_names_.end(),std::ostream_iterator<std::string>(err," ,"));
		slip::slip_exception exc(std::string("slip"), std::string("NetCDFReader::get_var()"), err.str());
		throw (exc);
	}

	// Define the netCDF data variables.
	for(std::size_t i = 0; i < Nb_components; ++i)
	{
		nc_var_.push_back(dataFile->get_var(var_names_[i].c_str()));
		if (!nc_var_[i])
		{
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | " << var_names_[i] << " is not a valid var name.";
			slip::slip_exception exc(std::string("slip"), std::string("NetCDFReader::get_var()"), err.str());
			throw (exc);
		}
	}
}

template<class Container, typename T, std::size_t Nb_components, std::size_t Nb_block, std::size_t Dim>
inline
void NetCDFReader<Container, T, Nb_components, Nb_block, Dim>::get_dim(NcVar* Var){
	if (!Var){
		std::ostringstream err;
		err << FILE_READ_ERROR << this->data_filename_ << " | No variables are defined.";
		slip::slip_exception exc(std::string("slip"), std::string("NetCDFReader::get_dim()"), err.str());
		throw (exc);
	}

	for(std::size_t i=0; i<Dim; i++){
		nc_dim_.push_back(Var->get_dim(i));
		if (!nc_dim_[i])
		{
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | Cannot find the dimension #" << i << ".";
			slip::slip_exception exc(std::string("slip"), std::string("NetCDFReader::get_dim()"), err.str());
			throw (exc);
		}
	}
}

template<class Container, typename T, std::size_t Nb_components, std::size_t Nb_block, std::size_t Dim>
inline
void NetCDFReader<Container, T, Nb_components, Nb_block, Dim>::initialize(const std::vector<std::string> & var_names){
	var_names_ = var_names;
	initialize();
}

template<class Container, typename T, std::size_t Nb_components, std::size_t Nb_block, std::size_t Dim>
inline
void NetCDFReader<Container, T, Nb_components, Nb_block, Dim>::initialize(){
	if (!initialized){
		if (Nb_components == 0 || Dim == 0 || Dim > 4){//Dim > 4 with 4D containers
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | Bad template parameters.";
			slip::slip_exception exc(std::string("slip"), std::string("NetCDFReader::initialize()"), err.str());
			throw (exc);
		}
		//open the file
		dataFile = new NcFile(this->data_filename_.c_str(), NcFile::ReadOnly,0,0,NcFile::Classic);
		NcError err(NcError::silent_nonfatal);
		if (!dataFile->is_valid())
		{
			delete dataFile;
			std::ostringstream err;
			err << FILE_OPEN_ERROR << this->data_filename_;
			slip::slip_exception exc(std::string("slip"), std::string("NetCDFReader::initialize()"), err.str());
			throw (exc);
		}

		//Get variables and dimensions
		try{
			get_var();
		}catch(std::exception &e){
			if(dataFile->is_valid()){
				dataFile->close();
				delete dataFile;
			}
			throw(e);
		}

		try{
			get_dim(nc_var_[0]);//suppose all components have the same dim.
		}catch(std::exception &e){
			if(dataFile->is_valid()){
				dataFile->close();
				delete dataFile;
			}
			throw(e);
		}

		block_size = static_cast<int>(nc_dim_[0]->size() / static_cast<long int>(Nb_block));
		last_block_size = static_cast<int>(nc_dim_[0]->size() % static_cast<long int>(Nb_block));
		last_block_size  = (last_block_size == 0 ? block_size : block_size+last_block_size);
		last_it = (Nb_block == 1);
		finished = (last_it && last_block_size == 0);
		initialized = true;
	}

}

template<class Container, typename T, std::size_t Nb_components, std::size_t Nb_block, std::size_t Dim>
inline
int NetCDFReader<Container, T, Nb_components, Nb_block, Dim>::read(Container & in){
	if(!initialized){
		std::ostringstream err;
		err << FILE_READ_ERROR << this->data_filename_ << " | The reader needs to be initialized before reading.";
		slip::slip_exception exc(std::string("slip"), std::string("NetCDFReader::read()"), err.str());
		throw (exc);
	}
	if (finished)
		return 0;

	int first_dim_size = (last_it ? last_block_size : block_size);
	long first_dim_pos = static_cast<long>((block_ind-1) *  block_size);

	try{
		__nc_resize<Dim>::template apply<Container> (in,first_dim_size,nc_dim_);
	}catch(std::exception &e){
		if(dataFile->is_valid()){
			dataFile->close();
			delete dataFile;
		}
		std::ostringstream err;
		err << FILE_READ_ERROR << this->data_filename_ << " | Container resize impossible : " << e.what() << ".";
		slip::slip_exception exc(std::string("slip"), std::string("NetCDFReader::read()"), err.str());
		throw(exc);
	}

	try{
		__nc_read_data<Nb_components,Dim>::template apply<Container,T> (in,nc_var_,first_dim_pos,
				static_cast<long>(first_dim_size),nc_dim_);
	}catch(std::exception &e){
		if(dataFile->is_valid()){
			dataFile->close();
			delete dataFile;
		}
		std::ostringstream err;
		err << FILE_READ_ERROR << this->data_filename_ << " | " << e.what() << ".";
		slip::slip_exception exc(std::string("slip"), std::string("NetCDFReader::read()"), err.str());
		throw(exc);
	}

	block_ind++;
	if (last_it){
		finished = true;
	}
	else
		last_it = (block_ind == Nb_block);
	return (!finished);
}

template<class Container, typename T, std::size_t Nb_components, std::size_t Nb_block, std::size_t Dim>
inline
void NetCDFReader<Container, T, Nb_components, Nb_block, Dim>::release(){
	if(initialized){

		if(dataFile->is_valid()){
			dataFile->close();
			delete dataFile;
		}

		var_names_.clear();
		nc_var_.clear();
		nc_dim_.clear();
		block_ind = 1;
		block_size = 0;
		last_block_size = 0;
		last_it = 0;
		finished = false;
		initialized = false;
	}
}

} /*! namespace slip */
#endif /* NETCDFREADER_HPP_ */

