/*!
 ** \file JpegReader.hpp
 ** \brief containers reader from jpeg files
 ** It includes jpeglib.h from libjpeg
 ** \version Fluex 1.0
 ** \date 2013/04/05
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef JPEGREADER_HPP_
#define JPEGREADER_HPP_

#include <iostream>
#include <sstream>

#include "JpegDef.hpp"
#include "error.hpp"
#include "ContainerReader.hpp"




namespace slip {

/*!
 ** @ingroup IO
 ** \version Fluex 1.0
 ** \date 2013/04/05
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief %JpegReader, inherited from %ContainerReader, is a reader for jpeg images.
 ** \param Container2d should be a 2d Container with a component number equal to Nb_components.
 ** \param T the type of the container data.
 ** \param Nb_components the number of image components (3 for a color image or 1 for a gray scale image).
 ** \param Nb_block is the number of containers to create (>1 if one want to split the input
 ** data into different containers, default is 1). Notice that the image data is always cut according to the
 ** rows dimension which must be the first dimension of the input container. In other words
 ** each container will contain a set of rows which size depends on the Nb_block.
 ** \pre Container2d should have Nb_components components
 ** \pre Nb_components should be 1 or 3.
 ** \par Axis conventions (dim1 = rows, dim2 = cols):
 ** \image html iterator2d_conventions.jpg "axis and notation conventions"
 ** \image latex iterator2d_conventions.eps "axis and notation conventions" width=5cm
 */
template<class Container2d, typename T, std::size_t Nb_components, std::size_t Nb_block>
class JpegReader: public ContainerReader<Container2d,T,Nb_block> {
public:
	typedef Container2d container_type;
	typedef T value_type;
	typedef ContainerReader<Container2d,T,Nb_block> base;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** Default constructor
	 */
	JpegReader()
	:base(),cinfo(),jerr(),infile(NULL),buffer(),row_stride(0),
	 block_ind(1),block_size(0),last_block_size(0),last_it(0),finished(0),initialized(0){}

	/*!
	 ** Constructor with parameters.
	 ** \param data_filename is the name of the file containing the data to read
	 ** \note this constructor automatically call the initialize() method.
	 */
	JpegReader(std::string data_filename)
	:base(data_filename),cinfo(),jerr(),infile(NULL),buffer(),row_stride(0),
	 block_ind(1),block_size(0),last_block_size(0),last_it(0),finished(0),initialized(0)
	{ initialize();}


	/*!
	 ** Destructor
	 ** \note the release() method is called.
	 */
	virtual ~JpegReader(){
		if(initialized){
			release();
		}
	}
	/*@} End Constructors */

	/*!
	 ** \brief initialized the reading process.
	 ** \note an initialization is called by the %JpegReader(std::string) constructor.
	 */
	void initialize();

	/*!
	 ** \brief release the reading process.
	 ** \note this method is called by the destructor.
	 */
	void release();

	/*!
	 ** \brief virtual read function. If Nb_block is more than one,
	 ** it reads only one container, and returns 0. Else it reads the
	 ** container and returns 1. If the file data_filename
	 ** has been completely read (no more block to read), it returns 1.
	 ** \param in is the container to fill. Warning, the container should be a 2d container
	 ** with [] operator, resize(), width() and height() methods defined.
	 ** \return true if there is still data to read, false if the file has been completely read.
	 */
	int read(Container2d & in);

private:
	jpeg_decompress_struct cinfo; /* JPEG decompression parameters and pointers to working space */
	my_error_mgr jerr;  /* We use our private extension JPEG error handler. */
	FILE * infile;		/* source file */
	JSAMPARRAY buffer;		/* Output row buffer */
	int row_stride;		/* physical row width in output buffer */
	int block_ind;/*current read block indice*/
	int block_size;/*block size*/
	int last_block_size;/*last block size*/
	bool last_it;/*indicates that the current read is the last.*/
	bool finished;/*indicates that the last read has been done and no more data are available.*/
	bool initialized;/*indicates that the reader has been initialized*/
};

} /*! namespace slip */

namespace slip {

template<class Container2d, typename T, std::size_t Nb_components, std::size_t Nb_block>
inline
void JpegReader<Container2d, T, Nb_components, Nb_block>::initialize(){
	if (!initialized){
		if (Nb_components != 1 && Nb_components != 3){
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | Nb_components should be 1 or 3.";
			slip::slip_exception exc(std::string("slip"), std::string("JpegReader::initialize()"), err.str());
			throw (exc);
		}

		/* VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
		 * requires it in order to read binary files. */
		if ((infile = fopen(this->data_filename_.c_str(), "rb")) == NULL) {
			std::ostringstream err;
			err << FILE_OPEN_ERROR << this->data_filename_;
			slip::slip_exception exc(std::string("slip"), std::string("JpegReader::initialize()"), err.str());
			throw (exc);
		}
		/* Step 1: allocate and initialize JPEG decompression object */

		/* We set up the normal JPEG error routines, then override error_exit. */
		cinfo.err = jpeg_std_error(&jerr.pub);
		jerr.pub.error_exit = my_error_exit;

		/* Establish the setjmp return context for my_error_exit to use. */
		if (setjmp(jerr.setjmp_buffer)) {
			/* If we get here, the JPEG code has signaled an error.
			 * We need to clean up the JPEG object, close the input file, and throw an exception
			 */
			char buffer[JMSG_LENGTH_MAX];
			/* Create the message */
			(*cinfo.err->format_message) (reinterpret_cast<j_common_ptr>(&cinfo), buffer);
			jpeg_destroy_decompress(&cinfo);
			fclose(infile);
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | JPEG library has signaled the error : "
					<< buffer;
			slip::slip_exception exc(std::string("slip"), std::string("JpegReader::initialize()"), err.str());
			throw (exc);
		}
		/* Now we can initialize the JPEG decompression object. */
		jpeg_create_decompress(&cinfo);

		/* Step 2: specify data source (eg, a file) */

		slip_jpeg_stdio_src(&cinfo, infile);

		/* Step 3: read file parameters with jpeg_read_header() */

		jpeg_read_header(&cinfo, TRUE);

		/* Step 4: set parameters for decompression */

		/* Here we don't need to change any of the defaults set by
		 * jpeg_read_header(), so we do nothing here.
		 */

		/* Step 5: Start decompressor */

		jpeg_start_decompress(&cinfo);

		/* Test the number of components and allocate the input container */
		if(cinfo.num_components != Nb_components){
			fclose(infile);
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | Nb_components does not match.";
			slip::slip_exception exc(std::string("slip"), std::string("JpegReader::initialize()"), err.str());
			throw(exc);
		}

		/* JSAMPLEs per row in output buffer */
		row_stride = cinfo.output_width * cinfo.output_components;
		/* Make a one-row-high sample array that will go away when done with image */
		buffer = (*cinfo.mem->alloc_sarray)((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);
		block_size = cinfo.output_height / Nb_block;
		last_block_size = cinfo.output_height % Nb_block;
		last_block_size  = (last_block_size == 0 ? block_size : block_size+last_block_size);
		last_it = (Nb_block == 1);
		finished = (last_it && last_block_size == 0);
		initialized = true;
	}
}

template<class Container2d, typename T, std::size_t Nb_components, std::size_t Nb_block>
int JpegReader<Container2d, T, Nb_components, Nb_block>::read(Container2d & in){
	if(!initialized){
		std::ostringstream err;
		err << FILE_READ_ERROR << this->data_filename_ << " | The reader needs to be initialized before reading.";
		slip::slip_exception exc(std::string("slip"), std::string("JpegReader::read()"), err.str());
		throw (exc);
	}
	if (finished)
		return 0;

	if (last_it)
		in.resize(last_block_size,cinfo.output_width,T());
	else{
		in.resize(block_size,cinfo.output_width,T());
	}
	if(cinfo.output_height - cinfo.output_scanline < in.height()){
		std::ostringstream err;
		err << FILE_READ_ERROR << this->data_filename_ << " | Bad image dimensions.";
		slip::slip_exception exc(std::string("slip"), std::string("JpegReader::read()"), err.str());
		throw (exc);
	}

	for(unsigned int i=0; i < in.height(); ++i){
		jpeg_read_scanlines(&cinfo, buffer, 1);
		T * ptr_i = reinterpret_cast<T *>(in[i]);
		std::copy(buffer[0],buffer[0]+row_stride,ptr_i);
	}

	block_ind++;
	if (last_it)
		finished = true;
	else
		last_it = (block_ind == Nb_block);
	return (!finished);
}

template<class Container2d, typename T, std::size_t Nb_components, std::size_t Nb_block>
void  JpegReader<Container2d, T, Nb_components, Nb_block>::release(){
	if(initialized){
		/* Step 7: Finish decompression */
		if(finished)
			(void) jpeg_finish_decompress(&cinfo);
		/* We can ignore the return value since suspension is not possible
		 * with the stdio data source.
		 */

		/* Step 8: Release JPEG decompression object */

		/* This is an important step since it will release a good deal of memory. */
		jpeg_destroy_decompress(&cinfo);

		/* After finish_decompress, we can close the input file.
		 * Here we postpone it until after no more JPEG errors are possible,
		 * so as to simplify the setjmp error logic above.  (Actually, I don't
		 * think that jpeg_destroy can do an error exit, but why assume anything...)
		 */
		if (infile != NULL)
			fclose(infile);

		row_stride = 0;
		block_ind = 1;
		block_size = 0;
		last_block_size = 0;
		last_it = false;
		finished = false;
		initialized = false;
	}
}

} /*! namespace slip */
#endif /* JPEGREADER_HPP_ */
