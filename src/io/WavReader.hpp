/*!
 ** \file WavReader.hpp
 ** \brief containers reader from wav files
 ** \version Fluex 1.0
 ** \date 2013/04/15
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef WAVREADER_HPP_
#define WAVREADER_HPP_

#include <fstream>
#include <iostream>
#include <sstream>

#include "ContainerReader.hpp"
#include "WavDef.hpp"

namespace slip {



/*!
 ** @ingroup IO
 ** \version Fluex 1.0
 ** \date 2013/04/15
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief %WavReader, inherited from %ContainerReader, is a reader for wav audio files.
 ** \param Container1d should be a 1d Container with only one component.
 ** \param T the type of the container data.
 ** \param Nb_block is the number of containers to create (>1 if one want to split the input
 ** data into different containers, default is 1).
 ** \pre The audio signal should be a mono signal. If it is not, only the first channel is read.
 ** \note It is assumed that there is at most 32 bits per samples
 */
template<class Container1d, typename T, std::size_t Nb_block>
class WavReader: public ContainerReader<Container1d,T,Nb_block> {
public:
	typedef Container1d container_type;
	typedef T value_type;
	typedef ContainerReader<Container1d,T,Nb_block> base;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** Default constructor
	 */
	WavReader()
	:base(),
	 wave(),header_size(0),output_size(0),infile(),indblock(1),block_size(0),last_block_size(0),
	 last_it(0),finished(0),initialized(0)
	{infile.exceptions(std::ifstream::failbit | std::ifstream::badbit);}

	/*!
	 ** Constructor with parameters.
	 ** \param data_filename is the name of the file containing the data to read
	 ** \note this constructor automatically call the initialize() method.
	 */
	WavReader(std::string data_filename)
	:base(data_filename),
	 wave(),header_size(0),output_size(0),infile(),indblock(1),block_size(0),last_block_size(0),
	 last_it(0),finished(0),initialized(0)
	{infile.exceptions(std::ifstream::failbit | std::ifstream::badbit);initialize();}

	/*!
	 ** Destructor
	 ** \note the release() method is called.
	 */
	virtual ~WavReader(){
		if(initialized){
			release();
		}
	}
	/*@} End Constructors */

	/*!
	 ** \brief initialized the reading process.
	 ** \note an initialization is called by the %WavReader(std::string) constructor.
	 */
	void initialize();

	/*!
	 ** \brief release the reading process.
	 ** \note this method is called by the destructor.
	 */
	void release();

	/*!
	 ** \brief virtual read function. If Nb_block is more than one,
	 ** it reads only one container, and returns 0. Else it reads the
	 ** container and returns 1. If the file data_filename
	 ** has been completely read (no more block to read), it returns 1.
	 ** \param in is the container to fill. Warning, the container should be a 1d container
	 ** with [] operator, resize() and size() methods defined.
	 ** \return true if there is still data to read, false if the file has been completely read.
	 ** \pre The audio signal should be a mono signal. If it is not, only the first channel is read.
	 ** \note It is assumed that there is at most 32 bits per samples
	 */
	int read(container_type & in);



	/*!
	 ** \brief Get the output signal size
	 ** \return the signal size
	 */
	std::size_t get_output_size() const {
		return output_size;
	}
	/*!
	 ** \brief Get the wav file header
	 ** \return the wav file header
	 */
	const WAVE_HEADER& get_header() const {
		return wave;
	}


private:

	/**
	 ** \name Wave file header parameters
	 */
	/*@{*/
	WAVE_HEADER wave;//wave header parameters
	std::size_t header_size;//number of bytes of the header (in most cases it is 44)
	std::size_t output_size;//Output signal size;
	/*@} End Wave file header parameters*/

	std::ifstream infile;//input file stream
	std::size_t indblock;//current read block indice
	std::size_t block_size;//block size
	std::size_t last_block_size;//last block size
	bool last_it;//indicates that the current read is the last.
	bool finished;//indicates that the last read has been done and no more data are available.
	bool initialized;//indicates that the reader has been initialized

};

} /*! namespace slip */

namespace slip {



template<class Container1d, typename T, std::size_t Nb_block>
inline
void WavReader<Container1d, T, Nb_block>::initialize(){
	if (!initialized){
		try {
			infile.open(this->data_filename_.c_str(), std::ifstream::binary);
		}
		catch(...){
			std::ostringstream err;
			err << FILE_OPEN_ERROR << this->data_filename_;
			slip::slip_exception exc(std::string("slip"), std::string("WavReader::initialize()"), err.str());
			throw (exc);
		}
		// Read the RIFF chunk header
		infile.read(wave.riff_header.chunk_id,4);
		wave.riff_header.chunk_id[4] = '\0';
		header_size += 4;
		if (wave.riff_header.chunk_id[0] != 'R' || wave.riff_header.chunk_id[1] != 'I'
				|| wave.riff_header.chunk_id[2] != 'F' || wave.riff_header.chunk_id[3] != 'F'){
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | unable to find the RIFF chunk.";
			slip::slip_exception exc(std::string("slip"), std::string("WavReader::initialize()"), err.str());
			if (infile.is_open())
						infile.close();
			throw (exc);
		}
		infile.read(reinterpret_cast<char8*>(&wave.riff_header.chunk_size),4);
		header_size += 4;
#ifdef REVERSE_ENDIANISM
		// reverse the endianism of the chunk size.
		wave.riff_header.chunk_size = SWAP_32(wave.riff_header.chunk_size);
#endif
		infile.read(wave.riff_header.format,4);
		wave.riff_header.format[4] = '\0';
		header_size += 4;
		if (wave.riff_header.format[0] != 'W' || wave.riff_header.format[1] != 'A'
				|| wave.riff_header.format[2] != 'V' || wave.riff_header.format[3] != 'E'){
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | not a WAVE file.";
			slip::slip_exception exc(std::string("slip"), std::string("WavReader::initialize()"), err.str());
			if (infile.is_open())
						infile.close();
			throw (exc);
		}

		// Read the FORMAT chunk header
		infile.read(wave.format_header.chunk_id,4);
		wave.format_header.chunk_id[4] = '\0';
		header_size += 4;
		if (wave.format_header.chunk_id[0] != 'f' || wave.format_header.chunk_id[1] != 'm'
				|| wave.format_header.chunk_id[2] != 't' || wave.format_header.chunk_id[3] != ' '){
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | unable to find the fmt chunk.";
			slip::slip_exception exc(std::string("slip"), std::string("WavReader::initialize()"), err.str());
			if (infile.is_open())
						infile.close();
			throw (exc);
		}
		infile.read(reinterpret_cast<char8*>(&wave.format_header.chunk_size),4);
		infile.read(reinterpret_cast<char8*>(&wave.format_header.format),2);
		infile.read(reinterpret_cast<char8*>(&wave.format_header.num_channels),2);
		infile.read(reinterpret_cast<char8*>(&wave.format_header.sample_rate),4);
		infile.read(reinterpret_cast<char8*>(&wave.format_header.byte_rate),4);
		infile.read(reinterpret_cast<char8*>(&wave.format_header.align),2);
		infile.read(reinterpret_cast<char8*>(&wave.format_header.bits_per_sample),2);
		header_size += 20;
#ifdef REVERSE_ENDIANISM
		wave.format_header.chunk_size = SWAP_32(wave.format_header.chunk_size);
		wave.format_header.format = SWAP_16(wave.format_header.format);
		wave.format_header.num_channels = SWAP_16(wave.format_header.num_channels);
		wave.format_header.sample_rate = SWAP_32(wave.format_header.sample_rate);
		wave.format_header.byte_rate = SWAP_32(wave.format_header.byte_rate);
		wave.format_header.align = SWAP_16(wave.format_header.align);
		wave.format_header.bits_per_sample = SWAP_16(wave.format_header.bits_per_sample);
#endif

		//assume we have 8,16 or 32 bits samples
		if (wave.format_header.bits_per_sample > 32){
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | Too much bit per sample.";
			slip::slip_exception exc(std::string("slip"), std::string("WavReader::read()"), err.str());
			if (infile.is_open())
						infile.close();
			throw (exc);
		}

		// As it is possible to have are other chunks, like "fact" chunks, in the file,
		// we will skip over these extra chunks until we find a "data" chunk
		std::size_t pos = infile.tellg();
		std::size_t length = wave.riff_header.chunk_size + 4;
		infile.read(wave.data_header.chunk_id,4);
		wave.data_header.chunk_id[4] = '\0';
		header_size += 4;
		while ((wave.data_header.chunk_id[0] != 'd' || wave.data_header.chunk_id[1] != 'a' ||
				wave.data_header.chunk_id[2] != 't' || wave.data_header.chunk_id[3] != 'a')
				&& (pos < length))
		{
			pos++;
			infile.seekg(pos);
			infile.read(wave.data_header.chunk_id,4);
			wave.data_header.chunk_id[4] = '\0';
			header_size += 1;
		}

		if (pos >= length){
			std::ostringstream err;
			err << FILE_READ_ERROR << this->data_filename_ << " | unable to find the data chunk.";
			slip::slip_exception exc(std::string("slip"), std::string("WavReader::initialize()"), err.str());
			if (infile.is_open())
						infile.close();
			throw (exc);
		}

		infile.read(reinterpret_cast<char8*>(&wave.data_header.chunk_size),4);
		header_size += 4;
#ifdef REVERSE_ENDIANISM
		wave.data_header.chunk_size = SWAP_32(wave.data_header.chunk_size);
#endif
		output_size = (wave.data_header.chunk_size) / static_cast<std::size_t>(wave.format_header.align);
		block_size = output_size / Nb_block;
		last_block_size = output_size % Nb_block;
		last_block_size  = (last_block_size == 0 ? block_size : block_size+last_block_size);
		last_it = (Nb_block == 1);
		finished = (last_it && last_block_size == 0);
		initialized = true;
	}
}

template<class Container1d, typename T, std::size_t Nb_block>
int WavReader<Container1d, T, Nb_block>::read(Container1d & in){
	if(!initialized){
		std::ostringstream err;
		err << FILE_READ_ERROR << this->data_filename_ << " | The reader needs to be initialized before reading.";
		slip::slip_exception exc(std::string("slip"), std::string("WavReader::read()"), err.str());
		throw (exc);
	}
	if (finished)
		return 0;

	if (last_it)
		in.resize(last_block_size,T());
	else{
		in.resize(block_size,T());
	}
	//	std::size_t step_size = static_cast<std::size_t>(wave.format_header.bits_per_sample) / 8;
	std::size_t pos = (static_cast<std::size_t>(infile.tellg()) - header_size) / static_cast<std::size_t>(wave.format_header.align);
	if(output_size - pos < in.size()){
		std::ostringstream err;
		err << FILE_READ_ERROR << this->data_filename_ << " | Bad audio dimensions.";
		slip::slip_exception exc(std::string("slip"), std::string("WavReader::read()"), err.str());
		release();
		throw (exc);
	}



	std::size_t bps = static_cast<std::size_t>(wave.format_header.bits_per_sample);

	//assume we have at most 32 bits per samples
	switch(bps){
	case 8:
	{
		for(std::size_t i=0; i<in.size(); i++){
			char8 d = 0;
			infile.read(reinterpret_cast<char8*>(&d),1);
			in[i] = static_cast<T>(d);
			if (wave.format_header.num_channels > 1){
				std::size_t newpos = static_cast<std::size_t>(infile.tellg()) +
						static_cast<std::size_t>(wave.format_header.num_channels - 1);
				infile.seekg(newpos);
			}
		}
		break;
	}
	case 16:
	{
		for(std::size_t i=0; i<in.size(); ++i){
			int16 d = 0;
			infile.read(reinterpret_cast<char8*>(&d),2);
#ifdef REVERSE_ENDIANISM
			d = SWAP_16(d);
#endif
			in[i] = static_cast<T>(d);
			if (wave.format_header.num_channels > 1){
				std::size_t newpos = static_cast<std::size_t>(infile.tellg()) +
						2* static_cast<std::size_t>(wave.format_header.num_channels - 1);
				infile.seekg(newpos);
			}
		}
		break;
	}
	case 32:
	{
		for(std::size_t i=0; i<in.size(); ++i){
			int32 d = 0;
			infile.read(reinterpret_cast<char8*>(&d),4);
#ifdef REVERSE_ENDIANISM
			d = SWAP_32(d);
#endif
			in[i] = static_cast<T>(d);
			if (wave.format_header.num_channels > 1){
				std::size_t newpos = static_cast<std::size_t>(infile.tellg()) +
						4* static_cast<std::size_t>(wave.format_header.num_channels - 1);
				infile.seekg(newpos);
			}
		}
		break;
	}
	default:
	{
		std::ostringstream err;
		err << FILE_WRITE_ERROR << this->data_filename_ << " | The specified bit per sample is not valid.";
		slip::slip_exception exc(std::string("slip"), std::string("WavReader::write()"), err.str());
		release();
		throw (exc);
	}
	}
	//int32_t d = 0;
	//infile.read(reinterpret_cast<char8*>(&d),step_size);
	//#ifdef REVERSE_ENDIANISM
	//d = SWAP_32(d);
	//#endif
	//d = d << ((4 - step_size) * 8);
	//in[i] = static_cast<T>(d);
	//if (wave.format_header.num_channels > 1){
	//	std::size_t newpos = static_cast<std::size_t>(infile.tellg()) +
	//			step_size * static_cast<std::size_t>(wave.format_header.num_channels - 1);
	//	infile.seekg(newpos);
	//}
	//}
	indblock++;
	if (last_it)
		finished = true;
	else
		last_it = (indblock == Nb_block);
	return (!finished);
}

template<class Container1d, typename T, std::size_t Nb_block>
void  WavReader<Container1d, T, Nb_block>::release(){
	if(initialized){

		if (infile.is_open())
			infile.close();
		wave = WAVE_HEADER();
		header_size = 0;
		output_size = 0;
		indblock = 1;
		block_size = 0;
		last_block_size = 0;
		last_it = false;
		finished = false;
		initialized = false;
	}
}

} /*! namespace slip */

#endif /* WAVREADER_HPP_ */
