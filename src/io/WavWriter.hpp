/*!
 ** \file WavWriter.hpp
 ** \brief containers writer from wav files
 ** \version Fluex 1.0
 ** \date 2013/04/18
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef WAVWRITER_HPP_
#define WAVWRITER_HPP_

#include <fstream>
#include <iostream>
#include <sstream>

#include "ContainerWriter.hpp"
#include "WavDef.hpp"

namespace slip {

/*!
 ** @ingroup IO
 ** \version Fluex 1.0
 ** \date 2013/04/18
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief WavWriter is the wave array writer.
 ** \param Container1d is the type of the containers that will be written in the wav file.
 ** It should be a one dimensional container with [] operator, begin(), end() and size()
 ** methods defined.
 ** \param T the type of the container data.
 ** \note only 8,16 and 32 bits/sample format could be written. If more than one channel is
 ** specified in the header, the container will be duplicated on each channel.
 */
template<class Container1d, typename T>
class WavWriter: public ContainerWriter<Container1d,T> {
public:
	typedef Container1d container_type;
	typedef T value_type;
	typedef ContainerWriter<Container1d, T> base;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** Default constructor
	 */
	WavWriter()
	:base(),wave_(),outfile(),finished(0),total_nb_elem(0),initialized(false),output_audio_size_(0)
	{outfile.exceptions(std::ofstream::failbit | std::ofstream::badbit);}

	/*!
	 ** Constructor with parameters.
	 ** \param output_filename is the name of the output file
	 ** where the data of the container will be saved.
	 ** \param wave the %WAVE_HEADER that has to be written in the output file
	 ** \param output_audio_size is the total audio array size.
	 */
	WavWriter(std::string output_filename, const WAVE_HEADER & wave,
			std::size_t output_audio_size);


	/*!
	 ** Constructor with parameters.
	 ** \param output_filename is the name of the output file
	 ** where the data of the container will be saved.
	 ** \param sample_rate is the sample frequency (11025,22050,44100,48000 or 96000)
	 ** \param bytes_per_sample should be 8, 16 or 32
	 ** \param output_audio_size is the total audio array size.
	 */
	WavWriter(std::string output_filename, short sample_rate,
			short byte_per_sample,	std::size_t output_audio_size);

	/*!
	 ** Destructor
	 */
	virtual ~WavWriter(){}

	/*@} End Constructors */

	/*!
	 ** \brief initialized the writing process. Has to be called before the first write() call of a given array.
	 ** \param wave the %WAVE_HEADER that has to be written in the output file
	 ** \param output_audio_size the total audio array size
	 */
	void initialize(const WAVE_HEADER & wave, std::size_t output_audio_size);

	/*!
	 ** \brief initialized the writing process. Has to be called before the first write() call of a given array.
	 ** \param sample_rate is the sample frequency (11025,22050,44100,48000 or 96000)
	 ** \param bytes_per_sample should be 8, 16 or 32
	 ** \param output_audio_size the total audio array size
	 */
	void initialize(short sample_rate, short byte_per_sample, std::size_t output_audio_size);


	/*!
	 ** \brief release the writing process. Has to be called when an audio file has been fully written.
	 */
	void release();

	/*!
	 ** \brief write function. This function write the data within the output
	 ** file after the previous one. When last is set to true, the output file is closed.
	 ** \param data is the audio container to save. Warning, the container should be a 1d container
	 ** with [] operator, begin(), end() and size() methods defined.
	 ** \return false if the file has been completely filled.
	 ** \note only 8,16 and 32 bits/sample are valid.
	 */
	int write(const container_type & data);

	/*!
	 ** \brief Set the wav header file.
	 ** \param wave the %WAVE_HEADER that has to be written in the output file
	 ** \note The data chunk size parameter will
	 ** be adjusted with the data size.
	 */
	void set_header(const WAVE_HEADER& wave) {
		this->wave_ = wave;
	}

	/*!
	 ** \brief Get the wav file header
	 ** \return the wav file header
	 ** \note The data chunk size parameter can be wrong as
	 ** it is adjusted by the read function
	 */
	const WAVE_HEADER& get_header() const {
		return wave_;
	}

private:

	/*! wave header parameters */
	WAVE_HEADER wave_;
	/*! output file stream */
	std::ofstream outfile;
	/*! flag indicating the end of the writing process */
	bool finished;
	/*! Total number of elements written in the output file */
	std::size_t total_nb_elem;
	/*! indicates if the initialized method has been called*/
	bool initialized;
	/*! Total size of the output data */
	std::size_t output_audio_size_;

	void initialize();

	void init_wave(short sample_rate, short byte_per_sample);
};

} /*! namespace slip */


namespace slip {

template<class Container1d, typename T>
inline
WavWriter<Container1d,T>::WavWriter(std::string output_filename, const WAVE_HEADER & wave,
		std::size_t output_audio_size)
		:base(output_filename),wave_(wave),outfile(),finished(0),total_nb_elem(0),initialized(false),
		 output_audio_size_(output_audio_size)
		 {
	outfile.exceptions(std::ofstream::failbit | std::ofstream::badbit);
	initialize();
		 }

template<class Container1d, typename T>
inline
WavWriter<Container1d,T>::WavWriter(std::string output_filename, short sample_rate,
		short byte_per_sample,	std::size_t output_audio_size)
		:base(output_filename),wave_(),outfile(),finished(0),total_nb_elem(0),initialized(false),
		 output_audio_size_(output_audio_size)
		 {
	outfile.exceptions(std::ofstream::failbit | std::ofstream::badbit);
	init_wave(sample_rate,byte_per_sample);
	initialize();
		 }

template<class Container1d, typename T>
inline
void WavWriter<Container1d,T>::init_wave(short sample_rate, short byte_per_sample){
	wave_.format_header.sample_rate = static_cast<int32>(sample_rate);
	wave_.format_header.bits_per_sample = static_cast<int16>(byte_per_sample);
	wave_.format_header.format = 1;
	wave_.format_header.num_channels = 1;
	wave_.format_header.align = wave_.format_header.bits_per_sample / 8;
	wave_.format_header.byte_rate = wave_.format_header.sample_rate
			* static_cast<int32>(wave_.format_header.align);

	wave_.riff_header.chunk_id[0] = 'R';
	wave_.riff_header.chunk_id[1] = 'I';
	wave_.riff_header.chunk_id[2] = 'F';
	wave_.riff_header.chunk_id[3] = 'F';
	wave_.riff_header.chunk_id[4] = '\0';
	wave_.riff_header.chunk_size = 36 + static_cast<int32>(output_audio_size_)
																															* static_cast<int32>(wave_.format_header.align);
	wave_.riff_header.format[0] = 'W';
	wave_.riff_header.format[1] = 'A';
	wave_.riff_header.format[2] = 'V';
	wave_.riff_header.format[3] = 'E';
	wave_.riff_header.format[4] = '\0';

	wave_.format_header.chunk_id[0] = 'f';
	wave_.format_header.chunk_id[1] = 'm';
	wave_.format_header.chunk_id[2] = 't';
	wave_.format_header.chunk_id[3] = ' ';
	wave_.format_header.chunk_id[4] = '\0';
	wave_.format_header.chunk_size = 16;

	wave_.data_header.chunk_id[0] = 'd';
	wave_.data_header.chunk_id[1] = 'a';
	wave_.data_header.chunk_id[2] = 't';
	wave_.data_header.chunk_id[3] = 'a';
	wave_.data_header.chunk_id[4] = '\0';
	wave_.data_header.chunk_size = static_cast<int32>(output_audio_size_) *
			static_cast<int32>(wave_.format_header.align);
}

template<class Container1d, typename T>
inline
void WavWriter<Container1d,T>::initialize(short sample_rate,
		short byte_per_sample, std::size_t output_audio_size){
	init_wave(sample_rate,byte_per_sample);
	output_audio_size_ = output_audio_size;
	initialize();
}

template<class Container1d, typename T>
inline
void WavWriter<Container1d,T>::initialize(const WAVE_HEADER & wave, std::size_t output_audio_size){
	wave_ = wave;
	output_audio_size_ = output_audio_size;
	initialize();
}


template<class Container1d, typename T>
inline
void WavWriter<Container1d,T>::initialize(){
	if(!initialized){
		try {
			outfile.open(this->output_filename_.c_str(), std::ofstream::binary);
		}
		catch(...){
			std::ostringstream err;
			err << FILE_OPEN_ERROR << this->output_filename_;
			slip::slip_exception exc(std::string("slip"), std::string("WavWriter::initialize()"), err.str());
			throw (exc);
		}

		// Write the RIFF chunk header
		outfile.write("RIFF",4);
		//Calculating the total number of bytes in the output file
		int32 chunk_size = 36 + static_cast<int32>(output_audio_size_)
																																	* static_cast<int32>(wave_.format_header.align);
#ifdef REVERSE_ENDIANISM
		// reversing the endianism of the chunk size.
		chunk_size = SWAP_32(chunk_size);
#endif
		outfile.write(reinterpret_cast<char8*>(&chunk_size),4);
		outfile.write("WAVE",4);

		// Write the Fmt chunk header
		outfile.write("fmt ",4);
		chunk_size = 16;
		int16 audiofmt = wave_.format_header.format;
		int16 numchannel = wave_.format_header.num_channels;
		int32 freq = wave_.format_header.sample_rate;
		int16 bytpersample = wave_.format_header.bits_per_sample;
		int16 bytperbl =  numchannel * bytpersample / 8;
		int32 bytrate = freq * static_cast<int32>(bytperbl);

#ifdef REVERSE_ENDIANISM
		chunk_size = SWAP_32(chunk_size);
		audiofmt = SWAP_16(audiofmt);
		numchannel = SWAP_16(numchannel);
		freq = SWAP_32(freq);
		bytrate = SWAP_32(bytrate);
		bytperbl = SWAP_16(bytperbl);
		bytpersample = SWAP_16(bytpersample);
#endif
		outfile.write(reinterpret_cast<char8*>(&chunk_size),4);
		outfile.write(reinterpret_cast<char8*>(&audiofmt),2);
		outfile.write(reinterpret_cast<char8*>(&numchannel),2);
		outfile.write(reinterpret_cast<char8*>(&freq),4);
		outfile.write(reinterpret_cast<char8*>(&bytrate),4);
		outfile.write(reinterpret_cast<char8*>(&bytperbl),2);
		outfile.write(reinterpret_cast<char8*>(&bytpersample),2);

		// Write the data chunk header
		outfile.write("data",4);
		chunk_size = output_audio_size_ * static_cast<std::size_t>(bytperbl);
#ifdef REVERSE_ENDIANISM
		chunk_size = SWAP_32(chunk_size);
#endif
		outfile.write(reinterpret_cast<char8*>(&chunk_size),4);

		initialized = true;
	}
}



template<class Container1d, typename T>
inline
int WavWriter<Container1d,T>::write(const container_type & data){
	if(!initialized){
		std::ostringstream err;
		err << FILE_WRITE_ERROR << this->output_filename_ << " | The writer to be initialized before writing.";
		slip_exception exc(std::string("slip"), std::string("WavWriter::write()"), err.str());
		throw (exc);
	}
	if (finished){
		return 0;
	}

	total_nb_elem += data.size();

	if(output_audio_size_ < total_nb_elem){
		std::ostringstream err;
		err << FILE_WRITE_ERROR << this->output_filename_ << " | Allocation problem during wav writing.";
		slip_exception exc(std::string("slip"), std::string("WavWriter::write()"), err.str());
		release();
		throw (exc);
	}

	std::size_t bps = static_cast<std::size_t>(wave_.format_header.bits_per_sample);
	std::size_t nbch = static_cast<std::size_t>(wave_.format_header.num_channels);
	std::size_t size_d = data.size();
	double max = static_cast<double>(*std::max_element(data.begin(),data.end()));
	double min = static_cast<double>(*std::min_element(data.begin(),data.end()));
	double delta = max - min;
	double max32 = static_cast<double>(std::numeric_limits<int32>::max());
	double min32 = static_cast<double>(std::numeric_limits<int32>::min());
	double delta32 = max32 - min32;
	double max16 = static_cast<double>(std::numeric_limits<int16>::max());
	double min16 = static_cast<double>(std::numeric_limits<int16>::min());
	double delta16 = max16 - min16;
	double max8 = static_cast<double>(255);
	switch(bps){
	case 8:
	{
		if ((max <= max8) && (min >= 0)){
			for(std::size_t j=0; j<size_d; j++){
				for(std::size_t i=0;i<nbch;i++){
					char d = static_cast<char>(data[j]);
					outfile.write(reinterpret_cast<char8*>(&d),1);
				}
			}
		}else{
			double alpha = max8/delta;
			for(std::size_t j=0; j<size_d; j++){
				char d = static_cast<char>(max8 - alpha*(max - static_cast<double>(data[j])));
				for(std::size_t i=0;i<nbch;i++){
					outfile.write(reinterpret_cast<char8*>(&d),1);
				}
			}
		}
		break;
	}
	case 16:
	{
		if ((max <= max16) && (min >= min16)){
			for(std::size_t j=0; j<size_d; j++){
				for(std::size_t i=0;i<nbch;i++){
					int16 d = static_cast<int16>(data[j]);
#ifdef REVERSE_ENDIANISM
					d = SWAP_16(d);
#endif
					outfile.write(reinterpret_cast<char8*>(&d),2);
				}
			}
		}else{
			double alpha = delta16/delta;
			for(std::size_t j=0; j<size_d; ++j){
				int16 d = static_cast<int16>(max16 - alpha*(max - static_cast<double>(data[j])));
#ifdef REVERSE_ENDIANISM
				d = SWAP_16(d);
#endif
				for(std::size_t i=0;i<nbch;i++){
					outfile.write(reinterpret_cast<char8*>(&d),2);
				}
			}
		}
		break;
	}
	case 32:
	{
		if ((max <= max32) && (min >= min32)){
			for(std::size_t j=0; j<size_d; j++){
				for(std::size_t i=0;i<nbch;i++){
					int32 d = static_cast<int32>(d);
#ifdef REVERSE_ENDIANISM
					d = SWAP_32(d);
#endif
					outfile.write(reinterpret_cast<char8*>(&d),4);
				}
			}
		}else{
			double alpha = delta32/delta;
			for(std::size_t j=0; j<size_d; ++j){
				int32 d = static_cast<int32>(max32 - alpha*(max - static_cast<double>(data[j])));
#ifdef REVERSE_ENDIANISM
				d = SWAP_32(d);
#endif
				for(std::size_t i=0;i<nbch;i++){
					outfile.write(reinterpret_cast<char8*>(&d),4);
				}
			}
		}
		break;
	}
	default:
		std::ostringstream err;
		err << FILE_WRITE_ERROR << this->output_filename_ << " | The specified bit per sample is not valid.";
		slip_exception exc(std::string("slip"), std::string("WavWriter::write()"), err.str());
		release();
		throw (exc);
	}

	if (total_nb_elem == output_audio_size_)
		finished = true;

	return (!finished);
}

template<class Container1d, typename T>
inline
void WavWriter<Container1d,T>::release(){
	if(initialized){
		if (outfile.is_open())
			outfile.close();
		wave_ = WAVE_HEADER();
		finished = false;
		total_nb_elem = 0;
		output_audio_size_ = 0;
		initialized = false;
	}
}

} /*! namespace slip */
#endif /* WAVWRITER_HPP_ */
