/*!
 ** \file NetCDFDef.hpp
 ** \brief Some definitions specific to NetCDF format
 ** \version Fluex 1.0
 ** \date 2013/05/15
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

/*!
 * \cond Do not put the following into Doxygen documentation
 */
#ifndef NETCDFDEF_HPP_
#define NETCDFDEF_HPP_

#include <iostream>
#include <sstream>
#include <vector>
#include <netcdfcpp.h>
#include "Vector.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "Array4d.hpp"
#include "error.hpp"
#include "utils_compilation.hpp"

/*!
 ** Information on NetCDF type of data, implemented types are the following:
 ** 	\c char,         \c int,         \c float
 ** \see NcTypeInfo<char> NcTypeInfo<int> NcTypeInfo<float>
 ** \note This list may not be updated, but have a look in \e Class \e Hierarchy of documentation
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \note All the NcTypeInfo structures were developed in the unofficial SLIP sources called "slip_addons"
 */
template<typename T> struct NcTypeInfo {
	//! Identifier - it selves (i.e. \c <int>value of \c NcType from \c ncvalues.h) on NetCDF type of data
  static const int ncId() {static const int i=ncNoType; return i;}
	//! Identifier as a string (i.e. \c <char*>value of \c NcType from \c ncvalues.h) on NetCDF type of data
	static const char* ncStr() {static const char *const s="ncNoType"; return s;}
};
//! Information on NetCDF type of a \c char data
template<> struct NcTypeInfo<char> {
	static const int ncId() {static const int i=ncChar; return i;}
	static const char* ncStr() {static const char *const s="ncChar"; return s;}
};
//! Information on NetCDF type of an \c int data
template<> struct NcTypeInfo<int> {
	static const int ncId() {static const int i=ncInt; return i;}
	static const char* ncStr() {static const char *const s="ncInt"; return s;}
};
//! Information on NetCDF type of a \c float data
template<> struct NcTypeInfo<float> {
	static const int ncId() {static const int i=ncFloat; return i;}
	static const char* ncStr() {static const char *const s="ncFloat"; return s;}
};
//! Information on NetCDF type of a \c double data
template<> struct NcTypeInfo<double> {
	static const int ncId() {static const int i=ncDouble; return i;}
	static const char* ncStr() {static const char *const s="ncDouble"; return s;}
};
//! Information on NetCDF type of a \c bool data
template<> struct NcTypeInfo<bool> {
	static const int ncId() {static const int i=ncByte; return i;}
	static const char* ncStr() {static const char *const s="ncByte (used as boolean)"; return s;}
};
//! Information on NetCDF type of a \c bool data
template<> struct NcTypeInfo<ncbyte> {
	static const int ncId() {static const int i=ncByte; return i;}
	static const char* ncStr() {static const char *const s="ncByte (used as boolean)"; return s;}
};
//! Information on NetCDF type as a string of any \c NcType
char *NcTypeStr(int ncId)
{
	char *s;
	switch(ncId)
	{
	case ncChar:  s=(char *)(NcTypeInfo<char>::ncStr());  break;
	case ncInt:   s=(char *)(NcTypeInfo<int>::ncStr());   break;
	case ncFloat: s=(char *)(NcTypeInfo<float>::ncStr()); break;
	case ncDouble:s=(char *)(NcTypeInfo<double>::ncStr());break;
	case ncByte:  s=(char *)(NcTypeInfo<bool>::ncStr());  break;
	default: return NULL;
	}
	return s;
}


namespace slip {
/*!
 ** \version Fluex 1.0
 ** \date 2013/05/15
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief nc_resize structure used for resizing the containers.
 ** \param Dim : container dimension
 */
template <std::size_t Dim>
struct __nc_resize{
	template <class Container>
	static void apply(Container & cont, int first_dim_size, const std::vector<NcDim*> & nc_dim){
		std::ostringstream err;
		err << "Dimension is not valid.";
		slip::slip_exception exc(std::string("slip"), std::string("nc_resize"), err.str());
		throw (exc);
	}
};

/*!
 ** \version Fluex 1.0
 ** \date 2013/05/15
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief nc_resize structure used for resizing the containers.
 ** \param Dim : container dimension = 1
 */
template<>
struct __nc_resize<1>{
	template <class Container1D>
	static void apply(Container1D & cont, int first_dim_size, const std::vector<NcDim*> & UNUSED(nc_dim)){
		typedef typename Container1D::value_type T;
		cont.resize(first_dim_size,T());
	}
};

/*!
 ** \version Fluex 1.0
 ** \date 2013/05/15
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief nc_resize structure used for resizing the containers.
 ** \param Dim : container dimension = 2
 */
template<>
struct __nc_resize<2>{
	template <class Container2D>
	static void apply(Container2D & cont, int first_dim_size, const std::vector<NcDim*> & nc_dim){
		typedef typename Container2D::value_type T;
		cont.resize(first_dim_size,static_cast<int>(nc_dim[1]->size()),T());
	}
};

/*!
 ** \version Fluex 1.0
 ** \date 2013/05/15
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief nc_resize structure used for resizing the containers.
 ** \param Dim : container dimension = 3
 */
template<>
struct __nc_resize<3>{
	template <class Container3D>
	static void apply(Container3D & cont, int first_dim_size, const std::vector<NcDim*> & nc_dim){
		typedef typename Container3D::value_type T;
		cont.resize(first_dim_size,static_cast<int>(nc_dim[1]->size()),
				static_cast<int>(nc_dim[2]->size()),T());
	}
};

/*!
 ** \version Fluex 1.0
 ** \date 2013/05/15
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief nc_resize structure used for resizing the containers.
 ** \param Dim : container dimension = 4
 */
template<>
struct __nc_resize<4>{
	template <class Container4D>
	static void apply(Container4D & cont, int first_dim_size, const std::vector<NcDim*> & nc_dim){
		typedef typename Container4D::value_type T;
		cont.resize(first_dim_size,static_cast<int>(nc_dim[1]->size()),
				static_cast<int>(nc_dim[2]->size()), static_cast<int>(nc_dim[3]->size()),T());
	}
};

} /*! namespace slip */

/*!
 * \endcond
 */

#endif /* NETCDFDEF_HPP_ */
