/*!
 ** \file FITSWriter.hpp
 ** \brief containers writer into fits files (write only Single Image FITS file)
 ** \version Fluex 1.0
 ** \date 2013/09/04
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \todo Adding writing of multicomponents signals (this version can only write one component signals)
 */

#ifndef FITSWRITER_HPP_
#define FITSWRITER_HPP_

#include "FITSDef.hpp"

#include "ContainerWriter.hpp"

namespace slip {
/*!
 * \cond Do not include the following into documentation
 */
/*!
 * \version Fluex 1.0
 * \date 2013/09/04
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_write structure used by FITSReader for writing a container into a PHDU.
 * \param Nb_components : the number of components
 * \param Dim : container dimension
 * \return the first dimension size that has been filled
 */
template <std::size_t Nb_components, std::size_t Dim>
struct __fits_write_data{
	template <typename MonoContainer, typename T>
	static std::size_t apply(const MonoContainer& container, CCfits::PHDU * phdu,
			long first_dim_pos){
		std::ostringstream err;
		err << "Nb Components or dimension is not valid.";
		slip::slip_exception exc(std::string("slip"), std::string("fits_write_data"), err.str());
		throw (exc);
		return 0;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/04
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_write structure used by FITSReader for writing a container into a PHDU.
 * \param Nb_components : the number of components = 1
 * \param Dim : container dimension = 1
 * \return the first dimension size that has been filled
 */
template <>
struct __fits_write_data<1,1>{
	template <typename MonoContainer1D, typename T>
	static std::size_t apply(const MonoContainer1D& container, CCfits::PHDU * phdu,
			long first_dim_pos)
	{
		std::size_t size = static_cast<std::size_t>(std::distance(container.begin(),container.end()));
		std::valarray<int> array(static_cast<int>(size));
		std::copy(container.begin(),container.begin() + size,&array[0]);
		long fpixel(first_dim_pos+1);
		try{
			phdu->write(fpixel,static_cast<long>(size),array);
		}catch(...){
			std::ostringstream err;
			err << "Cannot write 1D data.";
			slip::slip_exception exc(std::string("slip"), std::string("__fits_write_data_1_1"), err.str());
			throw (exc);
		}
		return size;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/04
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_write structure used by FITSReader for writing a container into a PHDU.
 * \param Nb_components : the number of components = N
 * \param Dim : container dimension = 1
 * \return the first dimension size that has been filled
 * \note Only the first component is copied in the fits file.
 */
template <std::size_t N>
struct __fits_write_data<N,1>{
	template <typename MultiContainer1D, typename T>
	static std::size_t apply(const MultiContainer1D& container, CCfits::PHDU * phdu,
			long first_dim_pos)
	{
		std::size_t size = static_cast<std::size_t>(std::distance(container.begin(),container.end()));
		std::valarray<int> array(static_cast<int>(size));
		std::copy(container.begin(0),container.begin(0) + size,&array[0]);
		long fpixel(first_dim_pos+1);
		try{
			phdu->write(fpixel,static_cast<long>(size),array);
		}catch(...){
			std::ostringstream err;
			err << "Cannot write 1D data.";
			slip::slip_exception exc(std::string("slip"), std::string("__fits_write_data_n_1"), err.str());
			throw (exc);
		}
		return size;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/04
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_write structure used by FITSReader for writing a container into a PHDU.
 * \param Nb_components : the number of components = 1
 * \param Dim : container dimension = 2
 * \return the first dimension size that has been filled
 */
template <>
struct __fits_write_data<1,2>{
	template <typename MonoContainer2D, typename T>
	static std::size_t apply(const MonoContainer2D& container, CCfits::PHDU * phdu,
			long first_dim_pos)
	{
		slip::DPoint2d<int> dim = container.bottom_right() - container.upper_left();
		std::size_t dim1 = dim.dx1();
		std::size_t dim2 = dim.dx2();
		std::size_t size = dim1 * dim2;
		std::valarray<int> array(static_cast<int>(size));
		std::copy(container.begin(),container.begin() + size,&array[0]);
		std::vector<long> first(2,1);
		first[1] = first_dim_pos+1;
		try{
			phdu->write(first,static_cast<long>(size),array);
		}catch(...){
			std::ostringstream err;
			err << "Cannot write 2D data.";
			slip::slip_exception exc(std::string("slip"), std::string("__fits_write_data_1_2"), err.str());
			throw (exc);
		}
		return dim1;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/04
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_write structure used by FITSReader for writing a container into a PHDU.
 * \param Nb_components : the number of components = N
 * \param Dim : container dimension = 2
 * \return the first dimension size that has been filled
 * \note Only the first component is copied in the fits file.
 */
template <std::size_t N>
struct __fits_write_data<N,2>{
	template <typename MultiContainer2D, typename T>
	static std::size_t apply(const MultiContainer2D& container, CCfits::PHDU * phdu,
			long first_dim_pos)
	{
		slip::DPoint2d<int> dim = container.bottom_right() - container.upper_left();
		std::size_t dim1 = dim.dx1();
		std::size_t dim2 = dim.dx2();
		std::size_t size = dim1 * dim2;
		std::valarray<int> array(static_cast<int>(size));
		std::copy(container.begin(0),container.begin(0) + size,&array[0]);
		std::vector<long> first(2,1);
		first[1] = first_dim_pos+1;
		try{
			phdu->write(first,static_cast<long>(size),array);
		}catch(...){
			std::ostringstream err;
			err << "Cannot write 2D data.";
			slip::slip_exception exc(std::string("slip"), std::string("__fits_write_data_N_2"), err.str());
			throw (exc);
		}
		return dim1;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/04
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_write structure used by FITSReader for writing a container into a PHDU.
 * \param Nb_components : the number of components = 1
 * \param Dim : container dimension = 3
 * \return the first dimension size that has been filled
 */
template <>
struct __fits_write_data<1,3>{
	template <typename MonoContainer3D, typename T>
	static std::size_t apply(const MonoContainer3D& container, CCfits::PHDU * phdu,
			long first_dim_pos)
	{
		slip::DPoint3d<int> dim = container.back_bottom_right() - container.front_upper_left();
		std::size_t dim1 = dim.dx1();
		std::size_t dim2 = dim.dx2();
		std::size_t dim3 = dim.dx3();
		std::size_t size = dim1 * dim2 * dim3;
		std::valarray<int> array(static_cast<int>(size));
		std::copy(container.begin(),container.begin() + size,&array[0]);
		std::vector<long> first(3,1);
		first[2] = first_dim_pos+1;
		try{
			phdu->write(first,static_cast<long>(size),array);
		}catch(...){
			std::ostringstream err;
			err << "Cannot write 3D data.";
			slip::slip_exception exc(std::string("slip"), std::string("__fits_write_data_1_3"), err.str());
			throw (exc);
		}
		return dim1;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/04
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_write structure used by FITSReader for writing a container into a PHDU.
 * \param Nb_components : the number of components = n
 * \param Dim : container dimension = 3
 * \return the first dimension size that has been filled
 * \note Only the first component is copied in the fits file.
 */
template <std::size_t N>
struct __fits_write_data<N,3>{
	template <typename MultiContainer3D, typename T>
	static std::size_t apply(const MultiContainer3D& container, CCfits::PHDU * phdu,
			long first_dim_pos)
	{
		slip::DPoint3d<int> dim = container.back_bottom_right() - container.front_upper_left();
		std::size_t dim1 = dim.dx1();
		std::size_t dim2 = dim.dx2();
		std::size_t dim3 = dim.dx3();
		std::size_t size = dim1 * dim2 * dim3;
		std::valarray<int> array(static_cast<int>(size));
		std::copy(container.begin(0),container.begin(0) + size,&array[0]);
		std::vector<long> first(3,1);
		first[2] = first_dim_pos+1;
		try{
			phdu->write(first,static_cast<long>(size),array);
		}catch(...){
			std::ostringstream err;
			err << "Cannot write 3D data.";
			slip::slip_exception exc(std::string("slip"), std::string("__fits_write_data_N_3"), err.str());
			throw (exc);
		}
		return dim1;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/04
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_write structure used by FITSReader for writing a container into a PHDU.
 * \param Nb_components : the number of components = 1
 * \param Dim : container dimension = 4
 * \return the first dimension size that has been filled
 */
template <>
struct __fits_write_data<1,4>{
	template <typename MonoContainer4D, typename T>
	static std::size_t apply(const MonoContainer4D& container, CCfits::PHDU * phdu,
			long first_dim_pos)
	{
		slip::DPoint4d<int> dim = container.last_back_bottom_right() - container.first_front_upper_left();
		std::size_t dim1 = dim.dx1();
		std::size_t dim2 = dim.dx2();
		std::size_t dim3 = dim.dx3();
		std::size_t dim4 = dim.dx4();
		std::size_t size = dim1 * dim2 * dim3 * dim4;
		std::valarray<int> array(static_cast<int>(size));
		std::copy(container.begin(),container.begin() + size,&array[0]);
		std::vector<long> first(4,1);
		first[3] = first_dim_pos+1;
		try{
			phdu->write(first,static_cast<long>(size),array);
		}catch(...){
			std::ostringstream err;
			err << "Cannot write 4D data.";
			slip::slip_exception exc(std::string("slip"), std::string("__fits_write_data_1_4"), err.str());
			throw (exc);
		}
		return dim1;
	}
};

/*!
 * \version Fluex 1.0
 * \date 2013/09/04
 * \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 * \brief fits_write structure used by FITSReader for writing a container into a PHDU.
 * \param Nb_components : the number of components = n
 * \param Dim : container dimension = 4
 * \return the first dimension size that has been filled
 * \note Only the first component is copied in the fits file.
 */
template <std::size_t N>
struct __fits_write_data<N,4>{
	template <typename MultiContainer4D, typename T>
	static std::size_t apply(const MultiContainer4D& container, CCfits::PHDU * phdu,
			long first_dim_pos)
	{
		slip::DPoint4d<int> dim = container.last_back_bottom_right() - container.first_front_upper_left();
		std::size_t dim1 = dim.dx1();
		std::size_t dim2 = dim.dx2();
		std::size_t dim3 = dim.dx3();
		std::size_t dim4 = dim.dx4();
		std::size_t size = dim1 * dim2 * dim3 * dim4;
		std::valarray<int> array(static_cast<int>(size));
		std::copy(container.begin(0),container.begin(0) + size,&array[0]);
		std::vector<long> first(4,1);
		first[3] = first_dim_pos+1;
		try{
			phdu->write(first,static_cast<long>(size),array);
		}catch(...){
			std::ostringstream err;
			err << "Cannot write 4D data.";
			slip::slip_exception exc(std::string("slip"), std::string("__fits_write_data_N_4"), err.str());
			throw (exc);
		}
		return dim1;
	}
};

/*!
 * \endcond
 */

/*!
 ** @ingroup IO
 ** \version Fluex 1.0
 ** \date 2013/09/04
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief FITSWriter is a fits writer.
 ** \param Container is the type of the containers that will be written in the fits file.
 ** It only process the single component container of dimension 1,2,3 (4 will come later). For multi-components
 ** containers only the first component is written.
 ** \param T the type of the container data.
 ** \param Nb_components the number of container components (ex : 3 for a color signal or 1 for a gray scale signal...).
 ** \param Dim is the dimension of the signal
 ** \note Every component will be registered as a separate variable.
 */
template<class Container, typename T, std::size_t Nb_components, std::size_t Dim>
class FITSWriter: public ContainerWriter<Container, T> {
public:
	typedef Container container_type;
	typedef T value_type;
	typedef ContainerWriter<Container, T> base;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** Default constructor
	 */
	FITSWriter()
	:base(),pInfile(0),data(0),output_signal_dimensions_(),finished(0),total_dim1_size(0),initialized(false){}

	/*!
	 ** Constructor with parameters.
	 ** \param output_filename is the name of the output file
	 ** \param output_signal_dimensions is the vector containing the dimension size of the signal = [dim1,dim2,dim3...]
	 */
	FITSWriter(std::string output_filename, const std::vector<std::size_t> & output_signal_dimensions)
	:base(output_filename),pInfile(0),data(0),output_signal_dimensions_(output_signal_dimensions),
	 finished(0),total_dim1_size(0),initialized(false)
	{initialize();}

	/*!
	 ** Destructor
	 */
	virtual ~FITSWriter(){
		release();
	}

	/*@} End Constructors */

	/*!
	 ** \brief initialized the writing process. Has to be called before the first write() call of a given signal.
	 ** \param output_signal_dimensions is the vector containing the dimension size of the signal = [dim1,dim2,dim3...]
	 */
	void initialize(const std::vector<std::size_t> & output_signal_dimensions);


	/*!
	 ** \brief release the writing process. Has to be called when an fits has been fully written.
	 */
	void release();

	/*!
	 ** \brief write function. This function write the data within the output
	 ** file after the previous one. When last is set to true, the output file is closed.
	 ** \param data is the container to save.
	 ** \return false if the file has been completely filled.
	 */
	int write(const container_type & data);

private:
	/*!FITS base object pointer*/
	std::auto_ptr<CCfits::FITS> pInfile;
	/*!Pointer on the Primary HDU that must contain the signal to read*/
	CCfits::PHDU * data;
	/*! output_signal_dimensions is the vector containing the dimension size of the signal = [dim1,dim2,dim3...] */
	std::vector<std::size_t> output_signal_dimensions_;
	/*! flag indicating the end of the writing process */
	bool finished;
	/*! total dim1 written counter*/
	std::size_t total_dim1_size;
	/*! bool indicates that initialization has been done */
	bool initialized;

	void initialize();
};

} /*! namespace slip */

namespace slip {

template<class Container, typename T, std::size_t Nb_components, std::size_t Dim>
inline
void FITSWriter<Container,T,Nb_components,Dim>::initialize(
		const std::vector<std::size_t> & output_signal_dimensions){
	output_signal_dimensions_.assign(output_signal_dimensions.begin(),output_signal_dimensions.end());
	initialize();
}

template<class Container, typename T, std::size_t Nb_components, std::size_t Dim>
inline
void FITSWriter<Container,T,Nb_components,Dim>::initialize(){
	if(!initialized){
		if (Nb_components == 0 || Dim == 0 || Dim > 4){
			std::ostringstream err;
			err << FILE_WRITE_ERROR << this->output_filename_ << " | Bad template parameters.";
			slip::slip_exception exc(std::string("slip"), std::string("FITSWriter::initialize()"), err.str());
			throw (exc);
		}
		if (output_signal_dimensions_.size() != Dim){
			std::ostringstream err;
			err << FILE_WRITE_ERROR << this->output_filename_ << " | Dimensions does not match.";
			slip::slip_exception exc(std::string("slip"), std::string("FITSWriter::initialize()"), err.str());
			throw (exc);
		}
		//open the file
		int nbaxis = static_cast<int>(Dim);
		long axes[nbaxis];
		std::copy(output_signal_dimensions_.rbegin(),output_signal_dimensions_.rend(),axes);
		//		try{
		//			pInfile.reset(new CCfits::FITS(this->output_filename_,__FitsTypeInfo<T>::fitsId(),nbaxis,axes));
		//		}catch(CCfits::FITS::CantCreate & e1){
		std::string filename = "!" + this->output_filename_;
		try{
			pInfile.reset(new CCfits::FITS(filename,__FitsTypeInfo<T>::fitsId(),nbaxis,axes));
		}catch(CCfits::FITS::CantCreate & e2){
			std::ostringstream err;
			err << e2.message() << " | " << FILE_OPEN_ERROR << " " << filename;
			slip::slip_exception exc(std::string("slip"), std::string("FITSWriter::initialize()"), err.str());
			throw (exc);
		}
		//}
		data = &pInfile->pHDU();
		initialized = true;
	}
}

template<class Container, typename T, std::size_t Nb_components, std::size_t Dim>
inline
int FITSWriter<Container,T,Nb_components,Dim>::write(const container_type & cont){
	if(!initialized){
		std::ostringstream err;
		err << FILE_WRITE_ERROR << this->output_filename_ << " | The writer to be initialized before writing.";
		slip::slip_exception exc(std::string("slip"), std::string("FITSWriter::write()"), err.str());
		throw (exc);
	}
	if (finished){
		return 0;
	}

	try{
		total_dim1_size += __fits_write_data<Nb_components,Dim>::
				template apply<Container,T> (cont,this->data,static_cast<long>(total_dim1_size));
	}catch(std::exception &e){
		std::ostringstream err;
		err << FILE_WRITE_ERROR << this->output_filename_ << " | " << e.what() << ".";
		slip::slip_exception exc(std::string("slip"), std::string("FITSWriter::write()"), err.str());
		throw(exc);
	}

	if (total_dim1_size == output_signal_dimensions_[0]){
		finished = true;
	}

	return (!finished);
}

template<class Container, typename T, std::size_t Nb_components, std::size_t Dim>
inline
void FITSWriter<Container,T,Nb_components,Dim>::release(){
	if(initialized){
		pInfile.release();
		data = 0;
		output_signal_dimensions_.clear();
		finished = false;
		total_dim1_size = 0;
		initialized = false;
	}
}




} /*! namespace slip */
#endif /* FITSWRITER_HPP_ */

