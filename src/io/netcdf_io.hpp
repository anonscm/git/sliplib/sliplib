/** 
 * \file netcdf_io.hpp
 * 
 * \brief Provides netcdf read/write slip data read/write algorithm
 * 
 */
#ifndef SLIP_NETCDF_IO_HPP
#define SLIP_NETCDF_IO_HPP

#include <ios>
#include <stdexcept>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <fstream>
#include <vector>

#include <netcdf>

#include <slip/Array.hpp>
#include <slip/Array2d.hpp>
#include <slip/Array3d.hpp>
#include <slip/error.hpp>
#include <slip/Polynomial.hpp>
#include <slip/MultivariatePolynomial.hpp>
//#include <netcdfcpp.h>



namespace slip
{

static const int NC_ERROR = 2;
static const int CODE_ERROR = 3;
static const int DIM_ERROR = 4;

const std::string NC_WRITE_ERR  = "Error writing NetCDF file: ";
const std::string NC_READ_ERR   = "Error reading NetCDF file: ";
const std::string NC_DIM_ERR    = "NetCDF dimension error file: ";
const std::string NC_ADD_DIM_ERR    = "NetCDF addDim error ";
const std::string NC_ADD_VAR_ERR    = "NetCDF addVar error ";
const std::string NC_WRITE_DATA_ERR = "NetCDF write data error ";
const std::string NC_GET_DIM_ERR    = "NetCDF getDim error ";
const std::string NC_GET_VAR_ERR    = "NetCDF getVar error ";
  
const std::string NC_MISMATCH_DIM_ERR    = "NetCDF dimensions are differents of container ones ";
const std::string NC_CODE_ERR   = "NetCDF code error";

 
template<typename T>
struct NcTypeInfo
  {
    static netCDF::NcType generate()
    {
      throw netCDF::exceptions::NcException("Invalid template parameter during the NcTypeInfo<T>::generate()", "", 0);
    }
    static netCDF::NcType::ncType getTypeClass()
    {
      throw netCDF::exceptions::NcException("Invalid template parameter during the NcTypeInfo<T>::getTypeClass()", "", 0);
    }
    static std::string getName()
    {
      throw netCDF::exceptions::NcException("Invalid template parameter during the NcTypeInfo<T>::getName()", "", 0);
    }
  };

template<>
struct NcTypeInfo<float>
{
  static const netCDF::NcType::ncType CURRENT_NC_TYPE = netCDF::NcType::ncType::nc_FLOAT;

  static netCDF::NcType generate()
  {
    return netCDF::NcType(CURRENT_NC_TYPE);
  }
  static netCDF::NcType::ncType getTypeClass()
  {
    return netCDF::NcType(CURRENT_NC_TYPE).getTypeClass();
  }
  static std::string getName()
  {
    return netCDF::NcType(CURRENT_NC_TYPE).getName();
  }
};

template<>
struct NcTypeInfo<double>
{
  static const netCDF::NcType::ncType CURRENT_NC_TYPE = netCDF::NcType::ncType::nc_DOUBLE;

  static netCDF::NcType generate()
  {
    return netCDF::NcType(CURRENT_NC_TYPE);
  }
  static netCDF::NcType::ncType getTypeClass()
  {
    return netCDF::NcType(CURRENT_NC_TYPE).getTypeClass();
  }
  static std::string getName()
  {
    return netCDF::NcType(CURRENT_NC_TYPE).getName();
  }
};

  template<>
  struct NcTypeInfo<uint16_t>
{
  static const netCDF::NcType::ncType CURRENT_NC_TYPE = netCDF::NcType::ncType::nc_USHORT;

  static netCDF::NcType generate()
  {
    return netCDF::NcType(CURRENT_NC_TYPE);
  }
  static netCDF::NcType::ncType getTypeClass()
  {
    return netCDF::NcType(CURRENT_NC_TYPE).getTypeClass();
  }
  static std::string getName()
  {
    return netCDF::NcType(CURRENT_NC_TYPE).getName();
  }
};
  //Windows and Linux/Unix/MacOSX 32 bits : int, long and pointer are 32 bits
  //Windows 64bits: int and long are 32 bits, pointer is 64 bits
  //Unix/Linux/MacOS 64bits: int is 32 bits and long and pointer are 64 bits
template<>
struct NcTypeInfo<int>
{
  static const netCDF::NcType::ncType CURRENT_NC_TYPE = netCDF::NcType::ncType::nc_INT;

  static netCDF::NcType generate()
  {
    return netCDF::NcType(CURRENT_NC_TYPE);
  }
  static netCDF::NcType::ncType getTypeClass()
  {
    return netCDF::NcType(CURRENT_NC_TYPE).getTypeClass();
  }
  static std::string getName()
  {
    return netCDF::NcType(CURRENT_NC_TYPE).getName();
  }
};

  template<>
struct NcTypeInfo<long>
{
  static const netCDF::NcType::ncType CURRENT_NC_TYPE = netCDF::NcType::ncType::nc_INT64;

  static netCDF::NcType generate()
  {
    return netCDF::NcType(CURRENT_NC_TYPE);
  }
  static netCDF::NcType::ncType getTypeClass()
  {
    return netCDF::NcType(CURRENT_NC_TYPE).getTypeClass();
  }
  static std::string getName()
  {
    return netCDF::NcType(CURRENT_NC_TYPE).getName();
  }
};


  //create dimensions for a 1d container
template <typename InputIterator>
netCDF::NcDim nc_create_dim_1d(InputIterator first,
			       InputIterator last,
			       netCDF::NcFile& dataFile,
			       const std::string& dim_name = std::string("size"))
{
    try
    {
      
        //Resolution of the container
        netCDF::NcDim nc_dim = dataFile.addDim(dim_name, static_cast<std::size_t>(last-first));

        if(nc_dim.isNull())
        {
	  throw std::ios_base::failure(slip::NC_ADD_DIM_ERR);
        }

        return nc_dim;
    }
    catch(std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        dataFile.close();
        exit(1);
    }
}



//create variables for 1d containers
template<typename Type>
netCDF::NcVar nc_create_var_1d(netCDF::NcFile& dataFile,
			       const netCDF::NcDim& nc_dim,
			       const std::string& var_name = std::string("I"))
{
  try
    {

      // Define the netCDF data variables.
      netCDF::NcVar vars = dataFile.addVar(var_name.c_str(),
					   slip::NcTypeInfo<Type>::generate(),
					   nc_dim);
      if(vars.isNull()) 
	{
	  throw std::ios_base::failure(slip::NC_ADD_VAR_ERR);
	}
      // Define units attributes for variables. 
      //...
      return vars;
    }
  catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
}

  //write data for a 1d container
template <typename InputIterator>
void nc_write_data_1d(InputIterator first,
		      InputIterator last,
		      netCDF::NcFile& dataFile,
		      netCDF::NcVar& var, 
		      netCDF::NcDim& nc_dim)
{
  
  try
    {
      if(nc_dim.getSize() != static_cast<std::size_t>(last-first))
	{
	  throw  std::ios_base::failure(slip::NC_MISMATCH_DIM_ERR);
	}
      std::vector<std::size_t> startp = {0};
      std::vector<std::size_t> countp = {nc_dim.getSize()};
      //Writing each component
      var.putVar(startp, countp, first);
      // if (!Var->put(first,
      // 		    nc_dim->size())) 
      //	{
      //	  throw   std::ios_base::failure(NC_WRITE_ERR + file_path_name);
      //	}
    }
  catch(std::exception& e)
    {
      std::cerr<<e.what()<<std::endl;
      dataFile.close();
      exit(1);
    }
}

/*!
** \author Benoit Tremblais <tremblais@sic.univ-poitiers.fr>
** \version 0.0.1
** \date 2008/08/26
** \brief Write a 1d container to a netcdf file.
** \param first InputIterator.
** \param last InputIterator.
** \param file_path_name String of the file path name.
** \param var_name std::string which contains the 
** the name of the variable (feature) default = "I".
** \param dim_name std::string which contains the name of the dimensions, default = "size")
** \param filemode netCDF::NcFile::FileMode write netcdf filemode.
** \param fileformat netCDF::NcFile::FileFormart.
** \par Data format:
**
** \par Example:
** \code
** slip::Vector<float> V(6);
** slip::iota(V.begin(),V.end(),1.0f,1.0f);
** std::cout<<V<<std::endl;
** const std::string var_name = "V";
** const std::string dim_name = "size";
** lip::write_netcdf_1d(V.begin(),V.end(),"V.nc",var_name,dim_name,netCDF::NcFile::FileMode::replace);
** \endcode
** \todo use boost::filesystem
*/
  template <typename InputIterator>
  inline
  void write_netcdf_1d(InputIterator first,
		       InputIterator last,
		       const std::string& file_path_name,
		       const std::string& var_name = std::string("I"),
		       const std::string& dim_name = std::string("size"),
		       const netCDF::NcFile::FileMode filemode = netCDF::NcFile::FileMode::replace,
		       const netCDF::NcFile::FileFormat fileformat = netCDF::NcFile::FileFormat::classic)
  {
    typedef typename std::iterator_traits<InputIterator>::value_type Type;
    netCDF::NcFile dataFile;
  
    try
      {
	//open the file
	dataFile.open(file_path_name.c_str(),
		      filemode,
		      fileformat);
	

	if(dataFile.isNull()) 
	  {
	    throw   std::ios_base::failure(slip::NC_WRITE_ERR + file_path_name);
	  }
	
	//create dimensions  of the container
	netCDF::NcDim nc_dim = slip::nc_create_dim_1d(first,last,
						      dataFile,
						      dim_name);
	//create variables  of the container
	netCDF::NcVar nc_var = slip::nc_create_var_1d<Type>(dataFile,
							    nc_dim,
							    var_name);
	
	//write data of the container
	slip::nc_write_data_1d(first,
			       last,
			       dataFile,
			       nc_var, 
			       nc_dim);
  
	//close file
	dataFile.close();
      }
    catch(std::exception& e)
      {
	std::cerr<<slip::NC_WRITE_ERR + file_path_name<<std::endl;
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
  }



 
netCDF::NcVar nc_get_var_1d(netCDF::NcFile& dataFile,
			    const std::string& var_name = std::string("I"))
  {
    // std::string var_name_tmp = "";
    // //default variables names
    // if (var_name.empty())
    //   {
    // 	var_name_tmp = std::string("I");
    //   }
    // else
    //   {
    // 	var_name_tmp = var_name;
    //   }
  
    // Define the netCDF data variables.
    netCDF::NcVar var = dataFile.getVar(var_name.c_str());
    if (var.isNull())
      {
	throw std::ios_base::failure(slip::NC_GET_VAR_ERR);
      }
        
    // Define units attributes for variables. 
    //...
    return var;
  }

  netCDF::NcDim nc_get_dim_1d(const netCDF::NcVar& var)
  {
    const netCDF::NcDim sizeDim = var.getDim(0);
      
    if (sizeDim.isNull())
      {
	throw   std::ios_base::failure(slip::NC_GET_DIM_ERR);
      }
    return sizeDim;
  }

template <typename Container>
void  nc_read_data_1d(Container& container,
		      const netCDF::NcVar& var,
		      const netCDF::NcDim& sizeDim)
  {
    if(sizeDim.getSize() != container.size())
      {
	throw   std::ios_base::failure(slip::NC_MISMATCH_DIM_ERR);
      }
    std::vector<std::size_t> startp = {0};
    std::vector<std::size_t> countp = {sizeDim.getSize()};
    var.getVar(startp,countp,container.begin());
    if (var.isNull())
	{
	  std::cerr<<"read data error"<<std::endl;
	  throw   std::ios_base::failure(slip::NC_READ_ERR);
	  
	}
  }
 
 
 /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
  ** \version 0.0.1
  ** \date 2008/08/26
  ** \brief Read a 1d container from a netcdf file.
  ** \param container 1d container
  ** \param file_path_name std::string of the file path name.
  ** \param var_name std::string which contains the 
  ** the name of the variables (features).
  ** \par Data format:
  **
  ** \par Example:
  ** \code
  ** slip::Vector<float> V(6);
  ** slip::iota(V.begin(),V.end(),1.0f,1.0f);
  ** std::cout<<V<<std::endl;
  ** std::string var_name = "V";
  ** std::string dim_name = "size";
  ** slip::write_netcdf_1d(V.begin(),V.end(),"V.nc",var_name,dim_name,NcFile::Replace);
  ** slip::Vector<float> V2;
  ** slip::read_netcdf_1d(V2,"V.nc",var_name);
  ** std::cout<<V2<<std::endl;
  ** \endcode
  */
template <typename Container>
inline
void read_netcdf_1d(Container& container,
		    const std::string& file_path_name,
		    const std::string& var_name = std::string("I"),
		    const netCDF::NcFile::FileFormat fileformat =  netCDF::NcFile::FileFormat::classic)
{	
 
  
  //open the file
  netCDF::NcFile dataFile;
  
  
  try
    {
      dataFile.open(file_path_name.c_str(),
		    netCDF::NcFile::FileMode::read,
		    fileformat);
      if (dataFile.isNull()) 
	{
	  throw   std::ios_base::failure(slip::NC_READ_ERR + file_path_name);
	}
  
 
      //get variables
      netCDF::NcVar var = slip::nc_get_var_1d(dataFile,var_name);	

      //get the dimensions of the container
      netCDF::NcDim sizeDim = slip::nc_get_dim_1d(var);
      
      //resize the container
      container.resize(sizeDim.getSize());
  
      //Get data from file
      slip::nc_read_data_1d(container,var,sizeDim);
      
      //close the file
      dataFile.close();
    }
  
  catch(std::exception& e)
      {
	std::cerr<<slip::NC_READ_ERR + file_path_name<<std::endl;
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
}



//create dimensions for a 2d container 
template <typename RandomAccessIterator2d>
std::vector<netCDF::NcDim> nc_create_dim_2d(RandomAccessIterator2d upper_left,
					    RandomAccessIterator2d bottom_right,
					    netCDF::NcFile& dataFile,
					    const std::vector<std::string>& dim_names = {std::string("height"), std::string("width")})
{
  try
    {
      //get dimensions from RandomAccessIterator2d
      typename RandomAccessIterator2d::difference_type size2d = 
      bottom_right - upper_left;
      const std::size_t height = static_cast<std::size_t>(size2d[0]);
      const std::size_t width  = static_cast<std::size_t>(size2d[1]);
      
      std::vector<netCDF::NcDim> nc_dims(2);
     
      nc_dims[0] = dataFile.addDim(dim_names[0], height);
      nc_dims[1] = dataFile.addDim(dim_names[1], width);

      
      if(nc_dims[0].isNull() or nc_dims[1].isNull())
	{
	  throw std::ios_base::failure(slip::NC_WRITE_ERR);
	}
      return nc_dims;
    }
  catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
  
  }

template <typename Type>
netCDF::NcVar nc_create_var_nd(netCDF::NcFile& dataFile,
			       const std::string& var_name = std::string("I"),
			       const std::vector<netCDF::NcDim>& nc_dims = {std::string("height"), std::string("width")})
{
  try
    {
      // Define the netCDF data variables.
      netCDF::NcVar Var = dataFile.addVar(var_name.c_str(),
					  slip::NcTypeInfo<Type>::generate(),
					  nc_dims);

      // Define units attributes for variables. 
      //...
      return Var;
    }
   catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
}

template <typename RandomAccessIterator2d>
void nc_write_data_2d(RandomAccessIterator2d upper_left,
		      RandomAccessIterator2d bottom_right,
		      netCDF::NcFile& dataFile,
		      netCDF::NcVar& var, 
		      const std::vector<netCDF::NcDim>& nc_dims)
{
  try
    {
      typedef typename std::iterator_traits<RandomAccessIterator2d>::value_type value_type;
      	const std::size_t height = static_cast<std::size_t>((bottom_right-upper_left)[0]);
	const std::size_t width  = static_cast<std::size_t>((bottom_right-upper_left)[1]);
	if(width != nc_dims[1].getSize() or height != nc_dims[0].getSize())
	  {
	    throw  std::ios_base::failure(slip::NC_MISMATCH_DIM_ERR);
	  }
	slip::Array2d<value_type> tmp(height,width,upper_left,bottom_right);

	//Writing each component
	// std::vector<std::size_t> startp = {0,0};
	// std::vector<std::size_t> countp = {nc_dims[0].getSize(), nc_dims[1].getSize()};
	//var.putVar(startp, countp, tmp.begin());
	var.putVar(tmp.begin());

	if(var.isNull())
	  {
	    throw   std::ios_base::failure(slip::NC_WRITE_ERR);
	  }

    }
   catch(std::exception& e)
     {
       std::cerr<<e.what()<<std::endl;
       dataFile.close();
       exit(1);
     }
  
}

/*!
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.1
** \date 2008/08/26
** \brief Write a Container2d (slip::Array2d, slip::Matrix, 
** slip::GrayscaleImage, slip::block2d) to a netcdf file.
** \param upper_left RandomAccessIterator2d.
** \param bottom_range RandomAccessIterator2d. 
** \param file_path_name String of the file path name.
** \param var_name std::string which contains the
**  name of the variable (feature)  
** \param dim_names std::vector<std::string> which contains the
**  names of the dimensions 
** \param  filemode NcFile::FileMode netcdf filemode
** \par Data format:
**
** \par Example:
** \code
** slip::Array2d<double> A(5,6);
** slip::iota(A.begin(),A.end(),2.0f,1.0f);
** std::cout<<A<<std::endl;
** std::string var_name_A = "I";
** std::vector<std::string> dim_names_A(0);
** dim_names_A.push_back("width");
** dim_names_A.push_back("height");
**
** slip::write_netcdf_2d(A.upper_left(),A.bottom_right(),
**		         "A.nc",
**		         var_name_A,
**		         dim_names_A,
**		         netCDF::NcFile::FileMode::replace);
** \endcode
*/
  template <typename RandomAccessIterator2d>
  inline
  void write_netcdf_2d(RandomAccessIterator2d upper_left,
		       RandomAccessIterator2d bottom_right,
		       const std::string& file_path_name,
		       const std::string& var_name = std::string("I"),
		       const std::vector<std::string>& dim_names = {std::string("height"), std::string("width")},
		       netCDF::NcFile::FileMode filemode = netCDF::NcFile::FileMode::replace,
		       netCDF::NcFile::FileFormat fileformat =  netCDF::NcFile::FileFormat::classic)
  {
    typedef typename std::iterator_traits<RandomAccessIterator2d>::value_type value_type;
   
    netCDF::NcFile dataFile;
    try
      {
	dataFile.open(file_path_name.c_str(),filemode,fileformat);
	
	std::vector<netCDF::NcDim> nc_dims = slip::nc_create_dim_2d(upper_left,
								    bottom_right,
								    dataFile,
								    dim_names);
	
	netCDF::NcVar Var = slip::nc_create_var_nd<value_type>(dataFile,
							       var_name,
							       nc_dims);
	
	
	slip::nc_write_data_2d(upper_left,
			       bottom_right,
			       dataFile,
			       Var, 
			       nc_dims);
  
	
	//close file
	dataFile.close();
	
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
}



/*!
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.1
** \date 2008/08/26
** \brief Write a Container2d (slip::Array2d, slip::Matrix, 
** slip::GrayscaleImage, slip::block2d) to a netcdf file.
** \param container A Container2d.
** \param file_path_name String of the file path name.
** \par Data format:
**
** \par Example:
** \code
** slip::Array2d<double> A(5,6);
** slip::iota(A.begin(),A.end(),2.0f,1.0f);
** std::cout<<A<<std::endl;
** std::string var_name_A = "I";
** std::vector<std::string> dim_names_A(0);
** dim_names_A.push_back("width");
** dim_names_A.push_back("height");
**
** slip::write_netcdf_2d(A,"A.nc");
** \endcode
*/
 template <typename Container2d>
  inline
  void write_netcdf_2d(const Container2d& container,
		       const std::string& file_path_name,
		       const std::string& var_name = std::string("I"),
		       const std::vector<std::string>& dim_names = {std::string("height"), std::string("width")},
		       netCDF::NcFile::FileMode filemode = netCDF::NcFile::FileMode::replace,
		       netCDF::NcFile::FileFormat fileformat =  netCDF::NcFile::FileFormat::classic)
  {
    slip::write_netcdf_2d(container.upper_left(), container.bottom_right(),
			  file_path_name,
			  var_name,
			  dim_names,
			  filemode,
			  fileformat);
  }





std::vector<netCDF::NcDim> nc_get_dim_nd(netCDF::NcFile& dataFile,
					 netCDF::NcVar& var)
  {
    try
      {
	std::vector<netCDF::NcDim> nc_dims = var.getDims();
	
	return nc_dims;
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
  }


netCDF::NcVar nc_get_var_nd(netCDF::NcFile& dataFile,
			    const std::string& var_name = std::string("I"))
{
  
  try
    {
      
      // Define the netCDF data variables.
      netCDF::NcVar var = dataFile.getVar(var_name.c_str());
      // if (!(Var->is_valid()))
      // 	{
      // 	  throw   std::ios_base::failure(slip::NC_READ_ERR + file_path_name);
      // 	}
        
      // Define units attributes for variables. 
      //...
      return var;
    }
  catch(std::exception& e)
    {
      std::cerr<<e.what()<<std::endl;
      dataFile.close();
      exit(1);
    }
}

 template <typename Containernd>
  void  nc_read_data_nd(Containernd& container,
			netCDF::NcFile& dataFile,
			netCDF::NcVar& var)
  {
    try
      {
	
	var.getVar(container.begin());

      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
  }
 
 /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
  ** \version 0.0.1
  ** \date 2008/08/26
  ** \brief Read a Container2d (slip::Array2d, slip::Matrix, 
  ** slip::GrayscaleImage, slip::block2d) from a netcdf file.
  ** \param container. Container2d.
  ** \param file_path_name String of the file path name
  ** \param var_name. vector of string which contains the 
  ** the name of the variables (features) 
  ** \par Data format:
  **
  ** \par Example:
  ** \code
  ** slip::Array2d<double> A(5,6);
  ** slip::iota(A.begin(),A.end(),2.0f,1.0f);
  ** std::cout<<A<<std::endl;
  ** std::string var_name_A = "data";
  ** std::vector<std::string> dim_names_A(0);
  ** dim_names_A.push_back("width");
  ** dim_names_A.push_back("height");
  ** slip::write_netcdf_2d(A.upper_left(),A.bottom_right(),
  **		"A.nc",
  **		var_name_A,
  **		dim_names_A,
  **		NcFile::Replace);
  **
  ** slip::Array2d<double> A2;
  ** std::string var_name2d = "data";
  ** slip::read_netcdf_2d(A2,"A.nc",var_name_A);
  ** std::cout<<"A2 = \n"<<A2<<std::endl;
  ** \endcode
  */
template <typename Container2d>
inline
void read_netcdf_2d(Container2d& container, 
		    const std::string& file_path_name,
		    const std::string& var_name = "I",
		    const netCDF::NcFile::FileFormat fileformat = netCDF::NcFile::FileFormat::classic)
{	
 
  
  
  netCDF::NcFile dataFile;
  
  try
    {
      dataFile.open(file_path_name.c_str(),
		    netCDF::NcFile::FileMode::read,
		    fileformat);
     
      //get variable
      netCDF::NcVar var = slip::nc_get_var_nd(dataFile,var_name);
	
      //Get the resolution of the container
      std::vector<netCDF::NcDim> nc_dims = slip::nc_get_dim_nd(dataFile,var);
    
      const std::size_t height = nc_dims[0].getSize();
      const std::size_t width  = nc_dims[1].getSize();
      
      container.resize(height,width);
   
      //Get data from file
      slip::nc_read_data_nd(container,dataFile,var);
      //close the file
      dataFile.close();
    }
  
  catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
}




//create variables for multi nd containers
template<typename Type>
std::vector<netCDF::NcVar> nc_create_var_multi_nd(netCDF::NcFile& dataFile,
						  const std::size_t& components,
						  const std::vector<std::string>& var_names, 
						  std::vector<netCDF::NcDim>& nc_dims)
{
  
  try
    {
      
      //const std::size_t size = components;
      std::vector <netCDF::NcVar>  vars;
      std::vector<std::string> var_names_tmp = var_names;
      //default variables names
      if(var_names.empty())
	{
	  var_names_tmp.resize(components);
	  for(std::size_t i = 0; i < components; ++i) 
	    {
	      std::ostringstream ost;
	      ost<<i;
	      var_names_tmp[i] = std::string("I"+ost.str());
	    }
	}
       assert(components == var_names_tmp.size());
      // Define the netCDF data variables.
      for(std::size_t i = 0; i < components; ++i)
	  {	
	    vars.push_back(dataFile.addVar(var_names_tmp[i].c_str(),
					   slip::NcTypeInfo<Type>::generate(),
					   nc_dims));
	  }

	// Define units attributes for variables. 
	//...
    
    return vars;
    }
  catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
  
  }


template <typename MultiContainer2d>
void nc_write_data_multi_2d(const MultiContainer2d& container,
			    netCDF::NcFile& dataFile,
			    std::vector<netCDF::NcVar>& Vars, 
			    std::vector<netCDF::NcDim>& nc_dims)
{
  typedef typename MultiContainer2d::value_type block_value_type;
  //typedef typename block_value_type::value_type value_type;
  assert(Vars.size() == block_value_type::SIZE);
    
  try
    {
     
      const std::size_t components = block_value_type::SIZE;
     
      for(std::size_t i = 0; i < components; ++i)
	{	
	  
	  slip::nc_write_data_2d(container.upper_left(i),
				 container.bottom_right(i),
				 dataFile,
				 Vars[i], 
				 nc_dims);
	 
	}
    }
  catch(std::exception& e)
    {
      std::cerr<<e.what()<<std::endl;
      dataFile.close();
      exit(1);
    }
}

/*!
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.3
** \date 2009/10/20
** \brief Write a DenseVector2dField2D to a netcdf file.
** \param container. A GenericMultiContainer2d (slip::ColorImage, slip::DenseVector2dField2d, slip::DenseVector3dField2d, slip::RegularVector2dField2d, slip::MultispectralImage
** \param file_path_name String of the file path name.
** \param var_names std::vector<std::string> which contains the 
** the names of the variables (features) 
** \param dim_names std::vector<std::string> which contains the 
** the names of the dimensions
** \param filemode NcFile::FileMode netcdf filemode
** \par Data format:
** \par Data format for a NxM MultiContainer2d with C+1 components:
** \code
** dimensions:
**         width = M ;
**	   height = N ;
** variables:
**         float I0(width, height) ;
**	   float I1(width, height) ;
**         ...
**         float IC(width, height) ;
**          
** \endcode
** \par Example:
** \code
** slip::ColorImage<float> C(3,4);
** slip::iota(C.begin(),C.end(),
** slip::Color<float>(1.0f,2.0f,3.0f),
** slip::Color<float>(1.0f,1.0f,1.0f));
** std::cout<<"C = \n"<<C<<std::endl;
** std::vector<std::string> var_names_color;
** var_names_color.push_back("R");
** var_names_color.push_back("G");
** var_names_color.push_back("B");
** std::vector<std::string> dim_names_A(0);
** dim_names_A.push_back("width");
** dim_names_A.push_back("height");
** slip::write_netcdf_multi_2d(C,"Color.nc",var_names_color,dim_names_A,
** NcFile::Replace);
** \endcode
*/
  template <typename MultiContainer2d>
  inline
  void write_netcdf_multi_2d(const MultiContainer2d& container,
			     const std::string& file_path_name,
			     const std::vector<std::string>& var_names = std::vector<std::string>(),
			     const std::vector<std::string>& dim_names = {std::string("height"),std::string("width")},
			     netCDF::NcFile::FileMode filemode = netCDF::NcFile::FileMode::replace,
			     netCDF::NcFile::FileFormat fileformat = netCDF::NcFile::FileFormat::classic)
  {
    typedef typename MultiContainer2d::value_type block_value_type;
    typedef typename block_value_type::value_type value_type;
    netCDF::NcFile dataFile;
    try
      {
	//open the file
	dataFile.open(file_path_name.c_str(),filemode,fileformat);
	//number of components of the container
	const std::size_t components = block_value_type::SIZE;
	//Resolution of the container
	std::vector<netCDF::NcDim> nc_dims = 
	  slip::nc_create_dim_2d(container.upper_left(),
				 container.bottom_right(),
				 dataFile,
				 dim_names);
	//create the variables
	std::vector<netCDF::NcVar> Vars = 
	  slip::nc_create_var_multi_nd<value_type>(dataFile,
						   components,
						   var_names, 
						   nc_dims);
	//Write data
	slip::nc_write_data_multi_2d(container,
				     dataFile,
				     Vars, 
				     nc_dims);


	//close file
	dataFile.close();
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
}






std::vector<netCDF::NcVar> nc_get_var_multi_nd(netCDF::NcFile& dataFile,
					       const std::size_t components,
					       const std::vector<std::string>& var_names)
{
  try
    {
      std::vector <netCDF::NcVar> vars(components);
  

      //default variables names
      std::vector<std::string> var_names_tmp = var_names;
      if (var_names_tmp.empty())
	{
	  var_names_tmp.resize(components);
	  for(std::size_t i = 0; i < components; ++i)
	    {	
	      std::ostringstream ost;
	      ost<<i;
	      var_names_tmp[i] = std::string("I"+ost.str());
	    }
	}
      assert(var_names_tmp.size() == components);

      // Define the netCDF data variables.
  
      for(std::size_t i = 0; i < components; ++i)
	{	
	  vars[i] = dataFile.getVar(var_names_tmp[i].c_str());
	}
        
      // Define units attributes for variables. 
      //...
      return vars;
    }
  
  catch(std::exception& e)
    {
      std::cerr<<e.what()<<std::endl;
      dataFile.close();
      exit(1);
    }
  
}


 template <typename MultiContainer2d>
  void  nc_read_data_multi_2d(MultiContainer2d& container,
			      netCDF::NcFile& dataFile,
			      std::vector<netCDF::NcVar>& nc_vars,
			      std::vector<netCDF::NcDim>& nc_dims)
  {
    try
      {
	typedef typename MultiContainer2d::value_type block_value_type;
	typedef typename block_value_type::value_type value_type;
	
	const std::size_t height = nc_dims[0].getSize();
	const std::size_t width  = nc_dims[1].getSize();

	const std::size_t components = block_value_type::SIZE;
	 
	slip::Array2d<value_type> tmp(height,width);
	for(std::size_t i = 0; i < components; ++i)
	  {
	    slip::nc_read_data_nd(tmp,
				  dataFile,
				  nc_vars[i]);
		
	    std::copy(tmp.begin(),tmp.end(),container.begin(i));
	  }
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
  }

  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
  ** \version 0.0.3
  ** \date 2008/08/25
  ** \brief Read a MultiContainer2d to a netcdf file.
  ** \param container. MultiContainer2d.
  ** \param file_path_name String of the file path name
  ** \param var_name. vector of string which contains the 
  ** the name of the variables (features) 
  ** \par Example:
  ** \code
  ** slip::ColorImage<float> C(3,4);
  ** slip::iota(C.begin(),C.end(),
  ** slip::Color<float>(1.0f,2.0f,3.0f),
  ** slip::Color<float>(1.0f,1.0f,1.0f));
  ** std::cout<<"C = \n"<<C<<std::endl;
  ** std::vector<std::string> var_names_color;
  ** var_names_color.push_back("R");
  ** var_names_color.push_back("G");
  ** var_names_color.push_back("B");
  ** slip::write_netcdf_multi_2d(C,
  **		                 "Color.nc",
  **		                 var_names_color,
  **		                 dim_names_A);
  ** slip::ColorImage<float> C2;
  ** slip::read_netcdf_multi_2d(C2,
  **		                "Color.nc",
  **		                var_names_color);
  ** std::cout<<"C2 = \n"<<C2<<std::endl;
  ** \endcode
  */
template <typename MultiContainer2d>
inline
void read_netcdf_multi_2d(MultiContainer2d& container, 
			  const std::string& file_path_name,
			  const std::vector<std::string>& var_names = std::vector<std::string>(),
			  const netCDF::NcFile::FileFormat fileformat = netCDF::NcFile::FileFormat::classic)
{	
    typedef typename MultiContainer2d::value_type block_value_type;
      
    //open the file
    netCDF::NcFile dataFile;
  
  try
    {
      dataFile.open(file_path_name.c_str(),
		    netCDF::NcFile::FileMode::read,
		    fileformat);
 
      const std::size_t components = block_value_type::SIZE;
      std::vector<netCDF::NcVar> nc_vars = 
	slip::nc_get_var_multi_nd(dataFile,components,var_names);
      
      //Dimension of the container
      std::vector<netCDF::NcDim> nc_dims =
	slip::nc_get_dim_nd(dataFile,nc_vars[0]);

     
      const std::size_t height = nc_dims[0].getSize();
      const std::size_t width  = nc_dims[1].getSize();
      //resize the container
      container.resize(height,width);


      //Get data from file
      slip::nc_read_data_multi_2d(container,dataFile,nc_vars,nc_dims);

 
      dataFile.close();
    }
  
  catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
}

 // /*!
//   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
//   ** \version 0.0.3
//   ** \date 2008/08/25
//   ** \brief Read a MultiContainer2d to a netcdf file.
//   ** \param container. MultiContainer2d.
//   ** \param file_path_name String of the file path name
//   ** the name of the variables (features) 
//   ** \par Data format for a NxM MultiContainer2d with C+1 components:
//   ** \code
//   ** dimensions:
//   **         width = M ;
//   **	     height = N ;
//   ** variables:
//   **         float I0(width, height) ;
//   **	     float I1(width, height) ;
//   **         ...
//   **         float IC(width, height) ;
//   **          
//   ** \endcode
//   ** \par Example:
//   ** \code
//   ** slip::ColorImage<float> C(3,4);
//   ** slip::iota(C.begin(),C.end(),
//   ** slip::Color<float>(1.0f,2.0f,3.0f),
//   ** slip::Color<float>(1.0f,1.0f,1.0f));
//   ** std::cout<<"C = \n"<<C<<std::endl;
//   ** slip::write_netcdf_multi_2d(C,"Color.nc");
//   ** slip::ColorImage<float> C2;
//   ** slip::read_netcdf_multi_2d(C2,"Color.nc");
//   ** std::cout<<"C2 = \n"<<C2<<std::endl;
//   ** \endcode
//   */
// template <typename MultiContainer2d>
// inline
// void read_netcdf_multi_2d(MultiContainer2d& container, 
// 			  const std::string& file_path_name)
// {	
//   std::vector<std::string> var_names;
//   slip::read_netcdf_multi_2d(container, 
// 			     file_path_name,
// 			     var_names);
// }
 

template <typename MultiContainer3d>
std::vector<netCDF::NcDim> nc_create_dim_3d(MultiContainer3d& container,
					    netCDF::NcFile& dataFile,
					    const std::vector<std::string>& dim_names = {std::string("depth"),std::string("height"),std::string("width")})
{
  try
    {
      
      // if(dim_names.empty())
      // 	{
      // 	  dim_names.push_back("depth");
      // 	  dim_names.push_back("heigth");
      // 	  dim_names.push_back("width");
      // 	}
      //Dimensions of the container
      std::vector<netCDF::NcDim> nc_dims(3);
     
      nc_dims[0] = dataFile.addDim(dim_names[0].c_str(),container.slices());
      nc_dims[1] = dataFile.addDim(dim_names[1].c_str(),container.rows());
      nc_dims[2] = dataFile.addDim(dim_names[2].c_str(),container.cols());

      
      return nc_dims;
    }
  catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
  
  }



template <typename RandomAccessIterator3d>
void nc_write_data_3d(RandomAccessIterator3d front_upper_left,
		      RandomAccessIterator3d back_bottom_right,
		      netCDF::NcFile& dataFile,
		      netCDF::NcVar& var, 
		      const std::vector<netCDF::NcDim>& nc_dims)
{
  
  try
    {
      typedef typename std::iterator_traits<RandomAccessIterator3d>::value_type value_type;
      const std::size_t depth = static_cast<std::size_t>((back_bottom_right-front_upper_left)[0]);
      const std::size_t height = static_cast<std::size_t>((back_bottom_right-front_upper_left)[1]);
      const std::size_t width  = static_cast<std::size_t>((back_bottom_right-front_upper_left)[2]);
      if(   depth  != nc_dims[0].getSize()
	 or height != nc_dims[1].getSize()
	 or width  != nc_dims[2].getSize())
	{
	  throw std::ios_base::failure(slip::NC_MISMATCH_DIM_ERR + dataFile.getName());
	}
      slip::Array3d<value_type> tmp(depth,height,width,
				    front_upper_left,back_bottom_right);
      
      //std::vector<std::size_t> startp = {0,0,0};
      //std::vector<std::size_t> countp = {nc_dims[0].getSize(), nc_dims[1].getSize(),nc_dims[2].getSize()};

      //Writing each component
      //Var.putVar(startp, countp, tmp.begin());
     
	var.putVar(tmp.begin());
	
     
    }
  catch(std::exception& e)
    {
      std::cerr<<e.what()<<std::endl;
      dataFile.close();
      exit(1);
    }
}

/*!
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \author Sebastien COUDERT <sebastien.coudert_AT_univ-lille1.fr>
** \version 0.0.2
** \date 2009/10/16
** \brief Write a Container3d (slip::Array3d, slip::Matrix3d, slip::Volume) to a netcdf file.
** \param container. Container3d.
** \param file_path_name String of the file path name.
** \param var_name std::string which contains the 
** the name of the variable (feature) 
** \param dim_names std::vector<std::string> which contains the 
** the names of the dimensions
** \param filemode NcFile::FileMode netcdf filemode
** \par Example:
** \code
** slip::Array3d<double> A3d(2,5,6);
** slip::iota(A3d.begin(),A3d.end(),2.0f,1.0f);
** std::cout<<A3d<<std::endl;
** std::string var_name_A3d = "data";
** std::vector<std::string> dim_names_A3d;
** dim_names_A3d.push_back("depth");
** dim_names_A3d.push_back("width");
** dim_names_A3d.push_back("height");
** slip::write_netcdf_3d(A3d,
**		 "A3d.nc",
**		 var_name_A3d,
**		 dim_names_A3d,
**		 NcFile::Replace);
** \endcode
*/
  template <typename Container3d>
  inline
  void write_netcdf_3d(const Container3d& container,
		       const std::string& file_path_name,
		       const std::string& var_name = "I",
		       const std::vector<std::string>& dim_names =
			 {std::string("depth"), std::string("height"), std::string("width")},
		       const netCDF::NcFile::FileMode filemode = netCDF::NcFile::FileMode::replace,
		       const netCDF::NcFile::FileFormat fileformat = netCDF::NcFile::FileFormat::classic)
  {
    typedef typename Container3d::value_type value_type;
    netCDF::NcFile dataFile;
   
    try
      {
	//open the file
	dataFile.open(file_path_name.c_str(), filemode, fileformat);
    
	//create dimensions
	std::vector<netCDF::NcDim> nc_dims = 
	  slip::nc_create_dim_3d(container,dataFile,dim_names);
	
	//create variable
	netCDF::NcVar var =
	  slip::nc_create_var_nd<value_type>(dataFile,var_name,nc_dims);
	//write data
	slip::nc_write_data_3d(container.front_upper_left(),
			       container.back_bottom_right(),
			       dataFile,
			       var, 
			       nc_dims);


	//close file
	dataFile.close();
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
}

// /*!
// ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
// ** \version 0.0.1
// ** \date 2009/10/20
// ** \brief Write a Container3d (slip::Array3d, slip::Matrix3d, slip::Volume) to a netcdf file.
// ** \param container. Container3d.
// ** \param file_path_name String of the file path name.
// ** \par Example:
// ** \code
// ** slip::Array3d<double> A3d(2,5,6);
// ** slip::iota(A3d.begin(),A3d.end(),2.0f,1.0f);
// ** std::cout<<A3d<<std::endl;
// ** slip::write_netcdf_3d(A3d,"A3d.nc");
// ** \endcode
// */
//   template <typename Container3d>
//   inline
//   void write_netcdf_3d(const Container3d& container,
// 		       const std::string& file_path_name)
//   {
//     std::string var_name;
//     std::vector<std::string> dim_names;
//     slip::write_netcdf_3d(container,file_path_name,
// 			  var_name,dim_names,
// 			  netCDF::NcFile::FileMode::replace);
//   }


/*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
  ** \author Sebastien COUDERT <sebastien.coudert_AT_univ-lille1.fr>
  ** \version 0.0.1
  ** \date 2008/08/26
  ** \brief Read a Container3d (slip::Array3d, slip::Matrix3d, slip::Volume) 
  ** from a netcdf file.
  ** \param container Container3d.
  ** \param file_path_name String of the file path name
  ** \param var_name vector of string which contains the 
  ** the name of the variables (features) 
  ** \par Example:
  ** \code
  ** slip::Array3d<double> A3d(2,5,6);
  ** slip::iota(A3d.begin(),A3d.end(),2.0f,1.0f);
  ** std::cout<<A3d<<std::endl;
  ** std::string var_name_A3d = "data";
  ** std::vector<std::string> dim_names_A3d;
  ** dim_names_A3d.push_back("depth");
  ** dim_names_A3d.push_back("width");
  ** dim_names_A3d.push_back("height");
  **
  ** slip::write_netcdf_3d(A3d,
  **			   "A3d.nc",
  **			    var_name_A3d,
  **			    dim_names_A3d,
  **			    NcFile::Replace);
  **    
  ** slip::Array3d<double> A3d2;
  ** slip::read_netcdf_3d(A3d2,"A3d.nc",var_name_A3d);
  ** std::cout<<"A3d2 = \n"<<A3d2<<std::endl;
  ** \endcode
  **
  */
template <typename Container3d>
inline
void read_netcdf_3d(Container3d& container, 
		    const std::string& file_path_name,
		    const std::string& var_name = "I",
		    const netCDF::NcFile::FileFormat fileformat = netCDF::NcFile::FileFormat::classic)
{	
 
  
  //open the file
  netCDF::NcFile dataFile;
  
  try
    {
      dataFile.open(file_path_name.c_str(),
		    netCDF::NcFile::FileMode::read,
		    fileformat);

      //get variable
      netCDF::NcVar var = slip::nc_get_var_nd(dataFile,var_name);
      
      //Get the resolution of the container
      std::vector<netCDF::NcDim> nc_dims = slip::nc_get_dim_nd(dataFile,var);
      const std::size_t depth  = nc_dims[0].getSize();
      const std::size_t height = nc_dims[1].getSize();
      const std::size_t width  = nc_dims[2].getSize();
      
      container.resize(depth,height,width);

      //Get data from file
      slip::nc_read_data_nd(container,dataFile,var);

     }
  
   catch(std::exception& e)
       {
 	std::cerr<<e.what()<<std::endl;
 	dataFile.close();
 	exit(1);
       }
 }

// /*!
//   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
//   ** \author Sebastien COUDERT <sebastien.coudert_AT_univ-lille1.fr>
//   ** \version 0.0.1
//   ** \date 2008/08/26
//   ** \brief Read a Container3d (slip::Array3d, slip::Matrix3d, slip::Volume) 
//   ** from a netcdf file.
//   ** \param container Container3d.
//   ** \param file_path_name String of the file path name
//   ** \param var_name vector of string which contains the 
//   ** the name of the variables (features) 
//   ** \par Data format for a double 3d container 2x5x6:
//   **  dimensions:
//   **          depth = 2 ;
//   **          heigth = 5 ;
//   **          width = 6 ;
//   **  variables:
//   **          double I(depth, heigth, width) ;
//   **
//   ** \par Example:
//   ** \code
//   ** slip::Array3d<double> A3d(2,5,6);
//   ** slip::iota(A3d.begin(),A3d.end(),2.0f,1.0f);
//   ** std::cout<<A3d<<std::endl;
//   ** slip::write_netcdf_3d(A3d,"A3d.nc");
//   **    
//   ** slip::Array3d<double> A3d2;
//   ** slip::read_netcdf_3d(A3d2,"A3d.nc");
//   ** std::cout<<"A3d2 = \n"<<A3d2<<std::endl;
//   ** \endcode
//   **
//   */
// template <typename Container3d>
// inline
// void read_netcdf_3d(Container3d& container, 
// 		    const std::string& file_path_name)
// {
//   std::string var_name;
//   slip::read_netcdf_3d(container,file_path_name,var_name);
// }


// //create variables for multi 3d containers
// template<typename Type>
// std::vector<netCDF::NcVar> nc_create_var_multi_3d(netCDF::NcFile& dataFile,
// 						  std::size_t components,
// 						  std::vector<std::string>& var_names, 
// 						  const std::vector<netCDF::NcDim>& nc_dims, 
// 						  const std::string& file_path_name = "file name unknown")
// {
//   try
//     {
     

//       std::vector <netCDF::NcVar>  Vars;
//       //default variables names
//       if(var_names.empty())
// 	{	
// 	  var_names.resize(components);
// 	  for(std::size_t i = 0; i < components; ++i) 
// 	    {
// 	      std::ostringstream ost;
// 	      ost<<i;
// 	      var_names[i] = std::string("I"+ost.str());
// 	    }
// 	}
//       assert(var_names.size() == components);
//       // Define the netCDF data variables.
//       for(std::size_t i = 0; i < components; ++i)
// 	  {	
// 	    Vars.push_back(dataFile.addVar(var_names[i].c_str(),
// 					   slip::NcTypeInfo<Type>::getTypeClass(),
// 					   nc_dims));
// 	  }

// 	// Define units attributes for variables. 
// 	//...
    
//     return Vars;
//     }
//   catch(std::exception& e)
//       {
// 	std::cerr<<e.what()<<std::endl;
// 	dataFile.close();
// 	exit(1);
//       }
  
//   }

//write data for multi 3d containers
template <typename MultiContainer3d>
void nc_write_data_multi_3d(const MultiContainer3d& container,
			    netCDF::NcFile& dataFile,
			    std::vector<netCDF::NcVar>& vars, 
			    std::vector<netCDF::NcDim>& nc_dims)
{
  typedef typename MultiContainer3d::value_type value_type;
  typedef typename MultiContainer3d::block_value_type block_value_type;
  try
    {
     if(   container.slices()  != nc_dims[0].getSize()
	or container.rows()    != nc_dims[1].getSize()
	or container.cols()    != nc_dims[2].getSize())
	  {
	    throw  std::ios_base::failure(slip::NC_MISMATCH_DIM_ERR);
	  }
     
     slip::Array3d<block_value_type> tmp(container.slices(),
					 container.rows(),
					 container.cols());
      
      const std::size_t components = value_type::SIZE;
      for(std::size_t i = 0; i < components; ++i)
	{	
	  std::copy(container.begin(i),container.end(i),tmp.begin());
	 
	  vars[i].putVar(tmp.begin());
	  
	}
    }
  catch(std::exception& e)
    {
      std::cerr<<e.what()<<std::endl;
      dataFile.close();
      exit(1);
    }
}


/*!
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \author Sebastien COUDERT <sebastien.coudert_AT_univ-lille1.fr>
** \version 0.0.3
** \date 2009/10/20
** \brief Write a DenseVector3dField3D to a netcdf file.
** \param field DenseVector3dField3D.
** \param file_path_name String of the file path name.
** \param var_names std::vector<std::string> which contains the 
** the names of the variables (features) 
** \param dim_names std::vector<std::string> which contains the 
** the names of the dimensions
** \param filemode NcFile::FileMode netcdf filemode
** \par Data format:
**
** \par Example:
** \code
** slip::DenseVector3dField3d<double> RVF(2,5,4);
** slip::iota(RVF.begin(),RVF.end(),
** slip::Vector3d<double>(1.0,2.0,3.0),
** slip::Vector3d<double>(1.0,1.0,1.0));
** std::cout<<"RVF = \n"<<RVF<<std::endl;
** std::vector<std::string> var_names_rvf;
** var_names_rvf.push_back("U");
** var_names_rvf.push_back("V");
** var_names_rvf.push_back("W");
** std::vector<std::string> dim_names_rvf;
** dim_names_rvf.push_back("depth");
** dim_names_rvf.push_back("width");
** dim_names_rvf.push_back("height");
** slip::write_netcdf_multi_3d(RVF,
**		               "RVF.nc",
**		               var_names_rvf,
**	                       dim_names_rvf,
**		               NcFile::Replace);
** \endcode
*/
  template <typename MultiContainer3d>
  inline
  void write_netcdf_multi_3d(const MultiContainer3d& container,
			     const std::string& file_path_name,
			     const std::vector<std::string>& var_names = std::vector<std::string>(),
			     const std::vector<std::string>& dim_names = {std::string("depth"), std::string("height"), std::string("width")},
			     const netCDF::NcFile::FileMode filemode = netCDF::NcFile::FileMode::replace,
			     const netCDF::NcFile::FileFormat fileformat = netCDF::NcFile::FileFormat::classic)
  {
    typedef typename MultiContainer3d::value_type value_type;
    typedef typename MultiContainer3d::block_value_type block_value_type;

    netCDF::NcFile dataFile;
    try
      {
	//open the file
	dataFile.open(file_path_name.c_str(),filemode,fileformat);
	
	//const std::size_t components = value_type::SIZE;
	//create dimensions
	std::vector<netCDF::NcDim> nc_dims = 
	  slip::nc_create_dim_3d(container,
				 dataFile,
				 dim_names);
	const std::size_t components = value_type::SIZE;
	//create variables
	std::vector<netCDF::NcVar> nc_vars = 
	  nc_create_var_multi_nd<block_value_type>(dataFile,
						   components,
						   var_names, 
						   nc_dims);

	//Writing data
	nc_write_data_multi_3d(container,
			       dataFile,
			       nc_vars, 
			       nc_dims);
  

	//close file
	dataFile.close();
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
}

// /*!
// ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
// ** \author Sebastien COUDERT <sebastien.coudert_AT_univ-lille1.fr>
// ** \version 0.0.3
// ** \date 2009/10/20
// ** \brief Write a MultiContainer3d (slip::DenseVector3dField3d, 
// ** slip::RegularVector3dField3d) to a netcdf file.
// ** \param container Container3d.
// ** \param file_path_name String of the file path name.
// ** \par Data format for a double PxNxM Container3d of C+1 components:
// ** \code
// ** dimensions:
// **	depth = P ;
// **	heigth = M ;
// **	width = N ;
// **  variables:
// **      double I0(depth, heigth, width) ;
// **      double I1(depth, heigth, width) ;
// **      ...
// **      double IC(depth, heigth, width) ;
// ** \endcode
// ** \par Example:
// ** \code
// ** slip::DenseVector3dField3d<double> RVF(2,5,4);
// ** slip::iota(RVF.begin(),RVF.end(),
// ** slip::Vector3d<double>(1.0,2.0,3.0),
// ** slip::Vector3d<double>(1.0,1.0,1.0));
// ** std::cout<<"RVF = \n"<<RVF<<std::endl;
// ** slip::write_netcdf_multi_3d(RVF,
// **		               "RVF.nc");
// ** \endcode
// */
//   template <typename MultiContainer3d>
//   inline
//   void write_netcdf_multi_3d(const MultiContainer3d& container,
// 			     const std::string& file_path_name)
//   {
//     std::vector<std::string> var_names;
//     std::vector<std::string> dim_names;
//     slip::write_netcdf_multi_3d(container,file_path_name,
// 				var_names,
// 				dim_names,
// 				netCDF::NcFile::FileMode::replace);
//   }


template <typename MultiContainer3d>
  void  nc_read_data_multi_3d(MultiContainer3d& container,
			      netCDF::NcFile& dataFile,
			      std::vector<netCDF::NcVar>& nc_vars,
			      std::vector<netCDF::NcDim>& nc_dims)
  {
    try
      {
	typedef typename MultiContainer3d::value_type block_value_type;
	typedef typename block_value_type::value_type value_type;
	const std::size_t depth  = nc_dims[0].getSize();
	const std::size_t height = nc_dims[1].getSize();
	const std::size_t width  = nc_dims[2].getSize();

	 
	slip::Array3d<value_type> tmp(depth,height,width);
	const std::size_t components = block_value_type::SIZE;
	for(std::size_t i = 0; i < components; ++i)
	  {
	    slip::nc_read_data_nd(tmp,
				  dataFile,
				  nc_vars[i]);
		
	    std::copy(tmp.begin(),tmp.end(),container.begin(i));
	  }
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
  }


 /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
  ** \author Sebastien COUDERT <sebastien.coudert_AT_univ-lille1.fr>
  ** \version 0.0.2
  ** \date 2008/08/25
  ** \brief Read a MultiContainer3d (slip::DenseVector3dField3d, 
** slip::RegularVector3dField3d) to a netcdf file.to from a netcdf file.
  ** \param container MultiContainer3d.
  ** \param file_path_name String of the file path name
  ** \param var_name std::vector<std::string> which contains the 
  ** the names of the variables (features) 
  ** \par Example:
  ** \code
  ** slip::DenseVector3dField3d<double> RVF(2,5,4);
  ** slip::iota(RVF.begin(),RVF.end(),
  ** slip::Vector3d<double>(1.0,2.0,3.0),
  ** slip::Vector3d<double>(1.0,1.0,1.0));
  ** std::cout<<"RVF = \n"<<RVF<<std::endl;
  ** std::vector<std::string> var_names_rvf;
  ** var_names_rvf.push_back("U");
  ** var_names_rvf.push_back("V");
  ** var_names_rvf.push_back("W");
  ** std::vector<std::string> dim_names_rvf;
  ** dim_names_rvf.push_back("depth");
  ** dim_names_rvf.push_back("width");
  ** dim_names_rvf.push_back("height");
  ** slip::write_netcdf_multi_3d(RVF,
  **			       "RVF.nc",
  **			       var_names_rvf,
  **			       dim_names_rvf,
  **			       NcFile::Replace);
  ** slip::DenseVector3dField3d<double> RVF2;
  ** slip::read_netcdf_multi_3d(RVF2,"RVF.nc",var_names_rvf);
  **  std::cout<<"RVF2 = \n"<<RVF2<<std::endl;
  ** \endcode
  */
template <typename MultiContainer3d>
inline
void read_netcdf_multi_3d(MultiContainer3d& container, 
			  const std::string& file_path_name,
			  const std::vector<std::string>& var_names = std::vector<std::string>(),
			  const netCDF::NcFile::FileFormat fileformat=netCDF::NcFile::FileFormat::classic)
{	
 
  
  //open the file
  netCDF::NcFile dataFile;
 
  
  try
    {
      dataFile.open(file_path_name.c_str(),
		    netCDF::NcFile::FileMode::read,
		    fileformat);
     
  
      const std::size_t size = MultiContainer3d::COMPONENTS;
      std::vector<netCDF::NcVar> nc_vars = 
	slip::nc_get_var_multi_nd(dataFile,size,var_names);
       //Dimensions of the container
      std::vector<netCDF::NcDim> nc_dims = 
	slip::nc_get_dim_nd(dataFile,nc_vars[0]);
     
      const std::size_t depth  = nc_dims[0].getSize();
      const std::size_t height = nc_dims[1].getSize();
      const std::size_t width  = nc_dims[2].getSize();
      //resize the container
      container.resize(depth,height,width);

      //Get data from file
      slip::nc_read_data_multi_3d(container,
				  dataFile,
				  nc_vars,
				  nc_dims);
  
          
      dataFile.close();
    }
  
  catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
}


// /*!
//   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
//   ** \version 0.0.2
//   ** \date 2009/10/20
//   ** \brief Read a MultiContainer3d (slip::DenseVector3dField3d, 
// ** slip::RegularVector3dField3d) to a netcdf file.to from a netcdf file.
//   ** \param container MultiContainer3d.
//   ** \param file_path_name String of the file path name
//   ** \param var_name std::vector<std::string> which contains the 
//   ** the names of the variables (features) 
//   ** \par Data format for a double PxNxM Container3d of C+1 components:
//   ** \code
//   ** dimensions:
//   **	depth = P ;
//   **	heigth = M ;
//   **	width = N ;
//   **  variables:
//   **      double I0(depth, heigth, width) ;
//   **      double I1(depth, heigth, width) ;
//   **      ...
//   **      double IC(depth, heigth, width) ;
//   ** \endcode
//   ** \par Example:
//   ** \code
//    ** slip::DenseVector3dField3d<double> RVF(2,5,4);
//   ** slip::iota(RVF.begin(),RVF.end(),
//   ** slip::Vector3d<double>(1.0,2.0,3.0),
//   ** slip::Vector3d<double>(1.0,1.0,1.0));
//   ** std::cout<<"RVF = \n"<<RVF<<std::endl;
//   ** slip::write_netcdf_multi_3d(RVF,"RVF2.nc");
//   ** slip::DenseVector3dField3d<double> RVF3;
//   ** slip::read_netcdf_multi_3d(RVF3,"RVF2.nc");
//   ** std::cout<<"RVF3 = \n"<<RVF3<<std::endl;
//   ** \endcode
//   */
// template <typename MultiContainer3d>
// inline
// void read_netcdf_multi_3d(MultiContainer3d& container, 
// 			  const std::string& file_path_name)
// {	
//   std::vector<std::string> var_names;
//   slip::read_netcdf_multi_3d(container,file_path_name,var_names);
// }









/*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
  ** \version 0.0.1
  ** \date 2009/10/20
  ** \brief Write a slip::Polynomial to a netcdf file
  ** \param P a slip::Polynomial<T>
  ** \param file_path_name netcdf file path name
  ** \param filemode NcFile::FileMode netcdf filemode (NcFile::Replace by default)
  ** \par Example:
  ** \code
  ** double d[] = {1.2,3.4,5.5,6.6,7.7,8.8,9.9};
  ** slip::Polynomial<double> P(6,d);
  ** std::cout<<"degree = "<<P.degree()<<std::endl;
  ** std::cout<<"P = \n"<<P<<std::endl;
  ** slip::write_netcdf(P,"P.nc");
  ** 
  ** slip::Polynomial<double> P2;
  ** slip::read_netcdf(P2,"P.nc");
  ** std::cout<<"P2 = \n"<<P2<<std::endl;
  ** std::cout<<"degree(P2) = "<<P2.degree()<<std::endl;
  ** \endcode
  */
template<typename T>
void write_netcdf(const slip::Polynomial<T>& P,
		  const std::string& file_path_name,
		  const netCDF::NcFile::FileMode filemode = netCDF::NcFile::FileMode::replace,
		  const netCDF::NcFile::FileFormat fileformat = netCDF::NcFile::FileFormat::classic)
{
  netCDF::NcFile dataFile;
  
  
  try
    {
      dataFile.open(file_path_name.c_str(),filemode,fileformat);
      
    
      //create dimensions
      const std::string dim_name = "size";
      netCDF::NcDim nc_dim =  slip::nc_create_dim_1d(P.begin(),
						     P.end(),
						     dataFile,
						     dim_name);
    
      //create var
      std::string var_name = "coefficients";
      netCDF::NcVar var_coefficients =
	slip::nc_create_var_1d<T>(dataFile,
				  nc_dim,
				  var_name);

      

      //write data
      slip::nc_write_data_1d(P.begin(),
			     P.end(),
			     dataFile,
			     var_coefficients, 
			     nc_dim);
      
    }
   catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	dataFile.close();
	exit(1);
      }
}

/*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
  ** \version 0.0.1
  ** \date 2009/10/20
  ** \brief Read a slip::Polynomial from a netcdf file
  ** \param P a slip::Polynomial<T>
  ** \param file_path_name netcdf file path name
  ** \par Example:
  ** \code
  ** double d[] = {1.2,3.4,5.5,6.6,7.7,8.8,9.9};
  ** slip::Polynomial<double> P(6,d);
  ** std::cout<<"degree = "<<P.degree()<<std::endl;
  ** std::cout<<"P = \n"<<P<<std::endl;
  ** slip::write_netcdf(P,"P.nc");
  ** 
  ** slip::Polynomial<double> P2;
  ** slip::read_netcdf(P2,"P.nc");
  ** std::cout<<"P2 = \n"<<P2<<std::endl;
  ** std::cout<<"degree(P2) = "<<P2.degree()<<std::endl;
  ** \endcode
  */
template<typename T>
void read_netcdf(slip::Polynomial<T>& P,
		 const std::string& file_path_name,
		 const netCDF::NcFile::FileFormat fileformat = netCDF::NcFile::FileFormat::classic)
{
  //open the file
  netCDF::NcFile dataFile;
  
  try
    {
      dataFile.open(file_path_name.c_str(),
		    netCDF::NcFile::FileMode::read,
		    fileformat);
    
      //get variables
      const std::string var_name = "coefficients";
      netCDF::NcVar var = slip::nc_get_var_1d(dataFile,var_name);	

      //get the dimensions of the container
      netCDF::NcDim sizeDim = slip::nc_get_dim_1d(var);
      
      //resize the container
      slip::Polynomial<T> tmp(sizeDim.getSize()-1);
	
  
      //Get data from file
      slip::nc_read_data_1d(tmp,var,sizeDim);
      P = tmp;
      
      //close the file
      dataFile.close();
    }
  catch(std::exception& e)
    {
      std::cerr<<e.what()<<std::endl;
      dataFile.close();
      exit(1);
    }
}


/*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
  ** \version 0.0.1
  ** \date 2009/10/20
  ** \brief Write a slip::Polynomial to a netcdf file
  ** \param P a slip::MultivariatePolynomial<T,DIM>
  ** \param file_path_name netcdf file path name
  ** \param filemode NcFile::FileMode netcdf filemode (NcFile::Replace by default)
  ** \par Example:
  ** \code
  ** slip::Vector<double> Coeff(10);
  ** slip::iota(Coeff.begin(),Coeff.end(),4.0,1.0);
  ** slip::MultivariatePolynomial<double,3> Pord2(2,Coeff.begin(),Coeff.end());
  ** std::cout<<Pord2<<std::endl;
  ** std::cout<<"size = "<<Pord2.size()<<std::endl;
  ** std::cout<<"degree = "<<Pord2.degree()<<std::endl;
  ** std::cout<<"total_nb_coeff = "<<Pord2.total_nb_coeff()<<std::endl;
  ** slip::write_netcdf(Pord2,"Pmulti.nc");
  ** \endcode
  */
template<typename T, std::size_t DIM>
void write_netcdf(const slip::MultivariatePolynomial<T,DIM>& P,
		  const std::string& file_path_name,
		  const netCDF::NcFile::FileMode filemode = netCDF::NcFile::FileMode::replace,
		  const netCDF::NcFile::FileFormat fileformat = netCDF::NcFile::FileFormat::classic)
{
  netCDF::NcFile dataFile;
 
  
  try
    {
      
      dataFile.open(file_path_name.c_str(),
		    filemode,
		    fileformat);
     
      slip::Array<T> tmp(P.total_nb_coeff());
      P.all_coeff(tmp.begin(),tmp.end());
      //create dimensions
      const std::string dim_name = "size";
      netCDF::NcDim nc_dim =  slip::nc_create_dim_1d(tmp.begin(),
						tmp.end(),
						dataFile,
						dim_name);
    
      //create var
      const std::string var_name = "coefficients";
      netCDF::NcVar var_coefficients =
	slip::nc_create_var_1d<T>(dataFile,
				  nc_dim,
				  var_name);

      

      //write data
      slip::nc_write_data_1d(tmp.begin(),
			     tmp.end(),
			     dataFile,
			     var_coefficients, 
			     nc_dim);
    }
   catch(std::exception& e)
    {
      std::cerr<<e.what()<<std::endl;
      dataFile.close();
      exit(1);
    }
}


/*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
  ** \version 0.0.1
  ** \date 2009/10/20
  ** \brief Read a slip::Polynomial from a netcdf file
  ** \param P a slip::Polynomial<T>
  ** \param file_path_name netcdf file path name
  ** \par Example:
  ** \code
  ** double d[] = {1.2,3.4,5.5,6.6,7.7,8.8,9.9};
  ** slip::Polynomial<double> P(6,d);
  ** std::cout<<"degree = "<<P.degree()<<std::endl;
  ** std::cout<<"P = \n"<<P<<std::endl;
  ** slip::write_netcdf(P,"P.nc");
  ** 
  ** slip::Polynomial<double> P2;
  ** slip::read_netcdf(P2,"P.nc");
  ** std::cout<<"P2 = \n"<<P2<<std::endl;
  ** std::cout<<"degree(P2) = "<<P2.degree()<<std::endl;
  ** \endcode
  */
template<typename Container>
void read_netcdf_multi_polynomial(Container& container,
				  const std::string& file_path_name,
				  const netCDF::NcFile::FileFormat fileformat = netCDF::NcFile::FileFormat::classic)
{
  //open the file
  netCDF::NcFile dataFile;
  
  try
    {
      dataFile.open(file_path_name.c_str(),
		    netCDF::NcFile::FileMode::read,
		    fileformat);
     
      //get variables
      const std::string var_name = "coefficients";
      netCDF::NcVar Var = slip::nc_get_var_1d(dataFile,var_name);	

      //get the dimensions of the container
      netCDF::NcDim sizeDim = slip::nc_get_dim_1d(Var);
      
      //resize the container
      container.resize(sizeDim.getSize());
	
  
      //Get data from file
      slip::nc_read_data_1d(container,Var,sizeDim);

      //close the file
      dataFile.close();

    }
   catch(std::exception& e)
    {
      std::cerr<<e.what()<<std::endl;
      dataFile.close();
      exit(1);
    }
}
}//namespace ::slip
#endif //VIVE3D_NETCDF_IO_HPP



