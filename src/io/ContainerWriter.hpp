/*!
 ** \file ContainerWriter.hpp
 ** \brief Contains the container writer base class
 ** \version Fluex 1.0
 ** \date 2013/04/09
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef CONTAINERWRITER_HPP_
#define CONTAINERWRITER_HPP_

#include <string>

namespace slip{

/*!
 ** @ingroup IO
 ** \version Fluex 1.0
 ** \date 2013/04/09
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief %ContainerWriter is the base class of the writer classes. All the writers are working the same way:
 **  - One output file is defined by its name.
 **  - This file can be filled by one or many containers depending on its format.
 **  - The write() method return a boolean that indicates if the file has be completely filled or not.
 **  .
 ** \param Container should be a type of container that matches the file format to write (an image for jpg format...)
 ** \param T the type of the container data.
 */
template<class Container, typename T>
class ContainerWriter {
public:

	typedef Container container_type;
	typedef T value_type;
	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** Default constructor
	 */
	ContainerWriter()
	:
		output_filename_("")
	{}

	/*!
	 ** Constructor with parameters.
	 ** \param output_filename is the name of the output file
	 ** where the data of the container will be saved
	 */
	ContainerWriter(std::string output_filename)
	:
		output_filename_(output_filename)
	{}

	/*!
	 ** Destructor
	 */
	virtual ~ContainerWriter(){}

	/*@} End Constructors */

	/**
	 ** \name Getters and setters
	 */
	/*@{*/
	/*!
	 * \brief return the output file name.
	 */
	const std::string& get_output_filename() const;
	/*!
	 * \brief set the output file name.
	 */
	void set_output_filename(const std::string& output_filename);
	/*@} End Getters and setters */

	/*!
	 ** \brief virtual write function. This function write the data within the output
	 ** file after the previous one. When last is set to true, the output file is closed.
	 ** \param data is a container to save.
	 ** \return false if the file has been completely filled.
	 */
	virtual int write(const container_type & data) = 0;

protected:

	std::string output_filename_;
};

} /*! namespace slip */

namespace slip {

template<class Container, typename T>
inline
const std::string& ContainerWriter<Container, T>::get_output_filename() const {
	return output_filename_;
}

template<class Container, typename T>
inline void ContainerWriter<Container, T>::set_output_filename(
		const std::string& output_filename) {
	output_filename_ = output_filename;
}

} /*! namespace slip */

#endif /* CONTAINERWRITER_HPP_ */
