/*!
 ** \file JpegWriter.hpp
 ** \brief containers writer from jpeg files
 ** It includes jpeglib.h from libjpeg
 ** \version Fluex 1.0
 ** \date 2013/04/09
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef JPEGWRITER_HPP_
#define JPEGWRITER_HPP_

#include <iostream>
#include <sstream>

#include "JpegDef.hpp"
#include "error.hpp"
#include "ContainerWriter.hpp"


template <typename T>
struct jpg_static_cast_func
{
	template <typename T1> // T1 models type statically convertible to T
	T operator()(const T1& x) const {
	  return static_cast<T>(x);
	}
};

namespace slip {

/*!
 ** @ingroup IO
 ** \version Fluex 1.0
 ** \date 2013/04/09
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief JpegWriter is the jpeg image writer.
 ** \param Container2d is the type of the containers that will be written in the jpeg file.
 ** It should be a 2 dimensional containers with [] operator, width() and height() methods defined.
 ** \param T the type of the container data.
 ** \param Nb_components the number of image components (3 for a color image or 1 for a gray scale image).
 ** \note The color space is RGB for color image and GRAYSCALE for grayscale images.
 */
template<class Container2d, typename T, std::size_t Nb_components>
class JpegWriter: public ContainerWriter<Container2d, T> {
public:
	typedef Container2d container_type;
	typedef T value_type;
	typedef ContainerWriter<Container2d, T> base;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** Default constructor
	 */
	JpegWriter()
	:base(),cinfo(),jerr(),outfile(NULL),row_pointer(NULL),row_stride(0),
	 finished(0),total_nb_row(0),initialized(false),output_image_width_(0),
	 output_image_height_(0){}

	/*!
	 ** Constructor with parameters.
	 ** \param output_filename is the name of the output file
	 ** where the data of the container will be saved
	 ** \param output_image_width the total image width
	 ** \param output_image_height the total image height
	 ** \param quality is the compression rate (default is 90%)
	 */
	JpegWriter(std::string output_filename, std::size_t output_image_width, std::size_t output_image_height, int quality = 90)
	:base(output_filename),cinfo(),jerr(),outfile(NULL),row_pointer(NULL),row_stride(0),
	 finished(0),total_nb_row(0),initialized(false),output_image_width_(output_image_width),
	 output_image_height_(output_image_height){
		initialize(quality);
	}

	/*!
	 ** Destructor
	 */
	virtual ~JpegWriter(){
		release();
	}

	/*@} End Constructors */

	/*!
	 ** \brief initialized the writing process. Has to be called before the first write() call of a given image.
	 ** \param output_image_width the total image width
	 ** \param output_image_height the total image height
	 ** \param quality is the compression rate (default is 90%)
	 */
	void initialize(std::size_t output_image_width, std::size_t output_image_height, int quality = 90);


	/*!
	 ** \brief release the writing process. Has to be called when an image has been fully written.
	 */
	void release();

	/*!
	 ** \brief write function. This function write the data within the output
	 ** file after the previous one. When last is set to true, the output file is closed.
	 ** \param data is the image container to save. Warning, the container should be a 2d container
	 ** with [] operator, width() and height() methods defined.
	 ** \return false if the file has been completely filled.
	 ** \note Whatever the type of the data in the container, a static_cast is done for getting unsigned char.
	 ** Be sure that the dynamic of the data is conform with this cast.
	 ** \note The color space is RGB for color image and GRAYSCALE for grayscale images.
	 */
	int write(const container_type & data);

private:
	jpeg_compress_struct cinfo; /** JPEG compression parameters and pointers to working space */
	my_error_mgr jerr;  /** We use our private extension JPEG error handler. */
	FILE * outfile;		/** destination file */
	JSAMPROW* row_pointer; /** pointer to JSAMPLE row[s] */
	int row_stride;		/** physical row width in image buffer */
	bool finished; /** flag indicating the end of the writting process */
	std::size_t total_nb_row;
	bool initialized;
	std::size_t output_image_width_;
	std::size_t output_image_height_;

	void initialize(int quality = 90);
};

} /*! namespace slip */

namespace slip {

template<class Container2d, typename T, std::size_t Nb_components>
inline
void JpegWriter<Container2d,T,Nb_components>::initialize(std::size_t output_image_width, std::size_t output_image_height, int quality){
	output_image_width_ = output_image_width;
	output_image_height_ = output_image_height;
	initialize(quality);
}


template<class Container2d, typename T, std::size_t Nb_components>
inline
void JpegWriter<Container2d,T,Nb_components>::initialize(int quality){
	if(!initialized){
		if (Nb_components != 1 && Nb_components != 3){
			std::ostringstream err;
			err << FILE_WRITE_ERROR << this->output_filename_ << " | Nb_components should be 1 or 3.";
			slip::slip_exception exc(std::string("slip"), std::string("JpegWriter::initialize()"), err.str());
			throw (exc);
		}

		/* Step 1: allocate and initialize JPEG compression object */

		cinfo.err = jpeg_std_error(&jerr.pub);
		jerr.pub.error_exit = my_error_exit;

		/* Establish the setjmp return context for my_error_exit to use. */
		if (setjmp(jerr.setjmp_buffer)) {
			/* If we get here, the JPEG code has signaled an error.
			 * We need to clean up the JPEG object, close the input file, and throw an exception
			 */
			char buffer[JMSG_LENGTH_MAX];
			/* Create the message */
			(*cinfo.err->format_message) (reinterpret_cast<j_common_ptr>(&cinfo), buffer);
			jpeg_destroy_compress(&cinfo);
			if (outfile != NULL)
				fclose(outfile);
			std::ostringstream err;
			err << FILE_WRITE_ERROR << this->output_filename_ << " | JPEG code has signaled the error : "
					<< buffer;
			slip::slip_exception exc(std::string("slip"), std::string("JpegWriter::initialize()"), err.str());
			//std::cerr << ">>>>" << err.str() << "\n";
			throw exc;
		}

		jpeg_create_compress(&cinfo);

		/* Step 2: specify data destination (eg, a file) */

		if ((outfile = fopen(this->output_filename_.c_str(), "wb")) == NULL) {
			std::ostringstream err;
			err << FILE_OPEN_ERROR << this->output_filename_;
			slip::slip_exception exc(std::string("slip"), std::string("JpegWriter::initialize()"), err.str());
			throw (exc);
		}
		slip_jpeg_stdio_dest(&cinfo, outfile);

		cinfo.image_width = output_image_width_; 	/* image width and height, in pixels */
		cinfo.image_height = output_image_height_;
		cinfo.input_components = Nb_components;		/* # of color components per pixel */
		if (Nb_components == 3)
			cinfo.in_color_space = JCS_RGB; 	/* colorspace of input image */
		else if(Nb_components == 1)
			cinfo.in_color_space = JCS_GRAYSCALE;

		jpeg_set_defaults(&cinfo);
		jpeg_set_quality(&cinfo, quality, TRUE /* limit to baseline-JPEG values */);

		/* Step 4: Start compressor */
		jpeg_start_compress(&cinfo, TRUE);

		/* Step 5: initialized parameters*/
		row_stride = output_image_width_ * Nb_components;	/* JSAMPLEs per row in image_buffer */
		row_pointer = new JSAMPROW[1];
		initialized = true;
	}
}



template<class Container2d, typename T, std::size_t Nb_components>
inline
int JpegWriter<Container2d,T,Nb_components>::write(const container_type & data){
	if(!initialized){
		std::ostringstream err;
		err << FILE_WRITE_ERROR << this->output_filename_ << " | The writer to be initialized before writing.";
		slip::slip_exception exc(std::string("slip"), std::string("JpegWriter::write()"), err.str());
		throw (exc);
	}
	if (finished){
		return 0;
	}

	total_nb_row += data.height();

	if(output_image_height_ < total_nb_row){
		std::ostringstream err;
		err << FILE_WRITE_ERROR << this->output_filename_ << " | Allocation problem during jpg writing.";
		slip::slip_exception exc(std::string("slip"), std::string("JpegWriter::write()"), err.str());
		throw (exc);
	}

	for(std::size_t j=0; j<data.height(); ++j){
		row_pointer[0] = new JSAMPLE[row_stride];	/* pointer to JSAMPLE row[s] */
		const T * data_ptr_j  = reinterpret_cast<const T *>(data[j]);
		std::transform(data_ptr_j, data_ptr_j + row_stride,
				row_pointer[0], jpg_static_cast_func<unsigned char>());
		(void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
		delete[] row_pointer[0];
	}

	if (total_nb_row == output_image_height_)
		finished = true;

	return (!finished);
}

template<class Container2d, typename T, std::size_t Nb_components>
inline
void JpegWriter<Container2d,T,Nb_components>::release(){
	if(initialized){
		/* Step 6: Finish compression */
		if(finished)
			jpeg_finish_compress(&cinfo);

		/* Step 7: release JPEG compression object */
		jpeg_destroy_compress(&cinfo);
		if (outfile != NULL)
			fclose(outfile);
		delete[] row_pointer;
		row_stride = 0;
		finished = false;
		total_nb_row = 0;
		output_image_width_ = 0;
		output_image_height_ = 0;
		initialized = false;
	}
}

} /*! namespace slip */
#endif /* JPEGWRITER_HPP_ */
