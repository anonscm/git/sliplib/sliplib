/*!
 ** \file ContainerReader.hpp
 ** \brief Contains the container reader base class
 ** \version Fluex 1.0
 ** \date 2013/04/05
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef CONTAINERREADER_HPP_
#define CONTAINERREADER_HPP_

#include <string>

namespace slip {

/** @defgroup IO
 *  Group of objects that are related to IO functionalities.
 *  @{
 */
/*!
 ** \version Fluex 1.0
 ** \date 2013/04/05
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief %ContainerReader is the base class of the readers classes. All readers are working the same way:
 ** 	- One input file is defined by its name.
 ** 	- The file data can be read in a single container or divided into Nb_blocks different containers.
 ** 	- The read() method return a boolean that indicates if the file contains some data that has not been read.
 ** 	.
 ** \param Container should be a type of container that can match the file format to read.
 ** \param T the type of the container data.
 ** \param Nb_block is the number of containers to create (>1 if one want to divide the input
 ** data into different containers, default is 1).
 */
template<class Container, typename T, std::size_t Nb_block>
class ContainerReader {

public:

	typedef Container container_type;
	typedef T value_type;
	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** Default constructor
	 */
	ContainerReader()
	:data_filename_("")
	{}

	/*!
	 ** Constructor with parameters.
	 ** \param data_filename is the name of the file containing the data to read

	 */
	ContainerReader(std::string data_filename)
	:data_filename_(data_filename)
	{}

	/*!
	 ** Destructor
	 */
	virtual ~ContainerReader(){}

	/*@} End Constructors */

	/**
	 ** \name Getters and setters
	 */
	/*@{*/
	/*!
	 ** \brief return the name of the file containing the data to read
	 */
	const std::string& get_data_filename() const;

	/*!
	 ** \param data_filename is the name of the file containing the data to read
	 */
	void set_data_filename(const std::string& data_filename);
	/*!
	 ** \brief return the number of splitting blocks of the data
	 */
	std::size_t get_nb_block() const;

	/*@} End Getters and setters */

	/*!
	 ** \brief virtual read function. If Nb_block is more than one,
	 ** it reads only one container, and returns 0. Else it reads the
	 ** container and returns 1. If the file data_filename
	 ** has been completely read (no more block to read), it returns 1.
	 ** \param sig_in is the signal to fill.
	 ** \return true if there is still data to read, false if the file has been completely read.
	 */
	virtual int read(container_type & dat_in) = 0;

protected:
	std::string data_filename_;


};
/** @} */ // end of IO group
} /*! namespace slip */

namespace slip {

template<class Container, typename T, std::size_t Nb_block>
inline
const std::string& ContainerReader<Container, T, Nb_block>::get_data_filename() const {
	return data_filename_;
}

template<class Container, typename T, std::size_t Nb_block>
inline
void ContainerReader<Container, T, Nb_block>::set_data_filename(const std::string& data_filename) {
	data_filename_ = data_filename;
}

template<class Container, typename T, std::size_t Nb_block>
inline
std::size_t ContainerReader<Container, T, Nb_block>::get_nb_block() const {
	return Nb_block;
}

} /*! namespace slip */

#endif /* CONTAINERREADER_HPP_ */


