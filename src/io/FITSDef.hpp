/*!
 ** \file FITSDef.hpp
 ** \brief Some definitions specific to FITS format
 ** \version Fluex 1.0
 ** \date 2013/09/04
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

/*!
 * \cond Do not put the following into Doxygen documentation
 */
#ifndef FITSDEF_HPP_
#define FITSDEF_HPP_

#include <iostream>
#include <sstream>
#include <vector>
#include <CCfits>
#include "Vector.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "Array4d.hpp"
#include "error.hpp"

namespace slip {

/*!
 ** Information on Fits type of data.
 */
template<typename T> struct __FitsTypeInfo {
	static const CCfits::ImageType fitsId() {static const int i=CCfits::Inotype; return i;}
	//! Identifier as a string (i.e. \c <char*>value of \c NcType from \c ncvalues.h) on FITS type of data
	static const char* fitsStr() {static const char *const s="Inotype"; return s;}
};
//! Information on Fits type of a \c char data
template<> struct __FitsTypeInfo<char> {
	static const int fitsId() {static const int i=CCfits::Ibyte; return i;}
	static const char* fitsStr() {static const char *const s="Ibyte"; return s;}
};
//! Information on Fits type of an \c int data
template<> struct __FitsTypeInfo<int> {
	static const int fitsId() {static const int i=CCfits::Ilong; return i;}
	static const char* fitsStr() {static const char *const s="Ilong"; return s;}
};
//! Information on Fits type of a \c float data
template<> struct __FitsTypeInfo<float> {
	static const int fitsId() {
		static const int i=CCfits::Ifloat;
		return i;
	}
	static const char* fitsStr() {static const char *const s="Ifloat"; return s;}
};
//! Information on Fits type of a \c double data
template<> struct __FitsTypeInfo<double> {
	static const int fitsId() {static const int i=CCfits::Idouble; return i;}
	static const char* fitsStr() {static const char *const s="Idouble"; return s;}
};
//! Information on Fits type of a \c bool data
template<> struct __FitsTypeInfo<bool> {
	static const int fitsId() {static const int i=CCfits::Ibyte; return i;}
	static const char* fitsStr() {static const char *const s="Ibyte (used as boolean)"; return s;}
};

//! Information on Fits type as a string of any Fits Type
char *__FitsTypeStr(int Id)
{
	char *s;
	switch(Id)
	{
	case CCfits::Ibyte: s=(char *)(__FitsTypeInfo<char>::fitsStr());  break;
	case CCfits::Ilong: s=(char *)(__FitsTypeInfo<int>::fitsStr());   break;
	case CCfits::Ifloat: s=(char *)(__FitsTypeInfo<float>::fitsStr()); break;
	case CCfits::Idouble: s=(char *)(__FitsTypeInfo<double>::fitsStr());break;
	case CCfits::Inotype:  s=(char *)(__FitsTypeInfo<bool>::fitsStr());  break;
	default: return NULL;
	}
	return s;
}




/*!
 ** \brief fits_resize structure used for resizing the containers.
 ** \param Dim : container dimension
 ** \version Fluex 1.0
 ** \date 2013/09/04
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */
template <std::size_t Dim>
struct __fits_resize{
	template <class Container>
	static void apply(Container & cont, int first_dim_size, const std::vector<int> & axis){
		std::ostringstream err;
		err << "Dimension is not valid.";
		slip::slip_exception exc(std::string("slip"), std::string("fits_resize"), err.str());
		throw (exc);
	}
};

/*!
 ** \brief fits_resize structure used for resizing the containers.
 ** \param Dim : container dimension = 1
 ** \version Fluex 1.0
 ** \date 2013/09/04
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */
template<>
struct __fits_resize<1>{
	template <class Container1D>
	static void apply(Container1D & cont, int first_dim_size, const std::vector<int> & axis){
		typedef typename Container1D::value_type T;
		cont.resize(first_dim_size,T());
	}
};

/*!
 ** \brief fits_resize structure used for resizing the containers.
 ** \param Dim : container dimension = 2
 ** \version Fluex 1.0
 ** \date 2013/09/04
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */
template<>
struct __fits_resize<2>{
	template <class Container2D>
	static void apply(Container2D & cont, int first_dim_size, const std::vector<int> & axis){
		typedef typename Container2D::value_type T;
		cont.resize(first_dim_size,axis[0],T());
	}
};

/*!
 ** \brief fits_resize structure used for resizing the containers.
 ** \param Dim : container dimension = 3
 ** \version Fluex 1.0
 ** \date 2013/09/04
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */
template<>
struct __fits_resize<3>{
	template <class Container3D>
	static void apply(Container3D & cont, int first_dim_size, const std::vector<int> & axis){
		typedef typename Container3D::value_type T;
		cont.resize(first_dim_size,axis[1],axis[0],T());
	}
};

/*!
 ** \brief fits_resize structure used for resizing the containers.
 ** \param Dim : container dimension = 4
 ** \version Fluex 1.0
 ** \date 2013/09/04
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */
template<>
struct __fits_resize<4>{
	template <class Container4D>
	static void apply(Container4D & cont, int first_dim_size, const std::vector<int> & axis){
		typedef typename Container4D::value_type T;
		cont.resize(first_dim_size,axis[2],axis[1],axis[0],T());
	}
};

} /*! namespace slip */

/*!
 * \endcond
 */

#endif /* FITSDEF_HPP_ */
