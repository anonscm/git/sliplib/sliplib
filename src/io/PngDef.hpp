/*!
 ** \file PngDef.hpp
 ** \brief Some definitions specific to libpng
 ** \version Fluex 1.0
 ** \date 2013/04/22
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef PNGDEF_HPP_
#define PNGDEF_HPP_
/*!
 * \cond Do not put the following into Doxygen documentation
 */
extern "C"{
#include <png.h>
#include <stdio.h>
}

#define PNG_BYTES_TO_CHECK 4
/*
 * \brief check if the file
 */
int check_if_png(char *file_name, FILE **fp)
{
	unsigned char buf[PNG_BYTES_TO_CHECK];

	/* Open the prospective PNG file. */
	if ((*fp = fopen(file_name, "rb")) == NULL)
		return 0;

	/* Read in some of the signature bytes */
	if (fread(buf, 1, PNG_BYTES_TO_CHECK, *fp) != PNG_BYTES_TO_CHECK)
		return 0;

	/* Compare the first PNG_BYTES_TO_CHECK bytes of the signature.
      Return nonzero (true) if they match */

	return(!png_sig_cmp(buf, (png_size_t)0, PNG_BYTES_TO_CHECK));
}

/*!
 * \endcond
 */


#endif /* PNGDEF_HPP_ */
