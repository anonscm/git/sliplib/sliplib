/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file DenseVector3dField2d.hpp
 * 
 * \brief Provides a class to manipulate dense 2d fields containing 3d vectors.
 * 
 */
#ifndef SLIP_DENSEVECTOR3DFIELD2D_HPP
#define SLIP_DENSEVECTOR3DFIELD2D_HPP

#include <iostream>
#include <fstream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <cmath>
#include <string>
#include <cstddef>
#include "Matrix.hpp"
#include "stride_iterator.hpp"
#include "kstride_iterator.hpp"
#include "iterator2d_box.hpp"
#include "apply.hpp"
#include "Vector3d.hpp"
#include "GenericMultiComponent2d.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/base_object.hpp>

namespace slip
{

template<class T>
class stride_iterator;

template<class T>
class iterator2d_box;

template <class T>
class DPoint2d;

template <class T>
class Point2d;

template <class T>
class Box2d;

template <typename T>
class DenseVector3dField2d;


/*! \class DenseVector3dField2d
**  \ingroup Containers Containers2d MultiComponent2dContainers
** \brief This is a dense 2d Field containing %Vector3d.
** This container statisfies the BidirectionnalContainer concepts of the STL.
** It is also an 2d extension of the RandomAccessContainer concept. That is
** to say the bracket element access is replaced by the double bracket element 
** access.
** It is a specialization of GenericMultiComponent2d using Vector3d blocks.
** It implements arithmetic and mathematical operators and read/write methods (tecplot and gnuplot file format...).
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/03/25
** \since 1.0.0
** \param T Type of object in the DenseVector3dField2d 
** \par Axis conventions:
** \image html iterator2d_conventions.jpg "axis and notation conventions"
** \image latex iterator2d_conventions.eps "axis and notation conventions" width=5cm
*/
template <typename T>
class DenseVector3dField2d:public GenericMultiComponent2d<slip::Vector3d<T> >
{
public :

  typedef slip::Vector3d<T> value_type;
  typedef DenseVector3dField2d<T> self;
  typedef GenericMultiComponent2d<slip::Vector3d<T> > base;


  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;
  
  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;


  typedef pointer row_iterator;
  typedef const_pointer const_row_iterator;
  typedef slip::stride_iterator<pointer> col_iterator;
  typedef slip::stride_iterator<const_pointer> const_col_iterator;

  typedef std::reverse_iterator<iterator> reverse_row_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
  typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
  typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;

  typedef typename slip::GenericMultiComponent2d<Vector3d<T> >::iterator2d iterator2d;

  typedef T vector3d_value_type;
  typedef vector3d_value_type* vector3d_pointer;
  typedef const vector3d_value_type* const_vector3d_pointer;
  typedef vector3d_value_type& vector3d_reference;
  typedef const vector3d_value_type const_vector3d_reference;
  typedef slip::kstride_iterator<vector3d_pointer,2> vector3d_iterator;
  typedef slip::kstride_iterator<const_vector3d_pointer,2> const_vector3d_iterator;

  static const std::size_t DIM = 2;
public:
 /**
  ** \name Constructors & Destructors
  */
 /*@{*/

  /*!
  ** \brief Constructs a %DenseVector3dField2d.
  */
  DenseVector3dField2d():
    base()
  {}
 
  /*!
  ** \brief Constructs a %DenseVector3dField2d.
  ** \param height first dimension of the %DenseVector3dField2d
  ** \param width second dimension of the %DenseVector3dField2d
  */
  DenseVector3dField2d(const size_type height,
	     const size_type width):
    base(height,width)
  {}
  
  /*!
  ** \brief Constructs a %DenseVector3dField2d initialized by the scalar value \a val.
  ** \param height first dimension of the %DenseVector3dField2d
  ** \param width second dimension of the %DenseVector3dField2d
  ** \param val initialization value of the elements 
  */
  DenseVector3dField2d(const size_type height,
	     const size_type width, 
	     const slip::Vector3d<T>& val):
    base(height,width,val)
  {}
 
  /*!
  ** \brief Constructs a %DenseVector3dField2d initialized by an array \a val.
  ** \param height first dimension of the %DenseVector3dField2d
  ** \param width second dimension of the %DenseVector3dField2d
  ** \param val initialization linear array value of the elements 
  */
  DenseVector3dField2d(const size_type height,
	     const size_type width, 
	     const T* val):
    base(height,width,val)
  {}
 
  /*!
  ** \brief Constructs a %DenseVector3dField2d initialized by an array \a val.
  ** \param height first dimension of the %DenseVector3dField2d
  ** \param width second dimension of the %DenseVector3dField2d
  ** \param val initialization array value of the elements 
  */
  DenseVector3dField2d(const size_type height,
		 const size_type width, 
	     const slip::Vector3d<T>* val):
    base(height,width,val)
  {}
 

  /**
  **  \brief  Contructs a %DenseVector3dField2d from a range.
  ** \param height first dimension of the %DenseVector3dField2d
  ** \param width second dimension of the %DenseVector3dField2d
  **  \param  first  An input iterator.
  **  \param  last  An input iterator.
  **
  ** Create a %DenseVector3dField2d consisting of copies of the elements from
  ** [first,last).
  */
  template<typename InputIterator>
  DenseVector3dField2d(const size_type height,
		       const size_type width, 
		       InputIterator first,
		       InputIterator last):
    base(height,width,first,last)
  {} 



  
 /**
  **  \brief  Contructs a %DenseVector3dField2d from a 3 ranges.
  ** \param height first dimension of the %DenseVector3dField2d
  ** \param width second dimension of the %DenseVector3dField2d
  **  \param  first1  An input iterator.
  **  \param  last1  An input iterator.
  **  \param  first2  An input iterator.
  **  \param  first3  An input iterator.
 
  **
  ** Create a %DenseVector3dField2d consisting of copies of the elements from
  ** [first1,last1), 
  ** [first2,first2 + (last1 - first1)), 
  ** [first3,first3 + (last1 - first1)), 
  */
  template<typename InputIterator>
  DenseVector3dField2d(const size_type height,
		       const size_type width, 
		       InputIterator first1,
		       InputIterator last1,
		       InputIterator first2,
		       InputIterator first3):
    base(height,width)
  {
    
    std::vector<InputIterator> first_iterators_list(3);
    first_iterators_list[0] = first1;
    first_iterators_list[1] = first2;
    first_iterators_list[2] = first3;
    this->fill(first_iterators_list,last1);
  
  } 


  /*!
   ** \brief Constructs a copy of the %DenseVector3dField2d \a rhs
  */
  DenseVector3dField2d(const self& rhs):
    base(rhs)
  {}

  /*!
  ** \brief Destructor of the %DenseVector3dField2d
  */
  ~DenseVector3dField2d()
  {}
  

  /*@} End Constructors */


  
  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;


  /**
   ** \name i/o operators
   */
  /*@{*/
  
   /*!
   ** \brief Writes a DenseVector3dField2d to a gnuplot file path name
   ** \param file_path_name
   ** 
   ** The data format is the following :
   ** x y Vx Vy Vz
   */ 
  void write_gnuplot(const std::string& file_path_name);


  /*!
  ** \brief Writes a DenseVector3dField2d to a tecplot file path name
  ** \param file_path_name
  ** \param title
  ** \param zone
  ** 
  ** The data format is the following :
  ** TITLE= title 
  ** VARIABLES= X Y U V W 
  ** ZONE T= zone, I= dim1(), J= << dim2()
  ** x y Vx Vy Vz
  */ 
  void write_tecplot(const std::string& file_path_name,
		     const std::string& title,
		     const std::string& zone);




 
  /*@} End i/o operators */

  /**
   ** \name  Assignment operators and methods
   */
  /*@{*/

 

  /*!
  ** \brief Affects all the element of the %DenseVector3dField2d by val
  ** \param val affectation value
  ** \return reference to corresponding %DenseVector3dField2d
  */
  self& operator=(const slip::Vector3d<T>& val);


  /*!
  ** \brief Affects all the element of the %DenseVector3dField2d by val
  ** \param val affectation value
  ** \return reference to corresponding %DenseVector3dField2d
  */
  self& operator=(const T& val);



  
 
  /*@} End Assignment operators and methods*/

  /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Subscript access to first element of 
  ** the data contained in the %DenseVector3dField2d.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read/write reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  T& Vx1(const size_type i,
	 const size_type j);

 
 
  /*!
  ** \brief Subscript access to the data contained in the %DenseVector3dField2d.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read_only (constant) reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const T& Vx1(const size_type i,
	       const size_type j) const;

 
/*!
  ** \brief Subscript access to second element of 
  ** the data contained in the %DenseVector3dField2d.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read/write reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  T& Vx2(const size_type i,
	 const size_type j);
 
  /*!
  ** \brief Subscript access to the data contained in the %DenseVector3dField2d.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read_only (constant) reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const T& Vx2(const size_type i,
	       const size_type j) const;

 /*!
  ** \brief Subscript access to third element of 
  ** the data contained in the %DenseVector3dField2d.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read/write reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  T& Vx3(const size_type i,
	 const size_type j);
 
  /*!
  ** \brief Subscript access to the data contained in the %DenseVector3dField2d.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read_only (constant) reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const T& Vx3(const size_type i,
	       const size_type j) const;


/*@} End Element access operators */



  
 /**
   ** \name  Arithmetic operators
   */
  /*@{*/
  /*!
  ** \brief Add val to each element of the %DenseVector3dField2d  
  ** \param val value
  ** \return reference to the resulting %DenseVector3dField2d
  */
  self& operator+=(const T& val);
  self& operator-=(const T& val);
  self& operator*=(const T& val);
  self& operator/=(const T& val);

//   self& operator%=(const T& val);
//   self& operator^=(const T& val);
//   self& operator&=(const T& val);
//   self& operator|=(const T& val);
//   self& operator<<=(const T& val);
//   self& operator>>=(const T& val);


   self  operator-() const;
//   self  operator!() const;

  /*!
  ** \brief Add val to each element of the %DenseVector3dField2d  
  ** \param val value
  ** \return reference to the resulting %DenseVector3dField2d
  */
  self& operator+=(const slip::Vector3d<T>& val);
  self& operator-=(const slip::Vector3d<T>& val);
  self& operator*=(const slip::Vector3d<T>& val);
  self& operator/=(const slip::Vector3d<T>& val);

  

  self& operator+=(const self& rhs);
  self& operator-=(const self& rhs);
  self& operator*=(const self& rhs);
  self& operator/=(const self& rhs);
    

  /*@} End Arithmetic operators */
 
 private :
   friend class boost::serialization::access;
 template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
       if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::GenericMultiComponent2d<slip::Vector3d<T> > >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    { 
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::GenericMultiComponent2d<slip::Vector3d<T> > >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
 };

///double alias
  typedef slip::DenseVector3dField2d<double> DenseVector3dField2d_d;
  ///float alias
  typedef slip::DenseVector3dField2d<float> DenseVector3dField2d_f;
  ///long alias
  typedef slip::DenseVector3dField2d<long> DenseVector3dField2d_l;
  ///unsigned long alias
  typedef slip::DenseVector3dField2d<unsigned long> DenseVector3dField2d_ul;
  ///short alias
  typedef slip::DenseVector3dField2d<short> DenseVector3dField2d_s;
  ///unsigned long alias
  typedef slip::DenseVector3dField2d<unsigned short> DenseVector3dField2d_us;
  ///int alias
  typedef slip::DenseVector3dField2d<int> DenseVector3dField2d_i;
  ///unsigned int alias
  typedef slip::DenseVector3dField2d<unsigned int> DenseVector3dField2d_ui;
  ///char alias
  typedef slip::DenseVector3dField2d<char> DenseVector3dField2d_c;
  ///unsigned char alias
  typedef slip::DenseVector3dField2d<unsigned char> DenseVector3dField2d_uc;


}//slip::

namespace slip{
 

/*!
  ** \brief addition of a scalar to each element of a %DenseVector3dField2d
  ** \param M1 the %DenseVector3dField2d 
  ** \param val the scalar
  ** \return resulting %DenseVector3dField2d
  */
template<typename T>
DenseVector3dField2d<T> operator+(const DenseVector3dField2d<T>& M1, 
				  const T& val);

 /*!
  ** \brief addition of a scalar to each element of a %DenseVector3dField2d
  ** \param val the scalar
  ** \param M1 the %DenseVector3dField2d  
  ** \return resulting %DenseVector3dField2d
  */
  template<typename T>
  DenseVector3dField2d<T> operator+(const T& val, 
				    const DenseVector3dField2d<T>& M1);


/*!
  ** \brief substraction of a scalar to each element of a %DenseVector3dField2d
  ** \param M1 the %DenseVector3dField2d 
  ** \param val the scalar
  ** \return resulting %DenseVector3dField2d
  */
template<typename T>
DenseVector3dField2d<T> operator-(const DenseVector3dField2d<T>& M1, 
				  const T& val);

 /*!
  ** \brief substraction of a scalar to each element of a %DenseVector3dField2d
  ** \param val the scalar
  ** \param M1 the %DenseVector3dField2d  
  ** \return resulting %DenseVector3dField2d
  */
  template<typename T>
  DenseVector3dField2d<T> operator-(const T& val, 
				    const DenseVector3dField2d<T>& M1);



   /*!
  ** \brief multiplication of a scalar to each element of a %DenseVector3dField2d
  ** \param M1 the %DenseVector3dField2d 
  ** \param val the scalar
  ** \return resulting %DenseVector3dField2d
  */
  template<typename T>
  DenseVector3dField2d<T> operator*(const DenseVector3dField2d<T>& M1, 
				    const T& val);

 /*!
  ** \brief multiplication of a scalar to each element of a %DenseVector3dField2d
  ** \param val the scalar
  ** \param M1 the %DenseVector3dField2d  
  ** \return resulting %DenseVector3dField2d
  */
  template<typename T>
  DenseVector3dField2d<T> operator*(const T& val, 
				    const DenseVector3dField2d<T>& M1);
  


 /*!
  ** \brief division of a scalar to each element of a %DenseVector3dField2d
  ** \param M1 the %DenseVector3dField2d 
  ** \param val the scalar
  ** \return resulting %DenseVector3dField2d
  */
  template<typename T>
  DenseVector3dField2d<T> operator/(const DenseVector3dField2d<T>& M1, 
				    const T& val);


 /*!
  ** \brief pointwise addition of two %DenseVector3dField2d
  ** \param M1 first %DenseVector3dField2d 
  ** \param M2 seconf %DenseVector3dField2d
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %DenseVector3dField2d
  */
template<typename T>
DenseVector3dField2d<T> operator+(const DenseVector3dField2d<T>& M1, 
				  const DenseVector3dField2d<T>& M2);

/*!
  ** \brief addition of a %Vector3d to each element of a %DenseVector3dField2d
  ** \param M1 the %DenseVector3dField2d 
  ** \param val the %Vector3d
  ** \return resulting %DenseVector3dField2d
  */
template<typename T>
DenseVector3dField2d<T> operator+(const DenseVector3dField2d<T>& M1, 
				  const slip::Vector3d<T>& val);

 /*!
  ** \brief addition of a %Vector3d to each element of a %DenseVector3dField2d
  ** \param val the %Vector3d
  ** \param M1 the %DenseVector3dField2d  
  ** \return resulting %DenseVector3dField2d
  */
  template<typename T>
  DenseVector3dField2d<T> operator+(const slip::Vector3d<T>& val, 
				    const DenseVector3dField2d<T>& M1);
  

 
/*!
  ** \brief pointwise substraction of two %DenseVector3dField2d
  ** \param M1 first %DenseVector3dField2d 
  ** \param M2 seconf %DenseVector3dField2d
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %DenseVector3dField2d
  */
template<typename T>
DenseVector3dField2d<T> operator-(const DenseVector3dField2d<T>& M1, 
				  const DenseVector3dField2d<T>& M2);

 
  /*!
  ** \brief substraction of a %Vector3d to each element of a %DenseVector3dField2d
  ** \param M1 the %DenseVector3dField2d 
  ** \param val the %Vector3d
  ** \return resulting %DenseVector3dField2d
  */
template<typename T>
DenseVector3dField2d<T> operator-(const DenseVector3dField2d<T>& M1, 
				  const slip::Vector3d<T>& val);

 /*!
  ** \brief substraction of a %Vector3d to each element of a %DenseVector3dField2d
  ** \param val the %Vector3d
  ** \param M1 the %DenseVector3dField2d  
  ** \return resulting %DenseVector3dField2d
  */
  template<typename T>
  DenseVector3dField2d<T> operator-(const slip::Vector3d<T>& val, 
				    const DenseVector3dField2d<T>& M1);
  

  
/*!
  ** \brief pointwise multiplication of two %DenseVector3dField2d
  ** \param M1 first %DenseVector3dField2d 
  ** \param M2 seconf %DenseVector3dField2d
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %DenseVector3dField2d
  */
  template<typename T>
  DenseVector3dField2d<T> operator*(const DenseVector3dField2d<T>& M1, 
		      const DenseVector3dField2d<T>& M2);


   /*!
  ** \brief multiplication of a %Vector3d to each element of a %DenseVector3dField2d
  ** \param M1 the %DenseVector3dField2d 
  ** \param val the %Vector3d
  ** \return resulting %DenseVector3dField2d
  */
template<typename T>
DenseVector3dField2d<T> operator*(const DenseVector3dField2d<T>& M1, 
				  const slip::Vector3d<T>& val);

 /*!
  ** \brief multiplication of a %Vector3d to each element of a %DenseVector3dField2d
  ** \param val the %Vector3d
  ** \param M1 the %DenseVector3dField2d  
  ** \return resulting %DenseVector3dField2d
  */
  template<typename T>
  DenseVector3dField2d<T> operator*(const slip::Vector3d<T>& val, 
				    const DenseVector3dField2d<T>& M1);
  


  /*!
  ** \brief pointwise division of two %DenseVector3dField2d
  ** \param M1 first %DenseVector3dField2d 
  ** \param M2 seconf %DenseVector3dField2d
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %DenseVector3dField2d
  */
  template<typename T>
  DenseVector3dField2d<T> operator/(const DenseVector3dField2d<T>& M1, 
		      const DenseVector3dField2d<T>& M2);

    /*!
  ** \brief division of a %Vector3d to each element of a %DenseVector3dField2d
  ** \param M1 the %DenseVector3dField2d 
  ** \param val the %Vector3d
  ** \return resulting %DenseVector3dField2d
  */
template<typename T>
DenseVector3dField2d<T> operator/(const DenseVector3dField2d<T>& M1, 
				  const slip::Vector3d<T>& val);

 


}//slip::

namespace slip
{


//   template<typename T>
//   inline
//   DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator=(const DenseVector3dField2d<T> & rhs)
//   {
//     if(this != &rhs)
//       {
// 	*matrix_ = *(rhs.matrix_);
//       }
//     return *this;
//   }

  template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator=(const slip::Vector3d<T>& val)
  {
    std::fill_n(this->begin(),this->size(),val);
    return *this;
  }
  
 
 template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator=(const T& val)
  {
    std::fill_n(this->begin(),this->size(),val);
    return *this;
  }


  template<typename T>
  inline
  T& DenseVector3dField2d<T>::Vx1(const typename DenseVector3dField2d::size_type i,
				  const typename DenseVector3dField2d::size_type j)
  {
    return (*this)[i][j][0];
  }

  template<typename T>
  inline
  const T& DenseVector3dField2d<T>::Vx1(const typename DenseVector3dField2d::size_type i,
					const typename DenseVector3dField2d::size_type j) const
  {
    return (*this)[i][j][0];
  }

   template<typename T>
  inline
  T& DenseVector3dField2d<T>::Vx2(const typename DenseVector3dField2d::size_type i,
				  const typename DenseVector3dField2d::size_type j)
  {
    return (*this)[i][j][1];
  }


  template<typename T>
  inline
  const T& DenseVector3dField2d<T>::Vx2(const typename DenseVector3dField2d::size_type i,
					const typename DenseVector3dField2d::size_type j) const
  {
    return (*this)[i][j][1];
  }


  template<typename T>
  inline
  T& DenseVector3dField2d<T>::Vx3(const typename DenseVector3dField2d::size_type i,
				  const typename DenseVector3dField2d::size_type j)
  {
    return (*this)[i][j][2];
  }

   template<typename T>
  inline
  const T& DenseVector3dField2d<T>::Vx3(const typename DenseVector3dField2d::size_type i,
					const typename DenseVector3dField2d::size_type j) const
  {
    return (*this)[i][j][2];
  } 
 
  template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator+=(const T&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::plus<slip::Vector3d<T> >(),val));
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator-=(const T&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::minus<slip::Vector3d<T> >(),val));
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator*=(const T&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::multiplies<slip::Vector3d<T> >(),val));
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator/=(const T&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::divides<slip::Vector3d<T> >(),val));
    return *this;
  }



 template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator+=(const slip::Vector3d<T>&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::plus<slip::Vector3d<T> >(),val));
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator-=(const slip::Vector3d<T>&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::minus<slip::Vector3d<T> >(),val));
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator*=(const slip::Vector3d<T>&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::multiplies<slip::Vector3d<T> >(),val));
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator/=(const slip::Vector3d<T>&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::divides<slip::Vector3d<T> >(),val));
    return *this;
  }
  

  
  template<typename T>
  inline
  DenseVector3dField2d<T> DenseVector3dField2d<T>::operator-() const
  {
    DenseVector3dField2d<T> tmp(*this);
    std::transform(this->begin(),this->end(),tmp.begin(),std::negate<slip::Vector3d<T> >());
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator+=(const DenseVector3dField2d<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2()); 
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::plus<typename DenseVector3dField2d::value_type>());
    return *this;
  }

   template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator-=(const DenseVector3dField2d<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::minus<typename DenseVector3dField2d::value_type>());
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator*=(const DenseVector3dField2d<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::multiplies<typename DenseVector3dField2d::value_type>());
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T>& DenseVector3dField2d<T>::operator/=(const DenseVector3dField2d<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::divides<typename DenseVector3dField2d::value_type>());
    return *this;
  }


  template<typename T>
  inline
  std::string 
  DenseVector3dField2d<T>::name() const {return "DenseVector3dField2d";} 
  
  template<typename T>
  inline
  void DenseVector3dField2d<T>::write_gnuplot(const std::string& file_path_name)
  {
    std::ofstream sortie(file_path_name.c_str(),std::ios::out);
    //Put an exception 
    if(!sortie)
    {
      std::cerr<<"Fail to open the file "<<file_path_name<<std::endl;
      sortie.close();
      exit(1);
    }
    
    typename DenseVector3dField2d::size_type dim1 = this->dim1();
    typename DenseVector3dField2d::size_type dim2 = this->dim2();
    
    sortie<<"#zone f=point, i="<<dim1<<", j="<<dim2<<"\n";
    for(typename DenseVector3dField2d::size_type i = 0; i < dim1; ++i)
      {
	for(typename DenseVector3dField2d::size_type j = 0; j < dim2; ++j)
	  {
	    sortie<<i<<" "<<j<<" "<<(*this)[i][j][0]<<" "<<(*this)[i][j][1]<<(*this)[i][j][2]<<"\n";
	  }
      }
  }

  template<typename T>
  inline
  void DenseVector3dField2d<T>::write_tecplot(const std::string& file_path_name,
					      const std::string& title,
					      const std::string& zone)
  {
     std::ofstream output(file_path_name.c_str(),std::ios::out);
    //Put an exception 
    if(!output)
    {
      std::cerr<<"Fail to open the file "<<file_path_name<<std::endl;
      output.close();
      exit(1);
    }
    
    typename DenseVector3dField2d::size_type dim1 = this->dim1();
    typename DenseVector3dField2d::size_type dim2 = this->dim2();

    //write file informations
    output<<"TITLE=\"" <<title<<"\""<<std::endl;
    output<<"VARIABLES=\"X\",\"Y\",\"U\",\"V\"" <<std::endl;
    output<<"ZONE T=\""<<zone<<"\", I="<<dim1<<", J="<<dim2<<std::endl;
    //write data
    for(typename DenseVector3dField2d::size_type i = 0; i < dim1; ++i)
      {
	for(typename DenseVector3dField2d::size_type j = 0; j < dim2; ++j)
	  {
	    output<<i<<" "<<j<<" "<<(*this)[i][j][0]<<" "<<(*this)[i][j][1]<<(*this)[i][j][2]<<"\n";
	  }
      }
  }

/** \name Arithmetical DenseVector3dField2d operators */
  /* @{ */
  template<typename T>
  inline
  DenseVector3dField2d<T> operator+(const DenseVector3dField2d<T>& M1, 
				    const T& val)
  {
    DenseVector3dField2d<T>  tmp = M1;
    tmp += val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T> operator+(const T& val,
				    const DenseVector3dField2d<T>& M1)
  {
    return M1 + val;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T> operator-(const DenseVector3dField2d<T>& M1, 
				    const T& val)
  {
    DenseVector3dField2d<T>  tmp = M1;
    tmp -= val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T> operator-(const T& val,
				    const DenseVector3dField2d<T>& M1)
  {
    return -(M1 - val);
  }


  template<typename T>
  inline
  DenseVector3dField2d<T> operator*(const DenseVector3dField2d<T>& M1, 
				    const T& val)
  {
    DenseVector3dField2d<T>  tmp = M1;
    tmp *= val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T> operator*(const T& val,
				    const DenseVector3dField2d<T>& M1)
  {
    return M1 * val;
  }


  template<typename T>
  inline
  DenseVector3dField2d<T> operator/(const DenseVector3dField2d<T>& M1, 
				    const T& val)
  {
    DenseVector3dField2d<T>  tmp = M1;
    tmp /= val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T> operator+(const DenseVector3dField2d<T>& M1, 
				    const DenseVector3dField2d<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    DenseVector3dField2d<T> tmp(M1.dim1(),M1.dim2());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::plus<slip::Vector3d<T> >());
    return tmp;
  }


  template<typename T>
  inline
  DenseVector3dField2d<T> operator+(const DenseVector3dField2d<T>& M1, 
			      const slip::Vector3d<T>&val)
  {
    DenseVector3dField2d<T>  tmp = M1;
    tmp += val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T> operator+(const slip::Vector3d<T>&val,
			      const DenseVector3dField2d<T>& M1)
  {
    return M1 + val;
  }



 template<typename T>
  inline
  DenseVector3dField2d<T> operator-(const DenseVector3dField2d<T>& M1, 
				    const DenseVector3dField2d<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    DenseVector3dField2d<T> tmp(M1.dim1(),M1.dim2());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::minus<slip::Vector3d<T> >());
    return tmp;
  }


  template<typename T>
  inline
  DenseVector3dField2d<T> operator-(const DenseVector3dField2d<T>& M1, 
			      const slip::Vector3d<T>&val)
  {
    DenseVector3dField2d<T>  tmp = M1;
    tmp -= val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T> operator-(const slip::Vector3d<T>&val,
			      const DenseVector3dField2d<T>& M1)
  {
    return -(M1 - val);
  }


  template<typename T>
  inline
  DenseVector3dField2d<T> operator*(const DenseVector3dField2d<T>& M1, 
				    const DenseVector3dField2d<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    DenseVector3dField2d<T> tmp(M1.dim1(),M1.dim2());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::multiplies<slip::Vector3d<T> >());
    return tmp;
  }


  template<typename T>
  inline
  DenseVector3dField2d<T> operator*(const DenseVector3dField2d<T>& M1, 
				    const slip::Vector3d<T>&val)
  {
    DenseVector3dField2d<T>  tmp = M1;
    tmp *= val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField2d<T> operator*(const slip::Vector3d<T>&val,
				    const DenseVector3dField2d<T>& M1)
  {
    return M1 * val;
  }


 template<typename T>
  inline
  DenseVector3dField2d<T> operator/(const DenseVector3dField2d<T>& M1, 
				    const DenseVector3dField2d<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    DenseVector3dField2d<T> tmp(M1.dim1(),M1.dim2());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::divides<slip::Vector3d<T> >());
    return tmp;
  }


  template<typename T>
  inline
  DenseVector3dField2d<T> operator/(const DenseVector3dField2d<T>& M1, 
				    const slip::Vector3d<T>&val)
  {
    DenseVector3dField2d<T>  tmp = M1;
    tmp /= val;
    return tmp;
  }


/* @} */



}//slip::

#endif //SLIP_DENSEVECTOR3DFIELD2D_HPP
