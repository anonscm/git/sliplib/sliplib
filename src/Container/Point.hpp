/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/*! \file Point.hpp
**
** \brief Provides an abstract class to modelize nd points.
*/
#ifndef SLIP_POINT_HPP
#define SLIP_POINT_HPP

#include <iostream>
#include <cassert>
#include <algorithm>
#include "Block.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>
namespace slip
{

template <typename CoordType, std::size_t DIM>
class DPoint;

template <typename CoordType, std::size_t DIM>
class Point;

template <typename CoordType, std::size_t DIM>
std::ostream& operator<<(std::ostream & out, const Point<CoordType,DIM>& p);

/*! \class Point
**  \ingroup Containers MiscellaneousContainers
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/03/15
** \since 1.0.0
** \brief Define an abstract %Point structure
** \param CoordType Type of the coordinates of the Point 
** \param DIM dimension of the Point
** \todo verify RandomAccessContainer Concepts (friend operator==...)
*/
template <typename CoordType, std::size_t DIM>
class Point
{
public :
  typedef CoordType value_type;
  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;


  typedef DPoint<CoordType,DIM> dpoint_type;
  typedef Point<CoordType,DIM> self;


  typedef pointer iterator;
  typedef const_pointer const_iterator;
  
  typedef slip::stride_iterator<pointer> iterator_range;
  typedef slip::stride_iterator<const_pointer> const_iterator_range;

  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

  typedef std::reverse_iterator<iterator_range> reverse_iterator_range;
  typedef std::reverse_iterator<const_iterator_range> const_reverse_iterator_range;
  
  //default iterator_category of the container
  typedef std::random_access_iterator_tag iterator_category;
  
  static const std::size_t SIZE = DIM;

  
  
 /**
 ** \name Constructors & Destructors
 */
 /*@{*/
  /*!
  ** \brief Default constructor
  */
  Point();
  
  /*!
  ** \brief Constructs a copy of the Point \a rhs
  ** \param other 
  */
  Point(const self& other);


  /*!
  ** \brief Constructs a Point from a CoordType array
  ** \param array
  */
  Point(const CoordType* array);

  /*@} End Constructors */


   /**
   ** \name i/o operators
   */
  /*@{*/
  /*!
  ** \brief Write the Point to the ouput stream
  ** \param out output stream
  ** \param p Point to write to the output stream
  */  
  friend std::ostream& operator<< <>(std::ostream & out,  const self& p);

   /*@} End i/o operators */

   /**
   ** \name  Assignment operators and methods
   */
  /*@{*/
  /*!
  ** \brief Assign a Point.
  **
  ** Assign elements of Point in \a other
  **
  ** \param other Point to get the values from.
  ** \return 
  */
  self& operator=(const self & other);


  /*@} End Assignment operators and methods*/

  /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Returns the i'th coordinate of the Point.
  ** \param i index of the coordinate to return
  ** \return value of  the \a i'th row of the Point.
  ** \pre 0 <= i < dim
  */
  reference operator[](const std::size_t i);

  /*!
  ** \brief Returns the \a i'th row of the Point.
  ** \param i index of the coordinate to return
  ** \return value of  the \a i'th row of the Point
  ** \pre 0 <= i < dim
  */
  const_reference operator[](const std::size_t i) const;

  /*@} End Element access operators */

  /**
   ** \name Comparison operators
   */
  /*@{*/
  /*!
  ** \brief compare  two Point.
  **
  ** \param other Point to compare
  ** \return true if the two point have the same coordinates
  ** 
  */
  bool operator==(const self& other) const;

  /*!
  ** \brief compare  two Point.
  **
  ** \param other Point to compare
  ** \return true if the two point don't have the same coordinates
  ** 
  */
  bool operator!=(const self& other) const;
  /*@} Comparison operators */
  
 
  /*!
  ** \brief negate all the coordinates of a Point
  **
  ** \pre CoordType must be a "signed" type
  ** \return a Point with negate coordinates 
  ** 
  */
  self operator-() const;
  

  /**
   ** \name  Arithmetic operators
   */
  /*@{*/
 /*!
  ** \brief Assign a Point to this point plus a deplacement
  **
  ** 
  ** \param dp deplacement 
  ** \return 
  */
  self& operator+=(const dpoint_type & dp);

  /*!
  ** \brief translate a point considering the deplacement a dp
  **
  ** 
  ** \param dp deplacement 
  ** \return 
  */
  self operator+(const dpoint_type & dp) const;

  
 /*!
  ** \brief Assign a Point to this point minus a deplacement
  **
  ** 
  ** \param dp deplacement 
  ** \return 
  */
  self& operator-=(const dpoint_type & dp);

  
  /*!
  ** \brief translate a point considering the deplacement a dp
  **
  ** 
  ** \param dp deplacement 
  ** \return 
  */
  self operator-(const dpoint_type & dp) const;

  /*!
  ** \brief compute the difference between two points
  **
  ** 
  ** \param other Point
  ** \return the deplacement between the two points
  */
  dpoint_type operator-(const self & other) const;

  /*@} End Arithmetic operators */

  /*!
  ** \brief Returns the dimension of the Point
  ** \return the dimension (number of coordinates) of the Point
  */
  std::size_t dim() const;
  
  /*!
  ** \brief Returns the dimension of the Point
  ** \return the dimension (number of coordinates) of the Point
  */
  std::size_t size() const;

  /*!
  ** \brief Returns the maximal size of the Point
  ** \return the dimension (number of coordinates) of the Point
  */
  std::size_t max_size() const;

  /*!
  ** \brief Returns true if dimension of the Point is 0
  ** \return true if the dimension of the Point is 0, false else
  */
  bool empty()const;


  
  /*!
  ** \brief Return the coordinates of the Point
  */
  const block<value_type,DIM>& coord() const;
 
  /*!
  ** \brief Return the coordinates of the Point
  */
  block<value_type,DIM>& coord();
 
  
  

  /*!
  ** \brief Swaps two Point
  ** \param other Point to swap with
  */
  void swap(self& other);


   /**
   ** \name iterators
   */
  /*@{*/
 
 
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %Point.  Iteration is done in ordinary
  **  element order.
  ** \return begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator begin(){return this->coord_.begin();}
  

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %Point.  Iteration is done in ordinary
  **  element order.
  ** \return const begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator begin() const
  {
    
    return this->coord_.begin();
  } 

  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %Point.  Iteration is done in ordinary
  **  element order.
  ** \return end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator end(){return this->coord_.end();}
 
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %Point.  Iteration is done in
  **  ordinary element order.
  ** \return const end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator end() const {return this->coord_.end();}

  /*!
  **  \brief Returns a read/write reverse iterator that points to the
  **  last element in the %Point.  Iteration is done in reverse
  **  element order.
  ** \return reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rbegin(){return reverse_iterator(this->end());}

  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to the last element in the %Point.  Iteration is done in
  **  reverse element order.
  ** \return const reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_reverse_iterator rbegin() const {return const_reverse_iterator(this->end());}

  /*!
  **  \brief Returns a read/write reverse iterator that points to one
  **  before the first element in the %Point.  Iteration is done
  **  in reverse element order.
  **  \return reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rend(){return reverse_iterator(this->begin());}

    
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to one before the first element in the %Point.  Iteration
  **  is done in reverse element order.
  **  \return const reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_reverse_iterator rend() const {return const_reverse_iterator(this->begin());}

 /*!
  **  \brief Returns a read/write iterator_range that points 
  **  the first element within the %Range.  
  **  Iteration is done in ordinary element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return end iterator_range value
  ** \pre The range must be inside the whole range of the %Point.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Point<double,10> pi10;
  ** slip::iota(pi10.begin(),pi10.end(),1.0);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(pi10.begin(range),pi10.end(range),std::ostream_iterator<double>(std::cout," "));
  ** std::cout<<std::endl;
  ** \endcode
  */
  iterator_range begin(const slip::Range<int>& range)
  {
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < int(this->size()));
    assert(range.stop()  < int(this->size()));
    return iterator_range(this->begin() + range.start(),range.stride());
  }
 
  /*!
  ** \brief Returns a read-only (constant) iterator_range 
  ** that points the first element within the %Range.  
  ** Iteration is done in ordinary element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return const_iterator_range value
  ** \pre The range must be inside the whole range of the %Point.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Point<double,10> pi10;
  ** slip::iota(pi10.begin(),pi10.end(),1.0);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(pi10.begin(range),pi10.end(range),std::ostream_iterator<double>(std::cout," "));
  ** std::cout<<std::endl;
  ** \endcode
  */
  const_iterator_range begin(const slip::Range<int>& range) const
  {
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < int(this->size()));
    assert(range.stop()  < int(this->size()));
    return const_iterator_range(this->begin() + range.start(),range.stride());
  }

  /*!
  **  \brief Returns a read/write iterator_range that points 
  **  one past the last element in the %Point.  
  **  Iteration is done in ordinary element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return end iterator_range value
  ** \pre The range must be inside the whole range of the %Point.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Point<double,10> pi10;
  ** slip::iota(pi10.begin(),pi10.end(),1.0);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(pi10.begin(range),pi10.end(range),std::ostream_iterator<double>(std::cout," "));
  ** std::cout<<std::endl;
  ** \endcode
   */
  iterator_range end(const slip::Range<int>& range)
  {
    return this->begin(range) + (range.iterations() + 1);
  }
 
  /*!
  **  \brief Returns a read-only (constant) iterator_range 
  ** that points one past the last element in the %Point.  
  ** Iteration is done in ordinary element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return const_iterator_range value
  ** \pre The range must be inside the whole range of the %Point.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Point<double,10> pi10;
  ** slip::iota(pi10.begin(),pi10.end(),1.0);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(pi10.begin(range),pi10.end(range),std::ostream_iterator<double>(std::cout," "));
  ** std::cout<<std::endl;
  ** \endcode
  */
  const_iterator_range end(const slip::Range<int>& range) const
  {
    return this->begin(range) + (range.iterations() + 1);
  }

  
    /*!
  **  \brief Returns a read/write reverse_iterator_range that points 
  **  the end element within the %Range.  
  **  Iteration is done in reverse element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %Point.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Point<double,10> pi10;
  ** slip::iota(pi10.rbegin(),pi10.rend(),1.0);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(pi10.rbegin(range),pi10.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** std::cout<<std::endl;
  ** \endcode
  */
  reverse_iterator_range rbegin(const slip::Range<int>& range)
  {
    return reverse_iterator_range(this->end(range));
  }
 
  /*!
  ** \brief Returns a read-only (constant) reverse_iterator_range 
  ** that points the end element within the %Range.  
  ** Iteration is done in reverse element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return cons_treverse__iterator_range value
  ** \pre The range must be inside the whole range of the %Point.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Point<double,10> pi10;
  ** slip::iota(pi10.rbegin(),pi10.rend(),1.0);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(pi10.rbegin(range),pi10.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** std::cout<<std::endl;
  ** \endcode
  */
  const_reverse_iterator_range rbegin(const slip::Range<int>& range) const
  {
    return const_reverse_iterator_range(this->end(range));
  }
 

  /*!
  **  \brief Returns a read/write reverse_iterator_range that points 
  **  one previous the first element in the %Range.  
  **  Iteration is done in reverse element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %Point.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Point<double,10> pi10;
  ** slip::iota(pi10.rbegin(),pi10.rend(),1.0);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(pi10.rbegin(range),pi10.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** std::cout<<std::endl;
  ** \endcode
  */
  reverse_iterator_range rend(const slip::Range<int>& range)
  {
    return  reverse_iterator_range(this->begin(range));
  }
 
  /*!
  **  \brief Returns a read-only (constant) reverse_iterator_range 
  ** that points one previous the first element in the %Range.  
  ** Iteration is done in reverse element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return const_reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %Point.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Point<double,10> pi10;
  ** slip::iota(pi10.rbegin(),pi10.rend(),1.0);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(pi10.rbegin(range),pi10.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** std::cout<<std::endl;
  ** \endcode
  */
  const_reverse_iterator_range rend(const slip::Range<int>& range) const
  {
     return  const_reverse_iterator_range(this->begin(range));
  }

  
   /*@} End iterators */

protected:
 /**
 ** \name Constructors & Destructors
 */
 /*@{*/

 
 
  /*@} End Constructors */
 
  /*! \brief The coordinates of the point are stored 
  ** in this block
  */
  block<value_type,DIM> coord_;

protected:
friend class boost::serialization::access;
  template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & coord_;
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & coord_;
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()

 };
}//slip::

namespace slip
{

  template<typename CoordType,std::size_t DIM>
  inline
  Point<CoordType,DIM>::Point()
  {
    std::fill_n((this->coord_).begin(),DIM,CoordType());
  }

  template<typename CoordType,std::size_t DIM>
  inline
  Point<CoordType,DIM>::Point(const Point<CoordType,DIM>& other):
    coord_(other.coord_)
  {}

  template<typename CoordType,std::size_t DIM>
  inline
  Point<CoordType,DIM>::Point(const CoordType* array)
  {
    std::copy(array,array+DIM,coord_.begin());
  }

  template<typename CoordType,std::size_t DIM>
  inline
  std::ostream& operator<<(std::ostream & out, 
				  const Point<CoordType,DIM>& p)
  {
    out<<p.coord_;
    return out;
  }

  
  template<typename CoordType,std::size_t DIM>
  inline
  Point<CoordType,DIM>& Point<CoordType,DIM>::operator=(const Point<CoordType,DIM> & other)
  {
    if(this != &other)
      {
	this->coord_ = other.coord_;
      }
    return *this;
  }
  
 
  template<typename CoordType,std::size_t DIM>
  inline
  CoordType& Point<CoordType,DIM>::operator[](const std::size_t i)
  {
    assert(i < DIM);
    return this->coord_[i];
  }
  
  template<typename CoordType,std::size_t DIM>
  inline
  const CoordType& Point<CoordType,DIM>::operator[](const std::size_t i) const 
  {
    assert(i < DIM);
    return this->coord_[i];
  }
  
  template<typename CoordType,std::size_t DIM>
  inline
  std::size_t Point<CoordType,DIM>::dim() const {return DIM;} 
  
  template<typename CoordType,std::size_t DIM>
  inline
  std::size_t Point<CoordType,DIM>::size() const {return DIM;}

  template<typename CoordType,std::size_t DIM>
  inline
  std::size_t Point<CoordType,DIM>::max_size() const {return DIM;}

  template<typename CoordType,std::size_t DIM>
  inline
  bool Point<CoordType,DIM>::empty()const {return coord_.empty();}


  template<typename CoordType,std::size_t DIM>
  inline
  const block<CoordType,DIM>& Point<CoordType,DIM>::coord() const
  {
    return coord_;
  }

  template<typename CoordType,std::size_t DIM>
  inline
  block<CoordType,DIM>& Point<CoordType,DIM>::coord()
  {
    return coord_;
  }
  
  template<typename CoordType,std::size_t DIM>
  inline
  void Point<CoordType,DIM>::swap(Point<CoordType,DIM>& other)
  {
    (this->coord_).swap(other.coord_);
  }
  

  template<typename CoordType,std::size_t DIM>
  inline
  bool Point<CoordType,DIM>::operator==(const Point<CoordType,DIM>& other) const
  {
    return this->coord_ == other.coord_;
  }
  
  template<typename CoordType,std::size_t DIM>
  inline
  bool Point<CoordType,DIM>::operator!=(const Point<CoordType,DIM>& other) const
  {
    return this->coord_ != other.coord_;
  }

 
  template<typename CoordType,std::size_t DIM>
  inline
  Point<CoordType,DIM> Point<CoordType,DIM>::operator-() const
  {
    Point<CoordType,DIM> tmp;
    std::transform(coord_.begin(),coord_.end(),(tmp.coord_).begin(),std::negate<CoordType>());
    return tmp; 
  }

  template<typename CoordType,std::size_t DIM>
  inline
  Point<CoordType,DIM>& Point<CoordType,DIM>::operator+=(const DPoint<CoordType,DIM>& dp)
  {
   
    for(std::size_t i = 0; i < DIM; ++i)
      {
	this->coord_[i] += dp[i];
      }
    return *this; 
  }


  template<typename CoordType,std::size_t DIM>
  inline
  Point<CoordType,DIM> Point<CoordType,DIM>::operator+(const DPoint<CoordType,DIM>& dp) const
  {
    Point<CoordType,DIM> tmp(*this);
    tmp+=dp;
    return tmp; 
  }

  template<typename CoordType,std::size_t DIM>
  inline
  Point<CoordType,DIM>& Point<CoordType,DIM>::operator-=(const DPoint<CoordType,DIM>& dp)
  {
   
    for(std::size_t i = 0; i < DIM; ++i)
      {
	this->coord_[i] -= dp[i];
      }
    return *this; 
  }

    template<typename CoordType,std::size_t DIM>
  inline
  Point<CoordType,DIM> Point<CoordType,DIM>::operator-(const DPoint<CoordType,DIM>& dp) const
  {
    Point<CoordType,DIM> tmp(*this);
    tmp-=dp;
    return tmp; 
  }

  template<typename CoordType,std::size_t DIM>
  inline
  DPoint<CoordType,DIM> Point<CoordType,DIM>::operator-(const Point<CoordType,DIM>& p) const
  {
    block<CoordType,DIM> tmp;
    std::transform(coord_.begin(),coord_.end(),(p.coord_).begin(),tmp.begin(),std::minus<CoordType>());
    return DPoint<CoordType,DIM>(tmp.begin()); 
  }

}//slip::

#endif //SLIP_POINT_HPP
