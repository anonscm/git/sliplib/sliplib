/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file QuadTreeBox.hpp
 * 
 * \brief Provides a class to define box of a  Point Quadtree
 * 
 */
#ifndef SLIP_QUADTREEBOX_HPP
#define SLIP_QUADTREEBOX_HPP

#include <iostream>
#include <string>
#include "macros.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>


namespace slip
{
template <typename Point>
class QuadTreeBox;

template <typename Point>
std::ostream& operator<< (std::ostream&,
			  const QuadTreeBox<Point>&);


template<typename Point>
bool operator==(const QuadTreeBox<Point>& x, 
		const QuadTreeBox<Point>& y);

template<typename Point>
bool operator!=(const QuadTreeBox<Point>& x, 
		const QuadTreeBox<Point>& y);

/*!
 *  @defgroup PointTreeContainers Point Tree Containers
 *  @brief 1d containers having the same interface
 *  @{
 */

/*! \class QuadTreeBox
**  \ingroup Containers PointTreeContainers
**  \brief The class defines box of a Point Quadtree.
** 
** \author Benoit Tremblais
** \version 0.0.1
** \date 2016/07/01
** \since 1.5.0
** \param Point Type of the Point to define upper left and bottom right point of the Box.
** \todo add intersection and union operators
*/
template <typename Point>
class QuadTreeBox
{
public:
  typedef typename Point::value_type value_type;
  typedef QuadTreeBox<Point> self;
private:
  Point minimal_point_;
  Point maximal_point_;
  //  Point middle_point_;

public:
  /*!
  ** \brief Default constructor
  */
  QuadTreeBox():
    minimal_point_(Point()),
    maximal_point_(Point())//,
    //    middle_point_(Point())
  {}
  /*!
  ** \brief Constructor a QuadTreeBox with  [(xmin,ymin),(xmax,ymax)]
  ** \param xmin minimal x coordinate of the %QuadTreeBox.
  ** \param ymin minimal y coordinate of the %QuadTreeBox.
  ** \param xmax maximal x coordinate of the %QuadTreeBox.
  ** \param ymax maximal y coordinate of the %QuadTreeBox.
  ** \pre xmax > xmin
  ** \pre ymax > ymin
  */
  QuadTreeBox(const value_type& xmin,
	      const value_type& ymin,
	      const value_type& xmax,
	      const value_type& ymax):
    minimal_point_(Point(xmin,ymin)),
    maximal_point_(Point(xmax,ymax))//,
    // middle_point_(Point(slip::constants<value_type>::half()*(xmin+xmax),
    //			slip::constants<value_type>::half()*(ymin+ymax)))
  {}

 /*!
  ** \brief Constructor a QuadTreeBox with  [minimal_point,maximal_point]
  ** \param minimal_point the minimal point of the %QuadTreeBox.
  ** \param maximal_point the maximal point of the %QuadTreeBox.
  ** \pre maximal_point.x1() > minimal_point.x1()
  ** \pre maximal_point.x2() > minimal_point.x2()
  */
  QuadTreeBox(const Point& minimal_point,
	      const Point& maximal_point):
    minimal_point_(minimal_point),
    maximal_point_(maximal_point)//,
    // middle_point_(Point(slip::constants<value_type>::half()*(minimal_point[0]+maximal_point[0]),
    // 			slip::constants<value_type>::half()*(minimal_point[1]+maximal_point[1])))
  {}
 
  
  /*!
  ** \brief Returns the name of the class.
  */
  const std::string name() const
  {
    return "QuadTreeBox"; 
  }


 /*!
  ** \brief Returns the minimal point of the %QuadTreeBox.
  */
  const Point& minimal() const
  {
    return this->minimal_point_;
  }


  /*!
  ** \brief Returns the minimal point x coordinate of the %QuadTreeBox.
  */
  const value_type& xmin() const
  {
    return (this->minimal_point_).x1();
  }

  value_type& xmin() 
  {
    return (this->minimal_point_).x1();
  }
  /*!
  ** \brief Returns the maximal point x coordinate of the %QuadTreeBox.
  */
  const value_type& xmax() const
  {
    return (this->maximal_point_).x1();
  }

  value_type& xmax()
  {
    return (this->maximal_point_).x1();
  }

   /*!
  ** \brief Returns the  x middle point coordinate of the %QuadTreeBox.
  */
  value_type xmiddle() const
  {
    return slip::constants<value_type>::half()*(minimal_point_[0]+maximal_point_[0]);
  }

/*!
  ** \brief Returns the minimal point y coordinate of the %QuadTreeBox.
  */
  const value_type& ymin() const
  {
    return (this->minimal_point_).x2();
  }

  value_type& ymin()
  {
    return (this->minimal_point_).x2();
  }

  /*!
  ** \brief Returns the maximal point y coordinate of the %QuadTreeBox.
  */
  const value_type& ymax() const
  {
    return (this->maximal_point_).x2();
  }

  value_type& ymax() 
  {
    return (this->maximal_point_).x2();
  }

   /*!
  ** \brief Returns the middle y coordinate of the %QuadTreeBox.
  */
  value_type ymiddle() const
  {
    return slip::constants<value_type>::half()*(minimal_point_[1]+maximal_point_[1]);
  }
  /*!
  ** \brief Returns the maximal point of the %QuadTreeBox.
  */
  const Point& maximal() const
  {
    return this->maximal_point_;
  }

   /*!
  ** \brief Returns upper left point of the %QuadTreeBox.
  ** x = xmin
  ** y = ymax
  */
  Point upper_left() const
  {
    return Point(this->xmin(),this->ymax());
  }

   /*!
   ** \brief Returns upper right point of the %QuadTreeBox.
   ** x = xmax
   ** y = ymax
   */
  const Point& upper_right() const
  {
    return this->maximal();
  }

  /*!
   ** \brief Returns bottom left  point of the %QuadTreeBox.
   ** x = xmin
   ** y = ymin
   */
  const Point& bottom_left() const
  {
    return this->minimal();
  }

  /*!
  ** \brief Returns bottom right point of the %QuadTreeBox.
  ** x = xmax
  ** y = ymin
  */
  Point bottom_right() const
  {
    return Point(this->xmax(),this->ymin());
  }

  /*!
  ** \brief Returns the middle point of the %QuadTreeBox.
  ** x_middle = (xmin+xmax)/2
  ** y_middle = (ymin+ymax)/2
  */
  Point middle() const
  {
    return Point(this->xmiddle(),
		 this->ymiddle());
  }

   /*!
  ** \brief Returns the middle left point of the %QuadTreeBox.
  ** x = xmin
  ** y = (ymin+ymax)/2
  */
  Point middle_left() const
  {
    return Point(minimal_point_[0],
		 this->ymiddle());
  }

  /*!
  ** \brief Returns the middle right point of the %QuadTreeBox.
  ** x = xmax
  ** y = (ymin+ymax)/2
  */
  Point middle_right() const
  {
    return Point(maximal_point_[0],
		 this->ymiddle());
  }


  /*!
  ** \brief Returns the middle top point of the %QuadTreeBox.
  ** x = (xmin+xmax)/2
  ** y = ymax
  */
  Point middle_top() const
  {
    return Point(this->xmiddle(),
		 maximal_point_[1]);
  }

   /*!
  ** \brief Returns the middle bottom point of the %QuadTreeBox.
  ** x = (xmin+xmax)/2
  ** y = ymin
  */
  Point middle_bottom() const
  {
    return Point(this->xmiddle(),
		 minimal_point_[1]);
  }

  /*!
  ** \brief Returns true if (p[0] >= minimal_point_[0]) AND (p[0] <= maximal_point_[0]) AND (p[1] >= minimal_point_[1]) AND  (p[1] <= maximal_point_[1]) else returns false.
  **  \param p a Point.
  **  \return a boolean 
  */
  bool contains(const Point& p) const
  {
    bool result = false;
    if(   (p[0] >= minimal_point_[0])
       && (p[0] <= maximal_point_[0])
       && (p[1] >= minimal_point_[1])
       && (p[1] <= maximal_point_[1])
      )
      {
	result = true;
      }
    return result;
  }

   /*!
  ** \brief Returns true if (p[0] >= minimal_point_[0]+magin) AND (p[0] <= maximal_point_[0]-margin) AND (p[1] >= minimal_point_[1]+margin) AND  (p[1] <= maximal_point_[1]-margin) else returns false.
  **  \param p a Point.
  **  \param margin margin 
  **  \return a boolean 
  */
  bool contains(const Point& p,
		const typename Point::value_type& margin) const
  {
    bool result = false;
   
    if(      (p[0] >= (minimal_point_[0] + margin))
	  && (p[0] <= (maximal_point_[0] - margin))
	  && (p[1] >= (minimal_point_[1] + margin))
	  && (p[1] <= (maximal_point_[1] - margin))
      )
      {
	result = true;
      }
    return result;
  }

  /*!
  ** \brief Adjusts %QuadTreeBox minimal and maximal points if they are not consistent (coordinates of minimal point are greater than coordinates of maximal point).
  */
  void make_consistent()
  {
    if(minimal_point_[0] > maximal_point_[0])
      {
	std::swap(minimal_point_[0],maximal_point_[0]);
      }
    if(minimal_point_[1] > maximal_point_[1])
      {
	std::swap(minimal_point_[1],maximal_point_[1]);
      }
  }
 /*!
  ** \brief verify if the %QuadTreeBox is consistent, that is to say
  ** if the first point p1 has the smaller coordinates than p2
  ** \return true if the "p1 < p2"
  */
  bool is_consistent() const
  {
    bool result = false;
    if(  (minimal_point_[0] < maximal_point_[0])
       &&(minimal_point_[1] < maximal_point_[1]))
      {
	result = true;
      }
    return result;
  }

  /*!
  ** \brief Swaps two  %QuadTreeBox
  ** \param other %QuadTreeBox to swap with
  */
  void swap(self& other)
  {
    (this->minimal_point_).swap(other.minimal_point_);
    (this->maximal_point_).swap(other.maximal_point_);
  }
  /*!
  ** \brief Returns the width of the %QuadTreeBox.
  */
  const value_type width() const
  {
    return (this->maximal_point_[0]-this->minimal_point_[0]);
  }

  /*!
  ** \brief Returns the height of the %QuadTreeBox.
  */
  const value_type height() const
  {
    return (this->maximal_point_[1]-this->minimal_point_[1]);
  }

  /*!
  ** \brief Returns the area of the %QuadTreeBox.
  */
  const value_type area() const
  {
    return this->width()*this->height();
  }

  /*!
  ** \brief Print the QuadTreeBox.
  */
  friend std::ostream& operator<< <>(std::ostream&,
				     const QuadTreeBox<Point>&);
  /*!
  ** \brief Compare two %QuadTreeBox. Returns true iff the minimal and maximal point of the  %QuadTreeBox are equal.
  ** \param x a %QuadTreeBox.
  ** \param y another %QuadTreeBox.
  ** 
  ** \return true iff the minimal and maximal point of the  %QuadTreeBox are equal.
  */
  friend bool operator== <>(const QuadTreeBox<Point>& x, 
			    const QuadTreeBox<Point>& y);

  /*!
  ** \brief Compare two %QuadTreeBox.  Returns true iff the minimal and maximal point of the  %QuadTreeBox are not equal.
  ** \param x a %QuadTreeBox.
  ** \param y another %QuadTreeBox.
  ** \return true iff the minimal and maximal point of the  %QuadTreeBox are not equal.
  */
  friend bool operator!= <>(const QuadTreeBox<Point>& x, 
			    const QuadTreeBox<Point>& y);


protected:
  friend class boost::serialization::access;
template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
       if(version >= 0)
	{
	  ar & minimal_point_;
	  ar & maximal_point_;
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & minimal_point_;
	  ar & maximal_point_;
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()


};


template <typename Point>
std::ostream& operator<<(std::ostream& flux, 
			 const QuadTreeBox<Point>& quadtreebox)
{
  flux <<"QuadTreeBox: \n ["<<quadtreebox.minimal()<<","<<quadtreebox.maximal()<<"]";
  //  flux<<"middle point: "<<quadtreebox.middle();
  return flux;
}


/** \name EqualityComparable functions */
  /* @{ */

template<typename Point>
inline
bool operator==(const QuadTreeBox<Point>& x, 
		const QuadTreeBox<Point>& y)
{
  return (    (x.minimal() == y.minimal())
	   && (x.maximal() == y.maximal())
	 ); 
}

template<typename Point>
inline
bool operator!=(const QuadTreeBox<Point>& x, 
		const QuadTreeBox<Point>& y)
{
  return !(x == y);
}

/* @} */

}//slip::

namespace std
{
  template <typename Point>
  inline
  void swap(slip::QuadTreeBox<Point>& x,
	    slip::QuadTreeBox<Point>& y)
  {
    x.swap(y);
  }
}
#endif //SLIP_QUADTREEBOX_HPP
