/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Vector3d.hpp
 * 
 * \brief Provides a class to manipulate 3d Vector.
 * 
 */
#ifndef SLIP_VECTOR3D_HPP
#define SLIP_VECTOR3D_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <cstddef>
#include <algorithm>
#include <numeric>
#include <string>
#include "KVector.hpp"
#include "norms.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template <typename T>
struct Vector3d;

template <typename T>
bool operator<(const Vector3d<T>& x, const Vector3d<T>& y);

template <typename T>
bool operator>(const Vector3d<T>& x, const Vector3d<T>& y);

template <typename T>
bool operator<=(const Vector3d<T>& x, const Vector3d<T>& y);

template <typename T>
bool operator>=(const Vector3d<T>& x, const Vector3d<T>& y);

/*! \class Vector3d
** \ingroup Containers Containers1d MathematicalContainer
** \brief This is a %Vector3d struct. It is a specialization of kvector.
**  It implements some specific 3d operations.
** 
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/03/25
** \since 1.0.0
** \param T Type of the Vector3d 
*/
template <typename T>
struct Vector3d : public kvector<T,3>
{

  typedef Vector3d<T> self;
  typedef T value_type;
  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;


  /**
   ** \name Constructors
   */
  /*@{*/

  /*!
  ** \brief Default constructor
  ** init all the components to 0
  */
  Vector3d();

  /*!
  ** \brief Constructor
  ** init all the components to val
  ** \param val initial value
  */
  Vector3d(const T& val);

   /*!
  ** \brief Constructor
  ** init all the components to val
  ** \param x1 initial value of the first %Vector3d dimension
  ** \param x2 initial value of the second %Vector3d dimension
  ** \param x3 initial value of the third %Vector3d dimension
  */
  Vector3d(const T& x1,
	   const T& x2,
	   const T& x3);

  /*!
  ** \brief Constructor
  ** init the Vector3d with the val array
  ** \param val initial value
  */
  Vector3d(const T* val);

  
  /*!
  ** \brief Copy constructor
  ** init the Vector3d with another %Vector3d
  ** \param rhs The other %Vector3d
  */
  Vector3d(const Vector3d<T>& rhs);

  

  /*@} End Constructors */

 

 

  self operator-() const;

 /**
 ** \name Comparison operators
 */
 /*@{*/


  /*!
  ** \brief Less Compare operator
  ** \param x first Vector3d to compare
  ** \param y second Vector3d to compare
  ** return true if ||x|| < ||y||, ||.|| is the Euclidean norm
  ** \pre x.dim() == y.dim()
  */
  friend bool operator< <>(const Vector3d<T>& x, const Vector3d<T>& y);

  /*!
  ** \brief Greater Compare operator
  ** \param x first Vector3d to compare
  ** \param y second Vector3d to compare
  ** return true if ||x|| > ||y||, ||.|| is the Euclidean norm
  ** \pre x.dim() == y.dim()
  */
  friend bool operator> <>(const Vector3d<T>& x, const Vector3d<T>& y);

   /*!
  ** \brief Less equal Compare operator
  ** \param x first Vector3d to compare
  ** \param y second Vector3d to compare
  ** return true if ||x|| <= ||y||, ||.|| is the Euclidean norm
  ** \pre x.dim() == y.dim()
  */
  friend bool operator<= <>(const Vector3d<T>& x, const Vector3d<T>& y);

  /*!
  ** \brief Greater equal Compare operator
  ** \param x first Vector3d to compare
  ** \param y second Vector3d to compare
  ** return true if ||x|| >= ||y||, ||.|| is the Euclidean norm
  ** \pre x.dim() == y.dim()
  */
  friend bool operator> <>(const Vector3d<T>& x, const Vector3d<T>& y);

 
  /*@} End comparison operators */

  /**
   ** \name Accesors
   */
 /*@{*/
  /*!
  ** \brief Accessor to the first dimension value of the Vector3d 
  ** \return Returns the first dimension value of the Vector3d 
  */
  const T& get_x1() const;
  /*!
  ** \brief Accessor to the first dimension value of the Vector3d 
  ** \return Returns the first dimension value of the Vector3d 
  */
  const T& get_x() const;
  /*!
  ** \brief Accessor to the first dimension value of the Vector3d 
  ** \return Returns the first dimension value of the Vector3d 
  */
  const T& u() const;

  /*!
  ** \brief  Accessor to the second dimension value of the Vector3d 
  ** \return Returns the second dimension value of the Vector3d 
  */
  const T& get_x2() const;
  /*!
  ** \brief  Accessor to the second dimension value of the Vector3d 
  ** \return Returns the second dimension value of the Vector3d 
  */
  const T& get_y() const;
  /*!
  ** \brief  Accessor to the second dimension value of the Vector3d 
  ** \return Returns the second dimension value of the Vector3d 
  */
  const T& v() const;
    

 /*!
  ** \brief  Accessor to the third dimension value of the Vector3d 
  ** \return Returns the second dimension value of the Vector3d 
  */
  const T& get_x3() const;
  /*!
  ** \brief  Accessor to the third dimension value of the Vector3d 
  ** \return Returns the second dimension value of the Vector3d 
  */
  const T& get_z() const;
   /*!
  ** \brief  Accessor to the third dimension value of the Vector3d 
  ** \return Returns the second dimension value of the Vector3d 
  */
  const T& w() const;

  

  

  /*!
  ** \brief  Mutator of the first dimension value of the Vector3d 
  ** \param x1 the first dimension value of the Vector3d 
  */
  void set_x1(const T& x1);
  /*!
  ** \brief  Mutator of the first dimension value of the Vector3d 
  ** \param r the first dimension value of the Vector3d 
  */
  void set_x(const T& r);
  /*!
  ** \brief  Mutator of the first dimension value of the Vector3d 
  ** \param u_ the first dimension value of the Vector3d 
  */
  void u(const T& u_);
  

  /*!
  ** \brief  Mutator of the second dimension value of the Vector3d 
  ** \param x2 the second dimension value of the Vector3d 
  */
  void set_x2(const T& x2);
  /*!
  ** \brief  Mutator of the second dimension value of the Vector3d 
  ** \param g the second dimension value of the Vector3d 
  */
  void set_y(const T& g);
  /*!
  ** \brief  Mutator of the second dimension value of the Vector3d 
  ** \param v_ the second dimension value of the Vector3d 
  */
  void v(const T& v_);
 

  /*!
  ** \brief  Mutator of the third dimension value of the Vector3d 
  ** \param x3 the third dimension value of the %Vector3d 
  */
  void set_x3(const T& x3);
  /*!
  ** \brief  Mutator of the third dimension value of the Vector3d 
  ** \param r the third dimension value of the %Vector3d 
  */
  void set_z(const T& r);
  /*!
  ** \brief  Mutator of the third dimension value of the Vector3d 
  ** \param w_ the third dimension value of the %Vector3d 
  */
  void w(const T& w_);
 

  /*!
  ** \brief  Mutator of the three dimension values of the Vector3d 
  ** \param x1 the first dimension value of the %Vector3d 
  ** \param x2 the second dimension value of the %Vector3d 
  ** \param x3 the third dimension value of the %Vector3d 
  
  */
  void set(const T& x1, 
	   const T& x2,
	   const T& x3);



 /*@} End comparison operators */

/**
 ** \name Mathematical operators
 */
/*@{*/
  /*!
  ** \brief  Computes the vector cross product betwin the current %Vector3d and the
  **  %Vector3d \a other
  ** \param other A %Vector3d.
  ** \return The resulting %Vector3d.
  */
  self product(const self& other) const ;
/*@} End Mathematical operators */

   
  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;


  private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::kvector<T,3> >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::kvector<T,3> >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
};

 ///double alias
  typedef slip::Vector3d<double> Vector3d_d;
  ///float alias
  typedef slip::Vector3d<float> Vector3d_f;
  ///long alias
  typedef slip::Vector3d<long> Vector3d_l;
  ///unsigned long alias
  typedef slip::Vector3d<unsigned long> Vector3d_ul;
  ///short alias
  typedef slip::Vector3d<short> Vector3d_s;
  ///unsigned long alias
  typedef slip::Vector3d<unsigned short> Vector3d_us;
  ///int alias
  typedef slip::Vector3d<int> Vector3d_i;
  ///unsigned int alias
  typedef slip::Vector3d<unsigned int> Vector3d_ui;
  ///char alias
  typedef slip::Vector3d<char> Vector3d_c;
  ///unsigned char alias
  typedef slip::Vector3d<unsigned char> Vector3d_uc;

}//slip::

namespace slip
{

  template<typename T>
  inline
  Vector3d<T>::Vector3d()
  {
    this->fill(T(0));
  }

  template<typename T>
  inline
  Vector3d<T>::Vector3d(const T& val)
  {
    this->fill(val);
  }

  template<typename T>
  inline
  Vector3d<T>::Vector3d(const T& x1,
			const T& x2,
			const T& x3)
  {
    (*this)[0] = x1;
    (*this)[1] = x2;
    (*this)[2] = x3;
    
  }

  template<typename T>
  inline
  Vector3d<T>::Vector3d(const T* val)
  {
    assert(val != 0);
    this->fill(val);
  }


  template<typename T>
  inline
  Vector3d<T>::Vector3d(const Vector3d<T>& rhs)
  {
    *this = rhs; 
  }

  template<typename T>
  inline
   Vector3d<T> Vector3d<T>::operator-() const
  {
    self tmp;
    tmp[0] = -(*this)[0];
    tmp[1] = -(*this)[1];
    tmp[2] = -(*this)[2];
    
    return tmp;
  }
 /** \name LessThanComparable functions */
  /* @{ */
  template<typename T>
  inline
  bool operator<(const Vector3d<T>& x, 
		 const Vector3d<T>& y)
  {
     return (slip::Euclidean_norm<T>(x.begin(),x.end()) 
	     < 
	     slip::Euclidean_norm<T>(y.begin(),y.end()) 
	     );
  }

  

  template<typename T>
  inline
  bool operator>(const Vector3d<T>& x, 
		 const Vector3d<T>& y)
  {
    return y < x;
  }

  template<typename T>
  inline
  bool operator<=(const Vector3d<T>& x, 
		 const Vector3d<T>& y)
  {
    return !(y < x);
  }

  template<typename T>
  inline
  bool operator>=(const Vector3d<T>& x, 
		 const Vector3d<T>& y)
  {
    return !(x < y);
  }
 /* @} */

  template<typename T>
  inline
  const T& Vector3d<T>::get_x1() const {return (*this)[0];}

  template<typename T>
  inline
  const T& Vector3d<T>::get_x() const {return (*this)[0];}

  template<typename T>
  inline
  const T& Vector3d<T>::u() const {return (*this)[0];}


   template<typename T>
  inline
  const T& Vector3d<T>::get_x2() const {return (*this)[1];}

  template<typename T>
  inline
  const T& Vector3d<T>::get_y() const {return (*this)[1];}

  template<typename T>
  inline
  const T& Vector3d<T>::v() const {return (*this)[1];}


  template<typename T>
  inline
  const T& Vector3d<T>::get_x3() const {return (*this)[2];}

  template<typename T>
  inline
  const T& Vector3d<T>::get_z() const {return (*this)[2];}

  template<typename T>
  inline
  const T& Vector3d<T>::w() const {return (*this)[2];}


  template<typename T>
  inline
  void Vector3d<T>::set_x1(const T& x1){(*this)[0] = x1;}

  template<typename T>
  inline
  void Vector3d<T>::set_x(const T& x){(*this)[0] = x;}

  template<typename T>
  inline
  void Vector3d<T>::u(const T& x){(*this)[0] = x;}


  template<typename T>
  inline
  void Vector3d<T>::set_x2(const T& x2){(*this)[1] = x2;}

  template<typename T>
  inline
  void Vector3d<T>::set_y(const T& y){(*this)[1] = y;}

  template<typename T>
  inline
  void Vector3d<T>::v(const T& y){(*this)[1] = y;}


  template<typename T>
  inline
  void Vector3d<T>::set_x3(const T& x3){(*this)[2] = x3;}
  
  template<typename T>
  inline
  void Vector3d<T>::set_z(const T& z){(*this)[2] = z;}

  template<typename T>
  inline
  void Vector3d<T>::w(const T& w_){(*this)[2] = w_;}


  template<typename T>
  inline
  void Vector3d<T>::set(const T& x1,
			const T& x2,
			const T& x3)
  {
    (*this)[0] = x1;
    (*this)[1] = x2;
    (*this)[2] = x3;
    
  }

 template<typename T>
 inline
 Vector3d<T> Vector3d<T>::product(const Vector3d<T>& other) const 
 {
   Vector3d<T> tmp(this->size());
   tmp[0] = ( ((*this)[1] * other[2]) - ((*this)[2] * other[1]) );
   tmp[1] = ( ((*this)[2] * other[0]) - ((*this)[0] * other[2]) );
   tmp[2] = ( ((*this)[0] * other[1]) - ((*this)[1] * other[0]) );
   return tmp;
 }


}//slip::


namespace slip
{
template<typename T>
inline
Vector3d<T> operator+(const Vector3d<T>& V1, 
		      const Vector3d<T>& V2)
{
  Vector3d<T> tmp;
  tmp[0] = V1[0] + V2[0];
  tmp[1] = V1[1] + V2[1];
  tmp[2] = V1[2] + V2[2];
  return tmp;
}

template<typename T>
inline
Vector3d<T> operator+(const Vector3d<T>& V1, 
		       const T& val)
{
  Vector3d<T> tmp = V1;
  tmp+=val;
  return tmp;
}

template<typename T>
inline
Vector3d<T> operator+(const T& val,
		       const Vector3d<T>& V1)
{
  return V1 + val;
}

template<typename T>
inline
Vector3d<T> operator-(const Vector3d<T>& V1, 
		      const Vector3d<T>& V2)
{
  Vector3d<T> tmp;
  tmp[0] = V1[0] - V2[0];
  tmp[1] = V1[1] - V2[1];
  tmp[2] = V1[2] - V2[2];
  return tmp;
}

template<typename T>
inline
Vector3d<T> operator-(const Vector3d<T>& V1, 
		       const T& val)
{
  Vector3d<T> tmp = V1;
  tmp-=val;
  return tmp;
}

template<typename T>
inline
Vector3d<T> operator-(const T& val,
		       const Vector3d<T>& V1)
{
  Vector3d<T> tmp;
  tmp[0] = val - V1[0];
  tmp[1] = val - V1[1];
  tmp[2] = val - V1[2];
  return tmp;
}


template<typename T>
inline
Vector3d<T> operator*(const Vector3d<T>& V1, 
		      const Vector3d<T>& V2)
{
  Vector3d<T> tmp;
  tmp[0] = V1[0] * V2[0];
  tmp[1] = V1[1] * V2[1];
  tmp[2] = V1[2] * V2[2];
  
  return tmp;
}

template<typename T>
inline
Vector3d<T> operator*(const Vector3d<T>& V1, 
		       const T& val)
{
  Vector3d<T> tmp = V1;
  tmp*=val;
  return tmp;
}

template<typename T>
inline
Vector3d<T> operator*(const T& val,
		       const Vector3d<T>& V1)
{
  return V1 * val;
}

template<typename T>
inline
Vector3d<T> operator/(const Vector3d<T>& V1, 
		      const Vector3d<T>& V2)
{
  Vector3d<T> tmp;
  tmp[0] = V1[0] / V2[0];
  tmp[1] = V1[1] / V2[1];
  tmp[2] = V1[2] / V2[2];
  return tmp;
}

template<typename T>
inline
Vector3d<T> operator/(const Vector3d<T>& V1, 
		       const T& val)
{
  Vector3d<T> tmp = V1;
  tmp/=val;
  return tmp;
}

template<typename T>
inline
std::string 
Vector3d<T>::name() const {return "Vector3d";} 


}//slip::

#endif //SLIP_VECTOR3D_HPP
