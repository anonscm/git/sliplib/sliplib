/** 
 * \file DiscretePolyBase1d.hpp
 * 
 * \brief Provides a class to handle global approximation of 1d container by orthogonal polynomials base.
 * 
 */

#ifndef SLIP_DISCRETEPOLYBASE1D_HPP
#define SLIP_DISCRETEPOLYBASE1D_HPP

#include <iostream>
#include <string>
#include <vector>
#include "Polynomial.hpp"
#include "Array.hpp"
#include "Array2d.hpp"
#include "arithmetic_op.hpp"
#include "linear_algebra.hpp"
#include "macros.hpp"
//#include <slip/gram_schmidt.hpp>
//#include <slip/statistics.hpp>
#include "polynomial_algo.hpp"
#include "poly_weight_functors.hpp"



namespace slip
{
  template<typename RandomAccessIterator1,
	   typename RandomAccessIterator2,
	   typename T> 
  struct discrete_poly_inner_prod : 
    public std::binary_function<std::pair<RandomAccessIterator1,RandomAccessIterator1>, 
				std::pair<RandomAccessIterator2,RandomAccessIterator2>,T>
  {
    discrete_poly_inner_prod(const slip::Array<T>* weight):
      weight_(const_cast<slip::Array<T>*>(weight))
    {}
    T operator() (const std::pair<RandomAccessIterator1,RandomAccessIterator1>& Pi,
		  const std::pair<RandomAccessIterator2,RandomAccessIterator2>& Pj)
  {

    T result = T();
    typename slip::Array<T>::const_iterator it_weight = weight_->begin();
    RandomAccessIterator1 it_pi = Pi.first;
    RandomAccessIterator2 it_pj = Pj.first;
    for (; it_pi != Pi.second; 
	   ++it_pi, 
	   ++it_pj,
	   ++it_weight)
      {
	result = result + *it_weight * (*it_pi * *it_pj);
      }
    return result;

  }
		  
   //  ~discrete_poly_inner_prod()
//     {
//       if(this->weight_ != 0)
// 	{
// 	  delete this->weight_;
// 	}
//     }

    slip::Array<T>* weight_;

 };
}

namespace slip
{


template <typename T, typename PolyWeight, typename PolyCollocation >
class DiscretePolyBase1d;

/*! \class DiscretePolyBase1d
** 
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2011/08/26
** \brief a class to handle global approximation of 1d container by orthogonal polynomials base
** \param T Type of data to approximate.
** \param PolyWeight Polynomial weight functor.
** \param PolyCollocation Polynomial collocation functor.
*/
  template <typename T, 
	    typename PolyWeight, 
	    typename PolyCollocation>
class DiscretePolyBase1d
{
  
public:
  
  typedef DiscretePolyBase1d<T, PolyWeight, PolyCollocation> self;
  typedef const self const_self;

   /**
   ** \name Constructors & Destructors
   */

   /*@{*
    /*!
    ** \brief Constructs a DiscretePolyBase1d.
    */
  DiscretePolyBase1d():
    range_size_(0),
    size_(0),
    degree_(-1),
    collocations_(0),
    weights_(0),
    alpha_(0),
    beta_(0),
    pkpk_(0),
    pi_at_x_(0),
    poly_evaluations_(0),
    poly_w_evaluations_(0),
    poly_weight_func_(PolyWeight()),
    poly_collocation_func_(PolyCollocation()),
    normalized_(false)
  {
  }
 
  /*!
    ** \brief Constructs a DiscretePolyBase1d.
    ** \param range_size size of the 1d container or range.
    ** \param degree degree of the base.
    ** \param poly_weight_fun Polynomial weight functor.
    ** \param poly_collocation_fun Polynomial collocation functor.
    ** \param normalized Boolean indicated if the base is normalized or not.
    */
  DiscretePolyBase1d(const std::size_t range_size,
	      const int degree,
	      const PolyWeight& poly_weight_fun,
	      const PolyCollocation& poly_collocation_fun,
	      const bool normalized):
    range_size_(range_size),
    size_(degree+1),
    degree_(degree),
    collocations_(new slip::Array<T>(range_size)),
    weights_(new slip::Array<T>(range_size)),
    alpha_(new slip::Array<T>(static_cast<std::size_t>(degree+1))),
    beta_(new slip::Array<T>(static_cast<std::size_t>(degree+1))),
    pkpk_(new slip::Array<T>(static_cast<std::size_t>(degree+1))),
    pi_at_x_(new slip::Array<T>(static_cast<std::size_t>(degree+1))),
    poly_evaluations_(new slip::Array2d<T>(static_cast<std::size_t>(degree+1),
					   range_size,
					   T())),
    poly_w_evaluations_(new slip::Array2d<T>(static_cast<std::size_t>(degree+1),
					     range_size,
					     T())),
    poly_weight_func_(poly_weight_fun),
    poly_collocation_func_(poly_collocation_fun),
    normalized_(normalized)
  {
  }


  void init_canonical(const std::size_t range_size,
		      const int degree,
		      const PolyWeight& poly_weight_fun,
		      const PolyCollocation& poly_collocation_fun)
  {
    //initializations and allocations
    this->range_size_ = range_size;
    this->size_ = degree + 1;
    this->degree_ = degree;
    this->collocations_ = new slip::Array<T>(range_size);
    this->weights_ = new slip::Array<T>(range_size);
    this->alpha_ = new slip::Array<T>(static_cast<std::size_t>(degree+1));
    this->beta_ = new slip::Array<T>(static_cast<std::size_t>(degree+1));
    this->pkpk_ = new slip::Array<T>(static_cast<std::size_t>(degree+1));
    this->pi_at_x_ = new slip::Array<T>(static_cast<std::size_t>(degree+1));
    this->poly_evaluations_ = 
      new slip::Array2d<T>(static_cast<std::size_t>(degree+1),
    			   range_size,
    			   T());
    this->poly_w_evaluations_ =  
      new slip::Array2d<T>(static_cast<std::size_t>(degree+1),
    			   range_size,
    			   T());
    this->poly_weight_func_ = poly_weight_fun;
    this->poly_collocation_func_ = poly_collocation_fun;
    this->normalized_ = false;
    
    //computes canonical base
    this->compute_collocations();
    this->compute_weights();
    for(std::size_t j = 0; j < this->range_size_; ++j)
      {
	slip::eval_power_basis((*(this->collocations_))[j],
			       this->degree_,
			       this->poly_evaluations_->col_begin(j),
			       this->poly_evaluations_->col_end(j));

      }
    //    for(std::size_t i = 0; i < this->range_size_; ++i)
     for(std::size_t i = 0; i < this->size_; ++i)
      {
	slip::multiplies(this->poly_evaluations_->row_begin(i),
			 this->poly_evaluations_->row_end(i),
			 this->weights_->begin(),
			 this->poly_w_evaluations_->row_begin(i));
      }
    //pkpk_ 
    this->update_pkpk();
    //alpha_ ??
    //beta_ ??
    //pi_at_x_ ??
    
  }

   /*!
    ** \brief Copy constructor
    ** \param other %DiscretePolyBase1d.
    */
  DiscretePolyBase1d(const self& other):
    range_size_(other.range_size_),
    size_(other.size_),
    degree_(other.degree_),
    collocations_(new slip::Array<T>(*(other.collocations_))),
    weights_(new slip::Array<T>(*(other.weights_))),
    alpha_(new slip::Array<T>(*(other.alpha_))),
    beta_(new slip::Array<T>(*(other.beta_))),
    pkpk_(new slip::Array<T>(*(other.pkpk_))),
    pi_at_x_(new slip::Array<T>(*(other.pi_at_x_))),
    poly_evaluations_(new slip::Array2d<T>(*(other.poly_evaluations_))),
    poly_w_evaluations_(new slip::Array2d<T>(*(other.poly_w_evaluations_))),
    poly_weight_func_(other.poly_weight_func_),
    poly_collocation_func_(other.poly_collocation_func_),
    normalized_(other.normalized_)
  {
  }

  /*!
  ** \brief Destructor of the 
  */
  ~DiscretePolyBase1d()
  {
    this->desallocate();
   
    
  }
  
/*@} End Constructors */
  
  /**
   ** \name Assignment operators
   */
  /*@{*
     /*!
    ** \brief Copy constructor
    ** \param other %DiscretePolyBase1d.
    */
  
  self& operator=(const self& other)
  {
     if(this == &other)
       {
	 return *this;
       }

     this->desallocate();

     this->range_size_ = other.range_size_;
     this->size_ = other.size_;
     this->degree_=other.degree_;
     
     this->collocations_ = new slip::Array<T>(*(other.collocations_));
    
     this->weights_= new slip::Array<T>(*(other.weights_));
     this->alpha_= new slip::Array<T>(*(other.alpha_));
     this->beta_= new slip::Array<T>(*(other.beta_));
     this->pkpk_= new slip::Array<T>(*(other.pkpk_));
     this->pi_at_x_= new slip::Array<T>(*(other.pi_at_x_));
     this->poly_evaluations_ = new slip::Array2d<T>(*(other.poly_evaluations_));
     this->poly_w_evaluations_ = new slip::Array2d<T>(*(other.poly_w_evaluations_));
     this->poly_weight_func_=other.poly_weight_func_;
     this->poly_collocation_func_=other.poly_collocation_func_;
     this->normalized_=other.normalized_;

	return *this;
  }
 /*@} End Assignment operators */


  //  /**
  //  ** \name Element access operators
  //  */
  // /*@{*
  //    /*!
  // ** \brief Subscript access to the data contained in the %DiscretePolyBase1d.
  // ** \param n The index of the element for which data should be
  // **  accessed.
  // ** \return Read/write reference to data.
  // ** \pre n < size()
  // **
  // ** This operator allows for easy, array-style, data access.
  // ** Note that data access with this operator is unchecked and
  // ** out_of_range lookups are not defined. 
  // */
  // reference operator[](const size_type n)
  // {
  //   assert(this->data_ != 0);
  //   assert(i < this->size_);
  //   return *(this->data_ + i);
  // }

   /*!
  ** \brief Get the i-th polynomial evaluations on the range.
  ** \param i Index of the polynomial of the base.
  ** \param first Pointer to the first element of the evalulation range.
  ** \param last Pointer to one-past-the-end of the evalulation range.
  */
  void get_poly(const std::size_t i,
  		T*& first,
  		T*& last)
  {
    first = (this->poly_evaluations_)->row_begin(i);
    last  = (this->poly_evaluations_)->row_end(i);

  }
  /*!
  ** \brief Get the i-th polynomial weighted evaluations on the range.
  ** \param i Index of the polynomial of the base.
  ** \param first Pointer to the first element of the weighted evalulation range.
  ** \param last Pointer to one-past-the-end of the weighted evalulation range.
  */
   void get_poly_w(const std::size_t i,
  		  T*& first,
  		  T*& last)
  {
    first = (this->poly_w_evaluations_)->row_begin(i);
    last  = (this->poly_w_evaluations_)->row_end(i);
    
  }
  
  /*!
  ** \brief Returns the evaluation of the i-th polynomial of the base at the j-th location.
  ** \param i Index of the polynomial of the base.
  ** \param j Index within the data range.
  ** \returns  the evaluation of the i-th polynomial of the base at the j-th location
  */
  T get_poly(const std::size_t i,
	     const std::size_t j)
  {
    assert(i < this->size_);
    assert(j < this->range_size_);
    return (*this->poly_evaluations_)[i][j];
  }
 
  /*!
  ** \brief Returns the weighted evaluation of the i-th polynomial of the base at the j-th location.
  ** \param i Index of the polynomial of the base.
  ** \param j Index within the data range.
  ** \returns  the weighted evaluation of the i-th polynomial of the base at the j-th location
  */
   T get_poly_w(const std::size_t i,
	     const std::size_t j)
  {
    assert(i < this->size_);
    assert(j < this->range_size_);
    return (*this->poly_w_evaluations_)[i][j];
  }


  //x belong to the polynomial definition range [-1,1] for Legendre for example
  template<typename RandomAccessIterator>
  T evaluate(const T& x, 
	     RandomAccessIterator coef_first,
	     RandomAccessIterator coef_last) const
  {
    T result = T();
    if(this->normalized_)
      {
	result = 
	  slip::clenshaw_poly_norm(x, 
				   this->alpha_->begin(),this->alpha_->end(),
				   this->beta_->begin(),this->beta_->end(), 
				   coef_first,coef_last,
				   this->size_);
      }
    else
      {
	result = 
	  slip::clenshaw_poly(x, 
			      this->alpha_->begin(),this->alpha_->end(),
			      this->beta_->begin(),this->beta_->end(), 
			      coef_first,coef_last,
			      this->size_);
      }
    return result;
  }
  

  template<typename RandomAccessIterator1,
	   typename RandomAccessIterator2,
	   typename RandomAccessIterator3>
  void evaluate(RandomAccessIterator1 x_first, 
		RandomAccessIterator1 x_last,
		RandomAccessIterator2 coef_first,
		RandomAccessIterator2 coef_last,
		RandomAccessIterator3 y_first,
		RandomAccessIterator3 y_last) const
  {
    if(this->normalized_)
      {
	 for(;x_first != x_last; ++x_first, ++y_first)
	   {
	     *y_first = 
	       slip::clenshaw_poly_norm(*x_first, 
					this->alpha_->begin(),this->alpha_->end(),
					this->beta_->begin(),this->beta_->end(), 
					coef_first,coef_last,
					this->size_);;
	   }
      }
    else
      {
	for(;x_first != x_last; ++x_first, ++y_first)
	   {
	     *y_first = 
	        slip::clenshaw_poly(*x_first, 
				    this->alpha_->begin(),this->alpha_->end(),
				    this->beta_->begin(),this->beta_->end(), 
				    coef_first,coef_last,
				    this->size_);
	   }
      }
  
  }


   //x belong to the polynomial definition range [-1,1] for Legendre for example
  template<typename RandomAccessIterator>
  T derivative(const T& x, 
	     RandomAccessIterator coef_first,
	     RandomAccessIterator coef_last) const
  {
    T der = static_cast<T>(0.0);
    if(this->normalized_)
      {
	der = slip::derivative_poly_norm(x,
					  this->alpha_->begin(),
					  this->alpha_->end(),
					  this->beta_->begin(),
					  this->beta_->end(),
					  coef_first,
					  coef_last,
					  this->degree_);
      }
    else
      {
	der = slip::derivative_poly(x,
				    this->alpha_->begin(),
				    this->alpha_->end(),
				    this->beta_->begin(),
				    this->beta_->end(),
				    coef_first,
				    coef_last,
				    this->degree_);
      }
    return der;
  }
 
 

  /*!
  ** \brief Generate the polynomial base.
  */
  void generate()
  {
  
    slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::compute_collocations();
    slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::compute_weights();
    slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::compute_polynomials();
 
  }

   /*!
  ** \brief Gram-Schmidt orthogonalisation.
  */
  void gram_schmidt()
  {
    typedef typename slip::Array2d<T>::row_iterator RowIterator;
    std::vector<std::pair<RowIterator,RowIterator> > base(this->size_);
  for(std::size_t i = 0; i < this->size_; ++i)
    {
      base[i].first  = poly_evaluations_->row_begin(i);
      base[i].second = poly_evaluations_->row_end(i);
      
    }
 
  this->modified_gram_schmidt(base);

  //update poly_w_evaluations_
  for(std::size_t i = 0; i < this->size_; ++i)
      {
	slip::multiplies(this->poly_evaluations_->row_begin(i),
			 this->poly_evaluations_->row_end(i),
			 this->weights_->begin(),
			 this->poly_w_evaluations_->row_begin(i));
      }
   
   this->update_pkpk();
   this->update_beta();
   this->update_alpha();

   //update pi_at_xi ??

  
  }

 /*!
  ** \brief Gram-Schmidt orthogonormalisation.
  */
 void gram_schmidt_normalized()
  {
    typedef typename slip::Array2d<T>::row_iterator RowIterator;
    std::vector<std::pair<RowIterator,RowIterator> > base(this->size_);
  for(std::size_t i = 0; i < this->size_; ++i)
    {
      base[i].first  = poly_evaluations_->row_begin(i);
      base[i].second = poly_evaluations_->row_end(i);
      
    }
   this->modified_gram_schmidt_normalized(base);
   //update poly_w_evaluations_
   for(std::size_t i = 0; i < this->range_size_; ++i)
      {
	slip::multiplies(this->poly_evaluations_->row_begin(i),
			 this->poly_evaluations_->row_end(i),
			 this->weights_->begin(),
			 this->poly_w_evaluations_->row_begin(i));
      }

   
   //std::fill(this->pkpk_->begin(),this->pkpk_->end(),T(1));
   
   //update normalized_
   this->normalized_ = true;
   //update pkk_, alpha_, beta_
   this->update_pkpk();
   this->update_beta();
   this->update_alpha();

   //pi_at_xi_ ??
   }

  /*!
  ** \brief Project data onto the P(i) polynomial of the base.
  ** \param first RandomAccessIterator on the data.
  ** \param last RandomAccessIterator on the data.
  ** \param i index of the polynomial.
  */
  template <typename RandomAccessIterator1>
  typename std::iterator_traits<RandomAccessIterator1>::value_type 
  project(RandomAccessIterator1 first,
	  RandomAccessIterator1 last,
	  const std::size_t i)
  {
    assert(i <= this->degree_);
    if(slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::normalized_)
      {
	return slip::inner_product(first,last,
				   slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_w_evaluations_->row_begin(i));
      }
    else
      {
	 return slip::inner_product(first,last,
				  slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_w_evaluations_->row_begin(i)) 
	      /
	      slip::inner_product(slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_w_evaluations_->row_begin(i),
				  slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_w_evaluations_->row_end(i),slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_evaluations_->row_begin(i));
      }
  }



   /*!
  ** \brief Project data onto the P(i) polynomial of the base.
  ** \param first RandomAccessIterator on the data.
  ** \param last RandomAccessIterator on the data.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \pre (coef_last - coef_first) <= this->size()
  */
  template <typename RandomAccessIterator1,
	    typename RandomAccessIterator2>
  void project(RandomAccessIterator1 first,
	       RandomAccessIterator1 last,
	       RandomAccessIterator2 coef_first,
	       RandomAccessIterator2 coef_last)
  {
    assert(static_cast<std::size_t>(coef_last - coef_first) <= this->size_);
    if(this->normalized_)
      {
	for(std::size_t i = 0;coef_first!=coef_last; ++i, ++coef_first)
	  {
	    *coef_first = 
	      slip::inner_product(first,last,
				  (this->poly_w_evaluations_)->row_begin(i));
	      
	  }
      }
    else
      {
	for(std::size_t i = 0;coef_first!=coef_last; ++i, ++coef_first)
	  {
	    *coef_first = 
	      slip::inner_product(first,last,
				  slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_w_evaluations_->row_begin(i)) 
	      /
	      slip::inner_product(slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_w_evaluations_->row_begin(i),
				  slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_w_evaluations_->row_end(i),slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_evaluations_->row_begin(i));
	  }
      }
  }
  
  /*!
  ** \brief Reconstruct the data from a coefficients range.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \param K number of coefficients to use for recontruct the data.
  ** \param first RandomAccessIterator on the data.
  ** \param last RandomAccessIterator on the data.
 s.
  ** \pre K <= (coef_last - coef_first)
  */
  template <typename RandomAccessIterator1,
	     typename RandomAccessIterator2>
  void reconstruct(RandomAccessIterator1 coef_first,
		   RandomAccessIterator1 coef_last,
		   const std::size_t K,
		   RandomAccessIterator2 first,
		   RandomAccessIterator2 last)
  {
    assert(K <= static_cast<std::size_t>(coef_last-coef_first)); 
    for(std::size_t j = 0; first != last; ++j, ++first)
      {
	*first = slip::inner_product(coef_first,coef_first+K,
				     slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_evaluations_->col_begin(j));
      }
  }

 /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const
  {
    return "DiscretePolyBase1d";
  }
  
  /*!
  ** \brief Returns the degree of the Base
  **
  */
  int degree() const
  {
    return slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::degree_;
  }

  /*!
  ** \brief Returns the order of the Base
  **
  ** \remarks Same value as degree
  */
  int order() const
  {
    return slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::degree_;
  }

  /*!
  ** \brief Returns the size of the base.
  **
  */
  std::size_t size() const
  {
    return slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::size_;
  }

   /*!
  ** \brief Returns the size of the domain
  **
  */
  std::size_t range_size() const
  {
    return slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::range_size_;
  }

  /*!
  ** \brief Returns true if the base is normalized, 
  ** else returns false
  **
  */
  bool is_normalized() const
  {
    return slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::normalized_;
  }

  /*!
  ** \brief Print polynomial basis features.
  **
  */
  void print()
  {
    
    std::cout<<"Collocation type = "<<poly_collocation_func_.name()<<std::endl;
    std::cout<<"Weight type = "<<poly_weight_func_.name()<<std::endl;
    std::cout<<"Normalized = "<<slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::normalized_<<std::endl;
    std::cout<<"Number of polynomials = "<<slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::size_<<std::endl;
    std::cout<<"Base degree = "<<slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::degree_<<std::endl;
     
    std::cout<<"Range size = "<<slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::range_size_<<std::endl;
    

    std::cout<<"Collocations = \n";
    if(slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::collocations_ != 0)
      {
	std::cout<<*(this->collocations_)<<std::endl;
      }
    std::cout<<"Weights = \n";
    if(slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::weights_ != 0)
      {
	std::cout<<*(this->weights_)<<std::endl;
      }
    
    std::cout<<"(Pk,Pk) inner products =\n"<<*(this->pkpk_)<<std::endl;
    std::cout<<"Polynomial evaluations =\n"<<*(this->poly_evaluations_)<<std::endl;
    std::cout<<"Polynomial x weight evaluations =\n"<<*(this->poly_w_evaluations_)<<std::endl;
     
  }
   

  /*!
  ** \brief Computes the inner_product between Pi and Pj.
  ** \param i index of the first polynomial.
  ** \param j index of the second polynomial.
  ** \return the value of the inner product.
  ** \pre (i >  this->size_)
  ** \pre (j >  this->size_)
  */
  T inner_product(const std::size_t i, const std::size_t j)
  {
    assert(i < this->size_);
    assert(j < this->size_);
    return slip::inner_product(slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_evaluations_->row_begin(i),
			       slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_evaluations_->row_end(i),
			       slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_w_evaluations_->row_begin(j));
  }

  /*!
  ** \brief Computes the Gram matrix of the polynomial base.
  ** \param Gram The resulting Gram matrix.
  */
  template<typename Matrix>
  void orthogonality_matrix(Matrix& Gram)
  {
    Gram.resize(slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::size_,slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::size_);
    for(std::size_t i = 0; i < slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::size_; ++i)
      {
	for(std::size_t j = 0; j <= i; ++j)
	  {
	    Gram[i][j] =  slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::inner_product(i,j);
	    Gram[j][i] = Gram[i][j];
	  }
      }
  }

   /*!
  ** \brief Computes the Jacobi matrix of the polynomial base.
  ** \param Jacobi The resulting Jacobi matrix.
  */
  template<typename Matrix>
  void jacobi_matrix(Matrix& Jacobi)
  {
    slip::Array<T> sqrt_beta(this->size_-1,
			     this->beta_->begin() + 1,
			     this->beta_->end());
    slip::apply(sqrt_beta.begin(),sqrt_beta.end(),sqrt_beta.begin(),std::sqrt);
    Jacobi.resize(this->size_,this->size_,static_cast<T>(0.0));
    slip::set_diagonal(this->alpha_->begin(),this->alpha_->end(),
			Jacobi);
    slip::set_diagonal(sqrt_beta.begin(),sqrt_beta.end(),
			Jacobi,
			1);
    slip::set_diagonal(sqrt_beta.begin(),sqrt_beta.end(),
			Jacobi,
			-1);
    
   
  }
 
  /*!
  ** \brief Indicate if the polynomial basis is orthogonal according to the tolerance \a tol.
  ** \param tol Tolerance of the orthogonality .
  ** \result True if orthogonal, false else.
  */
  bool is_orthogonal(const T tol 
		   = slip::epsilon<T>())
  {
    slip::Matrix<T> Gram;
    slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::orthogonality_matrix(Gram);
    return slip::is_diagonal(Gram,tol);
  }


 
  typename slip::Array<T>::const_iterator
  alpha_begin()
  {
    return alpha_->begin();
    
  }

  typename slip::Array<T>::const_iterator
  alpha_end()
  {
    return alpha_->end();
    
  }

  typename slip::Array<T>::const_iterator
  beta_begin()
  {
    return beta_->begin();
    
  }

  typename slip::Array<T>::const_iterator
  beta_end()
  {
    return beta_->end();
    
  }

 typename slip::Array<T>::const_iterator
  weights_begin()
  {
    return weights_->begin();
    
  }

  typename slip::Array<T>::const_iterator
  weights_end()
  {
    return weights_->end();
    
  }

  //Approximate memory size requirements: 4(N+D+1) + 2ND
  //N: size of the range
  //D: degree of the base
private:
  std::size_t range_size_;               ///size of the range
  std::size_t size_;///size of the base : number of polynomials
  int degree_;              ///degree of the base
  slip::Array<T>* collocations_; ///collocations values
  slip::Array<T>* weights_;      ///weights values
  slip::Array<T>* alpha_; /// three-terms recurrence alpha coefficient
  slip::Array<T>* beta_; /// three-terms recurrence beta coefficient
  slip::Array<T>* pkpk_; /// (pk,pk) inner products
  slip::Array<T>* pi_at_x_; /// contains the evaluations of the Pi at x
  ///pi_at_x_ is used to optimized multiple evaluations at various locations
  //  std::vector<slip::Polynomial<T> >* analytic_base_;  ///analytic base

  //each row i corresponds to the evaluation of the ith polynomial 
  //of the base: (Pi(collocations_[0]), Pi(collocations_[1]),...)
  slip::Array2d<T>* poly_evaluations_; 
   //each row i corresponds to the evaluation of the ith polynomial 
  //of the base times the weight: (Pi(collocations_[0])weights_[0], Pi(collocations_[1])weights_[1],...)
  slip::Array2d<T>* poly_w_evaluations_; 
  PolyWeight poly_weight_func_; ///polynomial weight functor
  PolyCollocation poly_collocation_func_; ///collocation functor
  bool normalized_; ///boolean indicated if the base is normalized or not

private:
  /*!
  ** \brief Computes the collocations on the range.
  */
  void compute_collocations()
  {
    this->poly_collocation_func_(slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::collocations_->begin(),
				slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::collocations_->end());
 
  }
   /*!
  ** \brief Computes the weights on the range.
  */
  void compute_weights()
  {

    	std::transform(slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::collocations_->begin(),
		       slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::collocations_->end(),
		       slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::weights_->begin(),
		       poly_weight_func_);
  }
 
  /*!
  ** \brief Computes the polynomial basis.
  */
  void compute_polynomials()
  {
    if(this->degree_ != -1)
      {
	slip::stieltjes(slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::collocations_->begin(),
			slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::collocations_->end(),
			slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::weights_->begin(),
			slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::weights_->end(),
			slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::degree_,
			slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::alpha_->begin(),
			slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::alpha_->end(),
			slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::beta_->begin(),
			slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::beta_->end(),
			slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::pkpk_->begin(),
			slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::pkpk_->end(),
			*slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_evaluations_,
			*slip::DiscretePolyBase1d<T,PolyWeight,PolyCollocation>::poly_w_evaluations_);

	if(this->normalized_)
	  {
	    T scalar = T();
	    for(std::size_t i = 0; i < this->size_; ++i)
	      {
		scalar =  std::sqrt(static_cast<T>(1)/(*(this->pkpk_))[i]);
		slip::multiplies_scalar((this->poly_evaluations_)->row_begin(i),
					(this->poly_evaluations_)->row_end(i),
					scalar,
					(this->poly_evaluations_)->row_begin(i));
		slip::multiplies_scalar((this->poly_w_evaluations_)->row_begin(i),
					(this->poly_w_evaluations_)->row_end(i),
					scalar,
					(this->poly_w_evaluations_)->row_begin(i));
	      } 
	     
	  }

      }
  }

  // /*!
  // ** \brief Computes analytic polynomials.
  // */
  // void compute_analytic_polynomials()
  // {
  //
  // }

  /*!
  ** \brief Desallocate the polynomial base data.
  */
  void desallocate()
  {
    if(this->collocations_ != 0)
       {
	 delete this->collocations_;
       }
   if(this->weights_ != 0)
       {
	 delete this->weights_;
       }
   if(this->alpha_ != 0)
       {
	 delete this->alpha_;
       }
  if(this->beta_ != 0)
       {
	 delete this->beta_;
       }
  if(this->pkpk_ != 0)
       {
	 delete this->pkpk_;
       }
  if(this->pi_at_x_ != 0)
       {
	 delete this->pi_at_x_;
       }
  if(this->poly_evaluations_ != 0)
       {
	 delete this->poly_evaluations_;
       }
   if(this->poly_w_evaluations_ != 0)
       {
	 delete this->poly_w_evaluations_;
       }
     }
  

  
  template <typename RowIterator>
  void modified_gram_schmidt(const std::vector<std::pair<RowIterator,RowIterator> >& base)
  {
    slip::Array<T> Pk_tmp(this->range_size_);
    std::pair<RowIterator,RowIterator> tmp;
    tmp.first = Pk_tmp.begin();
    tmp.second = Pk_tmp.end();
    
     slip::discrete_poly_inner_prod<RowIterator,RowIterator,T> 
       inner_prod(this->weights_);
     std::size_t  init_base_first_size = base.size();
       
     //???
     std::copy(poly_evaluations_->row_begin(0),poly_evaluations_->row_begin(0),
	       poly_evaluations_->row_begin(0));

     for(std::size_t k = 0; k < init_base_first_size; ++k)
       {
	  T PkPk = std::sqrt(inner_prod(base[k],base[k]));
	  slip::divides_scalar(base[k].first,base[k].second,
			       PkPk,
			       tmp.first);
	  for(std::size_t q = (k + 1); q < init_base_first_size; ++q)
	    {
	      T PkPq = inner_prod(base[q],tmp);
	      RowIterator it_basek = Pk_tmp.begin();
	      RowIterator it_baseq = base[q].first;
	      for(; it_baseq != base[q].second; ++it_baseq, ++it_basek)
		{
		  *it_baseq = *it_baseq - PkPq * *it_basek;
		}
	    }
       }
  }


  
 template <typename RowIterator>
  void modified_gram_schmidt_normalized(const std::vector<std::pair<RowIterator,RowIterator> >& base)
  {
     slip::discrete_poly_inner_prod<RowIterator,RowIterator,T> 
       inner_prod(this->weights_);
     std::size_t  init_base_first_size = base.size();
       
     //std::copy(poly_evaluations_->row_begin(0),poly_evaluations_->row_begin(0),
     //       poly_evaluations_->row_begin(0));

     for(std::size_t k = 0; k < init_base_first_size; ++k)
       {
	  T PkPk = std::sqrt(inner_prod(base[k],base[k]));
	  slip::divides_scalar(base[k].first,base[k].second,
			       PkPk,
			       base[k].first);
	  for(std::size_t q = (k + 1); q < init_base_first_size; ++q)
	    {
	      T PkPq = inner_prod(base[q],base[k]);
	      RowIterator it_baseq = base[q].first;
	      RowIterator it_basek = base[k].first;
	      for(; it_baseq != base[q].second; ++it_baseq, ++it_basek)
		{
		  *it_baseq = *it_baseq - PkPq * *it_basek;
		}
	    }
       }

  }

  T  alpha_inner_product(const std::size_t k)
  {
    slip::Array<T> xPk(this->range_size_,
		       this->poly_evaluations_->row_begin(k),
		       this->poly_evaluations_->row_end(k));
    slip::multiplies(this->collocations_->begin(),
		     this->collocations_->end(),
		     xPk.begin(),xPk.begin());
    return this->project(xPk.begin(),xPk.begin(),k);

  }

  void update_pkpk()
  {
    for(std::size_t k = 0; k < this->size_; ++k)
     {
       (*(this->pkpk_))[k] = this->inner_product(k,k);
     }
  }

  void update_beta()
  {
   typename slip::Array<T>::iterator it_beta  = this->beta_->begin();
   typename slip::Array<T>::iterator it_pkpk  = this->pkpk_->begin();
   *it_beta++ = *it_pkpk++;
   for(; it_beta != this->beta_->end();++it_beta,++it_pkpk)
     {
       *it_beta = *it_pkpk / *(it_pkpk-1);
     }
  }

  void update_alpha()
  {
    typename slip::Array<T>::iterator it_alpha = this->alpha_->begin();
    typename slip::Array<T>::iterator it_pkpk  = this->pkpk_->begin();
    for(std::size_t k = 0; 
       it_alpha != this->alpha_->end();
       ++k,++it_alpha,++it_pkpk)
     {
       *it_alpha = alpha_inner_product(k) / *it_pkpk;
     }
  }

};

}//::slip


#endif //SLIP_DISCRETEPOLYBASE1D_HPP
