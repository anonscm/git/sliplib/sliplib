/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file iterator3d_plane.hpp
 * 
 * \brief Provides a class to iterate 3d containers throw a planes.
 * 
 */
#ifndef SLIP_ITERATOR3D_PLANE_HPP
#define SLIP_ITERATOR3D_PLANE_HPP

#include <iterator>
#include <cassert>
#include "DPoint.hpp"
#include "Point2d.hpp"
#include "DPoint2d.hpp"
#include "Box2d.hpp"
#include "Box3d.hpp"
#include "iterator_types.hpp"

namespace slip
{
 /** 
      \enum PLANE_ORIENTATION
      \brief Choose between different plane orientations 
      \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
      \date 2008/02/13
      \code
      enum  
      {
      // Perpendicular to k axis
      IJ_PLANE, 
      // Perpendicular to i axis
      KJ_PLANE,
      // Perpendicular to j axis
      KI_PLANE
      };
      \endcode
      \image html iterator3d_plane_conventions.jpg "axis and notation conventions"
      \image latex iterator3d_plane_conventions.eps "axis and notation conventions" width=10cm
 */
  
  
  enum PLANE_ORIENTATION
    {
      IJ_PLANE = 0, 
      KJ_PLANE, 
      KI_PLANE
    };

  /*! \class iterator3d_plane
  ** 
  ** \brief This is some iterator to iterate a 3d container into a %plane area 
  **        defined by the subscripts of the 3d container.
  ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/01/09
  **
  ** \param Container3D Type of 3d container in the iterator3d_plane 
  **
  ** \par Description:
  **      This iterator is an 3d extension of the random_access_iterator.
  **      It is compatible with the bidirectional_iterator of the
  **      Standard library. It iterate into a plane area defined inside the
  **      subscripts range of the 3d container. Those subscripts are defined as 
  **      follows :
  ** \image html iterator3d_plane_conventions.jpg "axis and notation conventions"
  ** \image latex iterator3d_plane_conventions.eps "axis and notation conventions" width=10cm
  **
  */
  template <class Container3D>
  class iterator3d_plane  
  {
  private:
    
    /*!
    ** \brief calculate pos_
    **
    */
    void pos_calc()
    {
      if(zerodim_ == 0)
	{
	  this->pos_ = cont3d_->begin() + 
	    (this->xp_ * int(cont3d_->rows() * int(cont3d_->cols()))) + 
	    (this->x1_* int(cont3d_->cols())) + this->x2_;
	}
      else if(zerodim_ == 1)
	{
	  this->pos_ = cont3d_->begin() + 
	    (this->x1_ * int(cont3d_->rows() * int(cont3d_->cols()))) + 
	    (this->xp_* int(cont3d_->cols())) + this->x2_;
	}
      else
	{
	  this->pos_ = cont3d_->begin() + 
	    (this->x1_ * int(cont3d_->rows() * int(cont3d_->cols()))) + 
	    (this->x2_* int(cont3d_->cols())) + this->xp_;
	}
    }

  public:
    //typedef std::random_access_iterator_tag iterator_category;
    typedef std::random_access_iterator2d_tag iterator_category;
    typedef typename Container3D::value_type value_type;
    typedef DPoint2d<int> difference_type;
    typedef typename Container3D::pointer pointer;
    typedef typename Container3D::reference reference;

    typedef iterator3d_plane self;

    typedef typename Container3D::size_type size_type;
    /**
     ** \name Constructors & Destructors
     */
    /*@{*/
  
    /*!
    ** \brief Constructs a %iterator3d_plane.
    ** \par The box to iterate is in this case the full container
    */
    iterator3d_plane():
      cont3d_(0),pos_(0),zerodim_(0),xp_(0),x1_(0),x2_(0),box_(0,0,0,0)
    {}

    /*!
    ** \brief Constructs a %iterator3d_plane.
    ** \param c pointer to an existing 3d container
    ** \param P number of the zero dimension (the axe that is 
    ** perpendicular to the plane of b)
    ** \param plane_coordinate coordinate of the plane along the zero dimension axe 
    ** \param b Box2d<int> defining the range of the plane to iterate
    ** \pre box subscripts must be inside those of the 3d container
    ** \pre P has to be PLANE_ORIENTATION type or 0,1 or 2
    ** \pre plane_coordinate must be within the range of c
    */
    iterator3d_plane(Container3D* c, PLANE_ORIENTATION P, const int plane_coordinate, const Box2d<int>& b):
      cont3d_(c),pos_(0),zerodim_(P),xp_(plane_coordinate),x1_(b.upper_left()[0]),x2_(b.upper_left()[1]),box_(b)
    {
      assert((P == IJ_PLANE) || (P == KJ_PLANE) || (P == KI_PLANE));
      this->pos_calc();
    }

    /*!
    ** \brief Constructs a %iterator3d_plane.
    ** \param c pointer to an existing 3d container
    ** \param b Box3d<int> defining the range of plane to iterate
    ** \pre box subscripts must be inside those of the 3d container
    ** \pre box must be a plane
    */
    iterator3d_plane(Container3D* c,
		   const Box3d<int>& b):
      cont3d_(c),pos_(0),zerodim_(0),xp_(0),x1_(0),x2_(0),box_(0,0,0,0)
    {
      DPoint<int,3> dim = b.back_bottom_right() - b.front_upper_left();
      zerodim_ = ((dim[0] == 0)?0:((dim[1] == 0)?1:((dim[2] == 0)?2:-1)));
      assert(zerodim_ != -1);
      xp_ = b.back_bottom_right()[zerodim_];
      if (zerodim_ == 0)
	{
	  x1_ = b.front_upper_left()[1];
	  x2_ = b.front_upper_left()[2];
	  box_ = slip::Box2d<int>(x1_,x2_,b.back_bottom_right()[1],b.back_bottom_right()[2]);
	}
      else if (zerodim_ == 1)
	{
	  x1_ = b.front_upper_left()[0];
	  x2_ = b.front_upper_left()[2];
	  box_ = slip::Box2d<int>(x1_,x2_,b.back_bottom_right()[0],b.back_bottom_right()[2]);
	}
      else
	{
	  x1_ = b.front_upper_left()[0];
	  x2_ = b.front_upper_left()[1];
	  box_ = slip::Box2d<int>(x1_,x2_,b.back_bottom_right()[0],b.back_bottom_right()[1]);
	}
      this->pos_calc();
    }

    /*!
    ** \brief Constructs a copy of the %iterator3d_plane \a o
    ** 
    ** \param o %iterator3d_plane to copy.
    */
    iterator3d_plane(const self& o):
      cont3d_(o.cont3d_),pos_(o.pos_),zerodim_(o.zerodim_),xp_(o.xp_),x1_(o.x1_),x2_(o.x2_),box_(o.box_)
    {}

    /*@} End Constructors */

    /**
     ** \name  Assignment operators and methods
     */
    /*@{*/
    /*!
    ** \brief Assign a %iterator3d_plane.
    **
    ** Assign elements of %iterator3d_plane in \a o.
    **
    ** \param o %iterator3d_plane to get the values from.
    ** \return a %iterator3d_plane reference.
    */
    self& operator=(const self& o)
    {
      if(this != &o)
	{
	  this->cont3d_ = o.cont3d_;
	  this->pos_ = o.pos_;
	  this->zerodim_ = o.zerodim_;
	  this->xp_ = o.xp_;
	  this->x1_ = o.x1_;
	  this->x2_ = o.x2_;
	  this->box_ = o.box_;
	}
      return *this;
    }
    /*@} End Assignment operators and methods*/

    /*!
    ** \brief Dereference assignment operator. Returns the element 
    **        that the current %iterator3d_plane i point to.  
    ** 
    ** \return a %iterator3d_plane reference.       
    */
    inline
    reference operator*()
    {
      return *pos_;
    }

 

    /*!
    ** \brief Dereference operator.  Returns the element 
    **        that the current %iterator3d_plane i point to.  
    ** 
    ** \return a const %iterator3d_plane reference.     
    */
    inline
    const reference operator*() const
    {
      return *pos_;
    }


    inline
    pointer operator->() const
    { 
      return &(operator*()); 
    }

    inline
    pointer operator->()
    { 
      return &(operator*()); 
    }

    /**
     ** \name  Forward operators addons
     */
    /*@{*/
  
    /*!
    ** \brief Preincrement a %iterator3d_plane. Iterate to the next location
    **        inside the %Box3d.
    **          
    */
    inline
    self& operator++()
    {
      if(x2_ < (box_.bottom_right())[1])
	{
	  this->x2_++;
	  this->pos_calc();
	}
      else if(x1_ < (box_.bottom_right())[0])
	{
	  this->x1_++;
	  this->x2_ = (box_.upper_left())[1];
	  this->pos_calc();
	}
      else
	{
	  this->x1_ = (box_.bottom_right())[0] + 1;
	  this->x2_ = (box_.bottom_right())[1] + 1;
	  this->pos_calc();
	}
      return *this;
    }

    /*!
    ** \brief Postincrement a %iterator3d_plane. Iterate to the next location
    **        inside the %Box3d.
    **          
    */
    inline
    self operator++(int)
    {
      self tmp = *this;
      ++(*this);
      return tmp;
    }

    /*@} End Forward operators addons */

    /**
     ** \name  Bidirectional operators addons
     */
    /*@{*/
    /*!
    ** \brief Predecrement a %iterator3d_plane. Iterate to the previous location
    **        inside the %Box3d.
    **          
    */
    inline
    self& operator--()
    {
      if(x2_ > (box_.upper_left())[1])
	{
	  this->x2_--;
	  this->pos_calc();
	}
      else if (x1_ > (box_.upper_left())[0])
	{
	  this->x1_--;
	  this->x2_ = (box_.bottom_right())[1];
	  this->pos_calc();
	}
      else
	{
	  this->x1_ = (box_.upper_left())[0] - 1;
	  this->x2_ = (box_.upper_left())[1] - 1;
	  this->pos_calc();
	}
      
      return *this;
    }
    
    /*!
    ** \brief Postdecrement a %iterator3d_plane. Iterate to the previous location
    **        inside the %Box3d.
    **          
    */
    inline
    self operator--(int)
    {
      self tmp = *this;
      --(*this);
      return tmp;
    }

    /*@} End Bidirectional operators addons */
  
    /**
     ** \name  Equality comparable operators
     */
    /*@{*/
    /*!
    ** \brief Equality operator.
    ** \param i1 first %iterator3d_plane.
    ** \param i2 second %iterator3d_plane.
    ** \return true if i1 and i2 point to the same element of
    **         the 3d container
    */
    inline
    friend bool operator==(const self& i1,
			   const self& i2)
    {
    
      return ( (i1.cont3d_ == i2.cont3d_) && (i1.pos_ == i2.pos_) 
	       && (i1.zerodim_ == i2.zerodim_) && (i1.xp_ == i2.xp_) 
	       && (i1.x1_ == i2.x1_) && (i1.x2_ == i2.x2_) );
    }

    /*!
    ** \brief Inequality operator.
    ** \param i1 first %iterator3d_plane.
    ** \param i2 second %iterator3d_plane.
    ** \return true if !(i1 == i2)
    */
    inline
    friend bool operator!=(const self& i1,
			   const self& i2)
    {
      return ( (i1.cont3d_ != i2.cont3d_) || (i1.pos_ != i2.pos_)
	       || (i1.zerodim_ != i2.zerodim_) || (i1.xp_ != i2.xp_) 
	       || (i1.x1_ != i2.x1_) || (i1.x2_ != i2.x2_));
    }
    /*@} End Equality comparable operators*/

    /**
     ** \name  Strict Weakly comparable operators
     */
    /*@{*/
    /*!
    ** \brief < operator.
    ** \param i1 first %iterator3d_plane.
    ** \param i2 second %iterator3d_plane.
    ** \return true i1.pos_ < i2_pos
    */
    inline
    friend bool operator<(const self& i1,
			  const self& i2)
    {
      
      return ( i1.pos_ < i2.pos_);
    }
    
    /*!
    ** \brief > operator.
    ** \param i1 first %iterator3d_plane.
    ** \param i2 second %iterator3d_plane.
    ** \return true i2 < i1
    */
    inline
    friend bool operator>(const self& i1,
			  const self& i2)
    {
      
      return (i2 < i1);
    }
    
    /*!
    ** \brief <= operator.
    ** \param i1 first %iterator3d_plane.
    ** \param i2 second %iterator3d_plane.
    ** \return true if !(i2 < i1)
    */
    inline
    friend bool operator<=(const self& i1,
			   const self& i2)
    {
      
      return  !(i2 < i1);
    }
    
    /*!
    ** \brief >= operator.
    ** \param i1 first %iterator3d_plane.
    ** \param i2 second %iterator3d_plane.
    ** \return true if !(i1 < i2)
    */
    inline
    friend bool operator>=(const self& i1,
			   const self& i2)
    {
      
      return  !(i1 < i2);
    }
    /*@} End  Strict Weakly comparable operators*/
    
    
    /**
     ** \name  iterator3d_plane operators addons
     */
    /*@{*/
    /*!
    ** \brief iterator3d_plane addition.
    ** \param d difference_type
    ** \return a iterator3d_plane reference 
    ** \pre All the iterators between the current iterator i and i + d 
    **      must be dereferenceable.
    */
    inline
    self& operator+=(const difference_type& d)
    {
      this->x1_ += d.dx1();
      this->x2_ += d.dx2();
      this->pos_calc();
      return *this;
    }

 
  
    /*!
    ** \brief iterator3d_plane substraction.
    ** \param d difference_type
    ** \return a iterator3d_plane reference 
    ** \pre All the iterators between the current iterator i and i - d 
    **      must be dereferenceable.
    */
    inline
    self& operator-=(const difference_type& d)
    {
      this->x1_ -= d.dx1();
      this->x2_ -= d.dx2();
      this->pos_calc();
      return *this;
    }
  

   
    /*!
    ** \brief iterator3d_plane addition.
    ** \param d difference_type
    ** \return a iterator3d_plane reference 
    ** \pre All the iterators between the current iterator i and i + d 
    **      must be dereferenceable.
    */
    inline
    self operator+(const difference_type& d)
    {
      self tmp = *this;
      tmp += d;
      return tmp;
    }
  
    /*!
    ** \brief iterator3d_plane substraction.
    ** \param d difference_type
    ** \return a iterator3d_plane reference 
    ** \pre All the iterators between the current iterator i and i - d 
    **      must be dereferenceable.
    */
    inline
    self operator-(const difference_type& d)
    {
      self tmp = *this;
      tmp -= d;
      return tmp;
    }

    /*!
    ** \brief %iterator3d_plane difference operator.
    ** \param i1 first %iterator3d_plane.
    ** \param i2 second %iterator3d_plane.
    ** \return a difference_type d such that i1 = i2 + d.
    ** \pre Either i1 is reachable from i2, or i2 is reachable from i1.
    */ 
    inline
    friend difference_type operator-(const self& i1,
				     const self& i2)
    {
      return difference_type(int(i1.x1_ - i2.x1_),int(i1.x2_ - i2.x2_));
    }
  
  
    /*!
    ** \brief %iterator3d_plane element assignment operator.
    **        Equivalent to *(k + d) = t.
    ** \param d difference_type.
    ** \return a reference to the 3d container elements
    ** \pre (k + d) exists and is dereferenceable.
    ** \post k[d] is a copy of reference.
    */ 
    inline
    reference operator[](difference_type d)
    {
      if(zerodim_ == 0)
	return (*cont3d_)[this->xp_][this->x1_+d.dx1()][this->x2_+d.dx2()];
      if(zerodim_ == 1)
	return (*cont3d_)[this->x1_+d.dx1()][this->xp_][this->x2_+d.dx2()];
      return (*cont3d_)[this->x1_+d.dx1()][this->x2_+d.dx2()][this->xp_];
    }

    /*!
    ** \brief %iterator3d_plane element assignment operator.
    **        Equivalent to *(k + n).
    ** \param n offset.
    ** \return a pointer of pointer to the 3d container elements
    ** \pre (k + n) exists and is dereferenceable.
    ** \post k[n] is a copy of pointer of pointer.
    ** \todo add assert
    */ 
    inline
    pointer* operator[](int n)
    {
      if(zerodim_ == 0)
	return (*cont3d_)[this->xp_+ n];
      return (*cont3d_)[this->x1_+ n];
    }


    /*@} End  iterator3d_plane operators addons */
  

    /**
     ** \name Subscripts accessors
     */
    /*@{*/
    /*!
    ** \brief Access to the first subscript of the current %iterator3d_plane.
    ** \return the first subscript.
    */
    inline
    int x1() const
    {
      return this->x1_;
    }
    
    /*!
    ** \brief Access to the second subscript of the current %iterator3d_plane.
    ** \return the second subscript.
    */
    inline
    int x2() const
    {
      return this->x2_;
    }
    
    /*!
    ** \brief Access to the plane subscript of the current %iterator3d_plane.
    ** \return the third subscript.
    */
    inline
    int xp() const
    {
      return this->xp_;
    }
    /*!
    ** \brief Access to the plane axe number of the current %iterator3d_plane.
    ** \return the third subscript.
    */
    inline
    int zerodim() const
    {
      return this->zerodim_;
    }
  
    /*@} End  Subscripts accessors */


  
  private: 
    Container3D* cont3d_; // pointer to the 3d container
    pointer pos_;         // linear position within the container
    int zerodim_;         // index of the plane axe (0->k,1->i,2->j)
    int xp_;              // position of the plane
    int x1_;              // first subscript position (within the plane)
    int x2_;              // second subscript position (within the plane)
    Box2d<int> box_;      // plane to iterate 

 };

  
  /*! \class const_iterator3d_plane
  ** 
  ** \brief This is some iterator to iterate a 3d container into a %plane area 
  **        defined by the subscripts of the 3d container.
  ** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2008/01/09
  **
  ** \param Container3D Type of 3d container in the const_iterator3d_plane 
  **
  ** \par Description:
  **      This iterator is an 3d extension of the random_access_iterator.
  **      It is compatible with the bidirectional_iterator of the
  **      Standard library. It iterate into a plane area defined inside the
  **      subscripts range of the 3d container. Those subscripts are defined as 
  **      follows :
  **
  ** \image html iterator3d_plane_conventions.jpg "axis and notation conventions"
  ** \image latex iterator3d_plane_conventions.eps "axis and notation conventions" width=10cm
  */
  template <class Container3D>
  class const_iterator3d_plane  
  {
   private:
   
    /*!
    ** \brief calculate pos_
    **
    */
    inline
    void pos_calc()
    {
      if(zerodim_ == 0)
	{
	  this->pos_ = cont3d_->begin() + 
	    (this->xp_ * int(cont3d_->rows() * int(cont3d_->cols()))) + 
	    (this->x1_* int(cont3d_->cols())) + this->x2_;
	}
      else if(zerodim_ == 1)
	{
	  this->pos_ = cont3d_->begin() + 
	    (this->x1_ * int(cont3d_->rows() * int(cont3d_->cols()))) + 
	    (this->xp_* int(cont3d_->cols())) + this->x2_;
	}
      else
	{
	  this->pos_ = cont3d_->begin() + 
	    (this->x1_ * int(cont3d_->rows() * int(cont3d_->cols()))) + 
	    (this->x2_* int(cont3d_->cols())) + this->xp_;
	}
    }
    
  public:
    //typedef std::random_access_iterator_tag iterator_category;
    typedef std::random_access_iterator2d_tag iterator_category;
    typedef typename Container3D::value_type value_type;
    typedef DPoint2d<int> difference_type;
    typedef typename Container3D::pointer pointer;
    typedef typename Container3D::const_pointer const_pointer;
    typedef typename Container3D::const_reference reference;

    typedef const_iterator3d_plane self;

    typedef typename Container3D::size_type size_type;
    /**
     ** \name Constructors & Destructors
     */
    /*@{*/
  
    /*!
    ** \brief Constructs a %const_iterator3d_plane.
    ** \par The box to iterate is in this case the full container
    */
    const_iterator3d_plane():
      cont3d_(0),pos_(0),zerodim_(0),xp_(0),x1_(0),x2_(0),box_(0,0,0,0)
    {}

  
    /*!
    ** \brief Constructs a %iterator3d_plane.
    ** \param c pointer to an existing 3d container
    ** \param P number of the zero dimension (the axe that is 
    ** perpendicular to the plane of b)
    ** \param plane_coordinate coordinate of the plane along the zero dimension axe 
    ** \param b Box2d<int> defining the range of the plane to iterate
    ** \pre box subscripts must be inside those of the 3d container
    ** \pre P has to be PLANE_ORIENTATION type or 0,1 or 2
    ** \pre plane_coordinate must be within the range of c
    */
    const_iterator3d_plane(Container3D* c, PLANE_ORIENTATION P, const int plane_coordinate, const Box2d<int>& b):
      cont3d_(c),pos_(0),zerodim_(P),xp_(plane_coordinate),x1_(b.upper_left()[0]),x2_(b.upper_left()[1]),box_(b)
    {
      assert((P == IJ_PLANE) || (P == KJ_PLANE) || (P == KI_PLANE));
      this->pos_calc();
    }

    /*!
    ** \brief Constructs a %const_iterator3d_plane.
    ** \param c pointer to an existing 3d container
    ** \param b Box3d<int> defining the range of plane to iterate
    ** \pre box subscripts must be inside those of the 3d container
    ** \pre box must be a plane
    */
    const_iterator3d_plane(Container3D* c,
		   const Box3d<int>& b):
      cont3d_(c),pos_(0),zerodim_(0),xp_(0),x1_(0),x2_(0),box_(0,0,0,0)
    {
      DPoint<int,3> dim = b.back_bottom_right() - b.front_upper_left();
      zerodim_ = ((dim[0] == 0)?0:((dim[1] == 0)?1:((dim[2] == 0)?2:-1)));
      assert(zerodim_ != -1);
      xp_ = b.back_bottom_right()[zerodim_];
      if (zerodim_ == 0)
	{
	  x1_ = b.front_upper_left()[1];
	  x2_ = b.front_upper_left()[2];
	  box_ = slip::Box2d<int>(x1_,x2_,b.back_bottom_right()[1],b.back_bottom_right()[2]);
	}
      else if (zerodim_ == 1)
	{
	  x1_ = b.front_upper_left()[0];
	  x2_ = b.front_upper_left()[2];
	  box_ = slip::Box2d<int>(x1_,x2_,b.back_bottom_right()[0],b.back_bottom_right()[2]);
	}
      else
	{
	  x1_ = b.front_upper_left()[0];
	  x2_ = b.front_upper_left()[1];
	  box_ = slip::Box2d<int>(x1_,x2_,b.back_bottom_right()[0],b.back_bottom_right()[1]);
	}
      this->pos_calc();
    }

    /*!
    ** \brief Constructs a copy of the %const_iterator3d_plane \a o
    ** 
    ** \param o %iterator3d_plane to copy.
    */
    const_iterator3d_plane(const self& o):
      cont3d_(o.cont3d_),pos_(o.pos_),zerodim_(o.zerodim_),xp_(o.xp_),x1_(o.x1_),x2_(o.x2_),box_(o.box_)
    {}

    /*@} End Constructors */


    /**
     ** \name  Assignment operators and methods
     */
    /*@{*/
    /*!
    ** \brief Assign a %const_iterator3d_plane.
    **
    ** Assign elements of %const_iterator3d_plane in \a o.
    **
    ** \param o %const_iterator3d_plane to get the values from.
    ** \return a %const_iterator3d_plane reference.
    */
    self& operator=(const self& o)
    {
       if(this != &o)
	{
	  this->cont3d_ = o.cont3d_;
	  this->pos_ = o.pos_;
	  this->zerodim_ = o.zerodim_;
	  this->xp_ = o.xp_;
	  this->x1_ = o.x1_;
	  this->x2_ = o.x2_;
	  this->box_ = o.box_;
	}
      return *this;
    }
    /*@} End Assignment operators and methods*/

 
 

    /*!
    ** \brief Dereference operator.  Returns the element 
    **        that the current %const_iterator3d_plane i point to.  
    ** 
    ** \return a const %const_iterator3d_plane reference.     
    */
    inline
    reference operator*() const
    {
      return *pos_;
    }


    inline
    const_pointer operator->() const
    { 
      return &(operator*()); 
    }

    /**
     ** \name  Forward operators addons
     */
    /*@{*/
    
    /*!
    ** \brief Preincrement a %const_iterator3d_plane. Iterate to the next location
    ** inside the %Box3d.
    **          
    */
    inline
    self& operator++()
    {
      if(x2_ < (box_.bottom_right())[1])
	{
	  this->x2_++;
	  this->pos_calc();
	}
      else if(x1_ < (box_.bottom_right())[0])
	{
	  this->x1_++;
	  this->x2_ = (box_.upper_left())[1];
	  this->pos_calc();
	}
      else
	{
	  this->x1_ = (box_.bottom_right())[0] + 1;
	  this->x2_ = (box_.bottom_right())[1] + 1;
	  this->pos_calc();
	}
      return *this;
    }

    /*!
    ** \brief Postincrement a %const_iterator3d_plane. Iterate to the next location
    **        inside the %Box3d.
    **          
    */
    inline
    self operator++(int)
    {
      self tmp = *this;
      ++(*this);
      return tmp;
    }

    /*@} End Forward operators addons */

    /**
     ** \name  Bidirectional operators addons
     */
    /*@{*/
    /*!
    ** \brief Predecrement a %const_iterator3d_plane. Iterate to the previous location
    **        inside the %Box3d.
    **          
    */
    inline
    self& operator--()
    {
      if(x2_ > (box_.upper_left())[1])
	{
	  this->x2_--;
	  this->pos_calc();
	}
      else if (x1_ > (box_.upper_left())[0])
	{
	  this->x1_--;
	  this->x2_ = (box_.bottom_right())[1];
	  this->pos_calc();
	}
      else
	{
	  this->x1_ = (box_.upper_left())[0] - 1;
	  this->x2_ = (box_.upper_left())[1] - 1;
	  this->pos_calc();
	}
      
      return *this;
    }
  
    /*!
    ** \brief Postdecrement a %const_iterator3d_plane. Iterate to the previous location
    **        inside the %Box3d.
    **          
    */
    inline
    self operator--(int)
    {
      self tmp = *this;
      --(*this);
      return tmp;
    }

    /*@} End Bidirectional operators addons */
  
    /**
     ** \name  Equality comparable operators
     */
    /*@{*/
    /*!
    ** \brief Equality operator.
    ** \param i1 first %const_iterator3d_plane.
    ** \param i2 second %const_iterator3d_plane.
    ** \return true if i1 and i2 point to the same element of
    **         the 3d container
    */
    inline
    friend bool operator==(const self& i1,
			   const self& i2)
    {
      return ( (i1.cont3d_ == i2.cont3d_) && (i1.pos_ == i2.pos_) 
	       && (i1.zerodim_ == i2.zerodim_) && (i1.xp_ == i2.xp_) 
	       && (i1.x1_ == i2.x1_) && (i1.x2_ == i2.x2_) );
    }

    /*!
    ** \brief Inequality operator.
    ** \param i1 first %const_iterator3d_plane.
    ** \param i2 second %const_iterator3d_plane.
    ** \return true if !(i1 == i2)
    */
    inline
    friend bool operator!=(const self& i1,
			   const self& i2)
    {
      return ( (i1.cont3d_ != i2.cont3d_) || (i1.pos_ != i2.pos_)
	       || (i1.zerodim_ != i2.zerodim_) || (i1.xp_ != i2.xp_) 
	       || (i1.x1_ != i2.x1_) || (i1.x2_ != i2.x2_));
    }
    /*@} End Equality comparable operators*/
  
    /**
     ** \name  Strict Weakly comparable operators
     */
    /*@{*/
    /*!
    ** \brief < operator.
    ** \param i1 first %iterator3d_plane.
    ** \param i2 second %iterator3d_plane.
    ** \return true i1.pos_ < i2_pos
    */
    inline
    friend bool operator<(const self& i1,
			  const self& i2)
    {
      return ( i1.pos_ < i2.pos_);
    }
    
    /*!
    ** \brief > operator.
    ** \param i1 first %iterator3d_plane.
    ** \param i2 second %iterator3d_plane.
    ** \return true i2 < i1
    */
    inline
    friend bool operator>(const self& i1,
			  const self& i2)
    {
      return (i2 < i1);
    }
    
    /*!
    ** \brief <= operator.
    ** \param i1 first %iterator3d_plane.
    ** \param i2 second %iterator3d_plane.
    ** \return true if !(i2 < i1)
    */
    inline
    friend bool operator<=(const self& i1,
			   const self& i2)
    {
      return  !(i2 < i1);
    }
    
    /*!
    ** \brief >= operator.
    ** \param i1 first %iterator3d_plane.
    ** \param i2 second %iterator3d_plane.
    ** \return true if !(i1 < i2)
    */
    inline
    friend bool operator>=(const self& i1,
			   const self& i2)
    {
      return  !(i1 < i2);
    }
    /*@} End  Strict Weakly comparable operators*/

    /**
     ** \name  const_iterator3d_plane operators addons
     */
    /*@{*/
    /*!
    ** \brief const_iterator3d_plane addition.
    ** \param d difference_type
    ** \return a const_iterator3d_plane reference 
    ** \pre All the iterators between the current iterator i and i + d 
    **      must be dereferenceable.
    */
    inline
    self& operator+=(const difference_type& d)
    {
      this->x1_ += d.dx1();
      this->x2_ += d.dx2();
      this->pos_calc();
      return *this;
    }
  
    /*!
    ** \brief const_iterator3d_plane substraction.
    ** \param d difference_type
    ** \return a const_iterator3d_plane reference 
    ** \pre All the iterators between the current iterator i and i - d 
    **      must be dereferenceable.
    */
    inline
    self& operator-=(const difference_type& d)
    {
      this->x1_ -= d.dx1();
      this->x2_ -= d.dx2();
      this->pos_calc();
      return *this; 
    }
   
    /*!
    ** \brief const_iterator3d_plane addition.
    ** \param d difference_type
    ** \return a const_iterator3d_plane reference 
    ** \pre All the iterators between the current iterator i and i + d 
    **      must be dereferenceable.
    */
    inline
    self operator+(const difference_type& d)
    {
      self tmp = *this;
      tmp += d;
      return tmp;
    }

   
    /*!
    ** \brief const_iterator3d_plane substraction.
    ** \param d difference_type
    ** \return a const_iterator3d_plane reference 
    ** \pre All the iterators between the current iterator i and i - d 
    **      must be dereferenceable.
    */
    inline
    self operator-(const difference_type& d)
    {
      self tmp = *this;
      tmp -= d;
      return tmp;
    }

 
    /*!
    ** \brief %const_iterator3d_plane difference operator.
    ** \param i1 first %const_iterator3d_plane.
    ** \param i2 second %const_iterator3d_plane.
    ** \return a difference_type d such that i1 = i2 + d.
    ** \pre Either i1 is reachable from i2, or i2 is reachable from i1.
    */ 
    inline
    friend difference_type operator-(const self& i1,
				     const self& i2)
    {
      return difference_type(int(i1.x1_ - i2.x1_),int(i1.x2_ - i2.x2_));  
    }
   
    /*!
    ** \brief %const_iterator3d_plane element assignment operator.
    **        Equivalent to *(k + d).
    ** \param d difference_type.
    ** \return a const reference to the 3d container elements
    ** \pre (k + d) exists and is dereferenceable.
    */ 
    inline
    reference operator[](difference_type d) const
    {
      if(zerodim_ == 0)
	return (*cont3d_)[this->xp_][this->x1_+d.dx1()][this->x2_+d.dx2()];
      if(zerodim_ == 1)
	return (*cont3d_)[this->x1_+d.dx1()][this->xp_][this->x2_+d.dx2()];
      return (*cont3d_)[this->x1_+d.dx1()][this->x2_+d.dx2()][this->xp_]; 
    }

    /*!
    ** \brief %const_iterator3d_plane element assignment operator.
    **        Equivalent to *(k + n).
    ** \param n offset.
    ** \return a const pointer of pointer to the 3d container elements
    ** \pre (k + n) exists and is dereferenceable.
    ** \todo add assert
    */ 
    inline
    const_pointer* operator[](int n) const
    {
      if(zerodim_ == 0)
	return (*cont3d_)[this->xp_+ n];
      return (*cont3d_)[this->x1_+ n];
    }

 
 
    /*@} End  const_iterator3d_plane operators addons */
  

    /**
     ** \name Subscripts accessors
     */
    /*@{*/
  
    /*!
    ** \brief Access to the first subscript of the current %const_iterator3d_plane.
    ** \return the  first subscript.
    */
    inline
    int x1() const
    {
      return this->x1_;
    }

    /*!
    ** \brief Access to the first subscript of the current %const_iterator3d_plane.
    ** \return the  second subscript.
    */
    inline
    int x2() const
    {
      return this->x2_;
    }

    /*!
    ** \brief Access to the plane subscript of the current %const_iterator3d_plane.
    ** \return the plane subscript.
    */
    inline
    int xp() const
    {
      return this->xp_;
    }
    /*!
    ** \brief Access to the plane axe number of the current %const_iterator3d_plane.
    ** \return the plane axe number.
    */
    inline
    int zerodim() const
    {
      return this->zerodim_;
    }
    
    /*@} End  Subscripts accessors */
    
    
  
  private: 
    Container3D* cont3d_; // pointer to the 3d container
    const_pointer pos_;         // linear position within the container
    int zerodim_;         // index of the plane axe (0->k,1->i,2->j)
    int xp_;              // position of the plane
    int x1_;              // first subscript position (within the plane)
    int x2_;              // second subscript position (within the plane)
    Box2d<int> box_;      // plane to iterate 
  };

 
}//slip::

#endif //SLIP_ITERATOR3D_PLANE_HPP
