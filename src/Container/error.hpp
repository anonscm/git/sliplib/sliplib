/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file error.hpp
 * 
 * \brief Provides SLIP error messages.
 * 
 */
#ifndef SLIP_ERROR_HPP
#define SLIP_ERROR_HPP

#include <string>

namespace slip
{

  const std::string FILE_OPEN_ERROR = "Fail to open file: ";
  const std::string CROSSCORRELATION_BAD_ALGO = "Bad crosscorrelation algorithm selection: standard algorithm by default";
  const std::string SINGULAR_MATRIX_ERROR = "This matrix is singular";
  const std::string POSITIVE_DEFINITE_MATRIX_ERROR = "This matrix is not positive definite";

  const std::string GRID_INIT_POINT_ERROR = "The init points of the grid are different";
  const std::string GRID_STEP_ERROR = "The grid steps of the grid are different";

 const std::string FILE_READ_ERROR = "Unable to read the file ";
  const std::string FILE_WRITE_ERROR = "Unable to write into the file ";
  const std::string BAD_DIMENSION = "Bad dimension parameter.";
  const std::string BAD_PARAMETER = "Bad parameter.";

  const std::string SIGNAL_DIMENSION_ERROR = "One dimension of the signal must be one ! ";

/*!
 ** \date 2013/11/09
 ** \date 2014/09/08
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \author Benoit Tremblais <benoit.tremblais_AT_inria.fr>
 
 ** \version Fluex 1.0
 ** \brief standard exception extension
 ** \notes name have been changed to eliminate conflicts with QT
 */
// class slip_exception : public std::exception {

// private:
// 	std::string package_name;
// 	std::string function_name;
// 	std::string message;
// 	int label;

// public:
//   slip_exception() throw ();
//   slip_exception(const std::string&, 
// 		 const std::string&, 
// 		 const std::string&) throw ();
// 	slip_exception(const slip_exception& orig) throw ();
// 	virtual ~slip_exception() throw ();

// 	virtual slip_exception & operator=(const slip_exception&) throw ();

// 	virtual const char* what() const throw ();

// 	std::string what_str() const throw ();

// 	const std::string & getFunction_name() const {
// 		return this->function_name;
// 	}

// 	void setFunction_name(std::string function_name) {
// 		this->function_name = function_name;
// 	}

// 	const std::string & getMessage() const {
// 		return this->message;
// 	}

// 	void setMessage(std::string message) {
// 		this->message = message;
// 	}

// 	const std::string & getPackage_name() const {
// 		return package_name;
// 	}

// 	void setPackage_name(std::string package_name) {
// 		this->package_name = package_name;
// 	}

// 	const int & getLabel() const {
// 		return this->label;
// 	}

// 	void setLabel(const int &label) {
// 		this->label = label;
// 	}

// };
class slip_exception : public std::exception {

private:
	std::string package_name;
	std::string function_name;
	std::string message;
	int label;

public:
  slip_exception() throw ()
  : std::exception(), 
    package_name(), 
    function_name(), 
    message(), 
    label(-1)
  {}

  slip_exception(const std::string& p_name, 
		 const std::string& f_name, 
		 const std::string& mess) throw ()
    :
    std::exception(), 
    package_name(p_name), 
    function_name(f_name), 
    message(mess), 
    label(0) 
  {}

  slip_exception(const slip_exception& orig) throw ()
    : package_name(orig.package_name), 
      function_name(orig.function_name), 
      message(orig.message), 
      label(orig.label) 
  {}
    
  virtual ~slip_exception() throw ()
  {}

  virtual slip_exception & operator=(const slip_exception& orig) throw ()
  {
    this->package_name = orig.package_name;
    this->function_name = orig.function_name;
    this->message = orig.message;
    this->label = orig.label;
    return *this;
  }

  virtual const char* what() const throw ()
  {
    std::string what_mess;
    what_mess += package_name;
    what_mess += "::";
    what_mess += function_name;
    what_mess += "::";
    what_mess += message;
    return what_mess.c_str();
  }

  std::string what_str() const throw ()
  {
    std::string what_mess;
    what_mess += package_name;
    what_mess += "::";
    what_mess += function_name;
    what_mess += "::";
    what_mess += message;
    return what_mess;
  }
  
  const std::string & getFunction_name() const 
  {
    return this->function_name;
  }

  void setFunction_name(std::string function_name) 
  {
    this->function_name = function_name;
  }

  const std::string & getMessage() const 
  {
    return this->message;
  }

  void setMessage(std::string message) 
  {
    this->message = message;
  }

  const std::string & getPackage_name() const 
  {
    return package_name;
  }

  void setPackage_name(std::string package_name) 
  {
    this->package_name = package_name;
  }

  const int & getLabel() const 
  {
    return this->label;
  }
  
  void setLabel(const int &label) 
  {
    this->label = label;
  }

};
}//::slip


// namespace slip {

// slip_exception::slip_exception() throw ()
//     						: std::exception(), package_name(), function_name(), message(), label(-1) {
// }

// slip_exception::slip_exception(const std::string& p_name, const std::string& f_name, const std::string& mess) throw ()
//     						: std::exception(), package_name(p_name), function_name(f_name), message(mess), label(0) {
// }

// slip_exception::slip_exception(const slip_exception& orig) throw ()
//     						: package_name(orig.package_name), function_name(orig.function_name), message(orig.message), label(orig.label) {
// }

// slip_exception::~slip_exception() throw () {
// }

// inline
// slip_exception& slip_exception::operator=(const slip_exception& orig) throw () {
// 	this->package_name = orig.package_name;
// 	this->function_name = orig.function_name;
// 	this->message = orig.message;
// 	this->label = orig.label;
// 	return *this;
// }

// inline
// const char* slip_exception::what() const throw () {
// 	std::string what_mess;
// 	what_mess += package_name;
// 	what_mess += "::";
// 	what_mess += function_name;
// 	what_mess += "::";
// 	what_mess += message;
// 	return what_mess.c_str();
// }
// inline
// std::string slip_exception::what_str() const throw () {
// 	std::string what_mess;
// 	what_mess += package_name;
// 	what_mess += "::";
// 	what_mess += function_name;
// 	what_mess += "::";
// 	what_mess += message;
// 	return what_mess;
// }
// }//::slip

#endif //SLIP_ERROR_HPP
