/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Box3d.hpp
 * 
 * \brief Provides a class to manipulate 3d box.
 * 
 */
#ifndef SLIP_BOX3D_HPP
#define SLIP_BOX3D_HPP

#include <iostream>
#include <cassert>
#include <string>
#include "Box.hpp"
#include "Point3d.hpp"
#include "DPoint3d.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{


/*! \class Box3d
**  \ingroup Containers MiscellaneousContainers
** \brief This is a %Box3d class, a specialized version of slip::Box<CoordType,DIM>
**        with DIM = 3
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \date 2014/03/15
** \version 0.0.2
** \since 1.0.0
** \par Description:
** The box is defined by its front-upper-left and back-bottom-right points p1 and p2.
** \code
**      --------                            _
**    /        /|                        x1 /
**   /        / |                          /
**  p1--------  |                         /
**  |         | |                        / 
**  |         | |                       O------------> x3
**  |         | p2                      |
**  |         |/                        |
**  ----------                          |
**                                      |
**                                     \|
**                                      x2
**
** \endcode
** \param CoordType Type of the coordinates of the Box3d
*/
template <typename CoordType>
class Box3d: public slip::Box<CoordType,3>
{
public :
  typedef Box3d<CoordType> self;
  typedef Box<CoordType,3> base;
  
 /**
 ** \name Constructors & Destructors
 */
 /*@{*/
 /*!
  ** \brief Constructs a Box3d.
  ** \remarks call the default constructors of Point<CoordType,3>
  */
  Box3d();
  /*!
  ** \brief Constructs a Box3d from a CoordType array.
  ** \param p1 first extremal point
  ** \param p2 second extremal point
  */
  Box3d(const Point<CoordType,3>& p1, 
	const Point<CoordType,3>& p2);
  
  /*!
  ** \brief Constructs a Box3d.
  ** \param x11 first coordinate of the minimal point p1
  ** \param x12 second coordinate of the minimal point p1
  ** \param x13 third coordinate of the minimal point p1
  ** \param x21 first coordinate of the maximal point p2
  ** \param x22 second coordinate of the maximal point p2
  ** \param x23 third coordinate of the maximal point p3
  
  */
  Box3d(const CoordType& x11,
	const CoordType& x12,
	const CoordType& x13,
	const CoordType& x21,
	const CoordType& x22,
	const CoordType& x23);

  /*!
  ** \brief Constructs a square Box3d using a central point and a width.
  ** \param xc first coordinate of the central point
  ** \param yc second coordinate of the central point
  ** \param zc third coordinate of the central point
  ** \param w width of the box (real width and height=2*w+1)
  */
  Box3d(const CoordType& xc,
	const CoordType& yc,
	const CoordType& zc,
	const CoordType& w);

  /*!
  ** \brief Constructs a square Box3d using a central point and a width.
  ** \param pc central point
  ** \param w width of the box (real width and height=2*w+1)
  */
  Box3d(const Point<CoordType,3>& pc,
	const CoordType& w);
  
  /*!
  ** \brief Constructs a square Box3d using a central point and two widths.
  ** \param pc central point
  ** \param w1 width of the first coordinate box (real width =2*w1+1)
  ** \param w2 width of the second coordinate box (real height =2*w2+1)
  ** \param w3 width of the third coordinate box (real height =2*w3+1)
  */
  Box3d(const Point<CoordType,3>& pc,
	const CoordType& w1,
	const CoordType& w2,
	const CoordType& w3);  

  /*@} End Constructors */

   /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Accessor/Mutator of the front_upper_left point (p1) of Box3d. 
  ** \return reference to the front_upper_left Point3d of the Box3d
  */
  void front_upper_left(Point<CoordType,3>);
  /*!
  ** \brief Accessor/Mutator of the front_upper_left point (p1) of Box3d. 
  ** \return reference to the front_upper_left Point3d of the Box3d
  */
  Point<CoordType,3>& front_upper_left();
  /*!
  ** \brief Accessor/Mutator of the front_upper_left point (p1) of Box3d. 
  ** \return reference to the front_upper_left Point3d of the Box3d
  */
  const Point<CoordType,3>& front_upper_left() const;


  /*!
  ** \brief Accessor/Mutator of the back_bottom_right point (p2) of Box3d. 
  ** \return reference to the back_bottom_right Point3d of the Box3d
  */
  void back_bottom_right(Point<CoordType,3>);
  /*!
  ** \brief Accessor/Mutator of the back_bottom_right point (p2) of Box3d. 
  ** \return reference to the back_bottom_right Point3d of the Box3d
  */
  Point<CoordType,3>& back_bottom_right();
  /*!
  ** \brief Accessor/Mutator of the back_bottom_right point (p2) of Box3d. 
  ** \return reference to the back_bottom_right Point3d of the Box3d
  */
  const Point<CoordType,3>& back_bottom_right() const;

    /*!
  ** \brief Mutator of the coordinates of the %Box3d. 
  ** \param x11 first coordinate of the minimal point p1
  ** \param x12 second coordinate of the minimal point p1
  ** \param x13 third coordinate of the minimal point p1
  ** \param x21 first coordinate of the maximal point p2
  ** \param x22 second coordinate of the maximal point p2
  ** \param x23 third coordinate of the maximal point p2
  ** \return 
  */
  void set_coord(const CoordType& x11,
		 const CoordType& x12,
		 const CoordType& x13,
		 const CoordType& x21,
		 const CoordType& x22,
		 const CoordType& x23);

  /*@} End Element access operators */

  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;


  /*!
  ** \brief compute the volume of the Box3d.
  ** \return the volume
  */
  CoordType volume() const;

   /*!
  ** \brief compute the width of the Box3d.
  ** \return the width
  */
  CoordType width() const;

  /*!
  ** \brief compute the height of the Box3d.
  ** \return the height
  */
  CoordType height() const;
  
  /*!
  ** \brief compute the depth of the Box3d.
  ** \return the depth
  */
  CoordType depth() const;
  
  
 private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
       if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::Box<CoordType,3> >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::Box<CoordType,3> >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
};
}//slip::

namespace slip
{

  template<typename CoordType>
  inline
  Box3d<CoordType>::Box3d():
    Box<CoordType,3>()
  {}

  template<typename CoordType>
  inline
  Box3d<CoordType>::Box3d(const Point<CoordType,3>& p1,
			  const Point<CoordType,3>& p2):
    Box<CoordType,3>(p1,p2)
  {}
  
  template<typename CoordType>
  inline
  Box3d<CoordType>::Box3d(const CoordType& x11,
			  const CoordType& x12,
			  const CoordType& x13,
			  const CoordType& x21,
			  const CoordType& x22,
			  const CoordType& x23):
    Box<CoordType,3>(Point3d<CoordType>(x11,x12,x13),Point3d<CoordType>(x21,x22,x23))
  {}

  template<typename CoordType>
  inline
  Box3d<CoordType>::Box3d(const CoordType& xc,
			  const CoordType& yc,
			  const CoordType& zc,
			  const CoordType& w):
    Box<CoordType,3>(Point3d<CoordType>(xc-w,yc-w,zc-w),Point3d<CoordType>(xc+w,yc+w,zc+w))
  {}

  template<typename CoordType>
  inline
  Box3d<CoordType>::Box3d(const Point<CoordType,3>& pc,
			  const CoordType& w):
    Box<CoordType,3>(Point3d<CoordType>(pc[0]-w,pc[1]-w,pc[2]-w),Point3d<CoordType>(pc[0]+w,pc[1]+w,pc[2]+w))
  {}


  template<typename CoordType>
  inline
  Box3d<CoordType>::Box3d(const Point<CoordType,3>& pc,
			  const CoordType& w1,
			  const CoordType& w2,
			  const CoordType& w3):
    Box<CoordType,3>(Point3d<CoordType>(pc[0]-w1,pc[1]-w2,pc[2]-w3),Point3d<CoordType>(pc[0]+w1,pc[1]+w2,pc[2]+w3))
  {}


  template<typename CoordType>
  inline
  void Box3d<CoordType>::front_upper_left(Point<CoordType,3> p1) {this->p1_=p1;}

  template<typename CoordType>
  inline
  Point<CoordType,3>& Box3d<CoordType>::front_upper_left() {return this->p1_;}
  
  template<typename CoordType>
  inline
  const Point<CoordType,3>& Box3d<CoordType>::front_upper_left() const {return this->p1_;}


  template<typename CoordType>
  inline
  void Box3d<CoordType>::back_bottom_right(Point<CoordType,3> p2) {this->p2_=p2;}

  template<typename CoordType>
  inline
  Point<CoordType,3>& Box3d<CoordType>::back_bottom_right() {return this->p2_;}

  template<typename CoordType>
  inline
  const Point<CoordType,3>& Box3d<CoordType>::back_bottom_right() const {return this->p2_;}

   template<typename CoordType>
  inline
  void Box3d<CoordType>::set_coord(const CoordType& x11,
				   const CoordType& x12,
				   const CoordType& x13,
				   const CoordType& x21,
				   const CoordType& x22,
				   const CoordType& x23)
  {
    this->p1_[0] = x11;
    this->p1_[1] = x12;
    this->p1_[2] = x13;
    this->p2_[0] = x21;
    this->p2_[1] = x22;
    this->p2_[2] = x23;
  }


  template<typename T>
  inline
  std::string 
  Box3d<T>::name() const {return "Box3d";} 
 

  template<typename CoordType>
  inline
  CoordType Box3d<CoordType>::volume() const {return width() * height() * depth();}

  template<typename CoordType>
  inline
  CoordType Box3d<CoordType>::width() const {return (this->p2_[2] - this->p1_[2]+1);}

  template<typename CoordType>
  inline
  CoordType Box3d<CoordType>::height() const {return (this->p2_[1] - this->p1_[1]+1);}

  template<typename CoordType>
  inline
  CoordType Box3d<CoordType>::depth() const {return (this->p2_[0] - this->p1_[0]+1);}

}//slip::

#endif //SLIP_BOX3D_HPP
