/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file MultivariatePolynomial.hpp
 * 
 * \brief Provides a class to manipulate multivariate polynomial.
 * 
 */
#ifndef SLIP_MULTIVARIATEPOLYNOMIAL_HPP
#define SLIP_MULTIVARIATEPOLYNOMIAL_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <algorithm>
#include <map>
#include <cmath>
#include <complex>
#include <string>
#include "Block.hpp"
#include "Array2d.hpp"
#include "Polynomial.hpp"
#include "linear_least_squares.hpp"
#include "statistics.hpp"
#include "polynomial_algo.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp>

namespace slip
{
template <std::size_t DIM>
struct Monomial;

template <std::size_t DIM>
std::ostream& operator<<(std::ostream & out, 
			 const Monomial<DIM>& m);

template <std::size_t DIM>
bool operator==(const Monomial<DIM>& x, 
		const Monomial<DIM>& y);

template <std::size_t DIM>
bool operator!=(const Monomial<DIM>& x, 
		const Monomial<DIM>& y);

template <std::size_t DIM>
bool operator<(const Monomial<DIM>& x, 
	       const Monomial<DIM>& y);

template <std::size_t DIM>
bool operator>(const Monomial<DIM>& x, 
	       const Monomial<DIM>& y);

template <std::size_t DIM>
Monomial<DIM> operator*(const Monomial<DIM>& x, 
			const Monomial<DIM>& y);


/*! \struct Monomial
** \ingroup Containers MathematicalContainer
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/04/01
** \since 1.0.0
** \brief Numerical %Monomial structure 
** \param DIM Dimension of the Monomial 
**
** \pre DIM must be != 0
**
** \remarks the powers of the variables are less than 9
** \remarks The monomials are ordered according to the lexicographical 
**          order of the powers of x1,x2,...,xDIM
** 
*/
template <std::size_t DIM>
struct Monomial
{
  slip::block<unsigned char,DIM> powers;

  std::string name() const {return "Monomial";}
  std::size_t dim() const {return DIM;}
  std::size_t degree() const 
  {
    return std::accumulate(powers.begin(),powers.end(),std::size_t(0));
  }

  bool is_a_constant() const
  {
    return (std::size_t(std::count(powers.begin(),powers.end(),(unsigned char)(0))) == DIM);
  }

 
  
  /**
   ** \name Comparison operators
   */
  /*@{*/
  /*!
  ** \brief Monomial equality comparison
  ** \param x A %Monomial
  ** \param y A %Monomial of the same type of \a x
  ** \return true iff the powers of the Monomials are equal
  **
  */
  friend bool operator== <>(const Monomial<DIM>& x, 
			    const Monomial<DIM>& y);

  /*!
  ** \brief Monomial inequality comparison
  ** \param x A %Monomial
  ** \param y A %Monomial of the same type of \a x
  ** \return true if !(x == y) 
  */
  friend bool operator!= <>(const Monomial<DIM>& x, 
			    const Monomial<DIM>& y);


  /*!
  ** \brief Less than comparison operator (Monomial ordering relation)
  ** \param x A %Monomial
  ** \param y A %Monomial of the same type of \a x
  ** \return true iff \a the powers of x is lexicographically less than 
  **  thoseof \a y
  */
  friend bool operator< <>(const Monomial<DIM>& x, 
			   const Monomial<DIM>& y);

  /*!
  ** \brief More than comparison operator
  ** \param x A %Monomial
  ** \param y A %Monomial of the same type of \a x
  ** \return true iff y > x 
  */
  friend bool operator> <>(const Monomial<DIM>& x, 
			   const Monomial<DIM>& y);

  /*@} Comparison operators */


private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & powers;
	}
    }
  template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & powers;
	}
    }
   BOOST_SERIALIZATION_SPLIT_MEMBER()
  

};

}//end slip::


namespace slip{

  /**
   ** \name Arithmetic operators
   */
  /*@{*/
  /*!
  ** \brief Monomial multiplication
  ** \param monomial1 A %Monomial
  ** \param monomial2 A %Monomial of the same type of \a monomial1
  ** \return a %Monomial
  **
  */
  template<std::size_t DIM>
  Monomial<DIM> operator*(const Monomial<DIM>& monomial1, 
			  const Monomial<DIM>& monomial2);
  /*@} Arithmetic operators */
  
}
/** \name Input/output operator */
  /* @{ */
namespace slip
{
  template <std::size_t DIM>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const Monomial<DIM>& m)
  {
    if(m.is_a_constant())
      {
      }
    else
      {
	for(std::size_t i = 0; i < (m.powers).size(); ++i)
	  out<<"x"<<(i+1)<<"^"<<(int)m.powers[i]<<" ";
      }
    return out;
  }
/* @} */
/** \name EqualityComparable functions */
  /* @{ */
  template <std::size_t DIM>
  inline
  bool operator==(const Monomial<DIM>& x, 
		  const Monomial<DIM>& y)
  {
    return (x.powers == y.powers);
  }
  
  template <std::size_t DIM>
  inline
  bool operator!=(const Monomial<DIM>& x, 
		  const Monomial<DIM>& y)
  {
    return !(x == y);
  }

/* @} */

/** \name LessThan functions */
  /* @{ */
  template <std::size_t DIM>
  inline
  bool operator<(const Monomial<DIM>& x, 
		 const Monomial<DIM>& y)
  {
    return std::lexicographical_compare((x.powers).rbegin(), (x.powers).rend(),
					(y.powers).rbegin(), (y.powers).rend());
    //return x.powers < y.powers;
  }
  
  
  template <std::size_t DIM>
  inline
  bool operator>(const Monomial<DIM>& x, 
		 const Monomial<DIM>& y)
  {
    return (y < x);
  }

/* @} */
 
  template<std::size_t DIM>
  inline
  Monomial<DIM> operator*(const Monomial<DIM>& monomial1,
			  const Monomial<DIM>& monomial2)
  {
    Monomial<DIM> tmp;
    std::transform((monomial1.powers).begin(),
		   (monomial1.powers).end(),
		   (monomial2.powers).begin(),
		   (tmp.powers).begin(),
		   std::plus<unsigned char>());
    return tmp;
  }

}//end slip::
namespace slip
{

template <typename T, std::size_t DIM>
class MultivariatePolynomial;

template <typename T, std::size_t DIM>
std::ostream& operator<<(std::ostream & out, 
			 const MultivariatePolynomial<T,DIM>& v);

/*! \class MultivariatePolynomial
** \ingroup Containers MathematicalContainer
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2008/05/05
** \since 1.0.0
** \brief Numerical %MultivariatePolynomial class 
** \param T Type of the coefficients of the MultivariatePolynomial 
** \param DIM Dimension of the Polynomial.
**
** \remarks The monomials are ordered according to the lexicographical 
**          order of the powers x1,x2,...,xDIM. For example:
**          1 + x1 + x1^2 + x2 + x1x2 + x2^2 + x3 + x3x1 + x3x2 + x3^2.
** \remarks The variables and the coefficients of the MultivariatePolynomial
**          may be complex.
**
*/
template <typename T, std::size_t DIM>
class MultivariatePolynomial:public std::map<slip::Monomial<DIM>,T>
{
public :

  typedef std::map<slip::Monomial<DIM>,T> base;
  typedef typename std::map<slip::Monomial<DIM>,T>::key_type key_type;
  typedef typename std::map<slip::Monomial<DIM>,T>::mapped_type mapped_type;
  typedef typename std::map<slip::Monomial<DIM>,T>::value_type value_type;
  typedef typename std::map<slip::Monomial<DIM>,T>::key_compare key_compare;
  typedef typename std::map<slip::Monomial<DIM>,T>::value_compare value_compare;
  typedef MultivariatePolynomial<T,DIM> self;
  typedef const MultivariatePolynomial<T,DIM> const_self;

  typedef typename std::map<slip::Monomial<DIM>,T>::pointer pointer;
  typedef  const typename std::map<slip::Monomial<DIM>,T>::const_pointer const_pointer;
  typedef typename std::map<slip::Monomial<DIM>,T>::reference reference;
  typedef typename std::map<slip::Monomial<DIM>,T>::const_reference const_reference;

  typedef typename std::map<slip::Monomial<DIM>,T>::difference_type difference_type;
  typedef typename std::map<slip::Monomial<DIM>,T>::size_type size_type;

  typedef typename std::map<slip::Monomial<DIM>,T>::iterator iterator;
  typedef typename std::map<slip::Monomial<DIM>,T>::const_iterator const_iterator;

  typedef typename std::map<slip::Monomial<DIM>,T>::reverse_iterator reverse_iterator;
  typedef typename std::map<slip::Monomial<DIM>,T>::const_reverse_iterator const_reverse_iterator;


  /**
   ** \name Constructors & Destructors
   */
  /*@{*/
  
  /*!
  ** \brief Constructs a MultivariatePolynomial.
  */
  MultivariatePolynomial():
    base()
  {
    slip::Monomial<DIM> constant;
    constant.powers.fill((unsigned char)0);
    this->insert(constant,T());
  }
  
  /*!
  ** \brief Constructs a MultivariatePolynomial of degree \a degree.
  ** That is to say the sum of the powers of the greteast monomial is
  ** less than \a degree.
  ** \param degree Degree of the %MultivariatePolynomial.
  ** 
  ** \code
  ** //create 1 + x1 + x1^2 + x2 + x1x2 + x2^2 + x3 + x3x1 + x3x2 + x3^2
  ** slip::MultivariatePolynomial<double,3> P(2);
  ** 
  ** \endcode
  */
  MultivariatePolynomial(const std::size_t degree):
    base()
  {

    this->create(degree);
  }

  /*!
  ** \brief Constructs a MultivariatePolynomial of degree \a degree.
  ** That is to say the sum of the powers of the greteast monomial is
  ** less than \a degree. The coefficient of the polynomial are set
  ** to those provided by the range [first,last).
  ** \param degree Degree of the %MultivariatePolynomial.
  ** \param first InputIterator to the first coefficient.
  ** \param last InputIterator to one past-the-end coefficient.
  ** \pre (last-first) == total_nb_coeff()
  ** \code
  ** //create 1 + 2x1 + 3x1^2 + 4x2 + 5x1x2 + 6x2^2 + 7x3 + 8x3x1 + 9x3x2 + 10x3^2
  ** slip::Array<double> A(10);
  ** slip::iota(A.begin(),A.end(),1.0,1.0);
  ** slip::MultivariatePolynomial<double,3> P(2,A.begin(),A.end());
  ** 
  ** \endcode
  */
  template<typename InputIterator>
  MultivariatePolynomial(const std::size_t degree,
			 InputIterator first,
			 InputIterator last):
    base()
  {
    this->create(degree);
    assert(int(last-first)== int(this->total_nb_coeff()));
    
    for(iterator it = this->begin(); it!=this->end();++it)
      {
	(*it).second = *first++;
      }
  }

  /*!
  ** \brief Constructs a MultivariatePolynomial with a copy of range
  **
  ** \param first An input iterator.
  ** \param last An output iterator.
  */
  template<class InputIterator>
  MultivariatePolynomial(InputIterator first, InputIterator last):
    base(first,last)
  {}

  

  /*!
  ** \brief Constructs a copy of the MultivariatePolynomial \a rhs
  */
  MultivariatePolynomial(const self& rhs):
    base(rhs)
  {}

 /*!
  ** \brief Destructor of the MultivariatePolynomial
  */
  ~MultivariatePolynomial()
  {}

  /*@} End Constructors */


  

  /**
   ** \name i/o operators
   */
  /*@{*/
  /*!
  ** \brief Write the %MultivariatePolynomial to the ouput stream
  ** \param out output std:ostream
  ** \param v %MultivariatePolynomial to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const self& v);
  /*@} End i/o operators */

 


  /**
   ** \name Basic operetors  
   */
  /*@{*/
  /*!
  ** \brief Inserts the monomial \a monomial associated with the coefficient 
  **  \a coefficient into the %MultivariatePolynomial
  **
  ** \param monomial  %Monomial to insert
  ** \param coefficient Coefficient associated to the %Monomial \a monomial
  */
  void insert(const slip::Monomial<DIM>& monomial, const T& coefficient);
  /*@} End Basic operetors   */

  /**
   ** \name  Arithmetic operators
   */
  /*@{*/

  /*!
  ** \brief Add val to each element of the MultivariatePolynomial  
  ** \param val value
  ** \return reference to the resulting MultivariatePolynomial
  */
  self& operator+=(const T& val);
  self& operator-=(const T& val);
  self& operator*=(const T& val);
  self& operator/=(const T& val);

  self  operator-() const;


  self& operator+=(const self& rhs);
  self& operator-=(const self& rhs);
  self& operator*=(const self& rhs);

    

  /*@} End Arithmetic operators */

  /**
   ** \name  Mathematic operators
   */
  /*@{*/
  
  /*!
  ** \brief Returns the derivative the %MultivariatePolynomial
  **
  ** \param dim Dimension to derivate.
  ** 
  ** \pre  0 < dim <= DIM
  ** \notes Was an in-place derivative before version 1.4.0 !
  */
  self derivative(const std::size_t dim) const;

 /*!
  ** \brief compute the in-place derivative of the %MultivariatePolynomial
  **
  ** \param dim Dimension to derivate.
  ** \since 1.4.0
  ** \pre  0 < dim <= DIM
  */
  void derivative(const std::size_t dim);
  

  /*!
  ** \brief Returns the derivative the %MultivariatePolynomial
  **
  ** \param dim Dimension to derivate.
  ** \param order Order of derivation.
  **
  ** \pre  0 < dim <= DIM
  ** \notes Was an in-place derivative before version 1.4.0 !
  */
  self derivative(const std::size_t dim,
		  const std::size_t order) const;

  /*!
  ** \brief Computes the in-place derivative of the %MultivariatePolynomial
  **
  ** \param dim Dimension to derivate.
  ** \param order Order of derivation.
  **
  ** \pre  0 < dim <= DIM
  */
  void derivative(const std::size_t dim,
		   const std::size_t order);


  /*!
  ** \brief Returns the integral of the %MultivariatePolynomial. 
  **        By default, the constant of integrationis K is set to zero
  **
  ** \param dim Dimension to integrate.
  ** \param K Constant of integration. 
  **
  ** \pre  0 < dim <= DIM
  ** \notes Was an in-place derivative before version 1.4.0 !
  */
  self integral(const std::size_t dim,
		 const T& K = T()) const;

   /*!
  ** \brief Computes the in-place integral of the %MultivariatePolynomial. 
  **        By default, the constant of integrationis K is set to zero
  **
  ** \param dim Dimension to integrate.
  ** \param K Constant of integration. 
  **
  ** \pre  0 < dim <= DIM
  */
  void integral(const std::size_t dim,
		const T& K = T());

  /*!
  ** \brief Returns the integral of the %MultivariatePolynomial. 
  **        By default, the constant of integrationis K is set to zero
  **
  ** \param dim Dimension to integrate.
  ** \param order Order of integration.
  ** \param K Constant of integration. 
  **
  ** \pre  0 < dim <= DIM
  ** \notes Was an in-place derivative before version 1.4.0 !
  */
  self integral(const std::size_t dim,
		 const std::size_t order,
		 const T& K = T()) const;

  
/*!
  ** \brief Computes the in-place integral of the %MultivariatePolynomial. 
  **        By default, the constant of integrationis K is set to zero
  **
  ** \param dim Dimension to integrate.
  ** \param order Order of integration.
  ** \param K Constant of integration. 
  **
  ** \pre  0 < dim <= DIM
  */
  void integral(const std::size_t dim,
		 const std::size_t order,
		 const T& K = T());

  /*!
  ** \brief Return a polynomial P(X) of degree order that
  **        minimizes sum_i (P(x1(i),...,xDIM(i)) - y(i))^2  to best fit 
  **        the data in the least squares sense.
  ** \param X Vector of the data (each data must be of dimension DIM).
  ** \param Y Vector of the Y data.
  ** \param order Order or degree of the polynomial that fit the data.
  ** \code
  ** std::size_t n = 32;
  ** slip::Vector<double> x3(n+1);
  ** slip::iota(x3.begin(),x3.end(),-1.0,1.0/double(n));
  ** slip::Vector<double> y3(x3);
  ** slip::Vector<double> z3(x3);
  ** slip::Vector<slip::Vector<double> > datax3(x3.size()*x3.size()*x3.size());  
  ** std::size_t k = 0;
  **  forr(std::size_t i = 0; i < y3.size(); ++i)
  **   {
  **     for(std::size_t j = 0; j < x3.size(); ++j)
  **	 {
  **	   for(std::size_t l = 0; l < z3.size(); ++l)
  **	     {
  **	       datax3[k].resize(3);
  **	       datax3[k][0] = x3[i];
  **	       datax3[k][1] = y3[j];
  **	       datax3[k][2] = z3[l];
  **	       k++;
  **	     }
  ** 	 }
  **   }
  ** slip::Vector<double> datay3(datax3.size(),0.0);
  **
  ** for(std::size_t k = 0; k < datax3.size(); ++k)
  **   {
  **     double x = datax3[k][0];
  **     double y = datax3[k][1];
  **     double z = datax3[k][2];
  **     datay3[k] =  1.2 + 2.0 * x + 0.5* x*x - 1.0 * y + 0.8 * x *
  **      y + 0.3 * y * y + 1.1*z*y + 1.32 * z * z;
  **   }
  **
  ** std::size_t coeff_size3 = 10;
  ** slip::Vector<double> coeff3(coeff_size3,1.0);
  **
  ** slip::MultivariatePolynomial<double,3> P3d;
  ** double err = P3d.fit(datax3,datay3,2);
  ** std::cout<<P3d<<std::endl;
  ** std::cout<<"erreur = "<<err<<std::endl;
  ** \endcode
  */
  template <typename Vector1, typename Vector2>
  double fit(Vector1 X,
	     Vector2 Y,
	     const std::size_t order)
  {
  	
    //input data
    //computes the number of coefficients estimate
    slip::Array<std::size_t> V(DIM+order);
    slip::iota(V.begin(),V.end(),1,1);
    std::size_t coeff_size  = (std::accumulate(V.begin()+order,V.end(),1,std::multiplies<int>()))/(std::accumulate(V.begin(),V.begin()+DIM,1,std::multiplies<int>()));

    slip::Array<T> Coeff(coeff_size);
    //output data
    double chi_square = 0.0;
    slip::EvalPowerNdBasis<typename Vector1::value_type,typename slip::Array<T>::iterator> power_basis;
    slip::svd_least_square_nd(X,
			      Y,
			      Coeff,
			      order,
			      power_basis);
    this->create(order);
    typename slip::MultivariatePolynomial<T,DIM>::iterator it = this->begin();
    typename slip::Array<T>::const_iterator ita = Coeff.begin();
    for(;it != this->end();++it,++ita)
      {
	(*it).second = *ita;
      }
    return chi_square;
  }
  

  /*!
  ** \brief Returns the evaluation of the %MultivariatePolynomial at 
  **        (x1,x2,...,xDIM)
  ** \param first An InputIterator on the coordinates 
  ** \param last An InputIterator
  ** 
  ** \pre (last-first) == DIM
  **
  ** \todo accelerate computation for simple values (0.0, 1.0, -1.0, i, -i...)
  ** \todo test with complex variables
  ** \todo specialize for float, double, long double and complex<float>...
  */
  template<typename InputIterator>
  typename std::iterator_traits<InputIterator>::value_type
  evaluate(InputIterator first,
	   InputIterator last) const
  {
    assert((last-first) == DIM);
    typedef  typename slip::MultivariatePolynomial<T,DIM>::const_iterator const_it;
 
    typedef  typename std::iterator_traits<InputIterator>::value_type Return;
    slip::Array2d<Return> values(DIM,this->degree()+1);
   
    //pre-computation of the variables' powers
    for(std::size_t i = 0; i < values.rows(); ++i)
      {
	slip::eval_power_basis(*first++,this->degree(),values.row_begin(i),values.row_end(i));
      }
    
 
      Return result = Return();
    for(const_it first_m = this->begin();first_m!=this->end();++first_m)
      {
	Return tmp = values[0][((*first_m).first).powers[0]];
	for(std::size_t i = 1; i < DIM; ++i)
	  {
	    tmp*=values[i][((*first_m).first).powers[i]];
	  }
	result+= ((*first_m).second) * tmp;
      }

     return result;
  }


   /*!
  ** \brief Returns the evaluation of the %MultivariatePolynomial at 
  **        (x1,x2,...,val,...,xDIM)
  ** \param number Variable number
  ** \param val Value of this variable
  ** \return The evaluated %MultivariatePolynomial 
  ** \pre number <= DIM
  **
  ** \attention Normally the dimension DIM should be decreased, but here its value
  **            is hold. The power of the variable corresponding to the number is
  **            set to zero and the coefficients of monomials are uptated.
  **            
  **
  ** \todo accelerate computation for simple values (0.0, 1.0, -1.0, i, -i...)
  ** \todo test with complex variables
  ** \todo specialize for float, double, long double and complex<float>...
  */
  MultivariatePolynomial<T,DIM> partial_evaluate(const std::size_t number,
						 const T& val);
 

  /*!
  ** \brief Returns the composition of the %MultivariatePolynomial with a vector of  %MultivariatePolynomial.
  **
  ** \param VP a std::vector of slip::MultivariatePolynomial<T,DIM>
  ** \return P(Q1,Q2,...QDIM) where P is the slip::MultivariatePolynomial<T,DIM> of the  slip::MultivariatePolynomial<T,DIM> Q1,Q2...,QDIM
  **
  ** \pre  VP.size() == DIM
  */
  self compose(const std::vector<self>& VP);

  /*!
  ** \brief Truncates the Monomial of degree greater than \a degree.
  ** \since 1.4.0
  ** \param degree
  ** \return slip::MultivariatePolynomial
  */
  self truncate(const std::size_t degree) const;

  
  /*@} End Mathematic operators */

  /**
   ** \name Accessors
   */
  /*@{*/

  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;


  /*!
  ** \brief Returns the degree of the %MultivariatePolynomial
  */
  size_type degree() const;

  /*!
  ** \brief Returns the degree of the %MultivariatePolynomial
  **
  ** \remarks Same value as degree
  */
  size_type order() const;

  /*!
  ** \brief Returns the total number of coefficients
  */
  size_type total_nb_coeff() const;

  /*!
  ** \brief Returns all the coefficients of the polynomial 
  **        including zero ones
  **
  ** /pre (last - first) == total_nb_coeff()
  ** /pre DIM > 0
  */
  template<typename RandomAccessIterator>
  void all_coeff(RandomAccessIterator first,
		 RandomAccessIterator last) const
  {
    typedef typename std::iterator_traits<RandomAccessIterator>::difference_type _Distance;
    assert((last - first) == _Distance(this->total_nb_coeff()));
    assert(DIM > 0);

    //array of all the powers combinations sort by lexicographical order
  
    slip::Array2d<std::size_t> powers_a(slip::power(this->order()+1,DIM),DIM);
    for(std::size_t d = 0; d < powers_a.dim2(); ++d)
      {
	std::size_t step = slip::power((this->order()+1),d);
	for(std::size_t ord = 0, ord2 = 0; ord <= (powers_a.dim1() - step); ord+=step,++ord2)
	  {
	    std::size_t tmp = ord2%(this->order()+1);
	    for(std::size_t k = 0; k < step; ++k)
	      {
		powers_a[ord+k][d] = tmp;
	      }
	  }
      }

    std::map<slip::Monomial<DIM>,T> tmp_map;
    slip::Array2d<std::size_t> powers_cut(this->total_nb_coeff(),DIM);

    for(std::size_t ord = 0; ord < powers_a.dim1(); ++ord)
      {
	 Monomial<DIM> m;
	 std::copy(powers_a.row_begin(ord),powers_a.row_end(ord),(m.powers).begin());
	 if(m.degree() <= this->order())
	   {
	     tmp_map[m] = T();
	   }
      }
   
    
    typedef typename slip::MultivariatePolynomial<T,DIM>::const_iterator const_it;
    
    for(const_it it = this->begin();it!=this->end();++it)
      {
	Monomial<DIM> m = (*it).first;
	T coefficient = (*it).second;
	tmp_map[m] += coefficient;
      }

    for(const_it it = tmp_map.begin(); it!=tmp_map.end();++it)
      {
	*first++ = (*it).second;
      }
    
  }

  
  /*@} End Accessors */

private:
  /*! function to create all monomials for a %MultivariatePolynomial of
  **  degree \a degree.
  ** \param degree. degree of the %MultivariatePolynomial.
  */
  void create(const std::size_t degree);
 friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<std::map<slip::Monomial<DIM>,T> >(*this);
	}
    }
  template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & boost::serialization::base_object<std::map<slip::Monomial<DIM>,T> >(*this);
	}
    }
   BOOST_SERIALIZATION_SPLIT_MEMBER()
};

  /**
   ** \name External arithmetic methods
   */
  /*@{*/

  /*!
  ** \relates MultivariatePolynomial
  ** \brief pointwise addition of two %MultivariatePolynomial
  ** \param P1 The first %MultivariatePolynomial 
  ** \param P2 The second %MultivariatePolynomial
  ** \return resulting %MultivariatePolynomial
  */
template<typename T, std::size_t DIM>
MultivariatePolynomial<T,DIM> operator+(const MultivariatePolynomial<T,DIM>& P1, 
				    const MultivariatePolynomial<T,DIM>& P2);

 /*!
 ** \relates MultivariatePolynomial
 ** \brief addition of a scalar to each element of a %MultivariatePolynomial
 ** \param P The %MultivariatePolynomial 
 ** \param val The scalar
 ** \return resulting %MultivariatePolynomial
 */
template<typename T, std::size_t DIM>
MultivariatePolynomial<T,DIM> operator+(const MultivariatePolynomial<T,DIM>& P, 
					const T& val);

 /*!
 ** \relates MultivariatePolynomial 
 ** \brief addition of a scalar to each element of a %MultivariatePolynomial
 ** \param val The scalar
 ** \param P The %MultivariatePolynomial  
 ** \return resulting %MultivariatePolynomial
 */
template<typename T, std::size_t DIM>
MultivariatePolynomial<T,DIM> operator+(const T& val, 
					const MultivariatePolynomial<T,DIM>& P);


/*!
  ** \relates MultivariatePolynomial 
  ** \brief pointwise substraction of two %MultivariatePolynomial
  ** \param P1 The first %MultivariatePolynomial 
  ** \param P2 The second %MultivariatePolynomial
  ** \return resulting %MultivariatePolynomial
  */
template<typename T, std::size_t DIM>
MultivariatePolynomial<T,DIM> operator-(const MultivariatePolynomial<T,DIM>& P1, 
					const MultivariatePolynomial<T,DIM>& P2);

 /*!
 ** \relates MultivariatePolynomial 
  ** \brief substraction of a scalar to each element of a %MultivariatePolynomial
  ** \param P The %MultivariatePolynomial 
  ** \param val The scalar
  ** \return resulting %MultivariatePolynomial
  */
  template<typename T, std::size_t DIM>
  MultivariatePolynomial<T,DIM> operator-(const MultivariatePolynomial<T,DIM>& P, 
					  const T& val);

 /*!
 ** \relates MultivariatePolynomial
  ** \brief substraction of a scalar to each element of a %MultivariatePolynomial
  ** \param val The scalar
  ** \param P The %MultivariatePolynomial  
  ** \return resulting %MultivariatePolynomial
  */
template<typename T, std::size_t DIM>
MultivariatePolynomial<T,DIM> operator-(const T& val, 
					const MultivariatePolynomial<T,DIM>& P);

/*!
** \relates MultivariatePolynomial
** \brief pointwise multiplication of two %MultivariatePolynomial
** \param P1 The first %MultivariatePolynomial 
** \param P2 The second %MultivariatePolynomial
** \return resulting %MultivariatePolynomial
  */
template<typename T, std::size_t DIM>
MultivariatePolynomial<T,DIM> operator*(const MultivariatePolynomial<T,DIM>& P1, 
					const MultivariatePolynomial<T,DIM>& P2);

 /*!
 ** \relates MultivariatePolynomial
 ** \brief multiplication of a scalar to each element of a %MultivariatePolynomial
 ** \param P The %MultivariatePolynomial 
 ** \param val The scalar
 ** \return resulting %MultivariatePolynomial
 */
template<typename T, std::size_t DIM>
MultivariatePolynomial<T,DIM> operator*(const MultivariatePolynomial<T,DIM>& P, 
				    const T& val);

 /*!
 ** \relates MultivariatePolynomial
 ** \brief multiplication of a scalar to each element of a %MultivariatePolynomial
 ** \param val The scalar
 ** \param P The %MultivariatePolynomial  
 ** \return resulting %MultivariatePolynomial
 */
template<typename T, std::size_t DIM>
MultivariatePolynomial<T,DIM> operator*(const T& val, 
				    const MultivariatePolynomial<T,DIM>& P);


 /*!
 ** \relates MultivariatePolynomial
 ** \brief division of a scalar to each element of a %MultivariatePolynomial
 ** \param P The %MultivariatePolynomial 
 ** \param val The scalar
 ** \return resulting %MultivariatePolynomial
 */
template<typename T, std::size_t DIM>
MultivariatePolynomial<T,DIM> operator/(const MultivariatePolynomial<T,DIM>& P, 
				    const T& val);
 /*@} End External arithmetic methods */

}//slip::



namespace slip
{

  template<typename T, std::size_t DIM>
  inline
  void 
  MultivariatePolynomial<T,DIM>::insert(const slip::Monomial<DIM>& monomial, 
					const T& coefficient)
  {
    if(monomial.is_a_constant()) 
      {
	(*this)[monomial] += coefficient;
      }    
    else
      {
	if(coefficient != T())
	  {
	    (*this)[monomial] += coefficient;
	  }
      }
    
  }

 
  template <typename T, std::size_t DIM>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const MultivariatePolynomial<T,DIM>& m)
  {
    typename slip::MultivariatePolynomial<T,DIM>::const_iterator it = m.begin();
    out.setf(std::ios_base::scientific);
    out.setf(std::ios_base::internal);
    
    
    for(;it!=m.end();++it)
      {
	Monomial<DIM> m = (*it).first;
	T coefficient = (*it).second;
	if(m.is_a_constant())
	  {
	    out<<coefficient<<" ";
	  }
	else
	  {
	    if(coefficient >= T(0))
	      {
		out<<"+ "<<coefficient;
	      }
	    else
	      {
		out<<"- "<<std::abs(coefficient);
	      }
	    out<<" "<<(*it).first<<" ";
	  }
      }
    out<<std::endl;
    return out;
  }


template <std::size_t DIM>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const MultivariatePolynomial<std::complex<double>,DIM>& m)
  {
    typename slip::MultivariatePolynomial<std::complex<double>,DIM>::const_iterator it = m.begin();
    out.setf(std::ios_base::scientific);
    out.setf(std::ios_base::internal);
    
    
    for(;it!=m.end();++it)
      {
	Monomial<DIM> m = (*it).first;
	std::complex<double> coefficient = (*it).second;
	if(m.is_a_constant())
	  {
	    out<<coefficient<<" ";
	  }
	else
	  {
	   
	    out<<"+ "<<coefficient;
	    out<<" "<<(*it).first<<" ";
	  }
      }
    out<<std::endl;
    return out;
  }


  template <std::size_t DIM>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const MultivariatePolynomial<std::complex<float>,DIM>& m)
  {
    typename slip::MultivariatePolynomial<std::complex<float>,DIM>::const_iterator it = m.begin();
    out.setf(std::ios_base::scientific);
    out.setf(std::ios_base::internal);
    
    
    for(;it!=m.end();++it)
      {
	Monomial<DIM> m = (*it).first;
	std::complex<float> coefficient = (*it).second;
	if(m.is_a_constant())
	  {
	    out<<coefficient<<" ";
	  }
	else
	  {
	   
	    out<<"+ "<<coefficient;
	    out<<" "<<(*it).first<<" ";
	  }
      }
    out<<std::endl;
    return out;
  }

 template <std::size_t DIM>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const MultivariatePolynomial<std::complex<long double>,DIM>& m)
  {
    typename slip::MultivariatePolynomial<std::complex<long double>,DIM>::const_iterator it = m.begin();
    out.setf(std::ios_base::scientific);
    out.setf(std::ios_base::internal);
    
    
    for(;it!=m.end();++it)
      {
	Monomial<DIM> m = (*it).first;
	std::complex<long double> coefficient = (*it).second;
	if(m.is_a_constant())
	  {
	    out<<coefficient<<" ";
	  }
	else
	  {
	   
	    out<<"+ "<<coefficient;
	    out<<" "<<(*it).first<<" ";
	  }
      }
    out<<std::endl;
    return out;
  }


 // template<typename T, std::size_t DIM>
 //  inline
 //  MultivariatePolynomial<T,DIM>& MultivariatePolynomial<T,DIM>::derivative(const std::size_t dim) 
 //  {
 //    assert(dim > 0);
 //    assert(dim <= DIM);

 //    if(this->degree()!=0)
 //      {
 // 	slip::MultivariatePolynomial<T,DIM>* tmp = new slip::MultivariatePolynomial<T,DIM>(*this);
 // 	this->clear();
 // 	typename slip::MultivariatePolynomial<T,DIM>::iterator it = tmp->begin();
 // 	for(;it!=tmp->end();++it)
 // 	  {
 // 	    slip::Monomial<DIM> tmp_m = (*it).first;
 // 	    T tmp_coeff = (*it).second * tmp_m.powers[dim-1];
 // 	    int p =  (int)tmp_m.powers[dim-1] - 1;
 // 	    if(p >= 0)
 // 	      {
 // 		tmp_m.powers[dim-1]-=1;
 // 		this->insert(tmp_m,tmp_coeff);
 // 	      }
 // 	  }
 // 	 tmp->clear();
 // 	 delete tmp;
 //      }
 //     else
 //      {
 // 	slip::Monomial<DIM> constant;
 // 	constant.powers.fill((unsigned char)0);
 // 	this->insert(constant,T());
 //      }
 //      return *this;
 //  }

 // template<typename T, std::size_t DIM>
 //  inline
 //  MultivariatePolynomial<T,DIM>& MultivariatePolynomial<T,DIM>::derivative(const std::size_t dim,
 // const std::size_t order) 
 //  {
 //    for(std::size_t i = 0; i < order; ++i)
 //      {
 // 	this->derivative(dim);
 //      }
 //    return *this;
 //  }


 template<typename T, std::size_t DIM>
 inline
 void MultivariatePolynomial<T,DIM>::derivative(const std::size_t dim) 
  {
    assert(dim > 0);
    assert(dim <= DIM);

    if(this->degree()!=0)
      {
	slip::MultivariatePolynomial<T,DIM>* tmp = new slip::MultivariatePolynomial<T,DIM>(*this);
	this->clear();
	typename slip::MultivariatePolynomial<T,DIM>::iterator it = tmp->begin();
	for(;it!=tmp->end();++it)
	  {
	    slip::Monomial<DIM> tmp_m = (*it).first;
	    T tmp_coeff = (*it).second * tmp_m.powers[dim-1];
	    int p =  (int)tmp_m.powers[dim-1] - 1;
	    if(p >= 0)
	      {
		tmp_m.powers[dim-1]-=1;
		this->insert(tmp_m,tmp_coeff);
	      }
	  }
	 tmp->clear();
	 delete tmp;
      }
     else
      {
	slip::Monomial<DIM> constant;
	constant.powers.fill((unsigned char)0);
	this->insert(constant,T());
      }
    // return *this;
  }

 template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM> MultivariatePolynomial<T,DIM>::derivative(const std::size_t dim) const 
  {
    assert(dim > 0);
    assert(dim <= DIM);

    slip::MultivariatePolynomial<T,DIM> tmp(*this);
    tmp.derivative(dim);
    return tmp;
  }


 template<typename T, std::size_t DIM>
 inline
 void MultivariatePolynomial<T,DIM>::derivative(const std::size_t dim,
						const std::size_t order) 
  {
    for(std::size_t i = 0; i < order; ++i)
      {
	this->derivative(dim);
      }
  }

 template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM> MultivariatePolynomial<T,DIM>::derivative(const std::size_t dim,
 const std::size_t order) const
  {
     slip::MultivariatePolynomial<T,DIM> tmp(*this);
     tmp.derivative(dim,order);
     return tmp;
  }



  // template<typename T, std::size_t DIM>
 //  inline
 //  MultivariatePolynomial<T,DIM>& MultivariatePolynomial<T,DIM>::integral(const std::size_t dim,
 // 									 const T& K) 
 //  {
 //    assert(dim > 0);
 //    assert(dim <= DIM);
 //   slip::MultivariatePolynomial<T,DIM>* tmp = new slip::MultivariatePolynomial<T,DIM>(*this);
 //   this->clear();
 //    typename slip::MultivariatePolynomial<T,DIM>::iterator it = tmp->begin();
 //    for(;it!=tmp->end();++it)
 //      {
 // 	slip::Monomial<DIM> tmp_m;
 // 	tmp_m.powers = (it->first).powers;
 // 	T tmp_coeff = it->second / T(tmp_m.powers[dim-1] + 1);
 // 	tmp_m.powers[dim-1]+=1;
 // 	if(tmp_coeff != 0)
 // 	  {
 // 	    (*this)[tmp_m] = tmp_coeff;
 // 	  }
 //       }
  
 //    slip::Monomial<DIM> constant;
 //    constant.powers.fill((unsigned char)0);
 //    this->insert(constant,K);
 //    tmp->clear();
 //    delete tmp;
 //    return *this;
 //  }

 // template<typename T, std::size_t DIM>
 //  inline
 //  MultivariatePolynomial<T,DIM>& 
 // MultivariatePolynomial<T,DIM>::integral(const std::size_t dim,
 // 					 const std::size_t order,
 // 					 const T& K) 
 //  {
 //     for(std::size_t i = 0; i < order; ++i)
 //      {
 // 	this->integral(dim,K);
 //      }
 //    return *this;
 //  }


 template<typename T, std::size_t DIM>
  inline
  void MultivariatePolynomial<T,DIM>::integral(const std::size_t dim,
					       const T& K)
  {
    assert(dim > 0);
    assert(dim <= DIM);
   slip::MultivariatePolynomial<T,DIM>* tmp = new slip::MultivariatePolynomial<T,DIM>(*this);
   this->clear();
    typename slip::MultivariatePolynomial<T,DIM>::iterator it = tmp->begin();
    for(;it!=tmp->end();++it)
      {
 	slip::Monomial<DIM> tmp_m;
 	tmp_m.powers = (it->first).powers;
 	T tmp_coeff = it->second / T(tmp_m.powers[dim-1] + 1);
 	tmp_m.powers[dim-1]+=1;
 	if(tmp_coeff != 0)
 	  {
 	    (*this)[tmp_m] = tmp_coeff;
 	  }
       }
  
    slip::Monomial<DIM> constant;
    constant.powers.fill((unsigned char)0);
    this->insert(constant,K);
    tmp->clear();
    delete tmp;
  }

  template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM> MultivariatePolynomial<T,DIM>::integral(const std::size_t dim,
 									 const T& K) const
  {
    MultivariatePolynomial<T,DIM> tmp(*this);
    tmp.integral(dim,K);
    return tmp;
  }

 template<typename T, std::size_t DIM>
  inline
  void
 MultivariatePolynomial<T,DIM>::integral(const std::size_t dim,
 					 const std::size_t order,
 					 const T& K) 
  {
     for(std::size_t i = 0; i < order; ++i)
      {
 	this->integral(dim,K);
      }
  }

  template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM> 
  MultivariatePolynomial<T,DIM>::integral(const std::size_t dim,
					  const std::size_t order,
					  const T& K) const
  {
   MultivariatePolynomial<T,DIM> tmp(*this);
   tmp.integral(dim,order,K);
   return tmp;
  }



  template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM> 
  MultivariatePolynomial<T,DIM>::partial_evaluate(const std::size_t number,
						  const T& val)
  {
    assert(number <= DIM);
    MultivariatePolynomial<T,DIM> pol;   
    typename slip::MultivariatePolynomial<T,DIM>::const_iterator it = this->begin();
   
    for(;it!=this->end();++it)
      {
	Monomial<DIM> m((*it).first); 
	T coefficient = (*it).second * std::pow(val,((*it).first).powers[number-1]);
	m.powers[number-1] = 0;
	pol.insert(m,coefficient);
      }
    return pol;
    
  }
  
  template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM> 
  MultivariatePolynomial<T,DIM>::compose(const std::vector<self>& VP)
  {
    assert(VP.size() == DIM);
  //compute Polynomial powers
  const std::size_t P_degree = this->degree();
  std::vector<std::size_t> V(DIM+P_degree);
  slip::iota(V.begin(),V.end(),1,1);
   std::size_t result_size = (std::accumulate(V.begin()+P_degree,V.end(),1,std::multiplies<int>()))/(std::accumulate(V.begin(),V.begin()+DIM,1,std::multiplies<int>()));
  std::vector<slip::MultivariatePolynomial<T,DIM> > PolyPowers(result_size);
  slip::eval_multi_poly_power_nd_basis(VP,P_degree,PolyPowers.begin(),PolyPowers.end());
    
  std::vector<T> all_coeff(result_size);
  this->all_coeff(all_coeff.begin(),all_coeff.end());

  slip::MultivariatePolynomial<T,DIM> Result =
    std::inner_product(PolyPowers.begin(),PolyPowers.end(),
		       all_coeff.begin(),slip::MultivariatePolynomial<T,DIM>());
  
  
 
  return Result;
  }


  template<typename T, std::size_t DIM>
  inline
  typename MultivariatePolynomial<T,DIM>::self 
  MultivariatePolynomial<T,DIM>::truncate(const std::size_t degree) const
  {
    assert(degree <= this->degree());
    slip::MultivariatePolynomial<T,DIM> Ptronc;
    typename slip::MultivariatePolynomial<T,DIM>::const_iterator it = this->begin();
    typename slip::MultivariatePolynomial<T,DIM>::const_iterator ite = this->end();

	for(;it!=ite;++it)
	  {
	    if((*it).first.degree() <= degree)
	      {
		
		Ptronc.insert((*it).first,(*it).second);
	      }
	  }	
    return Ptronc;
  }

  template<typename T, std::size_t DIM>
  inline
  std::string 
  MultivariatePolynomial<T,DIM>::name() const
  {
    return "MultivariatePolynomial";
  }

  template<typename T, std::size_t DIM>
  inline
  typename MultivariatePolynomial<T,DIM>::size_type MultivariatePolynomial<T,DIM>::degree() const {
    if(this->size() != 0)
      {
	 typename slip::MultivariatePolynomial<T,DIM>::const_iterator it = this->end();
	 it--;
	 
	 return (it->first).degree();
      }
    else
      return 0;
  }

  template<typename T, std::size_t DIM>
  inline
  typename MultivariatePolynomial<T,DIM>::size_type MultivariatePolynomial<T,DIM>::order() const 
  {
    return this->degree();
  }

  template<typename T, std::size_t DIM>
  inline
  typename MultivariatePolynomial<T,DIM>::size_type MultivariatePolynomial<T,DIM>::total_nb_coeff() const 
  {
    std::size_t nb_coeff = 1;
    std::size_t fact = 1;
    std::size_t degree = this->degree();
    for(std::size_t i = 1; i <= DIM;++i)
      {
	nb_coeff *= degree + i;
	fact *= i;
      }
    nb_coeff/=fact;
    return nb_coeff;
  }

  

  template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM>& 
  MultivariatePolynomial<T,DIM>::operator+=(const T& val)
  {
    typename slip::MultivariatePolynomial<T,DIM>::iterator it = this->begin();

    if(((*it).first).is_a_constant())
      {
	(*it).second += val;
      }
    else
      {
	slip::Monomial<DIM> constant;
	constant.powers.fill((unsigned char)0);
	this->insert(constant,val);
      }
    return *this;
  }

  template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM>& 
  MultivariatePolynomial<T,DIM>::operator-=(const T& val)
  {
    typename slip::MultivariatePolynomial<T,DIM>::iterator it = this->begin();

    if(((*it).first).is_a_constant())
      {
	(*it).second -= val;
      }
    else
      {
	slip::Monomial<DIM> constant;
	constant.powers.fill((unsigned char)0);
	this->insert(constant,-val);
      }
    return *this;
  }

  template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM>& 
  MultivariatePolynomial<T,DIM>::operator*=(const T& val)
  {
    typename slip::MultivariatePolynomial<T,DIM>::iterator it = this->begin();
    typename slip::MultivariatePolynomial<T,DIM>::iterator last = this->end();
    for(;it!=last;++it)
      {
	(*it).second *= val;	
      }
    return *this;
  }

  template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM>& 
  MultivariatePolynomial<T,DIM>::operator/=(const T& val)
  {
    typename slip::MultivariatePolynomial<T,DIM>::iterator it = this->begin();
    typename slip::MultivariatePolynomial<T,DIM>::iterator last = this->end();
    for(;it!=last;++it)
      {
	(*it).second /= val;	
      }
    return *this;
  }

  template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM> 
  MultivariatePolynomial<T,DIM>::operator-() const
  {
    MultivariatePolynomial<T,DIM> tmp(*this);
    typename slip::MultivariatePolynomial<T,DIM>::const_iterator it = this->begin();
    typename slip::MultivariatePolynomial<T,DIM>::const_iterator last = this->end();
    typename slip::MultivariatePolynomial<T,DIM>::iterator it_tmp = tmp.begin();
   
    for(;it!=last;++it,++it_tmp)
      {
	(*it_tmp).second = -(*it).second;	
      }
    return tmp;
  }


  template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM>& MultivariatePolynomial<T,DIM>::operator+=(const MultivariatePolynomial<T,DIM>& rhs)
  {
    typename slip::MultivariatePolynomial<T,DIM>::const_iterator it_rhs = rhs.begin();
    typename slip::MultivariatePolynomial<T,DIM>::const_iterator last_rhs = rhs.end();
    
    for(;it_rhs!=last_rhs;++it_rhs)
      {
	typename slip::MultivariatePolynomial<T,DIM>::iterator it_tmp = 
	  this->find((*it_rhs).first);
	if(it_tmp!=this->end())
	  {
	   (*it_tmp).second += (*it_rhs).second;
	    if((*it_tmp).second == T(0))
	      {
		this->erase(it_tmp);
	      }
	  }
	else
	  {
	    this->insert((*it_rhs).first,(*it_rhs).second);
	  }
      }
    return *this;
  }
  
 template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM>& MultivariatePolynomial<T,DIM>::operator-=(const MultivariatePolynomial<T,DIM>& rhs)
  {
    typename slip::MultivariatePolynomial<T,DIM>::const_iterator it_rhs = rhs.begin();
    typename slip::MultivariatePolynomial<T,DIM>::const_iterator last_rhs = rhs.end();
    
    for(;it_rhs!=last_rhs;++it_rhs)
      {
	typename slip::MultivariatePolynomial<T,DIM>::iterator it_tmp = 
	  this->find((*it_rhs).first);
	if(it_tmp!=this->end())
	  {
	   (*it_tmp).second -= (*it_rhs).second;
	    if((*it_tmp).second == T(0) && !(*it_tmp).first.is_a_constant())
	      {
		this->erase(it_tmp);
	      }
	  }
	else
	  {
	    this->insert((*it_rhs).first,-(*it_rhs).second);
	  }
      }
  
    return *this;
  }
  


  template<typename T, std::size_t DIM>
  inline
  MultivariatePolynomial<T,DIM>& 
  MultivariatePolynomial<T,DIM>::operator*=(const MultivariatePolynomial<T,DIM>& rhs)
  {
    MultivariatePolynomial<T,DIM> tmp;

    for(typename MultivariatePolynomial<T,DIM>::const_iterator const_iter1 = 
      this->begin();const_iter1 != this->end(); ++const_iter1)
      {
	
	for(typename MultivariatePolynomial<T,DIM>::const_iterator const_iter2 = rhs.begin(); const_iter2 != rhs.end();++const_iter2)
	  {
	    tmp[(*const_iter1).first * (*const_iter2).first] +=
	        (*const_iter1).second * (*const_iter2).second;
	    if( tmp[(*const_iter1).first * (*const_iter2).first] == T())
	      {
		tmp.erase((*const_iter1).first * (*const_iter2).first);
	      }
	  }
      }
    
    //this->clear();
    *this = tmp;
    
    return *this;
  }


  template<typename T, std::size_t DIM>
  inline
  void MultivariatePolynomial<T,DIM>::create(const std::size_t order)
  {
    //array of all the powers combinations sort by lexicographical order
     slip::Array2d<std::size_t> powers(slip::power(order+1,DIM),DIM);
    for(std::size_t d = 0; d < powers.dim2(); ++d)
      {
	std::size_t step = slip::power((order+1),d);
	for(std::size_t ord = 0, ord2 = 0; ord <= (powers.dim1() - step); ord+=step,++ord2)
	  {
	    std::size_t tmp = ord2%(order+1);
	    for(std::size_t k = 0; k < step; ++k)
	      {
		powers[ord+k][d] = tmp;
	      }
	  }
      }

    for(std::size_t k = 0; k < powers.dim1(); ++k)
      {
	slip::Monomial<DIM> m;
	
	std::copy(powers.row_begin(k),powers.row_end(k),(m.powers).begin());
	if (m.degree() <= order)
	  {
	    this->insert(m,T(1));
	  }
      }
  }

}//slip::

 /** \name Arithmetical operators */
  /* @{ */
namespace slip{
template<typename T,std::size_t DIM>
inline
MultivariatePolynomial<T,DIM> operator+(const MultivariatePolynomial<T,DIM>& P1, 
					const MultivariatePolynomial<T,DIM>& P2)
{
  MultivariatePolynomial<T,DIM> tmp(P1);
  tmp+=P2;
  return tmp;
}

template<typename T,std::size_t DIM>
inline
MultivariatePolynomial<T,DIM> operator+(const MultivariatePolynomial<T,DIM>& P1, 
					const T& val)
{
  MultivariatePolynomial<T,DIM> tmp(P1);
  tmp+=val;
  return tmp;
}

template<typename T,std::size_t DIM>
inline
MultivariatePolynomial<T,DIM> operator+(const T& val,
					const MultivariatePolynomial<T,DIM>& P1)
{
  return P1+val;
}


template<typename T,std::size_t DIM>
inline
MultivariatePolynomial<T,DIM> operator-(const MultivariatePolynomial<T,DIM>& P1, 
					const MultivariatePolynomial<T,DIM>& P2)
{
  MultivariatePolynomial<T,DIM> tmp(P1);
  tmp-=P2;
  return tmp;
}

template<typename T,std::size_t DIM>
inline
MultivariatePolynomial<T,DIM> operator-(const MultivariatePolynomial<T,DIM>& P1, 
					const T& val)
{
  MultivariatePolynomial<T,DIM> tmp(P1);
  tmp-=val;
  return tmp;
}

template<typename T,std::size_t DIM>
inline
MultivariatePolynomial<T,DIM> operator-(const T& val,
					const MultivariatePolynomial<T,DIM>& P1)
{
  return -(P1-val);
}


template<typename T,std::size_t DIM>
inline
MultivariatePolynomial<T,DIM> operator*(const MultivariatePolynomial<T,DIM>& P1, 
					const MultivariatePolynomial<T,DIM>& P2)
{
  MultivariatePolynomial<T,DIM> tmp(P1);
  tmp*=P2;
  return tmp;
}

template<typename T,std::size_t DIM>
inline
MultivariatePolynomial<T,DIM> operator*(const MultivariatePolynomial<T,DIM>& P1, 
					const T& val)
{
  MultivariatePolynomial<T,DIM> tmp(P1);
  tmp*=val;
  return tmp;
}

template<typename T,std::size_t DIM>
inline
MultivariatePolynomial<T,DIM> operator*(const T& val,
					const MultivariatePolynomial<T,DIM>& P1)
{
  return P1*val;
}


template<typename T,std::size_t DIM>
inline
MultivariatePolynomial<T,DIM> operator/(const MultivariatePolynomial<T,DIM>& P1, 
					const T& val)
{
  MultivariatePolynomial<T,DIM> tmp(P1);
  tmp/=val;
  return tmp;
}
/* @} */
}//slip::
#endif //SLIP_MULTIVARIATEPOLYNOMIAL_HPP
