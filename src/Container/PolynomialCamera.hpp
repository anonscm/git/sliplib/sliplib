/** 
 * \file PolynomialCamera.hpp
 * 
 * \brief Provides a generic polynomial camera model class.
 * 
 */

#ifndef SLIP_POLYNOMIALCAMERA_HPP
#define SLIP_POLYNOMIALCAMERA_HPP

#include <iostream>
#include <string>


#include "Matrix.hpp"
#include "polynomial_algo.hpp"
#include "Array.hpp"
#include "Point2d.hpp"
#include "Point3d.hpp"
#include "Vector.hpp"
#include "io_tools.hpp"
#include "camera_algo.hpp"


#include "CameraModel.hpp"


namespace slip
{
  template <typename Type>
  class PolynomialCamera;

  template <typename Type>
  std::ostream& operator<<(std::ostream & out,
			   const slip::PolynomialCamera<Type>& c);

 
  // template<typename Type>
  // void fit_poly(slip::Vector<Type> &coeffs,
  // 		const std::size_t &Xdeg,
  // 		const std::size_t &Ydeg,
  // 		const std::size_t &Zdeg,
  // 		const slip::Matrix<Type> &data,
  // 		const slip::Vector<std::size_t> &datavars);

  // template<typename Type>
  // inline Type eval_poly(const slip::Vector<Type> &coeffs,
  // 			const std::size_t &Xdeg,
  // 			const std::size_t &Ydeg,
  // 			const std::size_t &Zdeg,
  // 			const slip::Point3d<Type> &pt);

  // template<typename Type>
  // inline Type eval_poly_Xderivative(const slip::Vector<Type> &coeffs,
  // 				    const std::size_t &Xdeg,
  // 				    const std::size_t &Ydeg,
  // 				    const std::size_t &Zdeg,
  // 				    const slip::Point3d<Type> &pt);

  // template<typename Type>
  // inline Type eval_poly_Yderivative(const slip::Vector<Type> &coeffs,
  // 				    const std::size_t &Xdeg,
  // 				    const std::size_t &Ydeg,
  // 				    const std::size_t &Zdeg,
  // 				    const slip::Point3d<Type> &pt);

  // template<typename Type>
  // inline Type eval_poly_Zderivative(const slip::Vector<Type> &coeffs,
  // 				    const std::size_t &Xdeg,
  // 				    const std::size_t &Ydeg,
  // 				    const std::size_t &Zdeg,
  // 				    const slip::Point3d<Type> &pt);

  /*! \class PolynomialCamera
  **  \ingroup Containers Camera
  ** \author Markus Jehle <jehle_markus_AT_yahoo.de>
  ** \author Benoit Tremblais benoit.tremblais_AT_sic.univ-poitiers.fr
  ** \author Lionel Thomas lionel.thomas_AT_univ-poitiers.fr
  ** \author Guillaume Gomit guillaume.gomit_AT_univ-poitiers.fr
  ** \author Gwenael Acher gwenael.acher01_AT_univ-poitiers.fr
  ** \version 0.0.137
  ** \date 2018/06/05
  ** \since 0.0.125
  ** \brief Define a %PolynomialCamera class
  ** \remarks Soloff SM,Adrian RJ,Liu ZC (1997) Distortion compensation for generalized stereoscopic particle image velocimetry. Meas. Sci. Technol. 8:1441-1445, doi:10.1088/0957-0233/8/12/008 is used as a default.
  ** \param Type Type of the coordinates of the PolynomialCamera.
  */
  template <typename Type>
  class PolynomialCamera:public slip::CameraModel<Type>
  {
    typedef slip::PolynomialCamera<Type> self;
    typedef slip::CameraModel<Type> base;

  public:
    /**
     ** \name Constructors & Destructors
     */
    /*@{*/

    /*!
    ** \brief Default constructor of PolynomialCamera (Soloff camera model)
    */
    PolynomialCamera():
      deg_x_(3),
      deg_y_(3),
      deg_z_(2),
      nc_(36),
      x_coeffs_(slip::Vector<Type>(nc_)),
      y_coeffs_(slip::Vector<Type>(nc_)),
      XZ_coeffs_(slip::Vector<Type>(nc_)),
      YZ_coeffs_(slip::Vector<Type>(nc_)),
      YX_coeffs_(slip::Vector<Type>(nc_)),
      ZX_coeffs_(slip::Vector<Type>(nc_)),
      XY_coeffs_(slip::Vector<Type>(nc_)),
      ZY_coeffs_(slip::Vector<Type>(nc_))
      
      
    {
    }
  

    /*!
    ** \brief constructor from data
    */
    PolynomialCamera(const slip::Matrix<Type>& P,
		     const std::size_t &deg_x,
		     const std::size_t &deg_y,
		     const std::size_t &deg_z):
      deg_x_(deg_x),
      deg_y_(deg_y),
      deg_z_(deg_z),
      nc_((deg_x+1)*(deg_y+1)*(deg_z+1)),
      x_coeffs_(slip::Vector<Type>(nc_)),
      y_coeffs_(slip::Vector<Type>(nc_)),
      XZ_coeffs_(slip::Vector<Type>(nc_)),
      YZ_coeffs_(slip::Vector<Type>(nc_)),
      YX_coeffs_(slip::Vector<Type>(nc_)),
      ZX_coeffs_(slip::Vector<Type>(nc_)),
      XY_coeffs_(slip::Vector<Type>(nc_)),
      ZY_coeffs_(slip::Vector<Type>(nc_))
     
      
    {
      this->compute(P);
    }

    /*!
    ** \brief constructor from degrees
    */
    PolynomialCamera(const std::size_t &deg_x,
		     const std::size_t &deg_y,
		     const std::size_t &deg_z):
      deg_x_(deg_x),
      deg_y_(deg_y),
      deg_z_(deg_z),
      nc_((deg_x+1)*(deg_y+1)*(deg_z+1)),
      x_coeffs_(slip::Vector<Type>(nc_)),
      y_coeffs_(slip::Vector<Type>(nc_)),
      XZ_coeffs_(slip::Vector<Type>(nc_)),
      YZ_coeffs_(slip::Vector<Type>(nc_)),
      YX_coeffs_(slip::Vector<Type>(nc_)),
      ZX_coeffs_(slip::Vector<Type>(nc_)),
      XY_coeffs_(slip::Vector<Type>(nc_)),
      ZY_coeffs_(slip::Vector<Type>(nc_))
    {
    }

    /*!
    ** \brief constructor from data
    */
    PolynomialCamera(const slip::Matrix<Type>& P):
      deg_x_(3),
      deg_y_(3),
      deg_z_(2),
      nc_(36),
      x_coeffs_(slip::Vector<Type>(nc_)),
      y_coeffs_(slip::Vector<Type>(nc_)),
      XZ_coeffs_(slip::Vector<Type>(nc_)),
      YZ_coeffs_(slip::Vector<Type>(nc_)),
      YX_coeffs_(slip::Vector<Type>(nc_)),
      ZX_coeffs_(slip::Vector<Type>(nc_)),
      XY_coeffs_(slip::Vector<Type>(nc_)),
      ZY_coeffs_(slip::Vector<Type>(nc_))
    {
      // this->deg_x_ = 3;
      // this->deg_y_ = 3;
      // this->deg_z_ = 2;
      // this->nc_ = (this->deg_x_ + 1)*(this->deg_y_ + 1)*(this->deg_z_ + 1);
      this->compute(P);
    }

    /*!
    ** \brief Constructs a copy of the PolynomialCamera \a rhs
    ** \param rhs
    */
    PolynomialCamera(const self& rhs):
      base(rhs),
      deg_x_(rhs.deg_x_),
      deg_y_(rhs.deg_y_),
      deg_z_(rhs.deg_z_),
      x_coeffs_(rhs.x_coeffs()),
      y_coeffs_(rhs.y_coeffs()),
      XZ_coeffs_(rhs.XZ_coeffs()),
      YZ_coeffs_(rhs.YZ_coeffs()),
      YX_coeffs_(rhs.YX_coeffs()),
      ZX_coeffs_(rhs.ZX_coeffs()),
      XY_coeffs_(rhs.XY_coeffs()),
      ZY_coeffs_(rhs.ZY_coeffs())
    {
    }

    
    /*!
    ** \brief Destructor of the %PolynomialCamera
    */
    virtual ~PolynomialCamera()
    {
    }
    /*@} End Constructors */

  
    /**
     ** \name i/o operators
     */
    /*@{*/

    /*!
    ** \brief Write the PolynomialCamera to the ouput stream
    ** \param out output stream
    ** \param c PolynomialCamera to write to the output stream
    */
    friend std::ostream& operator<< <>(std::ostream & out, const self& c);

    /*@} i/o  operators */


    /**
     ** \name  Assignment operators and methods
     */
    /*@{*/
    /*!
    ** \brief Assign a PolynomialCamera.
    **
    ** Assign elements of PolynomialCamera in \a rhs
    **
    ** \param rhs PolynomialCamera to get the values from.
    ** \return self.
    */
    self& operator=(const self & rhs)
    {
      if(this != &rhs)
        {
          this->base::operator=(rhs);
	  deg_x_ = rhs.deg_x();
          deg_y_ = rhs.deg_y();
          deg_z_ = rhs.deg_z();
	  nc_ = rhs.nc_;
	  x_coeffs_ = rhs.x_coeffs();
          y_coeffs_ = rhs.y_coeffs();
	  XZ_coeffs_ = rhs.XZ_coeffs();
	  YZ_coeffs_ = rhs.YZ_coeffs();
	  YX_coeffs_ = rhs.YX_coeffs();
	  ZX_coeffs_ = rhs.ZX_coeffs();
          XY_coeffs_ = rhs.XY_coeffs();
	  ZY_coeffs_ = rhs.ZY_coeffs();
         
        }
      return *this;
    }
    /*@} End Assignment operators and methods*/


    /**
     ** \name  Accessors/Mutators
     */

    /*!
    ** \param deg_xval %std::size_t
    ** \return void
    */
    void deg_x(const std::size_t &deg_xval)
    {
      deg_x_ = deg_xval;
    }

    /*!
    ** \return %std::size_t
    */
    std::size_t deg_x() const
    {
      return this->deg_x_;
    }

    /*!
    ** \param deg_xval %std::size_t
    ** \return void
    */
    void deg_y(const std::size_t &deg_yval)
    {
      deg_y_ = deg_yval;
    }

    /*!
    ** \return %std::size_t
    */
    std::size_t deg_y() const
    {
      return this->deg_y_;
    }

    /*!
    ** \param deg_zval %std::size_t
    ** \return void
    */
    void deg_z(const std::size_t &deg_zval)
    {
      deg_z_ = deg_zval;
    }

    /*!
    ** \return %std::size_t
    */
    std::size_t deg_z() const
    {
      return this->deg_z_;
    }
  

    /*!
    ** \param coeffs %slip::Vector<Type>
    ** \return void
    */
    void x_coeffs(const slip::Vector<Type> &coeffs)
    {
      x_coeffs_.resize(coeffs.size());
      std::copy(coeffs.begin(),coeffs.end(),x_coeffs_.begin());
    }

    /*!
    ** \return %slip::Vector<Type>
    */
    const slip::Vector<Type>& x_coeffs() const
    {
      return this->x_coeffs_;
    }

    /*!
    ** \param coeffs %slip::Vector<Type>
    ** \return void
    */
    void y_coeffs(const slip::Vector<Type> &coeffs)
    {
      y_coeffs_.resize(coeffs.size());
      std::copy(coeffs.begin(),coeffs.end(),y_coeffs_.begin());
    }

    /*!
    ** \return %slip::Vector<Type>
    */
    const slip::Vector<Type>& y_coeffs() const
    {
      return this->y_coeffs_;
    }

    /*!
    ** \param coeffs %slip::Vector<Type>
    ** \return void
    */
    void XY_coeffs(const slip::Vector<Type> &coeffs)
    {
      XY_coeffs_.resize(coeffs.size());
      std::copy(coeffs.begin(),coeffs.end(),XY_coeffs_.begin());
    }

    /*!
    ** \return %slip::Vector<Type>
    */
    const slip::Vector<Type>& XY_coeffs() const
    {
      return this->XY_coeffs_;
    }

    /*!
    ** \param coeffs %slip::Vector<Type>
    ** \return void
    */
    void YX_coeffs(const slip::Vector<Type> &coeffs)
    {
      YX_coeffs_.resize(coeffs.size());
      std::copy(coeffs.begin(),coeffs.end(),YX_coeffs_.begin());
    }

    /*!
    ** \return %slip::Vector<Type>
    */
    const slip::Vector<Type>& YX_coeffs() const
    {
      return this->YX_coeffs_;
    }

    /*!
    ** \param coeffs %slip::Vector<Type>
    ** \return void
    */
    void XZ_coeffs(const slip::Vector<Type> &coeffs)
    {
      XZ_coeffs_.resize(coeffs.size());
      std::copy(coeffs.begin(),coeffs.end(),XZ_coeffs_.begin());
    }

    /*!
    ** \return %slip::Vector<Type>
    */
    const slip::Vector<Type>& XZ_coeffs() const
    {
      return this->XZ_coeffs_;
    }

    /*!
    ** \param coeffs %slip::Vector<Type>
    ** \return void
    */
    void ZX_coeffs(const slip::Vector<Type> &coeffs)
    {
      ZX_coeffs_.resize(coeffs.size());
      std::copy(coeffs.begin(),coeffs.end(),ZX_coeffs_.begin());
    }

    /*!
    ** \return %slip::Vector<Type>
    */
    const slip::Vector<Type>& ZX_coeffs() const
    {
      return this->ZX_coeffs_;
    }

    /*!
    ** \param coeffs %slip::Vector<Type>
    ** \return void
    */
    void YZ_coeffs(const slip::Vector<Type> &coeffs)
    {
      YZ_coeffs_.resize(coeffs.size());
      std::copy(coeffs.begin(),coeffs.end(),YZ_coeffs_.begin());
    }

    /*!
    ** \return %slip::Vector<Type>
    */
    const slip::Vector<Type>& YZ_coeffs() const
    {
      return this->YZ_coeffs_;
    }

    /*!
    ** \param coeffs %slip::Vector<Type>
    ** \return void
    */
    void ZY_coeffs(const slip::Vector<Type> &coeffs)
    {
      ZY_coeffs_.resize(coeffs.size());
      std::copy(coeffs.begin(),coeffs.end(),ZY_coeffs_.begin());
    }

    /*!
    ** \return %slip::Vector<Type>
    */
    const slip::Vector<Type>& ZY_coeffs() const
    {
      return this->ZY_coeffs_;
    }

    /*@} End Accessors/Mutators*/


    /*!
    ** \brief Clone the %PolynomialCamera
    ** \return new pointer towards a copy of this %PolynomialCamera
    ** \remark must be detroyed after use
    */
    virtual base* clone () const
    {
      return new self(*this);
    }


    /**
     ** \name  Projection methods
     */
    /*@{*/
    /*!
    ** \brief Computes the projection of a 3d world point
    ** onto the image plane.
    ** \param p %Point3d.
    ** \return %Point2d
    */
    inline
    slip::Point2d<Type> projection(const slip::Point3d<Type>& pt) const
    {
      slip::Point2d<Type> res;
      //res[0] = static_cast<Type>(0);
      //res[1] = static_cast<Type>(0);

      res[0] = slip::eval_poly(x_coeffs_,deg_x_,deg_y_,deg_z_,pt);
      res[1] = slip::eval_poly(y_coeffs_,deg_x_,deg_y_,deg_z_,pt);

      return res;
    }

    /*!
    ** \brief Computes the projection gradient of a 3d world point
    ** onto the image plane.
    ** \param p %Point3d.
    ** \return %Matrix
    */
    inline
    slip::Matrix<Type> projection_gradient(const slip::Point3d<Type>& pt,
					   const Type& =1.) const
    {
      slip::Matrix<Type> res(2,3, Type());
      //res = static_cast<Type>(0);

      res(0,0) = slip::eval_poly_Xderivative(x_coeffs_,deg_x_,deg_y_,deg_z_,pt);
      res(1,0) = slip::eval_poly_Xderivative(y_coeffs_,deg_x_,deg_y_,deg_z_,pt);

      res(0,1) = slip::eval_poly_Yderivative(x_coeffs_,deg_x_,deg_y_,deg_z_,pt);
      res(1,1) = slip::eval_poly_Yderivative(y_coeffs_,deg_x_,deg_y_,deg_z_,pt);

      res(0,2) = slip::eval_poly_Zderivative(x_coeffs_,deg_x_,deg_y_,deg_z_,pt);
      res(1,2) = slip::eval_poly_Zderivative(y_coeffs_,deg_x_,deg_y_,deg_z_,pt);

      return res;
    }

    /*!
    ** \brief Computes the projection gradient of a 3d world point
    ** onto the image plane.
    ** \param p %Point3d.
    ** \return %Matrix
    */
    inline
    slip::Matrix<Type> numerical_projection_gradient(const slip::Point3d<Type>& pt,
						     const Type& delta=1.) const
    {
      return base::projection_gradient(pt, delta);
    }


    /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection of an image point.
    ** \param p %Point2d.
    ** \param Z Depth coordinate.
    ** \return %Point3d
    */
    inline
    slip::Point3d<Type> backprojection(const slip::Point2d<Type>& p2, 
				       const Type& Z) const
    {
     
      slip::Point3d<Type> pt(p2[0],p2[1],Z);
      slip::Point3d<Type> res;
      res[0] = slip::eval_poly(XZ_coeffs_,deg_x_,deg_y_,deg_z_,pt);
      res[1] = slip::eval_poly(YZ_coeffs_,deg_x_,deg_y_,deg_z_,pt);
      res[2] = Z;
      
      return res;
    
    }
    

    /*!
    ** \brief Computes the 3d world point corresponding to the ray backprojection of an image point on a X plane
    ** \param p2 %Point2d.
    ** \param X plane x coordinate.
    ** \return backprojected point
    */
    inline
    slip::Point3d<Type> X_backprojection(const slip::Point2d<Type>& p2, 
					 const Type& X) const
    {
 
      slip::Point3d<Type> pt(p2[0],p2[1],X);
      slip::Point3d<Type> res;
      res[0] = X;
      res[1] = slip::eval_poly(YX_coeffs_,deg_x_,deg_y_,deg_z_,pt);
      res[2] = slip::eval_poly(ZX_coeffs_,deg_x_,deg_y_,deg_z_,pt);

      return res;
    }


    /*!
    ** \brief Computes the 3d world point corresponding to the backprojection of an image point on a X plane and test if it is in the square.
    ** \param p2 %Point2d.
    ** \param X plane x coordinate.
    ** \param ymin minimum coordinate along y
    ** \param ymax maximum coordinate along y
    ** \param zmin minimum coordinate along z
    ** \param zmax maximum coordinate along z
    ** \param p3d backprojected point
    ** \return boolean if point exists in the square
    */
    inline
    bool X_backprojection_in_square(const slip::Point2d<Type>& p2, 
				    const Type& X,
				    const Type &ymin,
				    const Type &ymax,
				    const Type &zmin,
				    const Type &zmax,
				    slip::Point3d<Type> &p3d) const
    {
      bool res = false;
      p3d = this->X_backprojection(p2, X);
      if (p3d[1]>=ymin && p3d[1]<=ymax && p3d[2]>=zmin && p3d[2]<=zmax)
	{
	  res = true;
	}
      
      return res;
    }

    /*!
    ** \brief Computes the 3d world point corresponding to the backprojection of an image point on a Y plane
    ** \param p2 %Point2d.
    ** \param y plane y coordinate.
    ** \return backprojected point
    */
    inline
    slip::Point3d<Type> Y_backprojection(const slip::Point2d<Type>& p2, 
					 const Type& Y) const
    {
      //slip::Point3d<Type> pt(p2[0],Y,p2[1]);
      slip::Point3d<Type> pt(p2[0],p2[1],Y);
      slip::Point3d<Type> res;
      res[0] = slip::eval_poly(XY_coeffs_,deg_x_,deg_y_,deg_z_,pt);
      res[1] = Y;
      res[2] = slip::eval_poly(ZY_coeffs_,deg_x_,deg_y_,deg_z_,pt);

      return res;
    }

    
    /*!
    ** \brief Computes the 3d world point corresponding to the backprojection of an image point on a Y plane and test if it is in the square.
    ** \param p2 %Point2d.
    ** \param Y plane y coordinate.
    ** \param xmin minimum coordinate along x
    ** \param xmax maximum coordinate along x
    ** \param zmin minimum coordinate along z
    ** \param zmax maximum coordinate along z
    ** \param p3d backprojected point
    ** \return boolean if point exists in the square
    */
    inline
    virtual bool Y_backprojection_in_square(const slip::Point2d<Type>& p2, 
					    const Type& Y,
					    const Type &xmin,
					    const Type &xmax,
					    const Type &zmin,
					    const Type &zmax,
					    slip::Point3d<Type> &p3d) const
    {
      bool res = false;
      p3d = this->Y_backprojection(p2, Y);      
      if (p3d[0]>=xmin && p3d[0]<=xmax && p3d[2]>=zmin && p3d[2]<=zmax)
	{
	  res = true;
	}
      return res;
    }

    /*!
    ** \brief Computes the 3d world point corresponding to the backprojection of an image point on a Z plane
    ** \param p2 %Point2d.
    ** \param Z plane z coordinate.
    ** \return backprojected point
    */
    inline
    virtual slip::Point3d<Type> Z_backprojection(const slip::Point2d<Type>& p2, 
						 const Type& Z) const
    {
      
    

      // slip::Point3d<Type> pt(p2[0],p2[1],Z);
      // slip::Point3d<Type> res;
      // res[0] = eval_poly(XZ_coeffs_,deg_x_,deg_y_,deg_z_,pt);
      // res[1] = eval_poly(YZ_coeffs_,deg_x_,deg_y_,deg_z_,pt);
      // res[2] = Z;
      // return res;
      return this->backprojection(p2,Z);
    }


    /*!
    ** \brief Computes the 3d world point corresponding to the backprojection of an image point on a Z plane and test if it is in the square.
    ** \param p2 %Point2d.
    ** \param Z plane z coordinate.
    ** \param xmin minimum coordinate along x
    ** \param xmax maximum coordinate along x
    ** \param ymin minimum coordinate along y
    ** \param ymax maximum coordinate along y
    ** \param p3d backprojected point
    ** \return boolean if point exists in the square
    */
    inline
    virtual bool Z_backprojection_in_square(const slip::Point2d<Type>& p2, 
					    const Type& Z,
					    const Type &xmin,
					    const Type &xmax,
					    const Type &ymin,
					    const Type &ymax,
					    slip::Point3d<Type> &p3d) const

    {
      bool res = false;
      p3d = this->Z_backprojection(p2, Z);
      if (p3d[0]>=xmin && p3d[0]<=xmax && p3d[1]>=ymin && p3d[1]<=ymax)
	{
	  res = true;
	}
      return res;
    }


    /*@} End projection methods*/

    /**
     ** \name  Computation methods
     */
    /*@{*/


    /*!
    ** \brief Computes the parameters of the polynomial camera model
    ** \param P Matrix containing the input data.
    */
    void compute(const slip::Matrix<Type>& P)
    {
      // x=0, y=1, X=2, Y=3, Z=4
      slip::Vector<std::size_t> datavars(4);
      datavars[0]=2;
      datavars[1]=3;
      datavars[2]=4;
      datavars[3]=0;
      slip::fit_poly(x_coeffs_,deg_x_,deg_y_,deg_z_,P,datavars);
      datavars[3]=1;
      slip::fit_poly(y_coeffs_,deg_x_,deg_y_,deg_z_,P,datavars);
      datavars[0]=0;
      datavars[1]=1;
      datavars[2]=3;
      datavars[3]=2;
      slip::fit_poly(XY_coeffs_,deg_x_,deg_y_,deg_z_,P,datavars);
      datavars[2]=2;
      datavars[3]=3;
      slip::fit_poly(YX_coeffs_,deg_x_,deg_y_,deg_z_,P,datavars);
      datavars[2]=4;
      datavars[3]=2;
      slip::fit_poly(XZ_coeffs_,deg_x_,deg_y_,deg_z_,P,datavars);
      datavars[2]=2;
      datavars[3]=4;
      slip::fit_poly(ZX_coeffs_,deg_x_,deg_y_,deg_z_,P,datavars);
      datavars[2]=4;
      datavars[3]=3;
      slip::fit_poly(YZ_coeffs_,deg_x_,deg_y_,deg_z_,P,datavars);
      datavars[2]=3;
      datavars[3]=4;
      slip::fit_poly(ZY_coeffs_,deg_x_,deg_y_,deg_z_,P,datavars);
    }

    /*@} End computation methods*/

    /**
     ** \name  input/output methods
     */
    /*@{*/
    /*!
    ** \brief Read a multivariate polynomial from an ASCII file
    ** \param file File path name.
    ** \todo factorize code
    **
    */
    virtual void read(const std::string& file)
    {
      std::ifstream input;
      input.open(file.c_str());
      input >> deg_x_;
      input >> deg_y_;
      input >> deg_z_;
      const std::size_t nc = (deg_x_+1)*(deg_y_+1)*(deg_z_+1);
      nc_ = nc;
      x_coeffs_.resize(nc);
      y_coeffs_.resize(nc);
      XZ_coeffs_.resize(nc);
      YZ_coeffs_.resize(nc);
      YX_coeffs_.resize(nc);
      ZX_coeffs_.resize(nc);
      XY_coeffs_.resize(nc);
      ZY_coeffs_.resize(nc);
      for (std::size_t i=0; i<nc; ++i)
        {
          long double val;
          input >> val;
          x_coeffs_[i]=val;
        }
      for (std::size_t i=0; i<nc; ++i)
        {
          long double val;
          input >> val;
          y_coeffs_[i]=val;
        }
      for (std::size_t i=0; i<nc; ++i)
        {
          long double val;
          input >> val;
          XY_coeffs_[i]=val;
        }
      for (std::size_t i=0; i<nc; ++i)
        {
          long double val;
          input >> val;
          YX_coeffs_[i]=val;
        }
      for (std::size_t i=0; i<nc; ++i)
        {
          long double val;
          input >> val;
          XZ_coeffs_[i]=val;
        }
      for (std::size_t i=0; i<nc; ++i)
        {
          long double val;
          input >> val;
          ZX_coeffs_[i]=val;
        }
      for (std::size_t i=0; i<nc; ++i)
        {
          long double val;
          input >> val;
          YZ_coeffs_[i]=val;
        }
      for (std::size_t i=0; i<nc; ++i)
        {
          long double val;
          input >> val;
          ZY_coeffs_[i]=val;
        }
    }

    /*!
    ** \brief Write a multivariate polynomial to an ASCII file
    **
    ** \param file File path name.
    ** \todo factorize code
    */
    void write(const std::string& file)
    {
      std::ofstream output(file.c_str(),std::ios::out);
      std::size_t nc=x_coeffs_.size();
      output << deg_x_ << " " << deg_y_ << " " << deg_z_ << std::endl;
      // x_coeffs_
      output << std::setprecision(std::numeric_limits<Type>::digits10) << x_coeffs_[0];
      for (std::size_t i=1; i<nc; ++i)
        output << std::setprecision(std::numeric_limits<Type>::digits10) << " " << x_coeffs_[i];
      output << std::endl;
      // y_coeffs_
      output << std::setprecision(std::numeric_limits<Type>::digits10) << y_coeffs_[0];
      for (std::size_t i=1; i<nc; ++i)
        output << std::setprecision(std::numeric_limits<Type>::digits10) << " " << y_coeffs_[i];
      output << std::endl;
      // XY_coeffs_
      output << std::setprecision(std::numeric_limits<Type>::digits10) << XY_coeffs_[0];
      for (std::size_t i=1; i<nc; ++i)
        output << std::setprecision(std::numeric_limits<Type>::digits10) << " " << XY_coeffs_[i];
      output << std::endl;
      // YX_coeffs_
      output << std::setprecision(std::numeric_limits<Type>::digits10) << YX_coeffs_[0];
      for (std::size_t i=1; i<nc; ++i)
        output << std::setprecision(std::numeric_limits<Type>::digits10) << " " << YX_coeffs_[i];
      output << std::endl;
      // XZ_coeffs_
      output << std::setprecision(std::numeric_limits<Type>::digits10) << XZ_coeffs_[0];
      for (std::size_t i=1; i<nc; ++i)
        output << std::setprecision(std::numeric_limits<Type>::digits10) << " " << XZ_coeffs_[i];
      output << std::endl;
      // ZX_coeffs_
      output << std::setprecision(std::numeric_limits<Type>::digits10) << ZX_coeffs_[0];
      for (std::size_t i=1; i<nc; ++i)
        output << std::setprecision(std::numeric_limits<Type>::digits10) << " " << ZX_coeffs_[i];
      output << std::endl;
      // YZ_coeffs_
      output << std::setprecision(std::numeric_limits<Type>::digits10) << YZ_coeffs_[0];
      for (std::size_t i=1; i<nc; ++i)
        output << std::setprecision(std::numeric_limits<Type>::digits10) << " " << YZ_coeffs_[i];
      output << std::endl;
      // ZY_coeffs_
      output << std::setprecision(std::numeric_limits<Type>::digits10) << ZY_coeffs_[0];
      for (std::size_t i=1; i<nc; ++i)
        output << std::setprecision(std::numeric_limits<Type>::digits10) << " " << ZY_coeffs_[i];
      output << std::endl;

      output.close();
    }

    /*@} End input/output methods*/
    //protected:
    std::size_t deg_x_;
    std::size_t deg_y_;
    std::size_t deg_z_;
    std::size_t nc_;
    slip::Vector<Type> x_coeffs_;
    slip::Vector<Type> y_coeffs_;
    slip::Vector<Type> XZ_coeffs_;
    slip::Vector<Type> YZ_coeffs_;
    slip::Vector<Type> YX_coeffs_;
    slip::Vector<Type> ZX_coeffs_;
    slip::Vector<Type> XY_coeffs_;
    slip::Vector<Type> ZY_coeffs_;

    //TODO: Factorize code
    std::ostream& display(std::ostream& out) const
    {
      std::cerr << std::endl;
      std::cerr << "x coeffs" << std::endl;
      std::size_t i=0;
      if (this->x_coeffs().size()>0)
        for (std::size_t m=0; m<=this->deg_x(); ++m)
          for (std::size_t n=0; n<=this->deg_y(); ++n)
            for (std::size_t q=0; q<=this->deg_z(); ++q)
              {
                if (m==0 && n==0 && q==0)
                  out << this->x_coeffs()(0);
                else
                  {
                    out << "+" << this->x_coeffs()(i);
                    if (m>0)
                      out << "X^" << m;
                    if (n>0)
                      out << "Y^" << n;
                    if (q>0)
                      out << "Z^" << q;
                  }
                ++i;
              }
      std::cerr << std::endl;
      std::cerr << "y coeffs" << std::endl;
      i=0;
      if (this->y_coeffs().size()>0)
        for (std::size_t m=0; m<=this->deg_x(); ++m)
          for (std::size_t n=0; n<=this->deg_y(); ++n)
            for (std::size_t q=0; q<=this->deg_z(); ++q)
              {
                if (m==0 && n==0 && q==0)
                  out << this->y_coeffs()(0);
                else
                  {
                    out << "+" << this->y_coeffs()(i);
                    if (m>0)
                      out << "X^" << m;
                    if (n>0)
                      out << "Y^" << n;
                    if (q>0)
                      out << "Z^" << q;
                  }
                ++i;
              }
      std::cerr << std::endl;
      std::cerr << "YX coeffs" << std::endl;
      i=0;
      if (this->XY_coeffs().size()>0)
        for (std::size_t m=0; m<=this->deg_x(); ++m)
          for (std::size_t n=0; n<=this->deg_y(); ++n)
            for (std::size_t q=0; q<=this->deg_z(); ++q)
              {
                if (m==0 && n==0 && q==0)
                  out << this->YX_coeffs()(0);
                else
                  {
                    out << "+" << this->YX_coeffs()(i);
                    if (m>0)
                      out << "x^" << m;
                    if (n>0)
                      out << "y^" << n;
                    if (q>0)
                      out << "X^" << q;
                  }
                ++i;
              }
      std::cerr << std::endl;
      std::cerr << "ZX coeffs" << std::endl;
      i=0;
      if (this->ZX_coeffs().size()>0)
        for (std::size_t m=0; m<=this->deg_x(); ++m)
          for (std::size_t n=0; n<=this->deg_y(); ++n)
            for (std::size_t q=0; q<=this->deg_z(); ++q)
              {
                if (m==0 && n==0 && q==0)
                  out << this->ZX_coeffs()(0);
                else
                  {
                    out << "+" << this->ZX_coeffs()(i);
                    if (m>0)
                      out << "x^" << m;
                    if (n>0)
                      out << "y^" << n;
                    if (q>0)
                      out << "X^" << q;
                  }
                ++i;
              }
      std::cerr << std::endl;
      std::cerr << "XY coeffs" << std::endl;
      i=0;
      if (this->XY_coeffs().size()>0)
        for (std::size_t m=0; m<=this->deg_x(); ++m)
          for (std::size_t n=0; n<=this->deg_y(); ++n)
            for (std::size_t q=0; q<=this->deg_z(); ++q)
              {
                if (m==0 && n==0 && q==0)
                  out << this->XY_coeffs()(0);
                else
                  {
                    out << "+" << this->XY_coeffs()(i);
                    if (m>0)
                      out << "x^" << m;
                    if (n>0)
                      out << "y^" << n;
                    if (q>0)
                      out << "Y^" << q;
                  }
                ++i;
              }
      std::cerr << std::endl;
      std::cerr << "ZY coeffs" << std::endl;
      i=0;
      if (this->ZY_coeffs().size()>0)
        for (std::size_t m=0; m<=this->deg_x(); ++m)
          for (std::size_t n=0; n<=this->deg_y(); ++n)
            for (std::size_t q=0; q<=this->deg_z(); ++q)
              {
                if (m==0 && n==0 && q==0)
                  out << this->ZY_coeffs()(0);
                else
                  {
                    out << "+" << this->ZY_coeffs()(i);
                    if (m>0)
                      out << "x^" << m;
                    if (n>0)
                      out << "y^" << n;
                    if (q>0)
                      out << "Y^" << q;
                  }
                ++i;
              }
      std::cerr << std::endl;
      std::cerr << "XZ coeffs" << std::endl;
      i=0;
      if (this->ZX_coeffs().size()>0)
        for (std::size_t m=0; m<=this->deg_x(); ++m)
          for (std::size_t n=0; n<=this->deg_y(); ++n)
            for (std::size_t q=0; q<=this->deg_z(); ++q)
              {
                if (m==0 && n==0 && q==0)
                  out << this->XZ_coeffs()(0);
                else
                  {
                    out << "+" << this->XZ_coeffs()(i);
                    if (m>0)
                      out << "x^" << m;
                    if (n>0)
                      out << "y^" << n;
                    if (q>0)
                      out << "Z^" << q;
                  }
                ++i;
              }
      std::cerr << std::endl;
      std::cerr << "YZ coeffs" << std::endl;
      i=0;
      if (this->YZ_coeffs().size()>0)
        for (std::size_t m=0; m<=this->deg_x(); ++m)
          for (std::size_t n=0; n<=this->deg_y(); ++n)
            for (std::size_t q=0; q<=this->deg_z(); ++q)
              {
                if (m==0 && n==0 && q==0)
                  out << this->YZ_coeffs()(0);
                else
                  {
                    out << "+" << this->YZ_coeffs()(i);
                    if (m>0)
                      out << "x^" << m;
                    if (n>0)
                      out << "y^" << n;
                    if (q>0)
                      out << "Z^" << q;
                  }
                ++i;
              }

      return out;
    }

  };
}


namespace slip
{

  template<typename Type>
  std::ostream& operator<<(std::ostream & out,
			   const slip::PolynomialCamera<Type>& c)
  {
    return c.display(out);
  }


 

 

  // template <typename Type>
  // inline Type power(const Type &X, const std::size_t &n)
  // {
  //   Type res=static_cast<Type>(1);
  //   if (n==static_cast<std::size_t>(0))
  //     return (res);
  //   for (std::size_t i=0; i<n; ++i)
  //     res*=X;
  //   return(res);
  // }



}



#endif //SLIP_POLYNOMIALCAMERA_HPP
