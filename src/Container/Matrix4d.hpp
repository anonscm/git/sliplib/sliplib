/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/*!
 ** \file Matrix4d.hpp
 **
 ** \brief Provides a class to manipulate Matrix4d.
 ** \version Fluex 1.0
 ** \date 2013/07/11
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef SLIP_MATRIX4D_HPP
#define SLIP_MATRIX4D_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <algorithm>
#include <cmath>
#include <string>
#include <cstddef>

#include "Array4d.hpp"
#include "stride_iterator.hpp"
#include "apply.hpp"
#include "iterator4d_box.hpp"
#include "iterator4d_range.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>


namespace slip
{
template<class T>
class stride_iterator;
}

namespace slip
{

template <typename T>
class Matrix4d;

template <typename T>
class Array4d;

template <typename T>
std::ostream& operator<<(std::ostream & out, const slip::Matrix4d<T>& a);

template<typename T>
bool operator==(const slip::Matrix4d<T>& x,
		const slip::Matrix4d<T>& y);

template<typename T>
bool operator!=(const slip::Matrix4d<T>& x,
		const slip::Matrix4d<T>& y);

template<typename T>
bool operator<(const slip::Matrix4d<T>& x,
		const slip::Matrix4d<T>& y);

template<typename T>
bool operator>(const slip::Matrix4d<T>& x,
		const slip::Matrix4d<T>& y);

template<typename T>
bool operator<=(const slip::Matrix4d<T>& x,
		const slip::Matrix4d<T>& y);

template<typename T>
bool operator>=(const slip::Matrix4d<T>& x,
		const slip::Matrix4d<T>& y);

/*!
 ** @ingroup Containers Containers4d Fluex MathematicalContainer
 ** \class Matrix4d
 ** \version Fluex 1.0
 ** \date 2013/07/11
 ** \version 0.0.2
 ** \date 2014/04/05
 ** \since 1.4.0
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief Numerical matrix4d class.
 ** This container statisfies the RandomAccessContainer concepts of the STL
 ** except the simple bracket which is replaced by a double bracket. It extends
 ** the interface of Array4d adding arithmetical: +=, -=, *=, /=,+,-,/,*...
 ** and mathematical operators : min, max, abs, sqrt, cos, acos, sin, asin,
 ** tan, atan, exp, log, cosh, sinh, tanh, log10, sum, apply...
 ** \param T Type of object in the Matrix4d
 **
 ** \par Axis conventions:
 ** \image html iterator4d_conventions.jpg "axis and notation conventions"
 ** \image latex iterator4d_conventions.eps "axis and notation conventions" width=5cm
 **
 */

template <typename T>
class Matrix4d
{
public :

	typedef T value_type;
	typedef Matrix4d<T> self;
	typedef const Matrix4d<T> const_self;

	typedef value_type* pointer;
	typedef value_type const* const_pointer;
	typedef value_type& reference;
	typedef value_type const& const_reference;

	typedef ptrdiff_t difference_type;
	typedef std::size_t size_type;

	typedef pointer iterator;
	typedef const_pointer const_iterator;

	typedef std::reverse_iterator<iterator> reverse_iterator;
	typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

	//slab, slice, row and col iterator
	typedef slip::stride_iterator<pointer> slab_iterator;
	typedef slip::stride_iterator<const_pointer> const_slab_iterator;
	typedef slip::stride_iterator<pointer> slice_iterator;
	typedef slip::stride_iterator<const_pointer> const_slice_iterator;
	typedef pointer row_iterator;
	typedef const_pointer const_row_iterator;
	typedef slip::stride_iterator<pointer> col_iterator;
	typedef slip::stride_iterator<const_pointer> const_col_iterator;

	typedef slip::stride_iterator<slab_iterator> slab_range_iterator;
	typedef slip::stride_iterator<const_slab_iterator> const_slab_range_iterator;
	typedef slip::stride_iterator<slice_iterator> slice_range_iterator;
	typedef slip::stride_iterator<const_slice_iterator> const_slice_range_iterator;
	typedef slip::stride_iterator<pointer> row_range_iterator;
	typedef slip::stride_iterator<const_pointer> const_row_range_iterator;
	typedef slip::stride_iterator<col_iterator> col_range_iterator;
	typedef slip::stride_iterator<const_col_iterator> const_col_range_iterator;

	typedef std::reverse_iterator<slab_iterator> reverse_slab_iterator;
	typedef std::reverse_iterator<const_slab_iterator> const_reverse_slab_iterator;
	typedef std::reverse_iterator<slice_iterator> reverse_slice_iterator;
	typedef std::reverse_iterator<const_slice_iterator> const_reverse_slice_iterator;
	typedef std::reverse_iterator<iterator> reverse_row_iterator;
	typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
	typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
	typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;

	typedef std::reverse_iterator<slab_range_iterator> reverse_slab_range_iterator;
	typedef std::reverse_iterator<const_slab_range_iterator> const_reverse_slab_range_iterator;
	typedef std::reverse_iterator<slice_range_iterator> reverse_slice_range_iterator;
	typedef std::reverse_iterator<const_slice_range_iterator> const_reverse_slice_range_iterator;
	typedef std::reverse_iterator<row_range_iterator> reverse_row_range_iterator;
	typedef std::reverse_iterator<const_row_range_iterator> const_reverse_row_range_iterator;
	typedef std::reverse_iterator<col_range_iterator> reverse_col_range_iterator;
	typedef std::reverse_iterator<const_col_range_iterator> const_reverse_col_range_iterator;

	//iterator 4d
	typedef typename slip::Array4d<T>::iterator4d iterator4d;
	typedef typename slip::Array4d<T>::const_iterator4d const_iterator4d;
	typedef typename slip::Array4d<T>::iterator4d_range iterator4d_range;
	typedef typename slip::Array4d<T>::const_iterator4d_range const_iterator4d_range;

	typedef std::reverse_iterator<iterator4d> reverse_iterator4d;
	typedef std::reverse_iterator<const_iterator4d> const_reverse_iterator4d;
	typedef std::reverse_iterator<iterator4d_range> reverse_iterator4d_range;
	typedef std::reverse_iterator<const_iterator4d_range> const_reverse_iterator4d_range;

	//default iterator of the container
	typedef iterator4d default_iterator;
	typedef const_iterator4d const_default_iterator;

	//Range
	typedef slip::Range<int> range;


	static const std::size_t DIM = 4;

public:

	/**
	 ** \name Constructors & Destructors
	 */

	/*@{*/

	/*!
	 ** \brief Constructs a %Matrix4d.
	 */
	Matrix4d();

	/*!
	 ** \brief Constructs a %Matrix4d.
	 ** \param d1 first dimension of the %Matrix4d
	 ** \param d2 second dimension of the %Matrix4d
	 ** \param d3 third dimension of the %Matrix4d
	 ** \param d4 fourth dimension of the %Matrix4d
	 **
	 ** \par The %Matrix4d is initialized by the default value of T.
	 */
	Matrix4d(const std::size_t d1,
			const std::size_t d2,
			const std::size_t d3,
			const std::size_t d4);

	/*!
	 ** \brief Constructs a %Matrix4d initialized by the scalar value \a val.
	 ** \param d1 first dimension of the %Matrix4d
	 ** \param d2 second dimension of the %Matrix4d
	 ** \param d3 third dimension of the %Matrix4d
	 ** \param d4 fourth dimension of the %Matrix4d
	 ** \param val initialization value of the elements
	 */
	Matrix4d(const std::size_t d1,
			const std::size_t d2,
			const std::size_t d3,
			const std::size_t d4,
			const T& val);
	/*!
	 ** \brief Constructs a %Matrix4d initialized by an array \a val.
	 ** \param d1 first dimension of the %Matrix4d
	 ** \param d2 second dimension of the %Matrix4d
	 ** \param d3 third dimension of the %Matrix4d
	 ** \param d4 fourth dimension of the %Matrix4d
	 ** \param val initialization array value of the elements
	 */
	Matrix4d(const std::size_t d1,
			const std::size_t d2,
			const std::size_t d3,
			const std::size_t d4,
			const T* val);

	/**
	 **  \brief  Contructs a %Matrix4d from a range.
	 ** \param d1 first dimension of the %Matrix4d
	 ** \param d2 second dimension of the %Matrix4d
	 ** \param d3 third dimension of the %Matrix4d
	 ** \param d4 fourth dimension of the %Matrix4d
	 **  \param  first  An input iterator.
	 **  \param  last  An input iterator.
	 **
	 ** Create a %Matrix4d consisting of copies of the elements from
	 ** [first,last).
	 */
	template<typename InputIterator>
	Matrix4d(const size_type d1,
			const size_type d2,
			const size_type d3,
			const std::size_t d4,
			InputIterator first,
			InputIterator last):
			array_(new Array4d<T>(d1,d2,d3,d4,first,last))
			{}

	/*!
	 ** \brief Constructs a copy of the Matrix4d \a rhs
	 */
	Matrix4d(const Matrix4d<T>& rhs);


	/*!
	 ** \brief Destructor of the Matrix4d
	 */
	~Matrix4d();


	/*@} End Constructors */


	/*!
	 ** \brief Resizes a %Matrix4d.
	 ** \param d1 new first dimension
	 ** \param d2 new second dimension
	 ** \param d3 new third dimension
	 ** \param d4 new fourth dimension
	 ** \param val new value for all the elements
	 */
	void resize(std::size_t d1,
			std::size_t d2,
			std::size_t d3,
			std::size_t d4,
			const T& val = T());

	/**
	 ** \name One dimensional global iterators
	 */
	/*@{*/

	//****************************************************************************
	//                          One dimensional iterators
	//****************************************************************************



	//----------------------Global iterators------------------------------

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  first element in the %Matrix4d.  Iteration is done in ordinary
	 **  element order.
	 ** \return const begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> const A1(10,9,5,2);
	 **  slip::Matrix4d<double> const A2(10,9,5,2);
	 **  slip::Matrix4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.begin(),A1.end(),
	 **                A2.begin(),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_iterator begin() const;

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element in the %Matrix4d.  Iteration is done in ordinary
	 **  element order.
	 ** \return begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(10,9,5,2);
	 **  slip::Matrix4d<double> A2(10,9,5,2);
	 **  slip::Matrix4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.begin(),A1.end(),
	 **                A2.begin(),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	iterator begin();

	/*!
	 **  \brief Returns a read/write iterator that points one past the last
	 **  element in the %Matrix4d.  Iteration is done in ordinary
	 **  element order.
	 ** \return end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(10,9,5,2);
	 **  slip::Matrix4d<double> A2(10,9,5,2);
	 **  slip::Matrix4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.begin(),A1.end(),
	 **                A2.begin(),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	iterator end();

	/*!
	 **  \brief Returns a read-only (constant) iterator that points one past
	 **  the last element in the %Matrix4d.  Iteration is done in
	 **  ordinary element order.
	 ** \return const end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(10,9,5,2);
	 **  slip::Matrix4d<double> A2(10,9,5,2);
	 **  slip::Matrix4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.begin(),A1.end(),
	 **                A2.begin(),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_iterator end() const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the
	 **  last element in the %Matrix4d. Iteration is done in reverse
	 **  element order.
	 ** \return reverse begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(10,9,5,2);
	 **  slip::Matrix4d<double> A2(10,9,5,2);
	 **  slip::Matrix4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.rbegin(),A1.rend(),
	 **                A2.rbegin(),S.rbegin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	reverse_iterator rbegin();

	/*!
	 **  \brief Returns a read-only (constant) reverse iterator that points
	 **  to the last element in the %Matrix4d.  Iteration is done in
	 **  reverse element order.
	 ** \return const reverse begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(10,9,5,2);
	 **  slip::Matrix4d<double> A2(10,9,5,2);
	 **  slip::Matrix4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.rbegin(),A1.rend(),
	 **                A2.rbegin(),S.rbegin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_reverse_iterator rbegin() const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to one
	 **  before the first element in the %Matrix4d.  Iteration is done
	 **  in reverse element order.
	 **  \return reverse end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(10,9,5,2);
	 **  slip::Matrix4d<double> A2(10,9,5,2);
	 **  slip::Matrix4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.rbegin(),A1.rend(),
	 **                A2.rbegin(),S.rbegin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	reverse_iterator rend();

	/*!
	 **  \brief Returns a read-only (constant) reverse iterator that points
	 **  to one before the first element in the %Matrix4d.  Iteration
	 **  is done in reverse element order.
	 **  \return const reverse end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(10,9,5,2);
	 **  slip::Matrix4d<double> A2(10,9,5,2);
	 **  slip::Matrix4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.rbegin(),A1.rend(),
	 **                A2.rbegin(),S.rbegin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_reverse_iterator rend() const;

	/*@} End One dimensional global iterators */

	//****************************************************************************
	//                          One dimensional stride iterators
	//****************************************************************************

	/**
	 ** \name One dimensional slab iterators
	 */
	/*@{*/
	//--------------------One dimensional slab iterators----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the first
	 ** element of the line (slice,row,col) through the slabs in the %Matrix4d.
	 ** Iteration is done in ordinary element order (increasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row  row coordinate of the line
	 ** \param col  col coordinate of the line
	 ** \return slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Matrix4d<double> A1(10,9,5,2);
	 ** slip::Matrix4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slabs through the first slice,
	 ** the second row and the third column. Put the calculated sums in the S array.
	 ** std::transform(A1.slab_begin(0,1,2),A1.slab_end(0,1,2),
	 **                A2.slab_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	slab_iterator slab_begin(const size_type slice, const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the first
	 **  element of the line (slice,row,col) through the slabs in the %Matrix4d.
	 **  Iteration is done in ordinary element order (increasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return const_slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Matrix4d<double> A1(10,9,5,2);
	 ** slip::Matrix4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slabs through the first slice,
	 ** the second row and the third column. Put the calculated sums in the S array.
	 ** std::transform(A1.slab_begin(0,1,2),A1.slab_end(0,1,2),
	 **                A2.slab_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_slab_iterator slab_begin(const size_type slice, const size_type row, const size_type col) const;

	/*!
	 ** \brief Returns a read/write iterator that points to the one past the end
	 **  element of the line (slice,row,col) through the slabs in the %Matrix4d.
	 **  Iteration is done in ordinary element order (increasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Matrix4d<double> A1(10,9,5,2);
	 ** slip::Matrix4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slabs through the first slice,
	 ** the second row and the third column. Put the calculated sums in the S array.
	 ** std::transform(A1.slab_begin(0,1,2),A1.slab_end(0,1,2),
	 **                A2.slab_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	slab_iterator slab_end(const size_type slice, const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the one past the end
	 **  element of the line (slice,row,col) through the slabs in the %Matrix4d.
	 **  Iteration is done in ordinary element order (increasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return const_slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Matrix4d<double> A1(10,9,5,2);
	 ** slip::Matrix4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slabs through the first slice,
	 ** the second row and the third column. Put the calculated sums in the S array.
	 ** std::transform(A1.slab_begin(0,1,2),A1.slab_end(0,1,2),
	 **                A2.slab_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_slab_iterator slab_end(const size_type slice, const size_type row, const size_type col) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the
	 ** last element of the line (slice,row,col) through the slabs in the %Matrix4d.
	 ** Iteration is done in reverse element order (decreasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return reverse_slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Matrix4d<double> A1(10,9,5,2);
	 ** slip::Matrix4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 ** the slabs through the first slice, the second row and the third column.
	 ** Put the calculated sums in the S array.
	 ** std::transform(A1.slab_rbegin(0,1,2),A1.slab_rend(0,1,2),
	 **                A2.slab_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	reverse_slab_iterator slab_rbegin(const size_type slice, const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the
	 **  last element of the line (slice,row,col) through the slabs in the %Matrix4d.
	 **  Iteration is done in reverse element order (decreasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return const_reverse_slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Matrix4d<double> A1(10,9,5,2);
	 ** slip::Matrix4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 ** the slabs through the first slice, the second row and the third column.
	 ** Put the calculated sums in the S array.
	 ** std::transform(A1.slab_rbegin(0,1,2),A1.slab_rend(0,1,2),
	 **                A2.slab_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_reverse_slab_iterator slab_rbegin(const size_type slice, const size_type row, const size_type col) const;

	/*!
	 ** \brief Returns a read/write iterator that points to the
	 ** one before the first element of the line (slice,row,col) through the slabs in the %Matrix4d.
	 ** Iteration is done in reverse element order (decreasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return reverse_slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Matrix4d<double> A1(10,9,5,2);
	 ** slip::Matrix4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 ** the slabs through the first slice, the second row and the third column.
	 ** Put the calculated sums in the S array.
	 ** std::transform(A1.slab_rbegin(0,1,2),A1.slab_rend(0,1,2),
	 **                A2.slab_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	reverse_slab_iterator slab_rend(const size_type slice, const size_type row, const size_type col);


	/*!
	 ** \brief Returns a read (constant) iterator that points to the
	 ** one before the first element of the line (slice,row,col) through the slabs in the %Matrix4d.
	 ** Iteration is done in reverse element order (decreasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return const_reverse_slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 ** \code
	 **
	 ** slip::Matrix4d<double> A1(10,9,5,2);
	 ** slip::Matrix4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 ** the slabs through the first slice, the second row and the third column.
	 ** Put the calculated sums in the S array.
	 ** std::transform(A1.slab_rbegin(0,1,2),A1.slab_rend(0,1,2),
	 **                A2.slab_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_reverse_slab_iterator slab_rend(const size_type slice, const size_type row, const size_type col) const;

	/*@} End One dimensional slab iterators */

	/**
	 ** \name One dimensional slice iterators
	 */
	/*@{*/
	//--------------------One dimensional slice iterators----------------------


	/*!
	 ** \brief Returns a read/write iterator that points to the first
	 ** element of the line (slab,row,col) through the slices in the %Matrix4d.
	 ** Iteration is done in ordinary element order (increasing slice number).
	 ** \param slab slab coordinate of the line
	 ** \param row  row coordinate of the line
	 ** \param col  col coordinate of the line
	 ** \return slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::Matrix4d<double> A1(10,9,5,2);
	 **  slip::Matrix4d<double> A2(10,9,5,2);
	 **  slip::Array<double> S(9);
	 **  //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slices through the first slab,
	 **  the second row and the third column. Put the calculated sums in the S array.
	 **  std::transform(A1.slice_begin(0,1,2),A1.slice_end(0,1,2),
	 **                A2.slice_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 **  \endcode
	 */
	slice_iterator slice_begin(const size_type slab, const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the first
	 ** element of the line (slab,row,col) through the slices in the %Matrix4d.
	 ** Iteration is done in ordinary element order (increasing slice number).
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return const_slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::Matrix4d<double> A1(10,9,5,2);
	 **  slip::Matrix4d<double> A2(10,9,5,2);
	 **  slip::Array<double> S(9);
	 **  //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slices through the first slab,
	 **  the second row and the third column. Put the calculated sums in the S array.
	 **  std::transform(A1.slice_begin(0,1,2),A1.slice_end(0,1,2),
	 **                A2.slice_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 **  \endcode
	 */
	const_slice_iterator slice_begin(const size_type slab, const size_type row, const size_type col) const;

	/*!
	 ** \brief Returns a read/write iterator that points to the one past the end
	 ** element of the line (slab,row,col) through the slices in the %Matrix4d.
	 ** Iteration is done in ordinary element order (increasing slice number).
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::Matrix4d<double> A1(10,9,5,2);
	 **  slip::Matrix4d<double> A2(10,9,5,2);
	 **  slip::Array<double> S(9);
	 **  //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slices through the first slab,
	 **  the second row and the third column. Put the calculated sums in the S array.
	 **  std::transform(A1.slice_begin(0,1,2),A1.slice_end(0,1,2),
	 **                A2.slice_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 **  \endcode
	 */
	slice_iterator slice_end(const size_type slab, const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the one past the end
	 ** element of the line (slab,row,col) through the slices in the %Matrix4d.
	 ** Iteration is done in ordinary element order (increasing slice number).
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return const_slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::Matrix4d<double> A1(10,9,5,2);
	 **  slip::Matrix4d<double> A2(10,9,5,2);
	 **  slip::Array<double> S(9);
	 **  //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slices through the first slab,
	 **  the second row and the third column. Put the calculated sums in the S array.
	 **  std::transform(A1.slice_begin(0,1,2),A1.slice_end(0,1,2),
	 **                A2.slice_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 **  \endcode
	 */
	const_slice_iterator slice_end(const size_type slab, const size_type row, const size_type col) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the
	 ** last element of the line (slab,row,col) through the slices in the %Matrix4d.
	 ** Iteration is done in reverse element order (decreasing slice number).
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 **         \return reverse_slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::Matrix4d<double> A1(10,9,5,2);
	 **  slip::Matrix4d<double> A2(10,9,5,2);
	 **  slip::Array<double> S(9);
	 **  //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 **  the slices through the first slab, the second row and the third column.
	 **  Put the calculated sums in the S array.
	 **  std::transform(A1.slice_rbegin(0,1,2),A1.slice_rend(0,1,2),
	 **                A2.slice_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 **  \endcode
	 */
	reverse_slice_iterator slice_rbegin(const size_type slab, const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the
	 ** last element of the line (slab,row,col) through the slices in the %Matrix4d.
	 ** Iteration is done in reverse element order (decreasing slice number).
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return const_reverse_slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(10,9,5,2);
	 **  slip::Matrix4d<double> A2(10,9,5,2);
	 **  slip::Array<double> S(9);
	 **  //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 **  the slices through the first slab, the second row and the third column.
	 **  Put the calculated sums in the S array.
	 **  std::transform(A1.slice_rbegin(0,1,2),A1.slice_rend(0,1,2),
	 **                A2.slice_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_reverse_slice_iterator slice_rbegin(const size_type slab, const size_type row, const size_type col) const;

	/*!
	 ** \brief Returns a read/write iterator that points to the
	 ** one before the first element of the line (slab,row,col) through the slices in the %Matrix4d.
	 ** Iteration is done in reverse element order (decreasing slice number).
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return reverse_slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Matrix4d<double> A1(10,9,5,2);
	 ** slip::Matrix4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 ** the slices through the first slab, the second row and the third column.
	 ** Put the calculated sums in the S array.
	 ** std::transform(A1.slice_rbegin(0,1,2),A1.slice_rend(0,1,2),
	 **                A2.slice_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	reverse_slice_iterator slice_rend(const size_type slab, const size_type row, const size_type col);


	/*!
	 ** \brief Returns a read (constant) iterator that points to the
	 ** one before the first element of the line (slab,row,col) through the slices in the %Matrix4d.
	 ** Iteration is done in reverse element order (decreasing slice number).
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return const_reverse_slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Matrix4d<double> A1(10,9,5,2);
	 ** slip::Matrix4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 ** the slices through the first slab, the second row and the third column.
	 ** Put the calculated sums in the S array.
	 ** std::transform(A1.slice_rbegin(0,1,2),A1.slice_rend(0,1,2),
	 **                A2.slice_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_reverse_slice_iterator slice_rend(const size_type slab, const size_type row, const size_type col) const;

	/*@} End One dimensional slice iterators */

	/**
	 ** \name One dimensional row iterators
	 */
	/*@{*/
	//-------------------row iterators----------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done in ordinary element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	row_iterator row_begin(const size_type slab, const size_type slice,
			const size_type row);

	/*!
	 **  \brief Returns a read_only iterator that points to the first
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done in ordinary element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin const_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_row_iterator row_begin(const size_type slab, const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done in ordinary element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	row_iterator row_end(const size_type slab, const size_type slice,
			const size_type row);


	/*!
	 **  \brief Returns a read_only iterator that points to the past-the-end
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done in ordinary element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end const_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_row_iterator row_end(const size_type slab, const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the last
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done in reverse element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	reverse_row_iterator row_rbegin(const size_type slab, const size_type slice,
			const size_type row);

	/*!
	 **  \brief Returns a read_only reverse iterator that points to the last
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done in reverse element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin const_reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_reverse_row_iterator row_rbegin(const size_type slab, const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done in reverse element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	reverse_row_iterator row_rend(const size_type slab, const size_type slice,
			const size_type row);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the first
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done in reverse element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end const_reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_reverse_row_iterator row_rend(const size_type slab, const size_type slice,
			const size_type row) const;

	/*@} End One dimensional row iterators */
	/**
	 ** \name One dimensional col iterators
	 */
	/*@{*/
	//-------------------col iterators----------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	col_iterator col_begin(const size_type slab, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only iterator that points to the first
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin const_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_col_iterator col_begin(const size_type slab, const size_type slice,
			const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	col_iterator col_end(const size_type slab, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only iterator that points to the past-the-end
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end const_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_col_iterator col_end(const size_type slab, const size_type slice,
			const size_type col) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the last
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	reverse_col_iterator col_rbegin(const size_type slab, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the last
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin const_reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_reverse_col_iterator col_rbegin(const size_type slab, const size_type slice,
			const size_type col) const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	reverse_col_iterator col_rend(const size_type slab, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the first
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Matrix4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end const_reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_reverse_col_iterator col_rend(const size_type slab, const size_type slice,
			const size_type col) const;

	/*@} End One dimensional col iterators */

	//****************************************************************************
	//                          One dimensional range iterators
	//****************************************************************************

	/**
	 ** \name One dimensional slab range iterators
	 */
	/*@{*/
	//------------------------slab range iterators -----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Matrix4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,7,6,5);
	 **  slip::Matrix4d<double> A2(8,7,6,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the line (0,0,0) of A1 iterated according to the
	 ** //range in the line (1,1,1) of A2
	 ** std::copy(A1.slab_begin(0,0,0,range),A1.slab_end(0,0,0,range),A2.slab_begin(1,1,1));
	 ** \endcode
	 */
	slab_range_iterator slab_begin(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points one past the end
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Matrix4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,7,6,5);
	 **  slip::Matrix4d<double> A2(8,7,6,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the line (0,0,0) of A1 iterated according to the
	 ** //range in the line (1,1,1) of A2
	 ** std::copy(A1.slab_begin(0,0,0,range),A1.slab_end(0,0,0,range),A2.slab_begin(1,1,1));
	 ** \endcode
	 */
	slab_range_iterator slab_end(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the first
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Matrix4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> const A1(M); //M is an already existing %Matrix4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated according to the range
	 ** std::copy(A1.slab_begin(0,0,0,range),A1.slab_end(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 */
	const_slab_range_iterator slab_begin(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the last
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Matrix4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> const A1(M); //M is an already existing %Matrix4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated according to the range
	 ** std::copy(A1.slab_begin(0,0,0,range),A1.slab_end(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 */
	const_slab_range_iterator slab_end(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the last
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Matrix4d.
	 ** Iteration is done in the reverse element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin reverse_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> const A1(M); //M is an already existing %Matrix4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slab_rbegin(0,0,0,range),A1.slab_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 **/
	reverse_slab_range_iterator slab_rbegin(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Matrix4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end reverse_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> const A1(M); //M is an already existing %Matrix4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slab_rbegin(0,0,0,range),A1.slab_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 */
	reverse_slab_range_iterator slab_rend(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the last
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Matrix4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_reverse_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> const A1(M); //M is an already existing %Matrix4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slab_rbegin(0,0,0,range),A1.slab_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 */
	const_reverse_slab_range_iterator slab_rbegin(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the lastto the first
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Matrix4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_reverse_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> const A1(M); //M is an already existing %Matrix4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slab_rbegin(0,0,0,range),A1.slab_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 */
	const_reverse_slab_range_iterator slab_rend(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*@} End One dimensional slab range iterators */


	/**
	 ** \name One dimensional slice range iterators
	 */
	/*@{*/
	//------------------------slice range iterators -----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Matrix4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,7,6,5);
	 **  slip::Matrix4d<double> A2(8,7,6,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the line (0,0,0) of A1 iterated according to the
	 ** //range in the line (1,1,1) of A2
	 ** std::copy(A1.slice_begin(0,0,0,range),A1.slice_end(0,0,0,range),A2.slice_begin(1,1,1));
	 ** \endcode
	 */
	slice_range_iterator slice_begin(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points one past the end
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Matrix4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,7,6,5);
	 **  slip::Matrix4d<double> A2(8,7,6,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the line (0,0,0) of A1 iterated according to the
	 ** //range in the line (1,1,1) of A2
	 ** std::copy(A1.slice_begin(0,0,0,range),A1.slice_end(0,0,0,range),A2.slice_begin(1,1,1));
	 ** \endcode
	 */
	slice_range_iterator slice_end(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the first
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Matrix4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> const A1(M); //M is an already existing %Matrix4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated according to the range
	 ** std::copy(A1.slice_begin(0,0,0,range),A1.slice_end(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 */
	const_slice_range_iterator slice_begin(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the last
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Matrix4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> const A1(M); //M is an already existing %Matrix4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated according to the range
	 ** std::copy(A1.slice_begin(0,0,0,range),A1.slice_end(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 */
	const_slice_range_iterator slice_end(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the last
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Matrix4d.
	 ** Iteration is done in the reverse element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> const A1(M); //M is an already existing %Matrix4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slice_rbegin(0,0,0,range),A1.slice_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 **/
	reverse_slice_range_iterator slice_rbegin(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Matrix4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> const A1(M); //M is an already existing %Matrix4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slice_rbegin(0,0,0,range),A1.slice_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 */
	reverse_slice_range_iterator slice_rend(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the last
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Matrix4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> const A1(M); //M is an already existing %Matrix4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slice_rbegin(0,0,0,range),A1.slice_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 */
	const_reverse_slice_range_iterator slice_rbegin(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the lastto the first
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Matrix4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> const A1(M); //M is an already existing %Matrix4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slice_rbegin(0,0,0,range),A1.slice_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 */
	const_reverse_slice_range_iterator slice_rend(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*@} End One dimensional slice range iterators */

	/**
	 ** \name One dimensional row range iterators
	 */
	/*@{*/
	//------------------------row range iterators -----------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the %Range \a range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Matrix4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_begin(0,0,0,range),A1.row_end(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	row_range_iterator row_begin(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read/write iterator that points one past the end
	 **  element of the %Range \a range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Matrix4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return end row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_begin(0,0,0,range),A1.row_end(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	row_range_iterator row_end(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Matrix4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin const_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_begin(0,0,0,range),A1.row_end(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	const_row_range_iterator row_begin(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range) const;


	/*!
	 **  \brief Returns a read_only iterator that points one past the last
	 **  element of the %Range range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Matrix4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row Row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return begin const_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \pre row must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_begin(0,0,0,range),A1.row_end(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	const_row_range_iterator row_end(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read-write iterator that points to the last
	 ** element of the %Range \a range of the row \a row in the slab
	 ** \a slab and the slice \a slice in the %Matrix4d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated
	 ** in reverse order according to the range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_rbegin(0,0,0,range),A1.row_rend(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	reverse_row_range_iterator row_rbegin(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-write iterator that points one before
	 **  the first element of the %Range \a range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Matrix4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated
	 ** in reverse order according to the range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_rbegin(0,0,0,range),A1.row_rend(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	reverse_row_range_iterator row_rend(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range);



	/*!
	 **  \brief Returns a read-only iterator that points to the last
	 **  element of the %Range \a range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Matrix4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return begin const_reverse_row_range_iterator value
	 **
	 ** \pre row must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated
	 ** in reverse order according to the range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_rbegin(0,0,0,range),A1.row_rend(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	const_reverse_row_range_iterator row_rbegin(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range) const;


	/*!
	 **  \brief Returns a read-only iterator that points one before the first
	 **  element of the %Range \a range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Matrix4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return const_reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated
	 ** in reverse order according to the range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_rbegin(0,0,0,range),A1.row_rend(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	const_reverse_row_range_iterator row_rend(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range) const;

	/*@} End One dimensional row range iterators */
	/**
	 ** \name One dimensional col range iterators
	 */
	/*@{*/
	//------------------------col range iterators -----------------------

	/*!
	 **  \brief Returns a read-write iterator that points to the first
	 **  element of the %Range \a range of the col \a col in the slab
	 **  \a slab and the slice \a slice in the %Matrix4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate
	 ** \return begin col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 ** //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the column 1 in the slab 1 and the slice 1 of A2
	 ** std::copy(A1.col_begin(0,0,0,range),A1.col_end(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	col_range_iterator col_begin(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read-write iterator that points to the past
	 **  the end element of the %Range \a range of the col \a col in the slab
	 **  \a slab and the slice \a slice in the
	 **  %Matrix4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 ** //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the column 1 in the slab 1 and the slice 1 of A2
	 ** std::copy(A1.col_begin(0,0,0,range),A1.col_end(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	col_range_iterator col_end(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the col \a col in the slab
	 **  \a slab and the slice \a slice in the %Matrix4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 ** //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the column 1 in the slab 1 and the slice 1 of A2
	 ** std::copy(A1.col_begin(0,0,0,range),A1.col_end(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	const_col_range_iterator col_begin(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 **  \brief Returns a read-only iterator that points to the past
	 **  the end element of the %Range \a range of the col \a col in the slab
	 **  \a slab and the slice \a slice in
	 **  the %Matrix4d.
	 ** \param slab slab coordinate of the line
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate
	 ** \return begin const_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 ** //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the column 1 in the slab 1 and the slice 1 of A2
	 ** std::copy(A1.col_begin(0,0,0,range),A1.col_end(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	const_col_range_iterator col_end(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 **  \brief Returns a read-write iterator that points to the last
	 **  element of the %Range \a range of the col of a slice \a col in the slab
	 **  \a slab and the slice \a slice in the %Matrix4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 **  //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated
	 **  //in reverse order according to the range in the column 1 in the slab 1 and the slice 1 of A2
	 **  std::copy(A1.col_rbegin(0,0,0,range),A1.col_rend(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	reverse_col_range_iterator col_rbegin(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read-write iterator that points to one before
	 **  the first element of the %Range range of the col of a slice \a col in the slab
	 **  \a slab and the slice \a slice in the
	 **  %Matrix4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 **  //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated
	 **  //in reverse order according to the range in the column 1 in the slab 1 and the slice 1 of A2
	 **  std::copy(A1.col_rbegin(0,0,0,range),A1.col_rend(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	reverse_col_range_iterator col_rend(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read_only iterator that points to the last
	 **  element of the %Range \& range of the col of a slice \a col in the slab
	 **  \a slab and the slice \a slice in the %Matrix4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 **  //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated
	 **  //in reverse order according to the range in the column 1 in the slab 1 and the slice 1 of A2
	 **  std::copy(A1.col_rbegin(0,0,0,range),A1.col_rend(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	const_reverse_col_range_iterator col_rbegin(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the col of a slice \a col in the slab
	 **  \a slab and the slice \a slice in the %Matrix4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return const_reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Matrix4d.
	 ** \pre The range must be inside the whole range of the %Matrix4d.
	 ** \par Example:
	 ** \code
	 **  slip::Matrix4d<double> A1(8,8,8,8);
	 **  slip::Matrix4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 **  //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated
	 **  //in reverse order according to the range in the column 1 in the slab 1 and the slice 1 of A2
	 **  std::copy(A1.col_rbegin(0,0,0,range),A1.col_rend(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	const_reverse_col_range_iterator col_rend(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*@} End One dimensional col range iterators */

	//****************************************************************************
	//                          Four dimensional iterators
	//****************************************************************************

	/**
	 ** \name four dimensional iterators : Global iterators
	 */
	/*@{*/

	//------------------------ Global iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator4d that points to the first
	 **  element of the %Matrix4d. It points to the first front upper left element of
	 **  the %Matrix4d.
	 **
	 **  \return iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   //Print M
	 **   std::copy(M.first_front_upper_left(),M.last_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	iterator4d first_front_upper_left();


	/*!
	 **  \brief Returns a read/write iterator4d that points to the past
	 **  the end element of the %Matrix4d. It points to past the end element of
	 **  the last back bottom right element of the %Matrix4d.
	 **
	 **  \return iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   //Print M
	 **   std::copy(M.first_front_upper_left(),M.last_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	iterator4d last_back_bottom_right();


	/*!
	 **  \brief Returns a read-only iterator4d that points to the first
	 **  element of the %Matrix4d. It points to the fist front upper left element of
	 **  the %Matrix4d.
	 **
	 **  \return const iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   //Print M
	 **   std::copy(M.first_front_upper_left(),M.last_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d first_front_upper_left() const;


	/*!
	 **  \brief Returns a read-only iterator4d that points to the past
	 **  the end element of the %Matrix4d. It points to past the end element of
	 **  the last back bottom right element of the %Matrix4d.
	 **
	 **  \return const iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   //Print M
	 **   std::copy(M.first_front_upper_left(),M.last_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d last_back_bottom_right() const;

	/*!
	 **  \brief Returns a read/write reverse iterator4d. It points to the
	 **   last back bottom right element of the %Matrix4d.
	 *    Iteration is done within the %Matrix4d in the reverse order.
	 **
	 **  \return reverse_iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   //Print M in a reverse order
	 **   std::copy(M.rfirst_front_upper_left(),M.rlast_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d rfirst_front_upper_left();

	/*!
	 **  \brief Returns a read/write reverse iterator4d. It points to past the
	 **  first front upper left element of the %Matrix4d.
	 **  Iteration is done in the reverse order.
	 **
	 **  \return reverse iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   //Print M in a reverse order
	 **   std::copy(M.rfirst_front_upper_left(),M.rlast_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 **/
	reverse_iterator4d rlast_back_bottom_right();

	/*!
	 **  \brief Returns a read only reverse iterator4d that points.  It points
	 **  to the last back bottom right element of the %Matrix4d.
	 **  Iteration is done within the %Matrix4d in the reverse order.
	 **
	 ** \return const_reverse_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   //Print M in a reverse order
	 **   std::copy(M.rfirst_front_upper_left(),M.rlast_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d rfirst_front_upper_left() const;


	/*!
	 **  \brief Returns a read only reverse iterator4d. It points to past the
	 **  first front upper left element of the %Matrix4d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \return const reverse iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   //Print M in a reverse order
	 **   std::copy(M.rfirst_front_upper_left(),M.rlast_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d rlast_back_bottom_right() const;

	/*@} End four dimensional iterators : Global iterators */

	/**
	 ** \name four dimensional iterators : Box iterators
	 */
	/*@{*/

	//------------------------ Box iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator4d that points to the first
	 **  element of the %Matrix4d. It points to the first front upper left element of
	 **  the \a %Box4d associated to the %Matrix4d.
	 **
	 **  \param box A %Box4d defining the range of indices to iterate
	 **         within the %Matrix4d.
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \return iterator4d value
	 **  \pre The box indices must be inside the range of the %Matrix4d ones.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box
	 **   std::copy(M.first_front_upper_left(b),M.last_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	iterator4d first_front_upper_left(const Box4d<int>& box);


	/*!
	 **  \brief Returns a read/write iterator4d that points to the past
	 **  the end element of the %Matrix4d. It points to past the end element of
	 **  the last back bottom right element of the \a %Box4d associated to the %Matrix4d.
	 **
	 **  \param box a %Box4d defining the range of indices to iterate
	 **         within the %Matrix4d.
	 **
	 **  \return iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \pre The box indices must be inside the range of the %Matrix4d ones.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box
	 **   std::copy(M.first_front_upper_left(b),M.last_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	iterator4d last_back_bottom_right(const Box4d<int>& box);


	/*!
	 **  \brief Returns a read only iterator4d that points to the first
	 **  element of the %Matrix4d. It points to the front upper left element of
	 **  the \a %Box4d associated to the %Matrix4d.
	 **
	 **  \param box a %Box4d defining the range of indices to iterate
	 **         within the %Matrix4d.
	 **  \return end const iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **
	 **  \pre The box indices must be inside the range of the %Matrix4d ones.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box
	 **   std::copy(M.first_front_upper_left(b),M.last_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d first_front_upper_left(const Box4d<int>& box) const;


	/*!
	 **  \brief Returns a read only iterator4d that points to the past
	 **  the end element of the %Matrix4d. It points to past the end element of
	 **  the back bottom right element of the \a %Box4d associated to the %Matrix4d.
	 **
	 **  \param box a %Box4d defining the range of indices to iterate
	 **         within the %Matrix4d.
	 **  \return end const iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **
	 **  \pre The box indices must be inside the range of the %Matrix4d ones.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box
	 **   std::copy(M.first_front_upper_left(b),M.last_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d last_back_bottom_right(const Box4d<int>& box) const;



	/*!
	 **  \brief Returns a read/write reverse iterator4d. It points to the
	 **  back bottom right element of the \a %Box4d associated to the %Matrix4d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \param box a %Box4d defining the range of indices to iterate
	 **        within the %Matrix4d.
	 **
	 ** \pre The box indices must be inside the range of the %Matrix4d ones.
	 **
	 ** \return reverse iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box in reverse order.
	 **   std::copy(M.rfirst_front_upper_left(b),M.rlast_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d rfirst_front_upper_left(const Box4d<int>& box);

	/*!
	 **  \brief Returns a read/write reverse iterator4d. It points to one
	 **  before the front upper left element of the %Box4d \a box associated to
	 **  the %Matrix4d.
	 **
	 ** \param box A %Box4d defining the range of indices to iterate
	 **        within the %Matrix4d.
	 **
	 ** \pre The box indices must be inside the range of the %Matrix4d ones.
	 **
	 ** \return reverse iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box in reverse order.
	 **   std::copy(M.rfirst_front_upper_left(b),M.rlast_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d rlast_back_bottom_right(const Box4d<int>& box);

	/*!
	 **  \brief Returns a read only reverse iterator4d. It points to the
	 **  back bottom right element of the %Box4d \a box associated to the %Matrix4d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \param box A %Box4d defining the range of indices to iterate
	 **        within the %Matrix4d.
	 **
	 ** \pre The box indices must be inside the range of the %Matrix4d ones.
	 **
	 ** \return const reverse iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box in reverse order.
	 **   std::copy(M.rfirst_front_upper_left(b),M.rlast_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d rfirst_front_upper_left(const Box4d<int>& box) const;


	/*!
	 **  \brief Returns a read-only reverse iterator4d. It points to one
	 **  before the front element of the bottom right element of the %Box4d
	 **  \a box associated to the %Matrix4d.
	 **
	 ** \param box A %Box4d defining the range of indices to iterate
	 **        within the %Matrix4d.
	 **
	 ** \pre The box indices must be inside the range of the %Matrix4d ones.
	 **
	 ** \return const reverse iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box in reverse order.
	 **   std::copy(M.rfirst_front_upper_left(b),M.rlast_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d rlast_back_bottom_right(const Box4d<int>& box) const;


	/*@} End four dimensional iterators : Box iterators */

	/**
	 ** \name four dimensional iterators : Range iterators
	 */

	/*@{*/

	//------------------------ Range iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator4d_range that points to the
	 **  first front upper left element of the ranges \a slab_range, \a slice_range,
	 **  \a row_range and \a col_range associated to the %Matrix4d.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Matrix4d ones.
	 **
	 ** \return iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.first_front_upper_left(slar,slir,rr,cr),M.last_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	iterator4d_range first_front_upper_left(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range);

	/*!
	 ** \brief Returns a read/write iterator4d_range that points to the
	 **  past the end last back bottom right element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Matrix4d.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Matrix4d ones.
	 **
	 ** \return iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.first_front_upper_left(slar,slir,rr,cr),M.last_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	iterator4d_range last_back_bottom_right(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range);


	/*!
	 **  \brief Returns a read-only iterator4d_range that points to the
	 **   to the first front upper left element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Matrix4d.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Matrix4d ones.
	 **
	 ** \return const_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.first_front_upper_left(slar,slir,rr,cr),M.last_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d_range first_front_upper_left(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range) const;


	/*!
	 **  \brief Returns a read-only iterator4d_range that points to the past
	 **  the end last back bottom right element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Matrix4d.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Matrix4d ones.
	 **
	 ** \return const_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.first_front_upper_left(slar,slir,rr,cr),M.last_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d_range last_back_bottom_right(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range) const;

	/*!
	 **  \brief Returns a read/write reverse_iterator4d_range that points to the
	 **  past the last back bottom right element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Matrix4d.
	 **  Iteration is done in the reverse order.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Matrix4d ones.
	 **
	 ** \return reverse_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfirst_front_upper_left(slar,slir,rr,cr),M.rlast_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d_range rfirst_front_upper_left(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range);

	/*!
	 **  \brief Returns a read/write reverse_iterator4d_range that points
	 **  to one before the first front upper left element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Matrix4d.
	 **  Iteration is done in the reverse order.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Matrix4d ones.
	 **
	 ** \return reverse_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfirst_front_upper_left(slar,slir,rr,cr),M.rlast_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d_range rlast_back_bottom_right(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range);

	/*!
	 ** \brief Returns a read-only reverse_iterator4d_range that points
	 **  to the past the last back bottom right element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Matrix4d.
	 **  Iteration is done in the reverse order.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Matrix4d ones.
	 **
	 ** \return const_reverse_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfirst_front_upper_left(slar,slir,rr,cr),M.rlast_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d_range rfirst_front_upper_left(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range) const;

	/*!
	 **  \brief Returns a read-only reverse_iterator4d_range that points
	 **  to one before the first front upper left element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Matrix4d.
	 **  Iteration is done in the reverse order.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Matrix4d ones.
	 **
	 ** \return const_reverse_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Matrix4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfirst_front_upper_left(slar,slir,rr,cr),M.rlast_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d_range rlast_back_bottom_right(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range) const;


	/*@} End four dimensional iterators : Range iterators */

	//********************************************************************

	/**
	 ** \name i/o operators
	 */
	/*@{*/
	/*!
	 ** \brief Write the %Matrix4d to the ouput stream
	 ** \param out output std::ostream
	 ** \param a %Matrix4d to write to the output stream
	 */
	friend std::ostream& operator<< <>(std::ostream & out,
			const self& a);

	/*@} End i/o operators */

	/**
	 ** \name  Assignment operators and methods
	 */
	/*@{*/
	/*!
	 ** \brief Assign a %Matrix4d.
	 **
	 ** Assign elements of %Matrix4d in \a rhs
	 **
	 ** \param rhs %Matrix4d to get the values from.
	 ** \return
	 */
	self& operator=(const Matrix4d<T> & rhs);



	/*!
	 ** \brief Assign all the elments of the %Matrix4d by value
	 **
	 **
	 ** \param value A reference-to-const of arbitrary type.
	 ** \return
	 */
	self& operator=(const T& value);


	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with copies of value
	 ** \param value  A reference-to-const of arbitrary type.
	 */
	void fill(const T& value)
	{
		std::fill_n(this->begin(),this->size(),value);
	}

	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of
	 **        the value array
	 ** \param value  A pointer of arbitrary type.
	 */
	void fill(const T* value)
	{
		std::copy(value,value + this->size(), this->begin());
	}

	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of the range [first,last)
	 **  \param  first  An input iterator.
	 **  \param  last   An input iterator.
	 **
	 **
	 */
	template<typename InputIterator>
	void fill(InputIterator first,
			InputIterator last)
	{
		std::copy(first,last, this->begin());
	}
	/*@} End Assignment operators and methods*/



	/**
	 ** \name Comparison operators
	 */
	/*@{*/
	/*!
	 ** \brief %Matrix4d equality comparison
	 ** \param x A %Matrix4d
	 ** \param y A %Matrix4d of the same type of \a x
	 ** \return true iff the size and the elements of the Arrays are equal
	 */
	friend bool operator== <>(const Matrix4d<T>& x,
			const Matrix4d<T>& y);

	/*!
	 ** \brief %Matrix4d inequality comparison
	 ** \param x A %Matrix4d
	 ** \param y A %Matrix4d of the same type of \a x
	 ** \return true if !(x == y)
	 */
	friend bool operator!= <>(const Matrix4d<T>& x,
			const Matrix4d<T>& y);

	/*!
	 ** \brief Less than comparison operator (%Matrix4d ordering relation)
	 ** \param x A %Matrix4d
	 ** \param y A %Matrix4d of the same type of \a x
	 ** \return true iff \a x is lexicographically less than \a y
	 */
	friend bool operator< <>(const Matrix4d<T>& x,
			const Matrix4d<T>& y);

	/*!
	 ** \brief More than comparison operator
	 ** \param x A %Matrix4d
	 ** \param y A %Matrix4d of the same type of \a x
	 ** \return true iff y > x
	 */
	friend bool operator> <>(const Matrix4d<T>& x,
			const Matrix4d<T>& y);

	/*!
	 ** \brief Less than equal comparison operator
	 ** \param x A %Matrix4d
	 ** \param y A %Matrix4d of the same type of \a x
	 ** \return true iff !(y > x)
	 */
	friend bool operator<= <>(const Matrix4d<T>& x,
			const Matrix4d<T>& y);

	/*!
	 ** \brief More than equal comparison operator
	 ** \param x A %Matrix4d
	 ** \param y A %Matrix4d of the same type of \a x
	 ** \return true iff !(x < y)
	 */
	friend bool operator>= <>(const Matrix4d<T>& x,
			const Matrix4d<T>& y);


	/*@} Comparison operators */




	/**
	 ** \name  Element access operators
	 */
	/*@{*/

	T*** operator[](const size_type l);

	const T** const* operator[](const size_type l) const;



	/*!
	 ** \brief Subscript access to the data contained in the %Matrix4d.
	 ** \param l The index of the slab for which the data should be accessed.
	 ** \param k The index of the slice for which the data should be accessed.
	 ** \param i The index of the row for which the data should be accessed.
	 ** \param j The index of the column for which the data should be accessed.
	 ** \return Read/Write reference to data.
	 ** \pre l < slabs()
	 ** \pre k < slices()
	 ** \pre i < rows()
	 ** \pre j < cols()
	 ** This operator allows for easy, 4d array-style, data access.
	 ** Note that data access with this operator is unchecked and
	 ** out_of_range lookups are not defined.
	 */
	reference operator()(const size_type l, const size_type k,
			const size_type i,
			const size_type j);

	/*!
	 ** \brief Subscript access to the data contained in the %Matrix4d.
	 ** \param l The index of the slab for which the data should be accessed.
	 ** \param k The index of the slice for which the data should be accessed.
	 ** \param i The index of the row for which the data should be accessed.
	 ** \param j The index of the column for which the data should be accessed.
	 ** \return Read_only (constant) reference to data.
	 ** \pre l < slabs()
	 ** \pre k < slices()
	 ** \pre i < rows()
	 ** \pre j < cols()
	 ** This operator allows for easy, 4d array-style, data access.
	 ** Note that data access with this operator is unchecked and
	 ** out_of_range lookups are not defined.
	 */
	const_reference operator()(const size_type l, const size_type k,
			const size_type i,
			const size_type j) const;

	/*@} End Element access operators */


	/*!
	 ** \brief Returns the name of the class
	 **
	 */
	std::string name() const;

	/*!
	 ** \brief Returns the number of slabs (first dimension size)
	 **        in the %Matrix4d
	 */
	size_type dim1() const;

	/*!
	 ** \brief Returns the number of slabs (first dimension size)
	 **        in the %Matrix4d
	 */
	size_type slabs() const;

	/*!
	 ** \brief Returns the number of slices (second dimension size)
	 **        in the %Matrix4d
	 */
	size_type dim2() const;

	/*!
	 ** \brief Returns the number of slices (second dimension size)
	 **        in the %Matrix4d
	 */
	size_type slices() const;

	/*!
	 ** \brief Returns the number of rows (third dimension size)
	 **        in the %Matrix4d
	 */
	size_type dim3() const;

	/*!
	 ** \brief Returns the number of rows (third dimension size)
	 **        in the %Matrix4d
	 */
	size_type rows() const;

	/*!
	 ** \brief Returns the number of columns (fourth dimension size)
	 **        in the %Matrix4d
	 */
	size_type dim4() const;

	/*!
	 ** \brief Returns the number of columns (fourth dimension size)
	 **        in the %Matrix4d
	 */
	size_type cols() const;

	/*!
	 ** \brief Returns the number of columns (fourth dimension size)
	 **        in the %Matrix4d
	 */
	size_type columns() const;



	/*!
	 ** \brief Returns the number of elements in the %Matrix4d
	 */
	size_type size() const;


	/*!
	 ** \brief Returns the maximal size (number of elements) in the %Matrix4d
	 */
	size_type max_size() const;

	/*!
	 ** \brief Returns true if the %Matrix4d is empty.  (Thus size() == 0)
	 */
	bool empty()const;

	/*!
	 ** \brief Swaps data with another %Matrix4d.
	 ** \param M A %Matrix4d of the same element type
	 ** \pre dim1() == M.dim1()
	 ** \pre dim2() == M.dim2()
	 ** \pre dim3() == M.dim3()
	 ** \pre dim4() == M.dim4()
	 */
	void swap(Matrix4d& M);

	/**
	 ** \name  Arithmetic operators
	 */
	/*@{*/
	/*!
	 ** \brief Add val to each element of the Matrix
	 ** \param val value
	 ** \return reference to the resulting Matrix
	 */
	self& operator+=(const T& val);
	self& operator-=(const T& val);
	self& operator*=(const T& val);
	self& operator/=(const T& val);


	self  operator-() const;



	self& operator+=(const self& rhs);
	self& operator-=(const self& rhs);
	self& operator*=(const self& rhs);
	self& operator/=(const self& rhs);


	/*@} End Arithmetic operators */

	/**
	 ** \name  Mathematic operators
	 */
	/*@{*/
	/*!
	 ** \brief Returns the min element of the %Matrix
	 **  according to the operator <
	 ** \pre size() != 0
	 */
	T& min() const;


	/*!
	 ** \brief Returns the max element of the %Matrix
	 ** according to the operator <
	 ** \pre size() != 0
	 */
	T& max() const;

	/*!
	 ** \brief Returns the sum of the elements of the %Matrix
	 ** \pre size() != 0
	 */
	T sum() const;


	/*!
	 ** \brief Applys the one-parameter C-function \a fun
	 **        to each element of the %Matrix4d
	 ** \param fun The one-parameter C function
	 ** \return the resulting %Matrix4d
	 */
	Matrix4d<T>& apply(T (*fun)(T));

	/*!
	 ** \brief Applys the one-parameter C-function \a fun
	 **        to each element of the %Matrix4d
	 ** \param fun The one-const-parameter C function
	 ** \return the resulting %Matrix4d
	 */
	Matrix4d<T>& apply(T (*fun)(const T&));

	/*@} End Arithmetic operators */

private:
	Array4d<T>* array_;


 private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    { 
      if(version >= 0)
	{
	  ar & this->array_;
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & this->array_;
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

///double alias
typedef slip::Matrix4d<double> Matrix4d_d;
///float alias
typedef slip::Matrix4d<float> Matrix4d_f;
///long alias
typedef slip::Matrix4d<long> Matrix4d_l;
///unsigned long alias
typedef slip::Matrix4d<unsigned long> Matrix4d_ul;
///short alias
typedef slip::Matrix4d<short> Matrix4d_s;
///unsigned long alias
typedef slip::Matrix4d<unsigned short> Matrix4d_us;
///int alias
typedef slip::Matrix4d<int> Matrix4d_i;
///unsigned int alias
typedef slip::Matrix4d<unsigned int> Matrix4d_ui;
///char alias
typedef slip::Matrix4d<char> Matrix4d_c;
///unsigned char alias
typedef slip::Matrix4d<unsigned char> Matrix4d_uc;


}//slip::


namespace slip{
/*!
 ** \brief pointwise addition of two %Matrix4d
 ** \param M1 first %Matrix4d
 ** \param M2 seconf %Matrix4d
 ** \pre M1.dim1() == M2.dim1()
 ** \pre M1.dim2() == M2.dim2()
 ** \pre M1.dim3() == M2.dim3()
 ** \pre M1.dim4() == M2.dim4()
 ** \return resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> operator+(const Matrix4d<T>& M1,
		const Matrix4d<T>& M2);

/*!
 ** \brief addition of a scalar to each element of a %Matrix4d
 ** \param M1 the %Matrix4d
 ** \param val the scalar
 ** \return resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> operator+(const Matrix4d<T>& M1,
		const T& val);

/*!
 ** \brief addition of a scalar to each element of a %Matrix4d
 ** \param val the scalar
 ** \param M1 the %Matrix4d
 ** \return resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> operator+(const T& val,
		const Matrix4d<T>& M1);

/*!
 ** \brief pointwise substraction of two %Matrix4d
 ** \param M1 first %Matrix4d
 ** \param M2 seconf %Matrix4d
 ** \pre M1.dim1() == M2.dim1()
 ** \pre M1.dim2() == M2.dim2()
 ** \pre M1.dim3() == M2.dim3()
 ** \pre M1.dim4() == M2.dim4()
 ** \return resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> operator-(const Matrix4d<T>& M1,
		const Matrix4d<T>& M2);

/*!
 ** \brief substraction of a scalar to each element of a %Matrix4d
 ** \param M1 the %Matrix4d
 ** \param val the scalar
 ** \return resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> operator-(const Matrix4d<T>& M1,
		const T& val);

/*!
 ** \brief substraction of a scalar to each element of a %Matrix4d
 ** \param val the scalar
 ** \param M1 the %Matrix4d
 ** \return resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> operator-(const T& val,
		const Matrix4d<T>& M1);

/*!
 ** \brief pointwise multiplication of two %Matrix4d
 ** \param M1 first %Matrix4d
 ** \param M2 seconf %Matrix4d
 ** \pre M1.dim1() == M2.dim1()
 ** \pre M1.dim2() == M2.dim2()
 ** \pre M1.dim3() == M2.dim3()
 ** \pre M1.dim4() == M2.dim4()
 ** \return resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> operator*(const Matrix4d<T>& M1,
		const Matrix4d<T>& M2);

/*!
 ** \brief multiplication of a scalar to each element of a %Matrix4d
 ** \param M1 the %Matrix4d
 ** \param val the scalar
 ** \return resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> operator*(const Matrix4d<T>& M1,
		const T& val);

/*!
 ** \brief multiplication of a scalar to each element of a %Matrix4d
 ** \param val the scalar
 ** \param M1 the %Matrix4d
 ** \return resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> operator*(const T& val,
		const Matrix4d<T>& M1);

/*!
 ** \brief pointwise division of two %Matrix4d
 ** \param M1 first %Matrix4d
 ** \param M2 seconf %Matrix4d
 ** \pre M1.dim1() == M2.dim1()
 ** \pre M1.dim2() == M2.dim2()
 ** \pre M1.dim3() == M2.dim3()
 ** \pre M1.dim4() == M2.dim4()
 ** \return resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> operator/(const Matrix4d<T>& M1,
		const Matrix4d<T>& M2);

/*!
 ** \brief division of a scalar to each element of a %Matrix4d
 ** \param M1 the %Matrix4d
 ** \param val the scalar
 ** \return resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> operator/(const Matrix4d<T>& M1,
		const T& val);



/*!
 ** \relates Matrix4d
 ** \brief Returns the min element of a Matrix4d
 ** \param M1 the Matrix4d
 ** \return the min element
 */
template<typename T>
T& min(const Matrix4d<T>& M1);

/*!
 ** \relates Matrix4d
 ** \brief Returns the max element of a Matrix4d
 ** \param M1 the Matrix4d
 ** \return the min element
 */
template<typename T>
T& max(const Matrix4d<T>& M1);

/*!
 ** \relates Matrix4d
 ** \brief Returns the abs value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> abs(const Matrix4d<T>& V);

/*!
 ** \relates Matrix4d
 ** \brief Returns the sqrt value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> sqrt(const Matrix4d<T>& V);

/*!
 ** \relates Matrix4d
 ** \brief Returns the cos value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> cos(const Matrix4d<T>& V);

/*!
 ** \relates Matrix4d
 ** \brief Returns the acos value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> acos(const Matrix4d<T>& V);

/*!
 ** \relates Matrix4d
 ** \brief Returns the sin value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> sin(const Matrix4d<T>& V);

/*!
 ** \relates Matrix4d
 ** \brief Returns the sin value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> asin(const Matrix4d<T>& V);


/*!
 ** \relates Matrix4d
 ** \brief Returns the tan value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> tan(const Matrix4d<T>& V);

/*!
 ** \relates Matrix4d
 ** \brief Returns the atan value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> atan(const Matrix4d<T>& V);

/*!
 ** \relates Matrix4d
 ** \brief Returns the exp value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> exp(const Matrix4d<T>& V);

/*!
 ** \relates Matrix4d
 ** \brief Returns the log value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> log(const Matrix4d<T>& V);

/*!
 ** \relates Matrix4d
 ** \brief Returns the cosh value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> cosh(const Matrix4d<T>& V);

/*!
 ** \relates Matrix4d
 ** \brief Returns the sinh value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> sinh(const Matrix4d<T>& V);

/*!
 ** \relates Matrix4d
 ** \brief Returns the tanh value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> tanh(const Matrix4d<T>& V);

/*!
 ** \relates Matrix4d
 ** \brief Returns the log10 value of each element of the %Matrix4d
 ** \param V The %Matrix4d
 ** \return the resulting %Matrix4d
 */
template<typename T>
Matrix4d<T> log10(const Matrix4d<T>& V);

}//slip::



namespace slip
{
//////////////////////////////////////////
//   Constructors
//////////////////////////////////////////
template<typename T>
inline
Matrix4d<T>::Matrix4d():
array_(new slip::Array4d<T>())
{}

template<typename T>
inline
Matrix4d<T>::Matrix4d(const typename Matrix4d<T>::size_type d1,
		const typename Matrix4d<T>::size_type d2,
		const typename Matrix4d<T>::size_type d3,
		const typename Matrix4d<T>::size_type d4):
		array_(new slip::Array4d<T>(d1,d2,d3,d4))
		{}

template<typename T>
inline
Matrix4d<T>::Matrix4d(const typename Matrix4d<T>::size_type d1,
		const typename Matrix4d<T>::size_type d2,
		const typename Matrix4d<T>::size_type d3,
		const typename Matrix4d<T>::size_type d4,
		const T& val):
		array_(new slip::Array4d<T>(d1,d2,d3,d4,val))
		{}

template<typename T>
inline
Matrix4d<T>::Matrix4d(const typename Matrix4d<T>::size_type d1,
		const typename Matrix4d<T>::size_type d2,
		const typename Matrix4d<T>::size_type d3,
		const typename Matrix4d<T>::size_type d4,
		const T* val):
		array_(new slip::Array4d<T>(d1,d2,d3,d4,val))
		{}


template<typename T>
inline
Matrix4d<T>::Matrix4d(const Matrix4d<T>& rhs):
array_(new slip::Array4d<T>((*rhs.array_)))
{}

template<typename T>
inline
Matrix4d<T>::~Matrix4d()
{
	delete array_;
}

///////////////////////////////////////////


//////////////////////////////////////////
//   Assignment operators
//////////////////////////////////////////
template<typename T>
inline
Matrix4d<T>&  Matrix4d<T>::operator=(const Matrix4d<T> & rhs)
{
	if(this != &rhs)
	{
		*array_ = *(rhs.array_);
	}
	return *this;
}

template<typename T>
inline
Matrix4d<T>& Matrix4d<T>::operator=(const T& value)
{
	std::fill_n((*array_)[0][0][0],array_->size(),value);
	return *this;
}

template<typename T>
inline
void Matrix4d<T>::resize(const typename Matrix4d<T>::size_type d1,
		const typename Matrix4d<T>::size_type d2,
		const typename Matrix4d<T>::size_type d3,
		const typename Matrix4d<T>::size_type d4,
		const T& val)
		{
	array_->resize(d1,d2,d3,d4,val);
		}
///////////////////////////////////////////


//////////////////////////////////////////
//   Iterators
//////////////////////////////////////////

//****************************************************************************
//                          One dimensional iterators
//****************************************************************************



//----------------------Global iterators------------------------------

template<typename T>
inline
typename Matrix4d<T>::const_iterator Matrix4d<T>::begin() const
{
	Array4d<T> const * tp(array_);
	return tp->begin();
}

template<typename T>
inline
typename Matrix4d<T>::const_iterator Matrix4d<T>::end() const
{
	Array4d<T> const * tp(array_);
	return tp->end();
}

template<typename T>
inline
typename Matrix4d<T>::iterator Matrix4d<T>::begin()
{
	return array_->begin();
}

template<typename T>
inline
typename Matrix4d<T>::iterator Matrix4d<T>::end()
{
	return array_->end();
}

template<typename T>
inline
typename Matrix4d<T>::reverse_iterator Matrix4d<T>::rbegin()
{
	return typename Matrix4d<T>::reverse_iterator(this->end());
}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_iterator Matrix4d<T>::rbegin() const
{
	return typename Matrix4d<T>::const_reverse_iterator(this->end());
}

template<typename T>
inline
typename Matrix4d<T>::reverse_iterator Matrix4d<T>::rend()
{
	return typename Matrix4d<T>::reverse_iterator(this->begin());
}


template<typename T>
inline
typename Matrix4d<T>::const_reverse_iterator Matrix4d<T>::rend() const
{
	return typename Matrix4d<T>::const_reverse_iterator(this->begin());
}

//--------------------One dimensional slab iterators----------------------


template<typename T>
inline
typename Matrix4d<T>::slab_iterator Matrix4d<T>::slab_begin(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col){
	return array_->slab_begin(slice,row,col);
}


template<typename T>
inline
typename Matrix4d<T>::const_slab_iterator Matrix4d<T>::slab_begin(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col) const{
	Array4d<T> const * tp(array_);
	return tp->slab_begin(slice,row,col);
}

template<typename T>
inline
typename Matrix4d<T>::slab_iterator Matrix4d<T>::slab_end(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col){
	return array_->slab_end(slice,row,col);
}


template<typename T>
inline
typename Matrix4d<T>::const_slab_iterator Matrix4d<T>::slab_end(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col) const{
	Array4d<T> const * tp(array_);
	return tp->slab_end(slice,row,col);
}


template<typename T>
inline
typename Matrix4d<T>::reverse_slab_iterator Matrix4d<T>::slab_rbegin(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col){
	return array_->slab_rbegin(slice,row,col);
}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_slab_iterator Matrix4d<T>::slab_rbegin(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col) const{
	Array4d<T> const * tp(array_);
	return tp->slab_rbegin(slice,row,col);
}


template<typename T>
inline
typename Matrix4d<T>::reverse_slab_iterator Matrix4d<T>::slab_rend(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col){
	return array_->slab_rend(slice,row,col);
}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_slab_iterator Matrix4d<T>::slab_rend(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col) const{
	Array4d<T> const * tp(array_);
	return tp->slab_rend(slice,row,col);
}


//-------------------- One dimensional slice iterators----------------------

template<typename T>
inline
typename Matrix4d<T>::slice_iterator
Matrix4d<T>::slice_begin(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type row,
		const typename Matrix4d<T>::size_type col)
		{
	return array_->slice_begin(slab,row,col);
		}

template<typename T>
inline
typename Matrix4d<T>::const_slice_iterator
Matrix4d<T>::slice_begin(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type row,
		const typename Matrix4d<T>::size_type col) const
		{
	Array4d<T> const * tp(array_);
	return tp->slice_begin(slab,row,col);
		}

template<typename T>
inline
typename Matrix4d<T>::slice_iterator
Matrix4d<T>::slice_end(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type row,
		const typename Matrix4d<T>::size_type col)
		{
	return array_->slice_end(slab,row,col);
		}

template<typename T>
inline
typename Matrix4d<T>::const_slice_iterator
Matrix4d<T>::slice_end(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type row,
		const typename Matrix4d<T>::size_type col) const
		{
	Array4d<T> const * tp(array_);
	return tp->slice_end(slab,row,col);
		}

template<typename T>
inline
typename Matrix4d<T>::reverse_slice_iterator
Matrix4d<T>::slice_rbegin(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type row,
		const typename Matrix4d<T>::size_type col)
		{
	return array_->slice_rbegin(slab,row,col);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_slice_iterator
Matrix4d<T>::slice_rbegin(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type row,
		const typename Matrix4d<T>::size_type col) const
		{
	Array4d<T> const * tp(array_);
	return tp->slice_rbegin(slab,row,col);
		}

template<typename T>
inline
typename Matrix4d<T>::reverse_slice_iterator
Matrix4d<T>::slice_rend(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type row,
		const typename Matrix4d<T>::size_type col)
		{
	return array_->slice_rend(slab,row,col);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_slice_iterator
Matrix4d<T>::slice_rend(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type row,
		const typename Matrix4d<T>::size_type col) const
		{
	Array4d<T> const * tp(array_);
	return tp->slice_rend(slab,row,col);
		}

//--------------------One dimensional row iterators----------------------

template<typename T>
inline
typename Matrix4d<T>::row_iterator
Matrix4d<T>::row_begin(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row)
		{
	return array_->row_begin(slab,slice,row);
		}

template<typename T>
inline
typename Matrix4d<T>::const_row_iterator
Matrix4d<T>::row_begin(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row) const
		{
	Array4d<T> const * tp(array_);
	return tp->row_begin(slab,slice,row);
		}

template<typename T>
inline
typename Matrix4d<T>::row_iterator
Matrix4d<T>::row_end(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row)
		{
	return array_->row_end(slab,slice,row);
		}

template<typename T>
inline
typename Matrix4d<T>::const_row_iterator
Matrix4d<T>::row_end(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row) const
		{
	Array4d<T> const * tp(array_);
	return tp->row_end(slab,slice,row);
		}


template<typename T>
inline
typename Matrix4d<T>::reverse_row_iterator
Matrix4d<T>::row_rbegin(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row)
		{
	return array_->row_rbegin(slab,slice,row);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_row_iterator
Matrix4d<T>::row_rbegin(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row) const
		{
	Array4d<T> const * tp(array_);
	return tp->row_rbegin(slab,slice,row);
		}


template<typename T>
inline
typename Matrix4d<T>::reverse_row_iterator
Matrix4d<T>::row_rend(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row)
		{
	return array_->row_rend(slab,slice,row);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_row_iterator
Matrix4d<T>::row_rend(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row) const
		{
	Array4d<T> const * tp(array_);
	return tp->row_rend(slab,slice,row);
		}

//--------------------One dimensional col iterators----------------------

template<typename T>
inline
typename Matrix4d<T>::col_iterator
Matrix4d<T>::col_begin(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type col)
		{
	return array_->col_begin(slab,slice,col);
		}

template<typename T>
inline
typename Matrix4d<T>::const_col_iterator
Matrix4d<T>::col_begin(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type col) const
		{
	Array4d<T> const * tp(array_);
	return tp->col_begin(slab,slice,col);
		}


template<typename T>
inline
typename Matrix4d<T>::col_iterator
Matrix4d<T>::col_end(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type col)
		{
	return array_->col_end(slab,slice,col);
		}

template<typename T>
inline
typename Matrix4d<T>::const_col_iterator
Matrix4d<T>::col_end(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type col) const
		{
	Array4d<T> const * tp(array_);
	return tp->col_end(slab,slice,col);
		}

template<typename T>
inline
typename Matrix4d<T>::reverse_col_iterator
Matrix4d<T>::col_rbegin(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type col)
		{
	return array_->col_rbegin(slab,slice,col);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_col_iterator
Matrix4d<T>::col_rbegin(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type col) const
		{
	Array4d<T> const * tp(array_);
	return tp->col_rbegin(slab,slice,col);
		}


template<typename T>
inline
typename Matrix4d<T>::reverse_col_iterator
Matrix4d<T>::col_rend(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type col)
		{
	return array_->col_rend(slab,slice,col);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_col_iterator
Matrix4d<T>::col_rend(const typename Matrix4d<T>::size_type slab, const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type col) const
		{
	Array4d<T> const * tp(array_);
	return tp->col_rend(slab,slice,col);
		}

//--------------------slab range iterators----------------------

template<typename T>
inline
typename Matrix4d<T>::slab_range_iterator Matrix4d<T>::slab_begin(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range){
	return array_->slab_begin(slice,row,col,range);
}


template<typename T>
inline
typename Matrix4d<T>::slab_range_iterator Matrix4d<T>::slab_end(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range){
	return array_->slab_end(slice,row,col,range);
}


template<typename T>
inline
typename Matrix4d<T>::const_slab_range_iterator Matrix4d<T>::slab_begin(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range) const{
	Array4d<T> const * tp(array_);
	return tp->slab_begin(slice,row,col,range);
}

template<typename T>
inline
typename Matrix4d<T>::const_slab_range_iterator Matrix4d<T>::slab_end(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range) const{
	Array4d<T> const * tp(array_);
	return tp->slab_end(slice,row,col,range);
}

template<typename T>
inline
typename Matrix4d<T>::reverse_slab_range_iterator Matrix4d<T>::slab_rbegin(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range){
	return array_->slab_rbegin(slice,row,col,range);
}


template<typename T>
inline
typename Matrix4d<T>::reverse_slab_range_iterator Matrix4d<T>::slab_rend(const Matrix4d<T>::size_type slice,
		const Matrix4d<T>::size_type row, const Matrix4d<T>::size_type col,
		const slip::Range<int>& range){
	return array_->slab_rend(slice,row,col,range);
}


template<typename T>
inline
typename Matrix4d<T>::const_reverse_slab_range_iterator Matrix4d<T>::slab_rbegin(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range) const{
	Array4d<T> const * tp(array_);
	return tp->slab_rbegin(slice,row,col,range);
}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_slab_range_iterator Matrix4d<T>::slab_rend(const typename Matrix4d<T>::size_type slice,
		const typename Matrix4d<T>::size_type row,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range) const{
	Array4d<T> const * tp(array_);
	return tp->slab_rend(slice,row,col,range);
}


//--------------------Constant slice range iterators----------------------

template<typename T>
inline
typename Matrix4d<T>::slice_range_iterator
Matrix4d<T>::slice_begin(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	return array_->slice_begin(slab,row,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_slice_range_iterator
Matrix4d<T>::slice_begin(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	Array4d<T> const * tp(array_);
	return tp->slice_begin(slab,row,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::slice_range_iterator
Matrix4d<T>::slice_end(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	return array_->slice_end(slab,row,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_slice_range_iterator
Matrix4d<T>::slice_end(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	Array4d<T> const * tp(array_);
	return tp->slice_end(slab,row,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::reverse_slice_range_iterator
Matrix4d<T>::slice_rbegin(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	return array_->slice_rbegin(slab,row,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_slice_range_iterator
Matrix4d<T>::slice_rbegin(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	Array4d<T> const * tp(array_);
	return tp->slice_rbegin(slab,row,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::reverse_slice_range_iterator
Matrix4d<T>::slice_rend(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	return array_->slice_rend(slab,row,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_slice_range_iterator
Matrix4d<T>::slice_rend(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type row, const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	Array4d<T> const * tp(array_);
	return tp->slice_rend(slab,row,col,range);
		}

//--------------------Constant row range iterators----------------------

template<typename T>
inline
typename Matrix4d<T>::row_range_iterator
Matrix4d<T>::row_begin(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice, const typename Matrix4d<T>::size_type row,const slip::Range<int>& range)
		{
	return array_->row_begin(slab,slice,row,range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_row_range_iterator
Matrix4d<T>::row_begin(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type row,
		const slip::Range<int>& range) const
		{
	Array4d<T> const * tp(array_);
	return tp->row_begin(slab,slice,row,range);
		}

template<typename T>
inline
typename Matrix4d<T>::row_range_iterator
Matrix4d<T>::row_end(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type row,
		const slip::Range<int>& range)
		{
	return array_->row_end(slab,slice,row,range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_row_range_iterator
Matrix4d<T>::row_end(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type row,
		const slip::Range<int>& range) const
		{
	Array4d<T> const * tp(array_);
	return tp->row_end(slab,slice,row,range);
		}

template<typename T>
inline
typename Matrix4d<T>::reverse_row_range_iterator
Matrix4d<T>::row_rbegin(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type row,
		const slip::Range<int>& range)
		{
	return array_->row_rbegin(slab,slice,row,range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_row_range_iterator
Matrix4d<T>::row_rbegin(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type row,
		const slip::Range<int>& range) const
		{
	Array4d<T> const * tp(array_);
	return tp->row_rbegin(slab,slice,row,range);
		}

template<typename T>
inline
typename Matrix4d<T>::reverse_row_range_iterator
Matrix4d<T>::row_rend(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type row,
		const slip::Range<int>& range)
		{
	return array_->row_rend(slab,slice,row,range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_row_range_iterator
Matrix4d<T>::row_rend(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type row,
		const slip::Range<int>& range) const
		{
	Array4d<T> const * tp(array_);
	return tp->row_rend(slab,slice,row,range);
		}

//--------------------Constant col range iterators----------------------

template<typename T>
inline
typename Matrix4d<T>::col_range_iterator
Matrix4d<T>::col_begin(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	return array_->col_begin(slab,slice,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_col_range_iterator
Matrix4d<T>::col_begin(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	Array4d<T> const * tp(array_);
	return tp->col_begin(slab,slice,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::col_range_iterator
Matrix4d<T>::col_end(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	return array_->col_end(slab,slice,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_col_range_iterator
Matrix4d<T>::col_end(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	Array4d<T> const * tp(array_);
	return tp->col_end(slab,slice,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::reverse_col_range_iterator
Matrix4d<T>::col_rbegin(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	return array_->col_rbegin(slab,slice,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_col_range_iterator
Matrix4d<T>::col_rbegin(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	Array4d<T> const * tp(array_);
	return tp->col_rbegin(slab,slice,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::reverse_col_range_iterator
Matrix4d<T>::col_rend(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	return array_->col_rend(slab,slice,col,range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_col_range_iterator
Matrix4d<T>::col_rend(const typename Matrix4d<T>::size_type slab,
		const typename Matrix4d<T>::size_type slice,const typename Matrix4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	Array4d<T> const * tp(array_);
	return tp->col_rend(slab,slice,col,range);
		}


//****************************************************************************
//                          Four dimensional iterators
//****************************************************************************

//------------------------ Global iterators------------------------------------

template<typename T>
inline
typename Matrix4d<T>::iterator4d Matrix4d<T>::first_front_upper_left()
{
	return array_->first_front_upper_left();
}

template<typename T>
inline
typename Matrix4d<T>::const_iterator4d Matrix4d<T>::first_front_upper_left() const
{
	Array4d<T> const * tp(array_);
	return tp->first_front_upper_left();
}


template<typename T>
inline
typename Matrix4d<T>::iterator4d Matrix4d<T>::last_back_bottom_right()
{
	return array_->last_back_bottom_right();
}

template<typename T>
inline
typename Matrix4d<T>::const_iterator4d Matrix4d<T>::last_back_bottom_right() const
{
	Array4d<T> const * tp(array_);
	return tp->last_back_bottom_right();
}

template<typename T>
inline
typename Matrix4d<T>::reverse_iterator4d
Matrix4d<T>::rlast_back_bottom_right()
{
	return array_->rlast_back_bottom_right();
}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_iterator4d
Matrix4d<T>::rlast_back_bottom_right() const
{
	Array4d<T> const * tp(array_);
	return tp->rlast_back_bottom_right();
}

template<typename T>
inline
typename Matrix4d<T>::reverse_iterator4d
Matrix4d<T>::rfirst_front_upper_left()
{
	return array_->rfirst_front_upper_left();
}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_iterator4d
Matrix4d<T>::rfirst_front_upper_left() const
{
	Array4d<T> const * tp(array_);
	return tp->rfirst_front_upper_left();
}

//------------------------ Box iterators------------------------------------

template<typename T>
inline
typename Matrix4d<T>::iterator4d Matrix4d<T>::first_front_upper_left(const Box4d<int>& box)
{
	return array_->first_front_upper_left(box);
}

template<typename T>
inline
typename Matrix4d<T>::const_iterator4d Matrix4d<T>::first_front_upper_left(const Box4d<int>& box) const
{
	Array4d<T> const * tp(array_);
	return tp->first_front_upper_left(box);
}


template<typename T>
inline
typename Matrix4d<T>::iterator4d
Matrix4d<T>::last_back_bottom_right(const Box4d<int>& box)
{
	return array_->last_back_bottom_right(box);
}

template<typename T>
inline
typename Matrix4d<T>::const_iterator4d
Matrix4d<T>::last_back_bottom_right(const Box4d<int>& box) const
{
	Array4d<T> const * tp(array_);
	return tp->last_back_bottom_right(box);
}

template<typename T>
inline
typename Matrix4d<T>::reverse_iterator4d
Matrix4d<T>::rlast_back_bottom_right(const Box4d<int>& box)
{
	return array_->rlast_back_bottom_right(box);
}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_iterator4d
Matrix4d<T>::rlast_back_bottom_right(const Box4d<int>& box) const
{
	Array4d<T> const * tp(array_);
	return tp->rlast_back_bottom_right(box);
}

template<typename T>
inline
typename Matrix4d<T>::reverse_iterator4d
Matrix4d<T>::rfirst_front_upper_left(const Box4d<int>& box)
{
	return array_->rfirst_front_upper_left(box);
}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_iterator4d
Matrix4d<T>::rfirst_front_upper_left(const Box4d<int>& box) const
{
	Array4d<T> const * tp(array_);
	return tp->rfirst_front_upper_left(box);
}

//------------------------ Range iterators------------------------------------

template<typename T>
inline
typename Matrix4d<T>::iterator4d_range
Matrix4d<T>::first_front_upper_left(const typename Matrix4d<T>::range& slab_range,
		const typename Matrix4d<T>::range& slice_range,
		const typename Matrix4d<T>::range& row_range,
		const typename Matrix4d<T>::range& col_range)
		{
	return array_->first_front_upper_left(slab_range,slice_range,row_range,col_range);
		}

template<typename T>
inline
typename Matrix4d<T>::iterator4d_range
Matrix4d<T>::last_back_bottom_right(const typename Matrix4d<T>::range& slab_range,
		const typename Matrix4d<T>::range& slice_range,
		const typename Matrix4d<T>::range& row_range,
		const typename Matrix4d<T>::range& col_range)
		{
	return array_->last_back_bottom_right(slab_range,slice_range,row_range,col_range);
		}


template<typename T>
inline
typename Matrix4d<T>::const_iterator4d_range
Matrix4d<T>::first_front_upper_left(const typename Matrix4d<T>::range& slab_range,
		const typename Matrix4d<T>::range& slice_range,
		const typename Matrix4d<T>::range& row_range,
		const typename Matrix4d<T>::range& col_range) const
		{
	Array4d<T> const * tp(array_);
	return tp->first_front_upper_left(slab_range,slice_range,row_range,col_range);
		}


template<typename T>
inline
typename Matrix4d<T>::const_iterator4d_range
Matrix4d<T>::last_back_bottom_right(const typename Matrix4d<T>::range& slab_range,
		const typename Matrix4d<T>::range& slice_range,
		const typename Matrix4d<T>::range& row_range,
		const typename Matrix4d<T>::range& col_range) const
		{
	Array4d<T> const * tp(array_);
	return tp->last_back_bottom_right(slab_range,slice_range,row_range,col_range);
		}

template<typename T>
inline
typename Matrix4d<T>::reverse_iterator4d_range
Matrix4d<T>::rfirst_front_upper_left(const typename Matrix4d<T>::range& slab_range,
		const typename Matrix4d<T>::range& slice_range,
		const typename Matrix4d<T>::range& row_range,
		const typename Matrix4d<T>::range& col_range)
		{
	return array_->rfirst_front_upper_left(slab_range,slice_range,row_range,col_range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_iterator4d_range
Matrix4d<T>::rfirst_front_upper_left(const typename Matrix4d<T>::range& slab_range,
		const typename Matrix4d<T>::range& slice_range,
		const typename Matrix4d<T>::range& row_range,
		const typename Matrix4d<T>::range& col_range) const
		{
	Array4d<T> const * tp(array_);
	return tp->rfirst_front_upper_left(slab_range,slice_range,row_range,col_range);
		}

template<typename T>
inline
typename Matrix4d<T>::reverse_iterator4d_range
Matrix4d<T>::rlast_back_bottom_right(const typename Matrix4d<T>::range& slab_range,
		const typename Matrix4d<T>::range& slice_range,
		const typename Matrix4d<T>::range& row_range,
		const typename Matrix4d<T>::range& col_range)
		{
	return array_->rlast_back_bottom_right(slab_range,slice_range,row_range,col_range);
		}

template<typename T>
inline
typename Matrix4d<T>::const_reverse_iterator4d_range
Matrix4d<T>::rlast_back_bottom_right(const typename Matrix4d<T>::range& slab_range,
		const typename Matrix4d<T>::range& slice_range,
		const typename Matrix4d<T>::range& row_range,
		const typename Matrix4d<T>::range& col_range) const
		{
	Array4d<T> const * tp(array_);
	return tp->rlast_back_bottom_right(slab_range,slice_range,row_range,col_range);
		}


///////////////////////////////////////////

/** \name i/o operators */
/* @{ */
	template <typename T>
inline
std::ostream& operator<<(std::ostream & out, const Matrix4d<T>& a)
{
	out<<*(a.array_);
	return out;
}
/* @} */

////////////////////////////////////////////
	// Elements access operators
	////////////////////////////////////////////
	template<typename T>
	inline
	T***
	Matrix4d<T>::operator[](const typename Matrix4d<T>::size_type l)
	{
		return (*array_)[l];
	}

	template<typename T>
	inline
	const T** const*
	Matrix4d<T>::operator[](const typename Matrix4d<T>::size_type l) const
	{
		Array4d<T> const * tp(array_);
		return tp->operator[](l);
	}

	template<typename T>
	inline
	typename Matrix4d<T>::reference
	Matrix4d<T>::operator()(const typename Matrix4d<T>::size_type l,
			const typename Matrix4d<T>::size_type k,
			const typename Matrix4d<T>::size_type i,
			const typename Matrix4d<T>::size_type j)
	{
		return (*array_)[l][k][i][j];
	}

	template<typename T>
	inline
	typename Matrix4d<T>::const_reference
	Matrix4d<T>::operator()(const typename Matrix4d<T>::size_type l,
			const typename Matrix4d<T>::size_type k,
			const typename Matrix4d<T>::size_type i,
			const typename Matrix4d<T>::size_type j) const
	{
		return (*array_)[l][k][i][j];
	}

	///////////////////////////////////////////

	template<typename T>
	inline
	std::string
	Matrix4d<T>::name() const {return "Matrix4d";}

	template<typename T>
	inline
	typename Matrix4d<T>::size_type
	Matrix4d<T>::dim1() const {return array_->dim1();}

	template<typename T>
	inline
	typename Matrix4d<T>::size_type
	Matrix4d<T>::slabs() const {return array_->dim1();}

	template<typename T>
	inline
	typename Matrix4d<T>::size_type
	Matrix4d<T>::dim2() const {return array_->dim2();}

	template<typename T>
	inline
	typename Matrix4d<T>::size_type
	Matrix4d<T>::slices() const {return array_->dim2();}

	template<typename T>
	inline
	typename Matrix4d<T>::size_type
	Matrix4d<T>::dim3() const {return array_->dim3();}

	template<typename T>
	inline
	typename Matrix4d<T>::size_type
	Matrix4d<T>::rows() const {return array_->dim3();}

	template<typename T>
	inline
	typename Matrix4d<T>::size_type
	Matrix4d<T>::dim4() const {return array_->dim4();;}

	template<typename T>
	inline
	typename Matrix4d<T>::size_type
	Matrix4d<T>::cols() const {return array_->dim4();}

	template<typename T>
	inline
	typename Matrix4d<T>::size_type
	Matrix4d<T>::columns() const {return array_->dim4();;}


	template<typename T>
	inline
	typename Matrix4d<T>::size_type
	Matrix4d<T>::size() const {return array_->size();}

	template<typename T>
	inline
	typename Matrix4d<T>::size_type
	Matrix4d<T>::max_size() const {return array_->max_size();}

	template<typename T>
	inline
	bool Matrix4d<T>::empty()const {return array_->empty();}

	template<typename T>
	inline
	void Matrix4d<T>::swap(Matrix4d<T>& M)
	{
		array_->swap(*(M.array_));
	}


	////////////////////////////////////////////
	// comparison operators
	////////////////////////////////////////////
	/** \name EqualityComparable operators */
	/* @{ */
	template<typename T>
	inline
	bool operator==(const Matrix4d<T>& x,
			const Matrix4d<T>& y)
			{
		return ( x.size() == y.size()
				&& std::equal(x.begin(),x.end(),y.begin()));
			}

	template<typename T>
	inline
	bool operator!=(const Matrix4d<T>& x,
			const Matrix4d<T>& y)
			{
		return !(x == y);
			}
	/* @} */

	/** \name LessThanComparable operators */
	/* @{ */

	template<typename T>
	inline
	bool operator<(const Matrix4d<T>& x,
			const Matrix4d<T>& y)
	{
		return std::lexicographical_compare(x.begin(), x.end(),
				y.begin(), y.end());
	}


	template<typename T>
	inline
	bool operator>(const Matrix4d<T>& x,
			const Matrix4d<T>& y)
	{
		return (y < x);
	}

	template<typename T>
	inline
	bool operator<=(const Matrix4d<T>& x,
			const Matrix4d<T>& y)
			{
		return !(y < x);
			}

	template<typename T>
	inline
	bool operator>=(const Matrix4d<T>& x,
			const Matrix4d<T>& y)
			{
		return !(x < y);
			}
	/* @} */

	////////////////////////////////////////////


	////////////////////////////////////////////
	// arithmetic and mathematic operators
	////////////////////////////////////////////
	template<typename T>
	inline
	Matrix4d<T>& Matrix4d<T>::operator+=(const T& val)
	{
		std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::plus<T>(),val));
		return *this;
	}

	template<typename T>
	inline
	Matrix4d<T>& Matrix4d<T>::operator-=(const T& val)
	{
		std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::minus<T>(),val));
		return *this;
	}

	template<typename T>
	inline
	Matrix4d<T>& Matrix4d<T>::operator*=(const T& val)
	{
		std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::multiplies<T>(),val));
		return *this;
	}

	template<typename T>
	inline
	Matrix4d<T>& Matrix4d<T>::operator/=(const T& val)
	{
		std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::divides<T>(),val));
		return *this;
	}

	template<typename T>
	inline
	Matrix4d<T> Matrix4d<T>::operator-() const
	{
		Matrix4d<T> tmp(*this);
		std::transform(array_->begin(),array_->end(),tmp.begin(),std::negate<T>());
		return tmp;
	}

	template<typename T>
	inline
	Matrix4d<T>& Matrix4d<T>::operator+=(const Matrix4d<T>& rhs)
	{
		assert(this->dim1() == rhs.dim1());
		assert(this->dim2() == rhs.dim2());
		assert(this->dim3() == rhs.dim3());
		assert(this->dim4() == rhs.dim4());
		std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::plus<T>());
		return *this;
	}

	template<typename T>
	inline
	Matrix4d<T>& Matrix4d<T>::operator-=(const Matrix4d<T>& rhs)
	{
		assert(this->dim1() == rhs.dim1());
		assert(this->dim2() == rhs.dim2());
		assert(this->dim3() == rhs.dim3());
		assert(this->dim4() == rhs.dim4());
		std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::minus<T>());
		return *this;
	}

	template<typename T>
	inline
	Matrix4d<T>& Matrix4d<T>::operator*=(const Matrix4d<T>& rhs)
	{
		assert(this->dim1() == rhs.dim1());
		assert(this->dim2() == rhs.dim2());
		assert(this->dim3() == rhs.dim3());
		assert(this->dim4() == rhs.dim4());
		std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::multiplies<T>());
		return *this;
	}

	template<typename T>
	inline
	Matrix4d<T>& Matrix4d<T>::operator/=(const Matrix4d<T>& rhs)
	{
		assert(this->size() == rhs.size());
		std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::divides<T>());
		return *this;
	}



	template<typename T>
	inline
	T& Matrix4d<T>::min() const
	{
		assert(array_->size() != 0);
		return *std::min_element(array_->begin(),array_->end());
	}

	template<typename T>
	inline
	T& Matrix4d<T>::max() const
	{
		assert(array_->size() != 0);
		return *std::max_element(array_->begin(),array_->end());
	}

	template<typename T>
	inline
	T Matrix4d<T>::sum() const
	{
		assert(array_->size() != 0);
		return std::accumulate(array_->begin(),array_->end(),T());
	}


	template<typename T>
	inline
	Matrix4d<T>& Matrix4d<T>::apply(T (*fun)(T))
	{
		slip::apply(this->begin(),this->end(),this->begin(),fun);
		return *this;
	}

	template<typename T>
	inline
	Matrix4d<T>& Matrix4d<T>::apply(T (*fun)(const T&))
	{
		slip::apply(this->begin(),this->end(),this->begin(),fun);
		return *this;
	}


	/** \name Arithmetical operators */
	/* @{ */
		template<typename T>
	inline
	Matrix4d<T> operator+(const Matrix4d<T>& M1,
			const Matrix4d<T>& M2)
		{
			assert(M1.dim1() == M2.dim1());
			assert(M1.dim2() == M2.dim2());
			assert(M1.dim3() == M2.dim3());
			assert(M1.dim4() == M2.dim4());

			Matrix4d<T> tmp(M1.dim1(),M1.dim2(),M1.dim3(),M1.dim4());
			std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::plus<T>());
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> operator+(const Matrix4d<T>& M1,
				const T& val)
		{
			Matrix4d<T> tmp(M1);
			tmp+=val;
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> operator+(const T& val,
				const Matrix4d<T>& M1)
		{
			return M1 + val;
		}


		template<typename T>
		inline
		Matrix4d<T> operator-(const Matrix4d<T>& M1,
				const Matrix4d<T>& M2)
		{
			assert(M1.dim1() == M2.dim1());
			assert(M1.dim2() == M2.dim2());
			assert(M1.dim3() == M2.dim3());
			assert(M1.dim4() == M2.dim4());

			Matrix4d<T> tmp(M1.dim1(),M1.dim2(),M1.dim3(),M1.dim4());
			std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::minus<T>());
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> operator-(const Matrix4d<T>& M1,
				const T& val)
		{
			Matrix4d<T> tmp(M1);
			tmp-=val;
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> operator-(const T& val,
				const Matrix4d<T>& M1)
		{
			return -(M1 - val);
		}

		template<typename T>
		inline
		Matrix4d<T> operator*(const Matrix4d<T>& M1,
				const Matrix4d<T>& M2)
		{
			assert(M1.dim1() == M2.dim1());
			assert(M1.dim2() == M2.dim2());
			assert(M1.dim3() == M2.dim3());
			assert(M1.dim4() == M2.dim4());

			Matrix4d<T> tmp(M1.dim1(),M1.dim2(),M1.dim3(),M1.dim4());
			std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::multiplies<T>());
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> operator*(const Matrix4d<T>& M1,
				const T& val)
		{
			Matrix4d<T> tmp(M1);
			tmp*=val;
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> operator*(const T& val,
				const Matrix4d<T>& M1)
		{
			return M1 * val;
		}

		template<typename T>
		inline
		Matrix4d<T> operator/(const Matrix4d<T>& M1,
				const Matrix4d<T>& M2)
		{
			assert(M1.dim1() == M2.dim1());
			assert(M1.dim2() == M2.dim2());
			assert(M1.dim3() == M2.dim3());
			assert(M1.dim4() == M2.dim4());

			Matrix4d<T> tmp(M1.dim1(),M1.dim2(),M1.dim3(),M1.dim4());
			std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::divides<T>());
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> operator/(const Matrix4d<T>& M1,
				const T& val)
		{
			Matrix4d<T> tmp(M1);
			tmp/=val;
			return tmp;
		}

		/* @} */

		template<typename T>
		inline
		T& min(const Matrix4d<T>& M1)
		{
			return M1.min();
		}

		template<typename T>
		inline
		T& max(const Matrix4d<T>& M1)
		{
			return M1.max();
		}

		template<typename T>
		inline
		Matrix4d<T> abs(const Matrix4d<T>& M)
		{

			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::abs);
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> sqrt(const Matrix4d<T>& M)
		{
			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::sqrt);
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> cos(const Matrix4d<T>& M)
		{
			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::cos);
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> acos(const Matrix4d<T>& M)
		{
			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::acos);
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> sin(const Matrix4d<T>& M)
		{
			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::sin);
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> asin(const Matrix4d<T>& M)
		{
			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::asin);
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> tan(const Matrix4d<T>& M)
		{
			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::tan);
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> atan(const Matrix4d<T>& M)
		{
			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::atan);
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> exp(const Matrix4d<T>& M)
		{
			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::exp);
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> log(const Matrix4d<T>& M)
		{
			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::log);
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> cosh(const Matrix4d<T>& M)
		{
			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::cosh);
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> sinh(const Matrix4d<T>& M)
		{
			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::sinh);
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> tanh(const Matrix4d<T>& M)
		{
			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::tanh);
			return tmp;
		}

		template<typename T>
		inline
		Matrix4d<T> log10(const Matrix4d<T>& M)
		{
			Matrix4d<T> tmp(M.dim1(),M.dim2(),M.dim3(),M.dim4());
			slip::apply(M.begin(),M.end(),tmp.begin(),std::log10);
			return tmp;
		}


}//slip::

#endif //SLIP_MATRIX4D_HPP
