/** 
 * \file ContinuousPolyBase1d.hpp
 * 
 * \brief Provides a class to handle global approximation of 1d container by orthogonal polynomials base.
 * 
 */

#ifndef SLIP_CONTINUOUSPOLYBASE1D_HPP
#define SLIP_CONTINUOUSPOLYBASE1D_HPP

#include <iostream>
#include <string>
#include <vector>
#include "MultivariatePolynomial.hpp"
#include "Polynomial.hpp"
#include "Block.hpp"
#include "Array.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "arithmetic_op.hpp"
#include "linear_algebra.hpp"
#include "linear_algebra_svd.hpp"
#include "PolySupport.hpp"
#include "polynomial_algo.hpp"
#include "poly_weight_functors.hpp"
#include "integration.hpp"
//#include "./../../slipsandboxes/SIC/slip/linear_algebra_svd2.hpp"



namespace slip
{
  /** 
   ** \enum POLY_INTEGRATION_METHOD
   ** \brief Choose between different integration methods 
   ** \code
   enum POLY_INTEGRATION_METHOD
    {
    // Rectanglular integration method
    POLY_INTEGRATION_RECTANGULAR,
    //Trapezium integration method
    POLY_INTEGRATION_TRAPEZIUM,
    //Simpson integration method
    POLY_INTEGRATION_SIMPSON,
    // Newton-Cotes integration rule of order 3
    POLY_INTEGRATION_NEWTON_COTES_3,
    // Newton-Cotes integration rule of order 4
    POLY_INTEGRATION_NEWTON_COTES_4,
    // Newton-Cotes integration rule of order 5
    POLY_INTEGRATION_NEWTON_COTES_5,
    // Newton-Cotes integration rule of order 6
    POLY_INTEGRATION_NEWTON_COTES_6,
    // Newton-Cotes integration rule of order 7
    POLY_INTEGRATION_NEWTON_COTES_7,
    // Newton-Cotes integration rule of order 8
    POLY_INTEGRATION_NEWTON_COTES_8,
    // Newton-Cotes integration rule of order 9
    POLY_INTEGRATION_NEWTON_COTES_9,
    // Newton-Cotes integration rule of order 10
    POLY_INTEGRATION_NEWTON_COTES_10,
    // Exact integration method of Hosny PR 2007
    POLY_INTEGRATION_HOSNY2007,
    // Exact formal integration method 
    POLY_INTEGRATION_EXACT,
    //8 points Alternative Extendes Simpson's Rule (AESR)
    //"On Image Analysis by Moments" Liao & Pawlak PAMI 1996
    POLY_INTEGRATION_AESR_8,
    //13 points Alternative Extendes Simpson's Rule (AESR)
    //"On Image Analysis by Moments" Liao & Pawlak PAMI 1996
    POLY_INTEGRATION_AESR_13,
    //18 points Alternative Extendes Simpson's Rule (AESR)
    //"On Image Analysis by Moments" Liao & Pawlak PAMI 1996
    POLY_INTEGRATION_AESR_18,
    //23 points Alternative Extendes Simpson's Rule (AESR)
    //"On Image Analysis by Moments" Liao & Pawlak PAMI 1996
    POLY_INTEGRATION_AESR_23,
    //3 oversampling multigrid
    slip::POLY_INTEGRATION_MULTIGRID_3
    //5 oversampling multigrid
    slip::POLY_INTEGRATION_MULTIGRID_5
    //7 oversampling multigrid
    slip::POLY_INTEGRATION_MULTIGRID_7
    //9 oversampling multigrid
    slip::POLY_INTEGRATION_MULTIGRID_9
    //10 oversampling multigrid
    slip::POLY_INTEGRATION_MULTIGRID_10
    //11 oversampling multigrid
    slip::POLY_INTEGRATION_MULTIGRID_11
    };
    \endcode
*/
enum POLY_INTEGRATION_METHOD
{
   POLY_INTEGRATION_RECTANGULAR,
   POLY_INTEGRATION_TRAPEZIUM,
   POLY_INTEGRATION_SIMPSON,
   POLY_INTEGRATION_NEWTON_COTES_3,
   POLY_INTEGRATION_NEWTON_COTES_4,
   POLY_INTEGRATION_NEWTON_COTES_5,
   POLY_INTEGRATION_NEWTON_COTES_6,
   POLY_INTEGRATION_NEWTON_COTES_7,
   POLY_INTEGRATION_NEWTON_COTES_8,
   POLY_INTEGRATION_NEWTON_COTES_9,
   POLY_INTEGRATION_NEWTON_COTES_10,
   POLY_INTEGRATION_HOSNY2007,
   POLY_INTEGRATION_EXACT,
   POLY_INTEGRATION_AESR_8,
   POLY_INTEGRATION_AESR_13,
   POLY_INTEGRATION_AESR_18,
   POLY_INTEGRATION_AESR_23,
   POLY_INTEGRATION_MULTIGRID_3,
   POLY_INTEGRATION_MULTIGRID_5,
   POLY_INTEGRATION_MULTIGRID_7,
   POLY_INTEGRATION_MULTIGRID_9,
   POLY_INTEGRATION_MULTIGRID_10,
   POLY_INTEGRATION_MULTIGRID_11
   
};


  enum POLY_BASE_GENERATION_METHOD
    {
      POLY_BASE_GENERATION_FILE,
      POLY_BASE_GENERATION_STIELTJES,
      POLY_BASE_GENERATION_GRAM_SCHMIDT
    };


}//::slip

namespace slip
{


template <typename T, std::size_t DIM>
class ContinuousPolyBase1d;


template <typename T, std::size_t DIM>
std::ostream& operator<<(std::ostream & out, 
			 const ContinuousPolyBase1d<T,DIM>& a);

template<typename T, std::size_t DIM>
class ContinuousPolyBase1d: public std::vector<slip::MultivariatePolynomial<T,DIM> >
{

public:
  typedef ContinuousPolyBase1d<T,DIM> self;
  typedef const self const_self;
  typedef typename std::vector<slip::MultivariatePolynomial<T,DIM> > base;
  typedef typename base::value_type value_type;
   /**
   ** \name Constructors & Destructors
   */
  /*@{*/
    /*!
    ** \brief Default constructors of a ContinuousPolyBase1d.
    */
  ContinuousPolyBase1d():
    base(1),
    dim_(1),
    size_(1),
    degree_(0),
    alpha_(new slip::Array<T>(this->size_)),
    beta_(new slip::Array<T>(this->size_)),
    normalized_(false),
    monic_(false),
    method_(slip::POLY_BASE_GENERATION_GRAM_SCHMIDT),
    range_size_(3),
    discrete_base_(new slip::Matrix<T>(this->range_size_,this->size_)),
    poly_w_evaluations_(new slip::Matrix<T>(this->range_size_,this->size_)),
    pkpk_(new slip::Array<T>(this->size_)),
    support_(slip::PolySupport<T>(-slip::constants<T>::one(),slip::constants<T>::one())),
    integration_method_(slip::POLY_INTEGRATION_EXACT)
  {}


   /*!
    ** \brief Constructs a ContinuousPolyBase1d.
    ** \param dim dimension (associated variable) of the polynomial. For example dim=1 for x or x1, 2 for y or x2...
    ** \param degree highest degree of the monomials of the base
    ** \param normalized true if the base is normalized, false else
    ** \param monic true if the base is monic (coefficient of the monomial of highest degree is 1), false else
    ** \param method Method used to generate the base : 
    ** "gram_schmidt": gram-schmidt orthogonalisation
    ** "stieltjes": stieltjes (three terms recurrence)
    ** "file": read polynomial coefficients from file
    ** \param range_size size of the data
    ** \param support range of the coordinates of the data.
    ** \pre (dim > 0) && (dim <= DIM)
    */
  ContinuousPolyBase1d(const std::size_t dim,
		       const std::size_t degree,
		       const bool normalized,
		       const bool monic,
		       const slip::POLY_BASE_GENERATION_METHOD& method,
		       const std::size_t range_size,
		       const slip::PolySupport<T>& support):
    base(degree+1),
    dim_(dim),
    size_(degree+1),
    degree_(degree),
    alpha_(new slip::Array<T>(this->size_)),
    beta_(new slip::Array<T>(this->size_)),
    normalized_(normalized),
    monic_(monic),
    method_(method),
    range_size_(range_size),
    discrete_base_(new slip::Matrix<T>(this->range_size_,this->size_)),
    poly_w_evaluations_(new slip::Matrix<T>(this->range_size_,this->size_)),
    pkpk_(new slip::Array<T>(this->size_)),
    support_(support),
    integration_method_(slip::POLY_INTEGRATION_EXACT)
  {
    assert((dim > 0) && (dim <= DIM));
  }

 /*!
    ** \brief Constructs a ContinuousPolyBase1d.
    ** \param dim dimension (associated variable) of the polynomial. For example dim=1 for x or x1, 2 for y or x2...
    ** \param degree highest degree of the monomials of the base
    ** \param normalized true if the base is normalized, false else
    ** \param monic true if the base is monic (coefficient of the monomial of highest degree is 1), false else
    ** \param method Method used to generate the base : 
    ** "gram_schmidt": gram-schmidt orthogonalisation
    ** "stieltjes": stieltjes (three terms recurrence)
    ** "file": read Legendre polynomial coefficients from file
    ** \param range_size size of the data
    ** \param support range of the coordinates of the data.
    ** \param integration_method method of integration of polynomial inside samples:
    ** \pre (dim > 0) && (dim <= DIM)
    */
  ContinuousPolyBase1d(const std::size_t dim,
		       const std::size_t degree,
		       const bool normalized,
		       const bool monic,
		       const slip::POLY_BASE_GENERATION_METHOD& method,
		       const std::size_t range_size,
		       const slip::PolySupport<T>& support,
		       const slip::POLY_INTEGRATION_METHOD& integration_method):
    base(degree+1),
    dim_(dim),
    size_(degree+1),
    degree_(degree),
    alpha_(new slip::Array<T>(this->size_)),
    beta_(new slip::Array<T>(this->size_)),
    normalized_(normalized),
    monic_(monic),
    method_(method),
    range_size_(range_size),
    discrete_base_(new slip::Matrix<T>(this->range_size_,this->size_)),
    poly_w_evaluations_(new slip::Matrix<T>(this->range_size_,this->size_)),
    pkpk_(new slip::Array<T>(this->size_)),
    support_(support),
    integration_method_(integration_method)
  {
    assert((dim > 0) && (dim <= DIM));
  }
  
 
  
   /*!
    ** \brief Copy constructor
    ** \param other %ContinuousPolyBase1d.
    */
  ContinuousPolyBase1d(const self& other):
    base(other),
    dim_(other.dim_),
    size_(other.size_),
    degree_(other.degree_),
    alpha_(new slip::Array<T>(*(other.alpha_))),
    beta_(new slip::Array<T>(*(other.beta_))),
    normalized_(other.normalized_),
    monic_(other.monic_),
    method_(other.method_),
    range_size_(other.range_size_),
    discrete_base_(new slip::Matrix<T>(*(other.discrete_base_))),
    poly_w_evaluations_(new slip::Matrix<T>(*(other.poly_w_evaluations_))),
    pkpk_(new slip::Array<T>(*(other.pkpk_))),
    support_(other.support_),
    integration_method_(other.integration_method_)
  {}

   /*!
   ** \brief Destructor 
   */
  virtual ~ContinuousPolyBase1d()
  {
    this->desallocate();
  }

  /*!
  ** \brief operator = 
  ** \param other %ContinuousPolyBase1d.
  */
  self& operator=(const self& other)
  {
     if(this != &other)
       {
	 
	 base::operator=(other);
	 this->copy_attributes(other);
       }
     return *this;
       
  }
  /*@} End Constructors */

 /**
   ** \name Accessors/Mutators
   */
  /*@{*/
  
    /*!
    ** \brief indicate if the base is normalized or not.
    ** \return true if the base is normalized, false else.
    */
  bool is_normalized() const
  {
    return this->normalized_;
  }
   /*!
    ** \brief indicate if the polynomials of the base are monic (coefficient of the monomial of highest degree is 1) or not.
    ** \return true if the the polynomials are monic, false else.
    */
  bool is_monic() const
  {
    return this->monic_;
  }
  /*!
    ** \brief Size or number of polynomials of the base
    ** \return std::size_t 
    */
  std::size_t size() const
  {
    return this->size_;
  }
  /*!
    ** \brief Dimension of the base i.e. associated variable of the polynomial. For example dim=1 for x or x1, 2 for y or x2...
    ** \return std::size_t 
    */
 std::size_t dimension() const
  {
    return this->dim_;
  }

  /*!
    ** \brief Maximal degree of the polynomial of the base
    ** \return std::size_t 
    */
  std::size_t degree() const
  {
    return this->degree_;
  }

  /*!
  ** \brief Method used for generate the polynomial base.
  ** \return slip::POLY_BASE_GENERATION_METHOD
  */
  const slip::POLY_BASE_GENERATION_METHOD& method() const
  {
    return this->method_;
  }

  /*!
  ** \brief Integration method used for computing projection coefficients.
  ** \return slip::POLY_INTEGRATION_METHOD
  */
   const slip::POLY_INTEGRATION_METHOD& integration_method() const
  {
    return this->integration_method_;
  }

  /*!
    ** \brief Range size of the polynomial of the base.
    ** \return std::size_t 
    */
  const std::size_t& range_size() const
  {
    return this->range_size_;
  }

 /*!
 ** \brief Support range of the data
    ** \return slip::PolySupport<T>
    */
  const slip::PolySupport<T>& support() const
  {
    return this->support_;
  }

  /*@} Accessors/Mutators */
 

  /**
   ** \name i/o operators
   */
  /*@{*/
  
  /*!
  ** \brief Write the %ContinuousPolyBase1d to the ouput stream
  ** \param out output std::ostream
  ** \param a %ContinuousPolyBase1d to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const self& a);
  /*@} End i/o operators */

  /*!
  ** \brief Generate the polynomial base.
  */
  virtual void generate() =0;


    /*!
  ** \brief Project data onto the P(i)  polynomial of the base.
  ** \param first RandomAccessIterator on the data.
  ** \param last RandomAccessIterator on the data.
  ** \param i index of the polynomial (i=0,...,degree).
  ** \pre i >= 0 
  ** \pre i <= degree
  */
  template <typename RandomAccessIterator1>
  typename std::iterator_traits<RandomAccessIterator1>::value_type 
  project(RandomAccessIterator1 first,
  	  RandomAccessIterator1 last,
  	  const std::size_t i)
  {
    assert(i >= 0);
    assert(i <= this->degree());
    typedef typename std::iterator_traits<RandomAccessIterator1>::value_type value_type;
  
    value_type result = std::inner_product(first,last,this->poly_w_evaluations_->col_begin(i),value_type());
  
    if(!this->normalized_)
      {
	result /= (*this->pkpk_)[i];
      }
    return result;
  }


 

 /*!
  ** \brief Project data onto all the polynomial of the base.
  ** \param first RandomAccessIterator on the data.
  ** \param last RandomAccessIterator on the data.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \pre (coef_last - coef_first) <= this->size()
  */
  template <typename RandomAccessIterator1,
  	    typename RandomAccessIterator2>
  void project(RandomAccessIterator1 first,
  	       RandomAccessIterator1 last,
  	       RandomAccessIterator2 coef_first,
  	       RandomAccessIterator2 coef_last)
  {
    assert(static_cast<std::size_t>(coef_last - coef_first) <= this->size_);
   
    for(std::size_t i = 0;coef_first!=coef_last; ++i, ++coef_first)
      {
	*coef_first = this->project(first,last,i);
      }
  }

  /*!
  ** \brief Project data onto all the polynomials of the base in the case where polynomials may not be orthogonals (Experimental function).
  ** \param first RandomAccessIterator on the data.
  ** \param last RandomAccessIterator on the data.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \pre (coef_last - coef_first) <= this->size()
  */
  template <typename RandomAccessIterator1,
  	    typename RandomAccessIterator2>
  void project_non_ortho(RandomAccessIterator1 first,
			 RandomAccessIterator1 last,
			 RandomAccessIterator2 coef_first,
			 RandomAccessIterator2 coef_last)
  {
    assert(static_cast<std::size_t>(coef_last - coef_first) <= this->size_);
    slip::Matrix<T> G;
    const std::size_t N = static_cast<std::size_t>(coef_last-coef_first);
    this->orthogonality_matrix(G,N);
    std::cout<<"cond(G) = "<<G.cond()<<std::endl;
    std::cout<<"--G = \n"<<G<<std::endl;
    slip::Vector<T> B(N);
    auto B_first = B.begin();
     for(std::size_t i = 0;B_first!=B.end(); ++i, ++B_first)
       {
      	*B_first = std::inner_product(first,last,this->poly_begin(i),T());
       }
    //this->project(first,last,B.begin(),B.end());
    
    // slip::svd_solve(G,
    // 		    coef_first,coef_last,
    // 		    B.begin(),B.end());
    slip::normalized_svd_solve(G,coef_first,coef_last,
			       B.begin(),B.end());
  }

 /*!
  ** \brief Reconstructs the data from a coefficients range.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \param K number of coefficients to use for recontructing the data.
  ** \param first RandomAccessIterator on the reconstructed data.
  ** \param last RandomAccessIterator on the reconstructed data.
  ** \pre K <= (coef_last - coef_first)
  ** \pre K <= this->size()
  ** \pre (last-first) == this->range_size();
  */
  template <typename RandomAccessIterator1,
 	     typename RandomAccessIterator2>
  void reconstruct(RandomAccessIterator1 coef_first,
 		   RandomAccessIterator1 coef_last,
 		   const std::size_t K,
 		   RandomAccessIterator2 first,
 		   RandomAccessIterator2 last)
  {
    assert(K <= static_cast<std::size_t>(coef_last-coef_first)); 
    assert(K <= this->size());
    assert((last-first) == this->range_size());
    for(std::size_t i = 0; first != last; ++i, ++first)
      {
 	*first = slip::inner_product(coef_first,coef_first+K,
 				     (this->discrete_base_)->row_begin(i));
      }
  }

   /*!
  ** \brief Analytic reconstruction of the data from a coefficients range.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \param K number of coefficients to use for recontruct the data.
  ** \pre K <= (coef_last - coef_first)
  ** \pre K <= this->size()
  ** \return the reconstructed slip::MultivariatePolynomial<T,DIM> polynomial
  */
  template <typename RandomAccessIterator1>
  slip::MultivariatePolynomial<T,DIM> reconstruct(RandomAccessIterator1 coef_first,
						  RandomAccessIterator1 coef_last,
						  const std::size_t K)
  {
    assert(K <= static_cast<std::size_t>(coef_last-coef_first));
    assert(K <= this->size());
    slip::MultivariatePolynomial<T,DIM> init;
    slip::MultivariatePolynomial<T,DIM> result = std::inner_product(coef_first,coef_first+K,this->begin(),init);
  
    return result;
  }
  
 /*!
  ** \brief Normalize the base if it is not normalized
  */
  virtual void normalize()
  {
    if(this->normalized_ == false)
      {
	for(std::size_t k = 0; k <  this->size(); ++k)
	  {
	    (*this)[k] /= std::sqrt((*(this->pkpk_))[k]);
	  }
	std::fill_n(this->pkpk_->begin(),this->size_,slip::constants<T>::one());
      }
     this->normalized_ = true;
  }

 
 /*!
  ** \brief Computes the Gram matrix of the polynomials base.
  ** \param Gram The resulting Gram matrix.
  */
  void orthogonality_matrix(slip::Matrix<T>& Gram)
  {
    // Gram.resize(this->size_,this->size_);
    // std::size_t i = 0;
    // for(std::size_t i = 0; i < this->size_; ++i)
    //   {
    // 	for(std::size_t j = 0; j <= i; ++j)
    // 	  {
    // 	    Gram[i][j] =  this->inner_product(i,j);
    // 	    Gram[j][i] = Gram[i][j];
    // 	    //Gram[j][i] =  this->inner_product(j,i);
    // 	  }
    //   }
    this->orthogonality_matrix(Gram,this->size_);
  }


  /*!
  ** \brief Computes the Gram matrix of the N first polynomials.
  ** \param Gram The resulting Gram matrix.
  ** \param N number of polynomials.
  */
  void orthogonality_matrix(slip::Matrix<T>& Gram, 
			    const std::size_t& N)
  {
    Gram.resize(N,N);
    std::size_t i = 0;
    for(std::size_t i = 0; i < N; ++i)
      {
	for(std::size_t j = 0; j <= i; ++j)
  	  {
	    Gram[i][j] =  this->inner_product(i,j);
  	    Gram[j][i] = Gram[i][j];
	  }
      }
  }
 
  /*!
  ** \brief Indicate if the discretization of the polynomial basis is orthogonal according to the tolerance \a tol.
  ** \param tol Tolerance of the orthogonality .
  ** \result true if orthogonal, false else.
  */
  bool is_orthogonal(const T& tol = slip::epsilon<T>())
  {
    slip::Matrix<T> Gram;
    this->orthogonality_matrix(Gram);
    return slip::is_diagonal(Gram,tol);
  }

/*!
  ** \brief Returns the orthogonality precision of the base.
  ** If orthogonality precision > 1e-1 returns 1e-1.
  ** \return orthogonality precision 
  */
  T orthogonality_precision()
  {
  bool is_ortho = false;
  T eps = slip::epsilon<T>(); 
  slip::Matrix<T> Gram;
  this->orthogonality_matrix(Gram);
  while( (eps < static_cast<T>(1e-1)) && (!is_ortho))
    {
      is_ortho = slip::is_diagonal(Gram,eps);
      eps*=static_cast<T>(10.0);
    }
  if(eps > 1e-1)
    {
      eps = static_cast<T>(1e-1);
    }
  else
    {
      eps /= static_cast<T>(10.0);
    }
  return eps;
  }

   /*!
  ** \brief Computes the Gram matrix of the anlalytic polynomial base.
  ** \param Gram The resulting Gram matrix.
  ** \param innprod Inner product functor.
  */
  template<class InnerProduct>
  void analytic_orthogonality_matrix(slip::Matrix<T>& Gram, 
				     InnerProduct innprod)
  {
     Gram.resize(this->size_,this->size_);
    std::size_t i = 0;
    for(auto it1 = this->begin(); it1 != this->end(); ++it1, ++i)
      {
	std::size_t j = i;
	for(auto it2 = it1; it2 != this->end(); ++it2, ++j)
  	  {
	    Gram[i][j] =  innprod(*it1,*it2);
  	    Gram[j][i] = Gram[i][j];
  	  }
      }
  }


  /*!
  ** \brief Computes the analytic Gram matrix of the polynomial base.
  ** \param Gram The resulting Gram matrix.
  */
  virtual void analytic_orthogonality_matrix(slip::Matrix<T>& Gram) =0;

 
  /*!
  ** \brief Indicate if the polynomial basis is orthogonal according to the tolerance \a tol.
  ** \param innprod Inner product functor.
  ** \param tol Tolerance of the orthogonality .
  ** \result true if orthogonal, false else.
  */
  template <typename InnerProduct>
  bool is_analytically_orthogonal(InnerProduct innprod,
		     const T& tol = slip::epsilon<T>())
  {
     slip::Matrix<T> Gram;
     this->analytic_orthogonality_matrix(Gram,innprod);
    return slip::is_diagonal(Gram,tol);
  }

 
   /*!
  ** \brief Indicate if the polynomial basis is orthogonal according to the tolerance \a tol.
  ** \param tol Tolerance of the orthogonality .
  ** \result True if orthogonal, false else.
  */
  virtual bool is_analytically_orthogonal(const T& tol = slip::epsilon<T>()) = 0;
 
   /*!
  ** \brief Returns the orthogonality precision of the base.
  ** If orthogonality precision > 1e-1 returns 1e-1.
  ** \param innprod Inner product functor.
  ** \return orthogonality precision 
  */
  template <typename InnerProduct>
  T analytic_orthogonality_precision(InnerProduct innprod)
  {
  bool is_ortho = false;
  T eps = slip::epsilon<T>(); 
  slip::Matrix<T> Gram;
  this->analytic_orthogonality_matrix(Gram,innprod);
  while( (eps < static_cast<T>(1e-1)) && (!is_ortho))
    {
      is_ortho = slip::is_diagonal(Gram,eps);
      eps*=static_cast<T>(10.0);
    }
  if(eps > 1e-1)
    {
      eps = static_cast<T>(1e-1);
    }
  else
    {
      eps /= static_cast<T>(10.0);
    }
  return eps;
  }


 /*!
  ** \brief Returns the orthogonality precision of the base.
  ** If orthogonality precision > 1e-1 returns 1e-1.
  ** \return orthogonality precision 
  */
  virtual T analytic_orthogonality_precision() = 0;

/*!
  ** \brief Computes the analytic inner_product between the ith and jth polynomials.
  ** \param i index of the first polynomial.
  ** \param j index of the second polynomial.
  ** \pre (i >=0)
  ** \pre (i <= this->degree())
  ** \pre (j >=0)
  ** \pre (j <= this->degree())
  */
  virtual T analytic_inner_product(const std::size_t i,
				   const std::size_t j) = 0;

  /*!
  ** \brief Computes the inner_product between the ith and jth polynomials.
  ** \param i index of the first polynomial.
  ** \param j index of the second polynomial.
  ** \pre (i >=0)
  ** \pre (i <= this->degree())
  ** \pre (j >=0)
  ** \pre (j <= this->degree())
  */
  virtual T inner_product(const std::size_t i, const std::size_t j) = 0;

  /*!
  ** \brief Computes the Jacobi matrix of the polynomial base.
  ** \param Jacobi The resulting Jacobi matrix.
  */
  template<typename Matrix>
  void jacobi_matrix(Matrix& Jacobi)
  {
    slip::Array<T> sqrt_beta(this->size_-1,
  			     this->beta_->begin() + 1,
  			     this->beta_->end());
    slip::apply(sqrt_beta.begin(),sqrt_beta.end(),sqrt_beta.begin(),std::sqrt);
    Jacobi.resize(this->size_,this->size_,static_cast<T>(0.0));
    slip::set_diagonal(this->alpha_->begin(),this->alpha_->end(),
  			Jacobi);
    slip::set_diagonal(sqrt_beta.begin(),sqrt_beta.end(),
  			Jacobi,
  			1);
    slip::set_diagonal(sqrt_beta.begin(),sqrt_beta.end(),
  			Jacobi,
  			-1);
    
   
  }
  
 
 /*!
  ** \brief First iterator on the alpha monic three terms recurrence coefficients
   ** \return slip::Array<T>::const_iterator
  */
  typename slip::Array<T>::const_iterator
  alpha_cbegin() const
  {
    return alpha_->begin();
    
  }
   /*!
  ** \brief One-past-the-end iterator on the alpha monic three terms recurrence coefficients
   ** \return slip::Array<T>::const_iterator
  */
  typename slip::Array<T>::const_iterator
  alpha_cend() const
  {
    return alpha_->end();
    
  } 
  
 /*!
  ** \brief First iterator on the beta monic three terms recurrence coefficients
   ** \return slip::Array<T>::const_iterator
  */
  typename slip::Array<T>::const_iterator
  beta_cbegin() const
  {
    return beta_->begin();
    
  }
 /*!
  ** \brief One-past-the-end iterator on the beta monic three terms recurrence coefficients
   ** \return slip::Array<T>::const_iterator
  */
  typename slip::Array<T>::const_iterator
  beta_cend() const
  {
    return beta_->end();
    
  }


  /*!
  ** \brief First iterator on the i-th polynomial evaluations on the range.
  ** \param i Index of the polynomial of the base.
  ** \return slip::Matrix<T>::col_iterator 
  */
  typename slip::Matrix<T>::col_iterator 
  poly_begin(const std::size_t i) const

  {
    return (this->discrete_base_)->col_begin(i);
  }

  

  /*!
  ** \brief Past-the-end iterator on the i-th polynomial evaluations on the range.
  ** \param i Index of the polynomial of the base.
  ** \return slip::Matrix<T>::col_iterator 
  */
  typename slip::Matrix<T>::col_iterator 
  poly_end(const std::size_t i) const

  {
    return (this->discrete_base_)->col_end(i);
  }

   /*!
  ** \brief First reverse iterator on the i-th polynomial evaluations on the range.
  ** \param i Index of the polynomial of the base.
  ** \return slip::Matrix<T>::reverse_col_iterator 
  */
  typename slip::Matrix<T>::reverse_col_iterator 
  poly_rbegin(const std::size_t i) const

  {
    return (this->discrete_base_)->col_rbegin(i);
  }

/*!
  ** \brief Past-the-end reverse iterator on the i-th polynomial evaluations on the range.
  ** \param i Index of the polynomial of the base.
  ** \return slip::Matrix<T>::reverse_col_iterator 
  */
  typename slip::Matrix<T>::reverse_col_iterator 
  poly_rend(const std::size_t i) const

  {
    return (this->discrete_base_)->col_rend(i);
  }
  

  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const
  {
    return "ContinuousPolyBase1d";
  }

protected:
  std::size_t dim_;///dimension of the 1d base
  std::size_t size_;///size of the base : number of polynomials
  int degree_;              ///degree of the base
  slip::Array<T>* alpha_; /// three-terms recurrence alpha coefficient
  slip::Array<T>* beta_; /// three-terms recurrence beta coefficient
  bool normalized_; ///boolean indicated if the base is normalized
  bool monic_; ///boolean indicated if the base is monic
  //  std::string method_; ///method used to generate the base
  slip::POLY_BASE_GENERATION_METHOD method_;
  std::size_t range_size_;
  slip::Matrix<T>* discrete_base_;
  slip::Matrix<T>* poly_w_evaluations_;
  slip::Array<T>* pkpk_; /// (pk,pk) inner products
  slip::PolySupport<T> support_;
  slip::POLY_INTEGRATION_METHOD integration_method_;

private:

  /*!
  ** \brief Desallocate the polynomial base data.
  */
  void desallocate()
  {
  
   if(this->alpha_ != nullptr)
       {
	 delete this->alpha_;
       }
  if(this->beta_ != nullptr)
       {
	 delete this->beta_;
       }
   if(this->discrete_base_ != nullptr)
    {
      delete this->discrete_base_;
    }
    if(this->poly_w_evaluations_ != nullptr)
    {
      delete this->poly_w_evaluations_;
    }
    if(this->pkpk_ != nullptr)
    {
      delete this->pkpk_;
    }

  
  }
 
  void copy_attributes(const self& other)
  {
    this->dim_ = other.dim_;
    this->size_ = other.size_;
    this->degree_ = other.degree_;
    if((this->alpha_)->size() != (other.alpha_)->size())
      {
	this->desallocate();
	this->alpha_ = new slip::Array<T>(*(other.alpha_));
	this->beta_  = new slip::Array<T>(*(other.beta_));
	this->discrete_base_  = new slip::Matrix<T>(*(other.discrete_base_));
	this->poly_w_evaluations_  = new slip::Matrix<T>(*(other.poly_w_evaluations_));
	this->pkpk_  = new slip::Array<T>(*(other.pkpk_));
      }
    this->normalized_ = other.normalized_;
    this->monic_ = other.monic_;
    this->method_ =other.method_;
    this->range_size_ = other.range_size_;
    this->support_ = other.support_;
    this->integration_method_ = other.integration_method_;
  }

protected:
  /**
   ** \brief Converts a slip::MultivariatePolynomial<T,DIM> into a slip::Polynomial<T>
   ** \param mpoly slip::MultivariatePolynomial<T,DIM>
   ** \param dim dimension of the multivarite polynomial to use
   ** \param poly slip::Polynomial<T>
   */
  void mpoly_to_poly(const slip::MultivariatePolynomial<T,DIM>& mpoly,
		     const std::size_t dim,
		     slip::Polynomial<T>& poly)
  {

    for(auto it_mpoly = mpoly.begin(); it_mpoly != mpoly.end(); ++it_mpoly)
      {
	poly[(it_mpoly->first).powers[dim-1]] = it_mpoly->second;
      }
  }
};

}//::slip

namespace slip
{
   /** \name input/output operators */
  /* @{ */
  template <typename T, std::size_t DIM>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const ContinuousPolyBase1d<T,DIM>& b)
  {
    out<<"Dimension of the polynomials: "<<b.dimension()<<std::endl;
    out<<"Degree of the base: "<<b.degree()<<std::endl;
    out<<"Size of the base: "<<b.size()<<std::endl;
    if(b.is_normalized())
      {
	out<<"The base is normalized"<<std::endl;
      }
    else
      {
	out<<"The base is not normalized"<<std::endl;
      }
     if(b.is_monic())
      {
	out<<"The base is monic"<<std::endl;
      }
    else
      {
	out<<"The base is not monic"<<std::endl;
      }
     out<<"generation of the base method: ";

     switch(b.method())
       {
       case slip::POLY_BASE_GENERATION_FILE:
	 out<<" coefficients files reading"<<std::endl;
	 break;
       case slip::POLY_BASE_GENERATION_STIELTJES:
	 out<<" 3 terms Stieltjes recurrence"<<std::endl;
	 break;
       case slip::POLY_BASE_GENERATION_GRAM_SCHMIDT:
	 out<<" Gram-Schmidt canonic base integration"<<std::endl;
	 break;
       default:
	 out<<" unknown base generation algorithm"<<std::endl;
	 
       };
     out<<"Analytic base:"<<std::endl;
     std::size_t i = 0;
     for(auto it = b.begin(); it != b.end(); ++it, ++i)
       {
	 out<<"P"<<i<<std::endl;
	 out<<*it<<std::endl;
       }
     out<<"range size = "<<b.range_size()<<std::endl;
     out<<"support = "<<b.support()<<std::endl;
     out<<"integration method: ";
     switch(b.integration_method_)
      {
      case slip::POLY_INTEGRATION_RECTANGULAR:
	out<<" rectangular"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_TRAPEZIUM:
	out<<" trapezium"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_SIMPSON:
	out<<" Simpson"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_3:
	out<<" Newton-Cotes of order 3"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_4:
	out<<" Newton-Cotes of order 4"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_5:
	out<<" Newton-Cotes of order 5"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_6:
	out<<" Newton-Cotes of order 6"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_7:
	out<<" Newton-Cotes of order 7"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_8:
	out<<" Newton-Cotes of order 8"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_9:
	out<<" Newton-Cotes of order 9"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_10:
	out<<" Newton-Cotes of order 10"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_HOSNY2007:
	out<<" exact integration from HOSNY Pattern Recognition 2007"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_EXACT:
	out<<" formal exact integration"<<std::endl;
	break;
	 case slip::POLY_INTEGRATION_AESR_8:
	out<<" 8 points Alternative Extended Simpson's rule"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_AESR_13:
	out<<" 13 points Alternative Extended Simpson's rule"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_AESR_18:
	out<<" 18 points Alternative Extended Simpson's rule"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_AESR_23:
	out<<" 23 points Alternative Extended Simpson's rule"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_3:
	out<<" 3 oversampling multigrid"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_5:
	out<<" 5 oversampling multigrid"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_7:
	out<<" 7 oversampling multigrid"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_9:
	out<<" 9 oversampling multigrid"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_10:
	out<<" 10 oversampling multigrid"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_11:
	out<<" 11 oversampling multigrid"<<std::endl;
	break;
      default:
	out<<" unkown integration method "<<std::endl;
      };
     out<<"discrete base =\n"<<*(b.discrete_base_)<<std::endl;
     out<<"poly_w_evaluations =\n"<<*(b.poly_w_evaluations_)<<std::endl;
     out<<"(Pk,Pk) =\n"<<*(b.pkpk_)<<std::endl;
    return out;
  }
   /* @} */
}//::slip
#endif //SLIP_CONTINUOUSPOLYBASE1D_HPP
