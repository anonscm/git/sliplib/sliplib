/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file ColorImage.hpp
 * 
 * \brief Provides a class to manipulate Color images.
 * 
 */

#ifndef SLIP_COLORIMAGE_HPP
#define SLIP_COLORIMAGE_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <cstddef>
#include <cmath>
#include <string>
#include <stdexcept>

//read/write image with ImageMagick
//breaking the ImageMagick dependency possibility
// Author : denis.arrivault\AT\inria.fr
// Date : 2013/04/05
// \since 1.4.0
#ifdef HAVE_MAGICK
#include <wand/magick_wand.h> 
#include <magick/api.h>
#endif

#include "Matrix.hpp"
#include "stride_iterator.hpp"
#include "kstride_iterator.hpp"
#include "iterator2d_box.hpp"
#include "iterator2d_range.hpp"
#include "apply.hpp"
#include "Color.hpp"
#include "GenericMultiComponent2d.hpp"
#include "io_tools.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/base_object.hpp>


namespace slip
{

template<class T>
class stride_iterator;

template<class T>
class iterator2d_box;

template<class T>
class iterator2d_range;

template<class T>
class const_iterator2d_box;

template<class T>
class const_iterator2d_range;

template <class T>
class DPoint2d;

template <class T>
class Point2d;

template <class T>
class Box2d;

template <typename T>
class ColorImage;



/*! \class ColorImage
** 
** \brief This is a color image class.
** This container statisfies the BidirectionnalContainer concepts of the STL.
** It is also an 2d extension of the RandomAccessContainer concept. That is
** to say the bracket element access is replaced by the double bracket element 
** access.
** It is a specialization of GenericMulitComponent2d using Color blocks.
** Moreover it contains image read/write operations.
** These operations are done using the ImageMagick library.
**  \ingroup Containers Containers2d MultiComponent2dContainers
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/03/14
** \since 1.0.0
** \param T Type of object in the ColorImage 
** \par Axis conventions:
** \image html iterator2d_conventions.jpg "axis and notation conventions"
** \image latex iterator2d_conventions.eps "axis and notation conventions" width=5cm
*/
template <typename T>
class ColorImage:public GenericMultiComponent2d<slip::Color<T> >
{
public :

  typedef slip::Color<T> value_type;
  typedef ColorImage<T> self;
  typedef GenericMultiComponent2d<slip::Color<T> > base;


  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;
  
  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;


  typedef pointer row_iterator;
  typedef const_pointer const_row_iterator;
  typedef slip::stride_iterator<pointer> col_iterator;
  typedef slip::stride_iterator<const_pointer> const_col_iterator;

  typedef std::reverse_iterator<iterator> reverse_row_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
  typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
  typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;

  typedef typename slip::GenericMultiComponent2d<slip::Color<T> >::iterator2d iterator2d;
  typedef typename slip::GenericMultiComponent2d<slip::Color<T> >::const_iterator2d const_iterator2d;

  typedef T color_value_type;
  typedef color_value_type* color_pointer;
  typedef const color_value_type* const_color_pointer;
  typedef color_value_type& color_reference;
  typedef const color_value_type const_color_reference;
  typedef slip::kstride_iterator<color_pointer,3> color_iterator;
  typedef slip::kstride_iterator<const_color_pointer,3> const_color_iterator;

  static const std::size_t DIM = 2;

public:
 /**
  ** \name Constructors & Destructors
  */
 /*@{*/

  /*!
  ** \brief Constructs a %ColorImage.
  */
  ColorImage():
    base()
  {}
 
  /*!
  ** \brief Constructs a %ColorImage.
  ** \param height first dimension of the %ColorImage
  ** \param width second dimension of the %ColorImage
  */
  ColorImage(const size_type height,
	     const size_type width):
    base(height,width)
  {}
  
  /*!
  ** \brief Constructs a %ColorImage initialized by the scalar value \a val.
  ** \param height first dimension of the %ColorImage
  ** \param width second dimension of the %ColorImage
  ** \param val initialization value of the elements 
  */
  ColorImage(const size_type height,
	     const size_type width, 
	     const slip::Color<T>& val):
    base(height,width,val)
  {}
 
  /*!
  ** \brief Constructs a %ColorImage initialized by an array \a val.
  ** \param height first dimension of the %ColorImage
  ** \param width second dimension of the %ColorImage
  ** \param val initialization linear array value of the elements 
  */
  ColorImage(const size_type height,
	     const size_type width, 
	     const T* val):
    base(height,width,val)
  {}
 
  /*!
  ** \brief Constructs a %ColorImage initialized by an array \a val.
  ** \param height first dimension of the %ColorImage
  ** \param width second dimension of the %ColorImage
  ** \param val initialization array value of the elements 
  */
  ColorImage(const size_type height,
		 const size_type width, 
	     const slip::Color<T>* val):
    base(height,width,val)
  {}
 

  /**
  **  \brief  Contructs a %ColorImage from a range.
  ** \param height first dimension of the %ColorImage
  ** \param width second dimension of the %ColorImage
  **  \param  first  An input iterator.
  **  \param  last  An input iterator.
  **
  ** Create a %ColorImage consisting of copies of the elements from
  ** [first,last).
  */
  template<typename InputIterator>
  ColorImage(const size_type height,
	     const size_type width, 
	     InputIterator first,
	     InputIterator last):
    base(height,width,first,last)
  {} 



  
 /**
  **  \brief  Contructs a %ColorImage from a 3 ranges.
  ** \param height first dimension of the %ColorImage
  ** \param width second dimension of the %ColorImage
  **  \param  first1  An input iterator.
  **  \param  last1  An input iterator.
  **  \param  first2  An input iterator.
  **  \param  first3  An input iterator.
 
  **
  ** Create a %ColorImage consisting of copies of the elements from
  ** [first1,last1), 
  ** [first2,first2 + (last1 - first1)), 
  ** [first3, first3 + (last1 - first1).
  */
  template<typename InputIterator>
  ColorImage(const size_type height,
	     const size_type width, 
	     InputIterator first1,
	     InputIterator last1,
	     InputIterator first2,
	     InputIterator first3):
    base(height,width)
  {
    
    std::vector<InputIterator> first_iterators_list(3);
    first_iterators_list[0] = first1;
    first_iterators_list[1] = first2;
    first_iterators_list[2] = first3;
    this->fill(first_iterators_list,last1);
  
  } 


  /*!
  ** \brief Constructs a copy of the %ColorImage \a rhs
  */
  ColorImage(const self& rhs):
    base(rhs)
  {}

  /*!
  ** \brief Destructor of the %ColorImage
  */
  ~ColorImage()
  {}
  

  /*@} End Constructors */


 
 /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;

  /*!
  ** \brief Returns true if all the color plane are the same
  ** 
  */
  bool is_grayscale() const;


  #ifdef HAVE_MAGICK
  /**
   ** \name i/o operators
   */
  /*@{*/
  
 
  /*!
  ** \brief Reads a ColorImage from a file path name
  ** \param file_path_name
  ** 
  ** This method uses the ImageMagick library
  ** ColorImage<float> and ColorImage<double> data value are supposed
  ** to be between [0.0,1.0]
  */ 
  void read(const std::string& file_path_name);

  /*!
  ** \brief Write a ColorImage to a file path name
  ** \param file_path_name
  **
  ** This method uses the ImageMagick library
  ** ColorImage<float> and ColorImage<double> data value are supposed
  ** to be between [0.0,1.0]
  */ 
  void write(const std::string& file_path_name) const;
  /*@} End i/o operators */

  #else

  void read(const std::string& file_path_name)
  {
    throw std::runtime_error("ImageMagick has not been found: no 'read' method available.");
  }

  void write(const std::string& file_path_name) const
  {
    throw std::runtime_error("ImageMagick has not been found: no 'write' method available.");
  }

  #endif

  /**
   ** \name  Assignment operators and methods
   */
  /*@{*/

  

  /*!
  ** \brief Affects all the element of the %ColorImage by val
  ** \param val affectation value
  ** \return reference to corresponding %ColorImage
  */
  self& operator=(const Color<T>& val);


  /*!
  ** \brief Affects all the element of the %ColorImage by val
  ** \param val affectation value
  ** \return reference to corresponding %ColorImage
  */
  self& operator=(const T& val);



  
 
  /*@} End Assignment operators and methods*/



  
 /**
   ** \name  Arithmetic operators
   */
  /*@{*/
  /*!
  ** \brief Add val to each element of the %ColorImage  
  ** \param val value
  ** \return reference to the resulting %ColorImage
  */
  self& operator+=(const T& val);
  self& operator-=(const T& val);
  self& operator*=(const T& val);
  self& operator/=(const T& val);



   self  operator-() const;

/*!
  ** \brief Add val to each element of the %ColorImage  
  ** \param val value
  ** \return reference to the resulting %ColorImage
  */
  self& operator+=(const slip::Color<T>& val);
  self& operator-=(const slip::Color<T>& val);
  self& operator*=(const slip::Color<T>& val);
  self& operator/=(const slip::Color<T>& val);
  

  self& operator+=(const self& rhs);
  self& operator-=(const self& rhs);
  self& operator*=(const self& rhs);
  self& operator/=(const self& rhs);
    

  /*@} End Arithmetic operators */
 
private :
   friend class boost::serialization::access;
 template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::GenericMultiComponent2d<slip::Color<T> > >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::GenericMultiComponent2d<slip::Color<T> > >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
 };

///double alias
  typedef slip::ColorImage<double> ColorImage_d;
  ///float alias
  typedef slip::ColorImage<float> ColorImage_f;
  ///long alias
  typedef slip::ColorImage<long> ColorImage_l;
  ///unsigned long alias
  typedef slip::ColorImage<unsigned long> ColorImage_ul;
  ///short alias
  typedef slip::ColorImage<short> ColorImage_s;
  ///unsigned long alias
  typedef slip::ColorImage<unsigned short> ColorImage_us;
  ///int alias
  typedef slip::ColorImage<int> ColorImage_i;
  ///unsigned int alias
  typedef slip::ColorImage<unsigned int> ColorImage_ui;
  ///char alias
  typedef slip::ColorImage<char> ColorImage_c;
  ///unsigned char alias
  typedef slip::ColorImage<unsigned char> ColorImage_uc;

}//slip::

namespace slip{
  /*!
  ** \brief pointwise addition of two %ColorImage
  ** \param M1 first %ColorImage 
  ** \param M2 seconf %ColorImage
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %ColorImage
  */
template<typename T>
ColorImage<T> operator+(const ColorImage<T>& M1, 
			const ColorImage<T>& M2);

 /*!
  ** \brief addition of a scalar to each element of a %ColorImage
  ** \param M1 the %ColorImage 
  ** \param val the scalar
  ** \return resulting %ColorImage
  */
template<typename T>
ColorImage<T> operator+(const ColorImage<T>& M1, 
		    const T& val);

 /*!
  ** \brief addition of a scalar to each element of a %ColorImage
  ** \param val the scalar
  ** \param M1 the %ColorImage  
  ** \return resulting %ColorImage
  */
  template<typename T>
  ColorImage<T> operator+(const T& val, 
		      const ColorImage<T>& M1);
  
/*!
  ** \brief pointwise substraction of two %ColorImage
  ** \param M1 first %ColorImage 
  ** \param M2 seconf %ColorImage
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %ColorImage
  */
template<typename T>
ColorImage<T> operator-(const ColorImage<T>& M1, 
		    const ColorImage<T>& M2);

 /*!
  ** \brief substraction of a scalar to each element of a %ColorImage
  ** \param M1 the %ColorImage 
  ** \param val the scalar
  ** \return resulting %ColorImage
  */
template<typename T>
ColorImage<T> operator-(const ColorImage<T>& M1, 
		    const T& val);

 /*!
  ** \brief substraction of a scalar to each element of a %ColorImage
  ** \param val the scalar
  ** \param M1 the %ColorImage  
  ** \return resulting %ColorImage
  */
  template<typename T>
  ColorImage<T> operator-(const T& val, 
		      const ColorImage<T>& M1);
  
/*!
  ** \brief pointwise multiplication of two %ColorImage
  ** \param M1 first %ColorImage 
  ** \param M2 seconf %ColorImage
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %ColorImage
  */
  template<typename T>
  ColorImage<T> operator*(const ColorImage<T>& M1, 
		      const ColorImage<T>& M2);

 /*!
  ** \brief multiplication of a scalar to each element of a %ColorImage
  ** \param M1 the %ColorImage 
  ** \param val the scalar
  ** \return resulting %ColorImage
  */
  template<typename T>
  ColorImage<T> operator*(const ColorImage<T>& M1, 
		      const T& val);

 /*!
  ** \brief multiplication of a scalar to each element of a %ColorImage
  ** \param val the scalar
  ** \param M1 the %ColorImage  
  ** \return resulting %ColorImage
  */
  template<typename T>
  ColorImage<T> operator*(const T& val, 
		      const ColorImage<T>& M1);
  
  /*!
  ** \brief pointwise division of two %ColorImage
  ** \param M1 first %ColorImage 
  ** \param M2 seconf %ColorImage
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %ColorImage
  */
  template<typename T>
  ColorImage<T> operator/(const ColorImage<T>& M1, 
		      const ColorImage<T>& M2);

 /*!
  ** \brief division of a scalar to each element of a %ColorImage
  ** \param M1 the %ColorImage 
  ** \param val the scalar
  ** \return resulting %ColorImage
  */
  template<typename T>
  ColorImage<T> operator/(const ColorImage<T>& M1, 
		      const T& val);





}//slip::

namespace slip
{


//   template<typename T>
//   inline
//   ColorImage<T>& ColorImage<T>::operator=(const ColorImage<T> & rhs)
//   {
//     if(this != &rhs)
//       {
// 	*matrix_ = *(rhs.matrix_);
//       }
//     return *this;
//   }

  template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator=(const slip::Color<T>& val)
  {
    std::fill_n(this->begin(),this->size(),val);
    return *this;
  }
  
 
 template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator=(const T& val)
  {
    std::fill_n(this->begin(),this->size(),val);
    return *this;
  }


  
  template<typename T>
  inline
  std::string 
  ColorImage<T>::name() const {return "ColorImage";} 
 
  template<typename T>
  inline
  bool 
  ColorImage<T>::is_grayscale() const 
  {
    bool result = false;
    typename ColorImage<T>::const_iterator it = this->begin();
    std::size_t cpt = 0;
    while(it->is_grayscale() && (it != this->end()))
      {
	cpt++;
	++it;
      }
    if(cpt == this->size())
      {
	result = true;
      }
   
    return result;
  } 

 #ifdef HAVE_MAGICK
  template<>
  inline
  void ColorImage<unsigned char>::read(const std::string& file_path_name)
  {
    slip::read_magick_color(file_path_name,*this,CharPixel);
  }

   template<>
  inline
  void ColorImage<short>::read(const std::string& file_path_name)
  {
    slip::read_magick_color(file_path_name,*this,ShortPixel);
  }

  template<>
  inline
  void ColorImage<int>::read(const std::string& file_path_name)
  {
    slip::read_magick_color(file_path_name,*this,IntegerPixel);
  }

  template<>
  inline
  void ColorImage<long>::read(const std::string& file_path_name)
  {
    slip::read_magick_color(file_path_name,*this,LongPixel);
  }

  template<>
  inline
  void ColorImage<float>::read(const std::string& file_path_name)
  {
    slip::read_magick_color(file_path_name,*this,FloatPixel);
  }

  template<>
  inline
  void ColorImage<double>::read(const std::string& file_path_name)
  {
    slip::read_magick_color(file_path_name,*this,DoublePixel);
  }
  
  

  template<>
  inline
  void ColorImage<unsigned char>::write(const std::string& file_path_name) const
  {
    slip::write_magick_color_char(this->begin(),
				  this->width(),this->height(),
				  file_path_name);
  }

 template<>
  inline
  void ColorImage<short>::write(const std::string& file_path_name) const
  {
    slip::write_magick_color_short(this->begin(),
				   this->width(),this->height(),
				   file_path_name);     
  }

  template<>
  inline
  void ColorImage<int>::write(const std::string& file_path_name) const
  {
    slip::write_magick_color_int(this->begin(),
				 this->width(),this->height(),
				 file_path_name);     
  }

  template<>
  inline
  void ColorImage<long>::write(const std::string& file_path_name) const
  {
    slip::write_magick_color_long(this->begin(),
				  this->width(),this->height(),
				  file_path_name);
  }

  template<>
  inline
  void ColorImage<float>::write(const std::string& file_path_name) const
  {
    slip::write_magick_color_float(this->begin(),
				   this->width(),this->height(),
				   file_path_name);
  }

  template<>
  inline
  void ColorImage<double>::write(const std::string& file_path_name) const
  {
    slip::write_magick_color_double(this->begin(),
				   this->width(),this->height(),
				   file_path_name);
  }
#endif


 template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator+=(const T&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::plus<slip::Color<T> >(),val));
    return *this;
  }
 template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator-=(const T&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::minus<slip::Color<T> >(),val));
    return *this;
  }
template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator*=(const T&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::multiplies<slip::Color<T> >(),val));
    return *this;
  }
 template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator/=(const T&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::divides<slip::Color<T> >(),val));
    return *this;
  }
 template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator+=(const slip::Color<T>&val)
  {
    std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::plus<slip::Color<T> >(),val));
    return *this;
  }
  
  template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator-=(const slip::Color<T>&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::minus<slip::Color<T> >(),val));
    return *this;

  }

  template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator*=(const slip::Color<T>&val)
  {
    std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::multiplies<slip::Color<T> >(),val));
    return *this;
  }
  
  template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator/=(const slip::Color<T>&val)
  {
    std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::divides<slip::Color<T> >(),val));
    return *this;
  }
  
  template<typename T>
  inline
  ColorImage<T> ColorImage<T>::operator-() const
  {
    ColorImage<T> tmp(*this);
    std::transform(this->begin(),this->end(),tmp.begin(),std::negate<slip::Color<T> >());
    return tmp;
  }

  template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator+=(const ColorImage<T>& rhs)
  {
     assert(this->dim1() == rhs.dim1());
     assert(this->dim2() == rhs.dim2());
     std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::plus<slip::Color<T> >());
    return *this;
  }
  
  template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator-=(const ColorImage<T>& rhs)
  {
     assert(this->dim1() == rhs.dim1());
     assert(this->dim2() == rhs.dim2());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::minus<slip::Color<T> >());
    return *this;
  }
  
  template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator*=(const ColorImage<T>& rhs)
  {
     assert(this->dim1() == rhs.dim1());
     assert(this->dim2() == rhs.dim2());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::multiplies<slip::Color<T> >());
    return *this;
  }
  
  template<typename T>
  inline
  ColorImage<T>& ColorImage<T>::operator/=(const ColorImage<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::divides<slip::Color<T> >());
    return *this;
  }
	



 template<typename T>
  inline
  ColorImage<T> operator+(const ColorImage<T>& M1, 
			      const ColorImage<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    ColorImage<T> tmp(M1.dim1(),M1.dim2());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::plus<slip::Color<T> >());
    return tmp;
  }

  template<typename T>
  inline
  ColorImage<T> operator+(const ColorImage<T>& M1, 
			      const slip::Color<T>&val)
  {
    ColorImage<T>  tmp = M1;
    tmp += val;
    return tmp;
  }

  template<typename T>
  inline
  ColorImage<T> operator+(const slip::Color<T>&val,
			      const ColorImage<T>& M1)
  {
    return M1 + val;
  }

   template<typename T>
  inline
  ColorImage<T> operator+(const ColorImage<T>& M1, 
			      const T&val)
  {
    ColorImage<T>  tmp = M1;
    tmp += val;
    return tmp;
  }

  template<typename T>
  inline
  ColorImage<T> operator+(const T&val,
			      const ColorImage<T>& M1)
  {
    return M1 + val;
  }

template<typename T>
inline
ColorImage<T> operator-(const ColorImage<T>& M1, 
			    const ColorImage<T>& M2)
{
  assert(M1.dim1() == M2.dim1());
  assert(M1.dim2() == M2.dim2());
  ColorImage<T> tmp(M1.dim1(),M1.dim2());
  std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::minus<slip::Color<T> >());
  return tmp;
}

template<typename T>
inline
ColorImage<T> operator-(const ColorImage<T>& M1, 
			    const slip::Color<T>&val)
{
  ColorImage<T>  tmp = M1;
  tmp -= val;
  return tmp;
}

template<typename T>
inline
ColorImage<T> operator-(const slip::Color<T>&val,
			    const ColorImage<T>& M1)
{
  return -(M1 - val);
}

template<typename T>
inline
ColorImage<T> operator-(const ColorImage<T>& M1, 
			    const T&val)
{
  ColorImage<T>  tmp = M1;
  tmp -= val;
  return tmp;
}

template<typename T>
inline
ColorImage<T> operator-(const T&val,
			const ColorImage<T>& M1)
{
  return -(M1 - val);
}


template<typename T>
inline
ColorImage<T> operator*(const ColorImage<T>& M1, 
			    const ColorImage<T>& M2)
{
  assert(M1.dim1() == M2.dim1());
  assert(M1.dim2() == M2.dim2());
  ColorImage<T> tmp(M1.dim1(),M1.dim2());
  std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::multiplies<slip::Color<T> >());
  return tmp;
}

template<typename T>
inline
ColorImage<T> operator*(const ColorImage<T>& M1, 
			    const slip::Color<T>&val)
{
  ColorImage<T>  tmp = M1;
  tmp *= val;
  return tmp;
}

template<typename T>
inline
ColorImage<T> operator*(const slip::Color<T>&val,
			    const ColorImage<T>& M1)
{
  return M1 * val;
}

template<typename T>
inline
ColorImage<T> operator*(const ColorImage<T>& M1, 
			    const T&val)
{
  ColorImage<T>  tmp = M1;
  tmp *= val;
  return tmp;
}

template<typename T>
inline
ColorImage<T> operator*(const T&val,
			const ColorImage<T>& M1)
{
  return M1 * val;
}

template<typename T>
inline
ColorImage<T> operator/(const ColorImage<T>& M1, 
			    const ColorImage<T>& M2)
{
  assert(M1.dim1() == M2.dim1());
  assert(M1.dim2() == M2.dim2());
  ColorImage<T> tmp(M1.dim1(),M1.dim2());
  std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::divides<slip::Color<T> >());
  return tmp;
}

template<typename T>
inline
ColorImage<T> operator/(const ColorImage<T>& M1, 
		    const slip::Color<T>&val)
{
  ColorImage<T>  tmp = M1;
  tmp /= val;
  return tmp;
}

template<typename T>
inline
ColorImage<T> operator/(const ColorImage<T>& M1, 
			const T&val)
{
  ColorImage<T>  tmp = M1;
  tmp /= val;
  return tmp;
}








}//slip::

#endif //SLIP_COLORIMAGE_HPP
