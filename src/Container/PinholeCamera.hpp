/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  - HydEE Team http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file PinholeCamera.hpp
 * 
 * \brief Provides a Pinhole camera model class.
 * 
 */

#ifndef SLIP_PINHOLECAMERA_HPP
#define SLIP_PINHOLECAMERA_HPP

#include <iostream>
#include <cassert>
#include "Point2d.hpp"
#include "Point3d.hpp"
#include "Vector3d.hpp"
#include "Matrix.hpp"
#include "io_tools.hpp"
#include "linear_algebra.hpp"
#include "macros.hpp"

#include "CameraModel.hpp"
#include "camera_algo.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/base_object.hpp>


namespace slip {
  template <typename Type>
  class PinholeCamera;
  
  template <typename Type>
  std::ostream& operator<<(std::ostream & out, const PinholeCamera<Type>& c);


  // template <typename Type>
  // struct los;
  
  /*! \class PinholeCamera
  **  \ingroup Containers Camera
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \author Guillaume Gomit <guillaume.gomit_AT_univ-poitiers.fr>
  ** \author Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2013/03/28
  ** \since 1.2.0
  ** \brief Define a %PinholeCamera class
  ** \param Type Type of the coordinates of the PinholeCamera
  */
  template <typename Type>
  class PinholeCamera: public slip::CameraModel<Type> {
    typedef PinholeCamera<Type> self;
    typedef slip::CameraModel<Type> base;
  public:
    /**
     ** \name Constructors & Destructors
     */
    /*@{*/
    
    /*!
    ** \brief Default constructor of PinholeCamera
    */
    PinholeCamera():
      base(),
      calibration_matrix_(new slip::Matrix<Type>(3,4)),
      d_calibration_matrix_(new slip::Matrix<Type>(6,3)),
      origin_(new slip::Point3d<Type>)
    {
    }
    /*!
    ** \brief Constructs a copy of the PinholeCamera \a rhs
    ** \param rhs an other PinholeCamera.
    */
    PinholeCamera(const self& rhs):
      base(rhs),
      calibration_matrix_(new slip::Matrix<Type>(*(rhs.calibration_matrix_))),
      d_calibration_matrix_(new slip::Matrix<Type>(*(rhs.d_calibration_matrix_))),
      origin_(new slip::Point3d<Type>(*(rhs.origin_)))
    {}

    
    /*!
    ** \brief Destructor of the %PinholeCamera
    */
    virtual ~PinholeCamera()
    {
      if(calibration_matrix_ != 0)
	{
	  delete calibration_matrix_;
	}
      if(d_calibration_matrix_ != 0)
	{
	  delete d_calibration_matrix_;
	}
      if(origin_ != 0)
        {
          delete origin_;
        }
    }
     /*!
    ** \brief Clone the %PinholeCamera
    ** \return new pointer towards a copy of this %PinholeCamera
    ** \remark must be detroyed after use
    */
    virtual base* clone () const
    {
      return (new self(*this));
    }   
    /*@} End Constructors */
    
    
    /**
     ** \name i/o operators
     */
    /*@{*/
    /*!
    ** \brief Write the PinholeCamera to the ouput stream
    ** \param out output stream.
    ** \param c PinholeCamera to write to the output stream
    */
    friend std::ostream& operator<< <>(std::ostream & out, const self& c);
    /*@} End i/o  operators */
    
    
    /**
     ** \name  input/output methods
     */
    /*@{*/
    /*!
    ** \brief Read a calibration matrix from an ASCII file
    **
    ** \param file File path name.
    ** \remarks Format:
    ** \f[
    ** \left(
    ** \begin{array}{cccc}
    ** m_{11} & m_{12} & m_{13} & m_{14} \\
    ** m_{21} & m_{22} & m_{23} & m_{24} \\
    ** m_{31} & m_{32} & m_{33} & m_{34} \\
    ** \end{array}
    ** \right)
    ** =
    ** \left(\mathbf{R}|\mathbf{T}\right)\\
    ** \textrm{where}\\
    ** \mathbf{R} =\left(
    ** \begin{array}{ccc}
    ** r_{11} & r_{12} & r_{13}\\
    ** r_{21} & r_{22} & r_{23}\\
    ** r_{31} & r_{32} & r_{33}\\
    ** \end{array}
    ** \right) \\
    ** \f]
    ** is a \f$ 3\times 3\f$ matrix and
    ** \f[
    **  \mathbf{T} =
    ** \left(
    ** \begin{array}{c}
    ** t_1\\
    ** t_2\\
    ** t_3\\
    ** \end{array}
    ** \right)\\
    ** \f]
    ** is a \f$ 3\times 1\f$ vector.
    */
    void read(const std::string& file) 
    {
      slip::read_ascii_2d(*calibration_matrix_,file);
      this->update_d_calibration_matrix();
      this->update_origin();
    }
    
    /*!
    ** \brief Write a calibration matrix to an ASCII file
    **
    ** \param file File path name.
    ** \remarks Format:
    ** \f[
    ** \left(
    ** \begin{array}{cccc}
    ** m_{11} & m_{12} & m_{13} & m_{14} \\
    ** m_{21} & m_{22} & m_{23} & m_{24} \\
    ** m_{31} & m_{32} & m_{33} & m_{34} \\
    ** \end{array} 
    ** \right)
    ** =
    ** \left(\mathbf{R}|\mathbf{T}\right)\\
    ** \f]
    ** where
    ** \f[
    ** \mathbf{R} =\left(
    ** \begin{array}{ccc}
    ** r_{11} & r_{12} & r_{13}\\
    ** r_{21} & r_{22} & r_{23}\\
    ** r_{31} & r_{32} & r_{33}\\
    ** \end{array}
    ** \right) \\
    ** \f]
    ** is a \f$ 3\times 3\f$ matrix and
    ** \f[
    **  \mathbf{T} =
    ** \left(
    ** \begin{array}{c}
    ** t_1\\
    ** t_2\\
    ** t_3\\
    ** \end{array}
    ** \right)\\
    ** \f]
    ** is a \f$ 3\times 1\f$ vector.
    */
    void write(const std::string& file) 
    {
      slip::write_ascii_2d<slip::Matrix<Type>,Type>(*calibration_matrix_,file);
    }
    /*@} End input/output methods*/
    
    /**
     ** \name  Assignment operators and methods
     */
    /*@{*/
    /*!
    ** \brief Assign a PinholeCamera.
    **
    ** Assign elements of PinholeCamera in \a rhs
    **
    ** \param rhs PinholeCamera to get the values from.
    ** \return self.
    */
    self& operator=(const self & rhs)
    {
       if(this != &rhs)
       {
	 this->base::operator=(rhs);
	 if(this->calibration_matrix_ != 0)
	   {
	     delete this->calibration_matrix_;
	   }
	 this->calibration_matrix_ = new slip::Matrix<Type>(*(rhs.calibration_matrix_));
	 if(this->d_calibration_matrix_ != 0)
	   {
	     delete this->d_calibration_matrix_;
	   }
	 this->d_calibration_matrix_ = new slip::Matrix<Type>(*(rhs.d_calibration_matrix_));
	  if(origin_ != 0)
	    {
	      delete origin_;
	    }
          this->origin_ = new slip::Point3d<Type>(*(rhs.origin_));
       }
       return *this;
    }
    /*@} End Assignment operators and methods*/

    /**
     ** \name  Accessor/Mutator operators 
     */
    /*@{*/
    /*!
    ** \brief Get calibration matrix
    */
    slip::Matrix<Type>& calibration_matrix() const
    {
      return *calibration_matrix_;
    }

    /*!
    ** \brief Get back d_calibration matrix
    */
    slip::Matrix<Type>& d_calibration_matrix() const
    {
      return *d_calibration_matrix_;
    }

     /*!
    ** \brief Get back origin
    */
    slip::Point3d<Type>& origin() const
    {
      return *origin_;
    }
    
    /*!
    ** \brief Set the calibration matrix
    ** \param calibration_matrix Calibration matrix.
    */
    void calibration_matrix(const slip::Matrix<Type>& calibration_matrix)
    {
      assert(calibration_matrix.rows() == 3);
      assert(calibration_matrix.cols() == 4);
      *(this->calibration_matrix_) = calibration_matrix;
      this->update_d_calibration_matrix();
      this->update_origin();
    }

    /*!
    ** \brief Set the d_calibration matrix
    ** \param d_calibration_matrix Back-calibration matrix.
    */
    void d_calibration_matrix(const slip::Matrix<Type>& d_calibration_matrix)
    {
      assert(d_calibration_matrix.rows() == 6);
      assert(d_calibration_matrix.cols() == 3);
      *(this->d_calibration_matrix_) = d_calibration_matrix;
    }
    /*!
    ** \brief Set the origin
    ** \param origin Point origin.
    */
    void origin(const slip::Point3d<Type>& origin)
    {
      *(this->origin_) = origin;
    }
   
    /*@} End Accessor/Mutator operators*/

    /**
     ** \name  Projection methods
     */
    /*@{*/
    /*!
    ** \brief Computes the projection of a 3d world point onto the image plane.
    ** \param p %Point3d.
    ** \return %Point2d
    ** \remarks Projection:
    ** \f$(x,y)\f$ are the image coordinates, \f$(X,Y,Z)\f$ are the world coordinates.
    ** \f[
    ** \left( \begin{array}{c}
    ** s_x x \\ s_y y \\ s\\
    ** \end{array} \right)
    ** =
    ** \left( \begin{array}{cccc}
    ** m_{11} & m_{12} & m_{13} & m_{14} \\
    ** m_{21} & m_{22} & m_{23} & m_{24} \\
    ** m_{31} & m_{32} & m_{33} & m_{34} \\
    ** \end{array} \right)
    ** \left( \begin{array}{c}
    ** X \\ Y \\ Z \\ 1\\
    ** \end{array} \right)
    ** \f]
    */
    inline
    slip::Point2d<Type> projection(const slip::Point3d<Type>& p) const
    {
      Type px =  (*calibration_matrix_)[0][0]*p[0]
	+ (*calibration_matrix_)[0][1]*p[1]
	+ (*calibration_matrix_)[0][2]*p[2]
	+ (*calibration_matrix_)[0][3];
      Type py =  (*calibration_matrix_)[1][0]*p[0]
	+ (*calibration_matrix_)[1][1]*p[1]
	+ (*calibration_matrix_)[1][2]*p[2]
	+ (*calibration_matrix_)[1][3];
      const Type s  =  (*calibration_matrix_)[2][0]*p[0]
	+ (*calibration_matrix_)[2][1]*p[1]
	+ (*calibration_matrix_)[2][2]*p[2]
	+ (*calibration_matrix_)[2][3];
      const Type inv_s = slip::constants<Type>::one()/s;
      px*=inv_s;
      py*=inv_s;

      return slip::Point2d<Type>(px,py);
    }
    
   
    
    

  
    /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection of an image point on a Z plane.
    ** \param p %Point2d.
    ** \param z Depth coordinate.
    ** \return %Point3d
    */
    inline
    slip::Point3d<Type> backprojection(const slip::Point2d<Type>& p2, 
				       const Type& z) const
    {


      const Type zero = static_cast<Type>(0.0);

      Type px = 
        (z*(*d_calibration_matrix_)[0][0] - (*d_calibration_matrix_)[4][0]) * p2[0] + 
        (z*(*d_calibration_matrix_)[0][1] - (*d_calibration_matrix_)[4][1]) * p2[1] + 
        (z*(*d_calibration_matrix_)[0][2] - (*d_calibration_matrix_)[4][2]);

      Type py = 
        (z*(*d_calibration_matrix_)[1][0] - (*d_calibration_matrix_)[5][0]) * p2[0] + 
        (z*(*d_calibration_matrix_)[1][1] - (*d_calibration_matrix_)[5][1]) * p2[1] + 
        (z*(*d_calibration_matrix_)[1][2] - (*d_calibration_matrix_)[5][2]);

      Type s =
        ((*d_calibration_matrix_)[2][0]) * p2[0] + 
        ((*d_calibration_matrix_)[2][1]) * p2[1] + 
        ((*d_calibration_matrix_)[2][2]);

      if (s != zero)
        {
          Type inv_s = static_cast<Type>(1)/s;
          px*=inv_s;
          py*=inv_s;
        }

      return  slip::Point3d<Type>(px,py,z);
    }


     /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection of an image point on a X plane.
    ** \param p %Point2d.
    ** \param x Width coordinate.
    ** \return %Point3d
    */
    inline
    virtual slip::Point3d<Type> X_backprojection(const slip::Point2d<Type>& p2, 
						 const Type& x) const {


      const Type zero = static_cast<Type>(0.0);
      
      Type py = 
        (x*(*d_calibration_matrix_)[1][0] + (*d_calibration_matrix_)[3][0]) * p2[0] + 
        (x*(*d_calibration_matrix_)[1][1] + (*d_calibration_matrix_)[3][1]) * p2[1] + 
        (x*(*d_calibration_matrix_)[1][2] + (*d_calibration_matrix_)[3][2]);
      
      Type pz = 
        (x*(*d_calibration_matrix_)[2][0] + (*d_calibration_matrix_)[4][0]) * p2[0] + 
        (x*(*d_calibration_matrix_)[2][1] + (*d_calibration_matrix_)[4][1]) * p2[1] + 
        (x*(*d_calibration_matrix_)[2][2] + (*d_calibration_matrix_)[4][2]);
      
      Type s =
        ((*d_calibration_matrix_)[0][0]) * p2[0] + 
        ((*d_calibration_matrix_)[0][1]) * p2[1] + 
        ((*d_calibration_matrix_)[0][2]);
      
      if (s != zero)
        {
          Type inv_s = static_cast<Type>(1)/s;
          py*=inv_s;
          pz*=inv_s;
        }

      return  slip::Point3d<Type>(x,py,pz);
    }

    /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection of an image point on a Y plane and test if it is in the square.
    ** \param p %Point2d.
    ** \param x Width coordinate.
    ** \param ymin square y minimal value.
    ** \param ymax square y maximal value.
    ** \param zmin square z minimal value.
    ** \param zmax square z maximal value.
    ** \return %Point3d
    */
    inline
    virtual bool X_backprojection_in_square(const slip::Point2d<Type>& p2, 
					    const Type& x,
					    const Type &ymin,
					    const Type &ymax,
					    const Type &zmin,
					    const Type &zmax,
					    slip::Point3d<Type> &p3d) const
    {
      bool result = false;
      Type ux,uy,uz;
      this->computes_u(p2,ux,uy,uz);
      const Type zero = static_cast<Type>(0.0);
      if (ux == zero)
        {
          result= false;
        }
      else
        {
          slip::Point3d<Type> X0 = this->origin();
          const Type alpha = (x-X0[0])/ux;
          p3d[0] = x;
          p3d[1] = X0[1] + alpha*uy;
          p3d[2] = X0[2] + alpha*uz;

          if (p3d[1]>=ymin && p3d[1]<=ymax && p3d[2]>=zmin && p3d[2]<=zmax)
	    {
	      result = true;
	    }
          else
	    {
	      result = false;
	    }
	}
      return result;
    }

    /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection of an image point on a Y plane.
    ** \param p %Point2d.
    ** \param y Heigth coordinate.
    ** \return %Point3d
    */
    inline
    virtual slip::Point3d<Type> Y_backprojection(const slip::Point2d<Type>& p2, 
						 const Type& y) const {


      const Type zero = static_cast<Type>(0.0);
      
      Type px = 
        (y*(*d_calibration_matrix_)[0][0] - (*d_calibration_matrix_)[3][0]) * p2[0] + 
        (y*(*d_calibration_matrix_)[0][1] - (*d_calibration_matrix_)[3][1]) * p2[1] + 
        (y*(*d_calibration_matrix_)[0][2] - (*d_calibration_matrix_)[3][2]);
      
      Type pz = 
        (y*(*d_calibration_matrix_)[2][0] + (*d_calibration_matrix_)[5][0]) * p2[0] + 
        (y*(*d_calibration_matrix_)[2][1] + (*d_calibration_matrix_)[5][1]) * p2[1] + 
        (y*(*d_calibration_matrix_)[2][2] + (*d_calibration_matrix_)[5][2]);
      
      Type s =
        ((*d_calibration_matrix_)[1][0]) * p2[0] + 
        ((*d_calibration_matrix_)[1][1]) * p2[1] + 
        ((*d_calibration_matrix_)[1][2]);
      
      if (s != zero)
        {
          Type inv_s = static_cast<Type>(1)/s;
          px*=inv_s;
          pz*=inv_s;
        }

      return  slip::Point3d<Type>(px,y,pz);
    }

    /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection of an image point on a Y plane and test if it is in the square.
    ** \param p %Point2d.
    ** \param y  coordinate.
    ** \param xmin square x minimal value.
    ** \param xmax square x maximal value.
    ** \param zmin square z minimal value.
    ** \param zmax square z maximal value.
    ** \return %Point3d
    */
    inline
    virtual bool Y_backprojection_in_square(const slip::Point2d<Type>& p2, 
					    const Type& y,
					    const Type &xmin,
					    const Type &xmax,
					    const Type &zmin,
					    const Type &zmax,
					    slip::Point3d<Type> &p3d) const
    {
      bool result = false;
      Type ux,uy,uz;
      this->computes_u(p2,ux,uy,uz);
      const Type zero = static_cast<Type>(0.0);
      if (uy == zero)
        {
          result = false;
        }
      else
        {
          slip::Point3d<Type> X0=this->origin();
          const Type alpha = (y-X0[1])/uy;
          p3d[0] = X0[0] + alpha*ux;
          p3d[1] = y;
          p3d[2] = X0[2] + alpha*uz;

          if (p3d[0]>=xmin && p3d[0]<=xmax && p3d[2]>=zmin && p3d[2]<=zmax)
	    {
	      result = true;
	    }
          else
	    {
	      result = false;
	    }
        }
      return result;
    }

 /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection of an image point on a Z plane.
    ** \param p %Point2d.
    ** \param z Depth coordinate.
    ** \return %Point3d
    */
    inline
    virtual slip::Point3d<Type> Z_backprojection(const slip::Point2d<Type>& p2, 
						 const Type& z) const
    {
      return  this->backprojection(p2,z);
    }

    /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection of an image point on a Z plane and test if it is in the square.
    ** \param p %Point2d.
    ** \param z coordinate.
    ** \param xmin square x minimal value.
    ** \param xmax square x maximal value.
    ** \param ymin square y minimal value.
    ** \param ymax square y maximal value.
    ** \return %Point3d
    */
    inline
    virtual bool Z_backprojection_in_square(const slip::Point2d<Type>& p2, 
					    const Type& z,
					    const Type &xmin,
					    const Type &xmax,
					    const Type &ymin,
					    const Type &ymax,
					    slip::Point3d<Type> &p3d) const
    {
      bool result = false;
      Type ux,uy,uz;
      this->computes_u(p2,ux,uy,uz);
      const Type zero = static_cast<Type>(0.0);
      if (uz == zero)
        {
          result = false;
        }
      else
        {
          slip::Point3d<Type> X0=this->origin();
          const Type alpha = (z-X0[2])/uz;
          p3d[0] = X0[0] + alpha*ux;
          p3d[1] = X0[1] + alpha*uy;
          p3d[2] = z;

          if (p3d[0]>=xmin && p3d[0]<=xmax && p3d[1]>=ymin && p3d[1]<=ymax)
	    {
	      result = true;
	    }
          else
	    {
	      result = false;
	    }
        }
      return result;
    }

    /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection of an image point \f$ p2 = (x,y) \f$ on a plane of equation \f$ \mathbf{n_3}\cdot \mathbf{X} +  a = 0\f$ where \f$n_3 = (N_X,N_Y,N_Z)^T \f$
    ** The backprojection of the image point of coordinates \a p2 onto the plane is given by
    ** \f[
    ** \left(
    ** \begin{array}{c}
    ** sX\\
    ** sY\\
    ** sZ\\
    ** \end{array}
    ** \right)
    ** =
    ** \left(
    ** \begin{array}{c}
    ** -K_0 a-K_3 N_Y-K_4 N_Z\\
    ** -K_1 a+K_3 N_X-K_5 N_Z\\
    ** -K_2 a+K_4 N_X+K_5 N_Y\\
    ** K_0 N_X+K_1 N_Y+K_2 N_Z\\
    ** \end{array}
    ** \right)
    ** =
    ** \left(
    ** \begin{array}{cccc}
    ** 0 & -K_3 & -K_4 & -K_0\\
    ** K_3 & 0 & -K_5 & -K_1\\
    ** K_4 & K_5 & 0 & -K_2\\
    ** K_0 & K_1 & K_2 & 0\\
    ** \end{array}
    ** \right)
    ** \left(
    ** \begin{array}{c}
    ** N_X\\
    ** N_Y\\
    ** N_Z\\
    ** a\\
    ** \end{array}
    ** \right)
    ** \f]
    ** where
    ** \f[
    ** \mathbf{K} = \mathbf{J}
    ** \left(
    ** \begin{array}{c}
    ** x\\
    ** y\\
    ** 1\\
    ** \end{array}
    ** \right)
    ** \f]
    ** where \f$ \mathbf{J}\f$ is the cofactor matrix:
    ** \f[
   ** \mathbf{J} = 
   ** \left(
   ** \begin{array}{ccc}
   ** C_{1122} & C_{0221} & C_{0112}\\
   ** C_{1220} & C_{0022} & C_{0210}\\
   ** C_{1021} & C_{0120} & C_{0011}\\
   ** C_{1223} & C_{0322} & C_{0213}\\
   ** C_{1321} & C_{0123} & C_{0311}\\
   ** C_{1023} & C_{0320} & C_{0013}\\
   ** \end{array}
   ** \right)
   ** \f]
   ** where \f$ C_{ijkl}= m_{ij}m_{kl}-m_{il}m_{kj}\f$
    ** \param p2 %Point2d.
    ** \param n3 %Point3d.
    ** \param a %Type.
    ** \return %Point3d
    */
    inline
    virtual slip::Point3d<Type> plane_backprojection(const slip::Point2d<Type>& p2,
						     const slip::Point3d<Type>& n3,
						     const Type& a) const
    {


      const Type zero = static_cast<Type>(0.0);

      const Type K0=(*d_calibration_matrix_)[0][0]*p2[0]+(*d_calibration_matrix_)[0][1]*p2[1]+(*d_calibration_matrix_)[0][2];
      const Type K1=(*d_calibration_matrix_)[1][0]*p2[0]+(*d_calibration_matrix_)[1][1]*p2[1]+(*d_calibration_matrix_)[1][2];
      const Type K2=(*d_calibration_matrix_)[2][0]*p2[0]+(*d_calibration_matrix_)[2][1]*p2[1]+(*d_calibration_matrix_)[2][2];
      const Type K3=(*d_calibration_matrix_)[3][0]*p2[0]+(*d_calibration_matrix_)[3][1]*p2[1]+(*d_calibration_matrix_)[3][2];
      const Type K4=(*d_calibration_matrix_)[4][0]*p2[0]+(*d_calibration_matrix_)[4][1]*p2[1]+(*d_calibration_matrix_)[4][2];
      const Type K5=(*d_calibration_matrix_)[5][0]*p2[0]+(*d_calibration_matrix_)[5][1]*p2[1]+(*d_calibration_matrix_)[5][2];

      Type px = -K0*a - K3*n3[1] - K4*n3[2];
      Type py = -K1*a + K3*n3[0] - K5*n3[2];
      Type pz = -K2*a + K4*n3[0] + K5*n3[1];
      const Type s  = K0*n3[0] + K1*n3[1] + K2*n3[2];

      if (s != zero)
        {
          const Type inv_s = slip::constants<Type>::one()/s;
          px *= inv_s;
          py *= inv_s;
          pz *= inv_s;
        }

      return  slip::Point3d<Type>(px,py,pz);
    }

 
    /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection of an image point \f$ p2 = (x,y) \f$ on a plane of equation \f$ \mathbf{n_3}\cdot \mathbf{X} +  a = 0\f$ where \f$n_3 = (N_X,N_Y,N_Z)^T \f$
    ** The backprojection of the image point of coordinates \a p2 onto the plane is given by
    ** \f[
    ** \left(
    ** \begin{array}{c}
    ** sX\\
    ** sY\\
    ** sZ\\
    ** \end{array}
    ** \right)
    ** =
    ** \left(
    ** \begin{array}{c}
    ** -K_0 a-K_3 N_Y-K_4 N_Z\\
    ** -K_1 a+K_3 N_X-K_5 N_Z\\
    ** -K_2 a+K_4 N_X+K_5 N_Y\\
    ** K_0 N_X+K_1 N_Y+K_2 N_Z\\
    ** \end{array}
    ** \right)
    ** =
    ** \left(
    ** \begin{array}{cccc}
    ** 0 & -K_3 & -K_4 & -K_0\\
    ** K_3 & 0 & -K_5 & -K_1\\
    ** K_4 & K_5 & 0 & -K_2\\
    ** K_0 & K_1 & K_2 & 0\\
    ** \end{array}
    ** \right)
    ** \left(
    ** \begin{array}{c}
    ** N_X\\
    ** N_Y\\
    ** N_Z\\
    ** a\\
    ** \end{array}
    ** \right)
    ** \f]
    ** where
    ** \f[
    ** \mathbf{K} = \mathbf{J}
    ** \left(
    ** \begin{array}{c}
    ** x\\
    ** y\\
    ** 1\\
    ** \end{array}
    ** \right)
    ** \f]
    ** where \f$ \mathbf{J}\f$ is the cofactor matrix:
    ** \f[
   ** \mathbf{J} = 
   ** \left(
   ** \begin{array}{ccc}
   ** C_{1122} & C_{0221} & C_{0112}\\
   ** C_{1220} & C_{0022} & C_{0210}\\
   ** C_{1021} & C_{0120} & C_{0011}\\
   ** C_{1223} & C_{0322} & C_{0213}\\
   ** C_{1321} & C_{0123} & C_{0311}\\
   ** C_{1023} & C_{0320} & C_{0013}\\
   ** \end{array}
   ** \right)
   ** \f]
   ** where \f$ C_{ijkl}= m_{ij}m_{kl}-m_{il}m_{kj}\f$
    ** \param p2 %Point2d.
    ** \param n3 %Point3d.
    ** \param a %Type.
    ** \param K precomputed K vector.
    ** \return %Point3d
    */
    inline
    virtual slip::Point3d<Type> plane_backprojection(const slip::Point2d<Type>& p2,
						     const slip::Point3d<Type>& n3,
						     const Type& a,
						     const std::vector<Type> &K) const
    {


      const Type zero = static_cast<Type>(0.0);

      Type px = -K[0]*a-K[3]*n3[1]-K[4]*n3[2];
      Type py = -K[1]*a+K[3]*n3[0]-K[5]*n3[2];
      Type pz = -K[2]*a+K[4]*n3[0]+K[5]*n3[1];
      const Type s  = K[0]*n3[0]+K[1]*n3[1]+K[2]*n3[2];

      if (s != zero)
        {
          Type inv_s = static_cast<Type>(1)/s;
          px*=inv_s;
          py*=inv_s;
          pz*=inv_s;
        }

      return  slip::Point3d<Type>(px,py,pz);
    }

    
      /*!
    ** \brief Computes the projection gradient of a 3d world point
    ** onto the image plane.
    ** \param pt %Point3d.
    ** \param delta 
    ** \return %Matrix
    */
    inline
    virtual slip::Matrix<Type> projection_gradient(const slip::Point3d<Type>& pt,
						   const Type& delta = slip::constants<Type>::one()) const
    {
      const Type X0 = pt[0];
      const Type X1 = pt[1];
      const Type X2 = pt[2];
      slip::Matrix<Type> res(2,3,Type());

      const slip::Matrix<Type>& P = this->calibration_matrix();
      const Type Y0 = (P[0][0]*X0) + (P[0][1]*X1) + (P[0][2]*X2) + P[0][3];
      const Type Y1 = (P[1][0]*X0) + (P[1][1]*X1) + (P[1][2]*X2) + P[1][3];
      const Type Y2 = (P[2][0]*X0) + (P[2][1]*X1) + (P[2][2]*X2) + P[2][3];
     
      const Type inv_Y2_square = slip::constants<Type>::one()/(Y2*Y2);
          
      res(0,0) = (P[0][0]*Y2-P[2][0]*Y0)*inv_Y2_square; // dPsi_u/dX
      res(0,1) = (P[0][1]*Y2-P[2][1]*Y0)*inv_Y2_square; // dPsi_u/dY
      res(0,2) = (P[0][2]*Y2-P[2][2]*Y0)*inv_Y2_square; // dPsi_u/dZ
      res(1,0) = (P[1][0]*Y2-P[2][0]*Y1)*inv_Y2_square; // dPsi_v/dX
      res(1,1) = (P[1][1]*Y2-P[2][1]*Y1)*inv_Y2_square; // dPsi_v/dY
      res(1,2) = (P[1][2]*Y2-P[2][2]*Y1)*inv_Y2_square; // dPsi_v/dZ
      return res;
    }
    /*!
    ** \brief Computes the projection gradient of a 3d world point
    ** onto the image plane.
    ** \param p %Point3d.
    ** \return %Matrix
    */
    inline
    virtual slip::Matrix<Type> numerical_projection_gradient(const slip::Point3d<Type>& pt,
							     const Type& delta=slip::constants<Type>::one()) const
    {
      return base::projection_gradient(pt, delta);
    }
    /*@} End projection methods*/
    
    
    
    /**
     ** \name  Computation methods
     */
    /*@{*/
    /*!
    ** \brief Computes the parameters of the pinhole camera model
    ** \param P Matrix containing the input data.
    ** \remarks Format:
    ** \f$(x,y)\f$ are the image coordinates, \f$(X,Y,Z)\f$ are the world coordinates.
    ** \f[
    ** \begin{array}{ccccc}
    ** \cdots & \cdots & \cdots & \cdots & \cdots \\
    ** x_{i} & y_{i} & X_{i} & Y_{i} & Z_{i}  \\
    ** \cdots & \cdots & \cdots & \cdots & \cdots \\
    ** \end{array}
    ** \f]
    */
    virtual void compute(const slip::Matrix<Type>& P) 
    {
      slip::getpars_DLT_norm(P,*calibration_matrix_);
      this->update_d_calibration_matrix();
      this->update_origin();
    }
 
    
    /*@} End computation methods*/
    
    
    /**
     ** \name  Decomposition methods
     */
    /*@{*/
    /*!
    ** \brief Decomposes a pinhole camera matrix
    ** \param K Matrix containing the internal parameters
    ** \param R Matrix containing the external parameters
    ** \param c Vector3d containing the camera centre
    ** \param flag Flag of type DECOMP_TYPE, can be 'RQ' or 'direct'
    ** \return 1 if success an -1 if singular matrix
    ** \pre K.rows()==K.cols()==3
    ** \pre R.rows()==R.cols()==3
    */
    virtual int decompose(slip::Matrix<Type>& K, 
    			  slip::Matrix<Type>& R, 
    			  slip::Vector3d<Type>& c, 
    			  slip::DECOMP_TYPE flag) 
    {
      int sing = 1;
      if(flag == RQ) 
    	{
    	  sing=decompose_RQ(*calibration_matrix_,K,R,c);
    	}
      
      if(flag==direct) 
    	{
    	  decompose_direct(*calibration_matrix_,K,R,c);
    	}
      
      return sing;
    }
    /*@} End decomposition methods*/
    

   
    
    //protected:
    slip::Matrix<Type>* calibration_matrix_;
    slip::Matrix<Type>* d_calibration_matrix_;
    slip::Point3d<Type>* origin_;
    
 private:  
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    { 
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::CameraModel<Type> >(*this);
	  ar & calibration_matrix_;
	  ar & d_calibration_matrix_;
	  ar & origin_;
	}
    }
  template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::CameraModel<Type> >(*this);
	  ar & calibration_matrix_;
	  ar & d_calibration_matrix_;
	  ar & origin_;
	}
    }
   BOOST_SERIALIZATION_SPLIT_MEMBER()

   protected:
   /*!
   ** \brief update the cofactor matrix : 
   ** \f[
   ** \mathbf{J} = 
   ** \left(
   ** \begin{array}{ccc}
   ** C_{1122} & C_{0221} & C_{0112}\\
   ** C_{1220} & C_{0022} & C_{0210}\\
   ** C_{1021} & C_{0120} & C_{0011}\\
   ** C_{1223} & C_{0322} & C_{0213}\\
   ** C_{1321} & C_{0123} & C_{0311}\\
   ** C_{1023} & C_{0320} & C_{0013}\\
   ** \end{array}
   ** \right)
   ** \f]
   ** where \f$ C_{ijkl}= m_{ij}m_{kl}-m_{il}m_{kj}\f$
   ** we have 
   ** \f[
   ** \mathbf{R}^{-1}=
   ** \frac{1}{m_{00}J_{00} +m_{01}J_{10}+m_{02}J_{20}}
   ** \left(
   ** \begin{array}{ccc}
   ** J_{00} & J_{01} & J_{02}\\
   ** J_{10} & J_{11} & J_{12}\\
   ** J_{20} & J_{21} & J_{22}\\
   ** \end{array}
   ** \right)
   ** \f]
   **/
  void update_d_calibration_matrix()
    {
      d_calibration_matrix_->fill(static_cast<Type>(0.0));
      
      (*d_calibration_matrix_)[0][0] = (((*calibration_matrix_)[1][1] * (*calibration_matrix_)[2][2])-((*calibration_matrix_)[1][2] * (*calibration_matrix_)[2][1]));
      (*d_calibration_matrix_)[0][1] = (((*calibration_matrix_)[2][1] * (*calibration_matrix_)[0][2]) - ((*calibration_matrix_)[2][2] * (*calibration_matrix_)[0][1]));
      (*d_calibration_matrix_)[0][2] = (((*calibration_matrix_)[0][1] * (*calibration_matrix_)[1][2]) - ((*calibration_matrix_)[0][2] * (*calibration_matrix_)[1][1]));

      (*d_calibration_matrix_)[1][0] = (((*calibration_matrix_)[1][2] * (*calibration_matrix_)[2][0]) - ((*calibration_matrix_)[1][0] * (*calibration_matrix_)[2][2]));
      (*d_calibration_matrix_)[1][1] = (((*calibration_matrix_)[2][2] * (*calibration_matrix_)[0][0]) - ((*calibration_matrix_)[2][0] * (*calibration_matrix_)[0][2]));
      (*d_calibration_matrix_)[1][2] = (((*calibration_matrix_)[0][2] * (*calibration_matrix_)[1][0]) - ((*calibration_matrix_)[0][0] * (*calibration_matrix_)[1][2]));

      (*d_calibration_matrix_)[2][0] = (((*calibration_matrix_)[1][0] * (*calibration_matrix_)[2][1]) - ((*calibration_matrix_)[1][1] * (*calibration_matrix_)[2][0]));
      (*d_calibration_matrix_)[2][1] = (((*calibration_matrix_)[2][0] * (*calibration_matrix_)[0][1]) - ((*calibration_matrix_)[2][1] * (*calibration_matrix_)[0][0]));
      (*d_calibration_matrix_)[2][2] = (((*calibration_matrix_)[0][0] * (*calibration_matrix_)[1][1]) - ((*calibration_matrix_)[0][1] * (*calibration_matrix_)[1][0]));

      (*d_calibration_matrix_)[3][0] = (((*calibration_matrix_)[1][2] * (*calibration_matrix_)[2][3]) - ((*calibration_matrix_)[1][3] * (*calibration_matrix_)[2][2]));
      (*d_calibration_matrix_)[3][1] = (((*calibration_matrix_)[0][3] * (*calibration_matrix_)[2][2]) - ((*calibration_matrix_)[0][2] * (*calibration_matrix_)[2][3]));
      (*d_calibration_matrix_)[3][2] = (((*calibration_matrix_)[0][2] * (*calibration_matrix_)[1][3]) - ((*calibration_matrix_)[0][3] * (*calibration_matrix_)[1][2]));

      (*d_calibration_matrix_)[4][0] = (((*calibration_matrix_)[1][3] * (*calibration_matrix_)[2][1]) - ((*calibration_matrix_)[1][1] * (*calibration_matrix_)[2][3]));
      (*d_calibration_matrix_)[4][1] = (((*calibration_matrix_)[0][1] * (*calibration_matrix_)[2][3]) - ((*calibration_matrix_)[0][3] * (*calibration_matrix_)[2][1]));
      (*d_calibration_matrix_)[4][2] = (((*calibration_matrix_)[0][3] * (*calibration_matrix_)[1][1]) - ((*calibration_matrix_)[0][1] * (*calibration_matrix_)[1][3]));

      (*d_calibration_matrix_)[5][0] = (((*calibration_matrix_)[1][0] * (*calibration_matrix_)[2][3]) - ((*calibration_matrix_)[1][3] * (*calibration_matrix_)[2][0]));
      (*d_calibration_matrix_)[5][1] = (((*calibration_matrix_)[0][3] * (*calibration_matrix_)[2][0]) - ((*calibration_matrix_)[0][0] * (*calibration_matrix_)[2][3]));
      (*d_calibration_matrix_)[5][2] = (((*calibration_matrix_)[0][0] * (*calibration_matrix_)[1][3]) - ((*calibration_matrix_)[0][3] * (*calibration_matrix_)[1][0]));

    }
    /*!
    ** \brief update origin point (invariant) \f$ \mathbf{X_0} = -\mathbf{R}^{-1}\mathbf{T}\f$ 
    */
    void update_origin()
    {
      (*origin_)[0] = (*d_calibration_matrix_)[0][0] * (*calibration_matrix_)[0][3]
	            + (*d_calibration_matrix_)[0][1] * (*calibration_matrix_)[1][3]
	            + (*d_calibration_matrix_)[0][2] * (*calibration_matrix_)[2][3];
      (*origin_)[1] = (*d_calibration_matrix_)[1][0] * (*calibration_matrix_)[0][3]
	            + (*d_calibration_matrix_)[1][1] * (*calibration_matrix_)[1][3]
	            + (*d_calibration_matrix_)[1][2] * (*calibration_matrix_)[2][3];
      (*origin_)[2] = (*d_calibration_matrix_)[2][0] * (*calibration_matrix_)[0][3]
	            + (*d_calibration_matrix_)[2][1] * (*calibration_matrix_)[1][3]
	            + (*d_calibration_matrix_)[2][2] * (*calibration_matrix_)[2][3];
      Type s = (*d_calibration_matrix_)[0][0] * (*calibration_matrix_)[0][0]
	     + (*d_calibration_matrix_)[1][0] * (*calibration_matrix_)[0][1]
	     + (*d_calibration_matrix_)[2][0] * (*calibration_matrix_)[0][2];
      
      Type invs = -slip::constants<Type>::one()/s;
      (*origin_)[0] *= invs;
      (*origin_)[1] *= invs;
      (*origin_)[2] *= invs;
    }

    /*!
    ** \brief Computes the los direction :
    ** \f[
    ** \mathbf{u}=x\left(
    ** \begin{array}{c}
    ** J_{00}\\ 
    ** J_{10}\\ 
    ** J_{20}\\
    ** \end{array}\right)
    ** + y\left(
    ** \begin{array}{c}
    ** J_{01}\\ 
    ** J_{11}\\
    ** J_{21}\\
    ** \end{array}\right)
    ** + \left(
    ** \begin{array}{c}
    ** J_{02}\\
    ** J_{12}\\ 
    ** J_{22}\\
    ** \end{array}
    ** \right) =
    ** \mathbf{X}-\mathbf{X_0}
    ** \f] 
    ** \param p2 2d point with coordinate (x,y).
    ** \param ux x component of u.
    ** \param uy y component of u.
    ** \param uz z component of u.
    */
    inline
    void computes_u(const slip::Point2d<Type> &p2,
		    Type &ux,
		    Type &uy,
		    Type &uz) const
    {
      ux = (*d_calibration_matrix_)[0][0] * p2[0]
	 + (*d_calibration_matrix_)[0][1] * p2[1]
	 + (*d_calibration_matrix_)[0][2];
      uy = (*d_calibration_matrix_)[1][0] * p2[0]
	 + (*d_calibration_matrix_)[1][1] * p2[1]
	 + (*d_calibration_matrix_)[1][2];
      uz = (*d_calibration_matrix_)[2][0] * p2[0]
	 + (*d_calibration_matrix_)[2][1] * p2[1]
	 + (*d_calibration_matrix_)[2][2];
    }
    
    std::ostream& display(std::ostream& out) const
    {
      out<<*this->calibration_matrix_<<std::endl;
      return out;
    }
  };
  
}//slip::


namespace slip 
{
  template<typename Type>
  std::ostream& operator<<(std::ostream & out, const PinholeCamera<Type>& c) 
  {
    return c.display(out);
  }

  // /*!
  // ** \brief Defines a los function that gives the backprojection
  // ** of the points along the lines of sight for given planes
  // ** \param p %Point2d.
  // ** \param n %Point3d.
  // ** \param a %Type.
  // ** \return %Point3d
  // */
  // template <typename Type>
  // struct los
  // {
  //   Type K0,K1,K2,K3,K4,K5;
  //   const Type zero = static_cast<Type>(0.0);

  //   los(const PinholeCamera<Type> &camera_model_, const slip::Point2d<Type>& p2_):camera_model(camera_model_),p2(p2_)
  //   {
  //     K0=camera_model_.d_calibration_matrix()[0][0]*p2[0]+camera_model_.d_calibration_matrix()[0][1]*p2[1]+camera_model_.d_calibration_matrix()[0][2];
  //     K1=camera_model_.d_calibration_matrix()[1][0]*p2[0]+camera_model_.d_calibration_matrix()[1][1]*p2[1]+camera_model_.d_calibration_matrix()[1][2];
  //     K2=camera_model_.d_calibration_matrix()[2][0]*p2[0]+camera_model_.d_calibration_matrix()[2][1]*p2[1]+camera_model_.d_calibration_matrix()[2][2];
  //     K3=camera_model_.d_calibration_matrix()[3][0]*p2[0]+camera_model_.d_calibration_matrix()[3][1]*p2[1]+camera_model_.d_calibration_matrix()[3][2];
  //     K4=camera_model_.d_calibration_matrix()[4][0]*p2[0]+camera_model_.d_calibration_matrix()[4][1]*p2[1]+camera_model_.d_calibration_matrix()[4][2];
  //     K5=camera_model_.d_calibration_matrix()[5][0]*p2[0]+camera_model_.d_calibration_matrix()[5][1]*p2[1]+camera_model_.d_calibration_matrix()[5][2];
  //   }

  //   slip::Point3d<Type> operator()(const slip::Point3d<Type>& n3,
  // 				   const Type& a) const
  //   {
  //     Type px = -K0*a-K3*n3[1]-K4*n3[2];
  //     Type py = -K1*a+K3*n3[0]-K5*n3[2];
  //     Type pz = -K2*a+K4*n3[0]+K5*n3[1];
  //     Type s  = K0*n3[0]+K1*n3[1]+K2*n3[2];

  //     if (s != zero)
  // 	{
  // 	  Type inv_s = static_cast<Type>(1)/s;
  // 	  px*=inv_s;
  // 	  py*=inv_s;
  // 	  pz*=inv_s;
  // 	}

  //     return  slip::Point3d<Type>(px,py,pz);
  //   }

  //   ~los ()
  //   {}

  //   const PinholeCamera<Type> &camera_model;
  //   const slip::Point2d<Type>& p2;
  // };
  
}//slip::
#endif //SLIP_PINHOLECAMERA_HPP
