/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file Matrix3d.hpp
 * 
 * \brief Provides a class to manipulate Matrix3d.
 * 
 */
#ifndef SLIP_MATRIX3D_HPP
#define SLIP_MATRIX3D_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <algorithm>
#include <cmath>
#include <string>
#include <cstddef>
#include "Array3d.hpp"
#include "stride_iterator.hpp"
#include "apply.hpp"
#include "iterator3d_plane.hpp"
#include "iterator3d_box.hpp"
#include "iterator3d_range.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

  template <typename T>
  class Matrix3d;

  template <typename T>
  class Array3d;

  template <typename T>
  std::ostream& operator<<(std::ostream & out, const slip::Matrix3d<T>& a);

  template<typename T>
  bool operator==(const slip::Matrix3d<T>& x, 
		  const slip::Matrix3d<T>& y);

  template<typename T>
  bool operator!=(const slip::Matrix3d<T>& x, 
		  const slip::Matrix3d<T>& y);

  template<typename T>
  bool operator<(const slip::Matrix3d<T>& x, 
		 const slip::Matrix3d<T>& y);

  template<typename T>
  bool operator>(const slip::Matrix3d<T>& x, 
		 const slip::Matrix3d<T>& y);

  template<typename T>
  bool operator<=(const slip::Matrix3d<T>& x, 
		  const slip::Matrix3d<T>& y);

  template<typename T>
  bool operator>=(const slip::Matrix3d<T>& x, 
		  const slip::Matrix3d<T>& y);

  /*! \class Matrix3d
  **  \ingroup Containers Containers3d MathematicalContainer 
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2014/03/25
  ** \since 1.0.0
  ** \brief Numerical matrix3d class.
  ** This container statisfies the RandomAccessContainer concepts of the STL
  ** except the simple bracket which is replaced by a double bracket. It extends 
  ** the interface of Array3d adding arithmetical: +=, -=, *=, /=,+,-,/,*... 
  ** and mathematical operators : min, max, abs, sqrt, cos, acos, sin, asin, 
  ** tan, atan, exp, log, cosh, sinh, tanh, log10, sum, apply...
  ** \param T Type of object in the Matrix3d 
  **
  ** \par Axis conventions:
  ** \image html iterator3d_conventions.jpg "axis and notation conventions"
  ** \image latex iterator3d_conventions.eps "axis and notation conventions" width=5cm
  **
  */

  template <typename T>
  class Matrix3d
  {
  public :
  
    typedef T value_type;
    typedef Matrix3d<T> self;
    typedef const Matrix3d<T> const_self;

    typedef value_type* pointer;
    typedef value_type const* const_pointer;
    typedef value_type& reference;
    typedef value_type const& const_reference;

    typedef ptrdiff_t difference_type;
    typedef std::size_t size_type;

    typedef pointer iterator;
    typedef const_pointer const_iterator;
  
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    //slice, row and col iterator
    typedef slip::stride_iterator<pointer> slice_iterator;
    typedef slip::stride_iterator<const_pointer> const_slice_iterator;
    typedef pointer row_iterator;
    typedef const_pointer const_row_iterator;
    typedef slip::stride_iterator<pointer> col_iterator;
    typedef slip::stride_iterator<const_pointer> const_col_iterator;
    typedef slip::stride_iterator<slice_iterator> slice_range_iterator;
    typedef slip::stride_iterator<const_slice_iterator> const_slice_range_iterator;
    typedef slip::stride_iterator<pointer> row_range_iterator;
    typedef slip::stride_iterator<const_pointer> const_row_range_iterator;
    typedef slip::stride_iterator<col_iterator> col_range_iterator;
    typedef slip::stride_iterator<const_col_iterator> const_col_range_iterator;
  
    typedef std::reverse_iterator<slice_iterator> reverse_slice_iterator;
    typedef std::reverse_iterator<const_slice_iterator> const_reverse_slice_iterator;
    typedef std::reverse_iterator<iterator> reverse_row_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
    typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
    typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;
    typedef std::reverse_iterator<slice_range_iterator> reverse_slice_range_iterator;
    typedef std::reverse_iterator<const_slice_range_iterator> const_reverse_slice_range_iterator;
    typedef std::reverse_iterator<row_range_iterator> reverse_row_range_iterator;
    typedef std::reverse_iterator<const_row_range_iterator> const_reverse_row_range_iterator;
    typedef std::reverse_iterator<col_range_iterator> reverse_col_range_iterator;
    typedef std::reverse_iterator<const_col_range_iterator> const_reverse_col_range_iterator;
  
#ifdef ALL_PLANE_ITERATOR3D
    //generic 1d iterator
    typedef slip::stride_iterator<pointer> iterator1d;
    typedef slip::stride_iterator<const_pointer> const_iterator1d;
    typedef std::reverse_iterator<iterator1d> reverse_iterator1d;
    typedef std::reverse_iterator<const_iterator1d> const_reverse_iterator1d;
#endif //ALL_PLANE_ITERATOR3D

    //iterator 2d
    typedef typename slip::Array3d<T>::iterator2d iterator2d;
    typedef typename slip::Array3d<T>::const_iterator2d const_iterator2d;
    typedef std::reverse_iterator<iterator2d> reverse_iterator2d;
    typedef std::reverse_iterator<const_iterator2d> const_reverse_iterator2d;

    //iterator 3d
    typedef typename slip::Array3d<T>::iterator3d iterator3d;
    typedef typename slip::Array3d<T>::const_iterator3d const_iterator3d;
    typedef typename slip::Array3d<T>::iterator3d_range iterator3d_range;
    typedef typename slip::Array3d<T>::const_iterator3d_range const_iterator3d_range;
  
    typedef std::reverse_iterator<iterator3d> reverse_iterator3d;
    typedef std::reverse_iterator<const_iterator3d> const_reverse_iterator3d;
    typedef std::reverse_iterator<iterator3d_range> reverse_iterator3d_range;
    typedef std::reverse_iterator<const_iterator3d_range> const_reverse_iterator3d_range;
    
    //default iterator of the container
    typedef iterator3d default_iterator;
    typedef const_iterator3d const_default_iterator;
   
    static const std::size_t DIM = 3;
  
  public:
  
    /**
     ** \name Constructors & Destructors
     */

    /*@{*/
  
    /*!
    ** \brief Constructs a %Matrix3d.
    */
    Matrix3d();
  
    /*!
    ** \brief Constructs a %Matrix3d.
    ** \param d1 first dimension of the %Matrix3d
    ** \param d2 second dimension of the %Matrix3d
    ** \param d3 third dimension of the %Matrix3d
    ** 
    ** \par The %Matrix3d is initialized by the default value of T. 
    */
    Matrix3d(const std::size_t d1,
	     const std::size_t d2,
	     const std::size_t d3);
  
    /*!
    ** \brief Constructs a %Matrix3d initialized by the scalar value \a val.
    ** \param d1 first dimension of the %Matrix3d
    ** \param d2 second dimension of the %Matrix3d
    ** \param d3 third dimension of the %Matrix3d
    ** \param val initialization value of the elements 
    */
    Matrix3d(const std::size_t d1,
	     const std::size_t d2,
	     const std::size_t d3,
	     const T& val);
    /*!
    ** \brief Constructs a %Matrix3d initialized by an array \a val.
    ** \param d1 first dimension of the %Matrix3d
    ** \param d2 second dimension of the %Matrix3d
    ** \param d3 third dimension of the %Matrix3d
    ** \param val initialization array value of the elements 
    */  
    Matrix3d(const std::size_t d1,
	     const std::size_t d2,
	     const std::size_t d3,
	     const T* val);
  
    /**
     **  \brief  Contructs a %Matrix3d from a range.
     ** \param d1 first dimension of the %Matrix3d
     ** \param d2 second dimension of the %Matrix3d
     ** \param d3 third dimension of the %Matrix3d
   
     **  \param  first  An input iterator.
     **  \param  last  An input iterator.
     **
     ** Create a %Matrix3d consisting of copies of the elements from
     ** [first,last).
     */
    template<typename InputIterator>
    Matrix3d(const size_type d1,
	     const size_type d2,
	     const size_type d3,
	     InputIterator first,
	     InputIterator last):
      array_(new slip::Array3d<T>(d1,d2,d3,first,last))
    {}
  
    /*!
    ** \brief Constructs a copy of the Matrix3d \a rhs
    */
    Matrix3d(const Matrix3d<T>& rhs);
  
  
    /*!
    ** \brief Destructor of the Matrix3d
    */
    ~Matrix3d();
 

    /*@} End Constructors */

 
    /*!
    ** \brief Resizes a %Matrix3d.
    ** \param d1 new first dimension
    ** \param d2 new second dimension
    ** \param d3 new third dimension
 
    ** \param val new value for all the elements
    */ 
    void resize(std::size_t d1,
		std::size_t d2,
		std::size_t d3,
		const T& val = T()); 
  
    /**
     ** \name One dimensionnal global iterators
     */
    /*@{*/

    //****************************************************************************
    //                          One dimensionnal iterators
    //****************************************************************************



    //----------------------Global iterators------------------------------

    /*!
    **  \brief Returns a read-only (constant) iterator that points to the
    **  first element in the %Matrix3d.  Iteration is done in ordinary
    **  element order.
    ** \return const begin iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> const A1(10,9,5);
    **  slip::Matrix3d<double> const A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.begin(),A1.end(),
    **                A2.begin(),S.begin(),
    **                std::plus<double>());
    ** \endcode
    */
    const_iterator begin() const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element in the %Matrix3d.  Iteration is done in ordinary
    **  element order.
    ** \return begin iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> A1(10,9,5);
    **  slip::Matrix3d<double> A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.begin(),A1.end(),
    **                A2.begin(),S.begin(),
    **                std::plus<double>());
    ** \endcode
    */
    iterator begin();

    /*!
    **  \brief Returns a read/write iterator that points one past the last
    **  element in the %Matrix3d.  Iteration is done in ordinary
    **  element order.
    ** \return end iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> A1(10,9,5);
    **  slip::Matrix3d<double> A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.begin(),A1.end(),
    **                A2.begin(),S.begin(),
    **                std::plus<double>());
    ** \endcode
    */
     iterator end();
 
    /*!
    **  \brief Returns a read-only (constant) iterator that points one past
    **  the last element in the %Matrix3d.  Iteration is done in
    **  ordinary element order.
    ** \return const end iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> const A1(10,9,5);
    **  slip::Matrix3d<double> const A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.begin(),A1.end(),
    **                A2.begin(),S.begin(),
    **                std::plus<double>());
    ** \endcode
    */
    const_iterator end() const;
  
    /*!
    **  \brief Returns a read/write reverse iterator that points to the
    **  last element in the %Matrix3d. Iteration is done in reverse
    **  element order.
    ** \return reverse begin iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> A1(10,9,5);
    **  slip::Matrix3d<double> A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.rbegin(),A1.rend(),
    **                A2.rbegin(),S.rbegin(),
    **                std::plus<double>());
    ** \endcode
    */
    reverse_iterator rbegin();
  
    /*!
    **  \brief Returns a read-only (constant) reverse iterator that points
    **  to the last element in the %Matrix3d.  Iteration is done in
    **  reverse element order.
    ** \return const reverse begin iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> const A1(10,9,5);
    **  slip::Matrix3d<double> const A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.rbegin(),A1.rend(),
    **                A2.rbegin(),S.rbegin(),
    **                std::plus<double>());
    ** \endcode
    */
    const_reverse_iterator rbegin() const;

    /*!
    **  \brief Returns a read/write reverse iterator that points to one
    **  before the first element in the %Matrix3d.  Iteration is done
    **  in reverse element order.
    **  \return reverse end iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> A1(10,9,5);
    **  slip::Matrix3d<double> A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.rbegin(),A1.rend(),
    **                A2.rbegin(),S.rbegin(),
    **                std::plus<double>());
    ** \endcode
    */
    reverse_iterator rend();

    /*!
    **  \brief Returns a read-only (constant) reverse iterator that points
    **  to one before the first element in the %Matrix3d.  Iteration
    **  is done in reverse element order.
    **  \return const reverse end iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> const A1(10,9,5);
    **  slip::Matrix3d<double> const A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.rbegin(),A1.rend(),
    **                A2.rbegin(),S.rbegin(),
    **                std::plus<double>());
    ** \endcode
    */
    const_reverse_iterator rend() const;

    /*@} End One dimensionnal global iterators */
    /**
     ** \name One dimensionnal slice iterators
     */
    /*@{*/
    //--------------------One dimensionnal slice iterators----------------------
 
  
    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the line (row,col) threw the slices in the %Matrix3d.  
    **	Iteration is done in ordinary element order (increasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    ** 	\return slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Matrix3d<double> A1(10,9,5);
    **  slip::Matrix3d<double> A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 in the first slice of S
    **  std::transform(A1.begin_slice(0),A1.end_slice(0),
    **                A2.begin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    slice_iterator slice_begin(const size_type row, const size_type col);

    /*!
    **  \brief Returns a read-only (constant) iterator that points to the first
    **  element of the line (row,col) threw the slices in the %Matrix3d.  
    **	Iteration is done in ordinary element order (increasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return const_slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Matrix3d<double> const A1(10,9,5);
    **  slip::Matrix3d<double> const A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 in the first slice of S
    **  std::transform(A1.begin_slice(0),A1.end_slice(0),
    **                A2.begin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    const_slice_iterator slice_begin(const size_type row, const size_type col) const;
 
    /*!
    **  \brief Returns a read/write iterator that points to the one past the end
    **  element of the line (row,col) threw the slices in the %Matrix3d.  
    **	Iteration is done in ordinary element order (increasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Matrix3d<double> A1(10,9,5);
    **  slip::Matrix3d<double> A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 in the first slice of S
    **  std::transform(A1.begin_slice(0),A1.end_slice(0),
    **                A2.begin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    slice_iterator slice_end(const size_type row, const size_type col);
 
    /*!
    **  \brief Returns a read-only (constant) iterator that points to the one past the end
    **  element of the line (row,col) threw the slices in the %Matrix3d.  
    **	Iteration is done in ordinary element order (increasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return const_slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Matrix3d<double> const A1(10,9,5);
    **  slip::Matrix3d<double> const A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 in the first slice of S
    **  std::transform(A1.begin_slice(0),A1.end_slice(0),
    **                A2.begin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    const_slice_iterator slice_end(const size_type row, const size_type col) const;
 

    /*!
    **  \brief Returns a read/write iterator that points to the
    **  last element of the line (row,col) threw the slices in the %Matrix3d.
    ** 	Iteration is done in reverse element order (decreasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    ** 	\return reverse_slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Matrix3d<double> A1(10,9,5);
    **  slip::Matrix3d<double> A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 
    **  in the first slice of S in reverse order
    **  std::transform(A1.rbegin_slice(0),A1.rend_slice(0),
    **                A2.rbegin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    reverse_slice_iterator slice_rbegin(const size_type row, const size_type col);

    /*!
    **  \brief Returns a read-only (constant) iterator that points to the
    **  last element of the line (row,col) threw the slices in the %Matrix3d.
    **	Iteration is done in reverse element order (decreasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return const_reverse_slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Matrix3d<double> const A1(10,9,5);
    **  slip::Matrix3d<double> const A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 
    **  in the first slice of S in reverse order
    **  std::transform(A1.rbegin_slice(0),A1.rend_slice(0),
    **                A2.rbegin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    const_reverse_slice_iterator slice_rbegin(const size_type row, const size_type col) const;

    /*!
    **  \brief Returns a read/write iterator that points to the
    **  one before the first element of the line (row,col) threw the slices in the %Matrix3d.
    **  Iteration is done in reverse element order (decreasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **  \return reverse_slice_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Matrix3d<double> A1(10,9,5);
    **  slip::Matrix3d<double> A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 
    **  in the first slice of S in reverse order
    **  std::transform(A1.rbegin_slice(0),A1.rend_slice(0),
    **                A2.rbegin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    reverse_slice_iterator slice_rend(const size_type row, const size_type col);
 
  
    /*!
    **  \brief Returns a read (constant) iterator that points to the
    **  one before the first element of the line (row,col) threw the slices in the %Matrix3d.
    **  Iteration is done in reverse element order (decreasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return const_reverse_slice_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Matrix3d<double> const A1(10,9,5);
    **  slip::Matrix3d<double> const A2(10,9,5);
    **  slip::Matrix3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 
    **  in the first slice of S in reverse order
    **  std::transform(A1.rbegin_slice(0),A1.rend_slice(0),
    **                A2.rbegin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    const_reverse_slice_iterator slice_rend(const size_type row, const size_type col) const;

    /*@} End One dimensionnal slice iterators */

    /**
     ** \name One dimensionnal row iterators
     */
    /*@{*/
    //-------------------row iterators----------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of the slice \a slice in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row.
    **  \return begin row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    row_iterator row_begin(const size_type slice,
			   const size_type row);

    /*!
    **  \brief Returns a read_only iterator that points to the first
    **  element of the row \a row of the slice \a slice in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row. 
    **  \return begin const_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_row_iterator row_begin(const size_type slice,
				 const size_type row) const;

 
    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the row \a row of the slice \a slice in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row.
    **  \return end row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    row_iterator row_end(const size_type slice,
			 const size_type row);


    /*!
    **  \brief Returns a read_only iterator that points to the past-the-end
    **  element of the row \a row of the slice \a slice in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row. 
    **  \return end const_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_row_iterator row_end(const size_type slice,
			       const size_type row) const;


    /*!
    **  \brief Returns a read/write reverse iterator that points to the last
    **  element of the row \a row of the slice \a slice in the %Matrix3d.  
    **  Iteration is done in reverse element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row.
    **  \return begin reverse_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    reverse_row_iterator row_rbegin(const size_type slice,
				    const size_type row);

    /*!
    **  \brief Returns a read_only reverse iterator that points to the last
    **  element of the row \a row of the slice \a slice in the %Matrix3d.  
    **  Iteration is done in reverse element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row. 
    **  \return begin const_reverse_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_reverse_row_iterator row_rbegin(const size_type slice,
					  const size_type row) const;

 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the row \a row of the slice \a slice in the %Matrix3d.  
    **  Iteration is done in reverse element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row.
    **  \return end reverse_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    reverse_row_iterator row_rend(const size_type slice,
				  const size_type row);


    /*!
    **  \brief Returns a read_only reverse iterator that points to the first
    **  element of the row \a row of the slice \a slice in the %Matrix3d.  
    **  Iteration is done in reverse element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row. 
    **  \return end const_reverse_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_reverse_row_iterator row_rend(const size_type slice,
					const size_type row) const;

    /*@} End One dimensionnal row iterators */
    /**
     ** \name One dimensionnal col iterators
     */
    /*@{*/
    //-------------------col iterators----------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the column \a column of the slice \a slice in the %Matrix3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return begin col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    col_iterator col_begin(const size_type slice,
			   const size_type col);


    /*!
    **  \brief Returns a read_only iterator that points to the first
    **  element of the column \a column of the slice \a slice in the %Matrix3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return begin const_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_col_iterator col_begin(const size_type slice,
				 const size_type col) const;

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the column \a column of the slice \a slice in the %Matrix3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return end col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    col_iterator col_end(const size_type slice,
			 const size_type col);


    /*!
    **  \brief Returns a read_only iterator that points to the past-the-end
    **  element of the column \a column of the slice \a slice in the %Matrix3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return end const_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_col_iterator col_end(const size_type slice,
			       const size_type col) const;


    /*!
    **  \brief Returns a read/write reverse iterator that points to the last
    **  element of the column \a column of the slice \a slice in the %Matrix3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return begin reverse_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    reverse_col_iterator col_rbegin(const size_type slice,
				    const size_type col);


    /*!
    **  \brief Returns a read_only reverse iterator that points to the last
    **  element of the column \a column of the slice \a slice in the %Matrix3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return begin const_reverse_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_reverse_col_iterator col_rbegin(const size_type slice,
					  const size_type col) const;

    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the column \a column of the slice \a slice in the %Matrix3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return end reverse_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    reverse_col_iterator col_rend(const size_type slice,
				  const size_type col);


    /*!
    **  \brief Returns a read_only reverse iterator that points to the first
    **  element of the column \a column of the slice \a slice in the %Matrix3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return end const_reverse_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_reverse_col_iterator col_rend(const size_type slice,
					const size_type col) const;

    /*@} End One dimensionnal col iterators */


   /**
     ** \name One dimensionnal slice range iterators
     */
    /*@{*/
   //------------------------slice range iterators -----------------------

    /*!
    ** \brief Returns a read/write iterator that points to the first
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Matrix3d.
    ** Iteration is done in ordinary element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return begin slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Matrix3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> A1(8,8,8);
    **  slip::Matrix3d<double> A2(4,8,8);
    **  slip::Range<int> range(0,A1.dim1()-1,2);
    ** //copy the the elements of the line (0,0) of A1 iterated according to the
    ** //range in the line (1,1) of A2
    ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),A2.slice_begin(1,1));
    ** \endcode
    */
    slice_range_iterator slice_begin(const size_type row,const size_type col,
				     const slip::Range<int>& range);


    /*!
    **  \brief Returns a read/write iterator that points one past the end
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Matrix3d.
    ** Iteration is done in ordinary element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return end slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Matrix3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> A1(8,8,8);
    **  slip::Matrix3d<double> A2(4,8,8);
    **  slip::Range<int> range(0,A1.dim1()-1,2);
    ** //copy the the elements of the line (0,0) of A1 iterated according to the
    ** //range in the line (1,1) of A2
    ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),A2.slice_begin(1,1));
    ** \endcode
    */
    slice_range_iterator slice_end(const size_type row,const size_type col,
				   const slip::Range<int>& range);


    /*!
    ** \brief Returns a read only (constant) iterator that points to the first
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Matrix3d.
    ** Iteration is done in ordinary element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return begin const_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Matrix3d.
    ** \par Example:
    ** \code
    **  
    **  slip::Matrix3d<double> const A1(M); //M is an already existing %Matrix3d
    **  slip::Range<int> range(0,A1.dim1()-1,2);
    ** //display the elements of the line (0,0) of A1 iterated according to the range
    ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),
    ** std::ostream_iterator<double>(std::cout," "));
    ** \endcode
    */
    const_slice_range_iterator slice_begin(const size_type row,const size_type col,
					   const slip::Range<int>& range) const;

    /*!
    ** \brief Returns a read_only iterator that points one past the last
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Matrix3d.
    ** Iteration is done in ordinary element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return end const_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Matrix3d.
    ** \par Example:
    ** \code
    **  
    **  slip::Matrix3d<double> const A1(M); //M is an already existing %Matrix3d
    **  slip::Range<int> range(0,A1.dim1()-1,2);
    ** //display the elements of the line (0,0) of A1 iterated according to the range
    ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),
    ** std::ostream_iterator<double>(std::cout," "));
    ** \endcode
    */
    const_slice_range_iterator slice_end(const size_type row,const size_type col,
					 const slip::Range<int>& range) const;


    /*!
    ** \brief Returns a read/write iterator that points to the last
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Matrix3d.
    ** Iteration is done in the reverse element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return begin reverse_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Matrix3d.
    */
    reverse_slice_range_iterator slice_rbegin(const size_type row,const size_type col,
					      const slip::Range<int>& range);


    /*!
    **  \brief Returns a read/write iterator that points to the first
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Matrix3d.
    ** Iteration is done in reverse element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return end reverse_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Matrix3d.
    */
    reverse_slice_range_iterator slice_rend(const size_type row,const size_type col,
					   const slip::Range<int>& range);


    /*!
    ** \brief Returns a read only (constant) iterator that points to the last
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Matrix3d.
    ** Iteration is done in reverse element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return begin const_reverse_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Matrix3d.
    */
    const_reverse_slice_range_iterator slice_rbegin(const size_type row,const size_type col,
						   const slip::Range<int>& range) const;

    /*!
    ** \brief Returns a read_only iterator that points one past the lastto the first
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Matrix3d.
    ** Iteration is done in reverse element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return end const_reverse_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Matrix3d.
    */
    const_reverse_slice_range_iterator slice_rend(const size_type row,const size_type col,
						 const slip::Range<int>& range) const;

 
    /*@} End One dimensionnal slice range iterators */

    /**
     ** \name One dimensionnal row range iterators
     */
    /*@{*/
   //------------------------row range iterators -----------------------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the %Range \a range of the row \a row in the %Matrix3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return begin row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre row must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> A1(8,8,8);
    **  slip::Matrix3d<double> A2(8,4,5);
    **  slip::Range<int> range(0,A1.dim3()-1,2);
    ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
    ** //range in the row 1 of the slice 1 of A2
    ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
    ** \endcode
    */
    row_range_iterator row_begin(const size_type slice,const size_type row,
				 const slip::Range<int>& range);

    /*!
    **  \brief Returns a read/write iterator that points one past the end
    **  element of the %Range \a range of the row \a row in the %Matrix3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return end row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre row must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> A1(8,8,8);
    **  slip::Matrix3d<double> A2(8,4,5);
    **  slip::Range<int> range(0,A1.dim3()-1,2);
    ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
    ** //range in the row 1 of the slice 1 of A2
    ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
    ** \endcode
    */
    row_range_iterator row_end(const size_type slice,const size_type row,
			       const slip::Range<int>& range);


    /*!
    **  \brief Returns a read-only iterator that points to the first
    **  element of the %Range \a range of the row \a row in the %Matrix3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return begin const_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** 
    ** \pre row must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> const A1(8,8,8);
    **  slip::Matrix3d<double> A2(8,4,5);
    **  slip::Range<int> range(0,A1.dim3()-1,2);
    ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
    ** //range in the row 1 of the slice 1 of A2
    ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
    ** \endcode
    */
    const_row_range_iterator row_begin(const size_type slice,const size_type row,
				       const slip::Range<int>& range) const;


    /*!
    **  \brief Returns a read_only iterator that points one past the last
    **  element of the %Range range of the row \a row in the %Matrix3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row Row to iterate.
    ** \param range %Range of the row to iterate
    ** \return begin const_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \pre row must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> const A1(8,8,8);
    **  slip::Matrix3d<double> A2(8,4,5);
    **  slip::Range<int> range(0,A1.dim3()-1,2);
    ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
    ** //range in the row 1 of the slice 1 of A2
    ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
    ** \endcode
    */
    const_row_range_iterator row_end(const size_type slice,const size_type row,
				     const slip::Range<int>& range) const;
 
    /*!
    ** \brief Returns a read-write iterator that points to the last
    ** element of the %Range \a range of the row of a slice \a row and slice in the %Matrix3d.  
    ** Iteration is done in the reverse element order according to the %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return reverse_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre row must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    */
    reverse_row_range_iterator row_rbegin(const size_type slice,const size_type row,
					  const slip::Range<int>& range);
  
  
    /*!
    **  \brief Returns a read-write iterator that points one before
    **  the first element of the %Range \a range of the row of a slice \a row in the 
    **  %Matrix3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return reverse_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** 
    ** \pre row must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    */
    reverse_row_range_iterator row_rend(const size_type slice,const size_type row,
					const slip::Range<int>& range);



    /*!
    **  \brief Returns a read-only iterator that points to the last
    **  element of the %Range \a range of the row  of a slice \a row in the %Matrix3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate
    ** \return begin const_reverse_row_range_iterator value
    **
    ** \pre row must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    */
    const_reverse_row_range_iterator row_rbegin(const size_type slice,const size_type row,
						const slip::Range<int>& range) const;


    /*!
    **  \brief Returns a read-only iterator that points one before the first
    **  element of the %Range \a range of the row of a slice \a row in the %Matrix3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate
    ** \return const_reverse_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre row must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    */
    const_reverse_row_range_iterator row_rend(const size_type slice,const size_type row,
					      const slip::Range<int>& range) const;
 
    /*@} End One dimensionnal row range iterators */
    /**
     ** \name One dimensionnal col range iterators
     */
    /*@{*/
    //------------------------col range iterators -----------------------
  
    /*!
    **  \brief Returns a read-write iterator that points to the first
    **  element of the %Range \a range of the col \a col in the %Matrix3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate
    ** \return begin col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> A1(8,8,8);
    **  slip::Matrix3d<double> A2(4,8,5);
    **  slip::Range<int> range(0,A1.dim2()-1,2);
    ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
    ** //range in the column 1 of the slice 1 of A2
    ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
    ** \endcode
    */
    col_range_iterator col_begin(const size_type slice,const size_type col,
				 const slip::Range<int>& range);

    /*!
    **  \brief Returns a read-write iterator that points to the past
    **  the end element of the %Range \a range of the col \a col in the 
    **  %Matrix3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return begin col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> A1(8,8,8);
    **  slip::Matrix3d<double> A2(4,8,5);
    **  slip::Range<int> range(0,A1.dim2()-1,2);
    ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
    ** //range in the column 1 of the slice 1 of A2
    ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
    ** \endcode
    */
    col_range_iterator col_end(const size_type slice,const size_type col,
			       const slip::Range<int>& range);


    /*!
    **  \brief Returns a read-only iterator that points to the first
    **  element of the %Range \a range of the col \a col in the %Matrix3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return begin const_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> const A1(8,8,8);
    **  slip::Matrix3d<double> A2(4,8,5);
    **  slip::Range<int> range(0,A1.dim2()-1,2);
    ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
    ** //range in the column 1 of the slice 1 of A2
    ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
    ** \endcode
    */
    const_col_range_iterator col_begin(const size_type slice,const size_type col,
				       const slip::Range<int>& range) const;
    
    /*!
    **  \brief Returns a read-only iterator that points to the past
    **  the end element of the %Range \a range of the col \a col in 
    **  the %Matrix3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate
    ** \return begin const_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Matrix3d<double> const A1(8,8,8);
    **  slip::Matrix3d<double> A2(4,8,5);
    **  slip::Range<int> range(0,A1.dim2()-1,2);
    ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
    ** //range in the column 1 of the slice 1 of A2
    ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
    ** \endcode
    */
    const_col_range_iterator col_end(const size_type slice,const size_type col,
				     const slip::Range<int>& range) const;

    /*!
    **  \brief Returns a read-write iterator that points to the last
    **  element of the %Range \a range of the col of a slice \a col in the %Matrix3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return begin reverse_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    */
    reverse_col_range_iterator col_rbegin(const size_type slice,const size_type col,
					  const slip::Range<int>& range);

    /*!
    **  \brief Returns a read-write iterator that points to one before 
    **  the first element of the %Range range of the col of a slice \a col in the 
    **  %Matrix3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return reverse_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    */
    reverse_col_range_iterator col_rend(const size_type slice,const size_type col,
					const slip::Range<int>& range);

    /*!
    **  \brief Returns a read_only iterator that points to the last
    **  element of the %Range \& range of the col of a slice \a col in the %Matrix3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return begin const_reverse_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    */
    const_reverse_col_range_iterator col_rbegin(const size_type slice,const size_type col,
						const slip::Range<int>& range) const;
 
    /*!
    **  \brief Returns a read-only iterator that points to the first
    **  element of the %Range \a range of the col of a slice \a col in the %Matrix3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return const_reverse_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Matrix3d.
    ** \pre The range must be inside the whole range of the %Matrix3d.
    */
    const_reverse_col_range_iterator col_rend(const size_type slice,const size_type col,
					      const slip::Range<int>& range) const;

    /*@} End One dimensionnal col range iterators */

    //****************************************************************************
    //                          One dimensionnal plane iterators
    //****************************************************************************
   
    /**
     ** \name One dimensionnal global plane iterators
     */
    /*@{*/

    //----------------------Global plane iterators------------------------------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element in the in the slice plane of the %Matrix3d. 
    **  Iteration is done in ordinary element order.
    **  \param slice the slice coordinate of the plane
    **  \return begin iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Matrix3d<double> A1(2,3,2);
    **   slip::Matrix3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2
    **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
    **  \endcode
    */
    iterator plane_begin(const size_type slice);

    /*!
    **  \brief Returns a read-only (constant) iterator that points to the
    **  first element in the slice plane of the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param slice the slice coordinate of the plane
    **  \return const begin iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Matrix3d<double> const A1(2,3,2);
    **   slip::Matrix3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2
    **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
    **  \endcode
    */
    const_iterator plane_begin(const size_type slice) const;

    /*!
    **  \brief Returns a read/write iterator that points one past the last
    **  element in the slice plane of the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param slice the slice coordinate of the plane
    **  \return end iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Matrix3d<double> A1(2,3,2);
    **   slip::Matrix3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2
    **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
    **  \endcode
    */
    iterator plane_end(const size_type slice);
 
    /*!
    **  \brief Returns a read-only (constant) iterator that points one past
    **  the last element in the slice plane of the %Matrix3d.
    **  Iteration is done in ordinary element order.
    **  \param slice the slice coordinate of the plane
    **  \return const end iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Matrix3d<double> const A1(2,3,2);
    **   slip::Matrix3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2
    **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
    **  \endcode
    */
    const_iterator plane_end(const size_type slice) const;
  
    /*!
    **  \brief Returns a read/write reverse iterator that points to the
    **  last element in the slice plane of the %Matrix3d.
    **  Iteration is done in reverse element order.
    **  \param slice the slice coordinate of the plane
    **  \return reverse begin iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Matrix3d<double> A1(2,3,2);
    **   slip::Matrix3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
    **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
    **  \endcode
    */
    reverse_iterator plane_rbegin(const size_type slice);
  
    /*!
    **  \brief Returns a read-only (constant) reverse iterator that points
    **  to the last element in the slice plane k of the %Matrix3d.
    **  Iteration is done in reverse element order.
    ** \param slice the slice coordinate of the plane
    ** \return const reverse begin iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Matrix3d<double> const A1(2,3,2);
    **   slip::Matrix3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
    **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
    **  \endcode
    */
    const_reverse_iterator plane_rbegin(const size_type slice) const;

    /*!
    **  \brief Returns a read/write reverse iterator that points to one
    **  before the first element in the slice plane of the %Matrix3d.
    **  Iteration is done in reverse element order.
    **  \param slice the slice coordinate of the plane
    **  \return reverse end iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Matrix3d<double> A1(2,3,2);
    **   slip::Matrix3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
    **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
    **  \endcode
    */
    reverse_iterator plane_rend(const size_type slice);

    /*!
    **  \brief Returns a read-only (constant) reverse iterator that points
    **  to one before the first element in the slice plane of the %Matrix3d.
    **  Iteration is done in reverse element order.
    **  \param slice the slice coordinate of the plane
    **  \return const reverse end iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Matrix3d<double> const A1(2,3,2);
    **   slip::Matrix3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
    **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
    **  \endcode
    */
    const_reverse_iterator plane_rend(const size_type slice) const;

    /*@} End One dimensionnal global plane iterators */


#ifdef ALL_PLANE_ITERATOR3D
    
    /**
     ** \name One dimensionnal row and col plane iterators
     */
    /*@{*/
    //-------------------row and col plane iterators----------
    
      
    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
				     const size_type row) const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type row);
    
    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the row \a row of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return end iterator1d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type row);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the row \a row of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type row) const;


    /*!
    **  \brief Returns a read/write reverse_iterator that points to the last
    **  element of the row \a row of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin reverse_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type row);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **  element of the row \a row of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					      const size_type row) const;
 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the row \a row of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type row);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the first
    **  element of the row \a row of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type row) const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the col \a col of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type col);

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the col \a col of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
				     const size_type col) const;
    
    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the col \a col of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type col);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the col \a col of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type col) const;


    /*!
    **  \brief Returns a read/write reverse_iterator that points to the last
    **  element of the col \a col of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin reverse_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type col);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **  element of the col \a col of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin reverse_const_iterator1d value
    */
    const_reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					      const size_type col) const;
 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the col \a col of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type col);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the first
    **  element of the col \a col of a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type col) const;

    /*@} End One dimensionnal plane row and col iterators */
    
    /**
     ** \name One dimensionnal plane box iterators
     */
    /*@{*/
    //-------------------plane box iterators----------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of a box within a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
				     const size_type row, const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of a box within a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return begin iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type row, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the row \a row of a box within a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate, 
			     const size_type row, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **   element of the row \a row of a box within a plane \a plane in the %Matrix3d.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate,
				   const size_type row, const Box2d<int> & b) const;


    /*!
    **  \brief Returns a read/write reverse_iterator that points to the last
    **  element of the row \a row of a box within a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \param b The box within the plane
    **  \return begin reverse_iterator1d value
    */
    reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					const size_type row, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **  element of the row \a row of a box within a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					      const size_type row, const Box2d<int> & b) const;
 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the row \a row of a box within a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return end iterator1d value
    */
    reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,
				      const size_type row, const Box2d<int> & b);

    /*!
    ** \brief Returns a read/write const reverse iterator that points to the first
    **  element of the row \a row of a box within a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,
					    const size_type row, const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the col \a col of a box within a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin iterator1d value
    */
    iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type col, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the col \a col of a box within a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
				     const size_type col, const Box2d<int> & b) const;
    
    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the col \a col of a box within a plane \a plane in the %Matrix3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate, 
			     const size_type col, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **   element of the col \a col of a box within a plane \a plane in the %Matrix3d.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate,
				   const size_type col, const Box2d<int> & b) const;


    /*!
    **  \brief Returns a read/write reverse_iterator that points to the last
    **   element of the col \a col of a box within a plane \a plane in the %Matrix3d.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin reverse_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					const size_type col, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **   element of the col \a col of a box within a plane \a plane in the %Matrix3d.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					      const size_type col, const Box2d<int> & b) const;
 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **   element of the col \a col of a box within a plane \a plane in the %Matrix3d.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate, 
				      const size_type col, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the first
    **   element of the col \a col of a box within a plane \a plane in the %Matrix3d.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,
					    const size_type col, const Box2d<int> & b) const;


    /*@} End One dimensionnal plane box iterators */

#endif //ALL_PLANE_ITERATOR3D

    //****************************************************************************
    //                          Two dimensionnal plane iterators
    //****************************************************************************
    
    /**
     ** \name two dimensionnal plane iterators : Global iterators
     */
    /*@{*/

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **   element of the plane in the %Matrix3d. It points to the upper left 
    **   element of the plane
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate);

    /*!
    **  \brief Returns a read/write iterator that points to the last
    **   element of the plane in the %Matrix3d. It points to past the end element of
    **   the bottom right element of the plane
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate);

    /*!
    **  \brief Returns a read/write const iterator that points to the first
    **   element of the plane in the %Matrix3d. It points to the upper left 
    **   element of the plane
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate) const;

    /*!
    **  \brief Returns a read/write const iterator that points to the last
    **  element of the plane in the %Matrix3d. It points to past the end element of
    **  the bottom right element of the plane
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate) const;

    /*!
    **  \brief Returns a read/write reverse_iterator that points to the bottom right
    **  element of the plane in the %Matrix3d.
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate);

    /*!
    **  \brief Returns a read/write reverse_iterator that points to the upper left
    **  element of the plane in the %Matrix3d.
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example :
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the bottom
    **  right element of the plane in the %Matrix3d.
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example :
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate) const;

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the upper
    **  left element of the plane in the %Matrix3d.
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
                   slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate) const;


    /*@} End two dimensionnal plane iterators : Global iterators */
    
    /**
     ** \name two dimensionnal plane iterators : box iterators
     */
    /*@{*/

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of a box within a plane in the %Matrix3d. It points to the upper left 
    **  element of the box
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
				, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the last
    **  element of a box within a plane in the %Matrix3d. It points to past the end element of
    **  the bottom right element of the plane
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
				  , const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const iterator that points to the first
    **  element of a box within a plane in the %Matrix3d. It points to the upper left 
    **  element of the plane
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
				      , const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write const iterator that points to the last
    **  element of a box within a plane in the %Matrix3d. It points to past the end element of
    **  the bottom right element of the plane
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
					, const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write reverse iterator that points to the last
    **  element of a box within a plane in the %Matrix3d. It points to the  
    **  bottom right element of the box
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin reverse_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box in a reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
					 , const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of a box within a plane in the %Matrix3d. It points to the upper
    **  left element of the plane
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin reverse_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box in a reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
					   , const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **  element of a box within a plane in the %Matrix3d. It points to the bottom right 
    **  element of the plane
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin const_reverse_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box in a reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
					       , const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the first
    **  element of a box within a plane in the %Matrix3d. It points to
    **  the bottom right element of the plane
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin const_reverse_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box in a reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
						 , const Box2d<int> & b) const;
    

    /*@} End two dimensionnal plane iterators : box iterators */
    
    //****************************************************************************
    //                          Three dimensionnal iterators
    //****************************************************************************
   
    /**
     ** \name three dimensionnal iterators : Global iterators
     */
    /*@{*/

    //------------------------ Global iterators------------------------------------

    /*!
    **  \brief Returns a read/write iterator3d that points to the first
    **  element of the %Matrix3d. It points to the front upper left element of
    **  the %Matrix3d.
    **  
    **  \return begin iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   //Print M
    **   std::copy(M.front_upper_left(),M.back_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d front_upper_left();
  
  
    /*!
    **  \brief Returns a read/write iterator3d that points to the past
    **  the end element of the %Matrix3d. It points to past the end element of
    **  the back bottom right element of the %Matrix3d.
    **  
    **  \return begin iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   //Print M
    **   std::copy(M.front_upper_left(),M.back_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d back_bottom_right();


    /*!
    **  \brief Returns a read-only iterator3d that points to the first
    **  element of the %Matrix3d. It points to the front upper left element of
    **  the %Matrix3d.
    **  
    **  \return begin const iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   //Print M
    **   std::copy(M.front_upper_left(),M.back_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d front_upper_left() const;
  
 
    /*!
    **  \brief Returns a read-only iterator3d that points to the past
    **  the end element of the %Matrix3d. It points to past the end element of
    **  the back bottom right element of the %Matrix3d.
    **  
    **  \return begin const iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   //Print M
    **   std::copy(M.front_upper_left(),M.back_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d back_bottom_right() const;

    /*!
    **  \brief Returns a read/write reverse iterator3d. It points to the 
    **   back bottom right element of the %Matrix3d. 
    *    Iteration is done within the %Matrix3d in the reverse order.
    **  
    **  \return reverse_iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   //Print M in a reverse order
    **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator3d rfront_upper_left();
  
    /*!
    **  \brief Returns a read/write reverse iterator3d. It points to past the 
    **  front upper left element of the %Matrix3d.
    **  Iteration is done in the reverse order.
    **  
    **  \return reverse iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   //Print M in a reverse order
    **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    **/
    reverse_iterator3d rback_bottom_right();

    /*!
    **  \brief Returns a read only reverse iterator3d that points.  It points 
    **  to the back bottom right element of the %Matrix3d. 
    **  Iteration is done within the %Matrix3d in the reverse order.
    **  
    ** \return const_reverse_iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   //Print M in a reverse order
    **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator3d rfront_upper_left() const;
 
 
    /*!
    **  \brief Returns a read only reverse iterator3d. It points to past the 
    **  front upper left element of the %Matrix3d.
    **  Iteration is done in the reverse order.
    **  
    ** \return const reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   //Print M in a reverse order
    **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator3d rback_bottom_right() const;

    /*@} End three dimensionnal iterators : Global iterators */
    
    /**
     ** \name three dimensionnal iterators : Box iterators
     */
    /*@{*/
    
    //------------------------ Box iterators------------------------------------

    /*!
    **  \brief Returns a read/write iterator3d that points to the first
    **  element of the %Matrix3d. It points to the front upper left element of
    **  the \a %Box3d associated to the %Matrix3d.
    **
    **  \param box A %Box3d defining the range of indices to iterate
    **         within the %Matrix3d.
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \return end iterator3d value
    **  \pre The box indices must be inside the range of the %Matrix3d ones.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   slip::Box3d<int> b(1,1,1,2,2,3);
    **   //Print M within a box
    **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d front_upper_left(const Box3d<int>& box);

 
    /*!
    **  \brief Returns a read/write iterator3d that points to the past
    **  the end element of the %Matrix3d. It points to past the end element of
    **  the back bottom right element of the \a %Box3d associated to the %Matrix3d.
    **
    **  \param box a %Box3d defining the range of indices to iterate
    **         within the %Matrix3d.
    **
    **  \return end iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \pre The box indices must be inside the range of the %Matrix3d ones.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   slip::Box3d<int> b(1,1,1,2,2,3);
    **   //Print M within a box
    **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d back_bottom_right(const Box3d<int>& box);


    /*!
    **  \brief Returns a read only iterator3d that points to the first
    **  element of the %Matrix3d. It points to the front upper left element of
    **  the \a %Box3d associated to the %Matrix3d.
    **
    **  \param box a %Box3d defining the range of indices to iterate
    **         within the %Matrix3d.
    **  \return end const iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **
    **  \pre The box indices must be inside the range of the %Matrix3d ones.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   slip::Box3d<int> b(1,1,1,2,2,4);
    **   //Print M within a box
    **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d front_upper_left(const Box3d<int>& box) const;

 
    /*!
    **  \brief Returns a read only iterator3d that points to the past
    **  the end element of the %Matrix3d. It points to past the end element of
    **  the back bottom right element of the \a %Box3d associated to the %Matrix3d.
    **
    **  \param box a %Box3d defining the range of indices to iterate
    **         within the %Matrix3d.
    **  \return end const iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **           algorithms.
    **
    **  \pre The box indices must be inside the range of the %Matrix3d ones.
    ** 
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   slip::Box3d<int> b(1,1,1,2,2,4);
    **   //Print M within a box
    **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d back_bottom_right(const Box3d<int>& box) const;

  

    /*!
    **  \brief Returns a read/write reverse iterator3d. It points to the 
    **  back bottom right element of the \a %Box3d associated to the %Matrix3d.
    **  Iteration is done in the reverse order.
    **
    ** \param box a %Box3d defining the range of indices to iterate
    **        within the %Matrix3d.
    **
    ** \pre The box indices must be inside the range of the %Matrix3d ones.
    **  
    ** \return reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    */
    reverse_iterator3d rfront_upper_left(const Box3d<int>& box);

    /*!
    **  \brief Returns a read/write reverse iterator3d. It points to one
    **  before the front upper left element of the %Box3d \a box associated to 
    **  the %Matrix3d.
    **
    ** \param box A %Box3d defining the range of indices to iterate
    **        within the %Matrix3d.
    **
    ** \pre The box indices must be inside the range of the %Matrix3d ones.
    **  
    ** \return reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    */
    reverse_iterator3d rback_bottom_right(const Box3d<int>& box);

    /*!
    **  \brief Returns a read only reverse iterator3d. It points to the 
    **  back bottom right element of the %Box3d \a box associated to the %Matrix3d.
    **  Iteration is done in the reverse order.
    **
    ** \param box A %Box3d defining the range of indices to iterate
    **        within the %Matrix3d.
    **
    ** \pre The box indices must be inside the range of the %Matrix3d ones.
    **  
    ** \return const reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    */
    const_reverse_iterator3d rfront_upper_left(const Box3d<int>& box) const;


    /*!
    **  \brief Returns a read-only reverse iterator3d. It points to one 
    **  before the front element of the bottom right element of the %Box3d 
    **  \a box associated to the %Matrix3d.
    **
    ** \param box A %Box3d defining the range of indices to iterate
    **        within the %Matrix3d.
    **
    ** \pre The box indices must be inside the range of the %Matrix3d ones.
    **  
    ** \return const reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    */
    const_reverse_iterator3d rback_bottom_right(const Box3d<int>& box) const;

    
    /*@} End three dimensionnal iterators : Box iterators */
    
    /**
     ** \name three dimensionnal iterators : Range iterators
     */
    
    /*@{*/
    
    //------------------------ Range iterators------------------------------------
 
    /*!
    **  \brief Returns a read/write iterator3d_range that points to the 
    **  front upper left element of the ranges \a slice_range, \a row_range and \a col_range 
    **  associated to the %Matrix3d.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Matrix3d ones.
    **  
    ** \return iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range
    **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d_range front_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
				const Range<int>& col_range);

    /*!
    **  \brief Returns a read/write iterator3d_range that points to the 
    **   past the end back bottom right element of the ranges \a slice_range, \a row_range 
    **   and \a col_range associated to the %Matrix3d.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Matrix3d ones.
    **  
    ** \return iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range
    **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d_range back_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
				  const Range<int>& col_range);


    /*!
    **  \brief Returns a read-only iterator3d_range that points to the
    **   to the front upper left element of the ranges \a slice_range, \a row_range and \a col_range 
    **   associated to the %Matrix3d.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid.
    ** \pre The ranges indices must be inside the ranges of the %Matrix3d ones.
    **  
    ** \return const_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range
    **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d_range front_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
				      const Range<int>& col_range) const;


    /*!
    **  \brief Returns a read-only iterator3d_range that points to the past
    **  the end back bottom right element of the ranges \a slice_range, \a row_range and \a col_range 
    **  associated to the %Matrix3d.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Matrix3d ones.
    **  
    ** \return const_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range
    **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d_range back_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
					const Range<int>& col_range) const;

    /*!
    **  \brief Returns a read/write reverse_iterator3d_range that points to the 
    **  past the back bottom right element of the ranges \a row_range and 
    **  \a col_range associated to the %Matrix3d. Iteration is done in the 
    **  reverse order.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Matrix3d ones.
    **  
    ** \return reverse_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range in reverse order
    **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator3d_range rfront_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
					 const Range<int>& col_range);
    
    /*!
    **  \brief Returns a read/write reverse_iterator3d_range that points
    **  to one before the  front upper left element of the ranges \a row_range 
    **  and \a col_range associated to the %Matrix3d. Iteration is done
    **  in the reverse order.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Matrix3d ones.
    **  
    ** \return reverse_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range in reverse order
    **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator3d_range rback_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
					   const Range<int>& col_range);
    
    /*!
    **  \brief Returns a read-only reverse_iterator3d_range that points 
    **   to the past the back bottom right element of the ranges \a row_range and 
    **   \a col_range associated to the %Matrix3d. Iteration is done in the 
    **   reverse order.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid.
    ** \pre The ranges indices must be inside the ranges of the %Matrix3d ones.
    **  
    ** \return const_reverse_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range in reverse order
    **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator3d_range rfront_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
					       const Range<int>& col_range) const;
    
    /*!
    **  \brief Returns a read-only reverse_iterator3d_range that points 
    **  to one before the front upper left element of the ranges \a row_range 
    **  and \a col_range associated to the %Matrix3d.Iteration is done in 
    **  the reverse order.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Matrix3d ones.
    **  
    ** \return const_reverse_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Matrix3d<double> const M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range in reverse order
    **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator3d_range rback_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
						 const Range<int>& col_range) const;
        
    /*@} End three dimensionnal iterators : Range iterators */


    //********************************************************************
  
    /**
     ** \name i/o operators
     */
    /*@{*/
    /*!
    ** \brief Write the %Matrix3d to the ouput stream
    ** \param out output std::ostream
    ** \param a %Matrix3d to write to the output stream
    */
    friend std::ostream& operator<< <>(std::ostream & out, 
				       const self& a);

    /*@} End i/o operators */

    /**
     ** \name  Assignment operators and methods
     */
    /*@{*/
    /*!
    ** \brief Assign a %Matrix3d.
    **
    ** Assign elements of %Matrix3d in \a rhs
    **
    ** \param rhs %Matrix3d to get the values from.
    ** \return 
    */
    self& operator=(const Matrix3d<T> & rhs);



    /*!
    ** \brief Assign all the elments of the %Matrix3d by value
    **
    **
    ** \param value A reference-to-const of arbitrary type.
    ** \return 
    */
    self& operator=(const T& value);


    /*!
    ** \brief Fills the container range [begin(),begin()+size()) 
    **        with copies of value
    ** \param value  A reference-to-const of arbitrary type.
    */
    void fill(const T& value)
    {
      std::fill_n(this->begin(),this->size(),value);
    } 
  
    /*!
    ** \brief Fills the container range [begin(),begin()+size())
    **        with a copy of
    **        the value array
    ** \param value  A pointer of arbitrary type.
    */
    void fill(const T* value)
    {
      std::copy(value,value + this->size(), this->begin());
    } 

    /*!
    ** \brief Fills the container range [begin(),begin()+size()) 
    **        with a copy of the range [first,last)
    **  \param  first  An input iterator.
    **  \param  last   An input iterator.
    **   
    **
    */
    template<typename InputIterator>
    void fill(InputIterator first,
	      InputIterator last)
    {
      std::copy(first,last, this->begin());
    }
    /*@} End Assignment operators and methods*/



    /**
     ** \name Comparison operators
     */
    /*@{*/
    /*!
    ** \brief %Matrix3d equality comparison
    ** \param x A %Matrix3d
    ** \param y A %Matrix3d of the same type of \a x
    ** \return true iff the size and the elements of the Arrays are equal
    */
    friend bool operator== <>(const Matrix3d<T>& x, 
			      const Matrix3d<T>& y);

    /*!
    ** \brief %Matrix3d inequality comparison
    ** \param x A %Matrix3d
    ** \param y A %Matrix3d of the same type of \a x
    ** \return true if !(x == y) 
    */
    friend bool operator!= <>(const Matrix3d<T>& x, 
			      const Matrix3d<T>& y);

    /*!
    ** \brief Less than comparison operator (%Matrix3d ordering relation)
    ** \param x A %Matrix3d
    ** \param y A %Matrix3d of the same type of \a x
    ** \return true iff \a x is lexicographically less than \a y
    */
    friend bool operator< <>(const Matrix3d<T>& x, 
			     const Matrix3d<T>& y);

    /*!
    ** \brief More than comparison operator
    ** \param x A %Matrix3d
    ** \param y A %Matrix3d of the same type of \a x
    ** \return true iff y > x 
    */
    friend bool operator> <>(const Matrix3d<T>& x, 
			     const Matrix3d<T>& y);

    /*!
    ** \brief Less than equal comparison operator
    ** \param x A %Matrix3d
    ** \param y A %Matrix3d of the same type of \a x
    ** \return true iff !(y > x) 
    */
    friend bool operator<= <>(const Matrix3d<T>& x, 
			      const Matrix3d<T>& y);

    /*!
    ** \brief More than equal comparison operator
    ** \param x A %Matrix3d
    ** \param y A %Matrix3d of the same type of \a x
    ** \return true iff !(x < y) 
    */
    friend bool operator>= <>(const Matrix3d<T>& x, 
			      const Matrix3d<T>& y);


    /*@} Comparison operators */
  



    /**
     ** \name  Element access operators
     */
    /*@{*/

    /*!
    ** \brief Subscript access to the slice datas contained in the %GenericMultiComponent3d.
    ** \param k The index of the slice for which data should be accessed.
    ** \return Read/write pointer* to the slice data.
    ** \pre k < slices()
    **
    ** This operator allows for easy, 3d array-style, data access.
    ** Note that data access with this operator is unchecked and
    ** out_of_range lookups are not defined. 
    */
    T** operator[](const size_type k);

    /*!
    ** \brief Subscript access to the slice datas contained in the %GenericMultiComponent3d.
    ** \param k The index of the slice for which data should be accessed.
    ** \return Read-only pointer* to the slice data.
    ** \pre k < slices()
    **
    ** This operator allows for easy, 3d array-style, data access.
    ** Note that data access with this operator is unchecked and
    ** out_of_range lookups are not defined. 
    */
    const T* const* operator[](const size_type k) const;
  

    /*!
    ** \brief Subscript access to the data contained in the %Matrix3d.
    ** \param k The index of the slice for which the data should be accessed. 
    ** \param i The index of the row for which the data should be accessed. 
    ** \param j The index of the column for which the data should be accessed.
    ** \return Read/Write reference to data.
    ** \pre k < slices()
    ** \pre i < rows()
    ** \pre j < cols()
    ** This operator allows for easy, 3d array-style, data access.
    ** Note that data access with this operator is unchecked and
    ** out_of_range lookups are not defined. 
    */
    reference operator()(const size_type k,
			 const size_type i,
			 const size_type j);

    /*!
    ** \brief Subscript access to the data contained in the %Matrix3d.
    ** \param k The index of the slice for which the data should be accessed. 
    ** \param i The index of the row for which the data should be accessed. 
    ** \param j The index of the column for which the data should be accessed.
    ** \return Read_only (constant) reference to data.
    ** \pre k < slices()
    ** \pre i < rows()
    ** \pre j < cols()
    ** This operator allows for easy, 3d array-style, data access.
    ** Note that data access with this operator is unchecked and
    ** out_of_range lookups are not defined. 
    */
    const_reference operator()(const size_type k,
			       const size_type i,
			       const size_type j) const;
	
    /*@} End Element access operators */
  

    /*!
    ** \brief Returns the name of the class 
    **       
    */
    std::string name() const;



    /*!
    ** \brief Returns the number of slices (first dimension size) 
    **        in the %Matrix3d
    */
    size_type dim1() const;
  
    /*!
    ** \brief Returns the number of slices (first dimension size) 
    **        in the %Matrix3d
    */
    size_type slices() const;
 
    /*!
    ** \brief Returns the number of rows (second dimension size) 
    **        in the %Matrix3d
    */
    size_type dim2() const;

    /*!
    ** \brief Returns the number of rows (second dimension size) 
    **        in the %Matrix3d
    */
    size_type rows() const;

    /*!
    ** \brief Returns the number of columns (third dimension size) 
    **        in the %Matrix3d
    */
    size_type dim3() const;

    /*!
    ** \brief Returns the number of columns (third dimension size) 
    **        in the %Matrix3d
    */
    size_type cols() const;

    /*!
    ** \brief Returns the number of columns (third dimension size) 
    **        in the %Matrix3d
    */
    size_type columns() const;


  
    /*!
    ** \brief Returns the number of elements in the %Matrix3d
    */
    size_type size() const;


    /*!
    ** \brief Returns the maximal size (number of elements) in the %Matrix3d
    */
    size_type max_size() const;

    /*!
    ** \brief Returns the number of elements in a slice of the %Matrix3d
    */
    size_type slice_size() const;

  
    /*!
    ** \brief Returns true if the %Matrix3d is empty.  (Thus size() == 0)
    */
    bool empty()const;
  
    /*!
    ** \brief Swaps data with another %Matrix3d.
    ** \param M A %Matrix3d of the same element type
    **
    ** \pre dim1() == M.dim1()
    ** \pre dim2() == M.dim2()
    ** \pre dim3() == M.dim3()
    */
    void swap(Matrix3d& M);

    /**
     ** \name  Arithmetic operators
     */
    /*@{*/
    /*!
    ** \brief Add val to each element of the Matrix  
    ** \param val value
    ** \return reference to the resulting Matrix
    */
    self& operator+=(const T& val);
    self& operator-=(const T& val);
    self& operator*=(const T& val);
    self& operator/=(const T& val);
    //   self& operator%=(const T& val);
    //   self& operator^=(const T& val);
    //   self& operator&=(const T& val);
    //   self& operator|=(const T& val);
    //   self& operator<<=(const T& val);
    //   self& operator>>=(const T& val);


    self  operator-() const;
    //self  operator!() const;

  

    self& operator+=(const self& rhs);
    self& operator-=(const self& rhs);
    self& operator*=(const self& rhs);
    self& operator/=(const self& rhs);
    

    /*@} End Arithmetic operators */

    /**
     ** \name  Mathematic operators
     */
    /*@{*/
    /*!
    ** \brief Returns the min element of the %Matrix
    **  according to the operator <
    ** \pre size() != 0
    */
    T& min() const;


    /*!
    ** \brief Returns the max element of the %Matrix 
    ** according to the operator <
    ** \pre size() != 0
    */
    T& max() const;

    /*!
    ** \brief Returns the sum of the elements of the %Matrix 
    ** \pre size() != 0
    */
    T sum() const;

  
    /*!
    ** \brief Applys the one-parameter C-function \a fun 
    **        to each element of the %Matrix3d
    ** \param fun The one-parameter C function 
    ** \return the resulting %Matrix3d
    */
    Matrix3d<T>& apply(T (*fun)(T));

    /*!
    ** \brief Applys the one-parameter C-function \a fun 
    **        to each element of the %Matrix3d
    ** \param fun The one-const-parameter C function 
    ** \return the resulting %Matrix3d
    */
    Matrix3d<T>& apply(T (*fun)(const T&));
  
    /*@} End Arithmetic operators */

  private:
    Array3d<T>* array_;
 private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    { 
      if(version >= 0)
	{
	  ar & this->array_;
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    { 
      if(version >= 0)
	{
	  ar & this->array_;
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
  };

  ///double alias
  typedef slip::Matrix3d<double> Matrix3d_d;
  ///float alias
  typedef slip::Matrix3d<float> Matrix3d_f;
  ///long alias
  typedef slip::Matrix3d<long> Matrix3d_l;
  ///unsigned long alias
  typedef slip::Matrix3d<unsigned long> Matrix3d_ul;
  ///short alias
  typedef slip::Matrix3d<short> Matrix3d_s;
  ///unsigned long alias
  typedef slip::Matrix3d<unsigned short> Matrix3d_us;
  ///int alias
  typedef slip::Matrix3d<int> Matrix3d_i;
  ///unsigned int alias
  typedef slip::Matrix3d<unsigned int> Matrix3d_ui;
  ///char alias
  typedef slip::Matrix3d<char> Matrix3d_c;
  ///unsigned char alias
  typedef slip::Matrix3d<unsigned char> Matrix3d_uc;


}//slip::


namespace slip{
  /*!
  ** \brief pointwise addition of two %Matrix3d
  ** \param M1 first %Matrix3d 
  ** \param M2 seconf %Matrix3d
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> operator+(const Matrix3d<T>& M1, 
			const Matrix3d<T>& M2);

  /*!
  ** \brief addition of a scalar to each element of a %Matrix3d
  ** \param M1 the %Matrix3d 
  ** \param val the scalar
  ** \return resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> operator+(const Matrix3d<T>& M1, 
			const T& val);

  /*!
  ** \brief addition of a scalar to each element of a %Matrix3d
  ** \param val the scalar
  ** \param M1 the %Matrix3d  
  ** \return resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> operator+(const T& val, 
			const Matrix3d<T>& M1);
  
  /*!
  ** \brief pointwise substraction of two %Matrix3d
  ** \param M1 first %Matrix3d 
  ** \param M2 seconf %Matrix3d
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> operator-(const Matrix3d<T>& M1, 
			const Matrix3d<T>& M2);

  /*!
  ** \brief substraction of a scalar to each element of a %Matrix3d
  ** \param M1 the %Matrix3d 
  ** \param val the scalar
  ** \return resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> operator-(const Matrix3d<T>& M1, 
			const T& val);

  /*!
  ** \brief substraction of a scalar to each element of a %Matrix3d
  ** \param val the scalar
  ** \param M1 the %Matrix3d  
  ** \return resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> operator-(const T& val, 
			const Matrix3d<T>& M1);
  
  /*!
  ** \brief pointwise multiplication of two %Matrix3d
  ** \param M1 first %Matrix3d 
  ** \param M2 seconf %Matrix3d
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> operator*(const Matrix3d<T>& M1, 
			const Matrix3d<T>& M2);

  /*!
  ** \brief multiplication of a scalar to each element of a %Matrix3d
  ** \param M1 the %Matrix3d 
  ** \param val the scalar
  ** \return resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> operator*(const Matrix3d<T>& M1, 
			const T& val);

  /*!
  ** \brief multiplication of a scalar to each element of a %Matrix3d
  ** \param val the scalar
  ** \param M1 the %Matrix3d  
  ** \return resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> operator*(const T& val, 
			const Matrix3d<T>& M1);
  
  /*!
  ** \brief pointwise division of two %Matrix3d
  ** \param M1 first %Matrix3d 
  ** \param M2 seconf %Matrix3d
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> operator/(const Matrix3d<T>& M1, 
			const Matrix3d<T>& M2);

  /*!
  ** \brief division of a scalar to each element of a %Matrix3d
  ** \param M1 the %Matrix3d 
  ** \param val the scalar
  ** \return resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> operator/(const Matrix3d<T>& M1, 
			const T& val);

 

  /*!
  ** \relates Matrix3d
  ** \brief Returns the min element of a Matrix3d
  ** \param M1 the Matrix3d  
  ** \return the min element
  */
  template<typename T>
  T& min(const Matrix3d<T>& M1);
  
  /*!
  ** \relates Matrix3d
  ** \brief Returns the max element of a Matrix3d
  ** \param M1 the Matrix3d  
  ** \return the min element
  */
  template<typename T>
  T& max(const Matrix3d<T>& M1);

  /*!
  ** \relates Matrix3d
  ** \brief Returns the abs value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> abs(const Matrix3d<T>& V);

  /*!
  ** \relates Matrix3d
  ** \brief Returns the sqrt value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> sqrt(const Matrix3d<T>& V);

  /*!
  ** \relates Matrix3d
  ** \brief Returns the cos value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> cos(const Matrix3d<T>& V);

  /*!
  ** \relates Matrix3d
  ** \brief Returns the acos value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> acos(const Matrix3d<T>& V);

  /*!
  ** \relates Matrix3d
  ** \brief Returns the sin value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> sin(const Matrix3d<T>& V);

  /*!
  ** \relates Matrix3d
  ** \brief Returns the sin value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> asin(const Matrix3d<T>& V);


  /*!
  ** \relates Matrix3d
  ** \brief Returns the tan value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> tan(const Matrix3d<T>& V);

  /*!
  ** \relates Matrix3d
  ** \brief Returns the atan value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> atan(const Matrix3d<T>& V);

  /*!
  ** \relates Matrix3d
  ** \brief Returns the exp value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> exp(const Matrix3d<T>& V);

  /*!
  ** \relates Matrix3d
  ** \brief Returns the log value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> log(const Matrix3d<T>& V);

  /*!
  ** \relates Matrix3d
  ** \brief Returns the cosh value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> cosh(const Matrix3d<T>& V);

  /*!
  ** \relates Matrix3d
  ** \brief Returns the sinh value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> sinh(const Matrix3d<T>& V);

  /*!
  ** \relates Matrix3d
  ** \brief Returns the tanh value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> tanh(const Matrix3d<T>& V);

  /*!
  ** \relates Matrix3d
  ** \brief Returns the log10 value of each element of the %Matrix3d
  ** \param V The %Matrix3d  
  ** \return the resulting %Matrix3d
  */
  template<typename T>
  Matrix3d<T> log10(const Matrix3d<T>& V);
  

}//slip::



namespace slip
{
  //////////////////////////////////////////
  //   Constructors
  //////////////////////////////////////////
  template<typename T>
  inline
  Matrix3d<T>::Matrix3d():
    array_(new slip::Array3d<T>())
  {}
  
  template<typename T>
  inline
  Matrix3d<T>::Matrix3d(const typename Matrix3d<T>::size_type d1,
			const typename Matrix3d<T>::size_type d2,
			const typename Matrix3d<T>::size_type d3):
    array_(new slip::Array3d<T>(d1,d2,d3))
  {}

  template<typename T>
  inline
  Matrix3d<T>::Matrix3d(const typename Matrix3d<T>::size_type d1,
			const typename Matrix3d<T>::size_type d2,
			const typename Matrix3d<T>::size_type d3,
			const T& val):
    array_(new slip::Array3d<T>(d1,d2,d3,val))
  {}

  template<typename T>
  inline
  Matrix3d<T>::Matrix3d(const typename Matrix3d<T>::size_type d1,
			const typename Matrix3d<T>::size_type d2,
			const typename Matrix3d<T>::size_type d3,
			const T* val):
    array_(new slip::Array3d<T>(d1,d2,d3,val))
  {}


  template<typename T>
  inline
  Matrix3d<T>::Matrix3d(const Matrix3d<T>& rhs):
    array_(new slip::Array3d<T>((*rhs.array_)))
  {}

  template<typename T>
  inline
  Matrix3d<T>::~Matrix3d()
  {
    delete array_;
  }
  
  ///////////////////////////////////////////
  

  //////////////////////////////////////////
  //   Assignment operators
  //////////////////////////////////////////
  template<typename T>
  inline
  Matrix3d<T>&  Matrix3d<T>::operator=(const Matrix3d<T> & rhs)
  {
    if(this != &rhs)
      {
	*array_ = *(rhs.array_);
      }
    return *this;
  }

  template<typename T>
  inline
  Matrix3d<T>& Matrix3d<T>::operator=(const T& value)
  {
    std::fill_n((*array_)[0][0],array_->size(),value);
    return *this;
  }

  template<typename T>
  inline
  void Matrix3d<T>::resize(const typename Matrix3d<T>::size_type d1,
			   const typename Matrix3d<T>::size_type d2,
			   const typename Matrix3d<T>::size_type d3,
			   const T& val)
  {
    array_->resize(d1,d2,d3,val);
  }
  ///////////////////////////////////////////


  //////////////////////////////////////////
  //   Iterators
  //////////////////////////////////////////
  
  //****************************************************************************
  //                          One dimensionnal iterators
  //****************************************************************************
  
  
  
  //----------------------Global iterators------------------------------
  
  template<typename T>
  inline
  typename Matrix3d<T>::iterator Matrix3d<T>::begin()
  {
    return array_->begin();
  }

  template<typename T>
  inline
  typename Matrix3d<T>::iterator Matrix3d<T>::end()
  {
    return array_->end();
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator Matrix3d<T>::begin() const
  {
    Array3d<T> const * tp(array_);
    return tp->begin();
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator Matrix3d<T>::end() const
  {
    Array3d<T> const * tp(array_);
    return tp->end();
  }
  

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator Matrix3d<T>::rbegin()
  {
    return typename Matrix3d<T>::reverse_iterator(this->end());
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator Matrix3d<T>::rend()
  {
    return typename Matrix3d<T>::reverse_iterator(this->begin());
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator Matrix3d<T>::rbegin() const
  {
    return typename Matrix3d<T>::const_reverse_iterator(this->end());
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator Matrix3d<T>::rend() const
  {
    return typename Matrix3d<T>::const_reverse_iterator(this->begin());
  }

  //--------------------simple slice iterators----------------------

  template<typename T>
  inline
  typename Matrix3d<T>::slice_iterator 
  Matrix3d<T>::slice_begin(const typename Matrix3d<T>::size_type row, 
			   const typename Matrix3d<T>::size_type col)
  {
    return array_->slice_begin(row,col);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_slice_iterator 
  Matrix3d<T>::slice_begin(const typename Matrix3d<T>::size_type row, 
			   const typename Matrix3d<T>::size_type col) const
  {
    Array3d<T> const * tp(array_);
    return tp->slice_begin(row,col);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::slice_iterator 
  Matrix3d<T>::slice_end(const typename Matrix3d<T>::size_type row, 
			 const typename Matrix3d<T>::size_type col)
  {
    return array_->slice_end(row,col);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_slice_iterator 
  Matrix3d<T>::slice_end(const typename Matrix3d<T>::size_type row, 
			 const typename Matrix3d<T>::size_type col) const
  {
    Array3d<T> const * tp(array_);
    return tp->slice_end(row,col);
  }


  template<typename T>
  inline
  typename Matrix3d<T>::reverse_slice_iterator 
  Matrix3d<T>::slice_rbegin(const typename Matrix3d<T>::size_type row, 
			    const typename Matrix3d<T>::size_type col)
  {
    return array_->slice_rbegin(row,col);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_slice_iterator 
  Matrix3d<T>::slice_rbegin(const typename Matrix3d<T>::size_type row, 
			    const typename Matrix3d<T>::size_type col) const
  {
    Array3d<T> const * tp(array_);
    return tp->slice_rbegin(row,col);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_slice_iterator 
  Matrix3d<T>::slice_rend(const typename Matrix3d<T>::size_type row, 
			  const typename Matrix3d<T>::size_type col)
  {
    return array_->slice_rend(row,col);
  }

  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_slice_iterator
  Matrix3d<T>::slice_rend(const typename Matrix3d<T>::size_type row, 
			  const typename Matrix3d<T>::size_type col) const
  {
    Array3d<T> const * tp(array_);
    return tp->slice_rend(row,col);
  }

  //--------------------simple row iterators----------------------

  template<typename T>
  inline
  typename Matrix3d<T>::row_iterator 
  Matrix3d<T>::row_begin(const typename Matrix3d<T>::size_type slice,
			 const typename Matrix3d<T>::size_type row)
  {
    return array_->row_begin(slice,row);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_row_iterator 
  Matrix3d<T>::row_begin(const typename Matrix3d<T>::size_type slice,
			 const typename Matrix3d<T>::size_type row) const
  {
    Array3d<T> const * tp(array_);
    return tp->row_begin(slice,row);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::row_iterator 
  Matrix3d<T>::row_end(const typename Matrix3d<T>::size_type slice,
		       const typename Matrix3d<T>::size_type row)
  {
    return array_->row_end(slice,row);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_row_iterator 
  Matrix3d<T>::row_end(const typename Matrix3d<T>::size_type slice,
		       const typename Matrix3d<T>::size_type row) const
  {
    Array3d<T> const * tp(array_);
    return tp->row_end(slice,row);
  }


  template<typename T>
  inline
  typename Matrix3d<T>::reverse_row_iterator 
  Matrix3d<T>::row_rbegin(const typename Matrix3d<T>::size_type slice,
			  const typename Matrix3d<T>::size_type row)
  {
    return array_->row_rbegin(slice,row);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_row_iterator 
  Matrix3d<T>::row_rbegin(const typename Matrix3d<T>::size_type slice,
			  const typename Matrix3d<T>::size_type row) const
  {
    Array3d<T> const * tp(array_);
    return tp->row_rbegin(slice,row);
  }


  template<typename T>
  inline
  typename Matrix3d<T>::reverse_row_iterator 
  Matrix3d<T>::row_rend(const typename Matrix3d<T>::size_type slice,
			const typename Matrix3d<T>::size_type row)
  {
    return array_->row_rend(slice,row);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_row_iterator 
  Matrix3d<T>::row_rend(const typename Matrix3d<T>::size_type slice,
			const typename Matrix3d<T>::size_type row) const
  {
    Array3d<T> const * tp(array_);
    return tp->row_rend(slice,row);
  }

  //--------------------Simple col iterators----------------------
  
  template<typename T>
  inline
  typename Matrix3d<T>::col_iterator 
  Matrix3d<T>::col_begin(const typename Matrix3d<T>::size_type slice,
			 const typename Matrix3d<T>::size_type col)
  {
    return array_->col_begin(slice,col);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_col_iterator 
  Matrix3d<T>::col_begin(const typename Matrix3d<T>::size_type slice,
			 const typename Matrix3d<T>::size_type col) const
  {
    Array3d<T> const * tp(array_);
    return tp->col_begin(slice,col);
  }


  template<typename T>
  inline
  typename Matrix3d<T>::col_iterator 
  Matrix3d<T>::col_end(const typename Matrix3d<T>::size_type slice,
		       const typename Matrix3d<T>::size_type col)
  {
    return array_->col_end(slice,col);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_col_iterator 
  Matrix3d<T>::col_end(const typename Matrix3d<T>::size_type slice,
		       const typename Matrix3d<T>::size_type col) const
  {
    Array3d<T> const * tp(array_);
    return tp->col_end(slice,col);
  }

 
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_col_iterator 
  Matrix3d<T>::col_rbegin(const typename Matrix3d<T>::size_type slice,
			  const typename Matrix3d<T>::size_type col)
  {
    return array_->col_rbegin(slice,col);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_col_iterator 
  Matrix3d<T>::col_rbegin(const typename Matrix3d<T>::size_type slice,
			  const typename Matrix3d<T>::size_type col) const
  {
    Array3d<T> const * tp(array_);
    return tp->col_rbegin(slice,col);
  }


  template<typename T>
  inline
  typename Matrix3d<T>::reverse_col_iterator 
  Matrix3d<T>::col_rend(const typename Matrix3d<T>::size_type slice,
			const typename Matrix3d<T>::size_type col)
  {
    return array_->col_rend(slice,col);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_col_iterator 
  Matrix3d<T>::col_rend(const typename Matrix3d<T>::size_type slice,
			const typename Matrix3d<T>::size_type col) const
  {
    Array3d<T> const * tp(array_);
    return tp->col_rend(slice,col);
  }

  //--------------------Constant slice range iterators----------------------

  template<typename T>
  inline
  typename Matrix3d<T>::slice_range_iterator 
  Matrix3d<T>::slice_begin(const typename Matrix3d<T>::size_type row, 
			   const typename Matrix3d<T>::size_type col, 
			   const slip::Range<int>& range)
  {
    return array_->slice_begin(row,col,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_slice_range_iterator 
  Matrix3d<T>::slice_begin(const typename Matrix3d<T>::size_type row, 
			   const typename Matrix3d<T>::size_type col,
			   const slip::Range<int>& range) const
  {
    Array3d<T> const * tp(array_);
    return tp->slice_begin(row,col,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::slice_range_iterator 
  Matrix3d<T>::slice_end(const typename Matrix3d<T>::size_type row, 
			 const typename Matrix3d<T>::size_type col,
			 const slip::Range<int>& range)
  {
    return array_->slice_end(row,col,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_slice_range_iterator 
  Matrix3d<T>::slice_end(const typename Matrix3d<T>::size_type row,
			 const typename Matrix3d<T>::size_type col,
			 const slip::Range<int>& range) const
  {
    Array3d<T> const * tp(array_);
    return tp->slice_end(row,col,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_slice_range_iterator 
  Matrix3d<T>::slice_rbegin(const typename Matrix3d<T>::size_type row, 
			    const typename Matrix3d<T>::size_type col,
			    const slip::Range<int>& range)
  {
    return array_->slice_rbegin(row,col,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_slice_range_iterator 
  Matrix3d<T>::slice_rbegin(const typename Matrix3d<T>::size_type row, const typename Matrix3d<T>::size_type col,
			   const slip::Range<int>& range) const
  {
    Array3d<T> const * tp(array_);
    return tp->slice_rbegin(row,col,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_slice_range_iterator 
  Matrix3d<T>::slice_rend(const typename Matrix3d<T>::size_type row, const typename Matrix3d<T>::size_type col,
			 const slip::Range<int>& range)
  {
    return array_->slice_rend(row,col,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_slice_range_iterator 
  Matrix3d<T>::slice_rend(const typename Matrix3d<T>::size_type row, const typename Matrix3d<T>::size_type col,
			 const slip::Range<int>& range) const
  {
    Array3d<T> const * tp(array_);
    return tp->slice_rend(row,col,range);
  }
  
 
  //--------------------Constant row range iterators----------------------

  template<typename T>
  inline
  typename Matrix3d<T>::row_range_iterator 
  Matrix3d<T>::row_begin(const typename Matrix3d<T>::size_type slice, 
			 const typename Matrix3d<T>::size_type row,
			 const slip::Range<int>& range)
  {
    return array_->row_begin(slice,row,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_row_range_iterator 
  Matrix3d<T>::row_begin(const typename Matrix3d<T>::size_type slice,
			 const typename Matrix3d<T>::size_type row,
			 const slip::Range<int>& range) const
  {
    Array3d<T> const * tp(array_);
    return tp->row_begin(slice,row,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::row_range_iterator 
  Matrix3d<T>::row_end(const typename Matrix3d<T>::size_type slice,
		       const typename Matrix3d<T>::size_type row,
		       const slip::Range<int>& range)
  {
    return array_->row_end(slice,row,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_row_range_iterator 
  Matrix3d<T>::row_end(const typename Matrix3d<T>::size_type slice,
		       const typename Matrix3d<T>::size_type row,
		       const slip::Range<int>& range) const
  {
    Array3d<T> const * tp(array_);
    return tp->row_end(slice,row,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_row_range_iterator 
  Matrix3d<T>::row_rbegin(const typename Matrix3d<T>::size_type slice,
			  const typename Matrix3d<T>::size_type row,
			  const slip::Range<int>& range)
  {
    return array_->row_rbegin(slice,row,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_row_range_iterator 
  Matrix3d<T>::row_rbegin(const typename Matrix3d<T>::size_type slice,
			  const typename Matrix3d<T>::size_type row,
			  const slip::Range<int>& range) const
  {
    Array3d<T> const * tp(array_);
    return tp->row_rbegin(slice,row,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_row_range_iterator 
  Matrix3d<T>::row_rend(const typename Matrix3d<T>::size_type slice,
			const typename Matrix3d<T>::size_type row,
			const slip::Range<int>& range)
  {
    return array_->row_rend(slice,row,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_row_range_iterator 
  Matrix3d<T>::row_rend(const typename Matrix3d<T>::size_type slice,
			const typename Matrix3d<T>::size_type row,
			const slip::Range<int>& range) const
  {
    Array3d<T> const * tp(array_);
    return tp->row_rend(slice,row,range);
  }

  //--------------------Constant col range iterators----------------------  

  template<typename T>
  inline
  typename Matrix3d<T>::col_range_iterator 
  Matrix3d<T>::col_begin(const typename Matrix3d<T>::size_type slice,
			 const typename Matrix3d<T>::size_type col,
			 const slip::Range<int>& range)
  {
    return array_->col_begin(slice,col,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_col_range_iterator 
  Matrix3d<T>::col_begin(const typename Matrix3d<T>::size_type slice,
			 const typename Matrix3d<T>::size_type col,
			 const slip::Range<int>& range) const
  {
    Array3d<T> const * tp(array_);
    return tp->col_begin(slice,col,range);
  }
 
  template<typename T>
  inline
  typename Matrix3d<T>::col_range_iterator 
  Matrix3d<T>::col_end(const typename Matrix3d<T>::size_type slice,
		       const typename Matrix3d<T>::size_type col,
		       const slip::Range<int>& range)
  {
    return array_->col_end(slice,col,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_col_range_iterator 
  Matrix3d<T>::col_end(const typename Matrix3d<T>::size_type slice,
		       const typename Matrix3d<T>::size_type col,
		       const slip::Range<int>& range) const
  {
    Array3d<T> const * tp(array_);
    return tp->col_end(slice,col,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_col_range_iterator 
  Matrix3d<T>::col_rbegin(const typename Matrix3d<T>::size_type slice,
			  const typename Matrix3d<T>::size_type col,
			  const slip::Range<int>& range)
  {
    return array_->col_rbegin(slice,col,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_col_range_iterator 
  Matrix3d<T>::col_rbegin(const typename Matrix3d<T>::size_type slice,
			  const typename Matrix3d<T>::size_type col,
			  const slip::Range<int>& range) const
  {
    Array3d<T> const * tp(array_);
    return tp->col_rbegin(slice,col,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_col_range_iterator 
  Matrix3d<T>::col_rend(const typename Matrix3d<T>::size_type slice,
			const typename Matrix3d<T>::size_type col,
			const slip::Range<int>& range)
  {
    return array_->col_rend(slice,col,range);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_col_range_iterator 
  Matrix3d<T>::col_rend(const typename Matrix3d<T>::size_type slice,
			const typename Matrix3d<T>::size_type col,
			const slip::Range<int>& range) const
  {
    Array3d<T> const * tp(array_);
    return tp->col_rend(slice,col,range);
  }


  //-------------------plane global iterators----------

  template<typename T>
  inline
  typename Matrix3d<T>::iterator 
  Matrix3d<T>::plane_begin(const typename Matrix3d<T>::size_type slice)
  {
    return array_->plane_begin(slice);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator 
  Matrix3d<T>::plane_begin(const typename Matrix3d<T>::size_type slice) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_begin(slice);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::iterator 
  Matrix3d<T>::plane_end(const typename Matrix3d<T>::size_type slice)
  {
    return array_->plane_end(slice);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator 
  Matrix3d<T>::plane_end(const typename Matrix3d<T>::size_type slice) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_end(slice);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator 
  Matrix3d<T>::plane_rbegin(const typename Matrix3d<T>::size_type slice)
  {
   return array_->plane_rbegin(slice);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator 
  Matrix3d<T>::plane_rbegin(const typename Matrix3d<T>::size_type slice) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_rbegin(slice);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator 
  Matrix3d<T>::plane_rend(const typename Matrix3d<T>::size_type slice)
  {
   return array_->plane_rend(slice);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator 
  Matrix3d<T>::plane_rend(const typename Matrix3d<T>::size_type slice) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_rend(slice);
  }

#ifdef ALL_PLANE_ITERATOR3D

 //-------------------plane row col iterators----------
  

  template<typename T>
  inline
  typename Matrix3d<T>::iterator1d 
  Matrix3d<T>::plane_row_begin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			       const typename Matrix3d<T>::size_type row)
  {
    return array_->plane_row_begin(P,plane_coordinate,row);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator1d  
  Matrix3d<T>::plane_row_begin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			       const typename Matrix3d<T>::size_type row) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_row_begin(P,plane_coordinate,row);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::iterator1d 
  Matrix3d<T>::plane_row_end(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate, 
			     const typename Matrix3d<T>::size_type row)
  {
    return array_->plane_row_end(P,plane_coordinate,row);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator1d 
  Matrix3d<T>::plane_row_end(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			     const typename Matrix3d<T>::size_type row) const
  { 
    Array3d<T> const * tp(array_);
    return tp->plane_row_end(P,plane_coordinate,row);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator1d 
  Matrix3d<T>::plane_row_rbegin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
				const typename Matrix3d<T>::size_type row)
  {
    return array_->plane_row_rbegin(P,plane_coordinate,row);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator1d 
  Matrix3d<T>::plane_row_rbegin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
				const typename Matrix3d<T>::size_type row) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_row_rbegin(P,plane_coordinate,row);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator1d 
  Matrix3d<T>::plane_row_rend(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate, 
			      const typename Matrix3d<T>::size_type row)
  {
    return array_->plane_row_rend(P,plane_coordinate,row);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator1d 
  Matrix3d<T>::plane_row_rend(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			      const typename Matrix3d<T>::size_type row) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_row_rend(P,plane_coordinate,row);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::iterator1d 
  Matrix3d<T>::plane_col_begin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			       const typename Matrix3d<T>::size_type col)
  {
    return array_->plane_col_begin(P,plane_coordinate,col);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator1d 
  Matrix3d<T>::plane_col_begin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			       const typename Matrix3d<T>::size_type col) const
  { 
    Array3d<T> const * tp(array_);
    return tp->plane_col_begin(P,plane_coordinate,col);
  }
    
  template<typename T>
  inline
  typename Matrix3d<T>::iterator1d 
  Matrix3d<T>::plane_col_end(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate, 
			     const typename Matrix3d<T>::size_type col)
  {
    return array_->plane_col_end(P,plane_coordinate,col);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator1d 
  Matrix3d<T>::plane_col_end(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			     const typename Matrix3d<T>::size_type col) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_col_end(P,plane_coordinate,col);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator1d 
  Matrix3d<T>::plane_col_rbegin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
				const typename Matrix3d<T>::size_type col)
  {
    return array_->plane_col_rbegin(P,plane_coordinate,col);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator1d 
  Matrix3d<T>::plane_col_rbegin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
				const typename Matrix3d<T>::size_type col) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_col_rbegin(P,plane_coordinate,col);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator1d 
  Matrix3d<T>::plane_col_rend(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate, 
			      const typename Matrix3d<T>::size_type col)
  { 
    return array_->plane_col_rend(P,plane_coordinate,col);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator1d 
  Matrix3d<T>::plane_col_rend(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			      const typename Matrix3d<T>::size_type col) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_col_rend(P,plane_coordinate,col);
  }

  //-------------------plane box iterators----------

  
  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator1d 
  Matrix3d<T>::plane_row_begin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			      const size_type row, const Box2d<int> & b) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_row_begin(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::iterator1d 
  Matrix3d<T>::plane_row_begin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			      const size_type row, const Box2d<int> & b)
  {
    return array_->plane_row_begin(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::iterator1d
  Matrix3d<T>:: plane_row_end(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate, 
			     const size_type row, const Box2d<int> & b)
  {
    return array_->plane_row_end(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator1d 
  Matrix3d<T>::plane_row_end(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			    const size_type row, const Box2d<int> & b) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_row_end(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator1d
  Matrix3d<T>::plane_row_rbegin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			       const size_type row, const Box2d<int> & b)
  {
    return array_->plane_row_rbegin(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator1d
  Matrix3d<T>::plane_row_rbegin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			       const size_type row, const Box2d<int> & b) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_row_rbegin(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator1d
  Matrix3d<T>::plane_row_rend(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			     const size_type row, const Box2d<int> & b)
  {
    return array_->plane_row_rend(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator1d
  Matrix3d<T>::plane_row_rend(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			     const size_type row, const Box2d<int> & b) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_row_rend(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::iterator1d
  Matrix3d<T>::plane_col_begin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			      const size_type col, const Box2d<int> & b)
  {
    return array_->plane_col_begin(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator1d
  Matrix3d<T>::plane_col_begin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			      const size_type col, const Box2d<int> & b) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_col_begin(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::iterator1d
  Matrix3d<T>::plane_col_end(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate, 
			    const size_type col, const Box2d<int> & b)
  {
    return array_->plane_col_end(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator1d
  Matrix3d<T>::plane_col_end(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			    const size_type col, const Box2d<int> & b) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_col_end(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator1d
  Matrix3d<T>::plane_col_rbegin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			       const size_type col, const Box2d<int> & b)
  {
    return array_->plane_col_rbegin(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator1d
  Matrix3d<T>::plane_col_rbegin(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			       const size_type col, const Box2d<int> & b) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_col_rbegin(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator1d
  Matrix3d<T>::plane_col_rend(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate, 
			     const size_type col, const Box2d<int> & b)
  {
    return array_->plane_col_rend(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator1d
  Matrix3d<T>::plane_col_rend(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate,
			     const size_type col, const Box2d<int> & b) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_col_rend(P,plane_coordinate,col,b);
  }
#endif //ALL_PLANE_ITERATOR3D

  //****************************************************************************
  //                          Two dimensionnal plane iterators
  //****************************************************************************
  
  //------------------------ Global iterators------------------------------------
  
  template<typename T>
  inline
  typename Matrix3d<T>::iterator2d 
  Matrix3d<T>::plane_upper_left(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate)
  {
    return array_->plane_upper_left(P,plane_coordinate);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::iterator2d 
  Matrix3d<T>::plane_bottom_right(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate)
  { 
    return array_->plane_bottom_right(P,plane_coordinate);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator2d 
  Matrix3d<T>::plane_upper_left(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate) const
  { 
    Array3d<T> const * tp(array_);
    return tp->plane_upper_left(P,plane_coordinate);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator2d 
  Matrix3d<T>::plane_bottom_right(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_bottom_right(P,plane_coordinate);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator2d 
  Matrix3d<T>::plane_rupper_left(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate)
  {
    return array_->plane_rupper_left(P,plane_coordinate);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator2d 
  Matrix3d<T>::plane_rbottom_right(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate)
  { 
    return array_->plane_rbottom_right(P,plane_coordinate);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator2d 
  Matrix3d<T>::plane_rupper_left(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate) const
  { 
    Array3d<T> const * tp(array_);
    return tp->plane_rupper_left(P,plane_coordinate);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator2d 
  Matrix3d<T>::plane_rbottom_right(PLANE_ORIENTATION P, const typename Matrix3d<T>::size_type plane_coordinate) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_rbottom_right(P,plane_coordinate);
  }

  //------------------------ box iterators------------------------------------

  template<typename T>
  inline
  typename Matrix3d<T>::iterator2d 
  Matrix3d<T>::plane_upper_left(PLANE_ORIENTATION P, const typename Matrix3d::size_type plane_coordinate, const Box2d<int>& b)
  {
    return array_->plane_upper_left(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::iterator2d 
  Matrix3d<T>::plane_bottom_right(PLANE_ORIENTATION P, const typename Matrix3d::size_type plane_coordinate, const Box2d<int>& b)
  { 
    return array_->plane_bottom_right(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator2d 
  Matrix3d<T>::plane_upper_left(PLANE_ORIENTATION P, const typename Matrix3d::size_type plane_coordinate, const Box2d<int>& b) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_upper_left(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator2d 
  Matrix3d<T>::plane_bottom_right(PLANE_ORIENTATION P, const typename Matrix3d::size_type plane_coordinate, const Box2d<int>& b) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_bottom_right(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator2d 
  Matrix3d<T>::plane_rupper_left(PLANE_ORIENTATION P, const typename Matrix3d::size_type plane_coordinate, const Box2d<int>& b)
  {
    return array_->plane_rupper_left(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator2d 
  Matrix3d<T>::plane_rbottom_right(PLANE_ORIENTATION P, const typename Matrix3d::size_type plane_coordinate, const Box2d<int>& b)
  { 
    return array_->plane_rbottom_right(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator2d 
  Matrix3d<T>::plane_rupper_left(PLANE_ORIENTATION P, const typename Matrix3d::size_type plane_coordinate, const Box2d<int>& b) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_rupper_left(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator2d 
  Matrix3d<T>::plane_rbottom_right(PLANE_ORIENTATION P, const typename Matrix3d::size_type plane_coordinate, 
				   const Box2d<int>& b) const
  {
    Array3d<T> const * tp(array_);
    return tp->plane_rbottom_right(P,plane_coordinate,b);
  }

  //****************************************************************************
  //                          Three dimensionnal iterators
  //****************************************************************************
  
  //------------------------ Global iterators------------------------------------

  template<typename T>
  inline
  typename Matrix3d<T>::iterator3d Matrix3d<T>::front_upper_left()
  {
    return array_->front_upper_left();
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator3d Matrix3d<T>::front_upper_left() const
  {
    Array3d<T> const * tp(array_);
    return tp->front_upper_left();
  }

  template<typename T>
  inline
  typename Matrix3d<T>::iterator3d Matrix3d<T>::back_bottom_right()
  {
    return array_->back_bottom_right();
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator3d Matrix3d<T>::back_bottom_right() const
  {
    Array3d<T> const * tp(array_);
    return tp->back_bottom_right();
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator3d 
  Matrix3d<T>::rback_bottom_right()
  {
    return array_->rback_bottom_right();
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator3d 
  Matrix3d<T>::rback_bottom_right() const
  {
    Array3d<T> const * tp(array_);
    return tp->rback_bottom_right();
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator3d 
  Matrix3d<T>::rfront_upper_left()
  {
    return array_->rfront_upper_left();
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator3d 
  Matrix3d<T>::rfront_upper_left() const
  {
    Array3d<T> const * tp(array_);
    return tp->rfront_upper_left();
  }

  //------------------------ Box iterators------------------------------------

  template<typename T>
  inline
  typename Matrix3d<T>::iterator3d Matrix3d<T>::front_upper_left(const Box3d<int>& box)
  {
    return array_->front_upper_left(box);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator3d Matrix3d<T>::front_upper_left(const Box3d<int>& box) const
  {
    Array3d<T> const * tp(array_);
    return tp->front_upper_left(box);
  }


  template<typename T>
  inline
  typename Matrix3d<T>::iterator3d 
  Matrix3d<T>::back_bottom_right(const Box3d<int>& box)
  {
    return array_->back_bottom_right(box);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator3d 
  Matrix3d<T>::back_bottom_right(const Box3d<int>& box) const
  {
    Array3d<T> const * tp(array_);
    return tp->back_bottom_right(box);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator3d 
  Matrix3d<T>::rback_bottom_right(const Box3d<int>& box)
  {
    return array_->rback_bottom_right(box);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator3d 
  Matrix3d<T>::rback_bottom_right(const Box3d<int>& box) const
  {
    Array3d<T> const * tp(array_);
    return tp->rback_bottom_right(box);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator3d 
  Matrix3d<T>::rfront_upper_left(const Box3d<int>& box)
  {
    return array_->rfront_upper_left(box);
  }

  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator3d 
  Matrix3d<T>::rfront_upper_left(const Box3d<int>& box) const
  {
    Array3d<T> const * tp(array_);
    return tp->rfront_upper_left(box);
  }

  //------------------------ Range iterators------------------------------------

  template<typename T>
  inline
  typename Matrix3d<T>::iterator3d_range 
  Matrix3d<T>::front_upper_left(const Range<int>& slice_range, 
			  const Range<int>& row_range,
			  const Range<int>& col_range)
  {
    return array_->front_upper_left(slice_range,row_range,col_range);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::iterator3d_range 
  Matrix3d<T>::back_bottom_right(const Range<int>& slice_range,
			    const Range<int>& row_range,
			    const Range<int>& col_range)
  {
    return array_->back_bottom_right(slice_range,row_range,col_range);
  }
  

  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator3d_range 
  Matrix3d<T>::front_upper_left(const Range<int>& slice_range,
			  const Range<int>& row_range,
			  const Range<int>& col_range) const
  {
    Array3d<T> const * tp(array_);
    return tp->front_upper_left(slice_range,row_range,col_range);
  }


  template<typename T>
  inline
  typename Matrix3d<T>::const_iterator3d_range 
  Matrix3d<T>::back_bottom_right(const Range<int>& slice_range,
			    const Range<int>& row_range,
			    const Range<int>& col_range) const
  {
    Array3d<T> const * tp(array_);
    return tp->back_bottom_right(slice_range,row_range,col_range);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator3d_range 
  Matrix3d<T>::rfront_upper_left(const Range<int>& slice_range,
			   const Range<int>& row_range,
			   const Range<int>& col_range)
  {
    return array_->rfront_upper_left(slice_range,row_range,col_range);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator3d_range 
  Matrix3d<T>::rfront_upper_left(const Range<int>& slice_range,
			   const Range<int>& row_range,
			   const Range<int>& col_range) const
  {
    Array3d<T> const * tp(array_);
    return tp->rfront_upper_left(slice_range,row_range,col_range);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reverse_iterator3d_range 
  Matrix3d<T>::rback_bottom_right(const Range<int>& slice_range,
			     const Range<int>& row_range,
			     const Range<int>& col_range)
  {
    return array_->rback_bottom_right(slice_range,row_range,col_range);
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reverse_iterator3d_range 
  Matrix3d<T>::rback_bottom_right(const Range<int>& slice_range,
			     const Range<int>& row_range,
			     const Range<int>& col_range) const
  {
    Array3d<T> const * tp(array_);
    return tp->rback_bottom_right(slice_range,row_range,col_range);
  }


  ///////////////////////////////////////////



   /** \name i/o operators */
  /* @{ */
  template <typename T>
  inline
  std::ostream& operator<<(std::ostream & out, const Matrix3d<T>& a)
  {
    out<<*(a.array_);
    return out;
  }
  /* @} */
  ///////////////////////////////////////////
  

  ////////////////////////////////////////////
  // Elements access operators
  ////////////////////////////////////////////
  template<typename T>
  inline
  T** 
  Matrix3d<T>::operator[](const typename Matrix3d<T>::size_type k)
  {
    return (*array_)[k];
  }
  
  template<typename T>
  inline
  const T* const*
  Matrix3d<T>::operator[](const typename Matrix3d<T>::size_type k) const 
  {
    return (*array_)[k];
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::reference 
  Matrix3d<T>::operator()(const typename Matrix3d<T>::size_type k,
			  const typename Matrix3d<T>::size_type i,
			  const typename Matrix3d<T>::size_type j)
  {
    return (*array_)[k][i][j];
  }
  
  template<typename T>
  inline
  typename Matrix3d<T>::const_reference 
  Matrix3d<T>::operator()(const typename Matrix3d<T>::size_type k,
			  const typename Matrix3d<T>::size_type i,
			  const typename Matrix3d<T>::size_type j) const
  {
    return (*array_)[k][i][j];
  }
  
  ///////////////////////////////////////////

  template<typename T>
  inline
  std::string 
  Matrix3d<T>::name() const {return "Matrix3d";} 
 

  template<typename T>
  inline
  typename Matrix3d<T>::size_type 
  Matrix3d<T>::dim1() const {return array_->dim1();} 

  template<typename T>
  inline
  typename Matrix3d<T>::size_type 
  Matrix3d<T>::slices() const {return array_->dim1();} 

  template<typename T>
  inline
  typename Matrix3d<T>::size_type 
  Matrix3d<T>::dim2() const {return array_->dim2();} 

  template<typename T>
  inline
  typename Matrix3d<T>::size_type 
  Matrix3d<T>::rows() const {return array_->dim2();} 

  template<typename T>
  inline
  typename Matrix3d<T>::size_type 
  Matrix3d<T>::dim3() const {return array_->dim3();;} 

  template<typename T>
  inline
  typename Matrix3d<T>::size_type 
  Matrix3d<T>::cols() const {return array_->dim3();} 

  template<typename T>
  inline
  typename Matrix3d<T>::size_type 
  Matrix3d<T>::columns() const {return array_->dim3();;} 


  template<typename T>
  inline
  typename Matrix3d<T>::size_type 
  Matrix3d<T>::size() const {return array_->size();}

  template<typename T>
  inline
  typename Matrix3d<T>::size_type 
  Matrix3d<T>::max_size() const {return array_->max_size();}


  template<typename T>
  inline
  typename Matrix3d<T>::size_type 
  Matrix3d<T>::slice_size() const {return array_->slice_size();}

  template<typename T>
  inline
  bool Matrix3d<T>::empty()const {return array_->empty();}

  template<typename T>
  inline
  void Matrix3d<T>::swap(Matrix3d<T>& M)
  {
    array_->swap(*(M.array_));
  }


  ////////////////////////////////////////////
  // comparison operators
  ////////////////////////////////////////////
  /** \name EqualityComparable operators */
  /* @{ */
  template<typename T>
  inline
  bool operator==(const Matrix3d<T>& x, 
		  const Matrix3d<T>& y)
  {
    return ( x.size() == y.size()
	     && std::equal(x.begin(),x.end(),y.begin())); 
  }

  template<typename T>
  inline
  bool operator!=(const Matrix3d<T>& x, 
		  const Matrix3d<T>& y)
  {
    return !(x == y);
  }
  /* @} */

   /** \name LessThanComparable operators */
  /* @{ */
  
  template<typename T>
  inline
  bool operator<(const Matrix3d<T>& x, 
		 const Matrix3d<T>& y)
  {
    return std::lexicographical_compare(x.begin(), x.end(),
					y.begin(), y.end());
  }
  
  
  template<typename T>
  inline
  bool operator>(const Matrix3d<T>& x, 
		 const Matrix3d<T>& y)
  {
    return (y < x);
  }
  
  template<typename T>
  inline
  bool operator<=(const Matrix3d<T>& x, 
		  const Matrix3d<T>& y)
  {
    return !(y < x);
  }

  template<typename T>
  inline
  bool operator>=(const Matrix3d<T>& x, 
		  const Matrix3d<T>& y)
  {
    return !(x < y);
  } 
 /* @} */

  ////////////////////////////////////////////


  ////////////////////////////////////////////
  // arithmetic and mathematic operators
  ////////////////////////////////////////////
  template<typename T>
  inline
  Matrix3d<T>& Matrix3d<T>::operator+=(const T& val)
  {
    std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::plus<T>(),val));
    return *this;
  }
  
  template<typename T>
  inline
  Matrix3d<T>& Matrix3d<T>::operator-=(const T& val)
  {
    std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::minus<T>(),val));
    return *this;
  }

  template<typename T>
  inline
  Matrix3d<T>& Matrix3d<T>::operator*=(const T& val)
  {
    std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::multiplies<T>(),val));
    return *this;
  }
  
  template<typename T>
  inline
  Matrix3d<T>& Matrix3d<T>::operator/=(const T& val)
  {
    std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::divides<T>(),val));
    return *this;
  }
  
  template<typename T>
  inline
  Matrix3d<T> Matrix3d<T>::operator-() const
  {
    Matrix3d<T> tmp(*this);
    std::transform(array_->begin(),array_->end(),tmp.begin(),std::negate<T>());
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T>& Matrix3d<T>::operator+=(const Matrix3d<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    assert(this->dim3() == rhs.dim3());
   
    std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::plus<T>());
    return *this;
  }
  
  template<typename T>
  inline
  Matrix3d<T>& Matrix3d<T>::operator-=(const Matrix3d<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    assert(this->dim3() == rhs.dim3());
    std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::minus<T>());
    return *this;
  }
  
  template<typename T>
  inline
  Matrix3d<T>& Matrix3d<T>::operator*=(const Matrix3d<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    assert(this->dim3() == rhs.dim3());
    std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::multiplies<T>());
    return *this;
  }
  
  template<typename T>
  inline
  Matrix3d<T>& Matrix3d<T>::operator/=(const Matrix3d<T>& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::divides<T>());
    return *this;
  }
	
 

  template<typename T>
  inline
  T& Matrix3d<T>::min() const
  {
    assert(array_->size() != 0);
    return *std::min_element(array_->begin(),array_->end());
  }

  template<typename T>
  inline
  T& Matrix3d<T>::max() const
  {
    assert(array_->size() != 0);
    return *std::max_element(array_->begin(),array_->end());
  }

  template<typename T>
  inline
  T Matrix3d<T>::sum() const
  {
    assert(array_->size() != 0);
    return std::accumulate(array_->begin(),array_->end(),T());
  }

  
  template<typename T>
  inline
  Matrix3d<T>& Matrix3d<T>::apply(T (*fun)(T))
  {
    slip::apply(this->begin(),this->end(),this->begin(),fun);
    return *this;
  }

  template<typename T>
  inline
  Matrix3d<T>& Matrix3d<T>::apply(T (*fun)(const T&))
  {
    slip::apply(this->begin(),this->end(),this->begin(),fun);
    return *this;
  }


  /** \name Arithmetical operators */
  /* @{ */
  template<typename T>
  inline
  Matrix3d<T> operator+(const Matrix3d<T>& M1, 
			const Matrix3d<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    assert(M1.dim3() == M2.dim3());
 
    Matrix3d<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::plus<T>());
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> operator+(const Matrix3d<T>& M1, 
			const T& val)
  {
    Matrix3d<T> tmp(M1);
    tmp+=val;
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> operator+(const T& val,
			const Matrix3d<T>& M1)
  {
    return M1 + val;
  }


  template<typename T>
  inline
  Matrix3d<T> operator-(const Matrix3d<T>& M1, 
			const Matrix3d<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    assert(M1.dim3() == M2.dim3());
      
    Matrix3d<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::minus<T>());
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> operator-(const Matrix3d<T>& M1, 
			const T& val)
  {
    Matrix3d<T> tmp(M1);
    tmp-=val;
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> operator-(const T& val,
			const Matrix3d<T>& M1)
  {
    return -(M1 - val);
  }

  template<typename T>
  inline
  Matrix3d<T> operator*(const Matrix3d<T>& M1, 
			const Matrix3d<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    assert(M1.dim3() == M2.dim3());
 
    Matrix3d<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::multiplies<T>());
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> operator*(const Matrix3d<T>& M1, 
			const T& val)
  {
    Matrix3d<T> tmp(M1);
    tmp*=val;
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> operator*(const T& val,
			const Matrix3d<T>& M1)
  {
    return M1 * val;
  }

  template<typename T>
  inline
  Matrix3d<T> operator/(const Matrix3d<T>& M1, 
			const Matrix3d<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    assert(M1.dim3() == M2.dim3());
 
    Matrix3d<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::divides<T>());
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> operator/(const Matrix3d<T>& M1, 
			const T& val)
  {
    Matrix3d<T> tmp(M1);
    tmp/=val;
    return tmp;
  }

/* @} */

  template<typename T>
  inline
  T& min(const Matrix3d<T>& M1)
  {
    return M1.min();
  }

  template<typename T>
  inline
  T& max(const Matrix3d<T>& M1)
  {
    return M1.max();
  }

  template<typename T>
  inline
  Matrix3d<T> abs(const Matrix3d<T>& M)
  {
  
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::abs);
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> sqrt(const Matrix3d<T>& M)
  {
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::sqrt);
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> cos(const Matrix3d<T>& M)
  {
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::cos);
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> acos(const Matrix3d<T>& M)
  {
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::acos);
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> sin(const Matrix3d<T>& M)
  {
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::sin);
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> asin(const Matrix3d<T>& M)
  {
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::asin);
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> tan(const Matrix3d<T>& M)
  {
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::tan);
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> atan(const Matrix3d<T>& M)
  {
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::atan);
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> exp(const Matrix3d<T>& M)
  {
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::exp);
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> log(const Matrix3d<T>& M)
  {
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::log);
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> cosh(const Matrix3d<T>& M)
  {
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::cosh);
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> sinh(const Matrix3d<T>& M)
  {
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::sinh);
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> tanh(const Matrix3d<T>& M)
  {
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::tanh);
    return tmp;
  }

  template<typename T>
  inline
  Matrix3d<T> log10(const Matrix3d<T>& M)
  {
    Matrix3d<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::log10);
    return tmp;
  }


}//slip::

#endif //SLIP_MATRIX3D_HPP
