/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file OcTreeBox.hpp
 * 
 * \brief Provides a class to define box of a  Point Octree
 * 
 */
#ifndef SLIP_OCTREEBOX_HPP
#define SLIP_OCTREEBOX_HPP

#include <iostream>
#include <string>
#include "macros.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>


namespace slip
{
template <typename Point>
class OcTreeBox;

template <typename Point>
std::ostream& operator<< (std::ostream&,
			  const OcTreeBox<Point>&);


template<typename Point>
bool operator==(const OcTreeBox<Point>& x, 
		const OcTreeBox<Point>& y);

template<typename Point>
bool operator!=(const OcTreeBox<Point>& x, 
		const OcTreeBox<Point>& y);


/*! \class OcTreeBox
** \ingroup Containers PointTreeContainers
**  \brief The class defines box of a Point Octree.
** 
** \author Benoit Tremblais
** \version 0.0.1
** \date 2016/09/05
** \since 1.5.0
** \param Point Type of the Point to define minimal and maximal point of the Box.
** \todo add intersection and union operators
*/
template <typename Point>
class OcTreeBox
{
public:
  typedef typename Point::value_type value_type;
  typedef OcTreeBox<Point> self;
private:
  Point minimal_point_;
  Point maximal_point_;

public:
  /*!
  ** \brief Default constructor
  */
  OcTreeBox():
    minimal_point_(Point()),
    maximal_point_(Point())//,
  {}
  /*!
  ** \brief Constructor a OcTreeBox with  [(xmin,ymin,zmin),(xmax,ymax,zmax)]
  ** \param xmin minimal x coordinate of the %OcTreeBox.
  ** \param ymin minimal y coordinate of the %OcTreeBox.
  ** \param zmin minimal z coordinate of the %OcTreeBox.
  ** \param xmax maximal x coordinate of the %OcTreeBox.
  ** \param ymax maximal y coordinate of the %OcTreeBox.
  ** \param zmax maximal z coordinate of the %OcTreeBox.
  ** \pre xmax > xmin
  ** \pre ymax > ymin
  ** \pre zmax > zmin
  */
  OcTreeBox(const value_type& xmin,
	    const value_type& ymin,
	    const value_type& zmin,
	    const value_type& xmax,
	    const value_type& ymax,
	    const value_type& zmax):
    minimal_point_(Point(xmin,ymin,zmin)),
    maximal_point_(Point(xmax,ymax,zmax))
  {}

 /*!
  ** \brief Constructor a OcTreeBox with  [minimal_point,maximal_point]
  ** \param minimal_point the minimal point of the %OcTreeBox.
  ** \param maximal_point the maximal point of the %OcTreeBox.
  ** \pre maximal_point.x1() > minimal_point.x1()
  ** \pre maximal_point.x2() > minimal_point.x2()
  */
  OcTreeBox(const Point& minimal_point,
	      const Point& maximal_point):
    minimal_point_(minimal_point),
    maximal_point_(maximal_point)
  {}
 
  
  /*!
  ** \brief Returns the name of the class.
  */
  const std::string name() const
  {
    return "OcTreeBox"; 
  }


 /*!
  ** \brief Returns the minimal point of the %OcTreeBox.
  */
  const Point& minimal() const
  {
    return this->minimal_point_;
  }


  /*!
  ** \brief Returns the minimal point x coordinate of the %OcTreeBox.
  */
  const value_type& xmin() const
  {
    return (this->minimal_point_).x1();
  }

  value_type& xmin() 
  {
    return (this->minimal_point_).x1();
  }
  /*!
  ** \brief Returns the maximal point x coordinate of the %OcTreeBox.
  */
  const value_type& xmax() const
  {
    return (this->maximal_point_).x1();
  }

  value_type& xmax()
  {
    return (this->maximal_point_).x1();
  }

  /*!
  ** \brief Returns the  x middle point coordinate of the %OcTreeBox.
  */
  value_type xmiddle() const
  {
    return slip::constants<value_type>::half()*(minimal_point_[0]+maximal_point_[0]);
  }
  
/*!
  ** \brief Returns the minimal point y coordinate of the %OcTreeBox.
  */
  const value_type& ymin() const
  {
    return (this->minimal_point_).x2();
  }

  value_type& ymin()
  {
    return (this->minimal_point_).x2();
  }

  /*!
  ** \brief Returns the maximal point y coordinate of the %OcTreeBox.
  */
  const value_type& ymax() const
  {
    return (this->maximal_point_).x2();
  }

  value_type& ymax() 
  {
    return (this->maximal_point_).x2();
  }

  /*!
  ** \brief Returns the middle y coordinate of the %OcTreeBox.
  */
  value_type ymiddle() const
  {
    return slip::constants<value_type>::half()*(minimal_point_[1]+maximal_point_[1]);
  }
  
  /*!
  ** \brief Returns the minimal point z coordinate of the %OcTreeBox.
  */
  const value_type& zmin() const
  {
    return (this->minimal_point_).x3();
  }

  value_type& zmin()
  {
    return (this->minimal_point_).x3();
  }


  
  /*!
  ** \brief Returns the maximal point z coordinate of the %OcTreeBox.
  */
  const value_type& zmax() const
  {
    return (this->maximal_point_).x3();
  }

  value_type& zmax() 
  {
    return (this->maximal_point_).x3();
  }
/*!
  ** \brief Returns the middle z coordinate of the %OcTreeBox.
  */
  value_type zmiddle() const
  {
    return slip::constants<value_type>::half()*(minimal_point_[2]+maximal_point_[2]);
  }

  
  /*!
  ** \brief Returns the maximal point of the %OcTreeBox.
  */
  const Point& maximal() const
  {
    return this->maximal_point_;
  }

 /*!
  ** \brief Returns front upper left point of the %OcTreeBox.
  ** x = xmin
  ** y = ymax
  ** z = zmax
  */
  Point front_upper_left() const
  {
    return Point(this->xmin(),this->ymax(),this->zmax());
  }
/*!
  ** \brief Returns front upper right point of the %OcTreeBox.
  ** x = xmax
  ** y = ymax
  ** z = zmax
  */
  const Point& front_upper_right() const
  {
    return this->maximal();
  }

  /*!
  ** \brief Returns front bottom left point of the %OcTreeBox.
  ** x = xmin
  ** y = ymin
  ** z = zmax
  */
  Point front_bottom_left() const
  {
    return Point(this->xmin(),this->ymin(),this->zmax());
  }
  
/*!
  ** \brief Returns front bottom right point of the %OcTreeBox.
  ** x = xmax
  ** y = ymin
  ** z = zmax
  */
  Point front_bottom_right() const
  {
    return Point(this->xmax(),this->ymin(),this->zmax());
  }
  
/*!
  ** \brief Returns back upper left point of the %OcTreeBox.
  ** x = xmin
  ** y = ymax
  ** z = zmin
  */
  Point back_upper_left() const
  {
    return Point(this->xmin(),this->ymax(),this->zmin());
  }
/*!
  ** \brief Returns back upper right point of the %OcTreeBox.
  ** x = xmax
  ** y = ymax
  ** z = zmin
  */
  Point back_upper_right() const
  {
    return Point(this->xmax(),this->ymax(),this->zmin());
  }

  /*!
  ** \brief Returns back bottom left point of the %OcTreeBox.
  ** x = xmin
  ** y = ymin
  ** z = zmin
  */
  const Point& back_bottom_left() const
  {
    return this->minimal();
  }
  
/*!
  ** \brief Returns back bottom left point of the %OcTreeBox.
  ** x = xmax
  ** y = ymin
  ** z = zmax
  */
  Point back_bottom_right() const
  {
    return Point(this->xmax(),this->ymin(),this->zmax());
  }
 
  
  /*!
  ** \brief Returns the middle point of the %OcTreeBox.
  ** x_middle = (xmin+xmax)/2
  ** y_middle = (ymin+ymax)/2
  ** z_middle = (zmin+zmax)/2
  */
  Point middle() const
  {
    return Point(this->xmiddle(),
		 this->ymiddle(),
		 this->zmiddle());
  }

   /*!
  ** \brief Returns the middle left point of the %OcTreeBox.
  ** x_middle = xmin
  ** y_middle = (ymin+ymax)/2
  ** z_middle = (zmin+zmax)/2
  */
  Point middle_left() const
  {
    return Point(this->xmin(),
		 this->ymiddle(),
		 this->zmiddle());
  }

  /*!
  ** \brief Returns the middle right point of the %OcTreeBox.
  ** x_middle = xmax
  ** y_middle = (ymin+ymax)/2
  ** z_middle = (zmin+zmax)/2
  */
  Point middle_right() const
  {
    return Point(this->xmax(),
		 this->ymiddle(),
		 this->zmiddle());
  }


  /*!
  ** \brief Returns the upper middle point of the %OcTreeBox.
  ** x_middle = (xmin+xmax)/2
  ** y_middle = ymax
  ** z_middle = (zmin+zmax)/2
  */
  Point middle_upper() const
  {
    return Point(this->xmiddle(),
		 this->ymax(),
		 this->zmiddle());
  }

   /*!
  ** \brief Returns the bottom middle point of the %OcTreeBox.
  ** x_middle = (xmin+xmax)/2
  ** y_middle = ymin
  ** z_middle = (zmin+zmax)/2
  */
  Point middle_bottom() const
  {
    return Point(this->xmiddle(),
		 this->ymin(),
		 this->zmiddle());
  }

   /*!
  ** \brief Returns the front middle point of the %OcTreeBox.
  ** x_middle = (xmin+xmax)/2
  ** y_middle = (ymin+ymax)/2
  ** z_middle = zmax
  */
  Point middle_front() const
  {
    return Point(this->xmiddle(),
		 this->ymiddle(),
		 this->zmax());
  }

    /*!
  ** \brief Returns the back middle point of the %OcTreeBox.
  ** x_middle = (xmin+xmax)/2
  ** y_middle = (ymin+ymax)/2
  ** z_middle = zmin
  */
  Point middle_back() const
  {
     return Point(this->xmiddle(),
		 this->ymiddle(),
		  this->zmin());
  }

  
  /*!
  ** \brief Returns true if (p[0] >= minimal_point_[0]) AND (p[0] <= maximal_point_[0]) AND (p[1] >= minimal_point_[1]) AND  (p[1] <= maximal_point_[1]) AND (p[2] >= minimal_point_[2]) AND  (p[2] <= maximal_point_[2]) else returns false.
  **  \param p a Point.
  **  \return a boolean 
  */
  bool contains(const Point& p) const
  {
    bool result = false;
    if(   (p[0] >= minimal_point_[0])
       && (p[0] <= maximal_point_[0])
       && (p[1] >= minimal_point_[1])
       && (p[1] <= maximal_point_[1])
       && (p[2] >= minimal_point_[2])
       && (p[2] <= maximal_point_[2])
      )
      {
	result = true;
      }
    return result;
  }

   /*!
  ** \brief Returns true if (p[0] >= minimal_point_[0]+magin) AND (p[0] <= maximal_point_[0]-margin) AND (p[1] >= minimal_point_[1]+margin) AND  (p[1] <= maximal_point_[1]-margin) AND (p[2] >= minimal_point_[2]+margin) AND  (p[2] <= maximal_point_[2]-margin) else returns false.
  **  \param p a Point.
  **  \param margin margin 
  **  \return a boolean 
  */
  bool contains(const Point& p,
		const typename Point::value_type& margin) const
  {
    bool result = false;
   
    if(      (p[0] >= (minimal_point_[0] + margin))
	     && (p[0] <= (maximal_point_[0] - margin))
	     && (p[1] >= (minimal_point_[1] + margin))
	     && (p[1] <= (maximal_point_[1] - margin))
	     && (p[2] >= (minimal_point_[2] + margin))
	     && (p[2] <= (maximal_point_[2] - margin))
      )
      {
	result = true;
      }
    return result;
  }

  /*!
  ** \brief Adjusts %OcTreeBox minimal and maximal points if they are not consistent (coordinates of minimal point are greater than coordinates of maximal point).
  */
  void make_consistent()
  {
    if(minimal_point_[0] > maximal_point_[0])
      {
	std::swap(minimal_point_[0],maximal_point_[0]);
      }
    if(minimal_point_[1] > maximal_point_[1])
      {
	std::swap(minimal_point_[1],maximal_point_[1]);
      }
    if(minimal_point_[2] > maximal_point_[2])
      {
	std::swap(minimal_point_[2],maximal_point_[2]);
      }
  }
 /*!
  ** \brief verify if the %OcTreeBox is consistent, that is to say
  ** if the first point p1 has the smaller coordinates than p2
  ** \return true if the "p1 < p2"
  */
  bool is_consistent() const
  {
    bool result = false;
    if(  (minimal_point_[0] < maximal_point_[0])
       &&(minimal_point_[1] < maximal_point_[1])
	 &&(minimal_point_[2] < maximal_point_[2]))
      {
	result = true;
      }
    return result;
  }

  /*!
  ** \brief Swaps two  %OcTreeBox
  ** \param other %OcTreeBox to swap with
  */
  void swap(self& other)
  {
    (this->minimal_point_).swap(other.minimal_point_);
    (this->maximal_point_).swap(other.maximal_point_);
  }
  /*!
  ** \brief Returns the width of the %OcTreeBox.
  */
  const value_type width() const
  {
    return (this->maximal_point_[0]-this->minimal_point_[0]);
  }

  /*!
  ** \brief Returns the height of the %OcTreeBox.
  */
  const value_type height() const
  {
    return (this->maximal_point_[1]-this->minimal_point_[1]);
  }

  /*!
  ** \brief Returns the depth of the %OcTreeBox.
  */
  const value_type depth() const
  {
    return (this->maximal_point_[2]-this->minimal_point_[2]);
  }

  /*!
  ** \brief Returns the volume of the %OcTreeBox.
  */
  const value_type volume() const
  {
    return this->width()*this->height()*this->depth();
  }

  /*!
  ** \brief Print the OcTreeBox.
  */
  friend std::ostream& operator<< <>(std::ostream&,
				     const OcTreeBox<Point>&);
  /*!
  ** \brief Compare two %OcTreeBox. Returns true iff the minimal and maximal point of the  %OcTreeBox are equal.
  ** \param x a %OcTreeBox.
  ** \param y another %OcTreeBox.
  ** 
  ** \return true iff the minimal and maximal point of the  %OcTreeBox are equal.
  */
  friend bool operator== <>(const OcTreeBox<Point>& x, 
			    const OcTreeBox<Point>& y);

  /*!
  ** \brief Compare two %OcTreeBox.  Returns true iff the minimal and maximal point of the  %OcTreeBox are not equal.
  ** \param x a %OcTreeBox.
  ** \param y another %OcTreeBox.
  ** \return true iff the minimal and maximal point of the  %OcTreeBox are not equal.
  */
  friend bool operator!= <>(const OcTreeBox<Point>& x, 
			    const OcTreeBox<Point>& y);


protected:
  friend class boost::serialization::access;
template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
       if(version >= 0)
	{
	  ar & minimal_point_;
	  ar & maximal_point_;
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & minimal_point_;
	  ar & maximal_point_;
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()


};


template <typename Point>
std::ostream& operator<<(std::ostream& flux, 
			 const OcTreeBox<Point>& quadtreebox)
{
  flux <<"OcTreeBox: \n ["<<quadtreebox.minimal()<<","<<quadtreebox.maximal()<<"]";
  return flux;
}


/** \name EqualityComparable functions */
  /* @{ */

template<typename Point>
inline
bool operator==(const OcTreeBox<Point>& x, 
		const OcTreeBox<Point>& y)
{
  return (    (x.minimal() == y.minimal())
	   && (x.maximal() == y.maximal())
	 ); 
}

template<typename Point>
inline
bool operator!=(const OcTreeBox<Point>& x, 
		const OcTreeBox<Point>& y)
{
  return !(x == y);
}

/* @} */

}//slip::

namespace std
{
  template <typename Point>
  inline
  void swap(slip::OcTreeBox<Point>& x,
	    slip::OcTreeBox<Point>& y)
  {
    x.swap(y);
  }
}
#endif //SLIP_OCTREEBOX_HPP
