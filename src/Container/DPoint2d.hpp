/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file DPoint2d.hpp
 * 
 * \brief Provides a class to modelize the difference of slip::Point2d.
 * 
 */
#ifndef SLIP_DPOINT2D_HPP
#define SLIP_DPOINT2D_HPP

#include <iostream>
#include <cassert>
#include <string>
#include "Block.hpp"
#include "DPoint.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>


namespace slip
{

/** \class DPoint2d
 **  \ingroup Containers MiscellaneousContainers
 ** \brief Difference of Point2D class, specialization of DPoint<CoordType,DIM> 
 **        with DIM = 2
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
 ** \version 0.0.2
 ** \date 2014/04/02
 ** \since 1.0.0
 ** \param CoordType Type of the coordinates of the DPoint2d 
 */
template <typename CoordType>
class DPoint2d:public DPoint<CoordType,2>
{
public :
  typedef DPoint2d<CoordType> self;
  typedef DPoint<CoordType,2> base;
  
  /**
  ** \name Constructors & Destructors
  */
  /*@{*/

  /*!
  ** \brief Constructs a DPoint2d.
  */
  DPoint2d();

   /*!
   ** \brief Constructs a DPoint from a CoordType array
   ** \param array
   */
  DPoint2d(const CoordType* array);
 
  /*!
  ** \brief Constructs a DPoint2d.
  ** \param dx1 first coordinate of the DPoint2d
  ** \param dx2 second coordinate of the DPoint2d
  */
  DPoint2d(const CoordType& dx1,
	  const CoordType& dx2);
  
  /*@} End Constructors */

  
  /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Accessor of the first coordinate of DPoint2d.
  ** \param dx first coordinate of the DPoint2d
  */
  void dx1(const CoordType& dx);
  /*!
  ** \brief Accessor/Mutator of the first coordinate of DPoint2d.
  ** \return reference to the first coordinate of the DPoint2d
  */
  CoordType& dx1();
  /*!
  ** \brief Accessor/Mutator of the first coordinate of DPoint2d.
  ** \return reference to the first coordinate of the DPoint2d
  */
  const CoordType& dx1() const;
  /*!
  ** \brief Accessor of the second coordinate of DPoint2d.
  ** \param dx second coordinate of the DPoint2d
  */
  void dx2(const CoordType& dx);
  /*!
  ** \brief Accessor/Mutator of the second coordinate of DPoint2d.
  ** \return reference to the second coordinate of the DPoint2d
  */
  CoordType& dx2();
  /*!
  ** \brief Accessor/Mutator of the second coordinate of DPoint2d.
  ** \return reference to the second coordinate of the DPoint2d
  */
  const CoordType& dx2() const;

  /*!
  ** \brief Accessor/Mutator of the coordinates of DPoint2d.
  ** \param dx1 First displacement coordinate.
  ** \param dx2 Second displacement coordinate.
  */
  void set_coord(const CoordType& dx1, const CoordType& dx2);

   /*@} End Element access operators */

   /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;

private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::DPoint<CoordType,2> >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::DPoint<CoordType,2> >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
};
}//slip::

namespace slip
{
  template<typename CoordType>
  inline
  DPoint2d<CoordType>::DPoint2d()
  {
    this->coord_[0] = CoordType(0);
    this->coord_[1] = CoordType(0);
  }

  template<typename CoordType>
  inline
  DPoint2d<CoordType>::DPoint2d(const CoordType* array):
    DPoint<CoordType,2>(array)
  {}

  template<typename CoordType>
  inline
  DPoint2d<CoordType>::DPoint2d(const CoordType& dx1,
			      const CoordType& dx2)
  {
     this->coord_[0] = CoordType(dx1);
     this->coord_[1] = CoordType(dx2);
  }
  
  template<typename CoordType>
  inline
  void DPoint2d<CoordType>::dx1(const CoordType& dx) {this->coord_[0]=dx;}

  template<typename CoordType>
  inline
  CoordType& DPoint2d<CoordType>::dx1() {return this->coord_[0];}

  template<typename CoordType>
  inline
  const CoordType& DPoint2d<CoordType>::dx1() const {return this->coord_[0];}

  template<typename CoordType>
  inline
  void DPoint2d<CoordType>::dx2(const CoordType& dx) {this->coord_[1]=dx;}

  template<typename CoordType>
  inline
  CoordType& DPoint2d<CoordType>::dx2() {return this->coord_[1];}

  template<typename CoordType>
  inline
  const CoordType& DPoint2d<CoordType>::dx2() const {return this->coord_[1];}

  template<typename CoordType>
  inline
  void DPoint2d<CoordType>::set_coord(const CoordType& dx1, const CoordType& dx2) 
  {
   this->coord_[0]= dx1;
   this->coord_[1] = dx2;
  }
  template<typename CoordType>
  inline
  std::string 
  DPoint2d<CoordType>::name() const {return "DPoint2d";} 


}//slip::




#endif //SLIP_DPOINT2D_HPP
