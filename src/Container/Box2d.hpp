/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Box2d.hpp
 * 
 * \brief Provides a class to manipulate 2d box.
 * 
 */
#ifndef SLIP_BOX2D_HPP
#define SLIP_BOX2D_HPP

#include <iostream>
#include <cassert>
#include <string>
#include "Box.hpp"
#include "Point2d.hpp"
#include "DPoint2d.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{
  
  
  /*! \class Box2d
  **  \ingroup Containers MiscellaneousContainers
  ** \brief This is a %Box2d class, a specialized version of
  ** slip::Box<CoordType,DIM>  with DIM = 2.
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
  ** \version 0.0.3
  ** \date 2014/03/15
  ** \since 1.0.0
  ** \par Description:
  ** The box is defined by its upper-left and bottom-right points p1
  ** and p2.
  ** \code
  **  p1-------- 
  **  |         |
  **  |         |
  **  |         |
  **  |         |
  **   ---------p2
  ** \endcode
  ** \param CoordType Type of the coordinates of the Box2d
  */
  template <typename CoordType>
  class Box2d: public Box<CoordType,2>
  {
  public :
    typedef Box2d<CoordType> self;
    typedef Box<CoordType,2> base;
    
    /**
     ** \name Constructors & Destructors
     */
    /*@{*/
    /*!
    ** \brief Constructs a Box2d.
    ** \remarks call the default constructors of Point<CoordType,2>
    */
    Box2d();
    
    /*!
    ** \brief Constructs a Box2d from a CoordType array.
    ** \param p1 first extremal point
    ** \param p2 second extremal point
    */
    Box2d(const Point<CoordType,2>& p1, 
	  const Point<CoordType,2>& p2);
    
    /*!
    ** \brief Constructs a Box2d.
    ** \param x11 first coordinate of the minimal point p1
    ** \param x12 second coordinate of the minimal point p1
    ** \param x21 first coordinate of the maximal point p2
    ** \param x22 second coordinate of the maximal point p2
    */
    Box2d(const CoordType& x11,
	  const CoordType& x12,
	  const CoordType& x21,
	  const CoordType& x22);
    
    /*!
    ** \brief Constructs a square Box2d using a central point and a width.
    ** \param xc first coordinate of the central point
    ** \param yc second coordinate of the central point
    ** \param w width of the box (real width and height=2*w+1)
    */
    Box2d(const CoordType& xc,
	  const CoordType& yc,
	  const CoordType& w);
    
    /*!
    ** \brief Constructs a square Box2d using a central point and a width.
    ** \param pc central point
    ** \param w width of the box (real width and height=2*w+1)
    */
    Box2d(const Point<CoordType,2>& pc,
	  const CoordType& w);
    
    /*!
    ** \brief Constructs a square Box2d using a central point and two widths.
    ** \param pc central point
    ** \param w1 width of the first coordinate box (real width =2*w1+1)
    ** \param w2 width of the second coordinate box (real height =2*w2+1)
    */
    Box2d(const Point<CoordType,2>& pc,
	  const CoordType& w1,
	  const CoordType& w2);  

    /*@} End Constructors */
    
    /**
     ** \name  Element access operators
     */
    /*@{*/
    /*!
    ** \brief Accessor/Mutator of the upper_left point (p1) of Box2d. 
    ** \return reference to the upper_left Point2d of the Box2d
    */
    void upper_left(Point<CoordType,2>);
    /*!
    ** \brief Accessor/Mutator of the upper_left point (p1) of Box2d. 
    ** \return reference to the upper_left Point2d of the Box2d
    */
    Point<CoordType,2>& upper_left();
    /*!
    ** \brief Accessor/Mutator of the upper_left point (p1) of Box2d. 
    ** \return reference to the upper_left Point2d of the Box2d
    */
    const Point<CoordType,2>& upper_left() const;
    
    
    /*!
    ** \brief Accessor/Mutator of the bottom_right point (p2) of Box2d. 
    ** \return reference to the bottom_right Point2d of the Box2d
    */
    void bottom_right(Point<CoordType,2>);
    /*!
    ** \brief Accessor/Mutator of the bottom_right point (p2) of Box2d. 
    ** \return reference to the bottom_right Point2d of the Box2d
    */
    Point<CoordType,2>& bottom_right();
    /*!
    ** \brief Accessor/Mutator of the bottom_right point (p2) of Box2d. 
    ** \return reference to the bottom_right Point2d of the Box2d
    */
    const Point<CoordType,2>& bottom_right() const;
    
    /*!
    ** \brief Mutator of the coordinates of the %Box2d. 
    ** \param x11 first coordinate of the minimal point p1
    ** \param x12 second coordinate of the minimal point p1
    ** \param x21 first coordinate of the maximal point p2
    ** \param x22 second coordinate of the maximal point p2
    ** \return 
    */
    void set_coord(const CoordType& x11,
		   const CoordType& x12,
		   const CoordType& x21,
		   const CoordType& x22);
    /*@} End Element access operators */
    
    
     /*!
     ** \brief Returns the name of the class 
     **       
     */
    std::string name() const;

    /*!
    ** \brief compute the area of the Box2d.
    ** \return the area
    */
    CoordType area() const;
    
    /*!
    ** \brief compute the width of the Box2d.
    ** \return the width
    */
    CoordType width() const;
    
    /*!
    ** \brief compute the height of the Box2d.
    ** \return the height
    */
    CoordType height() const;

    private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::Box<CoordType,2> >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::Box<CoordType,2> >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
  };
}//slip::

namespace slip
{
  template<typename CoordType>
  inline
  Box2d<CoordType>::Box2d():
    Box<CoordType,2>()
  {}
  
  template<typename CoordType>
  inline
  Box2d<CoordType>::Box2d(const Point<CoordType,2>& p1,
			  const Point<CoordType,2>& p2):
    Box<CoordType,2>(p1,p2)
  {}
  
  template<typename CoordType>
  inline
  Box2d<CoordType>::Box2d(const CoordType& x11,
			  const CoordType& x12,
			  const CoordType& x21,
			  const CoordType& x22):
    Box<CoordType,2>(Point2d<CoordType>(x11,x12),Point2d<CoordType>(x21,x22))
  {}

  template<typename CoordType>
  inline
  Box2d<CoordType>::Box2d(const CoordType& xc,
			  const CoordType& yc,
			  const CoordType& w):
    Box<CoordType,2>(Point2d<CoordType>(xc-w,yc-w),Point2d<CoordType>(xc+w,yc+w))
  {}

  template<typename CoordType>
  inline
  Box2d<CoordType>::Box2d(const Point<CoordType,2>& pc,
			  const CoordType& w):
    Box<CoordType,2>(Point2d<CoordType>(pc[0]-w,pc[1]-w),Point2d<CoordType>(pc[0]+w,pc[1]+w))
  {}


  template<typename CoordType>
  inline
  Box2d<CoordType>::Box2d(const Point<CoordType,2>& pc,
			  const CoordType& w1,
			  const CoordType& w2):
    Box<CoordType,2>(Point2d<CoordType>(pc[0]-w1,pc[1]-w2),Point2d<CoordType>(pc[0]+w1,pc[1]+w2))
  {}


  template<typename CoordType>
  inline
  void Box2d<CoordType>::upper_left(Point<CoordType,2> p1) {this->p1_=p1;}

  template<typename CoordType>
  inline
  Point<CoordType,2>& Box2d<CoordType>::upper_left() {return this->p1_;}
  
  template<typename CoordType>
  inline
  const Point<CoordType,2>& Box2d<CoordType>::upper_left() const {return this->p1_;}


  template<typename CoordType>
  inline
  void Box2d<CoordType>::bottom_right(Point<CoordType,2> p2) {this->p2_=p2;}

  template<typename CoordType>
  inline
  Point<CoordType,2>& Box2d<CoordType>::bottom_right() {return this->p2_;}

  template<typename CoordType>
  inline
  const Point<CoordType,2>& Box2d<CoordType>::bottom_right() const {return this->p2_;}


  template<typename CoordType>
  inline
  void Box2d<CoordType>::set_coord(const CoordType& x11,
				   const CoordType& x12,
				   const CoordType& x21,
				   const CoordType& x22)
  {
    this->p1_[0] = x11;
    this->p1_[1] = x12;
    this->p2_[0] = x21;
    this->p2_[1] = x22;
  }

  template<typename T>
  inline
  std::string 
  Box2d<T>::name() const {return "Box2d";} 
  
  
  template<typename CoordType>
  inline
  CoordType Box2d<CoordType>::area() const {return width() * height();}

  template<typename CoordType>
  inline
  CoordType Box2d<CoordType>::width() const {return (this->p2_[1] - this->p1_[1]+1);}

  template<typename CoordType>
  inline
  CoordType Box2d<CoordType>::height() const {return (this->p2_[0] - this->p1_[0]+1);}


   /**
   ** \brief Computes the intersection between two Box2d.
   ** \param box1 First Box2d.
   ** \param box2 Second Box2d.
   ** \param inter_box Intersection box ((0,0,0,0) if no intersection)
   ** \return true if the boxes intersects, false else.
   ** \par Example:
   ** \code
   ** slip::Box2d<int> b11(1,2,5,6);
   ** slip::Box2d<int> b21(6,7,10,12);
   ** slip::Box2d<int> binter1;
   ** bool as_inter1 = slip::box_intersection(b11,b21,binter1);
   ** std::cout<<"b11 = "<<b11<<std::endl;
   ** std::cout<<"b21 = "<<b21<<std::endl;
   ** std::cout<<"b11 inter b21 ? "<<as_inter1<<std::endl;
   ** std::cout<<"binter1 = "<<binter1<<std::endl;
   ** 
   ** slip::Box2d<int> b12(1,2,5,6);
   ** slip::Box2d<int> b22(2,3,10,12);
   ** slip::Box2d<int> binter2;
   ** bool as_inter2 = slip::box_intersection(b12,b22,binter2);
   ** std::cout<<"b12 = "<<b12<<std::endl;
   ** std::cout<<"b22 = "<<b22<<std::endl;
   ** std::cout<<"b12 inter b22 ? "<<as_inter2<<std::endl;
   ** std::cout<<"binter2 = "<<binter2<<std::endl;
   ** \endcode
   */
  inline
  bool box_intersection(const slip::Box2d<int>& box1,
			const slip::Box2d<int>& box2,
			slip::Box2d<int>& inter_box)
  {
    bool as_intersection = false;
    inter_box.set_coord(0,0,0,0);
    
    int i11 = box1.upper_left()[0];
    int i12 = box1.bottom_right()[0];
    int j11 = box1.upper_left()[1];
    int j12 = box1.bottom_right()[1];

    int i21 = box2.upper_left()[0];
    int i22 = box2.bottom_right()[0];
    int j21 = box2.upper_left()[1];
    int j22 = box2.bottom_right()[1];

    int ub1 = std::min(std::max(i11,i12),std::max(i21,i22));
    int lb1 = std::max(std::min(i11,i12),std::min(i21,i22));
    int lb2 = std::max(std::min(j11,j12),std::min(j21,j22));
    int ub2 = std::min(std::max(j11,j12),std::max(j21,j22));
     
    if((lb1 <= ub1) && (lb2 <= ub2))
      {
	as_intersection = true;
	inter_box.set_coord(lb1,lb2,ub1,ub2);
      }

    return as_intersection;
  }
  
}//slip::

#endif //SLIP_BOX2D_HPP
