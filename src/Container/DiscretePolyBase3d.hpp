/** 
 * \file DiscretePolyBase3d.hpp
 * 
 * \brief Provides a class to handle global approximation of 3d container by orthogonal polynomials base.
 * 
 */

#ifndef SLIP_DISCRETEPOLYBASE3D_HPP
#define SLIP_DISCRETEPOLYBASE3D_HPP

#include <iostream>
#include <string>
#include <string>
#include <vector>
#include <map>
#include "Polynomial.hpp"
#include "Array.hpp"
#include "Array2d.hpp"
#include "Array3d.hpp"
#include "DPoint3d.hpp"
#include "arithmetic_op.hpp"
#include "linear_algebra.hpp"
#include "macros.hpp"
#include "Block.hpp"
#include "polynomial_algo.hpp"
#include "poly_weight_functors.hpp"
#include "DiscretePolyBase1d.hpp"


namespace slip
{
  template<typename RandomAccessIterator1,
	   typename RandomAccessIterator2,
	   typename T> 
  struct discrete_poly3d_inner_prod : 
    public std::binary_function<std::pair<RandomAccessIterator1,RandomAccessIterator1>, 
				std::pair<RandomAccessIterator2,RandomAccessIterator2>,T>
  {
    template< typename RandomAccessIterator3, 
	      typename RandomAccessIterator4,
	      typename RandomAccessIterator5>
    discrete_poly3d_inner_prod(RandomAccessIterator3 slice_weight_first,
			       RandomAccessIterator3 slice_weight_last,
			       RandomAccessIterator4 row_weight_first,
			       RandomAccessIterator4 row_weight_last,
			       RandomAccessIterator5 col_weight_first,
			       RandomAccessIterator5 col_weight_last):
      weight_(new slip::Array3d<T>( static_cast<std::size_t>(slice_weight_last-slice_weight_first),
				   static_cast<std::size_t>(row_weight_last-row_weight_first),
				   static_cast<std::size_t>(col_weight_last-col_weight_first)))
    {
      // slip::outer_product(row_weight_first, row_weight_last,
      // 			  col_weight_first, col_weight_last,
      // 			  this->weight_->upper_left(),this->weight_->bottom_right());
      // slip::tensorial_product3d(slice_weight_first,slice_weight_last,
      // 				row_weight_first,row_weight_last,
      // 				col_weight_first,col_weight_last,
      // 				this->weight_->front_upper_left(),
      // 				this->weight_->back_bottom_right());
      slip::rank1_tensorial_product(slice_weight_first,slice_weight_last,
				row_weight_first,row_weight_last,
				col_weight_first,col_weight_last,
				this->weight_->front_upper_left(),
				this->weight_->back_bottom_right());
      
    }
    T operator() (const std::pair<RandomAccessIterator1,RandomAccessIterator1>& Pi,
		  const std::pair<RandomAccessIterator2,RandomAccessIterator2>& Pj)
  {

    T result = T();
    typename slip::Array3d<T>::const_iterator it_weight = this->weight_->begin();
    RandomAccessIterator1 it_pi = Pi.first;
    RandomAccessIterator2 it_pj = Pj.first;
    for (; it_pi != Pi.second; 
	   ++it_pi, 
	   ++it_pj,
	   ++it_weight)
      {
	result = result + *it_weight * (*it_pi * *it_pj);
      }
    return result;

  }
		  
    ~discrete_poly3d_inner_prod()
    {
      if(this->weight_ != 0)
	{
	  delete this->weight_;
	}
    }

    slip::Array3d<T>* weight_;

 };
}


namespace slip
{

/*! \class DiscretePolyBase3d
** 
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2011/08/26
** \brief a class to handle global approximation of 3d container by orthogonal polynomials base
** \param T Type of data to approximate.
** \param PolyWeightSlice Weight functor for the slice polynomial.
** \param PolyCollocationSlice Collocation functor for the slice polynomial.
** \param PolyWeightRow Weight functor for the row polynomial.
** \param PolyCollocationRow Collocation functor for the row polynomial.
** \param PolyWeightCol Weight functor for the column polynomial.
** \param PolyCollocationCol Collocation functor for the column polynomial.
*/
  template <typename T, 
	    typename PolyWeightSlice, typename PolyCollocationSlice,
	    typename PolyWeightRow, typename PolyCollocationRow,
	    typename PolyWeightCol, typename PolyCollocationCol>
  class DiscretePolyBase3d 
{
  
public:

 
  typedef DiscretePolyBase3d<T, PolyWeightSlice, PolyCollocationSlice,
			        PolyWeightRow, PolyCollocationRow,
			        PolyWeightCol, PolyCollocationCol> self;
  typedef const self const_self;

  

   /**
   ** \name Constructors & Destructors
   */
  /*@{*
    /*!
    ** \brief Constructs a DiscretePolyBase3d.
    */
  DiscretePolyBase3d():
    slices_(0),
    rows_(0),
    cols_(0),
    size_(0),
    degree_(-1),
    slice_degree_(-1),
    row_degree_(-1),
    col_degree_(-1),
    slice_base_(new slip::DiscretePolyBase1d<T,PolyWeightSlice,PolyCollocationSlice>()),
    row_base_(new slip::DiscretePolyBase1d<T,PolyWeightRow,PolyCollocationRow>()),
    col_base_(new slip::DiscretePolyBase1d<T,PolyWeightCol,PolyCollocationCol>()),
   
     poly_evaluations_(0),
    poly_w_evaluations_(0),
    pkpk_(0),
    powers_(0),
    normalized_(false)
  {
  }
 
  /*!
    ** \brief Constructs a DiscretePolyBase3d.
    ** \param slices Number of slices of the 3d container.
    ** \param rows Number of rows of the 3d container.
    ** \param cols Number of cols of the 3d container.
    ** \param slice_degree Degree of the slice polynomial.
    ** \param poly_weight_slice_fun Weight functor for the slice polynomial.
    ** \param poly_collocation_slice_fun Collocation functor for the slice polynomial.
    ** \param row_degree Degree of the row polynomial.
    ** \param poly_weight_row_fun Weight functor for the row polynomial.
    ** \param poly_collocation_row_fun Collocation functor for the row polynomial.
    ** \param col_degree Degree of the col polynomial.
    ** \param poly_weight_col_fun Weight functor for the col polynomial.
    ** \param poly_collocation_col_fun Collocation functor for the col polynomial.
    ** \param normalized Boolean indicated if the base is normalized or not.
    */
  DiscretePolyBase3d(const std::size_t slices,
		     const std::size_t rows,
		     const std::size_t cols,
		     const int slice_degree,
		     const PolyWeightSlice& poly_weight_slice_fun,
		     const PolyCollocationSlice& poly_collocation_slice_fun,
		     const int row_degree,
		     const PolyWeightRow& poly_weight_row_fun,
		     const PolyCollocationRow& poly_collocation_row_fun,
		     const int col_degree,
		     const PolyWeightCol& poly_weight_col_fun,
		     const PolyCollocationCol& poly_collocation_col_fun,
		     const bool normalized):
    slices_(slices),
    rows_(rows),
    cols_(cols),
    size_((slice_degree+1)*(row_degree + 1)*(col_degree + 1)),
    degree_(slice_degree + row_degree + col_degree),
    slice_degree_(slice_degree),
    row_degree_(row_degree),
    col_degree_(col_degree),
   
    slice_base_(new slip::DiscretePolyBase1d<T,PolyWeightSlice,PolyCollocationSlice>(slices,slice_degree,poly_weight_slice_fun,poly_collocation_slice_fun,normalized)),
    row_base_(new slip::DiscretePolyBase1d<T,PolyWeightRow,PolyCollocationRow>(rows,row_degree,poly_weight_row_fun,poly_collocation_row_fun,normalized)),
    col_base_(new slip::DiscretePolyBase1d<T,PolyWeightCol,PolyCollocationCol>(cols,col_degree,poly_weight_col_fun,poly_collocation_col_fun,normalized)),
    poly_evaluations_(new std::map<slip::block<std::size_t,3>, slip::Array3d<T> >()),
    poly_w_evaluations_(new std::map<slip::block<std::size_t,3>, slip::Array3d<T> >()),
    pkpk_(new slip::Array<T>(this->size_)),
    powers_(new slip::Array2d<std::size_t>(this->size_,3)),
    normalized_(normalized)
  {}

   /*!
    ** \brief Constructs a DiscretePolyBase3d.
      ** \param slices Number of slices of the 3d container.
    ** \param rows Number of rows of the 3d container.
    ** \param cols Number of cols of the 3d container.
    ** \param degree Degree of the slice, row and column polynomials.
    ** \param poly_weight_slice_fun Weight functor for the slice polynomial.
    ** \param poly_collocation_slice_fun Collocation functor for the slice polynomial.
    ** \param poly_weight_row_fun Weight functor for the row polynomial.
    ** \param poly_collocation_row_fun Collocation functor for the row polynomial.
    ** \param poly_weight_col_fun Weight functor for the col polynomial.
    ** \param poly_collocation_col_fun Collocation functor for the col polynomial.
    ** \param normalized Boolean indicated if the base is normalized or not.
    */
  DiscretePolyBase3d(const std::size_t slices,
		     const std::size_t rows,
		     const std::size_t cols,
		     const int degree,
		     const PolyWeightSlice& poly_weight_slice_fun,
		     const PolyCollocationSlice& poly_collocation_slice_fun,
		     const PolyWeightRow& poly_weight_row_fun,
		     const PolyCollocationRow& poly_collocation_row_fun,
		     const PolyWeightCol& poly_weight_col_fun,
		     const PolyCollocationCol& poly_collocation_col_fun,
		     const bool normalized):
    slices_(slices),
    rows_(rows),
    cols_(cols),
    size_(((degree+1)*(degree + 2)*(degree + 3))/6),
    degree_(degree),
    slice_degree_(degree),
    row_degree_(degree),
    col_degree_(degree),
    slice_base_(new slip::DiscretePolyBase1d<T,PolyWeightSlice,PolyCollocationSlice>(slices,degree,poly_weight_slice_fun,poly_collocation_slice_fun,normalized)),
    row_base_(new slip::DiscretePolyBase1d<T,PolyWeightRow,PolyCollocationRow>(rows,degree,poly_weight_row_fun,poly_collocation_row_fun,normalized)),
    col_base_(new slip::DiscretePolyBase1d<T,PolyWeightCol,PolyCollocationCol>(cols,degree,poly_weight_col_fun,poly_collocation_col_fun,normalized)),
     poly_evaluations_(new std::map<slip::block<std::size_t,3>, slip::Array3d<T> >()),
    poly_w_evaluations_(new std::map<slip::block<std::size_t,3>, slip::Array3d<T> >()),
    pkpk_(new slip::Array<T>(this->size_)),
    powers_(new slip::Array2d<std::size_t>(this->size_,3)),
    normalized_(normalized)
  {
  }

   /*!
    ** \brief Constructs a DiscretePolyBase3d.
    ** \param slices Number of slices of the 3d container.
    ** \param rows Number of rows of the 3d container.
    ** \param cols Number of cols of the 3d container.
    ** \param degree Degree of the slice, row and column polynomials.
    ** \param poly_weight_fun Weight functor for the polynomial (the same weight functor is used for slice, row and column).
    ** \param poly_collocation_fun Collocation functor for the polynomial (the same collocation functor is used for slice, row and column).
    ** \param normalized Boolean indicated if the base is normalized or not.
    */
  DiscretePolyBase3d(const std::size_t slices,
		     const std::size_t rows,
		     const std::size_t cols,
		     const int degree,
		     const PolyWeightSlice& poly_weight_fun,
		     const PolyCollocationSlice& poly_collocation_fun,
		     const bool normalized):
    slices_(slices),
    rows_(rows),
    cols_(cols),
    size_(((degree+1)*(degree + 2)*(degree + 3))/6),
    degree_(degree),
    slice_degree_(degree),
    row_degree_(degree),
    col_degree_(degree),
    slice_base_(new slip::DiscretePolyBase1d<T,PolyWeightSlice,PolyCollocationSlice>(slices,degree,poly_weight_fun,poly_collocation_fun,normalized)),
    row_base_(0),
    col_base_(0),
    poly_evaluations_(new std::map<slip::block<std::size_t,3>, slip::Array3d<T> >()),
    poly_w_evaluations_(new std::map<slip::block<std::size_t,3>, slip::Array3d<T> >()),
    pkpk_(new slip::Array<T>(this->size_)),
    powers_(new slip::Array2d<std::size_t>(this->size_,3)),
    normalized_(normalized)
  {
  }

 /*!
  ** \brief Copy constructor.
  ** \param other %DiscretePolyBase3d.
  */
  DiscretePolyBase3d(const DiscretePolyBase3d& other):
    slices_(other.slices_),
    rows_(other.rows_),
    cols_(other.cols_),
    size_(other.size_),
    degree_(other.degree_),
    slice_degree_(other.slice_degree_),
    row_degree_(other.row_degree_),
    col_degree_(other.col_degree_),
    slice_base_(new slip::DiscretePolyBase1d<T,PolyWeightSlice,PolyCollocationSlice>(*other.slice_base_)),
    row_base_(new slip::DiscretePolyBase1d<T,PolyWeightRow,PolyCollocationRow>(*other.row_base_)),
     col_base_(new slip::DiscretePolyBase1d<T,PolyWeightCol,PolyCollocationCol>(*other.col_base_)),
    poly_evaluations_(new std::map<slip::block<std::size_t,3>, slip::Array3d<T> >(*other.poly_evaluations_)),
    poly_w_evaluations_(new std::map<slip::block<std::size_t,3>, slip::Array3d<T> >(*other.poly_w_evaluations_)),
    pkpk_(new slip::Array<T>(this->size_)),
    powers_(new slip::Array2d<std::size_t>(this->size_,3)),
    normalized_(other.normalized_)
  {
    
  }

  void init_canonical(const std::size_t slices,
		      const std::size_t rows,
		      const std::size_t cols,
		      const int slice_degree,
		      const PolyWeightSlice& poly_weight_slice_fun,
		      const PolyCollocationSlice& poly_collocation_slice_fun,
		      const int row_degree,
		      const PolyWeightRow& poly_weight_row_fun,
		      const PolyCollocationRow& poly_collocation_row_fun,
		      const int col_degree,
		      const PolyWeightCol& poly_weight_col_fun,
		      const PolyCollocationCol& poly_collocation_col_fun)

  {
    //initializations and allocations
    this->slices_ = slices;
    this->rows_ = rows;
    this->cols_ = cols;
    this->size_ = (slice_degree+1)*(row_degree + 1)*(col_degree + 1);
    this->degree_  = slice_degree + row_degree + col_degree;
    this->slice_degree_ = slice_degree;
    this->row_degree_ = row_degree;
    this->col_degree_ = col_degree;
    this->slice_base_->init_canonical(slices,
				      slice_degree,
				      poly_weight_slice_fun,
				      poly_collocation_slice_fun);
  
    this->row_base_->init_canonical(rows,
				    row_degree,
				    poly_weight_row_fun,
				    poly_collocation_row_fun);
    this->col_base_->init_canonical(cols,
				    col_degree,
				    poly_weight_col_fun,
				    poly_collocation_col_fun);
    
    this->poly_evaluations_   = new std::map<slip::block<std::size_t,3>, slip::Array3d<T> >();
    this->poly_w_evaluations_ = new std::map<slip::block<std::size_t,3>, slip::Array3d<T> >();
    this->pkpk_ = new slip::Array<T>(this->size_);
    this->powers_  = new slip::Array2d<std::size_t>(this->size_,3);
    this->normalized_ = false;

    this->compute_polynomials();

    //pkpk_ 
    this->update_pkpk();
  }

  /*!
  ** \brief Destructor of the Legendre
  */
  virtual ~DiscretePolyBase3d()
  {
    this->desallocate();
   
  }
  
/*@} End Constructors */
  

  /*!
  ** \brief Assignement operator
  ** \param other %DiscretePolyBase3d.
  */
  self& 
  operator=(const self& other)
  {
     if(this == &other)
       {
	 return *this;
       }
     this->desallocate();
     this->slices_ = other.slices_;
     this->rows_ = other.rows_;
     this->cols_ = other.cols_;
     this->size_ = other.size_;
     this->degree_ = other.degree_;
     this->slice_degree_ = other.slice_degree_;
     this->row_degree_ = other.row_degree_;
     this->col_degree_ = other.col_degree_;
     this->slice_base_ = new slip::DiscretePolyBase1d<T,PolyWeightSlice,PolyCollocationSlice>(*other.slice_base_);
     this->row_base_ = new slip::DiscretePolyBase1d<T,PolyWeightRow,PolyCollocationRow>(*other.row_base_);
     this->col_base_ = new slip::DiscretePolyBase1d<T,PolyWeightCol,PolyCollocationCol>(*other.col_base_);
     this->poly_evaluations_ = new std::map<slip::block<std::size_t,3>, slip::Array3d<T> >(*other.poly_evaluations_);
     this->poly_w_evaluations_ = new std::map<slip::block<std::size_t,3>, slip::Array3d<T> >(*other.poly_w_evaluations_);
     this->powers_ = new slip::Array2d<std::size_t>(*other.powers_);
     this->pkpk_ = (new slip::Array<T>(*other.pkpk_)),
     this->normalized_ = other.normalized_;
  
     return *this;
  }


void gram_schmidt()
  {
   
    typedef typename slip::Array2d<T>::iterator Iterator;
    std::vector<std::pair<Iterator,Iterator> > base(this->size_);
  for(std::size_t i = 0; i < this->size_; ++i)
    {
      slip::block<std::size_t, 3> ind_i;
      ind_i[0] = (*(this->powers_))[i][0];
      ind_i[1] = (*(this->powers_))[i][1];
      ind_i[2] = (*(this->powers_))[i][2];
      
      typename std::map<slip::block<std::size_t,3>, slip::Array3d<T> >::iterator it  = 
      (this->poly_evaluations_)->find(ind_i);
      base[i].first  = (it->second).begin();
      base[i].second = (it->second).end();
    }
 
  std::cout<<"orthogonalize...."<<std::endl;
  //orthogonalize
  this->modified_gram_schmidt(base);

  std::cout<<"update poly_w_evaluations_...."<<std::endl;
  //update poly_w_evaluations_
  this->update_poly_w_evaluations();
  std::cout<<"update pkpk_...."<<std::endl;

   //update_pkpk
  this->update_pkpk();



  }
 
  /*!
  ** \brief Generate the polynomial base.
  */
  void generate()
  {
    if(this->slice_base_ != 0)
      {
	(this->slice_base_)->generate();
      }
    if(this->row_base_ != 0)
      {
	(this->row_base_)->generate();
      }
     if(this->col_base_ != 0)
      {
	(this->col_base_)->generate();
      }
     if(this->slice_base_ != 0)
      {
    	this->compute_polynomials();
      }
    
  }

  /*!
  ** \brief Project data onto the P(i,j) polynomial of the base.
  ** \param up RandomAccessIterator3d on the data.
  ** \param bot RandomAccessIterator3d on the data.
  ** \param k index of the slice polynomial.
  ** \param i index of the row polynomial.
  ** \param j index of the column polynomial.
  */
  template <typename RandomAccessIterator3d>
  T project(RandomAccessIterator3d up,
	    RandomAccessIterator3d bot,
	    const std::size_t k,
	    const std::size_t i,
	    const std::size_t j)
  {
    slip::block<std::size_t,3> pkij;
    pkij[0] = k;
    pkij[1] = i;
    pkij[2] = j;
    typename std::map<slip::block<std::size_t,3>, slip::Array3d<T> >::iterator it_w  = (this->poly_w_evaluations_)->find(pkij); 
    assert(it_w != (this->poly_w_evaluations_)->end());

    T result = std::inner_product((it_w->second).begin(),
				  (it_w->second).end(),
				  up,
				  T());
     if(!this->normalized_)
      {
	if(    (this->degree_ == this->slice_degree_) 
	    && (this->degree_ == this->row_degree_) 
	    && (this->degree_ == this->col_degree_) )
	  {
	  
	    //step within the slice k
	    //step = i*((2(D-k)+3) - i)/2  = (D+1-k)(D+2-k)/2 - (D+1-k-i)(D+2-k-i)/2
	    std::size_t	step = ((this->degree_ + this->degree_ + 3 -k - k - i) * i)/2;
	    //slice step stepk = (D+1)(D+2)(D+3)/6  - (D+1 -k)(D+2 -k)(D+3 -k)/6
	    std::size_t	stepk = this->size_ - ((this->degree_ -k+1)*(this->degree_ -k+2)*(this->degree_ -k+3))/6 ;
	    
	    std::size_t kk = stepk + step + j;
	    result /= (*this->pkpk_)[stepk + step + j];
	  }
	else
	  {
	    std::size_t kk = k *(this->row_base_->size()*this->col_base_->size()) 
	      +i *(this->col_base_->size()) + j;
	    result /= (*this->pkpk_)[kk];
	  }
      }
     return result;
  }


  /*!
  ** \brief Project data onto the P(i,j) polynomial of the base.
  ** \param up RandomAccessIterator3d on the data.
  ** \param bot RandomAccessIterator3d on the data.
  ** \param slice_degree_max maximal degree of projection onto the slices.
  ** \param row_degree_max maximal degree of projection onto the rows.
  ** \param col_degree_max maximal degree of projection onto the columns.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \pre (coef_last - coef_first) <= this->size()
  */
  template <typename RandomAccessIterator3d,
	    typename RandomAccessIterator>
  void project(RandomAccessIterator3d up,
	       RandomAccessIterator3d bot,
	       const std::size_t slice_degree_max,
	       const std::size_t row_degree_max,
	       const std::size_t col_degree_max,
	       RandomAccessIterator coef_first,
	       RandomAccessIterator coef_last)
  {
    assert(static_cast<std::size_t>(coef_last - coef_first) <= this->size_);

    if(this->normalized_)
      {
	typename std::map<slip::block<std::size_t,3>, slip::Array3d<T> >::iterator it_w  = 
	  (this->poly_w_evaluations_)->begin();
	for(;it_w!=(this->poly_w_evaluations_)->end();++it_w)
	  {
	    if( ((it_w->first)[0] <= slice_degree_max)  && 
	        ((it_w->first)[1] <= row_degree_max) &&
		((it_w->first)[2] <= col_degree_max) )
                  {
		 *coef_first++ = std::inner_product((it_w->second).begin(),
						    (it_w->second).end(),
						    up,
						    T());
		    
		    
		  }
	  }

      }
    else
      {
	typename std::map<slip::block<std::size_t,3>, slip::Array3d<T> >::iterator it_w  = 
	  (this->poly_w_evaluations_)->begin();
	std::size_t kk = 0;
	for(;it_w!=(this->poly_w_evaluations_)->end();++it_w)
	  {
	    if( ((it_w->first)[0] <= slice_degree_max)  && 
	        ((it_w->first)[1] <= row_degree_max) &&
		((it_w->first)[2] <= col_degree_max) )
                  {
		
		    *coef_first++ = std::inner_product((it_w->second).begin(),
						       (it_w->second).end(),
						       up,
						       T()) / (*this->pkpk_)[kk];
		    ++kk;
		    
		  }
	  }


      }
    
    
  }
 
  /*!
  ** \brief Reconstruct the data from a coefficients range.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \param K number of coefficients to use for recontruct the data.
  ** \param up RandomAccessIterator3d on the data.
  ** \param bot RandomAccessIterator3d on the data.
 s.
  ** \pre K <= (coef_last - coef_first)
  */
  template <typename RandomAccessIterator,
	     typename RandomAccessIterator3d>
  void reconstruct(RandomAccessIterator coef_first,
		   RandomAccessIterator coef_last,
		   const std::size_t K,
		   RandomAccessIterator3d up,
		   RandomAccessIterator3d bot)
  {
    assert(K <= static_cast<std::size_t>(coef_last-coef_first)); 
    
    slip::DPoint3d<int> d;
    slip::block<std::size_t,3> pkij;
    for(std::size_t k = 0; k < this->slices_; ++k)
      {
	d[0] = k;
	for(std::size_t i = 0; i < this->rows_; ++i)
	  {
	    d[1] = i;
	    for(std::size_t j = 0; j < this->cols_; ++j)
	      {
		d[2] = j;
		T s = T();
		RandomAccessIterator it_coeff = coef_first;
		for(std::size_t kk = 0; kk < K; ++kk, ++it_coeff)
		  {
		    pkij[0] = (*(this->powers_))[kk][0];
		    pkij[1] = (*(this->powers_))[kk][1];
		    pkij[2] = (*(this->powers_))[kk][2];
		    typename std::map<slip::block<std::size_t,3>, slip::Array3d<T> >::iterator it  = 
		      (this->poly_evaluations_)->find(pkij); 
		    s += *it_coeff * (it->second)[k][i][j];
		  }
		up[d] = s;
	      }
	  }
      }
  
  }

 /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const
  {
    return "DiscretePolyBase3d";
  }
  
  /*!
  ** \brief Returns the degree of the Base
  **
  ** \remarks Same value as degree
  */
  int degree() const
  {
    return this->degree_;
  }

  /*!
  ** \brief Returns the order of the Base
  **
  ** \remarks Same value as degree
  */
  int order() const
  {
    return this->degree_;
  }

 /*!
  ** \brief Returns the size of the base.
  **
  */
  std::size_t size() const
  {
    return this->size_;
  }
 
  /*!
  ** \brief Returns the slice number of the range
  **
  */
  std::size_t slices() const
  {
    return this->slices_;
  }
 /*!
  ** \brief Returns the row number of the range
  **
  */
  std::size_t rows() const
  {
    return this->rows_;
  }

  /*!
  ** \brief Returns the column number of the range
  **
  */
  std::size_t cols() const
  {
    return this->cols_;
  }
 
  /*!
  ** \brief Returns the column number of the range
  **
  */
  std::size_t columns() const
  {
    return this->cols_;
  }

  /*!
  ** \brief Returns true if the base is normalized, 
  ** else returns false
  **
  */
  bool is_normalized() const
  {
    return this->normalized_;
  }

  /*!
  ** \brief Print polynomial basis features.
  **
  */
  void print()
  {
    std::cout<<"Slices = "<<this->slices_<<std::endl;
    std::cout<<"Rows = "<<this->rows_<<std::endl;
    std::cout<<"Columns = "<<this->cols_<<std::endl;
    std::cout<<"Base degree = "<<this->degree_<<std::endl;
    std::cout<<"Number of polynomials = "<<this->size_<<std::endl;
    std::cout<<"normalized = "<<this->normalized_<<std::endl;
    
    // std::cout<<"Row base properties:\n";
    // if(this->slice_base_ != 0)
    //   {
    // 	(this->slice_base_)->print();
    //   }
    // std::cout<<"Column base properties:\n";
    // if( this->row_base_ != 0)
    //   {
    // 	(this->row_base_)->print();
    //   }
    // else
    //   {
    // 	if(this->slice_base_ != 0)
    // 	  {
    // 	    (this->slice_base_)->print();
    // 	  }
    //   }
    //    slip::block<std::size_t,2> p00;
    //p00.fill(0);
    std::cout<<std::endl;
    std::cout<<"Polynomials:"<<std::endl;
    std::cout<<std::endl;
    typename std::map<slip::block<std::size_t,3>, slip::Array3d<T> >::iterator it  = 
      (this->poly_evaluations_)->begin();
    
    for(;it!=(this->poly_evaluations_)->end();++it)
      {
	std::cout<<"P"<<(*it).first<<":"<<std::endl;
	std::cout<<(*it).second<<std::endl;
	std::cout<<std::endl;
      }

    std::cout<<std::endl;
    std::cout<<"Weighted Polynomials:"<<std::endl;
    std::cout<<std::endl;
    typename std::map<slip::block<std::size_t,3>, slip::Array3d<T> >::iterator it_w  = 
      (this->poly_w_evaluations_)->begin();
    
    for(;it_w!=(this->poly_w_evaluations_)->end();++it_w)
      {
	std::cout<<"PW"<<(*it_w).first<<":"<<std::endl;
	std::cout<<(*it_w).second<<std::endl;
	std::cout<<std::endl;
      }
    
    std::cout<<std::endl;
    std::cout<<"(Pk,Pk):"<<std::endl;
    std::cout<<std::endl;
    std::cout<<*(this->pkpk_)<<std::endl;
    
  }
 
  
   /*!
  ** \brief Computes the inner_product between Pi and Pj.
  ** \param i index of the first polynomial.
  ** \param j index of the second polynomial.
  ** \return the value of the inner product.
  ** \pre (i >  this->size_)
  ** \pre (j >  this->size_)
  */
  T inner_product(const std::size_t i, const std::size_t j)
  {
    assert(i < this->size_);
    assert(j < this->size_);
    
    slip::block<std::size_t, 3> ind_i;
    ind_i[0] = (*(this->powers_))[i][0];
    ind_i[1] = (*(this->powers_))[i][1];
    ind_i[2] = (*(this->powers_))[i][2];
    
    slip::block<std::size_t, 3> ind_j;
    ind_j[0] = (*(this->powers_))[j][0];
    ind_j[1] = (*(this->powers_))[j][1];
    ind_j[2] = (*(this->powers_))[j][2];
 
      
    typename std::map<slip::block<std::size_t,3>, slip::Array3d<T> >::iterator it  = 
      (this->poly_evaluations_)->find(ind_i);
    typename std::map<slip::block<std::size_t,3>, slip::Array3d<T> >::iterator it_w  = 
      (this->poly_w_evaluations_)->find(ind_j);
    
    return std::inner_product((it->second).begin(),(it->second).end(),
			      (it_w->second).begin(),T());
    
  }
 
  /*!
  ** \brief Computes the Gram matrix of the polynomial base.
  ** \param Gram The resulting Gram matrix.
  */
  template<typename Matrix>
  void orthogonality_matrix(Matrix& Gram)
  {
    Gram.resize(this->size_,this->size_);
    for(std::size_t i = 0; i < this->size_; ++i)
      {
	for(std::size_t j = 0; j <= i; ++j)
	  {
	    Gram[i][j] =  (this->inner_product)(i,j);
	    Gram[j][i] = Gram[i][j];
	  }
      }
  }
  /*!
  ** \brief Indicate if the polynomial basis is orthogonal according to the tolerance \a tol.
  ** \param tol Tolerance of the orthogonality .
  ** \result True if orthogonal, false else.
  */
  bool is_orthogonal(const T tol 
		   = slip::epsilon<T>())
  {
    slip::Matrix<T> Gram;
    this->orthogonality_matrix(Gram);
    return slip::is_diagonal(Gram,tol);
  }

private:
  std::size_t slices_;             ///number of slices of the container3d
  std::size_t rows_;               ///number of rows of the container3d
  std::size_t cols_;               ///number of cols of the
				   ///container3d
  std::size_t size_;///size of the base : number of polynomials
  int degree_;              ///degree of the base
  int slice_degree_; ///degree of the slice base
  int row_degree_; ///degree of the row base
  int col_degree_; ///degree of the column base
  slip::DiscretePolyBase1d<T,PolyWeightSlice,PolyCollocationSlice>* slice_base_;
  slip::DiscretePolyBase1d<T,PolyWeightRow,PolyCollocationRow>* row_base_;
  slip::DiscretePolyBase1d<T,PolyWeightCol,PolyCollocationCol>* col_base_;
  std::map<slip::block<std::size_t,3>, slip::Array3d<T> >* poly_evaluations_; ///map which associate an %Array3d<T> containning polynomial evaluation at each range site to a block containing 3 polynomial indices
  std::map<slip::block<std::size_t,3>, slip::Array3d<T> >* poly_w_evaluations_;///map which associate an %Array3d<T> containning polynomial evaluation multiplies by the corresponding weight at each range site to a block containing 3 polynomial indices
  slip::Array<T>* pkpk_; ///(Pk,Pk) inner_products
  slip::Array2d<std::size_t>* powers_;  /// size_ x 3 matrix containing the spatial polynomial degrees for each polynomial of the base
  bool normalized_; ///boolean indicated if the base is normalized or not
 
  

private:
  
  /*!
  ** \brief Desallocate the polynomial base data.
  */
  void desallocate()
  {
     if(this->slice_base_ != 0)
      {
	delete this->slice_base_;
      }
    if(this->row_base_ != 0)
      {
	delete this->row_base_;
      }
    if(this->col_base_ != 0)
       {
	delete this->col_base_;
      }
    if(this->poly_evaluations_ != 0)
      {
	delete this->poly_evaluations_;
      }
    if(this->poly_w_evaluations_ != 0)
      {
	delete this->poly_w_evaluations_;
      }
    if(this->pkpk_ != 0)
      {
	delete this->pkpk_;
      }
    if(this->powers_ != 0)
      {
	delete this->powers_;
      }
  } 
 
  /*!
  ** \brief Computes the polynomial basis.
  */
  void compute_polynomials()
  {
    if(this->row_base_ != 0 && this->col_base_ != 0)
      {
	slip::Array3d<T> P(this->slices_,this->rows_,this->cols_);
	slip::Array3d<T> PW(this->slices_,this->rows_,this->cols_);
	for(std::size_t k = 0, n = 0; k <= this->slice_degree_; ++k)
	  {
	    for(std::size_t i = 0; i <= this->row_degree_; ++i)
	      {
		for(std::size_t j = 0; (j <= this->col_degree_) && (k+i+j <= this->degree_); ++j, ++n)
		  {
		    (*(this->powers_))[n][0] = k;
		    (*(this->powers_))[n][1] = i;
		    (*(this->powers_))[n][2] = j;
		    slip::block<std::size_t,3> indices;
		    indices[0] = k;
		    indices[1] = i;
		    indices[2] = j;
		    for(std::size_t s = 0; s < this->slices_; ++s)
		      {
			for(std::size_t r = 0; r < this->rows_; ++r)
			  {
			    for(std::size_t c = 0; c < this->cols_; ++c)
			      {
				P[s][r][c] = 
				  (this->slice_base_)->get_poly(k,s) * (this->row_base_)->get_poly(i,r) * (this->col_base_)->get_poly(j,c);
				PW[s][r][c] = 
				  (this->slice_base_)->get_poly_w(k,s) * (this->row_base_)->get_poly_w(i,r) * (this->col_base_)->get_poly_w(j,c);
			      }
			  }
		      }
		    (*this->poly_evaluations_)[indices] = P;
		    (*this->poly_w_evaluations_)[indices] = PW;

		  }
	      }
	  }
      }
  
   else
     {
       slip::Array3d<T> P(this->slices_,this->rows_,this->cols_);
       slip::Array3d<T> PW(this->slices_,this->rows_,this->cols_);
	
       for(std::size_t k = 0, kk = 0; k <= this->slice_degree_; ++k)
	 {
	   for(std::size_t i = 0; i <= this->row_degree_; ++i)
	     {
	       for(std::size_t j = 0; (j <= this->col_degree_) && (k+i+j <= this->degree_); ++j, ++kk)
		 {
		   (*(this->powers_))[kk][0] = k;
		   (*(this->powers_))[kk][1] = i;
		   (*(this->powers_))[kk][2] = j;
		   slip::block<std::size_t,3> indices;
		   indices[0] = k;
		   indices[1] = i;
		   indices[2] = j;
		
		   for(std::size_t s = 0; s < this->slices_; ++s)
		     {
		       for(std::size_t r = 0; r < this->rows_; ++r)
			 {
			   for(std::size_t c = 0; c < this->cols_; ++c)
			     {
			       P[s][r][c] = 
				 (this->slice_base_)->get_poly(k,s) * (this->slice_base_)->get_poly(i,r)* (this->slice_base_)->get_poly(j,c);
			       PW[s][r][c] = 
				 (this->slice_base_)->get_poly_w(k,s) * (this->slice_base_)->get_poly_w(i,r) *  (this->slice_base_)->get_poly_w(j,c);
			
			     }
			 }
		     }
		   (*this->poly_evaluations_)[indices] = P;
		   (*this->poly_w_evaluations_)[indices] = PW;
		 }
	     }
	 }

     }

    	if(this->normalized_)
	  {
	    std::fill_n((this->pkpk_)->begin(),this->size_,static_cast<T>(1.0));
	  }
	else
	  {
	    typename slip::Array<T>::iterator it_pkpkb = (this->pkpk_)->begin();
	    typename slip::Array<T>::iterator it_pkpke = (this->pkpk_)->end();
	    std::size_t k = 0;
	    for(; it_pkpkb != it_pkpke; ++it_pkpkb, ++k)
	      {
		*it_pkpkb = this->inner_product(k,k);
	      }
	  }
  }

  

  void update_pkpk()
  {
    for(std::size_t k = 0; k < this->size_; ++k)
      {
	(*(this->pkpk_))[k] = this->inner_product(k,k);
      }
  }

 void update_poly_w_evaluations()
  {
    for(std::size_t n = 0; n < this->size_; ++n)
      {
	slip::block<std::size_t, 3> ind_n;
	ind_n[0] = (*(this->powers_))[n][0];
	ind_n[1] = (*(this->powers_))[n][1];
	ind_n[2] = (*(this->powers_))[n][2];
	for(std::size_t i = 0; i < this->rows_; ++i)
	  {
	    for(std::size_t j = 0; j < this->cols_; ++j)
	      {
		slip::multiplies(((*poly_evaluations_)[ind_n]).slice_begin(i,j),
				 ((*poly_evaluations_)[ind_n]).slice_end(i,j),
				 this->slice_base_->weights_begin(),
				 ((*poly_w_evaluations_)[ind_n]).slice_begin(i,j));
	      }
	  }

	for(std::size_t k = 0; k < this->slices_; ++k)
	  {
	    for(std::size_t j = 0; j < this->cols_; ++j)
	      {
		slip::multiplies(((*poly_w_evaluations_)[ind_n]).col_begin(k,j),
				 ((*poly_w_evaluations_)[ind_n]).col_end(k,j),
				 this->col_base_->weights_begin(),
				 ((*poly_w_evaluations_)[ind_n]).col_begin(k,j));
	      }
	    
	  }
	//FIX BUG
	for(std::size_t k = 0; k < this->slices_; ++k)
	  {
	    for(std::size_t i = 0; i < this->rows_; ++i)
	      {
		slip::multiplies(((*poly_w_evaluations_)[ind_n]).row_begin(k,i),
				 ((*poly_w_evaluations_)[ind_n]).row_end(k,i),
				 this->row_base_->weights_begin(),
				 ((*poly_w_evaluations_)[ind_n]).row_begin(k,i));
	      }
	  }
      }
  }

template <typename Iterator>
  void modified_gram_schmidt(const std::vector<std::pair<Iterator,Iterator> >& base)
  {
    slip::Array3d<T> Pk_tmp(this->slices_,this->rows_,this->cols_);
    std::pair<Iterator,Iterator> tmp;
    tmp.first = Pk_tmp.begin();
    tmp.second = Pk_tmp.end();
    
     slip::discrete_poly3d_inner_prod<Iterator,Iterator,T> 
       inner_prod(this->slice_base_->weights_begin(),
		  this->slice_base_->weights_end(),
		  this->row_base_->weights_begin(),
		  this->row_base_->weights_end(),
		  this->col_base_->weights_begin(),
		  this->col_base_->weights_end());
     std::size_t  init_base_first_size = base.size();
       
     //???
     // std::copy(poly_evaluations_->row_begin(0),poly_evaluations_->row_begin(0),
// 	       poly_evaluations_->row_begin(0));

     for(std::size_t k = 0; k < init_base_first_size; ++k)
       {
	  T PkPk = std::sqrt(inner_prod(base[k],base[k]));
	  slip::divides_scalar(base[k].first,base[k].second,
			       PkPk,
			       tmp.first);
	  for(std::size_t q = (k + 1); q < init_base_first_size; ++q)
	    {
	      T PkPq = inner_prod(base[q],tmp);
	      Iterator it_basek = Pk_tmp.begin();
	      Iterator it_baseq = base[q].first;
	      for(; it_baseq != base[q].second; ++it_baseq, ++it_basek)
		{
		  *it_baseq = *it_baseq - PkPq * *it_basek;
		}
	    }
       }
  }

};

typedef slip::DiscretePolyBase3d<double,
                               slip::LegendreWeight<double>,
                               slip::UniformCollocations<double>,
                               slip::LegendreWeight<double>,
			       slip::UniformCollocations<double>,
			       slip::LegendreWeight<double>,
			       slip::UniformCollocations<double> > DLegendreBase3d_d;

typedef slip::DiscretePolyBase3d<float,
                               slip::LegendreWeight<float>,
                               slip::UniformCollocations<float>,
                               slip::LegendreWeight<float>,
			       slip::UniformCollocations<float>,
			       slip::LegendreWeight<float>,
			       slip::UniformCollocations<float> > DLegendreBase3d_f;

}//::slip


#endif //SLIP_DISCRETEPOLYBASE3D_HPP
