/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Block3d.hpp
 * 
 * \brief Provides a class to manipulate 3d static and generic arrays.
 * 
 */
#ifndef SLIP_BLOCK3D_HPP
#define SLIP_BLOCK3D_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <cstddef>
#include <string>
#include "Block2d.hpp"


#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template <typename T, std::size_t NP, std::size_t NR, std::size_t NC>
struct block3d;

template <typename T, std::size_t NP, std::size_t NR, std::size_t NC>
std::ostream& operator<<(std::ostream & out,const slip::block3d<T,NP,NR,NC>& b);

template <typename T, std::size_t NP, std::size_t NR, std::size_t NC>
bool operator==(const slip::block3d<T,NP,NR,NC>& x, const slip::block3d<T,NP,NR,NC>& y);

template <typename T, std::size_t NP, std::size_t NR, std::size_t NC>
bool operator!=(const slip::block3d<T,NP,NR,NC>& x, const slip::block3d<T,NP,NR,NC>& y);

template <typename T, std::size_t NP, std::size_t NR, std::size_t NC>
bool operator<(const slip::block3d<T,NP,NR,NC>& x, const slip::block3d<T,NP,NR,NC>& y);

template <typename T, std::size_t NP, std::size_t NR, std::size_t NC>
bool operator>(const slip::block3d<T,NP,NR,NC>& x, const slip::block3d<T,NP,NR,NC>& y);

template <typename T, std::size_t NP, std::size_t NR, std::size_t NC>
bool operator<=(const slip::block3d<T,NP,NR,NC>& x, const slip::block3d<T,NP,NR,NC>& y);

template <typename T, std::size_t NP, std::size_t NR, std::size_t NC>
bool operator>=(const slip::block3d<T,NP,NR,NC>& x, const slip::block3d<T,NP,NR,NC>& y);

/*! \class block3d
**  \ingroup Containers Containers3d 
** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
** \brief This a three-dimensional static and generic container.
** This container statisfies the BidirectionnalContainer concepts of
** the STL.
** It is also an 3d extension of the RandomAccessContainer concept. That is
** to say the bracket element access is replaced by the triple bracket element 
** access.
** \version 0.0.1
** \date 08/08/2006
** \since 1.0.0
** \param T Type of object in the block3d 
** \param NP number of plan of the block3d
** \param NR number of row of the block3d
** \param NC number of column of the block3d
** \todo add iterator member functions to be compliant with other
**  3d slip interfaces.
*/
template <typename T, std::size_t NP, std::size_t NR, std::size_t NC>
struct block3d
{
  typedef T value_type;
  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;

  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
 
  //default iterator_category of the container
  typedef std::random_access_iterator3d_tag iterator_category;

  //constants
  static const std::size_t SIZE = NP * NR * NC;
  static const std::size_t DIM = 3;

      
  iterator begin();
  iterator end();

  const_iterator begin() const;
  const_iterator end() const;

  reverse_iterator rbegin();
  reverse_iterator rend();

  const_reverse_iterator rbegin() const;
  const_reverse_iterator rend() const;

 /*!
  ** \brief Init the block3d filled it with val value
  ** \param val init value
  */
  void fill(const T& val);
  
 /*!
  ** \brief Init the block3d filled it with values contained by val array
  ** \param val init array value
  */
  void fill(const T* val);

 /**
 ** \name Operators
 */
 /*@{*/
  
 /*!
  ** \brief Write the block2d to ab ouput stream
  ** \param out output stream
  ** \param b block3d to write to an output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out,const slip::block3d<T,NP,NR,NC>& b);

 /*!
  ** \brief Equality Compare operator
  ** \param x first block3d to copare
  ** \param y second block3d to copare
  ** return true if x == y 
  ** \pre x.dim() == y.dim()
  */
  friend bool operator== <>(const slip::block3d<T,NP,NR,NC>& x, const slip::block3d<T,NP,NR,NC>& y);

 /*!
  ** \brief Equality Compare operator
  ** \param x first block3d to copare
  ** \param y second block3d to copare
  ** return true if x != y 
  ** \pre x.dim() == y.dim()
  */
  friend bool operator!= <>(const slip::block3d<T,NP,NR,NC>& x, const slip::block3d<T,NP,NR,NC>& y);

 /*!
  ** \brief LessThan Compare operator
  ** \param x first block3d to copare
  ** \param y second block3d to copare
  ** return true if x < y 
  ** \pre x.dim() == y.dim()
  */
  friend bool operator< <>(const slip::block3d<T,NP,NR,NC>& x, const slip::block3d<T,NP,NR,NC>& y);

 /*!
  ** \brief MoreThan Compare operator
  ** \param x first block3d to copare
  ** \param y second block3d to copare
  ** return true if x > y 
  ** \pre x.dim() == y.dim()
  */
  friend bool operator> <>(const slip::block3d<T,NP,NR,NC>& x, const slip::block3d<T,NP,NR,NC>& y);

 /*!
  ** \brief LessThan Compare operator
  ** \param x first block3d to copare
  ** \param y second block3d to copare
  ** return true if x <= y 
  ** \pre x.dim() == y.dim()
  */
  friend bool operator<= <>(const slip::block3d<T,NP,NR,NC>& x, const slip::block3d<T,NP,NR,NC>& y);

 /*!
  ** \brief MoreThan Compare operator
  ** \param x first block3d to copare
  ** \param y second block3d to copare
  ** return true if x >= y 
  ** \pre x.dim() == y.dim()
  */
  friend bool operator>= <>(const slip::block3d<T,NP,NR,NC>& x, const slip::block3d<T,NP,NR,NC>& y);


 /*!
  ** \brief Returns a pointer to the \a i'th plan of the block3d.
  ** \param i index of the plan to return
  ** \return the pointer of the block2d of the \a i'th plan of the block3d.
  ** \pre i < NP
  */
  slip::block2d<T,NR, NC>& operator[](const std::size_t i);

  /*!
  ** \brief Returns a const pointer to the \a i'th plan of the block3d.
  ** \param i index of the plan to return
  ** \return const the pointer of the block2d of the \a i'th plan of the block3d.
  ** \pre i < NP
  */
  const slip::block2d<T,NR, NC>& operator[](const std::size_t i) const;


  /*!
  ** \brief Returns a reference to the element at the \a i'th plan,
  ** the \a j'th row and the \a k'th column.
  ** \param i index of the plan 
  ** \param j index of the row
  ** \return reference to the element at the \a i'th plan, 
  ** and the \a j'th row
  ** \pre i < NP
  ** \pre j < NR
  */
  T* operator()(const std::size_t i,
		const std::size_t j);
  /*!
  ** \brief Returns a reference to the element at the \a i'th plan,
  ** the \a j'th row.
  ** \param i index of the plan 
  ** \param j index of the row
  ** \return const reference to the element at the \a i'th plan, 
  ** and the \a j'th row
  ** \pre i < NP
  ** \pre j < NR
  */
  const T* operator()(const std::size_t i,
		      const std::size_t j) const;

  /*!
  ** \brief Returns a reference to the element at the \a i'th plan,
  ** the \a j'th row and the \a k'th column.
  ** \param i index of the plan 
  ** \param j index of the row
  ** \param k index of the column
  ** \return reference to the element at the \a i'th plan, 
  ** the \a j'th row and the \a k'th column
  ** \pre i < NP
  ** \pre j < NR
  ** \pre k < NC
  */
  T& operator()(const std::size_t i,
		const std::size_t j,
		const std::size_t k);
  /*!
  ** \brief Returns a reference to the element at the \a i'th plan,
  ** the \a j'th row and the \a k'th column.
  ** \param i index of the plan 
  ** \param j index of the row
  ** \param k index of the column
  ** \return const reference to the element at the \a i'th plan, 
  ** the \a j'th row and the \a k'th column
  ** \pre i < NP
  ** \pre j < NR
  ** \pre k < NC
  */
  const T& operator()(const std::size_t i,
		      const std::size_t j,
		      const std::size_t k) const;

  /*@} End Operators */

  
  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const { return "block3d";}

  /// Returns the dimension of the block3d
  static std::size_t dim() {return SIZE;}
  /// Returns the first dimension of the block3d (number of plans)
  static std::size_t dim1() {return NP;}
  /// Returns the second dimension of the block3d (number of rows)
  static std::size_t dim2() {return NR;}
  /// Returns the third dimension of the block3d (number of columns)
  static std::size_t dim3() {return NC;}
  /// Returns the size (number of elements) of the block3d
  static std::size_t size() {return SIZE;}
  /// Returns the maximal size (number of elements) of the block3d
  static std::size_t size_max() {return SIZE;}
  /// Returns true if the block3d size is 0
  static bool empty(){return SIZE == 0;}

  /*!
  ** \brief Swaps the contents of two block3d
  ** \param M block3d to swap with
  */
  void swap(block3d<T,NP,NR,NC>& M);
 
  /// Prints the elements of the block3d on the standard output
  //void print() const;
 
  slip::block2d<T,NR, NC> data[NP];

  
private:
  friend class boost::serialization::access;
  template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
       if(version >= 0)
	{
	  ar & data;
	}
    }
  template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & data;
	}
    }
  
  BOOST_SERIALIZATION_SPLIT_MEMBER()
};
}//slip::

namespace slip
{
  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  T* block3d<T,NP,NR,NC>::begin()
  {
    return data[0].begin();
  }

  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  T* block3d<T,NP,NR,NC>::end()
  {
    return data[NP-1].end();
  }
  
  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  const T* block3d<T,NP,NR,NC>::begin() const
  {
    return data[0].begin();
  }

  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  const T* block3d<T,NP,NR,NC>::end() const
  {
    return data[NP-1].end();
  }
  

   template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  std::reverse_iterator<T*> block3d<T,NP,NR,NC>::rbegin()
  {
    return std::reverse_iterator<T*>(end());
  }

  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  std::reverse_iterator<T*> block3d<T,NP,NR,NC>::rend()
  {
    return std::reverse_iterator<T*>(begin());
  }
 
  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  std::reverse_iterator<const T*> block3d<T,NP,NR,NC>::rbegin() const
  {
    return std::reverse_iterator<const T*>(end());
  }

  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  std::reverse_iterator<const T*> block3d<T,NP,NR,NC>::rend() const
  {
    return std::reverse_iterator<const T*>(begin());
  }

  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  block2d<T,NR,NC>& block3d<T,NP,NR,NC>::operator[](const std::size_t i)
  {
    assert(i < NP);
    return data[i];
  }

  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  const block2d<T,NR,NC>& block3d<T,NP,NR,NC>::operator[](const std::size_t i) const
  {
    assert(i < NP);
    return data[i];
  }
/** \name input/output operators */
  /* @{ */
  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  std::ostream& operator<<(std::ostream & out,const block3d<T,NP,NR,NC>& b)
  {

    for(std::size_t i = 0; i < NP; ++i)
      {
	out << std::endl << "------------PLAN "<<i<<" ---------------" << std::endl<<b.data[i];
      }    	

    return out;
  }
/* @} */
/** \name EqualityComparable functions */
  /* @{ */
  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
bool operator==(const block3d<T,NP,NR,NC>& x, const block3d<T,NP,NR,NC>& y)
{
  assert(x.dim()==y.dim());
  for(std::size_t n = 0; n < NP; ++n)
    {
      if( x[n] != y[n])
	return false;
    } 
      return true;
}


  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
bool operator!=(const block3d<T,NP,NR,NC>& x, const block3d<T,NP,NR,NC>& y)
{
  assert(x.dim()==y.dim());
  for(std::size_t n = 0; n < NP; ++n)
    {
      if( x[n] == y[n])
	return false;
    } 
      return true;
}
/* @} */
/** \name LessThanComparable functions */
  /* @{ */
  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
bool operator<(const block3d<T,NP,NR,NC>& x, const block3d<T,NP,NR,NC>& y)
{
  assert(x.dim()==y.dim());
  for(std::size_t n = 0; n < NP; ++n)
    {
      if( x[n] >= y[n])
	return false;
    } 
      return true;
}


  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
bool operator>(const block3d<T,NP,NR,NC>& x, const block3d<T,NP,NR,NC>& y)
{
  assert(x.dim()==y.dim());
  for(std::size_t n = 0; n < NP; ++n)
    {
      if( x[n] <= y[n])
	return false;
    } 
      return true;
}


  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
bool operator<=(const block3d<T,NP,NR,NC>& x, const block3d<T,NP,NR,NC>& y)
{
  assert(x.dim()==y.dim());
  for(std::size_t n = 0; n < NP; ++n)
    {
      if( x[n] > y[n])
	return false;
    } 
      return true;
}


  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
bool operator>=(const block3d<T,NP,NR,NC>& x, const block3d<T,NP,NR,NC>& y)
{
  assert(x.dim()==y.dim());
  for(std::size_t n = 0; n < NP; ++n)
    {
      if( x[n] < y[n])
	return false;
    } 
      return true;
}
/* @} */
  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  T* block3d<T,NP,NR,NC>::operator()(const std::size_t i,
				const std::size_t j)
  {
    assert(i < NP);
    assert(j < NR);
    return data[i][j];
  }

  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  const T* block3d<T,NP,NR,NC>::operator()(const std::size_t i,
					const std::size_t j) const
  {
    assert(i < NP);
    assert(j < NR);
    return data[i][j];
  }


  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  T& block3d<T,NP,NR,NC>::operator()(const std::size_t i,
				const std::size_t j,
				const std::size_t k)
  {
    assert(i < NP);
    assert(j < NR);
    assert(k < NC);
    return data[i][j][k];
  }

  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  const T& block3d<T,NP,NR,NC>::operator()(const std::size_t i,
					const std::size_t j,
					const std::size_t k) const
  {
    assert(i < NP);
    assert(j < NR);
    assert(k < NC);
    return data[i * NC * NR + j * NC + k];
  }

  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  void block3d<T,NP,NR,NC>::fill(const T& val) 
  {
	for(std::size_t i = 0; i < NP; ++i){
		data[i].fill(val);
	}
  } 


  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  void block3d<T,NP,NR,NC>::fill(const T* val) 
  {
	for(std::size_t i = 0; i < NP; ++i){
		data[i].fill(&val[i*NR*NC]);
	}
  } 
  	

  template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
  inline
  void block3d<T,NP,NR,NC>::swap(block3d<T,NP,NR,NC>& M)
  {
    std::swap_ranges(begin(),end(),M.begin());
  }

   
  // template<typename T,std::size_t NP, std::size_t NR, std::size_t NC>
//   inline
//   void block3d<T,NP,NR,NC>::print() const
//   {
//     for(std::size_t i = 0; i < NP; ++i)
//       {
// 	std::cout << std::endl << "------------PLAN "<<i<<" ---------------" << std::endl;
// 	data[i].print() ;
//       }    	
//   }

}//slip::

#endif //SLIP_BLOCK3D_HPP
