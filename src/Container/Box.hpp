/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file Box.hpp
 * 
 * \brief Provides an abstract class to manipulate nd box.
 * 
 */
#ifndef SLIP_BOX_HPP
#define SLIP_BOX_HPP

#include <iostream>
#include <cassert>
#include <algorithm>
#include <string>
#include <functional>
#include "Block.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template <typename CoordType, std::size_t DIM>
class Point;

template <typename CoordType, std::size_t DIM>
class DPoint;

template <typename CoordType, std::size_t DIM>
class Box;

template <typename CoordType, std::size_t DIM>
std::ostream& operator<<(std::ostream & out, const Box<CoordType,DIM>& b);


/*!
 *  @defgroup MiscellaneousContainers Miscellaneous containers
 *  @brief Miscellaneous containers
 *  @{
 */
/*! \class Box
**  \ingroup Containers MiscellaneousContainers
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.1
** \date 2006/07/26
** \since 1.0.0
** \brief Define an abstract %Box structure
** \param CoordType Type of the coordinates of the Box 
** \param DIM dimension of the Box
*/
template <typename CoordType, std::size_t DIM>
class Box
{
public :
  typedef CoordType value_type;
  typedef value_type* boxer;
  typedef const value_type* const_boxer;
  typedef value_type& reference;
  typedef const value_type& const_reference;
  typedef Point<CoordType,DIM> point_type;
  typedef DPoint<CoordType,DIM> dpoint_type;
  typedef Box<CoordType,DIM> self;



 /**
 ** \name Constructors & Destructors
 */
 /*@{*/

  /*!
  ** \brief Default constructor (protected to avoid default construction)
  */
  Box();

  /*!
  ** \brief Constructs a copy of the Box \a rhs
  ** \param other 
  */
  Box(const self& other);


  /*!
  ** \brief Constructs a Box from a CoordType array
  ** \param p1 first extremal point
  ** \param p2 second extremal point
  */
  Box(const point_type& p1, 
      const point_type& p2);
  
  /*@} End Constructors */
  
  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;

  /*!
  ** \brief Returns true if the box contains the point \a p
  ** \param p point to analyse
  ** \return 
  */
  bool contains(const point_type& p) const;


  /*!
  ** \brief Translate the window along the deplacement \a dp
  ** the dimensions of the window are conserved
  ** \param dp deplacement of the window 
  ** \return 
  */
  void translate(const dpoint_type& dp);


  /*!
  ** \brief verify if the window is consistent, that is to say
  ** if the first point p1 has the smaller coordinates than p2
  ** \return true if the "p1 < p2"
  */
  bool is_consistent() const;

    
  /*!
  ** \brief Swaps two Box
  ** \param other Box to swap with
  */
  void swap(self& other);
 
 /**
 ** \name i/o operators
 */
 /*@{*/

 /*!
  ** \brief Write the Box to the ouput stream
  ** \param out output stream
  ** \param b Box to write to the output stream
  */  
  friend std::ostream& operator<< <>(std::ostream & out,  const self& b);

  /*@} i/o  operators */
  
   /**
   ** \name  Assignment operators and methods
   */
  /*@{*/
  /*!
  ** \brief Assign a Box.
  **
  ** Assign elements of Box in \a other
  **
  ** \param other Box to get the values from.
  ** \return 
  */
  self& operator=(const self & other);
  /*@} End Assignment operators and methods*/

  /**
   ** \name Comparison operators
   */
  /*@{*/  
  /*!
  ** \brief compare  two Box.
  **
  ** \param other Box to compare
  ** \return true if the two box have the same coordinates
  ** 
  */
  bool operator==(const self& other) const;

  /*!
  ** \brief compare  two Box.
  **
  ** \param other Box to compare
  ** \return true if the two box don't have the same coordinates
  ** 
  */
  bool operator!=(const self& other) const;
 /*@} Comparison operators */  

    
protected:

  
  /*! First extremal coordinate of the box  
  ** 
  */
  point_type p1_;
  /*! Second extremal coordinate of the box  
  ** 
  */
  point_type p2_;

protected:
  friend class boost::serialization::access;
template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
       if(version >= 0)
	{
	  ar & p1_;
	  ar & p2_;
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & p1_;
	  ar & p2_;
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
 };

/** @} */ // end of MiscellaneousContainers group
}//slip::

namespace slip
{

  template<typename CoordType,std::size_t DIM>
  inline
  Box<CoordType,DIM>::Box()
  {}

  template<typename CoordType,std::size_t DIM>
  inline
  Box<CoordType,DIM>::Box(const Box<CoordType,DIM>& other):
    p1_(other.p1_),p2_(other.p2_)
  {}

  template<typename CoordType,std::size_t DIM>
  inline
  Box<CoordType,DIM>::Box(const Point<CoordType,DIM>& p1,
			  const Point<CoordType,DIM>& p2):
    p1_(p1),p2_(p2)
  {}
  
  template<typename CoordType,std::size_t DIM>
  inline
  Box<CoordType,DIM>& Box<CoordType,DIM>::operator=(const Box<CoordType,DIM> & other)
  {
    if(this != &other)
      {
	this->p1_ = other.p1_;
	this->p2_ = other.p2_;
      }
    return *this;
  }

  template<typename CoordType,std::size_t DIM>
  inline
  std::string 
  Box<CoordType,DIM>::name() const {return "Box";} 

  //TO Optimize : double iteration of coord_ !!
  template<typename CoordType,std::size_t DIM>
  inline
  bool Box<CoordType,DIM>::contains(const Point<CoordType,DIM>& p) const 
  {
    assert(this->is_consistent());
    bool inf = std::lexicographical_compare((p.coord()).begin(),(p.coord()).end(),(p1_.coord()).begin(),(p1_.coord()).end(),std::greater_equal<CoordType>());
    bool sup = std::lexicographical_compare((p.coord()).begin(),(p.coord()).end(),(p2_.coord()).begin(),(p2_.coord()).end(),std::less_equal<CoordType>());
    return inf && sup;
  }

  template<typename CoordType,std::size_t DIM>
  inline
  void Box<CoordType,DIM>::translate(const DPoint<CoordType,DIM>& dp)
  {
    this->p1_+=dp;
    this->p2_+=dp;
  }

  template<typename CoordType,std::size_t DIM>
  inline
  bool Box<CoordType,DIM>::is_consistent()const 
  {
    dpoint_type diff = this->p2_ - this->p1_;
    typename slip::block<CoordType,DIM>::const_iterator first_negative = std::find_if((diff.coord()).begin(),(diff.coord()).end(), std::bind2nd(std::less<CoordType>(), CoordType(0)));
    if(first_negative ==(diff.coord()).end())
      return 1;
    return 0;
  }
  

  template<typename CoordType,std::size_t DIM>
  inline
  void Box<CoordType,DIM>::swap(Box<CoordType,DIM>& other)
  {
    (this->p1_).swap(other.p1_);
    (this->p2_).swap(other.p2_);
  }
  
  template<typename CoordType,std::size_t DIM>
  inline
  std::ostream& operator<<(std::ostream & out, const Box<CoordType,DIM>& b)
  {
    out<<"[ "<<b.p1_<<","<<b.p2_<<" ]";
    return out;
  }

  template<typename CoordType,std::size_t DIM>
  inline
  bool Box<CoordType,DIM>::operator==(const Box<CoordType,DIM>& other) const
  {
    return (this->p1_ == other.p1_) && (this->p2_ == other.p2_);
  }

  template<typename CoordType,std::size_t DIM>
  inline
  bool Box<CoordType,DIM>::operator!=(const Box<CoordType,DIM>& other) const
  {
    return (this->p1_ != other.p1_) || (this->p2_ != other.p2_);
  }
  

}//slip::

#endif //SLIP_BOX_HPP
