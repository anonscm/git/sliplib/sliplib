/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file RegularPointGrid2d.hpp
 * 
 * \brief Provides a class to define a feature point list indexed according to their coordinates within a regular grid. Inserting, erasing and finding are of constant complexity.
 * 
 */
#ifndef SLIP_REGULARPOINTGRID2D_HPP
#define SLIP_REGULARPOINTGRID2D_HPP

#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>
#include <ios>
#include <limits>

//#include <vector>
#include <unordered_set>
#include "Point2d.hpp"
#include "Array2d.hpp"
#include "Array.hpp"
#include "Color.hpp"
#include "error.hpp"
#include "dynamic.hpp"
#include "arithmetic_op.hpp"
#include "compare.hpp"
//#include "svg_shapes.hpp"
#include "svg_image_descriptor_types.hpp"

#include <boost/iterator/indirect_iterator.hpp>
#include "uniform_random_sampling.hpp"
#include "QuadTreeBox.hpp"


namespace slip
{
  template <typename FPoint>
class RegularPointGrid2d;

  template <typename FPoint>
std::ostream& operator<< (std::ostream&,
			  const RegularPointGrid2d<FPoint>&);

/*! \class RegularPointGrid2d
** \ingroup Containers PointTreeContainers
**  \brief The class defines a feature point list indexed according to their coordinates within a regular grid. Inserting, erasing and finding are of constant complexity.
** 
** \author Benoit Tremblais
** \version 0.0.1
** \date 2016/09/15
** \since 1.5.0
** \param FPoint slip::FeaturePoint<Point,Features> describing the Point of the RegularPointGrid2d.
** \todo swap ? 
*/
template <typename FPoint>
class RegularPointGrid2d
{
public:
  typedef RegularPointGrid2d<FPoint> self;
  typedef const RegularPointGrid2d<FPoint> const_self;
  
  typedef FPoint value_type;
  typedef value_type& reference;
  typedef const value_type& const_reference;
  typedef std::ptrdiff_t difference_type;
  typedef std::size_t size_type;

 
  typedef typename boost::indirect_iterator<typename std::unordered_set<FPoint*>::const_iterator> const_iterator;
 typedef typename boost::indirect_iterator<typename std::unordered_set<FPoint*>::iterator> iterator;
  typedef typename iterator::pointer pointer;
  typedef const pointer const_pointer;
  
  

  //default iterator of the container
  typedef iterator default_iterator;
  typedef const_iterator const_default_iterator;

  typedef typename FPoint::point_type point_type;
  typedef typename FPoint::features_type features_type;
  typedef typename point_type::value_type point_value_type;
 

protected:
  slip::QuadTreeBox<point_type> box_;///bounding box of the Points
  std::size_t max_point_number_; ///maximal points number in a grid element
  point_value_type margin_; ///margin 
  point_value_type  dx_; /// x grid step
  point_value_type  dy_;/// y grid step
  point_value_type  inv_dx_; /// inverse of x grid step
  point_value_type  inv_dy_;/// inverse of y grid step
  std::size_t width_; ///number of cells in x
  std::size_t height_; ///number of cells in y
  std::unordered_set<FPoint*>* point_list_; /// points list
  slip::Array2d<std::unordered_set<FPoint*>* >* grid_; ///2d Array of point list
  std::unordered_set<FPoint*>* query_point_list_;///query point list contains the pointers to the features points corresponding to the query


public:
  /**
   ** \name Constructors & Destructors & operator=
   */
  /*@{*/

  /*!
  ** \brief Default constructor
  */
  RegularPointGrid2d():
    box_(slip::QuadTreeBox<point_type>(point_value_type(0),point_value_type(0),point_value_type(1),point_value_type(1))),
    max_point_number_(std::size_t(1)),
    margin_(point_value_type()),
    dx_(static_cast<point_value_type>(1)),
    dy_(static_cast<point_value_type>(1)),
    inv_dx_(static_cast<point_value_type>(1)),
    inv_dy_(static_cast<point_value_type>(1)),
    width_(static_cast<std::size_t>(1)),
    height_(static_cast<std::size_t>(1)),
    point_list_(nullptr),
    grid_(nullptr),
    query_point_list_(nullptr)
  {
    this->allocate();
    this->reserve_memory(this->max_point_number_);
  }


   /*!
   ** \brief Constructs a RegularPointGrid2d which box is computed from the coordinates of the features points in the given range [first,last).
   ** \param first  first iterator of the range to copy the elements from.
   ** \param last past-the-end iterator of the range to copy the elements from.
   ** \param points_per_grid_element desired point number per grid element.
   ** \param margin margin around of the bounding box of the features points in the range [first, last) (useful if feature point have a size != 0)
   
   */
  template <typename InputIterator>
  RegularPointGrid2d(InputIterator first, 
		     InputIterator last,
		     const std::size_t points_per_grid_element,
		     const point_value_type& margin = point_value_type()):
    box_(this->compute_bounding_box(first,last,margin)),
    max_point_number_(points_per_grid_element),
    margin_(margin),
    dx_(point_value_type()),
    dy_(point_value_type()),
    inv_dx_(point_value_type()),
    inv_dy_(point_value_type()),
    width_(static_cast<std::size_t>(1)),
    height_(static_cast<std::size_t>(1)),
    point_list_(nullptr),
    grid_(nullptr),
    query_point_list_(nullptr)
  {
  
    this->compute_grid_parameters(static_cast<std::size_t>(last-first),
				  points_per_grid_element);
    this->allocate();
    this->reserve_memory(last-first);
    //constructs the point_list
    for(;first != last; ++first)
      {
	this->insert(*first);
      }
    
   
  }

 /*!
   ** \brief Constructs a RegularPointGrid2d with box \a box with \a n feature points which coordinates have been uniformly sampled inside the box and a desired  point number per grid element.
   ** Tries to constructs an equilibrate partition of points.
   ** \param n number of feature points.
   ** \param box surrounding box of the RegularPointGrid2d.
   ** \param estimated_point_number estimation of the number of point to handle.
   ** \param points_per_grid_element desired  point number per grid element.
   ** \param margin  margin around of the bounding box  (useful if feature point have a size != 0).
   */
  RegularPointGrid2d(const std::size_t n, 
		      const slip::QuadTreeBox<point_type>& box,
		      const std::size_t points_per_grid_element,
		      const point_value_type& margin = point_value_type()):
    box_(box),
    max_point_number_(points_per_grid_element),
    margin_(margin),
    dx_(point_value_type()),
    dy_(point_value_type()),
    inv_dx_(point_value_type()),
    inv_dy_(point_value_type()),
    width_(static_cast<std::size_t>(1)),
    height_(static_cast<std::size_t>(1)),
    point_list_(nullptr),
    grid_(nullptr),
    query_point_list_(nullptr)
  {
    this->box_.make_consistent();
   
    this->compute_grid_parameters(n,
				  points_per_grid_element);
    this->allocate();
    this->reserve_memory(n);
  
    slip::Rand1<point_value_type> rndx(this->box_.minimal().x1()+margin,
				       this->box_.maximal().x1()-margin);
    slip::Rand1<point_value_type> rndy(this->box_.minimal().x2()+margin,
				       this->box_.maximal().x2()-margin);
      
    
    for(std::size_t k = 0; k < n; ++k)
      {
	FPoint fp;
	fp.set_point(point_type(rndx(),rndy()));
	this->insert(fp);
      }
    
   
  }

 /*!
   ** \brief Constructs a RegularPointGrid2d with box \a box given an estimaton of the point number and a desired  point number per grid element.
   ** Tries to constructs an equilibrate partition of points.
   ** \param box surrounding box of the RegularPointGrid2d.
   ** \param estimated_point_number estimation of the number of point to handle.
   ** \param points_per_grid_element desired  point number per grid element.
   ** \param margin  margin around of the bounding box  (useful if feature point have a size != 0).
   */
  RegularPointGrid2d(const slip::QuadTreeBox<point_type>& box,
  		     const std::size_t estimated_point_number,
  		     const std::size_t points_per_grid_element,
  		     const point_value_type& margin = point_value_type()):
    box_(slip::QuadTreeBox<point_type>(box.xmin()-margin,box.ymin()-margin,box.xmax()+margin,box.ymax()+margin)),
    max_point_number_(points_per_grid_element),
    margin_(margin),
    dx_(point_value_type()),
    dy_(point_value_type()),
    inv_dx_(point_value_type()),
    inv_dy_(point_value_type()),
    width_(static_cast<std::size_t>(1)),
    height_(static_cast<std::size_t>(1)),
    point_list_(nullptr),
    grid_(nullptr),
    query_point_list_(nullptr)
  {
    this->box_.make_consistent();
    this->compute_grid_parameters(estimated_point_number,
				  points_per_grid_element);
    this->allocate();
    this->reserve_memory(estimated_point_number);
  }
  
 
  /*!
  ** \brief Copy constructor
  ** \param other other RegularPointGrid2d to copy
  */
  RegularPointGrid2d(const self& other):
    box_(other.box_),
    max_point_number_(other.max_point_number_),
    margin_(other.margin_),
    dx_(other.dx_),
    dy_(other.dy_),
    inv_dx_(other.inv_dx_), 
    inv_dy_(other.inv_dy_),
    width_(other.width_),
    height_(other.height_),
    point_list_(nullptr),
    grid_(nullptr),
    query_point_list_(nullptr)
  {
    this->allocate();
    this->copy_data(other);
  }

  /*!
  ** \brief Destructor
  */
  virtual ~RegularPointGrid2d()
  {
    this->desallocate();
  }

  /*!
  ** \brief Assigns a %RegularPointGrid2d in \a rhs
  **
  ** \param rhs %RegularPointGrid2d to get the values from.
  ** \return *this
  */
  self& operator=(const self& rhs)
  {
     if(this != &rhs)
      {
	this->desallocate();
	this->copy_attributes(rhs);
	this->allocate();
	this->copy_data(rhs);
      }
     return *this;
  }

/*@} End Constructors */

/**
   ** \name iterators
   */
  /*@{*/
  /**
   ** \brief Returns an iterator to the first element of the container.
   ** If the container is empty, the returned iterator will be equal to end()
   ** \return Iterator to the first element 
   */
  const_iterator cbegin() const
  {
    return const_iterator(this->point_list_->begin());
  }
  
  /**
   ** \brief Returns an iterator to the element following the last element of the container.

   **This element acts as a placeholder; attempting to access it results in undefined behavior. 
   \return Iterator to the element following the last element
   */
  const_iterator cend() const
  {
    return const_iterator(this->point_list_->end());
  }

  /**
   ** \brief Returns an iterator to the first element of the container.
   ** If the container is empty, the returned iterator will be equal to end()
   ** \return Iterator to the first element 
   */
  iterator begin() 
  {
    return iterator(this->point_list_->begin());
  }
  
  /**
   ** \brief Returns an iterator to the element following the last element of the container.

   **This element acts as a placeholder; attempting to access it results in undefined behavior. 
   \return Iterator to the element following the last element
   */
  iterator end() const
  {
    return iterator(this->point_list_->end());
  }

 
  /*@} End iterators */

 
  /**
   ** \name Accessors
   */
  /*@{*/
/*!
  ** \brief Returns the maximal number of points that should contains a RegularPointGrid2d in its nodes.
  ** \return std::size_t
  */
  const  std::size_t& max_point_number() const
  {
    return this->max_point_number_;
  }

  /*!
  ** \brief Returns the number of points per grid element that a %RegularPointGrid2d should contains.
  ** \return std::size_t
  */
  const  std::size_t& points_per_grid_element() const
  {
    return this->max_point_number_;
  }

  /*!
  ** \brief Returns the  load factor i.e the average number of elements per grid cell.
  ** \return double
  */
  double load_factor() const
  {
    double res = 0.0;
    const std::size_t rows = this->grid_->rows();
    const std::size_t cols = this->grid_->cols();
    
    for(std::size_t i = 0; i < rows; ++i)
      {
	 for(std::size_t j = 0; j < cols; ++j)
	   {
	     res += (*this->grid_)[i][j]->size();
	   }
      }
    return res/double(rows*cols);
  }
  /*!
  ** \brief Returns the  number of empty cells in the grid.
  ** \return the number of empty cells.
  */
  std::size_t empty_cells() const
  {
    const std::size_t rows = this->grid_->rows();
    const std::size_t cols = this->grid_->cols();
    std::size_t empty_cells = 0;
    for(std::size_t i = 0; i < rows; ++i)
      {
	 for(std::size_t j = 0; j < cols; ++j)
	   {
	     if((*this->grid_)[i][j]->size() == 0)
	       {
		 empty_cells++;
	       }
	   }
      }
    return empty_cells;
  }

  /*!
  ** \brief Returns the  number of non empty cells in the grid.
  ** \return the number of non empty cells.
  */
  std::size_t non_empty_cells() const
  {
    return this->grid_->size() - this->empty_cells();
  }
  
  /*!
  ** \brief Returns the x size of each grid cell.
  ** \return point_value_type
  */
  const  point_value_type& dx() const
  {
    return this->dx_;
  }
  /*!
  ** \brief Returns the y size of each grid cell.
  ** \return point_value_type
  */
  const  point_value_type& dy() const
  {
    return this->dy_;
  }
  
 

  /*!
  ** \brief Returns the QuadTreeBox associated to the RegularPointGrid2d.
  ** \return slip::QuadTreeBox<point_type> reference
  */
  const  slip::QuadTreeBox<point_type>& box() const
  {
    return this->box_;
  }


/*!
  ** \brief Returns the name of the class.
  ** \return std::string
  */
  const std::string name() const
  {
    return "RegularPointGrid2d"; 
  }

 /*@} End accessors */

 /**
   ** \name Modifiers
   */
  /*@{*/

 
  /** \brief Inserts feature point fpoint into the container.
   ** \param fpoint feature point to insert.
   ** \return  Returns a pair consisting of an iterator to the inserted element (or to the element that prevented the insertion) and a bool denoting whether the insertion took place.
   */
  std::pair<iterator,bool> insert(const FPoint& fpoint)
  {
    bool inserted = false;
   
    std::pair<typename std::unordered_set<FPoint*>::iterator,bool> res_ins;
    std::pair<iterator,bool> res;
    std::size_t i;
    std::size_t j;
    this->compute_indices(fpoint.get_point(),i,j);
    //    std::clog<<"(i,j) = ("<<i<<","<<j<<")"<<std::endl; 
    //if valid indices put FPoint to point_list_ and into the good grid element
    if(this->valid_indices(i,j))
      {
	FPoint* fp_ptr= new FPoint(fpoint);
	//std::clog<<"add point to the point list"<<std::endl;
	res_ins = this->point_list_->insert(fp_ptr);
	//std::clog<<"put the point into the good grid element"<<std::endl;
	if(res_ins.second)
	  {
	    (*this->grid_)[i][j]->insert(fp_ptr);	
	  }
	res.first = iterator(res_ins.first);
	res.second = res_ins.second;
      }
    else
      {
	//std::clog<<"point out of the grid"<<std::endl;
	res.first = iterator(this->end());
	res.second = inserted;
	
      }
    return res;
  }
  /** \brief Inserts elements from range [first, last) into the container.
   ** \param first first iterator of the range of elements to insert.
   ** \param last  past-the-end iterator of the range of elements to insert.    */
  template <typename InputIterator>
  void insert(InputIterator first,
	      InputIterator last)
  {
    for(; first != last; ++first)
      {
	this->insert(*first);
      }
  }
  
 /** \brief Inserts feature point fpoint  into the container and update surrounding box and grid if feature point outside the current RegularPointGrid2d box.
   ** \param fpoint feature point to insert.
   ** \return  Returns a pair consisting of an iterator to the inserted element (or to the element that prevented the insertion) and a bool denoting whether the insertion took place.
   ** \remarks slower than void insert(const FPoint& fpoint)
   */
   void insert_update(const FPoint& fpoint)
  {
    std::size_t i;
    std::size_t j;
    this->compute_indices(fpoint.get_point(),i,j);
    //std::clog<<"(i,j) = ("<<i<<","<<j<<")"<<std::endl;
    FPoint* fp_ptr= new FPoint(fpoint);
    //if valid indices put FPoint to point_list_ and into the good grid element
    if(!this->valid_indices(i,j))
      {
	std::clog<<"update grid"<<std::endl;
	this->update_bounding_box(fpoint);
	this->compute_grid_parameters(this->size()+1,
				      this->max_point_number_);
	//desallocate grid
	this->partial_desallocate();
	//allocate grid with new dimensions
	this->partial_allocate();
	//put old features points in the new grid 
	for(auto it = this->begin(); it != this->end() ;++it)
	  {
	    this->compute_indices(it->get_point(),i,j);
	    (*this->grid_)[i][j]->insert(*(it.base()));
	  }
	//compute new indices 
	this->compute_indices(fpoint.get_point(),i,j);
	//	std::clog<<"new (i,j) = ("<<i<<","<<j<<")"<<std::endl;
	
      }
    //std::clog<<"add point to the point list"<<std::endl;
    this->point_list_->insert(fp_ptr);
    //std::clog<<"put the point into the good grid element"<<std::endl;
    (*this->grid_)[i][j]->insert(fp_ptr);
    
  }

   /** \brief Inserts elements from range [first, last)  into the container into the containerand update surrounding box and grid if feature points are outside the current RegularPointGrid2d box.
   ** \param first first iterator of the range of elements to insert.
   ** \param last  past-the-end iterator of the range of elements to insert.    */
  template <typename InputIterator>
  void insert_update(InputIterator first,
		     InputIterator last)
  {
    //update bounding box
    slip::QuadTreeBox<point_type> range_bouding_box = this->compute_bounding_box(first,last,this->margin_);
    this->box_.xmin() = std::min(this->box_.xmin(),range_bouding_box.xmin());
    this->box_.ymin() = std::min(this->box_.ymin(),range_bouding_box.ymin());
    this->box_.xmax() = std::max(this->box_.xmax(),range_bouding_box.xmax());
    this->box_.ymax() = std::max(this->box_.ymax(),range_bouding_box.ymax());
    
    //computes new parameters
    this->compute_grid_parameters(this->size()+static_cast<std::size_t>(last-first),
				  this->max_point_number_);
    //desallocate grid
    this->partial_desallocate();
    //allocate grid with new dimensions
    this->partial_allocate();
    //put old features points in the new grid 
    for(auto it = this->begin(); it != this->end() ;++it)
      {
	std::size_t i = 0;
	std::size_t j = 0;
	this->compute_indices(it->get_point(),i,j);
	(*this->grid_)[i][j]->insert(*(it.base()));
      }
   
    //insert new elements of the range [first,last)
    this->insert(first,last);
  }

  /**
   ** \brief Removes the element at pos
   ** \pre The iterator pos must be valid and dereferenceable. Thus the end() iterator (which is valid, but is not dereferencable) cannot be used as a value for pos. 
    ** \post References and iterators to the erased elements are invalidated. Other references and iterators are not affected. 
   ** \param  pos const_iterator to the element to remove 
   ** \return Iterator following the last removed element.
   ** \todo fix memory lack with valgrind
   */
   iterator erase(const_iterator pos)
   {
     iterator result = this->end();
     //computes cell of the feature point
    std::size_t i = 0;
    std::size_t j = 0;
    this->compute_indices(pos->get_point(),i,j);
    if(this->valid_indices(i,j))
      {
	//if grid cell as elements
	if((*this->grid_)[i][j]->size() != 0)
	  {
	    //find feature point in the local grid cell
	    iterator local_result = std::find(iterator((*this->grid_)[i][j]->begin()),iterator((*this->grid_)[i][j]->end()),*pos);
	    if(local_result != iterator((*this->grid_)[i][j]->end()))
	      {
		
		typename std::unordered_set<FPoint*>::iterator local_res_it = local_result.base();
				
		//find equivalent feature point in the global feature point set
		typename std::unordered_set<FPoint*>::iterator res = this->point_list_->find(*local_res_it);
		//erase the FPoint* in the local grid cell feature point set
		(*this->grid_)[i][j]->erase(local_res_it);
		//delete feature point  in the global feature point set
		if(*res != nullptr)
		  {
		    delete *res;
		  }
	
		typename std::unordered_set<FPoint*>::iterator res_next = this->point_list_->erase(res);
	
		result = iterator(res_next);
		
	      }
	  }

      }
    
    //return this->end();
    return result;
   }

 
  /**
   ** \brief Removes the element in the range [first; last), which must be a valid range in *this.
   ** \pre The iterator pos must be valid and dereferenceable. Thus the end() iterator (which is valid, but is not dereferencable) cannot be used as a value for pos. 
   ** \param  first iterator of the range of elements to remove. 
   ** \param last past-the-end iterator of the range of elements to remove. 
   ** \return Iterator following the last removed element.
   ** \post References and iterators to the erased elements are invalidated. Other references and iterators are not affected. 
   */
  const_iterator erase(const_iterator first, 
		       const_iterator last)
  {
    const_iterator result = first;
    for(; first != last; ++first)
      {
	result = this->erase(first);
      }
    //iterator it(result);
    return result;
    //return std::advance(it,std::distance<const_iterator>(it,result));
  }

   /**
   ** \brief Removes the element fpoint
   ** \param  fpoint feature point to remove 
   ** \return Number of elements removed.
   */
  // size_type erase(const FPoint& fpoint)
  // {
  // }

  /**
   ** \brief Removes all elements from the container. 
   ** \post Invalidates any references, pointers, or iterators referring to contained elements. Any past-the-end iterator remains valid. 
   */
  void clear()
  {
    this->query_point_list_->clear();
    if(this->grid_ != nullptr)
       {
	 for(auto it = this->grid_->begin(); it !=this->grid_->end(); ++it)
	   {
	     if(*it != nullptr)
	       {
		 (*it)->clear();
	       }
	   }
       }
     if(this->point_list_ != nullptr)
      {
	typename std::unordered_set<FPoint*>::iterator first = this->point_list_->begin();
	
	for(;first != this->point_list_->end(); ++first)
	  {
	    delete *first;
	  }
	
	this->point_list_->clear();
      }
   
  }

  // void swap(const self& other)
  // {
  //   std::swap(this.box_,other.box_);
  //   std::swap(this.max_point_number_,other.max_point_number_);
  //   std::swap(this.margin_,other.margin_);
  //   std::swap(this.dx_,other.dx_);
  //   std::swap(this.dy_,other.dy_);
  //   std::swap(this.inv_dx_,other.inv_dx_);
  //   std::swap(this.inv_dy_,other.inv_dy_);
  //   //swap data
  // reallocate if size are different ?
  // }

  /*@} End Modifiers */

  
 /**
   ** \name Analyse
   */
  /*@{*/
   /*
  **\brief Computes the pair wise distance matrix between features points
  ** of the RegularPointGrid2d
  ** \param DistanceMatrix a Container2d
  ** \param dist_fun a distance functor
  ** 
  */
  template <typename Container2d,
	    typename BinaryFunctor>
  void distance(Container2d& DistanceMatrix,
		BinaryFunctor dist_fun)
  {
    typedef typename Container2d::value_type value_type;
    DistanceMatrix.resize(this->size(),this->size(),value_type());
    const_iterator itb1 = this->begin();
   
    const std::size_t n = this->size();
    for(std::size_t i = 0; i < n; ++i, ++itb1)
      {
	const_iterator itb2 = this->begin();
	for(std::size_t j = 0; j < i; ++j, ++itb2)
	  {
	    if(i != j)
	      {
		DistanceMatrix[i][j] = slip::distance<value_type>(itb1->get_point().coord().begin(),itb1->get_point().coord().end(),itb2->get_point().coord().begin(),dist_fun);
		DistanceMatrix[j][i] = DistanceMatrix[i][j];
	      }
	  }
      }
    
  }
  
  /*@} End Analyse */
  
  /**
   ** \name Lookup
   */
  /*@{*/

  
  /**
  ** \brief Returns a range containing all elements which feature points are inside the box \a box.
  ** \param box interrogating box.
  ** \param query_point_number number of feature points in the box.
  ** \return std::pair containing a pair of iterators defining the wanted range. If there are no such elements, past-the-end (see end()) iterators are returned as both elements of the pair. 
  \todo optimize inserting all feature points in the cells inside and testing only at bordering cells
  */
  std::pair<const_iterator,const_iterator> 
  points_in_range(const slip::QuadTreeBox<point_type>& box,
		  std::size_t& query_points_number)
  {
    this->query_point_list_->clear();
    std::pair<const_iterator,const_iterator> result(const_iterator(this->query_point_list_->end()),const_iterator(this->query_point_list_->end()));

    point_value_type pmin_x = point_value_type();
    point_value_type pmin_y = point_value_type();
    point_value_type pmax_x = point_value_type();
    point_value_type pmax_y = point_value_type();
    bool as_intersection =  this->boxes_intersection(box,
						     pmin_x,pmin_y,
						     pmax_x,pmax_y);
    
    if(as_intersection)
      {
	std::size_t imax = 0;
	std::size_t jmin = 0;
	std::size_t imin = 0;
	std::size_t jmax = 0;
	
	this->boxes_intersection_indices(pmin_x,pmin_y,
					 pmax_x,pmax_y,
					 imin,jmin,
					 imax,jmax);


	for(std::size_t i = imin; i <= imax; ++i)
	  {
	    for(std::size_t j = jmin; j <= jmax; ++j)
	      {
		if((*this->grid_)[i][j]->size() != 0)
		  {
		    typename std::unordered_set<FPoint*>::const_iterator itb = (*this->grid_)[i][j]->begin();
		    typename std::unordered_set<FPoint*>::const_iterator ite = (*this->grid_)[i][j]->end();
		    for(; itb !=ite; ++itb)
		      {
			if(box.contains((**itb).get_point()))
			  {
			    this->query_point_list_->insert(*itb);
			  }
		      }
		  }
	      }
	  }

	result.first = const_iterator(this->query_point_list_->begin());
	result.second  = const_iterator(this->query_point_list_->end());

      }

    query_points_number = this->query_point_list_->size();
    return result;
    
  }

 /**
  ** \brief Returns a vector containing all pointers toxard elements which feature points are inside the box \a box.
  ** \param box interrogating box.
  ** \param query_point_number number of feature points in the box.
  ** \return vector of pointers towards the particles
  ** \todo test points only at the border of the grid
  */
  std::vector<FPoint*>
  return_points_in_range(const slip::QuadTreeBox<point_type>& box,
		  std::size_t& query_points_number) 
  {
    std::vector<FPoint*> result;

    point_value_type pmin_x = point_value_type();
    point_value_type pmin_y = point_value_type();
    point_value_type pmax_x = point_value_type();
    point_value_type pmax_y = point_value_type();
    bool as_intersection =  this->boxes_intersection(box,
						     pmin_x,pmin_y,
						     pmax_x,pmax_y);
    
    if(as_intersection)
      {
	std::size_t imax = 0;
	std::size_t jmin = 0;
	std::size_t imin = 0;
	std::size_t jmax = 0;
	
	this->boxes_intersection_indices(pmin_x,pmin_y,
					 pmax_x,pmax_y,
					 imin,jmin,
					 imax,jmax);


	for(std::size_t i = imin; i <= imax; ++i)
	  {
	    for(std::size_t j = jmin; j <= jmax; ++j)
	      {
		if((*this->grid_)[i][j]->size() != 0)
		  {
		    typename std::unordered_set<FPoint*>::const_iterator itb = (*this->grid_)[i][j]->begin();
		    typename std::unordered_set<FPoint*>::const_iterator ite = (*this->grid_)[i][j]->end();
		    for(; itb !=ite; ++itb)
		      {
			if(box.contains((**itb).get_point()))
			  {
			    result.push_back(&(**itb));
			    //this->query_point_list_->insert(*itb);
			  }
		      }
		  }
	      }
	  }

	// result.first = const_iterator(this->query_point_list_->begin());
	// result.second  = const_iterator(this->query_point_list_->end());

      }

    query_points_number = result.size();
    return result;
    
  }


  
  /**
  ** \brief Counts the number of feature points in the box \a box.
  ** \param box interrogating box.
  ** \return number of feature points in the box.
  */
  std::size_t  count_in_range(const slip::QuadTreeBox<point_type>& box)
  {
    std::size_t result = std::size_t(0);
    this->points_in_range(box,result);
    return result;
  }

  /**
  ** \brief Estimates the number of feature points in the box \a box.
  ** 
  ** \param box interrogating box.
  ** \return number of cells within the box multiplies by the wished number of element by cell
  */
  std::size_t  estimated_count_in_range(const slip::QuadTreeBox<point_type>& box)
  {
    std::size_t result = std::size_t(0);
    point_value_type pmin_x = point_value_type();
    point_value_type pmin_y = point_value_type();
    point_value_type pmax_x = point_value_type();
    point_value_type pmax_y = point_value_type();
    bool as_intersection =  this->boxes_intersection(box,
						     pmin_x,pmin_y,
						     pmax_x,pmax_y);

    if(as_intersection)
      {
	std::size_t imax = 0;
	std::size_t jmin = 0;
	std::size_t imin = 0;
	std::size_t jmax = 0;
	
	this->boxes_intersection_indices(pmin_x,pmin_y,
					 pmax_x,pmax_y,
					 imin,jmin,
					 imax,jmax);
	result = (imax-imin+1)*(jmax-jmin+1) * this->max_point_number_;
      }
    return result;
  }

  

  /**
  ** \brief Returns a range containing all elements which feature points are inside the circle of center \a center and radius \a radius.
  ** \param center center of the circle.
  ** \param radius radius of the circle.
  ** \param query_point_number number of feature points in the box.
  ** \return std::pair containing a pair of iterators defining the wanted range. If there are no such elements, past-the-end (see end()) iterators are returned as both elements of the pair. 
  * \todo eliminates tests on interior square of the circle to accelerate algorithm
  */
  std::pair<const_iterator,const_iterator> 
  points_in_circle(const point_type& center,
		   const point_value_type& radius,
		   std::size_t& query_points_number)
  {
     this->query_point_list_->clear();
    std::pair<const_iterator,const_iterator> result(const_iterator(this->query_point_list_->end()),const_iterator(this->query_point_list_->end()));
 
    point_value_type pmin_x = point_value_type();
    point_value_type pmin_y = point_value_type();
    point_value_type pmax_x = point_value_type();
    point_value_type pmax_y = point_value_type();
    bool as_intersection = 
      this->boxes_intersection_outside(center,
				       radius,
				       pmin_x,pmin_y,
				       pmax_x,pmax_y);
    if(as_intersection)
      {
	    std::size_t imax = 0;
	    std::size_t jmin = 0;
	    std::size_t imin = 0;
	    std::size_t jmax = 0;
	    
	    this->boxes_intersection_indices(pmin_x,pmin_y,
					     pmax_x,pmax_y,
					     imin,jmin,
					     imax,jmax);

	    for(std::size_t i = imin; i <= imax; ++i)
	      {
		for(std::size_t j = jmin; j <= jmax; ++j)
		  {
		    if((*this->grid_)[i][j]->size() != 0)
		      {
			typename std::unordered_set<FPoint*>::const_iterator itb = (*this->grid_)[i][j]->begin();
			typename std::unordered_set<FPoint*>::const_iterator ite = (*this->grid_)[i][j]->end();
			for(; itb !=ite; ++itb)
			  {
			    if(this->is_inside_circle((**itb).get_point(),center,radius))
			      {
				this->query_point_list_->insert(*itb);
			      }
			  }
		      }
		  }
	      }
	 
	    result.first = const_iterator(this->query_point_list_->begin());
	    result.second  = const_iterator(this->query_point_list_->end());
      }
	  
    query_points_number = this->query_point_list_->size();

    return result;
  }

  /**
   ** \brief Count the number of feature points in the circle of center \a center and radius \a radius.
   ** \param center center of the circle.
   ** \param radius radius of the circle.
   ** \return the number of feature points in the circle.
   */
  std::size_t  count_in_circle(const point_type& center,
			       const point_value_type& radius)
  {
    std::size_t result = std::size_t(0);
    this->points_in_circle(center,radius,result);
    return result;
  }

  /**
   ** \brief Finds an element with point equivalent to the feature point p
   ** \param fp feature point to search for 
   ** \return Iterator to an element with feature point equivalent to key. If no such element is found, past-the-end (see end()) iterator is returned.
   ** \par Example:
   ** \code
   ** typedef typename slip::EmptyFeature Feature;
   ** typedef slip::FeaturePoint <slip::Point2d<T>,Feature> FPoint;
   ** FPoint p1(slip::Point2d<T>(T(-0.5),T(5.0)),slip::EmptyFeature());
   ** FPoint p2(slip::Point2d<T>(T(0.0),T(5.0)),slip::EmptyFeature());
   ** slip::RegularPointGrid2d<FPoint> rpg3(plist.begin(),plist.end(),
					  std::size_t(1));
   ** slip::RegularPointGrid2d<FPoint>::iterator it_find_rpg3 = rpg3.find(p1);
   ** if(it_find_rpg3 != rpg3.end())
   ** {
   **   std::cout<<p1.get_point()<<" is in the rpg3 feature point list"<<std::endl;
   ** }
   ** else
   ** {
   **   std::cout<<p1.get_point()<<" is not in the rpg3 feature point list"<<std::endl;
   ** }
   ** FPoint pfind(slip::Point2d<T>(T(0.0),T(1.0)),slip::EmptyFeature());
   **
   ** slip::RegularPointGrid2d<FPoint>::iterator it2_find_rpg3 = rpg3.find(pfind);
   ** if(it2_find_rpg3 != rpg3.end())
   ** {
   **   std::cout<<pfind.get_point()<<" is in the rpg3 feature point list"<<std::endl;
   ** }
   ** else
   ** {
   **   std::cout<<pfind.get_point()<<" is not in the rpg3 feature point list"<<std::endl;
   ** }
   ** \endcode
   */
  iterator find(const FPoint& fp)
  {
    iterator result = this->end();
    //computes cell of the feature point
    std::size_t i = 0;
    std::size_t j = 0;
    this->compute_indices(fp.get_point(),i,j);
    if(this->valid_indices(i,j))
      {
	//if grid cell as elements
	if((*this->grid_)[i][j]->size() != 0)
	  {
	    iterator local_result = std::find_if(iterator((*this->grid_)[i][j]->begin()),iterator((*this->grid_)[i][j]->end()),comp_FPoint_points(fp.get_point()));
	    if(local_result != iterator((*this->grid_)[i][j]->end()))
	      {
		result = iterator(this->point_list_->find(*(local_result.base())));
	      }
	  }

      }
    return result;
  }

  /**
   ** \brief Random selection of a feature point amoung the this->size() feature points in the container.
   ** \return iterator to the selecting point.
   ** \todo optimize selection (probably need to change unordered_set to unordered_map or to use another datastructure like vector to select.
   */
  iterator random_selection()
  {
    iterator result = this->end();
    if(this->point_list_->bucket_count() != 0)
      {
	std::size_t rand_bucket = std::size_t(0);
	do
	  {
	    rand_bucket = static_cast<std::size_t>(std::rand()%this->point_list_->bucket_count());
	  }
	while(this->point_list_->bucket_size(rand_bucket) == 0);
	std::size_t rand_in_bucket = static_cast<std::size_t>(std::rand()%this->point_list_->bucket_size(rand_bucket));
	typename std::unordered_set<FPoint*>::local_iterator it = this->point_list_->begin(rand_bucket);
	for(std::size_t i = 0; i < rand_in_bucket; ++i, ++it)
	  {}
	result = iterator(this->point_list_->find(*it));
      }
	return result;
  }

/*@} End Lookup */


/**
   ** \name Capacity
   */
  /*@{*/
  /**
   ** \brief Returns the number of elements in the container.
   ** \return The number of elements in the container. 
   */
  std::size_t size() const
  {
    return this->point_list_->size();
  }

   /**
   ** \brief Returns the maximum number of elements the container is able to hold due to system or library implementation limitations
   ** \return Maximum number of elements. 
   ** \remarks This value typically reflects the theoretical limit on the size of the container. At runtime, the size of the container may be limited to a value smaller than max_size() by the amount of RAM available. 
   */
  std::size_t max_size() const
  {
    return this->point_list_->max_size();
  }
   /**
   ** \brief Checks if the container has no elements.
   ** \return true if the container is empty, false otherwise 
   */
  bool is_empty() const
  {
    return this->point_list_->is_empty();
  }

  /**
   ** \brief Checks if the container has no elements.
   ** \return true if the container is empty, false otherwise 
   */
  bool empty() const
  {
    return this->is_empty();
  }
  
/*@} End Capacity */
  /**
   ** \name Input/Output
   */
  /*@{*/
  /*!
  ** \brief Print the RegularPointGrid2d.
  */
  friend std::ostream& operator<< <>(std::ostream&,
				     const self&);

  /**
   ** \brief write the list of feature points in a svg image file.
   ** \param file_path_name file path name of the svg image file.
   */
  void write_svg(const std::string file_path_name)
  {

    slip::SVGImageDescriptorType<point_value_type> SVGIDT;
   
    const std::size_t rows = this->grid_->rows();
    const std::size_t cols = this->grid_->cols();
    
    
    
    const std::size_t height = 600;
    const std::size_t width  = std::ceil((this->dx_/this->dy_)*point_value_type(height));

    SVGIDT.SetDimensions(width,height);
    //write grid
    slip::Array<point_value_type> x(cols+1);
    slip::Array<point_value_type> y(rows+1);
    point_value_type xmin = this->box_.minimal().x1();
    point_value_type ymin = this->box_.minimal().x2();
    point_value_type xmax = this->box_.maximal().x1();
    point_value_type ymax = this->box_.maximal().x2();
    slip::iota(x.begin(),x.end()-1,xmin,this->dx_);
    x[cols] = xmax;
    slip::iota(y.begin(),y.end()-1,ymin,this->dy_);
    y[rows] = ymax;
    //std::cout<<"x = \n"<<x<<std::endl;
    //std::cout<<"y = \n"<<y<<std::endl;

    slip::range_fun_interab<point_value_type,point_value_type> funx(xmin,xmax,point_value_type(0.0),point_value_type(width-1));
    slip::range_fun_interab<point_value_type,point_value_type> funy(ymin,ymax,point_value_type(0.0),point_value_type(height-1));
    
    std::transform(x.begin(),x.end(),x.begin(),funx);
    std::transform(y.begin(),y.end(),y.begin(),funy);
   
    
    for(std::size_t i = 0; i <= rows; ++i)
      {
       
    	SVGIDT.AddLine(slip::Point2d<point_value_type>(point_value_type(0.0),y[i]),slip::Point2d<point_value_type>(point_value_type(width-1),y[i]),1.0,slip::Color<unsigned char>(255,0,0));
      }
    for(std::size_t j = 0; j <= cols; ++j)
      {
       
    	SVGIDT.AddLine(slip::Point2d<point_value_type>(x[j],point_value_type(0.0)),slip::Point2d<point_value_type>(x[j],point_value_type(height-1)),1.0,slip::Color<unsigned char>(255,0,0));
      }


    //write points
     const_iterator it_b = this->begin();
     for(;it_b != this->end(); ++it_b)
       {
     	 point_value_type x = funx(it_b->get_point().x1());
     	 point_value_type y = point_value_type(height-1)-funy(it_b->get_point().x2());
	 SVGIDT.AddCircle(slip::Point2d<point_value_type>(x,y),1.0,slip::Color<unsigned char>(0,0,255));
	 
       }
    SVGIDT.Write(file_path_name);


  }
 /**
   ** \brief write the list of feature points in a VTK file.
   ** \param file_path_name file path name of the vtk file.
   */
  void write_vtk(const std::string file_path_name)
  {
    std::ofstream output(file_path_name.c_str(),std::ios::out);
    try
      {
	if(!output)
	  {
	    throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name)
	      ;
	  }
	else
	  {
	    const std::size_t rows = this->grid_->rows();
	    const std::size_t cols = this->grid_->cols();
	    const std::size_t rows_1 = rows + 1;
	    const std::size_t cols_1 = cols + 1;
	   
	    const std::size_t grid_point_number = rows_1 * cols_1;
	    const std::size_t point_number = this->size() + grid_point_number;
	    std::cout<<"grid_point_number = "<<grid_point_number<<std::endl;
	    
	    output<<"# vtk DataFile Version 3.0"<<std::endl;
	    output<<"# "<<file_path_name<<std::endl;
	    output<<"ASCII"<<std::endl;
	    output<<"DATASET UNSTRUCTURED_GRID"<<std::endl;
	    output<<"POINTS "<<point_number<<" FLOAT"<<std::endl;
	    //fetaures points
	    const_iterator beg = this->begin();
	    for(; beg != this->end(); ++beg)
	      {
		output<<beg->get_point().x1()<<" "<<beg->get_point().x2()<<" 0.0"<<std::endl;
	      }
	    //grid points
	    slip::Array<point_value_type> x(cols_1);
	    slip::Array<point_value_type> y(rows_1);
	  
	    point_value_type xmin = this->box_.minimal().x1();
	    point_value_type ymin = this->box_.minimal().x2();
	    point_value_type xmax = this->box_.maximal().x1();
	    point_value_type ymax = this->box_.maximal().x2();
	  
	    slip::iota(x.begin(),x.end()-1,xmin,this->dx_);
	    x[cols] = xmax;
	    std::cout<<"x = \n"<<x<<std::endl;
	    slip::iota(y.begin(),y.end()-1,ymin,this->dy_);
	    y[rows] = ymax;
	    std::cout<<"y = \n"<<y<<std::endl;
	    
	   
	    for(std::size_t i = 0; i < rows_1; ++i)
	      {
		for(std::size_t j = 0; j < cols_1; ++j)
		  {
		    output<<x[j]<<" "<<y[i]<<" 0.0"<<std::endl;
		  }
	      }
	  

	    const std::size_t cell_number = rows_1+cols_1;
	    std::cout<<"cell_number = "<<cell_number<<std::endl;
	   
	    const std::size_t offset = this->size();
	    std::size_t offset_y = (cols_1*rows_1) - cols_1;
	    output<<"CELLS "<<cell_number<<" "<<(cell_number*3)<<std::endl;
	    //x-y lines
	    for(std::size_t j = 0; j < cols_1; ++j)
	      {
		output<<"2 "<<(offset + j)<<" "<<(offset + j + offset_y)<<std::endl;
	      }
	    for(std::size_t i = 0; i < rows_1; ++i)
	      {
		output<<"2 "<<(offset+i*cols_1)<<" "<<(offset+i*cols_1+cols)<<std::endl;
	      }
	  
	   
	    output<<"CELL_TYPES "<<cell_number<<std::endl;
	    for(std::size_t k = 0; k < cell_number; ++k)
	      {
		output<<"3"<<std::endl;
	      }
	    output<<"POINT_DATA "<<point_number<<std::endl;
	    output<<"SCALARS Diameter float"<<std::endl;
	    output<<"LOOKUP_TABLE default"<<std::endl;
	    //diameters of points
	    if(this->margin_ != static_cast<point_value_type>(0))
	      {
		for(std::size_t k = 0 ; k < this->size(); ++k)
		  {
		    output<<this->margin_<<std::endl;
		  }
	      }
	    else
	      {
		for(std::size_t k = 0 ; k < this->size(); ++k)
		  {
		    output<<"0.1"<<std::endl;
		  }
	      }
	    //diameter of grid points
	    for(std::size_t k = offset; k < point_number; ++k)
	      { 
		output<<"0.0"<<std::endl;
	      }
	    
	  }
      }
    catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	output.close();
	exit(1);
      }

    
  }

  /*@} End Input/Output */

  void test_compute_indices(const point_value_type& delta_x,
			    const point_value_type& delta_y)
  {
    std::cout<<"test_compute_indices.................."<<std::endl;
    std::size_t i = 0; 
    std::size_t j = 0;
    point_type p1(this->box_.minimal());
    this->compute_indices(p1,i,j);
    std::cout<<"p1 "<<p1<<" (i,j) = ("<<i<<","<<j<<")"<<std::endl;
    point_type p2(this->box_.maximal());
    this->compute_indices(p2,i,j);
    std::cout<<"p2 "<<p2<<" (i,j) = ("<<i<<","<<j<<")"<<std::endl;
    point_type p3(this->box_.middle());
    this->compute_indices(p3,i,j);
    std::cout<<"p3 "<<p3<<" (i,j) = ("<<i<<","<<j<<")"<<std::endl;

 
    const std::size_t rows = this->grid_->rows();
    const std::size_t cols = this->grid_->cols();

    for(std::size_t y = 0; y < rows; ++y)
      {
	for(std::size_t x = 0; x < cols; ++x)
	  {
	    point_type p(this->box_.minimal().x1()+(this->dx_*x)+delta_x,
			 this->box_.minimal().x2()+(this->dy_*y)+delta_y);
	    this->compute_indices(p,i,j);
	    std::cout<<" (i,j) = ("<<(rows-1-y)<<","<<x<<") "<<"p "<<p<<" (i,j)c = ("<<i<<","<<j<<")"<<std::endl;
	  }
      }
    std::cout<<"....................................."<<std::endl;
  }


  

protected :
  
  struct comp_x1: public std::binary_function<FPoint,FPoint,bool>
  {
    inline
    bool operator()(const FPoint& p1, const FPoint& p2)
    {
      return p1.get_point().x1() <  p2.get_point().x1();
    }
  };

  struct comp_x2: public std::binary_function<FPoint,FPoint,bool>
  {
    inline
    bool operator()(const FPoint& p1, const FPoint& p2)
    {
      return p1.get_point().x2() <  p2.get_point().x2();
    }
   };

  struct comp_FPoint_points: public std::unary_function<FPoint,bool>
  {
    comp_FPoint_points(const FPoint& p):
      p_(p)
    {}
    inline
    bool operator()(const FPoint& p1)
    {
      return p1.get_point() ==  p_.get_point();
    }
    FPoint p_;
  };

  template <typename InputIterator>
  slip::QuadTreeBox<point_type> compute_bounding_box(InputIterator first,
						     InputIterator last,
						     const point_value_type& margin)
  {
    
    std::pair<InputIterator,InputIterator> xminmax =
      std::minmax_element(first,last,RegularPointGrid2d<FPoint>::comp_x1());
    std::pair<InputIterator,InputIterator> yminmax =
      std::minmax_element(first,last,RegularPointGrid2d<FPoint>::comp_x2());

    return slip::QuadTreeBox<point_type>(xminmax.first->get_point().x1()-margin,
					 yminmax.first->get_point().x2()-margin,
					 xminmax.second->get_point().x1()+margin,
					 yminmax.second->get_point().x2()+margin);
  }

  template <typename InputIterator>
  void update_bounding_box(InputIterator first,
			   InputIterator last)
  {
    const point_value_type margin = RegularPointGrid2d<FPoint>::margin_ + static_cast<point_value_type>(10.0)*std::numeric_limits< point_value_type>::epsilon();

    if(this->box_.xmin() == this->box_.xmax() == this->box_.ymin() == this->box_.ymax() == size_t(0))
      {
	this->box_ = this->compute_bounding_box(first,last,margin);
      } 
    else
      {
     std::pair<InputIterator,InputIterator> xminmax =
      std::minmax_element(first,last,RegularPointGrid2d<FPoint>::comp_x1());
     std::pair<InputIterator,InputIterator> yminmax =
      std::minmax_element(first,last,RegularPointGrid2d<FPoint>::comp_x2());
     if(xminmax.first->get_point().x1() < (this->box_.xmin() + margin))
       {
	 this->box_.xmin() = xminmax.first->get_point().x1() - margin;
       }
     if(yminmax.first->get_point().x2() < (this->box_.ymin() + margin))
       {
	 this->box_.ymin() = yminmax.first->get_point().x2() - margin;
       }
     if(xminmax.second->get_point().x1() > (this->box_.xmax() - margin))
       {
	 this->box_.xmax() = xminmax.second->get_point().x1() + margin;
       }
     if(yminmax.second->get_point().x2() > (this->box_.ymax() - margin))
       {
	 this->box_.ymax() = yminmax.second->get_point().x2() + margin;
       }
      }
  }

  
  void update_bounding_box(const FPoint& fp)
  {
    const point_value_type x1 = fp.get_point().x1();
     const point_value_type x2 = fp.get_point().x2();
     const point_value_type margin = RegularPointGrid2d<FPoint>::margin_ + static_cast<point_value_type>(10.0)*std::numeric_limits< point_value_type>::epsilon();

     if((this->box_.xmin() == size_t(0)) && 
	(this->box_.xmax() == size_t(0)) &&
	(this->box_.ymin() == size_t(0)) &&
	(this->box_.ymax() == size_t(0)))
      {
	this->box_ = slip::QuadTreeBox<point_type>(x1-margin,
						   x2-margin,
						   x1+margin,
						   x2+margin);
      } 
    else
      {
   
    if(x1 < (this->box_.xmin() - margin))
       {
	 this->box_.xmin() = x1 - margin;
       }
    if(x2 < (this->box_.ymin() - margin))
       {
	 this->box_.ymin() = x2 - margin;
       }
    if(x1 > (this->box_.xmax() + margin))
       {
	 this->box_.xmax() = x1 + margin;
       }
    if(x2 > (this->box_.ymax() + margin))
       {
	 this->box_.ymax() = x2 + margin;
       }
  }
  }


  void compute_indices(const point_value_type& x,
		       const point_value_type& y,
			std::size_t& i,
			std::size_t& j) const
  {
   
    i = (this->height_-1) - static_cast<std::size_t>(std::floor((y-this->box_.ymin()) * this->inv_dy_));
    j = static_cast<std::size_t>(std::floor((x-this->box_.xmin()) * this->inv_dx_));
  }

  void compute_indices(const point_type& p, 
		       std::size_t& i,
		       std::size_t& j) const
  {
      this->compute_indices(p.x1(),p.x2(),i,j);
  }

   
  bool boxes_intersection(const slip::QuadTreeBox<point_type>& box,
			  point_value_type& pmin_x,
			  point_value_type& pmin_y,
			  point_value_type& pmax_x,
			  point_value_type& pmax_y)
  {
    bool as_intersection = false;
    point_value_type max_left = std::max(box.minimal().x1(),this->box_.minimal().x1());
    point_value_type min_right = std::min(box.maximal().x1(),this->box_.maximal().x1());
    point_value_type max_bottom = point_value_type();
    point_value_type min_top = point_value_type();
    if(max_left < min_right)
      {
	max_bottom = std::max(box.minimal().x2(),this->box_.minimal().x2());
	min_top = std::min(box.maximal().x2(),this->box_.maximal().x2());
	as_intersection = (max_bottom < min_top);
      }

    pmin_x = max_left;
    pmin_y = max_bottom;
    pmax_x = min_right;
    pmax_y = min_top;
    return as_intersection;
  }



  bool boxes_intersection_outside(const point_type& center,
				  const point_value_type& radius,
				  point_value_type& pmin_x,
				  point_value_type& pmin_y,
				  point_value_type& pmax_x,
				  point_value_type& pmax_y)
  {
    bool as_intersection = false;
    point_value_type max_left = std::max(center.x1()-radius,this->box_.minimal().x1());
    point_value_type min_right = std::min(center.x1()+radius,this->box_.maximal().x1());
    point_value_type max_bottom = point_value_type();
    point_value_type min_top = point_value_type();
    if(max_left < min_right)
      {
	max_bottom = std::max(center.x2()-radius,this->box_.minimal().x2());
	min_top = std::min(center.x2()+radius,this->box_.maximal().x2());
	as_intersection = (max_bottom < min_top);
      }

    pmin_x = max_left;
    pmin_y = max_bottom;
    pmax_x = min_right;
    pmax_y = min_top;
    return as_intersection;
  }

  
  void boxes_intersection_indices(const point_value_type& pmin_x,
				  const point_value_type& pmin_y,
				  const point_value_type& pmax_x,
				  const point_value_type& pmax_y,
				  std::size_t& imin,
				  std::size_t& jmin,
				  std::size_t& imax,
				  std::size_t& jmax)
  {
	this->compute_indices(pmin_x,pmin_y,imax,jmin);
	this->compute_indices(pmax_x,pmax_y,imin,jmax);
	if(imin == std::numeric_limits<std::size_t>::max())
	  {
	    imin = 0;
	  }
	if(jmin == std::numeric_limits<std::size_t>::max())
	  {
	    jmin = 0;
	  }
	if(imax == this->height_)
	  {
	    imax = this->height_ - 1;
	  }
	if(jmax == this->width_)
	  {
	    jmax = this->width_ - 1;
	  }
  }

    
  bool valid_indices(const std::size_t i, 
		     const std::size_t j)
  {
    return ((i < this->grid_->rows() ) && ( j < this->grid_->cols()) );
  }

 


void compute_grid_parameters(const std::size_t nb_point,
			     const std::size_t points_per_grid_element)
  {
    //sort box dimensions
    slip::Array<point_value_type> dim(2);
    dim[0] = this->box_.height();
    dim[1] = this->box_.width();
    typename slip::Array<point_value_type>::const_iterator it_max = std::max_element(dim.begin(),dim.end());
    const std::size_t index_max = static_cast<std::size_t>(it_max - dim.begin());
    
    const std::size_t n = std::ceil(std::sqrt(point_value_type(nb_point)/point_value_type(points_per_grid_element)));
    if(index_max == 0)
      {
	this->height_ = n;
	this->width_  = std::ceil(double(this->box_.width()*this->height_)/double(this->box_.height()));
      } 
    else
      {
	this->width_  = n;
	this->height_ = std::ceil(double(this->box_.height()*this->width_)/double(this->box_.width()));
      }

    this->dx_ = point_value_type(this->box_.width())  / point_value_type(this->width_);
    this->dy_ = point_value_type(this->box_.height()) / point_value_type(this->height_);
    this->update_invdxdy();
    
  }

  void update_invdxdy()
  {
    this->inv_dx_ = static_cast<point_value_type>(1)/this->dx_;
    this->inv_dy_ = static_cast<point_value_type>(1)/this->dy_;
    
  }


 

   bool is_inside_circle(const point_type& point, 
			const point_type& center,
			const point_value_type& radius)
  {
    const point_value_type dx = (point.x1() - center.x1());
    const point_value_type dy = (point.x2() - center.x2());
    return (dx*dx+dy*dy <= radius*radius);
  }

 bool is_inside_circle(const point_type& point, 
			const point_type& center,
			const point_value_type& radius,
			const point_value_type& margin)
  {
    return this->is_inside_circle(point,center,radius-margin);
  }
  
  /**
   ** \brief Reserves (n+(n*percentage)) memory space for grid cell and point_list to avoid frequent memory reallocation
   ** \param n size of the point_list data
   ** \param percentage percentage of additionnal space
   ** \pre 0<=percentage <=1
   */
  void reserve_memory(const std::size_t n,
		      const double& percentage =
		      0.2)
  {
    this->query_point_list_->reserve(9*this->max_point_number_);
    for(auto it = this->grid_->begin(); it !=this->grid_->end(); ++it)
      {
	(*it)->reserve(this->max_point_number_);
      }
   
    this->point_list_->reserve(n+static_cast<std::size_t>(double(n)*percentage));
  }

  /**
   ** \brief Desallocates query_point_list and grid
   */
  void partial_desallocate()
  {
     if(this->query_point_list_ != nullptr)
      {
	delete this->query_point_list_;
      }
      if(this->grid_ != nullptr)
       {
	 for(auto it = this->grid_->begin(); it !=this->grid_->end(); ++it)
	   {
	     if(*it != nullptr)
	       {
		 delete *it;
	       }
	   }
	 delete this->grid_;
       }
  }


  /**
   ** \brief Desallocates memory
   */
  void desallocate()
  {
    this->partial_desallocate();
    if(this->point_list_ != nullptr)
      {
	typename std::unordered_set<FPoint*>::iterator first = this->point_list_->begin();
	
	for(;first != this->point_list_->end(); ++first)
	  {
	    delete *first;
	  }
	
	delete this->point_list_;
      }
   
  }

  /**
   ** \brief Allocates query_point_list and grid
   */
  void partial_allocate()
  {
    this->grid_ = new slip::Array2d<std::unordered_set<FPoint*>* >(this->height_,this->width_);
    //allocate grid_ elements
    for(auto it = this->grid_->begin(); it !=this->grid_->end(); ++it)
      {
  	*it = new std::unordered_set<FPoint*>();
      }
   this->query_point_list_ = new std::unordered_set<FPoint*>();
  }

  /**
   ** \brief Allocates memory
   */
  void allocate()
     {
       this->partial_allocate();
       this->point_list_ = new std::unordered_set<FPoint*>();
     }

 

  /**
   ** \brief Copy attributes of \a rhs
   ** \param rhs other %RegularPointGrid2d
   */
  void copy_attributes(const self& rhs)
  {
    this->box_ = rhs.box_;
    this->max_point_number_ = rhs.max_point_number_;
    this->margin_ = rhs.margin_;
    this->dx_ = rhs.dx_;
    this->dy_ = rhs.dy_;
    this->inv_dx_ = rhs.inv_dx_; 
    this->inv_dy_ = rhs.inv_dy_;
    this->width_ = rhs.width_;
    this->height_ = rhs.height_;
  }

  /**
   ** \brief Copy data of \a rhs
   ** \param rhs other %RegularPointGrid2d
   */
  void copy_data(const self& rhs)
  {
    this->query_point_list_->reserve(9*this->max_point_number_);
    for(auto it = this->grid_->begin(); it !=this->grid_->end(); ++it)
      {
	(*it)->reserve(this->max_point_number_);
      }
  
    //copy the point_list
    if(rhs.size() != 0)
      {
	this->point_list_->reserve(rhs.size());
      }
    else
      {
	this->point_list_->reserve(rhs.grid_->rows()*rhs.grid_->cols()*this->max_point_number_);
      }
    const_iterator first = rhs.cbegin();
    for(; first != rhs.end(); ++first)
      {
	this->insert(*first);
      }
  }
};

  template <typename FPoint>
std::ostream& operator<<(std::ostream& flux, 
			 const RegularPointGrid2d<FPoint>& rpg)
{
  flux <<"RegularPointGrid2d: \n"<<"Maximal point number: "<<rpg.max_point_number()<<"\n Box: "<<rpg.box()<<std::endl;
  flux<<" margin: "<<rpg.margin_<<std::endl;
  flux<<" grid element size in x: "<<rpg.dx_<<std::endl;
  flux<<" grid element size in y: "<<rpg.dy_<<std::endl;
  flux<<" grid size: "<<rpg.grid_->rows()<<" x "<<rpg.grid_->cols()<<std::endl;
  flux<<" number of empty cells: "<<rpg.empty_cells()<<std::endl;
  flux<<" number of non empty cells: "<<rpg.non_empty_cells()<<std::endl;
  flux<<" load factor: "<<rpg.load_factor()<<std::endl;
  flux<<" number of elements: "<<rpg.size()<<std::endl;
  flux<<" elements in grid: "<<std::endl;
  const std::size_t rows = rpg.grid_->rows();
  const std::size_t cols = rpg.grid_->cols();
  for(std::size_t i = 0; i < rows; ++i)
    {
      flux<<"---------------"<<std::endl;
       for(std::size_t j = 0; j < cols; ++j)
	 {
	   
	   flux<<"("<<i<<","<<j<<"):\n";
	   if((*rpg.grid_)[i][j]->size() != 0)
	     {
	       typename std::unordered_set<FPoint*>::const_iterator itb = (*rpg.grid_)[i][j]->begin();
	       typename std::unordered_set<FPoint*>::const_iterator ite = (*rpg.grid_)[i][j]->end();
	       for(; itb !=ite; ++itb)
		 {
		   flux<<**itb<<std::endl;
		 }
	       flux<<std::endl;
	     }
	   else
	     {
	       flux<<"empty"<<std::endl;
	     }
	   
	 }
       flux<<"---------------"<<std::endl; 
    }
       
  return flux;
}


}//slip::
#endif //SLIP_REGULARPOINTGRID2D_HPP
