/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file DPoint.hpp
 * 
 * \brief Provides an abstract class to modelize the difference of slip::Point.
 * 
 */
#ifndef SLIP_DPOINT_HPP
#define SLIP_DPOINT_HPP

#include <iostream>
#include <cassert>
#include <algorithm>
#include "Block.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template <typename CoordType, std::size_t DIM>
class DPoint;

template <typename CoordType, std::size_t DIM>
std::ostream& operator<<(std::ostream & out, const DPoint<CoordType,DIM>& p);


/*! \class DPoint
**  \ingroup Containers MiscellaneousContainers
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/04/02
** \since 1.0.0
** \brief Define an abstract %DPoint structure
** \param CoordType Type of the coordinates of the DPoint 
** \param DIM dimension of the DPoint
*/
template <typename CoordType, std::size_t DIM>
class DPoint
{
public :
  typedef CoordType value_type;
  typedef value_type* dpointer;
  typedef const value_type* const_dpointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef DPoint<CoordType,DIM> self;

  //default iterator_category of the container
  typedef std::random_access_iterator_tag iterator_category;

  
  /**
  ** \name Constructors & Destructors
  */
  /*@{*/

  /*!
  ** \brief Constructs a copy of the DPoint \a rhs
  ** \param other 
  */
  DPoint(const self& other);


  /*!
  ** \brief Constructs a DPoint from a CoordType array
  ** \param array
  */
  DPoint(const CoordType* array);
  /*@} End Constructors */

  /**
   ** \name i/o operators
   */
  /*@{*/
  /*!
  ** \brief Write the DPoint to the ouput stream
  ** \param out output stream
  ** \param p DPoint to write to the output stream
  */  
  friend std::ostream& operator<< <>(std::ostream & out,  const self& p);
  /*@} End i/o operators */

   /**
   ** \name  Assignment operators and methods
   */
  /*@{*/
  /*!
  ** \brief Assign a DPoint.
  **
  ** Assign elements of DPoint in \a other
  **
  ** \param other DPoint to get the values from.
  ** \return 
  */
  self& operator=(const self & other);
  /*@} End Assignment operators and methods*/

  /**
   ** \name  Element access operators
   */
  /*@{*/
  
  /*!
  ** \brief Returns the i'th coordinate of the DPoint.
  ** \param i index of the coordinate to return
  ** \return value of  the \a i'th row of the DPoint.
  ** \pre 0 <= i < dim
  */
  reference operator[](const std::size_t i);

  /*!
  ** \brief Returns the \a i'th row of the DPoint.
  ** \param i index of the coordinate to return
  ** \return value of  the \a i'th row of the DPoint
  ** \pre 0 <= i < dim
  */
  const_reference operator[](const std::size_t i) const;

  /*@} End Element access operators */


  /**
   ** \name Comparison operators
   */
  /*@{*/
  /*!
  ** \brief compare  two DPoint.
  **
  ** \param other DPoint to compare
  ** \return true if the two dpoint have the same coordinates
  ** 
  */
  bool operator==(const self& other) const;

  /*!
  ** \brief compare  two DPoint.
  **
  ** \param other DPoint to compare
  ** \return true if the two dpoint don't have the same coordinates
  ** 
  */
  bool operator!=(const self& other) const;

  /*@} Comparison operators */

  /**
   ** \name  Arithmetic operators
   */
  /*@{*/
  /*!
  ** \brief negate all the coordinates of a DPoint
  **
  ** \return a DPoint with negate coordinates 
  ** 
  */
  self operator-() const;


  /*!
  ** \brief add the coordinates of a DPoint to the DPoint
  ** \param other DPoint to add with
  ** \return a DPoint  
  ** 
  */
  self& operator+=(const self& other);

  /*!
  ** \brief add the coordinates of a two DPoint
  ** \param other DPoint to add with
  ** \return a DPoint  
  ** 
  */
  self operator+(const self& other) const;


   /*!
  ** \brief substract the coordinates of a DPoint to the DPoint
  ** \param other DPoint to add with
  ** \return a DPoint  
  ** 
  */
  self& operator-=(const self& other);

  /*!
  ** \brief substract the coordinates of a two DPoint
  ** \param other DPoint to add with
  ** \return a DPoint  
  ** 
  */
  self operator-(const self& other) const;
  /*@} End Arithmetic operators */


  /*!
  ** \brief Returns the dimension of the DPoint
  ** \return the dimension (number of coordinates) of the DPoint
  */
  std::size_t dim() const;
  
  /*!
  ** \brief Returns the dimension of the DPoint
  ** \return the dimension (number of coordinates) of the DPoint
  */
  std::size_t size() const;

  /*!
  ** \brief Returns the maximal size of the DPoint
  ** \return the dimension (number of coordinates) of the DPoint
  */
  std::size_t max_size() const;

  /*!
  ** \brief Returns true if dimension of the DPoint is 0
  ** \return true if the dimension of the DPoint is 0, false else
  */
  bool empty()const;

  /*!
  ** \brief Return the coordinates of the DPoint
  */
  const block<value_type,DIM>& coord() const;
  
  /*!
  ** \brief Swaps two DPoint
  ** \param other DPoint to swap with
  */
  void swap(self& other);
 
   
  
  
protected:
  /**
  ** \name Constructors & Destructors
  */
  /*@{*/

  /*!
  ** \brief Default constructor (protected to avoid default construction)
  */
  DPoint();

  /*@} End Constructors */

 
  /*! 
  ** \brief The coordinates of the dpoint are stored  in this block
  */
  block<value_type,DIM> coord_;

 protected:
friend class boost::serialization::access;
  template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & coord_;
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & coord_;
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()


 };
}//slip::

namespace slip
{

  template<typename CoordType,std::size_t DIM>
  inline
  DPoint<CoordType,DIM>::DPoint()
  {}

  template<typename CoordType,std::size_t DIM>
  inline
  DPoint<CoordType,DIM>::DPoint(const DPoint<CoordType,DIM>& other):
    coord_(other.coord_)
  {}

  template<typename CoordType,std::size_t DIM>
  inline
  DPoint<CoordType,DIM>::DPoint(const CoordType* array)
  {
    std::copy(array,array+DIM,coord_.begin());
  }
  
  template<typename CoordType,std::size_t DIM>
  inline
  DPoint<CoordType,DIM>& DPoint<CoordType,DIM>::operator=(const DPoint<CoordType,DIM> & other)
  {
    if(this != &other)
      {
	this->coord_ = other.coord_;
      }
    return *this;
  }
  
 
  template<typename CoordType,std::size_t DIM>
  inline
  CoordType& DPoint<CoordType,DIM>::operator[](const std::size_t i)
  {
    assert(i < DIM);
    return this->coord_[i];
  }
  
  template<typename CoordType,std::size_t DIM>
  inline
  const CoordType& DPoint<CoordType,DIM>::operator[](const std::size_t i) const 
  {
    assert(i < DIM);
    return this->coord_[i];
  }
  
  template<typename CoordType,std::size_t DIM>
  inline
  std::size_t DPoint<CoordType,DIM>::dim() const {return DIM;} 
  
  template<typename CoordType,std::size_t DIM>
  inline
  std::size_t DPoint<CoordType,DIM>::size() const {return DIM;}

  template<typename CoordType,std::size_t DIM>
  inline
  std::size_t DPoint<CoordType,DIM>::max_size() const {return DIM;}

  template<typename CoordType,std::size_t DIM>
  inline
  bool DPoint<CoordType,DIM>::empty()const {return coord_.empty();}

  template<typename CoordType,std::size_t DIM>
  inline
  const block<CoordType,DIM>& DPoint<CoordType,DIM>::coord() const
  {
    return coord_;
  }

  template<typename CoordType,std::size_t DIM>
  inline
  void DPoint<CoordType,DIM>::swap(DPoint<CoordType,DIM>& other)
  {
    (this->coord_).swap(other.coord_);
  }

  template<typename CoordType,std::size_t DIM>
  inline
  std::ostream& operator<<(std::ostream & out, 
				  const DPoint<CoordType,DIM>& p)
  {
    out<<p.coord_;
    return out;
  }  

  template<typename CoordType,std::size_t DIM>
  inline
  bool DPoint<CoordType,DIM>::operator==(const DPoint<CoordType,DIM>& other) const
  {
    return this->coord_ == other.coord_;
  }
  
  template<typename CoordType,std::size_t DIM>
  inline
  bool DPoint<CoordType,DIM>::operator!=(const DPoint<CoordType,DIM>& other) const
  {
    return this->coord_ != other.coord_;
  }

  template<typename CoordType,std::size_t DIM>
  inline
  DPoint<CoordType,DIM> DPoint<CoordType,DIM>::operator-() const
  {
    DPoint<CoordType,DIM> tmp;
    std::transform(coord_.begin(),coord_.end(),(tmp.coord_).begin(),std::negate<CoordType>());
    return tmp; 
  }


  template<typename CoordType,std::size_t DIM>
  inline
  DPoint<CoordType,DIM>& DPoint<CoordType,DIM>::operator+=(const DPoint<CoordType,DIM>& other)
  {
    for(std::size_t i = 0; i < DIM; ++i)
      {
	(this->coord_)[i] += (other.coord_)[i];
      }
    return *this; 
  }

  template<typename CoordType,std::size_t DIM>
  inline
  DPoint<CoordType,DIM> DPoint<CoordType,DIM>::operator+(const DPoint<CoordType,DIM>& other) const
  {
     DPoint<CoordType,DIM> tmp(*this);
     
     return tmp+=other; 
  }

 template<typename CoordType,std::size_t DIM>
  inline
  DPoint<CoordType,DIM>& DPoint<CoordType,DIM>::operator-=(const DPoint<CoordType,DIM>& other)
  {
    for(std::size_t i = 0; i < DIM; ++i)
      {
	(this->coord_)[i] -= (other.coord_)[i];
      }
    return *this; 
  }

  template<typename CoordType,std::size_t DIM>
  inline
  DPoint<CoordType,DIM> DPoint<CoordType,DIM>::operator-(const DPoint<CoordType,DIM>& other) const
  {
     DPoint<CoordType,DIM> tmp(*this);
     
     return tmp-=other; 
  }

  
}//slip::

#endif //SLIP_DPOINT_HPP
