/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file stride_iterator.hpp
 * 
 * \brief Provides a class to iterate a 1d range according to a step.
 * 
 */
#ifndef SLIP_STRIDE_ITERATOR_HPP
#define SLIP_STRIDE_ITERATOR_HPP

#include <iterator> //iterator tags & iterator_traits
#include <cassert>

namespace slip
{
template<class Iter_T>
class stride_iterator
{
public:
 //to specify the category of this iterator. Useful for optimazed algorithms
  typedef std::random_access_iterator_tag iterator_category;
   
 //nested typedefs to define an iterator
  typedef typename std::iterator_traits<Iter_T>::value_type value_type;
  typedef typename std::iterator_traits<Iter_T>::difference_type difference_type;
  typedef typename std::iterator_traits<Iter_T>::pointer pointer;
  typedef typename std::iterator_traits<Iter_T>::reference reference;
 
 //typedef to simplify the class code
  typedef stride_iterator self;

  //constructors
  stride_iterator():
    it_(),step_(0)
  {}
  stride_iterator(const self& o):
    it_(o.it_),step_(o.step_)
  {}
  stride_iterator(Iter_T it,
		  difference_type n):
    it_(it),step_(n)
  {}
  
  //operators
  self& operator++()
  {
    it_ += step_;
    return *this;
  }
  //postfixed ++ operator
  self operator++(int)
  {
    self tmp = *this;
    it_ += step_;
    return tmp;
  }
  
  self& operator+=(difference_type n)
  {
    it_ += n * step_;
    return *this;
  }
  self& operator--()
  {
    it_ -= step_;
    return *this;
  }

  //postfixed -- operator
  self operator--(int)
  {
    self tmp = *this;
    it_ -= step_;
    return tmp;
  }
  self& operator-=(difference_type n)
  {
    it_ -= n * step_;
    return *this;
  }

  reference operator[](difference_type n)
  {
    return *(it_ + (n * step_));
  }

  const reference operator[](difference_type n) const
  {
    return *(it_ + (n * step_));
  }

  reference operator*()
  {
    return *it_;
  }

  pointer operator->()
  { 
    return &(operator*()); 
  }

  //friend operators
  //Equality Comparable
  friend bool operator==(const self& x,
			 const self& y)
  {
    assert(x.step_ == y.step_);
    return x.it_ == y.it_;
  }

  friend bool operator!=(const self& x,
			 const self& y)
  {
    assert(x.step_ == y.step_);
    return x.it_ != y.it_;
  }

  //Less Than Comparable
  friend bool operator<(const self& x,
			const self& y)
  {
    assert(x.step_ == y.step_);
    return x.it_ < y.it_;
  }

  friend bool operator>(const self& x,
			const self& y)
  {
    return y < x;
  }
  
  friend bool operator<=(const self& x,
			const self& y)
  {
    return !(y < x);
  }

  friend bool operator>=(const self& x,
			const self& y)
  {
    return !(x < y);
  }

  friend difference_type operator-(const self& x,
				   const self& y)
  {
    assert(x.step_ == y.step_);
    return (x.it_ - y.it_) / x.step_;
  }

  friend self operator+(const self& x,
			difference_type n)
  {
    self tmp = x;
    tmp += n;
    return tmp;
  }
  friend self operator+(difference_type n,
			const self& y)
  {
    self tmp = y;
    tmp += n;
    return tmp;
  }

 friend self operator-(const self& x,
			difference_type n)
  {
    self tmp = x;
    tmp -= n;
    return tmp;
  }
  
//  friend self operator-(const self& x,
// 			difference_type n)
//   {
//     self tmp = x;
//     tmp -= n;
//     return tmp;
//   }
  


private:
  Iter_T it_;
  difference_type step_;

};
}//slip::
#endif //SLIP_STRIDE_ITERATOR_HPP
