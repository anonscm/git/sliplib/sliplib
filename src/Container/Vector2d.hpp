/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Vector2d.hpp
 * 
 * \brief Provides a class to manipulate 2d Vector.
 * 
 */

#ifndef SLIP_VECTOR2D_HPP
#define SLIP_VECTOR2D_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <cmath>
#include <string>
#include <cstddef>
#include "KVector.hpp"
#include "norms.hpp"
#include "macros.hpp"
#include "compare.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/base_object.hpp>

namespace slip
{

template <typename T>
struct Vector2d;

template <typename T>
bool operator<(const Vector2d<T>& x, const Vector2d<T>& y);

template <typename T>
bool operator>(const Vector2d<T>& x, const Vector2d<T>& y);

template <typename T>
bool operator<=(const Vector2d<T>& x, const Vector2d<T>& y);

template <typename T>
bool operator>=(const Vector2d<T>& x, const Vector2d<T>& y);

/*! \class Vector2d
** \ingroup Containers Containers1d MathematicalContainer
** \brief This is a %Vector2d struct. It is a specialization
**  of kvector. It implements some peculiar 2d operations.
** 
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/03/25
** \since 1.0.0
** \param T Type of the Vector2d 
*/
template <typename T>
struct Vector2d : public kvector<T,2>
{

  typedef Vector2d<T> self;
  typedef T value_type;
  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;


  /**
   ** \name Constructors
   */
  /*@{*/

  /*!
  ** \brief Default constructor
  ** init all the components to 0
  */
  Vector2d();

  /*!
  ** \brief Constructor
  ** init all the components to val
  ** \param val initial value
  */
  Vector2d(const T& val);

   /*!
  ** \brief Constructor
  ** init all the components to val
  ** \param x1 initial value of the first vector2d dimension
  ** \param x2 initial value of the second vector2d dimension
  */
  Vector2d(const T& x1,
	   const T& x2);

  /*!
  ** \brief Constructor
  ** init the Vector2d with the val array
  ** \param val initial value
  */
  Vector2d(const T* val);

  
  /*!
  ** \brief Copy constructor
  ** init the Vector2d with another %Vector2d
  ** \param rhs The other %Vector2d
  */
  Vector2d(const Vector2d<T>& rhs);

  

  /*@} End Constructors */

  
//   Vector2d<T>& operator=(const Vector2d<T>& rhs)
//   {
//     (*this)[0] = rhs[0];
//     (*this)[1] = rhs[1];
//     return *this;
//   }


  /**
   ** \name Arithmetic operators
   */
  /*@{*/
   /*!
   ** \brief Computes the oposite of a %Vector2d
  */
  self operator-() const;
  
  /*@} End Arithmeric operators */

  /**
   ** \name Mathematical operators
   */
  /*@{*/
   /*!
   ** \brief Computes the angle with the cartesian axis. 
   **        The result is between the range [-PI,PI]
   ** \return the angle (radian)
   */
  T angle() const;

  /*!
  ** \brief Computes the angle with the cartesian axis. 
  ** The result is between the range [-360,360]
  ** \return the angle (degree)
   */
  T angle_degree() const;
  
  /*@} End Mathematical operators */

 /**
 ** \name Comparison operators
 */
 /*@{*/


  /*!
  ** \brief Less Compare operator
  ** \param x first Vector2d to compare
  ** \param y second Vector2d to compare
  ** return true if ||x|| < ||y||, ||.|| is the Euclidean norm
  ** \pre x.dim() == y.dim()
  */
  friend bool operator< <>(const Vector2d<T>& x, const Vector2d<T>& y);

  /*!
  ** \brief Greater Compare operator
  ** \param x first Vector2d to compare
  ** \param y second Vector2d to compare
  ** return true if ||x|| > ||y||, ||.|| is the Euclidean norm
  ** \pre x.dim() == y.dim()
  */
  friend bool operator> <>(const Vector2d<T>& x, const Vector2d<T>& y);

   /*!
  ** \brief Less equal Compare operator
  ** \param x first Vector2d to compare
  ** \param y second Vector2d to compare
  ** return true if ||x|| <= ||y||, ||.|| is the Euclidean norm
  ** \pre x.dim() == y.dim()
  */
  friend bool operator<= <>(const Vector2d<T>& x, const Vector2d<T>& y);

  /*!
  ** \brief Greater equal Compare operator
  ** \param x first Vector2d to compare
  ** \param y second Vector2d to compare
  ** return true if ||x|| >= ||y||, ||.|| is the Euclidean norm
  ** \pre x.dim() == y.dim()
  */
  friend bool operator> <>(const Vector2d<T>& x, const Vector2d<T>& y);

 
  /*@} End comparison operators */

   /**
 ** \name Accessors
 */
 /*@{*/
  /*!
  ** \brief Accessor to the first dimension value of the Vector2d 
  ** \return Returns the first dimension value of the Vector2d 
  */
  const T& get_x1() const;
  /*!
  ** \brief Accessor to the first dimension value of the Vector2d 
  ** \return Returns the first dimension value of the Vector2d 
  */
  const T& get_x() const;
  /*!
  ** \brief Accessor to the first dimension value of the Vector2d 
  ** \return Returns the first dimension value of the Vector2d 
  */
  const T& u() const;

  /*!
  ** \brief  Accessor to the second dimension value of the Vector2d 
  ** \return Returns the second dimension value of the Vector2d 
  */
  const T& get_x2() const;
   /*!
  ** \brief  Accessor to the second dimension value of the Vector2d 
  ** \return Returns the second dimension value of the Vector2d 
  */
  const T& get_y() const;
   /*!
  ** \brief  Accessor to the second dimension value of the Vector2d 
  ** \return Returns the second dimension value of the Vector2d 
  */
  const T& v() const;
    

  

  /*!
  ** \brief  Mutator of the first dimension value of the Vector2d 
  ** \param x1 the first dimension value of the Vector2d 
  */
  void set_x1(const T& x1);
   /*!
  ** \brief  Mutator of the first dimension value of the Vector2d 
  ** \param r the first dimension value of the Vector2d 
  */
  void set_x(const T& r);
   /*!
  ** \brief  Mutator of the first dimension value of the Vector2d 
  ** \param u_ the first dimension value of the Vector2d 
  */
  void u(const T& u_);
  

  /*!
  ** \brief  Mutator of the second dimension value of the Vector2d 
  ** \param x2 the second dimension value of the Vector2d 
  */
  void set_x2(const T& x2);
  /*!
  ** \brief  Mutator of the second dimension value of the Vector2d 
  ** \param g the second dimension value of the Vector2d 
  */
  void set_y(const T& g);
  /*!
  ** \brief  Mutator of the second dimension value of the Vector2d 
  ** \param v_ the second dimension value of the Vector2d 
  */
  void v(const T& v_);
 

  /*!
  ** \brief  Mutator of the three dimension values of the Vector2d 
  ** \param x1 the first dimension value of the Vector2d 
  ** \param x2 the second dimension value of the Vector2d 
  */
  void set(const T& x1, 
	   const T& x2);


 /*@} End Accessors */

  
 /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;

  
 private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::kvector<T,2> >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::kvector<T,2> >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
};

   ///double alias
  typedef slip::Vector2d<double> Vector2d_d;
  ///float alias
  typedef slip::Vector2d<float> Vector2d_f;
  ///long alias
  typedef slip::Vector2d<long> Vector2d_l;
  ///unsigned long alias
  typedef slip::Vector2d<unsigned long> Vector2d_ul;
  ///short alias
  typedef slip::Vector2d<short> Vector2d_s;
  ///unsigned long alias
  typedef slip::Vector2d<unsigned short> Vector2d_us;
  ///int alias
  typedef slip::Vector2d<int> Vector2d_i;
  ///unsigned int alias
  typedef slip::Vector2d<unsigned int> Vector2d_ui;
  ///char alias
  typedef slip::Vector2d<char> Vector2d_c;
  ///unsigned char alias
  typedef slip::Vector2d<unsigned char> Vector2d_uc;
}//slip::

namespace slip
{

  template<typename T>
  inline
  Vector2d<T>::Vector2d()
  {
    this->fill(T(0));
  }

  template<typename T>
  inline
  Vector2d<T>::Vector2d(const T& val)
  {
    this->fill(val);
  }

  template<typename T>
  inline
  Vector2d<T>::Vector2d(const T& x1,
			const T& x2)
  {
    (*this)[0] = x1;
    (*this)[1] = x2;
  }

  template<typename T>
  inline
  Vector2d<T>::Vector2d(const T* val)
  {
    assert(val != 0);
    this->fill(val);
  }


  template<typename T>
  inline
  Vector2d<T>::Vector2d(const Vector2d<T>& rhs)
  {
    *this = rhs; 
  }

  template<typename T>
  inline
   Vector2d<T> Vector2d<T>::operator-() const
  {
    self tmp;
    tmp[0] = -(*this)[0];
    tmp[1] = -(*this)[1];
    return tmp;
  }

  template<typename T>
  inline
  T Vector2d<T>::angle() const
  {
    return std::atan2((*this)[1],(*this)[0]);
  }

  template<typename T>
  inline
  T Vector2d<T>::angle_degree() const
  {
    return (static_cast<T>(180.0)/slip::constants<T>::pi()) * this->angle();
  }
 
  /** \name LessThanComparable functions */
  /* @{ */
  template<typename T>
  inline
  bool operator<(const Vector2d<T>& x, 
		 const Vector2d<T>& y)
  {
     return (slip::Euclidean_norm<T>(x.begin(),x.end()) 
	     < 
	     slip::Euclidean_norm<T>(y.begin(),y.end()) 
	     );
  }

  

  template<typename T>
  inline
  bool operator>(const Vector2d<T>& x, 
		 const Vector2d<T>& y)
  {
    return y < x;
  }

  template<typename T>
  inline
  bool operator<=(const Vector2d<T>& x, 
		 const Vector2d<T>& y)
  {
    return !(y < x);
  }

  template<typename T>
  inline
  bool operator>=(const Vector2d<T>& x, 
		 const Vector2d<T>& y)
  {
    return !(x < y);
  }
  /* @} */

  template<typename T>
  inline
  const T& Vector2d<T>::get_x1() const {return (*this)[0];}

  template<typename T>
  inline
  const T& Vector2d<T>::get_x() const {return (*this)[0];}

  template<typename T>
  inline
  const T& Vector2d<T>::u() const {return (*this)[0];}


   template<typename T>
  inline
  const T& Vector2d<T>::get_x2() const {return (*this)[1];}

  template<typename T>
  inline
  const T& Vector2d<T>::get_y() const {return (*this)[1];}

  template<typename T>
  inline
  const T& Vector2d<T>::v() const {return (*this)[1];}


  template<typename T>
  inline
  void Vector2d<T>::set_x1(const T& x1){(*this)[0] = x1;}

  template<typename T>
  inline
  void Vector2d<T>::set_x(const T& x){(*this)[0] = x;}

  template<typename T>
  inline
  void Vector2d<T>::u(const T& x){(*this)[0] = x;}


  template<typename T>
  inline
  void Vector2d<T>::set_x2(const T& x2){(*this)[1] = x2;}

  template<typename T>
  inline
  void Vector2d<T>::set_y(const T& y){(*this)[1] = y;}

  template<typename T>
  inline
  void Vector2d<T>::v(const T& y){(*this)[1] = y;}

  template<typename T>
  inline
  void Vector2d<T>::set(const T& x1,
			const T& x2)
  {
    (*this)[0] = x1;
    (*this)[1] = x2;
  }
}//slip::


namespace slip
{
template<typename T>
inline
Vector2d<T> operator+(const Vector2d<T>& V1, 
		      const Vector2d<T>& V2)
{
  Vector2d<T> tmp;
  tmp[0] = V1[0] + V2[0];
  tmp[1] = V1[1] + V2[1];
  return tmp;
}

template<typename T>
inline
Vector2d<T> operator+(const Vector2d<T>& V1, 
		       const T& val)
{
  Vector2d<T> tmp = V1;
  tmp+=val;
  return tmp;
}

template<typename T>
inline
Vector2d<T> operator+(const T& val,
		       const Vector2d<T>& V1)
{
  return V1 + val;
}

template<typename T>
inline
Vector2d<T> operator-(const Vector2d<T>& V1, 
		      const Vector2d<T>& V2)
{
  Vector2d<T> tmp;
  tmp[0] = V1[0] - V2[0];
  tmp[1] = V1[1] - V2[1];
  return tmp;
}

template<typename T>
inline
Vector2d<T> operator-(const Vector2d<T>& V1, 
		       const T& val)
{
  Vector2d<T> tmp = V1;
  tmp-=val;
  return tmp;
}

template<typename T>
inline
Vector2d<T> operator-(const T& val,
		       const Vector2d<T>& V1)
{
  Vector2d<T> tmp;
  tmp[0] = val - V1[0];
  tmp[1] = val - V1[1];
  return tmp;
}


template<typename T>
inline
Vector2d<T> operator*(const Vector2d<T>& V1, 
		      const Vector2d<T>& V2)
{
  Vector2d<T> tmp;
  tmp[0] = V1[0] * V2[0];
  tmp[1] = V1[1] * V2[1];
  return tmp;
}

template<typename T>
inline
Vector2d<T> operator*(const Vector2d<T>& V1, 
		       const T& val)
{
  Vector2d<T> tmp = V1;
  tmp*=val;
  return tmp;
}

template<typename T>
inline
Vector2d<T> operator*(const T& val,
		       const Vector2d<T>& V1)
{
  return V1 * val;
}

template<typename T>
inline
Vector2d<T> operator/(const Vector2d<T>& V1, 
		      const Vector2d<T>& V2)
{
  Vector2d<T> tmp;
  tmp[0] = V1[0] / V2[0];
  tmp[1] = V1[1] / V2[1];
  return tmp;
}

template<typename T>
inline
Vector2d<T> operator/(const Vector2d<T>& V1, 
		       const T& val)
{
  Vector2d<T> tmp = V1;
  tmp/=val;
  return tmp;
}




}//slip::

namespace slip
{
  /*!
   ** \brief Computes the angular error usually used in optical flow estimation evaluations:
   ** \f[ 
   ** \arccos\left(\frac{w_g\cdot w_e}{||w_g||_2||w_e||_2}\right)
   ** \f]
   **  where :
   **  @arg \f$ w_g = (w_g^1,w_g^2,1)\f$
   **  @arg \f$ w_e = (w_e^1,w_e^2,1)\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/04/01
   ** \version 0.0.1
   ** \param __x x value.
   ** \param __y y value.
   ** \return the angular error in radian
   */
  template <class _Tp>
  struct AE_rad : public std::binary_function<_Tp, slip::Vector2d<_Tp>,slip::Vector2d<_Tp> >
    {
      _Tp
      operator()(const slip::Vector2d<_Tp>& __x, const slip::Vector2d<_Tp>& __y) const
      { 
	_Tp x_norm = std::sqrt(_Tp(1) + __x[0]*__x[0] + __x[1]*__x[1]);
	_Tp y_norm = std::sqrt(_Tp(1) + __y[0]*__y[0] + __y[1]*__y[1]);
	
	return std::acos((_Tp(1) +__x[0]*__y[0] + __x[1]*__y[1]) / (x_norm * y_norm)); 
      }
    };

 /*!
   ** \brief Computes the angular error usually used in optical flow estimation evaluations:
   ** \f[ 
   ** \frac{180}{\pi}\arccos\left(\frac{w_g\cdot w_e}{||w_g||_2||w_e||_2}\right)
   ** \f]
   **  where:
   **  @arg \f$ w_g = (w_g^1,w_g^2,1)\f$
   **  @arg \f$ w_e = (w_e^1,w_e^2,1)\f$
   ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2009/04/01
   ** \version 0.0.1
   ** \param __x x value.
   ** \param __y y value.
   ** \return the angular error in degree
   */
  template <class _Tp>
  struct AE_deg : public std::binary_function<_Tp, slip::Vector2d<_Tp>, slip::Vector2d<_Tp> >
    {
      _Tp
      operator()(const slip::Vector2d<_Tp>& __x, const slip::Vector2d<_Tp>& __y) const
      { 
	_Tp x_norm = std::sqrt(_Tp(1) + __x[0]*__x[0] + __x[1]*__x[1]);
	_Tp y_norm = std::sqrt(_Tp(1) + __y[0]*__y[0] + __y[1]*__y[1]);
	
	return (_Tp(180)/ slip::constants<_Tp>::pi())* std::acos((_Tp(1) +__x[0]*__y[0] + __x[1]*__y[1]) / (x_norm * y_norm)); 
      }
    };

  template<typename _Tp>
  inline
  std::string 
  Vector2d<_Tp>::name() const {return "Vector2d";} 
 


}//slip::

#endif //SLIP_VECTOR2D_HPP
