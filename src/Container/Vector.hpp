/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file Vector.hpp
 * 
 * \brief Provides a class to manipulate numerical vectors.
 * 
 */
#ifndef SLIP_VECTOR_HPP
#define SLIP_VECTOR_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <string>
#include "Array.hpp"
#include "Box1d.hpp"
#include "stride_iterator.hpp"
#include "apply.hpp"
#include "norms.hpp"
#include "linear_algebra_traits.hpp"
#include "linear_algebra.hpp"
#include "complex_cast.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template <typename T>
class Vector;

template <typename T>
std::ostream& operator<<(std::ostream & out, 
			 const Vector<T>& v);

template<typename T>
bool operator==(const Vector<T>& x, 
		const Vector<T>& y);

template<typename T>
bool operator!=(const Vector<T>& x, 
		const Vector<T>& y);

template<typename T>
bool operator<(const Vector<T>& x, 
	       const Vector<T>& y);

template<typename T>
bool operator>(const Vector<T>& x, 
	       const Vector<T>& y);

template<typename T>
bool operator<=(const Vector<T>& x, 
		const Vector<T>& y);

template<typename T>
bool operator>=(const Vector<T>& x, 
		const Vector<T>& y);
/*!
 *  @defgroup MathematicalContainer Mathematical containers
 *  @brief Some mathematical containers
 *  @{
 */
/*! \class Vector
** \ingroup Containers Containers1d MathematicalContainer
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.3
** \date 2014/03/25
** \since 1.0.0
** \brief Numerical %Vector class. 
** This container statisfies the RandomAccessContainer concepts of the 
** Standard Template Library (STL). It extends the interface of Array
** adding arithmetical: +=, -=, *=, /=,+,-,/,*... and mathematical operators :
** min, max, abs, sqrt, cos, acos, sin, asin, tan, atan, exp, log, cosh, sinh,
** tanh, log10, sum, apply...
** \param T. Type of object in the Vector 
*/
template <typename T>
class Vector
{
public :

  typedef T value_type;
  typedef Vector<T> self;
  typedef const Vector<T> const_self;

  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;

  typedef slip::stride_iterator<pointer> iterator_range;
  typedef slip::stride_iterator<const_pointer> const_iterator_range;

  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

  typedef std::reverse_iterator<iterator_range> reverse_iterator_range;
  typedef std::reverse_iterator<const_iterator_range> const_reverse_iterator_range;

  typedef typename slip::lin_alg_traits<value_type>::value_type norm_type;

   //default iterator of the container
  typedef iterator default_iterator;
  typedef const_iterator const_default_iterator;


  static const std::size_t DIM = 1;

public:
  /**
   ** \name Constructors & Destructors
   */
  /*@{*/
  
  /*!
  ** \brief Constructs a Vector.
  */
  Vector();
 
  /*!
  ** \brief Constructs a %Vector.
  ** \param n number of element in the %Vector
  */
  Vector(const size_type n);
  
  /*!
  ** \brief Constructs a %Vector initialized by the scalar value \a val.
  ** \param n number of element in the %Vector
  ** \param val initialization value of the elements 
  */
  Vector(const size_type n,
	 const T& val);
 
  /*!
  ** \brief Constructs a %Vector initialized by an array \a val.
  ** \param n number of element in the %Vector
  ** \param val initialization array value of the elements 
  */
  Vector(const size_type n,
	 const T* val);
 
  /**
  **  \brief  Contructs a %Vector from a range.
  **  \param  first  An input iterator.
  **  \param  last  An input iterator.
  **
  ** Create a %Vector consisting of copies of the elements from
  ** [first,last).
  */
  template<typename InputIterator>
  Vector(InputIterator first,
	 InputIterator last)
  {
    array_ = new Array<T>((size_type)(last - first),first,last);
  }

 /*!
  ** \brief Constructs a copy of the Vector \a rhs
  */
  Vector(const self& rhs);

 /*!
  ** \brief Destructor of the Vector
  */
  ~Vector();
  

  /*@} End Constructors */


  /*!
  ** \brief Resizes a %Vector.
  ** \param new_n new dimension of the %Vector
  ** \param val new value for all the elements
  */ 
  void resize(const size_type new_n,
	      const T& val = T());

 
  /**
   ** \name iterators
   */
  /*@{*/
 
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %Vector.  Iteration is done in ordinary
  **  element order.
  ** \return begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator begin();

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %Vector.  Iteration is done in ordinary
  **  element order.
  ** \return const begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator begin() const;

  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %Vector.  Iteration is done in ordinary
  **  element order.
  ** \return end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator end();
 
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %Vector.  Iteration is done in
  **  ordinary element order.
  ** \return const end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator end() const;
  
  /*!
  **  \brief Returns a read/write reverse iterator that points to the
  **  last element in the %Vector.  Iteration is done in reverse
  **  element order.
  ** \return reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rbegin();
  
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to the last element in the %Vector.  Iteration is done in
  **  reverse element order.
  ** \return const reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_reverse_iterator rbegin() const;

  /*!
  **  \brief Returns a read/write reverse iterator that points to one
  **  before the first element in the %Vector.  Iteration is done
  **  in reverse element order.
  **  \return reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rend();

  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to one before the first element in the %Vector.  Iteration
  **  is done in reverse element order.
  **  \return const reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.

  */
  const_reverse_iterator rend() const;
 

  /*!
  **  \brief Returns a read/write iterator_range that points 
  **  the first element within the %Range.  
  **  Iteration is done in ordinary element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return end iterator_range value
  ** \pre The range must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Vector<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator_range begin(const slip::Range<int>& range);
 
  /*!
  ** \brief Returns a read-only (constant) iterator_range 
  ** that points the first element within the %Range.  
  ** Iteration is done in ordinary element order according to the
  ** %Range
  ** \param range %Range to iterate.
  ** \return const_iterator_range value
  ** \pre The range must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Vector<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout,"
  "));
   ** \endcode
  */
  const_iterator_range begin(const slip::Range<int>& range) const;

  /*!
  **  \brief Returns a read/write iterator_range that points 
  **  one past the last element in the %Vector.  
  **  Iteration is done in ordinary element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return end iterator_range value
  ** \pre The range must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Vector<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator_range end(const slip::Range<int>& range);
 
  /*!
  **  \brief Returns a read-only (constant) iterator_range 
  ** that points one past the last element in the %Vector.  
  ** Iteration is done in ordinary element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return const_iterator_range value
  ** \pre The range must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Vector<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout,"
  "));
  ** \endcode
  */
  const_iterator_range end(const slip::Range<int>& range) const;



  /*!
  **  \brief Returns a read/write iterator that points 
  **  the first element within the %Box1d.  
  **  Iteration is done in ordinary element order.
  ** \param box %Box1d to iterate.
  ** \return end iterator value
  ** \pre The box must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Vector<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.begin(box),A5.end(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator begin(const slip::Box1d<int>& box);
 
  /*!
  ** \brief Returns a read-only (constant) iterator 
  ** that points the first element within the %Box1d.  
  ** Iteration is done in ordinary element order.
  ** \param box %Box1d to iterate.
  ** \return const_iterator value
  ** \pre The box must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Vector<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.begin(box),A5.end(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  const_iterator begin(const slip::Box1d<int>& box) const;

  /*!
  **  \brief Returns a read/write iterator that points 
  **  one past the last element in the %Vector.  
  **  Iteration is done in ordinary element order.
  ** \param box %Box1d to iterate.
  ** \return end iterator value
  ** \pre The box must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Vector<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.begin(box),A5.end(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator end(const slip::Box1d<int>& box);
 
  /*!
  **  \brief Returns a read-only (constant) iterator 
  ** that points one past the last element within the %Box1d.  
  ** Iteration is done in ordinary element order.
  ** \param box %Box1d to iterate.
  ** \return const_iterator value
  ** \pre The box must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Vector<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.begin(box),A5.end(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  const_iterator end(const slip::Box1d<int>& box) const;

  //
    /*!
  **  \brief Returns a read/write reverse_iterator_range that points 
  **  the end element within the %Range.  
  **  Iteration is done in reverse element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Vector<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator_range rbegin(const slip::Range<int>& range);
 
  /*!
  ** \brief Returns a read-only (constant) reverse_iterator_range 
  ** that points the end element within the %Range.  
  ** Iteration is done in reverse element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return cons_treverse__iterator_range value
  ** \pre The range must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Vector<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout,"
  "));
   ** \endcode
  */
  const_reverse_iterator_range rbegin(const slip::Range<int>& range) const;

  /*!
  **  \brief Returns a read/write reverse_iterator_range that points 
  **  one previous the first element in the %Range.  
  **  Iteration is done in reverse element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Vector<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator_range rend(const slip::Range<int>& range);
 
  /*!
  **  \brief Returns a read-only (constant) reverse_iterator_range 
  ** that points one previous the first element in the %Range.  
  ** Iteration is done in reverse element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return const_reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Vector<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout,"
  "));
  ** \endcode
  */
  const_reverse_iterator_range rend(const slip::Range<int>& range) const;



  /*!
  **  \brief Returns a read/write reverse_iterator that points 
  **  the last element within the %Box1d.  
  **  Iteration is done in reverse element order.
  ** \param box %Box1d to iterate.
  ** \return  reverse_iterator value
  ** \pre The box must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Vector<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.rbegin(box),A5.rend(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator rbegin(const slip::Box1d<int>& box);
 
  /*!
  ** \brief Returns a read-only (constant) reverse_iterator 
  ** that points the last element within the %Box1d.  
  ** Iteration is done in reverse element order.
  ** \param box %Box1d to iterate.
  ** \return const_reverse_iterator value
  ** \pre The box must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Vector<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.rbegin(box),A5.rend(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  const_reverse_iterator rbegin(const slip::Box1d<int>& box) const;

  /*!
  **  \brief Returns a read/write reverse_iterator that points 
  **  one previous the first element in the %Box1d.  
  **  Iteration is done in reverse element order.
  ** \param box %Box1d to iterate.
  ** \return reverse_iterator value
  ** \pre The box must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Vector<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.rbegin(box),A5.rend(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator rend(const slip::Box1d<int>& box);
 
  /*!
  **  \brief Returns a read-only (constant) reverse_iterator 
  ** that points one previous the first element of the %Box1d.  
  ** Iteration is done in reverse element order.
  ** \param box %Box1d to iterate.
  ** \return const_reverse_iterator value
  ** \pre The box must be inside the whole range of the %Vector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Vector<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.rbegin(box),A5.rend(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  const_reverse_iterator rend(const slip::Box1d<int>& box) const;
  /*@} End iterators */

  /**
   ** \name i/o operators
   */
  /*@{*/
  /*!
  ** \brief Write the %Vector to the ouput stream
  ** \param out output std:ostream
  ** \param v %Vector to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const self& v);
  /*@} End i/o operators */
  

   /**
   ** \name  Assignment operators and methods
   */
  /*@{*/
  /*!
  ** \brief Assign a %Vector.
  **
  ** Assign elements of %Vector in \a rhs
  **
  ** \param rhs %Vector to get the values from.
  ** \return 
  */
  self& operator=(const self & rhs);

  /*!
  ** \brief Assigns all the element of the %Vector by val
  ** \param val affectation value
  ** \return reference to corresponding %Vector
  */
  self& operator=(const T& val);

   /*!
   ** \brief Fills the container range [begin(),begin()+size()) 
   **        with copies of value
   ** \param value  A reference-to-const of arbitrary type.
  */
  void fill(const T& value)
  {
    std::fill_n(this->begin(),this->size(),value);
  } 
  
 /*!
  ** \brief Fills the container range [begin(),begin()+size())
  **        with a copy of
  **        the value array
  ** \param value  A pointer of arbitrary type.
  */
  void fill(const T* value)
  {
    std::copy(value,value + this->size(), this->begin());
  } 

  /*!
  ** \brief Fills the container range [begin(),begin()+size()) 
  **        with a copy of the range [first,last)
  **  \param  first  An input iterator.
  **  \param  last   An input iterator.
  **   
  **
  */
  template<typename InputIterator>
  void fill(InputIterator first,
	    InputIterator last)
  {
    std::copy(first,last, this->begin());
  }
  /*@} End Assignment operators and methods*/


   /**
   ** \name Comparison operators
   */
  /*@{*/
  /*!
  ** \brief Vector equality comparison
  ** \param x A %Vector
  ** \param y A %Vector of the same type of \a x
  ** \return true iff the size and the elements of the Arrays are equal
  */
  friend bool operator== <>(const Vector<T>& x, 
			    const Vector<T>& y);

 /*!
  ** \brief Vector inequality comparison
  ** \param x A %Vector
  ** \param y A %Vector of the same type of \a x
  ** \return true if !(x == y) 
  */
  friend bool operator!= <>(const Vector<T>& x, 
			    const Vector<T>& y);

 /*!
  ** \brief Less than comparison operator (Vector ordering relation)
  ** \param x A %Vector
  ** \param y A %Vector of the same type of \a x
  ** \return true iff \a x is lexicographically less than \a y
  */
  friend bool operator< <>(const Vector<T>& x, 
			   const Vector<T>& y);

 /*!
  ** \brief More than comparison operator
  ** \param x A %Vector
  ** \param y A %Vector of the same type of \a x
  ** \return true iff y > x 
  */
  friend bool operator> <>(const Vector<T>& x, 
			   const Vector<T>& y);

  /*!
  ** \brief Less than equal comparison operator
  ** \param x A %Vector
  ** \param y A %Vector of the same type of \a x
  ** \return true iff !(y > x) 
  */
  friend bool operator<= <>(const Vector<T>& x, 
			    const Vector<T>& y);

 /*!
  ** \brief More than equal comparison operator
  ** \param x A %Vector
  ** \param y A %Vector of the same type of \a x
  ** \return true iff !(x < y) 
  */
  friend bool operator>= <>(const Vector<T>& x, 
			    const Vector<T>& y);


  /*@} Comparison operators */
  
  
  /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Subscript access to the data contained in the %Vector.
  ** \param i The index of the row for which the data should be accessed. 
  ** \return Read/write reference to data.
  ** \pre i < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator[](const size_type i);

  /*!
  ** \brief Subscript access to the data contained in the %Vector.
  ** \param i The index of the row for which the data should be accessed. 
   ** \return Read_only (constant) reference to data.
  ** \pre i < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator[](const size_type i) const;

  /*!
  ** \brief Subscript access to the data contained in the %Vector.
  ** \param i The index of the row for which the data should be accessed. 
  ** \return Read/write reference to data.
  ** \pre i < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const size_type i);

  /*!
  ** \brief Subscript access to the data contained in the %Vector.
  ** \param i The index of the row for which the data should be accessed. 
  ** \return Read_only (constant) reference to data.
  ** \pre i < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const size_type i) const;

  /*!
  ** \brief Subscript access to the data contained in the %Vector.
  ** \param p The index point corresponding of the element for which data 
  ** should be accessed.
  ** \return Read/write reference to data.
  ** \pre p[0] < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const slip::Point1d<size_type>& p);
  
  /*!
  ** \brief Subscript access to the data contained in the %Vector.
  ** \param p The index point corresponding of the element for which data 
  ** should be accessed.
  ** \return Read-only (constant) reference to data.
  ** \pre p[0] < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const slip::Point1d<size_type>& p) const;

    /*!
  ** \brief Subscript access to the data contained in the %Vector.
  ** \param range The range of the %Vector.
  ** \return a copy of the range.
  ** \pre range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Vector ones.
  **
  ** This operator allows for easy, 1d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  self operator()(const slip::Range<int>& range);

  /*@} End Element access operators */


  /**
   ** \name  Arithmetic operators
   */
  /*@{*/

  /*!
  ** \brief Add val to each element of the Vector  
  ** \param val value
  ** \return reference to the resulting Vector
  */
  self& operator+=(const T& val);
  self& operator-=(const T& val);
  self& operator*=(const T& val);
  self& operator/=(const T& val);
  //   self& operator%=(const T& val);
  //   self& operator^=(const T& val);
  //   self& operator&=(const T& val);
  //   self& operator|=(const T& val);
  //   self& operator<<=(const T& val);
  //   self& operator>>=(const T& val);

  self  operator-() const;
  //self  operator!() const;

  self& operator+=(const self& rhs);
  self& operator-=(const self& rhs);
  self& operator*=(const self& rhs);
  self& operator/=(const self& rhs);
    

  /*@} End Arithmetic operators */

  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;

  /*!
  ** \brief Returns the number of elements in the %Vector
  */
  size_type size() const;

  /*!
  ** \brief Returns the maximal size (number of elements) in the %Vector
  */
  size_type max_size() const;

  /*!
  ** \brief Returns true if the %Vector is empty.  (Thus size() == 0)
  */
  bool empty()const;

  /*!
  ** \brief Swaps data with another %Vector.
  ** \param M A %Vector of the same element type
  */
  void swap(self& M);

  /*!
  ** \brief Returns the min element of the Vector according to the operator <
  ** if T is std::complex, it returns the element of minimum magnitude
  ** \pre size() != 0
  */
  T& min() const;


  /*!
  ** \brief Returns the max element of the Vector according to the operator >,
  ** if T is std::complex, it returns the element of maximum magnitude
  ** \pre size() != 0
  */
  T& max() const;

  /*!
  ** \brief Returns the sum of the elements of the Vector 
  ** \pre size() != 0
  */
  T sum() const;

   /*!
  ** \brief Returns the Euclidean norm \f$\left(\sum_i x_i\bar{x_i} \right)^{\frac{1}{2}}\f$ of the elements of the Vector 
  ** \pre size() != 0
  */
  norm_type Euclidean_norm() const
  {
    assert(this->size() != 0);
    return std::sqrt(slip::L22_norm_cplx(this->begin(),this->end()));
  }

  /*!
  ** \brief Returns the Euclidean norm \f$\left(\sum_i x_i\bar{x_i} \right)^{\frac{1}{2}}\f$ of the elements of the Vector 
  ** \pre size() != 0
  */
  norm_type L2_norm() const
  {
    assert(this->size() != 0);
    return this->Euclidean_norm();
  }

 /*!
  ** \brief Returns the L1 norm (\f$\sum_i |xi|\f$) of the elements of the Vector 
  ** \pre size() != 0
  */
  norm_type L1_norm() const
  {
    assert(this->size() != 0);
    return slip::L1_norm<norm_type>(this->begin(),this->end());
  }

  /*!
  ** \brief Returns the L22 norm (\f$\sum_i xi\bar{x_i}\f$) of the elements of the Vector 
  ** \pre size() != 0
  */
  norm_type L22_norm() const
  {
    assert(this->size() != 0);
    return slip::L22_norm_cplx(this->begin(),this->end());
  }

    /*!
  ** \brief Returns the infinite norm (\f$\max_i\{|xi|\}\f$) of the elements of the Vector 
  ** \pre size() != 0
  */
  norm_type infinite_norm() const
  {
    assert(this->size() != 0);
    return slip::infinite_norm<norm_type>(this->begin(),this->end());
  }
  /*!
  ** \brief Applys the one-parameter C-function \a fun 
  **        to each element of the %Vector
  ** \param fun The one-parameter C function 
  ** \return the resulting %Vector
  ** \par Example:
  ** \code
  ** slip::Vector<double> V(7);
  ** //fill V with values from 1 to 7 with step 1
  ** slip::iota(V.begin(),V.end(),1.0,1.0);
  ** //apply std::sqrt to each element of V
  ** V.apply(std::sqrt);
  **
  ** \endcode
  */
  Vector<T>& apply(T (*fun)(T));

  /*!
  ** \brief Applys the one-parameter C-function \a fun 
  **        to each element of the %Vector
  ** \param fun The one-const-parameter C function 
  ** \return the resulting %Vector
  ** \par Example:
  ** \code
  ** slip::Vector<double> V(7);
  ** //fill V with values from 1 to 7 with step 1
  ** slip::iota(V.begin(),V.end(),1.0,1.0);
  ** //apply std::sqrt to each element of V
  ** V.apply(std::sqrt);
  **
  ** \endcode
  */
  Vector<T>& apply(T (*fun)(const T&));

 private:
  Array<T>* array_;

private:
   friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & this->array_ ;
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & this->array_ ;
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()

};
/** @} */ // end of MathematicalContainer group

 ///double alias
  typedef slip::Vector<double> Vector_d;
  ///float alias
  typedef slip::Vector<float> Vector_f;
  ///long alias
  typedef slip::Vector<long> Vector_l;
  ///unsigned long alias
  typedef slip::Vector<unsigned long> Vector_ul;
  ///short alias
  typedef slip::Vector<short> Vector_s;
  ///unsigned long alias
  typedef slip::Vector<unsigned short> Vector_us;
  ///int alias
  typedef slip::Vector<int> Vector_i;
  ///unsigned int alias
  typedef slip::Vector<unsigned int> Vector_ui;
  ///char alias
  typedef slip::Vector<char> Vector_c;
  ///unsigned char alias
  typedef slip::Vector<unsigned char> Vector_uc;

  /*!
  ** \brief pointwise addition of two %Vector
  ** \param V1 The first %Vector 
  ** \param V2 The second %Vector
  ** \pre V1.size() == V2.size()
  ** \return resulting %Vector
  */
template<typename T>
Vector<T> operator+(const Vector<T>& V1, 
		    const Vector<T>& V2);

 /*!
  ** \brief addition of a scalar to each element of a %Vector
  ** \param V The %Vector 
  ** \param val The scalar
  ** \return resulting %Vector
  */
template<typename T>
Vector<T> operator+(const Vector<T>& V, 
		    const T& val);

 /*!
  ** \brief addition of a scalar to each element of a %Vector
  ** \param val The scalar
  ** \param V The %Vector  
  ** \return resulting %Vector
  */
template<typename T>
Vector<T> operator+(const T& val, 
		    const Vector<T>& V);


/*!
  ** \brief pointwise substraction of two %Vector
  ** \param V1 The first %Vector 
  ** \param V2 The second %Vector
  ** \pre V1.size() == V2.size()
  ** \return resulting %Vector
  */
template<typename T>
Vector<T> operator-(const Vector<T>& V1, 
		    const Vector<T>& V2);

 /*!
  ** \brief substraction of a scalar to each element of a %Vector
  ** \param V The %Vector 
  ** \param val The scalar
  ** \return resulting %Vector
  */
template<typename T>
Vector<T> operator-(const Vector<T>& V, 
		    const T& val);

 /*!
  ** \brief substraction of a scalar to each element of a %Vector
  ** \param val The scalar
  ** \param V The %Vector  
  ** \return resulting %Vector
  */
template<typename T>
Vector<T> operator-(const T& val, 
		    const Vector<T>& V);

/*!
  ** \brief pointwise multiplication of two %Vector
  ** \param V1 The first %Vector 
  ** \param V2 The second %Vector
  ** \pre V1.size() == V2.size()
  ** \return resulting %Vector
  */
template<typename T>
Vector<T> operator*(const Vector<T>& V1, 
		    const Vector<T>& V2);

 /*!
  ** \brief multiplication of a scalar to each element of a %Vector
  ** \param V The %Vector 
  ** \param val The scalar
  ** \return resulting %Vector
  */
template<typename T>
Vector<T> operator*(const Vector<T>& V, 
		    const T& val);

 /*!
  ** \brief multiplication of a scalar to each element of a %Vector
  ** \param val The scalar
  ** \param V The %Vector  
  ** \return resulting %Vector
  */
template<typename T>
Vector<T> operator*(const T& val, 
		    const Vector<T>& V);

/*!
  ** \brief pointwise division of two %Vector
  ** \param V1 The first %Vector 
  ** \param V2 The second %Vector
  ** \pre V1.size() == V2.size()
  ** \return resulting %Vector
  */
  template<typename T>
  Vector<T> operator/(const Vector<T>& V1, 
		      const Vector<T>& V2);

 /*!
  ** \brief division of a scalar to each element of a %Vector
  ** \param V The %Vector 
  ** \param val The scalar
  ** \return resulting %Vector
  */
  template<typename T>
  Vector<T> operator/(const Vector<T>& V, 
		      const T& val);



 /*!
  ** \relates Vector
  ** \brief Returns the min element of a Vector, if T is std::complex, 
  ** it returns the element of minimum magnitude
  ** \param M1 the Vector  
  ** \return the min element
  */
  template<typename T>
  T& min(const Vector<T>& M1);
  
  /*!
  ** \relates Vector
  ** \brief Returns the max element of a Vector, if T is std::complex, 
  ** it returns the element of maximum magnitude
  ** \param M1 the Vector  
  ** \return the max element
  */
  template<typename T>
  T& max(const Vector<T>& M1);

  
  /*!
  ** \relates Vector
  ** \brief Returns the abs value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  template<typename T>
  Vector<typename slip::lin_alg_traits<T>::value_type> abs(const Vector<T>& V);

  /*!
  ** \relates Vector
  ** \brief Returns the sqrt value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  template<typename T>
  Vector<T> sqrt(const Vector<T>& V);

  /*!
  ** \relates Vector
  ** \brief Returns the cos value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  template<typename T>
  Vector<T> cos(const Vector<T>& V);

  /*!
  ** \relates Vector
  ** \brief Returns the acos value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  Vector<float> acos(const Vector<float>& V);
  /*!
  ** \relates Vector
  ** \brief Returns the acos value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  Vector<double> acos(const Vector<double>& V);
  /*!
  ** \relates Vector
  ** \brief Returns the acos value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  Vector<long double> acos(const Vector<long double>& V);
  
  /*!
  ** \relates Vector
  ** \brief Returns the sin value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  template<typename T>
  Vector<T> sin(const Vector<T>& V);

  /*!
  ** \relates Vector
  ** \brief Returns the asin value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  Vector<float> asin(const Vector<float>& V);
  /*!
  ** \relates Vector
  ** \brief Returns the asin value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  Vector<double> asin(const Vector<double>& V);
  /*!
  ** \relates Vector
  ** \brief Returns the asin value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  Vector<long double> asin(const Vector<long double>& V);
  
  /*!
  ** \relates Vector
  ** \brief Returns the tan value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  template<typename T>
  Vector<T> tan(const Vector<T>& V);

  /*!
  ** \relates Vector
  ** \brief Returns the atan value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  Vector<float> atan(const Vector<float>& V);
  /*!
  ** \relates Vector
  ** \brief Returns the atan value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  Vector<double> atan(const Vector<double>& V);
  /*!
  ** \relates Vector
  ** \brief Returns the atan value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  Vector<long double> atan(const Vector<long double>& V);
  

  /*!
  ** \relates Vector
  ** \brief Returns the exp value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  template<typename T>
  Vector<T> exp(const Vector<T>& V);

  /*!
  ** \relates Vector
  ** \brief Returns the log value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  template<typename T>
  Vector<T> log(const Vector<T>& V);

  /*!
  ** \relates Vector
  ** \brief Returns the cosh value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  template<typename T>
  Vector<T> cosh(const Vector<T>& V);

  /*!
  ** \relates Vector
  ** \brief Returns the sinh value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  template<typename T>
  Vector<T> sinh(const Vector<T>& V);

  /*!
  ** \relates Vector
  ** \brief Returns the tanh value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  template<typename T>
  Vector<T> tanh(const Vector<T>& V);

  /*!
  ** \relates Vector
  ** \brief Returns the log10 value of each element of the %Vector
  ** \param V The %Vector  
  ** \return the resulting %Vector
  */
  template<typename T>
  Vector<T> log10(const Vector<T>& V);

}//slip::

namespace slip
{
  template<typename T>
  inline
  Vector<T>::Vector():
    array_(new slip::Array<T>())
  {}

  template<typename T>
  inline
  Vector<T>::Vector(typename Vector<T>::size_type n):
    array_(new slip::Array<T>(n))
  {}
  
  template<typename T>
  inline
  Vector<T>::Vector(typename Vector<T>::size_type n,
		    const T& val):
    array_(new slip::Array<T>(n,val))
  {}

  template<typename T>
  inline
  Vector<T>::Vector(typename Vector<T>::size_type n,
		    const T* val):
    array_(new slip::Array<T>(n,val))   
  {}
  
  template<typename T>
  inline
  Vector<T>::Vector(const Vector<T>& rhs):
    array_(new slip::Array<T>((*rhs.array_)))
  {}
  
  template<typename T>
  inline
  Vector<T>& Vector<T>::operator=(const Vector<T> & rhs)
  {
    if(this != &rhs)
      {
	*array_ = *(rhs.array_);
      }
    return *this;
  }

  template<typename T>
  inline
  Vector<T>::~Vector()
  {
    delete array_;
  }

  template<typename T>
  inline
  Vector<T>& Vector<T>::operator=(const T& val)
  {
    array_->operator=(val);
    return *this;
  }
  
  template<typename T>
  inline
  void Vector<T>::resize(typename Vector<T>::size_type new_size,
			 const T& val)
  {
    array_->resize(new_size,val);
  }


  template<typename T>
  inline
  typename Vector<T>::iterator Vector<T>::begin()
  {
    return array_->begin();
  }

  template<typename T>
  inline
  typename Vector<T>::iterator Vector<T>::end()
  {
    return array_->end();
  }
  
  template<typename T>
  inline
  typename Vector<T>::const_iterator Vector<T>::begin() const
  {
    slip::Array<T> const * tp(array_);
    return tp->begin();
  }

  template<typename T>
  inline
  typename Vector<T>::const_iterator Vector<T>::end() const
  {
    slip::Array<T> const * tp(array_);
    return tp->end();
  }
  

  template<typename T>
  inline
  typename Vector<T>::reverse_iterator Vector<T>::rbegin()
  {
    return array_->rbegin();
  }

  template<typename T>
  inline
  typename Vector<T>::reverse_iterator Vector<T>::rend()
  {
    return array_->rend();
  }

  template<typename T>
  inline
  typename Vector<T>::const_reverse_iterator Vector<T>::rbegin() const
  {
    slip::Array<T> const * tp(array_);
    return tp->rbegin();
  }

  template<typename T>
  inline
  typename Vector<T>::const_reverse_iterator  Vector<T>::rend() const
  {
    slip::Array<T> const * tp(array_);
    return tp->rend();
  }

  //
  template<typename T>
  inline
  typename Vector<T>::iterator_range 
  Vector<T>::begin(const slip::Range<int>& range)
  {
    return array_->begin(range);
  }

  template<typename T>
  inline
  typename Vector<T>::iterator_range 
  Vector<T>::end(const slip::Range<int>& range)
  {
    return array_->end(range);
  }
  
  template<typename T>
  inline
  typename Vector<T>::const_iterator_range 
  Vector<T>::begin(const slip::Range<int>& range) const
  {
   slip::Array<T> const * tp(array_);
    return tp->begin(range);
  }

  template<typename T>
  inline
  typename Vector<T>::const_iterator_range 
  Vector<T>::end(const slip::Range<int>& range) const
  {
    slip::Array<T> const * tp(array_);
    return tp->end(range);
  }

  //
  template<typename T>
  inline
  typename Vector<T>::iterator
  Vector<T>::begin(const slip::Box1d<int>& box)
  {
    return array_->begin(box);
  }

  template<typename T>
  inline
  typename Vector<T>::iterator 
  Vector<T>::end(const slip::Box1d<int>& box)
  {
    return array_->end(box);
  }
  
  template<typename T>
  inline
  typename Vector<T>::const_iterator 
  Vector<T>::begin(const slip::Box1d<int>& box) const
  {
    slip::Array<T> const * tp(array_);
    return tp->begin(box);
  }

  template<typename T>
  inline
  typename Vector<T>::const_iterator 
  Vector<T>::end(const slip::Box1d<int>& box) const
  {
    slip::Array<T> const * tp(array_);
    return tp->end(box);
  }

  //
  template<typename T>
  inline
  typename Vector<T>::reverse_iterator Vector<T>::rbegin(const slip::Box1d<int>& box)
  {
    return array_->rbegin(box);
  }

  template<typename T>
  inline
  typename Vector<T>::reverse_iterator 
  Vector<T>::rend(const slip::Box1d<int>& box)
  {
    return array_->rend(box);
  }

  template<typename T>
  inline
  typename Vector<T>::const_reverse_iterator 
  Vector<T>::rbegin(const slip::Box1d<int>& box) const
  {
   slip::Array<T> const * tp(array_);
    return tp->rbegin(box);
  }

  template<typename T>
  inline
  typename Vector<T>::const_reverse_iterator 
  Vector<T>::rend(const slip::Box1d<int>& box) const
  {
    slip::Array<T> const * tp(array_);
    return tp->rend(box);
  }
  //
  template<typename T>
  inline
  typename Vector<T>::reverse_iterator_range Vector<T>::rbegin(const slip::Range<int>& range)
  {
    return array_->rbegin(range);
  }

  template<typename T>
  inline
  typename Vector<T>::reverse_iterator_range 
  Vector<T>::rend(const slip::Range<int>& range)
  {
    return array_->rend(range);
  }

  template<typename T>
  inline
  typename Vector<T>::const_reverse_iterator_range 
  Vector<T>::rbegin(const slip::Range<int>& range) const
  {
    slip::Array<T> const * tp(array_);
    return tp->rbegin(range);
  }

  template<typename T>
  inline
  typename Vector<T>::const_reverse_iterator_range 
  Vector<T>::rend(const slip::Range<int>& range) const
  {
    slip::Array<T> const * tp(array_);
    return tp->rend(range);
  }


  template <typename T>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const Vector<T>& v)
  {
    out<<*(v.array_);
    return out;
  }

  template<typename T>
  inline
  typename Vector<T>::reference Vector<T>::operator[](typename Vector<T>::size_type i)
  {
    return (*array_)[i];
  }
  
  template<typename T>
  inline
  typename Vector<T>::const_reference Vector<T>::operator[](typename Vector<T>::size_type i) const 
  {
    return (*array_)[i];
  }
  
  template<typename T>
  inline
  typename Vector<T>::reference Vector<T>::operator()(typename Vector<T>::size_type i)
  {
    return (*array_)(i);
  }
  
  template<typename T>
  inline
  typename Vector<T>::const_reference Vector<T>::operator()(typename Vector<T>::size_type i) const
  {
    return (*array_)(i);
  }

  template <typename T>
  inline
  typename Vector<T>::reference 
  Vector<T>::operator()(const slip::Point1d<typename Vector<T>::size_type>& p)
  {
    return (*array_)(p);
  }

  template <typename T>
  inline
  typename Vector<T>::const_reference 
  Vector<T>::operator()(const slip::Point1d<typename Vector<T>::size_type>& p) const
  {
    return (*array_)(p);
  }

  template<typename T>
  inline
  Vector<T> Vector<T>::operator()(const slip::Range<int>& range)
  {
     assert(this->array_ != 0);
     assert(range.is_valid());
     assert(range.start() < (int)this->size());
     assert(range.stop()  < (int)this->size());
     return Vector<T>(array_->begin(range),array_->end(range));

  }

  template<typename T>
  inline
  std::string 
  Vector<T>::name() const {return "Vector";} 

  template<typename T>
  inline
  typename Vector<T>::size_type Vector<T>::size() const {return array_->size();}

  template<typename T>
  inline
  typename Vector<T>::size_type Vector<T>::max_size() const 
  {
    return typename Vector<T>::size_type(-1)/sizeof(T);
  }
  
  template<typename T>
  inline
  bool Vector<T>::empty()const {return array_->empty();}

  template<typename T>
  inline
  void Vector<T>::swap(Vector<T>& V)
  {
    array_->swap(*(V.array_));
  }


  template<typename T>
  inline
  Vector<T>& Vector<T>::operator+=(const T& val)
  {
    std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::plus<T>(),val));
    return *this;
  }

  template<typename T>
  inline
  Vector<T>& Vector<T>::operator-=(const T& val)
  {
    std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::minus<T>(),val));
    return *this;
  }

  template<typename T>
  inline
  Vector<T>& Vector<T>::operator*=(const T& val)
  {
    std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::multiplies<T>(),val));
    return *this;
  }
  
  template<typename T>
  inline
  Vector<T>& Vector<T>::operator/=(const T& val)
  {
    std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::multiplies<T>(),T(1)/val));
    return *this;
  }
  

  template<typename T>
  inline
  Vector<T> Vector<T>::operator-() const
  {
    Vector<T> tmp(*this);
    std::transform(array_->begin(),array_->end(),tmp.begin(),std::negate<T>());
    return tmp;
  }

  template<typename T>
  inline
  Vector<T>& Vector<T>::operator+=(const Vector<T>& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::plus<T>());
    return *this;
  }
  
  template<typename T>
  inline
  Vector<T>& Vector<T>::operator-=(const Vector<T>& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::minus<T>());
    return *this;
  }
  
  template<typename T>
  inline
  Vector<T>& Vector<T>::operator*=(const Vector<T>& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::multiplies<T>());
    return *this;
  }

   template<typename T>
  inline
  Vector<T>& Vector<T>::operator/=(const Vector<T>& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::divides<T>());
    return *this;
  }
  
  template<typename T>
  inline
  T& Vector<T>::min() const
  {
    assert(array_->size() != 0);
    return *std::min_element(array_->begin(),array_->end(),std::less<T>());
  }

  template<typename T>
  inline
  T& Vector<T>::max() const
  {
    assert(array_->size() != 0);
    return *std::max_element(array_->begin(),array_->end(),std::less<T>());
  }

  template<typename T>
  inline
  T Vector<T>::sum() const
  {
    assert(array_->size() != 0);
    return std::accumulate(array_->begin(),array_->end(),T(0));
  }

  template<typename T>
  inline
  Vector<T>& Vector<T>::apply(T (*fun)(T))
  {
    slip::apply(this->begin(),this->end(),this->begin(),fun);
    return *this;
  }

  template<typename T>
  inline
  Vector<T>& Vector<T>::apply(T (*fun)(const T&))
  {
    slip::apply(this->begin(),this->end(),this->begin(),fun);
    return *this;
  }


  
/** \name Arithmetical functions */
  /* @{ */
template<typename T>
inline
Vector<T> operator+(const Vector<T>& V1, 
		    const Vector<T>& V2)
{
    assert(V1.size() == V2.size());
    Vector<T> tmp(V1.size());
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::plus<T>());

    return tmp;
}

template<typename T>
inline
Vector<T> operator+(const Vector<T>& V1, 
		    const T& val)
{
    Vector<T> tmp(V1);
    tmp+=val;
    return tmp;
}

template<typename T>
inline
Vector<T> operator+(const T& val,
		    const Vector<T>& V1)
{
  return V1 + val;
}

template<typename T>
inline
Vector<T> operator-(const Vector<T>& V1, 
		    const Vector<T>& V2)
{
    assert(V1.size() == V2.size());
    Vector<T> tmp(V1.size());
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::minus<T>());
    return tmp;
}

template<typename T>
inline
Vector<T> operator-(const Vector<T>& V1, 
		    const T& val)
{
    Vector<T> tmp(V1);
    tmp-=val;
    return tmp;
}

template<typename T>
inline
Vector<T> operator-(const T& val,
		    const Vector<T>& V1)
{
  return -(V1 - val);
}

template<typename T>
inline
Vector<T> operator*(const Vector<T>& V1, 
		    const Vector<T>& V2)
{
    assert(V1.size() == V2.size());
    Vector<T> tmp(V1.size());
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::multiplies<T>());
    return tmp;
}

template<typename T>
inline
Vector<T> operator*(const Vector<T>& V1, 
		    const T& val)
{
    Vector<T> tmp(V1);
    tmp*=val;
    return tmp;
}

template<typename T>
inline
Vector<T> operator*(const T& val,
		    const Vector<T>& V1)
{
  return V1 * val;
}

template<typename T>
inline
Vector<T> operator/(const Vector<T>& V1, 
		    const Vector<T>& V2)
{
    assert(V1.size() == V2.size());
    Vector<T> tmp(V1.size());
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::divides<T>());
    return tmp;
}

template<typename T>
inline
Vector<T> operator/(const Vector<T>& V1, 
		    const T& val)
{
    Vector<T> tmp(V1);
    tmp/=val;
    return tmp;
}

/* @} */

template<typename T>
inline
T& min(const Vector<T>& V1)
{
  return V1.min();
}

template<typename T>
inline
T& max(const Vector<T>& V1)
{
  return V1.max();
}

template<typename T>
inline
Vector<typename slip::lin_alg_traits<T>::value_type> abs(const Vector<T>& V)
{
  
  Vector<typename slip::lin_alg_traits<T>::value_type> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::abs);
  return tmp;
}

template<typename T>
inline
Vector<T> sqrt(const Vector<T>& V)
{
  Vector<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::sqrt);
  return tmp;
}

template<typename T>
inline
Vector<T> cos(const Vector<T>& V)
{
  Vector<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::cos);
  return tmp;
}


inline
Vector<float> acos(const Vector<float>& V)
{
  Vector<float> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::acos);
  return tmp;
}

inline
Vector<double> acos(const Vector<double>& V)
{
  Vector<double> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::acos);
  return tmp;
}

inline
Vector<long double> acos(const Vector<long double>& V)
{
  Vector<long double> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::acos);
  return tmp;
}

template<typename T>
inline
Vector<T> sin(const Vector<T>& V)
{
  Vector<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::sin);
  return tmp;
}



inline
Vector<float> asin(const Vector<float>& V)
{
  Vector<float> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::asin);
  return tmp;
}

inline
Vector<double> asin(const Vector<double>& V)
{
  Vector<double> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::asin);
  return tmp;
}

inline
Vector<long double> asin(const Vector<long double>& V)
{
  Vector<long double> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::asin);
  return tmp;
}

template<typename T>
inline
Vector<T> tan(const Vector<T>& V)
{
  Vector<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::tan);
  return tmp;
}

inline
Vector<float> atan(const Vector<float>& V)
{
  Vector<float> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::atan);
  return tmp;
}

inline
Vector<double> atan(const Vector<double>& V)
{
  Vector<double> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::atan);
  return tmp;
}

inline
Vector<long double> atan(const Vector<long double>& V)
{
  Vector<long double> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::atan);
  return tmp;
}

template<typename T>
inline
Vector<T> exp(const Vector<T>& V)
{
  Vector<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::exp);
  return tmp;
}

template<typename T>
inline
Vector<T> log(const Vector<T>& V)
{
  Vector<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::log);
  return tmp;
}

template<typename T>
inline
Vector<T> cosh(const Vector<T>& V)
{
  Vector<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::cosh);
  return tmp;
}

template<typename T>
inline
Vector<T> sinh(const Vector<T>& V)
{
  Vector<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::sinh);
  return tmp;
}

template<typename T>
inline
Vector<T> tanh(const Vector<T>& V)
{
  Vector<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::tanh);
  return tmp;
}

template<typename T>
inline
Vector<T> log10(const Vector<T>& V)
{
  Vector<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::log10);
  return tmp;
}

/** \name EqualityComparable functions */
  /* @{ */
template<typename T>
inline
bool operator==(const Vector<T>& x, 
		const Vector<T>& y)
{
  return *(x.array_) == *(y.array_); 
}

template<typename T>
inline
bool operator!=(const Vector<T>& x, 
		const Vector<T>& y)
{
  return !(x == y);
}
/* @} */
/** \name LessThanComparable functions */
  /* @{ */
template<typename T>
inline
bool operator<(const Vector<T>& x, 
	       const Vector<T>& y)
{
  return  *(x.array_) < *(y.array_);
}

template<typename T>
inline
bool operator>(const Vector<T>& x, 
	       const Vector<T>& y)
{
  return (y < x);
}

template<typename T>
inline
bool operator<=(const Vector<T>& x, 
		const Vector<T>& y)
{
  return !(y < x);
}

template<typename T>
inline
bool operator>=(const Vector<T>& x, 
		const Vector<T>& y)
{
  return !(x < y);
} 
/* @} */
}//slip::

#endif //SLIP_VECTOR_HPP
