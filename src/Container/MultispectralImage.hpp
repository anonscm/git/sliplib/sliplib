/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file MultispectralImage.hpp
 * 
 * \brief Provides a class to manipulate 2d Multispectral Images.
 * 
 */
#ifndef SLIP_MULTISPECTRALIMAGE_HPP
#define SLIP_MULTISPECTRALIMAGE_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <cmath>
#include <string>
#include <vector>
#include <cstddef>
#include "stride_iterator.hpp"
#include "kstride_iterator.hpp"
#include "iterator2d_box.hpp"
#include "iterator2d_range.hpp"
#include "apply.hpp"
#include "Block.hpp"
#include "GenericMultiComponent2d.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/base_object.hpp>


namespace slip
{

template<class T>
class stride_iterator;

template<class T>
class iterator2d_box;

template<class T>
class iterator2d_range;

template<class T>
class const_iterator2d_box;

template<class T>
class const_iterator2d_range;

template <class T>
class DPoint2d;

template <class T>
class Point2d;

template <class T>
class Box2d;

template <typename T, std::size_t NB_COMPONENT>
class MultispectralImage;




/*! \class MultispectralImage
**  \ingroup Containers Containers2d MultiComponent2dContainers
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/04/01
** \since 1.0.0
** \brief This is a Multi Spectral Image container.
** This container statisfies the BidirectionnalContainer concepts of the STL.
** It is also an 2d extension of the RandomAccessContainer concept. That is
** to say the bracket element access is replaced by the double bracket element 
** access.
** It is a specialization of slip::GenericMulitComponent2d using blocks.
** \param T Type of the data in the MultispectralImage 
**
** \par Axis conventions:
** \image html iterator2d_conventions.jpg "axis and notation conventions"
** \image latex iterator2d_conventions.eps "axis and notation conventions" width=5cm
  **
*/
template <typename T, std::size_t NB_COMPONENT>
class MultispectralImage : public GenericMultiComponent2d<slip::block<T,NB_COMPONENT> >
{

public:
  typedef typename slip::GenericMultiComponent2d<slip::block<T,NB_COMPONENT> >::value_type value_type;
  typedef MultispectralImage<T,NB_COMPONENT> self;
  typedef const MultispectralImage<T,NB_COMPONENT> const_self;
  typedef GenericMultiComponent2d<slip::block<T,NB_COMPONENT> > base;
  
  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;

  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

  static const std::size_t DIM = 2;
public:
 /**
  ** \name Constructors & Destructors
  */
 /*@{*/

  /*!
  ** \brief Constructs a %MultispectralImage.
  */
  MultispectralImage():
    base()
    {}
 
  /*!
  ** \brief Constructs a %MultispectralImage.
  ** \param height first dimension of the %MultispectralImage
  ** \param width second dimension of the %MultispectralImage
  */
  MultispectralImage(const size_type height,
		     const size_type width):
    base(height,width)
  {}
  
  /*!
  ** \brief Constructs a %MultispectralImage initialized by the scalar value \a val.
  ** \param height first dimension of the %MultispectralImage
  ** \param width second dimension of the %MultispectralImage
  ** \param val initialization value of the elements 
  */
  MultispectralImage(const size_type height,
		     const size_type width, 
		     const slip::block<T,NB_COMPONENT>& val):
    base(height,width,val)
  {}
  
  /*!
  ** \brief Constructs a %MultispectralImage initialized by an array \a val.
  ** \param height first dimension of the %MultispectralImage
  ** \param width second dimension of the %MultispectralImage
  ** \param val initialization linear array value of the elements 
  */
  MultispectralImage(const size_type height,
		     const size_type width, 
		     const T* val):
    base(height,width,val)
  {}

 
  /*!
  ** \brief Constructs a %MultispectralImage initialized by an array \a val.
  ** \param height first dimension of the %MultispectralImage
  ** \param width second dimension of the %MultispectralImage
  ** \param val initialization array value of the elements 
  */
  MultispectralImage(const size_type height,
		     const size_type width, 
		     const slip::block<T,NB_COMPONENT>* val):
    base(height,width,val)
  {}
 

  /**
  **  \brief  Contructs a %MultispectralImage from a range.
  ** \param height first dimension of the %MultispectralImage
  ** \param width second dimension of the %MultispectralImage
  **  \param  first  An input iterator.
  **  \param  last  An input iterator.
  **
  ** Create a %MultispectralImage consisting of copies of the elements from
  ** [first,last).
  */
  template<typename InputIterator>
  MultispectralImage(const size_type height,
		     const size_type width, 
		     InputIterator first,
		     InputIterator last):
    base(height,width,first,last)
  {} 

 
 /**
  **  \brief  Contructs a %MultispectralImage from a N ranges.
  ** \param height first dimension of the %MultispectralImage
  ** \param width second dimension of the %MultispectralImage
  **  \param  first_iterators_list  A std::vector of the first input iterators.
  **  \param  last  An input iterator.
  **
  **
  ** Create a %Multispectrale consisting of copies of the elements from
  ** [first_iterators_list[0],last), 
  ** [first_iterators_list[1],first_iterators_list[1] + (last - first_iterators_list[0]), 
  ** ...
  */
  template<typename InputIterator>
  MultispectralImage(const size_type height,
		     const size_type width, 
		     std::vector<InputIterator> first_iterators_list,
		     InputIterator last):
    base(height,width,first_iterators_list,last)
  {} 


  /*!
  ** \brief Constructs a copy of the %MultispectralImage \a rhs
  */
  MultispectralImage(const self& rhs):
    base(rhs)
  {}

  /*!
  ** \brief Destructor of the %MultispectralImage
  */
  ~MultispectralImage()
  {}
  

  /*@} End Constructors */


 


  /*!
  ** \brief Affects all the element of the %MulitspectralImage by val
  ** \param val affectation value
  ** \return reference to corresponding %MulitspectralImage
  */
  self& operator=(const slip::block<T,NB_COMPONENT>& val);

  
 /*!
  ** \brief Affects all the element of the %MulitspectralImage by val
  ** \param val affectation value
  ** \return reference to corresponding %MulitspectralImage
  */
  self& operator=(const T& val);

  
  /*@} End Assignment operators and methods*/


   /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;

 
private :
   friend class boost::serialization::access;
 template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::GenericMultiComponent2d<slip::block<T,NB_COMPONENT> > >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::GenericMultiComponent2d<slip::block<T,NB_COMPONENT> >  >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
};



}//slip::

namespace slip{

}//slip::

namespace slip
{



  template<typename T, std::size_t NB_COMPONENT>
  inline
  MultispectralImage<T,NB_COMPONENT>& 
  MultispectralImage<T,NB_COMPONENT>::operator=(const slip::block<T,NB_COMPONENT>& val)
  {
    std::fill_n(this->begin(),this->size(),val);
    return *this;
  }

   template<typename T, std::size_t NB_COMPONENT>
   inline
   MultispectralImage<T,NB_COMPONENT>& 
   MultispectralImage<T,NB_COMPONENT>::operator=(const T& val)
   {
     std::fill_n(this->begin(),this->size(),val);
     return *this;
   }

  template<typename T, std::size_t NB_COMPONENT>
  inline
  std::string 
  MultispectralImage<T, NB_COMPONENT>::name() const {return "MultispectralImage";} 


}//slip::

#endif //SLIP_MULTISPECTRALIMAGE_HPP
