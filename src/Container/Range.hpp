/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Range.hpp
 * 
 * \brief Provides a class to manipulate Ranges.
 * 
 */
#ifndef SLIP_RANGE_HPP
#define SLIP_RANGE_HPP

#include <iostream>
#include <cassert>
#include <string>

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{


template <typename SubType>
class Range;

template <typename SubType>
std::ostream& operator<<(std::ostream & out, 
			 const Range<SubType>& b);

template <typename SubType>
bool operator==(const Range<SubType>& b1, 
		const Range<SubType>& b2);

template <typename SubType>
bool operator!=(const Range<SubType>& b1, 
		const Range<SubType>& b2);


/*! \class Range
**  \ingroup Containers MiscellaneousContainers
** \brief This is a %Range class
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/04/02
** \since 1.0.0
** \param SubType Subscript type of the Range
*/
template <typename SubType>
class Range
{
public:

   typedef Range<SubType> self;
  
  /**
   ** \name Constructors & Destructors
   */
  
  /*!
  ** \brief Constructs a default %Range
  ** start_sub = stop_sub = stride = 0
  */
  Range();

  /*!
  ** \brief Constructs a %Range
  ** \param start_sub Start subscript
  ** \param stop_sub Stop subscript
  ** \param stride Stride of the range
  */
  Range(const SubType& start_sub, 
	const SubType& stop_sub,
	const int stride = 1);
  
  /*@} End Constructors */

  /**
   ** \name i/o operators
   */
  /*@{*/

  /*!
  ** \brief Write the %Range to the ouput stream
  ** \param out output stream
  ** \param r Range to write to the output stream
  */  
  friend std::ostream& operator<< <>(std::ostream & out,  
				     const self& r);
  
  /*@} i/o  operators */

  
  /**
   ** \name  Comparison operators
   */
  /*@{*/
  /*!
  ** \brief Compare two %Range.
  ** \param r1 First range.
  ** \param r2 Second range.
  ** \return true iff :
  ** (r1.start_sub_ == r2.start_sub_) && (r1.stop_sub_ = r2.stop_sub_) && 
  ** (r1.stride_ == r2.stride_)
  **
  */  
  friend bool operator== <>(const Range<SubType>& r1,
			    const Range<SubType>& r2);
  
   /*!
  ** \brief Compare two ranges.
  ** \param r1 First range.
  ** \param r2 Second range.
  ** \return true iff !(r1 == r2).
  */  
  friend bool operator!= <>(const Range<SubType>& r1,
			    const Range<SubType>& r2);
  /*@} comparison  operators */

  
 /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;

  /*!
  ** \brief Accessor of the start subscript of the %Range.
  ** \return the start subscript.
  */
  SubType start() const;

   /*!
  ** \brief Accessor of the stop subscript of the %Range.
  ** \return the stop subscript.
  */
  SubType stop() const;

   /*!
  ** \brief Accessor of the stride of the %Range.
  ** \return the stride.
  */
  int stride() const;

  /*!
  ** \brief Rerturns the number of iterations of the range
  ** \return the number of iteration of the %Range
  */
  std::size_t iterations() const;
 

  /*@} End Element access operators */


  /*!
  ** \brief Returns true if the range is valid :
  **   - if stride >= 0 : start_sub_ <= stop_sub_
  **   - if stride <=0  : start_sub  >= stop_sub_
  ** else false.
  ** \return a boolean.
  */
  bool is_valid() const;
  
private :
  SubType start_sub_;
  SubType stop_sub_;
  int stride_;
private :
  friend class boost::serialization::access;
  template<class Archive>
  void save(Archive & ar, const unsigned int version) const
  {
    if(version >= 0)
      {
	ar & start_sub_;
	ar & stop_sub_;
	ar & stride_;
      }
  }
	template<class Archive>
	  void load(Archive & ar, const unsigned int version)
	{
	if(version >= 0)
	  {
	ar & start_sub_;
	ar & stop_sub_;
	ar & stride_;
      }
      }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

}//slip::

namespace slip
{

  template<typename SubType>
  inline
  Range<SubType>::Range():
    start_sub_(0),stop_sub_(0),stride_(0)
  {}

  template<typename SubType>
  inline
  Range<SubType>::Range(const SubType& start_sub,
			const SubType& stop_sub,
			int stride):
    start_sub_(start_sub),stop_sub_(stop_sub),stride_(stride)
  {}

  
  template<typename SubType>
  inline
  std::string
  Range<SubType>::name() const
  {
    return "Range";
  }


   template<typename SubType>
  inline
   SubType Range<SubType>::start() const
   {
     return start_sub_;
   }
 
  template<typename SubType>
  inline
  SubType Range<SubType>::stop() const
   {
     return stop_sub_;
   }
 
  template<typename SubType>
  inline
  int Range<SubType>::stride() const
   {
     return stride_;
   }
 
  template<typename SubType>
  inline
  std::size_t Range<SubType>::iterations() const
   {
     int ret = 0;
     if( stride_ != 0)
       {
	 ret = ( (stop_sub_ - start_sub_) / stride_ );
       }
     if (ret < 0)
       {
	 ret = - ret;
       }
     return (std::size_t)ret;
   }
 

  template<typename SubType>
  inline
  bool Range<SubType>::is_valid() const
   {
     bool result = false;
     if(stride_ >= 0)
       result = (start_sub_ <= stop_sub_);
     else
       result = (start_sub_ >= stop_sub_);
     return result;
   }
 
}//slip::


namespace slip
{
  template<typename SubType>
  inline
  std::ostream& operator<<(std::ostream & out,  
			   const Range<SubType>& b)
  {
    out<<"[ "<<b.start_sub_<<":"<<b.stop_sub_<<":"<<b.stride_<<" ]";
    return out;
  }

  template<typename SubType>
  inline
  bool operator==(const Range<SubType>& r1,
		  const Range<SubType>& r2)
  {
   
    return   (r1.start_sub_ == r2.start_sub_) 
          && (r1.stop_sub_  == r2.stop_sub_) 
          && (r1.stride_    == r2.stride_);
  }

  template<typename SubType>
  inline
  bool operator!=(const Range<SubType>& r1,
		  const Range<SubType>& r2)
  {

    return !(r1 == r2);
  }
}//::slip

#endif //SLIP_RANGE_HPP
