/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/*! \file Box4d.hpp
 ** \brief Provides a class to manipulate 4d box
 ** \version Fluex 1.0
 ** \date 2013/07/01
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef SLIP_BOX4D_HPP
#define SLIP_BOX4D_HPP

#include <iostream>
#include <cassert>
#include <string>
#include "Box.hpp"
#include "Point4d.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{


/*!
 **  \ingroup Containers MiscellaneousContainers Fluex
 ** \class Box4d
 ** \version Fluex 1.0
 ** \date 2013/07/01
 ** \since 1.4.0
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief This is a %Box4d class, a specialized version of slip::Box<CoordType,DIM>
 **        with DIM = 4
 ** \par Description:
 ** The box is defined by its first-front-upper-left and last-back-bottom-right points p1 and p2.
 ** \image html Conventions4d.jpg "Box4d conventions"
 ** \image latex Conventions4d.eps "Box4d conventions" width=5cm
 ** \param CoordType Type of the coordinates of the Box4d
 */
template <typename CoordType>
class Box4d: public slip::Box<CoordType,4>
{
public :
	typedef Box4d<CoordType> self;
	typedef slip::Box<CoordType,4> base;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/
	/*!
	 ** \brief Constructs a Box4d.
	 ** \remarks call the default constructors of slip::Point<CoordType,4>
	 */
	Box4d();
	/*!
	 ** \brief Constructs a Box4d from a CoordType array.
	 ** \param p1 first extremal point
	 ** \param p2 second extremal point
	 */
	Box4d(const slip::Point<CoordType,4>& p1,
			const slip::Point<CoordType,4>& p2);

	/*!
	 ** \brief Constructs a Box4d.
	 ** \param x11 first coordinate of the minimal point p1
	 ** \param x12 second coordinate of the minimal point p1
	 ** \param x13 third coordinate of the minimal point p1
	 ** \param x14 fourth coordinate of the minimal point p1
	 ** \param x21 first coordinate of the maximal point p2
	 ** \param x22 second coordinate of the maximal point p2
	 ** \param x23 third coordinate of the maximal point p2
	 ** \param x24 fourth coordinate of the maximal point p2
	 */
	Box4d(const CoordType& x11,
			const CoordType& x12,
			const CoordType& x13,
			const CoordType& x14,
			const CoordType& x21,
			const CoordType& x22,
			const CoordType& x23,
			const CoordType& x24);

	/*!
	 ** \brief Constructs a square Box4d using a central point and a width.
	 ** \param xc first coordinate of the central point
	 ** \param yc second coordinate of the central point
	 ** \param zc third coordinate of the central point
	 ** \param lc fourth coordinate of the central point
	 ** \param r radius of the box (dimensions = (2r,2r,2r,2r))
	 */
	Box4d(const CoordType& xc,
			const CoordType& yc,
			const CoordType& zc,
			const CoordType& lc,
			const CoordType& r);

	/*!
	 ** \brief Constructs a square Box4d using a central point and a radius.
	 ** \param pc central point
	 ** \param r radius of the box (dimensions = (2r,2r,2r,2r))
	 */
	Box4d(const slip::Point<CoordType,4>& pc,
			const CoordType& r);

	/*!
	 ** \brief Constructs a square Box4d using a central point and four radius.
	 ** \param pc central point
	 ** \param r1 radius of the first coordinate box (dim1 =2r1)
	 ** \param r2 radius of the second coordinate box (dim2 =2r2)
	 ** \param r3 radius of the third coordinate box (dim3 =2r3)
	 ** \param r4 radius of the fourth coordinate box (dim4 =2r4)
	 */
	Box4d(const slip::Point<CoordType,4>& pc,
			const CoordType& r1,
			const CoordType& r2,
			const CoordType& r3,
			const CoordType& r4);

	/*@} End Constructors */

	/**
	 ** \name  Element access operators
	 */
	/*@{*/
	/*!
	 ** \brief Accessor/Mutator of the first_front_upper_left point (p1) of Box4d.
	 ** \return reference to the first_front_upper_left Point4d of the Box4d
	 */
	void first_front_upper_left(slip::Point<CoordType,4>);
	/*!
	 ** \brief Accessor/Mutator of the first_front_upper_left point (p1) of Box4d.
	 ** \return reference to the first_front_upper_left Point4d of the Box4d
	 */
	slip::Point<CoordType,4>& first_front_upper_left();
	/*!
	 ** \brief Accessor/Mutator of the first_front_upper_left point (p1) of Box4d.
	 ** \return reference to the front_upper_left Point4d of the Box4d
	 */
	const slip::Point<CoordType,4>& first_front_upper_left() const;


	/*!
	 ** \brief Accessor/Mutator of the last_back_bottom_right point (p2) of Box4d.
	 ** \return reference to the last_back_bottom_right Point4d of the Box4d
	 */
	void last_back_bottom_right(slip::Point<CoordType,4>);
	/*!
	 ** \brief Accessor/Mutator of the last_back_bottom_right point (p2) of Box4d.
	 ** \return reference to the last_back_bottom_right Point4d of the Box4d
	 */
	slip::Point<CoordType,4>& last_back_bottom_right();
	/*!
	 ** \brief Accessor/Mutator of the last_back_bottom_right point (p2) of Box4d.
	 ** \return reference to the last_back_bottom_right Point4d of the Box4d
	 */
	const slip::Point<CoordType,4>& last_back_bottom_right() const;

	/*!
	 ** \brief Mutator of the coordinates of the %Box4d.
	 ** \param x11 first coordinate of the minimal point p1
	 ** \param x12 second coordinate of the minimal point p1
	 ** \param x13 third coordinate of the minimal point p1
	 ** \param x14 fourth coordinate of the minimal point p1
	 ** \param x21 first coordinate of the maximal point p2
	 ** \param x22 second coordinate of the maximal point p2
	 ** \param x23 third coordinate of the maximal point p2
	 ** \param x24 fourth coordinate of the maximal point p2
	 ** \return
	 */
	void set_coord(const CoordType& x11,
			const CoordType& x12,
			const CoordType& x13,
			const CoordType& x14,
			const CoordType& x21,
			const CoordType& x22,
			const CoordType& x23,
			const CoordType& x24);

	/*@} End Element access operators */

	/*!
	 ** \brief Returns the name of the class
	 **
	 */
	std::string name() const;


	/*!
	 ** \brief compute the hypervolume of the Box4d.
	 ** \return the hypervolume
	 */
	CoordType hypervolume() const;

	/*!
	 ** \brief compute the duration of the Box4d (first dimension size).
	 ** \return the duration
	 */
	CoordType duration() const;

	/*!
	 ** \brief compute the depth of the Box4d (second dimension size).
	 ** \return the depth
	 */
	CoordType depth() const;

	/*!
	 ** \brief compute the height of the Box4d  (third dimension size).
	 ** \return the height
	 */
	CoordType height() const;

	/*!
	 ** \brief compute the width of the Box4d (fourth dimension size).
	 ** \return the width
	 */
	CoordType width() const;

 private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
       if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::Box<CoordType,4> >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::Box<CoordType,4> >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()

};
}//slip::

namespace slip
{

template<typename CoordType>
inline
Box4d<CoordType>::Box4d():
slip::Box<CoordType,4>()
{}

template<typename CoordType>
inline
Box4d<CoordType>::Box4d(const slip::Point<CoordType,4>& p1,
		const slip::Point<CoordType,4>& p2):
		slip::Box<CoordType,4>(p1,p2)
		{}

template<typename CoordType>
inline
Box4d<CoordType>::Box4d(const CoordType& x11,
		const CoordType& x12,
		const CoordType& x13,
		const CoordType& x14,
		const CoordType& x21,
		const CoordType& x22,
		const CoordType& x23,
		const CoordType& x24):
		slip::Box<CoordType,4>(Point4d<CoordType>(x11,x12,x13,x14),Point4d<CoordType>(x21,x22,x23,x24))
		{}

template<typename CoordType>
inline
Box4d<CoordType>::Box4d(const CoordType& xc,
		const CoordType& yc,
		const CoordType& zc,
		const CoordType& lc,
		const CoordType& r):
		slip::Box<CoordType,4>(Point4d<CoordType>(xc-r,yc-r,zc-r,lc-r),Point4d<CoordType>(xc+r,yc+r,zc+r,lc+r))
		{}

template<typename CoordType>
inline
Box4d<CoordType>::Box4d(const slip::Point<CoordType,4>& pc,
		const CoordType& r):
		slip::Box<CoordType,4>(Point4d<CoordType>(pc[0]-r,pc[1]-r,pc[2]-r,pc[3]-r),
				Point4d<CoordType>(pc[0]+r,pc[1]+r,pc[2]+r,pc[3]+r))
				{}


template<typename CoordType>
inline
Box4d<CoordType>::Box4d(const slip::Point<CoordType,4>& pc,
		const CoordType& r1,
		const CoordType& r2,
		const CoordType& r3,
		const CoordType& r4):
		slip::Box<CoordType,4>(Point4d<CoordType>(pc[0]-r1,pc[1]-r2,pc[2]-r3,pc[3]-r4),
				Point4d<CoordType>(pc[0]+r1,pc[1]+r2,pc[2]+r3,pc[3]+r4))
				{}


template<typename CoordType>
inline
void Box4d<CoordType>::first_front_upper_left(slip::Point<CoordType,4> p1) {this->p1_=p1;}

template<typename CoordType>
inline
slip::Point<CoordType,4>& Box4d<CoordType>::first_front_upper_left() {return this->p1_;}

template<typename CoordType>
inline
const slip::Point<CoordType,4>& Box4d<CoordType>::first_front_upper_left() const {return this->p1_;}

template<typename CoordType>
inline
void Box4d<CoordType>::last_back_bottom_right(slip::Point<CoordType,4> p2) {this->p2_=p2;}

template<typename CoordType>
inline
slip::Point<CoordType,4>& Box4d<CoordType>::last_back_bottom_right() {return this->p2_;}

template<typename CoordType>
inline
const slip::Point<CoordType,4>& Box4d<CoordType>::last_back_bottom_right() const {return this->p2_;}

template<typename CoordType>
inline
void Box4d<CoordType>::set_coord(const CoordType& x11,
		const CoordType& x12,
		const CoordType& x13,
		const CoordType& x14,
		const CoordType& x21,
		const CoordType& x22,
		const CoordType& x23,
		const CoordType& x24)
		{
	this->p1_[0] = x11;
	this->p1_[1] = x12;
	this->p1_[2] = x13;
	this->p1_[3] = x14;
	this->p2_[0] = x21;
	this->p2_[1] = x22;
	this->p2_[2] = x23;
	this->p2_[3] = x24;
		}


template<typename T>
inline
std::string
Box4d<T>::name() const {return "Box4d";}


template<typename CoordType>
inline
CoordType Box4d<CoordType>::hypervolume() const {return duration() * width() * height() * depth();}

template<typename CoordType>
inline
CoordType Box4d<CoordType>::width() const {return (this->p2_[3] - this->p1_[3]+1);}

template<typename CoordType>
inline
CoordType Box4d<CoordType>::height() const {return (this->p2_[2] - this->p1_[2]+1);}

template<typename CoordType>
inline
CoordType Box4d<CoordType>::depth() const {return (this->p2_[1] - this->p1_[1]+1);}

template<typename CoordType>
inline
CoordType Box4d<CoordType>::duration() const {return (this->p2_[0] - this->p1_[0]+1);}

}//slip::

#endif //SLIP_BOX4D_HPP
