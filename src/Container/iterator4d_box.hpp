/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/*!
 ** \file iterator4d_box.hpp
 **
 ** \brief Provides a class to manipulate iterator4d within a slip::Box4d.
 ** It is used to iterate throw 4d containers.
 ** \version Fluex 1.0
 ** \date 2013/07/04
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 **
 */

#ifndef SLIP_ITERATOR4D_BOX_HPP
#define SLIP_ITERATOR4D_BOX_HPP

#include <iterator>
#include <cassert>

#include "DPoint2d.hpp"
#include "DPoint3d.hpp"
#include "DPoint4d.hpp"
#include "Point4d.hpp"
#include "Box4d.hpp"
#include "iterator_types.hpp"

#define INCR_OPT

namespace slip
{
/*!
 ** @ingroup Containers
 ** \class iterator4d_box
 ** \version Fluex 1.0
 ** \date 2013/07/04
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief This is some iterator to iterate a 4d container into a %Box area
 **        defined by the subscripts of the 4d container.
 ** \param Container4D Type of 4d container in the iterator4d_box
 **
 ** \par Description:
 **      This iterator is an 4d extension of the random_access_iterator.
 **      It is compatible with the bidirectional_iterator of the
 **      Standard library. It iterate into a box area defined inside the
 **      subscripts range of the 4d container. Those subscripts are defined as
 **      follows :
 ** \image html iterator4d_conventions.jpg "axis and notation conventions"
 ** \image latex iterator4d_conventions.eps "axis and notation conventions" width=5cm
 */
template <class Container4D>
class iterator4d_box
{
public:
	//typedef std::random_access_iterator_tag iterator_category;
	typedef std::random_access_iterator4d_tag iterator_category;
	typedef typename Container4D::value_type value_type;
	typedef DPoint4d<int> difference_type;
	typedef slip::DPoint3d<int> diff3d;
	typedef slip::DPoint2d<int> diff2d;
	typedef typename Container4D::pointer pointer;
	typedef typename Container4D::reference reference;

	typedef iterator4d_box self;
	typedef typename Container4D::size_type size_type;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** \brief Constructs a %iterator4d_box.
	 ** \par The box to iterate is in this case the full container
	 */
	iterator4d_box():
		cont4d_(0),pos_(0),x1_(0),x2_(0),x3_(0),x4_(0),box_(0,0,0,0,0,0,0,0)
	{}

	/*!
	 ** \brief Constructs a %iterator4d_box.
	 ** \param c pointer to an existing 4d container
	 ** \param b Box4d<int> defining the range of subscripts to iterate
	 ** \pre box subscripts must be inside those of the 4d container
	 */
	iterator4d_box(Container4D* c,
			const Box4d<int>& b):
				cont4d_(c),pos_((*c)[(b.first_front_upper_left())[0]][(b.first_front_upper_left())[1]][(b.first_front_upper_left())[2]]
				                                                                                       + (b.first_front_upper_left())[3]),
				                                                                                       x1_((b.first_front_upper_left())[0]),x2_((b.first_front_upper_left())[1]),x3_((b.first_front_upper_left())[2]),
				                                                                                       x4_((b.first_front_upper_left())[3]),box_(b)
	{}

	/*!
	 ** \brief Constructs a copy of the %iterator4d_box \a o
	 **
	 ** \param o %iterator4d_box to copy.
	 */
	iterator4d_box(const self& o):
		cont4d_(o.cont4d_),pos_(o.pos_),x1_(o.x1_),x2_(o.x2_),x3_(o.x3_),x4_(o.x4_),box_(o.box_)
	{}

	/*@} End Constructors */

	/**
	 ** \name  Assignment operators and methods
	 */
	/*@{*/
	/*!
	 ** \brief Assign a %iterator4d_box.
	 **
	 ** Assign elements of %iterator4d_box in \a o.
	 **
	 ** \param o %iterator4d_box to get the values from.
	 ** \return a %iterator4d_box reference.
	 */
	self& operator=(const self& o)
	{
		if(this != &o)
		{
			this->cont4d_ = o.cont4d_;
			this->pos_ = o.pos_;
			this->x1_ = o.x1_;
			this->x2_ = o.x2_;
			this->x3_ = o.x3_;
			this->x4_ = o.x4_;
			this->box_ = o.box_;
		}
		return *this;
	}
	/*@} End Assignment operators and methods*/

	/*!
	 ** \brief Dereference assignment operator. Returns the element
	 **        that the current %iterator4d_box i point to.
	 **
	 ** \return a %iterator4d_box reference.
	 */
	inline
	reference operator*()
	{
		return *pos_;
	}



	/*!
	 ** \brief Dereference operator.  Returns the element
	 **        that the current %iterator4d_box i point to.
	 **
	 ** \return a const %iterator4d_box reference.
	 */
	inline
	const reference operator*() const
	{
		return *pos_;
	}


	inline
	pointer operator->() const
	{
		return &(operator*());
	}

	inline
	pointer operator->()
	{
		return &(operator*());
	}

	/**
	 ** \name  Forward operators addons
	 */
	/*@{*/

	/*!
	 ** \brief Preincrement a %iterator4d_box. Iterate to the next location
	 **        inside the %Box4d.
	 **
	 */
	inline
	self& operator++()
	{
		if(x4_ < (box_.last_back_bottom_right())[3])
		{
			this->x4_++;
			this->pos_++;
		}
		else if(x3_ < (box_.last_back_bottom_right())[2])
		{
			this->x3_++;
			this->x4_ = (box_.first_front_upper_left())[3];
#ifdef INCR_OPT
this->pos_+= static_cast<int>(cont4d_->cols()) - box_.width() + 1 ;
#else
this->pos_ = cont4d_->begin() +
		(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x3_* static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}
		else if(x2_ < (box_.last_back_bottom_right())[1])
		{
			this->x2_++;
			this->x3_ = (box_.first_front_upper_left())[2];
			this->x4_ = (box_.first_front_upper_left())[3];
#ifdef INCR_OPT
			this->pos_+= static_cast<int>(cont4d_->cols())*(static_cast<int>(cont4d_->rows()) - box_.height() + 1)
                                        		  - box_.width() + 1 ;
#else
			this->pos_ = cont4d_->begin() +
					(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
					(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
					(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}

		else if(x1_ < (box_.last_back_bottom_right())[0])
		{
			this->x1_++;
			this->x2_ = (box_.first_front_upper_left())[1];
			this->x3_ = (box_.first_front_upper_left())[2];
			this->x4_ = (box_.first_front_upper_left())[3];
#ifdef INCR_OPT
this->pos_+= static_cast<int>(cont4d_->cols())*(static_cast<int>(cont4d_->rows()) *
		(static_cast<int>(cont4d_->slices()) - box_.depth() + 1)
		- box_.height() + 1) - box_.width() + 1 ;
#else
	this->pos_ = cont4d_->begin() +
			(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
			(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
			(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}
		else
		{
			this->x1_ = (box_.last_back_bottom_right())[0] + 1;
			this->x2_ = (box_.last_back_bottom_right())[1] + 1;
			this->x3_ = (box_.last_back_bottom_right())[2] + 1;
			this->x4_ = (box_.last_back_bottom_right())[3] + 1;
#ifdef INCR_OPT
this->pos_ += static_cast<int>(cont4d_->cols()) * (static_cast<int>(cont4d_->rows()) *
		(static_cast<int>(cont4d_->slices()) + 1) + 1) + 1;
#else
this->pos_ = cont4d_->begin() +
		(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}
		return *this;
	}

	/*!
	 ** \brief Postincrement a %iterator4d_box. Iterate to the next location
	 **        inside the %Box4d.
	 **
	 */
	inline
	self operator++(int)
	{
		self tmp = *this;
		++(*this);
		return tmp;
	}

	/*@} End Forward operators addons */

	/**
	 ** \name  Bidirectional operators addons
	 */
	/*@{*/
	/*!
	 ** \brief Predecrement a %iterator4d_box. Iterate to the previous location
	 **        inside the %Box4d.
	 **
	 */
	inline
	self& operator--()
	{
		if(x4_ > (box_.first_front_upper_left())[3])
		{
			this->x4_--;
			this->pos_--;
		}
		else if (x3_ > (box_.first_front_upper_left())[2])
		{
			this->x3_--;
			this->x4_ = (box_.last_back_bottom_right())[3];
#ifdef INCR_OPT
this->pos_-= (static_cast<int>(cont4d_->cols()) - box_.width() + 1) ;
#else
this->pos_ = cont4d_->begin() +
		(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}
		else if (x2_ > (box_.first_front_upper_left())[1])
		{
			this->x2_--;
			this->x3_ = (box_.last_back_bottom_right())[2];
			this->x4_ = (box_.last_back_bottom_right())[3];
#ifdef INCR_OPT
			this->pos_-= (static_cast<int>(cont4d_->cols())*(static_cast<int>(cont4d_->rows()) - box_.height() + 1)
					- box_.width() + 1) ;
#else
			this->pos_ = cont4d_->begin() +
					(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
					(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
					(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}
		else if (x1_ > (box_.first_front_upper_left())[0])
		{
			this->x1_--;
			this->x2_ = (box_.last_back_bottom_right())[1];
			this->x3_ = (box_.last_back_bottom_right())[2];
			this->x4_ = (box_.last_back_bottom_right())[3];
#ifdef INCR_OPT
this->pos_ -= (static_cast<int>(cont4d_->cols())*(static_cast<int>(cont4d_->rows()) *
		(static_cast<int>(cont4d_->slices()) - box_.depth() + 1)
		- box_.height() + 1) - box_.width() + 1) ;
#else
	this->pos_ = cont4d_->begin() +
			(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
			(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
			(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}
		else
		{
			this->x1_ = (box_.first_front_upper_left())[0] - 1;
			this->x2_ = (box_.first_front_upper_left())[1] - 1;
			this->x3_ = (box_.first_front_upper_left())[2] - 1;
			this->x4_ = (box_.first_front_upper_left())[3] - 1;
#ifdef INCR_OPT
this->pos_ -= static_cast<int>(cont4d_->cols()) * (static_cast<int>(cont4d_->rows()) *
		(static_cast<int>(cont4d_->slices()) + 1) + 1) + 1;
#else
this->pos_ = cont4d_->begin() +
		(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}

		return *this;
	}

	/*!
	 ** \brief Postdecrement a %iterator4d_box. Iterate to the previous location
	 **        inside the %Box4d.
	 **
	 */
	inline
	self operator--(int)
	{
		self tmp = *this;
		--(*this);
		return tmp;
	}

	/*@} End Bidirectional operators addons */

	/**
	 ** \name  Equality comparable operators
	 */
	/*@{*/
	/*!
	 ** \brief Equality operator.
	 ** \param i1 first %iterator4d_box.
	 ** \param i2 second %iterator4d_box.
	 ** \return true if i1 and i2 point to the same element of
	 **         the 4d container
	 */
	inline
	friend bool operator==(const self& i1,
			const self& i2)
			{

		return ( (i1.cont4d_ == i2.cont4d_) && (i1.pos_ == i2.pos_)
				&& (i1.x1_ == i2.x1_) && (i1.x2_ == i2.x2_) && (i1.x3_ == i2.x3_) && (i1.x4_ == i2.x4_));
			}

	/*!
	 ** \brief Inequality operator.
	 ** \param i1 first %iterator4d_box.
	 ** \param i2 second %iterator4d_box.
	 ** \return true if !(i1 == i2)
	 */
	inline
	friend bool operator!=(const self& i1,
			const self& i2)
			{
		return ( (i1.cont4d_ != i2.cont4d_) || (i1.pos_ != i2.pos_)
				|| (i1.x1_ != i2.x1_) || (i1.x2_ != i2.x2_) || (i1.x3_ != i2.x3_) || (i1.x4_ != i2.x4_));
			}
	/*@} End Equality comparable operators*/

	/**
	 ** \name  Strict Weakly comparable operators
	 */
	/*@{*/
	/*!
	 ** \brief < operator.
	 ** \param i1 first %iterator4d_box.
	 ** \param i2 second %iterator4d_box.
	 ** \return true i1.pos_ < i2_pos
	 */
	inline
	friend bool operator<(const self& i1,
			const self& i2)
	{
		return ( i1.pos_ < i2.pos_);
	}

	/*!
	 ** \brief > operator.
	 ** \param i1 first %iterator4d_box.
	 ** \param i2 second %iterator4d_box.
	 ** \return true i2 < i1
	 */
	inline
	friend bool operator>(const self& i1,
			const self& i2)
	{
		return (i2 < i1);
	}

	/*!
	 ** \brief <= operator.
	 ** \param i1 first %iterator4d_box.
	 ** \param i2 second %iterator4d_box.
	 ** \return true if !(i2 < i1)
	 */
	inline
	friend bool operator<=(const self& i1, const self& i2)
	{
		return  !(i2 < i1);
	}

	/*!
	 ** \brief >= operator.
	 ** \param i1 first %iterator4d_box.
	 ** \param i2 second %iterator4d_box.
	 ** \return true if !(i1 < i2)
	 */
	inline
	friend bool operator>=(const self& i1, const self& i2)
	{
		return  !(i1 < i2);
	}
	/*@} End  Strict Weakly comparable operators*/


	/**
	 ** \name  iterator4d_box operators addons
	 */
	/*@{*/
	/*!
	 ** \brief iterator4d_box addition.
	 ** \param d difference_type
	 ** \return a iterator4d_box reference
	 ** \pre All the iterators between the current iterator i and i + d
	 **      must be dereferenceable.
	 */
	inline
	self& operator+=(const difference_type& d)
	{
		this->x1_ += d.dx1();
		this->x2_ += d.dx2();
		this->x3_ += d.dx3();
		this->x4_ += d.dx4();
		this->pos_ = cont4d_->begin() +
				(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
				(this->x2_ * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
				(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
		return *this;
	}



	/*!
	 ** \brief iterator4d_box substraction.
	 ** \param d difference_type
	 ** \return a iterator4d_box reference
	 ** \pre All the iterators between the current iterator i and i - d
	 **      must be dereferenceable.
	 */
	inline
	self& operator-=(const difference_type& d)
	{
		this->x1_ -= d.dx1();
		this->x2_ -= d.dx2();
		this->x3_ -= d.dx3();
		this->x4_ -= d.dx4();
		this->pos_ = cont4d_->begin() +
				(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
				(this->x2_ * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
				(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
		return *this;
	}



	/*!
	 ** \brief iterator4d_box addition.
	 ** \param d difference_type
	 ** \return a iterator4d_box reference
	 ** \pre All the iterators between the current iterator i and i + d
	 **      must be dereferenceable.
	 */
	inline
	self operator+(const difference_type& d)
	{
		self tmp = *this;
		tmp += d;
		return tmp;
	}

	/*!
	 ** \brief iterator4d_box substraction.
	 ** \param d difference_type
	 ** \return a iterator4d_box reference
	 ** \pre All the iterators between the current iterator i and i - d
	 **      must be dereferenceable.
	 */
	inline
	self operator-(const difference_type& d)
	{
		self tmp = *this;
		tmp -= d;
		return tmp;
	}

	/*!
	 ** \brief %iterator4d_box difference operator.
	 ** \param i1 first %iterator4d_box.
	 ** \param i2 second %iterator4d_box.
	 ** \return a difference_type d such that i1 = i2 + d.
	 ** \pre Either i1 is reachable from i2, or i2 is reachable from i1.
	 */
	inline
	friend difference_type operator-(const self& i1,
			const self& i2)
	{
		return difference_type(int(i1.x1_ - i2.x1_),int(i1.x2_ - i2.x2_),int(i1.x3_ - i2.x3_),int(i1.x4_ - i2.x4_));
	}


	/*!
	 ** \brief %iterator4d_box element assignment operator.
	 **        Equivalent to *(k + d) = t.
	 ** \param d difference_type.
	 ** \return a reference to the 4d container elements
	 ** \pre (k + d) exists and is dereferenceable.
	 ** \post k[d] is a copy of reference.
	 */
	inline
	reference operator[](difference_type d)
	{
		return (*cont4d_)[this->x1_+d.dx1()][this->x2_+d.dx2()][this->x3_+d.dx3()][this->x4_+d.dx4()];
	}

	/*!
	 ** \brief %iterator4d_box element assignment operator.
	 **        Equivalent to *(k + d) = t.
	 ** \param d offset.
	 ** \return a pointer to the 4d container elements
	 ** \pre (k + d) exists and is dereferenceable.
	 ** \post k[d] is a copy of pointer.
	 ** \todo add assert
	 */
	inline
	pointer operator[](diff3d d)
	{
		return (*cont4d_)[this->x1_+ d.dx1()][this->x2_ + d.dx2()][this->x3_ + d.dx3()];
	}

	/*!
	 ** \brief %iterator4d_box element assignment operator.
	 **        Equivalent to *(k + d) = t.
	 ** \param d offset.
	 ** \return a pointer to the 4d container elements
	 ** \pre (k + d) exists and is dereferenceable.
	 ** \post k[d] is a copy of pointer.
	 ** \todo add assert
	 */
	inline
	pointer* operator[](diff2d d)
	{
		return (*cont4d_)[this->x1_+ d.dx1()][this->x2_ + d.dx2()];
	}

	/*!
	 ** \brief %iterator4d_box element assignment operator.
	 **        Equivalent to *(k + n).
	 ** \param n offset.
	 ** \return a pointer of pointer to the 4d container elements
	 ** \pre (k + n) exists and is dereferenceable.
	 ** \post k[n] is a copy of pointer of pointer.
	 ** \todo add assert
	 */
	inline
	pointer** operator[](int n)
	{
		return (*cont4d_)[this->x1_+ n];
	}



	/*!
	 ** \brief %iterator4d_box element assignment operator.
	 **        Equivalent to *(k + n).
	 ** \param n offset.
	 ** \return a const pointer of pointer to the 4d container elements
	 ** \pre (k + n) exists and is dereferenceable.
	 ** \todo add assert
	 */
	inline
	const pointer** operator[](int n) const
	{
		return (*cont4d_)[this->x1_+ n];
	}

	/*!
	 ** \brief %iterator4d_box element assignment operator.
	 ** \param slice offset.
	 ** \param row offset.
	 ** \param col offset
	 ** \return a slab_iterator to the first element of the
	 ** line defined in the container by (slice,row,col) indices.
	 ** \todo add assert
	 */
	inline
	typename Container4D::slab_iterator slab_begin(size_type slice, size_type row, size_type col)
	{
		return cont4d_->slab_begin(this->box_.first_front_upper_left()[1] + slice, this->box_.first_front_upper_left()[2] + row,
				this->box_.first_front_upper_left()[3] + col) +  this->box_.first_front_upper_left()[0];
	}

	/*!
	 ** \brief %iterator4d_box element assignment operator.
	 ** \param slice offset.
	 ** \param row row offset.
	 ** \param col col offset.
	 ** \return a slab_iterator to the past-the-end element of the
	 ** line defined in the container by (slice,row,col) indices.
	 ** \todo add assert
	 */
	inline
	typename Container4D::slab_iterator slab_end(size_type slice, size_type row, size_type col)
	{
		return this->slab_begin(slice,row,col) + this->box_.duration();
	}


	/*!
	 ** \brief %iterator4d_box element assignment operator.
	 ** \param slab offset.
	 ** \param row offset.
	 ** \param col offset
	 ** \return a slice_iterator to the first element of the
	 ** line defined in the container by (slab,row,col) indices.
	 ** \todo add assert
	 */
	inline
	typename Container4D::slice_iterator slice_begin(size_type slab, size_type row, size_type col)
	{
		return cont4d_->slice_begin(this->box_.first_front_upper_left()[0] + slab, this->box_.first_front_upper_left()[2] + row,
				this->box_.first_front_upper_left()[3] + col) + this->box_.first_front_upper_left()[1];
	}

	/*!
	 ** \brief %iterator4d_box element assignment operator.
	 ** \param slab offset.
	 ** \param row row offset.
	 ** \param col col offset.
	 ** \return a slice_iterator to the past-the-end element of the
	 ** line defined in the container by (slab,row,col) indices.
	 ** \todo add assert
	 */
	inline
	typename Container4D::slice_iterator slice_end(size_type slab, size_type row, size_type col)
	{
		return this->slice_begin(slab,row,col) + this->box_.depth();
	}


	/*!
	 ** \brief %iterator4d_box element assignment operator.
	 ** \param slab offset.
	 ** \param slice slice offset.
	 ** \param row offset.
	 ** \return a row_iterator to the first element of the
	 ** line defined in the container by (slab,slice,row) indices.
	 ** \note Be careful, row_iterators and col_iterators are iterating threw a row and a column respectively.
	 ** It is not the case with slab and slice iterators which are iterating cross slabs and slices respectively.
	 ** \todo add assert
	 */
	inline
	typename Container4D::row_iterator row_begin(size_type slab, size_type slice, size_type row)
	{
		return cont4d_->row_begin(this->box_.first_front_upper_left()[0] + slab, this->box_.first_front_upper_left()[1] + slice,
				this->box_.first_front_upper_left()[2] + row) + this->box_.first_front_upper_left()[3];
	}

	/*!
	 ** \brief %iterator4d_box element assignment operator.
	 ** \param slab offset.
	 ** \param slice slice offset.
	 ** \param row offset.
	 ** \return a row_iterator to the past-the-end element of the
	 ** line defined in the container by (slab,slice,row) indices.
	 ** \note Be careful, row_iterators and col_iterators are iterating threw a row and a column respectively.
	 ** It is not the case with slab and slice iterators which are iterating cross slabs and slices respectively.
	 ** \todo add assert
	 */
	inline
	typename Container4D::row_iterator row_end(size_type slab, size_type slice, size_type row)
	{
		return this->row_begin(slab,slice,row) + this->box_.width();
	}

	/*!
	 ** \brief %iterator4d_box element assignment operator.
	 ** \param slab offset.
	 ** \param slice slice offset.
	 ** \param col col offset.
	 ** \return a col_iterator to the first element of the
	 ** line defined in the container by (slab,slice,col) indices.
	 ** \note Be careful, row_iterators and col_iterators are iterating threw a row and a column respectively.
	 ** It is not the case with slab and slice iterators which are iterating cross slabs and slices respectively.
	 ** \todo add assert
	 */
	inline
	typename Container4D::col_iterator col_begin(size_type slab, size_type slice, size_type col)
	{
		return cont4d_->col_begin(this->box_.first_front_upper_left()[0] + slab, this->box_.first_front_upper_left()[1] + slice,
				this->box_.first_front_upper_left()[3] + col) +  this->box_.first_front_upper_left()[2];
	}

	/*!
	 ** \brief %iterator4d_box element assignment operator.
	 ** \param slab offset.
	 ** \param slice slice offset.
	 ** \param col col offset.
	 ** \return a col_iterator to the past-the-end element of the
	 ** line defined in the container by (slab,slice,col) indices.
	 ** \note Be careful, row_iterators and col_iterators are iterating threw a row and a column respectively.
	 ** It is not the case with slab and slice iterators which are iterating cross slabs and slices respectively.
	 ** \todo add assert
	 */
	inline
	typename Container4D::col_iterator col_end(size_type slab, size_type slice, size_type col)
	{
		return this->col_begin(slab,slice,col) + this->box_.height();
	}

	/*@} End  iterator4d_box operators addons */


	/**
	 ** \name Subscripts accessors
	 */
	/*@{*/
	/*!
	 ** \brief Access to the first subscript of the current %iterator4d_box.
	 ** \return the first subscript.
	 */
	inline
	int x1() const
	{
		return this->x1_;
	}
	/*!
	 ** \brief Access to the first subscript of the current %iterator4d_box.
	 ** \return the first subscript.
	 */
	inline
	int t() const
	{
		return this->x1_;
	}
	/*!
	 ** \brief Access to the second subscript of the current %iterator4d_box.
	 ** \return the second subscript.
	 */
	inline
	int x2() const
	{
		return this->x2_;
	}
	/*!
	 ** \brief Access to the second subscript of the current %iterator4d_box.
	 ** \return the second subscript.
	 */
	inline
	int k() const
	{
		return this->x2_;
	}
	/*!
	 ** \brief Access to the third subscript of the current %iterator4d_box.
	 ** \return the third subscript.
	 */
	inline
	int x3() const
	{
		return this->x3_;
	}
	/*!
	 ** \brief Access to the third subscript of the current %iterator4d_box.
	 ** \return the third subscript.
	 */
	inline
	int i() const
	{
		return this->x3_;
	}

	/*!
	 ** \brief Access to the fourth subscript of the current %iterator4d_box.
	 ** \return the fourth subscript.
	 */
	inline
	int x4() const
	{
		return this->x4_;
	}
	/*!
	 ** \brief Access to the fourth subscript of the current %iterator4d_box.
	 ** \return the fourth subscript.
	 */
	inline
	int j() const
	{
		return this->x4_;
	}

	/*@} End  Subscripts accessors */



private:
	Container4D* cont4d_; // pointer to the 4d container
	pointer pos_;         // linear position within the container
	int x1_;              // first subscript position
	int x2_;              // second subscript position
	int x3_;              // third subscript position
	int x4_;              // fourth subscript position
	Box4d<int> box_;      // box to iterate
};


/*!
 ** @ingroup Containers
 ** \class const_iterator4d_box
 ** \version Fluex 1.0
 ** \date 2013/07/04
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief This is some iterator to iterate a 4d container into a %Box area
 **        defined by the subscripts of the 4d container.
 ** \param Container4D Type of 4d container in the const_iterator4d_box
 **
 ** \par Description:
 **      This iterator is an 4d extension of the random_access_iterator.
 **      It is compatible with the bidirectional_iterator of the
 **      Standard library. It iterate into a box area defined inside the
 **      subscripts range of the 4d container. Those subscripts are defined as
 **      follows :
 **
 ** \image html iterator4d_conventions.jpg "axis and notation conventions"
 ** \image latex iterator4d_conventions.eps "axis and notation conventions" width=5cm
 */


template <class Container4D>
class const_iterator4d_box
{
public:
	//typedef std::random_access_iterator_tag iterator_category;
	typedef std::random_access_iterator4d_tag iterator_category;
	typedef typename Container4D::value_type value_type;
	typedef slip::DPoint2d<int> diff2d;
	typedef slip::DPoint3d<int> diff3d;
	typedef DPoint4d<int> difference_type;
	typedef typename Container4D::const_pointer pointer;
	typedef typename Container4D::const_reference reference;

	typedef const_iterator4d_box self;

	typedef typename Container4D::size_type size_type;
	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** \brief Constructs a %const_iterator4d_box.
	 ** \par The box to iterate is in this case the full container
	 */
	const_iterator4d_box():
		cont4d_(0),pos_(0),x1_(0),x2_(0),x3_(0),x4_(0),box_(0,0,0,0,0,0,0,0)
	{}

	/*!
	 ** \brief Constructs a %const_iterator4d_box.
	 ** \param c pointer to an existing 4d container
	 ** \param b Box4d<int> defining the range of subscripts to iterate
	 ** \pre box subscripts must be inside those of the 4d container
	 */
	const_iterator4d_box(Container4D* c,
			const Box4d<int>& b):
				cont4d_(c),pos_((*c)[(b.first_front_upper_left())[0]]
				                     [(b.first_front_upper_left())[1]]
				                      [(b.first_front_upper_left())[2]]
				                       + (b.first_front_upper_left())[3]),
				                       x1_((b.first_front_upper_left())[0]),
				                       x2_((b.first_front_upper_left())[1]),
				                       x3_((b.first_front_upper_left())[2]),
				                       x4_((b.first_front_upper_left())[3]),box_(b)
	{}

	/*!
	 ** \brief Constructs a copy of the %const_iterator4d_box \a o
	 **
	 ** \param o %const_iterator4d_box to copy.
	 */
	const_iterator4d_box(const self& o):
		cont4d_(o.cont4d_),pos_(o.pos_),x1_(o.x1_),x2_(o.x2_),x3_(o.x3_),x4_(o.x4_),box_(o.box_)
	{}

	/*@} End Constructors */

	/**
	 ** \name  Assignment operators and methods
	 */
	/*@{*/
	/*!
	 ** \brief Assign a %const_iterator4d_box.
	 **
	 ** Assign elements of %const_iterator4d_box in \a o.
	 **
	 ** \param o %const_iterator4d_box to get the values from.
	 ** \return a %const_iterator4d_box reference.
	 */
	self& operator=(const self& o)
	{
		if(this != &o)
		{
			this->cont4d_ = o.cont4d_;
			this->pos_ = o.pos_;
			this->x1_ = o.x1_;
			this->x2_ = o.x2_;
			this->x3_ = o.x3_;
			this->x4_ = o.x4_;
			this->box_ = o.box_;
		}
		return *this;
	}
	/*@} End Assignment operators and methods*/

	/*!
	 ** \brief Dereference assignment operator. Returns the element
	 **        that the current %iterator4d_box i point to.
	 **
	 ** \return a %iterator4d_box reference.
	 */
	inline
	reference operator*() const
	{
		return *pos_;
	}

	inline
	pointer operator->() const
	{
		return &(operator*());
	}

	/**
	 ** \name  Forward operators addons
	 */
	/*@{*/

	/*!
	 ** \brief Preincrement a %const_iterator4d_box. Iterate to the next location
	 **        inside the %Box4d.
	 **
	 */
	inline
	self& operator++()
	{
		if(x4_ < (box_.last_back_bottom_right())[3])
		{
			this->x4_++;
			this->pos_++;
		}
		else if(x3_ < (box_.last_back_bottom_right())[2])
		{
			this->x3_++;
			this->x4_ = (box_.first_front_upper_left())[3];
#ifdef INCR_OPT
this->pos_+= static_cast<int>(cont4d_->cols()) - box_.width() + 1 ;
#else
this->pos_ = cont4d_->begin() +
		(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x3_* static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}
		else if(x2_ < (box_.last_back_bottom_right())[1])
		{
			this->x2_++;
			this->x3_ = (box_.first_front_upper_left())[2];
			this->x4_ = (box_.first_front_upper_left())[3];
#ifdef INCR_OPT
			this->pos_+= static_cast<int>(cont4d_->cols())*(static_cast<int>(cont4d_->rows()) - box_.height() + 1)
                                            		  - box_.width() + 1 ;
#else
			this->pos_ = cont4d_->begin() +
					(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
					(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
					(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}

		else if(x1_ < (box_.last_back_bottom_right())[0])
		{
			this->x1_++;
			this->x2_ = (box_.first_front_upper_left())[1];
			this->x3_ = (box_.first_front_upper_left())[2];
			this->x4_ = (box_.first_front_upper_left())[3];
#ifdef INCR_OPT
this->pos_+= static_cast<int>(cont4d_->cols())*(static_cast<int>(cont4d_->rows()) *
		(static_cast<int>(cont4d_->slices()) - box_.depth() + 1)
		- box_.height() + 1) - box_.width() + 1 ;
#else
	this->pos_ = cont4d_->begin() +
			(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
			(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
			(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}
		else
		{
			this->x1_ = (box_.last_back_bottom_right())[0] + 1;
			this->x2_ = (box_.last_back_bottom_right())[1] + 1;
			this->x3_ = (box_.last_back_bottom_right())[2] + 1;
			this->x4_ = (box_.last_back_bottom_right())[3] + 1;
#ifdef INCR_OPT
this->pos_ += static_cast<int>(cont4d_->cols()) * (static_cast<int>(cont4d_->rows()) *
		(static_cast<int>(cont4d_->slices()) + 1) + 1) + 1;
#else
this->pos_ = cont4d_->begin() +
		(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}
		return *this;
	}

	/*!
	 ** \brief Postincrement a %const_iterator4d_box. Iterate to the next location
	 **        inside the %Box4d.
	 **
	 */
	inline
	self operator++(int)
	{
		self tmp = *this;
		++(*this);
		return tmp;
	}

	/*@} End Forward operators addons */

	/**
	 ** \name  Bidirectional operators addons
	 */
	/*@{*/
	/*!
	 ** \brief Predecrement a %const_iterator4d_box. Iterate to the previous location
	 **        inside the %Box4d.
	 **
	 */
	inline
	self& operator--()
	{
		if(x4_ > (box_.first_front_upper_left())[3])
		{
			this->x4_--;
			this->pos_--;
		}
		else if (x3_ > (box_.first_front_upper_left())[2])
		{
			this->x3_--;
			this->x4_ = (box_.last_back_bottom_right())[3];
#ifdef INCR_OPT
this->pos_-= (static_cast<int>(cont4d_->cols()) - box_.width() + 1) ;
#else
this->pos_ = cont4d_->begin() +
		(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}
		else if (x2_ > (box_.first_front_upper_left())[1])
		{
			this->x2_--;
			this->x3_ = (box_.last_back_bottom_right())[2];
			this->x4_ = (box_.last_back_bottom_right())[3];
#ifdef INCR_OPT
			this->pos_-= (static_cast<int>(cont4d_->cols())*(static_cast<int>(cont4d_->rows()) - box_.height() + 1)
					- box_.width() + 1) ;
#else
			this->pos_ = cont4d_->begin() +
					(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
					(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
					(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}
		else if (x1_ > (box_.first_front_upper_left())[0])
		{
			this->x1_--;
			this->x2_ = (box_.last_back_bottom_right())[1];
			this->x3_ = (box_.last_back_bottom_right())[2];
			this->x4_ = (box_.last_back_bottom_right())[3];
#ifdef INCR_OPT
this->pos_ -= (static_cast<int>(cont4d_->cols())*(static_cast<int>(cont4d_->rows()) *
		(static_cast<int>(cont4d_->slices()) - box_.depth() + 1)
		- box_.height() + 1) - box_.width() + 1) ;
#else
	this->pos_ = cont4d_->begin() +
			(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
			(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
			(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}
		else
		{
			this->x1_ = (box_.first_front_upper_left())[0] - 1;
			this->x2_ = (box_.first_front_upper_left())[1] - 1;
			this->x3_ = (box_.first_front_upper_left())[2] - 1;
			this->x4_ = (box_.first_front_upper_left())[3] - 1;
#ifdef INCR_OPT
this->pos_ -= static_cast<int>(cont4d_->cols()) * (static_cast<int>(cont4d_->rows()) *
		(static_cast<int>(cont4d_->slices()) + 1) + 1) + 1;
#else
this->pos_ = cont4d_->begin() +
		(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x2_* static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
		(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
#endif
		}

		return *this;
	}

	/*!
	 ** \brief Postdecrement a %const_iterator4d_box. Iterate to the previous location
	 **        inside the %Box4d.
	 **
	 */
	inline
	self operator--(int)
	{
		self tmp = *this;
		--(*this);
		return tmp;
	}

	/*@} End Bidirectional operators addons */

	/**
	 ** \name  Equality comparable operators
	 */
	/*@{*/
	/*!
	 ** \brief Equality operator.
	 ** \param i1 first %const_iterator4d_box.
	 ** \param i2 second %const_iterator4d_box.
	 ** \return true if i1 and i2 point to the same element of
	 **         the 4d container
	 */
	inline
	friend bool operator==(const self& i1,
			const self& i2)
			{

		return ( (i1.cont4d_ == i2.cont4d_) && (i1.pos_ == i2.pos_)
				&& (i1.x1_ == i2.x1_) && (i1.x2_ == i2.x2_) && (i1.x3_ == i2.x3_) && (i1.x4_ == i2.x4_));
			}

	/*!
	 ** \brief Inequality operator.
	 ** \param i1 first %const_iterator4d_box.
	 ** \param i2 second %const_iterator4d_box.
	 ** \return true if !(i1 == i2)
	 */
	inline
	friend bool operator!=(const self& i1,
			const self& i2)
			{
		return ( (i1.cont4d_ != i2.cont4d_) || (i1.pos_ != i2.pos_)
				|| (i1.x1_ != i2.x1_) || (i1.x2_ != i2.x2_) || (i1.x3_ != i2.x3_) || (i1.x4_ != i2.x4_));
			}
	/*@} End Equality comparable operators*/

	/**
	 ** \name  Strict Weakly comparable operators
	 */
	/*@{*/
	/*!
	 ** \brief < operator.
	 ** \param i1 first %const_iterator4d_box.
	 ** \param i2 second %const_iterator4d_box.
	 ** \return true i1.pos_ < i2_pos
	 */
	inline
	friend bool operator<(const self& i1,
			const self& i2)
	{
		return ( i1.pos_ < i2.pos_);
	}

	/*!
	 ** \brief > operator.
	 ** \param i1 first %const_iterator4d_box.
	 ** \param i2 second %const_iterator4d_box.
	 ** \return true i2 < i1
	 */
	inline
	friend bool operator>(const self& i1,
			const self& i2)
	{
		return (i2 < i1);
	}

	/*!
	 ** \brief <= operator.
	 ** \param i1 first %const_iterator4d_box.
	 ** \param i2 second %const_iterator4d_box.
	 ** \return true if !(i2 < i1)
	 */
	inline
	friend bool operator<=(const self& i1, const self& i2)
	{
		return  !(i2 < i1);
	}

	/*!
	 ** \brief >= operator.
	 ** \param i1 first %const_iterator4d_box.
	 ** \param i2 second %const_iterator4d_box.
	 ** \return true if !(i1 < i2)
	 */
	inline
	friend bool operator>=(const self& i1, const self& i2)
	{
		return  !(i1 < i2);
	}
	/*@} End  Strict Weakly comparable operators*/


	/**
	 ** \name  const_iterator4d_box operators addons
	 */
	/*@{*/
	/*!
	 ** \brief const_iterator4d_box addition.
	 ** \param d difference_type
	 ** \return a const_iterator4d_box reference
	 ** \pre All the const iterators between the current iterator i and i + d
	 **      must be dereferenceable.
	 */
	inline
	self& operator+=(const difference_type& d)
	{
		this->x1_ += d.dx1();
		this->x2_ += d.dx2();
		this->x3_ += d.dx3();
		this->x4_ += d.dx4();
		this->pos_ = cont4d_->begin() +
				(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
				(this->x2_ * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
				(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
		return *this;
	}



	/*!
	 ** \brief const_iterator4d_box substraction.
	 ** \param d difference_type
	 ** \return a const_iterator4d_box reference
	 ** \pre All the iterators between the current iterator i and i - d
	 **      must be dereferenceable.
	 */
	inline
	self& operator-=(const difference_type& d)
	{
		this->x1_ -= d.dx1();
		this->x2_ -= d.dx2();
		this->x3_ -= d.dx3();
		this->x4_ -= d.dx4();
		this->pos_ = cont4d_->begin() +
				(this->x1_ * static_cast<int>(cont4d_->slices()) * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
				(this->x2_ * static_cast<int>(cont4d_->rows()) * static_cast<int>(cont4d_->cols())) +
				(this->x3_ * static_cast<int>(cont4d_->cols())) + this->x4_;
		return *this;
	}



	/*!
	 ** \brief const_iterator4d_box addition.
	 ** \param d difference_type
	 ** \return a const_iterator4d_box reference
	 ** \pre All the iterators between the current iterator i and i + d
	 **      must be dereferenceable.
	 */
	inline
	self operator+(const difference_type& d)
	{
		self tmp = *this;
		tmp += d;
		return tmp;
	}

	/*!
	 ** \brief const_iterator4d_box substraction.
	 ** \param d difference_type
	 ** \return a const_iterator4d_box reference
	 ** \pre All the iterators between the current iterator i and i - d
	 **      must be dereferenceable.
	 */
	inline
	self operator-(const difference_type& d)
	{
		self tmp = *this;
		tmp -= d;
		return tmp;
	}

	/*!
	 ** \brief %const_iterator4d_box difference operator.
	 ** \param i1 first %const_iterator4d_box.
	 ** \param i2 second %const_iterator4d_box.
	 ** \return a difference_type d such that i1 = i2 + d.
	 ** \pre Either i1 is reachable from i2, or i2 is reachable from i1.
	 */
	inline
	friend difference_type operator-(const self& i1,
			const self& i2)
	{
		return difference_type(int(i1.x1_ - i2.x1_),int(i1.x2_ - i2.x2_),int(i1.x3_ - i2.x3_),int(i1.x4_ - i2.x4_));
	}


	/*!
	 ** \brief %const_iterator4d_box element assignment operator.
	 **        Equivalent to *(k + d) = t.
	 ** \param d difference_type.
	 ** \return a reference to the 4d container elements
	 ** \pre (k + d) exists and is dereferenceable.
	 ** \post k[d] is a copy of reference.
	 */
	inline
	reference operator[](difference_type d) const
	{
		return (*cont4d_)[this->x1_+d.dx1()][this->x2_+d.dx2()][this->x3_+d.dx3()][this->x4_+d.dx4()];
	}

	/*!
	 ** \brief %const_iterator4d_box element assignment operator.
	 **        Equivalent to *(k + d) = t.
	 ** \param d offset.
	 ** \return a pointer to the 4d container elements
	 ** \pre (k + d) exists and is dereferenceable.
	 ** \post k[d] is a copy of pointer.
	 ** \todo add assert
	 */
	inline
	pointer operator[](diff3d d) const
	{
		return (*cont4d_)[this->x1_+ d.dx1()][this->x2_ + d.dx2()][this->x3_ + d.dx3()];
	}

	/*!
	 ** \brief %const_iterator4d_box element assignment operator.
	 **        Equivalent to *(k + d) = t.
	 ** \param d offset.
	 ** \return a pointer to the 4d container elements
	 ** \pre (k + d) exists and is dereferenceable.
	 ** \post k[d] is a copy of pointer.
	 ** \todo add assert
	 */
	inline
	pointer* operator[](diff2d d) const
	{
		return (*cont4d_)[this->x1_+ d.dx1()][this->x2_ + d.dx2()];
	}

	/*!
	 ** \brief %const_iterator4d_box element assignment operator.
	 **        Equivalent to *(k + n).
	 ** \param n offset.
	 ** \return a pointer of pointer to the 4d container elements
	 ** \pre (k + n) exists and is dereferenceable.
	 ** \post k[n] is a copy of pointer of pointer.
	 ** \todo add assert
	 */
	inline
	pointer** operator[](int n) const
	{
		return (*cont4d_)[this->x1_+ n];
	}


	/*!
	 ** \brief %const_iterator4d_box element assignment operator.
	 ** \param slice offset.
	 ** \param row offset.
	 ** \param col offset
	 ** \return a const_slab_iterator to the first element of the
	 ** line defined in the container by (slice,row,col) indices.
	 ** \todo add assert
	 */
	inline
	typename Container4D::const_slab_iterator slab_begin(size_type slice, size_type row, size_type col) const
	{
		return cont4d_->slab_begin(this->box_.first_front_upper_left()[1] + slice,
				this->box_.first_front_upper_left()[2] + row,
				this->box_.first_front_upper_left()[3] + col) +
				this->box_.first_front_upper_left()[0];
	}

	/*!
	 ** \brief %const_iterator4d_box element assignment operator.
	 ** \param slice offset.
	 ** \param row row offset.
	 ** \param col col offset.
	 ** \return a const_slab_iterator to the past-the-end element of the
	 ** line defined in the container by (slice,row,col) indices.
	 ** \todo add assert
	 */
	inline
	typename Container4D::const_slab_iterator slab_end(size_type slice, size_type row, size_type col) const
	{
		return this->slab_begin(slice,row,col) + this->box_.duration();
	}


	/*!
	 ** \brief %const_iterator4d_box element assignment operator.
	 ** \param slab offset.
	 ** \param row offset.
	 ** \param col offset
	 ** \return a const_slice_iterator to the first element of the
	 ** line defined in the container by (slab,row,col) indices.
	 ** \todo add assert
	 */
	inline
	typename Container4D::const_slice_iterator slice_begin(size_type slab, size_type row, size_type col) const
	{
		return cont4d_->slice_begin(this->box_.first_front_upper_left()[0] + slab,
				this->box_.first_front_upper_left()[2] + row,
				this->box_.first_front_upper_left()[3] + col) +
				this->box_.first_front_upper_left()[1];
	}

	/*!
	 ** \brief %const_iterator4d_box element assignment operator.
	 ** \param slab offset.
	 ** \param row row offset.
	 ** \param col col offset.
	 ** \return a const_slice_iterator to the past-the-end element of the
	 ** line defined in the container by (slab,row,col) indices.
	 ** \todo add assert
	 */
	inline
	typename Container4D::const_slice_iterator slice_end(size_type slab, size_type row, size_type col) const
	{
		return this->slice_begin(slab,row,col) + this->box_.depth();
	}


	/*!
	 ** \brief %const_iterator4d_box element assignment operator.
	 ** \param slab offset.
	 ** \param slice slice offset.
	 ** \param row offset.
	 ** \return a const_row_iterator to the first element of the
	 ** line defined in the container by (slab,slice,row) indices.
	 ** \note Be careful, row_iterators and col_iterators are iterating threw a row and a column respectively.
	 ** It is not the case with slab and slice iterators which are iterating cross slabs and slices respectively.
	 ** \todo add assert
	 */
	inline
	typename Container4D::const_row_iterator row_begin(size_type slab, size_type slice, size_type row) const
	{
		return cont4d_->row_begin(this->box_.first_front_upper_left()[0] + slab, this->box_.first_front_upper_left()[1] + slice,
				this->box_.first_front_upper_left()[2] + row) + this->box_.first_front_upper_left()[3];
	}

	/*!
	 ** \brief %const_iterator4d_box element assignment operator.
	 ** \param slab offset.
	 ** \param slice slice offset.
	 ** \param row offset.
	 ** \return a const_row_iterator to the past-the-end element of the
	 ** line defined in the container by (slab,slice,row) indices.
	 ** \note Be careful, row_iterators and col_iterators are iterating threw a row and a column respectively.
	 ** It is not the case with slab and slice iterators which are iterating cross slabs and slices respectively.
	 ** \todo add assert
	 */
	inline
	typename Container4D::const_row_iterator row_end(size_type slab, size_type slice, size_type row) const
	{
		return this->row_begin(slab,slice,row) + this->box_.width();
	}

	/*!
	 ** \brief %const_iterator4d_box element assignment operator.
	 ** \param slab offset.
	 ** \param slice slice offset.
	 ** \param col col offset.
	 ** \return a const_col_iterator to the first element of the
	 ** line defined in the container by (slab,slice,col) indices.
	 ** \note Be careful, row_iterators and col_iterators are iterating threw a row and a column respectively.
	 ** It is not the case with slab and slice iterators which are iterating cross slabs and slices respectively.
	 ** \todo add assert
	 */
	inline
	typename Container4D::const_col_iterator col_begin(size_type slab, size_type slice, size_type col) const
	{
		return cont4d_->col_begin(this->box_.first_front_upper_left()[0] + slab, this->box_.first_front_upper_left()[1] + slice,
				this->box_.first_front_upper_left()[3] + col) +  this->box_.first_front_upper_left()[2];
	}
	/*!
	 ** \brief %const_iterator4d_box element assignment operator.
	 ** \param slab offset.
	 ** \param slice slice offset.
	 ** \param col col offset.
	 ** \return a const_col_iterator to the past-the-end element of the
	 ** line defined in the container by (slab,slice,col) indices.
	 ** \note Be careful, row_iterators and col_iterators are iterating threw a row and a column respectively.
	 ** It is not the case with slab and slice iterators which are iterating cross slabs and slices respectively.
	 ** \todo add assert
	 */
	inline
	typename Container4D::const_col_iterator col_end(size_type slab, size_type slice, size_type col) const
	{
		return this->col_begin(slab,slice,col) + this->box_.height();
	}

	/*@} End  iterator4d_box operators addons */


	/**
	 ** \name Subscripts accessors
	 */
	/*@{*/
	/*!
	 ** \brief Access to the first subscript of the current %const_iterator4d_box.
	 ** \return the first subscript.
	 */
	inline
	int x1() const
	{
		return this->x1_;
	}
	/*!
	 ** \brief Access to the first subscript of the current %const_iterator4d_box.
	 ** \return the first subscript.
	 */
	inline
	int t() const
	{
		return this->x1_;
	}
	/*!
	 ** \brief Access to the second subscript of the current %const_iterator4d_box.
	 ** \return the second subscript.
	 */
	inline
	int x2() const
	{
		return this->x2_;
	}
	/*!
	 ** \brief Access to the second subscript of the current %const_iterator4d_box.
	 ** \return the second subscript.
	 */
	inline
	int k() const
	{
		return this->x2_;
	}
	/*!
	 ** \brief Access to the third subscript of the current %const_iterator4d_box.
	 ** \return the third subscript.
	 */
	inline
	int x3() const
	{
		return this->x3_;
	}
	/*!
	 ** \brief Access to the third subscript of the current %const_iterator4d_box.
	 ** \return the third subscript.
	 */
	inline
	int i() const
	{
		return this->x3_;
	}

	/*!
	 ** \brief Access to the fourth subscript of the current %const_iterator4d_box.
	 ** \return the fourth subscript.
	 */
	inline
	int x4() const
	{
		return this->x4_;
	}
	/*!
	 ** \brief Access to the fourth subscript of the current %const_iterator4d_box.
	 ** \return the fourth subscript.
	 */
	inline
	int j() const
	{
		return this->x4_;
	}

	/*@} End  Subscripts accessors */




private:
	Container4D* cont4d_; // pointer to the 4d container
	pointer pos_;          // linear position within the container
	int x1_;              // first subscript position
	int x2_;              // second subscript position
	int x3_;              // third subscript position
	int x4_;              // fourth subscript position
	Box4d<int> box_;      // box to iterate
};



}//slip::

#endif //SLIP_ITERATOR4D_BOX_HPP
