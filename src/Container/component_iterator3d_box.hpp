/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/*!
 ** \file component_iterator3d_box.hpp
 ** \author Denis Arrivault <Denis.Arrivault_AT_INRIA.fr>
 ** \version Fluex 1.0
 ** \date 16/07/2013
 **
 ** \brief Provides a class to iterate a 3d MultiComponentContainer into a slip::Box3d.
 **
 */
#ifndef SLIP_COMPONENT_ITERATOR3D_BOX_HPP
#define SLIP_COMPONENT_ITERATOR3D_BOX_HPP

#include <iterator>
#include <cassert>

#include "Point3d.hpp"
#include "DPoint3d.hpp"
#include "Box3d.hpp"
#include "iterator_types.hpp"

namespace slip
{
/*! \class component_iterator3d_box
 **
 ** \brief This is some iterator to iterate a 3d MultiComponentContainer into a %Box area
 **        defined by the indices of the 3d container.
 ** \author Denis Arrivault <Denis.Arrivault_AT_INRIA.fr>
 ** \version Fluex 1.0
 ** \date 16/07/2013
 ** \param MultiComponentContainer3D Type of 3d container in the component_iterator3d_box
 ** \param N number of elements in one %block
 **
 ** \par This iterator is an 3d extension of the random_access_iterator.
 **      It is compatible with the bidirectional_iterator of the
 **      Standard library. It iterate into a box area defined inside the
 **      indices range of the 3 container. Those indices are defined as
 **      follows :
 ** \image html iterator3d_conventions.jpg "axis and notation conventions"
 ** \image latex iterator3d_conventions.eps "axis and notation conventions" width=5cm
 **
 */
template <class MultiComponentContainer3D, std::size_t N>
class component_iterator3d_box
{

public:

	typedef std::random_access_iterator3d_tag iterator_category;
	typedef typename MultiComponentContainer3D::value_type Block;
	typedef typename Block::value_type value_type;
	typedef DPoint3d<int> difference_type;
	typedef value_type* pointer;
	typedef value_type& reference;

	typedef component_iterator3d_box self;

	typedef typename MultiComponentContainer3D::size_type size_type;
	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** \brief Constructs a %component_iterator3d_box.
	 ** \par The box to iterate is in this case the full container
	 */
	component_iterator3d_box():
		cont3d_(0),pos_(0),x1_(0),x2_(0),x3_(0),cp_(0),box_(0,0,0,0,0,0)
	{}

	/*!
	 ** \brief Constructs a %component_iterator3d_box.
	 ** \param c pointer to an existing 3d container
	 ** \param cp index of the component to iterate
	 ** \param b Box3d<int> defining the range of indices to iterate
	 ** \pre box indices must be inside those of the 3d container
	 */
	component_iterator3d_box(MultiComponentContainer3D* c, std::size_t cp,
			const Box3d<int>& b):
				cont3d_(c),pos_(&(*c)[0][0][0][cp] +
						((b.front_upper_left())[0] * static_cast<int>(c->rows()) * static_cast<int>(c->cols()) * N) +
						((b.front_upper_left())[1] * static_cast<int>(c->cols()) * N) +
						((b.front_upper_left())[2] * N)),
						x1_((b.front_upper_left())[0]),x2_((b.front_upper_left())[1]),
						x3_((b.front_upper_left())[2]),cp_(cp),box_(b)
	{}

	/*!
	 ** \brief Constructs a copy of the %component_iterator3d_box \a o
	 **
	 ** \param o %component_iterator3d_box to copy.
	 */
	component_iterator3d_box(const self& o):
		cont3d_(o.cont3d_),pos_(o.pos_),x1_(o.x1_),x2_(o.x2_),x3_(o.x3_),cp_(o.cp_),box_(o.box_)
	{}

	/*@} End Constructors */

	/**
	 ** \name  Assignment operators and methods
	 */
	/*@{*/
	/*!
	 ** \brief Assign a %component_iterator3d_box.
	 **
	 ** Assign elements of %component_iterator3d_box in \a o.
	 **
	 ** \param o %component_iterator3d_box to get the values from.
	 ** \return a %component_iterator3d_box reference.
	 */
	self& operator=(const self& o)
	{
		if(this != &o)
		{
			this->cont3d_ = o.cont3d_;
			this->pos_ = o.pos_;
			this->x1_ = o.x1_;
			this->x2_ = o.x2_;
			this->x3_ = o.x3_;
			this->cp_ = o.cp_;
			this->box_ = o.box_;
		}
		return *this;
	}
	/*@} End Assignment operators and methods*/

	/*!
	 ** \brief Dereference assignment operator. Returns the element
	 **        that the current %component_iterator3d_box i point to.
	 **
	 ** \return a %component_iterator3d_box reference.
	 */
	inline
	reference operator*()
	{
		return *pos_;
	}

	inline
	pointer operator->()
	{
		return &(operator*());
	}

	/**
	 ** \name  Forward operators addons
	 */
	/*@{*/

	/*!
	 ** \brief Preincrement a %component_iterator3d_box.
	 ** Iterate to the next location inside the %Box3d.
	 **
	 */
	inline
	self& operator++()
	{
		if(x3_ < (box_.back_bottom_right())[2])
		{
			this->x3_++;
			this->pos_+=N;
		}
		else if(x2_ < (box_.back_bottom_right())[1])
		{
			this->x2_++;
			this->x3_ = (box_.front_upper_left())[2];
			this->pos_+= N * (static_cast<int>(cont3d_->cols()) - box_.width() + 1) ;
		}
		else if(x1_ < (box_.back_bottom_right())[0]){
			this->x1_++;
			this->x2_ = (box_.front_upper_left())[1];
			this->x3_ = (box_.front_upper_left())[2];
			this->pos_ += N * (static_cast<int>(cont3d_->cols())*(static_cast<int>(cont3d_->rows()) -
					box_.height() + 1) - box_.width() + 1 );
		}
		else{
			this->x1_ = (box_.back_bottom_right())[0] + 1;
			this->x2_ = (box_.back_bottom_right())[1] + 1;
			this->x3_ = (box_.back_bottom_right())[2] + 1;
			this->pos_ += N * (static_cast<int>(cont3d_->cols()) * (static_cast<int>(cont3d_->rows())
					+ 1) + 1);
		}
		return *this;
	}

	/*!
	 ** \brief Postincrement a %component_iterator3d_box.
	 ** Iterate to the next location inside the %Box3d.
	 */
	inline
	self operator++(int)
	{
		self tmp = *this;
		++(*this);
		return tmp;
	}

	/*@} End Forward operators addons */

	/**
	 ** \name  Bidirectional operators addons
	 */
	/*@{*/
	/*!
	 ** \brief Predecrement a %component_iterator3d_box.
	 **  Iterate to the previous location inside the %Box3d.
	 **
	 */
	inline
	self& operator--()
	{
		if(x3_ > (box_.front_upper_left())[2])
		{
			this->x3_--;
			this->pos_-=N;
		}
		else if (x2_ > (box_.front_upper_left())[1])
		{
			this->x3_ = (box_.back_bottom_right())[2];
			this->x2_--;
			this->pos_ -= N * (static_cast<int>(cont3d_->cols()) - box_.width() + 1);
		}
		else if (x1_ > (box_.front_upper_left())[0])
		{
			this->x3_ = (box_.back_bottom_right())[2];
			this->x2_ = (box_.back_bottom_right())[1];
			this->x1_--;
			this->pos_ -= N * (static_cast<int>(cont3d_->cols())*(static_cast<int>(cont3d_->rows()) -
					box_.height() + 1) - box_.width() + 1);
		}
		else{
			this->x3_ = (box_.front_upper_left())[2] - 1;
			this->x2_ = (box_.front_upper_left())[1] - 1;
			this->x1_ = (box_.front_upper_left())[0] - 1;
			this->pos_ -= N * (static_cast<int>(cont3d_->cols()) * (static_cast<int>(cont3d_->rows())
					+ 1) + 1);
		}
		return *this;
	}

	/*!
	 ** \brief Postdecrement a %component_iterator3d_box.
	 ** Iterate to the previous location inside the %Box3d.
	 **
	 */
	inline
	self operator--(int)
	{
		self tmp = *this;
		--(*this);
		return tmp;
	}

	/*@} End Bidirectional operators addons */

	/**
	 ** \name  Equality comparable operators
	 */
	/*@{*/
	/*!
	 ** \brief Equality operator.
	 ** \param i1 first %component_iterator3d_box.
	 ** \param i2 second %component_iterator3d_box.
	 ** \return true if i1 and i2 point to the same element of
	 **         the 3d container
	 */
	inline
	friend bool operator==(const self& i1,
			const self& i2)
			{

		return ( (i1.cont3d_ == i2.cont3d_) && (i1.pos_ == i2.pos_)
				&& (i1.x1_ == i2.x1_) && (i1.x2_ == i2.x2_) && (i1.x3_ == i2.x3_)
				&& (i1.cp_ == i2.cp_));
			}

	/*!
	 ** \brief Inequality operator.
	 ** \param i1 first %component_iterator3d_box.
	 ** \param i2 second %component_iterator3d_box.
	 ** \return true if !(i1 == i2)
	 */
	inline
	friend bool operator!=(const self& i1,
			const self& i2)
			{
		return ( (i1.cont3d_ != i2.cont3d_) || (i1.pos_ != i2.pos_)
				|| (i1.x1_ != i2.x1_) || (i1.x2_ != i2.x2_) || (i1.x3_ != i2.x3_)
				|| (i1.cp_ != i2.cp_));
			}

	/*@} End Equality comparable operators*/


	/**
	 ** \name  Strict Weakly comparable operators
	 */
	/*@{*/
	/*!
	 ** \brief < operator.
	 ** \param i1 first %component_iterator3d_box.
	 ** \param i2 second %component_iterator3d_box.
	 ** \return true i1.pos_ < i2_pos
	 */
	inline
	friend bool operator<(const self& i1,
			const self& i2)
	{

		return ( i1.pos_ < i2.pos_);
	}

	/*!
	 ** \brief > operator.
	 ** \param i1 first %component_iterator3d_box.
	 ** \param i2 second %component_iterator3d_box.
	 ** \return true i2 < i1
	 */
	inline
	friend bool operator>(const self& i1,
			const self& i2)
	{
		return (i2 < i1);
	}

	/*!
	 ** \brief <= operator.
	 ** \param i1 first %component_iterator3d_box.
	 ** \param i2 second %component_iterator3d_box.
	 ** \return true if !(i2 < i1)
	 */
	inline
	friend bool operator<=(const self& i1,
			const self& i2)
			{

		return  !(i2 < i1);
			}

	/*!
	 ** \brief >= operator.
	 ** \param i1 first %component_iterator3d_box.
	 ** \param i2 second %component_iterator3d_box.
	 ** \return true if !(i1 < i2)
	 */
	inline
	friend bool operator>=(const self& i1,
			const self& i2)
			{

		return  !(i1 < i2);
			}
	/*@} End  Strict Weakly comparable operators*/


	/**
	 ** \name  component_iterator3d_box operators addons
	 */
	/*@{*/
	/*!
	 ** \brief component_iterator3d_box addition.
	 ** \param d difference_type
	 ** \return a component_iterator3d_box reference
	 ** \pre All the iterators between the current iterator i and i + d
	 **      must be dereferenceable.
	 */
	inline
	self& operator+=(const difference_type& d)
	{
		this->x1_ += d.dx1();
		this->x2_ += d.dx2();
		this->x3_ += d.dx3();
		this->pos_ = &(*cont3d_)[0][0][0][this->cp_] +
				(this->x1_ * static_cast<int>(cont3d_->cols()) * static_cast<int>(cont3d_->rows()) * N) +
				(this->x2_ * static_cast<int>(cont3d_->cols()) * N) +
				this->x3_ * N;
		return *this;
	}



	/*!
	 ** \brief component_iterator3d_box substraction.
	 ** \param d difference_type
	 ** \return a component_iterator3d_box reference
	 ** \pre All the iterators between the current iterator i and i - d
	 **      must be dereferenceable.
	 */
	inline
	self& operator-=(const difference_type& d)
	{
		this->x1_ -= d.dx1();
		this->x2_ -= d.dx2();
		this->x3_ -= d.dx3();
		this->pos_ = &(*cont3d_)[0][0][0][this->cp_] +
				(this->x1_ * static_cast<int>(cont3d_->cols()) * static_cast<int>(cont3d_->rows()) * N) +
				(this->x2_ * static_cast<int>(cont3d_->cols()) * N) +
				this->x3_ * N;
		return *this;
	}

	/*!
	 ** \brief component_iterator3d_box addition.
	 ** \param d difference_type
	 ** \return a component_iterator3d_box reference
	 ** \pre All the iterators between the current iterator i and i + d
	 **      must be dereferenceable.
	 */
	inline
	self operator+(const difference_type& d)
	{
		self tmp = *this;
		tmp += d;
		return tmp;
	}

	/*!
	 ** \brief component_iterator3d_box substraction.
	 ** \param d difference_type
	 ** \return a component_iterator3d_box reference
	 ** \pre All the iterators between the current iterator i and i - d
	 **      must be dereferenceable.
	 */
	inline
	self operator-(const difference_type& d)
	{
		self tmp = *this;
		tmp -= d;
		return tmp;
	}


	/*!
	 ** \brief %component_iterator3d_box difference operator.
	 ** \param i1 first %component_iterator3d_box.
	 ** \param i2 second %component_iterator3d_box.
	 ** \return a difference_type d such that i1 = i2 + d.
	 ** \pre Either i1 is reachable from i2, or i2 is reachable from i1.
	 */
	inline
	friend difference_type operator-(const self& i1,
			const self& i2)
	{
		return difference_type(static_cast<int>(i1.x1_ - i2.x1_),static_cast<int>(i1.x2_ - i2.x2_),
				static_cast<int>(i1.x3_ - i2.x3_));
	}


	/*!
	 ** \brief %component_iterator3d_box element assignment operator.
	 **        Equivalent to *(i + d) = t.
	 ** \param d difference_type.
	 ** \return a reference to the 3d container elements
	 ** \pre (i + d) exists and is dereferenceable.
	 ** \post i[d] is a copy of reference.
	 */
	inline
	reference operator[](difference_type d)
	{
		assert( (this->x1_+d.dx1()) < cont3d_->dim1() );
		assert( (this->x2_+d.dx2()) < cont3d_->dim2() );
		assert( (this->x3_+d.dx3()) < cont3d_->dim3() );
		return (*cont3d_)[this->x1_+d.dx1()][this->x2_+d.dx2()][this->x3_+d.dx3()][cp_];
	}

	/*!
	 ** \brief %component_iterator3d_box element assignment operator.
	 ** \param row offset.
	 ** \param col offset
	 ** \return a slice_iterator to the first element of the
	 ** line (row,col) threw the slices in the box.
	 */
	inline
	typename MultiComponentContainer3D::component_slice_iterator slice_begin(size_type row, size_type col)
	{
		return cont3d_->slice_begin(this->cp_,this->box_.front_upper_left()[1] + row, this->box_.front_upper_left()[2] + col) +  this->box_.front_upper_left()[0];
	}


	/*!
	 ** \brief %component_iterator3d_box element assignment operator.
	 ** \param row row offset.
	 ** \param col col offset.
	 ** \return a slice_iterator to the last element of
	 ** line (row,col) threw the slices in the box.
	 */
	inline
	typename MultiComponentContainer3D::component_slice_iterator slice_end(size_type row, size_type col)
	{
		return this->slice_begin(row,col) + this->box_.depth();
	}

	/*!
	 ** \brief %component_iterator3d_box element assignment operator.
	 ** \param slice offset.
	 ** \param row offset.
	 ** \return a row_iterator to the first element of
	 **         the row \a row within the slice \a slice of the box
	 */
	inline
	typename MultiComponentContainer3D::component_row_iterator row_begin(size_type slice, size_type row)
	{
		return cont3d_->row_begin(this->cp_,this->box_.front_upper_left()[0] + slice, this->box_.front_upper_left()[1] + row) + this->box_.front_upper_left()[2];
	}


	/*!
	 ** \brief %component_iterator3d_box element assignment operator.
	 ** \param slice offset.
	 ** \param row offset.
	 ** \return a row_iterator to the past-the-end element of
	 **         the row \a row within the slice \a slice of the box
	 */
	inline
	typename MultiComponentContainer3D::component_row_iterator row_end(size_type slice, size_type row)
	{
		return this->row_begin(slice,row) + this->box_.width();
	}


	/*!
	 ** \brief %component_iterator3d_box element assignment operator.
	 ** \param slice offset.
	 ** \param row offset.
	 ** \return a col_iterator to the first element of
	 **         the col \a col within the slice \a slice of the box
	 */
	inline
	typename MultiComponentContainer3D::component_col_iterator col_begin(size_type slice, size_type col)
	{
		return cont3d_->col_begin(this->cp_,this->box_.front_upper_left()[0] + slice, this->box_.front_upper_left()[2] + col) +  this->box_.front_upper_left()[1];
	}


	/*!
	 ** \brief %component_iterator3d_box element assignment operator.
	 ** \param slice offset.
	 ** \param row offset.
	 ** \return a col_iterator to the first element of
	 **         the col \a col within the slice \a slice of the box
	 */
	inline
	typename MultiComponentContainer3D::component_col_iterator col_end(size_type slice, size_type col)
	{
		return this->col_begin(slice,col) + this->box_.height();
	}

	/*@} End  component_iterator3d_box operators addons */


	/**
	 ** \name Indices accessors
	 */
	/*@{*/
	/*!
	 ** \brief Access to the first index of the current %component_iterator3d_box.
	 ** \return the  first index.
	 */
	inline
	int x1() const
	{
		return this->x1_;
	}

	/*!
	 ** \brief Access to the first index of the current %component_iterator3d_box.
	 ** \return the  first index.
	 */
	inline
	int k() const
	{
		return this->x1_;
	}


	/*!
	 ** \brief Access to the second index of the current %component_iterator3d_box.
	 ** \return the  second index.
	 */
	inline
	int x2() const
	{
		return this->x2_;
	}

	/*!
	 ** \brief Access to the second index of the current %component_iterator3d_box.
	 ** \return the  second index.
	 */
	inline
	int i() const
	{
		return this->x2_;
	}

	/*!
	 ** \brief Access to the third index of the current %component_iterator3d_box.
	 ** \return the third index.
	 */
	inline
	int x3() const
	{
		return this->x3_;
	}

	/*!
	 ** \brief Access to the third index of the current %component_iterator3d_box.
	 ** \return the third index.
	 */
	inline
	int j() const
	{
		return this->x3_;
	}
	/*!
	 ** \brief Access to the component index of the current %component_iterator3d_box.
	 ** \return the component index
	 */
	inline
	size_type cp() const
	{
		return this->cp_;
	}


	/*@} End  Indices accessors */


private:
	MultiComponentContainer3D* cont3d_; // pointer to the 3d container
	pointer pos_;         // linear position within the container
	int x1_;              // first index position
	int x2_;              // second index position
	int x3_;              // Third index position
	size_type cp_;              // component index number
	Box3d<int> box_;      // box to iterate
};


/*! \class const_component_iterator3d_box
 **
 ** \brief This is some iterator to iterate a 3d MultiComponentContainer into a %Box area
 **        defined by the indices of the 3d container.
 ** \author Denis Arrivault <Denis.Arrivault_AT_INRIA.fr>
 ** \version Fluex 1.0
 ** \date 16/07/2013
 ** \param MultiComponentContainer3D Type of 3d container in the const_component_iterator3d_box
 ** \param N number of elements in one %block
 **
 ** \par This iterator is an 3d extension of the random_access_iterator.
 **      It is compatible with the bidirectional_iterator of the
 **      Standard library. It iterate into a box area defined inside the
 **      indices range of the 3d container. Those indices are defined as
 **      follows :
 ** \image html iterator3d_conventions.jpg "axis and notation conventions"
 ** \image latex iterator3d_conventions.eps "axis and notation conventions" width=5cm
 */
template <class MultiComponentContainer3D, std::size_t N>
class const_component_iterator3d_box
{

public:

	//typedef std::random_access_iterator_tag iterator_category;
	typedef std::random_access_iterator3d_tag iterator_category;
	typedef typename MultiComponentContainer3D::value_type Block;
	typedef typename Block::value_type value_type;
	typedef DPoint3d<int> difference_type;
	typedef value_type const * pointer;
	typedef value_type const & reference;

	typedef const_component_iterator3d_box self;

	typedef typename MultiComponentContainer3D::size_type size_type;
	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** \brief Constructs a %const_component_iterator3d_box.
	 ** \par The box to iterate is in this case the full container
	 */
	const_component_iterator3d_box():
		cont3d_(0),pos_(0),x1_(0),x2_(0),x3_(0),cp_(0),box_(0,0,0,0)
	{}

	/*!
	 ** \brief Constructs a %const_component_iterator3d_box.
	 ** \param c pointer to an existing 3d container
	 ** \param cp index of the component to iterate
	 ** \param b Box3d<int> defining the range of indices to iterate
	 ** \pre box indices must be inside those of the 3d container
	 */
	const_component_iterator3d_box(MultiComponentContainer3D* c, std::size_t cp,
			const Box3d<int>& b):
				cont3d_(c),pos_(&(*c)[0][0][0][cp] +
						((b.front_upper_left())[0] * static_cast<int>(c->rows()) * static_cast<int>(c->cols()) * N) +
						((b.front_upper_left())[1] * static_cast<int>(c->cols()) * N) +
						((b.front_upper_left())[2] * N)),
						x1_((b.front_upper_left())[0]),x2_((b.front_upper_left())[1]),
						x3_((b.front_upper_left())[2]),cp_(cp),box_(b)
	{}

	/*!
	 ** \brief Constructs a copy of the %const_component_iterator3d_box \a o
	 **
	 ** \param o %component_iterator3d_box to copy.
	 */
	const_component_iterator3d_box(const self& o):
		cont3d_(o.cont3d_),pos_(o.pos_),x1_(o.x1_),x2_(o.x2_),x3_(o.x3_),cp_(o.cp_),box_(o.box_)
	{}

	/*@} End Constructors */

	/**
	 ** \name  Assignment operators and methods
	 */
	/*@{*/
	/*!
	 ** \brief Assign a %const_component_iterator3d_box.
	 **
	 ** Assign elements of %const_component_iterator3d_box in \a o.
	 **
	 ** \param o %const_component_iterator3d_box to get the values from.
	 ** \return a %const_component_iterator3d_box reference.
	 */
	self& operator=(const self& o)
	{
		if(this != &o)
		{
			this->cont3d_ = o.cont3d_;
			this->pos_ = o.pos_;
			this->x1_ = o.x1_;
			this->x2_ = o.x2_;
			this->x3_ = o.x3_;
			this->cp_ = o.cp_;
			this->box_ = o.box_;
		}
		return *this;
	}
	/*@} End Assignment operators and methods*/

	/*!
	 ** \brief Dereference assignment operator. Returns the element
	 **        that the current %const_component_iterator3d_box i point to.
	 **
	 ** \return a %const_component_iterator3d_box reference.
	 */
	inline
	reference operator*() const
	{
		return *pos_;
	}

	inline
	pointer operator->() const
	{
		return &(operator*());
	}

	/**
	 ** \name  Forward operators addons
	 */
	/*@{*/

	/*!
	 ** \brief Preincrement a %const_component_iterator3d_box.
	 ** Iterate to the next location inside the %Box3d.
	 **
	 */
	inline
	self& operator++()
	{
		if(x3_ < (box_.back_bottom_right())[2])
		{
			this->x3_++;
			this->pos_+=N;
		}
		else if(x2_ < (box_.back_bottom_right())[1])
		{
			this->x2_++;
			this->x3_ = (box_.front_upper_left())[2];
			this->pos_ += N * (static_cast<int>(cont3d_->cols()) - box_.width() + 1);
		}
		else if(x1_ < (box_.back_bottom_right())[0]){
			this->x1_++;
			this->x2_ = (box_.front_upper_left())[1];
			this->x3_ = (box_.front_upper_left())[2];
			this->pos_ += N * (static_cast<int>(cont3d_->cols())*(static_cast<int>(cont3d_->rows()) - box_.height() + 1)
					- box_.width() + 1);
		}
		else{
			this->x1_ = (box_.back_bottom_right())[0] + 1;
			this->x2_ = (box_.back_bottom_right())[1] + 1;
			this->x3_ = (box_.back_bottom_right())[2] + 1;
			this->pos_ += N * (static_cast<int>(cont3d_->cols()) * (static_cast<int>(cont3d_->rows()) + 1) + 1);
		}
		return *this;
	}

	/*!
	 ** \brief Postincrement a %const_component_iterator3d_box.
	 ** Iterate to the next location inside the %Box3d.
	 */
	inline
	self operator++(int)
	{
		self tmp = *this;
		++(*this);
		return tmp;
	}

	/*@} End Forward operators addons */

	/**
	 ** \name  Bidirectional operators addons
	 */
	/*@{*/
	/*!
	 ** \brief Predecrement a %const_component_iterator3d_box.
	 **  Iterate to the previous location inside the %Box3d.
	 **
	 */
	inline
	self& operator--()
	{
		if(x3_ > (box_.front_upper_left())[2])
		{
			this->x3_--;
			this->pos_-=N;
		}
		else if (x2_ > (box_.front_upper_left())[1])
		{
			this->x3_ = (box_.back_bottom_right())[2];
			this->x2_--;
			this->pos_ -= N * (static_cast<int>(cont3d_->cols()) - box_.width() + 1);
		}
		else if (x1_ > (box_.front_upper_left())[0])
		{
			this->x3_ = (box_.back_bottom_right())[2];
			this->x2_ = (box_.back_bottom_right())[1];
			this->x1_--;
			this->pos_ -= N * (static_cast<int>(cont3d_->cols())*(static_cast<int>(cont3d_->rows()) - box_.height() + 1)
					- box_.width() + 1);
		}
		else{
			this->x3_ = (box_.front_upper_left())[2] - 1;
			this->x2_ = (box_.front_upper_left())[1] - 1;
			this->x1_ = (box_.front_upper_left())[0] - 1;
			this->pos_ -= N * (static_cast<int>(cont3d_->cols()) * (static_cast<int>(cont3d_->rows()) + 1) + 1);
		}
		return *this;
	}

	/*!
	 ** \brief Postdecrement a %const_component_iterator3d_box.
	 ** Iterate to the previous location inside the %Box3d.
	 **
	 */
	inline
	self operator--(int)
	{
		self tmp = *this;
		--(*this);
		return tmp;
	}

	/*@} End Bidirectional operators addons */

	/**
	 ** \name  Equality comparable operators
	 */
	/*@{*/
	/*!
	 ** \brief Equality operator.
	 ** \param i1 first %const_component_iterator3d_box.
	 ** \param i2 second %const_component_iterator3d_box.
	 ** \return true if i1 and i2 point to the same element of
	 **         the 3d container
	 */
	inline
	friend bool operator==(const self& i1,
			const self& i2)
			{

		return ( (i1.cont3d_ == i2.cont3d_) && (i1.pos_ == i2.pos_)
				&& (i1.x1_ == i2.x1_) && (i1.x2_ == i2.x2_) && (i1.x3_ == i2.x3_)
				&& (i1.cp_ == i2.cp_));
			}

	/*!
	 ** \brief Inequality operator.
	 ** \param i1 first %const_component_iterator3d_box.
	 ** \param i2 second %const_component_iterator3d_box.
	 ** \return true if !(i1 == i2)
	 */
	inline
	friend bool operator!=(const self& i1,
			const self& i2)
			{
		return ( (i1.cont3d_ != i2.cont3d_) || (i1.pos_ != i2.pos_)
				|| (i1.x1_ != i2.x1_) || (i1.x2_ != i2.x2_) || (i1.x3_ != i2.x3_)
				|| (i1.cp_ != i2.cp_));
			}

	/*@} End Equality comparable operators*/


	/**
	 ** \name  Strict Weakly comparable operators
	 */
	/*@{*/
	/*!
	 ** \brief < operator.
	 ** \param i1 first %const_component_iterator3d_box.
	 ** \param i2 second %const_component_iterator3d_box.
	 ** \return true i1.pos_ < i2_pos
	 */
	inline
	friend bool operator<(const self& i1,
			const self& i2)
	{

		return ( i1.pos_ < i2.pos_);
	}

	/*!
	 ** \brief > operator.
	 ** \param i1 first %const_component_iterator3d_box.
	 ** \param i2 second %const_component_iterator3d_box.
	 ** \return true i2 < i1
	 */
	inline
	friend bool operator>(const self& i1,
			const self& i2)
	{
		return (i2 < i1);
	}

	/*!
	 ** \brief <= operator.
	 ** \param i1 first %const_component_iterator3d_box.
	 ** \param i2 second %const_component_iterator3d_box.
	 ** \return true if !(i2 < i1)
	 */
	inline
	friend bool operator<=(const self& i1,
			const self& i2)
			{

		return  !(i2 < i1);
			}

	/*!
	 ** \brief >= operator.
	 ** \param i1 first %const_component_iterator3d_box.
	 ** \param i2 second %const_component_iterator3d_box.
	 ** \return true if !(i1 < i2)
	 */
	inline
	friend bool operator>=(const self& i1,
			const self& i2)
			{

		return  !(i1 < i2);
			}
	/*@} End  Strict Weakly comparable operators*/


	/**
	 ** \name  const_component_iterator3d_box operators addons
	 */
	/*@{*/
	/*!
	 ** \brief const_component_iterator3d_box addition.
	 ** \param d difference_type
	 ** \return a const_component_iterator3d_box reference
	 ** \pre All the iterators between the current iterator i and i + d
	 **      must be dereferenceable.
	 */
	inline
	self& operator+=(const difference_type& d)
	{
		this->x1_ += d.dx1();
		this->x2_ += d.dx2();
		this->x3_ += d.dx3();
		this->pos_ = &(*cont3d_)[0][0][0][this->cp_] +
				(this->x1_ * static_cast<int>(cont3d_->cols()) * static_cast<int>(cont3d_->rows()) * N) +
				(this->x2_ * static_cast<int>(cont3d_->cols()) * N) +
				this->x3_ * N;
		return *this;
	}



	/*!
	 ** \brief const_component_iterator3d_box substraction.
	 ** \param d difference_type
	 ** \return a const_component_iterator3d_box reference
	 ** \pre All the iterators between the current iterator i and i - d
	 **      must be dereferenceable.
	 */
	inline
	self& operator-=(const difference_type& d)
	{
		this->x1_ -= d.dx1();
		this->x2_ -= d.dx2();
		this->x3_ -= d.dx3();
		this->pos_ = &(*cont3d_)[0][0][0][this->cp_] +
				(this->x1_ * static_cast<int>(cont3d_->cols()) * static_cast<int>(cont3d_->rows()) * N) +
				(this->x2_ * static_cast<int>(cont3d_->cols()) * N) +
				this->x3_ * N;
		return *this;
	}

	/*!
	 ** \brief const_component_iterator3d_box addition.
	 ** \param d difference_type
	 ** \return a const_component_iterator3d_box reference
	 ** \pre All the iterators between the current iterator i and i + d
	 **      must be dereferenceable.
	 */
	inline
	self operator+(const difference_type& d)
	{
		self tmp = *this;
		tmp += d;
		return tmp;
	}

	/*!
	 ** \brief const_component_iterator3d_box substraction.
	 ** \param d difference_type
	 ** \return a const_component_iterator3d_box reference
	 ** \pre All the iterators between the current iterator i and i - d
	 **      must be dereferenceable.
	 */
	inline
	self operator-(const difference_type& d)
	{
		self tmp = *this;
		tmp -= d;
		return tmp;
	}


	/*!
	 ** \brief %const_component_iterator3d_box difference operator.
	 ** \param i1 first %const_component_iterator3d_box.
	 ** \param i2 second %const_component_iterator3d_box.
	 ** \return a difference_type d such that i1 = i2 + d.
	 ** \pre Either i1 is reachable from i2, or i2 is reachable from i1.
	 */
	inline
	friend difference_type operator-(const self& i1,
			const self& i2)
	{
		return difference_type(int(i1.x1_ - i2.x1_),int(i1.x2_ - i2.x2_),int(i1.x3_ - i2.x3_));
	}


	/*!
	 ** \brief %const_component_iterator3d_box element assignment operator.
	 **        Equivalent to *(i + d) = t.
	 ** \param d difference_type.
	 ** \return a reference to the 3d container elements
	 ** \pre (i + d) exists and is dereferenceable.
	 ** \post i[d] is a copy of reference.
	 */
	inline
	reference operator[](difference_type d) const
	{
		assert( (this->x1_+d.dx1()) < cont3d_->dim1() );
		assert( (this->x2_+d.dx2()) < cont3d_->dim2() );
		assert( (this->x3_+d.dx3()) < cont3d_->dim3() );
		return (*cont3d_)[this->x1_+d.dx1()][this->x2_+d.dx2()][this->x3_+d.dx3()][cp_];
	}


	/*!
	 ** \brief %const_component_iterator3d_box element assignment operator.
	 ** \param row offset.
	 ** \param col offset
	 ** \return a slice_iterator to the first element of the
	 ** line (row,col) threw the slices in the box.
	 */
	inline
	typename MultiComponentContainer3D::const_component_slice_iterator slice_begin(size_type row, size_type col)
	{
		return cont3d_->slice_begin(this->cp_,this->box_.front_upper_left()[1] + row, this->box_.front_upper_left()[2] + col) +  this->box_.front_upper_left()[0];
	}


	/*!
	 ** \brief %const_component_iterator3d_box element assignment operator.
	 ** \param row row offset.
	 ** \param col col offset.
	 ** \return a slice_iterator to the last element of
	 ** line (row,col) threw the slices in the box.
	 */
	inline
	typename MultiComponentContainer3D::const_component_slice_iterator slice_end(size_type row, size_type col)
	{
		return this->slice_begin(row,col) + this->box_.depth();
	}

	/*!
	 ** \brief %const_component_iterator3d_box element assignment operator.
	 ** \param slice offset.
	 ** \param row offset.
	 ** \return a row_iterator to the first element of
	 **         the row \a row within the slice \a slice of the box
	 */
	inline
	typename MultiComponentContainer3D::const_component_row_iterator row_begin(size_type slice, size_type row)
	{
		return cont3d_->row_begin(this->cp_,this->box_.front_upper_left()[0] + slice, this->box_.front_upper_left()[1] + row) + this->box_.front_upper_left()[2];
	}


	/*!
	 ** \brief %const_component_iterator3d_box element assignment operator.
	 ** \param slice offset.
	 ** \param row offset.
	 ** \return a row_iterator to the past-the-end element of
	 **         the row \a row within the slice \a slice of the box
	 */
	inline
	typename MultiComponentContainer3D::const_component_row_iterator row_end(size_type slice, size_type row)
	{
		return this->row_begin(slice,row) + this->box_.width();
	}


	/*!
	 ** \brief %const_component_iterator3d_box element assignment operator.
	 ** \param slice offset.
	 ** \param row offset.
	 ** \return a col_iterator to the first element of
	 **         the col \a col within the slice \a slice of the box
	 */
	inline
	typename MultiComponentContainer3D::const_component_col_iterator col_begin(size_type slice, size_type col)
	{
		return cont3d_->col_begin(this->cp_,this->box_.front_upper_left()[0] + slice, this->box_.front_upper_left()[2] + col) +  this->box_.front_upper_left()[1];
	}


	/*!
	 ** \brief %const_component_iterator3d_box element assignment operator.
	 ** \param slice offset.
	 ** \param row offset.
	 ** \return a col_iterator to the first element of
	 **         the col \a col within the slice \a slice of the box
	 */
	inline
	typename MultiComponentContainer3D::const_component_col_iterator col_end(size_type slice, size_type col)
	{
		return this->col_begin(slice,col) + this->box_.height();
	}

	/*@} End  const_component_iterator3d_box operators addons */


	/**
	 ** \name Indices accessors
	 */
	/*@{*/
	/*!
	 ** \brief Access to the first index of the current %const_component_iterator3d_box.
	 ** \return the  first index.
	 */
	inline
	int x1() const
	{
		return this->x1_;
	}

	/*!
	 ** \brief Access to the first index of the current %const_component_iterator3d_box.
	 ** \return the  first index.
	 */
	inline
	int k() const
	{
		return this->x1_;
	}


	/*!
	 ** \brief Access to the second index of the current %const_component_iterator3d_box.
	 ** \return the  second index.
	 */
	inline
	int x2() const
	{
		return this->x2_;
	}

	/*!
	 ** \brief Access to the second index of the current %const_component_iterator3d_box.
	 ** \return the  second index.
	 */
	inline
	int i() const
	{
		return this->x2_;
	}

	/*!
	 ** \brief Access to the third index of the current %const_component_iterator3d_box.
	 ** \return the third index.
	 */
	inline
	int x3() const
	{
		return this->x3_;
	}

	/*!
	 ** \brief Access to the third index of the current %const_component_iterator3d_box.
	 ** \return the third index.
	 */
	inline
	int j() const
	{
		return this->x3_;
	}

	/*!
	 ** \brief Access to the component index of the current %const_component_iterator3d_box.
	 ** \return the component index
	 */
	inline
	size_type cp() const
	{
		return this->cp_;
	}


	/*@} End  Indices accessors */


private:
	MultiComponentContainer3D* cont3d_; // pointer to the 3d container
	pointer pos_;         // linear position within the container
	int x1_;              // first index position
	int x2_;              // second index position
	int x3_;              // third index position
	size_type cp_;        // component index number
	Box3d<int> box_;      // box to iterate
};

}//slip::

#endif //SLIP_COMPONENT_ITERATOR3D_BOX_HPP
