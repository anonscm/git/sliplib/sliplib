/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file SoloffCamera.hpp
 * 
 * \brief Provides a polynomial (Soloff) camera model class.
 * 
 */

#ifndef SLIP_SOLOFFCAMERA_HPP
#define SLIP_SOLOFFCAMERA_HPP

#include <iostream>
#include <cassert>
#include <string>
#include "Point2d.hpp"
#include "Point3d.hpp"
#include "Vector3d.hpp"
#include "Matrix.hpp"
#include "io_tools.hpp"
#include "CameraModel.hpp"
#include "camera_algo.hpp"
#include "polynomial_algo.hpp"
#include "macros.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/base_object.hpp>

namespace slip 
{
  template <typename Type>
  class SoloffCamera;

  template <typename Type>
  std::ostream& operator<<(std::ostream & out, const slip::SoloffCamera<Type>& c);

  /*! \class SoloffCamera
  **  \ingroup Containers Camera
  ** \author Markus Jehle <jehle_markus_AT_yahoo.de>
  ** \author Benoit Tremblais benoit.tremblais_AT_sic.univ-poitiers.fr
  ** \author Lionel Thomas lionel.thomas_AT_univ-poitiers.fr
  ** \author Guillaume Gomit guillaume.gomit_AT_univ-poitiers.fr
  ** \version 0.0.2
  ** \date 2023/11/22 add X_backprojection Y_backprojection  projection_gradient and numerical_projection_gradient methods
  ** \since 1.2.0
  ** \brief Define a %SoloffCamera class
  ** \remarks Soloff SM,Adrian RJ,Liu ZC (1997) Distortion compensation for generalized stereoscopic particle image velocimetry. Meas. Sci. Technol. 8:1441-1445, doi:10.1088/0957-0233/8/12/008.
  ** \param Type Type of the coordinates of the SoloffCamera.
  */
  template <typename Type>
  class SoloffCamera:public slip::CameraModel<Type> {
    typedef slip::SoloffCamera<Type> self;
    typedef slip::CameraModel<Type> base;
  public:
    /**
     ** \name Constructors & Destructors
     */
    /*@{*/

    /*!
    ** \brief Default constructor of SoloffCamera
    */
    SoloffCamera():
      calibration_polynomial_x_(new slip::MultivariatePolynomial<Type,3>((std::size_t)3)),
      calibration_polynomial_y_(new slip::MultivariatePolynomial<Type,3>((std::size_t)3)),
      calibration_polynomial_X_(new slip::MultivariatePolynomial<Type,3>((std::size_t)3)),
      calibration_polynomial_Y_(new slip::MultivariatePolynomial<Type,3>((std::size_t)3)),
      calibration_polynomial_YX_(new slip::MultivariatePolynomial<Type,3>((std::size_t)3)),
      calibration_polynomial_ZX_(new slip::MultivariatePolynomial<Type,3>((std::size_t)3)),
      calibration_polynomial_XY_(new slip::MultivariatePolynomial<Type,3>((std::size_t)3)),
      calibration_polynomial_ZY_(new slip::MultivariatePolynomial<Type,3>((std::size_t)3)),
      calibration_polynomial_coeff_x_(new slip::Array<Type>(20)),
      calibration_polynomial_coeff_y_(new slip::Array<Type>(20)),
      calibration_polynomial_coeff_X_(new slip::Array<Type>(20)),
      calibration_polynomial_coeff_Y_(new slip::Array<Type>(20)),
      calibration_polynomial_coeff_YX_(new slip::Array<Type>(20)),
      calibration_polynomial_coeff_ZX_(new slip::Array<Type>(20)),
      calibration_polynomial_coeff_XY_(new slip::Array<Type>(20)),
      calibration_polynomial_coeff_ZY_(new slip::Array<Type>(20))
    {
    }
    /*!
    ** \brief Constructs a copy of the SoloffCamera \a rhs
    ** \param rhs
    */
    SoloffCamera(const self& rhs):
      base(rhs),
      calibration_polynomial_x_(new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_x_))),
      calibration_polynomial_y_(new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_y_))),
      calibration_polynomial_X_(new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_X_))),
      calibration_polynomial_Y_(new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_Y_))),
      calibration_polynomial_YX_(new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_YX_))),
      calibration_polynomial_ZX_(new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_ZX_))),
      calibration_polynomial_XY_(new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_XY_))),
      calibration_polynomial_ZY_(new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_ZY_))),
      calibration_polynomial_coeff_x_(new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_x_))),
      calibration_polynomial_coeff_y_(new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_y_))),
      calibration_polynomial_coeff_X_(new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_X_))),
      calibration_polynomial_coeff_Y_(new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_Y_))),
      calibration_polynomial_coeff_YX_(new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_YX_))),
      calibration_polynomial_coeff_ZX_(new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_ZX_))),
       calibration_polynomial_coeff_XY_(new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_XY_))),
      calibration_polynomial_coeff_ZY_(new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_ZY_)))

    {}

    /*!
    ** \brief Destructor of the %SoloffCamera
    */
    ~SoloffCamera()
    {
      delete calibration_polynomial_x_;
      delete calibration_polynomial_y_;
      delete calibration_polynomial_X_;
      delete calibration_polynomial_Y_;
      delete calibration_polynomial_YX_;
      delete calibration_polynomial_ZX_;
      delete calibration_polynomial_XY_;
      delete calibration_polynomial_ZY_;
      delete calibration_polynomial_coeff_x_;
      delete calibration_polynomial_coeff_y_;
      delete calibration_polynomial_coeff_X_;
      delete calibration_polynomial_coeff_Y_;
      delete calibration_polynomial_coeff_YX_;
      delete calibration_polynomial_coeff_ZX_;
      delete calibration_polynomial_coeff_XY_;
      delete calibration_polynomial_coeff_ZY_;
    }

    /*!
    ** \brief Clone the %SoloffCamera
    ** \return new pointer towards a copy of this %SoloffCamera
    ** \remark must be detroyed after use
    */
    virtual base* clone () const
    {
      return new self(*this);
    }

    
    /*@} End Constructors */


    /**
     ** \name i/o operators
     */
    /*@{*/

    /*!
    ** \brief Write the SoloffCamera to the ouput stream
    ** \param out output stream
    ** \param c SoloffCamera to write to the output stream
    */
    friend std::ostream& operator<< <>(std::ostream & out, const self& c);

    /*@} i/o  operators */


    /**
     ** \name  Assignment operators and methods
     */
    /*@{*/
    /*!
    ** \brief Assign a SoloffCamera.
    **
    ** Assign elements of SoloffCamera in \a rhs
    **
    ** \param rhs SoloffCamera to get the values from.
    ** \return self.
    */
    self& operator=(const self & rhs)
    {
       if(this != &rhs)
       {
	this->base::operator=(rhs);

	if(calibration_polynomial_x_ != 0)
	  {
	    delete calibration_polynomial_x_;
	  }
	calibration_polynomial_x_ = new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_x_));
	if(calibration_polynomial_y_ != 0)
	  {
	    delete calibration_polynomial_y_;
	  }
	calibration_polynomial_y_ = new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_y_));								     
	if(calibration_polynomial_X_ != 0)
	  {
	    delete calibration_polynomial_X_;
	  }
	 calibration_polynomial_X_ = new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_X_));							     
	if(calibration_polynomial_Y_ != 0)
	  {
	    delete calibration_polynomial_Y_;
	  }
	calibration_polynomial_Y_ = new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_Y_));
	if(calibration_polynomial_YX_ != 0)
	  {
	    delete calibration_polynomial_YX_;
	  }
	 calibration_polynomial_YX_ = new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_YX_));							     
	if(calibration_polynomial_ZX_ != 0)
	  {
	    delete calibration_polynomial_ZX_;
	  }
	calibration_polynomial_ZX_ = new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_ZX_));
	
	if(calibration_polynomial_XY_ != 0)
	  {
	    delete calibration_polynomial_XY_;
	  }
	 calibration_polynomial_XY_ = new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_XY_));							     
	if(calibration_polynomial_ZY_ != 0)
	  {
	    delete calibration_polynomial_ZY_;
	  }
	calibration_polynomial_ZY_ = new slip::MultivariatePolynomial<Type,3>(*(rhs.calibration_polynomial_ZY_));
	if(calibration_polynomial_coeff_x_ != 0)
	  {
	    delete calibration_polynomial_coeff_x_;
	  }
	calibration_polynomial_coeff_x_ = new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_x_));							     
	if(calibration_polynomial_coeff_y_ != 0)
	  {
	    delete calibration_polynomial_coeff_y_;
	  }
	calibration_polynomial_coeff_y_ = new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_y_));								 
	if(calibration_polynomial_coeff_X_ != 0)
	  {
	    delete calibration_polynomial_coeff_X_;
	  }
	calibration_polynomial_coeff_X_ = new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_X_));
	if(calibration_polynomial_coeff_Y_ != 0)
	  {
	    delete calibration_polynomial_coeff_Y_;
	  }
	calibration_polynomial_coeff_Y_ = new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_Y_));
	if(calibration_polynomial_coeff_YX_ != 0)
	  {
	    delete calibration_polynomial_coeff_YX_;
	  }
	calibration_polynomial_coeff_YX_ = new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_YX_));
	if(calibration_polynomial_coeff_ZX_ != 0)
	  {
	    delete calibration_polynomial_coeff_ZX_;
	  }
	calibration_polynomial_coeff_ZX_ = new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_ZX_));
	if(calibration_polynomial_coeff_XY_ != 0)
	  {
	    delete calibration_polynomial_coeff_XY_;
	  }
	calibration_polynomial_coeff_XY_ = new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_XY_));
	if(calibration_polynomial_coeff_ZY_ != 0)
	  {
	    delete calibration_polynomial_coeff_ZY_;
	  }
	calibration_polynomial_coeff_ZY_ = new slip::Array<Type>(*(rhs.calibration_polynomial_coeff_ZY_));
	

       }
    return *this;
  }
    /*@} End Assignment operators and methods*/


     /**
     ** \name  Accessors/Mutators
     */
    /*@{*/
     /*!
    ** \brief Get the image x calibration polynomial.
    ** \return a the x slip::MultivariatePolynomial<Type,3> calibration polynomial.
    */
    slip::MultivariatePolynomial<Type,3>& calibration_polynomial_x() const
    {
      return *calibration_polynomial_x_;
    }

    /*!
    ** \brief Set the image x calibration polynomial equal to calibration_polynomial_x.
    ** \param calibration_polynomial_x a slip::MultivariatePolynomial<Type,3> corresponding to the image x calibration polynomial 
    ** \pre calibration_polynomial_x.degree() == 3
    */
    void calibration_polynomial_x(const slip::MultivariatePolynomial<Type,3>& calibration_polynomial_x)
    {
      assert(calibration_polynomial_x.degree() == 3);
      *(this->calibration_polynomial_x_) = calibration_polynomial_x;
      calibration_polynomial_x.all_coeff(this->calibration_polynomial_coeff_x_->begin(),
					 this->calibration_polynomial_coeff_x_->end());				 
    }
    
    /*!
    ** \brief Get the image y calibration polynomial.
    ** \return a the y slip::MultivariatePolynomial<Type,3> calibration polynomial.
    */
    slip::MultivariatePolynomial<Type,3>& calibration_polynomial_y() const
    {
      return *calibration_polynomial_y_;
    }
 
    /*!
    ** \brief Set the image y calibration polynomial equal to calibration_polynomial_x.
    ** \param calibration_polynomial_y a slip::MultivariatePolynomial<Type,3> corresponding to the image y calibration polynomial 
    ** \pre calibration_polynomial_y.degree() == 3
    */
   void calibration_polynomial_y(const slip::MultivariatePolynomial<Type,3>& calibration_polynomial_y)
    {
      assert(calibration_polynomial_y.degree() == 3);
      *(this->calibration_polynomial_y_) = calibration_polynomial_y;
      calibration_polynomial_y.all_coeff(this->calibration_polynomial_coeff_y_->begin(),
					 this->calibration_polynomial_coeff_y_->end());				 
    }
    
  
    /*!
    ** \brief Get the real world X calibration polynomial.
    ** \return a the X slip::MultivariatePolynomial<Type,3> calibration polynomial.
    */
     slip::MultivariatePolynomial<Type,3>& calibration_polynomial_X() const
    {
      return *calibration_polynomial_X_;
    }
   
    /*!
    ** \brief Set the real world X calibration polynomial equal to calibration_polynomial_X.
    ** \param calibration_polynomial_X a slip::MultivariatePolynomial<Type,3> corresponding to the real world X calibration polynomial 
    ** \pre calibration_polynomial_X.degree() == 3
    */
     void calibration_polynomial_X(const slip::MultivariatePolynomial<Type,3>& calibration_polynomial_X)
    {
       assert(calibration_polynomial_X.degree() == 3);
      *(this->calibration_polynomial_X_) = calibration_polynomial_X;
      calibration_polynomial_X.all_coeff(this->calibration_polynomial_coeff_X_->begin(),
					 this->calibration_polynomial_coeff_X_->end());				 
    }
  
    /*!
    ** \brief Get the real world Y calibration polynomial.
    ** \return a the Y slip::MultivariatePolynomial<Type,3> calibration polynomial.
    */
    slip::MultivariatePolynomial<Type,3>& calibration_polynomial_Y()
    {
      return *calibration_polynomial_Y_;
    }
 
    /*!
    ** \brief Set the real world Y calibration polynomial equal to calibration_polynomial_Y.
    ** \param calibration_polynomial_Y a slip::MultivariatePolynomial<Type,3> corresponding to the real world Y calibration polynomial 
    ** \pre calibration_polynomial_Y.degree() == 3
    */
      void calibration_polynomial_Y(const slip::MultivariatePolynomial<Type,3>& calibration_polynomial_Y)
    {
       assert(calibration_polynomial_Y.degree() == 3);
      *(this->calibration_polynomial_Y_) = calibration_polynomial_Y;
      calibration_polynomial_Y.all_coeff(this->calibration_polynomial_coeff_Y_->begin(),
					 this->calibration_polynomial_coeff_Y_->end());				 
    }

    /*!
    ** \brief Get the real world YX calibration polynomial.
    ** \return a the YX slip::MultivariatePolynomial<Type,3> calibration polynomial.
    */
    slip::MultivariatePolynomial<Type,3>& calibration_polynomial_YX()
    {
      return *calibration_polynomial_YX_;
    }
 
    /*!
    ** \brief Set the real world YX calibration polynomial equal to calibration_polynomial_Y.
    ** \param calibration_polynomial_YX a slip::MultivariatePolynomial<Type,3> corresponding to the real world YX calibration polynomial 
    ** \pre calibration_polynomial_YX.degree() == 3
    */
      void calibration_polynomial_YX(const slip::MultivariatePolynomial<Type,3>& calibration_polynomial_YX)
    {
       assert(calibration_polynomial_YX.degree() == 3);
      *(this->calibration_polynomial_YX_) = calibration_polynomial_YX;
      calibration_polynomial_YX.all_coeff(this->calibration_polynomial_coeff_YX_->begin(),
					 this->calibration_polynomial_coeff_YX_->end());				 
    }


     /*!
    ** \brief Get the real world ZX calibration polynomial.
    ** \return a the ZX slip::MultivariatePolynomial<Type,3> calibration polynomial.
    */
    slip::MultivariatePolynomial<Type,3>& calibration_polynomial_ZX()
    {
      return *calibration_polynomial_ZX_;
    }
 
    /*!
    ** \brief Set the real world ZX calibration polynomial equal to calibration_polynomial_Y.
    ** \param calibration_polynomial_ZX a slip::MultivariatePolynomial<Type,3> corresponding to the real world ZX calibration polynomial 
    ** \pre calibration_polynomial_ZX.degree() == 3
    */
      void calibration_polynomial_ZX(const slip::MultivariatePolynomial<Type,3>& calibration_polynomial_ZX)
    {
       assert(calibration_polynomial_ZX.degree() == 3);
      *(this->calibration_polynomial_ZX_) = calibration_polynomial_ZX;
      calibration_polynomial_ZX.all_coeff(this->calibration_polynomial_coeff_ZX_->begin(),
					 this->calibration_polynomial_coeff_ZX_->end());				 
    }

    /*!
    ** \brief Get the real world XY calibration polynomial.
    ** \return a the XY slip::MultivariatePolynomial<Type,3> calibration polynomial.
    */
    slip::MultivariatePolynomial<Type,3>& calibration_polynomial_XY()
    {
      return *calibration_polynomial_XY_;
    }
 
    /*!
    ** \brief Set the real world XY calibration polynomial equal to calibration_polynomial_Y.
    ** \param calibration_polynomial_XY a slip::MultivariatePolynomial<Type,3> corresponding to the real world XY calibration polynomial 
    ** \pre calibration_polynomial_XY.degree() == 3
    */
      void calibration_polynomial_XY(const slip::MultivariatePolynomial<Type,3>& calibration_polynomial_XY)
    {
       assert(calibration_polynomial_XY.degree() == 3);
      *(this->calibration_polynomial_XY_) = calibration_polynomial_XY;
      calibration_polynomial_XY.all_coeff(this->calibration_polynomial_coeff_XY_->begin(),
					 this->calibration_polynomial_coeff_XY_->end());				 
    }


     /*!
    ** \brief Get the real world ZY calibration polynomial.
    ** \return a the ZY slip::MultivariatePolynomial<Type,3> calibration polynomial.
    */
    slip::MultivariatePolynomial<Type,3>& calibration_polynomial_ZY()
    {
      return *calibration_polynomial_ZY_;
    }
 
    /*!
    ** \brief Set the real world ZY calibration polynomial equal to calibration_polynomial_Y.
    ** \param calibration_polynomial_ZY a slip::MultivariatePolynomial<Type,3> corresponding to the real world ZY calibration polynomial 
    ** \pre calibration_polynomial_ZY.degree() == 3
    */
      void calibration_polynomial_ZY(const slip::MultivariatePolynomial<Type,3>& calibration_polynomial_ZY)
    {
       assert(calibration_polynomial_ZY.degree() == 3);
      *(this->calibration_polynomial_ZY_) = calibration_polynomial_ZY;
      calibration_polynomial_ZY.all_coeff(this->calibration_polynomial_coeff_ZY_->begin(),
					 this->calibration_polynomial_coeff_ZY_->end());				 
    }
    
  
    /*!
    ** \brief Get the image x calibration polynomial.
    ** \return a the x slip::Array<Type> calibration coefficient of the polynomial.
    */
    slip::Array<Type>& calibration_polynomial_coeff_x() const
    {
      return *calibration_polynomial_coeff_x_;
    }
    /*!
    ** \brief Set the image x calibration polynomial coefficient to the range [first,last)
    ** \param first RandomAccessIterator that points to the first element of the coefficients of the x calibration polynomial.
    ** \param last RandomAccessIterator that points one-past-the-end element of the coefficients of the x calibration polynomial.
    ** \pre [last,first) is a valid range.
    ** \pre (last-first) == 20
    */
    template <typename RandomAccessIterator>
    void calibration_polynomial_coeff_x(RandomAccessIterator first,
					RandomAccessIterator last) 
    {
      assert((last-first) == 20);
      *calibration_polynomial_coeff_x_ = slip::Array<Type>(static_cast<std::size_t>(last-first),first,last);
      *calibration_polynomial_x_ = slip::MultivariatePolynomial<Type,3>(3,first,last);
      
    }
     /*!
    ** \brief Get the image y calibration polynomial.
    ** \return a the y slip::Array<Type> calibration coefficient of the polynomial.
    */
    slip::Array<Type>& calibration_polynomial_coeff_y() const
    {
      return *calibration_polynomial_coeff_y_;
    }

      /*!
    ** \brief Set the image y calibration polynomial coefficient to the range [first,last)
    ** \param first RandomAccessIterator that points to the first element of the coefficients of the y calibration polynomial.
    ** \param last RandomAccessIterator that points one-past-the-end element of the coefficients of the y calibration polynomial.
    ** \pre [last,first) is a valid range.
    ** \pre (last-first) == 20
    */
    template <typename RandomAccessIterator>
    void calibration_polynomial_coeff_y(RandomAccessIterator first,
					RandomAccessIterator last) 
    {
      assert((last-first) == 20);
      *calibration_polynomial_coeff_y_ = slip::Array<Type>(static_cast<std::size_t>(last-first),first,last);
      *calibration_polynomial_y_ = slip::MultivariatePolynomial<Type,3>(3,first,last);
      
    }
    /*!
    ** \brief Get the real world X calibration polynomial.
    ** \return a the X slip::Array<Type> calibration coefficient of the polynomial.
    */
    slip::Array<Type>& calibration_polynomial_coeff_X() const
    {
      return *calibration_polynomial_coeff_X_;
    }

      /*!
    ** \brief Set the real world X calibration polynomial coefficient to the range [first,last)
    ** \param first RandomAccessIterator that points to the first element of the coefficients of the X calibration polynomial.
    ** \param last RandomAccessIterator that points one-past-the-end element of the coefficients of the X calibration polynomial.
    ** \pre [last,first) is a valid range.
    ** \pre (last-first) == 20
    */
      template <typename RandomAccessIterator>
    void calibration_polynomial_coeff_X(RandomAccessIterator first,
					RandomAccessIterator last) 
    {
      assert((last-first) == 20);
      *calibration_polynomial_coeff_X_ = slip::Array<Type>(static_cast<std::size_t>(last-first),first,last);
      *calibration_polynomial_X_ = slip::MultivariatePolynomial<Type,3>(3,first,last);
      
    }

    /*!
    ** \brief Get the real world Y calibration polynomial.
    ** \return a the Y slip::Array<Type> calibration coefficient of the polynomial.
    */
    slip::Array<Type>& calibration_polynomial_coeff_Y() const
    {
      return *calibration_polynomial_coeff_Y_;
    }

      /*!
    ** \brief Set the real world Y calibration polynomial coefficient to the range [first,last)
    ** \param first RandomAccessIterator that points to the first element of the coefficients of the Y calibration polynomial.
    ** \param last RandomAccessIterator that points one-past-the-end element of the coefficients of the Y calibration polynomial.
    ** \pre [last,first) is a valid range.
    ** \pre (last-first) == 20
    */
    template <typename RandomAccessIterator>
    void calibration_polynomial_coeff_Y(RandomAccessIterator first,
					RandomAccessIterator last) 
    {
      assert((last-first) == 20);
      *calibration_polynomial_coeff_Y_ = slip::Array<Type>(static_cast<std::size_t>(last-first),first,last);
      *calibration_polynomial_Y_ = slip::MultivariatePolynomial<Type,3>(3,first,last);
      
    }

    /*!
    ** \brief Get the real world YX calibration polynomial.
    ** \return a the YX slip::Array<Type> calibration coefficient of the polynomial.
    */
    slip::Array<Type>& calibration_polynomial_coeff_YX() const
    {
      return *calibration_polynomial_coeff_YX_;
    }

      /*!
    ** \brief Set the real world YX calibration polynomial coefficient to the range [first,last)
    ** \param first RandomAccessIterator that points to the first element of the coefficients of the YX calibration polynomial.
    ** \param last RandomAccessIterator that points one-past-the-end element of the coefficients of the YX calibration polynomial.
    ** \pre [last,first) is a valid range.
    ** \pre (last-first) == 20
    */
    template <typename RandomAccessIterator>
    void calibration_polynomial_coeff_YX(RandomAccessIterator first,
					RandomAccessIterator last) 
    {
      assert((last-first) == 20);
      *calibration_polynomial_coeff_YX_ = slip::Array<Type>(static_cast<std::size_t>(last-first),first,last);
      *calibration_polynomial_YX_ = slip::MultivariatePolynomial<Type,3>(3,first,last);
      
    }

   /*!
    ** \brief Get the real world ZX calibration polynomial.
    ** \return a the ZX slip::Array<Type> calibration coefficient of the polynomial.
    */
    slip::Array<Type>& calibration_polynomial_coeff_ZX() const
    {
      return *calibration_polynomial_coeff_ZX_;
    }

      /*!
    ** \brief Set the real world ZX calibration polynomial coefficient to the range [first,last)
    ** \param first RandomAccessIterator that points to the first element of the coefficients of the ZX calibration polynomial.
    ** \param last RandomAccessIterator that points one-past-the-end element of the coefficients of the ZX calibration polynomial.
    ** \pre [last,first) is a valid range.
    ** \pre (last-first) == 20
    */
    template <typename RandomAccessIterator>
    void calibration_polynomial_coeff_ZX(RandomAccessIterator first,
					RandomAccessIterator last) 
    {
      assert((last-first) == 20);
      *calibration_polynomial_coeff_ZX_ = slip::Array<Type>(static_cast<std::size_t>(last-first),first,last);
      *calibration_polynomial_ZX_ = slip::MultivariatePolynomial<Type,3>(3,first,last);
      
    }

     /*!
    ** \brief Get the real world XY calibration polynomial.
    ** \return a the XY slip::Array<Type> calibration coefficient of the polynomial.
    */
    slip::Array<Type>& calibration_polynomial_coeff_XY() const
    {
      return *calibration_polynomial_coeff_XY_;
    }

      /*!
    ** \brief Set the real world XY calibration polynomial coefficient to the range [first,last)
    ** \param first RandomAccessIterator that points to the first element of the coefficients of the XY calibration polynomial.
    ** \param last RandomAccessIterator that points one-past-the-end element of the coefficients of the XY calibration polynomial.
    ** \pre [last,first) is a valid range.
    ** \pre (last-first) == 20
    */
    template <typename RandomAccessIterator>
    void calibration_polynomial_coeff_XY(RandomAccessIterator first,
					RandomAccessIterator last) 
    {
      assert((last-first) == 20);
      *calibration_polynomial_coeff_XY_ = slip::Array<Type>(static_cast<std::size_t>(last-first),first,last);
      *calibration_polynomial_XY_ = slip::MultivariatePolynomial<Type,3>(3,first,last);
      
    }

    /*!
    ** \brief Get the real world ZY calibration polynomial.
    ** \return a the ZY slip::Array<Type> calibration coefficient of the polynomial.
    */
    slip::Array<Type>& calibration_polynomial_coeff_ZY() const
    {
      return *calibration_polynomial_coeff_ZY_;
    }

      /*!
    ** \brief Set the real world ZY calibration polynomial coefficient to the range [first,last)
    ** \param first RandomAccessIterator that points to the first element of the coefficients of the ZY calibration polynomial.
    ** \param last RandomAccessIterator that points one-past-the-end element of the coefficients of the ZY calibration polynomial.
    ** \pre [last,first) is a valid range.
    ** \pre (last-first) == 20
    */
    template <typename RandomAccessIterator>
    void calibration_polynomial_coeff_ZY(RandomAccessIterator first,
					RandomAccessIterator last) 
    {
      assert((last-first) == 20);
      *calibration_polynomial_coeff_ZY_ = slip::Array<Type>(static_cast<std::size_t>(last-first),first,last);
      *calibration_polynomial_ZY_ = slip::MultivariatePolynomial<Type,3>(3,first,last);
      
    }
    
    /*@} End Accessors/Mutators*/

    /**
     ** \name  Projection methods
     */
    /*@{*/
    /*!
    ** \brief Computes the projection of a 3d world point
    ** onto the image plane.
    ** \param p %Point3d.
    ** \return %Point2d
    ** \par Complexity: (19*,19+)
    */
    inline
    slip::Point2d<Type> projection(const slip::Point3d<Type>& p) const
    {
           
      const Type px =  
           (*calibration_polynomial_coeff_x_)[0] 
 	+ p[0]*(   (*calibration_polynomial_coeff_x_)[1]
 		+ p[0]*((*calibration_polynomial_coeff_x_)[2] + (*calibration_polynomial_coeff_x_)[3]*p[0])
		   + p[1]*((*calibration_polynomial_coeff_x_)[5] +
			   (*calibration_polynomial_coeff_x_)[6] *p[0] +
			   (*calibration_polynomial_coeff_x_)[8] *p[1] +
			   (*calibration_polynomial_coeff_x_)[14]*p[2])
		   + p[2]*((*calibration_polynomial_coeff_x_)[11] +
			   (*calibration_polynomial_coeff_x_)[12]*p[0] +
			   (*calibration_polynomial_coeff_x_)[17]*p[2]))
 	+ p[1] * ((*calibration_polynomial_coeff_x_)[4] + 
		  p[1]*((*calibration_polynomial_coeff_x_)[7]+
			(*calibration_polynomial_coeff_x_)[9]*p[1])) 
 	+ p[2] * ((*calibration_polynomial_coeff_x_)[10] +
		  p[1]*((*calibration_polynomial_coeff_x_)[13]   +
			(*calibration_polynomial_coeff_x_)[15]*p[1] +
			(*calibration_polynomial_coeff_x_)[18]*p[2]) +
		  p[2]*((*calibration_polynomial_coeff_x_)[16] +
			(*calibration_polynomial_coeff_x_)[19]*p[2]));
      const Type py =  
           (*calibration_polynomial_coeff_y_)[0] 
 	+ p[0]*(   (*calibration_polynomial_coeff_y_)[1]
 		+ p[0]*((*calibration_polynomial_coeff_y_)[2] + (*calibration_polynomial_coeff_y_)[3]*p[0])
 		+ p[1]*((*calibration_polynomial_coeff_y_)[5] +
 		     (*calibration_polynomial_coeff_y_)[6] *p[0] +
 		     (*calibration_polynomial_coeff_y_)[8] *p[1] +
 		     (*calibration_polynomial_coeff_y_)[14]*p[2])
 		+ p[2]*((*calibration_polynomial_coeff_y_)[11] +
 		     (*calibration_polynomial_coeff_y_)[12]*p[0] +
 		     (*calibration_polynomial_coeff_y_)[17]*p[2]))
 	+ p[1] * ((*calibration_polynomial_coeff_y_)[4] + 
 	      p[1]*((*calibration_polynomial_coeff_y_)[7]+
 		 (*calibration_polynomial_coeff_y_)[9]*p[1])) 
 	+ p[2] * ((*calibration_polynomial_coeff_y_)[10] +
 	       p[1]*((*calibration_polynomial_coeff_y_)[13]   +
 		  (*calibration_polynomial_coeff_y_)[15]*p[1] +
 		  (*calibration_polynomial_coeff_y_)[18]*p[2]) +
 	       p[2]*((*calibration_polynomial_coeff_y_)[16] +
 		  (*calibration_polynomial_coeff_y_)[19]*p[2]));
   
      return slip::Point2d<Type>(px,py);
    }


     /*!
    ** \brief Computes the projection gradient of a 3d world point
    ** onto the image plane.
    ** \param p %Point3d.
    ** \param delta spatial derivative step.
    ** \return %Matrix
    */
    inline
    slip::Matrix<Type> projection_gradient(const slip::Point3d<Type>& pt,
					   const Type& delta = slip::constants<Type>::one()) const
    {
      slip::Matrix<Type> res(2,3);
      const slip::MultivariatePolynomial<Type,3> Px =  (*calibration_polynomial_x_);
      const slip::MultivariatePolynomial<Type,3> Py =  (*calibration_polynomial_y_);
     
      slip::MultivariatePolynomial<Type,3> dPxdx = Px.derivative(0);
      slip::MultivariatePolynomial<Type,3> dPydx = Py.derivative(0);
      slip::MultivariatePolynomial<Type,3> dPxdy = Px.derivative(1);
      slip::MultivariatePolynomial<Type,3> dPydy = Py.derivative(1);
      slip::MultivariatePolynomial<Type,3> dPxdz = Px.derivative(2);
      slip::MultivariatePolynomial<Type,3> dPydz = Py.derivative(2);
      
      res[0][0] = dPxdx.evaluate(pt.coord().begin(),pt.coord().end());
      res[1][0] = dPydx.evaluate(pt.coord().begin(),pt.coord().end());
      res[0][1] = dPxdy.evaluate(pt.coord().begin(),pt.coord().end());
      res[1][1] = dPydy.evaluate(pt.coord().begin(),pt.coord().end());
      res[0][2] = dPxdz.evaluate(pt.coord().begin(),pt.coord().end());
      res[1][2] = dPydz.evaluate(pt.coord().begin(),pt.coord().end());

      return res;
    }

     /*!
    ** \brief Computes the projection gradient of a 3d world point
    ** onto the image plane.
    ** \param p %Point3d.
    ** \param delta .
    ** \return %Matrix
    */
    inline
    slip::Matrix<Type> numerical_projection_gradient(const slip::Point3d<Type>& pt,
							     const Type& delta=slip::constants<Type>::one()) const
    {
      return base::projection_gradient(pt, delta);
    }
    
    /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection of an image point.
    ** \param p %Point2d.
    ** \param z Depth coordinate.
    ** \return %Point3d
    ** \par Complexity: (19*,19+)
    */
    inline
    slip::Point3d<Type> backprojection(const slip::Point2d<Type>& p2, 
 	 			       const Type& z) const
    {
      
      
       const Type pX =  
           (*calibration_polynomial_coeff_X_)[0] 
 	+ p2[0]*(   (*calibration_polynomial_coeff_X_)[1]
 		+ p2[0]*((*calibration_polynomial_coeff_X_)[2] + (*calibration_polynomial_coeff_X_)[3]*p2[0])
 		+ p2[1]*((*calibration_polynomial_coeff_X_)[5] +
 		     (*calibration_polynomial_coeff_X_)[6] *p2[0] +
 		     (*calibration_polynomial_coeff_X_)[8] *p2[1] +
 		     (*calibration_polynomial_coeff_X_)[14]*z)
 		+ z*((*calibration_polynomial_coeff_X_)[11] +
 		     (*calibration_polynomial_coeff_X_)[12]*p2[0] +
 		     (*calibration_polynomial_coeff_X_)[17]*z))
 	+ p2[1] * ((*calibration_polynomial_coeff_X_)[4] + 
 	      p2[1]*((*calibration_polynomial_coeff_X_)[7]+
 		 (*calibration_polynomial_coeff_X_)[9]*p2[1])) 
 	+ z * ((*calibration_polynomial_coeff_X_)[10] +
 	       p2[1]*((*calibration_polynomial_coeff_X_)[13]   +
 		  (*calibration_polynomial_coeff_X_)[15]*p2[1] +
 		  (*calibration_polynomial_coeff_X_)[18]*z) +
 	       z*((*calibration_polynomial_coeff_X_)[16] +
 		  (*calibration_polynomial_coeff_X_)[19]*z));
       const Type pY =  
           (*calibration_polynomial_coeff_Y_)[0] 
 	+ p2[0]*(   (*calibration_polynomial_coeff_Y_)[1]
 		+ p2[0]*((*calibration_polynomial_coeff_Y_)[2] + (*calibration_polynomial_coeff_Y_)[3]*p2[0])
 		+ p2[1]*((*calibration_polynomial_coeff_Y_)[5] +
 		     (*calibration_polynomial_coeff_Y_)[6] *p2[0] +
 		     (*calibration_polynomial_coeff_Y_)[8] *p2[1] +
 		     (*calibration_polynomial_coeff_Y_)[14]*z)
 		+ z*((*calibration_polynomial_coeff_Y_)[11] +
 		     (*calibration_polynomial_coeff_Y_)[12]*p2[0] +
 		     (*calibration_polynomial_coeff_Y_)[17]*z))
 	+ p2[1] * ((*calibration_polynomial_coeff_Y_)[4] + 
 	      p2[1]*((*calibration_polynomial_coeff_Y_)[7]+
 		 (*calibration_polynomial_coeff_Y_)[9]*p2[1])) 
 	+ z * ((*calibration_polynomial_coeff_Y_)[10] +
 	       p2[1]*((*calibration_polynomial_coeff_Y_)[13]   +
 		  (*calibration_polynomial_coeff_Y_)[15]*p2[1] +
 		  (*calibration_polynomial_coeff_Y_)[18]*z) +
 	       z*((*calibration_polynomial_coeff_Y_)[16] +
 		  (*calibration_polynomial_coeff_Y_)[19]*z));
   
       return slip::Point3d<Type>(pX,pY,z);
    }


  

     /*!
    ** \brief Computes the 3d world point corresponding to the ray backprojection of an image point on a X plane
    ** \param p2 %Point2d.
    ** \param X plane x coordinate.
    ** \return backprojected point.
    */
    inline
    slip::Point3d<Type> X_backprojection(const slip::Point2d<Type>& p2, 
					 const Type& X) const
    {
      
      const slip::Point3d<Type> pt(p2[0],p2[1],X);
      const Type pYX =  this->eval_poly_horner(*calibration_polynomial_coeff_YX_, pt);
      const Type pZX =  this->eval_poly_horner(*calibration_polynomial_coeff_ZX_, pt);
      return slip::Point3d<Type>(X,pYX,pZX);
    }
    
    /*!
    ** \brief Computes the 3d world point corresponding to the backprojection of an image point on a X plane and test if it is in the square.
    ** \param p2 %Point2d.
    ** \param X plane x coordinate.
    ** \param ymin minimum coordinate along y.
    ** \param ymax maximum coordinate along y.
    ** \param zmin minimum coordinate along z.
    ** \param zmax maximum coordinate along z.
    ** \param p3d backprojected point.
    ** \return boolean if point exists in the square.
    */
    inline
    bool X_backprojection_in_square(const slip::Point2d<Type>& p2, 
				    const Type& X,
				    const Type &ymin,
				    const Type &ymax,
				    const Type &zmin,
				    const Type &zmax,
				    slip::Point3d<Type> &p3d) const
    {
      bool res = false;
      p3d = this->X_backprojection(p2, X);
      if (p3d[1]>=ymin && p3d[1]<=ymax && p3d[2]>=zmin && p3d[2]<=zmax)
	{
	  res = true;
	}
      
      return res;
    }

     /*!
    ** \brief Computes the 3d world point corresponding to the backprojection of an image point on a Y plane
    ** \param p2 %Point2d.
    ** \param y plane y coordinate.
    ** \return backprojected point
    */
    inline
    slip::Point3d<Type> Y_backprojection(const slip::Point2d<Type>& p2, 
					 const Type& Y) const
    {
      slip::Point3d<Type> pt(p2[0],p2[1],Y);

      const Type pXY = this->eval_poly_horner(*calibration_polynomial_coeff_XY_, pt);
      const Type pZY = this->eval_poly_horner(*calibration_polynomial_coeff_XY_, pt);
      
      return slip::Point3d<Type>(pXY,Y,pZY);
    }

    /*!
    ** \brief Computes the 3d world point corresponding to the backprojection of an image point on a Y plane and test if it is in the square.
    ** \param p2 %Point2d.
    ** \param Y plane y coordinate.
    ** \param xmin minimum coordinate along x.
    ** \param xmax maximum coordinate along x.
    ** \param zmin minimum coordinate along z.
    ** \param zmax maximum coordinate along z.
    ** \param p3d backprojected point.
    ** \return boolean if point exists in the square.
    */
    inline
    bool Y_backprojection_in_square(const slip::Point2d<Type>& p2, 
				    const Type& Y,
				    const Type &xmin,
				    const Type &xmax,
				    const Type &zmin,
				    const Type &zmax,
				    slip::Point3d<Type> &p3d) const
    {
      bool res = false;
      p3d = this->Y_backprojection(p2, Y);      
      if (p3d[0]>=xmin && p3d[0]<=xmax && p3d[2]>=zmin && p3d[2]<=zmax)
	{
	  res = true;
	}
      return res;
    }

       /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection of an image point.
    ** \param p %Point2d.
    ** \param Z Depth coordinate.
    ** \return %Point3d
    ** \par Complexity: (19*,19+)
    */
    inline
    slip::Point3d<Type> Z_backprojection(const slip::Point2d<Type>& p2, 
					 const Type& Z) const
    {
      return this->backprojection(p2,Z);
    }

     /*!
    ** \brief Computes the 3d world point corresponding to the backprojection of an image point on a Z plane and test if it is in the square.
    ** \param p2 %Point2d.
    ** \param Z plane z coordinate.
    ** \param xmin minimum coordinate along x.
    ** \param xmax maximum coordinate along x.
    ** \param ymin minimum coordinate along y.
    ** \param ymax maximum coordinate along y.
    ** \param p3d backprojected point.
    ** \return boolean if point exists in the square.
    */
    inline
    bool Z_backprojection_in_square(const slip::Point2d<Type>& p2, 
				    const Type& Z,
				    const Type &xmin,
				    const Type &xmax,
				    const Type &ymin,
				    const Type &ymax,
				    slip::Point3d<Type> &p3d) const

    {
      bool res = false;
      p3d = this->Z_backprojection(p2, Z);
      if (p3d[0]>=xmin && p3d[0]<=xmax && p3d[1]>=ymin && p3d[1]<=ymax)
	{
	  res = true;
	}
      return res;
    }
    
    /*@} End projection methods*/
    

    /**
     ** \name  Computation methods
     */
    /*@{*/
    /*!
    ** \brief Computes the parameters of the Soloff camera model
    ** \param P Matrix containing the input data.
    ** \remarks Format:
    ** \f$(x,y)\f$ are the image coordinates, \f$(X,Y,Z)\f$ are the world coordinates.
    ** \f[
    ** \begin{array}{ccccc}
    ** \cdots & \cdots & \cdots & \cdots & \cdots \\
    ** x_{i} & y_{i} & X_{i} & Y_{i} & Z_{i} \\
    ** \cdots & \cdots & \cdots & \cdots & \cdots \\
    ** \end{array}
    ** \f]
    */
    void compute(const slip::Matrix<Type>& P) 
    {
      //getpars_SoloffUV(P,*calibration_polynomial_x_,*calibration_polynomial_y_);
      //getpars_SoloffXY(P,*calibration_polynomial_X_,*calibration_polynomial_Y_);
      //computes calibration_polynomial_x_ and calibration_polynomial_y_
      slip::computes_SoloffUV(P,
			      *calibration_polynomial_x_,
			      *calibration_polynomial_y_);
      //computes calibration_polynomial_XZ_ (calibration_polynomial_X_)
      //and calibration_polynomial_YZ_ (calibration_polynomial_Y_)
      slip::computes_SoloffXY(P,
			      *calibration_polynomial_X_,
			      *calibration_polynomial_Y_);
      //computes calibration_polynomial_XY_ and calibration_polynomial_ZY_
      slip::computes_SoloffXZ(P,
			      *calibration_polynomial_XY_,
			      *calibration_polynomial_ZY_);
      
      //computes calibration_polynomial_YX_ and  calibration_polynomial_ZX_
      slip::computes_SoloffYZ(P,
			      *calibration_polynomial_YX_,
			      *calibration_polynomial_ZX_);
      
   
      
    
      calibration_polynomial_x_->all_coeff(calibration_polynomial_coeff_x_->begin(),
					   calibration_polynomial_coeff_x_->end());
      calibration_polynomial_y_->all_coeff(calibration_polynomial_coeff_y_->begin(),
					 calibration_polynomial_coeff_y_->end());
      calibration_polynomial_X_->all_coeff(calibration_polynomial_coeff_X_->begin(),
					   calibration_polynomial_coeff_X_->end());
      calibration_polynomial_Y_->all_coeff(calibration_polynomial_coeff_Y_->begin(),
					   calibration_polynomial_coeff_Y_->end());
      calibration_polynomial_YX_->all_coeff(calibration_polynomial_coeff_YX_->begin(),
					    calibration_polynomial_coeff_YX_->end());
      calibration_polynomial_ZX_->all_coeff(calibration_polynomial_coeff_ZX_->begin(),
					    calibration_polynomial_coeff_ZX_->end());
      calibration_polynomial_XY_->all_coeff(calibration_polynomial_coeff_XY_->begin(),
					    calibration_polynomial_coeff_XY_->end());
      calibration_polynomial_ZY_->all_coeff(calibration_polynomial_coeff_ZY_->begin(),
					    calibration_polynomial_coeff_ZY_->end());
 
    }

    /*@} End computation methods*/

    /**
     ** \name  input/output methods
     */
    /*@{*/
    /*!
    ** \brief Read a multivariate polynomial from an ASCII file
    **
    ** \param file File path name.
    ** \remarks Format:
    ** \f$(Px_i)\f$ are the coefficients of the polynomial model for the x camera component \f$(Py_i)\f$ are the coefficients of the polynomial model for the y camera component
    \f$(PX_i)\f$ are the coefficients of the polynomial model for the X world component \f$(PY_i)\f$ are the coefficients of the polynomial model for the Y world component
    ** \begin{array}{cccccccc}
    ** Px_{1} & Py_{1} & PX_{1} & PY_{1}& PYX_{1}&PZX_{1}&PXY_{1}&PZY_{1}\\
    ** \cdots & \cdots & \cdots & \cdots& \cdots & \cdots & \cdots&\cdots \\
    ** Px_{20} & Py_{20} & PX_{20} & PY_{20}& PYX_{20}&PZX_{20}&PXY_{1}&PZY_{20}\\
    ** \end{array}
    ** \f]
    */
    void read(const std::string& file)
    {
      slip::Array2d<double> F;
      slip::read_ascii_2d(F,file);
      const std::size_t nc = 20;
      slip::Vector<Type> mu(nc), mv(nc),
	mX(nc), mY(nc),
	mYX(nc), mZX(nc),
	mXY(nc), mZY(nc);
    
      //Px Py PX PY PYX PZX PXY PZY
      for(std::size_t i=0; i < nc; i++)
	{
	  mu[i]  = F[i][0];
	  mv[i]  = F[i][1];
	  mX[i]  = F[i][2];
	  mY[i]  = F[i][3];
	  mYX[i] = F[i][4];
	  mZX[i] = F[i][5];
	  mXY[i] = F[i][6];
	  mZY[i] = F[i][7];
	
	}
    
     
      slip::MultivariatePolynomial<Type,3> Pu(3,mu.begin(),mu.end());
      slip::MultivariatePolynomial<Type,3> Pv(3,mv.begin(),mv.end());
      slip::MultivariatePolynomial<Type,3> PX(3,mX.begin(),mX.end());
      slip::MultivariatePolynomial<Type,3> PY(3,mY.begin(),mY.end());
      slip::MultivariatePolynomial<Type,3> PYX(3,mYX.begin(),mYX.end());
      slip::MultivariatePolynomial<Type,3> PZX(3,mZX.begin(),mZX.end());
      slip::MultivariatePolynomial<Type,3> PXY(3,mXY.begin(),mXY.end());
      slip::MultivariatePolynomial<Type,3> PZY(3,mZY.begin(),mZY.end());
   
      *calibration_polynomial_x_ = Pu;
      *calibration_polynomial_y_ = Pv;
      *calibration_polynomial_X_ = PX;
      *calibration_polynomial_Y_ = PY;
      *calibration_polynomial_YX_ = PYX;
      *calibration_polynomial_ZX_ = PZX;
      *calibration_polynomial_XY_ = PYX;
      *calibration_polynomial_ZY_ = PZX;
      std::copy(mu.begin(),mu.end(),calibration_polynomial_coeff_x_->begin());
      std::copy(mv.begin(),mv.end(),calibration_polynomial_coeff_y_->begin());
      std::copy(mX.begin(),mX.end(),calibration_polynomial_coeff_X_->begin());
      std::copy(mY.begin(),mY.end(),calibration_polynomial_coeff_Y_->begin());
      std::copy(mYX.begin(),mYX.end(),calibration_polynomial_coeff_YX_->begin());
      std::copy(mZX.begin(),mZX.end(),calibration_polynomial_coeff_ZX_->begin());
      std::copy(mXY.begin(),mXY.end(),calibration_polynomial_coeff_XY_->begin());
      std::copy(mZY.begin(),mZY.end(),calibration_polynomial_coeff_ZY_->begin());
    }
    /*!
    ** \brief Write a multivariate polynomial to an ASCII file
    **
    ** \param file File path name.
    ** \remarks Format:
     ** \f$(Px_i)\f$ are the coefficients of the polynomial model for the x camera component \f$(Py_i)\f$ are the coefficients of the polynomial model for the y camera component
    \f$(PX_i)\f$ are the coefficients of the polynomial model for the X world component \f$(PY_i)\f$ are the coefficients of the polynomial model for the Y world component
    ** \begin{array}{cccccccc}
    ** Px_{1} & Py_{1} & PX_{1} & PY_{1}& PYX_{1}&PZX_{1}&PXY_{1}&PZY_{1}\\
    ** \cdots & \cdots & \cdots & \cdots& \cdots & \cdots & \cdots&\cdots \\
    ** Px_{20} & Py_{20} & PX_{20} & PY_{20}& PYX_{20}&PZX_{20}&PXY_{1}&PZY_{20}\\
    ** \end{array}
    ** \f]
    */
    void write(const std::string& file) {

      std::ofstream output(file.c_str(),std::ios::out);
      slip::Vector<Type> mu(20);
      slip::Vector<Type> mv(20);
      slip::Vector<Type> mX(20);
      slip::Vector<Type> mY(20);
      slip::Vector<Type> mYX(20);
      slip::Vector<Type> mZX(20);
      slip::Vector<Type> mXY(20);
      slip::Vector<Type> mZY(20);
     
      calibration_polynomial_x_->all_coeff(mu.begin(),mu.end());
      calibration_polynomial_y_->all_coeff(mv.begin(),mv.end());
      calibration_polynomial_X_->all_coeff(mX.begin(),mX.end());
      calibration_polynomial_Y_->all_coeff(mY.begin(),mY.end());
      calibration_polynomial_YX_->all_coeff(mYX.begin(),mYX.end());
      calibration_polynomial_ZX_->all_coeff(mZX.begin(),mZX.end());
      calibration_polynomial_XY_->all_coeff(mXY.begin(),mXY.end());
      calibration_polynomial_ZY_->all_coeff(mZY.begin(),mZY.end());
     
      for(int i=0;i<20; i++)
	{
	  output<<mu[i]<<" "<<mv[i]<<" "<<mX[i]<<" "<<mY[i]<<" "<<mYX[i]<<" "<<mZX[i]<<" "<<mXY[i]<<" "<<mZY[i]<<std::endl;
	}
      output.close();

    }
    /*@} End input/output methods*/
    //protected:
    slip::MultivariatePolynomial<Type,3> *calibration_polynomial_x_;
    slip::MultivariatePolynomial<Type,3> *calibration_polynomial_y_;
    slip::MultivariatePolynomial<Type,3> *calibration_polynomial_X_;
    slip::MultivariatePolynomial<Type,3> *calibration_polynomial_Y_;
    slip::MultivariatePolynomial<Type,3> *calibration_polynomial_YX_;
    slip::MultivariatePolynomial<Type,3> *calibration_polynomial_ZX_;
    slip::MultivariatePolynomial<Type,3> *calibration_polynomial_XY_;
    slip::MultivariatePolynomial<Type,3> *calibration_polynomial_ZY_;
   
    slip::Array<Type>* calibration_polynomial_coeff_x_;
    slip::Array<Type>* calibration_polynomial_coeff_y_;
    slip::Array<Type>* calibration_polynomial_coeff_X_;
    slip::Array<Type>* calibration_polynomial_coeff_Y_;
    slip::Array<Type>* calibration_polynomial_coeff_YX_;
    slip::Array<Type>* calibration_polynomial_coeff_ZX_;
    slip::Array<Type>* calibration_polynomial_coeff_XY_;
    slip::Array<Type>* calibration_polynomial_coeff_ZY_;

    std::ostream& display(std::ostream& out) const
    {
      slip::MultivariatePolynomial<Type,3> MPx=*calibration_polynomial_x_;
      slip::MultivariatePolynomial<Type,3> MPy=*calibration_polynomial_y_;
      slip::MultivariatePolynomial<Type,3> MPX=*calibration_polynomial_X_;
      slip::MultivariatePolynomial<Type,3> MPY=*calibration_polynomial_Y_;
      slip::MultivariatePolynomial<Type,3> MPYX=*calibration_polynomial_YX_;
      slip::MultivariatePolynomial<Type,3> MPZX=*calibration_polynomial_ZX_;
      slip::MultivariatePolynomial<Type,3> MPXY=*calibration_polynomial_XY_;
      slip::MultivariatePolynomial<Type,3> MPZY=*calibration_polynomial_ZY_;

    out<<MPx<<std::endl;
    out<<MPy<<std::endl;
    out<<MPX<<std::endl;
    out<<MPY<<std::endl;
    out<<MPYX<<std::endl;
    out<<MPZX<<std::endl;
    out<<MPXY<<std::endl;
    out<<MPZY<<std::endl;
    return out;
    }
    
  private:
        friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::CameraModel<Type> >(*this);
	  ar & calibration_polynomial_x_;
	  ar & calibration_polynomial_y_;
	  ar & calibration_polynomial_X_;
	  ar & calibration_polynomial_Y_;
	  ar & calibration_polynomial_YX_;
	  ar & calibration_polynomial_ZX_;
	  ar & calibration_polynomial_XY_;
	  ar & calibration_polynomial_ZY_;
	  ar & calibration_polynomial_coeff_x_;
	  ar & calibration_polynomial_coeff_y_;
	  ar & calibration_polynomial_coeff_X_;
	  ar & calibration_polynomial_coeff_Y_;
	  ar & calibration_polynomial_coeff_YX_;
	  ar & calibration_polynomial_coeff_ZX_;
	  ar & calibration_polynomial_coeff_XY_;
	  ar & calibration_polynomial_coeff_ZY_;
	}
      
    }
  template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::CameraModel<Type> >(*this);
	  ar & calibration_polynomial_x_;
	  ar & calibration_polynomial_y_;
	  ar & calibration_polynomial_X_;
	  ar & calibration_polynomial_Y_;
	  ar & calibration_polynomial_YX_;
	  ar & calibration_polynomial_ZX_;
	  ar & calibration_polynomial_XY_;
	  ar & calibration_polynomial_ZY_;
	  ar & calibration_polynomial_coeff_x_;
	  ar & calibration_polynomial_coeff_y_;
	  ar & calibration_polynomial_coeff_X_;
	  ar & calibration_polynomial_coeff_Y_;
	  ar & calibration_polynomial_coeff_YX_;
	  ar & calibration_polynomial_coeff_ZX_;
	  ar & calibration_polynomial_coeff_XY_;
	  ar & calibration_polynomial_coeff_ZY_;
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()


   Type eval_poly_horner(const slip::Array<Type>& coeff,
			 const slip::Point3d<Type>& p) const
    {
       const Type val =  
	 (coeff)[0] 
	 + p[0]*(   (coeff)[1]
		    + p[0]*((coeff)[2] + (coeff)[3]*p[0])
		    + p[1]*((coeff)[5] +
			    (coeff)[6] *p[0] +
			    (coeff)[8] *p[1] +
			    (coeff)[14]*p[2])
		    + p[2]*((coeff)[11] +
			    (coeff)[12]*p[0] +
			    (coeff)[17]*p[2]))
	 + p[1] * ((coeff)[4] + 
		   p[1]*((coeff)[7]+
			 (coeff)[9]*p[1])) 
	 + p[2] * ((coeff)[10] +
		   p[1]*((coeff)[13]   +
			 (coeff)[15]*p[1] +
			 (coeff)[18]*p[2]) +
		   p[2]*((coeff)[16] +
			 (coeff)[19]*p[2]));
       return val;
    }


     
    
  };

}//slip::


namespace slip {

  template<typename Type>
  std::ostream& operator<<(std::ostream & out, const slip::SoloffCamera<Type>& c) {
    return c.display(out);
  }

}//slip::
#endif //SLIP_SOLOFFCAMERA_HPP
