/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file neighbors.hpp
 * 
 * \brief Provides a class to manipulate neighbourhood configurations.
 * 
 */
#ifndef SLIP_NEIGHBORS_HPP
#define SLIP_NEIGHBORS_HPP

#include "DPoint1d.hpp"
#include "DPoint2d.hpp"
#include "DPoint3d.hpp"
#include "DPoint4d.hpp"
#include "Block.hpp"

namespace slip
{
  /** 
      \enum NEIGHBOURHOOD
      \brief Choose between different neighbourhood 
      \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
      \date 2013/11/12
      \code
      enum  
      {
      //unidimensional connexity neighbourhood
      N_2_C
      // 4-connexity neighbourhood
      N_4_C, 
      // hexagonal odd connexity neighbourhood
      N_6o_C
      // hexagonal even connexity neighbourhood
      N_6e_C
      // 8-connexity neighbourhood
      N_8_C
      // 6_connexity 3d neighbourhood
      N_6_C
      // 18_connexity 3d neighbourhood
      N_18_C
      // 26_connexity 3d neighbourhood
      N_26_C
      // 8_connexity 4d neighbourhood (added for Fluex V1.0)
      N_4D_8_C
      };
      \endcode
      \image html connexity.jpg "Connexity neighbourghood conventions"
      \image latex connexity.eps "Connexity neighbourghood conventions" width=10cm
  */
  
  
  enum NEIGHBOURHOOD
    {
      N_2_C, 
      N_4_C, 
      N_6o_C,
      N_6e_C,
      N_8_C,
      N_6_C,
      N_18_C,
      N_26_C,
      N_4D_8_C
    };


  /**
   ** \name  Some useful 1d-neighborhood configurations 
   */ 
  /*@{*/
  static slip::block<int,2> n_2c = {{1,-1}};
  
  static slip::block<int,1> prev_2c = {{-1}};
 
  static slip::block<int,1> next_2c = {{1}};



  /*@} End useful 1d-neighborhood configurations*/

  /**
   ** \name  Some useful 2d-neighborhood configurations 
   */ 
  /*@{*/
 /*!
 ** \brief 4 connexity freeman neighborhood displacements
 ** \par 4 connexity freeman neighborhood displacements
 ** \image html neighbors2d_4c.png "Neighbors displacements order"
 ** \image latex neighbors2d_4c.eps "Neighbors displacements order" width=5cm

  */
  
static slip::block<slip::DPoint2d<int>,4> n_4c = 
  {{slip::DPoint2d<int>(0,1),
   slip::DPoint2d<int>(-1,0),
   slip::DPoint2d<int>(0,-1),
   slip::DPoint2d<int>(1,0)}};

  /*!
  ** \brief 8 connexity freeman neighborhood displacements
  ** \par 8 connexity freeman neighborhood displacements
  ** \image html neighbors2d_8c.jpg "Neighbors displacements order"
  ** \image latex neighbors2d_8c.eps "Neighbors displacements order" width=5cm

  */

static slip::block<slip::DPoint2d<int>,8> n_8c = 
  {{slip::DPoint2d<int>(0,1),
   slip::DPoint2d<int>(-1,1),
   slip::DPoint2d<int>(-1,0),
   slip::DPoint2d<int>(-1,-1),
   slip::DPoint2d<int>(0,-1),
   slip::DPoint2d<int>(1,-1),
   slip::DPoint2d<int>(1,0),
   slip::DPoint2d<int>(1,1)}};

   /*!
  ** \brief 6 connexity odd freeman neighborhood displacements
  ** \image html neighbors2d_6oc.png "6 connexity odd neighbors displacements order"
  ** \image latex neighbors2d_6oc.eps "6 connexity odd neighbors displacements order" width=5cm

  */

static slip::block<slip::DPoint2d<int>,6> n_6co = 
  {{slip::DPoint2d<int>(0,1),
   slip::DPoint2d<int>(-1,0),
   slip::DPoint2d<int>(-1,-1),
   slip::DPoint2d<int>(0,-1),
   slip::DPoint2d<int>(1,-1),
   slip::DPoint2d<int>(1,0)}};


 /*!
  ** \brief 6 connexity even freeman neighborhood displacements
   ** \image html neighbors2d_6ec.png "6 connexity even neighbors displacements order"
  ** \image latex neighbors2d_6ec.eps "6 connexity even neighbors displacements order" width=5cm
  */

  static slip::block<slip::DPoint2d<int>,6> n_6ce = 
  {{slip::DPoint2d<int>(0,1),
   slip::DPoint2d<int>(-1,1),
   slip::DPoint2d<int>(-1,0),
   slip::DPoint2d<int>(0,-1),
   slip::DPoint2d<int>(1,0),
   slip::DPoint2d<int>(1,1)}};


 /*!
  ** \brief 4 connexity previous displacements
   ** \image html prev_4c.png "4 connexity previous displacements order"
  ** \image latex prev_4c.eps "4 connexity previous displacements order" width=5cm
  */

static slip::block<slip::DPoint2d<int>,2> prev_4c = 
  {{slip::DPoint2d<int>(-1,0),
   slip::DPoint2d<int>(0,-1)}};

  /*!
  ** \brief 4 connexity next displacements
  ** \image html next_4c.png "4 connexity next displacements order"
  ** \image latex next_4c.eps "4 connexity next displacements order" width=5cm
  */
static slip::block<slip::DPoint2d<int>,2> next_4c = 
  {{slip::DPoint2d<int>(0,1),
   slip::DPoint2d<int>(1,0)}};

  /*!
  ** \brief 8 connexity previous displacements
  ** \image html prev_8c.png "8 connexity previous displacements order"
  ** \image latex prev_8c.eps "8 connexity previous displacements order" width=5cm
  */
static slip::block<slip::DPoint2d<int>,4> prev_8c = 
  {{slip::DPoint2d<int>(-1,1),
   slip::DPoint2d<int>(-1,0),
   slip::DPoint2d<int>(-1,-1),
   slip::DPoint2d<int>(0,-1)}};

  /*!
  ** \brief 8 connexity next displacements
  ** \image html next_8c.png "8 connexity next displacements order"
  ** \image latex next_8c.eps "8 connexity next displacements order" width=5cm
  */
  static slip::block<slip::DPoint2d<int>,4> next_8c = 
  {{slip::DPoint2d<int>(0,1),
   slip::DPoint2d<int>(1,-1),
   slip::DPoint2d<int>(1,0),
   slip::DPoint2d<int>(1,1)}};

  /*!
  ** \brief 6o connexity previous displacements
  ** \image html prev_6co.png "6o connexity connexity previous displacements order"
  ** \image latex prev_6co.eps "6o connexity connexity previous displacements order" width=5cm
  */
  static slip::block<slip::DPoint2d<int>,3> prev_6co = 
    {{slip::DPoint2d<int>(-1,0),
     slip::DPoint2d<int>(-1,-1),
     slip::DPoint2d<int>(0,-1)}};
    
  /*!
  ** \brief 6o connexity next displacements
   ** \image html next_6co.png "6o connexity connexity next displacements order"
  ** \image latex next_6co.eps "6o connexity connexity next displacements order" width=5cm
  */
  static slip::block<slip::DPoint2d<int>,3> next_6co = 
    {{slip::DPoint2d<int>(1,-1),
     slip::DPoint2d<int>(1,0),
     slip::DPoint2d<int>(0,1)}};
  
  /*!
  ** \brief 6e connexity previous displacements
   ** \image html prev_6ec.png "6e connexity connexity previous displacements order"
  ** \image latex prev_6ec.eps "6e connexity connexity previous displacements order" width=5cm
  */
  static slip::block<slip::DPoint2d<int>,3> prev_6ce = 
    {{slip::DPoint2d<int>(-1,1),
     slip::DPoint2d<int>(-1,0),
     slip::DPoint2d<int>(0,-1)}};
    
  /*!
  ** \brief 6e connexity next displacements
   ** \image html next_6ec.png "6e connexity connexity next displacements order"
  ** \image latex next_6ec.eps "6e connexity connexity next displacements order" width=5cm
 
  */
  static slip::block<slip::DPoint2d<int>,3> next_6ce = 
    {{slip::DPoint2d<int>(1,0),
     slip::DPoint2d<int>(1,1),
     slip::DPoint2d<int>(0,1)}};

  /*@} End useful 2d-neighborhood configurations*/


 /**
   ** \name  Some useful 3d-neighborhood configurations 
   */ 
  /*@{*/
  /*!
  ** \brief 6 connexity 3d neighborhood displacements
  ** \par 6 connexity 3d neighborhood displacements
  ** \image html neighbors3d_6c.jpg "Neighbors displacements order"
  ** \image latex neighbors3d_6c.eps "Neighbors displacements order" width=5cm
  */
  static slip::block<slip::DPoint3d<int>,6> n_6c = 
    {{slip::DPoint3d<int>(-1,0,0),
     slip::DPoint3d<int>(0,0,1),
     slip::DPoint3d<int>(0,-1,0),
     slip::DPoint3d<int>(0,0,-1),
     slip::DPoint3d<int>(0,1,0),
     slip::DPoint3d<int>(1,0,0)
    }};
 /*!
  ** \brief 6 connexity 3d previous displacements
  */
  static slip::block<slip::DPoint3d<int>,3> prev_6c = 
     {{slip::DPoint3d<int>(-1,0,0),
      slip::DPoint3d<int>(0,-1,0),
      slip::DPoint3d<int>(0,0,-1)
     }};
  /*!
  ** \brief 6 connexity 3d next displacements
  */
  static slip::block<slip::DPoint3d<int>,3> next_6c = 
    {{slip::DPoint3d<int>(0,0,1),
     slip::DPoint3d<int>(0,1,0),
     slip::DPoint3d<int>(1,0,0)
    }};

 /*!
 ** \brief 18 connexity 3d neighborhood displacements
 ** \par 18 connexity 3d neighborhood displacements
 ** \image html neighbors3d_18c.jpg "Neighbors displacements order"
 ** \image latex neighbors3d_18c.eps "Neighbors displacements order" width=5cm
 */
  static slip::block<slip::DPoint3d<int>,18> n_18c = 
    {{slip::DPoint3d<int>(-1,0,0),
     slip::DPoint3d<int>(-1,0,1),
     slip::DPoint3d<int>(-1,-1,0),
     slip::DPoint3d<int>(-1,0,-1),
     slip::DPoint3d<int>(-1,1,0),
     slip::DPoint3d<int>(0,0,1),
     slip::DPoint3d<int>(0,-1,1),
     slip::DPoint3d<int>(0,-1,0),
     slip::DPoint3d<int>(0,-1,-1),
     slip::DPoint3d<int>(1,0,0),
     slip::DPoint3d<int>(1,0,-1),
     slip::DPoint3d<int>(1,1,0),
     slip::DPoint3d<int>(1,0,1),
     slip::DPoint3d<int>(1,-1,0),
     slip::DPoint3d<int>(0,0,-1),
     slip::DPoint3d<int>(0,1,-1),
     slip::DPoint3d<int>(0,1,0),
     slip::DPoint3d<int>(0,1,1)
    }};
  /*!
  ** \brief 26 connexity 3d neighborhood displacements
  ** \par 26 connexity 3d neighborhood displacements
  ** \image html neighbors3d_26c.jpg "Neighbors displacements order"
  ** \image latex neighbors3d_26c.eps "Neighbors displacements order" width=5cm
  */
   static slip::block<slip::DPoint3d<int>,26> n_26c = 
    {{slip::DPoint3d<int>(-1,0,0),
     slip::DPoint3d<int>(-1,0,1),
     slip::DPoint3d<int>(-1,-1,1),
     slip::DPoint3d<int>(-1,-1,0),
     slip::DPoint3d<int>(-1,-1,-1),
     slip::DPoint3d<int>(-1,0,-1),
     slip::DPoint3d<int>(-1,1,-1),
     slip::DPoint3d<int>(-1,1,0),
     slip::DPoint3d<int>(-1,1,1),
     slip::DPoint3d<int>(0,0,1),
     slip::DPoint3d<int>(0,-1,1),
     slip::DPoint3d<int>(0,-1,0),
     slip::DPoint3d<int>(0,-1,-1),
     slip::DPoint3d<int>(1,0,0),
     slip::DPoint3d<int>(1,0,-1),
     slip::DPoint3d<int>(1,1,-1),
     slip::DPoint3d<int>(1,1,0),
     slip::DPoint3d<int>(1,1,1),
     slip::DPoint3d<int>(1,0,1),
     slip::DPoint3d<int>(1,-1,1),
     slip::DPoint3d<int>(1,-1,0),
     slip::DPoint3d<int>(1,-1,-1),
     slip::DPoint3d<int>(0,0,-1),
     slip::DPoint3d<int>(0,1,-1),
     slip::DPoint3d<int>(0,1,0),
     slip::DPoint3d<int>(0,1,1)
    }};

   /*!
  ** \brief 26 connexity 3d previous displacements
  */
  static slip::block<slip::DPoint3d<int>,13> prev_26c = 
    {{slip::DPoint3d<int>(-1,0,0),
     slip::DPoint3d<int>(-1,0,1),
     slip::DPoint3d<int>(-1,-1,1),
     slip::DPoint3d<int>(-1,-1,0),
     slip::DPoint3d<int>(-1,-1,-1),
     slip::DPoint3d<int>(-1,0,-1),
     slip::DPoint3d<int>(-1,1,-1),
     slip::DPoint3d<int>(-1,1,0),
     slip::DPoint3d<int>(-1,1,1),
     slip::DPoint3d<int>(0,-1,1),
     slip::DPoint3d<int>(0,-1,0),
     slip::DPoint3d<int>(0,-1,-1),
     slip::DPoint3d<int>(0,0,-1)
    }};
 
  /*!
  ** \brief 26 connexity 3d previous displacements
  */
  static slip::block<slip::DPoint3d<int>,13> next_26c = 
    {{
     slip::DPoint3d<int>(0,0,1),
     slip::DPoint3d<int>(1,0,0),
     slip::DPoint3d<int>(1,0,-1),
     slip::DPoint3d<int>(1,1,-1),
     slip::DPoint3d<int>(1,1,0),
     slip::DPoint3d<int>(1,1,1),
     slip::DPoint3d<int>(1,0,1),
     slip::DPoint3d<int>(1,-1,1),
     slip::DPoint3d<int>(1,-1,0),
     slip::DPoint3d<int>(1,-1,-1),
     slip::DPoint3d<int>(0,1,-1),
     slip::DPoint3d<int>(0,1,0),
     slip::DPoint3d<int>(0,1,1)
    }};
 

    /*!
  ** \brief 18 connexity 3d previous displacements
  */
  static slip::block<slip::DPoint3d<int>,9> prev_18c = 
    {{slip::DPoint3d<int>(-1,0,0),
     slip::DPoint3d<int>(-1,0,1),
     slip::DPoint3d<int>(-1,-1,0),
     slip::DPoint3d<int>(-1,0,-1),
     slip::DPoint3d<int>(-1,1,0),
     slip::DPoint3d<int>(0,-1,1),
     slip::DPoint3d<int>(0,-1,0),
     slip::DPoint3d<int>(0,-1,-1),
     slip::DPoint3d<int>(0,0,-1)
    }};
 
  /*!
  ** \brief 18 connexity 3d previous displacements
  */
  static slip::block<slip::DPoint3d<int>,9> next_18c = 
    {{
     slip::DPoint3d<int>(0,0,1),
     slip::DPoint3d<int>(1,0,0),
     slip::DPoint3d<int>(1,0,-1),
     slip::DPoint3d<int>(1,1,0),
     slip::DPoint3d<int>(1,0,1),
     slip::DPoint3d<int>(1,-1,0),
     slip::DPoint3d<int>(0,1,-1),
     slip::DPoint3d<int>(0,1,0),
     slip::DPoint3d<int>(0,1,1)
    }};


  /*@} End useful 3d-neighborhood configurations*/


/**
 ** \name  Some useful 4d-neighborhood configurations
 */

/*@{*/

/*!
 ** @ingroup nbors4d
 ** \version Fluex 1.0
 ** \date 2013/08/26
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief 8 connexity 4d neighborhood displacements
 ** \par 8 connexity 4d neighborhood displacements
 ** \image html neighbors4d_8c.jpg "Neighbors displacements order"
 ** \image latex neighbors4d_8c.eps "Neighbors displacements order" width=5cm
 */
static slip::block<slip::DPoint4d<int>,8> n_4d_8c =
{{slip::DPoint4d<int>(-1,0,0,0),
		slip::DPoint4d<int>(0,-1,0,0),
		slip::DPoint4d<int>(0,0,0,1),
		slip::DPoint4d<int>(0,0,-1,0),
		slip::DPoint4d<int>(0,0,0,-1),
		slip::DPoint4d<int>(0,0,1,0),
		slip::DPoint4d<int>(0,1,0,0),
		slip::DPoint4d<int>(1,0,0,0)
}};
/*!
 ** @ingroup nbors4d
 ** \version Fluex 1.0
 ** \date 2013/08/26
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief 8 connexity 4d previous displacements
 */
static slip::block<slip::DPoint4d<int>,4> prev_4d_8c =
{{slip::DPoint4d<int>(-1,0,0,0),
		slip::DPoint4d<int>(0,-1,0,0),
		slip::DPoint4d<int>(0,0,-1,0),
		slip::DPoint4d<int>(0,0,0,-1)
}};
/*!
 ** @ingroup nbors4d
 ** \version Fluex 1.0
 ** \date 2013/08/26
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief 8 connexity 4d next displacements
 */
static slip::block<slip::DPoint4d<int>,4> next_4d_8c =
{{slip::DPoint4d<int>(0,0,0,1),
		slip::DPoint4d<int>(0,0,1,0),
		slip::DPoint4d<int>(0,1,0,0),
		slip::DPoint4d<int>(1,0,0,0)
}};

/*@} End useful 4d-neighborhood configurations*/
}//slip::

#endif //SLIP_NEIGHBORS_HPP
