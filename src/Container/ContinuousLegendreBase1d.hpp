/** 
 * \file ContinuousLegendreBase1d.hpp
 * 
 * \brief Provides a class to handle global approximation of 1d container by orthogonal polynomials base.
 * 
 */

#ifndef SLIP_CONTINUOUSLEGENDREBASE1D_HPP
#define SLIP_CONTINUOUSLEGENDREBASE1D_HPP

#include <iostream>
#include <string>
#include <vector>
#include "MultivariatePolynomial.hpp"
#include "Polynomial.hpp"
#include "Block.hpp"
#include "Array.hpp"
#include "Matrix.hpp"
#include "arithmetic_op.hpp"
#include "linear_algebra.hpp"
#include "macros.hpp"
#include "io_tools.hpp"
//#include "slip_algorithm.hpp"
#include "multivariate_polynomial_algos.hpp"
#include "PolySupport.hpp"

#include "polynomial_algo.hpp"
#include "poly_weight_functors.hpp"
#include "integration.hpp"

#include "ContinuousPolyBase1d.hpp"

namespace slip
{


template <typename T, std::size_t DIM>
class ContinuousLegendreBase1d;


template<typename T, std::size_t DIM>
class ContinuousLegendreBase1d: public slip::ContinuousPolyBase1d<T,DIM>
{

public:
  typedef ContinuousLegendreBase1d<T,DIM> self;
  typedef const self const_self;
  typedef typename slip::ContinuousPolyBase1d<T,DIM> base;
 
   /**
   ** \name Constructors & Destructors
   */
  /*@{*/
    /*!
    ** \brief Constructs a ContinuousLegendreBase1d.
    */
  ContinuousLegendreBase1d():
    base()
  {
  }


   /*!
    ** \brief Constructs a ContinuousLegendreBase1d.
    ** \param dim dimension (associated variable) of the polynomial. For example dim=1 for x or x1, 2 for y or x2...
    ** \param degree highest degree of the monomials of the base
    ** \param normalized true if the base is normalized, false else
    ** \param monic true if the base is monic (coefficient of the monomial of highest degree is 1), false else
    ** \param method Method used to generate the base : 
    ** "gram_schmidt": gram-schmidt orthogonalisation
    ** "stieltjes": stieltjes (three terms recurrence)
    ** "file": read Legendre polynomial coefficients from file
    ** \param range_size size of the data
    ** \param support support interval of the data
    ** \pre (dim > 0) && (dim <= DIM)
    */
  ContinuousLegendreBase1d(const std::size_t dim,
			   const std::size_t degree,
			   const bool normalized,
			   const bool monic,
			   const slip::POLY_BASE_GENERATION_METHOD& method,
			   const std::size_t range_size,
			   const slip::PolySupport<T>& support):
    base(dim,degree,normalized,monic,method,range_size,support)
  {
  }

   /*!
    ** \brief Constructs a ContinuousLegendreBase1d.
    ** \param dim dimension (associated variable) of the polynomial. For example dim=1 for x or x1, 2 for y or x2...
    ** \param degree highest degree of the monomials of the base
    ** \param normalized true if the base is normalized, false else
    ** \param monic true if the base is monic (coefficient of the monomial of highest degree is 1), false else
    ** \param method Method used to generate the base : 
    ** "gram_schmidt": gram-schmidt orthogonalisation
    ** "stieltjes": stieltjes (three terms recurrence)
    ** "file": read Legendre polynomial coefficients from file
    ** \param range_size size of the data
    ** \param support support interval of the data
    ** \param integration_method Newton Cotes integration method.
    ** \pre (dim > 0) && (dim <= DIM)
    */
  ContinuousLegendreBase1d(const std::size_t dim,
			   const std::size_t degree,
			   const bool normalized,
			   const bool monic,
			   const slip::POLY_BASE_GENERATION_METHOD& method,
			   const std::size_t range_size,
			   const slip::PolySupport<T>& support,
			   const slip::POLY_INTEGRATION_METHOD& integration_method):
    base(dim,degree,normalized,monic,method,range_size,support,integration_method)
  {
  }
  
  
   /*!
    ** \brief Copy constructor
    ** \param other %ContinuousLegendreBase1d.
    */
  ContinuousLegendreBase1d(const self& other):
    base(other)
  {}

   
  
  /*!
   ** \brief Destructor 
   */
  virtual ~ContinuousLegendreBase1d()
  {
  }

  /*!
  ** \brief Operator = 
  ** \param other %ContinuousLegendreBase1d.
  */
  self& operator=(const self& other)
  {
     if(this != &other)
       {
	 
	 base::operator=(other);
	 //this->copy_attributes(other);
       }
     return *this;
       
  }
  /*@} End Constructors */
  



  
  /*!
  ** \brief Generate the polynomial base.
  */
  void generate()
  {
    switch(this->method_)
      {
      case slip::POLY_BASE_GENERATION_FILE :
	{
	this->generate_file();
	this->init_alpha_beta();

	}
	break;
	case slip::POLY_BASE_GENERATION_STIELTJES :
	{
	  this->generate_stieltjes();
	  this->init_alpha_beta();
	}
	break;
      case slip::POLY_BASE_GENERATION_GRAM_SCHMIDT:
	{
	  std::cout<<"generation method not implemented yet "<<std::endl;
	}
	break;
      default:
	{ //slip::POLY_BASE_GENERATION_FILE
	  this->generate_file();
	  this->init_alpha_beta();
	}
      };
    //this->generate_discrete_base_newton_cotes();
    //    this->generate_discrete_base_exact();
    switch(base::integration_method_)
      {
      case slip::POLY_INTEGRATION_RECTANGULAR:
	this->generate_discrete_base_newton_cotes(new slip::Rectangular<T>());
	break;
      case slip::POLY_INTEGRATION_TRAPEZIUM:
	this->generate_discrete_base_newton_cotes(new slip::Trapezium<T>());
	break;
      case slip::POLY_INTEGRATION_SIMPSON:
	this->generate_discrete_base_newton_cotes(new slip::Simpson<T>());

	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_3:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes3<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_4:
	this->generate_discrete_base_newton_cotes(new slip::BooleVillarceau<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_5:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes5<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_6:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes6<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_7:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes7<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_8:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes8<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_9:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes9<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_10:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes10<T>());
	break;
      case slip::POLY_INTEGRATION_HOSNY2007:
	this->generate_discrete_base_exact();
	break;
      case slip::POLY_INTEGRATION_EXACT:
	this->generate_discrete_base_exact2();
	break;
	case slip::POLY_INTEGRATION_AESR_8:
	this->generate_discrete_base_aesr(new slip::AlternativeExtendedSimpsonsRule<T>(8));
	break;
      case slip::POLY_INTEGRATION_AESR_13:
	this->generate_discrete_base_aesr(new slip::AlternativeExtendedSimpsonsRule<T>(13));
	break;
      case slip::POLY_INTEGRATION_AESR_18:
	this->generate_discrete_base_aesr(new slip::AlternativeExtendedSimpsonsRule<T>(18));
	break;
      case slip::POLY_INTEGRATION_AESR_23:
	this->generate_discrete_base_aesr(new slip::AlternativeExtendedSimpsonsRule<T>(23));
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_3:
	this->generate_discrete_base_multigrid(3);
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_5:
	this->generate_discrete_base_multigrid(5);
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_7:
	this->generate_discrete_base_multigrid(7);
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_9:
	this->generate_discrete_base_multigrid(9);
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_10:
	this->generate_discrete_base_multigrid(10);
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_11:
	this->generate_discrete_base_multigrid(11);
	break;
      default:
	this->generate_discrete_base_exact2();
      };
    
  }

  /*!
  ** \brief Normalize the base if it is not normalized
  */
  void normalize()
  {
    base::normalize();
     switch(base::integration_method_)
      {
      case slip::POLY_INTEGRATION_RECTANGULAR:
	this->generate_discrete_base_newton_cotes(new slip::Rectangular<T>());
	break;
      case slip::POLY_INTEGRATION_TRAPEZIUM:
	this->generate_discrete_base_newton_cotes(new slip::Trapezium<T>());
	break;
      case slip::POLY_INTEGRATION_SIMPSON:
	this->generate_discrete_base_newton_cotes(new slip::Simpson<T>());

	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_3:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes3<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_4:
	this->generate_discrete_base_newton_cotes(new slip::BooleVillarceau<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_5:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes5<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_6:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes6<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_7:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes7<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_8:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes8<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_9:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes9<T>());
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_10:
	this->generate_discrete_base_newton_cotes(new slip::NewtonCotes10<T>());
	break;
      case slip::POLY_INTEGRATION_HOSNY2007:
	this->generate_discrete_base_exact();
	break;
      case slip::POLY_INTEGRATION_EXACT:
	this->generate_discrete_base_exact2();
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_3:
	this->generate_discrete_base_multigrid(3);
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_5:
	this->generate_discrete_base_multigrid(5);
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_7:
	this->generate_discrete_base_multigrid(7);
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_9:
	this->generate_discrete_base_multigrid(9);
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_10:
	this->generate_discrete_base_multigrid(10);
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_11:
	this->generate_discrete_base_multigrid(11);
	break;

      default:
	this->generate_discrete_base_exact2();
      };
   
  }

  /*!
  ** \brief Computes the Gram matrix of the polynomial base.
  ** \param Gram The resulting Gram matrix.
  */
  void analytic_orthogonality_matrix(slip::Matrix<T>& Gram)
  {
    base::analytic_orthogonality_matrix(Gram,slip::LegendreInnerProduct1d<T,DIM>());
  }

  /*!
  ** \brief Indicate if the polynomial basis is orthogonal according to the tolerance \a tol.
  ** \param tol Tolerance of the orthogonality .
  ** \result True if orthogonal, false else.
  */
  bool is_analytically_orthogonal(const T& tol = slip::epsilon<T>())
  {
    return base::is_analytically_orthogonal(slip::LegendreInnerProduct1d<T,DIM>(),tol);
  }


  /*!
  ** \brief Returns the orthogonality precision of the base.
  ** If orthogonality precision > 1e-1 returns 1e-1.
  ** \return orthogonality precision 
  */
  virtual T analytic_orthogonality_precision()
  {
    return base::analytic_orthogonality_precision(slip::LegendreInnerProduct1d<T,DIM>());
  }

  /*!
  ** \brief Computes the Legendre analytic inner_product between the ith and jth polynomials.
  ** \param i index of the first polynomial.
  ** \param j index of the second polynomial.
  ** \pre (i >=0)
  ** \pre (i <= this->degree())
  ** \pre (j >=0)
  ** \pre (j <= this->degree())
  */
  T analytic_inner_product(const std::size_t i, const std::size_t j)
  {
    return slip::LegendreInnerProduct1d<T,DIM>()((*this)[i],(*this)[j]);
  }


  /*!
  ** \brief Computes the inner_product between the ith and jth polynomials.
  ** \param i index of the first polynomial.
  ** \param j index of the second polynomial.
  ** \pre (i >=0)
  ** \pre (i <= this->degree())
  ** \pre (j >=0)
  ** \pre (j <= this->degree())
  */
  T inner_product(const std::size_t i, const std::size_t j)
  {
    //return this->inner_product_exact2(i,j);
    // return std::inner_product(this->poly_begin(i),this->poly_end(i),
    // 			      (this->poly_w_evaluations_)->col_begin(j),T());
    return std::inner_product(this->poly_begin(i),
    			      this->poly_end(i),
    			      this->poly_begin(j),T());
        
  }

  
 
  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const
  {
    return "ContinuousLegendreBase1d";
  }

protected:
 

private:

  /**
   ** \brief Generates polynomials base from coefficients files.
   */
  void generate_file()
  {
    if(this->degree_ < 100)
      {
	std::vector<std::vector<double> > legendre_triangle;
	//	slip::Matrix<double> Num;
	slip::Matrix<double> Den;
	if(this->monic_)
	  {
	    this->read_num_den_coefficients("./Legendre_numerator_monic.txt",
					    "./Legendre_denominator_monic.txt",
					    legendre_triangle,
					    Den);
	  }
	else
	  {
	    this->read_num_den_coefficients("./Legendre_numerator_b008316.txt",
					    "./Legendre_denominator_b060818.txt",
					    legendre_triangle,
					    Den);
	  }
				     

	// std::cout<<"legendre numerator coefficients "<<std::endl;
	// for(std::size_t k = 0; k < legendre_triangle.size(); ++k)
	//   {
	//     std::cout<<"degree "<<k<<std::endl;
	//     for(std::size_t j = 0; j < legendre_triangle[k].size(); ++j)
	//       {
	// 	std::cout<<legendre_triangle[k][j]<<" ";
	//       }
	//     std::cout<<std::endl;
	//   }




	    slip::Monomial<DIM> cte;
	    std::fill(cte.powers.begin(),cte.powers.end(),0);
	    (*this)[0].insert(cte,slip::constants<T>::one());
	    slip::Monomial<DIM> x;
	    std::fill(x.powers.begin(),x.powers.end(),0);
	    x.powers[this->dim_-1] = 1;
	    (*this)[1].insert(x,slip::constants<T>::one());
	    for(std::size_t k = 2; k < legendre_triangle.size(); ++k)
	      {
		if(k%2 == 0)
		  {
		    slip::Monomial<DIM> monome;
		    std::fill(monome.powers.begin(),monome.powers.end(),0);
		    (*this)[k].insert(monome,legendre_triangle[k][0]);
		    for(std::size_t p = 2, c = 1; c < legendre_triangle[k].size(); p+=2,++c)
		      {
			slip::Monomial<DIM> monome;
			std::fill(monome.powers.begin(),monome.powers.end(),0);
			monome.powers[this->dim_-1] = p;
			(*this)[k].insert(monome,legendre_triangle[k][c]);
		      }
		  }
		else
		  {
		    for(std::size_t p = 1, c = 0; c < legendre_triangle[k].size(); p+=2,++c)
		      {
			slip::Monomial<DIM> monome;
			std::fill(monome.powers.begin(),monome.powers.end(),0);
			monome.powers[this->dim_-1] = p;
			(*this)[k].insert(monome,legendre_triangle[k][c]);
		      }
		  }
		//divides by denominator coefficient
		(*this)[k] /= static_cast<T>(Den[k][1]);
	      }
      
	    this->compute_pkpk();
	    if(this->normalized_)
	      {
		this->generate_file_normalize();
		std::fill_n(this->pkpk_->begin(),this->size_,slip::constants<T>::one());
	      }
	   
	
	    // for(std::size_t k = 0; k < this->size(); ++k)
	    //   {
	    // 	std::cout<<(*this)[k]<<std::endl;
	    //   }
	  
      }
    else
      {
	std::cout<<"Legendre files are computed until degree 99"<<std::endl;
      }
  }

  /**
   ** \brief Read Legendre numerator and denominateur coefficients from files
   ** \param num_file numerator file path name.
   ** \param den_file denominator file path name.
   ** \param legendre_triangle legendre triangle of coefficients.
   ** \param Den Denomitaor matrix
   */
  void read_num_den_coefficients(const std::string& num_file,
				 const std::string& den_file,
				 std::vector<std::vector<double> >& legendre_triangle,
				 slip::Matrix<double>& Den)
  {
    	//load numerator coefficients
	slip::Matrix<double> Num;
	slip::read_ascii_2d<slip::Matrix<double> >(Num,num_file);
	//load denominator coefficients
	//slip::Matrix<double> Den;
	slip::read_ascii_2d<slip::Matrix<double> >(Den,den_file);
  
	//std::cout<<"size Num = "<<Num.rows()<<std::endl;
	legendre_triangle.resize(this->degree_+1);
	legendre_triangle[0].resize(1);
	legendre_triangle[0][0] = Num[0][1];
	legendre_triangle[1].resize(1);
	legendre_triangle[1][0] = Num[1][1];
	std::size_t nmax = legendre_triangle.size();
	if((this->degree_ % 2) == 0)
	  {
	    nmax = legendre_triangle.size() - 1;
	  }
	std::size_t index = 2;
	for(std::size_t n = 2, l = 2; n < nmax; n+=2,++l)
	  {
	    //std::cout<<"n = "<<n<<std::endl;
	    //std::cout<<"index = "<<index<<std::endl;
	    legendre_triangle[n].resize(l);
	    for(std::size_t k = 0; k < l; ++k, ++index)
	      {
		legendre_triangle[n][k] = Num[index][1];
	      }
	    //std::cout<<"index = "<<index<<std::endl;
	    legendre_triangle[n+1].resize(l);
	    for(std::size_t k = 0; k < l; ++k, ++index)
	      {
		legendre_triangle[n+1][k] = Num[index][1];
	      }
	  }
	
	if((this->degree_ % 2) == 0)
	  {
	    //std::cout<<"index = "<<index<<std::endl;
	    legendre_triangle[nmax].resize(legendre_triangle[nmax-1].size()+1);
	    for(std::size_t k = 0; k < legendre_triangle[nmax].size(); ++k, ++index)
	      {
		legendre_triangle[nmax][k] = Num[index][1];
	      }
	  }

  }

  /**
   ** \brief generate polynomials base using the analytical Stieltjes procedure.
   */
  void generate_stieltjes()
  {
    if(this->monic_)
      {
	slip::Monomial<DIM> cte;
	std::fill(cte.powers.begin(),cte.powers.end(),0);
	(*this)[0].insert(cte,slip::constants<T>::one());
	slip::Monomial<DIM> x;
	std::fill(x.powers.begin(),x.powers.end(),0);
	x.powers[this->dim_-1] = 1;
	(*this)[1].insert(x,slip::constants<T>::one());
	typename base::value_type P1((*this)[1]);

	const T four = static_cast<T>(4.0);
	const T one = slip::constants<T>::one();
	for(unsigned k = 1; k < this->degree_; ++k)
	  {
	    T k2 = k*k;
	    T betak = one/(four - one/k2);
	    (*this)[k+1] = P1 * (*this)[k] - betak * (*this)[k-1];
	  }
      }
    else
      {
	slip::Monomial<DIM> cte;
	std::fill(cte.powers.begin(),cte.powers.end(),0);
	(*this)[0].insert(cte,slip::constants<T>::one());
	slip::Monomial<DIM> x;
	std::fill(x.powers.begin(),x.powers.end(),0);
	x.powers[this->dim_-1] = 1;
	(*this)[1].insert(x,slip::constants<T>::one());
	typename base::value_type P1((*this)[1]);
	
	for(unsigned k = 1; k < this->degree_; ++k)
	  {
	    (*this)[k+1] = slip::legendre_nd_next<typename base::value_type >(k, 
    									P1, //x
									      (*this)[k], //Pk
									      (*this)[k-1]); //Pk-1
	  }
      }

     this->compute_pkpk();
     if(this->normalized_)
       {
	 this->generate_file_normalize();
	 std::fill_n(this->pkpk_->begin(),this->size_,slip::constants<T>::one());
       }
    
  }
  
  /**
   ** \brief computes monic Legendre three terms reccurence alpha and beta coefficients.
   */

  void init_alpha_beta()
  {
    // std::fill_n(this->alpha_->begin(),this->alpha_->size(),T());
    // auto beta_first = this->beta_->begin();
    // *beta_first++ = slip::constants<T>::two();
    // T k = slip::constants<T>::one();
    // T four = static_cast<T>(4.0);
    // for( ; beta_first != this->beta_->end(); ++beta_first, ++k)
    //   {
    // 	T k2 = k*k;
    // 	*beta_first = k2/(four*k2-slip::constants<T>::one());
    //   }
    // std::cout<<"alpha = \n"<<*(this->alpha_)<<std::endl;
    // std::cout<<"beta = \n"<<*(this->beta_)<<std::endl;
    slip::legendre_jacobi_coefficients(this->degree_,
					   this->alpha_->begin(),
					   this->alpha_->end(),
					   this->beta_->begin(),
					   this->beta_->end());
  }

 /**
   ** \brief computes square norms of Legendre polynomials.
   */
  void compute_pkpk()
  {
    if(!this->monic_)
      {
	
	for(std::size_t k = 0; k <  this->size(); ++k)
	  {
	    (*(this->pkpk_))[k] =slip::constants<T>::two()/static_cast<T>(k+k+1);
	  }
	
      }
    else
      {
	slip::Array<T> one(DIM,slip::constants<T>::one());
	
	for(std::size_t k = 0; k <  this->size(); ++k)
	  {
	    T pn1 = (*this)[k].evaluate(one.begin(),one.end());
	    (*(this->pkpk_))[k] = ((pn1*pn1)*slip::constants<T>::two())/static_cast<T>(k+k+1);
	    
	  }
      }
  }


  /*!
  ** \brief normalize a base generated by file reading
  */
  void generate_file_normalize()
  {
    for(std::size_t k = 0; k <  this->size(); ++k)
      {
	(*this)[k] /= std::sqrt((*(this->pkpk_))[k]);
      }
  }

  /**
   ** \brief Pre-computes polynomial integrals inside samples using
   ** Hosny method:
   ** Hosny, K.M., 2007. Exact Legendre moment computation for gray level images. Pattern Recognition 40, 3597–3605. https://doi.org/10.1016/j.patcog.2007.04.014
   *
   */
  void generate_discrete_base_exact()
  {
    //xi vector xi = -1 + (i-(1/2))dx / dx = 2/range_size i = 1,...,n
    slip::Array<T> x(this->range_size_);
    T dx_o_2 = slip::constants<T>::one()/static_cast<T>(this->range_size_);
    T dx = slip::constants<T>::two() * dx_o_2;
    slip::iota(x.begin(),x.end(),-slip::constants<T>::one()+dx_o_2,dx);
    //std::cout<<"x = \n"<<x<<std::endl;
    slip::Array<T> ui(this->range_size_+1);
    //ui vector ui = (-1 + i*dx) i = 0,...,n
    slip::iota(ui.begin(),ui.end(),-slip::constants<T>::one(),dx);
    //std::cout<<"ui = \n"<<ui<<std::endl;
    slip::Array<T> coord(DIM,slip::constants<T>::one());
    slip::Array<T> Pk_ui(ui.size());
    slip::Array<T> Pkm1_ui(ui.size());
    
    slip::Polynomial<T> P1(1);
    this->mpoly_to_poly(*(this->begin()),this->dim_,P1);
    T p1 = P1.evaluate(slip::constants<T>::one());
    std::fill((this->poly_w_evaluations_)->col_begin(0),
	      (this->poly_w_evaluations_)->col_end(0),
	      dx*p1);

    auto it_poly = this->begin()+1;
    for(std::size_t k = 1; k < this->size(); ++it_poly, ++k)
      {
	//MultivariatePolynomial to Polynomial
	slip::Polynomial<T> polyk(k);
	this->mpoly_to_poly(*it_poly,this->dim_,polyk);
	slip::Polynomial<T> polykm1(k-1);
	this->mpoly_to_poly(*(it_poly-1),this->dim_,polykm1);
	
	//evaluate polynomials
	polyk.multi_evaluate(ui.begin(),ui.end(),Pk_ui.begin());
	polykm1.multi_evaluate(ui.begin(),ui.end(),Pkm1_ui.begin());
	polykm1.multi_evaluate(x.begin(),x.end(),this->discrete_base_->col_begin(k-1));
	//evaluate integral for all ui
	auto it_ui = ui.begin();
	auto it_ui_e = ui.end()-1;
	auto it_pk_ui = Pk_ui.begin();
	//auto it_puik_e = Puik.end();
	auto it_pkm1_ui = Pkm1_ui.begin();
	auto it_poly_w_eval = (this->poly_w_evaluations_)->col_begin(k);
	//T coef = static_cast<T>(2*k+1)/static_cast<T>(2*k+2);
	//T coef = slip::constants<T>::two()/static_cast<T>(2*k+2);
	T coef = slip::constants<T>::one()/static_cast<T>(k+1);
	for(; it_ui != it_ui_e; ++it_ui,++it_pk_ui,++it_pkm1_ui,++it_poly_w_eval)
	  {
	    *it_poly_w_eval = this->evaluate_integral_exact(coef,
							    *it_ui,
							    *(it_ui+1),
							    *it_pk_ui,
							    *(it_pk_ui+1),
							    *it_pkm1_ui,
							    *(it_pkm1_ui+1));
	  }
	
	
      }
    slip::Polynomial<T> Pn(this->degree());
    this->mpoly_to_poly((*this)[this->size()-1],this->dim_,Pn);
    Pn.multi_evaluate(x.begin(),x.end(),this->discrete_base_->col_begin(this->size_-1));
    
  }

  /**
   ** \brief Pre-computes polynomial integrals inside samples using
   ** my formal polynomial integration. Probably equivalent to Yap's method:
   ** Yap, P.-T., Paramesran, R., 2005. An efficient method for the computation of Legendre moments. IEEE Transactions on Pattern Analysis and Machine Intelligence 27, 1996–2002. https://doi.org/10.1109/TPAMI.2005.232
   *
   */
 void generate_discrete_base_exact2()
  {
    //xi vector xi = -1 + (i-(1/2))dx / dx = 2/range_size i = 1,...,n
    slip::Array<T> x(this->range_size_);
    T dx_o_2 = slip::constants<T>::one()/static_cast<T>(this->range_size_);
    T dx = slip::constants<T>::two() * dx_o_2;
    slip::iota(x.begin(),x.end(),-slip::constants<T>::one()+dx_o_2,dx);
    //std::cout<<"x = \n"<<x<<std::endl;
    slip::Array<T> ui(this->range_size_+1);
    //ui vector ui = (-1 + i*dx) i = 0,...,n
    slip::iota(ui.begin(),ui.end(),-slip::constants<T>::one(),dx);

    slip::Array<T> z(DIM,T());

    auto it_poly = this->begin();
    for(std::size_t k = 0; k < this->size(); ++it_poly, ++k)
      {
 	slip::Polynomial<T> polyk(k);
 	this->mpoly_to_poly(*it_poly,this->dim_,polyk);
 	polyk.multi_evaluate(x.begin(),x.end(),this->discrete_base_->col_begin(k));
 	const slip::MultivariatePolynomial<T,DIM>& const_poly = *it_poly;
 	slip::MultivariatePolynomial<T,DIM> int_poly = const_poly.integral(this->dim_,T());
 	auto it_ui = ui.begin();
 	auto it_ui_e = ui.end()-1;
 	auto it_poly_w_eval = (this->poly_w_evaluations_)->col_begin(k);
 	for(; it_ui != it_ui_e; ++it_ui,++it_poly_w_eval)
 	  {
 	    slip::MultivariatePolynomial<T,DIM> cte =
 	      int_poly.partial_evaluate(this->dim_,*(it_ui+1))
 	      - int_poly.partial_evaluate(this->dim_,*(it_ui));
 	    *it_poly_w_eval = cte.evaluate(z.begin(),z.end());
 	  }
      }
  }

  /**
   ** \brief computes the exact integration of Pk+1 from Pk and Pk-1
   ** internal method of generate_discrete_base_exact()
   ** \param coef
   ** \param Ui
   ** \param Uip1
   ** \param Pk_ui
   ** \param Pk_uip1
   ** \param Pkm1_ui
   ** \param Pkm1_uip1
   */
T evaluate_integral_exact(const T& coef,
			  const T& Ui,
			  const T& Uip1,
			  const T& Pk_ui,
			  const T& Pk_uip1,
			  const T& Pkm1_ui,
			  const T& Pkm1_uip1)
  {
    return coef *(Uip1*Pk_uip1-Pkm1_uip1-Ui*Pk_ui+Pkm1_ui);
  }

 /**
   ** \brief computes the normalized exact integration of Pk+1 from Pk and Pk-1
   ** internal method of generate_discrete_base_exact()
   ** \param coef1
   ** \param coef2
   ** \param Ui
   ** \param Uip1
   ** \param Pk_ui
   ** \param Pk_uip1
   ** \param Pkm1_ui
   ** \param Pkm1_uip1
   */
T evaluate_integral_exact_norm(const T& coef1,
			       const T& coef2,
			       const T& Ui,
			       const T& Uip1,
			       const T& Pk_ui,
			       const T& Pk_uip1,
			       const T& Pkm1_ui,
			       const T& Pkm1_uip1)
  {
    return coef1 *(Uip1*Pk_uip1-coef2*Pkm1_uip1-Ui*Pk_ui+coef2*Pkm1_ui);
  }
  
  /**
   ** \brief Pre-computes polynomial integrals inside samples using
   ** a newton cotes integration method
   ** \param integration_method slip::NewtonCotesMethod<T>* integration method
   */
  void generate_discrete_base_newton_cotes(slip::NewtonCotesMethod<T>* integration_method)
  {
   
    slip::Array<T> x(this->range_size_);
    const T dx_o_2 = slip::constants<T>::one()/static_cast<T>(this->range_size_);
    const T dx = slip::constants<T>::two() * dx_o_2;

    //std::cout<<"dx_o_2 = "<<dx_o_2<<std::endl;
    slip::iota(x.begin(),x.end(),-slip::constants<T>::one()+dx_o_2,dx);
	       //slip::UniformCollocations<T> UC(-slip::constants<T>::one()+dx_o_2,slip::constants<T>::one()-dx_o_2);
	       //UC(x.begin(),x.end());
    // std::cout<<"----------------------------"<<std::endl;
    // std::cout<<"x = \n"<<x<<std::endl;
    // std::cout<<"----------------------------"<<std::endl;
    slip::Array<T> coord(DIM,slip::constants<T>::one());
    std::size_t k = 0;
    for(auto it_poly = this->begin(); it_poly != this->end(); ++it_poly, ++k)
      {
	//MultivariatePolynomial to Polynomial
	slip::Polynomial<T> poly(k);
	this->mpoly_to_poly(*it_poly,this->dim_,poly);
	//	std::cout<<poly<<std::endl;
	poly.multi_evaluate(x.begin(),x.end(),this->discrete_base_->col_begin(k));
	this->computes_poly_w_evaluations_newton_cotes(x.begin(),x.end(),poly,dx_o_2,integration_method);
      }
    //computes int_xi^{xi+1} P_n(x) w(x) dx by newton-cotes method or
    //by exact integration
    //std::copy(this->discrete_base_->begin(),this->discrete_base_->end(),
    //	      this->poly_w_evaluations_->begin());
    
  }
 /**
   ** \brief Pre-computes a given polynomial poly integral inside samples using
   ** a newton cotes integration method.
   ** \param x_first first iterator x range.
   ** \param x_last  one-past-the-end iterator of the x range.
   ** \param dx_o_2 half the distance between samples.
   ** \param integration_method slip::NewtonCotesMethod<T>* integration method
   */
  template <typename RandomAccessIterator>
  void computes_poly_w_evaluations_newton_cotes(RandomAccessIterator x_first,
						RandomAccessIterator x_last,
						const slip::Polynomial<T>& poly,
						const T& dx_o_2,
						slip::NewtonCotesMethod<T>* integration_method)
  {

    	RandomAccessIterator it_beg = x_first;
    	RandomAccessIterator it_end = x_last; 
	typename slip::Matrix<T>::col_iterator it_poly_w_evaluations = this->poly_w_evaluations_->col_begin(poly.order());
	for(; it_beg != it_end; ++it_beg, ++it_poly_w_evaluations)
	  {
	    // std::cout<<"["<<*it_beg-dx_o_2<<","<<*(it_beg)+dx_o_2<<"]"<<std::endl;
	    *it_poly_w_evaluations =
	      slip::newton_cotes(*it_beg-dx_o_2,*(it_beg)+dx_o_2,
				     integration_method->order()+1,
				     *(integration_method),
				     poly);
	  }
  }
  /**
   ** \brief Pre-computes polynomial integrals inside samples using
   ** a multigrid sample oversampling method. Method proposed by Wang et al.:
   ** Wang, X., Liao, S., 2013. Image Reconstruction from Orthogonal Fourier-Mellin Moments, in: Kamel, M., Campilho, A. (Eds.), Image Analysis and Recognition, Lecture Notes in Computer Science. Presented at the International Conference Image Analysis and Recognition, Springer Berlin Heidelberg, pp. 687–694. https://doi.org/10.1007/978-3-642-39094-4_78

   ** \param K oversampling factor of the sample.
   */
  void generate_discrete_base_multigrid(const std::size_t K)
  {
   
    slip::Array<T> x(this->range_size_);
    const T dx_o_2 = slip::constants<T>::one()/static_cast<T>(this->range_size_);
    const T dx = slip::constants<T>::two() * dx_o_2;
    slip::iota(x.begin(),x.end(),-slip::constants<T>::one()+dx_o_2,dx);

    slip::Array<T> coord(DIM,slip::constants<T>::one());
    std::size_t k = 0;
    for(auto it_poly = this->begin(); it_poly != this->end(); ++it_poly, ++k)
      {
	//MultivariatePolynomial to Polynomial
	slip::Polynomial<T> poly(k);
	this->mpoly_to_poly(*it_poly,this->dim_,poly);
	//	std::cout<<poly<<std::endl;
	poly.multi_evaluate(x.begin(),x.end(),this->discrete_base_->col_begin(k));
	this->computes_poly_w_evaluations_multigrid(x.begin(),x.end(),poly,dx_o_2,K);
      }
  }
/**
   ** \brief Pre-computes a given polynomial poly integral inside samples using
   ** multigrid method of Wang et al.
   ** \param x_first first iterator x range.
   ** \param x_last  one-past-the-end iterator of the x range.
   ** \param dx_o_2 half the distance between samples.
   ** \param k sample oversampling factor.
   */
template <typename RandomAccessIterator>
  void computes_poly_w_evaluations_multigrid(RandomAccessIterator x_first,
					     RandomAccessIterator x_last,
					     const slip::Polynomial<T>& poly,
					     const T& dx_o_2,
					     const std::size_t k)
  {

    	RandomAccessIterator it_beg = x_first;
    	RandomAccessIterator it_end = x_last; 
	typename slip::Matrix<T>::col_iterator it_poly_w_evaluations = this->poly_w_evaluations_->col_begin(poly.order());
	slip::Rectangular<T> rect;
	for(; it_beg != it_end; ++it_beg, ++it_poly_w_evaluations)
	  {
	    // std::cout<<"["<<*it_beg-dx_o_2<<","<<*(it_beg)+dx_o_2<<"]"<<std::endl;
	    *it_poly_w_evaluations =
	      slip::newton_cotes(*it_beg-dx_o_2,*(it_beg)+dx_o_2,
				     k+1,
				     rect,
				     poly);
	  }
  }

/**
   ** \brief Pre-computes a given polynomial poly integral inside samples using
   ** an Alternative Extended Simpsons Rule (AESR) integration scheme:
   ** Liao, S.X., Pawlak, M., 1996. On image analysis by moments. IEEE Transactions on Pattern Analysis and Machine Intelligence 18, 254–266. https://doi.org/10.1109/34.485554
   ** \param x_first first iterator x range.
   ** \param x_last  one-past-the-end iterator of the x range.
   ** \param dx_o_2 half the distance between samples.
   ** \param integration_method slip::AlternativeExtendedSimpsonsRule<T>* Alternative Extended Simpsons Rule scheme
   */
   template <typename RandomAccessIterator>
  void computes_poly_w_evaluations_aesr(RandomAccessIterator x_first,
					RandomAccessIterator x_last,
					const slip::Polynomial<T>& poly,
					const T& dx_o_2,
					slip::AlternativeExtendedSimpsonsRule<T>* integration_method)
  {

    	RandomAccessIterator it_beg = x_first;
    	RandomAccessIterator it_end = x_last; 
	typename slip::Matrix<T>::col_iterator it_poly_w_evaluations = this->poly_w_evaluations_->col_begin(poly.order());
	for(; it_beg != it_end; ++it_beg, ++it_poly_w_evaluations)
	  {
	    // std::cout<<"["<<*it_beg-dx_o_2<<","<<*(it_beg)+dx_o_2<<"]"<<std::endl;
	    *it_poly_w_evaluations =
	      slip::alternative_extended_simpsons_rule(*it_beg - dx_o_2,
							   *it_beg + dx_o_2,
							   integration_method->points_number(),
							   *(integration_method),
							   poly);
	  }
  }
  /**
   ** \brief Pre-computes polynomial integrals inside samples using
   ** an Alternative Extended Simpsons Rule (AESR) integration scheme:
   ** Liao, S.X., Pawlak, M., 1996. On image analysis by moments. IEEE Transactions on Pattern Analysis and Machine Intelligence 18, 254–266. https://doi.org/10.1109/34.485554
 ** \param integration_method slip::AlternativeExtendedSimpsonsRule<T>* Alternative Extended Simpsons Rule scheme
 */
  void generate_discrete_base_aesr(slip::AlternativeExtendedSimpsonsRule<T>* integration_method)
  {
   
    slip::Array<T> x(this->range_size_);
    const T dx_o_2 = slip::constants<T>::one()/static_cast<T>(this->range_size_);
    const T dx = slip::constants<T>::two() * dx_o_2;
    slip::iota(x.begin(),x.end(),-slip::constants<T>::one()+dx_o_2,dx);

    slip::Array<T> coord(DIM,slip::constants<T>::one());
    std::size_t k = 0;
    for(auto it_poly = this->begin(); it_poly != this->end(); ++it_poly, ++k)
      {
	//MultivariatePolynomial to Polynomial
	slip::Polynomial<T> poly(k);
	this->mpoly_to_poly(*it_poly,this->dim_,poly);
	//	std::cout<<poly<<std::endl;
	poly.multi_evaluate(x.begin(),x.end(),this->discrete_base_->col_begin(k));
	this->computes_poly_w_evaluations_aesr(x.begin(),x.end(),poly,dx_o_2,integration_method);
      }
  }


 /*!
  ** \brief Computes the inner_product between the ith and jth polynomials using formal integration.
  ** \param i index of the first polynomial.
  ** \param j index of the second polynomial.
  ** \pre (i >=0)
  ** \pre (i <= this->degree())
  ** \pre (j >=0)
  ** \pre (j <= this->degree())
  */
  T inner_product_exact2(const std::size_t i, const std::size_t j)
  {
    slip::Array<T> x(this->range_size_);
    T dx_o_2 = slip::constants<T>::one()/static_cast<T>(this->range_size_);
    T dx = slip::constants<T>::two() * dx_o_2;
    slip::iota(x.begin(),x.end(),-slip::constants<T>::one()+dx_o_2,dx);
    slip::MultivariatePolynomial<T,DIM> PiPj = (*this)[i] * (*this)[j];
     slip::Array<T> ui(this->range_size_+1);
    //ui vector ui = (-1 + i*dx) i = 0,...,n
    slip::iota(ui.begin(),ui.end(),-slip::constants<T>::one(),dx);
   
    slip::Array<T> z(DIM,T());
   
    slip::Array<T> hij(this->range_size_);
    const slip::MultivariatePolynomial<T,DIM>& const_poly = PiPj;
    slip::MultivariatePolynomial<T,DIM> int_poly = const_poly.integral(this->dim_,T());
 	auto it_ui = ui.begin();
 	auto it_ui_e = ui.end()-1;
 	auto it_hij = hij.begin();
 	for(; it_ui != it_ui_e; ++it_ui,++it_hij)
 	  {
 	    slip::MultivariatePolynomial<T,DIM> cte =
 	      int_poly.partial_evaluate(this->dim_,*(it_ui+1))
 	      - int_poly.partial_evaluate(this->dim_,*(it_ui));
 	    *it_hij = cte.evaluate(z.begin(),z.end());
 	  }
	return std::accumulate(hij.begin(),hij.end(),T());
  }
   


};

}//::slip


#endif //SLIP_CONTINUOUSLEGENDREBASE1D_HPP
