/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file iterator2d_box.hpp
 * 
 * \brief Provides a class to manipulate iterator2d within a slip::Box2d.
 * It is used to iterate throw 2d containers.
 * 
 */
#ifndef SLIP_ITERATOR2D_BOX_HPP
#define SLIP_ITERATOR2D_BOX_HPP

#include <iterator>
#include <cassert>

#include "Point2d.hpp"
#include "DPoint2d.hpp"
#include "Box2d.hpp"
#include "Range.hpp"

#include "iterator_types.hpp"

namespace slip
{
/*! \class iterator2d_box
** 
** \brief This is some iterator to iterate a 2d container into a slip::Box2d area 
**        defined by the indices of the 2d container.
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
** \version 0.0.4
** \date 18/12/2007
**
** \param Container2D Type of 2d container in the iterator2d_box 
**
** \par Description:
**      This iterator is an 2d extension of the random_access_iterator.
**      It is compatible with the bidirectional_iterator of the
**      Standard library. It iterate into a box area defined inside the
**      indices range of the 2d container. Those indices are defined as 
**      follows :
** \code
**      (0,0)--------> x2 (or j)
**        |
**        |
**        |
**        |
**       \|/
**        x1 (or i)
**
**      (0,0) is the upper left corner of the 2d container.
** \endcode
*/
template <class Container2D>
class iterator2d_box  
{
public:
  //typedef std::random_access_iterator_tag iterator_category;
  typedef std::random_access_iterator2d_tag iterator_category;
  typedef typename Container2D::value_type value_type;
  typedef DPoint2d<int> difference_type;
  typedef typename Container2D::pointer pointer;
  typedef typename Container2D::reference reference;

  typedef iterator2d_box self;

  typedef typename Container2D::size_type size_type;
  /**
   ** \name Constructors & Destructors
   */
  /*@{*/
  
  /*!
  ** \brief Constructs a %iterator2d_box.
  ** \par The box to iterate is in this case the full container
  */
  iterator2d_box():
    cont2d_(0),pos_(0),x1_(0),x2_(0),box_(0,0,0,0)
  {}

  /*!
  ** \brief Constructs a %iterator2d_box.
  ** \param c pointer to an existing 2d container
  ** \param b Box2d<int> defining the range of indices to iterate
  ** \pre box indices must be inside those of the 2d container
  */
  iterator2d_box(Container2D* c,
		 const Box2d<int>& b):
    cont2d_(c),pos_((*c)[(b.upper_left())[0]] + (b.upper_left())[1]),x1_((b.upper_left())[0]),x2_((b.upper_left())[1]),box_(b)
  {}

  /*!
  ** \brief Constructs a copy of the %iterator2d_box \a o
  ** 
  ** \param o %iterator2d_box to copy.
  */
  iterator2d_box(const self& o):
    cont2d_(o.cont2d_),pos_(o.pos_),x1_(o.x1_),x2_(o.x2_),box_(o.box_)
  {}

  /*@} End Constructors */

   /**
   ** \name  Assignment operators and methods
   */
  /*@{*/
  /*!
  ** \brief Assign a %iterator2d_box.
  **
  ** Assign elements of %iterator2d_box in \a o.
  **
  ** \param o %iterator2d_box to get the values from.
  ** \return a %iterator2d_box reference.
  */
  self& operator=(const self& o)
  {
    if(this != &o)
      {
	this->cont2d_ = o.cont2d_;
	this->pos_ = o.pos_;
	this->x1_ = o.x1_;
	this->x2_ = o.x2_;
	this->box_ = o.box_;
      }
    return *this;
  }
  /*@} End Assignment operators and methods*/

  /*!
  ** \brief Dereference assignment operator. Returns the element 
  **        that the current %iterator2d_box i point to.  
  ** 
  ** \return a %iterator2d_box reference.       
  */
  inline
  reference operator*()
  {
    return *pos_;
  }

  inline
  pointer operator->()
  { 
    return &(operator*()); 
  }

  /**
   ** \name  Forward operators addons
   */
  /*@{*/
  
  /*!
  ** \brief Preincrement a %iterator2d_box. Iterate to the next location
  **        inside the %Box2d.
  **          
  */
  inline
  self& operator++()
  {
    if( (x1_ != (box_.bottom_right())[0]) || (x2_ != (box_.bottom_right())[1]) )
      {
	if( (x2_ < (box_.bottom_right())[1]) || (x1_ == (box_.bottom_right())[0]))
	  {
	    this->x2_++;
	    this->pos_++;
	  }
	else
	  {
	    this->x1_++;
	    this->x2_ = (box_.upper_left())[1];
	    this->pos_ = cont2d_->begin() + (this->x1_ * int(cont2d_->cols())) + this->x2_; 
	  }
      }
    else
      {
	this->x1_ = (box_.bottom_right())[0] + 1;
	this->x2_ = (box_.bottom_right())[1] + 1;
	this->pos_ = cont2d_->begin() + (this->x1_ * int(cont2d_->cols())) + this->x2_; 
      }

    return *this;
  }

  /*!
  ** \brief Postincrement a %iterator2d_box. Iterate to the next location
  **        inside the %Box2d.
  **          
  */
  inline
  self operator++(int)
  {
    self tmp = *this;
    ++(*this);
    return tmp;
  }

   /*@} End Forward operators addons */

  /**
   ** \name  Bidirectional operators addons
   */
  /*@{*/
  /*!
  ** \brief Predecrement a %iterator2d_box. Iterate to the previous location
  **        inside the %Box2d.
  **          
  */
  inline
  self& operator--()
  {
    if( (x1_ != (box_.upper_left())[0]) || (x2_ != (box_.upper_left())[1]) )
      {
	if( (x2_ > (box_.upper_left())[1])|| (x1_ == (box_.upper_left())[0]))
	  {
	    this->x2_--;
	    this->pos_--;
	  }
	else
	  {
	    this->x1_--;
	    this->x2_ = (box_.bottom_right())[1];
	    this->pos_ = cont2d_->begin() + (this->x1_ * int(cont2d_->cols())) + this->x2_; 
	  }
      }
     else
       {
	 this->x1_ = (box_.upper_left())[0] - 1;
	 this->x2_ = (box_.upper_left())[1] - 1;
	 this->pos_ = cont2d_->begin() + (this->x1_ * int(cont2d_->cols())) + this->x2_; 
       }

    return *this;
  }
  
  /*!
  ** \brief Postdecrement a %iterator2d_box. Iterate to the previous location
  **        inside the %Box2d.
  **          
  */
  inline
  self operator--(int)
  {
    self tmp = *this;
    --(*this);
    return tmp;
  }

 /*@} End Bidirectional operators addons */
  
  /**
   ** \name  Equality comparable operators
   */
  /*@{*/
  /*!
  ** \brief Equality operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true if i1 and i2 point to the same element of
  **         the 2d container
  */
  inline
  friend bool operator==(const self& i1,
			 const self& i2)
  {
    
    return ( (i1.cont2d_ == i2.cont2d_) && (i1.pos_ == i2.pos_) 
	     && (i1.x1_ == i2.x1_) && (i1.x2_ == i2.x2_) );
  }

  /*!
  ** \brief Inequality operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true if !(i1 == i2)
  */
  inline
  friend bool operator!=(const self& i1,
			 const self& i2)
  {
    return ( (i1.cont2d_ != i2.cont2d_) || (i1.pos_ != i2.pos_)
    || (i1.x1_ != i2.x1_) || (i1.x2_ != i2.x2_) );
  }
  /*@} End Equality comparable operators*/

  
 /**
   ** \name  Strict Weakly comparable operators
   */
  /*@{*/
  /*!
  ** \brief < operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true i1.pos_ < i2_pos
  */
  inline
  friend bool operator<(const self& i1,
			const self& i2)
  {
    
    return ( i1.pos_ < i2.pos_);
  }

    /*!
  ** \brief > operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true i2 < i1
  */
  inline
  friend bool operator>(const self& i1,
			const self& i2)
  {
    
    return (i2 < i1);
  }
  
     /*!
  ** \brief <= operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true if !(i2 < i1)
  */
  inline
  friend bool operator<=(const self& i1,
			 const self& i2)
  {
    
    return  !(i2 < i1);
  }

  /*!
  ** \brief >= operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true if !(i1 < i2)
  */
  inline
  friend bool operator>=(const self& i1,
			 const self& i2)
  {
    
    return  !(i1 < i2);
  }
  /*@} End  Strict Weakly comparable operators*/


  /**
   ** \name  iterator2d_box operators addons
   */
  /*@{*/
  /*!
  ** \brief iterator2d_box addition.
  ** \param d difference_type
  ** \return a iterator2d_box reference 
  ** \pre All the iterators between the current iterator i and i + d 
  **      must be dereferenceable.
  */
  inline
  self& operator+=(const difference_type& d)
  {
    this->x1_ += d.dx1();
    this->x2_ += d.dx2();
    this->pos_ = cont2d_->begin() + (this->x1_ * int(cont2d_->cols())) + this->x2_; 
    return *this;
  }

 
  
  /*!
  ** \brief iterator2d_box substraction.
  ** \param d difference_type
  ** \return a iterator2d_box reference 
  ** \pre All the iterators between the current iterator i and i - d 
  **      must be dereferenceable.
  */
  inline
  self& operator-=(const difference_type& d)
  {
    this->x1_ -= d.dx1();
    this->x2_ -= d.dx2();
    this->pos_ = cont2d_->begin() + (this->x1_ * int(cont2d_->cols())) + this->x2_; 
    return *this;
  }
  

   
  /*!
  ** \brief iterator2d_box addition.
  ** \param d difference_type
  ** \return a iterator2d_box reference 
  ** \pre All the iterators between the current iterator i and i + d 
  **      must be dereferenceable.
  */
  inline
  self operator+(const difference_type& d)
  {
    self tmp = *this;
    tmp += d;
    return tmp;
  }

 
  
  /*!
  ** \brief iterator2d_box substraction.
  ** \param d difference_type
  ** \return a iterator2d_box reference 
  ** \pre All the iterators between the current iterator i and i - d 
  **      must be dereferenceable.
  */
  inline
  self operator-(const difference_type& d)
  {
    self tmp = *this;
    tmp -= d;
    return tmp;
  }

  
  

  /*!
  ** \brief %iterator2d_box difference operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return a difference_type d such that i1 = i2 + d.
  ** \pre Either i1 is reachable from i2, or i2 is reachable from i1.
  */ 
  inline
  friend difference_type operator-(const self& i1,
				   const self& i2)
  {
    return difference_type(int(i1.x1_ - i2.x1_),int(i1.x2_ - i2.x2_));
  }
  
  
  /*!
  ** \brief %iterator2d_box element assignment operator.
  **        Equivalent to *(i + d) = t.
  ** \param d difference_type.
  ** \return a reference to the 2d container elements
  ** \pre (i + d) exists and is dereferenceable.
  ** \post i[d] is a copy of reference.
  */ 
  inline
  reference operator[](difference_type d)
  {
    assert( (int(this->x1_)+d.dx1()) < int(cont2d_->dim1()) );
    assert( (int(this->x2_)+d.dx2()) < int(cont2d_->dim2()) );
    return (*cont2d_)[this->x1_+d.dx1()][this->x2_+d.dx2()];
  }



  /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param row offset.
  ** \return a row_iterator to the first element of 
  **         the row \a row of the box
  */ 
  inline
  typename Container2D::row_iterator row_begin(size_type row)
  {
    return cont2d_->row_begin(this->box_.upper_left()[0] + row) + this->box_.upper_left()[1];
  }


  /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param row offset.
  ** \return a row_iterator to the past-the-end element of 
  **         the row \a row of the box
  */ 
  inline
  typename Container2D::row_iterator row_end(size_type row)
  {
    return this->row_begin(row) + this->box_.width();
  }


  /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param row offset.
  ** \return a reverse_row_iterator to the last element of 
  **         the row \a row of the box
  */ 
  inline
  typename Container2D::reverse_row_iterator row_rbegin(size_type row)
  {
    return  typename Container2D::reverse_row_iterator(this->row_end(row));
  }
  
   /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param row offset.
  ** \return a reverse_row_iterator to one previois the first element of 
  **         the row \a row of the box
  */ 
  inline
  typename Container2D::reverse_row_iterator row_rend(size_type row)
  {
    return  typename Container2D::reverse_row_iterator(this->row_begin(row));
  }
 

  /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param col offset.
  ** \return a col_iterator to the first element of 
  **         the col \a col of the box
  */ 
  inline
  typename Container2D::col_iterator col_begin(size_type col)
  {
     return cont2d_->col_begin(this->box_.upper_left()[1] + col) +  this->box_.upper_left()[0];
  }



  /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param col offset.
  ** \return a col_iterator to the first element of
  **         the col \a col of the box
  */ 
  inline
  typename Container2D::col_iterator col_end(size_type col)
  {
    return this->col_begin(col) + this->box_.height();
  }


   /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param col offset.
  ** \return a reverse_col_iterator to the last element of 
  **         the column \a col of the box
  */ 
  inline
  typename Container2D::reverse_col_iterator col_rbegin(size_type col)
  {
    return  typename Container2D::reverse_col_iterator(this->col_end(col));
  }
  
   /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param col offset.
  ** \return a reverse_col_iterator to one previous the first element of 
  **         the column \a col of the box
  */ 
  inline
  typename Container2D::reverse_col_iterator col_rend(size_type col)
  {
    return  typename Container2D::reverse_col_iterator(this->col_begin(col));
  }
 

  /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param row offset.
  ** \param range %Range of the row to iterate.
  ** \return a row_range_iterator to the first element of 
  **         the range of \a row of the box
  */ 
  inline
  typename Container2D::row_range_iterator row_begin(size_type row, const slip::Range<int>& range)
  {
    return slip::stride_iterator<typename Container2D::row_iterator>(cont2d_->row_begin(this->box_.upper_left()[0] + row) + this->box_.upper_left()[1] + range.start(),range.stride());
  }


  /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param row offset.
   ** \param range %Range of the row to iterate.
   ** \return a row_iterator to the past-the-end element of 
  **         the range of \a row of the box
  */ 
  inline
  typename Container2D::row_range_iterator row_end(size_type row, const slip::Range<int>& range)
  {
    return ++(this->row_begin(row,range)+ range.iterations());
  }

  /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param col offset.
  ** \param range %Range of the col to iterate.
  ** \return a col_iterator to the first element of 
  **         the range of \a col of the box
  */ 
  inline
  typename Container2D::col_range_iterator col_begin(size_type col, const slip::Range<int>& range)
  {
    return slip::stride_iterator<typename Container2D::col_iterator>(cont2d_->col_begin(this->box_.upper_left()[1] + col) + this->box_.upper_left()[0] + range.start(),range.stride());
  }



  /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param col offset.
  ** \param range %Range of the col to iterate.
  ** \return a col_iterator to the first element of
  **         the range of \a col of the box
  */ 
  inline
  typename Container2D::col_range_iterator col_end(size_type col, const slip::Range<int>& range)
  {
    return ++(this->col_begin(col,range)+ range.iterations());
  }

 /*@} End  iterator2d_box operators addons */
  

  /**
   ** \name Indices accessors
   */
  /*@{*/
  /*!
  ** \brief Access to the first index of the current %iterator2d_box.
  ** \return the  first index.
  */
  inline
  int x1() const
  {
    return this->x1_;
  }
  /*!
  ** \brief Access to the first index of the current %iterator2d_box.
  ** \return the  first index.
  */
  inline
  int i() const
  {
    return this->x1_;
  }
  /*!
  ** \brief Access to the second index of the current %iterator2d_box.
  ** \return the  second index.
  */
  inline
  int x2() const
  {
    return this->x2_;
  }
  /*!
  ** \brief Access to the second index of the current %iterator2d_box.
  ** \return the  second index.
  */
  inline
  int j() const
  {
    return this->x2_;
  }
  
  /*@} End  Indices accessors */


  
private: 
  Container2D* cont2d_; // pointer to the 2d container
  pointer pos_;         // linear position within the container
  int x1_;              // first index position 
  int x2_;              // second index position
  Box2d<int> box_;      // box to iterate 
};


/*! \class const_iterator2d_box
** 
** \brief This is some iterator to iterate a 2d container into a %Box area 
**        defined by the indices of the 2d container.
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \author Julien Dombre <dombre_AT_sic.univ-poitiers.fr>
** \version 0.0.4
** \date 18/12/2007
**
** \param Container2D Type of 2d container in the const_iterator2d_box 
**
** \par Description:
**      This iterator is an 2d extension of the random_access_iterator.
**      It is compatible with the bidirectional_iterator of the
**      Standard library. It iterate into a box area defined inside the
**      indices range of the 2d container. Those indices are defined as 
**      follows :
** \code
**      (0,0)--------> x2 (or j)
**        |
**        |
**        |
**        |
**       \|/
**        x1 (or i)
**
**      (0,0) is the upper left corner of the 2d container.
** \endcode
*/
template <class Container2D>
class const_iterator2d_box  
{
public:
  //typedef std::random_access_iterator_tag iterator_category;
  typedef std::random_access_iterator2d_tag iterator_category;
  typedef typename Container2D::value_type value_type;
  typedef DPoint2d<int> difference_type;
  typedef typename Container2D::const_pointer pointer;
  typedef typename Container2D::const_reference reference;

  typedef const_iterator2d_box self;

  typedef typename Container2D::size_type size_type;
  /**
   ** \name Constructors & Destructors
   */
  /*@{*/
  
  /*!
  ** \brief Constructs a %const_iterator2d_box.
  ** \par The box to iterate is in this case the full container
  */
  const_iterator2d_box():
    cont2d_(0),pos_(0),x1_(0),x2_(0),box_(0,0,0,0)
  {}

  /*!
  ** \brief Constructs a %const_iterator2d_box.
  ** \param c pointer to an existing 2d container
  ** \param b Box2d<int> defining the range of indices to iterate
  ** \pre box indices must be inside those of the 2d container
  */
  const_iterator2d_box(Container2D* c,
		 const Box2d<int>& b):
    cont2d_(c),pos_((*c)[(b.upper_left())[0]] + (b.upper_left())[1]),x1_((b.upper_left())[0]),x2_((b.upper_left())[1]),box_(b)
  {}

  /*!
  ** \brief Constructs a copy of the %const_iterator2d_box \a o
  ** 
  ** \param o %const_iterator2d_box to copy.
  */
  const_iterator2d_box(const self& o):
    cont2d_(o.cont2d_),pos_(o.pos_),x1_(o.x1_),x2_(o.x2_),box_(o.box_)
  {}

  /*@} End Constructors */

   /**
   ** \name  Assignment operators and methods
   */
  /*@{*/
  /*!
  ** \brief Assign a %const_iterator2d_box.
  **
  ** Assign elements of %const_iterator2d_box in \a o.
  **
  ** \param o %const_iterator2d_box to get the values from.
  ** \return a %const_iterator2d_box reference.
  */
  self& operator=(const self& o)
  {
    if(this != &o)
      {
	this->cont2d_ = o.cont2d_;
	this->pos_ = o.pos_;
	this->x1_ = o.x1_;
	this->x2_ = o.x2_;
	this->box_ = o.box_;
      }
    return *this;
  }
  /*@} End Assignment operators and methods*/

 
 

  /*!
  ** \brief Dereference operator.  Returns the element 
  **        that the current %const_iterator2d_box i point to.  
  ** 
  ** \return a const %const_iterator2d_box reference.     
  */
  inline
  reference operator*() const
  {
    return *pos_;
  }


  inline
  pointer operator->() const
  { 
    return &(operator*()); 
  }

  /**
   ** \name  Forward operators addons
   */
  /*@{*/
  
  /*!
  ** \brief Preincrement a %const_iterator2d_box. Iterate to the next location
  **        inside the %Box2d.
  **          
  */
  inline
  self& operator++()
  {
    if( (x1_ != (box_.bottom_right())[0]) || (x2_ != (box_.bottom_right())[1]) )
      {
	if( (x2_ < (box_.bottom_right())[1]) || (x1_ == (box_.bottom_right())[0]))
	  {
	    this->x2_++;
	    this->pos_++;
	  }
	else
	  {
	    this->x1_++;
	    this->x2_ = (box_.upper_left())[1];
	    this->pos_ = cont2d_->begin() + (this->x1_ * int(cont2d_->cols())) + this->x2_; 
	  }
      }
    else
       {
	 this->x1_ = (box_.bottom_right())[0] + 1;
	 this->x2_ = (box_.bottom_right())[1] + 1;
	 this->pos_ = cont2d_->begin() + (this->x1_ * int(cont2d_->cols())) + this->x2_; 
       }
    return *this;
  }

  /*!
  ** \brief Postincrement a %const_iterator2d_box. Iterate to the next location
  **        inside the %Box2d.
  **          
  */
  inline
  self operator++(int)
  {
    self tmp = *this;
    ++(*this);
    return tmp;
  }

   /*@} End Forward operators addons */

  /**
   ** \name  Bidirectional operators addons
   */
  /*@{*/
  /*!
  ** \brief Predecrement a %const_iterator2d_box. Iterate to the previous location
  **        inside the %Box2d.
  */
  inline
  self& operator--()
  {
      if( (x1_ != (box_.upper_left())[0]) || (x2_ != (box_.upper_left())[1]) )
      {
	if( (x2_ > (box_.upper_left())[1])|| (x1_ == (box_.upper_left())[0]))
	  {
	    this->x2_--;
	    this->pos_--;
	  }
	else
	  {
	    this->x1_--;
	    this->x2_ = (box_.bottom_right())[1];
	    this->pos_ = cont2d_->begin() + (this->x1_ * int(cont2d_->cols())) + this->x2_; 
	  }
      }
     else
       {
	 this->x1_ = (box_.upper_left())[0] - 1;
	 this->x2_ = (box_.upper_left())[1] - 1;
	 this->pos_ = cont2d_->begin() + (this->x1_ * int(cont2d_->cols())) + this->x2_; 
       }

    return *this;
  }
  
  /*!
  ** \brief Postdecrement a %const_iterator2d_box. Iterate to the previous location
  **        inside the %Box2d.
  **          
  */
  inline
  self operator--(int)
  {
    self tmp = *this;
    --(*this);
    return tmp;
  }

 /*@} End Bidirectional operators addons */
  
  /**
   ** \name  Equality comparable operators
   */
  /*@{*/
  /*!
  ** \brief Equality operator.
  ** \param i1 first %const_iterator2d_box.
  ** \param i2 second %const_iterator2d_box.
  ** \return true if i1 and i2 point to the same element of
  **         the 2d container
  */
  inline
  friend bool operator==(const self& i1,
			 const self& i2)
  {
    
    return ( (i1.cont2d_ == i2.cont2d_) && (i1.pos_ == i2.pos_) 
	     && (i1.x1_ == i2.x1_) && (i1.x2_ == i2.x2_) );
  }

  /*!
  ** \brief Inequality operator.
  ** \param i1 first %const_iterator2d_box.
  ** \param i2 second %const_iterator2d_box.
  ** \return true if !(i1 == i2)
  */
  inline
  friend bool operator!=(const self& i1,
			 const self& i2)
  {
    return ( (i1.cont2d_ != i2.cont2d_) || (i1.pos_ != i2.pos_)
    || (i1.x1_ != i2.x1_) || (i1.x2_ != i2.x2_) );
  }
  /*@} End Equality comparable operators*/

  /**
   ** \name  Strict Weakly comparable operators
   */
  /*@{*/
  /*!
  ** \brief < operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true i1.pos_ < i2_pos
  */
  inline
  friend bool operator<(const self& i1,
			const self& i2)
  {
    
    return ( i1.pos_ < i2.pos_);
  }

    /*!
  ** \brief > operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true i2 < i1
  */
  inline
  friend bool operator>(const self& i1,
			const self& i2)
  {
    
    return (i2 < i1);
  }
  
     /*!
  ** \brief <= operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true if !(i2 < i1)
  */
  inline
  friend bool operator<=(const self& i1,
			 const self& i2)
  {
    
    return  !(i2 < i1);
  }

  /*!
  ** \brief >= operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true if !(i1 < i2)
  */
  inline
  friend bool operator>=(const self& i1,
			 const self& i2)
  {
    
    return  !(i1 < i2);
  }
  /*@} End  Strict Weakly comparable operators*/


  /**
   ** \name  const_iterator2d_box operators addons
   */
  /*@{*/
  /*!
  ** \brief const_iterator2d_box addition.
  ** \param d difference_type
  ** \return a const_iterator2d_box reference 
  ** \pre All the iterators between the current iterator i and i + d 
  **      must be dereferenceable.
  */
  inline
  self& operator+=(const difference_type& d)
  {
    this->x1_ += d.dx1();
    this->x2_ += d.dx2();
    this->pos_ = cont2d_->begin() + (this->x1_ * int(cont2d_->cols())) + this->x2_; 
    return *this;
  }

 
  
  /*!
  ** \brief const_iterator2d_box substraction.
  ** \param d difference_type
  ** \return a const_iterator2d_box reference 
  ** \pre All the iterators between the current iterator i and i - d 
  **      must be dereferenceable.
  */
  inline
  self& operator-=(const difference_type& d)
  {
    this->x1_ -= d.dx1();
    this->x2_ -= d.dx2();
    this->pos_ = cont2d_->begin() + (this->x1_ * int(cont2d_->cols())) + this->x2_; 
    return *this;
  }
  

   
  /*!
  ** \brief const_iterator2d_box addition.
  ** \param d difference_type
  ** \return a const_iterator2d_box reference 
  ** \pre All the iterators between the current iterator i and i + d 
  **      must be dereferenceable.
  */
  inline
  self operator+(const difference_type& d)
  {
    self tmp = *this;
    tmp += d;
    return tmp;
  }

 
  
  /*!
  ** \brief const_iterator2d_box substraction.
  ** \param d difference_type
  ** \return a const_iterator2d_box reference 
  ** \pre All the iterators between the current iterator i and i - d 
  **      must be dereferenceable.
  */
  inline
  self operator-(const difference_type& d)
  {
    self tmp = *this;
    tmp -= d;
    return tmp;
  }

  
  

  /*!
  ** \brief %const_iterator2d_box difference operator.
  ** \param i1 first %const_iterator2d_box.
  ** \param i2 second %const_iterator2d_box.
  ** \return a difference_type d such that i1 = i2 + d.
  ** \pre Either i1 is reachable from i2, or i2 is reachable from i1.
  */ 
  inline
  friend difference_type operator-(const self& i1,
				   const self& i2)
  {
    return difference_type(int(i1.x1_ - i2.x1_),int(i1.x2_ - i2.x2_));
  }
  
  
 
  /*!
  ** \brief %const_iterator2d_box element assignment operator.
  **        Equivalent to *(i + d).
  ** \param d difference_type.
  ** \return a const reference to the 2d container elements
  ** \pre (i + d) exists and is dereferenceable.
  
  */ 
  inline
  reference operator[](difference_type d) const
  {
    assert( int(this->x1_+d.dx1()) < int(cont2d_->dim1()) );
    assert( int(this->x2_+d.dx2()) < int(cont2d_->dim2()) );
    return (*cont2d_)[this->x1_+d.dx1()][this->x2_+d.dx2()];
  }


 


  /*!
  ** \brief %const_iterator2d_box element assignment operator.
  ** \param row offset.
  ** \return a const_row_iterator to the first element of 
  **         the row \a row of the box
  ** \todo add assert
  */ 
  inline
  typename Container2D::const_row_iterator row_begin(size_type row) const
  {
    return cont2d_->row_begin(this->box_.upper_left()[0] + row) + this->box_.upper_left()[1];
  }

 
 /*!
  ** \brief %const_iterator2d_box element assignment operator.
  ** \param row offset.
  ** \return a const_row_iterator to the past-the-end element of 
  **         the row \a row of the box
  ** \todo add assert
  */ 
  inline
  typename Container2D::const_row_iterator row_end(size_type row) const
  {
    return this->row_begin(row) + this->box_.width();
  }

 
 /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param row offset.
  ** \return a const_reverse_row_iterator to the last element of 
  **         the row \a row of the box
  */ 
  inline
  typename Container2D::const_reverse_row_iterator row_rbegin(size_type row) const
  {
    return  typename Container2D::const_reverse_row_iterator(this->row_end(row));
  }
  

  
  /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param row offset.
  ** \return a const_reverse_row_iterator to one previois the first element of 
  **         the row \a row of the box
  */ 
  inline
  typename Container2D::const_reverse_row_iterator row_rend(size_type row) const
  {
    return  typename Container2D::const_reverse_row_iterator(this->row_begin(row));
  }

  /*!
  ** \brief %const_iterator2d_box element assignment operator.
  ** \param col offset.
  ** \return a const col_iterator to the first element of 
  **         the col \a col of the box
  ** \todo add assert
  */ 
  inline
  typename Container2D::const_col_iterator col_begin(size_type col) const
  {
    return cont2d_->col_begin(this->box_.upper_left()[1] + col) +  this->box_.upper_left()[0];
  }


   /*!
  ** \brief %const_iterator2d_box element assignment operator.
  ** \param col offset.
  ** \return a const col_iterator to the past-the-end element of 
  **         the col \a col of the box
  ** \todo add assert
  */ 
  inline
  typename Container2D::const_col_iterator col_end(size_type col) const
  {
    return this->col_begin(col) + this->box_.height();
  }

  /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param col offset.
  ** \return a const_reverse_col_iterator to the last element of 
  **         the column \a col of the box
  */ 
  inline
  typename Container2D::const_reverse_col_iterator col_rbegin(size_type col) const
  {
    return  typename Container2D::const_reverse_col_iterator(this->col_end(col));
  }
  
   /*!
  ** \brief %iterator2d_box element assignment operator.
  ** \param col offset.
  ** \return a const_reverse_col_iterator to one previous the first element of 
  **         the column \a col of the box
  */ 
  inline
  typename Container2D::const_reverse_col_iterator col_rend(size_type col) const
  {
    return  typename Container2D::const_reverse_col_iterator(this->col_begin(col));
  }
 

  /*@} End  const_iterator2d_box operators addons */
  

  /**
   ** \name Indices accessors
   */
  /*@{*/
  /*!
  ** \brief Access to the first index of the current %const_iterator2d_box.
  ** \return the  first index.
  */
  inline
  int x1() const
  {
    return this->x1_;
  }
  /*!
  ** \brief Access to the first index of the current %const_iterator2d_box.
  ** \return the  first index.
  */
  inline
  int i() const
  {
    return this->x1_;
  }
  /*!
  ** \brief Access to the second index of the current %const_iterator2d_box.
  ** \return the  second index.
  */
  inline
  int x2() const
  {
    return this->x2_;
  }
  /*!
  ** \brief Access to the second index of the current %const_iterator2d_box.
  ** \return the  second index.
  */
  inline
  int j() const
  {
    return this->x2_;
  }
  
  /*@} End  Indices accessors */


  
private: 
  Container2D* cont2d_; // pointer to the 2d container
  pointer pos_;         // linear position within the container
  int x1_;              // first index position 
  int x2_;              // second index position
  Box2d<int> box_;      // box to iterate 
};


 
}//slip::

#endif //SLIP_ITERATOR2D_BOX_HPP
