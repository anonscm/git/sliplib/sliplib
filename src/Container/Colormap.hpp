/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Colormap.hpp
 * 
 * \brief Provides a class to manipulate colormap.
 * 
 */
#ifndef SLIP_COLORMAP_HPP
#define SLIP_COLORMAP_HPP
#include <iostream>
#include <iterator>
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <vector>
#include "Color.hpp"
#include "Array2d.hpp"
#include "io_tools.hpp"
#include "utils_compilation.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/vector.hpp>

namespace slip
{

template <typename T>
class Colormap;

template <typename T>
std::ostream& operator<<(std::ostream & out, 
			 const Colormap<T>& a);

template<typename T>
bool operator==(const Colormap<T>& x, 
		const Colormap<T>& y);

template<typename T>
bool operator!=(const Colormap<T>& x, 
		const Colormap<T>& y);


/*! \class Colormap
**  \ingroup Containers Containers1d
** \brief Colormap container.
** This is a linear (one-dimensional) dynamic template container.
** This container statisfies the RandomAccessContainer concepts of the 
** Standard Template Library (STL).
** 
** \author Thibault Roulier <troulier_AT_sic.univ-poitiers.fr>
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.3
** \date 2014/03/17
** \since 1.2.0
** \param T Type of the element in the %Colormap
**
*/
template <typename T>
class Colormap
{
public:
  typedef typename std::vector<slip::Color<T> >::value_type value_type;
  typedef Colormap<T> self;
  typedef const Colormap<T> const_self;

  typedef typename std::vector<slip::Color<T> >::pointer pointer;
  typedef typename std::vector<slip::Color<T> >::const_pointer const_pointer;
  typedef typename std::vector<slip::Color<T> >::reference reference;
  typedef typename std::vector<slip::Color<T> >::const_reference const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef typename std::vector<slip::Color<T> >::iterator iterator;
  typedef typename std::vector<slip::Color<T> >::const_iterator const_iterator;

  typedef slip::stride_iterator<pointer> iterator_range;
  typedef slip::stride_iterator<const_pointer> const_iterator_range;

  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

  typedef std::reverse_iterator<iterator_range> reverse_iterator_range;
  typedef std::reverse_iterator<const_iterator_range> const_reverse_iterator_range;

  //default iterator of the container
  typedef iterator default_iterator;
  typedef const_iterator const_default_iterator;

  //static const std::size_t DIM = 1;

public:
  /**
   ** \name Constructors & Destructors
   */
  /*@{*/

  /*!
  ** \brief Constructs a %Colormap.
  */
  Colormap();
 
  /*!
  ** \brief Constructs a %Colormap.
  ** \param size dimension of the %Colormap
  **
  ** \par The %Colormap is initialized with the default value of T.
  */
  Colormap(const size_type size);
  
  /*!
  ** \brief Constructs a %Colormap initialized by the scalar value \a val.
  ** \param size dimension of the %Colormap
  ** \param val initialization value of the elements 
  */
  Colormap(const size_type size,
	   const slip::Color<T>& val);
 
  
  /*!
  ** \brief Constructs a random %Colormap.
  ** \param size dimension of the %Colormap
  ** \param init_value  init generated value.
  ** \param final_value final generated value.
  **
  */
  Colormap(const size_type size,
	   const T& init_value,
	   const T& final_value);

 /**
  **  \brief  Constructs a %Colormap from a range.
  **  \param  n Size of the %Colormap.
  **  \param  first  An input iterator.
  **  \param  last  An input iterator.
  **
  ** Create a %Colormap consisting of copies of the elements from
  ** [first,last).
  */
  template<typename InputIterator>
  Colormap(const size_type UNUSED(n),
	InputIterator first,
	InputIterator last):
    data_(new std::vector<slip::Color<T> > (first,last))
  {}
  
 /*!
  ** \brief Constructs a copy of the %Colormap \a rhs
  */
  Colormap(const self& rhs);

 /*!
  ** \brief Destructor of the %Colormap
  */
  ~Colormap();
  

  /*@} End Constructors */


  /*!
  ** \brief Resizes a %Colormap.
  ** \param size new dimension
  ** \param val new value for all the elements
  */ 
  void resize(const size_type size,
	      const slip::Color<T>& val = slip::Color<T>());

  /**
   ** \name iterators
   */
  /*@{*/
 
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %Colormap.  Iteration is done in ordinary
  **  element order.
  ** \return begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator begin();

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %Colormap.  Iteration is done in ordinary
  **  element order.
  ** \return const begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator begin() const;

  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %Colormap.  Iteration is done in ordinary
  **  element order.
  ** \return end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator end();
 
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %Colormap.  Iteration is done in
  **  ordinary element order.
  ** \return const end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator end() const;
  
  /*!
  **  \brief Returns a read/write reverse iterator that points to the
  **  last element in the %Colormap.  Iteration is done in reverse
  **  element order.
  ** \return reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rbegin();
  
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to the last element in the %Colormap.  Iteration is done in
  **  reverse element order.
  ** \return const reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_reverse_iterator rbegin() const;

  /*!
  **  \brief Returns a read/write reverse iterator that points to one
  **  before the first element in the %Colormap.  Iteration is done
  **  in reverse element order.
  **  \return reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rend();

  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to one before the first element within the %Colormap.  Iteration
  **  is done in reverse element order.
  **  \return const reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_reverse_iterator rend() const;

  
 

  /*@} End iterators */

 
  /**
   ** \name i/o operators
   */
  /*@{*/
  
  /*!
  ** \brief Write the %Colormap to the ouput stream
  ** \param out output std::ostream
  ** \param a %Colormap to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const self& a);
  /*@} End i/o operators */
  

  /**
   ** \name  Assignment operators and methods
   */
  /*@{*/

  /*!
  ** \brief Assigns a %Colormap in \a rhs
  **
  ** \param rhs %Colormap to get the values from.
  ** \return 
  */
  self& operator=(const self& rhs);
  
  /*!
  ** \brief Assign all the elements of the %Colormap by value
  **
  **
  ** \param value A reference-to-const of arbitrary type.
  ** \return 
  */
  self& operator=(const slip::Color<T>& value)
  {
    this->fill(value);
    return *this;
  }

  /*!
   ** \brief Fills the container range [begin(),begin()+N) with copies of value
   ** \param value  A reference-to-const of arbitrary type.
  */
  void fill(const slip::Color<T>& value)
  {
    std::fill_n(this->begin(),this->size(),value);
  }

  /*!
   ** \brief convert the color map into array2D
   ** \deprecated
  */
  slip::Array2d<T> to_array2d()
  {
    	slip::Array2d<T> array_color(this->size(),3);

	for (std::size_t i=0 ; i<this->size() ; ++i)
	{
		array_color[i][0] = (*data_)[i].get_red();
		array_color[i][1] = (*data_)[i].get_green();
		array_color[i][2] = (*data_)[i].get_blue();

	}

	return array_color;

  }

/*!
   ** \brief convert the color map into array2D
   ** \param array_color Colormap Container2d.
  */
  template <typename Container2d>
  void to_array2d(Container2d& array_color)
  {
    const std::size_t n = this->size();
    array_color.resize(n,3);
    
    for (std::size_t i = 0 ; i < n ; ++i)
      {
	array_color[i][0] = (*data_)[i].get_red();
	array_color[i][1] = (*data_)[i].get_green();
	array_color[i][2] = (*data_)[i].get_blue();
	
      }

	   //return array_color;

  }

  /*!
   ** \brief create colormap from array2d
   ** \param array_color A 2D container 
   ** \return true if the colormap is created, false if the dimensions of the input 2D container doesn't match to a colormap
  */
template<typename Container2d>
  bool from_array2d(const Container2d& array_color)
  {
	
	if (array_color.cols() != 3)
	  {
	    return false;	
	  }
	else
	  {
	    data_->resize(array_color.rows());
	    for (std::size_t i=0 ; i < this->size() ; ++i)
	      {
		(*data_)[i].set_red(array_color[i][0]);
		(*data_)[i].set_green(array_color[i][1]);
		(*data_)[i].set_blue(array_color[i][2]);
		
	      }
	    
	    return true;
	  }
  }


    /*!
   ** \brief write Colormap in an ASCII file
   ** \param file_name A std::string for the file name which will be saved.
   */
  void write(const std::string& file_name)
  {
    	slip::Array2d<T> array_write(this->size(), 3);

	array_write = this->to_array2d();
	slip::write_ascii_2d<typename slip::Array2d<T>::iterator2d,T>(array_write.upper_left(), array_write.bottom_right(), file_name);

  }



  /*!
   ** \brief create a colormap from file
   ** \param file_name A std::string for the file which will be read to create the colormap
   ** \return true if the colormap is created, false if the file doesn't match
  */
  bool read(const std::string& file_name)
  {
	bool readed;
	slip::Array2d<T> array_read;
	
	slip::read_ascii_2d(array_read, file_name);
	
	data_ = new std::vector<slip::Color<T> >(array_read.rows());
        readed = this->from_array2d(array_read);

	return readed;
  }



  /*!
   ** \brief randomize the colormap
   ** \param low A value for the minimum value of the randomization
   ** \param high A value for the maximum value of the randomization
  */
  void randomize(const T &low, const T&high)
  {
      srand(time(NULL));

	
      for (std::size_t i=0 ; i<data_->size(); ++i)
	{
                (*data_)[i].set_red(this->random(low, high));
                (*data_)[i].set_green(this->random(low, high));
                (*data_)[i].set_blue(this->random(low, high));
                //std::cout<<(*data_)[i].get_red()<<"  ";
	}
  }






  /*!
  ** \brief Fills the container range [begin(),begin()+N) with a copy of
  **        the range [first,last)
  **  \param  first  An input iterator.
  **  \param  last   An input iterator.
  **   
  **
  */
  template<typename InputIterator>
  void fill(InputIterator first,
	    InputIterator last)
  {
    std::copy(first,last,this->begin());
  }
  /*@} End Assignment operators and methods*/

  /**
   ** \name Comparison operators
   */
  /*@{*/
  /*!
  ** \brief %Colormap equality comparison
  ** \param x A %Colormap
  ** \param y A %Colormap of the same type of \a x
  ** \return true iff the size and the elements of the Colormaps are equal
  */
  friend bool operator== <>(const Colormap<T>& x, 
			    const Colormap<T>& y);

 /*!
  ** \brief %Colormap inequality comparison
  ** \param x A %Colormap
  ** \param y A %Colormap of the same type of \a x
  ** \return true if !(x == y) 
  */
  friend bool operator!= <>(const Colormap<T>& x, 
			    const Colormap<T>& y);

 

  /*@} Comparison operators */
  
  /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Subscript access to the data contained in the %Colormap.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read/write reference to data.
  ** \pre n < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator[](const size_type n);

 
  /*!
  ** \brief Subscript access to the data contained in the %Colormap.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read-only (constant) reference to data.
  ** \pre n < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator[](const size_type n) const;
  
  /*!
  ** \brief Subscript access to the data contained in the %Colormap.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read/write reference to data.
  ** \pre n < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const size_type n);
  
  /*!
  ** \brief Subscript access to the data contained in the %Colormap.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read-only (constant) reference to data.
  ** \pre n < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const size_type n) const;


 

  /*@} End Element access operators */

  
  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;

  /*!
  ** \brief Returns the number of elements in the %Colormap
  */
  size_type size() const;
  /*!
  ** \brief Returns the maximal size (number of elements) in the %Colormap
  */
  size_type max_size() const;

  /*!
  ** \brief Returns true if the %Colormap is empty.  (Thus size() == 0)
  */
  bool empty()const;

  /*!
  ** \brief Swaps data with another %Colormap.
  ** \param rhs A %Colormap of the same element type
  */
  void swap(self& rhs);



  // /*!
  // ** \brief Set value at the defined index
  // ** \param rgb The rgb value we want to set in the colormap
  // ** \param ind The index of the colormap where we want to set the rgb value
  // ** \deprecated
  // */
  // void set_data_ind(const slip::Color<T> rgb,
  // 			int ind)
  // {
  // 	if (ind < (*data_).size())
  // 	{
  // 		(*data_)[ind] = rgb;
  // 	}

  // }

  // /*!
  // ** \brief return a slip::Color<T> ar the wanted indice in the colormap
  // ** \param int ind, the indice of return element
  // ** \deprecated
  // */
  // slip::Color<T> get_data_ind(int ind)
  // {
  // 	return (*data_)[ind];

  // }



private:
  std::vector<slip::Color<T> >* data_;

  /// Allocates the  memory to store the elements of the Colormap
  void allocate(const std::size_t n);
  /// Desallocates the memory
  void desallocate();
  /*!
  ** \brief Generate a random number
  ** \param
  */
  T random(const T& low, const T& high)
  {

    const double scale = rand()/static_cast<double>(RAND_MAX);
    return low + static_cast<T>(scale*(high-low));
  }
 
private:
friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & this->data_ ;
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & this->data_ ;
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
    
};

///double alias
  typedef slip::Colormap<double> Colormap_d;
  ///float alias
  typedef slip::Colormap<float> Colormap_f;
  ///long alias
  typedef slip::Colormap<long> Colormap_l;
  ///unsigned long alias
  typedef slip::Colormap<unsigned long> Colormap_ul;
  ///short alias
  typedef slip::Colormap<short> Colormap_s;
  ///unsigned long alias
  typedef slip::Colormap<unsigned short> Colormap_us;
  ///int alias
  typedef slip::Colormap<int> Colormap_i;
  ///unsigned int alias
  typedef slip::Colormap<unsigned int> Colormap_ui;
  ///char alias
  typedef slip::Colormap<char> Colormap_c;
  ///unsigned char alias
  typedef slip::Colormap<unsigned char> Colormap_uc;

}//slip::

namespace slip
{

  template<typename T>
  inline
  Colormap<T>::Colormap():
    data_(0)
  {}


  template<typename T>
  inline
  Colormap<T>::Colormap(const typename Colormap<T>::size_type n):
    data_(new std::vector<slip::Color<T> >(n))
  {}

  template<typename T>
  inline
  Colormap<T>::Colormap(typename Colormap<T>::size_type n,
			const slip::Color<T>& val):
	  data_(new std::vector<slip::Color<T> >(n,val))
  {}
  
  template<typename T>
  inline
  Colormap<T>::Colormap(const typename Colormap<T>::size_type n,
			const T& init_value,
			const T& final_value):
    data_(new std::vector<slip::Color<T> >(n))
  {
    srand(static_cast<unsigned int>(clock()));
    for(size_t i = 0; i < this->size(); ++i)
      {
	T r = this->random(init_value,final_value);
	T g = this->random(init_value,final_value);
	T b = this->random(init_value,final_value);
	(*data_)[i] = slip::Color<T>(r,g,b);
      }
  }


  template<typename T>
  inline
  Colormap<T>::Colormap(const Colormap<T>& rhs):
    data_(new std::vector<slip::Color<T> >(*rhs.data_))
  {
    //std::copy(rhs.data_,rhs.data_ + this->size(),this->data_);
  }

  template<typename T>
  inline
  Colormap<T>::~Colormap()
  {
    this->desallocate();
  }

  template<typename T>
  inline
  Colormap<T>& Colormap<T>::operator=(const self& rhs)
  {
    if(this != &rhs)
      {
	if(this->size() != rhs.size())
	  {
	    this->desallocate();
	    this->allocate(rhs.size());
	  }
	if(rhs.data_ != 0)
	  {
	    std::copy((rhs.data_)->begin(),(rhs.data_)->begin() + this->size(),this->data_->begin());
	  }
      }
    return *this;
  } 
  
  
  template<typename T>
  inline
  void Colormap<T>::resize(typename Colormap<T>::size_type size,
			   const slip::Color<T>& val)
  {
    Colormap<T>::data_->resize(size,val);
    
  }

  template<typename T>
  inline
  typename Colormap<T>::iterator Colormap<T>::begin()
  {
    return Colormap<T>::data_->begin();
  }

  template<typename T>
  inline
  typename Colormap<T>::iterator Colormap<T>::end()
  {
    return Colormap<T>::data_->end();
  }
  
  template<typename T>
  inline
  typename Colormap<T>::const_iterator Colormap<T>::begin() const
  {
    return Colormap<T>::data_->begin();
  }

  template<typename T>
  inline
  typename Colormap<T>::const_iterator Colormap<T>::end() const
  {
    return Colormap<T>::data_->end();
  }
  

template<typename T>
  inline
  typename Colormap<T>::reverse_iterator Colormap<T>::rbegin()
  {
    return Colormap<T>::data_->rbegin();
  }

  template<typename T>
  inline
  typename Colormap<T>::reverse_iterator Colormap<T>::rend()
  {
    return Colormap<T>::data_->rend();
  }
  
  template<typename T>
  inline
  typename Colormap<T>::const_reverse_iterator Colormap<T>::rbegin() const
  {
    return Colormap<T>::data_->rbegin();
  }

  template<typename T>
  inline
  typename Colormap<T>::const_reverse_iterator Colormap<T>::rend() const
  {
    return Colormap<T>::data_->rend();
  }
  

 
 /** \name input/output operators */
  /* @{ */
  template <typename T>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const Colormap<T>& a)
  {
    if(a.data_ != 0)
      {
	out<<"(";
	for(std::size_t n = 0; n < a.size() - 1; ++n)
	  {
	    out<<a[n]<<",";
	  }
	out<<a[a.size()-1];
	out<<")";
      }
    else
      {
	out<<"()";
      }
    return out;
  }
   /* @} */

  template <typename T>
  inline
  typename Colormap<T>::reference Colormap<T>::operator[](typename Colormap<T>::size_type i)
  {
    return (*data_)[i];
  }

  template <typename T>
  inline
  typename Colormap<T>::const_reference Colormap<T>::operator[](typename Colormap<T>::size_type i) const
  {
    return (*data_)[i];
  }

  template <typename T>
  inline
  typename Colormap<T>::reference Colormap<T>::operator()(typename Colormap<T>::size_type i)
  {
    return (*data_)[i];
  }

  template <typename T>
  inline
  typename Colormap<T>::const_reference Colormap<T>::operator()(typename Colormap<T>::size_type i) const
  {
    return (*data_)[i];
  }
 
 

  template<typename T>
  inline
  std::string 
  Colormap<T>::name() const {return "Colormap";} 


  template <typename T>
  inline
	  typename Colormap<T>::size_type Colormap<T>::size() const 
  {
    typename Colormap<T>::size_type s = 0;
    if(data_ != 0)
      {
	s = Colormap<T>::data_->size();
      }
    
    return s;
  }
  
  template <typename T>
  inline
  typename Colormap<T>::size_type Colormap<T>::max_size() const 
  {
    typename Colormap<T>::size_type s = 0;
    if(data_ != 0)
      {
	s = Colormap<T>::data_->max_size();
      }
    
    return s;
  }
  
  template<typename T>
  inline
  bool Colormap<T>::empty()const 
  {
    if(data_ != 0)
      {
	return Colormap<T>::data_->empty();
      }
    else
      {
	return true;
      }
  }

  template<typename T>
  inline
  void Colormap<T>::swap(Colormap<T>& M)
  {
    std::swap_ranges(data_->begin(),data_->end(),M.begin());
  }

 
  

template<typename T>
  inline
void Colormap<T>::allocate(const std::size_t n)
  {
    
    this->data_ = new std::vector<slip::Color<T> >(n);
  }

  template<typename T>
  inline
  void Colormap<T>::desallocate()
  {
    if(this->data_ != 0)
      {
	delete this->data_;
      }
  }

 

/** \name EqualityComparable functions */
  /* @{ */

template<typename T>
inline
bool operator==(const Colormap<T>& x, 
		const Colormap<T>& y)
{
  return ( x.size() == y.size()
	   && std::equal(x.begin(),x.end(),y.begin())); 
}

template<typename T>
inline
bool operator!=(const Colormap<T>& x, 
		const Colormap<T>& y)
{
  return !(x == y);
}

/* @} */


}//slip::
#endif //SLIP_COLORMAP_HPP
