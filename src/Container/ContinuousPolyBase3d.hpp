/** 
 * \file ContinuousPolyBase3d.hpp
 * 
 * \brief Provides a class to handle global approximation of 3d container by continuous polynomials.
 * 
 */

#ifndef SLIP_CONTINUOUSPOLYBASE3D_HPP
#define SLIP_CONTINUOUSPOLYBASE3D_HPP

#include <iostream>
#include <string>
#include <map>


#include "MultivariatePolynomial.hpp"
#include "Array.hpp"
#include "arithmetic_op.hpp"
#include "linear_algebra.hpp"
#include "macros.hpp"
#include "Block.hpp"
#include "Matrix.hpp"
#include "Matrix3d.hpp"
#include "io_tools.hpp"
#include "multivariate_polynomial_algos.hpp"
#include "ContinuousPolyBase1d.hpp"
#include "polynomial_algo.hpp"
#include "poly_weight_functors.hpp"



namespace slip
{


template <typename T>
class ContinuousPolyBase3d;

template <typename T>
std::ostream& operator<<(std::ostream & out, 
			 const ContinuousPolyBase3d<T>& a);

  template<typename T>
  class ContinuousPolyBase3d : public std::map<slip::block<std::size_t,3>, slip::MultivariatePolynomial<T,3> >
{
public:
  typedef ContinuousPolyBase3d<T> self;
  typedef const self const_self;
  typedef typename std::map<slip::block<std::size_t,3>,
			    slip::MultivariatePolynomial<T,3> > base;
  typedef typename base::key_type key_type;
  typedef typename base::value_type value_type;


public:
   /**
   ** \name Constructors & Destructors
   */
  /*@{*

    /*!
    ** \brief Constructs a ContinuousPolyBase3d.
    */
  ContinuousPolyBase3d():
    base(),
    range_size_slice_(1),
    range_size_row_(1),
    range_size_col_(1),
    size_(1),
    degree_(0),
    slice_degree_(0),
    row_degree_(0),
    col_degree_(0),
    normalized_(false),
    monic_(false),
    complete_(false),
    method_(slip::POLY_BASE_GENERATION_FILE),
    integration_method_(slip::POLY_INTEGRATION_RECTANGULAR),
    all_1d_bases_(new std::vector<slip::ContinuousPolyBase1d<T,3>* >(3))
  {
  }

    /*!
    ** \brief Constructs a ContinuousPolyBase3d.
    ** \param range_size_slice 
    ** \param range_size_row 
    ** \param range_size_col 
    ** \param degree highest degree of the monomials of the base
    ** \param normalized true if the base is normalized, false else
    ** \param monic true if the base is monic (coefficient of the monomial of highest degree is 1), false else
    ** \param method Method used to generate the base : 
    ** "gram_schmidt": gram-schmidt orthogonalisation
    ** "stieltjes": stieltjes (monic three terms recurrence)
    ** "file": read Legendre polynomial coefficients from file
    */
  ContinuousPolyBase3d(const std::size_t range_size_slice,
		       const std::size_t range_size_row,
		       const std::size_t range_size_col,
		       const std::size_t degree,
		       const bool normalized,
		       const bool monic,
		       const slip::POLY_BASE_GENERATION_METHOD& method,
		       const slip::POLY_INTEGRATION_METHOD& integration_method):
    base(),
    range_size_slice_(range_size_slice),
    range_size_row_(range_size_row),
    range_size_col_(range_size_col),
    size_(slip::base_poly_nb_lt_degree(3,degree)),
    degree_(degree),
    slice_degree_(degree),
    row_degree_(degree),
    col_degree_(degree),
    normalized_(normalized),
    monic_(monic),
    complete_(false),
    method_(method),
    integration_method_(integration_method),
    all_1d_bases_(new std::vector<slip::ContinuousPolyBase1d<T,3>* >(3))
  {
  }

  ContinuousPolyBase3d(const std::size_t range_size_slice,
		       const std::size_t range_size_row,
		       const std::size_t range_size_col,
		       const std::size_t slice_degree,
		       const std::size_t row_degree,
		       const std::size_t col_degree,
		       const bool normalized,
		       const bool monic,
		       const slip::POLY_BASE_GENERATION_METHOD& method,
		       const slip::POLY_INTEGRATION_METHOD& integration_method): 
    base(),
    range_size_slice_(range_size_slice),
    range_size_row_(range_size_row),
    range_size_col_(range_size_col),
    size_((row_degree + 1)* (col_degree +1)*(slice_degree+1)),
    degree_(row_degree+col_degree+slice_degree),
    slice_degree_(slice_degree),
    row_degree_(row_degree),
    col_degree_(col_degree),
    normalized_(normalized),
    monic_(monic),
    complete_(true),
    method_(method),
    integration_method_(integration_method),
    all_1d_bases_(new std::vector<slip::ContinuousPolyBase1d<T,3>* >(3))
  {
  }

   /*!
    ** \brief Copy constructor
    ** \param other %ContinuousPolyLegendreNd.
    */
  ContinuousPolyBase3d(const self& other):
    base(other),
    range_size_slice_(other.range_size_slice_),
    range_size_row_(other.range_size_row_),
    range_size_col_(other.range_size_col_),
    size_(other.size_),
    degree_(other.degree_),
    slice_degree_(other.slice_degree_),
    row_degree_(other.row_degree_),
    col_degree_(other.col_degree_),
    normalized_(other.normalized_),
    monic_(other.monic_),
    complete_(other.complete_),
    method_(other.method_),
    integration_method_(other.integration_method_)
  {
   
    this->all_1d_bases_ = new std::vector<slip::ContinuousPolyBase1d<T,3>* >(*other.all_1d_bases_);
   
  
  }

   /*!
   ** \brief Destructor 
   */
  virtual ~ContinuousPolyBase3d()
  {
    this->desallocate();
  }

  /*!
  ** \brief Copy constructor
  ** \param other %ContinuousPolyLegendreNd.
  */
  self& operator=(const self& other)
  {
     if(this != &other)
       {
	 base::operator=(other);
	 this->copy_attributes(other);
	 this->desallocate();
	 this->all_1d_bases_ = new std::vector<slip::ContinuousPolyBase1d<T,3>* >(*other.all_1d_bases_);
       }
     return *this;
   
  }
  /*@} End Constructors */
  

  const bool is_normalized() const
  {
    return this->normalized_;
  }
  const bool is_monic() const
  {
    return this->monic_;
  }
 
  const std::size_t size() const
  {
    return this->size_;
  }

  const std::size_t degree() const
  {
    return this->degree_;
  }

  const std::size_t slice_degree() const
  {
    return this->slice_degree_;
  }

  const std::size_t row_degree() const
  {
    return this->row_degree_;
  }

  const std::size_t col_degree() const
  {
    return this->col_degree_;
  }

  const std::size_t range_size_slice() const
  {
    return this->range_size_slice_;
  }
  
  const std::size_t range_size_row() const
  {
    return this->range_size_row_;
  }
  
  const std::size_t range_size_col() const
  {
    return this->range_size_col_;
  }
 
  
  const slip::POLY_BASE_GENERATION_METHOD& method() const
  {
    return this->method_;
  }

   const slip::POLY_INTEGRATION_METHOD& integration_method() const
  {
    return this->integration_method_;
  }

/**
   ** \name i/o operators
   */
  /*@{*/
  
  /*!
  ** \brief Write the %ContinuousPolyBase3d to the ouput stream
  ** \param out output std::ostream
  ** \param a %ContinuousPolyBase3d to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const self& a);
  /*@} End i/o operators */

  /*!
  ** \brief Generate the polynomial base.
  */
  void generate()
  {
    //generate analytic and discrete 1d basis
    
    ((*(this->all_1d_bases_))[0])->generate();
    ((*(this->all_1d_bases_))[1])->generate();
    ((*(this->all_1d_bases_))[2])->generate();
    //generate analytic 3d base
    this->tensorial_product();
    
  }

  /*!
  ** \brief Project data onto the P(k,i,j) polynomial of the base.
  ** \param fup RandomAccessIterator3d on the data.
  ** \param bbot RandomAccessIterator3d on the data.
  ** \param k x index of the polynomial.
  ** \param j y index of the polynomial.
  ** \param j z index of the polynomial.
  ** \todo to test
  */
  template <typename RandomAccessIterator3d>
  T project(RandomAccessIterator3d fup,
	    RandomAccessIterator3d bbot,
	    const std::size_t k,
	    const std::size_t i,
	    const std::size_t j)
  {
    const std::size_t slices = static_cast<std::size_t>((bbot-fup)[0]);
    const std::size_t rows   = static_cast<std::size_t>((bbot-fup)[1]);
    slip::Array<T> tmpz(slices);
    slip::Array<T> tmpy(rows);
    
    return this->project(fup,bbot,k,i,j,tmpy,tmpz);
  
  }

  /*!
  ** \brief Project data onto the P(k,i,j) polynomial of the base.
  ** \param up RandomAccessIterator3d on the data.
  ** \param bot RandomAccessIterator3d on the data.
  ** \param degree_max maximal degree of projection.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \pre (coef_last - coef_first) <= this->size()
  ** \pre degree_max <= this->degree()
  ** \todo VERIFY for complete base
  */
  template <class RandomAccessIterator3d,
  	    class RandomAccessIterator>
  void project(RandomAccessIterator3d up,
  	       RandomAccessIterator3d bot,
  	       const std::size_t degree_max,
  	       RandomAccessIterator coef_first,
  	       RandomAccessIterator coef_last)
  {
    assert(static_cast<std::size_t>(coef_last - coef_first) <= this->size_);
    //assert(degree_max <= this->degree_);
    const std::size_t slices = static_cast<std::size_t>((bot-up)[0]);
    slip::Array<T> tmpz(slices);
    const std::size_t rows = static_cast<std::size_t>((bot-up)[1]);
    slip::Array<T> tmpy(rows);
    if(!this->complete_)
      {
  	for(auto it = this->begin(); (it != this->end()); ++it)
  	  {
  	    if(((it->first)[0] + ((it->first)[1]) +  ((it->first)[2])) <= degree_max)
  	      {
  		*coef_first = this->project(up,bot,(it->first)[0],(it->first)[1],((it->first)[2]),tmpy,tmpz);
		//	std::cout<<"(k,i,j) = ("<<(it->first)[0]<<","<<(it->first)[1]<<(it->first)[2]<<")"<<std::endl;
  		++coef_first;
  	      }
  	  }
      }
    else
      {
  	for(auto it = this->begin(); (it != this->end()); ++it)
  	  {
  	      if(  (((it->first)[0]) <= degree_max) 
  		&& (((it->first)[1]) <= degree_max)
  		&& (((it->first)[2]) <= degree_max) )
  	      {
  		*coef_first = this->project(up,bot,(it->first)[0],(it->first)[1],(it->first)[2],tmpy,tmpz);
		//	std::cout<<"(k,i,j) = ("<<(it->first)[0]<<","<<(it->first)[1]<<(it->first)[2]<<")"<<std::endl;
		++coef_first;
  	      }
  	  }
      }
  }

template <typename RandomAccessIterator3d,
  	    typename RandomAccessIterator>
  void project(RandomAccessIterator3d up,
  	       RandomAccessIterator3d bot,
  	       const std::size_t row_degree_max,
	       const std::size_t col_degree_max,
	       const std::size_t slice_degree_max,
  	       RandomAccessIterator coef_first,
  	       RandomAccessIterator coef_last)
  {
    assert(static_cast<std::size_t>(coef_last - coef_first) <= this->size_);
    const std::size_t slices = static_cast<std::size_t>((bot-up)[0]);
    slip::Array<T> tmpz(slices);
    const std::size_t rows = static_cast<std::size_t>((bot-up)[1]);
    slip::Array<T> tmpy(rows);
    
    if(this->complete_)
      {
	for(auto it = this->begin(); (it != this->end()); ++it)
	  {
	    if(    (((it->first)[0]) <= col_degree_max) 
		&& (((it->first)[1]) <= row_degree_max)
		&& (((it->first)[2]) <= slice_degree_max)  )
	      {
		*coef_first = this->project(up,bot,(it->first)[0],(it->first)[1],(it->first)[2],tmpy,tmpz);
		//	std::cout<<"(k,i,j) = ("<<(it->first)[0]<<","<<(it->first)[1]<<(it->first)[2]<<")"<<std::endl;
		++coef_first;
	      }
	  }
      }
    else
      {
       	for(auto it = this->begin(); (it != this->end()); ++it)
	  {
	    if(    (((it->first)[0]) + ((it->first)[1]) + ((it->first)[2])) <= slice_degree_max+row_degree_max+col_degree_max) 
	      {
		*coef_first = this->project(up,bot,(it->first)[0],(it->first)[1],(it->first)[2],tmpy,tmpz);
		//	std::cout<<"(k,i,j) = ("<<(it->first)[0]<<","<<(it->first)[1]<<(it->first)[2]<<")"<<std::endl;
		++coef_first;
	      }
	  }
      }
  }
template <typename RandomAccessIterator3d,
	  typename RandomAccessIterator>
  void project(RandomAccessIterator3d up,
  	       RandomAccessIterator3d bot,
	       RandomAccessIterator coef_first,
  	       RandomAccessIterator coef_last)
  {
    assert(static_cast<std::size_t>(coef_last - coef_first) <= this->size_);
     const std::size_t slices = static_cast<std::size_t>((bot-up)[0]);
    slip::Array<T> tmpz(slices);
    const std::size_t rows = static_cast<std::size_t>((bot-up)[1]);
    slip::Array<T> tmpy(rows);
   

     for(auto it = this->begin(); (it != this->end()); ++it)
       {
	 *coef_first = this->project(up,bot,(it->first)[0],(it->first)[1],(it->first)[2],tmpy,tmpz);
	 //	std::cout<<"(k,i,j) = ("<<(it->first)[0]<<","<<(it->first)[1]<<(it->first)[2]<<")"<<std::endl;
	 ++coef_first;
       }
  }


  /*!
   ** \brief Normalize the polynomial base.
   ** \todo TEST
   */
  void normalize()
  {
     if(!this->normalized_)
       {
	 ((*(this->all_1d_bases_))[0])->normalize();
	 ((*(this->all_1d_bases_))[1])->normalize();
         ((*(this->all_1d_bases_))[2])->normalize();
	 this->normalized_ = true;
       }
   

  }

 
 

    
 template <typename RandomAccessIterator,
  	     typename RandomAccessIterator3d>
  void reconstruct(RandomAccessIterator coef_first,
  		   RandomAccessIterator coef_last,
  		   const std::size_t degree_max,
  		   RandomAccessIterator3d up,
  		   RandomAccessIterator3d bot)
  {
    //assert(K <= static_cast<std::size_t>(coef_last-coef_first)); 
   
    typedef typename std::iterator_traits<RandomAccessIterator3d>::value_type value_type;
    const std::size_t s = (bot-up)[0];
    const std::size_t r = (bot-up)[1];
    const std::size_t c = (bot-up)[2];
    
    slip::Matrix3d<value_type> M(s,r,c);
    std::fill(up,bot,value_type());
   

 if(!this->complete_)
      {
	for(auto it = this->begin(); (it != this->end()); ++it)
	  {
	    if(((it->first)[0] + ((it->first)[1]) + ((it->first)[2])) <= degree_max)
	      {
		
		this->evaluate(it->first[0],it->first[1],it->first[2],M.front_upper_left(),M.back_bottom_right());
		slip::aXpY(*coef_first,M.begin(),M.end(),up);
		//	std::cout<<"(k,i,j) = ("<<(it->first)[0]<<","<<(it->first)[1]<<(it->first)[2]<<")"<<std::endl;
		++coef_first;
	      }
	  }
      }
    else
      {
	for(auto it = this->begin(); (it != this->end()); ++it)
	  {
	    if(    (((it->first)[0]) <= degree_max) 
		&& (((it->first)[1]) <= degree_max)
		&& (((it->first)[2]) <= degree_max))
	      {
		this->evaluate(it->first[0],it->first[1],it->first[2],M.front_upper_left(),M.back_bottom_right());
		slip::aXpY(*coef_first,M.begin(),M.end(),up);
		//	std::cout<<"(k,i,j) = ("<<(it->first)[0]<<","<<(it->first)[1]<<(it->first)[2]<<")"<<std::endl;
		++coef_first;
	      }
	  }
      }

  }
 
template <typename RandomAccessIterator,
  	     typename RandomAccessIterator3d>
  void reconstruct(RandomAccessIterator coef_first,
  		   RandomAccessIterator coef_last,
		   RandomAccessIterator3d up,
  		   RandomAccessIterator3d bot)
  {
    //assert(K <= static_cast<std::size_t>(coef_last-coef_first)); 
   
    typedef typename std::iterator_traits<RandomAccessIterator3d>::value_type value_type;
    const std::size_t s = (bot-up)[0];
    const std::size_t r = (bot-up)[1];
    const std::size_t c = (bot-up)[2];
    
    slip::Matrix3d<value_type> M(s,r,c);
    std::fill(up,bot,value_type());
   

    for(auto it = this->begin(); (it != this->end()); ++it, ++coef_first)
      {
	
	this->evaluate(it->first[0],it->first[1],it->first[2],M.front_upper_left(),M.back_bottom_right());
	slip::aXpY(*coef_first,M.begin(),M.end(),up);
	//	std::cout<<"(k,i,j) = ("<<(it->first)[0]<<","<<(it->first)[1]<<(it->first)[2]<<")"<<std::endl;
      }

    
  }

  //analytical reconstruction
 template <typename RandomAccessIterator1>
  slip::MultivariatePolynomial<T,3> reconstruct(RandomAccessIterator1 coef_first,
						RandomAccessIterator1 coef_last,
						const std::size_t K)
  {
    assert(K <= static_cast<std::size_t>(coef_last-coef_first));
    slip::MultivariatePolynomial<T,3> init;
    auto coef_end = coef_first + K;
    auto poly_it = this->begin();
    for (; coef_first != coef_end; ++coef_first, ++poly_it)
      {
	init +=  (*coef_first * poly_it->second);
      }
    return init;
  }

  //EXPRIMENTAL !!!
 template <typename RandomAccessIterator>
 slip::MultivariatePolynomial<T,3> reconstruct_degmax(RandomAccessIterator coef_first,
  		   RandomAccessIterator coef_last,
  		   const std::size_t degree_max)
  {
    //assert(K <= static_cast<std::size_t>(coef_last-coef_first)); 
   
    // typedef typename std::iterator_traits<RandomAccessIterator3d>::value_type value_type;
    // const std::size_t s = (bot-up)[0];
    // const std::size_t r = (bot-up)[1];
    // const std::size_t c = (bot-up)[2];
    
    // slip::Matrix3d<value_type> M(s,r,c);
    // std::fill(up,bot,value_type());
    slip::MultivariatePolynomial<T,3> init;

 // if(!this->complete_)
 //      {
	//key_type m;
	for(auto it = this->begin(); (it != this->end()); ++it,++coef_first)
	  {
	    if(((it->first)[0] + ((it->first)[1]) + ((it->first)[2])) <= degree_max)
	      {
		
		// m[0] = (it->first)[0];
		// m[1] = (it->first)[1];
		// m[2] = (it->first)[2];
		init +=  (*coef_first) * it->second;
	
		//++coef_first;
	      }
	  }
    //   }
    // else
    //   {
    // 	//key_type m;
    // 	for(auto it = this->begin(); (it != this->end()); ++it,++coef_first)
    // 	  {
    // 	    if(    (((it->first)[0]) <= degree_max) 
    // 		&& (((it->first)[1]) <= degree_max)
    // 		&& (((it->first)[2]) <= degree_max))
    // 	      {
	
	
    // 		init +=  (*coef_first) * it->second;
	
    // 	      }
    // 	  }
    //   }
	return init;
  }
  template <typename RandomAccessIterator3d>
  void evaluate(const std::size_t k,
		const std::size_t i,
  		const std::size_t j,
  		RandomAccessIterator3d fup,
  		RandomAccessIterator3d bbot)
  {
    
    
    this->discrete_tensorial_product(((*(this->all_1d_bases_))[2])->poly_begin(k),
				     ((*(this->all_1d_bases_))[2])->poly_end(k),
				     ((*(this->all_1d_bases_))[1])->poly_rbegin(i),
				     ((*(this->all_1d_bases_))[1])->poly_rend(i),
				     ((*(this->all_1d_bases_))[0])->poly_rbegin(j),
				     ((*(this->all_1d_bases_))[0])->poly_rend(j),
				     fup,
				     bbot);
 
  	
 
  }
  

  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const
  {
    return "ContinuousPolyBase3d";
  }

protected:
  std::size_t range_size_slice_;
  std::size_t range_size_row_;
  std::size_t range_size_col_;
  std::size_t size_;///size of the base : number of polynomials
  std::size_t degree_;              ///degree of the base
  std::size_t slice_degree_; ///degree of the slices
  std::size_t row_degree_; ///degree of the rows
  std::size_t col_degree_; ///degree of the columns
  bool normalized_; ///boolean indicated if the base is normalized
  bool monic_; ///boolean indicated if the base is monic
  bool complete_; ///boolean indicated if the base is complete (degree = row_degree+col_degree) or not 
  slip::POLY_BASE_GENERATION_METHOD method_;
  slip::POLY_INTEGRATION_METHOD integration_method_;
  std::vector<slip::ContinuousPolyBase1d<T,3>* >* all_1d_bases_;
 protected:

  void desallocate()
  {
    if((*(this->all_1d_bases_))[0] != nullptr)
      {
	delete (*(this->all_1d_bases_))[0];
      }
    if((*(this->all_1d_bases_))[1] != nullptr)
      {
	delete (*(this->all_1d_bases_))[1];
      }
     if((*(this->all_1d_bases_))[2] != nullptr)
      {
	delete (*(this->all_1d_bases_))[2];
      }
    if(this->all_1d_bases_ != nullptr)
      {
	delete all_1d_bases_;
      }
  }
 private:
  void copy_attributes(const self& other)
  {
    this->range_size_slice_ = other.range_size_slice_;
    this->range_size_row_ = other.range_size_row_;
    this->range_size_col_ = other.range_size_col_;
    this->size_ = other.size_;
    this->degree_ = other.degree_;
    this->slice_degree_ = other.slice_degree_;
    this->row_degree_ = other.row_degree_;
    this->col_degree_ = other.col_degree_;
   
    this->normalized_ = other.normalized_;
    this->monic_ = other.monic_;
    this->complete_ = other.complete_;
    this->method_ = other.method_;
    this->integration_method_ = other.integration_method_;
  }
  
  void tensorial_product()
  {
    //tensorial products
    slip::Matrix<std::size_t> powers_matrix;
    if(!this->complete_)
      {
	//std::cout<<"not complete"<<std::endl;
	slip::lexicographic_powers_matrix_lt_degree(3,this->degree_,powers_matrix);
      }
    else
      {
	//std::cout<<"complete"<<std::endl;
	
	std::vector<std::size_t> orders(3);
	orders[0] = this->slice_degree_;
	orders[1] = this->row_degree_;
	orders[2] = this->col_degree_;
	
	slip::lexicographic_powers_matrix(3,orders.begin(),orders.end(),powers_matrix);

      }
    //std::cout<<"powers_matrix = \n"<<powers_matrix<<std::endl;
    const std::size_t powers_matrix_rows = powers_matrix.rows();
    for(std::size_t i = 0; i < powers_matrix_rows; ++i)
      {
  	slip::block<std::size_t,3> poly_indices;
  	std::copy(powers_matrix.row_rbegin(i),powers_matrix.row_rend(i),
  		  poly_indices.begin());
  	slip::MultivariatePolynomial<T,3> Pz = (*((*(this->all_1d_bases_))[0]))[poly_indices[2]];
	slip::MultivariatePolynomial<T,3> Px = (*((*(this->all_1d_bases_))[2]))[poly_indices[0]];
	
  	const slip::block<std::size_t,3>& const_poly_indices = poly_indices;
	
	slip::MultivariatePolynomial<T,3> P = Pz* Px * (*((*(this->all_1d_bases_))[1]))[poly_indices[1]];
  	
  	(*this)[const_poly_indices] = P;
      }
  }

template<typename RandomAccessIterator1, 
	 typename RandomAccessIterator2,
	 typename RandomAccessIterator3,
	 typename RandomAccessIterator3d>
 void discrete_tensorial_product(RandomAccessIterator1 basex_first,
				 RandomAccessIterator1 basex_end,
				 RandomAccessIterator2 basey_first,
				 RandomAccessIterator2 basey_end,
				 RandomAccessIterator3 basez_first,
				 RandomAccessIterator3 basez_end,
				 RandomAccessIterator3d vol_front_upper_left,
				 RandomAccessIterator3d vol_back_bottom_right)
  {
     assert((vol_back_bottom_right - vol_front_upper_left)[0] == (basez_end - basez_first));
     assert((vol_back_bottom_right - vol_front_upper_left)[1] == (basey_end - basey_first));
     assert((vol_back_bottom_right - vol_front_upper_left)[2] == (basex_end - basex_first));
     typedef typename std::iterator_traits<RandomAccessIterator1>::difference_type _Distance1;
     typedef typename std::iterator_traits<RandomAccessIterator2>::difference_type _Distance2;
     typedef typename std::iterator_traits<RandomAccessIterator3>::difference_type _Distance3;

     _Distance1 basex_size = (basex_end - basex_first);  
     _Distance2 basey_size = (basey_end - basey_first);  
     _Distance3 basez_size = (basez_end - basez_first);  
     for(_Distance3 k = 0 ; k < basez_size; ++k)
       {
	 for(_Distance2 i = 0 ; i < basey_size; ++i)
	   {
	     for(_Distance1 j = 0 ; j < basex_size; ++j)
	       {
		 *(vol_front_upper_left++) = basey_first[i] * basex_first[j] * basez_first[k];
	       }
	   }
       }
  }

template <typename RandomAccessIterator3d>
  T project(RandomAccessIterator3d up,
	    RandomAccessIterator3d bot,
	    const std::size_t k,
	    const std::size_t i,
	    const std::size_t j,
	    slip::Array<T>& tmpy,
	    slip::Array<T>& tmpz)
  {
    const std::size_t rows   = tmpy.size();
    const std::size_t slices = tmpz.size();
    for(std::size_t s = 0; s < slices; ++s)
      {
	for(std::size_t r = 0; r < rows; ++r)
	  {
	    tmpy[r] = ((*(this->all_1d_bases_))[2])->project(up.row_begin(s,r),up.row_end(s,r),k);
	  }
    
	tmpz[s] = ((*(this->all_1d_bases_))[1])->project(tmpy.rbegin(),tmpy.rend(),i);
      }
      
    return ((*(this->all_1d_bases_))[0])->project(tmpz.begin(),tmpz.end(),j);
    }
};

}//::slip

namespace slip
{

  /** \name input/output operators */
  /* @{ */
  template <typename T>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const ContinuousPolyBase3d<T>& base)
  {
    out<<"Dimension of the polynomials: "<<3<<std::endl;
    out<<"Degree of the base: "<<base.degree()<<std::endl;
    out<<"Size of the base: "<<base.size()<<std::endl;
    if(base.is_normalized())
      {
	out<<"The base is normalized"<<std::endl;
      }
    else
      {
	out<<"The base is not normalized"<<std::endl;
      }
     if(base.is_monic())
      {
	out<<"The base is monic"<<std::endl;
      }
    else
      {
	out<<"The base is not monic"<<std::endl;
      }
     if(base.complete_)
      {
	out<<"The base is complete"<<std::endl;
      }
    else
      {
	out<<"The base is not complete"<<std::endl;
      }

     out<<"generation of the base method: ";
     switch(base.method())
       {
       case slip::POLY_BASE_GENERATION_FILE:
	 out<<" coefficients files reading"<<std::endl;
	 break;
       case slip::POLY_BASE_GENERATION_STIELTJES:
	 out<<" 3 terms Stieltjes recurrence"<<std::endl;
	 break;
       case slip::POLY_BASE_GENERATION_GRAM_SCHMIDT:
	 out<<" Gram-Schmidt canonic base integration"<<std::endl;
	 break;
       default:
	 out<<" unknown base generation algorithm"<<std::endl;
	 
       };
     out<<"Analytic base:"<<std::endl;
     for(auto it = base.begin(); it != base.end(); ++it)
       {
	 out<<"P"<<it->first<<std::endl;
	 out<<it->second<<std::endl;
       }
     
     out<<"slice range size = "<<base.range_size_slice_<<std::endl;
     out<<"row range size   = "<<base.range_size_row_<<std::endl;
     out<<"col range size   = "<<base.range_size_col_<<std::endl;
     out<<"support = "<<((*base.all_1d_bases_)[0])->support()<<" x "<<((*base.all_1d_bases_)[1])->support()<<" x "<<((*base.all_1d_bases_)[2])->support()<<std::endl;
      out<<"integration method: ";
     switch(base.integration_method_)
      {
      case slip::POLY_INTEGRATION_RECTANGULAR:
	out<<" rectangular"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_TRAPEZIUM:
	out<<" trapezium"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_SIMPSON:
	out<<" Simpson"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_3:
	out<<" Newton-Cotes of order 3"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_4:
	out<<" Newton-Cotes of order 4"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_5:
	out<<" Newton-Cotes of order 5"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_6:
	out<<" Newton-Cotes of order 6"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_7:
	out<<" Newton-Cotes of order 7"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_8:
	out<<" Newton-Cotes of order 8"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_9:
	out<<" Newton-Cotes of order 9"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_NEWTON_COTES_10:
	out<<" Newton-Cotes of order 10"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_HOSNY2007:
	out<<" exact integration from HOSNY Pattern Recognition 2007"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_EXACT:
	out<<" formal exact integration"<<std::endl;
	break;
		 case slip::POLY_INTEGRATION_AESR_8:
	out<<" 8 points Alternative Extended Simpson's rule"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_AESR_13:
	out<<" 13 points Alternative Extended Simpson's rule"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_AESR_18:
	out<<" 18 points Alternative Extended Simpson's rule"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_AESR_23:
	out<<" 23 points Alternative Extended Simpson's rule"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_3:
	out<<" 3 oversampling multigrid"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_5:
	out<<" 5 oversampling multigrid"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_7:
	out<<" 7 oversampling multigrid"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_9:
	out<<" 9 oversampling multigrid"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_10:
	out<<" 10 oversampling multigrid"<<std::endl;
	break;
      case slip::POLY_INTEGRATION_MULTIGRID_11:
	out<<" 11 oversampling multigrid"<<std::endl;
	break;
      default:
	out<<" unkown integration method "<<std::endl;
      };
     
     out<<"Slice base: "<<std::endl;
     out<<*((*base.all_1d_bases_)[2])<<std::endl;
  //out<<"column orthogonality precision = "<<((*base.all_1d_bases_)[2])->orthogonality_precision()<<std::endl;

     out<<"row base: "<<std::endl;
     out<<*((*base.all_1d_bases_)[0])<<std::endl;
     //out<<"row orthogonality precision = "<<((*base.all_1d_bases_)[0])->orthogonality_precision()<<std::endl;
     
     out<<"Column base: "<<std::endl;
     out<<*((*base.all_1d_bases_)[1])<<std::endl;
     //out<<"column orthogonality precision = "<<((*base.all_1d_bases_)[1])->orthogonality_precision()<<std::endl;
     
 

    return out;
  }
   /* @} */

}//::slip

#endif //SLIP_CONTINUOUSPOLYBASE3D_HPP
