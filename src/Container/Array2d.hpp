/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */






/** 
 * \file Array2d.hpp
 * 
 * \brief Provides a class to manipulate 2d dynamic and generic arrays.
 * 
 */

#ifndef SLIP_ARRAY2D_HPP
#define SLIP_ARRAY2D_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <string>
#include <cstddef>
#include "Box2d.hpp"
#include "Point2d.hpp"
#include "DPoint2d.hpp"
#include "stride_iterator.hpp"
#include "iterator2d_box.hpp"
#include "Range.hpp"
#include "iterator2d_range.hpp"


#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>

/**
 ** \namespace slip 
 ** \brief This namespace corresponds to the kernel of the Simple
 ** Library on Image Processing (SLIP). That is to say
 ** that it contains the data structures and the algorithms needed by
 ** these data strucutres.
 */
namespace slip
{

template<class T>
class stride_iterator;

template<class T>
class iterator2d_box;

template<class T>
class iterator2d_range;

template<class T>
class const_iterator2d_box;

template<class T>
class const_iterator2d_range;


template <class T>
class DPoint2d;

template <class T>
class Point2d;

template <typename T>
class Array2d;

template <typename T>
std::ostream& operator<<(std::ostream & out, 
			 const slip::Array2d<T>& a);

template<typename T>
bool operator==(const slip::Array2d<T>& x, 
		const slip::Array2d<T>& y);

template<typename T>
bool operator!=(const slip::Array2d<T>& x, 
		const slip::Array2d<T>& y);

template<typename T>
bool operator<(const slip::Array2d<T>& x, 
	       const slip::Array2d<T>& y);

template<typename T>
bool operator>(const slip::Array2d<T>& x, 
	       const slip::Array2d<T>& y);

template<typename T>
bool operator<=(const slip::Array2d<T>& x, 
		const slip::Array2d<T>& y);

template<typename T>
bool operator>=(const slip::Array2d<T>& x, 
		const slip::Array2d<T>& y);


/*!
 *  @defgroup Containers2d 2d Containers
 *  @brief 2d containers having the same interface
 *  @{
 */
/*! \class Array2d
**  \ingroup Containers Containers2d
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.3
** \date 2007/12/15
** \since 1.0.0
** \brief This is a two-dimensional dynamic and generic container.
** This container statisfies the BidirectionnalContainer concepts of the STL.
** It is also an 2d extension of the RandomAccessContainer concept. That is
** to say the bracket element access is replaced by the double bracket element 
** access.
** \param T Type of the element in the %Array2d
** \par Axis conventions:
** \image html iterator2d_conventions.jpg "axis and notation conventions"
** \image latex iterator2d_conventions.eps "axis and notation conventions" width=5cm
  **
*/
template <typename T>
class Array2d
{
public:
  typedef T value_type;
  typedef Array2d<T> self;
  typedef const Array2d<T> const_self;

  typedef value_type& reference;
  typedef value_type const& const_reference;

  typedef value_type* pointer;
  typedef value_type const* const_pointer;
 

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;
  
  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

  typedef pointer row_iterator;
  typedef const_pointer const_row_iterator;

  typedef slip::stride_iterator<pointer> col_iterator;
  typedef slip::stride_iterator<const_pointer> const_col_iterator;
 
  typedef slip::iterator2d_box<self> iterator2d;
  typedef slip::const_iterator2d_box<const_self> const_iterator2d;
  
  typedef slip::stride_iterator<pointer> row_range_iterator;
  typedef slip::stride_iterator<const_pointer> const_row_range_iterator;
  typedef slip::stride_iterator<col_iterator> col_range_iterator;
  typedef slip::stride_iterator<const_col_iterator> const_col_range_iterator;

  typedef slip::iterator2d_range<self> iterator2d_range;
  typedef slip::const_iterator2d_range<const_self> const_iterator2d_range;

  typedef std::reverse_iterator<iterator> reverse_row_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
  typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
  typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;
  typedef std::reverse_iterator<iterator2d> reverse_iterator2d;
  typedef std::reverse_iterator<const_iterator2d> const_reverse_iterator2d;
  typedef std::reverse_iterator<row_range_iterator> reverse_row_range_iterator;
  typedef std::reverse_iterator<const_row_range_iterator> const_reverse_row_range_iterator;
  typedef std::reverse_iterator<col_range_iterator> reverse_col_range_iterator;
  typedef std::reverse_iterator<const_col_range_iterator> const_reverse_col_range_iterator;
  typedef std::reverse_iterator<iterator2d_range> reverse_iterator2d_range;
  typedef std::reverse_iterator<const_iterator2d_range> const_reverse_iterator2d_range;
  
 //default iterator of the container
  typedef iterator2d default_iterator;
  typedef const_iterator2d const_default_iterator;

  static const std::size_t DIM = 2;
 /**
 ** \name Constructors & Destructors
 */
 /*@{*/

  /*!
  ** \brief Constructs a %Array2d.
  */
  Array2d();
 
  /*!
  ** \brief Constructs a %Array2d.
  ** \param d1 first dimension of the %Array2d
  ** \param d2 second dimension of the %Array2d
  ** 
  ** \par The %Array2d is initialized by the default value of T. 
  */
  Array2d(const size_type d1,
	  const size_type d2);
  
  /*!
  ** \brief Constructs a %Array2d initialized by the scalar value \a val.
  ** \param d1 first dimension of the %Array2d
  ** \param d2 second dimension of the %Array2d
  ** \param val initialization value of the elements 
  */
  Array2d(const size_type d1,
	  const size_type d2, 
	  const T& val);
 
  /*!
  ** \brief Constructs a %Array2d initialized by an array \a val.
  ** \param d1 first dimension of the %Array2d
  ** \param d2 second dimension of the %Array2d
  ** \param val initialization array value of the elements 
  */
  Array2d(const size_type d1,
	  const size_type d2, 
	  const T* val);
 
  /**
  **  \brief  Contructs a %Array2d from a range.
  ** \param d1 first dimension of the %Array2d
  ** \param d2 second dimension of the %Array2d
  **  \param  first  An input iterator.
  **  \param  last  An input iterator.
  **
  ** Create a %Array2d consisting of copies of the elements from
  ** [first,last).
  */
  template<typename InputIterator>
  Array2d(const size_type d1,
	  const size_type d2, 
	  InputIterator first,
	  InputIterator last):
    d1_(d1),d2_(d2),size_(d1*d2)
  {
    this->allocate();
    std::fill_n(this->begin(),this->size_,T());
    std::copy(first,last, this->begin());
  }

 /*!
  ** \brief Constructs a copy of the Array2d \a rhs
  */
  Array2d(const self& rhs);

 /*!
  ** \brief Destructor of the Array2d
  */
  ~Array2d();
  

  /*@} End Constructors */


  /*!
  ** \brief Resizes a %Array2d.
  ** \param d1 new first dimension
  ** \param d2 new second dimension
  ** \param val new value for all the elements
  */ 
  void resize(const size_type d1,
	      const size_type d2,
	      const T& val = T());

  /**
   ** \name iterators
   */
  /*@{*/
 
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %Array2d.  Iteration is done in ordinary
  **  element order.
  ** \return begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  **  slip::Array2d<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  iterator begin();


  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %Array2d.  Iteration is done in ordinary
  **  element order.
  ** \return end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  **  slip::Array2d<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  iterator end();

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %Array2d.  Iteration is done in ordinary
  **  element order.
  ** 
  ** \return const begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> const A1(10,9);
  **  slip::Array2d<double> const A2(10,9);
  **  slip::Array2d<double> S(10,9);
  ** //copy the sum of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_iterator begin() const;

  
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %Array2d.  Iteration is done in
  **  ordinary element order.
  ** \return const end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> const A1(10,9);
  **  slip::Array2d<double> const A2(10,9);
  **  slip::Array2d<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_iterator end() const;
  
  /*!
  **  \brief Returns a read/write reverse iterator that points to the
  **  last element in the %Array2d.  Iteration is done in reverse
  **  element order.
  ** \return reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  **  slip::Array2d<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 
  ** //in S (iterated in the reverse order element)
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.rbegin(),
  **                std::plus<double>());
  ** \endcode
  */
  reverse_iterator rbegin();
  
  /*!
  **  \brief Returns a read/write reverse iterator that points to one
  **  before the first element in the %Array2d.  Iteration is done
  **  in reverse element order.
  **  \return reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  **  slip::Array2d<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 
  ** //in S (iterated in the reverse order element)
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.rbegin(),
  **                std::plus<double>());
  ** \endcode
  */
  reverse_iterator rend();
  
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to the last element in the %Array2d.  Iteration is done in
  **  reverse element order.
  ** \return const reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  **  slip::Array2d<double> S(10,9);
  ** //copy the sum of the elements of A1 (iterated in the reverse order element)
  ** //and A2 in S 
  ** std::transform(A1.rbegin(),A1.rend(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_reverse_iterator rbegin() const;

  
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to one before the first element in the %Array2d.  Iteration
  **  is done in reverse element order.
  **  \return const reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  **  slip::Array2d<double> S(10,9);
  ** //copy the sum of the elements of A1 (iterated in the reverse order element)
  ** //and A2 in S 
  ** std::transform(A1.rbegin(),A1.rend(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_reverse_iterator rend() const;

 
 /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the row \a row in the %Array2d.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  row_iterator row_begin(const size_type row);

  
  /*!
  **  \brief Returns a read/write iterator that points one past
  **  the end element of the row \a row in the %Array2d.  
  **  Iteration is done in ordinary element order.
  ** \param row  The index of the row to iterate.
  ** \return begin row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  row_iterator row_end(const size_type row);


  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the row \a row in the %Array2d.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  const_row_iterator row_begin(const size_type row) const;


  /*!
  **  \brief Returns a read-only iterator that points one past
  **  the end element of the row \a row in the %Array2d.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  const_row_iterator row_end(const size_type row) const;
  
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the column \a column in the %Array2d.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  col_iterator col_begin(const size_type col);


  /*!
  **  \brief Returns a read/write iterator that points one past
  **  the end element of the column \a column in the %Array2d.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  col_iterator col_end(const size_type col);

  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the column \a column in the %Array2d.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin const_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  const_col_iterator col_begin(const size_type col) const;



  /*!
  **  \brief Returns a read-only iterator that points one past
  **  the end element of the column \a column in the %Array2d.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column 
  ** \return begin const_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(10,9);
  **  slip::Array2d<double> A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  const_col_iterator col_end(const size_type col) const;


  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the %Range \a range of the row \a row in the %Array2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return begin row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  row_range_iterator row_begin(const size_type row,
			       const slip::Range<int>& range);



  /*!
  **  \brief Returns a read/write iterator that points one past the end
  **  element of the %Range \a range of the row \a row in the %Array2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return end row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  row_range_iterator row_end(const size_type row,
			     const slip::Range<int>& range);


  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the row \a row in the %Array2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return begin const_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  const_row_range_iterator row_begin(const size_type row,
				     const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read_only iterator that points one past the last
  **  element of the %Range range of the row \a row in the %Array2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row Row to iterate.
  ** \param range %Range of the row to iterate
  ** \return begin const_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \pre row must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  const_row_range_iterator row_end(const size_type row,
				   const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read-write iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %Array2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate
  ** \return begin col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  col_range_iterator col_begin(const size_type col,
			       const slip::Range<int>& range);

  /*!
  **  \brief Returns a read-write iterator that points to the past
  **  the end element of the %Range \a range of the col \a col in the 
  **  %Array2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  col_range_iterator col_end(const size_type col,
			     const slip::Range<int>& range);




  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %Array2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin const_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  const_col_range_iterator col_begin(const size_type col,
				     const slip::Range<int>& range) const;

 
  /*!
  **  \brief Returns a read-only iterator that points to the past
  **  the end element of the %Range \a range of the col \a col in 
  **  the %Array2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate
  ** \return begin const_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  const_col_range_iterator col_end(const size_type col,
				   const slip::Range<int>& range) const;

  

  /*!
  **  \brief Returns a read/write reverse iterator that points to the 
  **  last element of the row \a row in the %Array2d.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return begin reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Array2d.
  */
  reverse_row_iterator row_rbegin(const size_type row);


  /*!
  **  \brief Returns a read/write reverse iterator that points one past
  **  the first element of the row \a row in the %Array2d.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return end reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Array2d.
  */
  reverse_row_iterator row_rend(const size_type row);

  /*!
  **  \brief Returns a read-only reverse iterator that points to the last
  **  element of the row \a row in the %Array2d.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %Array2d.
  */
  const_reverse_row_iterator row_rbegin(const size_type row) const;

  /*!
  **  \brief Returns a read-only reverse iterator that points one past
  **  the first element of the row \a row in the %Array2d. 
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return end const_reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Array2d.
  */
  const_reverse_row_iterator row_rend(const size_type row) const;

  /*!
  **  \brief Returns a read/write reverse iterator that points to the last
  **  element of the column \a column in the %Array2d.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return begin col_reverse_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  */
  reverse_col_iterator col_rbegin(const size_type col);

  
  /*!
  **  \brief Returns a read/write reverse iterator that points one past
  **  the first element of the column \a column in the %Array2d.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return end reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  */
  reverse_col_iterator col_rend(const size_type col);

  /*!
  **  \brief Returns a read-only reverse iterator that points to the last
  **  element of the column \a column in the %Array2d.  
  **  Iteration is done modulo the number of columns and in the reverse 
  ** element order.
  ** \param col The index of the column to iterate.
  ** \return begin const_reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  */
  const_reverse_col_iterator col_rbegin(const size_type col) const;

    
  /*!
  **  \brief Returns a read-only reverse iterator that points one past
  **  the first element of the column \a column in the %Array2d.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return end const_reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  */
  const_reverse_col_iterator col_rend(const size_type col) const;
 

  /*!
  **  \brief Returns a read-write iterator that points to the last
  **  element of the %Range \a range of the row \a row in the %Array2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  */
  reverse_row_range_iterator row_rbegin(const size_type row,
					const slip::Range<int>& range);

 
  /*!
  **  \brief Returns a read-write iterator that points one before
  **  the first element of the %Range \a range of the row \a row in the 
  **  %Array2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  */
  reverse_row_range_iterator row_rend(const size_type row,
				      const slip::Range<int>& range);



  /*!
  **  \brief Returns a read-only iterator that points to the last
  **  element of the %Range \a range of the row \a row in the %Array2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate
  ** \return begin const_reverse_row_range_iterator value
  **
  ** \pre row must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  */
  const_reverse_row_range_iterator row_rbegin(const size_type row,
					      const slip::Range<int>& range) const;


   /*!
  **  \brief Returns a read-only iterator that points one before the first
  **  element of the %Range \a range of the row \a row in the %Array2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate
  ** \return const_reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  */
  const_reverse_row_range_iterator row_rend(const size_type row,
					    const slip::Range<int>& range) const;
 
 


  /*!
  **  \brief Returns a read-write iterator that points to the last
  **  element of the %Range \a range of the col \a col in the %Array2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  */
  reverse_col_range_iterator col_rbegin(const size_type col,
					const slip::Range<int>& range);

  /*!
  **  \brief Returns a read-write iterator that points to one before 
  **  the first element of the %Range range of the col \a col in the 
  **  %Array2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  */
  reverse_col_range_iterator col_rend(const size_type col,
				      const slip::Range<int>& range);


  /*!
  **  \brief Returns a read_only iterator that points to the last
  **  element of the %Range \& range of the col \a col in the %Array2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin const_reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  */
  const_reverse_col_range_iterator 
  col_rbegin(const size_type col,
	     const slip::Range<int>& range) const;

 
  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %Array2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return const_reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Array2d.
  ** \pre The range must be inside the whole range of the %Array2d.
  */
  const_reverse_col_range_iterator col_rend(const size_type col,
					    const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read/write iterator2d that points to the first
  **  element of the %Array2d. It points to the upper left element of
  **  the %Array2d.
  **  
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  iterator2d upper_left();
  
  
  /*!
  **  \brief Returns a read/write iterator2d that points to the past
  **  the end element of the %Array2d. It points to past the end element of
  **  the bottom right element of the %Array2d.
  **  
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  iterator2d bottom_right();


  /*!
  **  \brief Returns a read-only iterator2d that points to the first
  **  element of the %Array2d. It points to the upper left element of
  **  the %Array2d.
  **  
  ** \return begin const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  const_iterator2d upper_left() const;
  
 
  /*!
  **  \brief Returns a read-only iterator2d that points to the past
  **  the end element of the %Array2d. It points to past the end element of
  **  the bottom right element of the %Array2d.
  **  
  ** \return begin const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  const_iterator2d bottom_right() const;

  /*!
  **  \brief Returns a read/write iterator2d that points to the first
  **  element of the %Array2d. It points to the upper left element of
  **  the \a %Box2d associated to the %Array2d.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %Array2d.
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **  
  ** \return end iterator2d value
  ** \pre The box indices must be inside the range of the %Array2d ones.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
 */
  iterator2d upper_left(const Box2d<int>& box);

 
  /*!
  **  \brief Returns a read/write iterator2d that points to the past
  **  the end element of the %Array2d. It points to past the end element of
  **  the bottom right element of the \a %Box2d associated to the %Array2d.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %Array2d.
  **
  ** \return end iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \pre The box indices must be inside the range of the %Array2d ones.
  **  
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  iterator2d bottom_right(const Box2d<int>& box);


  /*!
  **  \brief Returns a read only iterator2d that points to the first
  **  element of the %Array2d. It points to the upper left element of
  **  the \a %Box2d associated to the %Array2d.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %Array2d.
  ** \return end const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **
  ** \pre The box indices must be inside the range of the %Array2d ones.
  ** 
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  const_iterator2d upper_left(const Box2d<int>& box) const;

 
  /*!
  **  \brief Returns a read only iterator2d that points to the past
  **  the end element of the %Array2d. It points to past the end element of
  **  the bottom right element of the \a %Box2d associated to the %Array2d.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %Array2d.
  ** \return end const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **
  ** \pre The box indices must be inside the range of the %Array2d ones.
  ** 
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  const_iterator2d bottom_right(const Box2d<int>& box) const;


  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **  upper left element of the ranges \a row_range and \a col_range 
  **  associated to the %Array2d.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range upper_left(const Range<int>& row_range,
			      const Range<int>& col_range);

  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **   past the end bottom right element of the ranges \a row_range 
  **   and \a col_range associated to the %Array2d.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range bottom_right(const Range<int>& row_range,
				const Range<int>& col_range);


  /*!
  **  \brief Returns a read-only iterator2d_range that points to the
  **   to the upper left element of the ranges \a row_range and \a col_range 
  **   associated to the %Array2d.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid.
  ** \pre The ranges indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range upper_left(const Range<int>& row_range,
				    const Range<int>& col_range) const;


 /*!
  **  \brief Returns a read-only iterator2d_range that points to the past
  **  the end bottom right element of the ranges \a row_range and \a col_range 
  **  associated to the %Array2d.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range bottom_right(const Range<int>& row_range,
				      const Range<int>& col_range) const;


 


  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **  upper left element of the %Range \a range associated to the %Array2d.
  **  The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range and must be valid. 
  ** \pre The range indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range upper_left(const Range<int>& range);
 

  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **   past the end bottom right element of the %Range \a range 
  **   associated to the %Array2d.
  **   The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range bottom_right(const Range<int>& range);

 

  
  /*!
  **  \brief Returns a read-only iterator2d_range that points to the
  **   to the upper left element of the %Range \a range
  **   associated to the %Array2d.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range must be valid.
  ** \pre The range indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range upper_left(const Range<int>& range) const;

 

  /*!
  **  \brief Returns a read-only const_iterator2d_range that points to the 
  **   past the end bottom right element of the %Range \a range 
  **   associated to the %Array2d.
  **   The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Array2d<double> A1(8,8);
  **  slip::Array2d<double> A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range bottom_right(const Range<int>& range) const;

 
  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to the 
  **   bottom right element of the %Array2d. 
  *    Iteration is done within the %Array2d in the reverse order.
  **  
  ** \return reverse_iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rupper_left();
  
  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to past the 
  **  upper left element of the %Array2d.
  **  Iteration is done in the reverse order.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rbottom_right();

  /*!
  **  \brief Returns a read only reverse iterator2d that points.  It points 
  **  to the bottom right element of the %Array2d. 
  **  Iteration is done within the %Array2d in the reverse order.
  **  
  ** \return const_reverse_iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rupper_left() const;
 
 
  /*!
  **  \brief Returns a read only reverse iterator2d. It points to past the 
  **  upper left element of the %Array2d.
  **  Iteration is done in the reverse order.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rbottom_right() const;
  

  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to the 
  **  bottom right element of the \a %Box2d associated to the %Array2d.
  **  Iteration is done in the reverse order.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %Array2d.
  **
  ** \pre The box indices must be inside the range of the %Array2d ones.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rupper_left(const Box2d<int>& box);

  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to one
  **  before the upper left element of the %Box2d \a box associated to 
  **  the %Array2d.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %Array2d.
  **
  ** \pre The box indices must be inside the range of the %Array2d ones.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rbottom_right(const Box2d<int>& box);

  /*!
  **  \brief Returns a read only reverse iterator2d. It points to the 
  **  bottom right element of the %Box2d \a box associated to the %Array2d.
  **  Iteration is done in the reverse order.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %Array2d.
  **
  ** \pre The box indices must be inside the range of the %Array2d ones.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rupper_left(const Box2d<int>& box) const;


  /*!
  **  \brief Returns a read-only reverse iterator2d. It points to one 
  **  before the element of the bottom right element of the %Box2d 
  **  \a box associated to the %Array2d.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %Array2d.
  **
  ** \pre The box indices must be inside the range of the %Array2d ones.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rbottom_right(const Box2d<int>& box) const;

 
  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to the 
  **  past the bottom right element of the ranges \a row_range and 
  **  \a col_range associated to the %Array2d. Iteration is done in the 
  **  reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rupper_left(const Range<int>& row_range,
				       const Range<int>& col_range);


 
  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points
  **  to one before the upper left element of the ranges \a row_range 
  **  and \a col_range associated to the %Array2d. Iteration is done
  **  in the reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rbottom_right(const Range<int>& row_range,
					 const Range<int>& col_range);


 
  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points 
  **   to the past the bottom right element of the ranges \a row_range and 
  **   \a col_range associated to the %Array2d. Iteration is done in the 
  **   reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid.
  ** \pre The ranges indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rupper_left(const Range<int>& row_range,
					     const Range<int>& col_range) const;

  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points 
  **  to one before the upper left element of the ranges \a row_range 
  **  and \a col_range associated to the %Array2d.Iteration is done in 
  **  the reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rbottom_right(const Range<int>& row_range,
					       const Range<int>& col_range) const;


 
 /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to the 
  **  bottom right element of the %Range \a range associated to the %Array2d.
  **  The same range is applied for rows and cols. Iteration is done in the
  **  reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range and must be valid. 
  ** \pre The range indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rupper_left(const Range<int>& range);



  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to  
  **   one before the upper left element of the %Range \a range 
  **   associated to the %Array2d.
  **   The same range is applied for rows and cols. Iteration is done
  **   in the reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rbottom_right(const Range<int>& range);
  

  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points to the
  **   to the bottom right element of the %Range \a range
  **   associated to the %Array2d.
  **  The same range is applied for rows and cols. Iteration is done in the
  **  reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range must be valid.
  ** \pre The range indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rupper_left(const Range<int>& range) const;


  /*!
  **  \brief Returns a read_only reverse_iterator2d_range that points 
  **   to one before the upper left element of the %Range \a range 
  **   associated to the %Array2d.
  **   The same range is applied for rows and cols. Iteration is done in the
  **   reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %Array2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rbottom_right(const Range<int>& range) const;
  

  /*@} End iterators */


  /**
   ** \name i/o operators
   */
  /*@{*/
 /*!
  ** \brief Write the %Array2d to the ouput stream
  ** \param out output std::ostream
  ** \param a %Array2d to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const self& a);

  /*@} End i/o operators */

  /**
   ** \name  Assignment operators and methods
   */
  /*@{*/
  /*!
  ** \brief Assign a %Array2d.
  **
  ** Assign elements of %Array2d in \a rhs
  **
  ** \param rhs %Array2d to get the values from.
  ** \return 
  */
  self& operator=(const self & rhs);


  /*!
  ** \brief Assign all the elments of the %Array2d by value
  **
  **
  ** \param value A reference-to-const of arbitrary type.
  ** \return 
  */
  self& operator=(const T& value)
  {
    this->fill(value);
    return *this;
  }


   /*!
   ** \brief Fills the container range [begin(),begin()+size()) 
   **        with copies of value
   ** \param value  A reference-to-const of arbitrary type.
  */
  void fill(const T& value)
  {
    std::fill_n(this->begin(),this->size_,value);
  } 
  
 /*!
  ** \brief Fills the container range [begin(),begin()+size())
  **        with a copy of
  **        the value array
  ** \param value  A pointer of arbitrary type.
  */
  void fill(const T* value)
  {
    std::copy(value,value + this->size_, this->begin());
  } 

  /*!
  ** \brief Fills the container range [begin(),begin()+size()) 
  **        with a copy of the range [first,last)
  **  \param  first  An input iterator.
  **  \param  last   An input iterator.
  **   
  **
  */
  template<typename InputIterator>
  void fill(InputIterator first,
	    InputIterator last)
  {
    std::copy(first,last, this->begin());
  }
  /*@} End Assignment operators and methods*/

  /**
   ** \name Comparison operators
   */
  /*@{*/
  /*!
  ** \brief %Array2d equality comparison
  ** \param x A %Array2d
  ** \param y A %Array2d of the same type of \a x
  ** \return true iff the size and the elements of the Arrays are equal
  */
  friend bool operator== <>(const Array2d<T>& x, 
			    const Array2d<T>& y);

 /*!
  ** \brief %Array2d inequality comparison
  ** \param x A %Array2d
  ** \param y A %Array2d of the same type of \a x
  ** \return true if !(x == y) 
  */
  friend bool operator!= <>(const Array2d<T>& x, 
			    const Array2d<T>& y);

 /*!
  ** \brief Less than comparison operator (%Array2d ordering relation)
  ** \param x A %Array2d
  ** \param y A %Array2d of the same type of \a x
  ** \return true iff \a x is lexicographically less than \a y
  */
  friend bool operator< <>(const Array2d<T>& x, 
			   const Array2d<T>& y);

 /*!
  ** \brief More than comparison operator
  ** \param x A %Array2d
  ** \param y A %Array2d of the same type of \a x
  ** \return true iff y > x 
  */
  friend bool operator> <>(const Array2d<T>& x, 
			   const Array2d<T>& y);

  /*!
  ** \brief Less than equal comparison operator
  ** \param x A %Array2d
  ** \param y A %Array2d of the same type of \a x
  ** \return true iff !(y > x) 
  */
  friend bool operator<= <>(const Array2d<T>& x, 
			    const Array2d<T>& y);

 /*!
  ** \brief More than equal comparison operator
  ** \param x A %Array2d
  ** \param y A %Array2d of the same type of \a x
  ** \return true iff !(x < y) 
  */
  friend bool operator>= <>(const Array2d<T>& x, 
			    const Array2d<T>& y);


  /*@} Comparison operators */
  
  
  /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Subscript access to the row datas contained in the %Array2d.
  ** \param i The index of the row for which data should be accessed.
  ** \return Read/write pointer to the row data.
  ** \pre i < rows()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  pointer operator[](const size_type i);

  /*!
  ** \brief Subscript access to the row datas contained in the %Array2d.
  ** \param i The index of the row for which data should be accessed.
  ** \return Read-only (constant) pointer to the row data.
  ** \pre i < rows()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_pointer operator[](const size_type i) const;


  /*!
  ** \brief Subscript access to the data contained in the %Array2d.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read/write reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const size_type i,
		       const size_type j);
 
  /*!
  ** \brief Subscript access to the data contained in the %Array2d.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read_only (constant) reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const size_type i,
			     const size_type j) const;

  /*!
  ** \brief Subscript access to the data contained in the %Array2d.
  ** \param point2d A %Point2d which indicate the subscripts of the data
  **        to access.
  ** \return Read/write reference to data.
  ** \pre point2d must be defined in the range of the %Array2d.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const Point2d<size_type>& point2d);
 
  /*!
  ** \brief Subscript access to the data contained in the %Array2d.
  ** \param point2d A %Point2d which indicate the subscripts of the data
  **        to access.
  ** \return Read_only (constant) reference to data.
  ** \pre point2d must be defined in the range of the %Array2d.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const Point2d<size_type>& point2d) const;


  /*!
  ** \brief Subscript access to the data contained in the %Array2d.
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  ** \return a copy of the range.
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Array2d ones.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  self operator()(const Range<int>& row_range,
		  const Range<int>& col_range);
 

  /*@} End Element access operators */

  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;


   /*!
   ** \brief Returns the number of rows (first dimension size) 
   **        in the %Array2d
   */
  size_type dim1() const;

  /*!
   ** \brief Returns the number of rows (first dimension size) 
   **        in the %Array2d
   */
  size_type rows() const;
  
  /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %Array2d
  */
  size_type dim2() const;

   /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %Array2d
  */
  size_type columns() const;

   /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %Array2d
  */
  size_type cols() const;

  /*!
  ** \brief Returns the number of elements in the %Array2d
  */
  size_type size() const;

  /*!
  ** \brief Returns the maximal size (number of elements) in the %Array2d
  */
  size_type max_size() const;

  /*!
  ** \brief Returns true if the %Array2d is empty.  (Thus size() == 0)
  */
  bool empty()const;

  /*!
  ** \brief Swaps data with another %Array.
  ** \param M A %Array of the same element type
  **
  ** \pre dim1() == M.dim1()
  ** \pre dim2() == M.dim2()
  */
  void swap(self& M);
 
private:
  size_type d1_;
  size_type d2_;
  size_type size_;
  T** data_;
  
  /// Allocates the  memory to store the elements of the Array2d
  void allocate();
  /// Desallocates the memory
  void desallocate();
  /*!
  ** \brief Copy the attributes of the Array2d \a rhs to the Array2d
  ** \param rhs the Array2d from which the attributes are copied
  */
  void copy_attributes(const self& rhs);

 
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >=0)
	{
	  ar & this->d1_ & this->d2_ & this->size_ ;
	  for(std::size_t i = 0; i < this->size_; ++i)
	    {
	      ar & data_[0][i];
	    }
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >=0)
	{
	  ar & this->d1_ & this->d2_ & this->size_ ;
	  this->desallocate();
	  this->allocate();
	  for(std::size_t i = 0; i < this->size_; ++i)
	    {
	      ar & data_[0][i];
	    }
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

/** @} */ // end of Containers2d group

  ///double alias
  typedef slip::Array2d<double> Array2d_d;
  ///float alias
  typedef slip::Array2d<float> Array2d_f;
  ///long alias
  typedef slip::Array2d<long> Array2d_l;
  ///unsigned long alias
  typedef slip::Array2d<unsigned long> Array2d_ul;
  ///short alias
  typedef slip::Array2d<short> Array2d_s;
  ///unsigned long alias
  typedef slip::Array2d<unsigned short> Array2d_us;
  ///int alias
  typedef slip::Array2d<int> Array2d_i;
  ///unsigned int alias
  typedef slip::Array2d<unsigned int> Array2d_ui;
  ///char alias
  typedef slip::Array2d<char> Array2d_c;
  ///unsigned char alias
  typedef slip::Array2d<unsigned char> Array2d_uc;
  

}//slip::

namespace slip
{
  template<typename T>
  inline
  Array2d<T>::Array2d():
    d1_(0),d2_(0),size_(0),data_(0)
  {}

  template<typename T>
  inline
  Array2d<T>::Array2d(const typename Array2d<T>::size_type d1,
		      const typename Array2d<T>::size_type d2):
    d1_(d1),d2_(d2),size_(d1*d2)
  {
    this->allocate();
    std::fill_n(this->data_[0],this->size_,T());
  }
  
  template<typename T>
  inline
  Array2d<T>::Array2d(const typename Array2d<T>::size_type d1,
		    const typename Array2d<T>::size_type d2, 
		    const T& val):
    d1_(d1),d2_(d2),size_(d1*d2)
  {
    this->allocate();
    std::fill_n(this->data_[0],this->size_,val);
  }

  template<typename T>
  inline
  Array2d<T>::Array2d(const typename Array2d<T>::size_type d1,
		    const typename Array2d<T>::size_type d2, 
		    const T* val):
    d1_(d1),d2_(d2),size_(d1*d2)
  {
    this->allocate();
    std::copy(val,val + this->size_, this->data_[0]); 
  }
  
  template<typename T>
  inline
  Array2d<T>::Array2d(const Array2d<T>& rhs):
    d1_(rhs.d1_),d2_(rhs.d2_),size_(rhs.size_)
  {
    this->allocate();
    if(this->size_ != 0)
      {
	std::copy(rhs.data_[0],rhs.data_[0] + this->size_,this->data_[0]);
      }
  }
  
  template<typename T>
  inline
  Array2d<T>& Array2d<T>::operator=(const Array2d<T> & rhs)
  {
    if(this != &rhs)
      {
	if( this->d1_ != rhs.d1_ || this->d2_ != rhs.d2_)
	  {
	    this->desallocate();
	    this->copy_attributes(rhs);
	    this->allocate();
	  }
	if(rhs.size_ != 0)
	  {
	    std::copy(rhs.data_[0],rhs.data_[0] + this->size_,this->data_[0]);
	  }
      }
    return *this;
  }
  
  template<typename T>
  inline
  void Array2d<T>::resize(const typename Array2d<T>::size_type d1,
			  const typename Array2d<T>::size_type d2,
			  const T& val)
  {
    if( this->d1_ != d1 || this->d2_ != d2 )
      {
	this->desallocate();
	this->d1_ = d1;
	this->d2_ = d2;
	this->size_ = d1 * d2;
	this->allocate();
      }
    if((this->d1_ != 0) && (this->d2_ != 0))
      {
	std::fill_n(this->data_[0],this->size_,val);
      }
    
  }


  template<typename T>
  inline
  typename Array2d<T>::iterator Array2d<T>::begin()
  {
    return this->data_[0];
  }

  template<typename T>
  inline
  typename Array2d<T>::iterator Array2d<T>::end()
  {
    return this->data_[0] + this->size_;
  }
  
  template<typename T>
  inline
  typename Array2d<T>::const_iterator Array2d<T>::begin() const
  {
    return this->data_[0];
  }

  template<typename T>
  inline
  typename Array2d<T>::const_iterator Array2d<T>::end() const
  {
    return this->data_[0] + this->size_;
  }
  

  template<typename T>
  inline
  typename Array2d<T>::reverse_iterator Array2d<T>::rbegin()
  {
    return typename Array2d<T>::reverse_iterator(this->end());
  }

  template<typename T>
  inline
  typename Array2d<T>::const_reverse_iterator Array2d<T>::rbegin() const
  {
    return typename Array2d<T>::const_reverse_iterator(this->end());
  }

  template<typename T>
  inline
  typename Array2d<T>::reverse_iterator Array2d<T>::rend()
  {
    return typename Array2d<T>::reverse_iterator(this->begin());
  }

  
  template<typename T>
  inline
  typename Array2d<T>::const_reverse_iterator Array2d<T>::rend() const
  {
    return  typename Array2d<T>::const_reverse_iterator(this->begin());
  }

  template<typename T>
  inline
  typename Array2d<T>::row_iterator 
  Array2d<T>::row_begin(const typename Array2d<T>::size_type row)
  {
    assert(row < this->d1_);
    return this->data_[row];
  }
 
  template<typename T>
  inline
  typename Array2d<T>::const_row_iterator 
  Array2d<T>::row_begin(const typename Array2d<T>::size_type row) const
  {
    assert(row < this->d1_);
    return this->data_[row];
  }

  template<typename T>
  inline
  typename Array2d<T>::row_range_iterator 
  Array2d<T>::row_begin(const typename Array2d<T>::size_type row,
			const slip::Range<int>& range)
  {
    assert(row < this->d1_);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < (int)this->d2_);
    assert(range.stop()  < (int)this->d2_);
    return slip::stride_iterator<typename Array2d<T>::row_iterator>(this->row_begin(row) + range.start(),range.stride());
  }

  template<typename T>
  inline
  typename Array2d<T>::const_row_range_iterator 
  Array2d<T>::row_begin(const typename Array2d<T>::size_type row,
			const slip::Range<int>& range) const
  {
    assert(row < this->d1_);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < (int)this->d2_);
    assert(range.stop() < (int)this->d2_);
    return slip::stride_iterator<typename Array2d<T>::const_row_iterator>(this->row_begin(row) + range.start(),range.stride());
  }
  
  template<typename T>
  inline
  typename Array2d<T>::col_range_iterator 
  Array2d<T>::col_begin(const typename Array2d<T>::size_type col,
			const slip::Range<int>& range)
  {
    assert(col < this->d2_);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < (int)this->d1_);
    assert(range.stop()  < (int)this->d1_);
    return slip::stride_iterator<typename Array2d<T>::col_iterator>(this->col_begin(col) + range.start(),range.stride());
  }

  template<typename T>
  inline
  typename Array2d<T>::const_col_range_iterator 
  Array2d<T>::col_begin(const typename Array2d<T>::size_type col,
			const slip::Range<int>& range) const
  {
    assert(col < this->d2_);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < (int)this->d1_);
    assert(range.stop()  < (int)this->d1_);
    return slip::stride_iterator<typename Array2d<T>::const_col_iterator>(this->col_begin(col) + range.start(),range.stride());
  }

 template<typename T>
 inline
 typename Array2d<T>::row_iterator 
 Array2d<T>::row_end(const typename Array2d<T>::size_type row)
 {
    assert(row < this->d1_);
    return this->data_[row] + this->d2_;
  }

 template<typename T>
 inline
 typename Array2d<T>::const_row_iterator 
 Array2d<T>::row_end(const typename Array2d<T>::size_type row) const
 {
    assert(row < this->d1_);
    return this->data_[row] + this->d2_;
  }
  
  template<typename T>
  inline
  typename Array2d<T>::row_range_iterator 
  Array2d<T>::row_end(const typename Array2d<T>::size_type row,
		      const slip::Range<int>& range)
  {
    assert(row < this->d1_);
    assert(range.start() < (int)this->d2_);
    assert(range.stop()  < (int)this->d2_);
    //  return ++(slip::stride_iterator<typename Array2d<T>::row_iterator>(this->data_[row] + range.stop(),range.stride()));
    return ++(this->row_begin(row,range) + range.iterations());
  }

  template<typename T>
 inline
 typename Array2d<T>::const_row_range_iterator 
 Array2d<T>::row_end(const typename Array2d<T>::size_type row,
		     const slip::Range<int>& range) const
 {
    assert(row < this->d1_);
    assert(range.start() < (int)this->d2_);
    assert(range.stop()  < (int)this->d2_);
    // return ++(slip::stride_iterator<typename Array2d<T>::const_row_iterator>(this->data_[row] + range.stop(),range.stride()));
    return ++(this->row_begin(row,range) + range.iterations());
  }

  template<typename T>
  inline
  typename Array2d<T>::col_range_iterator 
  Array2d<T>::col_end(const typename Array2d<T>::size_type col,
		      const slip::Range<int>& range)
  {
    assert(col < this->d2_);
    assert(range.start() < (int)this->d1_);
    assert(range.stop()  < (int)this->d1_);
    //  return ++(slip::stride_iterator<typename Array2d<T>::col_iterator>(this->col_begin(col) + range.stop(),range.stride()));
    return ++(this->col_begin(col,range) + range.iterations());
  }

  template<typename T>
  inline
  typename Array2d<T>::const_col_range_iterator 
  Array2d<T>::col_end(const typename Array2d<T>::size_type col,
		      const slip::Range<int>& range) const
  {
    assert(col < this->d2_);
    assert(range.start() < (int)this->d1_);
    assert(range.stop()  < (int)this->d1_);
    //  return ++(slip::stride_iterator<typename Array2d<T>::const_col_iterator>(this->col_begin(col) + range.stop(),range.stride()));
    return ++(this->col_begin(col,range) + range.iterations());
  }

  template<typename T>
  inline
  typename Array2d<T>::col_iterator 
  Array2d<T>::col_begin(const typename Array2d<T>::size_type col)
  {
    assert(col < this->d2_);
    return typename Array2d<T>::col_iterator(this->data_[0] + col,this->d2_);
  }

  template<typename T>
  inline
  typename Array2d<T>::const_col_iterator 
  Array2d<T>::col_begin(const typename Array2d<T>::size_type col) const
  {
    assert(col < this->d2_);
    return typename Array2d<T>::const_col_iterator(this->data_[0] + col,this->d2_);
  }

  template<typename T>
  inline
  typename Array2d<T>::col_iterator 
  Array2d<T>::col_end(const typename Array2d<T>::size_type col)
  {
    assert(col < this->d2_);
    return ++(typename Array2d<T>::col_iterator(this->data_[this->d1_- 1] + col,this->d2_));
  }
  
  template<typename T>
  inline
  typename Array2d<T>::const_col_iterator 
  Array2d<T>::col_end(const typename Array2d<T>::size_type col) const
  {
    assert(col < this->d2_);
    return ++(typename Array2d<T>::const_col_iterator(this->data_[this->d1_- 1] + col,this->d2_));
  }


  template<typename T>
  inline
  typename Array2d<T>::reverse_row_iterator 
  Array2d<T>::row_rbegin(const typename Array2d<T>::size_type row)
  {
    return typename Array2d<T>::reverse_row_iterator(this->row_end(row));
  }
 
  template<typename T>
  inline
  typename Array2d<T>::const_reverse_row_iterator 
  Array2d<T>::row_rbegin(const typename Array2d<T>::size_type row) const
  {
    return typename Array2d<T>::const_reverse_row_iterator(this->row_end(row));
  }
  
  template<typename T>
  inline
  typename Array2d<T>::reverse_row_iterator 
  Array2d<T>::row_rend(const typename Array2d<T>::size_type row)
  {
    return typename Array2d<T>::reverse_row_iterator(this->row_begin(row));
  }
 
  template<typename T>
  inline
  typename Array2d<T>::const_reverse_row_iterator 
  Array2d<T>::row_rend(const typename Array2d<T>::size_type row) const
  {
    return typename Array2d<T>::const_reverse_row_iterator(this->row_begin(row));
  }

  
  template<typename T>
  inline
  typename Array2d<T>::reverse_col_iterator 
  Array2d<T>::col_rbegin(const typename Array2d<T>::size_type col)
  {
    return typename Array2d<T>::reverse_col_iterator(this->col_end(col));
  }
 
  template<typename T>
  inline
  typename Array2d<T>::const_reverse_col_iterator 
  Array2d<T>::col_rbegin(const typename Array2d<T>::size_type col) const
  {
    return typename Array2d<T>::const_reverse_col_iterator(this->col_end(col));
  }
  
  template<typename T>
  inline
  typename Array2d<T>::reverse_col_iterator 
  Array2d<T>::col_rend(const typename Array2d<T>::size_type col)
  {
    return typename Array2d<T>::reverse_col_iterator(this->col_begin(col));
  }
 
  template<typename T>
  inline
  typename Array2d<T>::const_reverse_col_iterator 
  Array2d<T>::col_rend(const typename Array2d<T>::size_type col) const
  {
    return typename Array2d<T>::const_reverse_col_iterator(this->col_begin(col));
  }
  //------------
  template<typename T>
  inline
  typename Array2d<T>::reverse_row_range_iterator 
  Array2d<T>::row_rbegin(const typename Array2d<T>::size_type row,
			 const slip::Range<int>& range)
  {
    assert(row < this->d1_);
    assert(range.start() < (int)this->d2_);
    return typename Array2d<T>::reverse_row_range_iterator(this->row_end(row,range));
  }

  template<typename T>
  inline
  typename Array2d<T>::const_reverse_row_range_iterator 
  Array2d<T>::row_rbegin(const typename Array2d<T>::size_type row,
			 const slip::Range<int>& range) const
  {
    assert(row < this->d1_);
    assert(range.start() < (int)this->d2_);
    return typename Array2d<T>::const_reverse_row_range_iterator(this->row_end(row,range));
  }


  template<typename T>
  inline
  typename Array2d<T>::reverse_col_range_iterator 
  Array2d<T>::col_rbegin(const typename Array2d<T>::size_type col,
			 const slip::Range<int>& range)
  {
    assert(col < this->d2_);
    assert(range.start() < (int)this->d1_);
    return typename Array2d<T>::reverse_col_range_iterator(this->col_end(col,range));
  }

  template<typename T>
  inline
  typename Array2d<T>::const_reverse_col_range_iterator 
  Array2d<T>::col_rbegin(const typename Array2d<T>::size_type col,
			 const slip::Range<int>& range) const
  {
    assert(col < this->d2_);
    assert(range.start() < (int)this->d1_);
    return typename Array2d<T>::const_reverse_col_range_iterator(this->col_end(col,range));
  }

  template<typename T>
  inline
  typename Array2d<T>::reverse_row_range_iterator 
  Array2d<T>::row_rend(const typename Array2d<T>::size_type row,
		       const slip::Range<int>& range)
  {
    assert(row < this->d1_);
    assert(range.start() < (int)this->d2_);
    return typename Array2d<T>::reverse_row_range_iterator(this->row_begin(row,range));
  }

  template<typename T>
  inline
  typename Array2d<T>::const_reverse_row_range_iterator 
  Array2d<T>::row_rend(const typename Array2d<T>::size_type row,
			 const slip::Range<int>& range) const
  {
    assert(row < this->d1_);
    assert(range.start() < (int)this->d2_);
    return typename Array2d<T>::const_reverse_row_range_iterator(this->row_begin(row,range));
  }


  template<typename T>
  inline
  typename Array2d<T>::reverse_col_range_iterator 
  Array2d<T>::col_rend(const typename Array2d<T>::size_type col,
		       const slip::Range<int>& range)
  {
    assert(col < this->d2_);
    assert(range.start() < (int)this->d1_);
    return typename Array2d<T>::reverse_col_range_iterator(this->col_begin(col,range));
  }

  template<typename T>
  inline
  typename Array2d<T>::const_reverse_col_range_iterator 
  Array2d<T>::col_rend(const typename Array2d<T>::size_type col,
		       const slip::Range<int>& range) const
  {
    assert(col < this->d2_);
    assert(range.start() < (int)this->d1_);
    return typename Array2d<T>::const_reverse_col_range_iterator(this->col_begin(col,range));
  }


  template<typename T>
  inline
  typename Array2d<T>::iterator2d Array2d<T>::upper_left()
  {
    return typename Array2d<T>::iterator2d(this,Box2d<int>(0,0,this->d1_-1,this->d2_-1));
  }

  template<typename T>
  inline
  typename Array2d<T>::const_iterator2d Array2d<T>::upper_left() const
  {
    return typename Array2d<T>::const_iterator2d(this,Box2d<int>(0,0,this->d1_-1,this->d2_-1));
  }


  template<typename T>
  inline
  typename Array2d<T>::iterator2d Array2d<T>::bottom_right()
  {
     DPoint2d<int> dp(this->d1_,this->d2_);
     typename Array2d<T>::iterator2d it = (*this).upper_left() + dp;
    return it;
  }

  template<typename T>
  inline
  typename Array2d<T>::const_iterator2d Array2d<T>::bottom_right() const
  {
    DPoint2d<int> dp(this->d1_,this->d2_);
    typename Array2d<T>::const_iterator2d it = (*this).upper_left() + dp;
    return it;
  }

  template<typename T>
  inline
  typename Array2d<T>::iterator2d Array2d<T>::upper_left(const Box2d<int>& box)
  {
    return typename Array2d<T>::iterator2d(this,box);
  }

  template<typename T>
  inline
  typename Array2d<T>::const_iterator2d Array2d<T>::upper_left(const Box2d<int>& box) const
  {
    return typename Array2d<T>::const_iterator2d(this,box);
  }


  template<typename T>
  inline
  typename Array2d<T>::iterator2d 
  Array2d<T>::bottom_right(const Box2d<int>& box)
  {
    DPoint2d<int> dp(box.height(),box.width());
    typename Array2d<T>::iterator2d it = (*this).upper_left(box) + dp;
    return it;
  }

  template<typename T>
  inline
  typename Array2d<T>::const_iterator2d 
  Array2d<T>::bottom_right(const Box2d<int>& box) const
  {
    DPoint2d<int> dp(box.height(),box.width());
    typename Array2d<T>::const_iterator2d it = (*this).upper_left(box) + dp;
    return it;
  }
  
  template<typename T>
  inline
  typename Array2d<T>::iterator2d_range 
  Array2d<T>::upper_left(const Range<int>& row_range,
			 const Range<int>& col_range)
  {
    return typename Array2d<T>::iterator2d_range(this,row_range,col_range);
  }

  template<typename T>
  inline
  typename Array2d<T>::const_iterator2d_range 
  Array2d<T>::upper_left(const Range<int>& row_range,
			 const Range<int>& col_range) const
  {
    return typename Array2d<T>::const_iterator2d_range(this,row_range,col_range);
  }

  template<typename T>
  inline
  typename Array2d<T>::iterator2d_range 
  Array2d<T>::bottom_right(const Range<int>& row_range,
			   const Range<int>& col_range)
  {
     DPoint2d<int> dp(row_range.iterations()+1,col_range.iterations()+1);
    return  typename Array2d<T>::iterator2d_range((*this).upper_left(row_range,col_range) + dp);
  }

  template<typename T>
  inline
  typename Array2d<T>::const_iterator2d_range 
  Array2d<T>::bottom_right(const Range<int>& row_range,
			   const Range<int>& col_range) const
  {
    DPoint2d<int> dp(row_range.iterations()+1,col_range.iterations()+1);
    return  typename Array2d<T>::const_iterator2d_range ((*this).upper_left(row_range,col_range) + dp);
  }


   template<typename T>
  inline
  typename Array2d<T>::iterator2d_range 
  Array2d<T>::upper_left(const Range<int>& range)
  {
    return this->upper_left(range,range);
  }

  template<typename T>
  inline
  typename Array2d<T>::const_iterator2d_range 
  Array2d<T>::upper_left(const Range<int>& range) const
  {
    return this->upper_left(range,range);
  }

  template<typename T>
  inline
  typename Array2d<T>::iterator2d_range 
  Array2d<T>::bottom_right(const Range<int>& range)
  {
    return this->bottom_right(range,range);
  }

  template<typename T>
  inline
  typename Array2d<T>::const_iterator2d_range 
  Array2d<T>::bottom_right(const Range<int>& range) const
  {
    return this->bottom_right(range,range);
  }
  //--
  template<typename T>
  inline
  typename Array2d<T>::reverse_iterator2d 
  Array2d<T>::rbottom_right()
  {
    return typename Array2d<T>::reverse_iterator2d(this->upper_left());
  }

  template<typename T>
  inline
  typename Array2d<T>::const_reverse_iterator2d 
  Array2d<T>::rbottom_right() const
  {
    return typename Array2d<T>::const_reverse_iterator2d(this->upper_left());
  }

  template<typename T>
  inline
  typename Array2d<T>::reverse_iterator2d 
  Array2d<T>::rupper_left()
  {
    slip::DPoint2d<int> dp(1,0);
    return typename Array2d<T>::reverse_iterator2d(this->bottom_right() - dp);
   }

  template<typename T>
  inline
  typename Array2d<T>::const_reverse_iterator2d 
  Array2d<T>::rupper_left() const
  {
    slip::DPoint2d<int> dp(1,0);
    return typename Array2d<T>::const_reverse_iterator2d(this->bottom_right()-dp);
  }

  template<typename T>
  inline
  typename Array2d<T>::reverse_iterator2d 
  Array2d<T>::rbottom_right(const Box2d<int>& box)
  {
    return typename Array2d<T>::reverse_iterator2d(this->upper_left(box));
  }

  template<typename T>
  inline
  typename Array2d<T>::const_reverse_iterator2d 
  Array2d<T>::rbottom_right(const Box2d<int>& box) const
  {
    return typename Array2d<T>::const_reverse_iterator2d(this->upper_left(box));
  }

  template<typename T>
  inline
  typename Array2d<T>::reverse_iterator2d 
  Array2d<T>::rupper_left(const Box2d<int>& box)
  {
     slip::DPoint2d<int> dp(1,0);
    return typename Array2d<T>::reverse_iterator2d(this->bottom_right(box) - dp);
  }

  template<typename T>
  inline
  typename Array2d<T>::const_reverse_iterator2d 
  Array2d<T>::rupper_left(const Box2d<int>& box) const
  {
    slip::DPoint2d<int> dp(1,0);
    return typename Array2d<T>::const_reverse_iterator2d(this->bottom_right(box) - dp);
  }

  template<typename T>
  inline
  typename Array2d<T>::reverse_iterator2d_range 
  Array2d<T>::rupper_left(const Range<int>& row_range,
			  const Range<int>& col_range)
  {
    slip::DPoint2d<int> dp(1,0);
    return typename Array2d<T>::reverse_iterator2d_range(this->bottom_right(row_range,col_range) - dp);
  }

  template<typename T>
  inline
  typename Array2d<T>::const_reverse_iterator2d_range 
  Array2d<T>::rupper_left(const Range<int>& row_range,
			 const Range<int>& col_range) const
  {
    slip::DPoint2d<int> dp(1,0);
    return typename Array2d<T>::const_reverse_iterator2d_range(this->bottom_right(row_range,col_range)- dp);
  }

  template<typename T>
  inline
  typename Array2d<T>::reverse_iterator2d_range 
  Array2d<T>::rbottom_right(const Range<int>& row_range,
			    const Range<int>& col_range)
  {
    return typename Array2d<T>::reverse_iterator2d_range(this->upper_left(row_range,col_range));
  }

  template<typename T>
  inline
  typename Array2d<T>::const_reverse_iterator2d_range 
  Array2d<T>::rbottom_right(const Range<int>& row_range,
			    const Range<int>& col_range) const
  {
    return typename Array2d<T>::const_reverse_iterator2d_range(this->upper_left(row_range,col_range));
  }

  template<typename T>
  inline
  typename Array2d<T>::reverse_iterator2d_range 
  Array2d<T>::rupper_left(const Range<int>& range)
  {
    return this->rupper_left(range,range);
  }

  template<typename T>
  inline
  typename Array2d<T>::const_reverse_iterator2d_range 
  Array2d<T>::rupper_left(const Range<int>& range) const
  {
    return this->rupper_left(range,range);
  }

  template<typename T>
  inline
  typename Array2d<T>::reverse_iterator2d_range 
  Array2d<T>::rbottom_right(const Range<int>& range)
  {
    return this->rbottom_right(range,range);
  }

  template<typename T>
  inline
  typename Array2d<T>::const_reverse_iterator2d_range 
  Array2d<T>::rbottom_right(const Range<int>& range) const
  {
    return this->rbottom_right(range,range);
  }
 
/** \name input/output operators */
  /* @{ */
  template <typename T>
  inline
  std::ostream& operator<<(std::ostream & out, const Array2d<T>& a)
  {
    for(std::size_t i = 0; i < a.d1_; ++i)
      {
	for(std::size_t j = 0; j < a.d2_; ++j)
	  {
	    out<<a[i][j]<<" ";
	  }
	out<<std::endl;
      }    	
    //  out<<std::endl;
    return out;
  }
  /* @} */

  template<typename T>
  inline
  typename Array2d<T>::pointer 
  Array2d<T>::operator[](const typename Array2d<T>::size_type i)
  {
    assert(this->data_ != 0);
    assert(i < this->size_);
    return this->data_[i];
  }
  
  template<typename T>
  inline
  typename Array2d<T>::const_pointer 
  Array2d<T>::operator[](const typename Array2d<T>::size_type i) const 
  {
    assert(this->data_ != 0);
    assert(i < this->size_);
    return this->data_[i];
  }
  
  template<typename T>
  inline
  typename Array2d<T>::reference 
  Array2d<T>::operator()(const typename Array2d<T>::size_type i,
			 const typename Array2d<T>::size_type j)
  {
    assert(this->data_ != 0);
    assert(i < this->d1_);
    assert(j < this->d2_);
    return this->data_[i][j];
  }
  
  template<typename T>
  inline
  typename Array2d<T>::const_reference 
  Array2d<T>::operator()(const typename Array2d<T>::size_type i,
			 const typename Array2d<T>::size_type j) const
  {
    assert(this->data_ != 0);
    assert(i < this->d1_);
    assert(j < this->d2_);
    return this->data_[i][j];
  }
	
  template<typename T>
  inline
  typename Array2d<T>::reference 
  Array2d<T>::operator()(const Point2d<typename Array2d<T>::size_type>& point2d)
  {
    assert(this->data_ != 0);
    assert(point2d[0] < this->d1_);
    assert(point2d[1] < this->d2_);
    return this->data_[point2d[0]][point2d[1]];
  }
  
  template<typename T>
  inline
  typename Array2d<T>::const_reference 
  Array2d<T>::operator()(const Point2d<typename Array2d<T>::size_type>& point2d) const
  {
    assert(this->data_ != 0);
    assert(point2d[0] < this->d1_);
    assert(point2d[1] < this->d2_);
    return this->data_[point2d[0]][point2d[1]];
  }


  template<typename T>
  inline
  Array2d<T> 
  Array2d<T>::operator()(const Range<int>& row_range,
			 const Range<int>& col_range)
  {
     assert(this->data_ != 0);
     assert(row_range.is_valid());
     assert(row_range.start() < (int)this->d2_);
     assert(row_range.stop()  < (int)this->d2_);
     assert(col_range.is_valid());
     assert(col_range.start() < (int)this->d1_);
     assert(col_range.stop()  < (int)this->d1_);
     std::size_t rows = row_range.iterations() + 1;
     std::size_t cols = col_range.iterations() + 1;
     return Array2d<T>(rows,cols,this->upper_left(row_range,col_range),
		       this->bottom_right(row_range,col_range));
  }


  template<typename T>
  inline
  std::string 
  Array2d<T>::name() const {return "Array2d";} 
  

  template<typename T>
  inline
  typename Array2d<T>::size_type 
  Array2d<T>::dim1() const {return this->d1_;} 
  
  template<typename T>
  inline
  typename Array2d<T>::size_type 
  Array2d<T>::rows() const {return this->dim1();} 
  

  template<typename T>
  inline
  typename Array2d<T>::size_type 
  Array2d<T>::dim2() const {return this->d2_;} 
  
  template<typename T>
  inline
  typename Array2d<T>::size_type 
  Array2d<T>::columns() const {return this->dim2();} 
  
  template<typename T>
  inline
  typename Array2d<T>::size_type 
  Array2d<T>::cols() const {return this->dim2();} 
  
  template<typename T>
  inline
  typename Array2d<T>::size_type 
  Array2d<T>::size() const {return this->size_;}

  template<typename T>
  inline
  typename Array2d<T>::size_type 
  Array2d<T>::max_size() const 
  {
    return typename Array2d<T>::size_type(-1)/sizeof(T);
  }


  template<typename T>
  inline
  bool Array2d<T>::empty()const {return this->size_ == 0;}

  template<typename T>
  inline
  void Array2d<T>::swap(Array2d<T>& M)
  {
    assert(this->d1_ == M.d1_);
    assert(this->d2_ == M.d2_);
    std::swap_ranges(this->begin(),this->end(),M.begin());
  }

  template<typename T>
  inline
  Array2d<T>::~Array2d()
  {
    this->desallocate();
  }
  
  template<typename T>
  inline
  void Array2d<T>::allocate()
  {
    if((this->d1_ != 0) && (this->d2_ != 0))
      {
	this->data_ = new T*[this->d1_];
	//the first row of data_ "contains" all the elements 
	//of the 2D array 
	this->data_[0] = new T[this->d1_ * this->d2_];
	//the other elements of the data_ array contains pointers to the
	//other rows of the 2D array
	for(typename Array2d<T>::size_type i = 1; i < this->d1_; ++i)
	  {
	    this->data_[i] = this->data_[i - 1] + this->d2_;
	  }  
      }
    else
      {
	this->data_ = 0;
      }
  }

  template<typename T>
  inline
  void Array2d<T>::desallocate()
  {
    if(this->data_ != 0)
      {
	delete[] (this->data_[0]);
	delete[] (this->data_);
      }
  }

  template<typename T>
  inline
  void Array2d<T>::copy_attributes(const Array2d<T>& rhs)
  {
    this->d1_ = rhs.d1_;
    this->d2_ = rhs.d2_;
    this->size_ = rhs.d1_ * rhs.d2_;
  }

/** \name EqualityComparable functions */
  /* @{ */
template<typename T>
inline
bool operator==(const Array2d<T>& x, 
		const Array2d<T>& y)
{
  return ( x.size() == y.size()
	   && std::equal(x.begin(),x.end(),y.begin())); 
}

template<typename T>
inline
bool operator!=(const Array2d<T>& x, 
		const Array2d<T>& y)
{
  return !(x == y);
}

/* @} */

/** \name LessThanComparable functions */
  /* @{ */
template<typename T>
inline
bool operator<(const Array2d<T>& x, 
	       const Array2d<T>& y)
{
  return std::lexicographical_compare(x.begin(), x.end(),
				      y.begin(), y.end());
}


template<typename T>
inline
bool operator>(const Array2d<T>& x, 
	       const Array2d<T>& y)
{
  return (y < x);
}

template<typename T>
inline
bool operator<=(const Array2d<T>& x, 
		const Array2d<T>& y)
{
  return !(y < x);
}

template<typename T>
inline
bool operator>=(const Array2d<T>& x, 
		const Array2d<T>& y)
{
  return !(x < y);
} 
/* @} */

}//slip::

#endif //SLIP_ARRAY2D_HPP
