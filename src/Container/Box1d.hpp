/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Box1d.hpp
 * 
 * \brief Provides a class to manipulate 1d box.
 * 
 */
#ifndef SLIP_BOX1D_HPP
#define SLIP_BOX1D_HPP

#include <iostream>
#include <cassert>
#include <string>
#include "Box.hpp"
#include "Point.hpp"
#include "Point1d.hpp"
#include "DPoint1d.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{


/*! \class Box1d
**  \ingroup Containers MiscellaneousContainers
** \brief This is a %Box1d class, a specialized version of slip::Box<CoordType,DIM>
**        with DIM = 1
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/03/15
** \since 1.0.0
** \par Description:
** The box is defined by its two extremal 1d points p1 and p2.
** \code
** p1--------p2 
** \endcode
** \param CoordType Type of the coordinates of the Box1d.
*/
template <typename CoordType>
class Box1d: public slip::Box<CoordType,1>
{
public :
  typedef Box1d<CoordType> self;
  typedef Box<CoordType,1> base;
  
 /**
  ** \name Constructors & Destructors
  */
 /*@{*/
  /*!
  ** \brief Constructs a Box3d.
  ** \remarks call the default constructors of Point<CoordType,1>
  */
  Box1d();
  /*!
  ** \brief Constructs a Box1d from a CoordType array.
  ** \param p1 first extremal point
  ** \param p2 second extremal point
  */
  Box1d(const Point<CoordType,1>& p1, 
	const Point<CoordType,1>& p2);
  
  /*!
  ** \brief Constructs a Box1d.
  ** \param x1 coordinate of the minimal point p1
  ** \param x2 coordinate of the maximal point p2
  */
  Box1d(const CoordType& x1,
	const CoordType& x2);

 

  /*!
  ** \brief Constructs a plane Box1d using a central point and a width.
  ** \param pc central point
  ** \param w width of the box (real width and size=2*w+1)
  */
  Box1d(const Point<CoordType,1>& pc,
	const CoordType& w);

  /*@} End Constructors */

   /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Accessor/Mutator of the upper_left point (p1) of Box1d. 
  ** \return reference to the upper_left Point1d of the Box1d
  */
  void upper_left(const Point<CoordType,1>& p1);
  /*!
  ** \brief Accessor/Mutator of the upper_left point (p1) of Box1d. 
  ** \return reference to the upper_left Point2d of the Box1d
  */
  Point<CoordType,1>& upper_left();
  /*!
  ** \brief Accessor/Mutator of the upper_left point (p1) of Box1d. 
  ** \return reference to the upper_left Point2d of the Box1d
  */
  const Point<CoordType,1>& upper_left() const;


  /*!
  ** \brief Accessor/Mutator of the bottom_right point (p2) of Box1d. 
  ** \return reference to the bottom_right Point2d of the Box1d
  */
  void bottom_right(const Point<CoordType,1>& p2);
  /*!
  ** \brief Accessor/Mutator of the bottom_right point (p2) of Box1d. 
  ** \return reference to the bottom_right Point2d of the Box1d
  */
  Point<CoordType,1>& bottom_right();
  /*!
  ** \brief Accessor/Mutator of the bottom_right point (p2) of Box1d. 
  ** \return reference to the bottom_right Point2d of the Box1d
  */
  const Point<CoordType,1>& bottom_right() const;

  /*!
  ** \brief Mutator of the coordinates of the %Box1d. 
  ** \param x1 coordinate of the minimal point p1
  ** \param x2 coordinate of the maximal point p2
  ** \return 
  */
  void set_coord(const CoordType& x1,
		 const CoordType& x2);
  /*@} End Element access operators */

  
  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;

  /*!
  ** \brief compute the length of the Box1d.
  ** \return the area
  */
  CoordType length() const;

  /*!
  ** \brief compute the width of the Box1d.
  ** \return the width
  */
  CoordType width() const;

private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::Box<CoordType,1> >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::Box<CoordType,1> >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()

};
}//slip::

namespace slip
{
  template<typename CoordType>
  inline
  Box1d<CoordType>::Box1d():
    Box<CoordType,1>()
  {}

  template<typename CoordType>
  inline
  Box1d<CoordType>::Box1d(const Point<CoordType,1>& p1,
			  const Point<CoordType,1>& p2):
    Box<CoordType,1>(p1,p2)
  {}
  
  template<typename CoordType>
  inline
  Box1d<CoordType>::Box1d(const CoordType& x1,
			  const CoordType& x2):
    Box<CoordType,1>(Point1d<CoordType>(x1),Point1d<CoordType>(x2))
  {}

  template<typename CoordType>
  inline
  Box1d<CoordType>::Box1d(const Point<CoordType,1>& pc,
			  const CoordType& w):
    Box<CoordType,1>(Point1d<CoordType>(pc[0]-w),Point1d<CoordType>(pc[0]+w))
  {}




  template<typename CoordType>
  inline
  void Box1d<CoordType>::upper_left(const Point<CoordType,1>& p1) {this->p1_=p1;}

  template<typename CoordType>
  inline
  Point<CoordType,1>& Box1d<CoordType>::upper_left() {return this->p1_;}
  
  template<typename CoordType>
  inline
  const Point<CoordType,1>& Box1d<CoordType>::upper_left() const {return this->p1_;}


  template<typename CoordType>
  inline
  void Box1d<CoordType>::bottom_right(const Point<CoordType,1>& p2) {this->p2_=p2;}

  template<typename CoordType>
  inline
  Point<CoordType,1>& Box1d<CoordType>::bottom_right() {return this->p2_;}

  template<typename CoordType>
  inline
  const Point<CoordType,1>& Box1d<CoordType>::bottom_right() const {return this->p2_;}


  template<typename CoordType>
  inline
  void Box1d<CoordType>::set_coord(const CoordType& x1,
				   const CoordType& x2)
  {
    this->p1_[0] = x1;
    this->p2_[0] = x2;
  }

  template<typename T>
  inline
  std::string 
  Box1d<T>::name() const {return "Box1d";} 
 
  
  template<typename CoordType>
  inline
  CoordType Box1d<CoordType>::length() const {return this->width();}

  template<typename CoordType>
  inline
  CoordType Box1d<CoordType>::width() const {return (this->p2_[0] - this->p1_[0]+1);}

}//slip::

#endif //SLIP_BOX1D_HPP
