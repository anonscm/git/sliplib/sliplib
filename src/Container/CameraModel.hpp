/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file CameraModel.hpp
 * \brief Provides an abstract class to model cameras
 * 
 */

#ifndef SLIP_CAMERAMODEL_HPP
#define SLIP_CAMERAMODEL_HPP


#include <iostream>
#include <cassert>
#include "Point2d.hpp"
#include "Point3d.hpp"
#include "Matrix.hpp"
#include "utils_compilation.hpp"
#include "macros.hpp"
#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template <typename Type>
class CameraModel;

/*!
 *  @defgroup Camera Camera containers
 *  @brief Camera containers
 *  @{
 */
/*! \class CameraModel
**  \ingroup Containers Camera
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \author Guillaume Gomit <guillaume.gomit_AT_univ-poitiers.fr>
** \author Gwenaël ACHER <gwenael.acher01_AT_univ-poitiers.fr>
** \version 0.0.3
** \date 2014/03/25
** \date 2023/09/15 addition of plan projection methods
** \since 1.2.0
** \brief Define an abstract %CameraModel class
** \param Type Type of the coordinates of the CameraModel
*/
template <typename Type>
class CameraModel
{
typedef CameraModel<Type> self;

public:
 /**
 ** \name Constructors & Destructors
 */
 /*@{*/

  /*!
  ** \brief Constructs a copy of the CameraModel \a rhs
  ** \param other
  */
  CameraModel(const self& other);

  /*!
  ** \brief Destructor of the %CameraModel
  */
  virtual ~CameraModel();


 /*@} End Constructors */


 
  /**
   ** \name i/o methods
   */
  /*@{*/
  /*!
  ** \brief Read the CameraModel from a file
  ** \param file File path name.
  */
  virtual void read(const std::string& file) = 0;

   /*!
  ** \brief Write the CameraModel to a file
  ** \param file File path name.
  */
  virtual void write(const std::string& file) = 0;
  /*@} i/o  methods */


  /**
   ** \name  Assignment operators and methods
   */
  /*@{*/
  /*!
  ** \brief Assign a CameraModel.
  **
  ** Assign elements of CameraModel in \a other
  **
  ** \param other CameraModel to get the values from.
  ** \return self 
  */
  self& operator=(const self & other);

  /*!
  ** \brief Clone the %CameraModel.
  ** \since 1.5.0
  ** \return new pointer towards a copy of this %CameraModel
  ** \remark must be detroyed after use
  */
  virtual self* clone () const=0;

  /*@} End Assignment operators and methods*/



  /**
   ** \name  Projection methods
   */
  /*@{*/
  /*!
  ** \brief Computes the projection of a 3d world point
  ** onto the image plane.
  ** \param p %Point3d.
  ** \return %Point2d
   */
  virtual slip::Point2d<Type> projection(const slip::Point3d<Type>& p) const = 0;

  /*!
  ** \brief Computes the 3d world point corresponding to
  ** the backprojection of an image point.
  ** \param p %Point2d.
  ** \param z Depth coordinate.
  ** \return %Point3d
   */
  virtual slip::Point3d<Type> backprojection(const slip::Point2d<Type>& p2, const Type& z) const = 0;


 /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection on X plane of an image point.
    ** \since 1.5.0
    ** \author Gwenaël ACHER <gwenael.acher01_AT_univ-poitiers.fr>
    ** \param p %Point2d.
    ** \param x coordinate of x plane.
    ** \return %Point3d
   
    */
    virtual slip::Point3d<Type> X_backprojection(const slip::Point2d<Type>& p2, const Type& x) const = 0;

    /*!
    ** \brief Computes the 3d world point corresponding to the backprojection of an image point on a X plane and test if it is in the square.
    ** \since 1.5.0
    ** \author Gwenaël ACHER <gwenael.acher01_AT_univ-poitiers.fr>
    ** \param p2 %Point2d.
    ** \param x plane x coordinate.
    ** \param ymin minimum coordinate along y
    ** \param ymax maximum coordinate along y
    ** \param zmin minimum coordinate along z
    ** \param zmax maximum coordinate along z
    ** \param p3d backprojected point
    ** \return boolean if point exists in the square
    */
    virtual bool X_backprojection_in_square(const slip::Point2d<Type>& p2, 
					    const Type& x,
					    const Type &ymin,
					    const Type &ymax,
					    const Type &zmin,
					    const Type &zmax,
					    slip::Point3d<Type> &p3d) const = 0;
  
    /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection on Y plane of an image point.
    ** \since 1.5.0
    ** \author Gwenaël ACHER <gwenael.acher01_AT_univ-poitiers.fr>
    ** \param p %Point2d.
    ** \param y coordinate of y plane.
    ** \return %Point3d
    */
    virtual slip::Point3d<Type> Y_backprojection(const slip::Point2d<Type>& p2, const Type& y) const = 0;

    /*!
    ** \brief Computes the 3d world point corresponding to the backprojection of an image point on a Y plane and test if it is in the square.
    ** \since 1.5.0
    ** \author Gwenaël ACHER <gwenael.acher01_AT_univ-poitiers.fr>
    ** \param p2 %Point2d.
    ** \param y plane y coordinate.
    ** \param xmin minimum coordinate along x
    ** \param xmax maximum coordinate along x
    ** \param zmin minimum coordinate along z
    ** \param zmax maximum coordinate along z
    ** \param p3d backprojected point
    ** \return boolean if point exists in the square
    */
    virtual bool Y_backprojection_in_square(const slip::Point2d<Type>& p2, 
					    const Type& y,
					    const Type &xmin,
					    const Type &xmax,
					    const Type &zmin,
					    const Type &zmax,
					    slip::Point3d<Type> &p3d) const = 0;

    /*!
    ** \brief Computes the 3d world point corresponding to
    ** the backprojection on Z plane of an image point.
    ** \since 1.5.0
    ** \author Gwenaël ACHER <gwenael.acher01_AT_univ-poitiers.fr>
    ** \param p %Point2d.
    ** \param z coordinate of z plane.
    ** \return %Point3d
    */
    virtual slip::Point3d<Type> Z_backprojection(const slip::Point2d<Type>& p2,
						 const Type& z) const = 0;

    /*!
    ** \brief Computes the 3d world point corresponding to the backprojection of an image point on a Z plane and test if it is in the square.
    ** \since 1.5.0
    ** \author Gwenaël ACHER <gwenael.acher01_AT_univ-poitiers.fr>
    ** \param p2 %Point2d.
    ** \param z plane z coordinate.
    ** \param xmin minimum coordinate along x
    ** \param xmax maximum coordinate along x
    ** \param ymin minimum coordinate along y
    ** \param ymax maximum coordinate along y
    ** \param p3d backprojected point
    ** \return boolean if point exists in the square
    */
    virtual bool Z_backprojection_in_square(const slip::Point2d<Type>& p2, 
					    const Type& z,
					    const Type &xmin,
					    const Type &xmax,
					    const Type &ymin,
					    const Type &ymax,
					    slip::Point3d<Type> &p3d) const = 0;


   
     /*!
    ** \brief Computes the projection gradient of a 3d world point
    ** onto the image plane.
    ** \since 1.5.0
    ** \author Gwenaël ACHER <gwenael.acher01_AT_univ-poitiers.fr>
    ** \param p %Point3d.
    ** \param delta spatial derivative step.
    ** \return %Matrix
    */
    inline
    virtual slip::Matrix<Type> projection_gradient(const slip::Point3d<Type>& pt,
						   const Type& delta= slip::constants<Type>::one()) const
    {
      slip::Matrix<Type> res(2,3,Type());
      slip::Point3d<Type> pt_m(pt);
      slip::Point3d<Type> pt_p(pt);
      slip::Point2d<Type> p2d_m, p2d_p;    
      const Type two_delta = slip::constants<Type>::two()*delta;
      // dPsi/dx
      pt_m[0] -= delta;
      pt_p[0] += delta;
      p2d_p = this->projection(pt_p);
      p2d_m = this->projection(pt_m);
      res[0][0] = (p2d_p[0]-p2d_m[0])/two_delta;
      res[1][0] = (p2d_p[1]-p2d_m[1])/two_delta;

      // dPsi/dy
      pt_m[0] = pt[0];
      pt_p[0] = pt[0];
      pt_m[1] -= delta;
      pt_p[1] += delta;
      p2d_p = this->projection(pt_p);
      p2d_m = this->projection(pt_m);
      res[0][1] = (p2d_p[0]-p2d_m[0])/two_delta;
      res[1][1] = (p2d_p[1]-p2d_m[1])/two_delta;

      // dPsi/dz
      pt_m[1] = pt[1];
      pt_p[1] = pt[1];
      pt_m[2] -= delta;
      pt_p[2] += delta;
      p2d_p = this->projection(pt_p);
      p2d_m = this->projection(pt_m);
      res[0][2] = (p2d_p[0]-p2d_m[0])/two_delta;
      res[1][2] = (p2d_p[1]-p2d_m[1])/two_delta;
      
      return res;
    }

  
  /*@} End projection methods*/

  /**
   ** \name  Computation methods
   */
  /*@{*/
  /*!
  ** \brief Computes the parameters of a camera model
  ** \param P Matrix containing the input data.

   */
//  virtual void compute(const slip::Matrix<Type>& P, const slip::ArgsStruct &s) = 0;
virtual void compute(const slip::Matrix<Type>& P) = 0;
  
  /*@} End computation methods*/

  virtual std::ostream& display(std::ostream& out) const =0;

protected:

  /**
   ** \name Constructors
   */
  /*@{*/

  /*!
  ** \brief Default constructor of CameraModel
  */
  CameraModel();

 /*@} End Constructors */



private:
  void copy_attributes(const self& other);
 friend class boost::serialization::access;
  template<class Archive>
  void save(Archive & UNUSED(ar), const unsigned int version) const
    {
      if(version >= 0)
	{
	}
    }
  template<class Archive>
  void load(Archive & UNUSED(ar), const unsigned int version)
    {
      if(version >= 0)
	{
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
};
/** @} */ // end of Camera group
}//slip::


namespace slip
{

  template<typename Type>
  std::ostream& operator<<(std::ostream & out, const CameraModel<Type>& c) 
  {
    return c.display(out);
  }
  template<typename Type>
  inline
  CameraModel<Type>::CameraModel()
  {}

  template<typename Type>
  inline
  CameraModel<Type>::CameraModel(const CameraModel<Type>& UNUSED(c))
  {
  }

  template<typename Type>
  inline
  CameraModel<Type>::~CameraModel()
  {}

  template<typename Type>
  inline
  CameraModel<Type>&
  CameraModel<Type>::operator=(const CameraModel<Type> & other)
  {
    if(this != &other)
      {

      }
    return *this;
  }

}//slip::
#endif //SLIP_CAMERAMODEL_HPP

