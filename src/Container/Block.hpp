/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Block.hpp
 * 
 * \brief Provides a class to manipulate 1d static and generic arrays.
 * 
 */
#ifndef SLIP_BLOCK_HPP
#define SLIP_BLOCK_HPP

#include <iterator>
#include <iostream>
#include <cstddef>
#include "Range.hpp"
#include "stride_iterator.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template <class T, std::size_t N>
struct block;

template <typename T, std::size_t N>
std::ostream& operator<<(std::ostream & out,
			 const block<T,N>& b);

template <typename T, std::size_t N>
bool operator==(const block<T,N>& x, 
		const block<T,N>& y);

template <typename T, std::size_t N>
bool operator!=(const block<T,N>& x, 
		const block<T,N>& y);

template <typename T, std::size_t N>
bool operator<(const block<T,N>& x, 
	       const block<T,N>& y);

template <typename T, std::size_t N>
bool operator>(const block<T,N>& x, 
	       const block<T,N>& y);

template <typename T, std::size_t N>
bool operator<=(const block<T,N>& x, 
		const block<T,N>& y);

template <typename T, std::size_t N>
bool operator>=(const block<T,N>& x, 
		const block<T,N>& y);

/*!
 *  @defgroup Containers Group of Containers
 *  @brief Group of all containers (data structures) of SLIP
 *  @{
 */
 /* @} end of Containers group */ 

/*! \struct block
** \ingroup Containers Containers1d
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.3
** \date 2014/03/15
** \since 1.0.0
** \brief This is a linear (one-dimensional) static container.
**  This container statisfies the RandomAccessContainer concepts of the 
**  Standard Template Library (STL).
** \param T Type of the element in the %block
** \param N number of element in the %block
*/
template <class T, std::size_t N>
struct block{

  typedef block<T,N> self;
  typedef T value_type;
  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;
  
  typedef slip::stride_iterator<pointer> iterator_range;
  typedef slip::stride_iterator<const_pointer> const_iterator_range;

  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

  typedef std::reverse_iterator<iterator_range> reverse_iterator_range;
  typedef std::reverse_iterator<const_iterator_range> const_reverse_iterator_range;

  //default iterator of the container
  typedef iterator default_iterator;
  typedef const_iterator const_default_iterator;
  
  static const std::size_t SIZE = N;
  static const std::size_t DIM = 1;

  /**
   ** \name iterators
   */
  /*@{*/
 
 
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %block.  Iteration is done in ordinary
  **  element order.
  ** \return begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator begin(){return this->data;}
  

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %block.  Iteration is done in ordinary
  **  element order.
  ** \return const begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator begin() const {return this->data;} 

  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %block.  Iteration is done in ordinary
  **  element order.
  ** \return end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator end(){return this->data + N;}
 
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %block.  Iteration is done in
  **  ordinary element order.
  ** \return const end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator end() const {return this->data + N;}

  /*!
  **  \brief Returns a read/write reverse iterator that points to the
  **  last element in the %block.  Iteration is done in reverse
  **  element order.
  ** \return reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rbegin(){return reverse_iterator(this->end());}

  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to the last element in the %block.  Iteration is done in
  **  reverse element order.
  ** \return const reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_reverse_iterator rbegin() const {return const_reverse_iterator(this->end());}

  /*!
  **  \brief Returns a read/write reverse iterator that points to one
  **  before the first element in the %block.  Iteration is done
  **  in reverse element order.
  **  \return reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rend(){return reverse_iterator(this->begin());}

    
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to one before the first element in the %block.  Iteration
  **  is done in reverse element order.
  **  \return const reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_reverse_iterator rend() const {return const_reverse_iterator(this->begin());}

  /*!
  **  \brief Returns a read/write iterator_range that points 
  **  the first element within the %Range.  
  **  Iteration is done in ordinary element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return end iterator_range value
  ** \pre The range must be inside the whole range of the %block.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::block<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator_range begin(const slip::Range<int>& range)
  {
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < int(this->size()));
    assert(range.stop()  < int(this->size()));
    return iterator_range(this->begin() + range.start(),range.stride());
  }
 
  /*!
  ** \brief Returns a read-only (constant) iterator_range 
  ** that points the first element within the %Range.  
  ** Iteration is done in ordinary element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return const_iterator_range value
  ** \pre The range must be inside the whole range of the %block.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::block<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout,"
  "));
   ** \endcode
  */
  const_iterator_range begin(const slip::Range<int>& range) const
  {
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < int(this->size()));
    assert(range.stop()  < int(this->size()));
    return const_iterator_range(this->begin() + range.start(),range.stride());
  }

  /*!
  **  \brief Returns a read/write iterator_range that points 
  **  one past the last element in the %block.  
  **  Iteration is done in ordinary element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return end iterator_range value
  ** \pre The range must be inside the whole range of the %block.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::block<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator_range end(const slip::Range<int>& range)
  {
    return this->begin(range) + (range.iterations() + 1);
  }
 
  /*!
  **  \brief Returns a read-only (constant) iterator_range 
  ** that points one past the last element in the %block.  
  ** Iteration is done in ordinary element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return const_iterator_range value
  ** \pre The range must be inside the whole range of the %block.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::block<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout,"
  "));
  ** \endcode
  */
  const_iterator_range end(const slip::Range<int>& range) const
  {
    return this->begin(range) + (range.iterations() + 1);
  }

  //
    /*!
  **  \brief Returns a read/write reverse_iterator_range that points 
  **  the end element within the %Range.  
  **  Iteration is done in reverse element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %block.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::block<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator_range rbegin(const slip::Range<int>& range)
  {
    return reverse_iterator_range(this->end(range));
  }
 
  /*!
  ** \brief Returns a read-only (constant) reverse_iterator_range 
  ** that points the end element within the %Range.  
  ** Iteration is done in reverse element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return cons_treverse__iterator_range value
  ** \pre The range must be inside the whole range of the %block.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::block<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout,"
  "));
   ** \endcode
  */
  const_reverse_iterator_range rbegin(const slip::Range<int>& range) const
  {
    return const_reverse_iterator_range(this->end(range));
  }
 

  /*!
  **  \brief Returns a read/write reverse_iterator_range that points 
  **  one previous the first element in the %Range.  
  **  Iteration is done in reverse element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %block.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::block<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator_range rend(const slip::Range<int>& range)
  {
    return  reverse_iterator_range(this->begin(range));
  }
 
  /*!
  **  \brief Returns a read-only (constant) reverse_iterator_range 
  ** that points one previous the first element in the %Range.  
  ** Iteration is done in reverse element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return const_reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %block.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::block<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout,"
  "));
  ** \endcode
  */
  const_reverse_iterator_range rend(const slip::Range<int>& range) const
  {
     return  const_reverse_iterator_range(this->begin(range));
  }



 
  /*@} End iterators */

  /**
   ** \name i/o operators
   */
  /*@{*/
  
  /*!
  ** \brief Write the %block to an ouput stream
  ** \param out output std::ostream
  ** \param b %block to write to an output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const block<T,N>& b);

  /*@} End i/o operators */


  /**
   ** \name  Assignment methods
   */
  /*@{*/

  /*!
  ** \brief Assigns all the element of the %block by the %block rhs
  ** \param rhs block<T,N>
  ** \return reference to corresponding %block
  */
  block<T,N>& operator=(const block<T,N>& rhs)
  {
     if(this != &rhs)
      {
	std::copy(rhs.begin(),rhs.end(),this->begin());
      }
    return *this;
  }

  /*!
  ** \brief Assigns all the element of the %block by val
  ** \param val affectation value
  ** \return reference to corresponding %block
  */
  block<T,N>& operator=(const T& val)
  {
    this->fill(val);
    return *this;
  }

  /*!
  ** \brief Fills the container range [begin(),begin()+N) with copies of value
  ** \param value  A reference-to-const of arbitrary type.
  */
  void fill(const T& value)
  {
    std::fill_n(this->begin(),N,value);
  } 
  
 /*!
  ** \brief Fills the container range [begin(),begin()+N) with a copy of
  **        the value array
  ** \param value  A pointer of arbitrary type.
  */
  void fill(const T* value)
  {
    std::copy(value,value + N, this->begin());
  } 

  /*!
  ** \brief Fills the container range [begin(),begin()+N) with a copy of
  **        the range [first,last)
  **  \param  first  An input iterator.
  **  \param  last   An input iterator.
  **   
  **
  */
  template<typename InputIterator>
  void fill(InputIterator first,
	    InputIterator last)
  {
    std::copy(first,last, this->begin());
  }
  /*@} End Assignment operators and methods*/

  /**
   ** \name Comparison operators
   */
  /*@{*/
  /*!
  ** \brief Block equality comparison
  ** \param x A %block
  ** \param y A %block of the same type of \a x
  ** \return true iff the size and the elements of the blocks are equal
  */
  friend bool operator== <>(const block<T,N>& x, 
			    const block<T,N>& y);

 /*!
  ** \brief Block inequality comparison
  ** \param x A %block
  ** \param y A %block of the same type of \a x
  ** \return true if !(x == y) 
  */
  friend bool operator!= <>(const block<T,N>& x, 
			    const block<T,N>& y);

 /*!
  ** \brief Less than comparison operator (block ordering relation)
  ** \param x A %block
  ** \param y A %block of the same type of \a x
  ** \return true iff \a x is lexicographically less than \a y
  */
  friend bool operator< <>(const block<T,N>& x, 
			   const block<T,N>& y);

 /*!
  ** \brief More than comparison operator
  ** \param x A %block
  ** \param y A %block of the same type of \a x
  ** \return true iff y > x 
  */
  friend bool operator> <>(const block<T,N>& x, 
			   const block<T,N>& y);

  /*!
  ** \brief Less than equal comparison operator
  ** \param x A %block
  ** \param y A %block of the same type of \a x
  ** \return true iff !(y > x) 
  */
  friend bool operator<= <>(const block<T,N>& x, 
			    const block<T,N>& y);

 /*!
  ** \brief More than equal comparison operator
  ** \param x A %block
  ** \param y A %block of the same type of \a x
  ** \return true iff !(x < y) 
  */
  friend bool operator>= <>(const block<T,N>& x, 
			    const block<T,N>& y);


  /*@} Comparison operators */


  /**
   ** \name Element access operators
   */
  /*@{*/
  /*!
  ** \brief Subscript access to the data contained in the %block.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read/write reference to data.
  ** \pre n < N
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator[](const size_type n){return *(this->data + n);}

  /*!
  ** \brief Subscript access to the data contained in the %block.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read-only (constant) reference to data.
  ** \pre n < N
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator[](const size_type n) const {return *(this->data + n);}

  /*!
  ** \brief Subscript access to the data contained in the %block.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read/write reference to data.
  ** \pre n < N
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const size_type n) {return *(this->data + n);}
  
  
  /*!
  ** \brief Subscript access to the data contained in the %block.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read-only (constant) reference to data.
  ** \pre n < N
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const size_type n) const {return *(this->data + n);}



  /*@} End element access operators */

  /*!
  ** \brief Returns the number of elements in the %block
  */
  size_type size() const {return N;}

  /*!
  ** \brief Returns the largest possible %block
  */
  size_type max_size() const {return size_type(-1)/sizeof(value_type);}

  /*!
  ** \brief Returns true if the %block is empty.  (Thus size == 0)
  */
  bool empty() const {return N == 0;}

  /*!
  ** \brief Swaps data with another %block.
  ** \param x A %block of the same element type
  **
  ** \pre [x.begin(),x.begin()+(end() - begin())) is a valid range
  ** \pre [begin(),end()) do not overlap [x.begin(),x.begin()+(end() - begin()))
  */
  void swap(block& x)
  {
     std::swap_ranges(this->begin(),this->end(),x.begin());
  }
  
  /// Data array storage of the block
  T data[N];

private:
friend class boost::serialization::access;
  template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & data;
	}
    }
  template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & data;
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
};

}//slip::

namespace slip
{
/** \name input/output operators */
  /* @{ */
template <typename T, std::size_t N>
std::ostream& operator<<(std::ostream & out, 
				const block<T,N>& b)
  {
    out<<"(";
    if(N > std::size_t(1))
      for(std::size_t n = 0; n != N - 1; ++n)
	{
	  out<<b.data[n]<<",";
	}
    out<<b.data[N-1];
    out<<")";
    return out;
  }
 /* @} */
/** \name EqualityComparable functions */
  /* @{ */
template <typename T, std::size_t N>
inline
bool operator==(const block<T,N>& x, 
		const block<T,N>& y)
{
  return ( x.size() == y.size()
	   && std::equal(x.begin(),x.end(),y.begin())); 
}

template <typename T, std::size_t N>
inline
bool operator!=(const block<T,N>& x, 
		const block<T,N>& y)
{
  return !(x == y);
}

/* @} */
/** \name LessThanComparable functions */
  /* @{ */
template <typename T, std::size_t N>
inline
bool operator<(const block<T,N>& x, 
	       const block<T,N>& y)
{
  return std::lexicographical_compare(x.begin(), x.end(),
				      y.begin(), y.end());
}


template <typename T, std::size_t N>
inline
bool operator>(const block<T,N>& x, 
	       const block<T,N>& y)
{
  return (y < x);
}


template <typename T, std::size_t N>
inline
bool operator<=(const block<T,N>& x, 
		const block<T,N>& y)
{
  return !(y < x);
}


template <typename T, std::size_t N>
inline
bool operator>=(const block<T,N>& x, 
		const block<T,N>& y)
{
  return !(x < y);
}
/* @} */
}//slip::

#endif //SLIP_BLOCK_HPP
