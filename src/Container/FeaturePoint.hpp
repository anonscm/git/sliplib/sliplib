/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file FeaturePoint.hpp
 * 
 * \brief  Provides a class to manipulate feature points.
 * 
 */
#ifndef SLIP_FEATUREPOINT_HPP
#define SLIP_FEATUREPOINT_HPP

#include <iostream>
#include <iterator>
#include <ios>
#include <stdexcept>
#include <fstream>
#include <vector>
#include <map>
#include <cassert>
#include "error.hpp"

namespace slip
{

 
class EmptyFeature;

std::ostream& operator<<(std::ostream & out, 
			 const EmptyFeature& EF);

bool operator==(const EmptyFeature& x, 
		const EmptyFeature& y);
bool operator!=(const EmptyFeature& x, 
		const EmptyFeature& y);

  /*!
 *  @defgroup Feature Points
 *  @brief Classes defined to manipulate points with location and features.
 *  @{
 */
  
 /*! \class EmptyFeature
  ** \ingroup FeaturePoints MiscellaneousContainers
  ** \author Benoit Tremblais <benoit.tremblais_AT_univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2016/10/13
  ** \since 1.5.0
  ** \brief Define a %EmptyFeature class in order to define %FeaturePoint class with no features (i.e. Point coordinates only).
   */  
class EmptyFeature
{
public:
  typedef EmptyFeature self;

  friend std::ostream& operator<<(std::ostream & out, 
				  const self& F);
   
  friend bool operator==(const self& x, 
			 const self& y);
  
  friend bool operator!=(const self& x, 
			 const self& y);
  
  std::string descriptor() const
  {
    return "";
  }

};

inline
 std::ostream& operator<<(std::ostream & out, 
			  const EmptyFeature& F)
{
  out<<"";
  return out;
}


inline
bool operator==(const EmptyFeature& x, 
		const EmptyFeature& y)
{
  return true;
}

inline
bool operator!=(const EmptyFeature& x, 
		const EmptyFeature& y)
{
  return !(x == y);
}

}//::slip


namespace slip
{
  template<typename Point,
	   typename Features>
 class FeaturePoint;

  template <typename Point,
	    typename Features>
std::ostream& operator<<(std::ostream & out, 
			 const FeaturePoint<Point,Features>& IP);
template <typename Point,
	  typename Features>
bool operator<(const FeaturePoint<Point,Features>& x, 
	       const FeaturePoint<Point,Features>& y);

template <typename Point,
	  typename Features>
bool operator>(const FeaturePoint<Point,Features>& x, 
	       const FeaturePoint<Point,Features>& y);

template <typename Point,
	  typename Features>
bool operator<=(const FeaturePoint<Point,Features>& x, 
	       const FeaturePoint<Point,Features>& y);

template <typename Point,
	  typename Features>
bool operator>=(const FeaturePoint<Point,Features>& x, 
	       const FeaturePoint<Point,Features>& y);

template<typename Point,
	 typename Features>
bool operator==(const FeaturePoint<Point,Features>& x, 
		const FeaturePoint<Point,Features>& y);

template<typename Point,
	 typename Features>
bool operator!=(const FeaturePoint<Point,Features>& x, 
		const FeaturePoint<Point,Features>& y);

/*! \class FeaturePoint
** \ingroup FeaturePoints MiscellaneousContainers
**  \brief The class defines a  of a feature point that is to say a point location and features associated to this location.
** 
** \author Benoit Tremblais
** \version 0.0.1
** \date 2016/10/13
** \since 1.5.0
** \param Point generic point location
** \param Features generic features class
*/
  template<typename Point,
	   typename Features>
  class FeaturePoint
  {
 public:    
    typedef FeaturePoint<Point,Features> self;
    typedef Point point_type;
    typedef Features features_type;

    /**
     ** \name Constructors & Destructors & operator=
     */
    /*@{*/
     /*!
     ** \brief Default constructor. Call default constructors of Point and Features.
     */
     FeaturePoint():
       p_(Point()),
       features_(Features())
   {}

    /*!
     ** \brief Constructs a feature points at location \a p and default features.
     ** \param p Point location.
     */
     FeaturePoint(const Point& p):
       p_(p),
       features_(Features())
   {}
 
     /*!
     ** \brief Constructs a feature points at location \a p and default features.
     ** \param p Point location.
     ** \param features Features of the feature point.
     */
    FeaturePoint(const Point& p,
		  const Features& features):
       p_(p),
       features_(features)
   {}
    /*
    ** \brief Destructor.
    */
    virtual ~FeaturePoint()
    {}
    /*@} End Constructors */

    /**
     ** \name Accessors/Mutators
     */
    /*@{*/
    
    const Point& get_point() const
    {
      return this->p_;
    }

    Point& get_point()
    {
      return this->p_;
    }

    void set_point(const Point& p) 
    {
      this->p_ = p;
    }

    const  Features& get_features() const
    {
      return this->features_;
    }

    Features& get_features() 
    {
      return this->features_;
    }

    void set_features(const Features& features) 
    {
      this->features_ = features;
    }
    /*@} End Accessors/Mutators */
  
     /**
     ** \name Input/output
     */
    /*@{*/
    /*
    ** \brief Write a %FeaturePoint to the ouput stream
    ** \param out output std::ostream
    ** \param a %FeaturePoint to write to the output stream
    */
    friend std::ostream& operator<< <>(std::ostream & out, 
				       const self& IP);

    /**
     ** \brief Appends location and features of a features points in an ASCII file.
     ** \param file_path_name file path name.
     */
     void write(const std::string& file_path_name)
    {
       std::ofstream output(file_path_name.c_str(),std::ios::app);
       try
	 {
	   if(!output)
	     {
	       throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	     }
	   int default_precision = output.precision();
	   output.setf(std::ios::scientific);
	   output.precision(default_precision);
	   //write data
	   output<<*this<<std::endl;
	 
	   output.precision(default_precision);
	   output.close();
	 }
       catch(std::exception& e)
	 {
	   std::cerr<<e.what()<<std::endl;
	   output.close();
	   exit(1);
	 }
    }
    /*@} End Input/output */
    
    /**
     ** \name Comparisons operators
     */
    /*@{*/
    /*!
    ** \brief Less than comparison operator
    ** \param x A %FeaturePoint
    ** \param y A %FeaturePoint
    ** \return true iff x.get_features() < y.get_features()
    */
    friend bool operator< <>(const self& x, 
			     const self& y);

    /*!
    ** \brief Greater than comparison operator
    ** \param x A %FeaturePoint
    ** \param y A %FeaturePoint
    ** \return true iff (y < x)
    */
    friend bool operator> <>(const self& x, 
			     const self& y);

     /*!
     ** \brief Less than equal comparison operator
     ** \param x A %FeaturePoint
     ** \param y A %FeaturePoint
     ** \return true iff !(y < x)
    */
    friend bool operator<= <>(const self& x, 
			      const self& y);
    /*!
    ** \brief Greater than equal comparison operator
    ** \param x A %FeaturePoint
    ** \param y A %FeaturePoint
    ** \return true iff !(x < y)
    */
    friend bool operator>= <>(const self& x, 
			      const self& y);

     /*!
     ** \brief Equality comparison operator
     ** \param x A %FeaturePoint
     ** \param y A %FeaturePoint
     ** \return true iff (x.get_point() == y.get_point()) && (x.get_features() == y.get_features())
    */
    friend bool operator== <>(const self& x, 
			      const self& y);
    /*!
     ** \brief Inequality comparison operator
     ** \param x A %FeaturePoint
     ** \param y A %FeaturePoint
     ** \return true iff !(x == y)
    */
    friend bool operator!= <>(const self& x, 
			      const self& y);

    /*@} End  Comparisons operators */
   

  protected:
    Point p_; /// point location
    Features features_; ///features
  };

}//::slip

namespace slip
{ 
  template <typename Point,
	    typename Features>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const FeaturePoint<Point,Features>& IP)
  {

      Point p = IP.get_point();
      for(std::size_t k = 0; k < Point::SIZE; ++k)
	{
	  out<<p[k]<<" ";
	}
      out<<IP.get_features();
      return out;
    }

template<typename Point,
	 typename Features>
inline
bool operator<(const FeaturePoint<Point,Features>& x, 
	       const FeaturePoint<Point,Features>& y)
{
  return x.get_features() < y.get_features();
}

template<typename Point,
	 typename Features>
inline
bool operator>(const FeaturePoint<Point,Features>& x, 
	       const FeaturePoint<Point,Features>& y)
{
  return (y < x);
}

template<typename Point,
	 typename Features>
inline
bool operator<=(const FeaturePoint<Point,Features>& x, 
	       const FeaturePoint<Point,Features>& y)
{
  return !(y < x);
}

template<typename Point,
	 typename Features>
inline
bool operator>=(const FeaturePoint<Point,Features>& x, 
	       const FeaturePoint<Point,Features>& y)
{
  return !(x < y);
}


template<typename Point,
	 typename Features>
inline
bool operator==(const FeaturePoint<Point,Features>& x, 
		const FeaturePoint<Point,Features>& y)
{
  return (  (x.p_ == y.p_)
	 && (x.features_ == y.features_)); 
}

template<typename Point,
	 typename Features>
inline
bool operator!=(const FeaturePoint<Point,Features>& x, 
		const FeaturePoint<Point,Features>& y)
{ 
  return !(x == y);
}

}



namespace slip
{
   template<typename Point,
	   typename Features>
 class FeaturePointList;

  template <typename Point,
	    typename Features>
  std::ostream& operator<<(std::ostream & out, 
			 const FeaturePointList<Point,Features>& IPL);

  /*! \class FeaturePointList
  ** \ingroup Containers Container1d FeaturePoints MiscellaneousContainers
  **  \brief The class defines a  of a feature point list.
  ** 
  ** \author Benoit Tremblais
  ** \version 0.0.1
  ** \date 2016/10/13
  ** \since 1.5.0
  ** \param Point generic point location
  ** \param Features generic features class
  */
  template<typename Point,
	   typename Features>
  class FeaturePointList : public std::vector<FeaturePoint<Point,Features> >
  {
 public:    
    typedef FeaturePointList<Point,Features> self;
    typedef std::vector<FeaturePoint<Point,Features> > base;
    /**
     ** \name Constructors & Destructors 
     */
    /*@{*/
     /*!
     ** \brief Default constructor. Call default constructors of std::vector<FeaturePoint<Point,Features> >
     */
     FeaturePointList():
       base()
   {}
    /*!
     ** \brief Copy constructor
     ** \param list Other FeaturePointList.
     */
    FeaturePointList(const self& list):
       base(list)
   {}

     /*!
     ** \brief Constructs a %FeaturePointList from input iterators
     ** \param first An input iterator.
     ** \param  last  An input iterator.
     */
    template <typename Iterator>
    FeaturePointList(Iterator first,
		     Iterator last):
      base(first,last)
    {
    }
    /*
    ** \brief Destructor.
    */
    virtual ~FeaturePointList()
    {}
    /*@} End Constructors */

     /**
     ** \name Input/output
     */
    /*@{*/
     /*
    ** \brief Write %FeaturePointList to an ASCII file.
    ** \param file_path_name File path name.
    ** The file header is beginning with #x y and continues with features descriptor. For example if features descriptor is "U V", the header will be
    ** #x y U V
    ** Then each point location coordinates and U V features of feature points will be written on each line of the file.
    */
    void write(const std::string& file_path_name)
    {
      std::ofstream output(file_path_name.c_str(),std::ios::out);
       try
	 {
	   if(!output)
	     {
	       throw std::ios_base::failure(slip::FILE_OPEN_ERROR + file_path_name);	  
	     }
	   if(this->size() != 0)
	     {
	       std::map<std::size_t, std::string> m{{1,"# x "}, {2, "# x y "}, {3, "# x y z "}, {4, "# x y z t"}};
	       Point p;
	       output<<m[p.dim()]<<(*this)[0].get_features().descriptor()<<std::endl;
	       //write data
	       for(std::size_t k = 0; k < this->size(); ++k)
		 {
		   output<<(*this)[k]<<std::endl;
		 }
	     }
	   output.close();
	 }
       catch(std::exception& e)
	 {
	   std::cerr<<e.what()<<std::endl;
	   output.close();
	   exit(1);
	 }
    }
      /*@} End Input/output */
  };
  
}
#endif //SLIP_FEATUREPOINT
