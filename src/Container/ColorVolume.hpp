/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/*!
 ** \file ColorVolume.hpp
 ** \brief Provides a class to manipulate Color volumes.
 ** \version Fluex 1.0
 ** \date 2013/03/18
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef SLIP_COLORVOLUME_HPP_
#define SLIP_COLORVOLUME_HPP_


//#include <iostream>
//#include <iterator>
//#include <cassert>
//#include <numeric>
//#include <cstddef>
//#include <cmath>
//#include <string>
//
#include "Color.hpp"
//#include "kstride_iterator.hpp"
#include "GenericMultiComponent3d.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/base_object.hpp>


namespace slip {

template <typename T>
class ColorVolume;
/*!
 *  @defgroup Fluex Fluex project
 *  @brief Codes developped by Inria <a href="http://www.inria.fr/equipes/geostat">GeoStat</a> during FLUEX project integrated in SLIP in november 2013
 *  @{
 */
/*!
 ** @ingroup Containers Containers3d MultiComponent3dContainers Fluex
 ** \class ColorVolume
 ** \version Fluex 1.0
 ** \date 2013/03/18
 ** \version 0.0.2
 ** \date 2014/04/05
 ** \since 1.4.0
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief This is a color volume class.
 ** This container defines STL Bidirectionnal iterators begin and end.
 ** It defines also 3d extensions of the RandomAccess Iterators called front_upper_left
 **  and back_bottom_right(). As a consequence,
 ** the bracket element access is replaced by the triple bracket element access.
 ** It is a specialization of GenericMulitComponent3d using Color blocks.
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \version Fluex 1.0
 ** \date 2013/03/20
 ** \param T Type of object in the ColorVolume
 ** \par Axis conventions:
 ** \image html iterator3d_conventions.jpg "axis and notation conventions"
 ** \image latex iterator3d_conventions.eps "axis and notation conventions" width=5cm
 */

template <typename T>
class ColorVolume:public slip::GenericMultiComponent3d<slip::Color<T> >
{
public :

	typedef slip::Color<T> value_type;
	typedef ColorVolume<T> self;
	typedef slip::GenericMultiComponent3d<slip::Color<T> > base;

	typedef value_type* pointer;
	typedef const value_type* const_pointer;
	typedef value_type& reference;
	typedef const value_type& const_reference;

	typedef T color_value_type;
	typedef color_value_type* color_pointer;
	typedef const color_value_type* const_color_pointer;
	typedef color_value_type& color_reference;
	typedef const color_value_type const_color_reference;
	typedef slip::kstride_iterator<color_pointer,3> color_iterator;
	typedef slip::kstride_iterator<const_color_pointer,3> const_color_iterator;

	typedef ptrdiff_t difference_type;
	typedef std::size_t size_type;

	static const std::size_t DIM = 3;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** \brief Constructs a %ColorVolume.
	 */
	ColorVolume():
		base()
	{}

	/*!
	 ** \brief Constructs a %ColorVolume.
	 ** \param slices first dimension of the %ColorVolume
	 ** \param rows second dimension of the %ColorVolume
	 ** \param cols third dimension of the %ColorVolume
	 ** \pre if slice is
	 */
	ColorVolume(const size_type slices, const size_type rows, const size_type cols):
		base(slices,rows,cols)
	{}

	/*!
	 ** \brief Constructs a %ColorVolume initialized by the scalar value \a val.
	 ** \param slices first dimension of the %ColorVolume
	 ** \param rows second dimension of the %ColorVolume
	 ** \param cols third dimension of the %ColorVolume
	 ** \param val initialization value of the elements
	 */
	ColorVolume(const size_type slices, const size_type rows, const size_type cols,
			const slip::Color<T>& val):
				base(slices,rows,cols,val)
	{}

	/*!
	 ** \brief Constructs a %ColorVolume initialized by an array \a val.
	 ** \param slices first dimension of the %ColorVolume
	 ** \param rows second dimension of the %ColorVolume
	 ** \param cols third dimension of the %ColorVolume
	 ** \param val initialization linear array value of the elements
	 */
	ColorVolume(const size_type slices, const size_type rows, const size_type cols,
			const T* val):
				base(slices,rows,cols,val)
	{}

	/*!
	 ** \brief Constructs a %ColorVolume initialized by an array \a val.
	 ** \param slices first dimension of the %ColorVolume
	 ** \param rows second dimension of the %ColorVolume
	 ** \param cols third dimension of the %ColorVolume
	 ** \param val initialization array value of the elements
	 */
	ColorVolume(const size_type slices, const size_type rows, const size_type cols,
			const slip::Color<T>* val):
				base(slices,rows,cols,val)
	{}


	/**
	 ** \brief  Contructs a %ColorVolume from a range.
	 ** \param slices first dimension of the %ColorVolume
	 ** \param rows second dimension of the %ColorVolume
	 ** \param cols third dimension of the %ColorVolume
	 ** \param  first  An input iterator.
	 ** \param  last  An input iterator.
	 **
	 ** Create a %ColorVolume consisting of copies of the elements from
	 ** [first,last).
	 */
	template<typename InputIterator>
	ColorVolume(const size_type slices, const size_type rows, const size_type cols,
			InputIterator first,
			InputIterator last):
			base(slices,rows,cols,first,last)
			{}

	/**
	 ** \brief  Contructs a %ColorVolume from a 3 ranges.
	 ** \param slices first dimension of the %ColorVolume
	 ** \param rows second dimension of the %ColorVolume
	 ** \param cols third dimension of the %ColorVolume
	 ** \param  first1  An input iterator.
	 ** \param  last1  An input iterator.
	 ** \param  first2  An input iterator.
	 ** \param  first3  An input iterator.
	 **
	 ** Create a %ColorVolume consisting of copies of the elements from
	 ** [first1,last1),
	 ** [first2,first2 + (last1 - first1)),
	 ** [first3, first3 + (last1 - first1).
	 */
	template<typename InputIterator>
	ColorVolume(const size_type slices, const size_type rows, const size_type cols,
			InputIterator first1,
			InputIterator last1,
			InputIterator first2,
			InputIterator first3):
			base(slices,rows,cols)
			{

		std::vector<InputIterator> first_iterators_list(3);
		first_iterators_list[0] = first1;
		first_iterators_list[1] = first2;
		first_iterators_list[2] = first3;
		this->fill(first_iterators_list,last1);

			}


	/*!
	 ** \brief Constructs a copy of the %ColorVolume \a rhs
	 */
	ColorVolume(const self& rhs):
		base(rhs)
	{}

	/*!
	 ** \brief Destructor of the %ColorVolume
	 */
	~ColorVolume()
	{}


	/*@} End Constructors */

	/*!
	 ** \brief Returns the name of the class
	 **
	 */
	std::string name() const;

	/**
	 ** \name  Assignment operators and methods
	 */
	/*@{*/

	/*!
	 ** \brief Affects all the element of the %ColorVolume by val
	 ** \param val affectation value
	 ** \return reference to corresponding %ColorVolume
	 */
	self& operator=(const slip::Color<T>& val);


	/*!
	 ** \brief Affects all the element of the %ColorVolume by val
	 ** \param val affectation value
	 ** \return reference to corresponding %ColorVolume
	 */
	self& operator=(const T& val);


	/*@} End Assignment operators and methods*/

	/**
	 ** \name  Arithmetic operators
	 */
	/*@{*/


	/*!
	 ** \brief Add val to each element of the %ColorVolume
	 ** \param val value
	 ** \return reference to the resulting %ColorVolume
	 */
	self& operator+=(const T& val);
	self& operator-=(const T& val);
	self& operator*=(const T& val);
	self& operator/=(const T& val);



	self  operator-() const;

	/*!
	 ** \brief Add val to each element of the %ColorVolume
	 ** \param val value
	 ** \return reference to the resulting %ColorVolume
	 */
	self& operator+=(const slip::Color<T>& val);
	self& operator-=(const slip::Color<T>& val);
	self& operator*=(const slip::Color<T>& val);
	self& operator/=(const slip::Color<T>& val);


	self& operator+=(const self& rhs);
	self& operator-=(const self& rhs);
	self& operator*=(const self& rhs);
	self& operator/=(const self& rhs);


	/*@} End Arithmetic operators */
private :
   friend class boost::serialization::access;
 template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::GenericMultiComponent3d<slip::Color<T> > >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::GenericMultiComponent3d<slip::Color<T> > >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
};

///double alias
typedef slip::ColorVolume<double> ColorVolume_d;
///float alias
typedef slip::ColorVolume<float> ColorVolume_f;
///long alias
typedef slip::ColorVolume<long> ColorVolume_l;
///unsigned long alias
typedef slip::ColorVolume<unsigned long> ColorVolume_ul;
///short alias
typedef slip::ColorVolume<short> ColorVolume_s;
///unsigned long alias
typedef slip::ColorVolume<unsigned short> ColorVolume_us;
///int alias
typedef slip::ColorVolume<int> ColorVolume_i;
///unsigned int alias
typedef slip::ColorVolume<unsigned int> ColorVolume_ui;
///char alias
typedef slip::ColorVolume<char> ColorVolume_c;
///unsigned char alias
typedef slip::ColorVolume<unsigned char> ColorVolume_uc;

/*!
 ** \brief pointwise addition of two %ColorVolume
 ** \param M1 first %ColorVolume
 ** \param M2 second %ColorVolume
 ** \pre M1.dim1() == M2.dim1()
 ** \pre M1.dim2() == M2.dim2()
 ** \pre M1.dim3() == M2.dim3()
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator+(const ColorVolume<T>& M1,
		const ColorVolume<T>& M2);

/*!
 ** \brief addition of a scalar to each element of a %ColorVolume
 ** \param M1 the %ColorVolume
 ** \param val the scalar
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator+(const ColorVolume<T>& M1,
		const T& val);

/*!
 ** \brief addition of a scalar to each element of a %ColorVolume
 ** \param val the scalar
 ** \param M1 the %ColorVolume
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator+(const T& val,
		const ColorVolume<T>& M1);

/*!
 ** \brief addition of a %Color block to each element of a %ColorVolume
 ** \param M1 the %ColorVolume
 ** \param val the %Color block
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator+(const ColorVolume<T>& M1,
		const slip::Color<T>& val);

/*!
 ** \brief addition of a %Color block to each element of a %ColorVolume
 ** \param val the %Color block
 ** \param M1 the %ColorVolume
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator+(const slip::Color<T>& val,
		const ColorVolume<T>& M1);

/*!
 ** \brief pointwise subtraction of two %ColorVolume
 ** \param M1 first %ColorVolume
 ** \param M2 second %ColorVolume
 ** \pre M1.dim1() == M2.dim1()
 ** \pre M1.dim2() == M2.dim2()
 ** \pre M1.dim3() == M2.dim3()
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator-(const ColorVolume<T>& M1,
		const ColorVolume<T>& M2);

/*!
 ** \brief subtraction of a scalar to each element of a %ColorVolume
 ** \param M1 the %ColorVolume
 ** \param val the scalar
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator-(const ColorVolume<T>& M1,
		const T& val);

/*!
 ** \brief subtraction of a scalar to each element of a %ColorVolume
 ** \param val the scalar
 ** \param M1 the %ColorVolume
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator-(const T& val,
		const ColorVolume<T>& M1);

/*!
 ** \brief subtraction of a %Color block to each element of a %ColorVolume
 ** \param M1 the %ColorVolume
 ** \param val the %Color block
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator-(const ColorVolume<T>& M1,
		const slip::Color<T>& val);

/*!
 ** \brief pointwise multiplication of two %ColorVolume
 ** \param M1 first %ColorVolume
 ** \param M2 second %ColorVolume
 ** \pre M1.dim1() == M2.dim1()
 ** \pre M1.dim2() == M2.dim2()
 ** \pre M1.dim3() == M2.dim3()
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator*(const ColorVolume<T>& M1,
		const ColorVolume<T>& M2);

/*!
 ** \brief multiplication of a scalar to each element of a %ColorVolume
 ** \param M1 the %ColorVolume
 ** \param val the scalar
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator*(const ColorVolume<T>& M1,
		const T& val);

/*!
 ** \brief multiplication of a scalar to each element of a %ColorVolume
 ** \param val the scalar
 ** \param M1 the %ColorVolume
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator*(const T& val,
		const ColorVolume<T>& M1);

/*!
 ** \brief multiplication of a %Color block to each element of a %ColorVolume
 ** \param M1 the %ColorVolume
 ** \param val the %Color block
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator*(const ColorVolume<T>& M1,
		const slip::Color<T>& val);

/*!
 ** \brief multiplication of a %Color block to each element of a %ColorVolume
 ** \param val the %Color block
 ** \param M1 the %ColorVolume
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator*(const slip::Color<T>& val,
		const ColorVolume<T>& M1);

/*!
 ** \brief pointwise division of two %ColorVolume
 ** \param M1 first %ColorVolume
 ** \param M2 seconf %ColorVolume
 ** \pre M1.dim1() == M2.dim1()
 ** \pre M1.dim2() == M2.dim2()
 ** \pre M1.dim3() == M2.dim3()
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator/(const ColorVolume<T>& M1,
		const ColorVolume<T>& M2);

/*!
 ** \brief division of a scalar to each element of a %ColorVolume
 ** \param M1 the %ColorVolume
 ** \param val the scalar
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator/(const ColorVolume<T>& M1,
		const T& val);

/*!
 ** \brief division of each element of a %ColorVolume by a %Color block
 ** \param M1 the %ColorVolume
 ** \param val the %Color block
 ** \return resulting %ColorVolume
 */
template<typename T>
ColorVolume<T> operator/(const ColorVolume<T>& M1,
		const slip::Color<T>& val);

}  // namespace slip

namespace slip {

template<typename T>
inline
std::string ColorVolume<T>::name() const{
	return "ColorVolume";
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator=(const slip::Color<T>& val){
	if(this->size() > 0)
		std::fill_n(this->begin(),this->size(),val);
	return *this;
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator=(const T& val){
	if(this->size() > 0)
		std::fill_n(this->begin(),this->size(),val);
	return *this;
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator+=(const T& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::plus<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator-=(const T& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::minus<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator*=(const T& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::multiplies<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator/=(const T& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::divides<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorVolume<T> ColorVolume<T>::operator-() const{
	ColorVolume<T> tmp(*this);
	std::transform(this->begin(),this->end(),tmp.begin(),std::negate<slip::Color<T> >());
	return tmp;
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator+=(const slip::Color<T>& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::plus<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator-=(const slip::Color<T>& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::minus<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator*=(const slip::Color<T>& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::multiplies<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator/=(const slip::Color<T>& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::divides<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator+=(const ColorVolume<T>& rhs){
	assert(this->dim1() == rhs.dim1());
	assert(this->dim2() == rhs.dim2());
	assert(this->dim3() == rhs.dim3());
	std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::plus<slip::Color<T> >());
	return *this;
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator-=(const ColorVolume<T>& rhs){
	assert(this->dim1() == rhs.dim1());
	assert(this->dim2() == rhs.dim2());
	assert(this->dim3() == rhs.dim3());
	std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::minus<slip::Color<T> >());
	return *this;
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator*=(const ColorVolume<T>& rhs){
	assert(this->dim1() == rhs.dim1());
	assert(this->dim2() == rhs.dim2());
	assert(this->dim3() == rhs.dim3());
	std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::multiplies<slip::Color<T> >());
	return *this;
}

template<typename T>
inline
ColorVolume<T>& ColorVolume<T>::operator/=(const ColorVolume<T>& rhs){
	assert(this->dim1() == rhs.dim1());
	assert(this->dim2() == rhs.dim2());
	assert(this->dim3() == rhs.dim3());
	std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::divides<slip::Color<T> >());
	return *this;
}

template<typename T>
inline
ColorVolume<T> operator+(const ColorVolume<T>& M1,
		const ColorVolume<T>& M2){
	assert(M1.dim1() == M2.dim1());
	assert(M1.dim2() == M2.dim2());
	assert(M1.dim3() == M2.dim3());
	ColorVolume<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
	std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::plus<slip::Color<T> >());
	return tmp;
}

template<typename T>
inline
ColorVolume<T> operator+(const ColorVolume<T>& M1,
		const T& val){
	ColorVolume<T> tmp = M1;
	tmp += val;
	return tmp;
}

template<typename T>
inline
ColorVolume<T> operator+(const T& val,
		const ColorVolume<T>& M1){
	return M1 + val;
}

template<typename T>
inline
ColorVolume<T> operator+(const ColorVolume<T>& M1,
		const slip::Color<T>& val){
	ColorVolume<T>  tmp = M1;
	tmp += val;
	return tmp;
}

template<typename T>
inline
ColorVolume<T> operator+(const slip::Color<T>& val,
		const ColorVolume<T>& M1){
	return M1 + val;
}

template<typename T>
inline
ColorVolume<T> operator-(const ColorVolume<T>& M1,
		const ColorVolume<T>& M2){
	assert(M1.dim1() == M2.dim1());
	assert(M1.dim2() == M2.dim2());
	assert(M1.dim3() == M2.dim3());
	ColorVolume<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
	std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::minus<slip::Color<T> >());
	return tmp;
}

template<typename T>
inline
ColorVolume<T> operator-(const ColorVolume<T>& M1,
		const T& val){
	ColorVolume<T>  tmp = M1;
	tmp -= val;
	return tmp;
}

template<typename T>
inline
ColorVolume<T> operator-(const T& val,
		const ColorVolume<T>& M1){
	return -(M1 - val);
}

template<typename T>
inline
ColorVolume<T> operator-(const ColorVolume<T>& M1,
		const slip::Color<T>& val){
	ColorVolume<T> tmp = M1;
	tmp -= val;
	return tmp;
}

template<typename T>
inline
ColorVolume<T> operator*(const ColorVolume<T>& M1,
		const ColorVolume<T>& M2){
	assert(M1.dim1() == M2.dim1());
	assert(M1.dim2() == M2.dim2());
	assert(M1.dim3() == M2.dim3());
	ColorVolume<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
	std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::multiplies<slip::Color<T> >());
	return tmp;
}

template<typename T>
inline
ColorVolume<T> operator*(const ColorVolume<T>& M1,
		const T& val){
	ColorVolume<T>  tmp = M1;
	tmp *= val;
	return tmp;
}

template<typename T>
inline
ColorVolume<T> operator*(const T& val,
		const ColorVolume<T>& M1){
	return M1 * val;
}

template<typename T>
inline
ColorVolume<T> operator*(const ColorVolume<T>& M1,
		const slip::Color<T>& val){
	ColorVolume<T>  tmp = M1;
	tmp *= val;
	return tmp;
}

template<typename T>
inline
ColorVolume<T> operator*(const slip::Color<T>& val,
		const ColorVolume<T>& M1){
	return M1 * val;
}

template<typename T>
inline
ColorVolume<T> operator/(const ColorVolume<T>& M1,
		const ColorVolume<T>& M2){
	assert(M1.dim1() == M2.dim1());
	assert(M1.dim2() == M2.dim2());
	assert(M1.dim3() == M2.dim3());
	ColorVolume<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
	std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::divides<slip::Color<T> >());
	return tmp;
}

template<typename T>
inline
ColorVolume<T> operator/(const ColorVolume<T>& M1,
		const T& val){
	ColorVolume<T>  tmp = M1;
	tmp /= val;
	return tmp;
}

template<typename T>
inline
ColorVolume<T> operator/(const ColorVolume<T>& M1,
		const slip::Color<T>& val){
	ColorVolume<T>  tmp = M1;
	tmp /= val;
	return tmp;
}

}  // namespace slip

#endif /* SLIP_COLORVOLUME_HPP_ */
