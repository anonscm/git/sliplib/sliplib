#ifndef SLIP_SVG_SHAPES_HPP_
#define SLIP_SVG_SHAPES_HPP_

//#include "utils_color.hpp"
//#include "numeric_trait.hpp"
//#include "functors.hpp" //for elliptic arc and RosinPoint

#include "macros.hpp"
#include "Point2d.hpp"
#include "Color.hpp"
#include <string>
#include <iostream>

namespace slip
{


  /****************************************************************************************/
  /*                                                                                      */
  /*                                     CIRCLES                                          */
  /*                                                                                      */
  /****************************************************************************************/

  /**
   ** @brief Class holding a circle that can be output a SVG file
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @tparam Real internal representation of components of the circle
   **/
  template< typename Real >
  struct SVGCircle
  {
  public:

    /**
     ** @brief Constructor
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param cx X-coordinate of the center of the circle
     ** @param cy Y-coordinate of the center of the circle
     ** @param radius Radius of the circle
     ** @param col color of the circle
     **/
    SVGCircle( const Real cx , const Real cy , const Real radius , const slip::Color<unsigned char> & col = slip::Color<unsigned char>(0,0,0)) ;


    /**
     ** @brief Output circle in SVG format
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param out Stream in which circle will be written
     ** @param cir Circle to output in the stream
     ** @return Stream after write operation
     **/
    template <typename Real1>
    friend std::ostream & operator<<( std::ostream & out , const SVGCircle<Real1> & cir  ) ;

  private:
    Real _cx ; // Center X coord
    Real _cy ; // Center Y coord
    Real _rad ; // Radius

    slip::Color<unsigned char> _col ; // Color
  } ;


  /**
   ** @brief Constructor
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param cx X-coordinate of the center of the circle
   ** @param cy Y-coordinate of the center of the circle
   ** @param radius Radius of the circle
   ** @param col color of the circle
   **/
  template< typename Real >
  SVGCircle<Real>::SVGCircle( const Real cx , const Real cy , const Real radius , const slip::Color<unsigned char> & col )
    : _cx( cx ) ,
      _cy( cy ) ,
      _rad( radius ) ,
      _col( col )
  {

  }

  /**
   ** @brief Output circle in SVG format
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param out Stream in which circle will be written
   ** @param cir Circle to output in the stream
   ** @return Stream after write operation
   **/
  template< typename Real >
  std::ostream & operator<<( std::ostream & out , const SVGCircle<Real> & cir )
  {
    out << "<circle cx=\"" << cir._cx << "\" cy=\"" << cir._cy << "\" r=\"" << cir._rad << "\" fill=\"none\" stroke=\"black\" stroke-width=\"1\" />" ;
    return out ;
  }




  /****************************************************************************************/
  /*                                                                                      */
  /*                                     LINES                                            */
  /*                                                                                      */
  /****************************************************************************************/
  /**
   ** @brief Class holding a line that can be output a SVG file
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @tparam Real internal representation of components of the line
   **/
  template< typename Real >
  struct SVGLine
  {
  public:

    /**
     ** @brief Constructor
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param x1 X coordinate of the first point of the line
     ** @param y1 Y coordinate of the first point of the line
     ** @param x2 X coordinate of the second point of the line
     ** @param y2 Y coordinate of the second point of the line
     ** @param width Width of the line
     ** @param col Color of the line
     **/
    SVGLine( const Real x1 , const Real y1 , const Real x2 , const Real y2 , const Real width = static_cast<Real>( 1 ) , const slip::Color<unsigned char> & col = slip::Color<unsigned char>(0,0,0)) ;


    /**
     ** @brief Output line in SVG format
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param out Stream in which line will be written
     ** @param line Line to output in the stream
     ** @return Stream after write operation
     **/
    template <typename Real1>
    friend std::ostream & operator<<( std::ostream & out , const SVGLine<Real1> & line ) ;

  private:

    // First point
    Real _x1 ;
    Real _y1 ;

    // Second point
    Real _x2 ;
    Real _y2 ;

    // Width
    Real _width ;

    // Color
    slip::Color<unsigned char> _col ;
  } ;


  /**
   ** @brief Constructor
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param x1 X coordinate of the first point of the line
   ** @param y1 Y coordinate of the first point of the line
   ** @param x2 X coordinate of the second point of the line
   ** @param y2 Y coordinate of the second point of the line
   ** @param width Width of the line
   ** @param col Color of the line
   **/
  template< typename Real >
  SVGLine<Real>::SVGLine( const Real x1 , const Real y1 , const Real x2 , const Real y2 , const Real width , const slip::Color<unsigned char> & col )
    : _x1( x1 ) ,
      _y1( y1 ) ,
      _x2( x2 ) ,
      _y2( y2 ) ,
      _width( width ) ,
      _col( col )
  {

  }


  /**
   ** @brief Output line in SVG format
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param out Stream in which line will be written
   ** @param line Line to output in the stream
   ** @return Stream after write operation
   **/
  template< typename Real >
  std::ostream & operator<<( std::ostream & out , const SVGLine<Real> & line )
  {
    const int r = static_cast<int>( line._col.get_red() ) ;
    const int g = static_cast<int>( line._col.get_green() ) ;
    const int b = static_cast<int>( line._col.get_blue() ) ;

    out << "<line x1=\"" << line._x1 << "\" y1=\"" << line._y1 << "\" x2=\"" << line._x2 << "\" y2=\"" << line._y2 << "\" stroke=\"rgb(" << r << "," << g << "," << b << ")\" stroke-width=\"" << line._width << "\" />" ;
    return out ;
  }




  /****************************************************************************************/
  /*                                                                                      */
  /*                               AXIS-ALIGNED RECTANGLE                                 */
  /*                                                                                      */
  /****************************************************************************************/
  /**
   ** @brief Class holding an axis-aligned rectangle that can be output a SVG file
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @tparam Real internal representation of components of the rectangle
   **/
  template< typename Real >
  struct SVGRect
  {
  public:

    /**
     ** @brief Constructor
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param x X-coordinate of the basis point of the rectangle
     ** @param y Y-coordinate of the basis point of the rectangle
     ** @param width Width of the rectangle
     ** @param height Height of the rectangle
     ** @param col Color of the rectangle
     **/
    SVGRect( const Real x , const Real y , const Real width , const Real height , const slip::Color<unsigned char> & col = slip::Color<unsigned char>(0,0,0)) ;


    /**
     ** @brief Output rectangle in SVG format
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param out Stream in which rectangle will be written
     ** @param rec Rectangle to output in the stream
     ** @return Stream after write operation
     **/
    template <typename Real1>
    friend std::ostream & operator<<( std::ostream & out , const SVGRect<Real1> & rec ) ;

  private:

    Real _x ;
    Real _y ;
    Real _width ;
    Real _height ;

    slip::Color<unsigned char> _col ;
  } ;

  /**
   ** @brief Constructor
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param x X-coordinate of the basis point of the rectangle
   ** @param y Y-coordinate of the basis point of the rectangle
   ** @param width Width of the rectangle
   ** @param height Height of the rectangle
   ** @param col Color of the rectangle
   **/
  template< typename Real >
  SVGRect<Real>::SVGRect( const Real x , const Real y , const Real width , const Real height , const slip::Color<unsigned char> & col )
    : _x( x ) ,
      _y( y ) ,
      _width( width ) ,
      _height( height ) ,
      _col( col )
  {

  }


  /**
   ** @brief Output rectangle in SVG format
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param out Stream in which rectangle will be written
   ** @param rec Rectangle to output in the stream
   ** @return Stream after write operation
   **/
  template< typename Real >
  std::ostream & operator<<( std::ostream & out , const SVGRect<Real> & rec )
  {
    out << "<rect x=\"" << rec._x << "\" y=\"" << rec._y << "\" width=\"" << rec._width << "\" height=\"" << rec._height << "\" fill=\"none\" stroke=\"black\" stroke-width=\"1\" />" ;
    return out ;
  }


  /****************************************************************************************/
  /*                                                                                      */
  /*                                     FULL ELLIPSE                                     */
  /*                                                                                      */
  /****************************************************************************************/
  /**
   ** @brief Class holding an ellipse that can be output a SVG file
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @tparam Real internal representation of components of the ellipse
   **/
  template< typename Real >
  struct SVGEllipse
  {
  public:

    /**
     ** @brief constructor
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param cx X coordinate of the center of the ellipse
     ** @param cy Y coordinate of the center of the ellipse
     ** @param ax Major axis dimension
     ** @param ay Minor axis dimension
     ** @param orient Orientation relative to the X axis of the ellipse
     ** @param col Color of the ellipse
     **/
    SVGEllipse( const Real cx , const Real cy , const Real ax , const Real ay , const Real orient , const slip::Color<unsigned char> & col = slip::Color<unsigned char>(0,0,0)) ;


    /**
     ** @brief Output ellipse in SVG format
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param out Stream in which ellipse will be written
     ** @param ell Ellipse to output in the stream
     ** @return Stream after write operation
     **/
    template <typename Real1>
    friend std::ostream & operator<<( std::ostream & out , const SVGEllipse< Real1 > & ell ) ;

  private:

    // Center
    Real _cx ;
    Real _cy ;

    // Main axis (Major/minor)
    Real _ax ;
    Real _ay ;

    Real _orient ;

    slip::Color<unsigned char> _col ;
  } ;


  /**
   ** @brief constructor
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param cx X coordinate of the center of the ellipse
   ** @param cy Y coordinate of the center of the ellipse
   ** @param ax Major axis dimension
   ** @param ay Minor axis dimension
   ** @param orient Orientation relative to the X axis of the ellipse
   ** @param col Color of the ellipse
   **/
  template< typename Real >
  SVGEllipse<Real>::SVGEllipse( const Real cx , const Real cy , const Real ax , const Real ay , const Real orient , const slip::Color<unsigned char> & col )
    : _cx( cx ) ,
      _cy( cy ) ,
      _ax( ax ) ,
      _ay( ay ) ,
      _orient( orient ) ,
      _col( col )
  {

  }

  /**
   ** @brief Output ellipse in SVG format
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param out Stream in which ellipse will be written
   ** @param ell Ellipse to output in the stream
   ** @return Stream after write operation
   **/
  template< typename Real >
  std::ostream & operator<<( std::ostream & out , const SVGEllipse< Real > & ell )
  {
    // Draw a full ellipse
    const int r = static_cast<int>( ell._col.get_red() ) ;
    const int g = static_cast<int>( ell._col.get_green() ) ;
    const int b = static_cast<int>( ell._col.get_blue() ) ;

    const Real angle_deg = ell._orient * static_cast<Real>( 180 ) / slip::constants<Real>::pi() ;
    out << "<ellipse transform=\"translate(" << ell._cx << " " << ell._cy << ") rotate(" << angle_deg << ")\" rx=\""
        << ell._ax << "\" ry=\"" << ell._ay << "\" stroke=\"rgb(" << r << "," << g << "," << b << ")\" fill=\"none\" stroke-width=\"1\" />" ;
    return out ;
  }



  /****************************************************************************************/
  /*                                                                                      */
  /*                                    CIRCULAR ARC                                      */
  /*                                                                                      */
  /****************************************************************************************/
  /**
   ** @brief Class holding a circular arc that can be output a SVG file
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @tparam Real internal representation of components of the circular arc
   **/
  template <typename Real>
  struct SVGCircularArc
  {
  public:

    /**
     ** @brief Constructor
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param cx X coordinate of the center of the base circle
     ** @param cy Y coordinate of the center of the base circle
     ** @param radius Radius of the base circle
     ** @param angle_start lower bound angle on the circular arc
     ** @param angle_end higher bound angle on the circular arc
     ** @param ext1_x X coordinate of the first point of the circular arc
     ** @param ext1_y Y coordinate of the first point of the circular arc
     ** @param ext2_x X coordinate of the end point of the circular arc
     ** @param ext2_y Y coordinate of the end point of the circular arc
     ** @param col Color of the circular arc
     **/
    SVGCircularArc( const Real cx , const Real cy , const Real radius ,
                    const Real angle_start , const Real angle_end ,
                    const Real ext1_x , const Real ext1_y , const Real ext2_x , const Real ext2_y ,
                    const slip::Color<unsigned char> & col = slip::Color<unsigned char>(0,0,0) ) ;

    /**
     ** @brief Output circular arc in SVG format
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param out Stream in which the circular arc will be written
     ** @param ell Circular arc to output in the stream
     ** @return Stream after write operation
     **/
    template <typename Real1>
    friend std::ostream & operator<<( std::ostream & out , const SVGCircularArc< Real1 >&  ell ) ;


  private:

    Real _cx ;
    Real _cy ;

    Real _radius ;

    Real _angle_start ;
    Real _angle_end ;


    Real _ext1_x ;
    Real _ext1_y ;
    Real _ext2_x ;
    Real _ext2_y ;

    slip::Color<unsigned char> _col ;
  } ;


  /**
   ** @brief Constructor
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param cx X coordinate of the center of the base circle
   ** @param cy Y coordinate of the center of the base circle
   ** @param radius Radius of the base circle
   ** @param angle_start lower bound angle on the circular arc
   ** @param angle_end higher bound angle on the circular arc
   ** @param ext1_x X coordinate of the first point of the circular arc
   ** @param ext1_y Y coordinate of the first point of the circular arc
   ** @param ext2_x X coordinate of the end point of the circular arc
   ** @param ext2_y Y coordinate of the end point of the circular arc
   ** @param col Color of the circular arc
   **/
  template< typename Real>
  SVGCircularArc<Real>::SVGCircularArc( const Real cx , const Real cy , const Real radius ,
                                        const Real angle_start , const Real angle_end ,
                                        const Real ext1_x , const Real ext1_y , const Real ext2_x , const Real ext2_y ,
                                        const slip::Color<unsigned char> & col )
    : _cx( cx ) ,
      _cy( cy ) ,
      _radius( radius ) ,
      _angle_start( angle_start ) ,
      _angle_end( angle_end ) ,
      _ext1_x( ext1_x ) ,
      _ext1_y( ext1_y ) ,
      _ext2_x( ext2_x ) ,
      _ext2_y( ext2_y ) ,
      _col( col )
  {

  }

  /**
   ** @brief Output circular arc in SVG format
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param out Stream in which the circular arc will be written
   ** @param ell Circular arc to output in the stream
   ** @return Stream after write operation
   **/
  template <typename Real>
  std::ostream & operator<<( std::ostream & out , const SVGCircularArc< Real >&  ell )
  {
    // const Real _cx = ell._cx ;
    // const Real _cy = ell._cy ;
    const Real _radius = ell._radius ;
    const Real x1 = ell._ext1_x ;
    const Real y1 = ell._ext1_y ;
    const Real x2 = ell._ext2_x ;
    const Real y2 = ell._ext2_y ;

    const int r = static_cast<int>( ell._col.get_red() ) ;
    const int g = static_cast<int>( ell._col.get_green() ) ;
    const int b = static_cast<int>( ell._col.get_blue() ) ;


    // Compute extreme angles
    Real angle_start = ell._angle_start ;
    Real angle_end   = ell._angle_end ;

    // Make them between [0;2pi]
    if( angle_start < 0 )
    {
      angle_start += slip::constants<Real>::pi() * slip::constants<Real>::two() ;
    }
    if( angle_end < 0 )
    {
      angle_end += slip::constants<Real>::pi() * slip::constants<Real>::two() ;
    }

    if( angle_start > angle_end )
    {
      angle_end += slip::constants<Real>::pi() * slip::constants<Real>::two() ;
    }

    // Decide with elliptical arc will be drawn
    int fa = 0 ;
    int fs = 1 ;
    if( ( angle_end - angle_start ) > slip::constants<Real>::pi() )
    {
      fa = 1 ;
    }


    out << "<path d=\"M " << x1 << "," << y1 << " A" << _radius << "," << _radius << " " << 0.0 << " " << fa << "," << fs << " " << x2 << "," << y2
        << "\" fill=\"none\" stroke=\"rgb(" << r << "," << g << "," << b << ")\" stroke-width=\"1\" />" ;

    return out ;
  }



  // /****************************************************************************************/
  // /*                                                                                      */
  // /*                                  ELLIPTICAL ARC                                      */
  // /*                                                                                      */
  // /****************************************************************************************/
  // /**
  //   ** @brief Class holding an elliptical arc that can be output a SVG file
  //   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
  //   ** @date 2014/07/03
  //   ** @version 0.0.1
  //   ** @tparam Real internal representation of components of the elliptical arc
  //   **/
  // template <typename Real>
  // struct SVGEllipticalArc
  // {
  // public:

  //   /**
  //    ** @brief Constructor
  //    ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
  //    ** @date 2014/07/03
  //    ** @version 0.0.1
  //    ** @param cx X coordinate of the center of the base ellipse
  //    ** @param cy Y coordinate of the center of the base ellipse
  //    ** @param ax Major axis dimension
  //    ** @param ay Minor axis dimension
  //    ** @param orient Orientation of the base ellipse relative to the major axis (and the X axis)
  //    ** @param ext1_x X coordinate of the first point of the elliptical arc
  //    ** @param ext1_y Y coordinate of the first point of the elliptical arc
  //    ** @param ext2_x X coordinate of the last point of the elliptical arc
  //    ** @param ext2_y Y coordinate of the last point of the elliptical arc
  //    ** @param col Color of the elliptical arc
  //    **/
  //   SVGEllipticalArc( const Real cx , const Real cy , const Real ax , const Real ay , const Real orient ,
  //                     const Real ext1_x , const Real ext1_y , const Real ext2_x , const Real ext2_y ,
  //                     const slip::Color<unsigned char> & col = slip::Color<unsigned char>(0,0,0) ) ;


  //   /**
  //    ** @brief Output elliptical arc in SVG format
  //    ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
  //    ** @date 2014/07/03
  //    ** @version 0.0.1
  //    ** @param out Stream in which the elliptical arc will be written
  //    ** @param ell Elliptical arc to output in the stream
  //    ** @return Stream after write operation
  //    **/
  //   template <typename Real1>
  //   friend std::ostream & operator<<( std::ostream & out , const SVGEllipticalArc< Real1 >&  ell ) ;


  // private:

  //   Real _cx ;
  //   Real _cy ;

  //   Real _ax ;
  //   Real _ay ;

  //   Real _orient ;


  //   Real _ext1_x ;
  //   Real _ext1_y ;
  //   Real _ext2_x ;
  //   Real _ext2_y ;

  //   slip::Color<unsigned char> _col ;
  // } ;



  // /**
  //  ** @brief Constructor
  //  ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
  //  ** @date 2014/07/03
  //  ** @version 0.0.1
  //  ** @param cx X coordinate of the center of the base ellipse
  //  ** @param cy Y coordinate of the center of the base ellipse
  //  ** @param ax Major axis dimension
  //  ** @param ay Minor axis dimension
  //  ** @param orient Orientation of the base ellipse relative to the major axis (and the X axis)
  //  ** @param ext1_x X coordinate of the first point of the elliptical arc
  //  ** @param ext1_y Y coordinate of the first point of the elliptical arc
  //  ** @param ext2_x X coordinate of the last point of the elliptical arc
  //  ** @param ext2_y Y coordinate of the last point of the elliptical arc
  //  ** @param col Color of the elliptical arc
  //  **/
  // template <typename Real>
  // SVGEllipticalArc<Real>::SVGEllipticalArc( const Real cx , const Real cy , const Real ax , const Real ay , const Real orient ,
  //     const Real ext1_x , const Real ext1_y , const Real ext2_x , const Real ext2_y ,
  //     const slip::Color<unsigned char> & col )
  //   : _cx( cx ) ,
  //     _cy( cy ) ,
  //     _ax( ax ) ,
  //     _ay( ay ) ,
  //     _orient( orient ) ,
  //     _ext1_x( ext1_x ) ,
  //     _ext1_y( ext1_y ) ,
  //     _ext2_x( ext2_x ) ,
  //     _ext2_y( ext2_y ) ,
  //     _col( col )
  // {

  // }

  // /**
  //  ** @brief Computes an approximate point on the ellipse given a point near that ellipse
  //  ** @param cx X coordinate of the center of the ellipse
  //  ** @param cy Y coordinate of the center of the ellipse
  //  ** @param ax Major axis dimension
  //  ** @param ay Minor axis dimension
  //  ** @param orient Base orientation of the ellipse
  //  ** @param x X coordinate of the point near the ellipse
  //  ** @param y Y coordinate of the point near the ellipse
  //  ** @param out_x (Output) X coordinate of the point on the ellipse
  //  ** @param out_y (Output) Y coordinate of the point on the ellipse
  //  **/
  // template< typename Real >
  // static inline void RosinPoint( const Real cx , const Real cy , const Real ax , const Real ay , const Real orient ,
  //                                const Real x , const Real y ,
  //                                Real & out_x , Real & out_y )
  // {

  //   const Real ct = std::cos( - orient ) ;
  //   const Real st = std::sin( - orient ) ;

  //   const Real ae2 = ax * ax ;
  //   const Real be2 = ay * ay ;
  //   const Real dx = x - cx ;
  //   const Real dy = y - cy ;

  //   const Real xp = dx * ct - dy * st ;
  //   const Real yp = dx * st + dy * ct ;

  //   const slip::Point2d<Real> proj( xp , yp ) ;

  //   const Real fe2 = ae2 - be2 ;

  //   const Real X = xp * xp ;
  //   const Real Y = yp * yp ;

  //   const Real delta = ( X + Y + fe2 ) * ( X + Y + fe2 ) - static_cast<Real>( 4 ) * X * fe2;
  //   const Real A = ( X + Y + fe2 - std::sqrt( delta ) ) / static_cast<Real>( 2 ) ;
  //   const Real ah = std::sqrt( A );
  //   const Real bh2 = fe2 - A;
  //   const Real term = ( A * be2 + ae2 * bh2 );
  //   const Real base_xx = ah * std::sqrt( ae2 * ( be2 + bh2 ) / term );
  //   const Real base_yy = ay * std::sqrt( bh2 * ( ae2 - A ) / term );


  //   Distance<Real, L2> L2 ;

  //   slip::Point2d<Real> p1(  base_xx ,  base_yy ) ;
  //   slip::Point2d<Real> p2(  base_xx , -base_yy ) ;
  //   slip::Point2d<Real> p3( -base_xx ,  base_yy ) ;
  //   slip::Point2d<Real> p4( -base_xx , -base_yy ) ;

  //   Real xx = base_xx, yy = base_yy ;

  //   Real d[] =
  //   {
  //     L2( proj , p1 ) ,
  //     L2( proj , p2 ) ,
  //     L2( proj , p3 ) ,
  //     L2( proj , p4 )
  //   } ;
  //   Real min_d = d[0] ;
  //   if( d[1] < min_d )
  //   {
  //     min_d = d[1] ;
  //     xx =   base_xx ;
  //     yy = - base_yy ;
  //   }
  //   if( d[2] < min_d )
  //   {
  //     min_d = d[2] ;
  //     xx = - base_xx ;
  //     yy =   base_yy ;
  //   }
  //   if( d[3] < min_d )
  //   {
  //     min_d = d[3] ;
  //     xx = - base_xx ;
  //     yy = - base_yy ;
  //   }

  //   const Real ct_pos = std::cos( orient ) ;
  //   const Real st_pos = std::sin( orient ) ;

  //   out_x = xx * ct_pos - yy * st_pos + cx ;
  //   out_y = xx * st_pos + yy * ct_pos + cy ;

  // }

  // /**
  //  ** @brief Output elliptical arc in SVG format
  //  ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
  //  ** @date 2014/07/03
  //  ** @version 0.0.1
  //  ** @param out Stream in which the elliptical arc will be written
  //  ** @param ell Elliptical arc to output in the stream
  //  ** @return Stream after write operation
  //  **/
  // template <typename Real>
  // std::ostream & operator<<( std::ostream & out , const SVGEllipticalArc< Real > & ell  )
  // {
  //   const Real _cx = ell._cx ;
  //   const Real _cy = ell._cy ;
  //   const Real _ax = ell._ax ;
  //   const Real _ay = ell._ay ;
  //   const Real _orient = ell._orient * static_cast<Real>( 180 ) / slip::constants<Real>::pi() ;
  //   const Real _ext1_x = ell._ext1_x ;
  //   const Real _ext1_y = ell._ext1_y ;
  //   const Real _ext2_x = ell._ext2_x ;
  //   const Real _ext2_y = ell._ext2_y ;

  //   const int r = static_cast<int>( ell._col.get_red() ) ;
  //   const int g = static_cast<int>( ell._col.get_green() ) ;
  //   const int b = static_cast<int>( ell._col.get_blue() ) ;


  //   // Compute extreme angles
  //   Real angle_start = std::atan2( _ext1_y - _cy , _ext1_x - _cx ) ;
  //   Real angle_end   = std::atan2( _ext2_y - _cy , _ext2_x - _cx ) ;

  //   // Make them between [0;2pi]
  //   if( angle_start < 0 )
  //   {
  //     angle_start += slip::constants<Real>::pi() * slip::constants<Real>::two() ;
  //   }
  //   if( angle_end < 0 )
  //   {
  //     angle_end += slip::constants<Real>::pi() * slip::constants<Real>::two() ;
  //   }

  //   if( angle_start > angle_end )
  //   {
  //     angle_end += slip::constants<Real>::pi() * slip::constants<Real>::two() ;
  //   }

  //   // Decide with elliptical arc will be drawn
  //   int fa = 0 ;
  //   int fs = 1 ;
  //   if( ( angle_end - angle_start ) > slip::constants<Real>::pi() )
  //   {
  //     fa = 1 ;
  //   }

  //   // Compute extremal points on the ellipse
  //   Real x1, y1, x2, y2 ;
  //   RosinPoint( _cx , _cy , _ax , _ay , ell._orient , _ext1_x , _ext1_y , x1 , y1 ) ;
  //   RosinPoint( _cx , _cy , _ax , _ay , ell._orient , _ext2_x , _ext2_y , x2 , y2 ) ;

  //   out << "<path d=\"M " << x1 << "," << y1 << " A" << _ax << "," << _ay << " " << _orient << " " << fa << "," << fs << " " << x2 << "," << y2
  //       << "\" fill=\"none\" stroke=\"rgb(" << r << "," << g << "," << b << ")\" stroke-width=\"1\" />" ;

  //   return out ;
  // }


}//::slip

#endif//SLIP_SVG_SHAPES_HPP_
