/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file Pyramid.hpp
 * 
 * \brief Provides a generic pyramid data structure.
 * 
 */
#ifndef SLIP_PYRAMID_HPP
#define SLIP_PYRAMID_HPP

#include <iostream>
#include <vector>
#include <algorithm>
#include "statistics.hpp"
#include "Array.hpp"
#include "container_cast.hpp"
#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/version.hpp>


namespace slip
{

  template<typename Container,typename>
  struct __container_size
  {
    template <typename _II>
    static std::size_t
    container_size(_II first, _II last)
    {
      return static_cast<std::size_t>(last-first);
    }
 
};

template<typename Container>
struct __container_size<Container,std::random_access_iterator_tag>
{
  template <typename _II>
  static std::size_t
  container_size(_II first, _II last)
  {
    return static_cast<std::size_t>(last-first);
  }
};

template<typename Container>
struct __container_size<Container,std::random_access_iterator2d_tag>
{
  template <typename _II>
  static std::size_t
  container_size(_II first, _II last)
  {
    return static_cast<std::size_t>(((last-first)[0])*((last-first)[1]));
  }
};

template<typename Container>
struct __container_size<Container,std::random_access_iterator3d_tag>
{
  template <typename _II>
  static std::size_t
  container_size(_II first, _II last)
  {
    return static_cast<std::size_t>(((last-first)[0])*((last-first)[1])*((last-first)[2]));
  }
  
};


template<typename Container>
struct ContainerSize 
{
 
  ContainerSize(){}
  template<typename Iterator>
  std::size_t operator() (Iterator first, Iterator last) const
  {
    typedef typename std::iterator_traits<Iterator>::iterator_category _Category;
    return __container_size<Container,_Category>::container_size(first,last);
  }
};

  //-------------------------------------------------------------
  // container resolution
  //--------------------------------------------------------------
 template<typename Container,typename>
  struct __container_resolution
  {
    template <typename _II>
    static slip::Array<std::size_t> 
    container_resolution(_II first, _II last)
    {
      slip::Array<std::size_t> R(1);
      R[0] = static_cast<std::size_t>(last-first);
      return R;
    }
 
};

template<typename Container>
struct __container_resolution<Container,std::random_access_iterator_tag>
{
  template <typename _II>
  static slip::Array<std::size_t> 
  container_resolution(_II first, _II last)
  {
    slip::Array<std::size_t> R(1);
    R[0] = static_cast<std::size_t>(last-first);
    return R;
  }
};

template<typename Container>
struct __container_resolution<Container,std::random_access_iterator2d_tag>
{
  template <typename _II>
  static slip::Array<std::size_t> 
  container_resolution(_II first, _II last)
  {
   slip::Array<std::size_t> R(2);
   R[0] = static_cast<std::size_t>((last-first)[0]);
   R[1] = static_cast<std::size_t>((last-first)[1]);
   return R;
  }
};

template<typename Container>
struct __container_resolution<Container,std::random_access_iterator3d_tag>
{
  template <typename _II>
  static slip::Array<std::size_t>
  container_resolution(_II first, _II last)
  {
    slip::Array<std::size_t> R(3);
    R[0] = static_cast<std::size_t>((last-first)[0]);
    R[1] = static_cast<std::size_t>((last-first)[1]);
    R[2] = static_cast<std::size_t>((last-first)[2]);
   
   return R;
  }
  
};


template<typename Container>
struct ContainerResolution 
{
 
  ContainerResolution(){}
  template<typename Iterator>
   slip::Array<std::size_t> operator() (Iterator first, Iterator last) const
  {
    typedef typename std::iterator_traits<Iterator>::iterator_category _Category;
    return __container_resolution<Container,_Category>::container_resolution(first,last);
  }
};


  //--------------------------------------------------------------
  //  container allocator
  //--------------------------------------------------------------


  template<typename Container,typename>
  struct __container_allocator
  {
    static Container*
    container_allocator(const slip::Array<std::size_t>& res)
    {
      return new Container(res[0]);
    }
 
};

template<typename Container>
struct __container_allocator<Container,std::random_access_iterator_tag>
{

  static Container*
  container_allocator(const slip::Array<std::size_t>& res)
  {
    return new Container(res[0]);
  }
 
};

template<typename Container>
struct __container_allocator<Container,std::random_access_iterator2d_tag>
{
  static Container*
  container_allocator(const slip::Array<std::size_t>& res)
  {
    return new Container(res[0],res[1]);
  }
 
};

template<typename Container>
struct __container_allocator<Container,std::random_access_iterator3d_tag>
{
   static Container*
  container_allocator(const slip::Array<std::size_t>& res)
  {
    return new Container(res[0],res[1],res[2]);
  }
};


template<typename Container>
struct ContainerAllocator
{
 
  ContainerAllocator(){}
  Container* operator() (const slip::Array<std::size_t>& res) const
  {
    typedef typename std::iterator_traits<typename Container::default_iterator>::iterator_category _Category;
    return __container_allocator<Container,_Category>::container_allocator(res);
  }
};
  //--------------------------------------------------------------
  //  pyramid allocator
  //--------------------------------------------------------------

template<typename Container,typename>
struct __pyramid_allocator
{
  template <typename _II>
  static std::vector<Container*>*
  pyramid_allocator(_II first, _II last, const std::size_t levels)
  {
    assert(levels > 0);
    assert(slip::power(2,levels-1) <= int(last-first));
     std::size_t lenght = static_cast<std::size_t>(last-first);
   
    
    std::vector<Container*>* data_  = new std::vector<Container*>(levels);
    (*data_)[0] = new Container(lenght);
    std::copy(first,last,((*data_)[0])->begin());
    for(std::size_t i = 1; i < levels; ++i)
      {
	(*data_)[i] = new Container((*data_)[i-1]->size()/2);
      }
    return data_;
  }
 
};

template<typename Container>
struct __pyramid_allocator<Container,std::random_access_iterator_tag>
{
  template <typename _II>
  static std::vector<Container*>*
  pyramid_allocator(_II first, _II last, const std::size_t levels)
  {
    assert(levels > 0);
    assert(slip::power(2,levels-1) <= int(last-first));
    std::size_t lenght = static_cast<std::size_t>(last-first);
   
   
    std::vector<Container*>* data_  = new std::vector<Container*>(levels);
    (*data_)[0] = new Container(lenght);
    std::copy(first,last,((*data_)[0])->begin());
    for(std::size_t i = 1; i < levels; ++i)
      {
	(*data_)[i] = new Container((*data_)[i-1]->size()/2);
      }
    return data_;
  }
};

template<typename Container>
struct __pyramid_allocator<Container,std::random_access_iterator2d_tag>
{
  template <typename _II>
  static std::vector<Container*>*
  pyramid_allocator(_II first, _II last,const std::size_t levels)
  {
    assert(levels > 0);
    assert(slip::power(2,levels-1) <= int((last-first)[0]));
    assert(slip::power(2,levels-1) <= int((last-first)[1]));
    std::size_t rows = static_cast<std::size_t>((last-first)[0]);
    std::size_t cols = static_cast<std::size_t>((last-first)[1]);
   
    
    std::vector<Container*>* data_ = new std::vector<Container*>(levels);
    (*data_)[0] = new Container(rows,cols);
    std::copy(first,last,((*data_)[0])->begin());
    for(std::size_t i = 1; i < levels; ++i)
      {
	(*data_)[i] = new Container((*data_)[i-1]->rows()/2,(*data_)[i-1]->cols()/2);
      }
    return data_;
  }
};

template<typename Container>
struct __pyramid_allocator<Container,std::random_access_iterator3d_tag>
{
  template <typename _II>
  static std::vector<Container*>*
  pyramid_allocator(_II first, _II last,const std::size_t levels)
  {
    assert(levels > 0);
    assert(slip::power(2,levels-1) <= int((last-first)[0]));
    assert(slip::power(2,levels-1) <= int((last-first)[1]));
    assert(slip::power(2,levels-1) <= int((last-first)[2]));
    
    std::size_t slices = static_cast<std::size_t>((last-first)[0]);
    std::size_t rows   = static_cast<std::size_t>((last-first)[1]);
    std::size_t cols   = static_cast<std::size_t>((last-first)[2]);
  
 
  std::vector<Container*>* data_ = new std::vector<Container*>(levels);
  (*data_)[0] = new Container(slices,rows,cols);
  std::copy(first,last,((*data_)[0])->begin());
  for(std::size_t i = 1; i < levels; ++i)
    {
      (*data_)[i] = new Container((*data_)[i-1]->slices()/2,
				  (*data_)[i-1]->rows()/2,
				  (*data_)[i-1]->cols()/2);
    }
  return data_;
  }
  
};


template<typename Container>
struct Pyramid_Allocator 
{
 
  Pyramid_Allocator(){}
  template<typename Iterator>
  std::vector<Container*>* operator() (Iterator first, Iterator last, const std::size_t levels) const
  {
    typedef typename std::iterator_traits<Iterator>::iterator_category _Category;
    return __pyramid_allocator<Container,_Category>::pyramid_allocator(first,last,levels);
  }
};

//---------------------------------------------
//pyramid_copy
//--------------------------------------------
template<typename Container,typename>
struct __pyramid_copy
{
  static std::vector<Container*>*
  pyramid_copy(const Container& c, const std::size_t levels)
  {
    //    typedef typename Container::iterator_category _Category;
    typedef typename std::iterator_traits<typename Container::default_iterator>::iterator_category _Category;
    return __pyramid_allocator<Container,_Category>::pyramid_allocator(c.begin(),c.end(),levels);
  }
 
};

template<typename Container>
struct __pyramid_copy<Container,std::random_access_iterator_tag>
{
  static std::vector<Container*>*
  pyramid_copy(const Container& c, const std::size_t levels)
  {
    //typedef typename Container::iterator_category _Category;
    typedef typename std::iterator_traits<typename Container::default_iterator>::iterator_category _Category;
    return __pyramid_allocator<Container,_Category>::pyramid_allocator(c.begin(),c.end(),levels);
  }
};

template<typename Container>
struct __pyramid_copy<Container,std::random_access_iterator2d_tag>
{
  static std::vector<Container*>*
  pyramid_copy(const Container& c,const std::size_t levels)
  {
    //typedef typename Container::iterator_category _Category;
    typedef typename std::iterator_traits<typename Container::default_iterator>::iterator_category _Category;
    return __pyramid_allocator<Container,_Category>::pyramid_allocator(c.upper_left(),c.bottom_right(),levels);
  }
};

template<typename Container>
struct __pyramid_copy<Container,std::random_access_iterator3d_tag>
{
  static std::vector<Container*>*
  pyramid_copy(const Container& c, const std::size_t levels)
  {
    //typedef typename Container::iterator_category _Category;
    typedef typename std::iterator_traits<typename Container::default_iterator>::iterator_category _Category;
   return __pyramid_allocator<Container,_Category>::pyramid_allocator(c.front_upper_left(),c.back_bottom_right(),levels);
  }
  
};


template<typename Container>
struct Pyramid_Copy 
{
 
  Pyramid_Copy(){}
  std::vector<Container*>* operator() (const Container& c, 
				       const std::size_t levels) const
  {
    //    typedef typename Container::iterator_category _Category;
    typedef typename std::iterator_traits<typename Container::default_iterator>::iterator_category _Category;
    return __pyramid_copy<Container,_Category>::pyramid_copy(c,levels);
  }
};

}//::slip

namespace slip
{
template <typename Container>
class Pyramid;

template <typename Container>
std::ostream& operator<<(std::ostream & out, const slip::Pyramid<Container>& a);

template<typename Container>
bool operator==(const Pyramid<Container>& x, 
		const Pyramid<Container>& y);

template<typename Container>
bool operator!=(const Pyramid<Container>& x, 
		const Pyramid<Container>& y);

/*! \class Pyramid
** 
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/04/02
** \since 1.2.0
** \brief This is container to handle a pyramid of containers
** \param Container Type of the element in the %Pyramid
** \todo add iterators, add comparison operators to be complient with
** the Container concept
**
*/
template <class Container>
class Pyramid
{
public:
  typedef Container value_type;
  typedef Pyramid<Container> self;
  typedef const Pyramid<Container> const_self;

  typedef value_type& reference;
  typedef value_type const& const_reference;

  typedef value_type* pointer;
  typedef value_type const* const_pointer;
 

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;
 
  /**
   ** \name Constructors & Destructors
   */
  /*@{*/
  
  /*!
  ** \brief Constructs a %Pyramid with no data and zero levels
  */
  Pyramid():
  data_(0),
  levels_(0),
  base_size_(slip::Array<std::size_t>())
  {
  }

  /*!
  ** \brief Constructs a %Pyramid of levels \a level and copy the
  ** finest level (0 level) with the range [init_container_first,
  ** init_container_last). All other levels are initialized with the default 
  ** constructor of the Container elements.
  ** \param init_container_first initial container
  ** \param init_container_last  initial container
  ** \param levels
  ** \pre sizes of the range should be powers of 2
  ** \pre slip::power(2,levels-1) <= sizes
  ** \par Example:
  ** \code
  ** slip::Array2d<float> init2d(16,16);
  ** slip::iota(init2d.begin(),init2d.end(),1.0,1.0);
  ** slip::Pyramid<slip::Array2d<float> > pyd2d(init2d.upper_left(),
  **				     init2d.bottom_right(),
  **				     3);
  ** std::cout<<pyd2d<<std::endl;
  ** \endcode
  */
  template<typename Iterator>
  Pyramid(Iterator init_container_first,
	  Iterator init_container_last,
	  const size_type levels):
    data_(slip::Pyramid_Allocator<Container>()(init_container_first,
					       init_container_last,
					       levels)),
    levels_(levels),
    base_size_(slip::ContainerResolution<Container>()(init_container_first,
						      init_container_last))
  {
    
     // this->data_ = slip::Pyramid_Allocator<Container>()(init_container_first,
     // 						       init_container_last,
     // 						       levels);
  }
  /*!
  ** \brief Constructs a copy of the %Pyramid \a rhs
  */
  // Pyramid(const self& rhs):
  //   levels_(rhs.levels_)
  // {
  //   //allocate
  //   this->data_ =  this->data_ = slip::Pyramid_Copy<Container>()(*((*(rhs.data_))[0]),
  // 								 rhs.levels_);
  //   //copy the data
  //   for(std::size_t i = 0; i < this->levels_; ++i)
  //     {
  // 	std::copy(rhs[i].begin(),rhs[i].end(),((*data_)[i])->begin());
  //     }
  // }
   Pyramid(const self& rhs):
     data_(slip::Pyramid_Copy<Container>()(*((*(rhs.data_))[0]),rhs.levels_)),
     levels_(rhs.levels_),
     base_size_(rhs.base_size())
  {
  
    //copy the data
    for(std::size_t i = 0; i < this->levels_; ++i)
      {
	std::copy(rhs[i].begin(),rhs[i].end(),((*data_)[i])->begin());
      }
  }
 
   /*!
  ** \brief Destructor of the Pyramid
  */
  ~Pyramid()
  {
    this->desallocate();
  }
  

  /*@} End Constructors */
 
  /**
   ** \name Assignment operators
   */
  /*@{*/
  /*!
  ** \brief Assigns rhs to the %Pyramid 
  **
  ** \param rhs %Pyramid to get the values from.
  ** \return 
  */
  self& operator=(const self& rhs)
  {
     if(this != &rhs)
      {
	if((this->levels_ != rhs.levels_) || 
	   (((*data_)[0])->size() != ((*(rhs.data_))[0])->size()))
	  {
	    this->desallocate();
	    this->levels_ =  rhs.levels_;
	    this->base_size_ = rhs.base_size();
	    this->data_ = slip::Pyramid_Copy<Container>()(*((*(rhs.data_))[0]),
							  rhs.levels_);
	  }
	for(std::size_t i = 0; i < this->levels_; ++i)
	  {
	    std::copy(rhs[i].begin(),rhs[i].end(),((*data_)[i])->begin());
	  }
      }
    return *this;
  }
  /*@} End Assignment operators */


  

  /**
   ** \name Accessors
   */
  /*@{*/
  size_type levels() const
  {
    return this->levels_;
  }

  const slip::Array<size_type>& base_size() const
  {
    return this->base_size_;
  }
  /*@} End Accessors */


   /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Subscript access to the data contained in the %Pyramid.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read/write reference to data.
  ** \pre n < levels()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator[](const size_type n)
  {
    assert(n < this->levels());
    return *((*data_)[n]);
  }

 
  /*!
  ** \brief Subscript access to the data contained in the %Pyramid.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read-only (constant) reference to data.
  ** \pre n < levels()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator[](const size_type n) const
  {
    assert(n < this->levels());
    return *((*data_)[n]);
  }
  /*@} End Element access operators */

 /**
   ** \name i/o operators
   */
  /*@{*/
 /*!
  ** \brief Write the %Pyramid to the ouput stream
  ** \param out output std::ostream
  ** \param a %Pyramid to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const self& a);

  /*@} End i/o operators */

 /*!
  ** \brief Returns the number of containers in the %Pyramid
  */
  size_type size() const
  {
    return data_->size();
  }
  /*!
  ** \brief Returns the maximal size (number of container) in the %Pyramid
  */
  size_type max_size() const
  {
    return data_->max_size();
  }

  /*!
  ** \brief Returns true if the %Pyramid is empty.  (Thus size() == 0)
  */
  bool empty()const
  {
    return data_->empty();
  }

  /*!
  ** \brief Swaps data with another %Pyramid.
  ** \param rhs A %Pyramid of the same element type
  */
  void swap(self& rhs)
  {
    assert((this->levels_ == rhs.levels_) && (((*data_)[0])->size() == ((*(rhs.data_))[0])->size()));
    for(std::size_t i = 0; i < this->levels_; ++i)
      {
	(*(*rhs.data_)[i]).swap(*((*data_)[i]));
      }
  }
  
  /*!
  ** \brief Fill all the container of the Pyramid with val.
  ** \param val Value to fill
  */
  void fill(const typename Container::value_type& val)
  {
    for(std::size_t i = 0; i < this->levels_; ++i)
      {
	std::fill(((*data_)[i])->begin(),((*data_)[i])->end(),val);
      }
  }

 /**
   ** \name Comparison operators
   */
  /*@{*/
  /*!
  ** \brief %Array equality comparison
  ** \param x A %Array
  ** \param y A %Array of the same type of \a x
  ** \return true iff the size and the elements of the Arrays are equal
  */
  friend bool operator== <>(const Array<Container>& x, 
			    const Array<Container>& y);

 /*!
  ** \brief %Array inequality comparison
  ** \param x A %Array
  ** \param y A %Array of the same type of \a x
  ** \return true if !(x == y) 
  */
  friend bool operator!= <>(const Array<Container>& x, 
			    const Array<Container>& y);

  /*@} Comparison operators */
  
private:
  std::vector<Container* >* data_;
  std::size_t levels_;
  slip::Array<std::size_t> base_size_;
  

  void desallocate()
  {
     if(this->data_ != 0)
      {
	for(std::size_t i = 0; i < this->levels_; ++i)
	  {
	    if((*data_)[i] != 0)
	      {
		delete (*data_)[i];
	      }
	  }
	delete this->data_;
      }
   }

   friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & this->levels_;
	  ar & this->base_size_;
	  //ar & this->data_;
	  for(std::size_t l = 0; l < this->levels_;++l)
	    {
	      ar & *((*data_)[l]);
	    }
	}
    
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  // std::cout<<"read levels"<<std::endl;
	  ar & this->levels_;
	  //std::cout<<"read base_size"<<std::endl;
	  ar & this->base_size_;
	  //std::cout<<"desallocate"<<std::endl;
	  this->desallocate();
	  Container* cont = slip::ContainerAllocator<Container>()(this->base_size_);
	  std::cout<<"this->base_size_ "<<this->base_size_<<std::endl;
	  //std::cout<<"cont->size() "<<cont->size()<<std::endl;
	  typename Container::default_iterator first;
	  typename Container::default_iterator last;
	  slip::container_cast(*cont,first,last);
	  std::cout<<"last-first = "<<(last-first)<<std::endl;
	  this->data_ = slip::Pyramid_Allocator<Container>()(first,
							     last,
							     this->levels_);
	  for(std::size_t l = 0; l < this->levels_;++l)
	    {
	      ar & *((*data_)[l]);
	    }
	  delete cont;
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};
  
} //slip::


namespace slip
{

  template <typename Container>
  inline
  std::ostream& operator<<(std::ostream & out, const slip::Pyramid<Container>& a)
  {
    out<<"Pyramid levels : "<<a.levels_<<std::endl;
    out<<"Pyramid base size : "<<a.base_size_<<std::endl;
    for(std::size_t i = 0; i < a.levels_; ++i)
      {
	out<<"Level "<<i<<std::endl;
	out<<*((*a.data_)[i])<<std::endl;
      }
    return out;
  }

}//slip::

namespace slip
{
  template<typename Container>
bool operator==(const Pyramid<Container>& x, 
		const Pyramid<Container>& y)
{
  
  if (x.size() != y.size())
    {
      return false;
    }
  typedef typename slip::Pyramid<Container>::size_type size_type;
  const size_type size = x.size();
  for (size_type i = 0; i < size; ++i)
    {
      if (!(x[i] == y[i]))
	{
	  return false;
	}
    }
  return true;
}

template<typename Container>
bool operator!=(const Pyramid<Container>& x, 
		const Pyramid<Container>& y)
{
  return !(x == y);
}

}//::slip

#endif // SLIP_PYRAMID_HPP
