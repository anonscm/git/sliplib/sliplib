/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file GrayscaleImage.hpp
 * 
 * \brief Provides a class to manipulate grayscale images.
 * 
 */
#ifndef SLIP_GRAYSCALEIMAGE_HPP
#define SLIP_GRAYSCALEIMAGE_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <cmath>
#include <string>
#include <cstddef>
#include <stdexcept>

//read/write image with ImageMagick
//breaking the ImageMagick dependency possibility
// Author : denis.arrivault\AT\inria.fr
// Date : 2013/04/05
// \since 1.4.0
#ifdef HAVE_MAGICK
#include <wand/magick_wand.h> 
#include <magick/api.h>
#endif
#include "Matrix.hpp"
#include "stride_iterator.hpp"
#include "iterator2d_box.hpp"
#include "iterator2d_range.hpp"
#include "apply.hpp"
#include "io_tools.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template<class T>
class stride_iterator;

template<class T>
class iterator2d_box;


template<class T>
class const_iterator2d_box;

template<class T>
class const_iterator2d_range;

template <class T>
class DPoint2d;

template <class T>
class Point2d;

template <class T>
class Box2d;

template <class T>
class Range;

template <typename T>
class GrayscaleImage;

template <typename T>
class Matrix;

template <typename T>
std::ostream& operator<<(std::ostream & out, const slip::GrayscaleImage<T>& a);

template<typename T>
bool operator==(const slip::GrayscaleImage<T>& x, 
		const slip::GrayscaleImage<T>& y);

template<typename T>
bool operator!=(const slip::GrayscaleImage<T>& x, 
		const slip::GrayscaleImage<T>& y);

template<typename T>
bool operator<(const slip::GrayscaleImage<T>& x, 
	       const slip::GrayscaleImage<T>& y);

template<typename T>
bool operator>(const slip::GrayscaleImage<T>& x, 
	       const slip::GrayscaleImage<T>& y);

template<typename T>
bool operator<=(const slip::GrayscaleImage<T>& x, 
		const slip::GrayscaleImage<T>& y);

template<typename T>
bool operator>=(const slip::GrayscaleImage<T>& x, 
		const slip::GrayscaleImage<T>& y);

/*! \class GrayscaleImage
**  \ingroup Containers Containers2d
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.3
** \since 1.0.0
** \date 2014/04/01
** \brief This is a grayscaleimage class.
**  This is a two-dimensional dynamic and generic container.
** This container statisfies the BidirectionnalContainer concepts of the STL.
** It is also an 2d extension of the RandomAccessContainer concept. That is
** to say the bracket element access is replaced by the double bracket element 
** access.
** Data are stored using a %Matrix class.
** It extends the interface of Matrix adding image read/write operations.
** These operations are done using the ImageMagick library.
** \param T. Type of object in the GrayscaleImage 
** \par Axis conventions:
** \image html iterator2d_conventions.jpg "axis and notation conventions"
** \image latex iterator2d_conventions.eps "axis and notation conventions" width=5cm
  **
**
*/
template <typename T>
class GrayscaleImage
{
public :

  typedef T value_type;
  typedef GrayscaleImage<T> self;
  typedef const GrayscaleImage<T> const_self;

  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;
  
  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;


  typedef pointer row_iterator;
  typedef const_pointer const_row_iterator;
  typedef slip::stride_iterator<pointer> col_iterator;
  typedef slip::stride_iterator<const_pointer> const_col_iterator;

 
  typedef typename slip::Matrix<T>::iterator2d iterator2d;
  typedef typename slip::Matrix<T>::const_iterator2d const_iterator2d;
  typedef slip::stride_iterator<pointer> row_range_iterator;
  typedef slip::stride_iterator<const_pointer> const_row_range_iterator;
  typedef slip::stride_iterator<col_iterator> col_range_iterator;
  typedef slip::stride_iterator<const_col_iterator> const_col_range_iterator;
  typedef typename slip::Matrix<T>::iterator2d_range iterator2d_range;
  typedef typename slip::Matrix<T>::const_iterator2d_range const_iterator2d_range;


  typedef std::reverse_iterator<iterator> reverse_row_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
  typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
  typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;
  typedef std::reverse_iterator<iterator2d> reverse_iterator2d;
  typedef std::reverse_iterator<const_iterator2d> const_reverse_iterator2d;
  typedef std::reverse_iterator<row_range_iterator> reverse_row_range_iterator;
  typedef std::reverse_iterator<const_row_range_iterator> const_reverse_row_range_iterator;
  typedef std::reverse_iterator<col_range_iterator> reverse_col_range_iterator;
  typedef std::reverse_iterator<const_col_range_iterator> const_reverse_col_range_iterator;
  typedef std::reverse_iterator<iterator2d_range> reverse_iterator2d_range;
  typedef std::reverse_iterator<const_iterator2d_range> const_reverse_iterator2d_range;

 //default iterator of the container
  typedef iterator2d default_iterator;
  typedef const_iterator2d const_default_iterator;

  static const std::size_t DIM = 2;

public:
 /**
  ** \name Constructors & Destructors
  */
 /*@{*/

  /*!
  ** \brief Constructs a %GrayscaleImage.
  */
  GrayscaleImage();
 
  /*!
  ** \brief Constructs a %GrayscaleImage.
  ** \param height first dimension of the %GrayscaleImage
  ** \param width second dimension of the %GrayscaleImage
  */
  GrayscaleImage(const size_type height,
		 const size_type width);
  
  /*!
  ** \brief Constructs a %GrayscaleImage initialized by the scalar value \a val.
  ** \param height first dimension of the %GrayscaleImage
  ** \param width second dimension of the %GrayscaleImage
  ** \param val initialization value of the elements 
  */
  GrayscaleImage(const size_type height,
		 const size_type width, 
		 const T& val);
 
  /*!
  ** \brief Constructs a %GrayscaleImage initialized by an array \a val.
  ** \param height first dimension of the %GrayscaleImage
  ** \param width second dimension of the %GrayscaleImage
  ** \param val initialization array value of the elements 
  */
  GrayscaleImage(const size_type height,
		 const size_type width, 
		 const T* val);
 

  /**
  **  \brief  Contructs a %GrayscaleImage from a range.
  ** \param height first dimension of the %GrayscaleImage
  ** \param width second dimension of the %GrayscaleImage
  **  \param  first  An input iterator.
  **  \param  last  An input iterator.
  **
  ** Create a %GrayscaleImage consisting of copies of the elements from
  ** [first,last).
  */
  template<typename InputIterator>
  GrayscaleImage(const size_type height,
		 const size_type width, 
		 InputIterator first,
		 InputIterator last)
  {
    matrix_ = new slip::Matrix<T>(height,width,first,last);
  } 

  /*!
  ** \brief Constructs a copy of the %GrayscaleImage \a rhs
  */
  GrayscaleImage(const self& rhs);

  /*!
  ** \brief Destructor of the %GrayscaleImage
  */
  ~GrayscaleImage();
  

  /*@} End Constructors */


  /*!
  ** \brief Resizes a GrayscaleImage.
  ** \param height new first dimension
  ** \param width new second dimension
  ** \param val new value for all the elements
  */ 
  void resize(const size_type height,
	      const size_type width,
	      const T& val = T());


 

  /**
   ** \name iterators
   */
  /*@{*/
 
   /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %GrayscaleImage.  Iteration is done in ordinary
  **  element order.
  ** \return begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  **  slip::GrayscaleImage<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  iterator begin();


  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %GrayscaleImage.  Iteration is done in ordinary
  **  element order.
  ** \return end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  **  slip::GrayscaleImage<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  iterator end();

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %GrayscaleImage.  Iteration is done in ordinary
  **  element order.
  ** 
  ** \return const begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  **  slip::GrayscaleImage<double> S(10,9);
  ** //copy the sum of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_iterator begin() const;

  
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %GrayscaleImage.  Iteration is done in
  **  ordinary element order.
  ** \return const end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  **  slip::GrayscaleImage<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_iterator end() const;
  
  /*!
  **  \brief Returns a read/write reverse iterator that points to the
  **  last element in the %GrayscaleImage.  Iteration is done in reverse
  **  element order.
  ** \return reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  **  slip::GrayscaleImage<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 
  ** //in S (iterated in the reverse order element)
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.rbegin(),
  **                std::plus<double>());
  ** \endcode
  */
  reverse_iterator rbegin();
  
  /*!
  **  \brief Returns a read/write reverse iterator that points to one
  **  before the first element in the %GrayscaleImage.  Iteration is done
  **  in reverse element order.
  **  \return reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  **  slip::GrayscaleImage<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 
  ** //in S (iterated in the reverse order element)
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.rbegin(),
  **                std::plus<double>());
  ** \endcode
  */
  reverse_iterator rend();
  
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to the last element in the %GrayscaleImage.  Iteration is done in
  **  reverse element order.
  ** \return const reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  **  slip::GrayscaleImage<double> S(10,9);
  ** //copy the sum of the elements of A1 (iterated in the reverse order element)
  ** //and A2 in S 
  ** std::transform(A1.rbegin(),A1.rend(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_reverse_iterator rbegin() const;

  
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to one before the first element in the %GrayscaleImage.  Iteration
  **  is done in reverse element order.
  **  \return const reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  **  slip::GrayscaleImage<double> S(10,9);
  ** //copy the sum of the elements of A1 (iterated in the reverse order element)
  ** //and A2 in S 
  ** std::transform(A1.rbegin(),A1.rend(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_reverse_iterator rend() const;

 
 /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  row_iterator row_begin(const size_type row);

  
  /*!
  **  \brief Returns a read/write iterator that points one past
  **  the end element of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in ordinary element order.
  ** \param row  The index of the row to iterate.
  ** \return begin row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  row_iterator row_end(const size_type row);


  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  const_row_iterator row_begin(const size_type row) const;


  /*!
  **  \brief Returns a read-only iterator that points one past
  **  the end element of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  const_row_iterator row_end(const size_type row) const;
  
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the column \a column in the %GrayscaleImage.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  col_iterator col_begin(const size_type col);


  /*!
  **  \brief Returns a read/write iterator that points one past
  **  the end element of the column \a column in the %GrayscaleImage.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  col_iterator col_end(const size_type col);

  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the column \a column in the %GrayscaleImage.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin const_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  const_col_iterator col_begin(const size_type col) const;



  /*!
  **  \brief Returns a read-only iterator that points one past
  **  the end element of the column \a column in the %GrayscaleImage.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column 
  ** \return begin const_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(10,9);
  **  slip::GrayscaleImage<double> A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  const_col_iterator col_end(const size_type col) const;


  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the %Range \a range of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return begin row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  row_range_iterator row_begin(const size_type row,
			       const slip::Range<int>& range);



  /*!
  **  \brief Returns a read/write iterator that points one past the end
  **  element of the %Range \a range of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return end row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  row_range_iterator row_end(const size_type row,
			     const slip::Range<int>& range);


  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return begin const_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  const_row_range_iterator row_begin(const size_type row,
				     const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read_only iterator that points one past the last
  **  element of the %Range range of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row Row to iterate.
  ** \param range %Range of the row to iterate
  ** \return begin const_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  const_row_range_iterator row_end(const size_type row,
				   const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read-write iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %GrayscaleImage.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate
  ** \return begin col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  col_range_iterator col_begin(const size_type col,
			       const slip::Range<int>& range);

  /*!
  **  \brief Returns a read-write iterator that points to the past
  **  the end element of the %Range \a range of the col \a col in the 
  **  %GrayscaleImage.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  col_range_iterator col_end(const size_type col,
			     const slip::Range<int>& range);




  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %GrayscaleImage.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin const_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  const_col_range_iterator col_begin(const size_type col,
				     const slip::Range<int>& range) const;

 
  /*!
  **  \brief Returns a read-only iterator that points to the past
  **  the end element of the %Range \a range of the col \a col in 
  **  the %GrayscaleImage.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate
  ** \return begin const_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  const_col_range_iterator col_end(const size_type col,
				   const slip::Range<int>& range) const;

  

  /*!
  **  \brief Returns a read/write reverse iterator that points to the 
  **  last element of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return begin reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  */
  reverse_row_iterator row_rbegin(const size_type row);


  /*!
  **  \brief Returns a read/write reverse iterator that points one past
  **  the first element of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return end reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  */
  reverse_row_iterator row_rend(const size_type row);

  /*!
  **  \brief Returns a read-only reverse iterator that points to the last
  **  element of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  */
  const_reverse_row_iterator row_rbegin(const size_type row) const;

  /*!
  **  \brief Returns a read-only reverse iterator that points one past
  **  the first element of the row \a row in the %GrayscaleImage. 
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return end const_reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  */
  const_reverse_row_iterator row_rend(const size_type row) const;

  /*!
  **  \brief Returns a read/write reverse iterator that points to the last
  **  element of the column \a column in the %GrayscaleImage.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return begin col_reverse_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  */
  reverse_col_iterator col_rbegin(const size_type col);

  
  /*!
  **  \brief Returns a read/write reverse iterator that points one past
  **  the first element of the column \a column in the %GrayscaleImage.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return end reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  */
  reverse_col_iterator col_rend(const size_type col);

  /*!
  **  \brief Returns a read-only reverse iterator that points to the last
  **  element of the column \a column in the %GrayscaleImage.  
  **  Iteration is done modulo the number of columns and in the reverse 
  ** element order.
  ** \param col The index of the column to iterate.
  ** \return begin const_reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  */
  const_reverse_col_iterator col_rbegin(const size_type col) const;

    
  /*!
  **  \brief Returns a read-only reverse iterator that points one past
  **  the first element of the column \a column in the %GrayscaleImage.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return end const_reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  */
  const_reverse_col_iterator col_rend(const size_type col) const;
 

  /*!
  **  \brief Returns a read-write iterator that points to the last
  **  element of the %Range \a range of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  */
  reverse_row_range_iterator row_rbegin(const size_type row,
					const slip::Range<int>& range);

 
  /*!
  **  \brief Returns a read-write iterator that points one before
  **  the first element of the %Range \a range of the row \a row in the 
  **  %GrayscaleImage.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  */
  reverse_row_range_iterator row_rend(const size_type row,
				      const slip::Range<int>& range);



  /*!
  **  \brief Returns a read-only iterator that points to the last
  **  element of the %Range \a range of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate
  ** \return begin const_reverse_row_range_iterator value
  **
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  */
  const_reverse_row_range_iterator row_rbegin(const size_type row,
					      const slip::Range<int>& range) const;


   /*!
  **  \brief Returns a read-only iterator that points one before the first
  **  element of the %Range \a range of the row \a row in the %GrayscaleImage.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate
  ** \return const_reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  */
  const_reverse_row_range_iterator row_rend(const size_type row,
					    const slip::Range<int>& range) const;
 
 


  /*!
  **  \brief Returns a read-write iterator that points to the last
  **  element of the %Range \a range of the col \a col in the %GrayscaleImage.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  */
  reverse_col_range_iterator col_rbegin(const size_type col,
					const slip::Range<int>& range);

  /*!
  **  \brief Returns a read-write iterator that points to one before 
  **  the first element of the %Range range of the col \a col in the 
  **  %GrayscaleImage.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  */
  reverse_col_range_iterator col_rend(const size_type col,
				      const slip::Range<int>& range);


  /*!
  **  \brief Returns a read_only iterator that points to the last
  **  element of the %Range \& range of the col \a col in the %GrayscaleImage.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin const_reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  */
  const_reverse_col_range_iterator 
  col_rbegin(const size_type col,
	     const slip::Range<int>& range) const;

 
  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %GrayscaleImage.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return const_reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GrayscaleImage.
  ** \pre The range must be inside the whole range of the %GrayscaleImage.
  */
  const_reverse_col_range_iterator col_rend(const size_type col,
					    const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read/write iterator2d that points to the first
  **  element of the %GrayscaleImage. It points to the upper left element of
  **  the %GrayscaleImage.
  **  
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  iterator2d upper_left();
  
  
  /*!
  **  \brief Returns a read/write iterator2d that points to the past
  **  the end element of the %GrayscaleImage. It points to past the end element of
  **  the bottom right element of the %GrayscaleImage.
  **  
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  iterator2d bottom_right();


  /*!
  **  \brief Returns a read-only iterator2d that points to the first
  **  element of the %GrayscaleImage. It points to the upper left element of
  **  the %GrayscaleImage.
  **  
  ** \return begin const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  const_iterator2d upper_left() const;
  
 
  /*!
  **  \brief Returns a read-only iterator2d that points to the past
  **  the end element of the %GrayscaleImage. It points to past the end element of
  **  the bottom right element of the %GrayscaleImage.
  **  
  ** \return begin const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  const_iterator2d bottom_right() const;

  /*!
  **  \brief Returns a read/write iterator2d that points to the first
  **  element of the %GrayscaleImage. It points to the upper left element of
  **  the \a %Box2d associated to the %GrayscaleImage.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %GrayscaleImage.
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **  
  ** \return end iterator2d value
  ** \pre The box indices must be inside the range of the %GrayscaleImage ones.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
 */
  iterator2d upper_left(const Box2d<int>& box);

 
  /*!
  **  \brief Returns a read/write iterator2d that points to the past
  **  the end element of the %GrayscaleImage. It points to past the end element of
  **  the bottom right element of the \a %Box2d associated to the %GrayscaleImage.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %GrayscaleImage.
  **
  ** \return end iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \pre The box indices must be inside the range of the %GrayscaleImage ones.
  **  
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  iterator2d bottom_right(const Box2d<int>& box);


  /*!
  **  \brief Returns a read only iterator2d that points to the first
  **  element of the %GrayscaleImage. It points to the upper left element of
  **  the \a %Box2d associated to the %GrayscaleImage.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %GrayscaleImage.
  ** \return end const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **
  ** \pre The box indices must be inside the range of the %GrayscaleImage ones.
  ** 
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  const_iterator2d upper_left(const Box2d<int>& box) const;

 
  /*!
  **  \brief Returns a read only iterator2d that points to the past
  **  the end element of the %GrayscaleImage. It points to past the end element of
  **  the bottom right element of the \a %Box2d associated to the %GrayscaleImage.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %GrayscaleImage.
  ** \return end const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **
  ** \pre The box indices must be inside the range of the %GrayscaleImage ones.
  ** 
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  const_iterator2d bottom_right(const Box2d<int>& box) const;


  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **  upper left element of the ranges \a row_range and \a col_range 
  **  associated to the %GrayscaleImage.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range upper_left(const Range<int>& row_range,
			      const Range<int>& col_range);

  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **   past the end bottom right element of the ranges \a row_range 
  **   and \a col_range associated to the %GrayscaleImage.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range bottom_right(const Range<int>& row_range,
				const Range<int>& col_range);


  /*!
  **  \brief Returns a read-only iterator2d_range that points to the
  **   to the upper left element of the ranges \a row_range and \a col_range 
  **   associated to the %GrayscaleImage.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid.
  ** \pre The ranges indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range upper_left(const Range<int>& row_range,
				    const Range<int>& col_range) const;


 /*!
  **  \brief Returns a read-only iterator2d_range that points to the past
  **  the end bottom right element of the ranges \a row_range and \a col_range 
  **  associated to the %GrayscaleImage.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range bottom_right(const Range<int>& row_range,
				      const Range<int>& col_range) const;


 


  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **  upper left element of the %Range \a range associated to the %GrayscaleImage.
  **  The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range and must be valid. 
  ** \pre The range indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range upper_left(const Range<int>& range);
 

  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **   past the end bottom right element of the %Range \a range 
  **   associated to the %GrayscaleImage.
  **   The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range bottom_right(const Range<int>& range);

 

  
  /*!
  **  \brief Returns a read-only iterator2d_range that points to the
  **   to the upper left element of the %Range \a range
  **   associated to the %GrayscaleImage.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range must be valid.
  ** \pre The range indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range upper_left(const Range<int>& range) const;

 

  /*!
  **  \brief Returns a read-only const_iterator2d_range that points to the 
  **   past the end bottom right element of the %Range \a range 
  **   associated to the %GrayscaleImage.
  **   The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GrayscaleImage<double> A1(8,8);
  **  slip::GrayscaleImage<double> A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range bottom_right(const Range<int>& range) const;

 
  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to the 
  **   bottom right element of the %GrayscaleImage. 
  *    Iteration is done within the %GrayscaleImage in the reverse order.
  **  
  ** \return reverse_iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rupper_left();
  
  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to past the 
  **  upper left element of the %GrayscaleImage.
  **  Iteration is done in the reverse order.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rbottom_right();

  /*!
  **  \brief Returns a read only reverse iterator2d that points.  It points 
  **  to the bottom right element of the %GrayscaleImage. 
  **  Iteration is done within the %GrayscaleImage in the reverse order.
  **  
  ** \return const_reverse_iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rupper_left() const;
 
 
  /*!
  **  \brief Returns a read only reverse iterator2d. It points to past the 
  **  upper left element of the %GrayscaleImage.
  **  Iteration is done in the reverse order.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rbottom_right() const;
  

  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to the 
  **  bottom right element of the \a %Box2d associated to the %GrayscaleImage.
  **  Iteration is done in the reverse order.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %GrayscaleImage.
  **
  ** \pre The box indices must be inside the range of the %GrayscaleImage ones.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rupper_left(const Box2d<int>& box);

  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to one
  **  before the upper left element of the %Box2d \a box associated to 
  **  the %GrayscaleImage.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %GrayscaleImage.
  **
  ** \pre The box indices must be inside the range of the %GrayscaleImage ones.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rbottom_right(const Box2d<int>& box);

  /*!
  **  \brief Returns a read only reverse iterator2d. It points to the 
  **  bottom right element of the %Box2d \a box associated to the %GrayscaleImage.
  **  Iteration is done in the reverse order.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %GrayscaleImage.
  **
  ** \pre The box indices must be inside the range of the %GrayscaleImage ones.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rupper_left(const Box2d<int>& box) const;


  /*!
  **  \brief Returns a read-only reverse iterator2d. It points to one 
  **  before the element of the bottom right element of the %Box2d 
  **  \a box associated to the %GrayscaleImage.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %GrayscaleImage.
  **
  ** \pre The box indices must be inside the range of the %GrayscaleImage ones.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rbottom_right(const Box2d<int>& box) const;

 
  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to the 
  **  past the bottom right element of the ranges \a row_range and 
  **  \a col_range associated to the %GrayscaleImage. Iteration is done in the 
  **  reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rupper_left(const Range<int>& row_range,
				       const Range<int>& col_range);


 
  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points
  **  to one before the upper left element of the ranges \a row_range 
  **  and \a col_range associated to the %GrayscaleImage. Iteration is done
  **  in the reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rbottom_right(const Range<int>& row_range,
					 const Range<int>& col_range);


 
  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points 
  **   to the past the bottom right element of the ranges \a row_range and 
  **   \a col_range associated to the %GrayscaleImage. Iteration is done in the 
  **   reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid.
  ** \pre The ranges indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rupper_left(const Range<int>& row_range,
					     const Range<int>& col_range) const;

  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points 
  **  to one before the upper left element of the ranges \a row_range 
  **  and \a col_range associated to the %GrayscaleImage.Iteration is done in 
  **  the reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rbottom_right(const Range<int>& row_range,
					       const Range<int>& col_range) const;


 
 /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to the 
  **  bottom right element of the %Range \a range associated to the %GrayscaleImage.
  **  The same range is applied for rows and cols. Iteration is done in the
  **  reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range and must be valid. 
  ** \pre The range indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rupper_left(const Range<int>& range);



  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to  
  **   one before the upper left element of the %Range \a range 
  **   associated to the %GrayscaleImage.
  **   The same range is applied for rows and cols. Iteration is done
  **   in the reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rbottom_right(const Range<int>& range);
  

  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points to the
  **   to the bottom right element of the %Range \a range
  **   associated to the %GrayscaleImage.
  **  The same range is applied for rows and cols. Iteration is done in the
  **  reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range must be valid.
  ** \pre The range indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rupper_left(const Range<int>& range) const;


  /*!
  **  \brief Returns a read_only reverse_iterator2d_range that points 
  **   to one before the upper left element of the %Range \a range 
  **   associated to the %GrayscaleImage.
  **   The same range is applied for rows and cols. Iteration is done in the
  **   reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %GrayscaleImage ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rbottom_right(const Range<int>& range) const;
  

  /*@} End iterators */

  /**
   ** \name i/o operators
   */
  /*@{*/
  
  /*!
  ** \brief Write the GrayscaleImage to the ouput stream
  ** \param out output stream
  ** \param a GrayscaleImage to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, const self& a);

  #ifdef HAVE_MAGICK
   /*!
  ** \brief Reads a GrayscaleImage from a file path name
  ** \param file_path_name
  ** 
  ** \remarks This method uses the ImageMagick library
  ** GrascaleImage<float> and Grascale<double> data value are supposed
  ** to be between [0.0,1.0]
  */ 
  void read(const std::string& file_path_name);

  /*!
  ** \brief Write a GrayscaleImage to a file path name
  ** \param file_path_name
  **
  ** \remarks This method uses the ImageMagick library
  ** GrascaleImage<float> and Grascale<double> data value are supposed
  ** to be between [0.0,1.0]
  */ 
  void write(const std::string& file_path_name) const;

  #else

  void read(const std::string& file_path_name)
  {
    throw std::runtime_error("ImageMagick has not been found: no 'read' method available.");
  }

  void write(const std::string& file_path_name) const
  {
    throw std::runtime_error("ImageMagick has not been found: no 'write' method available.");
  }

  #endif

  /*!
  ** \brief Write the GrayscaleImage to an ASCII file.
  ** \param file_path_name
  **
  */ 
  void write_ascii(const std::string& file_path_name) const
  {
    slip::write_ascii_2d<self,value_type>(*this,file_path_name);
  }

  /*!
  ** \brief Read the GrayscaleImage from an ASCII file.
  ** \param file_path_name
  **
  */ 
  void read_ascii(const std::string& file_path_name)
  {
    slip::read_ascii_2d<self>(*this,file_path_name);
  }
  /*@} End i/o operators */

  /**
   ** \name  Assignment operators and methods
   */
  /*@{*/

  /*!
  ** \brief Assign a GrayscaleImage.
  **
  ** Assign elements of GrayscaleImage in \a rhs
  **
  ** \param rhs GrayscaleImage to get the values from.
  ** \return 
  */
  GrayscaleImage& operator=(const self & rhs);

  /*!
  ** \brief Affects all the element of the %GrayscaleImage by val
  ** \param val affectation value
  ** \return reference to corresponding %GrayscaleImage
  */
  self& operator=(const T& val);

  

   /*!
   ** \brief Fills the container range [begin(),begin()+size()) 
   **        with copies of value
   ** \param value  A reference-to-const of arbitrary type.
   */
  void fill(const T& value)
  {
    std::fill_n(this->begin(),size(),value);
  } 
  
 /*!
  ** \brief Fills the container range [begin(),begin()+size())
  **        with a copy of
  **        the value array
  ** \param value  A pointer of arbitrary type.
  */
  void fill(const T* value)
  {
    std::copy(value,value + size(),this->begin());
  } 

  /*!
  ** \brief Fills the container range [begin(),begin()+size()) 
  **        with a copy of the range [first,last)
  **  \param  first  An input iterator.
  **  \param  last   An input iterator.
  **   
  **
  */
  template<typename InputIterator>
  void fill(InputIterator first,
	    InputIterator last)
  {
    std::copy(first,last,this->begin());
  }
  /*@} End Assignment operators and methods*/

 /**
  ** \name Comparison operators
 */
  /*@{*/
  /*!
  ** \brief GrayscaleImage equality comparison
  ** \param x A %GrayscaleImage
  ** \param y A %GrayscaleImage of the same type of \a x
  ** \return true iff the size and the elements of the Arrays are equal
  ** \pre x.size() == y.size()
  */
  friend bool operator== <>(const GrayscaleImage<T>& x, 
			    const GrayscaleImage<T>& y);

 /*!
  ** \brief GrayscaleImage inequality comparison
  ** \param x A %GrayscaleImage
  ** \param y A %GrayscaleImage of the same type of \a x
  ** \return true if !(x == y) 
  ** \pre x.size() == y.size()
  */
  friend bool operator!= <>(const GrayscaleImage<T>& x, 
			    const GrayscaleImage<T>& y);

 /*!
  ** \brief Less than comparison operator (GrayscaleImage ordering relation)
  ** \param x A %GrayscaleImage
  ** \param y A %GrayscaleImage of the same type of \a x
  ** \return true iff \a x is lexicographically less than \a y
  ** \pre x.size() == y.size()
  */
  friend bool operator< <>(const GrayscaleImage<T>& x, 
			   const GrayscaleImage<T>& y);

 /*!
  ** \brief More than comparison operator
  ** \param x A %GrayscaleImage
  ** \param y A %GrayscaleImage of the same type of \a x
  ** \return true iff y > x 
  ** \pre x.size() == y.size()
  */
  friend bool operator> <>(const GrayscaleImage<T>& x, 
			   const GrayscaleImage<T>& y);

  /*!
  ** \brief Less than equal comparison operator
  ** \param x A %GrayscaleImage
  ** \param y A %GrayscaleImage of the same type of \a x
  ** \return true iff !(y > x) 
  ** \pre x.size() == y.size()
  */
  friend bool operator<= <>(const GrayscaleImage<T>& x, 
			    const GrayscaleImage<T>& y);

  /*!
  ** \brief More than equal comparison operator
  ** \param x A %GrayscaleImage
  ** \param y A %GrayscaleImage of the same type of \a x
  ** \return true iff !(x < y) 
  ** \pre x.size() == y.size()
  */
  friend bool operator>= <>(const GrayscaleImage<T>& x, 
			    const GrayscaleImage<T>& y);


  /*@} Comparison operators */

  /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Subscript access to the row datas contained in the %GrayscaleImage.
  ** \param i The index of the row for which data should be accessed.
  ** \return Read/write pointer to the row data.
  ** \pre i < rows()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  pointer operator[](const size_type i);

  /*!
  ** \brief Subscript access to the row datas contained in the %GrayscaleImage.
  ** \param i The index of the row for which data should be accessed.
  ** \return Read-only (constant) pointer to the row data.
  ** \pre i < rows()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_pointer operator[](const size_type i) const;


  /*!
  ** \brief Subscript access to the data contained in the %GrayscaleImage.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read/write reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const size_type i,
		       const size_type j);
 
  /*!
  ** \brief Subscript access to the data contained in the %GrayscaleImage.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read_only (constant) reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const size_type i,
			     const size_type j) const;

  /*!
  ** \brief Subscript access to the data contained in the %GrayscaleImage.
  ** \param point2d A %Point2d which indicate the subscripts of the data
  **        to access.
  ** \return Read/write reference to data.
  ** \pre point2d must be defined in the range of the %GrayscaleImage.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const Point2d<size_type>& point2d);
 
  /*!
  ** \brief Subscript access to the data contained in the %GrayscaleImage.
  ** \param point2d A %Point2d which indicate the subscripts of the data
  **        to access.
  ** \return Read_only (constant) reference to data.
  ** \pre point2d must be defined in the range of the %GrayscaleImage.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const Point2d<size_type>& point2d) const;


  /*!
  ** \brief Subscript access to the data contained in the %GrayscaleImage.
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  ** \return a copy of the range.
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GrayscaleImage ones.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  self operator()(const Range<int>& row_range,
		  const Range<int>& col_range);

  /*@} End Element access operators */

  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;
  
 /*!
   ** \brief Returns the number of rows (first dimension size) 
   **        in the %GrayscaleImage
   */
  size_type dim1() const;

  /*!
   ** \brief Returns the number of rows (first dimension size) 
   **        in the %GrayscaleImage
   */
  size_type rows() const;
  
  /*!
   ** \brief Returns the height (first dimension size) 
   **        in the %GrayscaleImage
   */
  size_type height() const;
  
  /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %GrayscaleImage
  */
  size_type dim2() const;

   /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %GrayscaleImage
  */
  size_type columns() const;
 
  /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %GrayscaleImage
  */
  size_type cols() const;

  /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %GrayscaleImage
  */
  size_type width() const;

  /*!
  ** \brief Returns the number of elements in the %GrayscaleImage
  */
  size_type size() const;

  /*!
  ** \brief Returns the maximal size (number of elements) in the %GrayscaleImage
  */
  size_type max_size() const;

  /*!
  ** \brief Returns true if the %GrayscaleImage is empty.  (Thus size() == 0)
  */
  bool empty()const;

  /*!
  ** \brief Swaps data with another %GrayscaleImage.
  ** \param M A %GrayscaleImage of the same element type
  */
  void swap(self& M);

 /**
   ** \name  Arithmetic operators
   */
  /*@{*/
  /*!
  ** \brief Add val to each element of the %GrayscaleImage  
  ** \param val value
  ** \return reference to the resulting %GrayscaleImage
  */
  self& operator+=(const T& val);
  self& operator-=(const T& val);
  self& operator*=(const T& val);
  self& operator/=(const T& val);
//   self& operator%=(const T& val);
//   self& operator^=(const T& val);
//   self& operator&=(const T& val);
//   self& operator|=(const T& val);
//   self& operator<<=(const T& val);
//   self& operator>>=(const T& val);


  self  operator-() const;
  //self  operator!() const;

  

  self& operator+=(const self& rhs);
  self& operator-=(const self& rhs);
  self& operator*=(const self& rhs);
  self& operator/=(const self& rhs);
    

  /*@} End Arithmetic operators */
 

  /*!
  ** \brief Returns the min element of the %GrayscaleImage 
  ** according to the operator <
  ** \pre size() != 0
  */
  T& min() const;


  /*!
  ** \brief Returns the max element of the GrayscaleImage 
  ** according to the operator <
  ** \pre size() != 0
  */
  T& max() const;

  /*!
  ** \brief Returns the sum of the elements of the %GrayscaleImage 
  ** \pre size() != 0
  */
  T sum() const;

  /*!
  ** \brief Applys the one-parameter C-function \a fun 
  **        to each element of the %GrayscaleImage
  ** \param fun The one-parameter C function 
  ** \return the resulting %GrayscaleImage
  ** \par Example:
  ** \code
  ** slip::GrayscaleImage<double> M(7,7);
  ** //fill M with values from 1 to 49 with step 1
  ** slip::iota(M.begin(),M.end(),1.0,1.0);
  ** //apply std::sqrt to each element of M
  ** M.apply(std::sqrt);
  **
  ** \endcode
  */
  GrayscaleImage<T>& apply(T (*fun)(T));

  /*!
  ** \brief Applys the one-parameter C-function \a fun 
  **        to each element of the %GrayscaleImage
  ** \param fun The one-const-parameter C function 
  ** \return the resulting %GrayscaleImage
  ** \par Example:
  ** \code
  ** slip::GrayscaleImage<double> M(7,7);
  ** //fill M with values from 1 to 49 with step 1
  ** slip::iota(M.begin(),M.end(),1.0,1.0);
  ** //apply std::sqrt to each element of M
  ** M.apply(std::sqrt);
  **
  ** \endcode
  */
  GrayscaleImage<T>& apply(T (*fun)(const T&));


 private:
  slip::Matrix<T>* matrix_;

 private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
       if(version >= 0)
	{
	  ar & this->matrix_;
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    { 
      if(version >= 0)
	{
	  ar & this->matrix_;
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

  ///double alias
  typedef slip::GrayscaleImage<double> GrayscaleImage_d;
  ///float alias
  typedef slip::GrayscaleImage<float> GrayscaleImage_f;
  ///long alias
  typedef slip::GrayscaleImage<long> GrayscaleImage_l;
  ///unsigned long alias
  typedef slip::GrayscaleImage<unsigned long> GrayscaleImage_ul;
  ///short alias
  typedef slip::GrayscaleImage<short> GrayscaleImage_s;
  ///unsigned long alias
  typedef slip::GrayscaleImage<unsigned short> GrayscaleImage_us;
  ///int alias
  typedef slip::GrayscaleImage<int> GrayscaleImage_i;
  ///unsigned int alias
  typedef slip::GrayscaleImage<unsigned int> GrayscaleImage_ui;
  ///char alias
  typedef slip::GrayscaleImage<char> GrayscaleImage_c;
  ///unsigned char alias
  typedef slip::GrayscaleImage<unsigned char> GrayscaleImage_uc;


}//slip::

namespace slip{
  /*!
  ** \brief pointwise addition of two %GrayscaleImage
  ** \param M1 first %GrayscaleImage 
  ** \param M2 seconf %GrayscaleImage
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %GrayscaleImage
  */
template<typename T>
GrayscaleImage<T> operator+(const GrayscaleImage<T>& M1, 
			    const GrayscaleImage<T>& M2);

 /*!
  ** \brief addition of a scalar to each element of a %GrayscaleImage
  ** \param M1 the %GrayscaleImage 
  ** \param val the scalar
  ** \return resulting %GrayscaleImage
  */
template<typename T>
GrayscaleImage<T> operator+(const GrayscaleImage<T>& M1, 
		    const T& val);

 /*!
  ** \brief addition of a scalar to each element of a %GrayscaleImage
  ** \param val the scalar
  ** \param M1 the %GrayscaleImage  
  ** \return resulting %GrayscaleImage
  */
  template<typename T>
  GrayscaleImage<T> operator+(const T& val, 
		      const GrayscaleImage<T>& M1);
  
/*!
  ** \brief pointwise substraction of two %GrayscaleImage
  ** \param M1 first %GrayscaleImage 
  ** \param M2 seconf %GrayscaleImage
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %GrayscaleImage
  */
template<typename T>
GrayscaleImage<T> operator-(const GrayscaleImage<T>& M1, 
		    const GrayscaleImage<T>& M2);

 /*!
  ** \brief substraction of a scalar to each element of a %GrayscaleImage
  ** \param M1 the %GrayscaleImage 
  ** \param val the scalar
  ** \return resulting %GrayscaleImage
  */
template<typename T>
GrayscaleImage<T> operator-(const GrayscaleImage<T>& M1, 
		    const T& val);

 /*!
  ** \brief substraction of a scalar to each element of a %GrayscaleImage
  ** \param val the scalar
  ** \param M1 the %GrayscaleImage  
  ** \return resulting %GrayscaleImage
  */
  template<typename T>
  GrayscaleImage<T> operator-(const T& val, 
		      const GrayscaleImage<T>& M1);
  
/*!
  ** \brief pointwise multiplication of two %GrayscaleImage
  ** \param M1 first %GrayscaleImage 
  ** \param M2 seconf %GrayscaleImage
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %GrayscaleImage
  */
  template<typename T>
  GrayscaleImage<T> operator*(const GrayscaleImage<T>& M1, 
		      const GrayscaleImage<T>& M2);

 /*!
  ** \brief multiplication of a scalar to each element of a %GrayscaleImage
  ** \param M1 the %GrayscaleImage 
  ** \param val the scalar
  ** \return resulting %GrayscaleImage
  */
  template<typename T>
  GrayscaleImage<T> operator*(const GrayscaleImage<T>& M1, 
		      const T& val);

 /*!
  ** \brief multiplication of a scalar to each element of a %GrayscaleImage
  ** \param val the scalar
  ** \param M1 the %GrayscaleImage  
  ** \return resulting %GrayscaleImage
  */
  template<typename T>
  GrayscaleImage<T> operator*(const T& val, 
		      const GrayscaleImage<T>& M1);
  
  /*!
  ** \brief pointwise division of two %GrayscaleImage
  ** \param M1 first %GrayscaleImage 
  ** \param M2 seconf %GrayscaleImage
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %GrayscaleImage
  */
  template<typename T>
  GrayscaleImage<T> operator/(const GrayscaleImage<T>& M1, 
		      const GrayscaleImage<T>& M2);

 /*!
  ** \brief division of a scalar to each element of a %GrayscaleImage
  ** \param M1 the %GrayscaleImage 
  ** \param val the scalar
  ** \return resulting %GrayscaleImage
  */
  template<typename T>
  GrayscaleImage<T> operator/(const GrayscaleImage<T>& M1, 
		      const T& val);



  /*!
  ** \relates GrayscaleImage
  ** \brief Returns the min element of a GrayscaleImage
  ** \param M1 the GrayscaleImage  
  ** \return the min element
  */
  template<typename T>
  T& min(const GrayscaleImage<T>& M1);
  
  /*!
  ** \relates GrayscaleImage
  ** \brief Returns the max element of a GrayscaleImage
  ** \param M1 the GrayscaleImage  
  ** \return the min element
  */
  template<typename T>
  T& max(const GrayscaleImage<T>& M1);

}//slip::

namespace slip
{
  template<typename T>
  inline
  GrayscaleImage<T>::GrayscaleImage():
    matrix_(new slip::Matrix<T>())
  {}

  template<typename T>
  inline
  GrayscaleImage<T>::GrayscaleImage(const typename GrayscaleImage<T>::size_type height,
				    const typename GrayscaleImage<T>::size_type width):
    matrix_(new slip::Matrix<T>(height,width))
  {}
  
  template<typename T>
  inline
  GrayscaleImage<T>::GrayscaleImage(const typename GrayscaleImage<T>::size_type height,
				    const typename GrayscaleImage<T>::size_type width, 
				    const T& val):
    matrix_(new slip::Matrix<T>(height,width,val))
  {}

  template<typename T>
  inline
  GrayscaleImage<T>::GrayscaleImage(const typename GrayscaleImage<T>::size_type height,
				    const typename GrayscaleImage<T>::size_type width, 
				    const T* val):
    matrix_(new slip::Matrix<T>(height,width,val))   
  {}
  
  template<typename T>
  inline
  GrayscaleImage<T>::GrayscaleImage(const GrayscaleImage<T>& rhs):
    matrix_(new slip::Matrix<T>((*rhs.matrix_)))
  {}

  template<typename T>
  inline
  GrayscaleImage<T>::~GrayscaleImage()
  {
    delete matrix_;
  }

  template<typename T>
  inline
  GrayscaleImage<T>& GrayscaleImage<T>::operator=(const GrayscaleImage<T> & rhs)
  {
    if(this != &rhs)
      {
	*matrix_ = *(rhs.matrix_);
      }
    return *this;
  }

  template<typename T>
  inline
  GrayscaleImage<T>& GrayscaleImage<T>::operator=(const T& val)
  {
    std::fill_n(matrix_->begin(),matrix_->size(),val);
    return *this;
  }
  
  template<typename T>
  inline
  void GrayscaleImage<T>::resize(const typename GrayscaleImage<T>::size_type height,
				 const typename GrayscaleImage<T>::size_type width,
				 const T& val)
  {
    matrix_->resize(height,width,val);
  }

 
  template<typename T>
  inline
  typename GrayscaleImage<T>::iterator GrayscaleImage<T>::begin()
  {
    return matrix_->begin();
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::iterator GrayscaleImage<T>::end()
  {
    return matrix_->end();
  }
  
  template<typename T>
  inline
  typename GrayscaleImage<T>::const_iterator GrayscaleImage<T>::begin() const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->begin();
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_iterator GrayscaleImage<T>::end() const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->end();
  }
  

  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_iterator GrayscaleImage<T>::rbegin()
  {
    return typename GrayscaleImage<T>::reverse_iterator(this->end());
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_iterator GrayscaleImage<T>::rend()
  {
    return typename GrayscaleImage<T>::reverse_iterator(this->begin());
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_iterator 
  GrayscaleImage<T>::rbegin() const
  {
    return typename GrayscaleImage<T>::const_reverse_iterator(this->end());
  }

  template<typename T>
  inline
  std::reverse_iterator<const T*> 
  GrayscaleImage<T>::rend() const
  {
    return std::reverse_iterator<const T*>(begin());
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::row_iterator 
  GrayscaleImage<T>::row_begin(const typename GrayscaleImage<T>::size_type row)
  {
    return matrix_->row_begin(row);
  }

 template<typename T>
 inline
 typename GrayscaleImage<T>::row_iterator 
 GrayscaleImage<T>::row_end(const typename GrayscaleImage<T>::size_type row)
 {
    return matrix_->row_end(row);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::col_iterator
  GrayscaleImage<T>::col_begin(const typename GrayscaleImage<T>::size_type col)
  {
    return matrix_->col_begin(col);
  }


  template<typename T>
  inline
  typename GrayscaleImage<T>::col_iterator 
  GrayscaleImage<T>::col_end(const typename GrayscaleImage<T>::size_type col)
  {
    return matrix_->col_end(col);
  }


 template<typename T>
 inline
 typename GrayscaleImage<T>::const_row_iterator 
 GrayscaleImage<T>::row_begin(const typename GrayscaleImage<T>::size_type row) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->row_begin(row);
  }

 template<typename T>
 inline
 typename GrayscaleImage<T>::const_row_iterator 
 GrayscaleImage<T>::row_end(const typename GrayscaleImage<T>::size_type row) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->row_end(row);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_col_iterator 
  GrayscaleImage<T>::col_begin(const typename GrayscaleImage<T>::size_type col) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->col_begin(col);
  }
  
  template<typename T>
  inline
  typename GrayscaleImage<T>::const_col_iterator
  GrayscaleImage<T>::col_end(const typename GrayscaleImage<T>::size_type col) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->col_end(col);
  }


  //
 template<typename T>
 inline
 typename GrayscaleImage<T>::reverse_row_iterator 
 GrayscaleImage<T>::row_rbegin(const typename GrayscaleImage<T>::size_type row)
  {
    return matrix_->row_rbegin(row);
  }
 
  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_row_iterator  
  GrayscaleImage<T>::row_rbegin(const typename GrayscaleImage<T>::size_type row) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->row_rbegin(row);
  }
  
  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_row_iterator 
  GrayscaleImage<T>::row_rend(const typename GrayscaleImage<T>::size_type row)
  {
    return matrix_->row_rend(row);
  }
 
  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_row_iterator  
  GrayscaleImage<T>::row_rend(const typename GrayscaleImage<T>::size_type row) const
  {
   slip::Matrix<T> const * tp(matrix_);
    return tp->row_rend(row);
  }
  
  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_col_iterator
  GrayscaleImage<T>::col_rbegin(const typename GrayscaleImage<T>::size_type col)
  {
    return matrix_->col_rbegin(col);
  }
 
  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_col_iterator GrayscaleImage<T>::col_rbegin(const typename GrayscaleImage<T>::size_type col) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->col_rbegin(col);
  }
  
  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_col_iterator 
  GrayscaleImage<T>::col_rend(const typename GrayscaleImage<T>::size_type col)
  {
    return matrix_->col_rend(col);
  }
 
  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_col_iterator
  GrayscaleImage<T>::col_rend(const typename GrayscaleImage<T>::size_type col) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->col_rend(col);
  }

   template<typename T>
  inline
  typename GrayscaleImage<T>::row_range_iterator 
  GrayscaleImage<T>::row_begin(const typename GrayscaleImage<T>::size_type row,
		       const slip::Range<int>& range)
  {
    return matrix_->row_begin(row,range);
  }

   template<typename T>
  inline
   typename GrayscaleImage<T>::const_row_range_iterator 
   GrayscaleImage<T>::row_begin(const typename GrayscaleImage<T>::size_type row,
			const slip::Range<int>& range) const
   {
     slip::Matrix<T> const * tp(matrix_);
     return tp->row_begin(row,range);
   }

  template<typename T>
  inline
  typename GrayscaleImage<T>::col_range_iterator 
  GrayscaleImage<T>::col_begin(const typename GrayscaleImage<T>::size_type col,
		       const slip::Range<int>& range)
  {
    return matrix_->col_begin(col,range);
  }

   template<typename T>
  inline
   typename GrayscaleImage<T>::const_col_range_iterator 
   GrayscaleImage<T>::col_begin(const typename GrayscaleImage<T>::size_type col,
			const slip::Range<int>& range) const
   {
     slip::Matrix<T> const * tp(matrix_);
    return tp->col_begin(col,range);
   }
  

  template<typename T>
  inline
  typename GrayscaleImage<T>::row_range_iterator 
  GrayscaleImage<T>::row_end(const typename GrayscaleImage<T>::size_type row,
		       const slip::Range<int>& range)
  {
    return matrix_->row_end(row,range);
  }

   template<typename T>
  inline
   typename GrayscaleImage<T>::const_row_range_iterator 
   GrayscaleImage<T>::row_end(const typename GrayscaleImage<T>::size_type row,
			const slip::Range<int>& range) const
   {
     slip::Matrix<T> const * tp(matrix_);
     return tp->row_end(row,range);
   }

  template<typename T>
  inline
  typename GrayscaleImage<T>::col_range_iterator 
  GrayscaleImage<T>::col_end(const typename GrayscaleImage<T>::size_type col,
		     const slip::Range<int>& range)
  {
    return matrix_->col_end(col,range);
  }

   template<typename T>
  inline
   typename GrayscaleImage<T>::const_col_range_iterator 
   GrayscaleImage<T>::col_end(const typename GrayscaleImage<T>::size_type col,
		      const slip::Range<int>& range) const
   {
     slip::Matrix<T> const * tp(matrix_);
     return tp->col_end(col,range);
   }

  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_row_range_iterator 
  GrayscaleImage<T>::row_rbegin(const typename GrayscaleImage<T>::size_type row,
			const slip::Range<int>& range)
  {
     return typename GrayscaleImage<T>::reverse_row_range_iterator(this->row_end(row,range));
  }

 template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_row_range_iterator 
  GrayscaleImage<T>::row_rbegin(const typename GrayscaleImage<T>::size_type row,
			 const slip::Range<int>& range) const
  {
    return typename GrayscaleImage<T>::const_reverse_row_range_iterator(this->row_end(row,range));
  }


  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_col_range_iterator 
  GrayscaleImage<T>::col_rbegin(const typename GrayscaleImage<T>::size_type col,
			 const slip::Range<int>& range)
  {
    return typename GrayscaleImage<T>::reverse_col_range_iterator(this->col_end(col,range));
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_col_range_iterator 
  GrayscaleImage<T>::col_rbegin(const typename GrayscaleImage<T>::size_type col,
			 const slip::Range<int>& range) const
  {
    return typename GrayscaleImage<T>::const_reverse_col_range_iterator(this->col_end(col,range));
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_row_range_iterator 
  GrayscaleImage<T>::row_rend(const typename GrayscaleImage<T>::size_type row,
		       const slip::Range<int>& range)
  {
    return typename GrayscaleImage<T>::reverse_row_range_iterator(this->row_begin(row,range));
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_row_range_iterator 
  GrayscaleImage<T>::row_rend(const typename GrayscaleImage<T>::size_type row,
			 const slip::Range<int>& range) const
  {
    return typename GrayscaleImage<T>::const_reverse_row_range_iterator(this->row_begin(row,range));
  }


  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_col_range_iterator 
  GrayscaleImage<T>::col_rend(const typename GrayscaleImage<T>::size_type col,
		       const slip::Range<int>& range)
  {
    return typename GrayscaleImage<T>::reverse_col_range_iterator(this->col_begin(col,range));
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_col_range_iterator 
  GrayscaleImage<T>::col_rend(const typename GrayscaleImage<T>::size_type col,
		       const slip::Range<int>& range) const
  {
    return typename GrayscaleImage<T>::const_reverse_col_range_iterator(this->col_begin(col,range));
  }




  template<typename T>
  inline
  typename GrayscaleImage<T>::iterator2d GrayscaleImage<T>::upper_left()
  {
    return  matrix_->upper_left();
  }

   template<typename T>
  inline
  typename GrayscaleImage<T>::const_iterator2d GrayscaleImage<T>::upper_left() const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->upper_left();
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::iterator2d GrayscaleImage<T>::bottom_right()
  {
    return matrix_->bottom_right();
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_iterator2d GrayscaleImage<T>::bottom_right() const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->bottom_right();
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::iterator2d 
  GrayscaleImage<T>::upper_left(const Box2d<int>& box)
  {
    return matrix_->upper_left(box);
  }


  template<typename T>
  inline
  typename GrayscaleImage<T>::const_iterator2d GrayscaleImage<T>::upper_left(const Box2d<int>& box) const
  {
   slip::Matrix<T> const * tp(matrix_);
    return tp->upper_left(box);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::iterator2d 
  GrayscaleImage<T>::bottom_right(const Box2d<int>& box)
  {
    return matrix_->bottom_right(box);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_iterator2d GrayscaleImage<T>::bottom_right(const Box2d<int>& box) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->bottom_right(box);
  }

   template<typename T>
  inline
  typename GrayscaleImage<T>::iterator2d_range 
  GrayscaleImage<T>::upper_left(const Range<int>& row_range,
			 const Range<int>& col_range)
  {
    return matrix_->upper_left(row_range,col_range);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_iterator2d_range 
  GrayscaleImage<T>::upper_left(const Range<int>& row_range,
			const Range<int>& col_range) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->upper_left(row_range,col_range);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::iterator2d_range 
  GrayscaleImage<T>::bottom_right(const Range<int>& row_range,
			  const Range<int>& col_range)
  {
    return matrix_->bottom_right(row_range,col_range);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_iterator2d_range 
  GrayscaleImage<T>::bottom_right(const Range<int>& row_range,
			  const Range<int>& col_range) const
  {
   slip::Matrix<T> const * tp(matrix_);
    return tp->bottom_right(row_range,col_range);
  }

   template<typename T>
  inline
  typename GrayscaleImage<T>::iterator2d_range 
  GrayscaleImage<T>::upper_left(const Range<int>& range)
  {
    return matrix_->upper_left(range);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_iterator2d_range 
  GrayscaleImage<T>::upper_left(const Range<int>& range) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->upper_left(range);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::iterator2d_range 
  GrayscaleImage<T>::bottom_right(const Range<int>& range)
  {
    return matrix_->bottom_right(range);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_iterator2d_range 
  GrayscaleImage<T>::bottom_right(const Range<int>& range) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->bottom_right(range);
  }

  
    template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_iterator2d 
  GrayscaleImage<T>::rbottom_right()
  {
    return matrix_->rbottom_right();
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_iterator2d 
  GrayscaleImage<T>::rbottom_right() const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->rbottom_right();
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_iterator2d 
  GrayscaleImage<T>::rupper_left()
  {
    return matrix_->rupper_left();
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_iterator2d 
  GrayscaleImage<T>::rupper_left() const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->rupper_left();
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_iterator2d 
  GrayscaleImage<T>::rbottom_right(const Box2d<int>& box)
  {
    return matrix_->rbottom_right(box);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_iterator2d 
  GrayscaleImage<T>::rbottom_right(const Box2d<int>& box) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->rbottom_right(box);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_iterator2d 
  GrayscaleImage<T>::rupper_left(const Box2d<int>& box)
  {
    return matrix_->rupper_left(box);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_iterator2d 
  GrayscaleImage<T>::rupper_left(const Box2d<int>& box) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->rupper_left(box);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_iterator2d_range 
  GrayscaleImage<T>::rupper_left(const Range<int>& row_range,
			  const Range<int>& col_range)
  {
    return matrix_->rupper_left(row_range,col_range);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_iterator2d_range 
  GrayscaleImage<T>::rupper_left(const Range<int>& row_range,
			 const Range<int>& col_range) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->rupper_left(row_range,col_range);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_iterator2d_range 
  GrayscaleImage<T>::rbottom_right(const Range<int>& row_range,
			    const Range<int>& col_range)
  {
    return matrix_->rbottom_right(row_range,col_range);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_iterator2d_range 
  GrayscaleImage<T>::rbottom_right(const Range<int>& row_range,
			    const Range<int>& col_range) const
  {
    slip::Matrix<T> const * tp(matrix_);
    return tp->rbottom_right(row_range,col_range);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_iterator2d_range 
  GrayscaleImage<T>::rupper_left(const Range<int>& range)
  {
    return this->rupper_left(range,range);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_iterator2d_range 
  GrayscaleImage<T>::rupper_left(const Range<int>& range) const
  {
    return this->rupper_left(range,range);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::reverse_iterator2d_range 
  GrayscaleImage<T>::rbottom_right(const Range<int>& range)
  {
    return this->rbottom_right(range,range);
  }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reverse_iterator2d_range 
  GrayscaleImage<T>::rbottom_right(const Range<int>& range) const
  {
    return this->rbottom_right(range,range);
  }

  template <typename T>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const GrayscaleImage<T>& a)
  {
    out<<*(a.matrix_)<<std::endl;
    return out;
  }

  //////////////////
  #ifdef HAVE_MAGICK
  template<>
  inline
  void GrayscaleImage<unsigned char>::read(const std::string& file_path_name)
  {
    slip::read_magick_gray_char(file_path_name,*this);
  }

   template<>
  inline
  void GrayscaleImage<short>::read(const std::string& file_path_name)
  {
    slip::read_magick_gray_short(file_path_name,*this);
  }

  template<>
  inline
  void GrayscaleImage<int>::read(const std::string& file_path_name)
  {
    slip::read_magick_gray_int(file_path_name,*this);
  }

  template<>
  inline
  void GrayscaleImage<long>::read(const std::string& file_path_name)
  {
    slip::read_magick_gray_long(file_path_name,*this);
  }

  template<>
  inline
  void GrayscaleImage<float>::read(const std::string& file_path_name)
  {
    slip::read_magick_gray_float(file_path_name,*this);
  }

  template<>
  inline
  void GrayscaleImage<double>::read(const std::string& file_path_name)
  {
    slip::read_magick_gray_double(file_path_name,*this);
  }
#endif
  ////////////////////////

  template<>
  inline
  void GrayscaleImage<unsigned char>::write_ascii(const std::string& file_path_name) const
  {
    slip::write_ascii_2d<self,int>(*this,file_path_name); 
  }
#ifdef HAVE_MAGICK
  template<>
  inline
  void GrayscaleImage<unsigned char>::write(const std::string& file_path_name) const
  {
    slip::write_magick_gray_char(this->begin(),
				 this->width(),this->height(),
				 file_path_name);
  }

 template<>
  inline
  void GrayscaleImage<short>::write(const std::string& file_path_name) const
  {
    slip::write_magick_gray_short(this->begin(),
				  this->width(),this->height(),
				  file_path_name);
  }

  template<>
  inline
  void GrayscaleImage<int>::write(const std::string& file_path_name) const
  {
     slip::write_magick_gray_int(this->begin(),
				 this->width(),this->height(),
				 file_path_name);
  }

  template<>
  inline
  void GrayscaleImage<long>::write(const std::string& file_path_name) const
  {
      slip::write_magick_gray_long(this->begin(),
				   this->width(),this->height(),
				   file_path_name);
  }

  template<>
  inline
  void GrayscaleImage<float>::write(const std::string& file_path_name) const
  {
    slip::write_magick_gray_float(this->begin(),
				  this->width(),this->height(),
				  file_path_name);
  }

  template<>
  inline
  void GrayscaleImage<double>::write(const std::string& file_path_name) const
  {
    slip::write_magick_gray_double(this->begin(),
				   this->width(),this->height(),
				   file_path_name);
  }
#endif
  /////////////////////////

  template<typename T>
  inline
  typename GrayscaleImage<T>::pointer 
  GrayscaleImage<T>::operator[](const typename GrayscaleImage<T>::size_type i)
  {
    return (*matrix_)[i];
  }
  
  template<typename T>
  inline
  typename GrayscaleImage<T>::const_pointer  
  GrayscaleImage<T>::operator[](const typename GrayscaleImage<T>::size_type i) const 
  {
    return (*matrix_)[i];
  }
  
  template<typename T>
  inline
  typename GrayscaleImage<T>::reference
  GrayscaleImage<T>::operator()(const typename GrayscaleImage<T>::size_type i,
				const typename GrayscaleImage<T>::size_type j)
  {
    return (*matrix_)[i][j];
  }
  
  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reference 
  GrayscaleImage<T>::operator()(const typename GrayscaleImage<T>::size_type i,
				const typename GrayscaleImage<T>::size_type j) const
  {
    return (*matrix_)[i][j];
  }

 template<typename T>
  inline
  typename GrayscaleImage<T>::reference 
  GrayscaleImage<T>::operator()(const Point2d<typename GrayscaleImage<T>::size_type>& point2d)
   {
     return (*matrix_)(point2d);
   }

  template<typename T>
  inline
  typename GrayscaleImage<T>::const_reference 
  GrayscaleImage<T>::operator()(const Point2d<typename GrayscaleImage<T>::size_type>& point2d) const
   {
     return (*matrix_)(point2d);
   }


  template<typename T>
  inline
  GrayscaleImage<T> 
  GrayscaleImage<T>::operator()(const Range<int>& row_range,
			 const Range<int>& col_range)
  {
    assert((*this)[0] != 0);
    assert(row_range.is_valid());
    assert(row_range.start() < int(this->dim2()));
    assert(row_range.stop()  < int(this->dim2()));
    assert(col_range.is_valid());
    assert(col_range.start() < int(this->dim1()));
    assert(col_range.stop()  < int(this->dim1()));
    std::size_t rows = row_range.iterations();
    std::size_t cols = col_range.iterations();
    return GrayscaleImage<T>(rows+1,cols+1,matrix_->upper_left(row_range,col_range),
		       matrix_->bottom_right(row_range,col_range));
  } 
  

  template<typename T>
  inline
  std::string 
  GrayscaleImage<T>::name() const {return "GrayscaleImage";} 

  template<typename T>
  inline
  typename GrayscaleImage<T>::size_type 
  GrayscaleImage<T>::dim1() const {return matrix_->dim1();} 
 
  template<typename T>
  inline
  typename GrayscaleImage<T>::size_type 
  GrayscaleImage<T>::height() const {return matrix_->dim1();} 
 
  template<typename T>
  inline
  typename GrayscaleImage<T>::size_type 
  GrayscaleImage<T>::rows() const {return matrix_->dim1();} 
 
  template<typename T>
  inline
  typename GrayscaleImage<T>::size_type 
  GrayscaleImage<T>::dim2() const {return matrix_->dim2();} 
 
  template<typename T>
  inline
  typename GrayscaleImage<T>::size_type 
  GrayscaleImage<T>::width() const {return matrix_->dim2();} 
 
  template<typename T>
  inline
  typename GrayscaleImage<T>::size_type 
  GrayscaleImage<T>::columns() const {return matrix_->dim2();} 

  template<typename T>
  inline
  typename GrayscaleImage<T>::size_type 
  GrayscaleImage<T>::cols() const {return matrix_->dim2();} 
 
  template<typename T>
  inline
  typename GrayscaleImage<T>::size_type 
  GrayscaleImage<T>::size() const {return matrix_->size();}

  template<typename T>
  inline
  typename GrayscaleImage<T>::size_type 
  GrayscaleImage<T>::max_size() const 
  {
    return typename GrayscaleImage<T>::size_type(-1)/sizeof(T);
  }

  
  template<typename T>
  inline
  bool GrayscaleImage<T>::empty()const {return matrix_->empty();}

  template<typename T>
  inline
  void GrayscaleImage<T>::swap(GrayscaleImage<T>& M)
  {
    matrix_->swap(*(M.matrix_));
  }

 template<typename T>
  inline
  GrayscaleImage<T>& GrayscaleImage<T>::operator+=(const T& val)
  {
    matrix_->operator+=(val);
    return *this;
  }
  
  template<typename T>
  inline
  GrayscaleImage<T>& GrayscaleImage<T>::operator-=(const T& val)
  {
    matrix_->operator-=(val);
    return *this;

  }

  template<typename T>
  inline
  GrayscaleImage<T>& GrayscaleImage<T>::operator*=(const T& val)
  {
    matrix_->operator*=(val);
    return *this;
  }
  
  template<typename T>
  inline
  GrayscaleImage<T>& GrayscaleImage<T>::operator/=(const T& val)
  {
    matrix_->operator/=(val);
    return *this;
  }
  
  template<typename T>
  inline
  GrayscaleImage<T> GrayscaleImage<T>::operator-() const
  {
    *matrix_ = matrix_->operator-();
    return *this;
  }

  template<typename T>
  inline
  GrayscaleImage<T>& GrayscaleImage<T>::operator+=(const GrayscaleImage<T>& rhs)
  {
    matrix_->operator+=(*(rhs.matrix_));
    return *this;
  }
  
  template<typename T>
  inline
  GrayscaleImage<T>& GrayscaleImage<T>::operator-=(const GrayscaleImage<T>& rhs)
  {
    matrix_->operator-=(*(rhs.matrix_));
    return *this;
  }
  
  template<typename T>
  inline
  GrayscaleImage<T>& GrayscaleImage<T>::operator*=(const GrayscaleImage<T>& rhs)
  {
    matrix_->operator*=(*(rhs.matrix_));
    return *this;
  }
  
  template<typename T>
  inline
  GrayscaleImage<T>& GrayscaleImage<T>::operator/=(const GrayscaleImage<T>& rhs)
  {
    matrix_->operator/=(*(rhs.matrix_));
    return *this;
  }
	

  template<typename T>
  inline
  T& GrayscaleImage<T>::min() const
  {
    return matrix_->min();
  }

  template<typename T>
  inline
  T& GrayscaleImage<T>::max() const
  {
    return matrix_->max();
  }

  template<typename T>
  inline
  T GrayscaleImage<T>::sum() const
  {
    return matrix_->sum();
  }

  template<typename T>
  inline
  GrayscaleImage<T>& GrayscaleImage<T>::apply(T (*fun)(T))
  {
    matrix_->apply(fun);
    return *this;
  }

  template<typename T>
  inline
  GrayscaleImage<T>& GrayscaleImage<T>::apply(T (*fun)(const T&))
  {
    matrix_->apply(fun);
    return *this;
  }
  /** \name Arithmetical operators */
  /* @{ */
 template<typename T>
  inline
  GrayscaleImage<T> operator+(const GrayscaleImage<T>& M1, 
			      const GrayscaleImage<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    GrayscaleImage<T> tmp(M1.dim1(),M1.dim2());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::plus<T>());
    return tmp;
  }

  template<typename T>
  inline
  GrayscaleImage<T> operator+(const GrayscaleImage<T>& M1, 
			      const T& val)
  {
    GrayscaleImage<T>  tmp = M1;
    tmp += val;
    return tmp;
  }

  template<typename T>
  inline
  GrayscaleImage<T> operator+(const T& val,
			      const GrayscaleImage<T>& M1)
  {
    return M1 + val;
  }


template<typename T>
inline
GrayscaleImage<T> operator-(const GrayscaleImage<T>& M1, 
			    const GrayscaleImage<T>& M2)
{
  assert(M1.dim1() == M2.dim1());
  assert(M1.dim2() == M2.dim2());
  GrayscaleImage<T> tmp(M1.dim1(),M1.dim2());
  std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::minus<T>());
  return tmp;
}

template<typename T>
inline
GrayscaleImage<T> operator-(const GrayscaleImage<T>& M1, 
			    const T& val)
{
  GrayscaleImage<T>  tmp = M1;
  tmp -= val;
  return tmp;
}

template<typename T>
inline
GrayscaleImage<T> operator-(const T& val,
			    const GrayscaleImage<T>& M1)
{
  return M1 - val;
}

template<typename T>
inline
GrayscaleImage<T> operator*(const GrayscaleImage<T>& M1, 
			    const GrayscaleImage<T>& M2)
{
  assert(M1.dim1() == M2.dim1());
  assert(M1.dim2() == M2.dim2());
  GrayscaleImage<T> tmp(M1.dim1(),M1.dim2());
  std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::multiplies<T>());
  return tmp;
}

template<typename T>
inline
GrayscaleImage<T> operator*(const GrayscaleImage<T>& M1, 
			    const T& val)
{
  GrayscaleImage<T>  tmp = M1;
  tmp *= val;
  return tmp;
}

template<typename T>
inline
GrayscaleImage<T> operator*(const T& val,
			    const GrayscaleImage<T>& M1)
{
  return M1 * val;
}

template<typename T>
inline
GrayscaleImage<T> operator/(const GrayscaleImage<T>& M1, 
			    const GrayscaleImage<T>& M2)
{
  assert(M1.dim1() == M2.dim1());
  assert(M1.dim2() == M2.dim2());
  GrayscaleImage<T> tmp(M1.dim1(),M1.dim2());
  std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::divides<T>());
  return tmp;
}

template<typename T>
inline
GrayscaleImage<T> operator/(const GrayscaleImage<T>& M1, 
		    const T& val)
{
  GrayscaleImage<T>  tmp = M1;
  tmp /= val;
  return tmp;
}


/* @} */


 /*!
  ** \brief Returns the min element of a GrayscaleImage
  ** \param M1 the GrayscaleImage  
  ** \return the min element
  */
template<typename T>
inline
T& min(const GrayscaleImage<T>& M1)
{
  return M1.min();
}
 /*!
  ** \brief Returns the max element of a GrayscaleImage
  ** \param M1 the GrayscaleImage  
  ** \return the max element
  */
template<typename T>
inline
T& max(const GrayscaleImage<T>& M1)
{
  return M1.max();
}

/** \name EqualityComparable functions */
  /* @{ */
template<typename T>
inline
bool operator==(const GrayscaleImage<T>& x, 
		const GrayscaleImage<T>& y)
{
  return ( x.size() == y.size()
	   && std::equal(x.begin(),x.end(),y.begin())); 
}

template<typename T>
inline
bool operator!=(const GrayscaleImage<T>& x, 
		const GrayscaleImage<T>& y)
{
  return !(x == y);
}
/* @} */

/** \name LessThanComparable functions */
  /* @{ */
template<typename T>
inline
bool operator<(const GrayscaleImage<T>& x, 
	       const GrayscaleImage<T>& y)
{
  return std::lexicographical_compare(x.begin(), x.end(),
				      y.begin(), y.end());
}

//MoreThanComparable
template<typename T>
inline
bool operator>(const GrayscaleImage<T>& x, 
	       const GrayscaleImage<T>& y)
{
  return (y < x);
}

//LessThanEqualComparable
template<typename T>
inline
bool operator<=(const GrayscaleImage<T>& x, 
		const GrayscaleImage<T>& y)
{
  return !(y < x);
}

//MoreThanEqualComparable
template<typename T>
inline
bool operator>=(const GrayscaleImage<T>& x, 
		const GrayscaleImage<T>& y)
{
  return !(x < y);
} 
/* @} */

}//slip::

#endif //SLIP_GRAYSCALEIMAGE_HPP
