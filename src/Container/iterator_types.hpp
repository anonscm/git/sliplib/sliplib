/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file iterator_types.hpp
 * 
 * \brief Provides a class to tag SLIP iterators
 * 
 */
#ifndef SLIP_ITERATOR_TYPES_HPP
#define SLIP_ITERATOR_TYPES_HPP

#include <iterator>

namespace std
{
  struct random_access_iterator2d_tag : public bidirectional_iterator_tag {};
  struct random_access_iterator3d_tag : public bidirectional_iterator_tag {};
  struct random_access_iterator4d_tag : public bidirectional_iterator_tag {}; //from FLUEX 1.0 
}

namespace slip
{
//get the number of elements in a range according to iterator dimension
template<typename>
struct __range_size
{
  template<typename RandomAccessIterator1>
  std::size_t operator()(RandomAccessIterator1 first,
			 RandomAccessIterator1 last)
  {
    return static_cast<std::size_t>(last-first);
  }
};

template<>
struct __range_size<std::random_access_iterator_tag>
{
  template<typename RandomAccessIterator1>
  std::size_t operator()(RandomAccessIterator1 first,
		       RandomAccessIterator1 last)
  {
    return static_cast<std::size_t>(last-first);
  }
};

template<>
struct __range_size<std::random_access_iterator2d_tag>
{
  template<typename RandomAccessIterator1>
  std::size_t operator()(RandomAccessIterator1 first,
		       RandomAccessIterator1 last)
  {
    return static_cast<std::size_t>((last-first)[0])*static_cast<std::size_t>((last-first)[1]);
  }
};

template<>
struct __range_size<std::random_access_iterator3d_tag>
{
  template<typename RandomAccessIterator1>
  std::size_t operator()(RandomAccessIterator1 first,
		       RandomAccessIterator1 last)
  {
    return static_cast<std::size_t>((last-first)[0])*static_cast<std::size_t>((last-first)[1])*static_cast<std::size_t>((last-first)[2]);
  }
};
  
template<>
struct __range_size<std::random_access_iterator4d_tag>
{
  template<typename RandomAccessIterator1>
  std::size_t operator()(RandomAccessIterator1 first,
		       RandomAccessIterator1 last)
  {
    return static_cast<std::size_t>((last-first)[0])*static_cast<std::size_t>((last-first)[1])*static_cast<std::size_t>((last-first)[2])*static_cast<std::size_t>((last-first)[3]);
  }
};
}//::slip


#endif //SLIP_ITERATOR_TYPES_HPP
