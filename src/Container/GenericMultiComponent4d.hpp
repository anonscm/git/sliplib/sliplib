/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/*!
 ** \file GenericMultiComponent4d.hpp
 ** \brief Provides a class to manipulate multicomponent 4d containers.
 ** \version Fluex 1.0
 ** \date 2013/08/20
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */
#ifndef SLIP_GENERICMULTICOMPONENT4D_HPP
#define SLIP_GENERICMULTICOMPONENT4D_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <cmath>
#include <string>
#include <vector>
#include <cstddef>
#include "Matrix4d.hpp"
#include "Matrix.hpp"
#include "stride_iterator.hpp"
#include "kstride_iterator.hpp"
#include "apply.hpp"
#include "iterator4d_box.hpp"
#include "iterator4d_range.hpp"

#include "component_iterator4d_box.hpp"
#include "component_iterator4d_range.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template<class Block>
class stride_iterator;

}//slip::

namespace slip
{

template <class Block>
class DPoint4d;

template <class Block>
class Point4d;

template <class Block>
class Box4d;

template <typename Block>
class GenericMultiComponent4d;

template <typename Block>
class Matrix4d;

template <class MultiComponentContainer4D, std::size_t N>
class component_iterator4d_box;

template <class MultiComponentContainer4d, std::size_t N>
class component_iterator4d_range;

template <typename Block>
std::ostream& operator<<(std::ostream & out, const GenericMultiComponent4d<Block>& a);

template<typename Block>
bool operator==(const GenericMultiComponent4d<Block>& x,
		const GenericMultiComponent4d<Block>& y);

template<typename Block>
bool operator!=(const GenericMultiComponent4d<Block>& x,
		const GenericMultiComponent4d<Block>& y);

// template<typename Block>
// bool operator<(const GenericMultiComponent4d<Block>& x,
// 	       const GenericMultiComponent4d<Block>& y);

// template<typename Block>
// bool operator>(const GenericMultiComponent4d<Block>& x,
// 	       const GenericMultiComponent4d<Block>& y);

// template<typename Block>
// bool operator<=(const GenericMultiComponent4d<Block>& x,
// 		const GenericMultiComponent4d<Block>& y);

// template<typename Block>
// bool operator>=(const GenericMultiComponent4d<Block>& x,
// 		const GenericMultiComponent4d<Block>& y);

/*!
 *  @defgroup MultiComponent4dContainers Multi-component 4d Containers
 *  @brief 4d multicomponents containers having the same interface
 *  @{
 */
/*!
 ** \ingroup Containers Containers4d Fluex MultiComponent4dContainers
 ** \class GenericMultiComponent4d
 ** \version Fluex 1.0
 ** \date 2013/08/20
 ** \version 0.0.2
 ** \date 2014/04/05
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 **\brief This is a GenericMultiComponent4d class.
 ** This container statisfies the BidirectionnalContainer concepts of the STL.
 ** It is also an 4d extension of the RandomAccessContainer concept. That is
 ** to say the bracket element access is replaced by the triple bracket element
 ** access.
 ** Data are stored using a %Matrix4d of Block.
 ** This class can be used to define 4d Multicomponent structures.
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \version Fluex 1.0
 ** \date 2013/08/20
 ** \param Block Type of object in the GenericMultiComponent4d
 **        Must be a static block
 ** \par Axis conventions:
 ** \image html iterator4d_conventions.jpg "axis and notation conventions"
 ** \image latex iterator4d_conventions.eps "axis and notation conventions" width=5cm
 **
 */
template <typename Block>
class GenericMultiComponent4d
{
public :

	typedef Block value_type;
	typedef GenericMultiComponent4d<Block> self;
	typedef const GenericMultiComponent4d<Block> const_self;
	typedef value_type* pointer;
	typedef value_type const * const_pointer;
	typedef value_type& reference;
	typedef value_type const & const_reference;

	typedef ptrdiff_t difference_type;
	typedef std::size_t size_type;

	typedef pointer iterator;
	typedef const_pointer const_iterator;

	typedef std::reverse_iterator<iterator> reverse_iterator;
	typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

	//slab, slice, row and col iterators
	typedef slip::stride_iterator<pointer> slab_iterator;
	typedef slip::stride_iterator<const_pointer> const_slab_iterator;
	typedef slip::stride_iterator<pointer> slice_iterator;
	typedef slip::stride_iterator<const_pointer> const_slice_iterator;
	typedef pointer row_iterator;
	typedef const_pointer const_row_iterator;
	typedef slip::stride_iterator<pointer> col_iterator;
	typedef slip::stride_iterator<const_pointer> const_col_iterator;
	//slab, slice, row and col range iterators
	typedef slip::stride_iterator<slab_iterator> slab_range_iterator;
	typedef slip::stride_iterator<const_slab_iterator> const_slab_range_iterator;
	typedef slip::stride_iterator<slice_iterator> slice_range_iterator;
	typedef slip::stride_iterator<const_slice_iterator> const_slice_range_iterator;
	typedef slip::stride_iterator<pointer> row_range_iterator;
	typedef slip::stride_iterator<const_pointer> const_row_range_iterator;
	typedef slip::stride_iterator<col_iterator> col_range_iterator;
	typedef slip::stride_iterator<const_col_iterator> const_col_range_iterator;
	//slab, slice, row and col reverse iterators
	typedef std::reverse_iterator<slab_iterator> reverse_slab_iterator;
	typedef std::reverse_iterator<const_slab_iterator> const_reverse_slab_iterator;
	typedef std::reverse_iterator<slice_iterator> reverse_slice_iterator;
	typedef std::reverse_iterator<const_slice_iterator> const_reverse_slice_iterator;
	typedef std::reverse_iterator<iterator> reverse_row_iterator;
	typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
	typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
	typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;
	//slab, slice, row and col reverse range iterators
	typedef std::reverse_iterator<slab_range_iterator> reverse_slab_range_iterator;
	typedef std::reverse_iterator<const_slab_range_iterator> const_reverse_slab_range_iterator;
	typedef std::reverse_iterator<slice_range_iterator> reverse_slice_range_iterator;
	typedef std::reverse_iterator<const_slice_range_iterator> const_reverse_slice_range_iterator;
	typedef std::reverse_iterator<row_range_iterator> reverse_row_range_iterator;
	typedef std::reverse_iterator<const_row_range_iterator> const_reverse_row_range_iterator;
	typedef std::reverse_iterator<col_range_iterator> reverse_col_range_iterator;
	typedef std::reverse_iterator<const_col_range_iterator> const_reverse_col_range_iterator;

	//Component iterators
	typedef typename Block::value_type block_value_type;
	typedef block_value_type* block_pointer;
	typedef const block_value_type* const_block_pointer;
	typedef block_value_type& block_reference;
	typedef const block_value_type const_block_reference;
	typedef slip::kstride_iterator<typename Block::pointer,Block::SIZE> component_iterator;
	typedef slip::kstride_iterator<typename Block::const_pointer,Block::SIZE> const_component_iterator;
	//Component reverse iterators
	typedef std::reverse_iterator<component_iterator> reverse_component_iterator;
	typedef std::reverse_iterator<const_component_iterator> const_reverse_component_iterator;

	//component slab, slice, row and col iterator
	typedef slip::stride_iterator<block_pointer> component_slab_iterator;
	typedef slip::stride_iterator<const_block_pointer> const_component_slab_iterator;
	typedef slip::stride_iterator<block_pointer> component_slice_iterator;
	typedef slip::stride_iterator<const_block_pointer> const_component_slice_iterator;
	typedef component_iterator component_row_iterator;
	typedef const_component_iterator const_component_row_iterator;
	typedef slip::stride_iterator<block_pointer> component_col_iterator;
	typedef slip::stride_iterator<const_block_pointer> const_component_col_iterator;
	//component slab, slice, row and col range iterator
	typedef slip::stride_iterator<block_pointer> component_slab_range_iterator;
	typedef slip::stride_iterator<const_block_pointer> const_component_slab_range_iterator;
	typedef slip::stride_iterator<block_pointer> component_slice_range_iterator;
	typedef slip::stride_iterator<const_block_pointer> const_component_slice_range_iterator;
	typedef slip::stride_iterator<block_pointer> component_row_range_iterator;
	typedef slip::stride_iterator<const_block_pointer> const_component_row_range_iterator;
	typedef slip::stride_iterator<block_pointer> component_col_range_iterator;
	typedef slip::stride_iterator<const_block_pointer> const_component_col_range_iterator;
	//component slab, slice, row and col reverse iterator
	typedef std::reverse_iterator<component_slab_iterator> reverse_component_slab_iterator;
	typedef std::reverse_iterator<const_component_slab_iterator> const_reverse_component_slab_iterator;
	typedef std::reverse_iterator<component_slice_iterator> reverse_component_slice_iterator;
	typedef std::reverse_iterator<const_component_slice_iterator> const_reverse_component_slice_iterator;
	typedef std::reverse_iterator<component_row_iterator> reverse_component_row_iterator;
	typedef std::reverse_iterator<const_component_row_iterator> const_reverse_component_row_iterator;
	typedef std::reverse_iterator<component_col_iterator> reverse_component_col_iterator;
	typedef std::reverse_iterator<const_component_col_iterator> const_reverse_component_col_iterator;
	//component slab, slice, row and col reverse range iterator
	typedef std::reverse_iterator<component_slab_range_iterator> reverse_component_slab_range_iterator;
	typedef std::reverse_iterator<const_component_slab_range_iterator> const_reverse_component_slab_range_iterator;
	typedef std::reverse_iterator<component_slice_range_iterator> reverse_component_slice_range_iterator;
	typedef std::reverse_iterator<const_component_slice_range_iterator> const_reverse_component_slice_range_iterator;
	typedef std::reverse_iterator<component_row_range_iterator> reverse_component_row_range_iterator;
	typedef std::reverse_iterator<const_component_row_range_iterator> const_reverse_component_row_range_iterator;
	typedef std::reverse_iterator<component_col_range_iterator> reverse_component_col_range_iterator;
	typedef std::reverse_iterator<const_component_col_range_iterator> const_reverse_component_col_range_iterator;

	//iterator 4d
	typedef typename Array4d<Block>::iterator4d iterator4d;
	typedef typename Array4d<Block>::const_iterator4d const_iterator4d;
	typedef typename Array4d<Block>::iterator4d_range iterator4d_range;
	typedef typename Array4d<Block>::const_iterator4d_range const_iterator4d_range;
	//iterator reverse 4d
	typedef std::reverse_iterator<iterator4d> reverse_iterator4d;
	typedef std::reverse_iterator<const_iterator4d> const_reverse_iterator4d;
	typedef std::reverse_iterator<iterator4d_range> reverse_iterator4d_range;
	typedef std::reverse_iterator<const_iterator4d_range> const_reverse_iterator4d_range;


	//component iterator 4d
	typedef component_iterator4d_box<self,Block::SIZE> component_iterator4d;
	typedef const_component_iterator4d_box<const_self,Block::SIZE> const_component_iterator4d;
	typedef slip::component_iterator4d_range<self,Block::SIZE> component_iterator4d_range;
	typedef slip::const_component_iterator4d_range<const_self,Block::SIZE> const_component_iterator4d_range;
	//component reverse iterator 4d
	typedef std::reverse_iterator<component_iterator4d> reverse_component_iterator4d;
	typedef std::reverse_iterator<const_component_iterator4d> const_reverse_component_iterator4d;
	typedef std::reverse_iterator<component_iterator4d_range> reverse_component_iterator4d_range;
	typedef std::reverse_iterator<const_component_iterator4d_range> const_reverse_component_iterator4d_range;


	//default iterator of the container
	typedef iterator4d default_iterator;
	typedef const_iterator4d const_default_iterator;


	static const std::size_t DIM = 4;
	static const std::size_t COMPONENTS = Block::SIZE;

public:
	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** \brief Constructs a %GenericMultiComponent4d.
	 */
	GenericMultiComponent4d();

	/*!
	 ** \brief Constructs a %GenericMultiComponent4d.
	 ** \param slabs first dimension of the %GenericMultiComponent4d
	 ** \param slices second dimension of the %GenericMultiComponent4d
	 ** \param rows third dimension of the %GenericMultiComponent4d
	 ** \param cols fourth dimension of the %GenericMultiComponent4d
	 */
	GenericMultiComponent4d(const size_type slabs, const size_type slices,
			const size_type rows,
			const size_type cols);

	/*!
	 ** \brief Constructs a %GenericMultiComponent4d initialized by the scalar value \a val.
	 ** \param slabs first dimension of the %GenericMultiComponent4d
	 ** \param slices second dimension of the %GenericMultiComponent4d
	 ** \param rows third dimension of the %GenericMultiComponent4d
	 ** \param cols fourth dimension of the %GenericMultiComponent4d
	 ** \param val initialization value of the elements
	 */
	GenericMultiComponent4d(const size_type slabs, const size_type slices,
			const size_type rows,
			const size_type cols,
			const Block& val);

	/*!
	 ** \brief Constructs a %GenericMultiComponent4d initialized by an array \a val.
	 ** \param slabs first dimension of the %GenericMultiComponent4d
	 ** \param slices second dimension of the %GenericMultiComponent4d
	 ** \param rows third dimension of the %GenericMultiComponent4d
	 ** \param cols fourth dimension of the %GenericMultiComponent4d
	 ** \param val initialization linear array value of the elements
	 */
	GenericMultiComponent4d(const size_type slabs, const size_type slices,
			const size_type rows,
			const size_type cols,
			typename Block::const_pointer val);

	/*!
	 ** \brief Constructs a %GenericMultiComponent4d initialized by an array \a val.
	 ** \param slabs first dimension of the %GenericMultiComponent4d
	 ** \param slices second dimension of the %GenericMultiComponent4d
	 ** \param rows third dimension of the %GenericMultiComponent4d
	 ** \param cols fourth dimension of the %GenericMultiComponent4d
	 ** \param val initialization array value of the elements
	 */
	GenericMultiComponent4d(const size_type slabs, const size_type slices,
			const size_type rows,
			const size_type cols,
			const Block* val);


	/**
	 ** \brief  Contructs a %GenericMultiComponent4d from a range.
	 ** \param slabs first dimension of the %GenericMultiComponent4d
	 ** \param slices second dimension of the %GenericMultiComponent4d
	 ** \param rows third dimension of the %GenericMultiComponent4d
	 ** \param cols fourth dimension of the %GenericMultiComponent4d
	 ** \param  first  An input iterator.
	 ** \param  last  An input iterator.
	 **
	 ** Create a %GenericMultiComponent4d consisting of copies of the elements from
	 ** [first,last).
	 */
	template<typename InputIterator>
	GenericMultiComponent4d(const size_type slabs, const size_type slices,
			const size_type rows,
			const size_type cols,
			InputIterator first,
			InputIterator last)
			{
		matrix_ = new Matrix4d<Block>(slabs,slices,rows,cols,first,last);
			}

	/**
	 ** \brief  Contructs a %GenericMultiComponent4d from a 3 ranges.
	 ** \param slabs first dimension of the %GenericMultiComponent4d
	 ** \param slices second dimension of the %GenericMultiComponent4d
	 ** \param rows third dimension of the %GenericMultiComponent4d
	 ** \param cols fourth dimension of the %GenericMultiComponent4d
	 ** \param  first_iterators_list  A std::vector of the first input iterators.
	 ** \param  last  An input iterator.
	 **
	 **
	 ** Create a %GenericMultiComponent4d consisting of copies of the elements from
	 ** [first_iterators_list[0],last),
	 ** [first_iterators_list[1],first_iterators_list[1] + (last - first_iterators_list[0]),
	 ** ...
	 */
	template<typename InputIterator>
	GenericMultiComponent4d(const size_type slabs, const size_type slices,
			const size_type rows,
			const size_type cols,
			std::vector<InputIterator> first_iterators_list,
			InputIterator last)

			{
		matrix_ = new Matrix4d<Block>(slabs,slices,rows,cols);
		this->fill(first_iterators_list,last);
			}


	/*!
	 ** \brief Constructs a copy of the %GenericMultiComponent4d \a rhs
	 */
	GenericMultiComponent4d(const self& rhs);

	/*!
	 ** \brief Destructor of the %GenericMultiComponent4d
	 */
	virtual ~GenericMultiComponent4d();


	/*@} End Constructors */


	/*!
	 ** \brief Resizes a GenericMultiComponent4d.
	 ** \param slabs first dimension of the %GenericMultiComponent4d
	 ** \param slices second dimension of the %GenericMultiComponent4d
	 ** \param rows third dimension of the %GenericMultiComponent4d
	 ** \param cols fourth dimension of the %GenericMultiComponent4d
	 ** \param val new value for all the elements
	 */
	void resize(const size_type slabs,
			const size_type slices,
			const size_type rows,
			const size_type cols,
			const Block& val = Block());

	/**
	 ** \name One dimensional global iterators
	 */
	/*@{*/

	//****************************************************************************
	//                          One dimensional iterators
	//****************************************************************************



	//----------------------Global iterators------------------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element in the %GenericMultiComponent4d. Iteration is done in ordinary
	 **  element order.
	 ** \return begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > A2(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	iterator begin();

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  first element in the %GenericMultiComponent4d.  Iteration is done in ordinary
	 **  element order.
	 ** \return const begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent4d<slip::color<double> > const A1(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > const A2(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	const_iterator begin() const;

	/*!
	 **  \brief Returns a read/write iterator that points one past the last
	 **  element in the %GenericMultiComponent4d.  Iteration is done in ordinary
	 **  element order.
	 ** \return end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > A2(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	iterator end();

	/*!
	 **  \brief Returns a read-only (constant) iterator that points one past
	 **  the last element in the %GenericMultiComponent4d.  Iteration is done in
	 **  ordinary element order.
	 ** \return const end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent4d<slip::color<double> > const A1(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > const A2(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > S(10,9,5);
	 ** \endcode
	 */
	const_iterator end() const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the
	 **  last element in the %GenericMultiComponent4d.  Iteration is done in reverse
	 **  element order.
	 ** \return reverse begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > A2(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	reverse_iterator rbegin();

	/*!
	 **  \brief Returns a read-only (constant) reverse iterator that points
	 **  to the last element in the %GenericMultiComponent4d.  Iteration is done in
	 **  reverse element order.
	 ** \return const reverse begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent4d<slip::color<double> > const A1(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > const A2(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	const_reverse_iterator rbegin() const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to one
	 **  before the first element in the %GenericMultiComponent4d.  Iteration is done
	 **  in reverse element order.
	 **  \return reverse end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > A2(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	reverse_iterator rend();

	/*!
	 **  \brief Returns a read-only (constant) reverse iterator that points
	 **  to one before the first element in the %GenericMultiComponent4d.  Iteration
	 **  is done in reverse element order.
	 **  \return const reverse end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent4d<slip::color<double> > const A1(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > const A2(10,9,5);
	 **  slip::GenericMultiComponent4d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	const_reverse_iterator rend() const;

	/*@} End One dimensional global iterators */

	/**
	 ** \name One dimensional slab iterators
	 */
	/*@{*/
	//--------------------One dimensional slab iterators----------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **	 Iteration is done in ordinary element order (increasing slab number).
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(10);
	 **  //copy the line defined by the 4th slice, the 3rd row and the 3rd column of A1 in S
	 **  std::copy(A1.slab_begin(4,3,3),A1.slab_end(4,3,3),S.begin());
	 **  \endcode
	 */
	slab_iterator slab_begin(const size_type slice, const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the first
	 **  element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **	 Iteration is done in ordinary element order (increasing slab number).
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return const_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(10);
	 **  //copy the line defined by the 4th slice, the 3rd row and the 3rd column of A1 in S
	 **  std::copy(A1.slab_begin(4,3,3),A1.slab_end(4,3,3),S.begin());
	 **  \endcode
	 */
	const_slab_iterator slab_begin(const size_type slice, const size_type row, const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the one past the end
	 **  element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **	 Iteration is done in ordinary element order (increasing slab number).
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(10);
	 **  //copy the line defined by the 4th slice, the 3rd row and the 3rd column of A1 in S
	 **  std::copy(A1.slab_begin(4,3,3),A1.slab_end(4,3,3),S.begin());
	 **  \endcode
	 */
	slab_iterator slab_end(const size_type slice, const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the one past the end
	 **  element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **	 Iteration is done in ordinary element order (increasing slab number).
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return const_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(10);
	 **  //copy the line defined by the 4th slice, the 3rd row and the 3rd column of A1 in S
	 **  std::copy(A1.slab_begin(4,3,3),A1.slab_end(4,3,3),S.begin());
	 **  \endcode
	 */
	const_slab_iterator slab_end(const size_type slice, const size_type row, const size_type col) const;


	/*!
	 **  \brief Returns a read/write iterator that points to the
	 **  last element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **  Iteration is done in reverse element order (decreasing slab number).
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 ** 	\return reverse_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(10);
	 **  //copy the line defined by the 4th slice, the 3rd row and the 3rd column of A1
	 **  //in S in reverse order
	 **  std::copy(A1.slab_rbegin(4,3,3),A1.slab_rend(4,3,3),S.begin());
	 **  \endcode
	 */
	reverse_slab_iterator slab_rbegin(const size_type slice, const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  last element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **	 Iteration is done in reverse element order (decreasing slab number).
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return const_reverse_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(10);
	 **  //copy the line defined by the 4th slice, the 3rd row and the 3rd column of A1
	 **  //in S in reverse order
	 **  std::copy(A1.slab_rbegin(4,3,3),A1.slab_rend(4,3,3),S.begin());
	 **  \endcode
	 */
	const_reverse_slab_iterator slab_rbegin(const size_type slice, const size_type row, const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the
	 **  one before the first element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **  Iteration is done in reverse element order (decreasing slab number).
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **  \return reverse_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(10);
	 **  //copy the line defined by the 4th slice, the 3rd row and the 3rd column of A1
	 **  //in S in reverse order
	 **  std::copy(A1.slab_rbegin(4,3,3),A1.slab_rend(4,3,3),S.begin());
	 **  \endcode
	 */
	reverse_slab_iterator slab_rend(const size_type slice, const size_type row, const size_type col);


	/*!
	 **  \brief Returns a read (constant) iterator that points to the
	 **  one before the first element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **  Iteration is done in reverse element order (decreasing slab number).
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return const_reverse_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(10);
	 **  //copy the line defined by the 4th slice, the 3rd row and the 3rd column of A1
	 **  //in S in reverse order
	 **  std::copy(A1.slab_rbegin(4,3,3),A1.slab_rend(4,3,3),S.begin());
	 **  \endcode
	 */
	const_reverse_slab_iterator slab_rend(const size_type slice, const size_type row, const size_type col) const;

	/*@} End One dimensional slab iterators */


	/**
	 ** \name One dimensional slice iterators
	 */
	/*@{*/
	//--------------------One dimensional slice iterators----------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 **	 Iteration is done in ordinary element order (increasing slice number).
	 **	 \param slab slab coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(9);
	 **  //copy the line defined by the 4th slab, the 3rd row and the 3rd column of A1
	 **  //in S
	 **  std::copy(A1.slice_begin(4,3,3),A1.slice_end(4,3,3),S.begin());
	 **  \endcode
	 */
	slice_iterator slice_begin(const size_type slab, const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the first
	 **  element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 **	 Iteration is done in ordinary element order (increasing slice number).
	 **	 \param slab slab coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return const_slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(9);
	 **  //copy the line defined by the 4th slab, the 3rd row and the 3rd column of A1
	 **  //in S
	 **  std::copy(A1.slice_begin(4,3,3),A1.slice_end(4,3,3),S.begin());
	 **  \endcode
	 */
	const_slice_iterator slice_begin(const size_type slab, const size_type row, const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the one past the end
	 **  element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 **	 Iteration is done in ordinary element order (increasing slice number).
	 **	 \param slab slab coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(9);
	 **  //copy the line defined by the 4th slab, the 3rd row and the 3rd column of A1
	 **  //in S
	 **  std::copy(A1.slice_begin(4,3,3),A1.slice_end(4,3,3),S.begin());
	 **  \endcode
	 */
	slice_iterator slice_end(const size_type slab, const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the one past the end
	 **  element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 **	 Iteration is done in ordinary element order (increasing slice number).
	 **	 \param slab slab coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return const_slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(9);
	 **  //copy the line defined by the 4th slab, the 3rd row and the 3rd column of A1
	 **  //in S
	 **  std::copy(A1.slice_begin(4,3,3),A1.slice_end(4,3,3),S.begin());
	 **  \endcode
	 */
	const_slice_iterator slice_end(const size_type slab, const size_type row, const size_type col) const;


	/*!
	 **  \brief Returns a read/write iterator that points to the
	 **  last element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 **  Iteration is done in reverse element order (decreasing slice number).
	 **	 \param slab slab coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 ** 	\return reverse_slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(9);
	 **  //copy the line defined by the 4th slab, the 3rd row and the 3rd column of A1
	 **  //in S in reverse order
	 **  std::copy(A1.slice_rbegin(4,3,3),A1.slice_rend(4,3,3),S.begin());
	 **  \endcode
	 */
	reverse_slice_iterator slice_rbegin(const size_type slab, const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  last element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 **	 Iteration is done in reverse element order (decreasing slice number).
	 **	 \param slab slab coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return const_reverse_slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(9);
	 **  //copy the line defined by the 4th slab, the 3rd row and the 3rd column of A1
	 **  //in S in reverse order
	 **  std::copy(A1.slice_rbegin(4,3,3),A1.slice_rend(4,3,3),S.begin());
	 **  \endcode
	 */
	const_reverse_slice_iterator slice_rbegin(const size_type slab, const size_type row, const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the
	 **  one before the first element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 **  Iteration is done in reverse element order (decreasing slice number).
	 **	 \param slab slab coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **  \return reverse_slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(9);
	 **  //copy the line defined by the 4th slab, the 3rd row and the 3rd column of A1
	 **  //in S in reverse order
	 **  std::copy(A1.slice_rbegin(4,3,3),A1.slice_rend(4,3,3),S.begin());
	 **  \endcode
	 */
	reverse_slice_iterator slice_rend(const size_type slab, const size_type row, const size_type col);


	/*!
	 **  \brief Returns a read (constant) iterator that points to the
	 **  one before the first element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 **  Iteration is done in reverse element order (decreasing slice number).
	 **	 \param slab slab coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return const_reverse_slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<slip::color<double> > S(9);
	 **  //copy the line defined by the 4th slab, the 3rd row and the 3rd column of A1
	 **  //in S in reverse order
	 **  std::copy(A1.slice_rbegin(4,3,3),A1.slice_rend(4,3,3),S.begin());
	 **  \endcode
	 */
	const_reverse_slice_iterator slice_rend(const size_type slab,
			const size_type row, const size_type col) const;

	/*@} End One dimensional slice iterators */

	/**
	 ** \name One dimensional row iterators
	 */
	/*@{*/
	//-------------------row iterators----------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the row \a row of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done in ordinary element order.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	row_iterator row_begin(const size_type slab, const size_type slice,
			const size_type row);

	/*!
	 **  \brief Returns a read_only iterator that points to the first
	 **  element of the row \a row of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done in ordinary element order.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin const_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_row_iterator row_begin(const size_type slab, const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the row \a row of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done in ordinary element order.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	row_iterator row_end(const size_type slab, const size_type slice,
			const size_type row);


	/*!
	 **  \brief Returns a read_only iterator that points to the past-the-end
	 **  element of the row \a row of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done in ordinary element order.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end const_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_row_iterator row_end(const size_type slab, const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the last
	 **  element of the row \a row of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done in reverse element order.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	reverse_row_iterator row_rbegin(const size_type slab, const size_type slice,
			const size_type row);

	/*!
	 **  \brief Returns a read_only reverse iterator that points to the last
	 **  element of the row \a row of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done in reverse element order.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin const_reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_reverse_row_iterator row_rbegin(const size_type slab, const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the row \a row of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done in reverse element order.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	reverse_row_iterator row_rend(const size_type slab, const size_type slice,
			const size_type row);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the first
	 **  element of the row \a row of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done in reverse element order.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end const_reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_reverse_row_iterator row_rend(const size_type slab, const size_type slice,
			const size_type row) const;


	/*@} End One dimensional row iterators */
	/**
	 ** \name One dimensional col iterators
	 */
	/*@{*/
	//-------------------Constant col iterators----------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the column \a column of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done modulo the number of columns.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	col_iterator col_begin(const size_type slab, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only iterator that points to the first
	 **  element of the column \a column of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done modulo the number of columns.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin const_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_col_iterator col_begin(const size_type slab, const size_type slice,
			const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the column \a column of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done modulo the number of columns.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	col_iterator col_end(const size_type slab, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only iterator that points to the past-the-end
	 **  element of the column \a column of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done modulo the number of columns.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end const_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_col_iterator col_end(const size_type slab, const size_type slice,
			const size_type col) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the last
	 **  element of the column \a column of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done modulo the number of columns.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	reverse_col_iterator col_rbegin(const size_type slab, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the last
	 **  element of the column \a column of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done modulo the number of columns.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin const_reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_reverse_col_iterator col_rbegin(const size_type slab, const size_type slice,
			const size_type col) const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the column \a column of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done modulo the number of columns.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	reverse_col_iterator col_rend(const size_type slab, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the first
	 **  element of the column \a column of the slice \a slice and of the slab \a slab
	 **  in the %GenericMultiComponent4d.
	 **  Iteration is done modulo the number of columns.
	 **	 \param slab slab coordinate of the line
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end const_reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_reverse_col_iterator col_rend(const size_type slab, const size_type slice,
			const size_type col) const;


	/*@} End One dimensional col iterators */

	/**
	 ** \name One dimensional slab range iterators
	 */
	/*@{*/
	//------------------------slab range iterators -----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the line defined by ( \a slice , \a row , \a col )
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slab_begin(4,3,3,range),A1.slab_end(4,3,3,range),S.begin());
	 ** \endcode
	 */
	slab_range_iterator slab_begin(const size_type slice, const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points one past the end
	 ** element of the %Range \a range of the line defined by ( \a slice , \a row , \a col )
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slab_begin(4,3,3,range),A1.slab_end(4,3,3,range),S.begin());
	 ** \endcode
	 */
	slab_range_iterator slab_end(const size_type slice, const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the first
	 ** element of the %Range \a range of the line defined by ( \a slice , \a row , \a col )
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slab_begin(4,3,3,range),A1.slab_end(4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_slab_range_iterator slab_begin(const size_type slice, const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the last
	 ** element of the %Range \a range of the line defined by ( \a slice , \a row , \a col )
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slab_begin(4,3,3,range),A1.slab_end(4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_slab_range_iterator slab_end(const size_type slice, const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the last
	 ** element of the %Range \a range of the line defined by ( \a slice , \a row , \a col )
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin reverse_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slab_rbegin(4,3,3,range),A1.slab_rend(4,3,3,range),S.begin());
	 ** \endcode
	 */
	reverse_slab_range_iterator slab_rbegin(const size_type slice, const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the line defined by ( \a slice , \a row , \a col )
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end reverse_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slab_rbegin(4,3,3,range),A1.slab_rend(4,3,3,range),S.begin());
	 ** \endcode
	 */
	reverse_slab_range_iterator slab_rend(const size_type slice, const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the last
	 ** element of the %Range \a range of the line defined by ( \a slice , \a row , \a col )
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_reverse_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slab_rbegin(4,3,3,range),A1.slab_rend(4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_slab_range_iterator slab_rbegin(const size_type slice, const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the lastto the first
	 ** element of the %Range \a range of the line defined by ( \a slice , \a row , \a col )
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_reverse_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slab_rbegin(4,3,3,range),A1.slab_rend(4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_slab_range_iterator slab_rend(const size_type slice, const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*@} End One dimensional slab range iterators */

	/**
	 ** \name One dimensional slice range iterators
	 */
	/*@{*/
	//------------------------slice range iterators -----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the line defined by ( \a slab , \a row , \a col )
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slab,
	 **  //the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slice_begin(4,3,3,range),A1.slice_end(4,3,3,range),S.begin());
	 ** \endcode
	 */
	slice_range_iterator slice_begin(const size_type slab, const size_type row, const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points one past the end
	 ** element of the %Range \a range of the line defined by ( \a slab , \a row , \a col )
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slab,
	 **  //the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slice_begin(4,3,3,range),A1.slice_end(4,3,3,range),S.begin());
	 ** \endcode
	 */
	slice_range_iterator slice_end(const size_type slab, const size_type row, const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the first
	 ** element of the %Range \a range of the line defined by ( \a slab , \a row , \a col )
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slab,
	 **  //the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slice_begin(4,3,3,range),A1.slice_end(4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_slice_range_iterator slice_begin(const size_type slab, const size_type row, const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the last
	 ** element of the %Range \a range of the line defined by ( \a slab , \a row , \a col )
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slab,
	 **  //the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slice_begin(4,3,3,range),A1.slice_end(4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_slice_range_iterator slice_end(const size_type slab, const size_type row, const size_type col,
			const slip::Range<int>& range) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the last
	 ** element of the %Range \a range of the line defined by ( \a slab , \a row , \a col )
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slab,
	 **  //the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slice_rbegin(4,3,3,range),A1.slice_rend(4,3,3,range),S.begin());
	 ** \endcode
	 */
	reverse_slice_range_iterator slice_rbegin(const size_type slab, const size_type row, const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the line defined by ( \a slab , \a row , \a col )
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slab,
	 **  //the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slice_rbegin(4,3,3,range),A1.slice_rend(4,3,3,range),S.begin());
	 ** \endcode
	 */
	reverse_slice_range_iterator slice_rend(const size_type slab, const size_type row, const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the last
	 ** element of the %Range \a range of the line defined by ( \a slab , \a row , \a col )
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slab,
	 **  //the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slice_rbegin(4,3,3,range),A1.slice_rend(4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_slice_range_iterator slice_rbegin(const size_type slab, const size_type row, const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the lastto the first
	 ** element of the %Range \a range of the line defined by ( \a slab , \a row , \a col )
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the line defined by the 4th slab,
	 **  //the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slice_rbegin(4,3,3,range),A1.slice_rend(4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_slice_range_iterator slice_rend(const size_type slab, const size_type row, const size_type col,
			const slip::Range<int>& range) const;


	/*@} End One dimensional slice range iterators */

	/**
	 ** \name One dimensional row range iterators
	 */
	/*@{*/
	//------------------------row range iterators -----------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the %Range \a range of the row \a row of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice the index of the slice containing the row to iterate.
	 ** \param row the index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd row of the 5th slab
	 **  //and the 4th slice of A1 in S.
	 **  std::copy(A1.row_begin(5,4,3,range),A1.row_end(5,4,3,range),S.begin());
	 ** \endcode
	 */
	row_range_iterator row_begin(const size_type slab, const size_type slice, const size_type row,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read/write iterator that points one past the end
	 **  element of the %Range \a range of the row \a row of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice the index of the slice containing the row to iterate.
	 ** \param row the index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return end row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd row of the 5th slab
	 **  //and the 4th slice of A1 in S.
	 **  std::copy(A1.row_begin(5,4,3,range),A1.row_end(5,4,3,range),S.begin());
	 ** \endcode
	 */
	row_range_iterator row_end(const size_type slab, const size_type slice, const size_type row,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the row \a row of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice the index of the slice containing the row to iterate.
	 ** \param row the index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin const_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd row of the 5th slab
	 **  //and the 4th slice of A1 in S.
	 **  std::copy(A1.row_begin(5,4,3,range),A1.row_end(5,4,3,range),S.begin());
	 ** \endcode
	 */
	const_row_range_iterator row_begin(const size_type slab, const size_type slice, const size_type row,
			const slip::Range<int>& range) const;


	/*!
	 **  \brief Returns a read_only iterator that points one past the last
	 **  element of the %Range range of the row \a row of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice the index of the slice containing the row to iterate.
	 ** \param row the index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin const_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd row of the 5th slab
	 **  //and the 4th slice of A1 in S.
	 **  std::copy(A1.row_begin(5,4,3,range),A1.row_end(5,4,3,range),S.begin());
	 ** \endcode
	 */
	const_row_range_iterator row_end(const size_type slab, const size_type slice, const size_type row,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read-write iterator that points to the last
	 ** element of the %Range \a range of the row \a row of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice the index of the slice containing the row to iterate.
	 ** \param row the index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd row of the 5th slab
	 **  //and the 4th slice of A1 in S in reverse order.
	 **  std::copy(A1.row_rbegin(5,4,3,range),A1.row_rend(5,4,3,range),S.begin());
	 ** \endcode
	 */
	reverse_row_range_iterator row_rbegin(const size_type slab, const size_type slice, const size_type row,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-write iterator that points one before
	 **  the first element of the %Range \a range of the row \a row of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice the index of the slice containing the row to iterate.
	 ** \param row the index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd row of the 5th slab
	 **  //and the 4th slice of A1 in S in reverse order.
	 **  std::copy(A1.row_rbegin(5,4,3,range),A1.row_rend(5,4,3,range),S.begin());
	 ** \endcode
	 */
	reverse_row_range_iterator row_rend(const size_type slab, const size_type slice, const size_type row,
			const slip::Range<int>& range);



	/*!
	 **  \brief Returns a read-only iterator that points to the last
	 **  element of the %Range \a range of the row \a row of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice the index of the slice containing the row to iterate.
	 ** \param row the index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin const_reverse_row_range_iterator value
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd row of the 5th slab
	 **  //and the 4th slice of A1 in S in reverse order.
	 **  std::copy(A1.row_rbegin(5,4,3,range),A1.row_rend(5,4,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_row_range_iterator row_rbegin(const size_type slab, const size_type slice, const size_type row,
			const slip::Range<int>& range) const;


	/*!
	 **  \brief Returns a read-only iterator that points one before the first
	 **  element of the %Range \a range of the row \a row of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice the index of the slice containing the row to iterate.
	 ** \param row the index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return const_reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd row of the 5th slab
	 **  //and the 4th slice of A1 in S in reverse order.
	 **  std::copy(A1.row_rbegin(5,4,3,range),A1.row_rend(5,4,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_row_range_iterator row_rend(const size_type slab, const size_type slice, const size_type row,
			const slip::Range<int>& range) const;

	/*@} End One dimensional row range iterators */
	/**
	 ** \name One dimensional col range iterators
	 */
	/*@{*/
	//------------------------col range iterators -----------------------


	/*!
	 **  \brief Returns a read-write iterator that points to the first
	 **  element of the %Range \a range of the col \a col of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate
	 ** \return begin col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd column of the 5th slab
	 **  //and the 4th slice of A1 in S.
	 **  std::copy(A1.col_begin(5,4,3,range),A1.col_end(5,4,3,range),S.begin());
	 ** \endcode
	 */
	col_range_iterator col_begin(const size_type slab, const size_type slice, const size_type col,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read-write iterator that points to the past
	 **  the end element of the %Range \a range of the col \a col of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd column of the 5th slab
	 **  //and the 4th slice of A1 in S.
	 **  std::copy(A1.col_begin(5,4,3,range),A1.col_end(5,4,3,range),S.begin());
	 ** \endcode
	 */
	col_range_iterator col_end(const size_type slab, const size_type slice, const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the col \a col of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd column of the 5th slab
	 **  //and the 4th slice of A1 in S.
	 **  std::copy(A1.col_begin(5,4,3,range),A1.col_end(5,4,3,range),S.begin());
	 ** \endcode
	 */
	const_col_range_iterator col_begin(const size_type slab, const size_type slice, const size_type col,
			const slip::Range<int>& range) const;


	/*!
	 **  \brief Returns a read-only iterator that points to the past
	 **  the end element of the %Range \a range of the col \a col of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate
	 ** \return begin const_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd column of the 5th slab
	 **  //and the 4th slice of A1 in S.
	 **  std::copy(A1.col_begin(5,4,3,range),A1.col_end(5,4,3,range),S.begin());
	 ** \endcode
	 */
	const_col_range_iterator col_end(const size_type slab, const size_type slice, const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 **  \brief Returns a read-write iterator that points to the last
	 **  element of the %Range \a range of the col \a col of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd column of the 5th slab
	 **  //and the 4th slice of A1 in S in reverse order.
	 **  std::copy(A1.col_rbegin(5,4,3,range),A1.col_rend(5,4,3,range),S.begin());
	 ** \endcode
	 */
	reverse_col_range_iterator col_rbegin(const size_type slab, const size_type slice,
			const size_type col, const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read-write iterator that points to one before
	 **  the first element of the %Range range of the col \a col of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd column of the 5th slab
	 **  //and the 4th slice of A1 in S in reverse order.
	 **  std::copy(A1.col_rbegin(5,4,3,range),A1.col_rend(5,4,3,range),S.begin());
	 ** \endcode
	 */
	reverse_col_range_iterator col_rend(const size_type slab, const size_type slice,
			const size_type col, const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read_only iterator that points to the last
	 **  element of the %Range \& range of the col \a col of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd column of the 5th slab
	 **  //and the 4th slice of A1 in S.
	 **  std::copy(A1.col_begin(5,4,3,range),A1.col_end(5,4,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_col_range_iterator col_rbegin(const size_type slab, const size_type slice,
			const size_type col, const slip::Range<int>& range) const;

	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the col \a col of the slab \a slab
	 **  and the slice \a slice in the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab the index of the slab containing the row to iterate.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return const_reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example:
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,8,4,6);
	 **  slip::Vector<slip::color<double> > S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the 3rd column of the 5th slab
	 **  //and the 4th slice of A1 in S.
	 **  std::copy(A1.col_begin(5,4,3,range),A1.col_end(5,4,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_col_range_iterator col_rend(const size_type slab, const size_type slice,
			const size_type col, const slip::Range<int>& range) const;

	/*@} End One dimensional col range iterators */


	//****************************************************************************
	//                          Four dimensional iterators
	//****************************************************************************

	/**
	 ** \name Four dimensional iterators : Global iterators
	 */
	/*@{*/

	//------------------------ Global iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator4d that points to the first
	 **  element of the %GenericMultiComponent4d. It points to the first front upper left element of
	 **  the %GenericMultiComponent4d.
	 **
	 **  \return begin iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   //Print M
	 **   std::copy(M.first_front_upper_left(),M.last_back_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator4d first_front_upper_left();


	/*!
	 **  \brief Returns a read/write iterator4d that points to the past
	 **  the end element of the %GenericMultiComponent4d. It points to past the end element of
	 **  the last back bottom right element of the %GenericMultiComponent4d.
	 **
	 **  \return begin iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   //Print M
	 **   std::copy(M.first_front_upper_left(),M.last_back_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator4d last_back_bottom_right();


	/*!
	 **  \brief Returns a read-only iterator4d that points to the first
	 **  element of the %GenericMultiComponent4d. It points to the first front upper left element of
	 **  the %GenericMultiComponent4d.
	 **
	 **  \return begin const iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > const M(3,4,5,4);
	 **   //Print M
	 **   std::copy(M.first_front_upper_left(),M.last_back_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d first_front_upper_left() const;


	/*!
	 **  \brief Returns a read-only iterator4d that points to the past
	 **  the end element of the %GenericMultiComponent4d. It points to past the end element of
	 **  the last back bottom right element of the %GenericMultiComponent4d.
	 **
	 **  \return begin const iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > const M(3,4,5,4);
	 **   //Print M
	 **   std::copy(M.first_front_upper_left(),M.last_back_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d last_back_bottom_right() const;

	/*!
	 **  \brief Returns a read/write reverse iterator4d. It points to the
	 **   last back bottom right element of the %GenericMultiComponent4d.
	 *    Iteration is done within the %GenericMultiComponent4d in the reverse order.
	 **
	 **  \return reverse_iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   //Print M in reverse order
	 **   std::copy(M.rfirst_front_upper_left(),M.rlast_back_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d rfirst_front_upper_left();

	/*!
	 **  \brief Returns a read/write reverse iterator4d. It points to past the
	 **  first front upper left element of the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse order.
	 **
	 **  \return reverse iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   //Print M in reverse order
	 **   std::copy(M.rfirst_front_upper_left(),M.rlast_back_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d rlast_back_bottom_right();

	/*!
	 **  \brief Returns a read only reverse iterator4d that points.  It points
	 **  to the last back bottom right element of the %GenericMultiComponent4d.
	 **  Iteration is done within the %GenericMultiComponent4d in the reverse order.
	 **
	 **  \return const_reverse_iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > const M(3,4,5,4);
	 **   //Print M in reverse order
	 **   std::copy(M.rfirst_front_upper_left(),M.rlast_back_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d rfirst_front_upper_left() const;


	/*!
	 **  \brief Returns a read only reverse iterator4d. It points to past the
	 **  first front upper left element of the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \return const reverse iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > const M(3,4,5,4);
	 **   //Print M in reverse order
	 **   std::copy(M.rfirst_front_upper_left(),M.rlast_back_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d rlast_back_bottom_right() const;

	/*@} End four dimensional iterators : Global iterators */

	/**
	 ** \name four dimensional iterators : Box iterators
	 */
	/*@{*/

	//------------------------ Box iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator4d that points to the first
	 **  element of the %GenericMultiComponent4d. It points to the first front upper left element of
	 **  the \a %Box4d associated to the %GenericMultiComponent4d.
	 **
	 **  \param box A %Box4d defining the range of indices to iterate
	 **         within the %GenericMultiComponent4d.
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **
	 **  \return end iterator4d value
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M within a box
	 **   std::copy(M.first_front_upper_left(b),M.last_back_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator4d first_front_upper_left(const Box4d<int>& box);


	/*!
	 **  \brief Returns a read/write iterator4d that points to the past
	 **  the end element of the %GenericMultiComponent4d. It points to past the end element of
	 **  the last back bottom right element of the \a %Box4d associated to the %GenericMultiComponent4d.
	 **
	 **  \param box a %Box4d defining the range of indices to iterate
	 **         within the %GenericMultiComponent4d.
	 **
	 **  \return end iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M within a box
	 **   std::copy(M.first_front_upper_left(b),M.last_back_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 **
	 */
	iterator4d last_back_bottom_right(const Box4d<int>& box);


	/*!
	 **  \brief Returns a read only iterator4d that points to the first
	 **   element of the %GenericMultiComponent4d. It points to the first front upper left element of
	 **   the \a %Box4d associated to the %GenericMultiComponent4d.
	 **
	 **  \param box a %Box4d defining the range of indices to iterate
	 **         within the %GenericMultiComponent4d.
	 **  \return end const iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M within a box
	 **   std::copy(M.first_front_upper_left(b),M.last_back_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 **
	 */
	const_iterator4d first_front_upper_left(const Box4d<int>& box) const;


	/*!
	 **  \brief Returns a read only iterator4d that points to the past
	 **   the end element of the %GenericMultiComponent4d. It points to past the end element of
	 **   the last back bottom right element of the \a %Box4d associated to the %GenericMultiComponent4d.
	 **
	 **  \param box a %Box4d defining the range of indices to iterate
	 **         within the %GenericMultiComponent4d.
	 **  \return end const iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M within a box
	 **   std::copy(M.first_front_upper_left(b),M.last_back_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 **
	 */
	const_iterator4d last_back_bottom_right(const Box4d<int>& box) const;



	/*!
	 **  \brief Returns a read/write reverse iterator4d. It points to the
	 **   last back bottom right element of the \a %Box4d associated to the %GenericMultiComponent4d.
	 **   Iteration is done in the reverse order.
	 **
	 **  \param box a %Box4d defining the range of indices to iterate
	 **         within the %GenericMultiComponent4d.
	 **
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 **
	 **  \return reverse iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M within a box in reverse order
	 **   std::copy(M.rfirst_front_upper_left(b),M.rlast_back_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d rfirst_front_upper_left(const Box4d<int>& box);

	/*!
	 **  \brief Returns a read/write reverse iterator4d. It points to one
	 **   before the first front upper left element of the %Box4d \a box associated to
	 **   the %GenericMultiComponent4d.
	 **
	 **  \param box A %Box4d defining the range of indices to iterate
	 **         within the %GenericMultiComponent4d.
	 **
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 **
	 **  \return reverse iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M within a box in reverse order
	 **   std::copy(M.rfirst_front_upper_left(b),M.rlast_back_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d rlast_back_bottom_right(const Box4d<int>& box);

	/*!
	 **  \brief Returns a read only reverse iterator4d. It points to the
	 **   last back bottom right element of the %Box4d \a box associated to the %GenericMultiComponent4d.
	 **   Iteration is done in the reverse order.
	 **
	 **  \param box A %Box4d defining the range of indices to iterate
	 **         within the %GenericMultiComponent4d.
	 **
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 **
	 **  \return const reverse iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M within a box in reverse order
	 **   std::copy(M.rfirst_front_upper_left(b),M.rlast_back_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d rfirst_front_upper_left(const Box4d<int>& box) const;


	/*!
	 **  \brief Returns a read-only reverse iterator4d. It points to one
	 **   before the first front upper left element of the %Box4d
	 **   \a box associated to the %GenericMultiComponent4d.
	 **
	 **  \param box A %Box4d defining the range of indices to iterate
	 **         within the %GenericMultiComponent4d.
	 **
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 **
	 **  \return const reverse iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M within a box in reverse order
	 **   std::copy(M.rfirst_front_upper_left(b),M.rlast_back_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d rlast_back_bottom_right(const Box4d<int>& box) const;


	/*@} End four dimensional iterators : Box iterators */

	/**
	 ** \name four dimensional iterators : Range iterators
	 */

	/*@{*/

	//------------------------ Range iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator4d_range that points to the
	 **  first front upper left element of the ranges \a slab_range, \a slice_range, \a row_range
	 **  and \a col_range associated to the %GenericMultiComponent4d.
	 **
	 **  \param slab_range The range of the slabs.
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slab_range, slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 **  \return iterator4d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Range<int> fr(0,2,2);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.first_front_upper_left(fr,sr,rr,cr),M.last_back_bottom_right(fr,sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator4d_range first_front_upper_left(const slip::Range<int>& slab_range,
			const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
			const slip::Range<int>& col_range);

	/*!
	 **  \brief Returns a read/write iterator4d_range that points to the
	 **   past the end last back bottom right element of the ranges \a slab_range,
	 **   \a slice_range, \a row_range and \a col_range associated to the %GenericMultiComponent4d.
	 **
	 **  \param slab_range The range of the slabs.
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slab_range, slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 **  \return iterator4d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Range<int> fr(0,2,2);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.first_front_upper_left(fr,sr,rr,cr),M.last_back_bottom_right(fr,sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator4d_range last_back_bottom_right(const slip::Range<int>& slab_range,
			const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
			const slip::Range<int>& col_range);

	/*!
	 **  \brief Returns a read-only iterator4d_range that points to the
	 **   to the first front upper left element of the ranges \a slab_range,
	 **   \a slice_range, \a row_range and \a col_range
	 **   associated to the %GenericMultiComponent4d.
	 **
	 **  \param slab_range The range of the slabs.
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slab_range, slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 **  \return const_iterator4d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Range<int> fr(0,2,2);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.first_front_upper_left(fr,sr,rr,cr),M.last_back_bottom_right(fr,sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d_range first_front_upper_left(const slip::Range<int>& slab_range,
			const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
			const slip::Range<int>& col_range) const;


	/*!
	 **  \brief Returns a read-only iterator4d_range that points to the past
	 **  the end last back bottom right element of the ranges \a slab_range, \a slice_range,
	 **  \a row_range and \a col_range associated to the %GenericMultiComponent4d.
	 **
	 **  \param slab_range The range of the slabs.
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slab_range, slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 **  \return const_iterator4d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Range<int> fr(0,2,2);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.first_front_upper_left(fr,sr,rr,cr),M.last_back_bottom_right(fr,sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d_range last_back_bottom_right(const slip::Range<int>& slab_range,
			const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
			const slip::Range<int>& col_range) const;

	/*!
	 **  \brief Returns a read/write reverse_iterator4d_range that points to the
	 **  past the last back bottom right element of the ranges \a slab_range, \a slice_range,
	 **  \a row_range and \a col_range associated to the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse order.
	 **
	 **  \param slab_range The range of the slabs.
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slab_range, slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 **  \return reverse_iterator4d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Range<int> fr(0,2,2);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfirst_front_upper_left(fr,sr,rr,cr),M.rlast_back_bottom_right(fr,sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d_range rfirst_front_upper_left(const slip::Range<int>& slab_range,
			const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
			const slip::Range<int>& col_range);

	/*!
	 **  \brief Returns a read/write reverse_iterator4d_range that points
	 **  to one before the first front upper left element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse order.
	 **
	 **  \param slab_range The range of the slabs.
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slab_range, slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 **  \return reverse_iterator4d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Range<int> fr(0,2,2);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfirst_front_upper_left(fr,sr,rr,cr),M.rlast_back_bottom_right(fr,sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d_range rlast_back_bottom_right(const slip::Range<int>& slab_range,
			const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
			const slip::Range<int>& col_range);

	/*!
	 **  \brief Returns a read-only reverse_iterator4d_range that points
	 **  to the past the last back bottom right element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse order.
	 **
	 **  \param slab_range The range of the slabs.
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slab_range, slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 **  \return const_reverse_iterator4d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Range<int> fr(0,2,2);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfirst_front_upper_left(fr,sr,rr,cr),M.rlast_back_bottom_right(fr,sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d_range rfirst_front_upper_left(const slip::Range<int>& slab_range,
			const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
			const slip::Range<int>& col_range) const;

	/*!
	 **  \brief Returns a read-only reverse_iterator4d_range that points
	 **  to one before the first front upper left element of the ranges
	 **  \a slab_range, \a slice_range, \a row_range
	 **  and \a col_range associated to the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \param slab_range The range of the slabs.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 ** \return const_reverse_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent4d<slip::color<double> > M(3,4,5,4);
	 **   slip::Range<int> fr(0,2,2);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfirst_front_upper_left(fr,sr,rr,cr),M.rlast_back_bottom_right(fr,sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d_range rlast_back_bottom_right(const slip::Range<int>& slab_range,
			const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
			const slip::Range<int>& col_range) const;


	/*@} End four dimensional iterators : Range iterators */

	//**************************************************************************************************************
	//**************************************************************************************************************
	//************************                   Component iterator              ***********************************
	//**************************************************************************************************************
	//**************************************************************************************************************

	/**
	 ** \name One dimensional component global iterators
	 */
	/*@{*/

	//------------------------ Component iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element in the %GenericMultiComponent4d.  Iteration is done in ordinary
	 **  element order.
	 ** \param component The component number.
	 ** \return begin component_iterator value
	 */
	component_iterator begin(const std::size_t component);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  first element in the %GenericMultiComponent4d.  Iteration is done in ordinary
	 **  element order.
	 ** \param component The component number.
	 ** \return const begin component_iterator value
	 */
	const_component_iterator begin(const std::size_t component) const;

	/*!
	 **  \brief Returns a read/write iterator that points one past the last
	 **  element in the %GenericMultiComponent4d.  Iteration is done in ordinary
	 **  element order.
	 ** \param component The component number.
	 ** \return end component_iterator value
	 */
	component_iterator end(const std::size_t component);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points one past
	 **  the last element in the %GenericMultiComponent4d.  Iteration is done in
	 **  ordinary element order.
	 ** \param component The component number.
	 ** \return const end component_iterator value
	 */
	const_component_iterator end(const std::size_t component) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element in the %GenericMultiComponent4d.  Iteration is done in ordinary
	 **  element order.
	 ** \param component The component number.
	 ** \return begin reverse_component_iterator value
	 */
	reverse_component_iterator rbegin(const std::size_t component);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  first element in the %GenericMultiComponent4d.  Iteration is done in ordinary
	 **  element order.
	 ** \param component The component number.
	 ** \return const begin reverse_component_iterator value
	 */
	const_reverse_component_iterator rbegin(const std::size_t component) const;

	/*!
	 **  \brief Returns a read/write iterator that points one past the last
	 **  element in the %GenericMultiComponent4d.  Iteration is done in ordinary
	 **  element order.
	 ** \param component The component number.
	 ** \return end reverse_component_iterator value
	 */
	reverse_component_iterator rend(const std::size_t component);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points one past
	 **  the last element in the %GenericMultiComponent4d.  Iteration is done in
	 **  ordinary element order.
	 ** \param component The component number.
	 ** \return const end reverse_component_iterator value
	 */
	const_reverse_component_iterator rend(const std::size_t component) const;



	/*@} End One dimensional component global iterators */

	/**
	 ** \name One dimensional component slab iterators
	 */
	/*@{*/
	//--------------------One dimensional component slab iterators----------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the component \a component of the first
	 **  element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **	 Iteration is done in ordinary element order (increasing slab number).
	 **  \param component The component number.
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return component_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(10);
	 **  //copy the first component of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S
	 **  std::copy(A1.slab_begin(0,4,3,3),A1.slab_end(0,4,3,3),S.begin());
	 **  \endcode
	 */
	component_slab_iterator slab_begin(const std::size_t component, const size_type slice,
			const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the component \a component of the first
	 **  element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **	 Iteration is done in ordinary element order (increasing slab number).
	 **  \param component The component number.
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return const_component_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(10);
	 **  //copy the first component of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S
	 **  std::copy(A1.slab_begin(0,4,3,3),A1.slab_end(0,4,3,3),S.begin());
	 **  \endcode
	 */
	const_component_slab_iterator slab_begin(const std::size_t component, const size_type slice,
			const size_type row, const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the component \a component of the past the end
	 **  element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **	 Iteration is done in ordinary element order (increasing slab number).
	 **  \param component The component number.
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return component_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(10);
	 **  //copy the first component of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S
	 **  std::copy(A1.slab_begin(0,4,3,3),A1.slab_end(0,4,3,3),S.begin());
	 **  \endcode
	 */
	component_slab_iterator slab_end(const std::size_t component, const size_type slice,
			const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the component \a component of
	 **  the past the end element of the line (slice,row,col) through the slabs in the
	 **  %GenericMultiComponent4d.
	 **	 Iteration is done in ordinary element order (increasing slab number).
	 **  \param component The component number.
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return const_component_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(10);
	 **  //copy the first component of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S
	 **  std::copy(A1.slab_begin(0,4,3,3),A1.slab_end(0,4,3,3),S.begin());
	 **  \endcode
	 */
	const_component_slab_iterator slab_end(const std::size_t component, const size_type slice,
			const size_type row, const size_type col) const;


	/*!
	 **  \brief Returns a read/write iterator that points to the component \a component of the
	 **  last element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **  Iteration is done in reverse element order (decreasing slab number).
	 **  \param component The component number.
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **  \return reverse_component_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(10);
	 **  //copy the first component of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slab_rbegin(0,4,3,3),A1.slab_rend(0,4,3,3),S.begin());
	 **  \endcode
	 */
	reverse_component_slab_iterator slab_rbegin(const std::size_t component, const size_type slice,
			const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the component \a component of the
	 **  last element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **	 Iteration is done in reverse element order (decreasing slab number).
	 **  \param component The component number.
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return const_reverse_component_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(10);
	 **  //copy the first component of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slab_rbegin(0,4,3,3),A1.slab_rend(0,4,3,3),S.begin());
	 **  \endcode
	 */
	const_reverse_component_slab_iterator slab_rbegin(const std::size_t component, const size_type slice,
			const size_type row, const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the component \a component of the
	 **  before the first element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **  Iteration is done in reverse element order (decreasing slab number).
	 **  \param component The component number.
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **  \return reverse_component_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(10);
	 **  //copy the first component of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slab_rbegin(0,4,3,3),A1.slab_rend(0,4,3,3),S.begin());
	 **  \endcode
	 */
	reverse_component_slab_iterator slab_rend(const std::size_t component, const size_type slice,
			const size_type row, const size_type col);


	/*!
	 **  \brief Returns a read (constant) iterator that points to the component \a component of the
	 **  before the first element of the line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 **  Iteration is done in reverse element order (decreasing slab number).
	 **  \param component The component number.
	 **	 \param slice slice coordinate of the line
	 **	 \param row row coordinate of the line
	 **	 \param col col coordinate of the line
	 **	 \return const_reverse_component_slab_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(10);
	 **  //copy the first component of the line defined by the 4th slice,
	 **  //the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slab_rbegin(0,4,3,3),A1.slab_rend(0,4,3,3),S.begin());
	 **  \endcode
	 */
	const_reverse_component_slab_iterator slab_rend(const std::size_t component, const size_type slice,
			const size_type row, const size_type col) const;

	/*@} End One dimensional component slab iterators */


	/**
	 ** \name One dimensional component slice iterators
	 */
	/*@{*/
	//--------------------One dimensional component slice iterators----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component of the first
	 ** element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return component_slice_iterator value
	 */
	component_slice_iterator slice_begin(const std::size_t component, const size_type slab,
			const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the component \a component of the first
	 ** element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_component_slice_iterator value
	 */
	const_component_slice_iterator slice_begin(const std::size_t component, const size_type slab,
			const size_type row, const size_type col) const;

	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component of the one past the end
	 ** element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return component_slice_iterator value
	 */
	component_slice_iterator slice_end(const std::size_t component, const size_type slab,
			const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the component \a component of
	 ** the one past the end element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_component_slice_iterator value
	 */
	const_component_slice_iterator slice_end(const std::size_t component, const size_type slab,
			const size_type row, const size_type col) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component of the
	 ** last element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order (decreasing slice number).
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 ** \return reverse_component_slice_iterator value
	 */
	reverse_component_slice_iterator slice_rbegin(const std::size_t component, const size_type slab,
			const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the component \a component of the
	 ** last element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 **	Iteration is done in reverse element order (decreasing slice number).
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_reverse_component_slice_iterator value
	 */
	const_reverse_component_slice_iterator slice_rbegin(const std::size_t component, const size_type slab,
			const size_type row, const size_type col) const;

	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component of the
	 ** before the first element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order (decreasing slice number).
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 ** \return reverse_component_slice_iterator value
	 */
	reverse_component_slice_iterator slice_rend(const std::size_t component, const size_type slab,
			const size_type row, const size_type col);


	/*!
	 ** \brief Returns a read (constant) iterator that points to the component \a component of the
	 ** before the first element of the line (slab,row,col) through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order (decreasing slice number).
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_reverse_component_slice_iterator value
	 */
	const_reverse_component_slice_iterator slice_rend(const std::size_t component, const size_type slab,
			const size_type row, const size_type col) const;

	/*@} End One dimensional component slice iterators */

	/**
	 ** \name One dimensional component row iterators
	 */
	/*@{*/
	//-------------------row component iterators----------

	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component of the first
	 ** element of the row \a row of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param row the index of the row.
	 ** \return begin component_component_row_iterator value
	 */
	component_row_iterator row_begin(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row);

	/*!
	 ** \brief Returns a read_only iterator that points to the component \a component of the first
	 ** element of the row \a row of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param row the index of the row.
	 ** \return begin const_component_row_iterator value
	 */
	const_component_row_iterator row_begin(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component of the past-the-end
	 ** element of the row \a row of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param row the index of the row.
	 ** \return end component_row_iterator value
	 */
	component_row_iterator row_end(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row);


	/*!
	 ** \brief Returns a read_only iterator that points to the component \a component of the past-the-end
	 ** element of the row \a row of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param row the index of the row.
	 ** \param component The component number.
	 ** \return end const_component_row_iterator value
	 */
	const_component_row_iterator row_end(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row) const;


	/*!
	 ** \brief Returns a read/write reverse iterator that points to the component \a component of the last
	 ** element of the row \a row of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param row the index of the row.
	 ** \return begin reverse_component_row_iterator value
	 */
	reverse_component_row_iterator row_rbegin(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row);

	/*!
	 ** \brief Returns a read_only reverse iterator that points to the component \a component of the last
	 ** element of the row \a row of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param row the index of the row.
	 ** \return begin const_reverse_component_row_iterator value
	 */
	const_reverse_component_row_iterator row_rbegin(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row) const;


	/*!
	 ** \brief Returns a read/write reverse iterator that points to the component \a component
	 ** of the before the first element of the row \a row of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param row the index of the row.
	 ** \return end reverse_component_row_iterator value
	 */
	reverse_component_row_iterator row_rend(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row);


	/*!
	 ** \brief Returns a read_only reverse iterator that points to the component \a component
	 ** of the before the first element of the row \a row of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param row the index of the row.
	 ** \return end const_reverse_component_row_iterator value
	 */
	const_reverse_component_row_iterator row_rend(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row) const;


	/*@} End One dimensional component row iterators */
	/**
	 ** \name One dimensional component col iterators
	 */
	/*@{*/
	//-------------------Constant component col iterators----------

	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component of the first
	 ** element of the column \a column of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param col the index of the column.
	 ** \return begin component_col_iterator value
	 */
	component_col_iterator col_begin(const std::size_t component, const size_type slab,
			const size_type slice,	const size_type col);


	/*!
	 ** \brief Returns a read_only iterator that points to the component \a component of the first
	 ** element of the column \a column of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param col the index of the column.
	 ** \return begin const_component_col_iterator value
	 */
	const_component_col_iterator col_begin(const std::size_t component, const size_type slab,
			const size_type slice,	const size_type col) const;

	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component of the past-the-end
	 ** element of the column \a column of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param col the index of the column.
	 ** \return end component_col_iterator value
	 */
	component_col_iterator col_end(const std::size_t component, const size_type slab,
			const size_type slice,	const size_type col);


	/*!
	 ** \brief Returns a read_only iterator that points to the component \a component of the past-the-end
	 ** element of the column \a column of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param col the index of the column.
	 ** \return end const_component_col_iterator value
	 */
	const_component_col_iterator col_end(const std::size_t component, const size_type slab,
			const size_type slice,	const size_type col) const;


	/*!
	 ** \brief Returns a read/write reverse iterator that points to the component \a component of the last
	 ** element of the column \a column of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param col the index of the column.
	 ** \return begin reverse_component_col_iterator value
	 */
	reverse_component_col_iterator col_rbegin(const std::size_t component, const size_type slab,
			const size_type slice,	const size_type col);


	/*!
	 ** \brief Returns a read_only reverse iterator that points to the component \a component of the last
	 ** element of the column \a column of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param col the index of the column.
	 ** \return begin const_reverse_component_col_iterator value
	 */
	const_reverse_component_col_iterator col_rbegin(const std::size_t component, const size_type slab,
			const size_type slice,	const size_type col) const;

	/*!
	 ** \brief Returns a read/write reverse iterator that points to the component \a component
	 ** of the before the first element of the column \a column of the slab \a slab and the slice \a slice
	 ** in the %GenericMultiComponent4d.
	 ** Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param col the index of the column.
	 ** \return end reverse_component_col_iterator value
	 */
	reverse_component_col_iterator col_rend(const std::size_t component, const size_type slab,
			const size_type slice,	const size_type col);


	/*!
	 ** \brief Returns a read_only reverse iterator that points to the component \a component
	 ** of the before the first element of the column \a column of the slab \a slab and
	 ** the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slab the index of the slab.
	 ** \param slice the index of the slice.
	 ** \param col the index of the column.
	 ** \return end const_reverse_component_col_iterator value
	 */
	const_reverse_component_col_iterator col_rend(const std::size_t component, const size_type slab,
			const size_type slice,	const size_type col) const;


	/*@} End One dimensional component col iterators */

	/**
	 ** \name One dimensional component slab range iterators
	 */
	/*@{*/
	//------------------------component slab range iterators -----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the component
	 ** \a component of the first element of the %Range \a range of the
	 ** line (slice,row,col) through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin component_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slice, the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slab_begin(0,4,3,3,range),A1.slab_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	component_slab_range_iterator slab_begin(const std::size_t component, const size_type slice,
			const size_type row, const size_type col, const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component
	 ** of the past the end element of the %Range \a range of the line (slice,row,col)
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end component_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slice, the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slab_begin(0,4,3,3,range),A1.slab_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	component_slab_range_iterator slab_end(const std::size_t component, const size_type slice,
			const size_type row, const size_type col, const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the component
	 ** \a component of the first element of the %Range \a range of the line (slice,row,col)
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_component_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slice, the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slab_begin(0,4,3,3,range),A1.slab_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_component_slab_range_iterator slab_begin(const std::size_t component, const size_type slice,
			const size_type row, const size_type col, const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points to the component \a component
	 ** of the past the end element of the %Range \a range of the line (slice,row,col)
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_component_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slice, the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slab_begin(0,4,3,3,range),A1.slab_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_component_slab_range_iterator slab_end(const std::size_t component, const size_type slice,
			const size_type row, const size_type col, const slip::Range<int>& range) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component
	 ** of the last element of the %Range \a range of the line (slice,row,col)
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin reverse_component_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slice, the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slab_rbegin(0,4,3,3,range),A1.slab_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	reverse_component_slab_range_iterator slab_rbegin(const std::size_t component, const size_type slice,
			const size_type row, const size_type col, const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component
	 ** of the before the first element of the %Range \a range of the line (slice,row,col)
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end reverse_component_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slice, the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slab_rbegin(0,4,3,3,range),A1.slab_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	reverse_component_slab_range_iterator slab_rend(const std::size_t component, const size_type slice,
			const size_type row, const size_type col, const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the component \a component
	 ** of the last element of the %Range \a range of the line (slice,row,col)
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_reverse_component_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slice, the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slab_rbegin(0,4,3,3,range),A1.slab_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_component_slab_range_iterator slab_rbegin(const std::size_t component, const size_type slice,
			const size_type row, const size_type col, const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points to the component \a component
	 ** of the before the first element of the %Range \a range of the line (slice,row,col)
	 ** through the slabs in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_reverse_component_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slice, the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slab_rbegin(0,4,3,3,range),A1.slab_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_component_slab_range_iterator slab_rend(const std::size_t component, const size_type slice,
			const size_type row, const size_type col, const slip::Range<int>& range) const;


	/*@} End One dimensional component slab range iterators */

	/**
	 ** \name One dimensional component slice range iterators
	 */
	/*@{*/
	//------------------------component slice range iterators -----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component of
	 ** the first element of the %Range \a range of the line (slab,row,col)
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin component_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slab, the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slice_begin(0,4,3,3,range),A1.slice_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	component_slice_range_iterator slice_begin(const std::size_t component, const size_type slab,
			const size_type row, const size_type col, const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component
	 ** of the past the end element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end component_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slab, the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slice_begin(0,4,3,3,range),A1.slice_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	component_slice_range_iterator slice_end(const std::size_t component, const size_type slab,
			const size_type row, const size_type col, const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the component \a component
	 ** of the first element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_component_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slab, the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slice_begin(0,4,3,3,range),A1.slice_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_component_slice_range_iterator slice_begin(const std::size_t component, const size_type slab,
			const size_type row, const size_type col, const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points to the component \a component
	 ** of the past the last element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_component_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slab, the 3rd row and the 3rd column of A1 in S.
	 **  std::copy(A1.slice_begin(0,4,3,3,range),A1.slice_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_component_slice_range_iterator slice_end(const std::size_t component, const size_type slab,
			const size_type row, const size_type col, const slip::Range<int>& range) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component
	 ** of the last element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin reverse_component_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slab, the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slice_rbegin(0,4,3,3,range),A1.slice_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	reverse_component_slice_range_iterator slice_rbegin(const std::size_t component, const size_type slab,
			const size_type row, const size_type col, const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component
	 ** of the before the first element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end reverse_component_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slab, the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slice_rbegin(0,4,3,3,range),A1.slice_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	reverse_component_slice_range_iterator slice_rend(const std::size_t component, const size_type slab,
			const size_type row, const size_type col, const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the component \a component
	 ** of the last element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_reverse_component_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slab, the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slice_rbegin(0,4,3,3,range),A1.slice_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_component_slice_range_iterator slice_rbegin(const std::size_t component, const size_type slab,
			const size_type row, const size_type col, const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points to the component \a component
	 ** of the before the first element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %GenericMultiComponent4d.
	 ** Iteration is done in reverse element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_reverse_component_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the line
	 **  //defined by the 4th slab, the 3rd row and the 3rd column of A1 in S in reverse order.
	 **  std::copy(A1.slice_rbegin(0,4,3,3,range),A1.slice_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_component_slice_range_iterator slice_rend(const std::size_t component, const size_type slab,
			const size_type row, const size_type col, const slip::Range<int>& range) const;


	/*@} End One dimensional component slice range iterators */

	/**
	 ** \name One dimensional component row range iterators
	 */
	/*@{*/
	//------------------------component row range iterators -----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component
	 ** of the first element of the %Range \a range of the row \a row of the
	 ** slab \a slab and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin component_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //row of the 4th slab and the 3rd slice of A1 in S.
	 **  std::copy(A1.row_begin(0,4,3,3,range),A1.row_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	component_row_range_iterator row_begin(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row, const slip::Range<int>& range);

	/*!
	 ** \brief Returns a read/write iterator that points to the component \a component
	 ** of the past the end element of the %Range \a range of the row \a row
	 ** of the slab \a slab and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return end component_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //row of the 4th slab and the 3rd slice of A1 in S.
	 **  std::copy(A1.row_begin(0,4,3,3,range),A1.row_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	component_row_range_iterator row_end(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row, const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read-only iterator that points to the component \a component
	 ** of the first element of the %Range \a range of the row \a row
	 ** of the slab \a slab and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin const_component_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //row of the 4th slab and the 3rd slice of A1 in S.
	 **  std::copy(A1.row_begin(0,4,3,3,range),A1.row_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_component_row_range_iterator row_begin(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row, const slip::Range<int>& range) const;


	/*!
	 ** \brief Returns a read_only iterator that points to the component \a component
	 ** of the past the last element of the %Range \a range of the row \a row
	 ** of the slab \a slab and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param row Row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return begin const_component_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //row of the 4th slab and the 3rd slice of A1 in S.
	 **  std::copy(A1.row_begin(0,4,3,3,range),A1.row_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_component_row_range_iterator row_end(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row, const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read-write iterator that points to the component \a component
	 ** of the last element of the %Range \a range of the row \a row
	 ** of the slab \a slab and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return reverse_component_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //row of the 4th slab and the 3rd slice of A1 in S in reverse order.
	 **  std::copy(A1.row_rbegin(0,4,3,3,range),A1.row_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	reverse_component_row_range_iterator row_rbegin(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row, const slip::Range<int>& range);

	/*!
	 ** \brief Returns a read-write iterator that points to the component \a component
	 ** of the before the first element of the %Range \a range of the row \a row
	 ** of the slab \a slab and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return reverse_component_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //row of the 4th slab and the 3rd slice of A1 in S in reverse order.
	 **  std::copy(A1.row_rbegin(0,4,3,3,range),A1.row_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	reverse_component_row_range_iterator row_rend(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row, const slip::Range<int>& range);

	/*!
	 ** \brief Returns a read-only iterator that points to the component \a component
	 ** of the last element of the %Range \a range of the row \a row
	 ** of the slab \a slab and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return begin const_reverse_component_row_range_iterator value
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //row of the 4th slab and the 3rd slice of A1 in S in reverse order.
	 **  std::copy(A1.row_rbegin(0,4,3,3,range),A1.row_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_component_row_range_iterator row_rbegin(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row, const slip::Range<int>& range) const;


	/*!
	 ** \brief Returns a read-only iterator that points to the component \a component
	 ** of the before the first element of the %Range \a range of the row \a row
	 ** of the slab \a slab and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return const_reverse_component_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //row of the 4th slab and the 3rd slice of A1 in S in reverse order.
	 **  std::copy(A1.row_rbegin(0,4,3,3,range),A1.row_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_component_row_range_iterator row_rend(const std::size_t component, const size_type slab,
			const size_type slice, const size_type row, const slip::Range<int>& range) const;

	/*@} End One dimensional component row range iterators */
	/**
	 ** \name One dimensional component col range iterators
	 */
	/*@{*/
	//------------------------component col range iterators -----------------------


	/*!
	 ** \brief Returns a read-write iterator that points to the component \a component
	 ** of the first element of the %Range \a range of the col \a col of the slab \a slab
	 ** and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate
	 ** \return begin component_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //column of the 4th slab and the 3rd slice of A1 in S.
	 **  std::copy(A1.col_begin(0,4,3,3,range),A1.col_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	component_col_range_iterator col_begin(const std::size_t component, const size_type slab,
			const size_type slice, const size_type col, const slip::Range<int>& range);

	/*!
	 ** \brief Returns a read-write iterator that points to the component \a component
	 ** of the past the end element of the %Range \a range of the col \a col of the slab \a slab
	 ** and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin component_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //column of the 4th slab and the 3rd slice of A1 in S.
	 **  std::copy(A1.col_begin(0,4,3,3,range),A1.col_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	component_col_range_iterator col_end(const std::size_t component, const size_type slab,
			const size_type slice, const size_type col, const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read-only iterator that points to the component \a component
	 ** of the first element of the %Range \a range of the col \a col of the slab \a slab
	 ** and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_component_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //column of the 4th slab and the 3rd slice of A1 in S.
	 **  std::copy(A1.col_begin(0,4,3,3,range),A1.col_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_component_col_range_iterator col_begin(const std::size_t component, const size_type slab,
			const size_type slice, const size_type col, const slip::Range<int>& range) const;


	/*!
	 ** \brief Returns a read-only iterator that points to the component \a component
	 ** of the past the end element of the %Range \a range of the col \a col of the slab \a slab
	 ** and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in ordinary element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_component_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //column of the 4th slab and the 3rd slice of A1 in S.
	 **  std::copy(A1.col_begin(0,4,3,3,range),A1.col_end(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_component_col_range_iterator col_end(const std::size_t component, const size_type slab,
			const size_type slice, const size_type col, const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read-write iterator that points to the component \a component
	 ** of the last element of the %Range \a range of the col of a slice \a col of the slab \a slab
	 ** and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin reverse_component_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //column of the 4th slab and the 3rd slice of A1 in S in reverse order.
	 **  std::copy(A1.col_rbegin(0,4,3,3,range),A1.col_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	reverse_component_col_range_iterator col_rbegin(const std::size_t component, const size_type slab,
			const size_type slice, const size_type col, const slip::Range<int>& range);

	/*!
	 ** \brief Returns a read-write iterator that points to the component \a component
	 ** of the before the first element of the %Range range of the col of a slice \a col of the slab \a slab
	 ** and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return reverse_ccomponent_ol_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //column of the 4th slab and the 3rd slice of A1 in S in reverse order.
	 **  std::copy(A1.col_rbegin(0,4,3,3,range),A1.col_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	reverse_component_col_range_iterator col_rend(const std::size_t component, const size_type slab,
			const size_type slice, const size_type col, const slip::Range<int>& range);

	/*!
	 ** \brief Returns a read_only iterator that points to the component \a component
	 ** of the last element of the %Range \& range of the col of a slice \a col of the slab \a slab
	 ** and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_reverse_component_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //column of the 4th slab and the 3rd slice of A1 in S in reverse order.
	 **  std::copy(A1.col_rbegin(0,4,3,3,range),A1.col_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_component_col_range_iterator col_rbegin(const std::size_t component, const size_type slab,
			const size_type slice, const size_type col, const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read-only iterator that points to the component \a component
	 ** of the first element of the %Range \a range of the col of a slice \a col of the slab \a slab
	 ** and the slice \a slice in the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param component The component number.
	 ** \param slab The index of the slab.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return const_reverse_component_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent4d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent4d.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::color<double> > A1(10,9,5,5);
	 **  slip::Vector<double> S(5);
	 **  slip::Range<int> range(3,7,1);
	 **  //copy five elements (from the 3rd to the 7th) of the first component of the 3rd
	 **  //column of the 4th slab and the 3rd slice of A1 in S in reverse order.
	 **  std::copy(A1.col_rbegin(0,4,3,3,range),A1.col_rend(0,4,3,3,range),S.begin());
	 ** \endcode
	 */
	const_reverse_component_col_range_iterator col_rend(const std::size_t component, const size_type slab,
			const size_type slice, const size_type col, const slip::Range<int>& range) const;

	/*@} End One dimensional component col range iterators */


	//****************************************************************************
	//                          four dimensional component iterators
	//****************************************************************************

	/**
	 ** \name Four dimensional component iterators : Global component iterators
	 */
	/*@{*/

	//------------------------ Global component iterators------------------------------------

	/*!
	 ** \brief Returns a read/write component_iterator4d that points to the first
	 ** element of the %GenericMultiComponent4d. It points to the first front upper left element of
	 ** the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \return the component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A1(8,9,4,3);
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A2(8,9,4,3);
	 **  //copy the first component of the elements of A1 in A2
	 **  std::copy(A1.first_front_upper_left(0),A1.last_back_bottom_right(0),A2.first_front_upper_left(0));
	 ** \endcode
	 */
	component_iterator4d first_front_upper_left(const std::size_t component);

	/*!
	 ** \brief Returns a read/write component_iterator4d that points to the past
	 ** the end element of the %GenericMultiComponent4d. It points to past the end element of
	 ** the last back bottom right element of the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \return the component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A1(8,9,4,3);
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A2(8,9,4,3);
	 **  //copy the first component of the elements of A1 in A2
	 **  std::copy(A1.first_front_upper_left(0),A1.last_back_bottom_right(0),A2.first_front_upper_left(0));
	 ** \endcode
	 */
	component_iterator4d last_back_bottom_right(const std::size_t component);

	/*!
	 ** \brief Returns a read only const_component_iterator4d that points to the first
	 ** element of the %GenericMultiComponent4d. It points to the first front upper left element of
	 ** the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \return the const_component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A1(8,9,4,3);
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A2(8,9,4,3);
	 **  //copy the first component of the elements of A1 in A2
	 **  std::copy(A1.first_front_upper_left(0),A1.last_back_bottom_right(0),A2.first_front_upper_left(0));
	 ** \endcode
	 */
	const_component_iterator4d first_front_upper_left(const std::size_t component) const;

	/*!
	 ** \brief Returns a read only const_component_iterator4d that points to the past
	 ** the end element of the %GenericMultiComponent4d. It points to past the end element of
	 ** the last back bottom right element of the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \return the const_component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A1(8,9,4,3);
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A2(8,9,4,3);
	 **  //copy the first component of the elements of A1 in A2
	 **  std::copy(A1.first_front_upper_left(0),A1.last_back_bottom_right(0),A2.first_front_upper_left(0));
	 ** \endcode
	 */
	const_component_iterator4d last_back_bottom_right(const std::size_t component) const;

	/*!
	 ** \brief Returns a read/write reverse component iterator4d. It points to the
	 ** last back bottom right element of the %GenericMultiComponent4d.
	 *  Iteration is done within the %GenericMultiComponent4d in the reverse order.
	 **
	 ** \param component The component number.
	 ** \return the reverse_component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A1(8,9,4,3);
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A2(8,9,4,3);
	 **  //copy the first component of the elements of A1 in A2 in reverse order
	 **  std::copy(A1.rfirst_front_upper_left(0),A1.rlast_back_bottom_right(0),A2.rfirst_front_upper_left(0));
	 ** \endcode
	 */
	reverse_component_iterator4d rfirst_front_upper_left(const std::size_t component);

	/*!
	 ** \brief Returns a read/write reverse component iterator4d. It points to past the
	 ** first front upper left element of the %GenericMultiComponent4d.
	 ** Iteration is done in the reverse order.
	 **
	 ** \param component The component number.
	 ** \return the reverse_component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A1(8,9,4,3);
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A2(8,9,4,3);
	 **  //copy the first component of the elements of A1 in A2 in reverse order
	 **  std::copy(A1.rfirst_front_upper_left(0),A1.rlast_back_bottom_right(0),A2.rfirst_front_upper_left(0));
	 ** \endcode
	 */
	reverse_component_iterator4d rlast_back_bottom_right(const std::size_t component);

	/*!
	 ** \brief Returns a read only reverse component iterator4d that points. It points
	 ** to the last back bottom right element of the %GenericMultiComponent4d.
	 ** Iteration is done within the %GenericMultiComponent4d in the reverse order.
	 **
	 ** \param component The component number.
	 ** \return the const_reverse_component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A1(8,9,4,3);
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A2(8,9,4,3);
	 **  //copy the first component of the elements of A1 in A2 in reverse order
	 **  std::copy(A1.rfirst_front_upper_left(0),A1.rlast_back_bottom_right(0),A2.rfirst_front_upper_left(0));
	 ** \endcode
	 */
	const_reverse_component_iterator4d rfirst_front_upper_left(const std::size_t component) const;


	/*!
	 **  \brief Returns a read only reverse component iterator4d. It points to past the
	 **  first front upper left element of the %GenericMultiComponent4d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \param component The component number.
	 ** \return the const_reverse_component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 ** \par Example :
	 ** \code
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A1(8,9,4,3);
	 **  slip::GenericMultiComponent4d<slip::Vector4d<double> > A2(8,9,4,3);
	 **  //copy the first component of the elements of A1 in A2 in reverse order
	 **  std::copy(A1.rfirst_front_upper_left(0),A1.rlast_back_bottom_right(0),A2.rfirst_front_upper_left(0));
	 ** \endcode
	 */
	const_reverse_component_iterator4d rlast_back_bottom_right(const std::size_t component) const;

	//------------------------ Box component iterators------------------------------------

	/*!
	 ** \brief Returns a read/write component iterator4d that points to the first
	 ** element of the %GenericMultiComponent4d. It points to the first front upper left element of
	 ** the \a %Box4d associated to the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param box A %Box4d defining the range of indices to iterate
	 **        within the %GenericMultiComponent4d.
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 ** \return the component_iterator4d value
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 */
	component_iterator4d first_front_upper_left(const std::size_t component,
			const Box4d<int>& box);

	/*!
	 ** \brief Returns a read/write component iterator4d that points to the past
	 ** the end element of the %GenericMultiComponent4d. It points to past the end element of
	 ** the last back bottom right element of the \a %Box4d associated to the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param box a %Box4d defining the range of indices to iterate
	 **        within the %GenericMultiComponent4d.
	 **
	 ** \return the component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 **
	 */
	component_iterator4d last_back_bottom_right(const std::size_t component,
			const Box4d<int>& box);

	/*!
	 ** \brief Returns a read only component iterator4d that points to the first
	 ** element of the %GenericMultiComponent4d. It points to the first front upper
	 ** left element of the \a %Box4d associated to the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param box a %Box4d defining the range of indices to iterate
	 **        within the %GenericMultiComponent4d.
	 ** \return the const_component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 */
	const_component_iterator4d first_front_upper_left(const std::size_t component,
			const Box4d<int>& box) const;

	/*!
	 ** \brief Returns a read only component iterator4d that points to the past
	 ** the end element of the %GenericMultiComponent4d. It points to past the end
	 ** element of the last back bottom right element of the \a %Box4d associated
	 ** to the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param box a %Box4d defining the range of indices to iterate
	 **        within the %GenericMultiComponent4d.
	 ** \return the const_component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 */
	const_component_iterator4d last_back_bottom_right(const std::size_t component,
			const Box4d<int>& box) const;

	/*!
	 ** \brief Returns a read/write reverse component iterator4d. It points to the
	 ** last back bottom right element of the \a %Box4d associated to the
	 ** %GenericMultiComponent4d. Iteration is done in the reverse order.
	 **
	 ** \param component The component number.
	 ** \param box a %Box4d defining the range of indices to iterate
	 **        within the %GenericMultiComponent4d.
	 **
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 **
	 ** \return the reverse_component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	reverse_component_iterator4d rfirst_front_upper_left(const std::size_t component,
			const Box4d<int>& box);

	/*!
	 ** \brief Returns a read/write reverse component iterator4d. It points to one
	 ** before the first front upper left element of the %Box4d \a box associated to
	 ** the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param box A %Box4d defining the range of indices to iterate
	 **        within the %GenericMultiComponent4d.
	 **
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 **
	 ** \return the reverse_component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	reverse_component_iterator4d rlast_back_bottom_right(const std::size_t component,
			const Box4d<int>& box);

	/*!
	 ** \brief Returns a read only reverse component iterator4d. It points to the
	 ** last back bottom right element of the %Box4d \a box associated to the
	 ** %GenericMultiComponent4d. Iteration is done in the reverse order.
	 **
	 ** \param component The component number.
	 ** \param box A %Box4d defining the range of indices to iterate
	 **        within the %GenericMultiComponent4d.
	 **
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 **
	 ** \return the const_reverse_component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_reverse_component_iterator4d rfirst_front_upper_left(const std::size_t component,
			const Box4d<int>& box) const;


	/*!
	 **  \brief Returns a read-only reverse component iterator4d. It points to one
	 **  before the first front upper left element of the %Box4d \a box associated to
	 **  the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param box A %Box4d defining the range of indices to iterate
	 **        within the %GenericMultiComponent4d.
	 **
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent4d ones.
	 **
	 ** \return the const_reverse_component_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_reverse_component_iterator4d rlast_back_bottom_right(const std::size_t component,
			const Box4d<int>& box) const;

	//------------------------ Range component iterators------------------------------------

	/*!
	 ** \brief Returns a read/write component_iterator4d_range that points to the
	 ** first front upper left element of the ranges \a slab_range, \a slice_range,
	 ** \a row_range and \a col_range associated to the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param slab_range The range of the slabs.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 ** \return the component_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	component_iterator4d_range first_front_upper_left(const std::size_t component,
			const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
			const slip::Range<int>& row_range,	const slip::Range<int>& col_range);

	/*!
	 ** \brief Returns a read/write component_iterator4d_range that points to the
	 ** past the end last back bottom right element of the ranges \a slab_range,
	 ** \a slice_range, \a row_range and \a col_range associated to the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param slab_range The range of the slabs.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 ** \return the component_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	component_iterator4d_range last_back_bottom_right(const std::size_t component,
			const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
			const slip::Range<int>& row_range,	const slip::Range<int>& col_range);

	/*!
	 ** \brief Returns a read/write component_const_iterator4d_range that points to the
	 ** first front upper left element of the ranges \a slab_range, \a slice_range,
	 ** \a row_range and \a col_range associated to the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param slab_range The range of the slabs.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 ** \return the const_component_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_component_iterator4d_range first_front_upper_left(const std::size_t component,
			const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
			const slip::Range<int>& row_range,	const slip::Range<int>& col_range) const;

	/*!
	 ** \brief Returns a read/write const_component iterator4d_range that points to the
	 ** past the end last back bottom right element of the ranges \a slab_range,
	 ** \a slice_range, \a row_range and \a col_range associated to the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param slab_range The range of the slabs.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 ** \return the const_component_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_component_iterator4d_range last_back_bottom_right(const std::size_t component,
			const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
			const slip::Range<int>& row_range,	const slip::Range<int>& col_range) const;

	/*!
	 ** \brief Returns a read/write reverse_component_iterator4d_range. It points to the
	 ** last back bottom right element of the ranges \a slab_range, \a slice_range,
	 ** \a row_range and \a col_range associated to the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param slab_range The range of the slabs.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 ** \return the reverse_component_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	reverse_component_iterator4d_range rfirst_front_upper_left(const std::size_t component,
			const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
			const slip::Range<int>& row_range,	const slip::Range<int>& col_range);

	/*!
	 ** \brief Returns a read/write reverse_component_iterator4d_range. It points to one
	 ** before the first front upper left element of the ranges \a slab_range,
	 ** \a slice_range, \a row_range and \a col_range associated to the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param slab_range The range of the slabs.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 ** \return the reverse_component_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	reverse_component_iterator4d_range rlast_back_bottom_right(const std::size_t component,
			const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
			const slip::Range<int>& row_range,	const slip::Range<int>& col_range);

	/*!
	 ** \brief Returns a read/write reverse_const_component_iterator4d_range. It points to the
	 ** last back bottom right element of the ranges \a slab_range, \a slice_range, \a row_range
	 ** and \a col_range associated to the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param slab_range The range of the slabs.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 ** \return the const_reverse_component_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_reverse_component_iterator4d_range rfirst_front_upper_left(const std::size_t component,
			const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
			const slip::Range<int>& row_range,	const slip::Range<int>& col_range) const;

	/*!
	 ** \brief Returns a read/write reverse_const_component_iterator4d_range. It points to one
	 ** before the first front upper left element of the ranges \a slab_range,
	 ** \a slice_range, \a row_range and \a col_range associated to the %GenericMultiComponent4d.
	 **
	 ** \param component The component number.
	 ** \param slab_range The range of the slabs.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent4d ones.
	 **
	 ** \return the const_reverse_component_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_reverse_component_iterator4d_range rlast_back_bottom_right(const std::size_t component,
			const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
			const slip::Range<int>& row_range,	const slip::Range<int>& col_range) const;

	/*@} End four dimensional component iterators : Global component iterators */

	/**
	 ** \name i/o operators
	 */
	/*@{*/

	/*!
	 ** \brief Write the GenericMultiComponent4d to the ouput stream
	 ** \param out output stream
	 ** \param a GenericMultiComponent4d to write to the output stream
	 */
	friend std::ostream& operator<< <>(std::ostream & out, const self& a);

	/*@} End i/o operators */

	/**
	 ** \name  Assignment operators and methods
	 */
	/*@{*/

	/*!
	 ** \brief Assign a %GenericMultiComponent4d.
	 ** Assign elements from the %GenericMultiComponent4d \a rhs
	 ** \param rhs GenericMultiComponent4d to get the values from.
	 ** \return reference to corresponding %GenericMultiComponent4d
	 */
	self& operator=(const self & rhs);

	/*!
	 ** \brief Affects all the element of the %GenericMultiComponent4d by val
	 ** \param val affectation value
	 ** \return reference to corresponding %GenericMultiComponent4d
	 */
	self& operator=(const Block& val);

	/*!
	 ** \brief Affects all the element of the %GenericMultiComponent4d by val
	 ** \param val affectation value
	 ** \return reference to corresponding %GenericMultiComponent4d
	 */
	self& operator=(const typename Block::value_type& val);

	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with copies of value
	 ** \param value  A reference-to-const of arbitrary type.
	 */
	void fill(const Block& value)
	{
		std::fill_n(this->begin(),size(),value);
	}

	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of
	 **        the value array
	 ** \param value  A pointer of arbitrary type.
	 */
	void fill(const typename Block::pointer value)
	{
		std::copy(value,value + size(), (typename Block::pointer)begin());
	}

	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of
	 **        the value array
	 ** \param value  A pointer of arbitrary type.
	 */
	void fill(const Block* value)
	{
		std::copy(value,value + size(), this->begin());
	}

	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of the range [first,last)
	 **  \param  first  An input iterator.
	 **  \param  last   An input iterator.
	 **
	 **
	 */
	template<typename InputIterator>
	void fill(InputIterator first,
			InputIterator last)
	{
		std::copy(first,last,this->begin());
	}

	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of the range [first,last)
	 **  \param  first_iterators_list  An input iterator list.
	 **  \param  last   An input iterator.
	 **
	 **
	 */
	template<typename InputIterator>
	void fill(std::vector<InputIterator> first_iterators_list,
			InputIterator last)
	{
		std::vector<typename slip::kstride_iterator<typename Block::pointer,Block::SIZE> >
		iterators_list(Block::SIZE);

		for(std::size_t component = 0; component < Block::SIZE; ++component)
		{
			iterators_list[component]= slip::kstride_iterator<typename Block::pointer,Block::SIZE>
			((typename Block::pointer)this->begin() + component);
		}


		while(first_iterators_list[0] != last)
		{
			for(std::size_t component = 0; component < Block::SIZE; ++component)
			{
				*iterators_list[component]++ = *first_iterators_list[component]++;
			}
		}
		iterators_list.clear();
	}


	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of the range [first,last)
	 ** \param component The component number.
	 ** \param  first  An input iterator.
	 ** \param  last   An input iterator.
	 **
	 **
	 */
	template<typename InputIterator>
	void fill(std::size_t component,
			InputIterator first,
			InputIterator last)
	{
		std::copy(first,last,this->begin(component));
	}

	/*@} End Assignment operators and methods*/

	/**
	 ** \name Comparison operators
	 */
	/*@{*/
	/*!
	 ** \brief GenericMultiComponent4d equality comparison
	 ** \param x A %GenericMultiComponent4d
	 ** \param y A %GenericMultiComponent4d of the same type of \a x
	 ** \return true iff the size and the elements of the Arrays are equal
	 ** \pre x.size() == y.size()
	 */
	friend bool operator== <>(const GenericMultiComponent4d<Block>& x,
			const GenericMultiComponent4d<Block>& y);

	/*!
	 ** \brief GenericMultiComponent4d inequality comparison
	 ** \param x A %GenericMultiComponent4d
	 ** \param y A %GenericMultiComponent4d of the same type of \a x
	 ** \return true if !(x == y)
	 ** \pre x.size() == y.size()
	 */
	friend bool operator!= <>(const GenericMultiComponent4d<Block>& x,
			const GenericMultiComponent4d<Block>& y);

	//  /*!
	//   ** \brief Less than comparison operator (GenericMultiComponent4d ordering relation)
	//   ** \param x A %GenericMultiComponent4d
	//   ** \param y A %GenericMultiComponent4d of the same type of \a x
	//   ** \return true iff \a x is lexicographically less than \a y
	//   ** \pre x.size() == y.size()
	//   */
	//   friend bool operator< <>(const GenericMultiComponent4d<Block>& x,
	// 			   const GenericMultiComponent4d<Block>& y);

	//  /*!
	//   ** \brief More than comparison operator
	//   ** \param x A %GenericMultiComponent4d
	//   ** \param y A %GenericMultiComponent4d of the same type of \a x
	//   ** \return true iff y > x
	//   ** \pre x.size() == y.size()
	//   */
	//   friend bool operator> <>(const GenericMultiComponent4d<Block>& x,
	// 			   const GenericMultiComponent4d<Block>& y);

	//   /*!
	//   ** \brief Less than equal comparison operator
	//   ** \param x A %GenericMultiComponent4d
	//   ** \param y A %GenericMultiComponent4d of the same type of \a x
	//   ** \return true iff !(y > x)
	//   ** \pre x.size() == y.size()
	//   */
	//   friend bool operator<= <>(const GenericMultiComponent4d<Block>& x,
	// 			    const GenericMultiComponent4d<Block>& y);

	//   /*!
	//   ** \brief More than equal comparison operator
	//   ** \param x A %GenericMultiComponent4d
	//   ** \param y A %GenericMultiComponent4d of the same type of \a x
	//   ** \return true iff !(x < y)
	//   ** \pre x.size() == y.size()
	//   */
	//   friend bool operator>= <>(const GenericMultiComponent4d<Block>& x,
	// 			    const GenericMultiComponent4d<Block>& y);


	/*@} Comparison operators */

	/**
	 ** \name  Element access operators
	 */
	/*@{*/
	/*!
	 ** \brief Subscript access to the slice datas contained in the %GenericMultiComponent4d.
	 ** \param t The index of the slab for which data should be accessed.
	 ** \return Read/write pointer to the slab data.
	 ** \pre k < slabs()
	 **
	 ** This operator allows for easy, 4d array-style, data access.
	 ** Note that data access with this operator is unchecked and
	 ** out_of_range lookups are not defined.
	 */
	pointer** operator[](const size_type t);

	/*!
	 ** \brief Subscript access to the slice datas contained in the %GenericMultiComponent4d.
	 ** \param t The index of the slab for which data should be accessed.
	 ** \return Read-only (constant) pointer* to the slab data.
	 ** \pre t < slabs()
	 **
	 ** This operator allows for easy, 4d array-style, data access.
	 ** Note that data access with this operator is unchecked and
	 ** out_of_range lookups are not defined.
	 */
	const_pointer* const * operator[](const size_type t) const;

	/*!
	 ** \brief Subscript access to the data contained in the %GenericMultiComponent4d.
	 ** \param t The index of the slab for which the data should be accessed.
	 ** \param k The index of the slice for which the data should be accessed.
	 ** \param i The index of the row for which the data should be accessed.
	 ** \param j The index of the column for which the data should be accessed.
	 ** \return Read/Write reference to data.
	 ** \pre t < slabs()
	 ** \pre k < slices()
	 ** \pre i < rows()
	 ** \pre j < cols()
	 ** This operator allows for easy, 4d array-style, data access.
	 ** Note that data access with this operator is unchecked and
	 ** out_of_range lookups are not defined.
	 */
	reference operator()(const size_type t,
			const size_type k,
			const size_type i,
			const size_type j);

	/*!
	 ** \brief Subscript access to the data contained in the %GenericMultiComponent4d.
	 ** \param t The index of the slab for which the data should be accessed.
	 ** \param k The index of the slice for which the data should be accessed.
	 ** \param i The index of the row for which the data should be accessed.
	 ** \param j The index of the column for which the data should be accessed.
	 ** \return Read_only (constant) reference to data.
	 ** \pre t < slabs()
	 ** \pre k < slices()
	 ** \pre i < rows()
	 ** \pre j < cols()
	 ** This operator allows for easy, 4d array-style, data access.
	 ** Note that data access with this operator is unchecked and
	 ** out_of_range lookups are not defined.
	 */
	const_reference operator()(const size_type t,
			const size_type k,
			const size_type i,
			const size_type j) const;

	/*@} End Element access operators */
	/*!
	 ** \brief Returns the number of slabs (first dimension size)
	 **        in the %GenericMultiComponent4d
	 */
	size_type dim1() const;

	/*!
	 ** \brief Returns the number of slabs (first dimension size)
	 **        in the %GenericMultiComponent4d
	 */
	size_type slabs() const;

	/*!
	 ** \brief Returns the number of slices (second dimension size)
	 **        in the %GenericMultiComponent4d
	 */
	size_type dim2() const;

	/*!
	 ** \brief Returns the number of slices (second dimension size)
	 **        in the %GenericMultiComponent4d
	 */
	size_type slices() const;


	/*!
	 ** \brief Returns the number of rows (third dimension size)
	 **        in the %GenericMultiComponent4d
	 */
	size_type dim3() const;


	/*!
	 ** \brief Returns the number of rows (third dimension size)
	 **        in the %GenericMultiComponent4d
	 */
	size_type rows() const;


	/*!
	 ** \brief Returns the number of columns (fourth dimension size)
	 **        in the %GenericMultiComponent4d
	 */
	size_type dim4() const;

	/*!
	 ** \brief Returns the number of columns (fourth dimension size)
	 **        in the %GenericMultiComponent4d
	 */
	size_type columns() const;

	/*!
	 ** \brief Returns the number of columns (fourth dimension size)
	 **        in the %GenericMultiComponent4d
	 */
	size_type cols() const;


	/*!
	 ** \brief Returns the number of elements in the %GenericMultiComponent4d
	 */
	size_type size() const;

	/*!
	 ** \brief Returns the maximal size (number of elements) in the %GenericMultiComponent4d
	 */
	size_type max_size() const;

	/*!
	 ** \brief Returns true if the %GenericMultiComponent4d is empty.  (Thus size() == 0)
	 */
	bool empty()const;

	/*!
	 ** \brief Swaps data with another %GenericMultiComponent4d.
	 ** \param M A %GenericMultiComponent4d of the same element type
	 */
	void swap(self& M);


	/*!
	 ** \brief Returns the min elements of the %GenericMultiComponent4d
	 ** according to the operator <
	 ** \pre size() != 0
	 */
	Block min() const;


	/*!
	 ** \brief Returns the max elements of the GenericMultiComponent4d
	 ** according to the operator <
	 ** \pre size() != 0
	 */
	Block max() const;

	//  /*!
	//   ** \brief Returns the sums of the elements of the %GenericMultiComponent4d
	//   ** \pre size() != 0
	//   */
	//   Block sum() const;

	/*!
	 ** \brief Applys the one-parameter C-function \a fun
	 **        to each element of the %GenericMultiComponent4d
	 ** \param fun The one-parameter C function
	 ** \return the resulting %GenericMultiComponent4d
	 */
	GenericMultiComponent4d<Block>& apply(Block(*fun)(Block));

	/*!
	 ** \brief Applys the one-parameter C-function \a fun
	 **        to each element of the %GenericMultiComponent4d
	 ** \param fun The one-const-parameter C function
	 ** \return the resulting %GenericMultiComponent4d
	 */
	GenericMultiComponent4d<Block>& apply(Block(*fun)(const Block&));


private:
	Matrix4d<Block>* matrix_;
 private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & this->matrix_;
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    { 
      if(version >= 0)
	{
	  ar & this->matrix_;
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()

};
  /** @} */ // end of MultiComponent4dContainer group
}//slip::


namespace slip
{

template<typename Block>
inline
GenericMultiComponent4d<Block>::GenericMultiComponent4d():
matrix_(new Matrix4d<Block>()){}

template<typename Block>
inline
GenericMultiComponent4d<Block>::GenericMultiComponent4d(const GenericMultiComponent4d<Block>::size_type slabs,
		const GenericMultiComponent4d<Block>::size_type slices, const GenericMultiComponent4d<Block>::size_type rows,
		const GenericMultiComponent4d<Block>::size_type cols):
		matrix_(new Matrix4d<Block>(slabs,slices,rows,cols)){}

template<typename Block>
inline
GenericMultiComponent4d<Block>::GenericMultiComponent4d(const GenericMultiComponent4d<Block>::size_type slabs,
		const GenericMultiComponent4d<Block>::size_type slices, const GenericMultiComponent4d<Block>::size_type rows,
		const GenericMultiComponent4d<Block>::size_type cols, const Block& val):
		matrix_(new Matrix4d<Block>(slabs,slices,rows,cols,val)){}

template<typename Block>
inline
GenericMultiComponent4d<Block>::GenericMultiComponent4d(const GenericMultiComponent4d<Block>::size_type slabs,
		const GenericMultiComponent4d<Block>::size_type slices, const GenericMultiComponent4d<Block>::size_type rows,
		const GenericMultiComponent4d<Block>::size_type cols, typename Block::const_pointer val):
		matrix_(new Matrix4d<Block>(slabs,slices,rows,cols)){
	typename Matrix4d<Block>::iterator it = matrix_->begin();
	typename Matrix4d<Block>::iterator ite = matrix_->end();
	for(;it!=ite; it++)
	{
		for(typename GenericMultiComponent4d<Block>::size_type component = 0;
				component < Block::SIZE; ++component)
		{
			(*it)[component] = *val++;
		}
	}
}

template<typename Block>
inline
GenericMultiComponent4d<Block>::GenericMultiComponent4d(const GenericMultiComponent4d<Block>::size_type slabs,
		const GenericMultiComponent4d<Block>::size_type slices, const GenericMultiComponent4d<Block>::size_type rows,
		const GenericMultiComponent4d<Block>::size_type cols, const Block* val):
		matrix_(new Matrix4d<Block>(slabs,slices,rows,cols,val)){}

template<typename Block>
inline
GenericMultiComponent4d<Block>::GenericMultiComponent4d(const GenericMultiComponent4d<Block>& rhs):
matrix_(new Matrix4d<Block>(*rhs.matrix_)){}

template<typename Block>
inline
GenericMultiComponent4d<Block>::~GenericMultiComponent4d(){
	delete matrix_;
}

template<typename Block>
inline
void GenericMultiComponent4d<Block>::resize(const GenericMultiComponent4d<Block>::size_type slabs,
		const GenericMultiComponent4d<Block>::size_type slices,
		const GenericMultiComponent4d<Block>::size_type rows,
		const GenericMultiComponent4d<Block>::size_type cols,
		const Block& val){
	matrix_->resize(slabs,slices,rows,cols,val);
}


//****************************************************************************
//                          One dimensional iterators
//****************************************************************************

//----------------------Global iterators------------------------------


template<typename Block>
inline
typename GenericMultiComponent4d<Block>::iterator
GenericMultiComponent4d<Block>::begin(){
	return matrix_->begin();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_iterator
GenericMultiComponent4d<Block>::begin() const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->begin();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::iterator
GenericMultiComponent4d<Block>::end(){
	return matrix_->end();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_iterator
GenericMultiComponent4d<Block>::end() const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->end();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_iterator
GenericMultiComponent4d<Block>::rbegin(){
	return typename GenericMultiComponent4d<Block>::reverse_iterator(this->end());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_iterator
GenericMultiComponent4d<Block>::rbegin() const{
	return typename GenericMultiComponent4d<Block>::const_reverse_iterator(this->end());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_iterator
GenericMultiComponent4d<Block>::rend(){
	return typename GenericMultiComponent4d<Block>::reverse_iterator(this->begin());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_iterator
GenericMultiComponent4d<Block>::rend() const{
	return typename GenericMultiComponent4d<Block>::const_reverse_iterator(this->begin());
}

//--------------------One dimensional slab iterators----------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::slab_iterator
GenericMultiComponent4d<Block>::slab_begin(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col){
	return matrix_->slab_begin(slice,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_slab_iterator
GenericMultiComponent4d<Block>::slab_begin(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slab_begin(slice,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::slab_iterator
GenericMultiComponent4d<Block>::slab_end(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col){
	return matrix_->slab_end(slice,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_slab_iterator
GenericMultiComponent4d<Block>::slab_end(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slab_end(slice,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_slab_iterator
GenericMultiComponent4d<Block>::slab_rbegin(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col){
	return matrix_->slab_rbegin(slice,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_slab_iterator
GenericMultiComponent4d<Block>::slab_rbegin(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slab_rbegin(slice,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_slab_iterator
GenericMultiComponent4d<Block>::slab_rend(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col){
	return matrix_->slab_rend(slice,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_slab_iterator
GenericMultiComponent4d<Block>::slab_rend(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slab_rend(slice,row,col);
}

//--------------------One dimensional slice iterators----------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::slice_iterator
GenericMultiComponent4d<Block>::slice_begin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col){
	return matrix_->slice_begin(slab,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_slice_iterator
GenericMultiComponent4d<Block>::slice_begin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slice_begin(slab,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::slice_iterator
GenericMultiComponent4d<Block>::slice_end(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col){
	return matrix_->slice_end(slab,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_slice_iterator
GenericMultiComponent4d<Block>::slice_end(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slice_end(slab,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_slice_iterator
GenericMultiComponent4d<Block>::slice_rbegin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col){
	return matrix_->slice_rbegin(slab,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_slice_iterator
GenericMultiComponent4d<Block>::slice_rbegin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slice_rbegin(slab,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_slice_iterator
GenericMultiComponent4d<Block>::slice_rend(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col){
	return matrix_->slice_rend(slab,row,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_slice_iterator
GenericMultiComponent4d<Block>::slice_rend(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slice_rend(slab,row,col);
}

//-------------------row iterators----------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::row_iterator
GenericMultiComponent4d<Block>::row_begin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row){
	return matrix_->row_begin(slab,slice,row);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_row_iterator
GenericMultiComponent4d<Block>::row_begin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->row_begin(slab,slice,row);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::row_iterator
GenericMultiComponent4d<Block>::row_end(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row){
	return matrix_->row_end(slab,slice,row);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_row_iterator
GenericMultiComponent4d<Block>::row_end(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->row_end(slab,slice,row);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_row_iterator
GenericMultiComponent4d<Block>::row_rbegin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row){
	return matrix_->row_rbegin(slab,slice,row);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_row_iterator
GenericMultiComponent4d<Block>::row_rbegin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->row_rbegin(slab,slice,row);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_row_iterator
GenericMultiComponent4d<Block>::row_rend(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row){
	return matrix_->row_rend(slab,slice,row);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_row_iterator
GenericMultiComponent4d<Block>::row_rend(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->row_rend(slab,slice,row);
}

//-------------------Constant col iterators----------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::col_iterator
GenericMultiComponent4d<Block>::col_begin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col){
	return matrix_->col_begin(slab,slice,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_col_iterator
GenericMultiComponent4d<Block>::col_begin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->col_begin(slab,slice,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::col_iterator
GenericMultiComponent4d<Block>::col_end(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col){
	return matrix_->col_end(slab,slice,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_col_iterator
GenericMultiComponent4d<Block>::col_end(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->col_end(slab,slice,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_col_iterator
GenericMultiComponent4d<Block>::col_rbegin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col){
	return matrix_->col_rbegin(slab,slice,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_col_iterator
GenericMultiComponent4d<Block>::col_rbegin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->col_rbegin(slab,slice,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_col_iterator
GenericMultiComponent4d<Block>::col_rend(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col){
	return matrix_->col_rend(slab,slice,col);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_col_iterator
GenericMultiComponent4d<Block>::col_rend(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->col_rend(slab,slice,col);
}

//------------------------slab range iterators -----------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::slab_range_iterator
GenericMultiComponent4d<Block>::slab_begin(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return matrix_->slab_begin(slice,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::slab_range_iterator
GenericMultiComponent4d<Block>::slab_end(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return matrix_->slab_end(slice,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_slab_range_iterator
GenericMultiComponent4d<Block>::slab_begin(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slab_begin(slice,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_slab_range_iterator
GenericMultiComponent4d<Block>::slab_end(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slab_end(slice,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_slab_range_iterator
GenericMultiComponent4d<Block>::slab_rbegin(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return matrix_->slab_rbegin(slice,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_slab_range_iterator
GenericMultiComponent4d<Block>::slab_rend(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return matrix_->slab_rend(slice,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_slab_range_iterator
GenericMultiComponent4d<Block>::slab_rbegin(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slab_rbegin(slice,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_slab_range_iterator
GenericMultiComponent4d<Block>::slab_rend(const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slab_rend(slice,row,col,range);
}

//------------------------slice range iterators -----------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::slice_range_iterator
GenericMultiComponent4d<Block>::slice_begin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return matrix_->slice_begin(slab,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::slice_range_iterator
GenericMultiComponent4d<Block>::slice_end(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return matrix_->slice_end(slab,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_slice_range_iterator
GenericMultiComponent4d<Block>::slice_begin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slice_begin(slab,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_slice_range_iterator
GenericMultiComponent4d<Block>::slice_end(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slice_end(slab,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_slice_range_iterator
GenericMultiComponent4d<Block>::slice_rbegin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return matrix_->slice_rbegin(slab,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_slice_range_iterator
GenericMultiComponent4d<Block>::slice_rend(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return matrix_->slice_rend(slab,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_slice_range_iterator
GenericMultiComponent4d<Block>::slice_rbegin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slice_rbegin(slab,row,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_slice_range_iterator
GenericMultiComponent4d<Block>::slice_rend(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->slice_rend(slab,row,col,range);
}

//------------------------row range iterators -----------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::row_range_iterator
GenericMultiComponent4d<Block>::row_begin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range){
	return matrix_->row_begin(slab,slice,row,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::row_range_iterator
GenericMultiComponent4d<Block>::row_end(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range){
	return matrix_->row_end(slab,slice,row,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_row_range_iterator
GenericMultiComponent4d<Block>::row_begin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->row_begin(slab,slice,row,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_row_range_iterator
GenericMultiComponent4d<Block>::row_end(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->row_end(slab,slice,row,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_row_range_iterator
GenericMultiComponent4d<Block>::row_rbegin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range){
	return matrix_->row_rbegin(slab,slice,row,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_row_range_iterator
GenericMultiComponent4d<Block>::row_rend(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range){
	return matrix_->row_rend(slab,slice,row,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_row_range_iterator
GenericMultiComponent4d<Block>::row_rbegin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->row_rbegin(slab,slice,row,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_row_range_iterator
GenericMultiComponent4d<Block>::row_rend(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->row_rend(slab,slice,row,range);
}

//------------------------col range iterators -----------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::col_range_iterator
GenericMultiComponent4d<Block>::col_begin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return matrix_->col_begin(slab,slice,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::col_range_iterator
GenericMultiComponent4d<Block>::col_end(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return matrix_->col_end(slab,slice,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_col_range_iterator
GenericMultiComponent4d<Block>::col_begin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->col_begin(slab,slice,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_col_range_iterator
GenericMultiComponent4d<Block>::col_end(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->col_end(slab,slice,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_col_range_iterator
GenericMultiComponent4d<Block>::col_rbegin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return matrix_->col_rbegin(slab,slice,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_col_range_iterator
GenericMultiComponent4d<Block>::col_rend(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return matrix_->col_rend(slab,slice,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_col_range_iterator
GenericMultiComponent4d<Block>::col_rbegin(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->col_rbegin(slab,slice,col,range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_col_range_iterator
GenericMultiComponent4d<Block>::col_rend(const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->col_rend(slab,slice,col,range);
}

//****************************************************************************
//                          Four dimensional iterators
//****************************************************************************

//------------------------ Global iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::iterator4d
GenericMultiComponent4d<Block>::first_front_upper_left(){
	return matrix_->first_front_upper_left();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::iterator4d
GenericMultiComponent4d<Block>::last_back_bottom_right(){
	return matrix_->last_back_bottom_right();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_iterator4d
GenericMultiComponent4d<Block>::first_front_upper_left() const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->first_front_upper_left();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_iterator4d
GenericMultiComponent4d<Block>::last_back_bottom_right() const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->last_back_bottom_right();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_iterator4d
GenericMultiComponent4d<Block>::rfirst_front_upper_left(){
	return matrix_->rfirst_front_upper_left();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_iterator4d
GenericMultiComponent4d<Block>::rlast_back_bottom_right(){
	return matrix_->rlast_back_bottom_right();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_iterator4d
GenericMultiComponent4d<Block>::rfirst_front_upper_left() const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->rfirst_front_upper_left();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_iterator4d
GenericMultiComponent4d<Block>::rlast_back_bottom_right() const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->rlast_back_bottom_right();
}

//------------------------ Box iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::iterator4d
GenericMultiComponent4d<Block>::first_front_upper_left(const Box4d<int>& box){
	return matrix_->first_front_upper_left(box);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::iterator4d
GenericMultiComponent4d<Block>::last_back_bottom_right(const Box4d<int>& box){
	return matrix_->last_back_bottom_right(box);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_iterator4d
GenericMultiComponent4d<Block>::first_front_upper_left(const Box4d<int>& box) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->first_front_upper_left(box);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_iterator4d
GenericMultiComponent4d<Block>::last_back_bottom_right(const Box4d<int>& box) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->last_back_bottom_right(box);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_iterator4d
GenericMultiComponent4d<Block>::rfirst_front_upper_left(const Box4d<int>& box){
	return matrix_->rfirst_front_upper_left(box);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_iterator4d
GenericMultiComponent4d<Block>::rlast_back_bottom_right(const Box4d<int>& box){
	return matrix_->rlast_back_bottom_right(box);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_iterator4d
GenericMultiComponent4d<Block>::rfirst_front_upper_left(const Box4d<int>& box) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->rfirst_front_upper_left(box);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_iterator4d
GenericMultiComponent4d<Block>::rlast_back_bottom_right(const Box4d<int>& box) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->rlast_back_bottom_right(box);
}

//------------------------ Range iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::iterator4d_range
GenericMultiComponent4d<Block>::first_front_upper_left(const slip::Range<int>& slab_range,
		const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
		const slip::Range<int>& col_range){
	return matrix_->first_front_upper_left(slab_range,slice_range,row_range,col_range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::iterator4d_range
GenericMultiComponent4d<Block>::last_back_bottom_right(const slip::Range<int>& slab_range,
		const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
		const slip::Range<int>& col_range){
	return matrix_->last_back_bottom_right(slab_range,slice_range,row_range,col_range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_iterator4d_range
GenericMultiComponent4d<Block>::first_front_upper_left(const slip::Range<int>& slab_range,
		const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
		const slip::Range<int>& col_range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->first_front_upper_left(slab_range,slice_range,row_range,col_range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_iterator4d_range
GenericMultiComponent4d<Block>::last_back_bottom_right(const slip::Range<int>& slab_range,
		const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
		const slip::Range<int>& col_range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->last_back_bottom_right(slab_range,slice_range,row_range,col_range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_iterator4d_range
GenericMultiComponent4d<Block>::rfirst_front_upper_left(const slip::Range<int>& slab_range,
		const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
		const slip::Range<int>& col_range){
	return matrix_->rfirst_front_upper_left(slab_range,slice_range,row_range,col_range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_iterator4d_range
GenericMultiComponent4d<Block>::rlast_back_bottom_right(const slip::Range<int>& slab_range,
		const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
		const slip::Range<int>& col_range){
	return matrix_->rlast_back_bottom_right(slab_range,slice_range,row_range,col_range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_iterator4d_range
GenericMultiComponent4d<Block>::rfirst_front_upper_left(const slip::Range<int>& slab_range,
		const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
		const slip::Range<int>& col_range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->rfirst_front_upper_left(slab_range,slice_range,row_range,col_range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_iterator4d_range
GenericMultiComponent4d<Block>::rlast_back_bottom_right(const slip::Range<int>& slab_range,
		const slip::Range<int>& slice_range, const slip::Range<int>& row_range,
		const slip::Range<int>& col_range) const{
	Matrix4d<Block> const * tp(matrix_);
	return tp->rlast_back_bottom_right(slab_range,slice_range,row_range,col_range);
}



//********************************************************************************
//                         Component iterators
//********************************************************************************

//------------------------ Component iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_iterator
GenericMultiComponent4d<Block>::begin(const std::size_t component){
	return typename GenericMultiComponent4d<Block>::component_iterator((typename Block::pointer)begin() + component);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_iterator
GenericMultiComponent4d<Block>::begin(const std::size_t component) const{
	return typename GenericMultiComponent4d<Block>::const_component_iterator((typename Block::const_pointer)begin() + component);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_iterator
GenericMultiComponent4d<Block>::end(const std::size_t component){
	return typename GenericMultiComponent4d<Block>::component_iterator((typename Block::pointer)end() + component);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_iterator
GenericMultiComponent4d<Block>::end(const std::size_t component) const{
	return typename GenericMultiComponent4d<Block>::const_component_iterator((typename Block::const_pointer)end() + component);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_iterator
GenericMultiComponent4d<Block>::rbegin(const std::size_t component){
	return typename GenericMultiComponent4d<Block>::reverse_component_iterator(this->end(component));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_iterator
GenericMultiComponent4d<Block>::rbegin(const std::size_t component) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_iterator(this->end(component));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_iterator
GenericMultiComponent4d<Block>::rend(const std::size_t component){
	return typename GenericMultiComponent4d<Block>::reverse_component_iterator(this->begin(component));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_iterator
GenericMultiComponent4d<Block>::rend(const std::size_t component) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_iterator(this->begin(component));
}

//--------------------One dimensional component slab iterators----------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_slab_iterator
GenericMultiComponent4d<Block>::slab_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col){
	return typename GenericMultiComponent4d<Block>::component_slab_iterator(&((*this)[0][slice][row][col][component]),
			Block::SIZE * this->dim2() * this->dim3() * this->dim4());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_slab_iterator
GenericMultiComponent4d<Block>::slab_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col) const{
	return typename GenericMultiComponent4d<Block>::const_component_slab_iterator(&((*this)[0][slice][row][col][component]),
			Block::SIZE * this->dim2() * this->dim3() * this->dim4());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_slab_iterator
GenericMultiComponent4d<Block>::slab_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col){
	return ++(typename GenericMultiComponent4d<Block>::component_slab_iterator(&((*this)[this->dim1()-1][slice][row][col][component]),
			Block::SIZE * this->dim2() * this->dim3() * this->dim4()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_slab_iterator
GenericMultiComponent4d<Block>::slab_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col) const{
	return ++(typename GenericMultiComponent4d<Block>::const_component_slab_iterator(&((*this)[this->dim1()-1][slice][row][col][component]),
			Block::SIZE * this->dim2() * this->dim3() * this->dim4()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_slab_iterator
GenericMultiComponent4d<Block>::slab_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col){
	return typename GenericMultiComponent4d<Block>::reverse_component_slab_iterator(this->slab_end(component,slice,row,col));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_slab_iterator
GenericMultiComponent4d<Block>::slab_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_slab_iterator(this->slab_end(component,slice,row,col));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_slab_iterator
GenericMultiComponent4d<Block>::slab_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col){
	return typename GenericMultiComponent4d<Block>::reverse_component_slab_iterator(this->slab_begin(component,slice,row,col));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_slab_iterator
GenericMultiComponent4d<Block>::slab_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_slab_iterator(this->slab_begin(component,slice,row,col));
}

//--------------------One dimensional component slice iterators----------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_slice_iterator
GenericMultiComponent4d<Block>::slice_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col){
	return GenericMultiComponent4d<Block>::component_slice_iterator(&((*this)[slab][0][row][col][component]),
			Block::SIZE * this->dim3() * this->dim4());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_slice_iterator
GenericMultiComponent4d<Block>::slice_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col) const{
	return GenericMultiComponent4d<Block>::const_component_slice_iterator(&((*this)[slab][0][row][col][component]),
			Block::SIZE * this->dim3() * this->dim4());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_slice_iterator
GenericMultiComponent4d<Block>::slice_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col){
	return ++(typename GenericMultiComponent4d<Block>::component_slice_iterator(&((*this)[slab][this->dim2()-1][row][col][component]),
			Block::SIZE * this->dim3() * this->dim4()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_slice_iterator
GenericMultiComponent4d<Block>::slice_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col) const{
	return ++(typename GenericMultiComponent4d<Block>::const_component_slice_iterator(&((*this)[slab][this->dim2()-1][row][col][component]),
			Block::SIZE * this->dim3() * this->dim4()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_slice_iterator
GenericMultiComponent4d<Block>::slice_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col){
	return typename GenericMultiComponent4d<Block>::reverse_component_slice_iterator(this->slice_end(component,slab,row,col));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_slice_iterator
GenericMultiComponent4d<Block>::slice_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_slice_iterator(this->slice_end(component,slab,row,col));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_slice_iterator
GenericMultiComponent4d<Block>::slice_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col){
	return typename GenericMultiComponent4d<Block>::reverse_component_slice_iterator(this->slice_begin(component,slab,row,col));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_slice_iterator
GenericMultiComponent4d<Block>::slice_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_slice_iterator(this->slice_begin(component,slab,row,col));
}

//-------------------row component iterators----------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_row_iterator
GenericMultiComponent4d<Block>::row_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row){
	return typename GenericMultiComponent4d<Block>::component_row_iterator(
			(typename Block::pointer)this->row_begin(slab,slice,row) + component);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_row_iterator
GenericMultiComponent4d<Block>::row_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row) const{
	return typename GenericMultiComponent4d<Block>::const_component_row_iterator(
			(typename Block::const_pointer)this->row_begin(slab,slice,row) + component);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_row_iterator
GenericMultiComponent4d<Block>::row_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row){
	return typename GenericMultiComponent4d<Block>::component_row_iterator(
			(typename Block::pointer)this->row_end(slab,slice,row) + component);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_row_iterator
GenericMultiComponent4d<Block>::row_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row) const{
	return typename GenericMultiComponent4d<Block>::const_component_row_iterator(
			(typename Block::const_pointer)this->row_end(slab,slice,row) + component);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_row_iterator
GenericMultiComponent4d<Block>::row_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row){
	return typename GenericMultiComponent4d<Block>::reverse_component_row_iterator(this->row_end(component,slab,slice,row));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_row_iterator
GenericMultiComponent4d<Block>::row_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_row_iterator(this->row_end(component,slab,slice,row));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_row_iterator
GenericMultiComponent4d<Block>::row_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row){
	return typename GenericMultiComponent4d<Block>::reverse_component_row_iterator(this->row_begin(component,slab,slice,row));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_row_iterator
GenericMultiComponent4d<Block>::row_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_row_iterator(this->row_begin(component,slab,slice,row));
}

//-------------------Constant component col iterators----------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_col_iterator
GenericMultiComponent4d<Block>::col_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,	const GenericMultiComponent4d<Block>::size_type col){
	return typename GenericMultiComponent4d<Block>::component_col_iterator(&((*this)[slab][slice][0][col][component]),
			Block::SIZE * this->dim4());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_col_iterator
GenericMultiComponent4d<Block>::col_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,	const GenericMultiComponent4d<Block>::size_type col) const{
	return typename GenericMultiComponent4d<Block>::const_component_col_iterator(&((*this)[slab][slice][0][col][component]),
			Block::SIZE * this->dim4());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_col_iterator
GenericMultiComponent4d<Block>::col_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,	const GenericMultiComponent4d<Block>::size_type col){
	return ++(typename GenericMultiComponent4d<Block>::component_col_iterator(&((*this)[slab][slice][this->dim3()-1][col][component]),
			Block::SIZE * this->dim4()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_col_iterator
GenericMultiComponent4d<Block>::col_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,	const GenericMultiComponent4d<Block>::size_type col) const{
	return ++(typename GenericMultiComponent4d<Block>::const_component_col_iterator(&((*this)[slab][slice][this->dim3()-1][col][component]),
			Block::SIZE * this->dim4()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_col_iterator
GenericMultiComponent4d<Block>::col_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,	const GenericMultiComponent4d<Block>::size_type col){
	return typename GenericMultiComponent4d<Block>::reverse_component_col_iterator(this->col_end(component,slab,slice,col));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_col_iterator
GenericMultiComponent4d<Block>::col_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,	const GenericMultiComponent4d<Block>::size_type col) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_col_iterator(this->col_end(component,slab,slice,col));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_col_iterator
GenericMultiComponent4d<Block>::col_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,	const GenericMultiComponent4d<Block>::size_type col){
	return typename GenericMultiComponent4d<Block>::reverse_component_col_iterator(this->col_begin(component,slab,slice,col));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_col_iterator
GenericMultiComponent4d<Block>::col_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice,	const GenericMultiComponent4d<Block>::size_type col) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_col_iterator(this->col_begin(component,slab,slice,col));
}

//------------------------component slab range iterators -----------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_slab_range_iterator
GenericMultiComponent4d<Block>::slab_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return typename GenericMultiComponent4d<Block>::component_slab_range_iterator(&((*this)[range.start()][slice][row][col][component]),
			Block::SIZE * this->dim2() * this->dim3() * this->dim4() * range.stride());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_slab_range_iterator
GenericMultiComponent4d<Block>::slab_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return ++(typename GenericMultiComponent4d<Block>::component_slab_range_iterator
			(&((*this)[range.start() + range.iterations() * range.stride()][slice][row][col][component]),
					Block::SIZE * this->dim2() * this->dim3() * this->dim4() * range.stride()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_slab_range_iterator
GenericMultiComponent4d<Block>::slab_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	return typename GenericMultiComponent4d<Block>::const_component_slab_range_iterator(&((*this)[range.start()][slice][row][col][component]),
			Block::SIZE * this->dim2() * this->dim3() * this->dim4() * range.stride());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_slab_range_iterator
GenericMultiComponent4d<Block>::slab_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	return ++(typename GenericMultiComponent4d<Block>::const_component_slab_range_iterator
			(&((*this)[range.start() + range.iterations() * range.stride()][slice][row][col][component]),
					Block::SIZE * this->dim2() * this->dim3() * this->dim4() * range.stride()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_slab_range_iterator
GenericMultiComponent4d<Block>::slab_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return typename GenericMultiComponent4d<Block>::reverse_component_slab_range_iterator(this->slab_end(component,slice,row,col,range));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_slab_range_iterator
GenericMultiComponent4d<Block>::slab_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return typename GenericMultiComponent4d<Block>::reverse_component_slab_range_iterator(this->slab_begin(component,slice,row,col,range));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_slab_range_iterator
GenericMultiComponent4d<Block>::slab_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_slab_range_iterator(this->slab_end(component,slice,row,col,range));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_slab_range_iterator
GenericMultiComponent4d<Block>::slab_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slice,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_slab_range_iterator(this->slab_begin(component,slice,row,col,range));
}

//------------------------component slice range iterators -----------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_slice_range_iterator
GenericMultiComponent4d<Block>::slice_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return typename GenericMultiComponent4d<Block>::component_slice_range_iterator(&((*this)[slab][range.start()][row][col][component]),
			Block::SIZE * this->dim3() * this->dim4() * range.stride());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_slice_range_iterator
GenericMultiComponent4d<Block>::slice_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return ++(typename GenericMultiComponent4d<Block>::component_slice_range_iterator
			(&((*this)[slab][range.start() + range.iterations() * range.stride()][row][col][component]),
					Block::SIZE * this->dim3() * this->dim4() * range.stride()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_slice_range_iterator
GenericMultiComponent4d<Block>::slice_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	return typename GenericMultiComponent4d<Block>::const_component_slice_range_iterator(&((*this)[slab][range.start()][row][col][component]),
			Block::SIZE * this->dim3() * this->dim4() * range.stride());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_slice_range_iterator
GenericMultiComponent4d<Block>::slice_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	return ++(typename GenericMultiComponent4d<Block>::const_component_slice_range_iterator
			(&((*this)[slab][range.start() + range.iterations() * range.stride()][row][col][component]),
					Block::SIZE * this->dim3() * this->dim4() * range.stride()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_slice_range_iterator
GenericMultiComponent4d<Block>::slice_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return typename GenericMultiComponent4d<Block>::reverse_component_slice_range_iterator(this->slice_end(component,slab,row,col,range));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_slice_range_iterator
GenericMultiComponent4d<Block>::slice_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return typename GenericMultiComponent4d<Block>::reverse_component_slice_range_iterator(this->slice_begin(component,slab,row,col,range));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_slice_range_iterator
GenericMultiComponent4d<Block>::slice_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_slice_range_iterator(this->slice_end(component,slab,row,col,range));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_slice_range_iterator
GenericMultiComponent4d<Block>::slice_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type row, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_slice_range_iterator(this->slice_begin(component,slab,row,col,range));
}

//------------------------component row range iterators -----------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_row_range_iterator
GenericMultiComponent4d<Block>::row_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range){
	return typename GenericMultiComponent4d<Block>::component_row_range_iterator
			(&((*this)[slab][slice][row][range.start()][component]), Block::SIZE * range.stride());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_row_range_iterator
GenericMultiComponent4d<Block>::row_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range){
	return ++(typename GenericMultiComponent4d<Block>::component_row_range_iterator
			(&((*this)[slab][slice][row][range.start()+ range.iterations() * range.stride()][component]), Block::SIZE * range.stride()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_row_range_iterator
GenericMultiComponent4d<Block>::row_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range) const{
	return typename GenericMultiComponent4d<Block>::const_component_row_range_iterator
			(&((*this)[slab][slice][row][range.start()][component]), Block::SIZE * range.stride());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_row_range_iterator
GenericMultiComponent4d<Block>::row_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range) const{
	return ++(typename GenericMultiComponent4d<Block>::const_component_row_range_iterator
			(&((*this)[slab][slice][row][range.start()+ range.iterations() * range.stride()][component]), Block::SIZE * range.stride()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_row_range_iterator
GenericMultiComponent4d<Block>::row_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range){
	return typename GenericMultiComponent4d<Block>::reverse_component_row_range_iterator(this->row_end(component,slab,slice,row,range));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_row_range_iterator
GenericMultiComponent4d<Block>::row_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range){
	return typename GenericMultiComponent4d<Block>::reverse_component_row_range_iterator(this->row_begin(component,slab,slice,row,range));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_row_range_iterator
GenericMultiComponent4d<Block>::row_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_row_range_iterator(this->row_end(component,slab,slice,row,range));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_row_range_iterator
GenericMultiComponent4d<Block>::row_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type row,
		const slip::Range<int>& range) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_row_range_iterator(this->row_begin(component,slab,slice,row,range));
}

//------------------------component col range iterators -----------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_col_range_iterator
GenericMultiComponent4d<Block>::col_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return typename GenericMultiComponent4d<Block>::component_col_range_iterator(&((*this)[slab][slice][range.start()][col][component]),
			Block::SIZE * this->dim4() * range.stride());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_col_range_iterator
GenericMultiComponent4d<Block>::col_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return ++(typename GenericMultiComponent4d<Block>::component_col_range_iterator
			(&((*this)[slab][slice][range.start()+ range.iterations() * range.stride()][col][component]),
					Block::SIZE * this->dim4() * range.stride()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_col_range_iterator
GenericMultiComponent4d<Block>::col_begin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	return typename GenericMultiComponent4d<Block>::const_component_col_range_iterator(&((*this)[slab][slice][range.start()][col][component]),
			Block::SIZE * this->dim4() * range.stride());
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_col_range_iterator
GenericMultiComponent4d<Block>::col_end(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	return ++(typename GenericMultiComponent4d<Block>::const_component_col_range_iterator
			(&((*this)[slab][slice][range.start()+ range.iterations() * range.stride()][col][component]),
					Block::SIZE * this->dim4() * range.stride()));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_col_range_iterator
GenericMultiComponent4d<Block>::col_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return typename GenericMultiComponent4d<Block>::reverse_component_col_range_iterator(this->col_end(component,slab,slice,col,range));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_col_range_iterator
GenericMultiComponent4d<Block>::col_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range){
	return typename GenericMultiComponent4d<Block>::reverse_component_col_range_iterator(this->col_begin(component,slab,slice,col,range));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_col_range_iterator
GenericMultiComponent4d<Block>::col_rbegin(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_col_range_iterator(this->col_end(component,slab,slice,col,range));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_col_range_iterator
GenericMultiComponent4d<Block>::col_rend(const std::size_t component, const GenericMultiComponent4d<Block>::size_type slab,
		const GenericMultiComponent4d<Block>::size_type slice, const GenericMultiComponent4d<Block>::size_type col,
		const slip::Range<int>& range) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_col_range_iterator(this->col_begin(component,slab,slice,col,range));
}

//****************************************************************************
//                          four dimensional component iterators
//****************************************************************************

//------------------------ Global component iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_iterator4d
GenericMultiComponent4d<Block>::first_front_upper_left(const std::size_t component){
	return typename GenericMultiComponent4d<Block>::component_iterator4d
			(this,component,Box4d<int>(0,0,0,0,this->dim1()-1,this->dim2()-1,this->dim3()-1,this->dim4()-1));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_iterator4d
GenericMultiComponent4d<Block>::last_back_bottom_right(const std::size_t component){
	DPoint4d<int> dp(this->dim1(),this->dim2(),this->dim3(),this->dim4());
	typename GenericMultiComponent4d<Block>::component_iterator4d it =
			(*this).first_front_upper_left(component) + dp;
	return it;
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_iterator4d
GenericMultiComponent4d<Block>::first_front_upper_left(const std::size_t component) const{
	return typename GenericMultiComponent4d<Block>::const_component_iterator4d
			(this,component,Box4d<int>(0,0,0,0,this->dim1()-1,this->dim2()-1,this->dim3()-1,this->dim4()-1));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_iterator4d
GenericMultiComponent4d<Block>::last_back_bottom_right(const std::size_t component) const{
	DPoint4d<int> dp(this->dim1(),this->dim2(),this->dim3(),this->dim4());
	typename GenericMultiComponent4d<Block>::const_component_iterator4d it =
			(*this).first_front_upper_left(component) + dp;
	return it;
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_iterator4d
GenericMultiComponent4d<Block>::rfirst_front_upper_left(const std::size_t component){
	DPoint4d<int> dp(1,1,1,0);
	return typename GenericMultiComponent4d<Block>::reverse_component_iterator4d
			(this->last_back_bottom_right(component) - dp);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_iterator4d
GenericMultiComponent4d<Block>::rlast_back_bottom_right(const std::size_t component){
	return typename GenericMultiComponent4d<Block>::reverse_component_iterator4d
			(this->first_front_upper_left(component));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_iterator4d
GenericMultiComponent4d<Block>::rfirst_front_upper_left(const std::size_t component) const{
	DPoint4d<int> dp(1,1,1,0);
	return typename GenericMultiComponent4d<Block>::const_reverse_component_iterator4d
			(this->last_back_bottom_right(component) - dp);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_iterator4d
GenericMultiComponent4d<Block>::rlast_back_bottom_right(const std::size_t component) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_iterator4d
			(this->first_front_upper_left(component));
}

//------------------------ Box component iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_iterator4d
GenericMultiComponent4d<Block>::first_front_upper_left(const std::size_t component,
		const Box4d<int>& box){
	return typename GenericMultiComponent4d<Block>::component_iterator4d(this,component,box);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_iterator4d
GenericMultiComponent4d<Block>::last_back_bottom_right(const std::size_t component,
		const Box4d<int>& box){
	DPoint4d<int> dp(box.duration(),box.depth(),box.height(),box.width());
	return (typename GenericMultiComponent4d<Block>::component_iterator4d
			((*this).first_front_upper_left(component,box) + dp));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_iterator4d
GenericMultiComponent4d<Block>::first_front_upper_left(const std::size_t component,
		const Box4d<int>& box) const{
	return typename GenericMultiComponent4d<Block>::const_component_iterator4d(this,component,box);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_iterator4d
GenericMultiComponent4d<Block>::last_back_bottom_right(const std::size_t component,
		const Box4d<int>& box) const{
	DPoint4d<int> dp(box.duration(),box.depth(),box.height(),box.width());
	return (typename GenericMultiComponent4d<Block>::const_component_iterator4d
			((*this).first_front_upper_left(component,box) + dp));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_iterator4d
GenericMultiComponent4d<Block>::rfirst_front_upper_left(const std::size_t component,
		const Box4d<int>& box){
	DPoint4d<int> dp(1,1,1,0);
	return typename GenericMultiComponent4d<Block>::reverse_component_iterator4d
			(this->last_back_bottom_right(component,box) - dp);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_iterator4d
GenericMultiComponent4d<Block>::rlast_back_bottom_right(const std::size_t component,
		const Box4d<int>& box){
	return typename GenericMultiComponent4d<Block>::reverse_component_iterator4d
			(this->first_front_upper_left(component,box));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_iterator4d
GenericMultiComponent4d<Block>::rfirst_front_upper_left(const std::size_t component,
		const Box4d<int>& box) const{
	DPoint4d<int> dp(1,1,1,0);
	return typename GenericMultiComponent4d<Block>::const_reverse_component_iterator4d
			(this->last_back_bottom_right(component,box) - dp);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_iterator4d
GenericMultiComponent4d<Block>::rlast_back_bottom_right(const std::size_t component,
		const Box4d<int>& box) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_iterator4d
			(this->first_front_upper_left(component,box));
}

//------------------------ Range component iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_iterator4d_range
GenericMultiComponent4d<Block>::first_front_upper_left(const std::size_t component,
		const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
		const slip::Range<int>& row_range,	const slip::Range<int>& col_range){
	return typename GenericMultiComponent4d<Block>::component_iterator4d_range
			(this,component,slab_range,slice_range,row_range,col_range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::component_iterator4d_range
GenericMultiComponent4d<Block>::last_back_bottom_right(const std::size_t component,
		const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
		const slip::Range<int>& row_range,	const slip::Range<int>& col_range){
	DPoint4d<int> dp(slab_range.iterations()+1,slice_range.iterations()+1,row_range.iterations()+1,col_range.iterations()+1);
	return typename GenericMultiComponent4d<Block>::component_iterator4d_range
			((*this).first_front_upper_left(component,slab_range,slice_range,row_range,col_range) + dp);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_iterator4d_range
GenericMultiComponent4d<Block>::first_front_upper_left(const std::size_t component,
		const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
		const slip::Range<int>& row_range,	const slip::Range<int>& col_range) const{
	return typename GenericMultiComponent4d<Block>::const_component_iterator4d_range
			(this,component,slab_range,slice_range,row_range,col_range);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_component_iterator4d_range
GenericMultiComponent4d<Block>::last_back_bottom_right(const std::size_t component,
		const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
		const slip::Range<int>& row_range,	const slip::Range<int>& col_range) const{
	DPoint4d<int> dp(slab_range.iterations()+1,slice_range.iterations()+1,row_range.iterations()+1,col_range.iterations()+1);
	return typename GenericMultiComponent4d<Block>::const_component_iterator4d_range
			((*this).first_front_upper_left(component,slab_range,slice_range,row_range,col_range) + dp);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_iterator4d_range
GenericMultiComponent4d<Block>::rfirst_front_upper_left(const std::size_t component,
		const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
		const slip::Range<int>& row_range,	const slip::Range<int>& col_range){
	DPoint4d<int> dp(1,1,1,0);
	return typename GenericMultiComponent4d<Block>::reverse_component_iterator4d_range
			(this->last_back_bottom_right(component,slab_range,slice_range,row_range,col_range) - dp);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reverse_component_iterator4d_range
GenericMultiComponent4d<Block>::rlast_back_bottom_right(const std::size_t component,
		const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
		const slip::Range<int>& row_range,	const slip::Range<int>& col_range){
	return typename GenericMultiComponent4d<Block>::reverse_component_iterator4d_range
			(this->first_front_upper_left(component,slab_range,slice_range,row_range,col_range));
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_iterator4d_range
GenericMultiComponent4d<Block>::rfirst_front_upper_left(const std::size_t component,
		const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
		const slip::Range<int>& row_range,	const slip::Range<int>& col_range) const{
	DPoint4d<int> dp(1,1,1,0);
	return typename GenericMultiComponent4d<Block>::const_reverse_component_iterator4d_range
			(this->last_back_bottom_right(component,slab_range,slice_range,row_range,col_range) - dp);
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reverse_component_iterator4d_range
GenericMultiComponent4d<Block>::rlast_back_bottom_right(const std::size_t component,
		const slip::Range<int>& slab_range, const slip::Range<int>& slice_range,
		const slip::Range<int>& row_range,	const slip::Range<int>& col_range) const{
	return typename GenericMultiComponent4d<Block>::const_reverse_component_iterator4d_range
			(this->first_front_upper_left(component,slab_range,slice_range,row_range,col_range));
}

// i/o operators

template<typename Block>
inline
std::ostream& operator<<(std::ostream & out, const GenericMultiComponent4d<Block>& a){
	out << *(a.matrix_);
	out << std::endl;
	return out;
}

//  Assignment operators and methods
template<typename Block>
inline
GenericMultiComponent4d<Block>& GenericMultiComponent4d<Block>::operator=
		(const GenericMultiComponent4d<Block> & rhs){
	if(this != &rhs)
	{
		*matrix_ = *(rhs.matrix_);
	}
	return *this;
}

template<typename Block>
inline
GenericMultiComponent4d<Block>& GenericMultiComponent4d<Block>::operator=
		(const Block& val){
	std::fill_n(matrix_->begin(),matrix_->size(),val);
	return *this;
}

template<typename Block>
inline
GenericMultiComponent4d<Block>& GenericMultiComponent4d<Block>::operator=
		(const typename Block::value_type& val){
	std::fill_n(matrix_->begin(),matrix_->size(),val);
	return *this;
}

// Comparison operators
template<typename Block>
inline
bool operator==(const GenericMultiComponent4d<Block>& x,
		const GenericMultiComponent4d<Block>& y){
	return ( x.size() == y.size()
			&& std::equal(x.begin(),x.end(),y.begin()));
}

template<typename Block>
inline
bool operator!=(const GenericMultiComponent4d<Block>& x,
		const GenericMultiComponent4d<Block>& y){
	return !(x == y);
}

//  Element access operators
template<typename Block>
inline
typename GenericMultiComponent4d<Block>::pointer**
GenericMultiComponent4d<Block>::operator[](const GenericMultiComponent4d<Block>::size_type t){
	return (*matrix_)[t];
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_pointer* const *
GenericMultiComponent4d<Block>::operator[](const GenericMultiComponent4d<Block>::size_type t) const{
	Matrix4d<Block> const *tp(matrix_);
	return (*tp)[t];
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::reference
GenericMultiComponent4d<Block>::operator()(const GenericMultiComponent4d<Block>::size_type t,
		const GenericMultiComponent4d<Block>::size_type k,
		const GenericMultiComponent4d<Block>::size_type i,
		const GenericMultiComponent4d<Block>::size_type j){
	return (*matrix_)[t][k][i][j];
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::const_reference
GenericMultiComponent4d<Block>::operator()(const GenericMultiComponent4d<Block>::size_type t,
		const GenericMultiComponent4d<Block>::size_type k,
		const GenericMultiComponent4d<Block>::size_type i,
		const GenericMultiComponent4d<Block>::size_type j) const{
	Matrix4d<Block> const *tp(matrix_);
	return (*tp)[t][k][i][j];
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::size_type
GenericMultiComponent4d<Block>::dim1() const{
	return matrix_->dim1();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::size_type
GenericMultiComponent4d<Block>::slabs() const{
	return matrix_->dim1();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::size_type
GenericMultiComponent4d<Block>::dim2() const{
	return matrix_->dim2();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::size_type
GenericMultiComponent4d<Block>::slices() const{
	return matrix_->dim2();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::size_type
GenericMultiComponent4d<Block>::dim3() const{
	return matrix_->dim3();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::size_type
GenericMultiComponent4d<Block>::rows() const{
	return matrix_->dim3();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::size_type
GenericMultiComponent4d<Block>::dim4() const{
	return matrix_->dim4();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::size_type
GenericMultiComponent4d<Block>::columns() const{
	return matrix_->dim4();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::size_type
GenericMultiComponent4d<Block>::cols() const{
	return matrix_->dim4();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::size_type
GenericMultiComponent4d<Block>::size() const{
	return matrix_->size();
}

template<typename Block>
inline
typename GenericMultiComponent4d<Block>::size_type
GenericMultiComponent4d<Block>::max_size() const{
	return matrix_->max_size();
}

template<typename Block>
inline
bool
GenericMultiComponent4d<Block>::empty()const{
	return matrix_->empty();
}

template<typename Block>
inline
void
GenericMultiComponent4d<Block>::swap(GenericMultiComponent4d<Block>& M){
	matrix_->swap(*(M.matrix_));
}

template<typename Block>
inline
Block
GenericMultiComponent4d<Block>::min() const{
	Block tmp;
	for(typename GenericMultiComponent4d<Block>::size_type component = 0; component < Block::SIZE; ++component)
	{
		typename Block::value_type min = *std::min_element(this->begin(component),this->end(component));
		tmp[component] = min;
	}
	return tmp;
}

template<typename Block>
inline
Block
GenericMultiComponent4d<Block>::max() const{
	Block tmp;
	for(typename GenericMultiComponent4d<Block>::size_type component = 0; component < Block::SIZE; ++component)
	{
		typename Block::value_type max = *std::max_element(this->begin(component),this->end(component));
		tmp[component] = max;
	}
	return tmp;
}

template<typename Block>
inline
GenericMultiComponent4d<Block>&
GenericMultiComponent4d<Block>::apply(Block(*fun)(Block)){
	matrix_->apply(fun);
	return *this;
}

template<typename Block>
inline
GenericMultiComponent4d<Block>&
GenericMultiComponent4d<Block>::apply(Block(*fun)(const Block&)){
	matrix_->apply(fun);
	return *this;
}

}//slip::

#endif //SLIP_GENERICMULTICOMPONENT4D_HPP
