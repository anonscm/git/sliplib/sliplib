/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file Signal.hpp
 * 
 * \brief Provides a class to manipulate signals.
 * 
 */
#ifndef SLIP_SIGNAL_HPP
#define SLIP_SIGNAL_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <complex>
#include <string>
#include "Vector.hpp"
#include "Box1d.hpp"
#include "stride_iterator.hpp"
#include "apply.hpp"
#include "norms.hpp"
#include "linear_algebra_traits.hpp"
#include "linear_algebra.hpp"
#include "complex_cast.hpp"
#include "Array2d.hpp"
#include "error.hpp"
#include "io_tools.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template <typename T>
class Signal;

template <typename T>
std::ostream& operator<<(std::ostream & out, 
			 const Signal<T>& v);

template<typename T>
bool operator==(const Signal<T>& x, 
		const Signal<T>& y);

template<typename T>
bool operator!=(const Signal<T>& x, 
		const Signal<T>& y);

template<typename T>
bool operator<(const Signal<T>& x, 
	       const Signal<T>& y);

template<typename T>
bool operator>(const Signal<T>& x, 
	       const Signal<T>& y);

template<typename T>
bool operator<=(const Signal<T>& x, 
		const Signal<T>& y);

template<typename T>
bool operator>=(const Signal<T>& x, 
		const Signal<T>& y);
/*!
 *  @defgroup MathematicalContainer Mathematical containers
 *  @brief Some mathematical containers
 *  @{
 */
/*! \class Signal
** \ingroup Containers Containers1d 
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/04/05
** \since 1.4.0
** \brief Numerical %Signal class. 
** This container statisfies the RandomAccessContainer concepts of the 
** Standard Template Library (STL).
** Data are stored using a %Vector class.
** It extends the interface of Vector adding signal read/write operations. 
** \param T. Type of object in the Signal 
*/
template <typename T>
class Signal
{
public :

  typedef T value_type;
  typedef Signal<T> self;
  typedef const Signal<T> const_self;

  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;

  typedef slip::stride_iterator<pointer> iterator_range;
  typedef slip::stride_iterator<const_pointer> const_iterator_range;

  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

  typedef std::reverse_iterator<iterator_range> reverse_iterator_range;
  typedef std::reverse_iterator<const_iterator_range> const_reverse_iterator_range;

  typedef typename slip::lin_alg_traits<value_type>::value_type norm_type;

   //default iterator of the container
  typedef iterator default_iterator;
  typedef const_iterator const_default_iterator;


  static const std::size_t DIM = 1;

public:
  /**
   ** \name Constructors & Destructors
   */
  /*@{*/
  
  /*!
  ** \brief Constructs a Signal.
  */
  Signal();
 
  /*!
  ** \brief Constructs a %Signal.
  ** \param n number of element in the %Signal
  */
  Signal(const size_type n);
  
  /*!
  ** \brief Constructs a %Signal initialized by the scalar value \a val.
  ** \param n number of element in the %Signal
  ** \param val initialization value of the elements 
  */
  Signal(const size_type n,
	 const T& val);
 
  /*!
  ** \brief Constructs a %Signal initialized by an array \a val.
  ** \param n number of element in the %Signal
  ** \param val initialization array value of the elements 
  */
  Signal(const size_type n,
	 const T* val);
 
  /**
  **  \brief  Contructs a %Signal from a range.
  **  \param  first  An input iterator.
  **  \param  last  An input iterator.
  **
  ** Create a %Signal consisting of copies of the elements from
  ** [first,last).
  */
  template<typename InputIterator>
  Signal(InputIterator first,
	 InputIterator last):
    vector_(new slip::Vector<T>(first,last))
  {}

 /*!
  ** \brief Constructs a copy of the Signal \a rhs
  */
  Signal(const self& rhs);

 /*!
  ** \brief Destructor of the Signal
  */
  ~Signal();
  

  /*@} End Constructors */


  /*!
  ** \brief Resizes a %Signal.
  ** \param new_n new dimension of the %Signal
  ** \param val new value for all the elements
  */ 
  void resize(const size_type new_n,
	      const T& val = T());

 
  /**
   ** \name iterators
   */
  /*@{*/
 
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %Signal.  Iteration is done in ordinary
  **  element order.
  ** \return begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator begin();

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %Signal.  Iteration is done in ordinary
  **  element order.
  ** \return const begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator begin() const;

  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %Signal.  Iteration is done in ordinary
  **  element order.
  ** \return end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator end();
 
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %Signal.  Iteration is done in
  **  ordinary element order.
  ** \return const end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator end() const;
  
  /*!
  **  \brief Returns a read/write reverse iterator that points to the
  **  last element in the %Signal.  Iteration is done in reverse
  **  element order.
  ** \return reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rbegin();
  
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to the last element in the %Signal.  Iteration is done in
  **  reverse element order.
  ** \return const reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_reverse_iterator rbegin() const;

  /*!
  **  \brief Returns a read/write reverse iterator that points to one
  **  before the first element in the %Signal.  Iteration is done
  **  in reverse element order.
  **  \return reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rend();

  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to one before the first element in the %Signal.  Iteration
  **  is done in reverse element order.
  **  \return const reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.

  */
  const_reverse_iterator rend() const;
 

  /*!
  **  \brief Returns a read/write iterator_range that points 
  **  the first element within the %Range.  
  **  Iteration is done in ordinary element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return end iterator_range value
  ** \pre The range must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Signal<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator_range begin(const slip::Range<int>& range);
 
  /*!
  ** \brief Returns a read-only (constant) iterator_range 
  ** that points the first element within the %Range.  
  ** Iteration is done in ordinary element order according to the
  ** %Range
  ** \param range %Range to iterate.
  ** \return const_iterator_range value
  ** \pre The range must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Signal<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout,"
  "));
   ** \endcode
  */
  const_iterator_range begin(const slip::Range<int>& range) const;

  /*!
  **  \brief Returns a read/write iterator_range that points 
  **  one past the last element in the %Signal.  
  **  Iteration is done in ordinary element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return end iterator_range value
  ** \pre The range must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Signal<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator_range end(const slip::Range<int>& range);
 
  /*!
  **  \brief Returns a read-only (constant) iterator_range 
  ** that points one past the last element in the %Signal.  
  ** Iteration is done in ordinary element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return const_iterator_range value
  ** \pre The range must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Signal<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout,"
  "));
  ** \endcode
  */
  const_iterator_range end(const slip::Range<int>& range) const;



  /*!
  **  \brief Returns a read/write iterator that points 
  **  the first element within the %Box1d.  
  **  Iteration is done in ordinary element order.
  ** \param box %Box1d to iterate.
  ** \return end iterator value
  ** \pre The box must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Signal<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.begin(box),A5.end(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator begin(const slip::Box1d<int>& box);
 
  /*!
  ** \brief Returns a read-only (constant) iterator 
  ** that points the first element within the %Box1d.  
  ** Iteration is done in ordinary element order.
  ** \param box %Box1d to iterate.
  ** \return const_iterator value
  ** \pre The box must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Signal<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.begin(box),A5.end(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  const_iterator begin(const slip::Box1d<int>& box) const;

  /*!
  **  \brief Returns a read/write iterator that points 
  **  one past the last element in the %Signal.  
  **  Iteration is done in ordinary element order.
  ** \param box %Box1d to iterate.
  ** \return end iterator value
  ** \pre The box must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Signal<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.begin(box),A5.end(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator end(const slip::Box1d<int>& box);
 
  /*!
  **  \brief Returns a read-only (constant) iterator 
  ** that points one past the last element within the %Box1d.  
  ** Iteration is done in ordinary element order.
  ** \param box %Box1d to iterate.
  ** \return const_iterator value
  ** \pre The box must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Signal<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.begin(box),A5.end(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  const_iterator end(const slip::Box1d<int>& box) const;

  //
    /*!
  **  \brief Returns a read/write reverse_iterator_range that points 
  **  the end element within the %Range.  
  **  Iteration is done in reverse element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Signal<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator_range rbegin(const slip::Range<int>& range);
 
  /*!
  ** \brief Returns a read-only (constant) reverse_iterator_range 
  ** that points the end element within the %Range.  
  ** Iteration is done in reverse element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return cons_treverse__iterator_range value
  ** \pre The range must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Signal<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout,"
  "));
   ** \endcode
  */
  const_reverse_iterator_range rbegin(const slip::Range<int>& range) const;

  /*!
  **  \brief Returns a read/write reverse_iterator_range that points 
  **  one previous the first element in the %Range.  
  **  Iteration is done in reverse element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Signal<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator_range rend(const slip::Range<int>& range);
 
  /*!
  **  \brief Returns a read-only (constant) reverse_iterator_range 
  ** that points one previous the first element in the %Range.  
  ** Iteration is done in reverse element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return const_reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Signal<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout,"
  "));
  ** \endcode
  */
  const_reverse_iterator_range rend(const slip::Range<int>& range) const;



  /*!
  **  \brief Returns a read/write reverse_iterator that points 
  **  the last element within the %Box1d.  
  **  Iteration is done in reverse element order.
  ** \param box %Box1d to iterate.
  ** \return  reverse_iterator value
  ** \pre The box must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Signal<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.rbegin(box),A5.rend(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator rbegin(const slip::Box1d<int>& box);
 
  /*!
  ** \brief Returns a read-only (constant) reverse_iterator 
  ** that points the last element within the %Box1d.  
  ** Iteration is done in reverse element order.
  ** \param box %Box1d to iterate.
  ** \return const_reverse_iterator value
  ** \pre The box must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Signal<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.rbegin(box),A5.rend(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  const_reverse_iterator rbegin(const slip::Box1d<int>& box) const;

  /*!
  **  \brief Returns a read/write reverse_iterator that points 
  **  one previous the first element in the %Box1d.  
  **  Iteration is done in reverse element order.
  ** \param box %Box1d to iterate.
  ** \return reverse_iterator value
  ** \pre The box must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Signal<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.rbegin(box),A5.rend(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator rend(const slip::Box1d<int>& box);
 
  /*!
  **  \brief Returns a read-only (constant) reverse_iterator 
  ** that points one previous the first element of the %Box1d.  
  ** Iteration is done in reverse element order.
  ** \param box %Box1d to iterate.
  ** \return const_reverse_iterator value
  ** \pre The box must be inside the whole range of the %Signal.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Signal<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.rbegin(box),A5.rend(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  const_reverse_iterator rend(const slip::Box1d<int>& box) const;
  /*@} End iterators */

  /**
   ** \name i/o operators
   */
  /*@{*/
  /*!
  ** \brief Write the %Signal to the ouput stream
  ** \param out output std:ostream
  ** \param v %Signal to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const self& v);

  /*!
  ** \brief Write the Signal to an ASCII file.
  ** \param file_path_name
  **
  */ 
  void write_ascii(const std::string& file_path_name) const
  {
   
    slip::write_ascii_1d<const_iterator,value_type>(this->begin(),
						    this->end(),
						    file_path_name);
  }

  /*!
  ** \brief Read the Signal from an ASCII file.
  ** \param file_path_name
  **
  */ 
  void read_ascii(const std::string& file_path_name)
  {
    slip::Array2d<value_type> A;
    slip::read_ascii_2d<slip::Array2d<value_type> >(A,file_path_name);
     try
       {
	 if((A.cols() == 1) && (A.rows() >= 1))
	   {
	     this->resize(A.rows(),value_type());
	     std::copy(A.col_begin(0),A.col_end(0),this->begin());
	   }
	 else if((A.rows() == 1) && (A.cols() > 1))
	       {
		 this->resize(A.cols(),value_type());
		 std::copy(A.row_begin(0),A.row_end(0),this->begin());
	       }
	 else
	   {
	     throw std::ios_base::failure(slip::SIGNAL_DIMENSION_ERROR + " in " +file_path_name);         
	   }
       }
      catch(std::exception& e)
      {
	std::cerr<<e.what()<<std::endl;
	exit(1);
      }
    }
  
  /*@} End i/o operators */
  

   /**
   ** \name  Assignment operators and methods
   */
  /*@{*/
  /*!
  ** \brief Assign a %Signal.
  **
  ** Assign elements of %Signal in \a rhs
  **
  ** \param rhs %Signal to get the values from.
  ** \return 
  */
  self& operator=(const self & rhs);

  /*!
  ** \brief Assigns all the element of the %Signal by val
  ** \param val affectation value
  ** \return reference to corresponding %Signal
  */
  self& operator=(const T& val);

   /*!
   ** \brief Fills the container range [begin(),begin()+size()) 
   **        with copies of value
   ** \param value  A reference-to-const of arbitrary type.
  */
  void fill(const T& value)
  {
    std::fill_n(this->begin(),this->size(),value);
  } 
  
 /*!
  ** \brief Fills the container range [begin(),begin()+size())
  **        with a copy of
  **        the value array
  ** \param value  A pointer of arbitrary type.
  */
  void fill(const T* value)
  {
    std::copy(value,value + this->size(), this->begin());
  } 

  /*!
  ** \brief Fills the container range [begin(),begin()+size()) 
  **        with a copy of the range [first,last)
  **  \param  first  An input iterator.
  **  \param  last   An input iterator.
  **   
  **
  */
  template<typename InputIterator>
  void fill(InputIterator first,
	    InputIterator last)
  {
    std::copy(first,last, this->begin());
  }
  /*@} End Assignment operators and methods*/


   /**
   ** \name Comparison operators
   */
  /*@{*/
  /*!
  ** \brief Signal equality comparison
  ** \param x A %Signal
  ** \param y A %Signal of the same type of \a x
  ** \return true iff the size and the elements of the Arrays are equal
  */
  friend bool operator== <>(const Signal<T>& x, 
			    const Signal<T>& y);

 /*!
  ** \brief Signal inequality comparison
  ** \param x A %Signal
  ** \param y A %Signal of the same type of \a x
  ** \return true if !(x == y) 
  */
  friend bool operator!= <>(const Signal<T>& x, 
			    const Signal<T>& y);

 /*!
  ** \brief Less than comparison operator (Signal ordering relation)
  ** \param x A %Signal
  ** \param y A %Signal of the same type of \a x
  ** \return true iff \a x is lexicographically less than \a y
  */
  friend bool operator< <>(const Signal<T>& x, 
			   const Signal<T>& y);

 /*!
  ** \brief More than comparison operator
  ** \param x A %Signal
  ** \param y A %Signal of the same type of \a x
  ** \return true iff y > x 
  */
  friend bool operator> <>(const Signal<T>& x, 
			   const Signal<T>& y);

  /*!
  ** \brief Less than equal comparison operator
  ** \param x A %Signal
  ** \param y A %Signal of the same type of \a x
  ** \return true iff !(y > x) 
  */
  friend bool operator<= <>(const Signal<T>& x, 
			    const Signal<T>& y);

 /*!
  ** \brief More than equal comparison operator
  ** \param x A %Signal
  ** \param y A %Signal of the same type of \a x
  ** \return true iff !(x < y) 
  */
  friend bool operator>= <>(const Signal<T>& x, 
			    const Signal<T>& y);


  /*@} Comparison operators */
  
  
  /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Subscript access to the data contained in the %Signal.
  ** \param i The index of the row for which the data should be accessed. 
  ** \return Read/write reference to data.
  ** \pre i < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator[](const size_type i);

  /*!
  ** \brief Subscript access to the data contained in the %Signal.
  ** \param i The index of the row for which the data should be accessed. 
   ** \return Read_only (constant) reference to data.
  ** \pre i < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator[](const size_type i) const;

  /*!
  ** \brief Subscript access to the data contained in the %Signal.
  ** \param i The index of the row for which the data should be accessed. 
  ** \return Read/write reference to data.
  ** \pre i < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const size_type i);

  /*!
  ** \brief Subscript access to the data contained in the %Signal.
  ** \param i The index of the row for which the data should be accessed. 
  ** \return Read_only (constant) reference to data.
  ** \pre i < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const size_type i) const;

  /*!
  ** \brief Subscript access to the data contained in the %Signal.
  ** \param p The index point corresponding of the element for which data 
  ** should be accessed.
  ** \return Read/write reference to data.
  ** \pre p[0] < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const slip::Point1d<size_type>& p);
  
  /*!
  ** \brief Subscript access to the data contained in the %Signal.
  ** \param p The index point corresponding of the element for which data 
  ** should be accessed.
  ** \return Read-only (constant) reference to data.
  ** \pre p[0] < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const slip::Point1d<size_type>& p) const;

    /*!
  ** \brief Subscript access to the data contained in the %Signal.
  ** \param range The range of the %Signal.
  ** \return a copy of the range.
  ** \pre range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Signal ones.
  **
  ** This operator allows for easy, 1d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  self operator()(const slip::Range<int>& range);

  /*@} End Element access operators */


  /**
   ** \name  Arithmetic operators
   */
  /*@{*/

  /*!
  ** \brief Add val to each element of the Signal  
  ** \param val value
  ** \return reference to the resulting Signal
  */
  self& operator+=(const T& val);
  self& operator-=(const T& val);
  self& operator*=(const T& val);
  self& operator/=(const T& val);
  //   self& operator%=(const T& val);
  //   self& operator^=(const T& val);
  //   self& operator&=(const T& val);
  //   self& operator|=(const T& val);
  //   self& operator<<=(const T& val);
  //   self& operator>>=(const T& val);

  self  operator-() const;
  //self  operator!() const;

  self& operator+=(const self& rhs);
  self& operator-=(const self& rhs);
  self& operator*=(const self& rhs);
  self& operator/=(const self& rhs);
    

  /*@} End Arithmetic operators */

  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;

  /*!
  ** \brief Returns the number of elements in the %Signal
  */
  size_type size() const;

  /*!
  ** \brief Returns the maximal size (number of elements) in the %Signal
  */
  size_type max_size() const;

  /*!
  ** \brief Returns true if the %Signal is empty.  (Thus size() == 0)
  */
  bool empty()const;

  /*!
  ** \brief Swaps data with another %Signal.
  ** \param M A %Signal of the same element type
  */
  void swap(self& M);

  /*!
  ** \brief Returns the min element of the Signal according to the operator <
  ** if T is std::complex, it returns the element of minimum magnitude
  ** \pre size() != 0
  */
  T& min() const;


  /*!
  ** \brief Returns the max element of the Signal according to the operator >,
  ** if T is std::complex, it returns the element of maximum magnitude
  ** \pre size() != 0
  */
  T& max() const;

  /*!
  ** \brief Returns the sum of the elements of the Signal 
  ** \pre size() != 0
  */
  T sum() const;

   /*!
  ** \brief Returns the Euclidean norm \f$\left(\sum_i x_i\bar{x_i} \right)^{\frac{1}{2}}\f$ of the elements of the Signal 
  ** \pre size() != 0
  */
  norm_type Euclidean_norm() const
  { 
    return (this->vector_)->Euclidean_norm();
  }

  /*!
  ** \brief Returns the Euclidean norm \f$\left(\sum_i x_i\bar{x_i} \right)^{\frac{1}{2}}\f$ of the elements of the Signal 
  ** \pre size() != 0
  */
  norm_type L2_norm() const
  {
    return (this->vector_)->L2_norm();
  }

 /*!
  ** \brief Returns the L1 norm (\f$\sum_i |xi|\f$) of the elements of the Signal 
  ** \pre size() != 0
  */
  norm_type L1_norm() const
  {
    
    return (this->vector_)->L1_norm();
  }

  /*!
  ** \brief Returns the L22 norm (\f$\sum_i xi\bar{x_i}\f$) of the elements of the Signal 
  ** \pre size() != 0
  */
  norm_type L22_norm() const
  {
    return (this->vector_)->L22_norm();
  }

    /*!
  ** \brief Returns the infinite norm (\f$\max_i\{|xi|\}\f$) of the elements of the Signal 
  ** \pre size() != 0
  */
  norm_type infinite_norm() const
  {
    return (this->vector_)->infinite_norm();
  }
  /*!
  ** \brief Applys the one-parameter C-function \a fun 
  **        to each element of the %Signal
  ** \param fun The one-parameter C function 
  ** \return the resulting %Signal
  ** \par Example:
  ** \code
  ** slip::Signal<double> V(7);
  ** //fill V with values from 1 to 7 with step 1
  ** slip::iota(V.begin(),V.end(),1.0,1.0);
  ** //apply std::sqrt to each element of V
  ** V.apply(std::sqrt);
  **
  ** \endcode
  */
  Signal<T>& apply(T (*fun)(T));

  /*!
  ** \brief Applys the one-parameter C-function \a fun 
  **        to each element of the %Signal
  ** \param fun The one-const-parameter C function 
  ** \return the resulting %Signal
  ** \par Example:
  ** \code
  ** slip::Signal<double> V(7);
  ** //fill V with values from 1 to 7 with step 1
  ** slip::iota(V.begin(),V.end(),1.0,1.0);
  ** //apply std::sqrt to each element of V
  ** V.apply(std::sqrt);
  **
  ** \endcode
  */
  Signal<T>& apply(T (*fun)(const T&));


 private:
  slip::Vector<T>* vector_;

   private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    { 
      if(version >= 0)
	{
	  ar & this->vector_;
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    { 
      if(version >= 0)
	{
	  ar & this->vector_;
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};
/** @} */ // end of MathematicalContainer group

 ///double alias
  typedef slip::Signal<double> Signal_d;
  ///float alias
  typedef slip::Signal<float> Signal_f;
  ///long alias
  typedef slip::Signal<long> Signal_l;
  ///unsigned long alias
  typedef slip::Signal<unsigned long> Signal_ul;
  ///short alias
  typedef slip::Signal<short> Signal_s;
  ///unsigned long alias
  typedef slip::Signal<unsigned short> Signal_us;
  ///int alias
  typedef slip::Signal<int> Signal_i;
  ///unsigned int alias
  typedef slip::Signal<unsigned int> Signal_ui;
  ///char alias
  typedef slip::Signal<char> Signal_c;
  ///unsigned char alias
  typedef slip::Signal<unsigned char> Signal_uc;

  /*!
  ** \brief pointwise addition of two %Signal
  ** \param V1 The first %Signal 
  ** \param V2 The second %Signal
  ** \pre V1.size() == V2.size()
  ** \return resulting %Signal
  */
template<typename T>
Signal<T> operator+(const Signal<T>& V1, 
		    const Signal<T>& V2);

 /*!
  ** \brief addition of a scalar to each element of a %Signal
  ** \param V The %Signal 
  ** \param val The scalar
  ** \return resulting %Signal
  */
template<typename T>
Signal<T> operator+(const Signal<T>& V, 
		    const T& val);

 /*!
  ** \brief addition of a scalar to each element of a %Signal
  ** \param val The scalar
  ** \param V The %Signal  
  ** \return resulting %Signal
  */
template<typename T>
Signal<T> operator+(const T& val, 
		    const Signal<T>& V);


/*!
  ** \brief pointwise substraction of two %Signal
  ** \param V1 The first %Signal 
  ** \param V2 The second %Signal
  ** \pre V1.size() == V2.size()
  ** \return resulting %Signal
  */
template<typename T>
Signal<T> operator-(const Signal<T>& V1, 
		    const Signal<T>& V2);

 /*!
  ** \brief substraction of a scalar to each element of a %Signal
  ** \param V The %Signal 
  ** \param val The scalar
  ** \return resulting %Signal
  */
template<typename T>
Signal<T> operator-(const Signal<T>& V, 
		    const T& val);

 /*!
  ** \brief substraction of a scalar to each element of a %Signal
  ** \param val The scalar
  ** \param V The %Signal  
  ** \return resulting %Signal
  */
template<typename T>
Signal<T> operator-(const T& val, 
		    const Signal<T>& V);

/*!
  ** \brief pointwise multiplication of two %Signal
  ** \param V1 The first %Signal 
  ** \param V2 The second %Signal
  ** \pre V1.size() == V2.size()
  ** \return resulting %Signal
  */
template<typename T>
Signal<T> operator*(const Signal<T>& V1, 
		    const Signal<T>& V2);

 /*!
  ** \brief multiplication of a scalar to each element of a %Signal
  ** \param V The %Signal 
  ** \param val The scalar
  ** \return resulting %Signal
  */
template<typename T>
Signal<T> operator*(const Signal<T>& V, 
		    const T& val);

 /*!
  ** \brief multiplication of a scalar to each element of a %Signal
  ** \param val The scalar
  ** \param V The %Signal  
  ** \return resulting %Signal
  */
template<typename T>
Signal<T> operator*(const T& val, 
		    const Signal<T>& V);

/*!
  ** \brief pointwise division of two %Signal
  ** \param V1 The first %Signal 
  ** \param V2 The second %Signal
  ** \pre V1.size() == V2.size()
  ** \return resulting %Signal
  */
  template<typename T>
  Signal<T> operator/(const Signal<T>& V1, 
		      const Signal<T>& V2);

 /*!
  ** \brief division of a scalar to each element of a %Signal
  ** \param V The %Signal 
  ** \param val The scalar
  ** \return resulting %Signal
  */
  template<typename T>
  Signal<T> operator/(const Signal<T>& V, 
		      const T& val);



 /*!
  ** \relates Signal
  ** \brief Returns the min element of a Signal, if T is std::complex, 
  ** it returns the element of minimum magnitude
  ** \param M1 the Signal  
  ** \return the min element
  */
  template<typename T>
  T& min(const Signal<T>& M1);
  
  /*!
  ** \relates Signal
  ** \brief Returns the max element of a Signal, if T is std::complex, 
  ** it returns the element of maximum magnitude
  ** \param M1 the Signal  
  ** \return the max element
  */
  template<typename T>
  T& max(const Signal<T>& M1);

  
  /*!
  ** \relates Signal
  ** \brief Returns the abs value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  template<typename T>
  Signal<typename slip::lin_alg_traits<T>::value_type> abs(const Signal<T>& V);

  /*!
  ** \relates Signal
  ** \brief Returns the sqrt value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  template<typename T>
  Signal<T> sqrt(const Signal<T>& V);

  /*!
  ** \relates Signal
  ** \brief Returns the cos value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  template<typename T>
  Signal<T> cos(const Signal<T>& V);

  /*!
  ** \relates Signal
  ** \brief Returns the acos value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  Signal<float> acos(const Signal<float>& V);
  /*!
  ** \relates Signal
  ** \brief Returns the acos value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  Signal<double> acos(const Signal<double>& V);
  /*!
  ** \relates Signal
  ** \brief Returns the acos value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  Signal<long double> acos(const Signal<long double>& V);
  
  /*!
  ** \relates Signal
  ** \brief Returns the sin value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  template<typename T>
  Signal<T> sin(const Signal<T>& V);

  /*!
  ** \relates Signal
  ** \brief Returns the asin value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  Signal<float> asin(const Signal<float>& V);
  /*!
  ** \relates Signal
  ** \brief Returns the asin value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  Signal<double> asin(const Signal<double>& V);
  /*!
  ** \relates Signal
  ** \brief Returns the asin value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  Signal<long double> asin(const Signal<long double>& V);
  
  /*!
  ** \relates Signal
  ** \brief Returns the tan value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  template<typename T>
  Signal<T> tan(const Signal<T>& V);

  /*!
  ** \relates Signal
  ** \brief Returns the atan value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  Signal<float> atan(const Signal<float>& V);
  /*!
  ** \relates Signal
  ** \brief Returns the atan value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  Signal<double> atan(const Signal<double>& V);
  /*!
  ** \relates Signal
  ** \brief Returns the atan value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  Signal<long double> atan(const Signal<long double>& V);
  

  /*!
  ** \relates Signal
  ** \brief Returns the exp value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  template<typename T>
  Signal<T> exp(const Signal<T>& V);

  /*!
  ** \relates Signal
  ** \brief Returns the log value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  template<typename T>
  Signal<T> log(const Signal<T>& V);

  /*!
  ** \relates Signal
  ** \brief Returns the cosh value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  template<typename T>
  Signal<T> cosh(const Signal<T>& V);

  /*!
  ** \relates Signal
  ** \brief Returns the sinh value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  template<typename T>
  Signal<T> sinh(const Signal<T>& V);

  /*!
  ** \relates Signal
  ** \brief Returns the tanh value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  template<typename T>
  Signal<T> tanh(const Signal<T>& V);

  /*!
  ** \relates Signal
  ** \brief Returns the log10 value of each element of the %Signal
  ** \param V The %Signal  
  ** \return the resulting %Signal
  */
  template<typename T>
  Signal<T> log10(const Signal<T>& V);

}//slip::

namespace slip
{
  template<typename T>
  inline
  Signal<T>::Signal():
    vector_(new slip::Vector<T>())
  {}

  template<typename T>
  inline
  Signal<T>::Signal(typename Signal<T>::size_type n):
    vector_(new slip::Vector<T>(n))
  {}
  
  template<typename T>
  inline
  Signal<T>::Signal(typename Signal<T>::size_type n,
		    const T& val):
    vector_(new slip::Vector<T>(n,val))
  {}

  template<typename T>
  inline
  Signal<T>::Signal(typename Signal<T>::size_type n,
		    const T* val):
    vector_(new slip::Vector<T>(n,val))   
  {}
  
  template<typename T>
  inline
  Signal<T>::Signal(const Signal<T>& rhs):
    vector_(new slip::Vector<T>((*rhs.vector_)))
  {}
  
  template<typename T>
  inline
  Signal<T>& Signal<T>::operator=(const Signal<T> & rhs)
  {
    if(this != &rhs)
      {
	*(this->vector_) = *(rhs.vector_);
      }
    return *this;
  }

  template<typename T>
  inline
  Signal<T>::~Signal()
  {
    delete vector_;
  }

  template<typename T>
  inline
  Signal<T>& Signal<T>::operator=(const T& val)
  {
    (this->vector_)->operator=(val);
    return *this;
  }
  
  template<typename T>
  inline
  void Signal<T>::resize(typename Signal<T>::size_type new_size,
			 const T& val)
  {
    (this->vector_)->resize(new_size,val);
  }


  template<typename T>
  inline
  typename Signal<T>::iterator Signal<T>::begin()
  {
    return vector_->begin();
  }

  template<typename T>
  inline
  typename Signal<T>::iterator Signal<T>::end()
  {
    return vector_->end();
  }
  
  template<typename T>
  inline
  typename Signal<T>::const_iterator Signal<T>::begin() const
  {
    slip::Vector<T> const * tp(vector_);
    return tp->begin();
  }

  template<typename T>
  inline
  typename Signal<T>::const_iterator Signal<T>::end() const
  {
    slip::Vector<T> const * tp(vector_);
    return tp->end();
  }
  

  template<typename T>
  inline
  typename Signal<T>::reverse_iterator Signal<T>::rbegin()
  {
    return vector_->rbegin();
  }

  template<typename T>
  inline
  typename Signal<T>::reverse_iterator Signal<T>::rend()
  {
    return vector_->rend();
  }

  template<typename T>
  inline
  typename Signal<T>::const_reverse_iterator Signal<T>::rbegin() const
  {
    slip::Vector<T> const * tp(vector_);
    return tp->rbegin();
  }

  template<typename T>
  inline
  typename Signal<T>::const_reverse_iterator  Signal<T>::rend() const
  {
    slip::Vector<T> const * tp(vector_);
    return tp->rend();
  }

  //
  template<typename T>
  inline
  typename Signal<T>::iterator_range 
  Signal<T>::begin(const slip::Range<int>& range)
  {
    return vector_->begin(range);
  }

  template<typename T>
  inline
  typename Signal<T>::iterator_range 
  Signal<T>::end(const slip::Range<int>& range)
  {
    return vector_->end(range);
  }
  
  template<typename T>
  inline
  typename Signal<T>::const_iterator_range 
  Signal<T>::begin(const slip::Range<int>& range) const
  {
   slip::Vector<T> const * tp(vector_);
    return tp->begin(range);
  }

  template<typename T>
  inline
  typename Signal<T>::const_iterator_range 
  Signal<T>::end(const slip::Range<int>& range) const
  {
    slip::Vector<T> const * tp(vector_);
    return tp->end(range);
  }

  //
  template<typename T>
  inline
  typename Signal<T>::iterator
  Signal<T>::begin(const slip::Box1d<int>& box)
  {
    return vector_->begin(box);
  }

  template<typename T>
  inline
  typename Signal<T>::iterator 
  Signal<T>::end(const slip::Box1d<int>& box)
  {
    return vector_->end(box);
  }
  
  template<typename T>
  inline
  typename Signal<T>::const_iterator 
  Signal<T>::begin(const slip::Box1d<int>& box) const
  {
    slip::Vector<T> const * tp(vector_);
    return tp->begin(box);
  }

  template<typename T>
  inline
  typename Signal<T>::const_iterator 
  Signal<T>::end(const slip::Box1d<int>& box) const
  {
    slip::Vector<T> const * tp(vector_);
    return tp->end(box);
  }

  //
  template<typename T>
  inline
  typename Signal<T>::reverse_iterator Signal<T>::rbegin(const slip::Box1d<int>& box)
  {
    return vector_->rbegin(box);
  }

  template<typename T>
  inline
  typename Signal<T>::reverse_iterator 
  Signal<T>::rend(const slip::Box1d<int>& box)
  {
    return vector_->rend(box);
  }

  template<typename T>
  inline
  typename Signal<T>::const_reverse_iterator 
  Signal<T>::rbegin(const slip::Box1d<int>& box) const
  {
   slip::Vector<T> const * tp(vector_);
    return tp->rbegin(box);
  }

  template<typename T>
  inline
  typename Signal<T>::const_reverse_iterator 
  Signal<T>::rend(const slip::Box1d<int>& box) const
  {
    slip::Vector<T> const * tp(vector_);
    return tp->rend(box);
  }
  //
  template<typename T>
  inline
  typename Signal<T>::reverse_iterator_range Signal<T>::rbegin(const slip::Range<int>& range)
  {
    return vector_->rbegin(range);
  }

  template<typename T>
  inline
  typename Signal<T>::reverse_iterator_range 
  Signal<T>::rend(const slip::Range<int>& range)
  {
    return vector_->rend(range);
  }

  template<typename T>
  inline
  typename Signal<T>::const_reverse_iterator_range 
  Signal<T>::rbegin(const slip::Range<int>& range) const
  {
    slip::Vector<T> const * tp(vector_);
    return tp->rbegin(range);
  }

  template<typename T>
  inline
  typename Signal<T>::const_reverse_iterator_range 
  Signal<T>::rend(const slip::Range<int>& range) const
  {
    slip::Vector<T> const * tp(vector_);
    return tp->rend(range);
  }


  template <typename T>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const Signal<T>& v)
  {
    out<<*(v.vector_);
    return out;
  }

  template<typename T>
  inline
  typename Signal<T>::reference Signal<T>::operator[](typename Signal<T>::size_type i)
  {
    return (*vector_)[i];
  }
  
  template<typename T>
  inline
  typename Signal<T>::const_reference Signal<T>::operator[](typename Signal<T>::size_type i) const 
  {
    return (*vector_)[i];
  }
  
  template<typename T>
  inline
  typename Signal<T>::reference Signal<T>::operator()(typename Signal<T>::size_type i)
  {
    return (*vector_)(i);
  }
  
  template<typename T>
  inline
  typename Signal<T>::const_reference Signal<T>::operator()(typename Signal<T>::size_type i) const
  {
    return (*vector_)(i);
  }

  template <typename T>
  inline
  typename Signal<T>::reference 
  Signal<T>::operator()(const slip::Point1d<typename Signal<T>::size_type>& p)
  {
    return (*vector_)(p);
  }

  template <typename T>
  inline
  typename Signal<T>::const_reference 
  Signal<T>::operator()(const slip::Point1d<typename Signal<T>::size_type>& p) const
  {
    return (*vector_)(p);
  }

  template<typename T>
  inline
  Signal<T> Signal<T>::operator()(const slip::Range<int>& range)
  {
     assert(this->vector_ != 0);
     assert(range.is_valid());
     assert(range.start() < (int)this->size());
     assert(range.stop()  < (int)this->size());
     return Signal<T>(vector_->begin(range),vector_->end(range));

  }

  template<typename T>
  inline
  std::string 
  Signal<T>::name() const {return "Signal";} 

  template<typename T>
  inline
  typename Signal<T>::size_type Signal<T>::size() const 
  {
    return (this->vector_)->size();
  }

  template<typename T>
  inline
  typename Signal<T>::size_type Signal<T>::max_size() const 
  {
    return (this->vector_)->max_size();
  }
  
  template<typename T>
  inline
  bool Signal<T>::empty()const 
  {
    return (this->vector_)->empty();
  }

  template<typename T>
  inline
  void Signal<T>::swap(Signal<T>& V)
  {
    (this->vector_)->swap(*(V.vector_));
  }


  template<typename T>
  inline
  Signal<T>& Signal<T>::operator+=(const T& val)
  {
    (this->vector_)->operator+=(val);
    return *this;
  }

  template<typename T>
  inline
  Signal<T>& Signal<T>::operator-=(const T& val)
  {
    (this->vector_)->operator-=(val);
    return *this;
  }

  template<typename T>
  inline
  Signal<T>& Signal<T>::operator*=(const T& val)
  {
    (this->vector_)->operator*=(val);
    return *this;
  }
  
  template<typename T>
  inline
  Signal<T>& Signal<T>::operator/=(const T& val)
  {
    (this->vector_)->operator/=(val);
    return *this;
  }
  

  template<typename T>
  inline
  Signal<T> Signal<T>::operator-() const
  {
    *(this->vector_) = (this->vector_)->operator-();
    return *this;
  }

  template<typename T>
  inline
  Signal<T>& Signal<T>::operator+=(const Signal<T>& rhs)
  {
    (this->vector_)->operator+=(*(rhs.vector_));
    return *this;
  }
  
  template<typename T>
  inline
  Signal<T>& Signal<T>::operator-=(const Signal<T>& rhs)
  {
    (this->vector_)->operator-=(*(rhs.vector_));
    return *this;
  }
  
  template<typename T>
  inline
  Signal<T>& Signal<T>::operator*=(const Signal<T>& rhs)
  {
    (this->vector_)->operator*=(*(rhs.vector_));
    return *this;
  }

   template<typename T>
  inline
  Signal<T>& Signal<T>::operator/=(const Signal<T>& rhs)
  {
    (this->vector_)->operator/=(*(rhs.vector_));
    return *this;
  }
  
  template<typename T>
  inline
  T& Signal<T>::min() const
  {
    return (this->vector_)->min();
  }

  template<typename T>
  inline
  T& Signal<T>::max() const
  {
    return (this->vector_)->max();
  }

  template<typename T>
  inline
  T Signal<T>::sum() const
  {
    return (this->vector_)->sum();
  }

  template<typename T>
  inline
  Signal<T>& Signal<T>::apply(T (*fun)(T))
  {
    (this->vector_)->apply(fun);
    return *this;

  }

  template<typename T>
  inline
  Signal<T>& Signal<T>::apply(T (*fun)(const T&))
  {
    (this->vector_)->apply(fun);
    return *this;
  }


  
/** \name Arithmetical functions */
  /* @{ */
template<typename T>
inline
Signal<T> operator+(const Signal<T>& V1, 
		    const Signal<T>& V2)
{
    assert(V1.size() == V2.size());
    Signal<T> tmp(V1.size());
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::plus<T>());

    return tmp;
}

template<typename T>
inline
Signal<T> operator+(const Signal<T>& V1, 
		    const T& val)
{
    Signal<T> tmp(V1);
    tmp+=val;
    return tmp;
}

template<typename T>
inline
Signal<T> operator+(const T& val,
		    const Signal<T>& V1)
{
  return V1 + val;
}

template<typename T>
inline
Signal<T> operator-(const Signal<T>& V1, 
		    const Signal<T>& V2)
{
    assert(V1.size() == V2.size());
    Signal<T> tmp(V1.size());
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::minus<T>());
    return tmp;
}

template<typename T>
inline
Signal<T> operator-(const Signal<T>& V1, 
		    const T& val)
{
    Signal<T> tmp(V1);
    tmp-=val;
    return tmp;
}

template<typename T>
inline
Signal<T> operator-(const T& val,
		    const Signal<T>& V1)
{
  return -(V1 - val);
}

template<typename T>
inline
Signal<T> operator*(const Signal<T>& V1, 
		    const Signal<T>& V2)
{
    assert(V1.size() == V2.size());
    Signal<T> tmp(V1.size());
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::multiplies<T>());
    return tmp;
}

template<typename T>
inline
Signal<T> operator*(const Signal<T>& V1, 
		    const T& val)
{
    Signal<T> tmp(V1);
    tmp*=val;
    return tmp;
}

template<typename T>
inline
Signal<T> operator*(const T& val,
		    const Signal<T>& V1)
{
  return V1 * val;
}

template<typename T>
inline
Signal<T> operator/(const Signal<T>& V1, 
		    const Signal<T>& V2)
{
    assert(V1.size() == V2.size());
    Signal<T> tmp(V1.size());
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::divides<T>());
    return tmp;
}

template<typename T>
inline
Signal<T> operator/(const Signal<T>& V1, 
		    const T& val)
{
    Signal<T> tmp(V1);
    tmp/=val;
    return tmp;
}

/* @} */

template<typename T>
inline
T& min(const Signal<T>& V1)
{
  return V1.min();
}

template<typename T>
inline
T& max(const Signal<T>& V1)
{
  return V1.max();
}

template<typename T>
inline
Signal<typename slip::lin_alg_traits<T>::value_type> abs(const Signal<T>& V)
{
  
  Signal<typename slip::lin_alg_traits<T>::value_type> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::abs);
  return tmp;
}

template<typename T>
inline
Signal<T> sqrt(const Signal<T>& V)
{
  Signal<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::sqrt);
  return tmp;
}

template<typename T>
inline
Signal<T> cos(const Signal<T>& V)
{
  Signal<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::cos);
  return tmp;
}


inline
Signal<float> acos(const Signal<float>& V)
{
  Signal<float> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::acos);
  return tmp;
}

inline
Signal<double> acos(const Signal<double>& V)
{
  Signal<double> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::acos);
  return tmp;
}

inline
Signal<long double> acos(const Signal<long double>& V)
{
  Signal<long double> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::acos);
  return tmp;
}

template<typename T>
inline
Signal<T> sin(const Signal<T>& V)
{
  Signal<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::sin);
  return tmp;
}



inline
Signal<float> asin(const Signal<float>& V)
{
  Signal<float> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::asin);
  return tmp;
}

inline
Signal<double> asin(const Signal<double>& V)
{
  Signal<double> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::asin);
  return tmp;
}

inline
Signal<long double> asin(const Signal<long double>& V)
{
  Signal<long double> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::asin);
  return tmp;
}

template<typename T>
inline
Signal<T> tan(const Signal<T>& V)
{
  Signal<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::tan);
  return tmp;
}

inline
Signal<float> atan(const Signal<float>& V)
{
  Signal<float> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::atan);
  return tmp;
}

inline
Signal<double> atan(const Signal<double>& V)
{
  Signal<double> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::atan);
  return tmp;
}

inline
Signal<long double> atan(const Signal<long double>& V)
{
  Signal<long double> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::atan);
  return tmp;
}

template<typename T>
inline
Signal<T> exp(const Signal<T>& V)
{
  Signal<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::exp);
  return tmp;
}

template<typename T>
inline
Signal<T> log(const Signal<T>& V)
{
  Signal<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::log);
  return tmp;
}

template<typename T>
inline
Signal<T> cosh(const Signal<T>& V)
{
  Signal<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::cosh);
  return tmp;
}

template<typename T>
inline
Signal<T> sinh(const Signal<T>& V)
{
  Signal<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::sinh);
  return tmp;
}

template<typename T>
inline
Signal<T> tanh(const Signal<T>& V)
{
  Signal<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::tanh);
  return tmp;
}

template<typename T>
inline
Signal<T> log10(const Signal<T>& V)
{
  Signal<T> tmp(V.size());
  slip::apply(V.begin(),V.end(),tmp.begin(),std::log10);
  return tmp;
}

/** \name EqualityComparable functions */
  /* @{ */
template<typename T>
inline
bool operator==(const Signal<T>& x, 
		const Signal<T>& y)
{
  return *(x.vector_) == *(y.vector_); 
}

template<typename T>
inline
bool operator!=(const Signal<T>& x, 
		const Signal<T>& y)
{
  return !(x == y);
}
/* @} */
/** \name LessThanComparable functions */
  /* @{ */
template<typename T>
inline
bool operator<(const Signal<T>& x, 
	       const Signal<T>& y)
{
  return  *(x.vector_) < *(y.vector_);
}

template<typename T>
inline
bool operator>(const Signal<T>& x, 
	       const Signal<T>& y)
{
  return (y < x);
}

template<typename T>
inline
bool operator<=(const Signal<T>& x, 
		const Signal<T>& y)
{
  return !(y < x);
}

template<typename T>
inline
bool operator>=(const Signal<T>& x, 
		const Signal<T>& y)
{
  return !(x < y);
} 
/* @} */
}//slip::

#endif //SLIP_SIGNAL_HPP
