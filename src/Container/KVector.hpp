/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file KVector.hpp
 * 
 * \brief Provides a class to manipulate static and generic vectors.
 * 
 */
#ifndef KVECTOR_HPP
#define KVECTOR_HPP

#include <iterator>
#include <iostream>
#include <numeric>
#include <algorithm>
#include <string>
#include <cstddef>
#include "Range.hpp"
#include "stride_iterator.hpp"
#include "norms.hpp"
#include "apply.hpp"
#include "Block.hpp"
#include "complex_cast.hpp"
#include "linear_algebra.hpp"
#include "linear_algebra_traits.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template <class T, std::size_t N>
struct kvector;

template <typename T, std::size_t N>
std::ostream& operator<<(std::ostream & out,
			 const kvector<T,N>& b);

template <typename T, std::size_t N>
bool operator==(const kvector<T,N>& x, 
		const kvector<T,N>& y);

template <typename T, std::size_t N>
bool operator!=(const kvector<T,N>& x, 
		const kvector<T,N>& y);

template <typename T, std::size_t N>
bool operator<(const kvector<T,N>& x, 
	       const kvector<T,N>& y);

template <typename T, std::size_t N>
bool operator>(const kvector<T,N>& x, 
	       const kvector<T,N>& y);

template <typename T, std::size_t N>
bool operator<=(const kvector<T,N>& x, 
		const kvector<T,N>& y);

template <typename T, std::size_t N>
bool operator>=(const kvector<T,N>& x, 
		const kvector<T,N>& y);

/*! \struct kvector
** \ingroup Containers Containers1d MathematicalContainer
** \author Benoit Tremblais <tremblais@sic.univ-poitiers.fr>
** \version 0.0.3
** \date 2014/03/25
** \since 1.0.0
** \brief This is a linear (one-dimensional) static vector.
**  This container statisfies the RandomAccessContainer concepts of the 
**  Standard Template Library (STL).
**  It extends the interface of Array adding arithmetical: +=, -=, *=, /=,
**  +,-,/,*... and mathematical operators : min, max, abs, sqrt, cos, acos, 
**  sin, asin, tan, atan, exp, log, cosh, sinh, tanh, log10, sum, apply,
**  Euclidean norm, L1 norm, L2 norm...
** \param T Type of the element in the %kvector
** \param N number of element in the %kvector 
**
*/
template <class T, std::size_t N>
struct kvector{

  typedef T value_type;
  typedef kvector<T,N> self;
  typedef const kvector<T,N> const_self;

  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;
  typedef slip::stride_iterator<pointer> iterator_range;
  typedef slip::stride_iterator<const_pointer> const_iterator_range;

  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
  
  typedef std::reverse_iterator<iterator_range> reverse_iterator_range;
  typedef std::reverse_iterator<const_iterator_range> const_reverse_iterator_range;

  typedef typename slip::lin_alg_traits<value_type>::value_type norm_type;

  
  //default iterator of the container
  typedef iterator default_iterator;
  typedef const_iterator const_default_iterator;

  static const std::size_t SIZE = N;
  static const std::size_t DIM = 1;
  /**
   ** \name iterators
   */
  /*@{*/
 
 
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %kvector.  Iteration is done in ordinary
  **  element order.
  ** \return begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator begin(){return this->data;}
  

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %kvector.  Iteration is done in ordinary
  **  element order.
  ** \return const begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator begin() const {return this->data;} 

  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %kvector.  Iteration is done in ordinary
  **  element order.
  ** \return end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator end(){return this->data + N;}
 
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %kvector.  Iteration is done in
  **  ordinary element order.
  ** \return const end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator end() const {return this->data + N;}


  /*!
  **  \brief Returns a read/write iterator_range that points 
  **  the first element within the %Range.  
  **  Iteration is done in ordinary element order  according to the
  **  %Range.
  ** \param range. %Range to iterate.
  ** \return end iterator_range value
  ** \pre The range must be inside the whole range of the %kvector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::kvector<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator_range begin(const slip::Range<int>& range)
  {
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < int(this->size()));
    assert(range.stop()  < int(this->size()));
    return iterator_range(this->begin() + range.start(),range.stride());
  }
 
  /*!
  ** \brief Returns a read-only (constant) iterator_range 
  ** that points the first element within the %Range.  
  ** Iteration is done in ordinary element order according to the
  ** %Range.
  ** \param range. %Range to iterate.
  ** \return const_iterator_range value
  ** \pre The range must be inside the whole range of the %kvector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::kvector<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout,"
  "));
   ** \endcode
  */
  const_iterator_range begin(const slip::Range<int>& range) const
  {
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < int(this->size()));
    assert(range.stop()  < int(this->size()));
    return const_iterator_range(this->begin() + range.start(),range.stride());
  }

  /*!
  **  \brief Returns a read/write iterator_range that points 
  **  one past the last element in the %kvector.  
  **  Iteration is done in ordinary element order  according to the
  **  %Range.
  ** \param range. %Range to iterate.
  ** \return end iterator_range value
  ** \pre The range must be inside the whole range of the %kvector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::kvector<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator_range end(const slip::Range<int>& range)
  {
    return this->begin(range) + (range.iterations() + 1);
  }
 
  /*!
  **  \brief Returns a read-only (constant) iterator_range 
  ** that points one past the last element in the %kvector.  
  ** Iteration is done in ordinary element order according to the
  ** %Range.
  ** \param range. %Range to iterate.
  ** \return const_iterator_range value
  ** \pre The range must be inside the whole range of the %kvector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::kvector<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout,"
  "));
  ** \endcode
  */
  const_iterator_range end(const slip::Range<int>& range) const
  {
    return this->begin(range) + (range.iterations() + 1);
  }


  /*!
  **  \brief Returns a read/write reverse iterator that points to the
  **  last element in the %kvector.  Iteration is done in reverse
  **  element order.
  ** \return reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rbegin(){return reverse_iterator(this->end());}

  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to the last element in the %kvector.  Iteration is done in
  **  reverse element order.
  ** \return const reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_reverse_iterator rbegin() const {return const_reverse_iterator(this->end());}

  /*!
  **  \brief Returns a read/write reverse iterator that points to one
  **  before the first element in the %kvector.  Iteration is done
  **  in reverse element order.
  **  \return reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rend(){return reverse_iterator(this->begin());}

    
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to one before the first element in the %kvector.  Iteration
  **  is done in reverse element order.
  **  \return const reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_reverse_iterator rend() const {return const_reverse_iterator(this->begin());}
  

  /*!
  **  \brief Returns a read/write reverse_iterator_range that points 
  **  the end element within the %Range.  
  **  Iteration is done in reverse element order  according to the
  **  %Range.
  ** \param range. %Range to iterate.
  ** \return reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %kvector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::kvector<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator_range rbegin(const slip::Range<int>& range)
  {
    return reverse_iterator_range(this->end(range));
  }
 
  /*!
  ** \brief Returns a read-only (constant) reverse_iterator_range 
  ** that points the end element within the %Range.  
  ** Iteration is done in reverse element order according to the
  ** %Range.
  ** \param range. %Range to iterate.
  ** \return cons_treverse__iterator_range value
  ** \pre The range must be inside the whole range of the %kvector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::kvector<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout,"
  "));
   ** \endcode
  */
  const_reverse_iterator_range rbegin(const slip::Range<int>& range) const
  {
    return const_reverse_iterator_range(this->end(range));
  }
 

  /*!
  **  \brief Returns a read/write reverse_iterator_range that points 
  **  one previous the first element in the %Range.  
  **  Iteration is done in reverse element order  according to the
  **  %Range.
  ** \param range. %Range to iterate.
  ** \return reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %kvector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::kvector<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator_range rend(const slip::Range<int>& range)
  {
    return  reverse_iterator_range(this->begin(range));
  }
 
  /*!
  **  \brief Returns a read-only (constant) reverse_iterator_range 
  ** that points one previous the first element in the %Range.  
  ** Iteration is done in reverse element order according to the
  ** %Range.
  ** \param range. %Range to iterate.
  ** \return const_reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %kvector.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::kvector<double,8> A5;
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout,"
  "));
  ** \endcode
  */
  const_reverse_iterator_range rend(const slip::Range<int>& range) const
  {
     return  const_reverse_iterator_range(this->begin(range));
  }

  /*@} End iterators */

  /**
   ** \name i/o operators
   */
  /*@{*/
  
  /*!
  ** \brief Write the %kvector to an ouput stream
  ** \param out output std::ostream
  ** \param b %kvector to write to an output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const kvector<T,N>& b);

  /*@} End i/o operators */


  /**
   ** \name  Assignment methods
   */
  /*@{*/


  /*!
  ** \brief Assigns all the element of the %kvector by the %kvector rhs
  ** \param rhs kvector<T,N>
  ** \return reference to corresponding %kvector
  */
  kvector<T,N>& operator=(const kvector<T,N>& rhs)
  {
     if(this != &rhs)
      {
	std::copy(rhs.begin(),rhs.end(),this->begin());
      }
    return *this;
  }

  /*!
  ** \brief Assigns all the element of the %kvector by val
  ** \param val affectation value
  ** \return reference to corresponding %kvector
  */
  kvector<T,N>& operator=(const T& val)
  {
    this->fill(val);
    return *this;
  }

 

  /*!
  ** \brief Fills the container range [begin(),begin()+N) with copies of value
  ** \param value  A reference-to-const of arbitrary type.
  */
  void fill(const T& value)
  {
    std::fill_n(this->begin(),N,value);
  } 
  
 /*!
  ** \brief Fills the container range [begin(),begin()+N) with a copy of
  **        the value array
  ** \param value  A pointer of arbitrary type.
  */
  void fill(const T* value)
  {
    std::copy(value,value + N, this->begin());
  } 

  /*!
  ** \brief Fills the container range [begin(),begin()+N) with a copy of
  **        the range [first,last)
  **  \param  first  An input iterator.
  **  \param  last   An input iterator.
  **   
  **
  */
  template<typename InputIterator>
  void fill(InputIterator first,
	    InputIterator last)
  {
    std::copy(first,last, this->begin());
  }
  /*@} End Assignment operators and methods*/

  /**
   ** \name Comparison operators
   */
  /*@{*/
  /*!
  ** \brief Kvector equality comparison
  ** \param x A %kvector
  ** \param y A %kvector of the same type of \a x
  ** \return true iff the size and the elements of the %kvectors are equal
  */
  friend bool operator== <>(const kvector<T,N>& x, 
			    const kvector<T,N>& y);

 /*!
  ** \brief Kvector inequality comparison
  ** \param x A %kvector
  ** \param y A %kvector of the same type of \a x
  ** \return true if !(x == y) 
  */
  friend bool operator!= <>(const kvector<T,N>& x, 
			    const kvector<T,N>& y);

 /*!
  ** \brief Less than comparison operator (%kvector ordering relation)
  ** \param x A %kvector
  ** \param y A %kvector of the same type of \a x
  ** \return true iff \a x is lexicographically less than \a y
  ** \pre x.size() == y.size()
  */
  friend bool operator< <>(const kvector<T,N>& x, 
			   const kvector<T,N>& y);

 /*!
  ** \brief More than comparison operator
  ** \param x A %kvector
  ** \param y A %kvector of the same type of \a x
  ** \return true iff y > x 
  ** \pre x.size() == y.size()
  */
  friend bool operator> <>(const kvector<T,N>& x, 
			   const kvector<T,N>& y);

  /*!
  ** \brief Less than equal comparison operator
  ** \param x A %kvector
  ** \param y A %kvector of the same type of \a x
  ** \return true iff !(y > x) 
  ** \pre x.size() == y.size()
  */
  friend bool operator<= <>(const kvector<T,N>& x, 
			    const kvector<T,N>& y);

 /*!
  ** \brief More than equal comparison operator
  ** \param x A %kvector
  ** \param y A %kvector of the same type of \a x
  ** \return true iff !(x < y) 
  ** \pre x.size() == y.size()
  */
  friend bool operator>= <>(const kvector<T,N>& x, 
			    const kvector<T,N>& y);


  /*@} Comparison operators */


  /**
   ** \name Element access operators
   */
  /*@{*/
  /*!
  ** \brief Subscript access to the data contained in the %kvector.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read/write reference to data.
  ** \pre n < N
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator[](const size_type n){return *(this->data + n);}

  /*!
  ** \brief Subscript access to the data contained in the %kvector.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read-only (constant) reference to data.
  ** \pre n < N
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator[](const size_type n) const {return *(this->data + n);}

  /*!
  ** \brief Subscript access to the data contained in the %kvector.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read/write reference to data.
  ** \pre n < N
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const size_type n) {return *(this->data + n);}
  
  
  /*!
  ** \brief Subscript access to the data contained in the %kvector.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read-only (constant) reference to data.
  ** \pre n < N
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const size_type n) const {return *(this->data + n);}

  /*@} End element access operators */

/**
   ** \name  Arithmetic operators
   */
  /*@{*/

  /*!
  ** \brief Add val to each element of the kvector  
  ** \param val value
  ** \return reference to the resulting kvector
  */
  self& operator+=(const T& val)
  {
    std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::plus<T>(),val));
    return *this;
  }
   
  self& operator-=(const T& val)
  {
    std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::minus<T>(),val));
    return *this;
  }

  self& operator*=(const T& val)
  {
    std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::multiplies<T>(),val));
    return *this;
  }
  
  self& operator/=(const T& val)
  {
    std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::divides<T>(),val));
    return *this;
  }
  //   self& operator%=(const T& val);
  //   self& operator^=(const T& val);
  //   self& operator&=(const T& val);
  //   self& operator|=(const T& val);
  //   self& operator<<=(const T& val);
  //   self& operator>>=(const T& val);

  self  operator-() const
  {
    self tmp = *this;
    std::transform(this->begin(),this->end(),tmp.begin(),std::negate<T>());
    return tmp;
  }
  //self  operator!() const;

  self& operator+=(const self& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::plus<T>());
    return *this;
  }

  self& operator-=(const self& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::minus<T>());
    return *this;
  }
  
  self& operator*=(const self& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::multiplies<T>());
    return *this;
  }
  
  self& operator/=(const self& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::divides<T>());
    return *this;
  }    
  
 
  /*@} End Arithmetic operators */
  
  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const {return "kvector";}


  /*!
  ** \brief Returns the number of elements in the %kvector
  */
  size_type size() const {return N;}

  /*!
  ** \brief Returns the maximal size (number of elements) in the %kvector
  */
  size_type max_size() const 
  {
   return size_type(-1)/sizeof(value_type);
  }

  /*!
  ** \brief Returns true if the %kvector is empty.  (Thus size == 0)
  */
  bool empty() const {return N == 0;}

  /*!
  ** \brief Swaps data with another %kvector.
  ** \param x A %kvector of the same element type
  */
  void swap(kvector& x)
  {
     std::swap_ranges(this->begin(),this->end(),x.begin());
  }


  /**
   ** \name  Mathematical operators
   */
  /*@{*/
   /*!
  ** \brief Returns the min element of the kvector according to the operator <,
  ** if T is std::complex, it returns the element of minimum magnitude
  ** \pre size() != 0
  */
  T& min() 
  {
    assert(this->size() != 0);
    return *std::min_element(this->begin(),this->end(),std::less<T>());
  }


  /*!
  ** \brief Returns the max element of the kvector according to the operator >,
  ** if T is std::complex, it returns the element of maximum magnitude
  ** \pre size() != 0
  */
  T& max() 
  {
    assert(this->size() != 0);
    return *std::max_element(this->begin(),this->end(),std::less<T>());
  }

  /*!
  ** \brief Returns the sum of the elements of the kvector 
  ** \pre size() != 0
  */
  T sum() const
  {
    assert(this->size() != 0);
    return std::accumulate(this->begin(),this->end(),T(0));
  }

  /*!
  ** \brief Returns the Euclidean norm \f$\left(\sum_i x_i\bar{x_i} \right)^{\frac{1}{2}}\f$ of the elements of the kvector 
  ** \pre size() != 0
  */
  norm_type Euclidean_norm() const
  {
    assert(this->size() != 0);
    return std::sqrt(slip::L22_norm_cplx(this->begin(),this->end()));
  }

  /*!
  ** \brief Returns the Euclidean norm \f$\left(\sum_i x_i\bar{x_i} \right)^{\frac{1}{2}}\f$ of the elements of the kvector 
  ** \pre size() != 0
  */
  norm_type L2_norm() const
  {
    assert(this->size() != 0);
    return this->Euclidean_norm();
  }


  /*!
  ** \brief Returns the L1 norm (\f$\sum_i |xi|\f$) of the elements of the kvector 
  ** \pre size() != 0
  */
  norm_type L1_norm() const
  {
    assert(this->size() != 0);
    return slip::L1_norm<norm_type>(this->begin(),this->end());
  }

  /*!
  ** \brief Returns the L22 norm (\f$\sum_i xi^2\f$) of the elements of the kvector 
  ** \pre size() != 0
  */
  norm_type L22_norm() const
  {
    assert(this->size() != 0);
    return slip::L22_norm_cplx(this->begin(),this->end());
  }

  /*!
  ** \brief Returns the infinite norm (\f$\max_i\{|xi|\}\f$) of the elements of the kvector 
  ** \pre size() != 0
  */
  norm_type infinite_norm() const
  {
    assert(this->size() != 0);
    return slip::infinite_norm<norm_type>(this->begin(),this->end());
  }

   /*!
  ** \brief Computes the dot product with another %kvector \a other
  ** \param other. Other %Kvector.
  ** \return The Real result of the dot product.
  */
  template<typename Real>
  Real dot(const self& other)
  {
    return std::inner_product(this->begin(),this->end(),other.begin(),
			      Real());
  }
   
  /*!
  ** \brief Applys the one-parameter C-function \a fun 
  **        to each element of the %kvector
  ** \param fun The one-parameter C function 
  ** \return the resulting %kvector
  ** \par Example:
  ** \code
  ** slip::kvector<double,7> V;
  ** //fill V with values from 1 to 7 with step 1
  ** slip::iota(V.begin(),V.end(),1.0,1.0);
  ** //apply std::sqrt to each element of V
  ** V.apply(std::sqrt);
  **
  ** \endcode
  */
  kvector<T,N>& apply(T (*fun)(T))
  {
    slip::apply(this->begin(),this->end(),this->begin(),fun);
    return *this;
  }

  /*!
  ** \brief Applys the one-parameter C-function \a fun 
  **        to each element of the %kvector
  ** \param fun The one-const-parameter C function 
  ** \return the resulting %kvector
  ** \par Example:
  ** \code
  ** slip::kvector<double,7> V;
  ** //fill V with values from 1 to 7 with step 1
  ** slip::iota(V.begin(),V.end(),1.0,1.0);
  ** //apply std::sqrt to each element of V
  ** V.apply(std::sqrt);
  **
  ** \endcode
  */
  kvector<T,N>& apply(T (*fun)(const T&))
  {
    slip::apply(this->begin(),this->end(),this->begin(),fun);
    return *this;
  }
/*@} End Mathematical operators */
  
  /// Data array storage of the kvector
  T data[N];

  protected:
  friend class boost::serialization::access;
  template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    { 
      if(version >= 0)
	{
	  ar & data;
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    { 
      if(version >= 0)
	{
	  ar & data;
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
};


  /*!
  ** \brief pointwise addition of two %kvector
  ** \param V1 The first %kvector 
  ** \param V2 The second %kvector
  ** \pre V1.size() == V2.size()
  ** \return resulting %kvector
  */
template<typename T, std::size_t N>
kvector<T,N> operator+(const kvector<T,N>& V1, 
		       const kvector<T,N>& V2);

 /*!
  ** \brief addition of a scalar to each element of a %kvector
  ** \param V The %kvector 
  ** \param val The scalar
  ** \return resulting %kvector
  */
template<typename T, std::size_t N>
kvector<T,N> operator+(const kvector<T,N>& V, 
		    const T& val);

 /*!
  ** \brief addition of a scalar to each element of a %kvector
  ** \param val The scalar
  ** \param V The %kvector  
  ** \return resulting %kvector
  */
template<typename T, std::size_t N>
kvector<T,N> operator+(const T& val, 
		    const kvector<T,N>& V);


/*!
  ** \brief pointwise substraction of two %kvector
  ** \param V1 The first %kvector 
  ** \param V2 The second %kvector
  ** \pre V1.size() == V2.size()
  ** \return resulting %kvector
  */
template<typename T, std::size_t N>
kvector<T,N> operator-(const kvector<T,N>& V1, 
		    const kvector<T,N>& V2);

 /*!
  ** \brief substraction of a scalar to each element of a %kvector
  ** \param V The %kvector 
  ** \param val The scalar
  ** \return resulting %kvector
  */
template<typename T, std::size_t N>
kvector<T,N> operator-(const kvector<T,N>& V, 
		    const T& val);

 /*!
  ** \brief substraction of a scalar to each element of a %kvector
  ** \param val The scalar
  ** \param V The %kvector  
  ** \return resulting %kvector
  */
template<typename T, std::size_t N>
kvector<T,N> operator-(const T& val, 
		    const kvector<T,N>& V);

/*!
  ** \brief pointwise multiplication of two %kvector
  ** \param V1 The first %kvector 
  ** \param V2 The second %kvector
  ** \pre V1.size() == V2.size()
  ** \return resulting %kvector
  */
template<typename T, std::size_t N>
kvector<T,N> operator*(const kvector<T,N>& V1, 
		    const kvector<T,N>& V2);

 /*!
  ** \brief multiplication of a scalar to each element of a %kvector
  ** \param V The %kvector 
  ** \param val The scalar
  ** \return resulting %kvector
  */
template<typename T, std::size_t N>
kvector<T,N> operator*(const kvector<T,N>& V, 
		    const T& val);

 /*!
  ** \brief multiplication of a scalar to each element of a %kvector
  ** \param val The scalar
  ** \param V The %kvector  
  ** \return resulting %kvector
  */
template<typename T, std::size_t N>
kvector<T,N> operator*(const T& val, 
		    const kvector<T,N>& V);

/*!
  ** \brief pointwise division of two %kvector
  ** \param V1 The first %kvector 
  ** \param V2 The second %kvector
  ** \pre V1.size() == V2.size()
  ** \return resulting %kvector
  */
  template<typename T, std::size_t N>
  kvector<T,N> operator/(const kvector<T,N>& V1, 
		      const kvector<T,N>& V2);

 /*!
  ** \brief division of a scalar to each element of a %kvector
  ** \param V The %kvector 
  ** \param val The scalar
  ** \return resulting %kvector
  */
  template<typename T, std::size_t N>
  kvector<T,N> operator/(const kvector<T,N>& V, 
		      const T& val);

//  /*!
//   ** \brief division of a scalar to each element of a %kvector
//   ** \param val The scalar
//   ** \param V The %kvector  
//   ** \return resulting %kvector
//   */
//   template<typename T, std::size_t N>
//   kvector<T,N> operator/(const T& val, 
// 		      const kvector<T,N>& V);

  //de m�me pour atan2, pow, 
  //^, &, |, <<, >>, &&, ||

 /*!
  ** \relates kvector
  ** \brief Returns the min element of a kvector, if T is std::complex, 
  ** it returns the element of minimum magnitude
  ** \param M1 the kvector  
  ** \return the min element
  */
  template<typename T, std::size_t N>
  T& min(kvector<T,N>& M1);
  
  /*!
  ** \relates kvector
  ** \brief Returns the max element of a kvector, if T is std::complex, 
  ** it returns the element of maximum magnitude
  ** \param M1 the kvector  
  ** \return the max element
  */
  template<typename T, std::size_t N>
  T& max(kvector<T,N>& M1);

  
  /*!
  ** \relates kvector
  ** \brief Returns the abs value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<typename T, std::size_t N>
  kvector<typename slip::lin_alg_traits<T>::value_type,N> abs(const kvector<T,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the sqrt value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<typename T, std::size_t N>
  kvector<T,N> sqrt(const kvector<T,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the cos value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<typename T, std::size_t N>
  kvector<T,N> cos(const kvector<T,N>& V);

  
  /*!
  ** \relates kvector
  ** \brief Returns the acos value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<std::size_t N>
  kvector<float,N> acos(const kvector<float,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the acos value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<std::size_t N>
  kvector<double,N> acos(const kvector<double,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the acos value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<std::size_t N>
  kvector<long double,N> acos(const kvector<long double,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the sin value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<typename T, std::size_t N>
  kvector<T,N> sin(const kvector<T,N>& V);

 
  /*!
  ** \relates kvector
  ** \brief Returns the asin value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<std::size_t N>
  kvector<float,N> asin(const kvector<float,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the asin value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<std::size_t N>
  kvector<double,N> asin(const kvector<double,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the asin value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<std::size_t N>
  kvector<long double,N> asin(const kvector<long double,N>& V);


  /*!
  ** \relates kvector
  ** \brief Returns the tan value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<typename T, std::size_t N>
  kvector<T,N> tan(const kvector<T,N>& V);

  
  /*!
  ** \relates kvector
  ** \brief Returns the atan value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<std::size_t N>
  kvector<float,N> atan(const kvector<float,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the atan value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<std::size_t N>
  kvector<double,N> atan(const kvector<double,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the atan value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<std::size_t N>
  kvector<long double,N> atan(const kvector<long double,N>& V);


  /*!
  ** \relates kvector
  ** \brief Returns the exp value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<typename T, std::size_t N>
  kvector<T,N> exp(const kvector<T,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the log value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<typename T, std::size_t N>
  kvector<T,N> log(const kvector<T,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the cosh value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<typename T, std::size_t N>
  kvector<T,N> cosh(const kvector<T,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the sinh value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<typename T, std::size_t N>
  kvector<T,N> sinh(const kvector<T,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the tanh value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<typename T, std::size_t N>
  kvector<T,N> tanh(const kvector<T,N>& V);

  /*!
  ** \relates kvector
  ** \brief Returns the log10 value of each element of the %kvector
  ** \param V The %kvector  
  ** \return the resulting %kvector
  */
  template<typename T, std::size_t N>
  kvector<T,N> log10(const kvector<T,N>& V);

}//slip::

namespace slip
{

template <typename T, std::size_t N>
std::ostream& operator<<(std::ostream & out, 
				const kvector<T,N>& b)
  {
    out<<"(";
    for(std::size_t n = 0; n < N - 1; ++n)
      {
	out<<b.data[n]<<",";
      }
    out<<b.data[N-1];
    out<<")";
    return out;
  }

template<typename T, std::size_t N>
inline
kvector<T,N> operator+(const kvector<T,N>& V1, 
		       const kvector<T,N>& V2)
{
  kvector<T,N> tmp;
  std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::plus<T>());
  return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> operator+(const kvector<T,N>& V1, 
		       const T& val)
{
  kvector<T,N> tmp = V1;
  tmp+=val;
  return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> operator+(const T& val,
		       const kvector<T,N>& V1)
{
  return V1 + val;
}

template<typename T, std::size_t N>
inline
kvector<T,N> operator-(const kvector<T,N>& V1, 
		       const kvector<T,N>& V2)
{
    kvector<T,N> tmp;
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::minus<T>());
    return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> operator-(const kvector<T,N>& V1, 
		    const T& val)
{
    kvector<T,N> tmp = V1;
    tmp-=val;
    return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> operator-(const T& val,
		       const kvector<T,N>& V1)
{
  return -(V1 - val);
}

template<typename T, std::size_t N>
inline
kvector<T,N> operator*(const kvector<T,N>& V1, 
		       const kvector<T,N>& V2)
{
    kvector<T,N> tmp;
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::multiplies<T>());
    return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> operator*(const kvector<T,N>& V1, 
		       const T& val)
{
    kvector<T,N> tmp = V1;
    tmp*=val;
    return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> operator*(const T& val,
		       const kvector<T,N>& V1)
{
  return V1 * val;
}

template<typename T, std::size_t N>
inline
kvector<T,N> operator/(const kvector<T,N>& V1, 
		       const kvector<T,N>& V2)
{
    kvector<T,N> tmp;
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::divides<T>());
    return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> operator/(const kvector<T,N>& V1, 
		    const T& val)
{
    kvector<T,N> tmp = V1;
    tmp/=val;
    return tmp;
}

// template<typename T, std::size_t N>
// inline
// kvector<T,N> operator/(const T& val,
// 		    const kvector<T,N>& V1)
// {
//   return V1 / val;
// }


template<typename T, std::size_t N>
inline
T& min(kvector<T,N>& V1)
{
  return V1.min();
}

template<typename T, std::size_t N>
inline
T& max(kvector<T,N>& V1)
{
  return V1.max();
}

template<typename T, std::size_t N>
inline
kvector<typename slip::lin_alg_traits<T>::value_type,N> abs(const kvector<T,N>& V)
{
  
  kvector<typename slip::lin_alg_traits<T>::value_type,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::abs);
  return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> sqrt(const kvector<T,N>& V)
{
  kvector<T,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::sqrt);
  return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> cos(const kvector<T,N>& V)
{
  kvector<T,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::cos);
  return tmp;
}


template<std::size_t N>
inline
kvector<float,N> acos(const kvector<float,N>& V)
{
  kvector<float,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::acos);
  return tmp;
}

template<std::size_t N>
inline
kvector<double,N> acos(const kvector<double,N>& V)
{
  kvector<double,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::acos);
  return tmp;
}

template<std::size_t N>
inline
kvector<long double,N> acos(const kvector<long double,N>& V)
{
  kvector<long double,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::acos);
  return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> sin(const kvector<T,N>& V)
{
  kvector<T,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::sin);
  return tmp;
}


template<std::size_t N>
inline
kvector<float,N> asin(const kvector<float,N>& V)
{
  kvector<float,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::asin);
  return tmp;
}

template<std::size_t N>
inline
kvector<double,N> asin(const kvector<double,N>& V)
{
  kvector<double,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::asin);
  return tmp;
}

template<std::size_t N>
inline
kvector<long double,N> asin(const kvector<long double,N>& V)
{
  kvector<long double,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::asin);
  return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> tan(const kvector<T,N>& V)
{
  kvector<T,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::tan);
  return tmp;
}



template<std::size_t N>
inline
kvector<float,N> atan(const kvector<float,N>& V)
{
  kvector<float,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::atan);
  return tmp;
}

template<std::size_t N>
inline
kvector<double,N> atan(const kvector<double,N>& V)
{
  kvector<double,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::atan);
  return tmp;
}

template<std::size_t N>
inline
kvector<long double,N> atan(const kvector<long double,N>& V)
{
  kvector<long double,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::atan);
  return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> exp(const kvector<T,N>& V)
{
  kvector<T,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::exp);
  return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> log(const kvector<T,N>& V)
{
  kvector<T,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::log);
  return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> cosh(const kvector<T,N>& V)
{
  kvector<T,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::cosh);
  return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> sinh(const kvector<T,N>& V)
{
  kvector<T,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::sinh);
  return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> tanh(const kvector<T,N>& V)
{
  kvector<T,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::tanh);
  return tmp;
}

template<typename T, std::size_t N>
inline
kvector<T,N> log10(const kvector<T,N>& V)
{
  kvector<T,N> tmp;
  slip::apply(V.begin(),V.end(),tmp.begin(),std::log10);
  return tmp;
}


template <typename T, std::size_t N>
inline
bool operator==(const kvector<T,N>& x, 
		const kvector<T,N>& y)
{
  return ( x.size() == y.size()
	   && std::equal(x.begin(),x.end(),y.begin())); 
}

template <typename T, std::size_t N>
inline
bool operator!=(const kvector<T,N>& x, 
		const kvector<T,N>& y)
{
  return !(x == y);
}


template <typename T, std::size_t N>
inline
bool operator<(const kvector<T,N>& x, 
	       const kvector<T,N>& y)
{
  return std::lexicographical_compare(x.begin(), x.end(),
				      y.begin(), y.end());
}

template <typename T, std::size_t N>
inline
bool operator>(const kvector<T,N>& x, 
	       const kvector<T,N>& y)
{
  return (y < x);
}


template <typename T, std::size_t N>
inline
bool operator<=(const kvector<T,N>& x, 
		const kvector<T,N>& y)
{
  return !(y < x);
}


template <typename T, std::size_t N>
inline
bool operator>=(const kvector<T,N>& x, 
		const kvector<T,N>& y)
{
  return !(x < y);
}
}//slip::

#endif //KVECTOR_HPP
