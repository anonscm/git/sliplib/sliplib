/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Array.hpp
 * 
 * \brief Provides a class to manipulate 1d dynamic and generic arrays.
 * 
 */
#ifndef SLIP_ARRAY_HPP
#define SLIP_ARRAY_HPP
#include <iostream>
#include <iterator>
#include <cassert>
#include <cstddef>
#include "Range.hpp"
#include "Box1d.hpp"
#include "stride_iterator.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>
namespace slip
{

template <typename T>
class Array;

template <typename T>
std::ostream& operator<<(std::ostream & out, 
			 const Array<T>& a);

template<typename T>
bool operator==(const Array<T>& x, 
		const Array<T>& y);

template<typename T>
bool operator!=(const Array<T>& x, 
		const Array<T>& y);

template<typename T>
bool operator<(const Array<T>& x, 
	       const Array<T>& y);

template<typename T>
bool operator>(const Array<T>& x, 
	       const Array<T>& y);

template<typename T>
bool operator<=(const Array<T>& x, 
		const Array<T>& y);

template<typename T>
bool operator>=(const Array<T>& x, 
		const Array<T>& y);

/*!
 *  @defgroup Containers1d 1d Containers
 *  @brief 1d containers having the same interface
 *  @{
 */
/*! \class Array
**  \ingroup Containers Containers1d
** \brief This is a linear (one-dimensional) dynamic template container.
** This container statisfies the RandomAccessContainer concepts of the 
** Standard Template Library (STL).
** 
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.4
** \since 1.0.0
** \date 2014/03/13
** \param T Type of the element in the %Array
**
*/
template <typename T>
class Array
{
public:
  typedef T value_type;
  typedef Array<T> self;
  typedef const Array<T> const_self;

  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;

  typedef slip::stride_iterator<pointer> iterator_range;
  typedef slip::stride_iterator<const_pointer> const_iterator_range;

  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

  typedef std::reverse_iterator<iterator_range> reverse_iterator_range;
  typedef std::reverse_iterator<const_iterator_range> const_reverse_iterator_range;

  //default iterator of the container
  typedef iterator default_iterator;
  typedef const_iterator const_default_iterator;

  static const std::size_t DIM = 1;

public:
  /**
   ** \name Constructors & Destructors
   */
  /*@{*/

  /*!
  ** \brief Constructs a %Array.
  */
  Array();
 
  /*!
  ** \brief Constructs a %Array.
  ** \param size dimension of the %Array
  **
  ** \par The %Array is initialized with the default value of T.
  */
  Array(const size_type size);
  
  /*!
  ** \brief Constructs a %Array initialized by the scalar value \a val.
  ** \param size dimension of the %Array
  ** \param val initialization value of the elements 
  */
  Array(const size_type size,
	const T& val);
 
  /*!
  ** \brief Constructs a %Array initialized by an array \a val.
  ** \param size dimension of the %Array
  ** \param val initialization array value of the elements 
  */
  Array(const size_type size,
	const T* val);
 
 /**
  **  \brief  Constructs a %Array from a range.
  **  \param  n Size of the %Array.
  **  \param  first  An input iterator.
  **  \param  last  An input iterator.
  **  \date 2014/03/13
  ** Create a %Array consisting of copies of the elements from
  ** [first,last).
  */
  template<typename InputIterator>
  Array(const size_type n,
	InputIterator first,
	InputIterator last):
    size_(n)
  {
    this->allocate();
    std::fill_n(this->begin(),n,T());
    std::copy(first,last,this->begin());
  }
  
 /*!
  ** \brief Constructs a copy of the %Array \a rhs
  */
  Array(const self& rhs);

 /*!
  ** \brief Destructor of the %Array
  */
  ~Array();
  

  /*@} End Constructors */


  /*!
  ** \brief Resizes a %Array.
  ** \param size new dimension
  ** \param val new value for all the elements
  */ 
  void resize(const size_type size,
	      const T& val = T());

  /**
   ** \name iterators
   */
  /*@{*/
 
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %Array.  Iteration is done in ordinary
  **  element order.
  ** \return begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator begin();

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %Array.  Iteration is done in ordinary
  **  element order.
  ** \return const begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator begin() const;

  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %Array.  Iteration is done in ordinary
  **  element order.
  ** \return end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  iterator end();
 
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %Array.  Iteration is done in
  **  ordinary element order.
  ** \return const end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_iterator end() const;
  
  /*!
  **  \brief Returns a read/write reverse iterator that points to the
  **  last element in the %Array.  Iteration is done in reverse
  **  element order.
  ** \return reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rbegin();
  
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to the last element in the %Array.  Iteration is done in
  **  reverse element order.
  ** \return const reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_reverse_iterator rbegin() const;

  /*!
  **  \brief Returns a read/write reverse iterator that points to one
  **  before the first element in the %Array.  Iteration is done
  **  in reverse element order.
  **  \return reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  reverse_iterator rend();

  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to one before the first element within the %Array.  Iteration
  **  is done in reverse element order.
  **  \return const reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  */
  const_reverse_iterator rend() const;

  /*!
  **  \brief Returns a read/write iterator_range that points 
  **  the first element within the %Range.  
  **  Iteration is done in ordinary element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return end iterator_range value
  ** \pre The range must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Array<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator_range begin(const slip::Range<int>& range);
 
  /*!
  ** \brief Returns a read-only (constant) iterator_range 
  ** that points the first element within the %Range.  
  ** Iteration is done in ordinary element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return const_iterator_range value
  ** \pre The range must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Array<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout,"
  "));
   ** \endcode
  */
  const_iterator_range begin(const slip::Range<int>& range) const;

  /*!
  **  \brief Returns a read/write iterator_range that points 
  **  one past the last element in the %Array.  
  **  Iteration is done in ordinary element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return end iterator_range value
  ** \pre The range must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Array<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator_range end(const slip::Range<int>& range);
 
  /*!
  **  \brief Returns a read-only (constant) iterator_range 
  ** that points one past the last element in the %Array.  
  ** Iteration is done in ordinary element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return const_iterator_range value
  ** \pre The range must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Array<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.begin(range),A5.end(range),std::ostream_iterator<double>(std::cout,"
  "));
  ** \endcode
  */
  const_iterator_range end(const slip::Range<int>& range) const;



  /*!
  **  \brief Returns a read/write iterator that points 
  **  the first element within the %Box1d.  
  **  Iteration is done in ordinary element order.
  ** \param box %Box1d to iterate.
  ** \return end iterator value
  ** \pre The box must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Array<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.begin(box),A5.end(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator begin(const slip::Box1d<int>& box);
 
  /*!
  ** \brief Returns a read-only (constant) iterator 
  ** that points the first element within the %Box1d.  
  ** Iteration is done in ordinary element order.
  ** \param box %Box1d to iterate.
  ** \return const_iterator value
  ** \pre The box must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Array<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.begin(box),A5.end(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  const_iterator begin(const slip::Box1d<int>& box) const;

  /*!
  **  \brief Returns a read/write iterator that points 
  **  one past the last element in the %Array.  
  **  Iteration is done in ordinary element order.
  ** \param box %Box1d to iterate.
  ** \return end iterator value
  ** \pre The box must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Array<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.begin(box),A5.end(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  iterator end(const slip::Box1d<int>& box);
 
  /*!
  **  \brief Returns a read-only (constant) iterator 
  ** that points one past the last element within the %Box1d.  
  ** Iteration is done in ordinary element order.
  ** \param box %Box1d to iterate.
  ** \return const_iterator value
  ** \pre The box must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Array<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.begin(box),A5.end(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  const_iterator end(const slip::Box1d<int>& box) const;

  //
    /*!
  **  \brief Returns a read/write reverse_iterator_range that points 
  **  the end element within the %Range.  
  **  Iteration is done in reverse element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Array<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator_range rbegin(const slip::Range<int>& range);
 
  /*!
  ** \brief Returns a read-only (constant) reverse_iterator_range 
  ** that points the end element within the %Range.  
  ** Iteration is done in reverse element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return cons_treverse__iterator_range value
  ** \pre The range must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Array<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout,"
  "));
   ** \endcode
  */
  const_reverse_iterator_range rbegin(const slip::Range<int>& range) const;

  /*!
  **  \brief Returns a read/write reverse_iterator_range that points 
  **  one previous the first element in the %Range.  
  **  Iteration is done in reverse element order  according to the
  **  %Range.
  ** \param range %Range to iterate.
  ** \return reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Array<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator_range rend(const slip::Range<int>& range);
 
  /*!
  **  \brief Returns a read-only (constant) reverse_iterator_range 
  ** that points one previous the first element in the %Range.  
  ** Iteration is done in reverse element order according to the
  ** %Range.
  ** \param range %Range to iterate.
  ** \return const_reverse_iterator_range value
  ** \pre The range must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** slip::Array<double> A5(8);
  ** slip::Range<int> range(1,5,2);
  ** std::copy(A5.rbegin(range),A5.rend(range),std::ostream_iterator<double>(std::cout,"
  "));
  ** \endcode
  */
  const_reverse_iterator_range rend(const slip::Range<int>& range) const;



  /*!
  **  \brief Returns a read/write reverse_iterator that points 
  **  the last element within the %Box1d.  
  **  Iteration is done in reverse element order.
  ** \param box %Box1d to iterate.
  ** \return  reverse_iterator value
  ** \pre The box must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Array<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.rbegin(box),A5.rend(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator rbegin(const slip::Box1d<int>& box);
 
  /*!
  ** \brief Returns a read-only (constant) reverse_iterator 
  ** that points the last element within the %Box1d.  
  ** Iteration is done in reverse element order.
  ** \param box %Box1d to iterate.
  ** \return const_reverse_iterator value
  ** \pre The box must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Array<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.rbegin(box),A5.rend(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  const_reverse_iterator rbegin(const slip::Box1d<int>& box) const;

  /*!
  **  \brief Returns a read/write reverse_iterator that points 
  **  one previous the first element in the %Box1d.  
  **  Iteration is done in reverse element order.
  ** \param box %Box1d to iterate.
  ** \return reverse_iterator value
  ** \pre The box must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Array<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.rbegin(box),A5.rend(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  reverse_iterator rend(const slip::Box1d<int>& box);
 
  /*!
  **  \brief Returns a read-only (constant) reverse_iterator 
  ** that points one previous the first element of the %Box1d.  
  ** Iteration is done in reverse element order.
  ** \param box %Box1d to iterate.
  ** \return const_reverse_iterator value
  ** \pre The box must be inside the whole range of the %Array.
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  **
  ** slip::Array<double> A5(8);
  ** slip::Box1d<int> box(2,5);
  ** std::copy(A5.rbegin(box),A5.rend(box),std::ostream_iterator<double>(std::cout," "));
  ** \endcode
  */
  const_reverse_iterator rend(const slip::Box1d<int>& box) const;
  /*@} End iterators */

 
  /**
   ** \name i/o operators
   */
  /*@{*/
  
  /*!
  ** \brief Write the %Array to the ouput stream
  ** \param out output std::ostream
  ** \param a %Array to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const self& a);
  /*@} End i/o operators */
  

  /**
   ** \name  Assignment operators and methods
   */
  /*@{*/

  /*!
  ** \brief Assigns a %Array in \a rhs
  **
  ** \param rhs %Array to get the values from.
  ** \return 
  */
  self& operator=(const self& rhs);
  
  /*!
  ** \brief Assign all the elements of the %Array by value
  **
  **
  ** \param value A reference-to-const of arbitrary type.
  ** \return 
  */
  self& operator=(const T& value)
  {
    this->fill(value);
    return *this;
  }

  /*!
   ** \brief Fills the container range [begin(),begin()+N) with copies of value
   ** \param value  A reference-to-const of arbitrary type.
  */
  void fill(const T& value)
  {
    std::fill_n(this->begin(),size_,value);
  } 
  
 /*!
  ** \brief Fills the container range [begin(),begin()+N) with a copy of
  **        the value array
  ** \param value  A pointer of arbitrary type.
  */
  void fill(const T* value)
  {
    std::copy(value,value + size_,this->begin());
  } 

  /*!
  ** \brief Fills the container range [begin(),begin()+N) with a copy of
  **        the range [first,last)
  **  \param  first  An input iterator.
  **  \param  last   An input iterator.
  **   
  **
  */
  template<typename InputIterator>
  void fill(InputIterator first,
	    InputIterator last)
  {
    std::copy(first,last,this->begin());
  }
  /*@} End Assignment operators and methods*/

  /**
   ** \name Comparison operators
   */
  /*@{*/
  /*!
  ** \brief %Array equality comparison
  ** \param x A %Array
  ** \param y A %Array of the same type of \a x
  ** \return true iff the size and the elements of the Arrays are equal
  */
  friend bool operator== <>(const Array<T>& x, 
			    const Array<T>& y);

 /*!
  ** \brief %Array inequality comparison
  ** \param x A %Array
  ** \param y A %Array of the same type of \a x
  ** \return true if !(x == y) 
  */
  friend bool operator!= <>(const Array<T>& x, 
			    const Array<T>& y);

 /*!
  ** \brief Less than comparison operator (Array ordering relation)
  ** \param x A %Array
  ** \param y A %Array of the same type of \a x
  ** \return true iff \a x is lexicographically less than \a y
  */
  friend bool operator< <>(const Array<T>& x, 
			   const Array<T>& y);

 /*!
  ** \brief More than comparison operator
  ** \param x A %Array
  ** \param y A %Array of the same type of \a x
  ** \return true iff y > x 
  */
  friend bool operator> <>(const Array<T>& x, 
			   const Array<T>& y);

  /*!
  ** \brief Less than equal comparison operator
  ** \param x A %Array
  ** \param y A %Array of the same type of \a x
  ** \return true iff !(y > x) 
  */
  friend bool operator<= <>(const Array<T>& x, 
			    const Array<T>& y);

 /*!
  ** \brief More than equal comparison operator
  ** \param x A %Array
  ** \param y A %Array of the same type of \a x
  ** \return true iff !(x < y) 
  */
  friend bool operator>= <>(const Array<T>& x, 
			    const Array<T>& y);


  /*@} Comparison operators */
  
  /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Subscript access to the data contained in the %Array.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read/write reference to data.
  ** \pre n < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator[](const size_type n);

 
  /*!
  ** \brief Subscript access to the data contained in the %Array.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read-only (constant) reference to data.
  ** \pre n < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator[](const size_type n) const;
  
  /*!
  ** \brief Subscript access to the data contained in the %Array.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read/write reference to data.
  ** \pre n < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const size_type n);
  
  /*!
  ** \brief Subscript access to the data contained in the %Array.
  ** \param n The index of the element for which data should be
  **  accessed.
  ** \return Read-only (constant) reference to data.
  ** \pre n < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const size_type n) const;


   /*!
  ** \brief Subscript access to the data contained in the %Array.
  ** \param p The index point corresponding of the element for which data 
  ** should be accessed.
  ** \return Read/write reference to data.
  ** \pre p[0] < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const slip::Point1d<size_type>& p);
  
  /*!
  ** \brief Subscript access to the data contained in the %Array.
  ** \param p The index point corresponding of the element for which data 
  ** should be accessed.
  ** \return Read-only (constant) reference to data.
  ** \pre p[0] < size()
  **
  ** This operator allows for easy, array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const slip::Point1d<size_type>& p) const;

    /*!
  ** \brief Subscript access to the data contained in the %Array.
  ** \param range The range of the %Array.
  ** \return a copy of the range.
  ** \pre range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Array ones.
  **
  ** This operator allows for easy, 1d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  self operator()(const slip::Range<int>& range);

  /*@} End Element access operators */

  
  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;

  /*!
  ** \brief Returns the number of elements in the %Array
  */
  size_type size() const;
  /*!
  ** \brief Returns the maximal size (number of elements) in the %Array
  */
  size_type max_size() const;

  /*!
  ** \brief Returns true if the %Array is empty.  (Thus size() == 0)
  */
  bool empty()const;

  /*!
  ** \brief Swaps data with another %Array.
  ** \param rhs A %Array of the same element type
  */
  void swap(self& rhs);
 
private:
  size_type size_;
  T* data_;

  /// Allocates the  memory to store the elements of the Array
  void allocate();
  /// Desallocates the memory
  void desallocate();
  /*!
  ** \brief Copy the attributes of the Array \a rhs to the Array
  ** \param rhs the Array from which the attributes are copied
  */
  void copy_attributes(const self& rhs);
    
  friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & this->size_ ;
	  for(std::size_t i = 0; i < this->size_; ++i)
	    {
	      ar & *(data_ + i);
	    }
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & this->size_;
	  this->desallocate();
	  this->allocate();
	  for(std::size_t i = 0; i < this->size_; ++i)
	    {
	      ar & *(data_+ i);
	    }
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()

};
/** @} */ // end of Containers1d group

///double alias
  typedef slip::Array<double> Array_d;
  ///float alias
  typedef slip::Array<float> Array_f;
  ///long alias
  typedef slip::Array<long> Array_l;
  ///unsigned long alias
  typedef slip::Array<unsigned long> Array_ul;
  ///short alias
  typedef slip::Array<short> Array_s;
  ///unsigned long alias
  typedef slip::Array<unsigned short> Array_us;
  ///int alias
  typedef slip::Array<int> Array_i;
  ///unsigned int alias
  typedef slip::Array<unsigned int> Array_ui;
  ///char alias
  typedef slip::Array<char> Array_c;
  ///unsigned char alias
  typedef slip::Array<unsigned char> Array_uc;

}//slip::

namespace slip
{

  template<typename T>
  inline
  Array<T>::Array():
    size_(0),data_(0)
  {}


  template<typename T>
  inline
  Array<T>::Array(const typename Array<T>::size_type n):
    size_(n)
  {
    this->allocate();
    std::fill_n(this->data_,this->size_,T());
  }

  template<typename T>
  inline
  Array<T>::Array(typename Array<T>::size_type n,
		  const T& val):
    size_(n)
  {
    this->allocate();
    std::fill_n(this->data_,this->size_,val);
  }
  
  template<typename T>
  inline
  Array<T>::Array(typename Array<T>::size_type n,
		  const T* val):
    size_(n)
  {
    this->allocate();
    std::copy(val,val + this->size_, this->data_); 
  }

  template<typename T>
  inline
  Array<T>::Array(const Array<T>& rhs):
    size_(rhs.size_)
  {
    this->allocate();
    std::copy(rhs.data_,rhs.data_ + this->size_,this->data_);
  }

  template<typename T>
  inline
  Array<T>::~Array()
  {
    this->desallocate();
  }

  template<typename T>
  inline
  Array<T>& Array<T>::operator=(const self& rhs)
  {
    if(this != &rhs)
      {
	if(this->size_ != rhs.size_)
	  {
	    this->desallocate();
	    this->copy_attributes(rhs);
	    this->allocate();
	  }
	if(rhs.size_ != 0)
	  {
	    std::copy(rhs.data_,rhs.data_ + this->size_,this->data_);
	  }
      }
    return *this;
  } 
  
  
  template<typename T>
  inline
  void Array<T>::resize(typename Array<T>::size_type size,
			const T& val)
  {
    if( this->size_ != size)
      {
	this->desallocate();
	this->size_ = size;
	this->allocate();
      }
    if(size != 0)
      {
      std::fill_n(this->data_,this->size_,val);
      }
    
  }

  template<typename T>
  inline
  typename Array<T>::iterator Array<T>::begin()
  {
    return this->data_;
  }

  template<typename T>
  inline
  typename Array<T>::iterator Array<T>::end()
  {
    return this->data_ + this->size_;
  }
  
  template<typename T>
  inline
  typename Array<T>::const_iterator Array<T>::begin() const
  {
    return this->data_;
  }

  template<typename T>
  inline
  typename Array<T>::const_iterator Array<T>::end() const
  {
    return this->data_ + this->size_;
  }
  

  //
  template<typename T>
  inline
  typename Array<T>::iterator_range 
  Array<T>::begin(const slip::Range<int>& range)
  {
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < int(this->size()));
    assert(range.stop()  < int(this->size()));
    return typename slip::Array<T>::iterator_range(this->data_+range.start(),range.stride());
  }

  template<typename T>
  inline
  typename Array<T>::iterator_range 
  Array<T>::end(const slip::Range<int>& range)
  {
    
    return this->begin(range) + (range.iterations() + 1);
  }
  
  template<typename T>
  inline
  typename Array<T>::const_iterator_range 
  Array<T>::begin(const slip::Range<int>& range) const
  {
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < int(this->size()));
    assert(range.stop()  < int(this->size()));
    return typename slip::Array<T>::const_iterator_range(this->data_+range.start(),range.stride());
  }

  template<typename T>
  inline
  typename Array<T>::const_iterator_range 
  Array<T>::end(const slip::Range<int>& range) const
  {
    return this->begin(range) + (range.iterations() + 1);
  }


  //
  template<typename T>
  inline
  typename Array<T>::iterator
  Array<T>::begin(const slip::Box1d<int>& box)
  {
    assert(box.is_consistent());
    return this->data_ + (box.upper_left())[0];
  }

  template<typename T>
  inline
  typename Array<T>::iterator 
  Array<T>::end(const slip::Box1d<int>& box)
  {
    assert(box.is_consistent());
    return this->begin(box) + box.length();
  }
  
  template<typename T>
  inline
  typename Array<T>::const_iterator 
  Array<T>::begin(const slip::Box1d<int>& box) const
  {
    assert(box.is_consistent());
    return this->data_ + (box.upper_left())[0];
  }

  template<typename T>
  inline
  typename Array<T>::const_iterator 
  Array<T>::end(const slip::Box1d<int>& box) const
  {
    assert(box.is_consistent());
    return this->begin(box) + box.length();
  }
  //
  template<typename T>
  inline
  typename Array<T>::reverse_iterator Array<T>::rbegin()
  {
    return typename Array<T>::reverse_iterator(this->end());
  }

  template<typename T>
  inline
  typename Array<T>::reverse_iterator Array<T>::rend()
  {
    return  typename Array<T>::reverse_iterator(this->begin());
  }

  template<typename T>
  inline
  typename Array<T>::const_reverse_iterator Array<T>::rbegin() const
  {
    return typename Array<T>::const_reverse_iterator(this->end());
  }

  template<typename T>
  inline
  typename Array<T>::const_reverse_iterator Array<T>::rend() const
  {
    return typename Array<T>::const_reverse_iterator(this->begin());
  }

  //
  template<typename T>
  inline
  typename Array<T>::reverse_iterator Array<T>::rbegin(const slip::Box1d<int>& box)
  {
    return typename Array<T>::reverse_iterator(this->end(box));
  }

  template<typename T>
  inline
  typename Array<T>::reverse_iterator 
  Array<T>::rend(const slip::Box1d<int>& box)
  {
    return  typename Array<T>::reverse_iterator(this->begin(box));
  }

  template<typename T>
  inline
  typename Array<T>::const_reverse_iterator 
  Array<T>::rbegin(const slip::Box1d<int>& box) const
  {
    return typename Array<T>::const_reverse_iterator(this->end(box));
  }

  template<typename T>
  inline
  typename Array<T>::const_reverse_iterator 
  Array<T>::rend(const slip::Box1d<int>& box) const
  {
    return typename Array<T>::const_reverse_iterator(this->begin(box));
  }
  //
  template<typename T>
  inline
  typename Array<T>::reverse_iterator_range Array<T>::rbegin(const slip::Range<int>& range)
  {
    return typename Array<T>::reverse_iterator_range(this->end(range));
  }

  template<typename T>
  inline
  typename Array<T>::reverse_iterator_range 
  Array<T>::rend(const slip::Range<int>& range)
  {
    return  typename Array<T>::reverse_iterator_range(this->begin(range));
  }

  template<typename T>
  inline
  typename Array<T>::const_reverse_iterator_range 
  Array<T>::rbegin(const slip::Range<int>& range) const
  {
    return typename Array<T>::const_reverse_iterator_range(this->end(range));
  }

  template<typename T>
  inline
  typename Array<T>::const_reverse_iterator_range 
  Array<T>::rend(const slip::Range<int>& range) const
  {
    return typename Array<T>::const_reverse_iterator_range(this->begin(range));
  }


 /** \name input/output operators */
  /* @{ */
  template <typename T>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const Array<T>& a)
  {
    if(a.data_ != 0)
      {
	out<<"(";
	for(std::size_t n = 0; n < a.size_ - 1; ++n)
	  {
	    out<<a.data_[n]<<",";
	  }
	out<<a.data_[a.size_-1];
	out<<")";
      }
    else
      {
	out<<"()";
      }
    return out;
  }
   /* @} */

  template <typename T>
  inline
  typename Array<T>::reference Array<T>::operator[](typename Array<T>::size_type i)
  {
    assert(this->data_ != 0);
    assert(i < this->size_);
    return *(this->data_ + i);
  }

  template <typename T>
  inline
  typename Array<T>::const_reference Array<T>::operator[](typename Array<T>::size_type i) const
  {
    assert(this->data_ != 0);
    assert(i < this->size_);
    return *(this->data_ + i);
  }

  template <typename T>
  inline
  typename Array<T>::reference Array<T>::operator()(typename Array<T>::size_type i)
  {
    assert(this->data_ != 0);
    assert(i < this->size_);
    return *(this->data_ + i);
  }

  template <typename T>
  inline
  typename Array<T>::const_reference Array<T>::operator()(typename Array<T>::size_type i) const
  {
    assert(this->data_ != 0);
    assert(i < this->size_);
    return *(this->data_ + i);
  }
 
  template <typename T>
  inline
  typename Array<T>::reference 
  Array<T>::operator()(const slip::Point1d<typename Array<T>::size_type>& p)
  {
    assert(this->data_ != 0);
    assert(p[0] < this->size_);
    return *(this->data_ + p[0]);
  }

  template <typename T>
  inline
  typename Array<T>::const_reference 
  Array<T>::operator()(const slip::Point1d<typename Array<T>::size_type>& p) const
  {
    assert(this->data_ != 0);
    assert(p[0] < this->size_);
    return *(this->data_ + p[0]);
  }

  template<typename T>
  inline
  Array<T> Array<T>::operator()(const slip::Range<int>& range)
  {
     assert(this->data_ != 0);
     assert(range.is_valid());
     assert(range.start() < (int)this->size_);
     assert(range.stop()  < (int)this->size_);

     std::size_t dim = range.iterations() + 1;

     return Array<T>(dim,this->begin(range),this->end(range));
  }

  template<typename T>
  inline
  std::string 
  Array<T>::name() const {return "Array";} 


  template <typename T>
  inline
  typename Array<T>::size_type Array<T>::size() const {return this->size_;}
  
  template <typename T>
  inline
  typename Array<T>::size_type Array<T>::max_size() const 
  {
    return typename Array<T>::size_type(-1)/sizeof(T);
  }
  
  template<typename T>
  inline
  bool Array<T>::empty()const {return this->size_ == 0;}

  template<typename T>
  inline
  void Array<T>::swap(Array<T>& M)
  {
    assert(this->size_ == M.size_);
    std::swap_ranges(this->begin(),this->end(),M.begin());
  }

 
  

template<typename T>
  inline
  void Array<T>::allocate()
  {
    if(this->size_ != 0)
      {
	this->data_ = new T[this->size_];
      }
    else
      {
	this->data_ = 0;
      }
  }

  template<typename T>
  inline
  void Array<T>::desallocate()
  {
    if (this->data_ != 0)
      delete[] (this->data_);
  }

  template<typename T>
  inline
  void Array<T>::copy_attributes(const Array<T>& rhs)
  {
    this->size_ = rhs.size_;
  }

/** \name EqualityComparable functions */
  /* @{ */

template<typename T>
inline
bool operator==(const Array<T>& x, 
		const Array<T>& y)
{
  return ( x.size() == y.size()
	   && std::equal(x.begin(),x.end(),y.begin())); 
}

template<typename T>
inline
bool operator!=(const Array<T>& x, 
		const Array<T>& y)
{
  return !(x == y);
}

/* @} */

/** \name LessThanComparable functions */
  /* @{ */
template<typename T>
inline
bool operator<(const Array<T>& x, 
	       const Array<T>& y)
{
  return std::lexicographical_compare(x.begin(), x.end(),
				      y.begin(), y.end());
}

template<typename T>
inline
bool operator>(const Array<T>& x, 
	       const Array<T>& y)
{
  return (y < x);
}

template<typename T>
inline
bool operator<=(const Array<T>& x, 
		const Array<T>& y)
{
  return !(y < x);
}

template<typename T>
inline
bool operator>=(const Array<T>& x, 
		const Array<T>& y)
{
  return !(x < y);
} 

/* @} */

}//slip::
#endif //SLIP_ARRAY_HPP
