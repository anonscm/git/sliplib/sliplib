/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file component_iterator2d_range.hpp
 * 
 * \brief Provides a class to iterate a 2d multicomponent container into two %Ranges.
 * 
 */
#ifndef SLIP_COMPONENT_ITERATOR2D_RANGE_HPP
#define SLIP_COMPONENT_ITERATOR2D_RANGE_HPP

#include <iterator>
#include <cassert>

#include "Point2d.hpp"
#include "DPoint2d.hpp"
#include "Range.hpp"
#include "iterator_types.hpp"

namespace slip
{
/*! \class component_iterator2d_range
** 
** \brief This is some iterator to iterate a 2d container into two %Range 
**        defined by the indices and strides of the 2d container.
** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
** \version 0.0.1
** \date 2008/02/27
**
** \param MultiComponentContainer2D Type of 2d container in the component_iterator2d_range 
** \param N number of elements in one %block
**
** \par This iterator is an 2d extension of the random_access_iterator.
**      It is compatible with the bidirectional_iterator of the
**      Standard library. It iterate into a range area defined inside the
**      indices range of the 2d container. Those indices are defined as 
**      follows :
** \image html iterator2d_conventions.jpg "axis and notation conventions"
** \image latex iterator2d_conventions.eps "axis and notation conventions" width=5cm
*/
template <class MultiComponentContainer2D, std::size_t N>
class component_iterator2d_range  
{
public:
  //typedef std::random_access_iterator_tag iterator_category;
  typedef std::random_access_iterator2d_tag iterator_category;
  typedef typename MultiComponentContainer2D::value_type Block;
  typedef typename Block::value_type value_type;
  typedef DPoint2d<int> difference_type;
  typedef value_type* pointer;
  typedef value_type& reference;
  
  typedef component_iterator2d_range self;

  typedef typename MultiComponentContainer2D::size_type size_type;
  /**
   ** \name Constructors & Destructors
   */
  /*@{*/
  
  /*!
  ** \brief Constructs a %component_iterator2d_range.
  ** \par The range to iterate is in this case the full container
  */
  component_iterator2d_range():
    cont2d_(0),pos_(0),x1_(0),x2_(0),cp_(0),range_x1_(0,0),range_x2_(0,0)
  {}

  /*!
  ** \brief Constructs a %component_iterator2d_range.
  ** \param c pointer to an existing 2d container
  ** \param cp index of the component to iterate 
  ** \param r1 Range<int> defining the range of indices of the first
  **           axis to iterate
  ** \param r2 Range<int> defining the range of indices of the second
  **           axis to iterate
  ** 
  ** \pre range indices must be inside those of the 2d container
  */
  component_iterator2d_range(MultiComponentContainer2D* c, std::size_t cp,
			     const slip::Range<int>& r1,
			     const slip::Range<int>& r2):
    cont2d_(c),pos_(&(*c)[0][0][cp] + (r1.start() *  int(c->cols()) * N) + (r2.start() * N)),x1_(r1.start()),x2_(r2.start()),cp_(cp),range_x1_(r1),range_x2_(r2)
  {}
  
  /*!
  ** \brief Constructs a copy of the %component_iterator2d_range \a o
  ** 
  ** \param o %component_iterator2d_range to copy.
  */
  component_iterator2d_range(const self& o):
    cont2d_(o.cont2d_),pos_(o.pos_),x1_(o.x1_),x2_(o.x2_),cp_(o.cp_),range_x1_(o.range_x1_), range_x2_(o.range_x2_)
  {}
  
  /*@} End Constructors */
  
  /**
   ** \name  Assignment operators and methods
   */
  /*@{*/
  /*!
  ** \brief Assign a %component_iterator2d_range.
  **
  ** Assign elements of %component_iterator2d_range in \a o.
  **
  ** \param o %component_iterator2d_range to get the values from.
  ** \return a %component_iterator2d_range reference.
  */
  self& operator=(const self& o)
  {
    if(this != &o)
      {
	this->cont2d_ = o.cont2d_;
	this->pos_ = o.pos_;
	this->x1_ = o.x1_;
	this->x2_ = o.x2_;
	this->cp_ = o.cp_;
	this->range_x1_ = o.range_x1_;
	this->range_x2_ = o.range_x2_;
      }
    return *this;
  }
  /*@} End Assignment operators and methods*/

  /*!
  ** \brief Dereference assignment operator. Returns the element 
  **        that the current %component_iterator2d_range i point to.  
  ** 
  ** \return a %component_iterator2d_range reference.       
  */
  inline
  reference operator*()
  {
    return *pos_;
  }

  inline
  pointer operator->()
  { 
    return &(operator*()); 
  }

  /**
   ** \name  Forward operators addons
   */
  /*@{*/
  
  /*!
  ** \brief Preincrement a %component_iterator2d_range. Iterate to the next location
  **        inside the %Range.
  */
  inline
  self& operator++()
  {
     if( (x1_ <  int(range_x1_.start() + range_x1_.iterations() *  range_x1_.stride())) 
     || (x2_ <  int(range_x2_.start() + range_x2_.iterations() *  range_x2_.stride())) )
      {
	if(    (x2_ <  int(range_x2_.start() + range_x2_.iterations() *  range_x2_.stride()))
	    || (x1_ == int(range_x1_.start() + range_x1_.iterations() *  range_x1_.stride())) )
	  {
	    this->x2_+= range_x2_.stride();
	    this->pos_+= range_x2_.stride()*N;
	  }
	else 
	  {
	    this->x1_+= range_x1_.stride();
	    this->x2_ = range_x2_.start();
	    this->pos_ = &(*cont2d_)[0][0][this->cp_] + (this->x1_ * int(cont2d_->cols()) * N) + this->x2_ * N;
	  }
      }
     else
       {
	 this->x1_  = int(range_x1_.start() 
			  + (range_x1_.iterations() + 1) *  range_x1_.stride());
	 this->x2_  =  int(range_x2_.start() 
			   + (range_x2_.iterations() + 1) *  range_x2_.stride()); 
	 this->pos_ = &(*cont2d_)[0][0][this->cp_] + (this->x1_ * int(cont2d_->cols()) * N) + this->x2_ * N;
       }
     return *this;
  }

  /*!
  ** \brief Postincrement a %component_iterator2d_range. Iterate to the next location
  **        inside the %Range2d.
  **          
  */
  inline
  self operator++(int)
  {
    self tmp = *this;
    ++(*this);
    return tmp;
  }

   /*@} End Forward operators addons */

  /**
   ** \name  Bidirectional operators addons
   */
  /*@{*/
  /*!
  ** \brief Predecrement a %component_iterator2d_range. Iterate to the previous location
  **        inside the %Range2d.
  **          
  */
  inline
  self& operator--()
  {
    if( (x1_ >  int(range_x1_.stop() - range_x1_.iterations() *  range_x1_.stride())) || (x2_ >  int(range_x2_.stop() - range_x2_.iterations() *  range_x2_.stride())) )
      {
	if(    (x2_ > (int(range_x2_.stop() - range_x2_.iterations() *  range_x2_.stride()))) 
	       || (x1_ == int(range_x1_.stop() - range_x1_.iterations() *  range_x1_.stride())) )
	  {
	    this->x2_  -= range_x2_.stride();
	    this->pos_ -= range_x2_.stride()*N;
	  }
	else
	  {
	    this->x1_-= range_x1_.stride();
	    this->x2_ = int(range_x2_.start() + range_x2_.iterations() *  range_x2_.stride());
	    this->pos_ = &(*cont2d_)[0][0][this->cp_] + (this->x1_ * int(cont2d_->cols()) * N) + this->x2_ * N;
	  }
      }
    else
      {
	this->x1_ = int(range_x1_.stop() - (range_x1_.iterations() + 1) *  range_x1_.stride());
	this->x2_ = int(range_x2_.stop() - (range_x2_.iterations() + 1) *  range_x2_.stride());
	this->pos_ = &(*cont2d_)[0][0][this->cp_] + (this->x1_ * int(cont2d_->cols()) * N) + this->x2_ * N;
      }
    
    return *this;
  }
  
  /*!
  ** \brief Postdecrement a %component_iterator2d_range. Iterate to the previous location
  **        inside the %Range2d.
  **          
  */
  inline
  self operator--(int)
  {
    self tmp = *this;
    --(*this);
    return tmp;
  }

 /*@} End Bidirectional operators addons */
  
  /**
   ** \name  Equality comparable operators
   */
  /*@{*/
  /*!
  ** \brief Equality operator.
  ** \param i1 first %component_iterator2d_range.
  ** \param i2 second %component_iterator2d_range.
  ** \return true if i1 and i2 point to the same element of
  **         the 2d container
  */
  inline
  friend bool operator==(const self& i1,
			 const self& i2)
  {
    
    return ( (i1.cont2d_ == i2.cont2d_) && (i1.pos_ == i2.pos_) 
	     && (i1.x1_ == i2.x1_) && (i1.x2_ == i2.x2_)  && (i1.cp_ == i2.cp_));
  }

  /*!
  ** \brief Inequality operator.
  ** \param i1 first %component_iterator2d_range.
  ** \param i2 second %component_iterator2d_range.
  ** \return true if !(i1 == i2)
  */
  inline
  friend bool operator!=(const self& i1,
			 const self& i2)
  {
    return ( (i1.cont2d_ != i2.cont2d_) || (i1.pos_ != i2.pos_)
    || (i1.x1_ != i2.x1_) || (i1.x2_ != i2.x2_) || (i1.cp_ != i2.cp_));
  }
  /*@} End Equality comparable operators*/

  /**
   ** \name  Strict Weakly comparable operators
   */
  /*@{*/
  /*!
  ** \brief < operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true i1.pos_ < i2_pos
  */
  inline
  friend bool operator<(const self& i1,
			const self& i2)
  {
    
    return ( i1.pos_ < i2.pos_);
  }

    /*!
  ** \brief > operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true i2 < i1
  */
  inline
  friend bool operator>(const self& i1,
			const self& i2)
  {
    
    return (i2 < i1);
  }
  
     /*!
  ** \brief <= operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true if !(i2 < i1)
  */
  inline
  friend bool operator<=(const self& i1,
			 const self& i2)
  {
    
    return  !(i2 < i1);
  }

  /*!
  ** \brief >= operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true if !(i1 < i2)
  */
  inline
  friend bool operator>=(const self& i1,
			 const self& i2)
  {
    
    return  !(i1 < i2);
  }
  /*@} End  Strict Weakly comparable operators*/


  /**
   ** \name  component_iterator2d_range operators addons
   */
  /*@{*/
  /*!
  ** \brief component_iterator2d_range addition.
  ** \param d difference_type
  ** \return a component_iterator2d_range reference 
  ** \pre All the iterators between the current iterator i and i + d 
  **      must be dereferenceable.
   */
  inline
  self& operator+=(const difference_type& d)
  {
    this->x1_ += this->range_x1_.stride() * d.dx1();
    this->x2_ += this->range_x2_.stride() * d.dx2();
    this->pos_ = &(*cont2d_)[0][0][this->cp_] + (this->x1_ * int(cont2d_->cols()) * N) + this->x2_ * N;
    return *this;
  }
  
  /*!
  ** \brief component_iterator2d_range substraction.
  ** \param d difference_type
  ** \return a component_iterator2d_range reference 
  ** \pre All the iterators between the current iterator i and i - d 
  **      must be dereferenceable.
  */
  inline
  self& operator-=(const difference_type& d)
  {
    this->x1_ -= this->range_x1_.stride() * d.dx1();
    this->x2_ -= this->range_x2_.stride() * d.dx2();
    this->pos_ = &(*cont2d_)[0][0][this->cp_] + (this->x1_ * int(cont2d_->cols()) * N) + this->x2_ * N;
    return *this;
  }
  
   /*!
  ** \brief component_iterator2d_range addition.
  ** \param d difference_type
  ** \return a component_iterator2d_range reference 
  ** \pre All the iterators between the current iterator i and i + d 
  **      must be dereferenceable.
  */
  inline
  self operator+(const difference_type& d)
  {
    self tmp = *this;
    tmp += d;
    return tmp;
  }
  
  /*!
  ** \brief component_iterator2d_range substraction.
  ** \param d difference_type
  ** \return a component_iterator2d_range reference 
  ** \pre All the iterators between the current iterator i and i - d 
  **      must be dereferenceable.
  */
  inline
  self operator-(const difference_type& d)
  {
    self tmp = *this;
    tmp -= d;
    return tmp;
  }

  /*!
  ** \brief %component_iterator2d_range difference operator.
  ** \param i1 first %component_iterator2d_range.
  ** \param i2 second %component_iterator2d_range.
  ** \return a difference_type d such that i1 = i2 + stride*d.
  ** \pre Either i1 is reachable from i2, or i2 is reachable from i1.
  ** \pre Both strides must be equal
  */ 
  inline
  friend difference_type operator-(const self& i1,
				   const self& i2)
  {
    assert(i1.range_x1_.stride() == i2.range_x1_.stride());
    assert(i1.range_x2_.stride() == i2.range_x2_.stride());
    return difference_type(int((i1.x1_ - i2.x1_)/i1.range_x1_.stride()),int((i1.x2_ - i2.x2_))/i1.range_x2_.stride());
  }
  
  
  /*!
  ** \brief %component_iterator2d_range element assignment operator.
  **        Equivalent to *(i + d) = t.
  ** \param d difference_type.
  ** \return a reference to the 2d container elements
  ** \pre (i + d) exists and is dereferenceable.
  ** \post i[d] is a copy of reference.
  */ 
  inline
  reference operator[](difference_type d)
  {
    assert( (this->x1_+ range_x1_.stride() * d.dx1()) < cont2d_->dim1() );
    assert( (this->x2_+ range_x2_.stride() * d.dx2()) < cont2d_->dim2() );
    return (*cont2d_)[this->x1_+ range_x1_.stride() * d.dx1()][this->x2_+ range_x2_.stride() * d.dx2()][cp_];
  }
  



  /*!
  ** \brief %component_iterator2d_range element assignment operator.
  ** \param row offset.
  ** \return a row_range_iterator to the first element of range
  **         at the row \a row of the range
  */ 
  inline
  typename MultiComponentContainer2D::component_row_range_iterator row_begin(size_type row)
  {
    return cont2d_->row_begin(this->cp_, range_x1_.start() + row * range_x1_.stride(), range_x2_);
  }

 
  /*!
  ** \brief %component_iterator2d_range element assignment operator.
  ** \param row offset.
  ** \return a row_range_iterator to the past-the-end element of range
  **         at the row \a row of the range
  */ 
  inline
  typename MultiComponentContainer2D::component_row_range_iterator row_end(size_type row)
  {
    return this->row_begin(this->cp_, row) + ((range_x2_.iterations() + 1) * N);
  }

 

  /*!
  ** \brief %component_iterator2d_range element assignment operator.
  ** \param row offset.
  ** \return a range_col_iterator to the first element of range
  **         at the col \a col of the range
  */ 
  inline
  typename MultiComponentContainer2D::component_col_range_iterator col_begin(size_type col)
  {
    return cont2d_->col_begin(this->cp_, range_x2_.start() + col * range_x2_.stride(), range_x1_);
  }

  /*!
  ** \brief %component_iterator2d_range element assignment operator.
  ** \param row offset.
  ** \return a range_col_iterator to the past-the-end element of range
  **         at the col \a col of the range
  */ 
  inline
  typename MultiComponentContainer2D::component_col_range_iterator col_end(size_type col)
  {
    return this->col_begin(this->cp_, col) + ((range_x1_.iterations() + 1) * N);
  }


  /*@} End  component_iterator2d_range operators addons */
  

  /**
   ** \name Indices accessors
   */
  /*@{*/
  /*!
  ** \brief Access to the first index of the current %component_iterator2d_range.
  ** \return the  first index.
  */
  inline
  int x1() const
  {
    return this->x1_;
  }
  /*!
  ** \brief Access to the first index of the current %component_iterator2d_range.
  ** \return the  first index.
  */
  inline
  int i() const
  {
    return this->x1_;
  }
  /*!
  ** \brief Access to the second index of the current %component_iterator2d_range.
  ** \return the  second index.
  */
  inline
  int x2() const
  {
    return this->x2_;
  }
  /*!
  ** \brief Access to the second index of the current %component_iterator2d_range.
  ** \return the  second index.
  */
  inline
  int j() const
  {
    return this->x2_;
  }

  /*!
  ** \brief Access to the component index of the current %component_iterator2d_range.
  ** \return the component index
  */
  inline
  int cp() const
  {
    return this->cp_;
  }
  
 
  /*@} End  Indices accessors */


  
private: 
  MultiComponentContainer2D* cont2d_; // pointer to the 2d container
  pointer pos_;         // linear position within the container
  int x1_;              // first index position 
  int x2_;              // second index position
  int cp_;              // component index number
  slip::Range<int> range_x1_; // range to iterate on the first axis
  slip::Range<int> range_x2_; //range to iterate on the second axis
};


/*! \class const_component_iterator2d_range
** 
** \brief This is some iterator to iterate a 2d container into two %Range 
**        defined by the indices and strides of the 2d container.
** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
** \version 0.0.1
** \date 27/02/2008
**
** \param MultiComponentContainer2D Type of 2d container in the const_component_iterator2d_range 
** \param N number of elements in one %block
**
** \par This iterator is an 2d extension of the random_access_iterator.
**      It is compatible with the bidirectional_iterator of the
**      Standard library. It iterate into a range area defined inside the
**      indices range of the 2d container. Those indices are defined as 
**      follows :
** \image html iterator2d_conventions.jpg "axis and notation conventions"
** \image latex iterator2d_conventions.eps "axis and notation conventions" width=5cm
*/
template <class MultiComponentContainer2D, std::size_t N>
class const_component_iterator2d_range  
{
public:
  //typedef std::random_access_iterator_tag iterator_category;
  typedef std::random_access_iterator2d_tag iterator_category;
  typedef typename MultiComponentContainer2D::value_type Block;
  typedef typename Block::value_type value_type;
  typedef DPoint2d<int> difference_type;
  typedef value_type const * pointer;
  typedef value_type const & reference;
  
  typedef const_component_iterator2d_range self;
  
  typedef typename MultiComponentContainer2D::size_type size_type;
  /**
   ** \name Constructors & Destructors
   */
  /*@{*/
  
  /*!
  ** \brief Constructs a %const_component_iterator2d_range.
  ** \par The range to iterate is in this case the full container
  */
  const_component_iterator2d_range():
    cont2d_(0),pos_(0),x1_(0),x2_(0),cp_(0),range_x1_(0,0),range_x2_(0,0)
  {}

  /*!
  ** \brief Constructs a %const_component_iterator2d_range.
  ** \param c pointer to an existing 2d container
  ** \param r1 Range<int> defining the range of indices of the first
  **           axis to iterate
  ** \param r2 Range<int> defining the range of indices of the second
  **           axis to iterate
  ** 
  ** \pre range indices must be inside those of the 2d container
  */
  const_component_iterator2d_range(MultiComponentContainer2D* c, std::size_t cp,
		   const slip::Range<int>& r1,
		   const slip::Range<int>& r2):
    cont2d_(c),pos_(&(*c)[0][0][cp] + (r1.start() *  int(c->cols()) * N) + (r2.start() * N)),x1_(r1.start()),x2_(r2.start()),cp_(cp),range_x1_(r1),range_x2_(r2)
  {}

  /*!
  ** \brief Constructs a copy of the %const_component_iterator2d_range \a o
  ** 
  ** \param o %const_component_iterator2d_range to copy.
  */
  const_component_iterator2d_range(const self& o):
    cont2d_(o.cont2d_),pos_(o.pos_),x1_(o.x1_),x2_(o.x2_),cp_(o.cp_),
    range_x1_(o.range_x1_), range_x2_(o.range_x2_)
  {}

  /*@} End Constructors */

   /**
   ** \name  Assignment operators and methods
   */
  /*@{*/
  /*!
  ** \brief Assign a %const_component_iterator2d_range.
  **
  ** Assign elements of %const_component_iterator2d_range in \a o.
  **
  ** \param o %const_component_iterator2d_range to get the values from.
  ** \return a %const_component_iterator2d_range reference.
  */
  self& operator=(const self& o)
  {
    if(this != &o)
      {
	this->cont2d_ = o.cont2d_;
	this->pos_ = o.pos_;
	this->x1_ = o.x1_;
	this->x2_ = o.x2_;
	this->cp_ = o.cp_;
	this->range_x1_ = o.range_x1_;
	this->range_x2_ = o.range_x2_;
      }
    return *this;
  }
  /*@} End Assignment operators and methods*/

 
  /*!
  ** \brief Dereference operator.  Returns the element 
  **        that the current %const_component_iterator2d_range i point to.  
  ** 
  ** \return a const %const_component_iterator2d_range reference.     
  */
  inline
  reference operator*() const
  {
    return *pos_;
  }

  inline
  pointer operator->() const
  { 
    return &(operator*()); 
  }

 

  /**
   ** \name  Forward operators addons
   */
  /*@{*/
  
  /*!
  ** \brief Preincrement a %const_component_iterator2d_range. Iterate to the next location
  **        inside the %Range.
  **          
  */
  inline
  self& operator++()
  {
     if( (x1_ <  int(range_x1_.start() + range_x1_.iterations() *  range_x1_.stride())) 
     || (x2_ <  int(range_x2_.start() + range_x2_.iterations() *  range_x2_.stride())) )
      {
	if(    (x2_ <  int(range_x2_.start() + range_x2_.iterations() *  range_x2_.stride())) 
	       || (x1_ == int(range_x1_.start() + range_x1_.iterations() *  range_x1_.stride())) )
	  {
	    this->x2_+= range_x2_.stride();
	    this->pos_+= range_x2_.stride()*N;
	  }
	else 
	  {
	    this->x1_+= range_x1_.stride();
	    this->x2_ = range_x2_.start();
	    this->pos_ = &(*cont2d_)[0][0][this->cp_] + (this->x1_ * int(cont2d_->cols()) * N) + this->x2_ * N; 
	  }
      }
     else
       {
	 this->x1_  = int(range_x1_.start() 
			  + (range_x1_.iterations() + 1)*  range_x1_.stride());
	 this->x2_  = int(range_x2_.start() 
			  + (range_x2_.iterations() + 1)*  range_x2_.stride());
	 this->pos_ = &(*cont2d_)[0][0][this->cp_] + (this->x1_ * int(cont2d_->cols()) * N) + this->x2_ * N; 
       }
    return *this;
  }

  /*!
  ** \brief Postincrement a %const_component_iterator2d_range. Iterate to the next location
  **        inside the %Range2d.
  **          
  */
  inline
  self operator++(int)
  {
    self tmp = *this;
    ++(*this);
    return tmp;
  }

   /*@} End Forward operators addons */

  /**
   ** \name  Bidirectional operators addons
   */
  /*@{*/
  /*!
  ** \brief Predecrement a %const_component_iterator2d_range. Iterate to the previous location
  **        inside the %Range2d.
  **          
  */
  inline
  self& operator--()
  {
      if( (x1_ >  int(range_x1_.stop() - range_x1_.iterations() *  range_x1_.stride())) || (x2_ >  int(range_x2_.stop() - range_x2_.iterations() *  range_x2_.stride())) )
      {
	if(    (x2_ > (int(range_x2_.stop() - range_x2_.iterations() *  range_x2_.stride()))) 
	       || (x1_ == int(range_x1_.stop() - range_x1_.iterations() *  range_x1_.stride())) )
	  {
	    this->x2_  -= range_x2_.stride();
	    this->pos_ -= range_x2_.stride()*N;
	  }
	else
	  {
	    this->x1_-= range_x1_.stride();
	    this->x2_ = int(range_x2_.start() + range_x2_.iterations() *  range_x2_.stride());
	    this->pos_ = &(*cont2d_)[0][0][this->cp_] + (this->x1_ * int(cont2d_->cols()) * N) + this->x2_ * N; 
	  }
      }
    else
      {
	this->x1_ = int(range_x1_.stop() - (range_x1_.iterations() + 1) *  range_x1_.stride());
	this->x2_ = int(range_x2_.stop() - (range_x2_.iterations() + 1) *  range_x2_.stride());
	this->pos_ = &(*cont2d_)[0][0][this->cp_] + (this->x1_ * int(cont2d_->cols()) * N) + this->x2_ * N; 
      }

    return *this;
  }
  
  /*!
  ** \brief Postdecrement a %const_component_iterator2d_range. Iterate to the previous location
  **        inside the %Range2d.
  **          
  */
  inline
  self operator--(int)
  {
    self tmp = *this;
    --(*this);
    return tmp;
  }

 /*@} End Bidirectional operators addons */
  
  /**
   ** \name  Equality comparable operators
   */
  /*@{*/
  /*!
  ** \brief Equality operator.
  ** \param i1 first %const_component_iterator2d_range.
  ** \param i2 second %const_component_iterator2d_range.
  ** \return true if i1 and i2 point to the same element of
  **         the 2d container
  */
  inline
  friend bool operator==(const self& i1,
			 const self& i2)
  {
    
    return ( (i1.cont2d_ == i2.cont2d_) && (i1.pos_ == i2.pos_) 
	     && (i1.x1_ == i2.x1_) && (i1.x2_ == i2.x2_) && (i1.cp_ == i2.cp_));
  }

  /*!
  ** \brief Inequality operator.
  ** \param i1 first %const_component_iterator2d_range.
  ** \param i2 second %const_component_iterator2d_range.
  ** \return true if !(i1 == i2)
  */
  inline
  friend bool operator!=(const self& i1,
			 const self& i2)
  {
    return ( (i1.cont2d_ != i2.cont2d_) || (i1.pos_ != i2.pos_)
    || (i1.x1_ != i2.x1_) || (i1.x2_ != i2.x2_) || (i1.cp_ != i2.cp_));
  }
  /*@} End Equality comparable operators*/

   /**
   ** \name  Strict Weakly comparable operators
   */
  /*@{*/
  /*!
  ** \brief < operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true i1.pos_ < i2_pos
  */
  inline
  friend bool operator<(const self& i1,
			const self& i2)
  {
    
    return ( i1.pos_ < i2.pos_);
  }

    /*!
  ** \brief > operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true i2 < i1
  */
  inline
  friend bool operator>(const self& i1,
			const self& i2)
  {
    
    return (i2 < i1);
  }
  
     /*!
  ** \brief <= operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true if !(i2 < i1)
  */
  inline
  friend bool operator<=(const self& i1,
			 const self& i2)
  {
    
    return  !(i2 < i1);
  }

  /*!
  ** \brief >= operator.
  ** \param i1 first %iterator2d_box.
  ** \param i2 second %iterator2d_box.
  ** \return true if !(i1 < i2)
  */
  inline
  friend bool operator>=(const self& i1,
			 const self& i2)
  {
    
    return  !(i1 < i2);
  }
  /*@} End  Strict Weakly comparable operators*/



  /**
   ** \name  const_component_iterator2d_range operators addons
   */
  /*@{*/
  /*!
  ** \brief const_component_iterator2d_range addition.
  ** \param d difference_type
  ** \return a const_component_iterator2d_range reference 
  ** \pre All the iterators between the current iterator i and i + d 
  **      must be dereferenceable.
  **  \internal Invalid read of data in	
  **   this->pos_ = (*cont2d_)[this->x1_] + this->x2_;        
  **   when this->x1_ > cont2d_->dim1()

  */
  inline
  self& operator+=(const difference_type& d)
  {
    this->x1_ += this->range_x1_.stride() * d.dx1();
    this->x2_ += this->range_x2_.stride() * d.dx2();
    this->pos_ = &(*cont2d_)[0][0][this->cp_] + (this->x1_ * int(cont2d_->cols()) * N) + this->x2_ * N;
    return *this;
  }
  
  /*!
  ** \brief const_component_iterator2d_range substraction.
  ** \param d difference_type
  ** \return a const_component_iterator2d_range reference 
  ** \pre All the iterators between the current iterator i and i - d 
  **      must be dereferenceable.
  **  \internal Invalid read of data in	
  **   this->pos_ = (*cont2d_)[this->x1_] + this->x2_;        
  **   when this->x1_ > cont2d_->dim1()

  */
  inline
  self& operator-=(const difference_type& d)
  {
    this->x1_ -= this->range_x1_.stride() * d.dx1();
    this->x2_ -= this->range_x2_.stride() * d.dx2();
    this->pos_ = &(*cont2d_)[0][0][this->cp_] + (this->x1_ * int(cont2d_->cols()) * N) + this->x2_ * N; 
    return *this;
  }
  
   /*!
  ** \brief const_component_iterator2d_range addition.
  ** \param d difference_type
  ** \return a const_component_iterator2d_range reference 
  ** \pre All the iterators between the current iterator i and i + d 
  **      must be dereferenceable.
  */
  inline
  self operator+(const difference_type& d)
  {
    self tmp = *this;
    tmp += d;
    return tmp;
  }
  
  /*!
  ** \brief const_component_iterator2d_range substraction.
  ** \param d difference_type
  ** \return a const_component_iterator2d_range reference 
  ** \pre All the iterators between the current iterator i and i - d 
  **      must be dereferenceable.
  */
  inline
  self operator-(const difference_type& d)
  {
    self tmp = *this;
    tmp -= d;
    return tmp;
  }

  /*!
  ** \brief %const_component_iterator2d_range difference operator.
  ** \param i1 first %const_component_iterator2d_range.
  ** \param i2 second %const_component_iterator2d_range.
  ** \return a difference_type d such that i1 = i2 + stride*d.
  ** \pre Either i1 is reachable from i2, or i2 is reachable from i1.
  ** \pre Both strides must be equal
  */ 
  inline
  friend difference_type operator-(const self& i1,
				   const self& i2)
  {
    assert(i1.range_x1_.stride() == i2.range_x1_.stride());
    assert(i1.range_x2_.stride() == i2.range_x2_.stride());
    return difference_type(int((i1.x1_ - i2.x1_)/i1.range_x1_.stride()),int((i1.x2_ - i2.x2_))/i1.range_x2_.stride());
  }
  
  
 
  
 
 
   /*!
  ** \brief %const_component_iterator2d_range element assignment operator.
  **        Equivalent to *(i + d).
  ** \param d difference_type.
  ** \return a const reference to the 2d container elements
  ** \pre (i + d) exists and is dereferenceable.
  */ 
  inline
  reference operator[](difference_type d) const
  {
    assert( (this->x1_+ range_x1_.stride() * d.dx1()) < cont2d_->dim1() );
    assert( (this->x2_+ range_x2_.stride() * d.dx2()) < cont2d_->dim2() );
    return (*cont2d_)[this->x1_+ range_x1_.stride() * d.dx1()][this->x2_+ range_x2_.stride() * d.dx2()][cp_];
  }



  /*!
  ** \brief %const_component_iterator2d_range element assignment operator.
  ** \param row offset.
  ** \return a const_row_range_iterator to the first element of range
  **         at the row \a row of range
  ** \todo add assert
  */ 
  inline
  typename MultiComponentContainer2D::const_row_range_iterator row_begin(size_type row) const
  {
    return cont2d_->row_begin(this->cp_, range_x1_.start() + row * range_x1_.stride(), range_x2_);
  }

 
   /*!
  ** \brief %const_component_iterator2d_range element assignment operator.
  ** \param row offset.
  ** \return a const_row_range_iterator to the past-the-end element of range
  **         at the row \a row of range
  ** \todo add assert
  */ 
  inline
  typename MultiComponentContainer2D::const_row_range_iterator row_end(size_type row) const
  {
    return cont2d_->row_begin(this->cp_, row) + ((range_x2_.iterations() + 1) * N);
  }


  /*!
  ** \brief %const_component_iterator2d_range element assignment operator.
  ** \param row offset.
  ** \return a const col_range_iterator to the first element of range
  **         at the col \a col of the container
  ** \todo add assert
  */ 
  inline
  typename MultiComponentContainer2D::const_col_range_iterator col_begin(size_type col) const
  {
    return cont2d_->col_begin(this->cp_, range_x2_.start() + col * range_x2_.stride(), range_x1_);
  }



  /*!
  ** \brief %const_component_iterator2d_range element assignment operator.
  ** \param row offset.
  ** \return a const col_range_iterator to the past-the-end element of range
  **         at the col \a col of the container
  ** \todo add assert
  */ 
  inline
  typename MultiComponentContainer2D::const_col_range_iterator col_end(size_type col) const
  {
    return cont2d_->col_begin(this->cp_, col) + ((range_x1_.iterations() + 1) * N);
  }



  /*@} End  const_component_iterator2d_range operators addons */
  

  /**
   ** \name Indices accessors
   */
  /*@{*/
  /*!
  ** \brief Access to the first index of the current %const_component_iterator2d_range.
  ** \return the  first index.
  */
  inline
  int x1() const
  {
    return this->x1_;
  }
  /*!
  ** \brief Access to the first index of the current %const_component_iterator2d_range.
  ** \return the  first index.
  */
  inline
  int i() const
  {
    return this->x1_;
  }
  /*!
  ** \brief Access to the second index of the current %const_component_iterator2d_range.
  ** \return the  second index.
  */
  inline
  int x2() const
  {
    return this->x2_;
  }
  /*!
  ** \brief Access to the second index of the current %const_component_iterator2d_range.
  ** \return the  second index.
  */
  inline
  int j() const
  {
    return this->x2_;
  }
  
  /*!
  ** \brief Access to the component index of the current %const_component_iterator2d_box.
  ** \return the component index
  */
  inline
  int cp() const
  {
    return this->cp_;
  }
  
  /*@} End  Indices accessors */ 

private: 
  MultiComponentContainer2D* cont2d_; // pointer to the 2d container
  pointer pos_;         // linear position within the container
  int x1_;              // first index position 
  int x2_;              // second index position
  int cp_;              // component index number
  slip::Range<int> range_x1_; // range to iterate on the first axis
  slip::Range<int> range_x2_; //range to iterate on the second axis
};



 
}//slip::

#endif //SLIP_COMPONENT_ITERATOR2D_RANGE_HPP
