/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/*!
 ** \file Array4d.hpp
 **
 ** \brief Provides a class to manipulate 4d dynamic and generic arrays.
 ** \version Fluex 1.0
 ** \date 2013/07/04
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef SLIP_ARRAY4D_HPP
#define SLIP_ARRAY4D_HPP
#include <iostream>
#include <iterator>
#include <cassert>
#include <string>
#include <cstddef>
#include "Box4d.hpp"
#include "Point4d.hpp"
#include "DPoint4d.hpp"
#include "stride_iterator.hpp"
#include "iterator4d_box.hpp"
#include "iterator4d_range.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{
template<class T>
class stride_iterator;
}
namespace slip
{

template <typename T>
class Array4d;

template <typename T>
std::ostream& operator<<(std::ostream & out,
		const slip::Array4d<T>& a);

template<typename T>
bool operator==(const slip::Array4d<T>& x,
		const slip::Array4d<T>& y);

template<typename T>
bool operator!=(const slip::Array4d<T>& x,
		const slip::Array4d<T>& y);

template<typename T>
bool operator<(const slip::Array4d<T>& x,
		const slip::Array4d<T>& y);

template<typename T>
bool operator>(const slip::Array4d<T>& x,
		const slip::Array4d<T>& y);

template<typename T>
bool operator<=(const slip::Array4d<T>& x,
		const slip::Array4d<T>& y);

template<typename T>
bool operator>=(const slip::Array4d<T>& x,
		const slip::Array4d<T>& y);

/*!
 *  @defgroup Containers4d 4d containers
 *  @brief 4d containers having the same interface
 *  @{
 */

/*! \class Array4d
 ** \ingroup Containers Fluex Containers4d
 ** \version Fluex 1.0
 ** \date 2013/07/04
 ** \date 2014/03/13
 ** \since 1.4.0
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief This is a four-dimensional dynamic and generic container.
 ** This container statisfies the BidirectionnalContainer concepts of the STL.
 ** It is also an 4d extension of the RandomAccessContainer concept. That is
 ** to say the bracket element access is replaced by the quadruple
 ** bracket element access.
 ** \param T Type of the elements in the %Array4d.
 ** \par Axis conventions:
 ** \image html iterator4d_conventions.jpg "axis and notation conventions"
 ** \image latex iterator4d_conventions.eps "axis and notation conventions" width=5cm
 **
 */
template <typename T>
class Array4d
{
public:

	typedef T value_type;
	typedef Array4d<T> self;
	typedef const Array4d<T> const_self;

	typedef value_type & reference;
	typedef value_type const & const_reference;

	typedef value_type * pointer;
	typedef value_type const * const_pointer;

	typedef ptrdiff_t difference_type;
	typedef std::size_t size_type;

	typedef value_type * iterator;
	typedef value_type const * const_iterator;

	typedef std::reverse_iterator<iterator> reverse_iterator;
	typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

	//slab, slice, row and col iterator
	typedef slip::stride_iterator<pointer> slab_iterator;
	typedef slip::stride_iterator<const_pointer> const_slab_iterator;
	typedef slip::stride_iterator<pointer> slice_iterator;
	typedef slip::stride_iterator<const_pointer> const_slice_iterator;
	typedef pointer row_iterator;
	typedef const_pointer const_row_iterator;
	typedef slip::stride_iterator<pointer> col_iterator;
	typedef slip::stride_iterator<const_pointer> const_col_iterator;

	typedef slip::stride_iterator<slab_iterator> slab_range_iterator;
	typedef slip::stride_iterator<const_slab_iterator> const_slab_range_iterator;
	typedef slip::stride_iterator<slice_iterator> slice_range_iterator;
	typedef slip::stride_iterator<const_slice_iterator> const_slice_range_iterator;
	typedef slip::stride_iterator<pointer> row_range_iterator;
	typedef slip::stride_iterator<const_pointer> const_row_range_iterator;
	typedef slip::stride_iterator<col_iterator> col_range_iterator;
	typedef slip::stride_iterator<const_col_iterator> const_col_range_iterator;

	typedef std::reverse_iterator<slab_iterator> reverse_slab_iterator;
	typedef std::reverse_iterator<const_slab_iterator> const_reverse_slab_iterator;
	typedef std::reverse_iterator<slice_iterator> reverse_slice_iterator;
	typedef std::reverse_iterator<const_slice_iterator> const_reverse_slice_iterator;
	typedef std::reverse_iterator<iterator> reverse_row_iterator;
	typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
	typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
	typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;
	typedef std::reverse_iterator<slice_range_iterator> reverse_slab_range_iterator;
	typedef std::reverse_iterator<const_slice_range_iterator> const_reverse_slab_range_iterator;
	typedef std::reverse_iterator<slice_range_iterator> reverse_slice_range_iterator;
	typedef std::reverse_iterator<const_slice_range_iterator> const_reverse_slice_range_iterator;
	typedef std::reverse_iterator<row_range_iterator> reverse_row_range_iterator;
	typedef std::reverse_iterator<const_row_range_iterator> const_reverse_row_range_iterator;
	typedef std::reverse_iterator<col_range_iterator> reverse_col_range_iterator;
	typedef std::reverse_iterator<const_col_range_iterator> const_reverse_col_range_iterator;

	//iterator 4d
	typedef slip::iterator4d_box<self> iterator4d;
	typedef slip::const_iterator4d_box<const_self> const_iterator4d;
	typedef slip::iterator4d_range<self> iterator4d_range;
	typedef slip::const_iterator4d_range<const_self> const_iterator4d_range;

	typedef std::reverse_iterator<iterator4d> reverse_iterator4d;
	typedef std::reverse_iterator<const_iterator4d> const_reverse_iterator4d;
	typedef std::reverse_iterator<iterator4d_range> reverse_iterator4d_range;
	typedef std::reverse_iterator<const_iterator4d_range> const_reverse_iterator4d_range;

	//default iterator of the container
	typedef iterator4d default_iterator;
	typedef const_iterator4d const_default_iterator;

	//Range
	typedef slip::Range<int> range;

	static const std::size_t DIM = 4;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** \brief Constructs a %Array4d.
	 */
	Array4d();

	/*!
	 ** \brief Constructs a %Array4d.
	 ** \param d1 first dimension of the %Array4d
	 ** \param d2 second dimension of the %Array4d
	 ** \param d3 third dimension of the %Array4d
	 ** \param d4 fourth dimension of the %Array4d
	 **
	 ** \par The %Array4d is initialized by the default value of T.
	 */
	Array4d(const std::size_t d1,
			const std::size_t d2,
			const std::size_t d3,
			const std::size_t d4);

	/*!
	 ** \brief Constructs a %Array4d initialized by the scalar value \a val.
	 ** \param d1 first dimension of the %Array4d
	 ** \param d2 second dimension of the %Array4d
	 ** \param d3 third dimension of the %Array4d
	 ** \param d4 fourth dimension of the %Array4d
	 ** \param val initialization value of the elements
	 */
	Array4d(const std::size_t d1,
			const std::size_t d2,
			const std::size_t d3,
			const std::size_t d4,
			const T& val);
	/*!
	 ** \brief Constructs a %Array4d initialized by an array \a val.
	 ** \param d1 first dimension of the %Array4d
	 ** \param d2 second dimension of the %Array4d
	 ** \param d3 third dimension of the %Array4d
	 ** \param d4 fourth dimension of the %Array4d
	 ** \param val initialization array value of the elements
	 */
	Array4d(const std::size_t d1,
			const std::size_t d2,
			const std::size_t d3,
			const std::size_t d4,
			const T* val);

	/**
	 **  \brief  Contructs a %Array4d from a range.
	 ** \param d1 first dimension of the %Array4d
	 ** \param d2 second dimension of the %Array4d
	 ** \param d3 third dimension of the %Array4d
	 ** \param d4 fourth dimension of the %Array4d
	 **  \param  first  An input iterator.
	 **  \param  last  An input iterator.
	 ** \date 2014/03/13
	 ** Create a %Array4d consisting of copies of the elements from
	 ** [first,last).
	 */
	template<typename InputIterator>
	Array4d(const size_type d1,
			const size_type d2,
			const size_type d3,
			const size_type d4,
			InputIterator first,
			InputIterator last):
			d1_(d1),d2_(d2),d3_(d3),d4_(d4),size_(d1 * d2 * d3 * d4),data_(0)
			{
		this->allocate();
	
std::fill_n(this->begin(),this->size_,T());
		std::copy(first,last, this->begin());
			}

	/*!
	 ** \brief Constructs a copy of the Array4d \a rhs
	 */
	Array4d(const Array4d<T>& rhs);


	/*!
	 ** \brief Destructor of the Array4d
	 */
	~Array4d();


	/*@} End Constructors */


	/*!
	 ** \brief Resizes a %Array4d.
	 ** \param d1 new first dimension
	 ** \param d2 new second dimension
	 ** \param d3 new third dimension
	 ** \param d4 new fourth dimension
	 ** \param val new value for all the elements
	 */
	void resize(std::size_t d1,
			std::size_t d2,
			std::size_t d3,
			std::size_t d4,
			const T& val = T());
	/**
	 ** \name One dimensional global iterators
	 */
	/*@{*/

	//****************************************************************************
	//                          One dimensional iterators
	//****************************************************************************



	//----------------------Global iterators------------------------------

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  first element in the %Array4d.  Iteration is done in ordinary
	 **  element order.
	 ** \return const begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> const A1(10,9,5,2);
	 **  slip::Array4d<double> const A2(10,9,5,2);
	 **  slip::Array4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.begin(),A1.end(),
	 **                A2.begin(),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_iterator begin() const;

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element in the %Array4d.  Iteration is done in ordinary
	 **  element order.
	 ** \return begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(10,9,5,2);
	 **  slip::Array4d<double> A2(10,9,5,2);
	 **  slip::Array4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.begin(),A1.end(),
	 **                A2.begin(),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	iterator begin();

	/*!
	 **  \brief Returns a read/write iterator that points one past the last
	 **  element in the %Array4d.  Iteration is done in ordinary
	 **  element order.
	 ** \return end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(10,9,5,2);
	 **  slip::Array4d<double> A2(10,9,5,2);
	 **  slip::Array4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.begin(),A1.end(),
	 **                A2.begin(),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	iterator end();

	/*!
	 **  \brief Returns a read-only (constant) iterator that points one past
	 **  the last element in the %Array4d.  Iteration is done in
	 **  ordinary element order.
	 ** \return const end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(10,9,5,2);
	 **  slip::Array4d<double> A2(10,9,5,2);
	 **  slip::Array4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.begin(),A1.end(),
	 **                A2.begin(),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_iterator end() const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the
	 **  last element in the %Array4d. Iteration is done in reverse
	 **  element order.
	 ** \return reverse begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(10,9,5,2);
	 **  slip::Array4d<double> A2(10,9,5,2);
	 **  slip::Array4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.rbegin(),A1.rend(),
	 **                A2.rbegin(),S.rbegin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	reverse_iterator rbegin();

	/*!
	 **  \brief Returns a read-only (constant) reverse iterator that points
	 **  to the last element in the %Array4d.  Iteration is done in
	 **  reverse element order.
	 ** \return const reverse begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(10,9,5,2);
	 **  slip::Array4d<double> A2(10,9,5,2);
	 **  slip::Array4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.rbegin(),A1.rend(),
	 **                A2.rbegin(),S.rbegin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_reverse_iterator rbegin() const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to one
	 **  before the first element in the %Array4d.  Iteration is done
	 **  in reverse element order.
	 **  \return reverse end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(10,9,5,2);
	 **  slip::Array4d<double> A2(10,9,5,2);
	 **  slip::Array4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.rbegin(),A1.rend(),
	 **                A2.rbegin(),S.rbegin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	reverse_iterator rend();

	/*!
	 **  \brief Returns a read-only (constant) reverse iterator that points
	 **  to one before the first element in the %Array4d.  Iteration
	 **  is done in reverse element order.
	 **  \return const reverse end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(10,9,5,2);
	 **  slip::Array4d<double> A2(10,9,5,2);
	 **  slip::Array4d<double> S(10,9,5,2);
	 ** //copy the sum of A1 and A2 in S
	 ** std::transform(A1.rbegin(),A1.rend(),
	 **                A2.rbegin(),S.rbegin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_reverse_iterator rend() const;

	/*@} End One dimensional global iterators */

	//****************************************************************************
	//                          One dimensional stride iterators
	//****************************************************************************

	/**
	 ** \name One dimensional slab iterators
	 */
	/*@{*/
	//--------------------One dimensional slab iterators----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the first
	 ** element of the line (slice,row,col) through the slabs in the %Array4d.
	 ** Iteration is done in ordinary element order (increasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row  row coordinate of the line
	 ** \param col  col coordinate of the line
	 ** \return slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Array4d<double> A1(10,9,5,2);
	 ** slip::Array4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slabs through the first slice,
	 ** the second row and the third column. Put the calculated sums in the S array.
	 ** std::transform(A1.slab_begin(0,1,2),A1.slab_end(0,1,2),
	 **                A2.slab_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	slab_iterator slab_begin(const size_type slice, const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the first
	 **  element of the line (slice,row,col) through the slabs in the %Array4d.
	 **  Iteration is done in ordinary element order (increasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return const_slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Array4d<double> A1(10,9,5,2);
	 ** slip::Array4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slabs through the first slice,
	 ** the second row and the third column. Put the calculated sums in the S array.
	 ** std::transform(A1.slab_begin(0,1,2),A1.slab_end(0,1,2),
	 **                A2.slab_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_slab_iterator slab_begin(const size_type slice, const size_type row, const size_type col) const;

	/*!
	 ** \brief Returns a read/write iterator that points to the one past the end
	 **  element of the line (slice,row,col) through the slabs in the %Array4d.
	 **  Iteration is done in ordinary element order (increasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Array4d<double> A1(10,9,5,2);
	 ** slip::Array4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slabs through the first slice,
	 ** the second row and the third column. Put the calculated sums in the S array.
	 ** std::transform(A1.slab_begin(0,1,2),A1.slab_end(0,1,2),
	 **                A2.slab_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	slab_iterator slab_end(const size_type slice, const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the one past the end
	 **  element of the line (slice,row,col) through the slabs in the %Array4d.
	 **  Iteration is done in ordinary element order (increasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return const_slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Array4d<double> A1(10,9,5,2);
	 ** slip::Array4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slabs through the first slice,
	 ** the second row and the third column. Put the calculated sums in the S array.
	 ** std::transform(A1.slab_begin(0,1,2),A1.slab_end(0,1,2),
	 **                A2.slab_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_slab_iterator slab_end(const size_type slice, const size_type row, const size_type col) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the
	 ** last element of the line (slice,row,col) through the slabs in the %Array4d.
	 ** Iteration is done in reverse element order (decreasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return reverse_slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Array4d<double> A1(10,9,5,2);
	 ** slip::Array4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 ** the slabs through the first slice, the second row and the third column.
	 ** Put the calculated sums in the S array.
	 ** std::transform(A1.slab_rbegin(0,1,2),A1.slab_rend(0,1,2),
	 **                A2.slab_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	reverse_slab_iterator slab_rbegin(const size_type slice, const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the
	 **  last element of the line (slice,row,col) through the slabs in the %Array4d.
	 **  Iteration is done in reverse element order (decreasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return const_reverse_slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Array4d<double> A1(10,9,5,2);
	 ** slip::Array4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 ** the slabs through the first slice, the second row and the third column.
	 ** Put the calculated sums in the S array.
	 ** std::transform(A1.slab_rbegin(0,1,2),A1.slab_rend(0,1,2),
	 **                A2.slab_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_reverse_slab_iterator slab_rbegin(const size_type slice, const size_type row, const size_type col) const;

	/*!
	 ** \brief Returns a read/write iterator that points to the
	 ** one before the first element of the line (slice,row,col) through the slabs in the %Array4d.
	 ** Iteration is done in reverse element order (decreasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return reverse_slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Array4d<double> A1(10,9,5,2);
	 ** slip::Array4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 ** the slabs through the first slice, the second row and the third column.
	 ** Put the calculated sums in the S array.
	 ** std::transform(A1.slab_rbegin(0,1,2),A1.slab_rend(0,1,2),
	 **                A2.slab_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	reverse_slab_iterator slab_rend(const size_type slice, const size_type row, const size_type col);


	/*!
	 ** \brief Returns a read (constant) iterator that points to the
	 ** one before the first element of the line (slice,row,col) through the slabs in the %Array4d.
	 ** Iteration is done in reverse element order (decreasing slab number).
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \return const_reverse_slab_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 ** \code
	 **
	 ** slip::Array4d<double> A1(10,9,5,2);
	 ** slip::Array4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 ** the slabs through the first slice, the second row and the third column.
	 ** Put the calculated sums in the S array.
	 ** std::transform(A1.slab_rbegin(0,1,2),A1.slab_rend(0,1,2),
	 **                A2.slab_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_reverse_slab_iterator slab_rend(const size_type slice, const size_type row, const size_type col) const;

	/*@} End One dimensional slab iterators */

	/**
	 ** \name One dimensional slice iterators
	 */
	/*@{*/
	//--------------------One dimensional slice iterators----------------------


	/*!
	 ** \brief Returns a read/write iterator that points to the first
	 ** element of the line (slab,row,col) through the slices in the %Array4d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 **	\param slab slab coordinate of the line
	 **	\param row  row coordinate of the line
	 **	\param col  col coordinate of the line
	 ** \return slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::Array4d<double> A1(10,9,5,2);
	 **  slip::Array4d<double> A2(10,9,5,2);
	 **  slip::Array<double> S(9);
	 **  //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slices through the first slab,
	 **  the second row and the third column. Put the calculated sums in the S array.
	 **  std::transform(A1.slice_begin(0,1,2),A1.slice_end(0,1,2),
	 **                A2.slice_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 **  \endcode
	 */
	slice_iterator slice_begin(const size_type slab, const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the first
	 ** element of the line (slab,row,col) through the slices in the %Array4d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 ** \param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::Array4d<double> A1(10,9,5,2);
	 **  slip::Array4d<double> A2(10,9,5,2);
	 **  slip::Array<double> S(9);
	 **  //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slices through the first slab,
	 **  the second row and the third column. Put the calculated sums in the S array.
	 **  std::transform(A1.slice_begin(0,1,2),A1.slice_end(0,1,2),
	 **                A2.slice_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 **  \endcode
	 */
	const_slice_iterator slice_begin(const size_type slab, const size_type row, const size_type col) const;

	/*!
	 ** \brief Returns a read/write iterator that points to the one past the end
	 ** element of the line (slab,row,col) through the slices in the %Array4d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 **	\param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::Array4d<double> A1(10,9,5,2);
	 **  slip::Array4d<double> A2(10,9,5,2);
	 **  slip::Array<double> S(9);
	 **  //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slices through the first slab,
	 **  the second row and the third column. Put the calculated sums in the S array.
	 **  std::transform(A1.slice_begin(0,1,2),A1.slice_end(0,1,2),
	 **                A2.slice_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 **  \endcode
	 */
	slice_iterator slice_end(const size_type slab, const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the one past the end
	 ** element of the line (slab,row,col) through the slices in the %Array4d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 **	\param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::Array4d<double> A1(10,9,5,2);
	 **  slip::Array4d<double> A2(10,9,5,2);
	 **  slip::Array<double> S(9);
	 **  //calculate the sums of the A1 and A2 elements pairwise for the line crossing the slices through the first slab,
	 **  the second row and the third column. Put the calculated sums in the S array.
	 **  std::transform(A1.slice_begin(0,1,2),A1.slice_end(0,1,2),
	 **                A2.slice_begin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 **  \endcode
	 */
	const_slice_iterator slice_end(const size_type slab, const size_type row, const size_type col) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the
	 ** last element of the line (slab,row,col) through the slices in the %Array4d.
	 ** Iteration is done in reverse element order (decreasing slice number).
	 ** \param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 ** 	\return reverse_slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::Array4d<double> A1(10,9,5,2);
	 **  slip::Array4d<double> A2(10,9,5,2);
	 **  slip::Array<double> S(9);
	 **  //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 **  the slices through the first slab, the second row and the third column.
	 **  Put the calculated sums in the S array.
	 **  std::transform(A1.slice_rbegin(0,1,2),A1.slice_rend(0,1,2),
	 **                A2.slice_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 **  \endcode
	 */
	reverse_slice_iterator slice_rbegin(const size_type slab, const size_type row, const size_type col);

	/*!
	 ** \brief Returns a read-only (constant) iterator that points to the
	 ** last element of the line (slab,row,col) through the slices in the %Array4d.
	 **	Iteration is done in reverse element order (decreasing slice number).
	 **	\param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_reverse_slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(10,9,5,2);
	 **  slip::Array4d<double> A2(10,9,5,2);
	 **  slip::Array<double> S(9);
	 **  //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 **  the slices through the first slab, the second row and the third column.
	 **  Put the calculated sums in the S array.
	 **  std::transform(A1.slice_rbegin(0,1,2),A1.slice_rend(0,1,2),
	 **                A2.slice_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_reverse_slice_iterator slice_rbegin(const size_type slab, const size_type row, const size_type col) const;

	/*!
	 ** \brief Returns a read/write iterator that points to the
	 ** one before the first element of the line (slab,row,col) through the slices in the %Array4d.
	 ** Iteration is done in reverse element order (decreasing slice number).
	 ** \param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 ** \return reverse_slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Array4d<double> A1(10,9,5,2);
	 ** slip::Array4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 ** the slices through the first slab, the second row and the third column.
	 ** Put the calculated sums in the S array.
	 ** std::transform(A1.slice_rbegin(0,1,2),A1.slice_rend(0,1,2),
	 **                A2.slice_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	reverse_slice_iterator slice_rend(const size_type slab, const size_type row, const size_type col);


	/*!
	 ** \brief Returns a read (constant) iterator that points to the
	 ** one before the first element of the line (slab,row,col) through the slices in the %Array4d.
	 ** Iteration is done in reverse element order (decreasing slice number).
	 ** \param slab slab coordinate of the line
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_reverse_slice_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 ** slip::Array4d<double> A1(10,9,5,2);
	 ** slip::Array4d<double> A2(10,9,5,2);
	 ** slip::Array<double> S(9);
	 ** //calculate, in reverse order, the sums of the A1 and A2 elements pairwise for the line crossing
	 ** the slices through the first slab, the second row and the third column.
	 ** Put the calculated sums in the S array.
	 ** std::transform(A1.slice_rbegin(0,1,2),A1.slice_rend(0,1,2),
	 **                A2.slice_rbegin(0,1,2),S.begin(),
	 **                std::plus<double>());
	 ** \endcode
	 */
	const_reverse_slice_iterator slice_rend(const size_type slab, const size_type row, const size_type col) const;

	/*@} End One dimensional slice iterators */

	/**
	 ** \name One dimensional row iterators
	 */
	/*@{*/
	//-------------------row iterators----------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done in ordinary element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	row_iterator row_begin(const size_type slab, const size_type slice,
			const size_type row);

	/*!
	 **  \brief Returns a read_only iterator that points to the first
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done in ordinary element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin const_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_row_iterator row_begin(const size_type slab, const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done in ordinary element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	row_iterator row_end(const size_type slab, const size_type slice,
			const size_type row);


	/*!
	 **  \brief Returns a read_only iterator that points to the past-the-end
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done in ordinary element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end const_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_row_iterator row_end(const size_type slab, const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the last
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done in reverse element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	reverse_row_iterator row_rbegin(const size_type slab, const size_type slice,
			const size_type row);

	/*!
	 **  \brief Returns a read_only reverse iterator that points to the last
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done in reverse element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin const_reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_reverse_row_iterator row_rbegin(const size_type slab, const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done in reverse element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	reverse_row_iterator row_rend(const size_type slab, const size_type slice,
			const size_type row);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the first
	 **  element of the row \a row of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done in reverse element order.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end const_reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_reverse_row_iterator row_rend(const size_type slab, const size_type slice,
			const size_type row) const;

	/*@} End One dimensional row iterators */
	/**
	 ** \name One dimensional col iterators
	 */
	/*@{*/
	//-------------------col iterators----------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	col_iterator col_begin(const size_type slab, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only iterator that points to the first
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin const_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_col_iterator col_begin(const size_type slab, const size_type slice,
			const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	col_iterator col_end(const size_type slab, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only iterator that points to the past-the-end
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end const_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_col_iterator col_end(const size_type slab, const size_type slice,
			const size_type col) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the last
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	reverse_col_iterator col_rbegin(const size_type slab, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the last
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin const_reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_reverse_col_iterator col_rbegin(const size_type slab, const size_type slice,
			const size_type col) const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	reverse_col_iterator col_rend(const size_type slab, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the first
	 **  element of the column \a column of a given slab \a slab and a given slice \a slice in the %Array4d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slab The index of the slab.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end const_reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \note The row and col iterators iterate in the rows and columns respectively. There are
	 **  different from slab and slice iterators that cross the slabs and slices respectively.
	 */
	const_reverse_col_iterator col_rend(const size_type slab, const size_type slice,
			const size_type col) const;

	/*@} End One dimensional col iterators */

	//****************************************************************************
	//                          One dimensional range iterators
	//****************************************************************************

	/**
	 ** \name One dimensional slab range iterators
	 */
	/*@{*/
	//------------------------slab range iterators -----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Array4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,7,6,5);
	 **  slip::Array4d<double> A2(8,7,6,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the line (0,0,0) of A1 iterated according to the
	 ** //range in the line (1,1,1) of A2
	 ** std::copy(A1.slab_begin(0,0,0,range),A1.slab_end(0,0,0,range),A2.slab_begin(1,1,1));
	 ** \endcode
	 */
	slab_range_iterator slab_begin(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points one past the end
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Array4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,7,6,5);
	 **  slip::Array4d<double> A2(8,7,6,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the line (0,0,0) of A1 iterated according to the
	 ** //range in the line (1,1,1) of A2
	 ** std::copy(A1.slab_begin(0,0,0,range),A1.slab_end(0,0,0,range),A2.slab_begin(1,1,1));
	 ** \endcode
	 */
	slab_range_iterator slab_end(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the first
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Array4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> const A1(M); //M is an already existing %Array4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated according to the range
	 ** std::copy(A1.slab_begin(0,0,0,range),A1.slab_end(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 */
	const_slab_range_iterator slab_begin(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the last
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Array4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> const A1(M); //M is an already existing %Array4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated according to the range
	 ** std::copy(A1.slab_begin(0,0,0,range),A1.slab_end(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 */
	const_slab_range_iterator slab_end(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the last
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Array4d.
	 ** Iteration is done in the reverse element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin reverse_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> const A1(M); //M is an already existing %Array4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slab_rbegin(0,0,0,range),A1.slab_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 **/
	reverse_slab_range_iterator slab_rbegin(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Array4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end reverse_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> const A1(M); //M is an already existing %Array4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slab_rbegin(0,0,0,range),A1.slab_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 */
	reverse_slab_range_iterator slab_rend(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the last
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Array4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_reverse_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> const A1(M); //M is an already existing %Array4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slab_rbegin(0,0,0,range),A1.slab_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 */
	const_reverse_slab_range_iterator slab_rbegin(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the lastto the first
	 ** element of the %Range \a range of the of the line (slice,row,col)
	 ** through the slabs in the %Array4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slice slice coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_reverse_slab_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slab dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> const A1(M); //M is an already existing %Array4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slab_rbegin(0,0,0,range),A1.slab_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 */
	const_reverse_slab_range_iterator slab_rend(const size_type slice,const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*@} End One dimensional slab range iterators */


	/**
	 ** \name One dimensional slice range iterators
	 */
	/*@{*/
	//------------------------slice range iterators -----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Array4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,7,6,5);
	 **  slip::Array4d<double> A2(8,7,6,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the line (0,0,0) of A1 iterated according to the
	 ** //range in the line (1,1,1) of A2
	 ** std::copy(A1.slice_begin(0,0,0,range),A1.slice_end(0,0,0,range),A2.slice_begin(1,1,1));
	 ** \endcode
	 */
	slice_range_iterator slice_begin(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points one past the end
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Array4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,7,6,5);
	 **  slip::Array4d<double> A2(8,7,6,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the line (0,0,0) of A1 iterated according to the
	 ** //range in the line (1,1,1) of A2
	 ** std::copy(A1.slice_begin(0,0,0,range),A1.slice_end(0,0,0,range),A2.slice_begin(1,1,1));
	 ** \endcode
	 */
	slice_range_iterator slice_end(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the first
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Array4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> const A1(M); //M is an already existing %Array4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated according to the range
	 ** std::copy(A1.slice_begin(0,0,0,range),A1.slice_end(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 */
	const_slice_range_iterator slice_begin(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the last
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Array4d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> const A1(M); //M is an already existing %Array4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated according to the range
	 ** std::copy(A1.slice_begin(0,0,0,range),A1.slice_end(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 */
	const_slice_range_iterator slice_end(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the last
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Array4d.
	 ** Iteration is done in the reverse element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> const A1(M); //M is an already existing %Array4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slice_rbegin(0,0,0,range),A1.slice_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 **/
	reverse_slice_range_iterator slice_rbegin(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Array4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> const A1(M); //M is an already existing %Array4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slice_rbegin(0,0,0,range),A1.slice_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 */
	reverse_slice_range_iterator slice_rend(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the last
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Array4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> const A1(M); //M is an already existing %Array4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slice_rbegin(0,0,0,range),A1.slice_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 */
	const_reverse_slice_range_iterator slice_rbegin(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the lastto the first
	 ** element of the %Range \a range of the of the line (slab,row,col)
	 ** through the slices in the %Array4d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param slab slab coordinate of the line
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> const A1(M); //M is an already existing %Array4d
	 **  slip::Range<int> range(0,A2.dim2()-1,2);
	 ** //display the elements of the line (0,0,0) of A1 iterated in reverse order
	 ** //according to the range
	 ** std::copy(A1.slice_rbegin(0,0,0,range),A1.slice_rend(0,0,0,range),
	 ** std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 **
	 */
	const_reverse_slice_range_iterator slice_rend(const size_type slab,const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*@} End One dimensional slice range iterators */

	/**
	 ** \name One dimensional row range iterators
	 */
	/*@{*/
	//------------------------row range iterators -----------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the %Range \a range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Array4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_begin(0,0,0,range),A1.row_end(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	row_range_iterator row_begin(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read/write iterator that points one past the end
	 **  element of the %Range \a range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Array4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return end row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_begin(0,0,0,range),A1.row_end(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	row_range_iterator row_end(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Array4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin const_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_begin(0,0,0,range),A1.row_end(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	const_row_range_iterator row_begin(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range) const;


	/*!
	 **  \brief Returns a read_only iterator that points one past the last
	 **  element of the %Range range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Array4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row Row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return begin const_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \pre row must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_begin(0,0,0,range),A1.row_end(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	const_row_range_iterator row_end(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read-write iterator that points to the last
	 ** element of the %Range \a range of the row \a row in the slab
	 ** \a slab and the slice \a slice in the %Array4d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated
	 ** in reverse order according to the range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_rbegin(0,0,0,range),A1.row_rend(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	reverse_row_range_iterator row_rbegin(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-write iterator that points one before
	 **  the first element of the %Range \a range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Array4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated
	 ** in reverse order according to the range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_rbegin(0,0,0,range),A1.row_rend(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	reverse_row_range_iterator row_rend(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range);



	/*!
	 **  \brief Returns a read-only iterator that points to the last
	 **  element of the %Range \a range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Array4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return begin const_reverse_row_range_iterator value
	 **
	 ** \pre row must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated
	 ** in reverse order according to the range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_rbegin(0,0,0,range),A1.row_rend(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	const_reverse_row_range_iterator row_rbegin(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range) const;


	/*!
	 **  \brief Returns a read-only iterator that points one before the first
	 **  element of the %Range \a range of the row \a row in the slab
	 **  \a slab and the slice \a slice in the %Array4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return const_reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(5,4,8,4);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slab 0 and the slice 0 of A1 iterated
	 ** in reverse order according to the range in the row 1 of the slab 1 and slice 1 of A2
	 ** std::copy(A1.row_rbegin(0,0,0,range),A1.row_rend(0,0,0,range),A2.row_begin(1,1,1));
	 ** \endcode
	 */
	const_reverse_row_range_iterator row_rend(const size_type slab,const size_type slice,const size_type row,
			const slip::Range<int>& range) const;

	/*@} End One dimensional row range iterators */
	/**
	 ** \name One dimensional col range iterators
	 */
	/*@{*/
	//------------------------col range iterators -----------------------

	/*!
	 **  \brief Returns a read-write iterator that points to the first
	 **  element of the %Range \a range of the col \a col in the slab
	 **  \a slab and the slice \a slice in the %Array4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate
	 ** \return begin col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 ** //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the column 1 in the slab 1 and the slice 1 of A2
	 ** std::copy(A1.col_begin(0,0,0,range),A1.col_end(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	col_range_iterator col_begin(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read-write iterator that points to the past
	 **  the end element of the %Range \a range of the col \a col in the slab
	 **  \a slab and the slice \a slice in the
	 **  %Array4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 ** //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the column 1 in the slab 1 and the slice 1 of A2
	 ** std::copy(A1.col_begin(0,0,0,range),A1.col_end(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	col_range_iterator col_end(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the col \a col in the slab
	 **  \a slab and the slice \a slice in the %Array4d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 ** //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the column 1 in the slab 1 and the slice 1 of A2
	 ** std::copy(A1.col_begin(0,0,0,range),A1.col_end(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	const_col_range_iterator col_begin(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 **  \brief Returns a read-only iterator that points to the past
	 **  the end element of the %Range \a range of the col \a col in the slab
	 **  \a slab and the slice \a slice in
	 **  the %Array4d.
	 ** \param slab slab coordinate of the line
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate
	 ** \return begin const_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 ** //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated according to the
	 ** //range in the column 1 in the slab 1 and the slice 1 of A2
	 ** std::copy(A1.col_begin(0,0,0,range),A1.col_end(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	const_col_range_iterator col_end(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 **  \brief Returns a read-write iterator that points to the last
	 **  element of the %Range \a range of the col of a slice \a col in the slab
	 **  \a slab and the slice \a slice in the %Array4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 **  //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated
	 **  //in reverse order according to the range in the column 1 in the slab 1 and the slice 1 of A2
	 **  std::copy(A1.col_rbegin(0,0,0,range),A1.col_rend(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	reverse_col_range_iterator col_rbegin(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read-write iterator that points to one before
	 **  the first element of the %Range range of the col of a slice \a col in the slab
	 **  \a slab and the slice \a slice in the
	 **  %Array4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 **  //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated
	 **  //in reverse order according to the range in the column 1 in the slab 1 and the slice 1 of A2
	 **  std::copy(A1.col_rbegin(0,0,0,range),A1.col_rend(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	reverse_col_range_iterator col_rend(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read_only iterator that points to the last
	 **  element of the %Range \& range of the col of a slice \a col in the slab
	 **  \a slab and the slice \a slice in the %Array4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 **  //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated
	 **  //in reverse order according to the range in the column 1 in the slab 1 and the slice 1 of A2
	 **  std::copy(A1.col_rbegin(0,0,0,range),A1.col_rend(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	const_reverse_col_range_iterator col_rbegin(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the col of a slice \a col in the slab
	 **  \a slab and the slice \a slice in the %Array4d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slab slab coordinate of the line
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return const_reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %Array4d.
	 ** \pre The range must be inside the whole range of the %Array4d.
	 ** \par Example:
	 ** \code
	 **  slip::Array4d<double> A1(8,8,8,8);
	 **  slip::Array4d<double> A2(4,8,5,8);
	 **  slip::Range<int> range(0,A1.dim4()-1,2);
	 **  //copy the the elements of the column 0 in the slab 0 and the slice 0 of A1 iterated
	 **  //in reverse order according to the range in the column 1 in the slab 1 and the slice 1 of A2
	 **  std::copy(A1.col_rbegin(0,0,0,range),A1.col_rend(0,0,0,range),A2.col_begin(1,1,1));
	 ** \endcode
	 */
	const_reverse_col_range_iterator col_rend(const size_type slab,const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*@} End One dimensional col range iterators */

	//****************************************************************************
	//                          Four dimensional iterators
	//****************************************************************************

	/**
	 ** \name four dimensional iterators : Global iterators
	 */
	/*@{*/

	//------------------------ Global iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator4d that points to the first
	 **  element of the %Array4d. It points to the first front upper left element of
	 **  the %Array4d.
	 **
	 **  \return iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   //Print M
	 **   std::copy(M.first_front_upper_left(),M.last_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	iterator4d first_front_upper_left();


	/*!
	 **  \brief Returns a read/write iterator4d that points to the past
	 **  the end element of the %Array4d. It points to past the end element of
	 **  the last back bottom right element of the %Array4d.
	 **
	 **  \return iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   //Print M
	 **   std::copy(M.first_front_upper_left(),M.last_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	iterator4d last_back_bottom_right();


	/*!
	 **  \brief Returns a read-only iterator4d that points to the first
	 **  element of the %Array4d. It points to the fist front upper left element of
	 **  the %Array4d.
	 **
	 **  \return const iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   //Print M
	 **   std::copy(M.first_front_upper_left(),M.last_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d first_front_upper_left() const;


	/*!
	 **  \brief Returns a read-only iterator4d that points to the past
	 **  the end element of the %Array4d. It points to past the end element of
	 **  the last back bottom right element of the %Array4d.
	 **
	 **  \return const iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   //Print M
	 **   std::copy(M.first_front_upper_left(),M.last_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d last_back_bottom_right() const;

	/*!
	 **  \brief Returns a read/write reverse iterator4d. It points to the
	 **   last back bottom right element of the %Array4d.
	 *    Iteration is done within the %Array4d in the reverse order.
	 **
	 **  \return reverse_iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   //Print M in a reverse order
	 **   std::copy(M.rfirst_front_upper_left(),M.rlast_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d rfirst_front_upper_left();

	/*!
	 **  \brief Returns a read/write reverse iterator4d. It points to past the
	 **  first front upper left element of the %Array4d.
	 **  Iteration is done in the reverse order.
	 **
	 **  \return reverse iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   //Print M in a reverse order
	 **   std::copy(M.rfirst_front_upper_left(),M.rlast_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 **/
	reverse_iterator4d rlast_back_bottom_right();

	/*!
	 **  \brief Returns a read only reverse iterator4d that points.  It points
	 **  to the last back bottom right element of the %Array4d.
	 **  Iteration is done within the %Array4d in the reverse order.
	 **
	 ** \return const_reverse_iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   //Print M in a reverse order
	 **   std::copy(M.rfirst_front_upper_left(),M.rlast_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d rfirst_front_upper_left() const;


	/*!
	 **  \brief Returns a read only reverse iterator4d. It points to past the
	 **  first front upper left element of the %Array4d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \return const reverse iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   //Print M in a reverse order
	 **   std::copy(M.rfirst_front_upper_left(),M.rlast_back_bottom_right(),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d rlast_back_bottom_right() const;

	/*@} End four dimensional iterators : Global iterators */

	/**
	 ** \name four dimensional iterators : Box iterators
	 */
	/*@{*/

	//------------------------ Box iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator4d that points to the first
	 **  element of the %Array4d. It points to the first front upper left element of
	 **  the \a %Box4d associated to the %Array4d.
	 **
	 **  \param box A %Box4d defining the range of indices to iterate
	 **         within the %Array4d.
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \return iterator4d value
	 **  \pre The box indices must be inside the range of the %Array4d ones.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box
	 **   std::copy(M.first_front_upper_left(b),M.last_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	iterator4d first_front_upper_left(const Box4d<int>& box);


	/*!
	 **  \brief Returns a read/write iterator4d that points to the past
	 **  the end element of the %Array4d. It points to past the end element of
	 **  the last back bottom right element of the \a %Box4d associated to the %Array4d.
	 **
	 **  \param box a %Box4d defining the range of indices to iterate
	 **         within the %Array4d.
	 **
	 **  \return iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \pre The box indices must be inside the range of the %Array4d ones.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box
	 **   std::copy(M.first_front_upper_left(b),M.last_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	iterator4d last_back_bottom_right(const Box4d<int>& box);


	/*!
	 **  \brief Returns a read only iterator4d that points to the first
	 **  element of the %Array4d. It points to the front upper left element of
	 **  the \a %Box4d associated to the %Array4d.
	 **
	 **  \param box a %Box4d defining the range of indices to iterate
	 **         within the %Array4d.
	 **  \return end const iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **
	 **  \pre The box indices must be inside the range of the %Array4d ones.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box
	 **   std::copy(M.first_front_upper_left(b),M.last_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d first_front_upper_left(const Box4d<int>& box) const;


	/*!
	 **  \brief Returns a read only iterator4d that points to the past
	 **  the end element of the %Array4d. It points to past the end element of
	 **  the back bottom right element of the \a %Box4d associated to the %Array4d.
	 **
	 **  \param box a %Box4d defining the range of indices to iterate
	 **         within the %Array4d.
	 **  \return end const iterator4d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **
	 **  \pre The box indices must be inside the range of the %Array4d ones.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box
	 **   std::copy(M.first_front_upper_left(b),M.last_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d last_back_bottom_right(const Box4d<int>& box) const;



	/*!
	 **  \brief Returns a read/write reverse iterator4d. It points to the
	 **  back bottom right element of the \a %Box4d associated to the %Array4d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \param box a %Box4d defining the range of indices to iterate
	 **        within the %Array4d.
	 **
	 ** \pre The box indices must be inside the range of the %Array4d ones.
	 **
	 ** \return reverse iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box in reverse order.
	 **   std::copy(M.rfirst_front_upper_left(b),M.rlast_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d rfirst_front_upper_left(const Box4d<int>& box);

	/*!
	 **  \brief Returns a read/write reverse iterator4d. It points to one
	 **  before the front upper left element of the %Box4d \a box associated to
	 **  the %Array4d.
	 **
	 ** \param box A %Box4d defining the range of indices to iterate
	 **        within the %Array4d.
	 **
	 ** \pre The box indices must be inside the range of the %Array4d ones.
	 **
	 ** \return reverse iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box in reverse order.
	 **   std::copy(M.rfirst_front_upper_left(b),M.rlast_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d rlast_back_bottom_right(const Box4d<int>& box);

	/*!
	 **  \brief Returns a read only reverse iterator4d. It points to the
	 **  back bottom right element of the %Box4d \a box associated to the %Array4d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \param box A %Box4d defining the range of indices to iterate
	 **        within the %Array4d.
	 **
	 ** \pre The box indices must be inside the range of the %Array4d ones.
	 **
	 ** \return const reverse iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box in reverse order.
	 **   std::copy(M.rfirst_front_upper_left(b),M.rlast_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d rfirst_front_upper_left(const Box4d<int>& box) const;


	/*!
	 **  \brief Returns a read-only reverse iterator4d. It points to one
	 **  before the front element of the bottom right element of the %Box4d
	 **  \a box associated to the %Array4d.
	 **
	 ** \param box A %Box4d defining the range of indices to iterate
	 **        within the %Array4d.
	 **
	 ** \pre The box indices must be inside the range of the %Array4d ones.
	 **
	 ** \return const reverse iterator4d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Box4d<int> b(1,1,1,1,2,2,3,3);
	 **   //Print M values within the box in reverse order.
	 **   std::copy(M.rfirst_front_upper_left(b),M.rlast_back_bottom_right(b),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d rlast_back_bottom_right(const Box4d<int>& box) const;


	/*@} End four dimensional iterators : Box iterators */

	/**
	 ** \name four dimensional iterators : Range iterators
	 */

	/*@{*/

	//------------------------ Range iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator4d_range that points to the
	 **  first front upper left element of the ranges \a slab_range, \a slice_range,
	 **  \a row_range and \a col_range associated to the %Array4d.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Array4d ones.
	 **
	 ** \return iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.first_front_upper_left(slar,slir,rr,cr),M.last_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	iterator4d_range first_front_upper_left(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range);

	/*!
	 ** \brief Returns a read/write iterator4d_range that points to the
	 **  past the end last back bottom right element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Array4d.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Array4d ones.
	 **
	 ** \return iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.first_front_upper_left(slar,slir,rr,cr),M.last_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	iterator4d_range last_back_bottom_right(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range);


	/*!
	 **  \brief Returns a read-only iterator4d_range that points to the
	 **   to the first front upper left element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Array4d.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Array4d ones.
	 **
	 ** \return const_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.first_front_upper_left(slar,slir,rr,cr),M.last_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d_range first_front_upper_left(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range) const;


	/*!
	 **  \brief Returns a read-only iterator4d_range that points to the past
	 **  the end last back bottom right element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Array4d.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Array4d ones.
	 **
	 ** \return const_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.first_front_upper_left(slar,slir,rr,cr),M.last_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_iterator4d_range last_back_bottom_right(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range) const;

	/*!
	 **  \brief Returns a read/write reverse_iterator4d_range that points to the
	 **  past the last back bottom right element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Array4d.
	 **  Iteration is done in the reverse order.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Array4d ones.
	 **
	 ** \return reverse_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfirst_front_upper_left(slar,slir,rr,cr),M.rlast_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d_range rfirst_front_upper_left(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range);

	/*!
	 **  \brief Returns a read/write reverse_iterator4d_range that points
	 **  to one before the first front upper left element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Array4d.
	 **  Iteration is done in the reverse order.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Array4d ones.
	 **
	 ** \return reverse_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfirst_front_upper_left(slar,slir,rr,cr),M.rlast_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator4d_range rlast_back_bottom_right(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range);

	/*!
	 ** \brief Returns a read-only reverse_iterator4d_range that points
	 **  to the past the last back bottom right element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Array4d.
	 **  Iteration is done in the reverse order.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Array4d ones.
	 **
	 ** \return const_reverse_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfirst_front_upper_left(slar,slir,rr,cr),M.rlast_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d_range rfirst_front_upper_left(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range) const;

	/*!
	 **  \brief Returns a read-only reverse_iterator4d_range that points
	 **  to one before the first front upper left element of the ranges \a slab_range,
	 **  \a slice_range, \a row_range and \a col_range associated to the %Array4d.
	 **  Iteration is done in the reverse order.
	 ** \param slab_range The range of the slices.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slab_range, slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %Array4d ones.
	 **
	 ** \return const_reverse_iterator4d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::Array4d<double> M(3,4,5,6);
	 **   slip::Range<int> slar(0,2,2);
	 **   slip::Range<int> slir(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfirst_front_upper_left(slar,slir,rr,cr),M.rlast_back_bottom_right(slar,slir,rr,cr),
	 **             std::ostream_iterator<double>(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator4d_range rlast_back_bottom_right(const range & slab_range, const range & slice_range,
			const range & row_range, const range & col_range) const;


	/*@} End four dimensional iterators : Range iterators */

	//********************************************************************

	/**
	 ** \name i/o operators
	 */
	/*@{*/
	/*!
	 ** \brief Write the %Array4d to the ouput stream
	 ** \param out output std::ostream
	 ** \param a %Array4d to write to the output stream
	 */
	friend std::ostream& operator<< <>(std::ostream & out,
			const self& a);

	/*@} End i/o operators */

	/**
	 ** \name  Assignment operators and methods
	 */
	/*@{*/
	/*!
	 ** \brief Assign a %Array4d.
	 **
	 ** Assign elements of %Array4d in \a rhs
	 **
	 ** \param rhs %Array4d to get the values from.
	 ** \return
	 */
	self& operator=(const Array4d<T> & rhs);



	/*!
	 ** \brief Assign all the elments of the %Array4d by value
	 **
	 **
	 ** \param value A reference-to-const of arbitrary type.
	 ** \return
	 */
	self& operator=(const T& value);


	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with copies of value
	 ** \param value  A reference-to-const of arbitrary type.
	 */
	void fill(const T& value)
	{
		std::fill_n(this->begin(),this->size_,value);
	}

	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of
	 **        the value array
	 ** \param value  A pointer of arbitrary type.
	 */
	void fill(const T* value)
	{
		std::copy(value,value + this->size_, this->begin());
	}

	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of the range [first,last)
	 **  \param  first  An input iterator.
	 **  \param  last   An input iterator.
	 **
	 **
	 */
	template<typename InputIterator>
	void fill(InputIterator first,
			InputIterator last)
	{
		std::copy(first,last, this->begin());
	}
	/*@} End Assignment operators and methods*/



	/**
	 ** \name Comparison operators
	 */
	/*@{*/
	/*!
	 ** \brief %Array4d equality comparison
	 ** \param x A %Array4d
	 ** \param y A %Array4d of the same type of \a x
	 ** \return true iff the size and the elements of the Arrays are equal
	 */
	friend bool operator== <>(const Array4d<T>& x,
			const Array4d<T>& y);

	/*!
	 ** \brief %Array4d inequality comparison
	 ** \param x A %Array4d
	 ** \param y A %Array4d of the same type of \a x
	 ** \return true if !(x == y)
	 */
	friend bool operator!= <>(const Array4d<T>& x,
			const Array4d<T>& y);

	/*!
	 ** \brief Less than comparison operator (%Array4d ordering relation)
	 ** \param x A %Array4d
	 ** \param y A %Array4d of the same type of \a x
	 ** \return true iff \a x is lexicographically less than \a y
	 */
	friend bool operator< <>(const Array4d<T>& x,
			const Array4d<T>& y);

	/*!
	 ** \brief More than comparison operator
	 ** \param x A %Array4d
	 ** \param y A %Array4d of the same type of \a x
	 ** \return true iff y > x
	 */
	friend bool operator> <>(const Array4d<T>& x,
			const Array4d<T>& y);

	/*!
	 ** \brief Less than equal comparison operator
	 ** \param x A %Array4d
	 ** \param y A %Array4d of the same type of \a x
	 ** \return true iff !(y > x)
	 */
	friend bool operator<= <>(const Array4d<T>& x,
			const Array4d<T>& y);

	/*!
	 ** \brief More than equal comparison operator
	 ** \param x A %Array4d
	 ** \param y A %Array4d of the same type of \a x
	 ** \return true iff !(x < y)
	 */
	friend bool operator>= <>(const Array4d<T>& x,
			const Array4d<T>& y);


	/*@} Comparison operators */

	/**
	 ** \name  Element access operators
	 */
	/*@{*/

	T*** operator[](const size_type l);

	const T** const* operator[](const size_type l) const;


	/*!
	 ** \brief Subscript access to the data contained in the %Array4d.
	 ** \param l The index of the slab for which the data should be accessed.
	 ** \param k The index of the slice for which the data should be accessed.
	 ** \param i The index of the row for which the data should be accessed.
	 ** \param j The index of the column for which the data should be accessed.
	 ** \return Read/Write reference to data.
	 ** \pre l < slabs()
	 ** \pre k < slices()
	 ** \pre i < rows()
	 ** \pre j < cols()
	 ** This operator allows for easy, 4d array-style, data access.
	 ** Note that data access with this operator is unchecked and
	 ** out_of_range lookups are not defined.
	 */
	reference operator()(const size_type l, const size_type k,
			const size_type i,
			const size_type j);

	/*!
	 ** \brief Subscript access to the data contained in the %Array4d.
	 ** \param l The index of the slab for which the data should be accessed.
	 ** \param k The index of the slice for which the data should be accessed.
	 ** \param i The index of the row for which the data should be accessed.
	 ** \param j The index of the column for which the data should be accessed.
	 ** \return Read_only (constant) reference to data.
	 ** \pre l < slabs()
	 ** \pre k < slices()
	 ** \pre i < rows()
	 ** \pre j < cols()
	 ** This operator allows for easy, 4d array-style, data access.
	 ** Note that data access with this operator is unchecked and
	 ** out_of_range lookups are not defined.
	 */
	const_reference operator()(const size_type l, const size_type k,
			const size_type i,
			const size_type j) const;

	/*@} End Element access operators */

	/*!
	 ** \brief Returns the name of the class
	 **
	 */
	std::string name() const;

	/*!
	 ** \brief Returns the number of slabs (first dimension size, called time dimension too)
	 **        in the %Array4d
	 */
	size_type dim1() const;

	/*!
	 ** \brief Returns the number of slabs (first dimension size, called time dimension too)
	 **        in the %Array4d
	 */
	size_type slabs() const;

	/*!
	 ** \brief Returns the number of slices (second dimension size)
	 **        in the %Array4d
	 */
	size_type dim2() const;

	/*!
	 ** \brief Returns the number of slices (second dimension size)
	 **        in the %Array4d
	 */
	size_type slices() const;

	/*!
	 ** \brief Returns the number of rows (third dimension size)
	 **        in the %Array4d
	 */
	size_type dim3() const;

	/*!
	 ** \brief Returns the number of rows (third dimension size)
	 **        in the %Array4d
	 */
	size_type rows() const;

	/*!
	 ** \brief Returns the number of columns (fourth dimension size)
	 **        in the %Array4d
	 */
	size_type dim4() const;

	/*!
	 ** \brief Returns the number of columns (fourth dimension size)
	 **        in the %Array4d
	 */
	size_type cols() const;

	/*!
	 ** \brief Returns the number of columns (fourth dimension size)
	 **        in the %Array4d
	 */
	size_type columns() const;



	/*!
	 ** \brief Returns the number of elements in the %Array4d
	 */
	size_type size() const;


	/*!
	 ** \brief Returns the maximal size (number of elements) in the %Array4d
	 */
	size_type max_size() const;


	/*!
	 ** \brief Returns true if the %Array4d is empty.  (Thus size() == 0)
	 */
	bool empty()const;

	/*!
	 ** \brief Swaps data with another %Array4d.
	 ** \param M A %Array4d of the same element type
	 **
	 ** \pre dim1() == M.dim1()
	 ** \pre dim2() == M.dim2()
	 ** \pre dim3() == M.dim3()
	 ** \pre dim4() == M.dim4()
	 */
	void swap(Array4d& M);




private:
	size_type d1_;
	size_type d2_;
	size_type d3_;
	size_type d4_;
	size_type size_;
	T**** data_;

	/*!
	 ** \brief Allocates the  memory to store the elements of the Array4d
	 ** \par Axis conventions:
	 ** \image html memory4d.jpg "Memory structure"
	 ** \image latex memory4d.eps "Memory structure" width=5cm
	 */
	void allocate();

	/*!
	 ** \brief Deallocates the  memory
	 ** \par Axis conventions:
	 ** \image html memory4d.jpg "Memory structure"
	 ** \image latex memory4d.eps "Memory structure" width=5cm
	 */
	void desallocate();

	/*!
	 ** \brief Copy the attributes of the Array4d \a rhs to the Array4d
	 ** \param rhs the Array4d from which the attributes are copied
	 */
	void copy_attributes(const self& rhs);

  
 friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & this->d1_ & this->d2_ & this->d3_  & this->d4_& this->size_ ;
	  for (size_t i = 0; i < this->size_; ++i)
	    {
	      ar & (***data_)[i];
	    }
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & this->d1_ & this->d2_ & this->d3_ & this->d4_& this->size_ ;
	  desallocate();
	  allocate();
	  for (size_t i = 0; i < this->size_; ++i)
	    {
	      ar & (***data_)[i];
	    }
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()

};

/** @} */ // end of Containers4d group

///double alias
typedef slip::Array4d<double> Array4d_d;
///float alias
typedef slip::Array4d<float> Array4d_f;
///long alias
typedef slip::Array4d<long> Array4d_l;
///unsigned long alias
typedef slip::Array4d<unsigned long> Array4d_ul;
///short alias
typedef slip::Array4d<short> Array4d_s;
///unsigned long alias
typedef slip::Array4d<unsigned short> Array4d_us;
///int alias
typedef slip::Array4d<int> Array4d_i;
///unsigned int alias
typedef slip::Array4d<unsigned int> Array4d_ui;
///char alias
typedef slip::Array4d<char> Array4d_c;
///unsigned char alias
typedef slip::Array4d<unsigned char> Array4d_uc;
}//slip::


namespace slip
{

//////////////////////////////////////////
//   Constructors
//////////////////////////////////////////
template<typename T>
inline
Array4d<T>::Array4d():
d1_(0),d2_(0),d3_(0),d4_(0),size_(0),data_(0)
{}


template<typename T>
inline
Array4d<T>::Array4d(const typename Array4d<T>::size_type d1,
		const typename Array4d<T>::size_type d2,
		const typename Array4d<T>::size_type d3,
		const typename Array4d<T>::size_type d4):
		d1_(d1),d2_(d2),d3_(d3),d4_(d4),size_(d1 * d2 * d3 * d4),data_(0)
		{
	this->allocate();
	std::fill_n(this->data_[0][0][0],this->size_,T());
		}

template<typename T>
inline
Array4d<T>::Array4d(const typename Array4d<T>::size_type d1,
		const typename Array4d<T>::size_type d2,
		const typename Array4d<T>::size_type d3,
		const typename Array4d<T>::size_type d4,
		const T& val):
		d1_(d1),d2_(d2),d3_(d3),d4_(d4),size_(d1 * d2 * d3 * d4),data_(0)
		{
	this->allocate();
	std::fill_n(this->data_[0][0][0],this->size_,val);
		}

template<typename T>
inline
Array4d<T>::Array4d(const typename Array4d<T>::size_type d1,
		const typename Array4d<T>::size_type d2,
		const typename Array4d<T>::size_type d3,
		const typename Array4d<T>::size_type d4,
		const T* val):
		d1_(d1),d2_(d2),d3_(d3),d4_(d4),size_(d1 * d2 * d3 * d4),data_(0)
		{
	this->allocate();
	std::copy(val,val + this->size_, this->data_[0][0][0]);
		}

template<typename T>
inline
Array4d<T>::Array4d(const Array4d<T>& rhs):
d1_(rhs.d1_),d2_(rhs.d2_),d3_(rhs.d3_),d4_(rhs.d4_),size_(rhs.size_),data_(0)
{
	this->allocate();
	if(this->size_ != 0)
	{
		std::copy(rhs.data_[0][0][0],rhs.data_[0][0][0] + this->size_,this->data_[0][0][0]);
	}
}

template<typename T>
inline
Array4d<T>::~Array4d()
{
	this->desallocate();
}

///////////////////////////////////////////


//////////////////////////////////////////
//   Assignment operators
//////////////////////////////////////////
template<typename T>
inline
Array4d<T>&  Array4d<T>::operator=(const Array4d<T> & rhs)
{
	if(this != &rhs)
	{
		if( this->d1_ != rhs.d1_ || this->d2_ != rhs.d2_ || this->d3_ != rhs.d3_ || this->d4_ != rhs.d4_)
		{
			this->desallocate();
			this->copy_attributes(rhs);
			this->allocate();
		}
		if(rhs.size_ != 0)
		{
			std::copy(rhs.data_[0][0][0],rhs.data_[0][0][0] + this->size_,this->data_[0][0][0]);
		}
	}
	return *this;
}

template<typename T>
inline
Array4d<T>& Array4d<T>::operator=(const T& value)
{
	this->fill(value);
	return *this;
}
///////////////////////////////////////////


template<typename T>
inline
void Array4d<T>::resize(const typename Array4d<T>::size_type d1,
		const typename Array4d<T>::size_type d2,
		const typename Array4d<T>::size_type d3,
		const typename Array4d<T>::size_type d4,
		const T& val)
		{
	if( this->d1_ != d1 || this->d2_ != d2 || this->d3_ != d3 || this->d4_ != d4)
	{
		this->desallocate();
		this->d1_ = d1;
		this->d2_ = d2;
		this->d3_ = d3;
		this->d4_ = d4;
		this->size_ = d1 * d2 * d3 * d4;
		this->allocate();
	}
	if( this->d1_ != 0 && this->d2_ != 0 && this->d3_ != 0 && this->d4_ != 0)
	{
		std::fill_n(this->data_[0][0][0],this->size_,val);
	}

		}



//////////////////////////////////////////
//   Iterators
//////////////////////////////////////////

//****************************************************************************
//                          One dimensional iterators
//****************************************************************************



//----------------------Global iterators------------------------------

template<typename T>
inline
typename Array4d<T>::const_iterator Array4d<T>::begin() const
{
	return this->data_[0][0][0];
}

template<typename T>
inline
typename Array4d<T>::const_iterator Array4d<T>::end() const
{
	return this->data_[0][0][0] + this->size_;
}

template<typename T>
inline
typename Array4d<T>::iterator Array4d<T>::begin()
{
	return this->data_[0][0][0];
}

template<typename T>
inline
typename Array4d<T>::iterator Array4d<T>::end()
{
	return this->data_[0][0][0] + this->size_;
}

template<typename T>
inline
typename Array4d<T>::reverse_iterator Array4d<T>::rbegin()
{
	return typename Array4d<T>::reverse_iterator(this->end());
}

template<typename T>
inline
typename Array4d<T>::const_reverse_iterator Array4d<T>::rbegin() const
{
	return typename Array4d<T>::const_reverse_iterator(this->end());
}

template<typename T>
inline
typename Array4d<T>::reverse_iterator Array4d<T>::rend()
{
	return typename Array4d<T>::reverse_iterator(this->begin());
}


template<typename T>
inline
typename Array4d<T>::const_reverse_iterator Array4d<T>::rend() const
{
	return  typename Array4d<T>::const_reverse_iterator(this->begin());
}

//--------------------One dimensional slab iterators----------------------


template<typename T>
inline
typename Array4d<T>::slab_iterator Array4d<T>::slab_begin(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col){
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return typename Array4d<T>::slab_iterator(this->data_[0][slice][row] + col,this->d2_*this->d3_*this->d4_);
}


template<typename T>
inline
typename Array4d<T>::const_slab_iterator Array4d<T>::slab_begin(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col) const{
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return typename Array4d<T>::const_slab_iterator(this->data_[0][slice][row] + col,this->d2_*this->d3_*this->d4_);
}

template<typename T>
inline
typename Array4d<T>::slab_iterator Array4d<T>::slab_end(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col){
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return (this->slab_begin(slice,row,col) + this->d1_);
}


template<typename T>
inline
typename Array4d<T>::const_slab_iterator Array4d<T>::slab_end(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col) const{
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	slip::Array4d<T> const * tp(this);
	return (tp->slab_begin(slice,row,col) + this->d1_);
}


template<typename T>
inline
typename Array4d<T>::reverse_slab_iterator Array4d<T>::slab_rbegin(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col){
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return typename Array4d<T>::reverse_slab_iterator(this->slab_end(slice,row,col));
}

template<typename T>
inline
typename Array4d<T>::const_reverse_slab_iterator Array4d<T>::slab_rbegin(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col) const{
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return typename Array4d<T>::const_reverse_slab_iterator(this->slab_end(slice,row,col));
}


template<typename T>
inline
typename Array4d<T>::reverse_slab_iterator Array4d<T>::slab_rend(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col){
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return typename Array4d<T>::reverse_slab_iterator(this->slab_begin(slice,row,col));
}

template<typename T>
inline
typename Array4d<T>::const_reverse_slab_iterator Array4d<T>::slab_rend(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col) const{
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return typename Array4d<T>::const_reverse_slab_iterator(this->slab_begin(slice,row,col));
}


//-------------------- One dimensional slice iterators----------------------

template<typename T>
inline
typename Array4d<T>::slice_iterator
Array4d<T>::slice_begin(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type row,
		const typename Array4d<T>::size_type col)
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return typename Array4d<T>::slice_iterator(this->data_[slab][0][row] + col,this->d3_*this->d4_);
		}

template<typename T>
inline
typename Array4d<T>::const_slice_iterator
Array4d<T>::slice_begin(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type row,
		const typename Array4d<T>::size_type col) const
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return typename Array4d<T>::const_slice_iterator(this->data_[slab][0][row] + col,this->d3_*this->d4_);
		}

template<typename T>
inline
typename Array4d<T>::slice_iterator
Array4d<T>::slice_end(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type row,
		const typename Array4d<T>::size_type col)
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return ++(typename Array4d<T>::slice_iterator(this->data_[slab][this->d2_-1][row] + col,this->d3_*this->d4_));
		}

template<typename T>
inline
typename Array4d<T>::const_slice_iterator
Array4d<T>::slice_end(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type row,
		const typename Array4d<T>::size_type col) const
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return ++(typename Array4d<T>::const_slice_iterator(this->data_[slab][this->d2_-1][row] + col,this->d3_*this->d4_));
		}

template<typename T>
inline
typename Array4d<T>::reverse_slice_iterator
Array4d<T>::slice_rbegin(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type row,
		const typename Array4d<T>::size_type col)
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return typename Array4d<T>::reverse_slice_iterator(this->slice_end(slab,row,col));
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_slice_iterator
Array4d<T>::slice_rbegin(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type row,
		const typename Array4d<T>::size_type col) const
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return typename Array4d<T>::const_reverse_slice_iterator(this->slice_end(slab,row,col));
		}

template<typename T>
inline
typename Array4d<T>::reverse_slice_iterator
Array4d<T>::slice_rend(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type row,
		const typename Array4d<T>::size_type col)
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return typename Array4d<T>::reverse_slice_iterator(this->slice_begin(slab,row,col));
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_slice_iterator
Array4d<T>::slice_rend(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type row,
		const typename Array4d<T>::size_type col) const
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	return typename Array4d<T>::const_reverse_slice_iterator(this->slice_begin(slab,row,col));
		}

//--------------------One dimensional row iterators----------------------

template<typename T>
inline
typename Array4d<T>::row_iterator
Array4d<T>::row_begin(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row   < this->d3_);
	return this->data_[slab][slice][row];
		}

template<typename T>
inline
typename Array4d<T>::const_row_iterator
Array4d<T>::row_begin(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row   < this->d3_);
	return this->data_[slab][slice][row];
		}

template<typename T>
inline
typename Array4d<T>::row_iterator
Array4d<T>::row_end(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row   < this->d3_);
	return this->data_[slab][slice][row] + this->d4_;
		}

template<typename T>
inline
typename Array4d<T>::const_row_iterator
Array4d<T>::row_end(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row   < this->d3_);
	return this->data_[slab][slice][row] + this->d4_;
		}


template<typename T>
inline
typename Array4d<T>::reverse_row_iterator
Array4d<T>::row_rbegin(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row   < this->d3_);
	return typename Array4d<T>::reverse_row_iterator(this->row_end(slab,slice,row));
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_row_iterator
Array4d<T>::row_rbegin(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row   < this->d3_);
	return typename Array4d<T>::const_reverse_row_iterator(this->row_end(slab,slice,row));
		}


template<typename T>
inline
typename Array4d<T>::reverse_row_iterator
Array4d<T>::row_rend(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row   < this->d3_);
	return typename Array4d<T>::reverse_row_iterator(this->row_begin(slab,slice,row));
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_row_iterator
Array4d<T>::row_rend(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row   < this->d3_);
	return typename Array4d<T>::const_reverse_row_iterator(this->row_begin(slab,slice,row));
		}

//--------------------One dimensional col iterators----------------------

template<typename T>
inline
typename Array4d<T>::col_iterator
Array4d<T>::col_begin(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type col)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	return typename Array4d<T>::col_iterator(this->data_[slab][slice][0] + col,this->d4_);
		}

template<typename T>
inline
typename Array4d<T>::const_col_iterator
Array4d<T>::col_begin(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type col) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	return typename Array4d<T>::const_col_iterator(this->data_[slab][slice][0] + col,this->d4_);
		}


template<typename T>
inline
typename Array4d<T>::col_iterator
Array4d<T>::col_end(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type col)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	return ++(typename Array4d<T>::col_iterator(this->data_[slab][slice][this->d3_- 1] + col,this->d4_));
		}

template<typename T>
inline
typename Array4d<T>::const_col_iterator
Array4d<T>::col_end(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type col) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	return ++(typename Array4d<T>::const_col_iterator(this->data_[slab][slice][this->d3_- 1] + col,this->d4_));

		}

template<typename T>
inline
typename Array4d<T>::reverse_col_iterator
Array4d<T>::col_rbegin(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type col)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	return typename Array4d<T>::reverse_col_iterator(this->col_end(slab,slice,col));
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_col_iterator
Array4d<T>::col_rbegin(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type col) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	return typename Array4d<T>::const_reverse_col_iterator(this->col_end(slab,slice,col));
		}


template<typename T>
inline
typename Array4d<T>::reverse_col_iterator
Array4d<T>::col_rend(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type col)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	return typename Array4d<T>::reverse_col_iterator(this->col_begin(slab,slice,col));
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_col_iterator
Array4d<T>::col_rend(const typename Array4d<T>::size_type slab, const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type col) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	return typename Array4d<T>::const_reverse_col_iterator(this->col_begin(slab,slice,col));
		}

//--------------------slab range iterators----------------------

template<typename T>
inline
typename Array4d<T>::slab_range_iterator Array4d<T>::slab_begin(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range){
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.is_valid());
	assert(range.start() >= 0);
	assert(range.stop()  >= 0);
	assert(range.start() < (int)this->d1_);
	assert(range.stop()  < (int)this->d1_);
	return slip::stride_iterator<typename Array4d<T>::slab_iterator>(this->slab_begin(slice,row,col) + range.start(),
			range.stride());
}


template<typename T>
inline
typename Array4d<T>::slab_range_iterator Array4d<T>::slab_end(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range){
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d1_);
	assert(range.stop()  < (int)this->d1_);
	return ++(slip::stride_iterator<typename Array4d<T>::slab_iterator>(this->slab_begin(slice,row,col,range)
			+ range.iterations()));
}


template<typename T>
inline
typename Array4d<T>::const_slab_range_iterator Array4d<T>::slab_begin(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range) const{
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.is_valid());
	assert(range.start() >= 0);
	assert(range.stop()  >= 0);
	assert(range.start() < (int)this->d1_);
	assert(range.stop()  < (int)this->d1_);
	return slip::stride_iterator<typename Array4d<T>::const_slab_iterator>(this->slab_begin(slice,row,col) +
			range.start(),range.stride());

}

template<typename T>
inline
typename Array4d<T>::const_slab_range_iterator Array4d<T>::slab_end(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range) const{
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d1_);
	assert(range.stop()  < (int)this->d1_);
	return ++(slip::stride_iterator<typename Array4d<T>::const_slab_iterator>(this->slab_begin(slice,row,col,range)
			+ range.iterations()));
}

template<typename T>
inline
typename Array4d<T>::reverse_slab_range_iterator Array4d<T>::slab_rbegin(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range){
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d1_);
	return typename Array4d<T>::reverse_slab_range_iterator(this->slab_end(slice,row,col,range));
}


template<typename T>
inline
typename Array4d<T>::reverse_slab_range_iterator Array4d<T>::slab_rend(const Array4d<T>::size_type slice,
		const Array4d<T>::size_type row, const Array4d<T>::size_type col,
		const slip::Range<int>& range){
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d1_);
	return typename Array4d<T>::reverse_slab_range_iterator(this->slab_begin(slice,row,col,range));
}


template<typename T>
inline
typename Array4d<T>::const_reverse_slab_range_iterator Array4d<T>::slab_rbegin(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range) const{
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d1_);
	return typename Array4d<T>::const_reverse_slab_range_iterator(this->slab_end(slice,row,col,range));
}

template<typename T>
inline
typename Array4d<T>::const_reverse_slab_range_iterator Array4d<T>::slab_rend(const typename Array4d<T>::size_type slice,
		const typename Array4d<T>::size_type row,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range) const{
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d1_);
	return typename Array4d<T>::const_reverse_slab_range_iterator(this->slab_begin(slice,row,col,range));
}


//--------------------Constant slice range iterators----------------------

template<typename T>
inline
typename Array4d<T>::slice_range_iterator
Array4d<T>::slice_begin(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.is_valid());
	assert(range.start() >= 0);
	assert(range.stop()  >= 0);
	assert(range.start() < (int)this->d2_);
	assert(range.stop()  < (int)this->d2_);
	return slip::stride_iterator<typename Array4d<T>::slice_iterator>(this->slice_begin(slab,row,col) + range.start(),range.stride());
		}

template<typename T>
inline
typename Array4d<T>::const_slice_range_iterator
Array4d<T>::slice_begin(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.is_valid());
	assert(range.start() >= 0);
	assert(range.stop()  >= 0);
	assert(range.start() < (int)this->d2_);
	assert(range.stop() < (int)this->d2_);
	return slip::stride_iterator<typename Array4d<T>::const_slice_iterator>(this->slice_begin(slab,row,col) + range.start(),range.stride());
		}

template<typename T>
inline
typename Array4d<T>::slice_range_iterator
Array4d<T>::slice_end(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d2_);
	assert(range.stop()  < (int)this->d2_);
	return ++(slip::stride_iterator<typename Array4d<T>::slice_iterator>(this->slice_begin(slab,row,col,range) + range.iterations()));
		}

template<typename T>
inline
typename Array4d<T>::const_slice_range_iterator
Array4d<T>::slice_end(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d2_);
	assert(range.stop()  < (int)this->d2_);
	return ++(slip::stride_iterator<typename Array4d<T>::const_slice_iterator>(this->slice_begin(slab,row,col,range) + range.iterations()));
		}

template<typename T>
inline
typename Array4d<T>::reverse_slice_range_iterator
Array4d<T>::slice_rbegin(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d2_);
	return typename Array4d<T>::reverse_slice_range_iterator(this->slice_end(slab,row,col,range));
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_slice_range_iterator
Array4d<T>::slice_rbegin(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d2_);
	return typename Array4d<T>::const_reverse_slice_range_iterator(this->slice_end(slab,row,col,range));
		}

template<typename T>
inline
typename Array4d<T>::reverse_slice_range_iterator
Array4d<T>::slice_rend(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d2_);
	return typename Array4d<T>::reverse_slice_range_iterator(this->slice_begin(slab,row,col,range));
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_slice_range_iterator
Array4d<T>::slice_rend(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type row, const typename Array4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	assert(slab < this->d1_);
	assert(row < this->d3_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d2_);
	return typename Array4d<T>::const_reverse_slice_range_iterator(this->slice_begin(slab,row,col,range));
		}

//--------------------Constant row range iterators----------------------

template<typename T>
inline
typename Array4d<T>::row_range_iterator
Array4d<T>::row_begin(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice, const typename Array4d<T>::size_type row,const slip::Range<int>& range)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(range.is_valid());
	assert(range.start() >= 0);
	assert(range.stop()  >= 0);
	assert(range.start() < (int)this->d4_);
	assert(range.stop()  < (int)this->d4_);
	return slip::stride_iterator<typename Array4d<T>::row_iterator>(this->row_begin(slab,slice,row) + range.start(),range.stride());
		}

template<typename T>
inline
typename Array4d<T>::const_row_range_iterator
Array4d<T>::row_begin(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type row,
		const slip::Range<int>& range) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(range.is_valid());
	assert(range.start() >= 0);
	assert(range.stop()  >= 0);
	assert(range.start() < (int)this->d4_);
	assert(range.stop() < (int)this->d4_);
	return slip::stride_iterator<typename Array4d<T>::const_row_iterator>(this->row_begin(slab,slice,row) + range.start(),range.stride());
		}

template<typename T>
inline
typename Array4d<T>::row_range_iterator
Array4d<T>::row_end(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type row,
		const slip::Range<int>& range)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(range.start() < (int)this->d4_);
	assert(range.stop()  < (int)this->d4_);
	return ++(slip::stride_iterator<typename Array4d<T>::row_iterator>(this->row_begin(slab,slice,row) + range.start() + range.stride() * range.iterations(),range.stride()));
		}

template<typename T>
inline
typename Array4d<T>::const_row_range_iterator
Array4d<T>::row_end(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type row,
		const slip::Range<int>& range) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(range.start() < (int)this->d4_);
	assert(range.stop()  < (int)this->d4_);
	return ++(slip::stride_iterator<typename Array4d<T>::const_row_iterator>(this->row_begin(slab,slice,row) + range.start() + range.stride() * range.iterations(),range.stride()));
		}

template<typename T>
inline
typename Array4d<T>::reverse_row_range_iterator
Array4d<T>::row_rbegin(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type row,
		const slip::Range<int>& range)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(range.start() < (int)this->d4_);
	return typename Array4d<T>::reverse_row_range_iterator(this->row_end(slab,slice,row,range));
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_row_range_iterator
Array4d<T>::row_rbegin(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type row,
		const slip::Range<int>& range) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(range.start() < (int)this->d4_);
	return typename Array4d<T>::const_reverse_row_range_iterator(this->row_end(slab,slice,row,range));
		}

template<typename T>
inline
typename Array4d<T>::reverse_row_range_iterator
Array4d<T>::row_rend(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type row,
		const slip::Range<int>& range)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(range.start() < (int)this->d4_);
	return typename Array4d<T>::reverse_row_range_iterator(this->row_begin(slab,slice,row,range));
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_row_range_iterator
Array4d<T>::row_rend(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type row,
		const slip::Range<int>& range) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(row < this->d3_);
	assert(range.start() < (int)this->d4_);
	return typename Array4d<T>::const_reverse_row_range_iterator(this->row_begin(slab,slice,row,range));
		}

//--------------------Constant col range iterators----------------------

template<typename T>
inline
typename Array4d<T>::col_range_iterator
Array4d<T>::col_begin(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	assert(range.is_valid());
	assert(range.start() >= 0);
	assert(range.stop()  >= 0);
	assert(range.start() < (int)this->d3_);
	assert(range.stop()  < (int)this->d3_);
	return slip::stride_iterator<typename Array4d<T>::col_iterator>(this->col_begin(slab,slice,col) + range.start(),
			range.stride());
		}

template<typename T>
inline
typename Array4d<T>::const_col_range_iterator
Array4d<T>::col_begin(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	assert(range.is_valid());
	assert(range.start() >= 0);
	assert(range.stop()  >= 0);
	assert(range.start() < (int)this->d3_);
	assert(range.stop()  < (int)this->d3_);
	return slip::stride_iterator<typename Array4d<T>::const_col_iterator>(this->col_begin(slab,slice,col) + range.start(),
			range.stride());
		}

template<typename T>
inline
typename Array4d<T>::col_range_iterator
Array4d<T>::col_end(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	assert(range.is_valid());
	assert(range.start() >= 0);
	assert(range.stop()  >= 0);
	assert(range.start() < (int)this->d3_);
	assert(range.stop()  < (int)this->d3_);
	return ++(slip::stride_iterator<typename Array4d<T>::col_iterator>(this->col_begin(slab,slice,col) + range.start() +
			range.stride() * range.iterations(),range.stride()));
		}

template<typename T>
inline
typename Array4d<T>::const_col_range_iterator
Array4d<T>::col_end(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	assert(range.is_valid());
	assert(range.start() >= 0);
	assert(range.stop()  >= 0);
	assert(range.start() < (int)this->d3_);
	assert(range.stop()  < (int)this->d3_);
	return ++(slip::stride_iterator<typename Array4d<T>::const_col_iterator>(this->col_begin(slab,slice,col) +
			range.start() + range.stride() * range.iterations(),range.stride()));
		}

template<typename T>
inline
typename Array4d<T>::reverse_col_range_iterator
Array4d<T>::col_rbegin(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d3_);
	return typename Array4d<T>::reverse_col_range_iterator(this->col_end(slab,slice,col,range));
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_col_range_iterator
Array4d<T>::col_rbegin(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d3_);
	return typename Array4d<T>::const_reverse_col_range_iterator(this->col_end(slab,slice,col,range));
		}

template<typename T>
inline
typename Array4d<T>::reverse_col_range_iterator
Array4d<T>::col_rend(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range)
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d3_);
	return typename Array4d<T>::reverse_col_range_iterator(this->col_begin(slab,slice,col,range));
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_col_range_iterator
Array4d<T>::col_rend(const typename Array4d<T>::size_type slab,
		const typename Array4d<T>::size_type slice,const typename Array4d<T>::size_type col,
		const slip::Range<int>& range) const
		{
	assert(slab < this->d1_);
	assert(slice < this->d2_);
	assert(col < this->d4_);
	assert(range.start() < (int)this->d3_);
	return typename Array4d<T>::const_reverse_col_range_iterator(this->col_begin(slab,slice,col,range));
		}


//****************************************************************************
//                          Four dimensional iterators
//****************************************************************************

//------------------------ Global iterators------------------------------------

template<typename T>
inline
typename Array4d<T>::iterator4d Array4d<T>::first_front_upper_left()
{
	return typename Array4d<T>::iterator4d(this,Box4d<int>(0,0,0,0,this->d1_-1,this->d2_-1,this->d3_-1,this->d4_-1));
}

template<typename T>
inline
typename Array4d<T>::const_iterator4d Array4d<T>::first_front_upper_left() const
{
	return typename Array4d<T>::const_iterator4d(this,Box4d<int>(0,0,0,0,this->d1_-1,this->d2_-1,this->d3_-1,this->d4_-1));
}


template<typename T>
inline
typename Array4d<T>::iterator4d Array4d<T>::last_back_bottom_right()
{
	DPoint4d<int> dp(this->d1_,this->d2_,this->d3_,this->d4_);
	typename Array4d<T>::iterator4d it = (*this).first_front_upper_left() + dp;
	return it;
}

template<typename T>
inline
typename Array4d<T>::const_iterator4d Array4d<T>::last_back_bottom_right() const
{
	DPoint4d<int> dp(this->d1_,this->d2_,this->d3_,this->d4_);
	typename Array4d<T>::const_iterator4d it = (*this).first_front_upper_left() + dp;
	return it;
}

template<typename T>
inline
typename Array4d<T>::reverse_iterator4d
Array4d<T>::rlast_back_bottom_right()
{
	return typename Array4d<T>::reverse_iterator4d(this->first_front_upper_left());
}

template<typename T>
inline
typename Array4d<T>::const_reverse_iterator4d
Array4d<T>::rlast_back_bottom_right() const
{
	return typename Array4d<T>::const_reverse_iterator4d(this->first_front_upper_left());
}

template<typename T>
inline
typename Array4d<T>::reverse_iterator4d
Array4d<T>::rfirst_front_upper_left()
{
	DPoint4d<int> dp(1,1,1,0);
	return typename Array4d<T>::reverse_iterator4d(this->last_back_bottom_right() - dp);
}

template<typename T>
inline
typename Array4d<T>::const_reverse_iterator4d
Array4d<T>::rfirst_front_upper_left() const
{
	DPoint4d<int> dp(1,1,1,0);
	return typename Array4d<T>::const_reverse_iterator4d(this->last_back_bottom_right() - dp);
}

//------------------------ Box iterators------------------------------------

template<typename T>
inline
typename Array4d<T>::iterator4d Array4d<T>::first_front_upper_left(const Box4d<int>& box)
{
	return typename Array4d<T>::iterator4d(this,box);
}

template<typename T>
inline
typename Array4d<T>::const_iterator4d Array4d<T>::first_front_upper_left(const Box4d<int>& box) const
{
	return typename Array4d<T>::const_iterator4d(this,box);
}


template<typename T>
inline
typename Array4d<T>::iterator4d
Array4d<T>::last_back_bottom_right(const Box4d<int>& box)
{
	DPoint4d<int> dp(box.duration(),box.depth(),box.height(),box.width());
	typename Array4d<T>::iterator4d it = (*this).first_front_upper_left(box) + dp;
	return it;
}

template<typename T>
inline
typename Array4d<T>::const_iterator4d
Array4d<T>::last_back_bottom_right(const Box4d<int>& box) const
{
	DPoint4d<int> dp(box.duration(),box.depth(),box.height(),box.width());
	typename Array4d<T>::const_iterator4d it = (*this).first_front_upper_left(box) + dp;
	return it;
}

template<typename T>
inline
typename Array4d<T>::reverse_iterator4d
Array4d<T>::rlast_back_bottom_right(const Box4d<int>& box)
{
	return typename Array4d<T>::reverse_iterator4d(this->first_front_upper_left(box));
}

template<typename T>
inline
typename Array4d<T>::const_reverse_iterator4d
Array4d<T>::rlast_back_bottom_right(const Box4d<int>& box) const
{
	return typename Array4d<T>::const_reverse_iterator4d(this->first_front_upper_left(box));
}

template<typename T>
inline
typename Array4d<T>::reverse_iterator4d
Array4d<T>::rfirst_front_upper_left(const Box4d<int>& box)
{
	DPoint4d<int> dp(1,1,1,0);
	return typename Array4d<T>::reverse_iterator4d(this->last_back_bottom_right(box) - dp);
}

template<typename T>
inline
typename Array4d<T>::const_reverse_iterator4d
Array4d<T>::rfirst_front_upper_left(const Box4d<int>& box) const
{
	DPoint4d<int> dp(1,1,1,0);
	return typename Array4d<T>::const_reverse_iterator4d(this->last_back_bottom_right(box) - dp);
}

//------------------------ Range iterators------------------------------------

template<typename T>
inline
typename Array4d<T>::iterator4d_range
Array4d<T>::first_front_upper_left(const typename Array4d<T>::range& slab_range,
		const typename Array4d<T>::range& slice_range,
		const typename Array4d<T>::range& row_range,
		const typename Array4d<T>::range& col_range)
		{
	return typename Array4d<T>::iterator4d_range(this,slab_range,slice_range,row_range,col_range);
		}

template<typename T>
inline
typename Array4d<T>::iterator4d_range
Array4d<T>::last_back_bottom_right(const typename Array4d<T>::range& slab_range,
		const typename Array4d<T>::range& slice_range,
		const typename Array4d<T>::range& row_range,
		const typename Array4d<T>::range& col_range)
		{
	DPoint4d<int> dp(slab_range.iterations()+1,slice_range.iterations()+1,row_range.iterations()+1,
			col_range.iterations()+1);
	return  typename Array4d<T>::iterator4d_range((*this).first_front_upper_left(slab_range,slice_range,
			row_range,col_range) + dp);
		}


template<typename T>
inline
typename Array4d<T>::const_iterator4d_range
Array4d<T>::first_front_upper_left(const typename Array4d<T>::range& slab_range,
		const typename Array4d<T>::range& slice_range,
		const typename Array4d<T>::range& row_range,
		const typename Array4d<T>::range& col_range) const
		{
	return typename Array4d<T>::const_iterator4d_range(this,slab_range,slice_range,row_range,col_range);
		}


template<typename T>
inline
typename Array4d<T>::const_iterator4d_range
Array4d<T>::last_back_bottom_right(const typename Array4d<T>::range& slab_range,
		const typename Array4d<T>::range& slice_range,
		const typename Array4d<T>::range& row_range,
		const typename Array4d<T>::range& col_range) const
		{
	DPoint4d<int> dp(slab_range.iterations()+1,slice_range.iterations()+1,row_range.iterations()+1,
			col_range.iterations()+1);
	return  typename Array4d<T>::const_iterator4d_range((*this).first_front_upper_left(slab_range,slice_range,
			row_range,col_range) + dp);
		}

template<typename T>
inline
typename Array4d<T>::reverse_iterator4d_range
Array4d<T>::rfirst_front_upper_left(const typename Array4d<T>::range& slab_range,
		const typename Array4d<T>::range& slice_range,
		const typename Array4d<T>::range& row_range,
		const typename Array4d<T>::range& col_range)
		{
	DPoint4d<int> dp(1,1,1,0);
	return typename Array4d<T>::reverse_iterator4d_range(this->last_back_bottom_right(
			slab_range,slice_range,row_range,col_range) - dp);
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_iterator4d_range
Array4d<T>::rfirst_front_upper_left(const typename Array4d<T>::range& slab_range,
		const typename Array4d<T>::range& slice_range,
		const typename Array4d<T>::range& row_range,
		const typename Array4d<T>::range& col_range) const
		{
	DPoint4d<int> dp(1,1,1,0);
	return typename Array4d<T>::const_reverse_iterator4d_range(typename Array4d<T>::const_iterator4d_range(
			this->last_back_bottom_right(slab_range,slice_range,row_range,col_range)- dp));
		}

template<typename T>
inline
typename Array4d<T>::reverse_iterator4d_range
Array4d<T>::rlast_back_bottom_right(const typename Array4d<T>::range& slab_range,
		const typename Array4d<T>::range& slice_range,
		const typename Array4d<T>::range& row_range,
		const typename Array4d<T>::range& col_range)
		{
	return typename Array4d<T>::reverse_iterator4d_range(this->first_front_upper_left(slab_range,slice_range,
			row_range,col_range));
		}

template<typename T>
inline
typename Array4d<T>::const_reverse_iterator4d_range
Array4d<T>::rlast_back_bottom_right(const typename Array4d<T>::range& slab_range,
		const typename Array4d<T>::range& slice_range,
		const typename Array4d<T>::range& row_range,
		const typename Array4d<T>::range& col_range) const
		{
	return typename Array4d<T>::const_reverse_iterator4d_range(this->first_front_upper_left(slab_range,slice_range,
			row_range,col_range));
		}


///////////////////////////////////////////


////////////////////////////////////////////
// i/o operators
////////////////////////////////////////////
/** \name input/output operators */
/* @{ */
	template <typename T>
inline
std::ostream& operator<<(std::ostream & out, const Array4d<T>& a)
	{
		for(std::size_t i = 0; i < a.d1_; ++i)
		{
			for(std::size_t j = 0; j < a.d2_; ++j)
			{
				for(std::size_t k = 0; k < a.d3_; ++k)
				{
					for(std::size_t l = 0; l < a.d4_; ++l)
					{
						out<<a.data_[i][j][k][l]<<" ";
					}
					out<<std::endl;
				}
				out<<std::endl;
			}
			out<<"----" << std::endl << std::endl;
		}
		out<<std::endl;
		return out;
	}
	/* @} */
	///////////////////////////////////////////


	////////////////////////////////////////////
	// Elements access operators
	////////////////////////////////////////////
	template<typename T>
	inline
	T*** Array4d<T>::operator[](const typename Array4d<T>::size_type l)
	{
		assert(this->data_ != 0);
		assert(l < this->d1_);
		return this->data_[l];
	}

	template<typename T>
	inline
	const T** const* Array4d<T>::operator[](const typename Array4d<T>::size_type l) const
	{
		assert(this->data_ != 0);
		assert(l < this->d1_);
		return const_cast<const T** const*>(this->data_[l]);
	}



	template<typename T>
	inline
	typename Array4d<T>::reference
	Array4d<T>::operator()(const typename Array4d<T>::size_type l, const typename Array4d<T>::size_type k,
			const typename Array4d<T>::size_type i,
			const typename Array4d<T>::size_type j)
	{
		assert(this->data_ != 0);
		assert(l < this->d1_);
		assert(k < this->d2_);
		assert(i < this->d3_);
		assert(j < this->d4_);
		return this->data_[l][k][i][j];
	}

	template<typename T>
	inline
	typename Array4d<T>::const_reference
	Array4d<T>::operator()(const typename Array4d<T>::size_type l, const typename Array4d<T>::size_type k,
			const typename Array4d<T>::size_type i,
			const typename Array4d<T>::size_type j) const
	{
		assert(this->data_ != 0);
		assert(l < this->d1_);
		assert(k < this->d2_);
		assert(i < this->d3_);
		assert(j < this->d4_);
		return this->data_[l][k][i][j];
	}
	///////////////////////////////////////////

	template<typename T>
	inline
	std::string
	Array4d<T>::name() const {return "Array4d";}


	template<typename T>
	inline
	typename Array4d<T>::size_type
	Array4d<T>::dim1() const {return this->d1_;}

	template<typename T>
	inline
	typename Array4d<T>::size_type
	Array4d<T>::slabs() const {return this->dim1();}

	template<typename T>
	inline
	typename Array4d<T>::size_type
	Array4d<T>::dim2() const {return this->d2_;}

	template<typename T>
	inline
	typename Array4d<T>::size_type
	Array4d<T>::slices() const {return this->dim2();}

	template<typename T>
	inline
	typename Array4d<T>::size_type
	Array4d<T>::dim3() const {return this->d3_;}

	template<typename T>
	inline
	typename Array4d<T>::size_type
	Array4d<T>::rows() const {return this->dim3();}

	template<typename T>
	inline
	typename Array4d<T>::size_type
	Array4d<T>::dim4() const {return this->d4_;}

	template<typename T>
	inline
	typename Array4d<T>::size_type
	Array4d<T>::cols() const {return this->dim4();}

	template<typename T>
	inline
	typename Array4d<T>::size_type
	Array4d<T>::columns() const {return this->dim4();}


	template<typename T>
	inline
	typename Array4d<T>::size_type
	Array4d<T>::size() const {return this->size_;}

	template<typename T>
	inline
	typename Array4d<T>::size_type
	Array4d<T>::max_size() const
	{
		return typename Array4d<T>::size_type(-1)/sizeof(T);
	}

	template<typename T>
	inline
	bool Array4d<T>::empty()const {return this->size_ == 0;}

	template<typename T>
	inline
	void Array4d<T>::swap(Array4d<T>& M)
	{
		assert(this->d1_ == M.d1_);
		assert(this->d2_ == M.d2_);
		assert(this->d3_ == M.d3_);
		assert(this->d4_ == M.d4_);
		std::swap_ranges(this->begin(),this->end(),M.begin());
	}


	////////////////////////////////////////////
	// comparison operators
	////////////////////////////////////////////
	/** \name EqualityComparable functions */
	/* @{ */
	template<typename T>
	inline
	bool operator==(const Array4d<T>& x,
			const Array4d<T>& y)
			{
		return ( x.size() == y.size()
				&& std::equal(x.begin(),x.end(),y.begin()));
			}

	template<typename T>
	inline
	bool operator!=(const Array4d<T>& x,
			const Array4d<T>& y)
			{
		return !(x == y);
			}
	/* @} */
	/** \name LessThanComparable functions */
	/* @{ */
	template<typename T>
	inline
	bool operator<(const Array4d<T>& x,
			const Array4d<T>& y)
	{
		return std::lexicographical_compare(x.begin(), x.end(),
				y.begin(), y.end());
	}


	template<typename T>
	inline
	bool operator>(const Array4d<T>& x,
			const Array4d<T>& y)
	{
		return (y < x);
	}

	template<typename T>
	inline
	bool operator<=(const Array4d<T>& x,
			const Array4d<T>& y)
			{
		return !(y < x);
			}

	template<typename T>
	inline
	bool operator>=(const Array4d<T>& x,
			const Array4d<T>& y)
			{
		return !(x < y);
			}
	/* @} */

	////////////////////////////////////////////


	////////////////////////////////////////////
	// private methods
	////////////////////////////////////////////

	template<typename T>
	inline
	void Array4d<T>::allocate()
	{
		if( this->d1_ != 0 && this->d2_ != 0 && this->d3_ != 0 && this->d4_ != 0)
		{
			this->data_ = new T***[this->d1_];
			this->data_[0] = new T**[this->d1_ * this->d2_];
			this->data_[0][0] = new T*[this->d1_ * this->d2_ * this->d3_];
			this->data_[0][0][0] = new T[this->d1_ * this->d2_ * this->d3_ * this->d4_];

			for(std::size_t l = 0; l < this->d1_; ++l)
			{
				//if(l != 0) -> d1_ tests are worst than one identity affectation
				this->data_[l] = this->data_[0] + l * this->d2_;
				for(std::size_t k = 0; k < this->d2_; ++k){
					//if(l != 0 || k != 0) -> d1_*d2_ tests are worst than one identity affectation
					this->data_[l][k] = this->data_[0][0] + l * this->d2_ *  this->d3_ + k * this->d3_;
					for(std::size_t i = 0; i < this->d3_; ++i){
						//if(l != 0 || k != 0 || i != 0) -> d1_*d2_*d3_ tests are worst than one identity affectation
						this->data_[l][k][i] = this->data_[0][0][0] + l * this->d2_ *  this->d3_ * this->d4_
								+ k * this->d3_ * this->d4_ + i * this->d4_;
					}
				}
			}
		}
		else
		{
			this->data_ = 0;
		}
	}

	template<typename T>
	inline
	void Array4d<T>::desallocate()
	{
		if(this->data_ != 0)
		{
			delete[] (this->data_[0][0][0]);
			delete[] (this->data_[0][0]);
			delete[] (this->data_[0]);
			delete[] (this->data_);
		}
	}

	template<typename T>
	inline
	void Array4d<T>::copy_attributes(const Array4d<T>& rhs)
	{
		this->d1_ = rhs.d1_;
		this->d2_ = rhs.d2_;
		this->d3_ = rhs.d3_;
		this->d4_ = rhs.d4_;
		this->size_ = rhs.size_;
	}


	////////////////////////////////////////////

}//slip::

#endif //SLIP_ARRAY4D_HH
