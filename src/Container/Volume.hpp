/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Volume.hpp
 * 
 * \brief Provides a class to manipulate 3d dynamic and generic containers.
 * 
 */
#ifndef SLIP_VOLUME_HPP
#define SLIP_VOLUME_HPP

#include <iostream>
#include <iomanip>
#include <fstream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <algorithm>
#include <cmath>
#include <vector>
#include <string>
#include <cstddef>
#include <stdexcept>

#include "Matrix3d.hpp"
#include "stride_iterator.hpp"
#include "apply.hpp"
#include "io_tools.hpp"
#include "iterator3d_plane.hpp"
#include "iterator3d_box.hpp"
#include "iterator3d_range.hpp"


#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>


namespace slip
{

  template <typename T>
  class Volume;

  template <typename T>
  class Matrix3d;

  template <typename T>
  std::ostream& operator<<(std::ostream & out, const slip::Volume<T>& a);

  template<typename T>
  bool operator==(const slip::Volume<T>& x, 
		  const slip::Volume<T>& y);

  template<typename T>
  bool operator!=(const slip::Volume<T>& x, 
		  const slip::Volume<T>& y);

  template<typename T>
  bool operator<(const slip::Volume<T>& x, 
		 const slip::Volume<T>& y);

  template<typename T>
  bool operator>(const slip::Volume<T>& x, 
		 const slip::Volume<T>& y);

  template<typename T>
  bool operator<=(const slip::Volume<T>& x, 
		  const slip::Volume<T>& y);

  template<typename T>
  bool operator>=(const slip::Volume<T>& x, 
		  const slip::Volume<T>& y);

  /*! \class Volume
  **  \ingroup Containers Containers3d
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.2
  ** \date 2014/03/14
  ** \since 1.0.0
  ** \brief Numerical %volume class 
  ** This is a two-dimensional dynamic and generic container.
  ** This container statisfies the BidirectionnalContainer concepts of the STL.
  ** It is also an 3d extension of the RandomAccessContainer concept. That is
  ** to say the bracket element access is replaced by the triple bracket 
  ** element access.
  ** Data are stored using a %Matrix3d class.
  ** It extends the interface of Matrix3d adding image read/write operations.
  ** These operations are done using the ImageMagick library.
  ** \param T Type of object in the Volume 
  **
  ** \image html iterator3d_conventions.jpg "axis and notation conventions"
  ** \image latex iterator3d_conventions.eps "axis and notation conventions" width=5cm
  **
  */

  template <typename T>
  class Volume
  {
  public :

    typedef T value_type;
    typedef Volume<T> self;

    typedef value_type* pointer;
    typedef const value_type* const_pointer;
    typedef value_type& reference;
    typedef const value_type& const_reference;

    typedef ptrdiff_t difference_type;
    typedef std::size_t size_type;

    typedef pointer iterator;
    typedef const_pointer const_iterator;
  
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;


    //slice, row and col iterator
    typedef slip::stride_iterator<pointer> slice_iterator;
    typedef slip::stride_iterator<const_pointer> const_slice_iterator;
    typedef pointer row_iterator;
    typedef const_pointer const_row_iterator;
    typedef slip::stride_iterator<pointer> col_iterator;
    typedef slip::stride_iterator<const_pointer> const_col_iterator;
    typedef slip::stride_iterator<slice_iterator> slice_range_iterator;
    typedef slip::stride_iterator<const_slice_iterator> const_slice_range_iterator;
    typedef slip::stride_iterator<pointer> row_range_iterator;
    typedef slip::stride_iterator<const_pointer> const_row_range_iterator;
    typedef slip::stride_iterator<col_iterator> col_range_iterator;
    typedef slip::stride_iterator<const_col_iterator> const_col_range_iterator;
    typedef std::reverse_iterator<slice_range_iterator> reverse_slice_range_iterator;
    typedef std::reverse_iterator<const_slice_range_iterator> const_reverse_slice_range_iterator;
    typedef std::reverse_iterator<slice_iterator> reverse_slice_iterator;
    typedef std::reverse_iterator<const_slice_iterator> const_reverse_slice_iterator;
    typedef std::reverse_iterator<iterator> reverse_row_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
    typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
    typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;

    typedef std::reverse_iterator<row_range_iterator> reverse_row_range_iterator;
    typedef std::reverse_iterator<const_row_range_iterator> const_reverse_row_range_iterator;
    typedef std::reverse_iterator<col_range_iterator> reverse_col_range_iterator;
    typedef std::reverse_iterator<const_col_range_iterator> const_reverse_col_range_iterator;
  
#ifdef ALL_PLANE_ITERATOR3D
    //generic 1d iterator
    typedef slip::stride_iterator<pointer> iterator1d;
    typedef slip::stride_iterator<const_pointer> const_iterator1d;
    
    typedef std::reverse_iterator<iterator1d> reverse_iterator1d;
    typedef std::reverse_iterator<const_iterator1d> const_reverse_iterator1d;
#endif //ALL_PLANE_ITERATOR3D
  
    //iterator 2d
    typedef typename slip::Array3d<T>::iterator2d iterator2d;
    typedef typename slip::Array3d<T>::const_iterator2d const_iterator2d;
    typedef std::reverse_iterator<iterator2d> reverse_iterator2d;
    typedef std::reverse_iterator<const_iterator2d> const_reverse_iterator2d;
  
    //iterator 3d
    typedef typename slip::Array3d<T>::iterator3d iterator3d;
    typedef typename slip::Array3d<T>::const_iterator3d const_iterator3d;
    typedef typename slip::Array3d<T>::iterator3d_range iterator3d_range;
    typedef typename slip::Array3d<T>::const_iterator3d_range const_iterator3d_range;
  
    typedef std::reverse_iterator<iterator3d> reverse_iterator3d;
    typedef std::reverse_iterator<const_iterator3d> const_reverse_iterator3d;
    typedef std::reverse_iterator<iterator3d_range> reverse_iterator3d_range;
    typedef std::reverse_iterator<const_iterator3d_range> const_reverse_iterator3d_range;
  
    //default iterator of the container
    typedef iterator3d default_iterator;
    typedef const_iterator3d const_default_iterator;
   
    static const std::size_t DIM = 3;
  public:
    /**
     ** \name Constructors & Destructors
     */
    /*@{*/
  
    /*!
    ** \brief Constructs a %Volume.
    */
    Volume();

    /*!
    ** \brief Constructs a %Volume.
    ** \param d1 first dimension of the %Volume
    ** \param d2 second dimension of the %Volume
    ** \param d3 third dimension of the %Volume
    ** 
    ** \par The %Volume is initialized by the default value of T. 
    */
    Volume(const std::size_t d1,
	   const std::size_t d2,
	   const std::size_t d3);

    /*!
    ** \brief Constructs a %Volume initialized by the scalar value \a val.
    ** \param d1 first dimension of the %Volume
    ** \param d2 second dimension of the %Volume
    ** \param d3 third dimension of the %Volume
    ** \param val initialization value of the elements 
    */
    Volume(const std::size_t d1,
	   const std::size_t d2,
	   const std::size_t d3,
	   const T& val);
    /*!
    ** \brief Constructs a %Volume initialized by an array \a val.
    ** \param d1 first dimension of the %Volume
    ** \param d2 second dimension of the %Volume
    ** \param d3 third dimension of the %Volume
    ** \param val initialization array value of the elements 
    */  
    Volume(const std::size_t d1,
	   const std::size_t d2,
	   const std::size_t d3,
	   const T* val);

    /**
     **  \brief  Contructs a %Volume from a range.
     ** \param d1 first dimension of the %Volume
     ** \param d2 second dimension of the %Volume
     ** \param d3 third dimension of the %Volume
 
     **  \param  first  An input iterator.
     **  \param  last  An input iterator.
     **
     ** Create a %Volume consisting of copies of the elements from
     ** [first,last).
     */
    template<typename InputIterator>
    Volume(const size_type d1,
	   const size_type d2,
	   const size_type d3,
	   InputIterator first,
	   InputIterator last):
      matrix_(new slip::Matrix3d<T>(d1,d2,d3,first,last))
    {}
  
    /*!
    ** \brief Constructs a copy of the Volume \a rhs
    */
    Volume(const Volume<T>& rhs);
  

    /*!
    ** \brief Destructor of the Volume
    */
    ~Volume();
 

    /*@} End Constructors */

 
    /*!
    ** \brief Resizes a %Volume.
    ** \param d1 new first dimension
    ** \param d2 new second dimension
    ** \param d3 new third dimension
 
    ** \param val new value for all the elements
    */ 
    void resize(std::size_t d1,
		std::size_t d2,
		std::size_t d3,
		const T& val = T()); 

    /**
     ** \name One dimensional global iterators
     */
    /*@{*/

    //****************************************************************************
    //                          One dimensional iterators
    //****************************************************************************



    //----------------------Global iterators------------------------------

    /*!
    **  \brief Returns a read-only (constant) iterator that points to the
    **  first element in the %Volume.  Iteration is done in ordinary
    **  element order.
    ** \return const begin iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> const A1(10,9,5);
    **  slip::Volume<double> const A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.begin(),A1.end(),
    **                A2.begin(),S.begin(),
    **                std::plus<double>());
    ** \endcode
    */
    const_iterator begin() const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element in the %Volume.  Iteration is done in ordinary
    **  element order.
    ** \return begin iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> A1(10,9,5);
    **  slip::Volume<double> A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.begin(),A1.end(),
    **                A2.begin(),S.begin(),
    **                std::plus<double>());
    ** \endcode
    */
    iterator begin();

    /*!
    **  \brief Returns a read/write iterator that points one past the last
    **  element in the %Volume.  Iteration is done in ordinary
    **  element order.
    ** \return end iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> A1(10,9,5);
    **  slip::Volume<double> A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.begin(),A1.end(),
    **                A2.begin(),S.begin(),
    **                std::plus<double>());
    ** \endcode
    */
     iterator end();
 
    /*!
    **  \brief Returns a read-only (constant) iterator that points one past
    **  the last element in the %Volume.  Iteration is done in
    **  ordinary element order.
    ** \return const end iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> const A1(10,9,5);
    **  slip::Volume<double> const A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.begin(),A1.end(),
    **                A2.begin(),S.begin(),
    **                std::plus<double>());
    ** \endcode
    */
    const_iterator end() const;
  
    /*!
    **  \brief Returns a read/write reverse iterator that points to the
    **  last element in the %Volume. Iteration is done in reverse
    **  element order.
    ** \return reverse begin iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> A1(10,9,5);
    **  slip::Volume<double> A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.rbegin(),A1.rend(),
    **                A2.rbegin(),S.rbegin(),
    **                std::plus<double>());
    ** \endcode
    */
    reverse_iterator rbegin();
  
    /*!
    **  \brief Returns a read-only (constant) reverse iterator that points
    **  to the last element in the %Volume.  Iteration is done in
    **  reverse element order.
    ** \return const reverse begin iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> const A1(10,9,5);
    **  slip::Volume<double> const A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.rbegin(),A1.rend(),
    **                A2.rbegin(),S.rbegin(),
    **                std::plus<double>());
    ** \endcode
    */
    const_reverse_iterator rbegin() const;

    /*!
    **  \brief Returns a read/write reverse iterator that points to one
    **  before the first element in the %Volume.  Iteration is done
    **  in reverse element order.
    **  \return reverse end iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> A1(10,9,5);
    **  slip::Volume<double> A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.rbegin(),A1.rend(),
    **                A2.rbegin(),S.rbegin(),
    **                std::plus<double>());
    ** \endcode
    */
    reverse_iterator rend();

    /*!
    **  \brief Returns a read-only (constant) reverse iterator that points
    **  to one before the first element in the %Volume.  Iteration
    **  is done in reverse element order.
    **  \return const reverse end iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> const A1(10,9,5);
    **  slip::Volume<double> const A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.rbegin(),A1.rend(),
    **                A2.rbegin(),S.rbegin(),
    **                std::plus<double>());
    ** \endcode
    */
    const_reverse_iterator rend() const;

    /*@} End One dimensional global iterators */
    /**
     ** \name One dimensional slice iterators
     */
    /*@{*/
    //--------------------One dimensional slice iterators----------------------
 
  
    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the line (row,col) threw the slices in the %Volume.  
    **	Iteration is done in ordinary element order (increasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    ** 	\return slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Volume<double> A1(10,9,5);
    **  slip::Volume<double> A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 in the first slice of S
    **  std::transform(A1.begin_slice(0),A1.end_slice(0),
    **                A2.begin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    slice_iterator slice_begin(const size_type row, const size_type col);

    /*!
    **  \brief Returns a read-only (constant) iterator that points to the first
    **  element of the line (row,col) threw the slices in the %Volume.  
    **	Iteration is done in ordinary element order (increasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return const_slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Volume<double> const A1(10,9,5);
    **  slip::Volume<double> const A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 in the first slice of S
    **  std::transform(A1.begin_slice(0),A1.end_slice(0),
    **                A2.begin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    const_slice_iterator slice_begin(const size_type row, const size_type col) const;
 
    /*!
    **  \brief Returns a read/write iterator that points to the one past the end
    **  element of the line (row,col) threw the slices in the %Volume.  
    **	Iteration is done in ordinary element order (increasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Volume<double> A1(10,9,5);
    **  slip::Volume<double> A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 in the first slice of S
    **  std::transform(A1.begin_slice(0),A1.end_slice(0),
    **                A2.begin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    slice_iterator slice_end(const size_type row, const size_type col);
 
    /*!
    **  \brief Returns a read-only (constant) iterator that points to the one past the end
    **  element of the line (row,col) threw the slices in the %Volume.  
    **	Iteration is done in ordinary element order (increasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return const_slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Volume<double> const A1(10,9,5);
    **  slip::Volume<double> const A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 in the first slice of S
    **  std::transform(A1.begin_slice(0),A1.end_slice(0),
    **                A2.begin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    const_slice_iterator slice_end(const size_type row, const size_type col) const;
 

    /*!
    **  \brief Returns a read/write iterator that points to the
    **  last element of the line (row,col) threw the slices in the %Volume.
    ** 	Iteration is done in reverse element order (decreasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    ** 	\return reverse_slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Volume<double> A1(10,9,5);
    **  slip::Volume<double> A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 
    **  in the first slice of S in reverse order
    **  std::transform(A1.rbegin_slice(0),A1.rend_slice(0),
    **                A2.rbegin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    reverse_slice_iterator slice_rbegin(const size_type row, const size_type col);

    /*!
    **  \brief Returns a read-only (constant) iterator that points to the
    **  last element of the line (row,col) threw the slices in the %Volume.
    **	Iteration is done in reverse element order (decreasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return const_reverse_slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Volume<double> const A1(10,9,5);
    **  slip::Volume<double> const A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 
    **  in the first slice of S in reverse order
    **  std::transform(A1.rbegin_slice(0),A1.rend_slice(0),
    **                A2.rbegin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    const_reverse_slice_iterator slice_rbegin(const size_type row, const size_type col) const;

    /*!
    **  \brief Returns a read/write iterator that points to the
    **  one before the first element of the line (row,col) threw the slices in the %Volume.
    **  Iteration is done in reverse element order (decreasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **  \return reverse_slice_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Volume<double> A1(10,9,5);
    **  slip::Volume<double> A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 
    **  in the first slice of S in reverse order
    **  std::transform(A1.rbegin_slice(0),A1.rend_slice(0),
    **                A2.rbegin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    reverse_slice_iterator slice_rend(const size_type row, const size_type col);
 
  
    /*!
    **  \brief Returns a read (constant) iterator that points to the
    **  one before the first element of the line (row,col) threw the slices in the %Volume.
    **  Iteration is done in reverse element order (decreasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return const_reverse_slice_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Volume<double> const A1(10,9,5);
    **  slip::Volume<double> const A2(10,9,5);
    **  slip::Volume<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 
    **  in the first slice of S in reverse order
    **  std::transform(A1.rbegin_slice(0),A1.rend_slice(0),
    **                A2.rbegin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    const_reverse_slice_iterator slice_rend(const size_type row, const size_type col) const;

    /*@} End One dimensional slice iterators */

    /**
     ** \name One dimensional row iterators
     */
    /*@{*/
    //-------------------row iterators----------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of the slice \a slice in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row.
    **  \return begin row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    row_iterator row_begin(const size_type slice,
			   const size_type row);

    /*!
    **  \brief Returns a read_only iterator that points to the first
    **  element of the row \a row of the slice \a slice in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row. 
    **  \return begin const_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_row_iterator row_begin(const size_type slice,
				 const size_type row) const;

 
    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the row \a row of the slice \a slice in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row.
    **  \return end row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    row_iterator row_end(const size_type slice,
			 const size_type row);


    /*!
    **  \brief Returns a read_only iterator that points to the past-the-end
    **  element of the row \a row of the slice \a slice in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row. 
    **  \return end const_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_row_iterator row_end(const size_type slice,
			       const size_type row) const;


    /*!
    **  \brief Returns a read/write reverse iterator that points to the last
    **  element of the row \a row of the slice \a slice in the %Volume.  
    **  Iteration is done in reverse element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row.
    **  \return begin reverse_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    reverse_row_iterator row_rbegin(const size_type slice,
				    const size_type row);

    /*!
    **  \brief Returns a read_only reverse iterator that points to the last
    **  element of the row \a row of the slice \a slice in the %Volume.  
    **  Iteration is done in reverse element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row. 
    **  \return begin const_reverse_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_reverse_row_iterator row_rbegin(const size_type slice,
					  const size_type row) const;

 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the row \a row of the slice \a slice in the %Volume.  
    **  Iteration is done in reverse element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row.
    **  \return end reverse_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    reverse_row_iterator row_rend(const size_type slice,
				  const size_type row);


    /*!
    **  \brief Returns a read_only reverse iterator that points to the first
    **  element of the row \a row of the slice \a slice in the %Volume.  
    **  Iteration is done in reverse element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row. 
    **  \return end const_reverse_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_reverse_row_iterator row_rend(const size_type slice,
					const size_type row) const;

    /*@} End One dimensional row iterators */
    /**
     ** \name One dimensional col iterators
     */
    /*@{*/
    //-------------------col iterators----------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the column \a column of the slice \a slice in the %Volume.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return begin col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    col_iterator col_begin(const size_type slice,
			   const size_type col);


    /*!
    **  \brief Returns a read_only iterator that points to the first
    **  element of the column \a column of the slice \a slice in the %Volume.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return begin const_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_col_iterator col_begin(const size_type slice,
				 const size_type col) const;

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the column \a column of the slice \a slice in the %Volume.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return end col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    col_iterator col_end(const size_type slice,
			 const size_type col);


    /*!
    **  \brief Returns a read_only iterator that points to the past-the-end
    **  element of the column \a column of the slice \a slice in the %Volume.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return end const_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_col_iterator col_end(const size_type slice,
			       const size_type col) const;


    /*!
    **  \brief Returns a read/write reverse iterator that points to the last
    **  element of the column \a column of the slice \a slice in the %Volume.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return begin reverse_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    reverse_col_iterator col_rbegin(const size_type slice,
				    const size_type col);


    /*!
    **  \brief Returns a read_only reverse iterator that points to the last
    **  element of the column \a column of the slice \a slice in the %Volume.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return begin const_reverse_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_reverse_col_iterator col_rbegin(const size_type slice,
					  const size_type col) const;

    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the column \a column of the slice \a slice in the %Volume.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return end reverse_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    reverse_col_iterator col_rend(const size_type slice,
				  const size_type col);


    /*!
    **  \brief Returns a read_only reverse iterator that points to the first
    **  element of the column \a column of the slice \a slice in the %Volume.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return end const_reverse_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_reverse_col_iterator col_rend(const size_type slice,
					const size_type col) const;

    /*@} End One dimensional col iterators */


   /**
     ** \name One dimensional slice range iterators
     */
    /*@{*/
   //------------------------slice range iterators -----------------------

    /*!
    ** \brief Returns a read/write iterator that points to the first
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Volume.
    ** Iteration is done in ordinary element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return begin slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Volume.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> A1(8,8,8);
    **  slip::Volume<double> A2(4,8,8);
    **  slip::Range<int> range(0,A1.dim1()-1,2);
    ** //copy the the elements of the line (0,0) of A1 iterated according to the
    ** //range in the line (1,1) of A2
    ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),A2.slice_begin(1,1));
    ** \endcode
    */
    slice_range_iterator slice_begin(const size_type row,const size_type col,
				     const slip::Range<int>& range);


    /*!
    **  \brief Returns a read/write iterator that points one past the end
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Volume.
    ** Iteration is done in ordinary element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return end slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Volume.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> A1(8,8,8);
    **  slip::Volume<double> A2(4,8,8);
    **  slip::Range<int> range(0,A1.dim1()-1,2);
    ** //copy the the elements of the line (0,0) of A1 iterated according to the
    ** //range in the line (1,1) of A2
    ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),A2.slice_begin(1,1));
    ** \endcode
    */
    slice_range_iterator slice_end(const size_type row,const size_type col,
				   const slip::Range<int>& range);


    /*!
    ** \brief Returns a read only (constant) iterator that points to the first
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Volume.
    ** Iteration is done in ordinary element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return begin const_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Volume.
    ** \par Example:
    ** \code
    **  
    **  slip::Volume<double> const A1(M); //M is an already existing %Volume
    **  slip::Range<int> range(0,A1.dim1()-1,2);
    ** //display the elements of the line (0,0) of A1 iterated according to the range
    ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),
    ** std::ostream_iterator<double>(std::cout," "));
    ** \endcode
    */
    const_slice_range_iterator slice_begin(const size_type row,const size_type col,
					   const slip::Range<int>& range) const;

    /*!
    ** \brief Returns a read_only iterator that points one past the last
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Volume.
    ** Iteration is done in ordinary element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return end const_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Volume.
    ** \par Example:
    ** \code
    **  
    **  slip::Volume<double> const A1(M); //M is an already existing %Volume
    **  slip::Range<int> range(0,A1.dim1()-1,2);
    ** //display the elements of the line (0,0) of A1 iterated according to the range
    ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),
    ** std::ostream_iterator<double>(std::cout," "));
    ** \endcode
    */
    const_slice_range_iterator slice_end(const size_type row,const size_type col,
					 const slip::Range<int>& range) const;


    /*!
    ** \brief Returns a read/write iterator that points to the last
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Volume.
    ** Iteration is done in the reverse element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return begin reverse_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Volume.
    */
    reverse_slice_range_iterator slice_rbegin(const size_type row,const size_type col,
					      const slip::Range<int>& range);


    /*!
    **  \brief Returns a read/write iterator that points to the first
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Volume.
    ** Iteration is done in reverse element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return end reverse_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Volume.
    */
    reverse_slice_range_iterator slice_rend(const size_type row,const size_type col,
					   const slip::Range<int>& range);


    /*!
    ** \brief Returns a read only (constant) iterator that points to the last
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Volume.
    ** Iteration is done in reverse element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return begin const_reverse_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Volume.
    */
    const_reverse_slice_range_iterator slice_rbegin(const size_type row,const size_type col,
						   const slip::Range<int>& range) const;

    /*!
    ** \brief Returns a read_only iterator that points one past the lastto the first
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Volume.
    ** Iteration is done in reverse element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return end const_reverse_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Volume.
    */
    const_reverse_slice_range_iterator slice_rend(const size_type row,const size_type col,
						 const slip::Range<int>& range) const;

 
    /*@} End One dimensional slice range iterators */

    /**
     ** \name One dimensional row range iterators
     */
    /*@{*/
   //------------------------row range iterators -----------------------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the %Range \a range of the row \a row in the %Volume.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return begin row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre row must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> A1(8,8,8);
    **  slip::Volume<double> A2(8,4,5);
    **  slip::Range<int> range(0,A1.dim3()-1,2);
    ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
    ** //range in the row 1 of the slice 1 of A2
    ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
    ** \endcode
    */
    row_range_iterator row_begin(const size_type slice,const size_type row,
				 const slip::Range<int>& range);

    /*!
    **  \brief Returns a read/write iterator that points one past the end
    **  element of the %Range \a range of the row \a row in the %Volume.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return end row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre row must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> A1(8,8,8);
    **  slip::Volume<double> A2(8,4,5);
    **  slip::Range<int> range(0,A1.dim3()-1,2);
    ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
    ** //range in the row 1 of the slice 1 of A2
    ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
    ** \endcode
    */
    row_range_iterator row_end(const size_type slice,const size_type row,
			       const slip::Range<int>& range);


    /*!
    **  \brief Returns a read-only iterator that points to the first
    **  element of the %Range \a range of the row \a row in the %Volume.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return begin const_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** 
    ** \pre row must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> const A1(8,8,8);
    **  slip::Volume<double> A2(8,4,5);
    **  slip::Range<int> range(0,A1.dim3()-1,2);
    ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
    ** //range in the row 1 of the slice 1 of A2
    ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
    ** \endcode
    */
    const_row_range_iterator row_begin(const size_type slice,const size_type row,
				       const slip::Range<int>& range) const;


    /*!
    **  \brief Returns a read_only iterator that points one past the last
    **  element of the %Range range of the row \a row in the %Volume.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row Row to iterate.
    ** \param range %Range of the row to iterate
    ** \return begin const_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \pre row must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> const A1(8,8,8);
    **  slip::Volume<double> A2(8,4,5);
    **  slip::Range<int> range(0,A1.dim3()-1,2);
    ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
    ** //range in the row 1 of the slice 1 of A2
    ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
    ** \endcode
    */
    const_row_range_iterator row_end(const size_type slice,const size_type row,
				     const slip::Range<int>& range) const;
 
    /*!
    ** \brief Returns a read-write iterator that points to the last
    ** element of the %Range \a range of the row of a slice \a row and slice in the %Volume.  
    ** Iteration is done in the reverse element order according to the %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return reverse_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre row must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    */
    reverse_row_range_iterator row_rbegin(const size_type slice,const size_type row,
					  const slip::Range<int>& range);
  
  
    /*!
    **  \brief Returns a read-write iterator that points one before
    **  the first element of the %Range \a range of the row of a slice \a row in the 
    **  %Volume.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return reverse_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** 
    ** \pre row must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    */
    reverse_row_range_iterator row_rend(const size_type slice,const size_type row,
					const slip::Range<int>& range);



    /*!
    **  \brief Returns a read-only iterator that points to the last
    **  element of the %Range \a range of the row  of a slice \a row in the %Volume.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate
    ** \return begin const_reverse_row_range_iterator value
    **
    ** \pre row must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    */
    const_reverse_row_range_iterator row_rbegin(const size_type slice,const size_type row,
						const slip::Range<int>& range) const;


    /*!
    **  \brief Returns a read-only iterator that points one before the first
    **  element of the %Range \a range of the row of a slice \a row in the %Volume.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate
    ** \return const_reverse_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre row must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    */
    const_reverse_row_range_iterator row_rend(const size_type slice,const size_type row,
					      const slip::Range<int>& range) const;
 
    /*@} End One dimensional row range iterators */
    /**
     ** \name One dimensional col range iterators
     */
    /*@{*/
    //------------------------col range iterators -----------------------
  
    /*!
    **  \brief Returns a read-write iterator that points to the first
    **  element of the %Range \a range of the col \a col in the %Volume.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate
    ** \return begin col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> A1(8,8,8);
    **  slip::Volume<double> A2(4,8,5);
    **  slip::Range<int> range(0,A1.dim2()-1,2);
    ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
    ** //range in the column 1 of the slice 1 of A2
    ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
    ** \endcode
    */
    col_range_iterator col_begin(const size_type slice,const size_type col,
				 const slip::Range<int>& range);

    /*!
    **  \brief Returns a read-write iterator that points to the past
    **  the end element of the %Range \a range of the col \a col in the 
    **  %Volume.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return begin col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> A1(8,8,8);
    **  slip::Volume<double> A2(4,8,5);
    **  slip::Range<int> range(0,A1.dim2()-1,2);
    ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
    ** //range in the column 1 of the slice 1 of A2
    ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
    ** \endcode
    */
    col_range_iterator col_end(const size_type slice,const size_type col,
			       const slip::Range<int>& range);


    /*!
    **  \brief Returns a read-only iterator that points to the first
    **  element of the %Range \a range of the col \a col in the %Volume.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return begin const_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> const A1(8,8,8);
    **  slip::Volume<double> A2(4,8,5);
    **  slip::Range<int> range(0,A1.dim2()-1,2);
    ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
    ** //range in the column 1 of the slice 1 of A2
    ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
    ** \endcode
    */
    const_col_range_iterator col_begin(const size_type slice,const size_type col,
				       const slip::Range<int>& range) const;
    
    /*!
    **  \brief Returns a read-only iterator that points to the past
    **  the end element of the %Range \a range of the col \a col in 
    **  the %Volume.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate
    ** \return begin const_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    ** \par Example:
    ** \code
    ** 
    **  slip::Volume<double> const A1(8,8,8);
    **  slip::Volume<double> A2(4,8,5);
    **  slip::Range<int> range(0,A1.dim2()-1,2);
    ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
    ** //range in the column 1 of the slice 1 of A2
    ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
    ** \endcode
    */
    const_col_range_iterator col_end(const size_type slice,const size_type col,
				     const slip::Range<int>& range) const;

    /*!
    **  \brief Returns a read-write iterator that points to the last
    **  element of the %Range \a range of the col of a slice \a col in the %Volume.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return begin reverse_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    */
    reverse_col_range_iterator col_rbegin(const size_type slice,const size_type col,
					  const slip::Range<int>& range);

    /*!
    **  \brief Returns a read-write iterator that points to one before 
    **  the first element of the %Range range of the col of a slice \a col in the 
    **  %Volume.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return reverse_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    */
    reverse_col_range_iterator col_rend(const size_type slice,const size_type col,
					const slip::Range<int>& range);

    /*!
    **  \brief Returns a read_only iterator that points to the last
    **  element of the %Range \& range of the col of a slice \a col in the %Volume.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return begin const_reverse_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    */
    const_reverse_col_range_iterator col_rbegin(const size_type slice,const size_type col,
						const slip::Range<int>& range) const;
 
    /*!
    **  \brief Returns a read-only iterator that points to the first
    **  element of the %Range \a range of the col of a slice \a col in the %Volume.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return const_reverse_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Volume.
    ** \pre The range must be inside the whole range of the %Volume.
    */
    const_reverse_col_range_iterator col_rend(const size_type slice,const size_type col,
					      const slip::Range<int>& range) const;

    /*@} End One dimensional col range iterators */

    //****************************************************************************
    //                          One dimensional plane iterators
    //****************************************************************************
   
    /**
     ** \name One dimensional global plane iterators
     */
    /*@{*/

    //----------------------Global plane iterators------------------------------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element in the in the slice plane of the %Volume. 
    **  Iteration is done in ordinary element order.
    **  \param slice the slice coordinate of the plane
    **  \return begin iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Volume<double> A1(2,3,2);
    **   slip::Volume<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2
    **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
    **  \endcode
    */
    iterator plane_begin(const size_type slice);

    /*!
    **  \brief Returns a read-only (constant) iterator that points to the
    **  first element in the slice plane of the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param slice the slice coordinate of the plane
    **  \return const begin iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Volume<double> const A1(2,3,2);
    **   slip::Volume<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2
    **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
    **  \endcode
    */
    const_iterator plane_begin(const size_type slice) const;

    /*!
    **  \brief Returns a read/write iterator that points one past the last
    **  element in the slice plane of the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param slice the slice coordinate of the plane
    **  \return end iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Volume<double> A1(2,3,2);
    **   slip::Volume<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2
    **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
    **  \endcode
    */
    iterator plane_end(const size_type slice);
 
    /*!
    **  \brief Returns a read-only (constant) iterator that points one past
    **  the last element in the slice plane of the %Volume.
    **  Iteration is done in ordinary element order.
    **  \param slice the slice coordinate of the plane
    **  \return const end iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Volume<double> const A1(2,3,2);
    **   slip::Volume<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2
    **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
    **  \endcode
    */
    const_iterator plane_end(const size_type slice) const;
  
    /*!
    **  \brief Returns a read/write reverse iterator that points to the
    **  last element in the slice plane of the %Volume.
    **  Iteration is done in reverse element order.
    **  \param slice the slice coordinate of the plane
    **  \return reverse begin iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Volume<double> A1(2,3,2);
    **   slip::Volume<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
    **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
    **  \endcode
    */
    reverse_iterator plane_rbegin(const size_type slice);
  
    /*!
    **  \brief Returns a read-only (constant) reverse iterator that points
    **  to the last element in the slice plane k of the %Volume.
    **  Iteration is done in reverse element order.
    ** \param slice the slice coordinate of the plane
    ** \return const reverse begin iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Volume<double> const A1(2,3,2);
    **   slip::Volume<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
    **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
    **  \endcode
    */
    const_reverse_iterator plane_rbegin(const size_type slice) const;

    /*!
    **  \brief Returns a read/write reverse iterator that points to one
    **  before the first element in the slice plane of the %Volume.
    **  Iteration is done in reverse element order.
    **  \param slice the slice coordinate of the plane
    **  \return reverse end iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Volume<double> A1(2,3,2);
    **   slip::Volume<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
    **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
    **  \endcode
    */
    reverse_iterator plane_rend(const size_type slice);

    /*!
    **  \brief Returns a read-only (constant) reverse iterator that points
    **  to one before the first element in the slice plane of the %Volume.
    **  Iteration is done in reverse element order.
    **  \param slice the slice coordinate of the plane
    **  \return const reverse end iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Volume<double> const A1(2,3,2);
    **   slip::Volume<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
    **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
    **  \endcode
    */
    const_reverse_iterator plane_rend(const size_type slice) const;

    /*@} End One dimensional global plane iterators */


#ifdef ALL_PLANE_ITERATOR3D
    
    /**
     ** \name One dimensional row and col plane iterators
     */
    /*@{*/
    //-------------------row and col plane iterators----------
    
      
    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
				     const size_type row) const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type row);
    
    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the row \a row of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return end iterator1d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type row);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the row \a row of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type row) const;


    /*!
    **  \brief Returns a read/write reverse_iterator that points to the last
    **  element of the row \a row of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin reverse_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type row);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **  element of the row \a row of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					      const size_type row) const;
 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the row \a row of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type row);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the first
    **  element of the row \a row of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type row) const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the col \a col of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type col);

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the col \a col of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
				     const size_type col) const;
    
    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the col \a col of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type col);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the col \a col of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type col) const;


    /*!
    **  \brief Returns a read/write reverse_iterator that points to the last
    **  element of the col \a col of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin reverse_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type col);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **  element of the col \a col of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin reverse_const_iterator1d value
    */
    const_reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					      const size_type col) const;
 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the col \a col of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type col);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the first
    **  element of the col \a col of a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type col) const;

    /*@} End One dimensional plane row and col iterators */
    
    /**
     ** \name One dimensional plane box iterators
     */
    /*@{*/
    //-------------------plane box iterators----------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of a box within a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
				     const size_type row, const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of a box within a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return begin iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type row, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the row \a row of a box within a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate, 
			     const size_type row, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **   element of the row \a row of a box within a plane \a plane in the %Volume.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate,
				   const size_type row, const Box2d<int> & b) const;


    /*!
    **  \brief Returns a read/write reverse_iterator that points to the last
    **  element of the row \a row of a box within a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \param b The box within the plane
    **  \return begin reverse_iterator1d value
    */
    reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					const size_type row, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **  element of the row \a row of a box within a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					      const size_type row, const Box2d<int> & b) const;
 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the row \a row of a box within a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return end iterator1d value
    */
    reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,
				      const size_type row, const Box2d<int> & b);

    /*!
    ** \brief Returns a read/write const reverse iterator that points to the first
    **  element of the row \a row of a box within a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,
					    const size_type row, const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the col \a col of a box within a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin iterator1d value
    */
    iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type col, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the col \a col of a box within a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
				     const size_type col, const Box2d<int> & b) const;
    
    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the col \a col of a box within a plane \a plane in the %Volume.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate, 
			     const size_type col, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **   element of the col \a col of a box within a plane \a plane in the %Volume.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate,
				   const size_type col, const Box2d<int> & b) const;


    /*!
    **  \brief Returns a read/write reverse_iterator that points to the last
    **   element of the col \a col of a box within a plane \a plane in the %Volume.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin reverse_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					const size_type col, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **   element of the col \a col of a box within a plane \a plane in the %Volume.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					      const size_type col, const Box2d<int> & b) const;
 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **   element of the col \a col of a box within a plane \a plane in the %Volume.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate, 
				      const size_type col, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the first
    **   element of the col \a col of a box within a plane \a plane in the %Volume.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,
					    const size_type col, const Box2d<int> & b) const;


    /*@} End One dimensional plane box iterators */

#endif //ALL_PLANE_ITERATOR3D

    //****************************************************************************
    //                          Two dimensional plane iterators
    //****************************************************************************
    
    /**
     ** \name two dimensionnal plane iterators : Global iterators
     */
    /*@{*/

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **   element of the plane in the %Volume. It points to the upper left 
    **   element of the plane
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate);

    /*!
    **  \brief Returns a read/write iterator that points to the last
    **   element of the plane in the %Volume. It points to past the end element of
    **   the bottom right element of the plane
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate);

    /*!
    **  \brief Returns a read/write const iterator that points to the first
    **   element of the plane in the %Volume. It points to the upper left 
    **   element of the plane
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate) const;

    /*!
    **  \brief Returns a read/write const iterator that points to the last
    **  element of the plane in the %Volume. It points to past the end element of
    **  the bottom right element of the plane
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate) const;

    /*!
    **  \brief Returns a read/write reverse_iterator that points to the bottom right
    **  element of the plane in the %Volume.
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate);

    /*!
    **  \brief Returns a read/write reverse_iterator that points to the upper left
    **  element of the plane in the %Volume.
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example :
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the bottom
    **  right element of the plane in the %Volume.
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example :
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate) const;

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the upper
    **  left element of the plane in the %Volume.
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
                   slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate) const;


    /*@} End two dimensional plane iterators : Global iterators */
    
    /**
     ** \name two dimensionnal plane iterators : box iterators
     */
    /*@{*/

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of a box within a plane in the %Volume. It points to the upper left 
    **  element of the box
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
				, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the last
    **  element of a box within a plane in the %Volume. It points to past the end element of
    **  the bottom right element of the plane
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
				  , const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const iterator that points to the first
    **  element of a box within a plane in the %Volume. It points to the upper left 
    **  element of the plane
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
				      , const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write const iterator that points to the last
    **  element of a box within a plane in the %Volume. It points to past the end element of
    **  the bottom right element of the plane
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
					, const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write reverse iterator that points to the last
    **  element of a box within a plane in the %Volume. It points to the  
    **  bottom right element of the box
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin reverse_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box in a reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
					 , const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of a box within a plane in the %Volume. It points to the upper
    **  left element of the plane
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin reverse_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box in a reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
					   , const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **  element of a box within a plane in the %Volume. It points to the bottom right 
    **  element of the plane
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin const_reverse_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box in a reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
					       , const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the first
    **  element of a box within a plane in the %Volume. It points to
    **  the bottom right element of the plane
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin const_reverse_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box in a reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
						 , const Box2d<int> & b) const;
    

    /*@} End two dimensionnal plane iterators : box iterators */
    
    //****************************************************************************
    //                          Three dimensional iterators
    //****************************************************************************
   
    /**
     ** \name three dimensionnal iterators : Global iterators
     */
    /*@{*/

    //------------------------ Global iterators------------------------------------

    /*!
    **  \brief Returns a read/write iterator3d that points to the first
    **  element of the %Volume. It points to the front upper left element of
    **  the %Volume.
    **  
    **  \return begin iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   //Print M
    **   std::copy(M.front_upper_left(),M.back_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d front_upper_left();
  
  
    /*!
    **  \brief Returns a read/write iterator3d that points to the past
    **  the end element of the %Volume. It points to past the end element of
    **  the back bottom right element of the %Volume.
    **  
    **  \return begin iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   //Print M
    **   std::copy(M.front_upper_left(),M.back_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d back_bottom_right();


    /*!
    **  \brief Returns a read-only iterator3d that points to the first
    **  element of the %Volume. It points to the front upper left element of
    **  the %Volume.
    **  
    **  \return begin const iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   //Print M
    **   std::copy(M.front_upper_left(),M.back_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d front_upper_left() const;
  
 
    /*!
    **  \brief Returns a read-only iterator3d that points to the past
    **  the end element of the %Volume. It points to past the end element of
    **  the back bottom right element of the %Volume.
    **  
    **  \return begin const iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   //Print M
    **   std::copy(M.front_upper_left(),M.back_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d back_bottom_right() const;

    /*!
    **  \brief Returns a read/write reverse iterator3d. It points to the 
    **   back bottom right element of the %Volume. 
    *    Iteration is done within the %Volume in the reverse order.
    **  
    **  \return reverse_iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   //Print M in a reverse order
    **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator3d rfront_upper_left();
  
    /*!
    **  \brief Returns a read/write reverse iterator3d. It points to past the 
    **  front upper left element of the %Volume.
    **  Iteration is done in the reverse order.
    **  
    **  \return reverse iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   //Print M in a reverse order
    **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    **/
    reverse_iterator3d rback_bottom_right();

    /*!
    **  \brief Returns a read only reverse iterator3d that points.  It points 
    **  to the back bottom right element of the %Volume. 
    **  Iteration is done within the %Volume in the reverse order.
    **  
    ** \return const_reverse_iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   //Print M in a reverse order
    **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator3d rfront_upper_left() const;
 
 
    /*!
    **  \brief Returns a read only reverse iterator3d. It points to past the 
    **  front upper left element of the %Volume.
    **  Iteration is done in the reverse order.
    **  
    ** \return const reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   //Print M in a reverse order
    **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator3d rback_bottom_right() const;

    /*@} End three dimensionnal iterators : Global iterators */
    
    /**
     ** \name three dimensionnal iterators : Box iterators
     */
    /*@{*/
    
    //------------------------ Box iterators------------------------------------

    /*!
    **  \brief Returns a read/write iterator3d that points to the first
    **  element of the %Volume. It points to the front upper left element of
    **  the \a %Box3d associated to the %Volume.
    **
    **  \param box A %Box3d defining the range of indices to iterate
    **         within the %Volume.
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \return end iterator3d value
    **  \pre The box indices must be inside the range of the %Volume ones.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   slip::Box3d<int> b(1,1,1,2,2,3);
    **   //Print M within a box
    **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d front_upper_left(const Box3d<int>& box);

 
    /*!
    **  \brief Returns a read/write iterator3d that points to the past
    **  the end element of the %Volume. It points to past the end element of
    **  the back bottom right element of the \a %Box3d associated to the %Volume.
    **
    **  \param box a %Box3d defining the range of indices to iterate
    **         within the %Volume.
    **
    **  \return end iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \pre The box indices must be inside the range of the %Volume ones.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   slip::Box3d<int> b(1,1,1,2,2,3);
    **   //Print M within a box
    **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d back_bottom_right(const Box3d<int>& box);


    /*!
    **  \brief Returns a read only iterator3d that points to the first
    **  element of the %Volume. It points to the front upper left element of
    **  the \a %Box3d associated to the %Volume.
    **
    **  \param box a %Box3d defining the range of indices to iterate
    **         within the %Volume.
    **  \return end const iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **
    **  \pre The box indices must be inside the range of the %Volume ones.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   slip::Box3d<int> b(1,1,1,2,2,4);
    **   //Print M within a box
    **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d front_upper_left(const Box3d<int>& box) const;

 
    /*!
    **  \brief Returns a read only iterator3d that points to the past
    **  the end element of the %Volume. It points to past the end element of
    **  the back bottom right element of the \a %Box3d associated to the %Volume.
    **
    **  \param box a %Box3d defining the range of indices to iterate
    **         within the %Volume.
    **  \return end const iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **           algorithms.
    **
    **  \pre The box indices must be inside the range of the %Volume ones.
    ** 
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   slip::Box3d<int> b(1,1,1,2,2,4);
    **   //Print M within a box
    **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d back_bottom_right(const Box3d<int>& box) const;

  

    /*!
    **  \brief Returns a read/write reverse iterator3d. It points to the 
    **  back bottom right element of the \a %Box3d associated to the %Volume.
    **  Iteration is done in the reverse order.
    **
    ** \param box a %Box3d defining the range of indices to iterate
    **        within the %Volume.
    **
    ** \pre The box indices must be inside the range of the %Volume ones.
    **  
    ** \return reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    */
    reverse_iterator3d rfront_upper_left(const Box3d<int>& box);

    /*!
    **  \brief Returns a read/write reverse iterator3d. It points to one
    **  before the front upper left element of the %Box3d \a box associated to 
    **  the %Volume.
    **
    ** \param box A %Box3d defining the range of indices to iterate
    **        within the %Volume.
    **
    ** \pre The box indices must be inside the range of the %Volume ones.
    **  
    ** \return reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    */
    reverse_iterator3d rback_bottom_right(const Box3d<int>& box);

    /*!
    **  \brief Returns a read only reverse iterator3d. It points to the 
    **  back bottom right element of the %Box3d \a box associated to the %Volume.
    **  Iteration is done in the reverse order.
    **
    ** \param box A %Box3d defining the range of indices to iterate
    **        within the %Volume.
    **
    ** \pre The box indices must be inside the range of the %Volume ones.
    **  
    ** \return const reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    */
    const_reverse_iterator3d rfront_upper_left(const Box3d<int>& box) const;


    /*!
    **  \brief Returns a read-only reverse iterator3d. It points to one 
    **  before the front element of the bottom right element of the %Box3d 
    **  \a box associated to the %Volume.
    **
    ** \param box A %Box3d defining the range of indices to iterate
    **        within the %Volume.
    **
    ** \pre The box indices must be inside the range of the %Volume ones.
    **  
    ** \return const reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    */
    const_reverse_iterator3d rback_bottom_right(const Box3d<int>& box) const;

    
    /*@} End three dimensionnal iterators : Box iterators */
    
    /**
     ** \name three dimensionnal iterators : Range iterators
     */
    
    /*@{*/
    
    //------------------------ Range iterators------------------------------------
 
    /*!
    **  \brief Returns a read/write iterator3d_range that points to the 
    **  front upper left element of the ranges \a slice_range, \a row_range and \a col_range 
    **  associated to the %Volume.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Volume ones.
    **  
    ** \return iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range
    **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d_range front_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
				const Range<int>& col_range);

    /*!
    **  \brief Returns a read/write iterator3d_range that points to the 
    **   past the end back bottom right element of the ranges \a slice_range, \a row_range 
    **   and \a col_range associated to the %Volume.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Volume ones.
    **  
    ** \return iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range
    **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d_range back_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
				  const Range<int>& col_range);


    /*!
    **  \brief Returns a read-only iterator3d_range that points to the
    **   to the front upper left element of the ranges \a slice_range, \a row_range and \a col_range 
    **   associated to the %Volume.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid.
    ** \pre The ranges indices must be inside the ranges of the %Volume ones.
    **  
    ** \return const_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range
    **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d_range front_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
				      const Range<int>& col_range) const;


    /*!
    **  \brief Returns a read-only iterator3d_range that points to the past
    **  the end back bottom right element of the ranges \a slice_range, \a row_range and \a col_range 
    **  associated to the %Volume.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Volume ones.
    **  
    ** \return const_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range
    **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d_range back_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
					const Range<int>& col_range) const;

    /*!
    **  \brief Returns a read/write reverse_iterator3d_range that points to the 
    **  past the back bottom right element of the ranges \a row_range and 
    **  \a col_range associated to the %Volume. Iteration is done in the 
    **  reverse order.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Volume ones.
    **  
    ** \return reverse_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range in reverse order
    **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator3d_range rfront_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
					 const Range<int>& col_range);
    
    /*!
    **  \brief Returns a read/write reverse_iterator3d_range that points
    **  to one before the  front upper left element of the ranges \a row_range 
    **  and \a col_range associated to the %Volume. Iteration is done
    **  in the reverse order.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Volume ones.
    **  
    ** \return reverse_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range in reverse order
    **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator3d_range rback_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
					   const Range<int>& col_range);
    
    /*!
    **  \brief Returns a read-only reverse_iterator3d_range that points 
    **   to the past the back bottom right element of the ranges \a row_range and 
    **   \a col_range associated to the %Volume. Iteration is done in the 
    **   reverse order.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid.
    ** \pre The ranges indices must be inside the ranges of the %Volume ones.
    **  
    ** \return const_reverse_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range in reverse order
    **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator3d_range rfront_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
					       const Range<int>& col_range) const;
    
    /*!
    **  \brief Returns a read-only reverse_iterator3d_range that points 
    **  to one before the front upper left element of the ranges \a row_range 
    **  and \a col_range associated to the %Volume.Iteration is done in 
    **  the reverse order.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Volume ones.
    **  
    ** \return const_reverse_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Volume<double> const M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range in reverse order
    **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator3d_range rback_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
						 const Range<int>& col_range) const;
    
    
    /*@} End three dimensionnal iterators : Range iterators */
  
    //********************************************************************
  

  
    /**
     ** \name i/o operators
     */
    /*@{*/
    /*!
    ** \brief Write the %Volume to the ouput stream
    ** \param out output std::ostream
    ** \param a %Volume to write to the output stream
    */
    friend std::ostream& operator<< <>(std::ostream & out, 
				       const self& a);

//read/write image with ImageMagick
//breaking the ImageMagick dependency possibility
// Author : denis.arrivault\AT\inria.fr
// Date : 2013/04/05
// \since 1.4.0
#ifdef HAVE_MAGICK
    /*!
    ** \brief Reads a Volume from a file path name
    ** \param file_path_name Format base_file_name%nd.ext
    ** \param from Number of the first image to read 
    ** \param to Number of the last image to read
    ** 
    ** This method uses the ImageMagick library
    ** Volume<float> and Volume<double> data value are supposed
    ** to be between [0.0,1.0]
    */ 
    void read_from_images(const std::string& file_path_name,
			  const std::size_t from,
			  const std::size_t to);

    /*!
    ** \brief Write a Volume to a file path name
    ** \param file_path_name
    ** \param from Indice of the first image to write 
    ** \param to Indice of the last image to write
    **
    ** This method uses the ImageMagick library
    ** Volume<float> and Volume<double> data value are supposed
    ** to be between [0.0,1.0]
    */ 
    void write_to_images(const std::string& file_path_name,
			 const std::size_t from,
			 const std::size_t to ) const;

#else

    void read_from_images(const std::string& file_path_name,
                  const std::size_t from,
                  const std::size_t to)
    {
        throw std::runtime_error("ImageMagick has not been found: no 'read_from_images' method available.");
    }

    void write_to_images(const std::string& file_path_name,
                 const std::size_t from,
                 const std::size_t to ) const
    {
        throw std::runtime_error("ImageMagick has not been found: no 'write_to_images' method available.");
    }

#endif//HAVE_MAGICK

    /*!
    ** \brief Reads a Volume from a file path name
    ** \param file_path_name
    ** \param slices Number of slices
    ** \param rows Number of rows
    ** \param cols Number of cols
    ** 
    */ 
    void read_raw(const std::string& file_path_name,
		  const std::size_t slices,
		  const std::size_t rows,
		  const std::size_t cols);

    /*!
    ** \brief Write a Volume to a file path name
    ** \param file_path_name
    **
    */ 
    void write_raw(const std::string& file_path_name) const;


  

    /*!
    ** \brief Write a Volume to a tecplot file
    ** \param file_path_name
    ** \param title Title of the file.
    ** The data format is the following:
    ** \code
    ** TITLE= title 
    ** VARIABLES= X Y Z I
    ** ZONE T= data, I= rows() J=  cols() K= slices() F=POINT
    ** x y z I(x,y,z)
    ** \endcode
    ** \remarks x = j and y = (dim1 - 1) - i and z = k
    **
    */ 
    void write_tecplot(const std::string& file_path_name,
		       const std::string& title) const;
    
    /*!
    ** \brief Write slices of a Container3D to a tecplot file.
    ** \param file_path_name String of the file path name
    ** \param title Title of the file.
    ** \param from Index of the first slice.
    ** \param to Index of the last slice.
    ** The data format is the following:
    ** \code
    ** TITLE= title 
    ** VARIABLES= X Y I
    ** ZONE T= image to I= rows() J=  cols() F=POINT
    ** <data of the first slice>
    ** ZONE T= image (to+1) I= rows() J=  cols() F=POINT
    ** <data of the second slice>
    ** ...
    ** \endcode
    ** \remarks x = j and y = (dim1 - 1) - i
    **
    */ 
    void write_to_images_tecplot(const std::string& file_path_name,
				 const std::string& title,
				 const std::size_t from,
				 const std::size_t to) const;
    

    /*@} End i/o operators */

    /**
     ** \name  Assignment operators and methods
     */
    /*@{*/
    /*!
    ** \brief Assign a %Volume.
    **
    ** Assign elements of %Volume in \a rhs
    **
    ** \param rhs %Volume to get the values from.
    ** \return 
    */
    self& operator=(const Volume<T> & rhs);



    /*!
    ** \brief Assign all the elments of the %Volume by value
    **
    **
    ** \param value A reference-to-const of arbitrary type.
    ** \return 
    */
    self& operator=(const T& value);


    /*!
    ** \brief Fills the container range [begin(),begin()+size()) 
    **        with copies of value
    ** \param value  A reference-to-const of arbitrary type.
    */
    void fill(const T& value)
    {
      std::fill_n(this->begin(),this->size(),value);
    } 
  
    /*!
    ** \brief Fills the container range [begin(),begin()+size())
    **        with a copy of
    **        the value array
    ** \param value  A pointer of arbitrary type.
    */
    void fill(const T* value)
    {
      std::copy(value,value + this->size(), this->begin());
    } 

    /*!
    ** \brief Fills the container range [begin(),begin()+size()) 
    **        with a copy of the range [first,last)
    **  \param  first  An input iterator.
    **  \param  last   An input iterator.
    **   
    **
    */
    template<typename InputIterator>
    void fill(InputIterator first,
	      InputIterator last)
    {
      std::copy(first,last, this->begin());
    }
    /*@} End Assignment operators and methods*/



    /**
     ** \name Comparison operators
     */
    /*@{*/
    /*!
    ** \brief %Volume equality comparison
    ** \param x A %Volume
    ** \param y A %Volume of the same type of \a x
    ** \return true iff the size and the elements of the Arrays are equal
    */
    friend bool operator== <>(const Volume<T>& x, 
			      const Volume<T>& y);

    /*!
    ** \brief %Volume inequality comparison
    ** \param x A %Volume
    ** \param y A %Volume of the same type of \a x
    ** \return true if !(x == y) 
    */
    friend bool operator!= <>(const Volume<T>& x, 
			      const Volume<T>& y);

    /*!
    ** \brief Less than comparison operator (%Volume ordering relation)
    ** \param x A %Volume
    ** \param y A %Volume of the same type of \a x
    ** \return true iff \a x is lexicographically less than \a y
    */
    friend bool operator< <>(const Volume<T>& x, 
			     const Volume<T>& y);

    /*!
    ** \brief More than comparison operator
    ** \param x A %Volume
    ** \param y A %Volume of the same type of \a x
    ** \return true iff y > x 
    */
    friend bool operator> <>(const Volume<T>& x, 
			     const Volume<T>& y);

    /*!
    ** \brief Less than equal comparison operator
    ** \param x A %Volume
    ** \param y A %Volume of the same type of \a x
    ** \return true iff !(y > x) 
    */
    friend bool operator<= <>(const Volume<T>& x, 
			      const Volume<T>& y);

    /*!
    ** \brief More than equal comparison operator
    ** \param x A %Volume
    ** \param y A %Volume of the same type of \a x
    ** \return true iff !(x < y) 
    */
    friend bool operator>= <>(const Volume<T>& x, 
			      const Volume<T>& y);


    /*@} Comparison operators */
  



    /**
     ** \name  Element access operators
     */
    /*@{*/

    T** operator[](const size_type k);

    const T* const* operator[](const size_type k) const;
  

    /*!
    ** \brief Subscript access to the data contained in the %Volume.
    ** \param k The index of the slice for which the data should be accessed. 
    ** \param i The index of the row for which the data should be accessed. 
    ** \param j The index of the column for which the data should be accessed.
    ** \return Read/Write reference to data.
    ** \pre k < slices()
    ** \pre i < rows()
    ** \pre j < cols()
    ** This operator allows for easy, 3d array-style, data access.
    ** Note that data access with this operator is unchecked and
    ** out_of_range lookups are not defined. 
    */
    reference operator()(const size_type k,
			 const size_type i,
			 const size_type j);

    /*!
    ** \brief Subscript access to the data contained in the %Volume.
    ** \param k The index of the slice for which the data should be accessed. 
    ** \param i The index of the row for which the data should be accessed. 
    ** \param j The index of the column for which the data should be accessed.
    ** \return Read_only (constant) reference to data.
    ** \pre k < slices()
    ** \pre i < rows()
    ** \pre j < cols()
    ** This operator allows for easy, 3d array-style, data access.
    ** Note that data access with this operator is unchecked and
    ** out_of_range lookups are not defined. 
    */
    const_reference operator()(const size_type k,
			       const size_type i,
			       const size_type j) const;
	
    /*@} End Element access operators */

     /*!
     ** \brief Returns the name of the class 
     **       
     */
    std::string name() const;
    

    /*!
    ** \brief Returns the number of slices (first dimension size) 
    **        in the %Volume
    */
    size_type dim1() const;
  
    /*!
    ** \brief Returns the number of slices (first dimension size) 
    **        in the %Volume
    */
    size_type slices() const;
 
    /*!
    ** \brief Returns the number of rows (second dimension size) 
    **        in the %Volume
    */
    size_type dim2() const;

    /*!
    ** \brief Returns the number of rows (second dimension size) 
    **        in the %Volume
    */
    size_type rows() const;

    /*!
    ** \brief Returns the number of columns (third dimension size) 
    **        in the %Volume
    */
    size_type dim3() const;

    /*!
    ** \brief Returns the number of columns (third dimension size) 
    **        in the %Volume
    */
    size_type cols() const;

    /*!
    ** \brief Returns the number of columns (third dimension size) 
    **        in the %Volume
    */
    size_type columns() const;
  
    /*!
    ** \brief Returns the number of elements in the %Volume
    */
    size_type size() const;


    /*!
    ** \brief Returns the maximal size (number of elements) in the %Volume
    */
    size_type max_size() const;


    /*!
    ** \brief Returns the number of elements in a slice of the %Volume
    */
    size_type slice_size() const;


    /*!
    ** \brief Returns true if the %Volume is empty.  (Thus size() == 0)
    */
    bool empty()const;
  
    /*!
    ** \brief Swaps data with another %Volume.
    ** \param M A %Volume of the same element type
    **
    ** \pre dim1() == M.dim1()
    ** \pre dim2() == M.dim2()
    ** \pre dim3() == M.dim3()
    */
    void swap(Volume& M);

    /**
     ** \name  Arithmetic operators
     */
    /*@{*/
    /*!
    ** \brief Add val to each element of the Volume  
    ** \param val value
    ** \return reference to the resulting Volume
    */
    self& operator+=(const T& val);
    self& operator-=(const T& val);
    self& operator*=(const T& val);
    self& operator/=(const T& val);
    //   self& operator%=(const T& val);
    //   self& operator^=(const T& val);
    //   self& operator&=(const T& val);
    //   self& operator|=(const T& val);
    //   self& operator<<=(const T& val);
    //   self& operator>>=(const T& val);


    self  operator-() const;
    //self  operator!() const;

  

    self& operator+=(const self& rhs);
    self& operator-=(const self& rhs);
    self& operator*=(const self& rhs);
    self& operator/=(const self& rhs);
    

    /*@} End Arithmetic operators */

    /**
     ** \name  Mathematic operators
     */
    /*@{*/
    /*!
    ** \brief Returns the min element of the %Volume
    **  according to the operator <
    ** \pre size() != 0
    */
    T& min() const;


    /*!
    ** \brief Returns the max element of the %Volume 
    ** according to the operator <
    ** \pre size() != 0
    */
    T& max() const;

    /*!
    ** \brief Returns the sum of the elements of the %Volume 
    ** \pre size() != 0
    */
    T sum() const;

  
    /*!
    ** \brief Applys the one-parameter C-function \a fun 
    **        to each element of the %Volume
    ** \param fun The one-parameter C function 
    ** \return the resulting %Volume
    */
    Volume<T>& apply(T (*fun)(T));

    /*!
    ** \brief Applys the one-parameter C-function \a fun 
    **        to each element of the %Volume
    ** \param fun The one-const-parameter C function 
    ** \return the resulting %Volume
    */
    Volume<T>& apply(T (*fun)(const T&));
  
    /*@} End Arithmetic operators */

  private:
    Matrix3d<T>* matrix_;
   private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & this->matrix_;
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & this->matrix_;
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()

  };

   ///double alias
  typedef slip::Volume<double> Volume_d;
  ///float alias
  typedef slip::Volume<float> Volume_f;
  ///long alias
  typedef slip::Volume<long> Volume_l;
  ///unsigned long alias
  typedef slip::Volume<unsigned long> Volume_ul;
  ///short alias
  typedef slip::Volume<short> Volume_s;
  ///unsigned long alias
  typedef slip::Volume<unsigned short> Volume_us;
  ///int alias
  typedef slip::Volume<int> Volume_i;
  ///unsigned int alias
  typedef slip::Volume<unsigned int> Volume_ui;
  ///char alias
  typedef slip::Volume<char> Volume_c;
  ///unsigned char alias
  typedef slip::Volume<unsigned char> Volume_uc;

}//slip::


namespace slip{
  /*!
  ** \brief pointwise addition of two %Volume
  ** \param M1 first %Volume 
  ** \param M2 seconf %Volume
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %Volume
  */
  template<typename T>
  Volume<T> operator+(const Volume<T>& M1, 
		      const Volume<T>& M2);

  /*!
  ** \brief addition of a scalar to each element of a %Volume
  ** \param M1 the %Volume 
  ** \param val the scalar
  ** \return resulting %Volume
  */
  template<typename T>
  Volume<T> operator+(const Volume<T>& M1, 
		      const T& val);

  /*!
  ** \brief addition of a scalar to each element of a %Volume
  ** \param val the scalar
  ** \param M1 the %Volume  
  ** \return resulting %Volume
  */
  template<typename T>
  Volume<T> operator+(const T& val, 
		      const Volume<T>& M1);
  
  /*!
  ** \brief pointwise substraction of two %Volume
  ** \param M1 first %Volume 
  ** \param M2 seconf %Volume
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %Volume
  */
  template<typename T>
  Volume<T> operator-(const Volume<T>& M1, 
		      const Volume<T>& M2);

  /*!
  ** \brief substraction of a scalar to each element of a %Volume
  ** \param M1 the %Volume 
  ** \param val the scalar
  ** \return resulting %Volume
  */
  template<typename T>
  Volume<T> operator-(const Volume<T>& M1, 
		      const T& val);

  /*!
  ** \brief substraction of a scalar to each element of a %Volume
  ** \param val the scalar
  ** \param M1 the %Volume  
  ** \return resulting %Volume
  */
  template<typename T>
  Volume<T> operator-(const T& val, 
		      const Volume<T>& M1);
  
  /*!
  ** \brief pointwise multiplication of two %Volume
  ** \param M1 first %Volume 
  ** \param M2 seconf %Volume
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %Volume
  */
  template<typename T>
  Volume<T> operator*(const Volume<T>& M1, 
		      const Volume<T>& M2);

  /*!
  ** \brief multiplication of a scalar to each element of a %Volume
  ** \param M1 the %Volume 
  ** \param val the scalar
  ** \return resulting %Volume
  */
  template<typename T>
  Volume<T> operator*(const Volume<T>& M1, 
		      const T& val);

  /*!
  ** \brief multiplication of a scalar to each element of a %Volume
  ** \param val the scalar
  ** \param M1 the %Volume  
  ** \return resulting %Volume
  */
  template<typename T>
  Volume<T> operator*(const T& val, 
		      const Volume<T>& M1);
  
  /*!
  ** \brief pointwise division of two %Volume
  ** \param M1 first %Volume 
  ** \param M2 seconf %Volume
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %Volume
  */
  template<typename T>
  Volume<T> operator/(const Volume<T>& M1, 
		      const Volume<T>& M2);

  /*!
  ** \brief division of a scalar to each element of a %Volume
  ** \param M1 the %Volume 
  ** \param val the scalar
  ** \return resulting %Volume
  */
  template<typename T>
  Volume<T> operator/(const Volume<T>& M1, 
		      const T& val);

 
  /*!
  ** \relates Volume
  ** \brief Returns the min element of a Volume
  ** \param M1 the Volume  
  ** \return the min element
  */
  template<typename T>
  T& min(const Volume<T>& M1);
  
  /*!
  ** \relates Volume
  ** \brief Returns the max element of a Volume
  ** \param M1 the Volume  
  ** \return the min element
  */
  template<typename T>
  T& max(const Volume<T>& M1);

  /*!
  ** \relates Volume
  ** \brief Returns the abs value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> abs(const Volume<T>& V);

  /*!
  ** \relates Volume
  ** \brief Returns the sqrt value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> sqrt(const Volume<T>& V);

  /*!
  ** \relates Volume
  ** \brief Returns the cos value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> cos(const Volume<T>& V);

  /*!
  ** \relates Volume
  ** \brief Returns the acos value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> acos(const Volume<T>& V);

  /*!
  ** \relates Volume
  ** \brief Returns the sin value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> sin(const Volume<T>& V);

  /*!
  ** \relates Volume
  ** \brief Returns the sin value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> asin(const Volume<T>& V);


  /*!
  ** \relates Volume
  ** \brief Returns the tan value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> tan(const Volume<T>& V);

  /*!
  ** \relates Volume
  ** \brief Returns the atan value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> atan(const Volume<T>& V);

  /*!
  ** \relates Volume
  ** \brief Returns the exp value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> exp(const Volume<T>& V);

  /*!
  ** \relates Volume
  ** \brief Returns the log value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> log(const Volume<T>& V);

  /*!
  ** \relates Volume
  ** \brief Returns the cosh value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> cosh(const Volume<T>& V);

  /*!
  ** \relates Volume
  ** \brief Returns the sinh value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> sinh(const Volume<T>& V);

  /*!
  ** \relates Volume
  ** \brief Returns the tanh value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> tanh(const Volume<T>& V);

  /*!
  ** \relates Volume
  ** \brief Returns the log10 value of each element of the %Volume
  ** \param V The %Volume  
  ** \return the resulting %Volume
  */
  template<typename T>
  Volume<T> log10(const Volume<T>& V);
  

}//slip::



namespace slip
{
  //////////////////////////////////////////
  //   Constructors
  //////////////////////////////////////////
  template<typename T>
  inline
  Volume<T>::Volume():
    matrix_(new slip::Matrix3d<T>())
  {}
  
  template<typename T>
  inline
  Volume<T>::Volume(const typename Volume<T>::size_type d1,
		    const typename Volume<T>::size_type d2,
		    const typename Volume<T>::size_type d3):
    matrix_(new slip::Matrix3d<T>(d1,d2,d3))
  {}

  template<typename T>
  inline
  Volume<T>::Volume(const typename Volume<T>::size_type d1,
		    const typename Volume<T>::size_type d2,
		    const typename Volume<T>::size_type d3,
		    const T& val):
    matrix_(new slip::Matrix3d<T>(d1,d2,d3,val))
  {}

  template<typename T>
  inline
  Volume<T>::Volume(const typename Volume<T>::size_type d1,
		    const typename Volume<T>::size_type d2,
		    const typename Volume<T>::size_type d3,
		    const T* val):
    matrix_(new slip::Matrix3d<T>(d1,d2,d3,val))
  {}


  template<typename T>
  inline
  Volume<T>::Volume(const Volume<T>& rhs):
    matrix_(new slip::Matrix3d<T>((*rhs.matrix_)))
  {}

  template<typename T>
  inline
  Volume<T>::~Volume()
  {
    delete matrix_;
  }
  
  ///////////////////////////////////////////
  

  //////////////////////////////////////////
  //   Assignment operators
  //////////////////////////////////////////
  template<typename T>
  inline
  Volume<T>&  Volume<T>::operator=(const Volume<T> & rhs)
  {
    if(this != &rhs)
      {
	*matrix_ = *(rhs.matrix_);
      }
    return *this;
  }

  template<typename T>
  inline
  Volume<T>& Volume<T>::operator=(const T& value)
  {
    std::fill_n((*matrix_)[0][0],matrix_->size(),value);
    return *this;
  }

  template<typename T>
  inline
  void Volume<T>::resize(const typename Volume<T>::size_type d1,
			 const typename Volume<T>::size_type d2,
			 const typename Volume<T>::size_type d3,
			 const T& val)
  {
    matrix_->resize(d1,d2,d3,val);
  }
  ///////////////////////////////////////////


  //////////////////////////////////////////
  //   Iterators
  //////////////////////////////////////////

  //****************************************************************************
  //                          One dimensional iterators
  //****************************************************************************
  
  
  
  //----------------------Global iterators------------------------------
  
  template<typename T>
  inline
  typename Volume<T>::iterator Volume<T>::begin()
  {
    return matrix_->begin();
  }

  template<typename T>
  inline
  typename Volume<T>::iterator Volume<T>::end()
  {
    return matrix_->end();
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_iterator Volume<T>::begin() const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->begin();
  }

  template<typename T>
  inline
  typename Volume<T>::const_iterator Volume<T>::end() const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->end();
  }
  

  template<typename T>
  inline
  typename Volume<T>::reverse_iterator Volume<T>::rbegin()
  {
    return typename Volume<T>::reverse_iterator(this->end());
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_iterator Volume<T>::rend()
  {
    return typename Volume<T>::reverse_iterator(this->begin());
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator Volume<T>::rbegin() const
  {
    return typename Volume<T>::const_reverse_iterator(this->end());
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator Volume<T>::rend() const
  {
    return typename Volume<T>::const_reverse_iterator(this->begin());
  }

  //--------------------Constant slice global iterators----------------------
  
  template<typename T>
  inline
  typename Volume<T>::slice_iterator 
  Volume<T>::slice_begin(const typename Volume<T>::size_type row, 
			   const typename Volume<T>::size_type col)
  {
    return matrix_->slice_begin(row,col);
  }

  template<typename T>
  inline
  typename Volume<T>::const_slice_iterator 
  Volume<T>::slice_begin(const typename Volume<T>::size_type row, 
			   const typename Volume<T>::size_type col) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->slice_begin(row,col);
  }

  template<typename T>
  inline
  typename Volume<T>::slice_iterator 
  Volume<T>::slice_end(const typename Volume<T>::size_type row, 
			 const typename Volume<T>::size_type col)
  {
    return matrix_->slice_end(row,col);
  }

  template<typename T>
  inline
  typename Volume<T>::const_slice_iterator 
  Volume<T>::slice_end(const typename Volume<T>::size_type row, 
			 const typename Volume<T>::size_type col) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->slice_end(row,col);
  }


  template<typename T>
  inline
  typename Volume<T>::reverse_slice_iterator 
  Volume<T>::slice_rbegin(const typename Volume<T>::size_type row, 
			    const typename Volume<T>::size_type col)
  {
    return matrix_->slice_rbegin(row,col);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_slice_iterator 
  Volume<T>::slice_rbegin(const typename Volume<T>::size_type row, 
			  const typename Volume<T>::size_type col) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->slice_rbegin(row,col);
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_slice_iterator 
  Volume<T>::slice_rend(const typename Volume<T>::size_type row, 
			const typename Volume<T>::size_type col)
  {
    return matrix_->slice_rend(row,col);
  }

  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_slice_iterator
  Volume<T>::slice_rend(const typename Volume<T>::size_type row, 
			const typename Volume<T>::size_type col) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->slice_rend(row,col);
  }

  //--------------------simple row iterators----------------------


  template<typename T>
  inline
  typename Volume<T>::row_iterator 
  Volume<T>::row_begin(const typename Volume<T>::size_type slice,
		       const typename Volume<T>::size_type row)
  {
    return matrix_->row_begin(slice,row);
  }

  template<typename T>
  inline
  typename Volume<T>::const_row_iterator 
  Volume<T>::row_begin(const typename Volume<T>::size_type slice,
		       const typename Volume<T>::size_type row) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->row_begin(slice,row);
  }

  template<typename T>
  inline
  typename Volume<T>::row_iterator 
  Volume<T>::row_end(const typename Volume<T>::size_type slice,
		     const typename Volume<T>::size_type row)
  {
    return matrix_->row_end(slice,row);
  }

  template<typename T>
  inline
  typename Volume<T>::const_row_iterator 
  Volume<T>::row_end(const typename Volume<T>::size_type slice,
		     const typename Volume<T>::size_type row) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->row_end(slice,row);
  }


  template<typename T>
  inline
  typename Volume<T>::reverse_row_iterator 
  Volume<T>::row_rbegin(const typename Volume<T>::size_type slice,
			const typename Volume<T>::size_type row)
  {
    return matrix_->row_rbegin(slice,row);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_row_iterator 
  Volume<T>::row_rbegin(const typename Volume<T>::size_type slice,
			const typename Volume<T>::size_type row) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->row_rbegin(slice,row);
  }


  template<typename T>
  inline
  typename Volume<T>::reverse_row_iterator 
  Volume<T>::row_rend(const typename Volume<T>::size_type slice,
		      const typename Volume<T>::size_type row)
  {
    return matrix_->row_rend(slice,row);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_row_iterator 
  Volume<T>::row_rend(const typename Volume<T>::size_type slice,
		      const typename Volume<T>::size_type row) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->row_rend(slice,row);
  }

  //--------------------Simple col iterators----------------------

  template<typename T>
  inline
  typename Volume<T>::col_iterator 
  Volume<T>::col_begin(const typename Volume<T>::size_type slice,
		       const typename Volume<T>::size_type col)
  {
    return matrix_->col_begin(slice,col);
  }

  template<typename T>
  inline
  typename Volume<T>::const_col_iterator 
  Volume<T>::col_begin(const typename Volume<T>::size_type slice,
		       const typename Volume<T>::size_type col) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->col_begin(slice,col);
  }


  template<typename T>
  inline
  typename Volume<T>::col_iterator 
  Volume<T>::col_end(const typename Volume<T>::size_type slice,
		     const typename Volume<T>::size_type col)
  {
    return matrix_->col_end(slice,col);
  }

  template<typename T>
  inline
  typename Volume<T>::const_col_iterator 
  Volume<T>::col_end(const typename Volume<T>::size_type slice,
		     const typename Volume<T>::size_type col) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->col_end(slice,col);
  }

 
  template<typename T>
  inline
  typename Volume<T>::reverse_col_iterator 
  Volume<T>::col_rbegin(const typename Volume<T>::size_type slice,
			const typename Volume<T>::size_type col)
  {
    return matrix_->col_rbegin(slice,col);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_col_iterator 
  Volume<T>::col_rbegin(const typename Volume<T>::size_type slice,
			const typename Volume<T>::size_type col) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->col_rbegin(slice,col);
  }


  template<typename T>
  inline
  typename Volume<T>::reverse_col_iterator 
  Volume<T>::col_rend(const typename Volume<T>::size_type slice,
		      const typename Volume<T>::size_type col)
  {
    return matrix_->col_rend(slice,col);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_col_iterator 
  Volume<T>::col_rend(const typename Volume<T>::size_type slice,
		      const typename Volume<T>::size_type col) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->col_rend(slice,col);
  }

 //--------------------Constant slice range iterators----------------------

  template<typename T>
  inline
  typename Volume<T>::slice_range_iterator 
  Volume<T>::slice_begin(const typename Volume<T>::size_type row, 
			 const typename Volume<T>::size_type col, 
			 const slip::Range<int>& range)
  {
    return matrix_->slice_begin(row,col,range);
  }

  template<typename T>
  inline
  typename Volume<T>::const_slice_range_iterator 
  Volume<T>::slice_begin(const typename Volume<T>::size_type row, 
			 const typename Volume<T>::size_type col,
			 const slip::Range<int>& range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->slice_begin(row,col,range);
  }

  template<typename T>
  inline
  typename Volume<T>::slice_range_iterator 
  Volume<T>::slice_end(const typename Volume<T>::size_type row, 
		       const typename Volume<T>::size_type col,
		       const slip::Range<int>& range)
  {
    return matrix_->slice_end(row,col,range);
  }

  template<typename T>
  inline
  typename Volume<T>::const_slice_range_iterator 
  Volume<T>::slice_end(const typename Volume<T>::size_type row,
		       const typename Volume<T>::size_type col,
		       const slip::Range<int>& range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->slice_end(row,col,range);
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_slice_range_iterator 
  Volume<T>::slice_rbegin(const typename Volume<T>::size_type row, 
			  const typename Volume<T>::size_type col,
			  const slip::Range<int>& range)
  {
    return matrix_->slice_rbegin(row,col,range);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_slice_range_iterator 
  Volume<T>::slice_rbegin(const typename Volume<T>::size_type row, 
			  const typename Volume<T>::size_type col, 
			  const slip::Range<int>& range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->slice_rbegin(row,col,range);
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_slice_range_iterator 
  Volume<T>::slice_rend(const typename Volume<T>::size_type row, 
			const typename Volume<T>::size_type col,
			const slip::Range<int>& range)
  {
    return matrix_->slice_rend(row,col,range);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_slice_range_iterator 
  Volume<T>::slice_rend(const typename Volume<T>::size_type row, 
			const typename Volume<T>::size_type col,
			const slip::Range<int>& range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->slice_rend(row,col,range);
  }
  
  //--------------------Constant row range iterators----------------------

  template<typename T>
  inline
  typename Volume<T>::row_range_iterator 
  Volume<T>::row_begin(const typename Volume<T>::size_type slice, 
			 const typename Volume<T>::size_type row,
			 const slip::Range<int>& range)
  {
    return matrix_->row_begin(slice,row,range);
  }

  template<typename T>
  inline
  typename Volume<T>::const_row_range_iterator 
  Volume<T>::row_begin(const typename Volume<T>::size_type slice,
			 const typename Volume<T>::size_type row,
			 const slip::Range<int>& range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->row_begin(slice,row,range);
  }

  template<typename T>
  inline
  typename Volume<T>::row_range_iterator 
  Volume<T>::row_end(const typename Volume<T>::size_type slice,
		       const typename Volume<T>::size_type row,
		       const slip::Range<int>& range)
  {
    return matrix_->row_end(slice,row,range);
  }

  template<typename T>
  inline
  typename Volume<T>::const_row_range_iterator 
  Volume<T>::row_end(const typename Volume<T>::size_type slice,
		       const typename Volume<T>::size_type row,
		       const slip::Range<int>& range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->row_end(slice,row,range);
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_row_range_iterator 
  Volume<T>::row_rbegin(const typename Volume<T>::size_type slice,
			  const typename Volume<T>::size_type row,
			  const slip::Range<int>& range)
  {
    return matrix_->row_rbegin(slice,row,range);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_row_range_iterator 
  Volume<T>::row_rbegin(const typename Volume<T>::size_type slice,
			  const typename Volume<T>::size_type row,
			  const slip::Range<int>& range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->row_rbegin(slice,row,range);
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_row_range_iterator 
  Volume<T>::row_rend(const typename Volume<T>::size_type slice,
			const typename Volume<T>::size_type row,
			const slip::Range<int>& range)
  {
    return matrix_->row_rend(slice,row,range);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_row_range_iterator 
  Volume<T>::row_rend(const typename Volume<T>::size_type slice,
			const typename Volume<T>::size_type row,
			const slip::Range<int>& range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->row_rend(slice,row,range);
  }

  //--------------------Constant col range iterators----------------------  

  template<typename T>
  inline
  typename Volume<T>::col_range_iterator 
  Volume<T>::col_begin(const typename Volume<T>::size_type slice,
			 const typename Volume<T>::size_type col,
			 const slip::Range<int>& range)
  {
    return matrix_->col_begin(slice,col,range);
  }

  template<typename T>
  inline
  typename Volume<T>::const_col_range_iterator 
  Volume<T>::col_begin(const typename Volume<T>::size_type slice,
			 const typename Volume<T>::size_type col,
			 const slip::Range<int>& range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->col_begin(slice,col,range);
  }
 
  template<typename T>
  inline
  typename Volume<T>::col_range_iterator 
  Volume<T>::col_end(const typename Volume<T>::size_type slice,
		       const typename Volume<T>::size_type col,
		       const slip::Range<int>& range)
  {
    return matrix_->col_end(slice,col,range);
  }

  template<typename T>
  inline
  typename Volume<T>::const_col_range_iterator 
  Volume<T>::col_end(const typename Volume<T>::size_type slice,
		       const typename Volume<T>::size_type col,
		       const slip::Range<int>& range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->col_end(slice,col,range);
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_col_range_iterator 
  Volume<T>::col_rbegin(const typename Volume<T>::size_type slice,
			  const typename Volume<T>::size_type col,
			  const slip::Range<int>& range)
  {
    return matrix_->col_rbegin(slice,col,range);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_col_range_iterator 
  Volume<T>::col_rbegin(const typename Volume<T>::size_type slice,
			  const typename Volume<T>::size_type col,
			  const slip::Range<int>& range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->col_rbegin(slice,col,range);
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_col_range_iterator 
  Volume<T>::col_rend(const typename Volume<T>::size_type slice,
			const typename Volume<T>::size_type col,
			const slip::Range<int>& range)
  {
    return matrix_->col_rend(slice,col,range);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_col_range_iterator 
  Volume<T>::col_rend(const typename Volume<T>::size_type slice,
			const typename Volume<T>::size_type col,
			const slip::Range<int>& range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->col_rend(slice,col,range);
  }
 
  //-------------------plane global iterators----------

  template<typename T>
  inline
  typename Volume<T>::iterator 
  Volume<T>::plane_begin(const typename Volume<T>::size_type slice)
  {
    return matrix_->plane_begin(slice);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_iterator 
  Volume<T>::plane_begin(const typename Volume<T>::size_type slice) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_begin(slice);
  }
  
  template<typename T>
  inline
  typename Volume<T>::iterator 
  Volume<T>::plane_end(const typename Volume<T>::size_type slice)
  {
    return matrix_->plane_end(slice);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_iterator 
  Volume<T>::plane_end(const typename Volume<T>::size_type slice) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_end(slice);
  }
  
  template<typename T>
  inline
  typename Volume<T>::reverse_iterator 
  Volume<T>::plane_rbegin(const typename Volume<T>::size_type slice)
  {
    return matrix_->plane_rbegin(slice);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator 
  Volume<T>::plane_rbegin(const typename Volume<T>::size_type slice) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_rbegin(slice);
  }
  
  template<typename T>
  inline
  typename Volume<T>::reverse_iterator 
  Volume<T>::plane_rend(const typename Volume<T>::size_type slice)
  {
    return matrix_->plane_rend(slice);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator 
  Volume<T>::plane_rend(const typename Volume<T>::size_type slice) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_rend(slice);
  }
 
#ifdef ALL_PLANE_ITERATOR3D

  //-------------------plane row and col iterators----------
  
 
  template<typename T>
  inline
  typename Volume<T>::iterator1d 
  Volume<T>::plane_row_begin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			       const typename Volume<T>::size_type row)
  {
    return matrix_->plane_row_begin(P,plane_coordinate,row);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_iterator1d  
  Volume<T>::plane_row_begin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			       const typename Volume<T>::size_type row) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_row_begin(P,plane_coordinate,row);
  }
  
  template<typename T>
  inline
  typename Volume<T>::iterator1d 
  Volume<T>::plane_row_end(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, 
			     const typename Volume<T>::size_type row)
  {
    return matrix_->plane_row_end(P,plane_coordinate,row);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_iterator1d 
  Volume<T>::plane_row_end(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			     const typename Volume<T>::size_type row) const
  { 
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_row_end(P,plane_coordinate,row);
  }
  
  template<typename T>
  inline
  typename Volume<T>::reverse_iterator1d 
  Volume<T>::plane_row_rbegin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
				const typename Volume<T>::size_type row)
  {
    return matrix_->plane_row_rbegin(P,plane_coordinate,row);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator1d 
  Volume<T>::plane_row_rbegin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
				const typename Volume<T>::size_type row) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_row_rbegin(P,plane_coordinate,row);
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_iterator1d 
  Volume<T>::plane_row_rend(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, 
			      const typename Volume<T>::size_type row)
  {
    return matrix_->plane_row_rend(P,plane_coordinate,row);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator1d 
  Volume<T>::plane_row_rend(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			      const typename Volume<T>::size_type row) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_row_rend(P,plane_coordinate,row);
  }

  template<typename T>
  inline
  typename Volume<T>::iterator1d 
  Volume<T>::plane_col_begin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			       const typename Volume<T>::size_type col)
  {
    return matrix_->plane_col_begin(P,plane_coordinate,col);
  }

  template<typename T>
  inline
  typename Volume<T>::const_iterator1d 
  Volume<T>::plane_col_begin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			       const typename Volume<T>::size_type col) const
  { 
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_col_begin(P,plane_coordinate,col);
  }
    
  template<typename T>
  inline
  typename Volume<T>::iterator1d 
  Volume<T>::plane_col_end(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, 
			     const typename Volume<T>::size_type col)
  {
    return matrix_->plane_col_end(P,plane_coordinate,col);
  }

  template<typename T>
  inline
  typename Volume<T>::const_iterator1d 
  Volume<T>::plane_col_end(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			     const typename Volume<T>::size_type col) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_col_end(P,plane_coordinate,col);
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_iterator1d 
  Volume<T>::plane_col_rbegin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
				const typename Volume<T>::size_type col)
  {
    return matrix_->plane_col_rbegin(P,plane_coordinate,col);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator1d 
  Volume<T>::plane_col_rbegin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
				const typename Volume<T>::size_type col) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_col_rbegin(P,plane_coordinate,col);
  }
  
  template<typename T>
  inline
  typename Volume<T>::reverse_iterator1d 
  Volume<T>::plane_col_rend(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, 
			      const typename Volume<T>::size_type col)
  { 
    return matrix_->plane_col_rend(P,plane_coordinate,col);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator1d 
  Volume<T>::plane_col_rend(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			      const typename Volume<T>::size_type col) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_col_rend(P,plane_coordinate,col);
  }

  //-------------------plane box iterators----------

  template<typename T>
  inline
  typename Volume<T>::const_iterator1d 
  Volume<T>::plane_row_begin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			      const size_type row, const Box2d<int> & b) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_row_begin(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::iterator1d 
  Volume<T>::plane_row_begin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			      const size_type row, const Box2d<int> & b)
  {
    return matrix_->plane_row_begin(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::iterator1d
  Volume<T>:: plane_row_end(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, 
			     const size_type row, const Box2d<int> & b)
  {
    return matrix_->plane_row_end(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_iterator1d 
  Volume<T>::plane_row_end(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			    const size_type row, const Box2d<int> & b) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_row_end(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::reverse_iterator1d
  Volume<T>::plane_row_rbegin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			       const size_type row, const Box2d<int> & b)
  {
    return matrix_->plane_row_rbegin(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator1d
  Volume<T>::plane_row_rbegin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			       const size_type row, const Box2d<int> & b) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_row_rbegin(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::reverse_iterator1d
  Volume<T>::plane_row_rend(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			     const size_type row, const Box2d<int> & b)
  {
    return matrix_->plane_row_rend(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator1d
  Volume<T>::plane_row_rend(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			     const size_type row, const Box2d<int> & b) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_row_rend(P,plane_coordinate,row,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::iterator1d
  Volume<T>::plane_col_begin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			      const size_type col, const Box2d<int> & b)
  {
    return matrix_->plane_col_begin(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_iterator1d
  Volume<T>::plane_col_begin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			      const size_type col, const Box2d<int> & b) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_col_begin(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::iterator1d
  Volume<T>::plane_col_end(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, 
			    const size_type col, const Box2d<int> & b)
  {
    return matrix_->plane_col_end(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_iterator1d
  Volume<T>::plane_col_end(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			    const size_type col, const Box2d<int> & b) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_col_end(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::reverse_iterator1d
  Volume<T>::plane_col_rbegin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			       const size_type col, const Box2d<int> & b)
  {
    return matrix_->plane_col_rbegin(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator1d
  Volume<T>::plane_col_rbegin(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			       const size_type col, const Box2d<int> & b) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_col_rbegin(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::reverse_iterator1d
  Volume<T>::plane_col_rend(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, 
			     const size_type col, const Box2d<int> & b)
  {
    return matrix_->plane_col_rend(P,plane_coordinate,col,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator1d
  Volume<T>::plane_col_rend(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate,
			     const size_type col, const Box2d<int> & b) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_col_rend(P,plane_coordinate,col,b);
  }

#endif //ALL_PLANE_ITERATOR3D

  //****************************************************************************
  //                          Two dimensional plane iterators
  //****************************************************************************
  
  //------------------------ Global iterators------------------------------------

  template<typename T>
  inline
  typename Volume<T>::iterator2d 
  Volume<T>::plane_upper_left(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate)
  {
    return matrix_->plane_upper_left(P,plane_coordinate);
  }
  
  template<typename T>
  inline
  typename Volume<T>::iterator2d 
  Volume<T>::plane_bottom_right(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate)
  { 
    return matrix_->plane_bottom_right(P,plane_coordinate);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_iterator2d 
  Volume<T>::plane_upper_left(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate) const
  { 
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_upper_left(P,plane_coordinate);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_iterator2d 
  Volume<T>::plane_bottom_right(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_bottom_right(P,plane_coordinate);
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_iterator2d 
  Volume<T>::plane_rupper_left(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate)
  {
    return matrix_->plane_rupper_left(P,plane_coordinate);
  }
  
  template<typename T>
  inline
  typename Volume<T>::reverse_iterator2d 
  Volume<T>::plane_rbottom_right(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate)
  { 
    return matrix_->plane_rbottom_right(P,plane_coordinate);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator2d 
  Volume<T>::plane_rupper_left(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate) const
  { 
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_rupper_left(P,plane_coordinate);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator2d 
  Volume<T>::plane_rbottom_right(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_rbottom_right(P,plane_coordinate);
  }

  //------------------------ Box iterators------------------------------------

  template<typename T>
  inline
  typename Volume<T>::iterator2d 
  Volume<T>::plane_upper_left(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, const Box2d<int>& b)
  {
    return matrix_->plane_upper_left(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::iterator2d 
  Volume<T>::plane_bottom_right(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, const Box2d<int>& b)
  { 
    return matrix_->plane_bottom_right(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_iterator2d 
  Volume<T>::plane_upper_left(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, const Box2d<int>& b) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_upper_left(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_iterator2d 
  Volume<T>::plane_bottom_right(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, const Box2d<int>& b) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_bottom_right(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::reverse_iterator2d 
  Volume<T>::plane_rupper_left(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, const Box2d<int>& b)
  {
    return matrix_->plane_rupper_left(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::reverse_iterator2d 
  Volume<T>::plane_rbottom_right(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, const Box2d<int>& b)
  { 
    return matrix_->plane_rbottom_right(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator2d 
  Volume<T>::plane_rupper_left(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, const Box2d<int>& b) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_rupper_left(P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator2d 
  Volume<T>::plane_rbottom_right(PLANE_ORIENTATION P, const typename Volume<T>::size_type plane_coordinate, const Box2d<int>& b) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->plane_rbottom_right(P,plane_coordinate,b);
  }

  //****************************************************************************
  //                          Three dimensional iterators
  //****************************************************************************
  
  //------------------------ Global iterators------------------------------------

  template<typename T>
  inline
  typename Volume<T>::iterator3d Volume<T>::front_upper_left()
  {
    return matrix_->front_upper_left();
  }

  template<typename T>
  inline
  typename Volume<T>::const_iterator3d Volume<T>::front_upper_left() const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->front_upper_left();
  }

  template<typename T>
  inline
  typename Volume<T>::iterator3d Volume<T>::back_bottom_right()
  {
    return matrix_->back_bottom_right();
  }

  template<typename T>
  inline
  typename Volume<T>::const_iterator3d Volume<T>::back_bottom_right() const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->back_bottom_right();
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_iterator3d 
  Volume<T>::rback_bottom_right()
  {
    return matrix_->rback_bottom_right();
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator3d 
  Volume<T>::rback_bottom_right() const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->rback_bottom_right();
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_iterator3d 
  Volume<T>::rfront_upper_left()
  {
    return matrix_->rfront_upper_left();
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator3d 
  Volume<T>::rfront_upper_left() const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->rfront_upper_left();
  }

  //------------------------ Box iterators------------------------------------

  template<typename T>
  inline
  typename Volume<T>::iterator3d Volume<T>::front_upper_left(const Box3d<int>& box)
  {
    return matrix_->front_upper_left(box);
  }

  template<typename T>
  inline
  typename Volume<T>::const_iterator3d Volume<T>::front_upper_left(const Box3d<int>& box) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->front_upper_left(box);
  }


  template<typename T>
  inline
  typename Volume<T>::iterator3d 
  Volume<T>::back_bottom_right(const Box3d<int>& box)
  {
    return matrix_->back_bottom_right(box);
  }

  template<typename T>
  inline
  typename Volume<T>::const_iterator3d 
  Volume<T>::back_bottom_right(const Box3d<int>& box) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->back_bottom_right(box);
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_iterator3d 
  Volume<T>::rback_bottom_right(const Box3d<int>& box)
  {
    return matrix_->rback_bottom_right(box);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator3d 
  Volume<T>::rback_bottom_right(const Box3d<int>& box) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->rback_bottom_right(box);
  }

  template<typename T>
  inline
  typename Volume<T>::reverse_iterator3d 
  Volume<T>::rfront_upper_left(const Box3d<int>& box)
  {
    return matrix_->rfront_upper_left(box);
  }

  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator3d 
  Volume<T>::rfront_upper_left(const Box3d<int>& box) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->rfront_upper_left(box);
  }

  //------------------------ Range iterators------------------------------------

  template<typename T>
  inline
  typename Volume<T>::iterator3d_range 
  Volume<T>::front_upper_left(const Range<int>& slice_range, 
			  const Range<int>& row_range,
			  const Range<int>& col_range)
  {
    return matrix_->front_upper_left(slice_range,row_range,col_range);
  }
  
  template<typename T>
  inline
  typename Volume<T>::iterator3d_range 
  Volume<T>::back_bottom_right(const Range<int>& slice_range,
			    const Range<int>& row_range,
			    const Range<int>& col_range)
  {
    return matrix_->back_bottom_right(slice_range,row_range,col_range);
  }
  

  template<typename T>
  inline
  typename Volume<T>::const_iterator3d_range 
  Volume<T>::front_upper_left(const Range<int>& slice_range,
			  const Range<int>& row_range,
			  const Range<int>& col_range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->front_upper_left(slice_range,row_range,col_range);
  }


  template<typename T>
  inline
  typename Volume<T>::const_iterator3d_range 
  Volume<T>::back_bottom_right(const Range<int>& slice_range,
			    const Range<int>& row_range,
			    const Range<int>& col_range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->back_bottom_right(slice_range,row_range,col_range);
  }
  
  template<typename T>
  inline
  typename Volume<T>::reverse_iterator3d_range 
  Volume<T>::rfront_upper_left(const Range<int>& slice_range,
			   const Range<int>& row_range,
			   const Range<int>& col_range)
  {
    return matrix_->rfront_upper_left(slice_range,row_range,col_range);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator3d_range 
  Volume<T>::rfront_upper_left(const Range<int>& slice_range,
			   const Range<int>& row_range,
			   const Range<int>& col_range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->rfront_upper_left(slice_range,row_range,col_range);
  }
  
  template<typename T>
  inline
  typename Volume<T>::reverse_iterator3d_range 
  Volume<T>::rback_bottom_right(const Range<int>& slice_range,
			     const Range<int>& row_range,
			     const Range<int>& col_range)
  {
    return matrix_->rback_bottom_right(slice_range,row_range,col_range);
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reverse_iterator3d_range 
  Volume<T>::rback_bottom_right(const Range<int>& slice_range,
			     const Range<int>& row_range,
			     const Range<int>& col_range) const
  {
    Matrix3d<T> const * tp(matrix_);
    return tp->rback_bottom_right(slice_range,row_range,col_range);
  }


  ///////////////////////////////////////////


  /** \name input/output functions */
  /* @{ */
  template <typename T>
  inline
  std::ostream& operator<<(std::ostream & out, const Volume<T>& a)
  {
    out<<*(a.matrix_);
    return out;
  }

  /* @} */
  #ifdef HAVE_MAGICK
  template<>
  inline
  void Volume<double>::read_from_images(const std::string& file_path_name,
					const std::size_t from,
					const std::size_t to)
  {
    
    slip::read_from_images_double(file_path_name,from,to,*this);
   //  assert(((to - from) + 1) > 0);
//     //read first slice
//     std::string file_name_img = slip::compose_file_name(file_path_name,from);
//     std::ifstream input(file_name_img.c_str(),std::ios::in);
//     if (!input) 
//       {
// 	std::cerr<<"Fail to open file "<<file_name_img<<std::endl;
// 	input.close();
// 	exit(1);
//       }
//     //Create the wand
//     MagickWand* wand = NewMagickWand();
//     //Read the image located at file_path_name
//     MagickReadImage(wand,file_name_img.c_str());
//     unsigned long width  = MagickGetImageWidth(wand);
//     unsigned long height = MagickGetImageHeight(wand);
//     std::cout<<width<<"x"<<height<<std::endl;
//     matrix_->resize((to-from) + 1,std::size_t(height),std::size_t(width),0);

//     MagickGetImagePixels(wand,0,0,width,height,"I",DoublePixel,matrix_->plane_begin(0));
//     wand = DestroyMagickWand(wand);

   
//     input.close();

//     for(std::size_t img = (from + 1); img <= to; ++img)
//       {
// 	file_name_img = slip::compose_file_name(file_path_name,img);
// 	std::ifstream input_i(file_name_img.c_str(),std::ios::in);
// 	if (!input_i) 
// 	  {
// 	    std::cerr<<"Fail to open file "<<file_name_img<<std::endl;
// 	    input_i.close();
// 	    exit(1);
// 	  }
// 	//Create the wand
// 	MagickWand* wand_i = NewMagickWand();
// 	//Read the image located at file_path_name
// 	MagickReadImage(wand_i,file_name_img.c_str());
// 	unsigned long width_i  = MagickGetImageWidth(wand_i);
// 	unsigned long height_i = MagickGetImageHeight(wand_i);
// 	std::cout<<width_i<<"x"<<height_i<<std::endl;
// 	assert(width  == width_i);
// 	assert(height == height_i);
// 	MagickGetImagePixels(wand_i,0,0,width_i,height_i,"I",DoublePixel,matrix_->plane_begin(img - from));
// 	wand_i = DestroyMagickWand(wand_i);
	
// 	input_i.close();
	
//       }
  }

  template<>
  inline
  void Volume<float>::read_from_images(const std::string& file_path_name,
				       const std::size_t from,
				       const std::size_t to)
  {
    slip::read_from_images_float(file_path_name,from,to,*this);
  }


  template<>
  inline
  void Volume<long>::read_from_images(const std::string& file_path_name,
				      const std::size_t from,
				      const std::size_t to)
  {
    slip::read_from_images_long(file_path_name,from,to,*this);
  }


  template<>
  inline
  void Volume<int>::read_from_images(const std::string& file_path_name,
				     const std::size_t from,
				     const std::size_t to)
  {
    slip::read_from_images_int(file_path_name,from,to,*this);
  }


  template<>
  inline
  void Volume<short>::read_from_images(const std::string& file_path_name,
				       const std::size_t from,
				       const std::size_t to)
  {
    slip::read_from_images_short(file_path_name,from,to,*this);
   }


  template<>
  inline
  void Volume<unsigned char>::read_from_images(const std::string& file_path_name,
					       const std::size_t from,
					       const std::size_t to)
  {
    slip::read_from_images_char(file_path_name,from,to,*this);
  }


  template<>
  inline
  void Volume<double>::write_to_images(const std::string& file_path_name,
				       const std::size_t from,
				       const std::size_t to) const
  {

    std::vector<slip::Volume<double>::const_iterator> planes((to - from) + 1);
    for(std::size_t i = from; i <= to; ++i)
      {
	planes[i-from] = this->plane_begin(i);
      }
    slip::write_to_images_double(file_path_name,from,to,this->cols(),this->rows(),planes);
   
  }

  template<>
  inline
  void Volume<float>::write_to_images(const std::string& file_path_name,
				      const std::size_t from,
				      const std::size_t to) const
  {
     std::vector<slip::Volume<float>::const_iterator> planes((to - from) + 1);
    for(std::size_t i = from; i <= to; ++i)
      {
	planes[i-from] = this->plane_begin(i);
      }
    slip::write_to_images_float(file_path_name,from,to,this->cols(),this->rows(),planes);   
  }

  template<>
  inline
  void Volume<long>::write_to_images(const std::string& file_path_name,
				     const std::size_t from,
				     const std::size_t to) const
  {
    std::vector<slip::Volume<long>::const_iterator> planes((to - from) + 1);
    for(std::size_t i = from; i <= to; ++i)
      {
	planes[i-from] = this->plane_begin(i);
      }
    slip::write_to_images_long(file_path_name,from,to,this->cols(),this->rows(),planes);   
  }

  template<>
  inline
  void Volume<int>::write_to_images(const std::string& file_path_name,
				    const std::size_t from,
				    const std::size_t to) const
  {
    std::vector<slip::Volume<int>::const_iterator> planes((to - from) + 1);
    for(std::size_t i = from; i <= to; ++i)
      {
	planes[i-from] = this->plane_begin(i);
      }
    slip::write_to_images_int(file_path_name,from,to,this->cols(),this->rows(),planes);  
   
  }

  template<>
  inline
  void Volume<short>::write_to_images(const std::string& file_path_name,
				      const std::size_t from,
				      const std::size_t to) const
  {
     std::vector<slip::Volume<short>::const_iterator> planes((to - from) + 1);
    for(std::size_t i = from; i <= to; ++i)
      {
	planes[i-from] = this->plane_begin(i);
      }
    slip::write_to_images_short(file_path_name,from,to,this->cols(),this->rows(),planes);     
  }


  template<>
  inline
  void Volume<unsigned char>::write_to_images(const std::string& file_path_name,
					      const std::size_t from,
					      const std::size_t to) const
  {
    std::vector<slip::Volume<unsigned char>::const_iterator> planes((to - from) + 1);
    for(std::size_t i = from; i <= to; ++i)
      {
	planes[i-from] = this->plane_begin(i);
      }
    slip::write_to_images_char(file_path_name,from,to,this->cols(),this->rows(),planes);    
   
  }
#endif //HAVE_MAGICK
  
  template<typename T>
  inline
  void Volume<T>::read_raw(const std::string& file_path_name,
			   const std::size_t slices,
			   const std::size_t rows,
			   const std::size_t cols)
  {
    matrix_->resize(slices,rows,cols,0);
    slip::read_raw(file_path_name,this->begin(),this->end());
  }


  template<typename T>
  inline
  void Volume<T>::write_raw(const std::string& file_path_name) const
  {
    slip::write_raw(this->begin(),this->end(),file_path_name);
  }

  template<typename T>
  inline
  void Volume<T>::write_tecplot(const std::string& file_path_name,
				const std::string& title) const
  {
    slip::write_tecplot_vol(*this,file_path_name,title);
  }

  template<typename T>
  inline
  void Volume<T>::write_to_images_tecplot(const std::string& file_path_name,
					  const std::string& title,
					  const std::size_t from,
					  const std::size_t to) const
  {
    slip::write_to_images_tecplot(*this,file_path_name,title,from,to);
  }
  ///////////////////////////////////////////
  

  ////////////////////////////////////////////
  // Elements access operators
  ////////////////////////////////////////////
  template<typename T>
  inline
  T** 
  Volume<T>::operator[](const typename Volume<T>::size_type k)
  {
    return (*matrix_)[k];
  }
  
  template<typename T>
  inline
  const T* const*
  Volume<T>::operator[](const typename Volume<T>::size_type k) const 
  {
    return (*matrix_)[k];
  }
  
  template<typename T>
  inline
  typename Volume<T>::reference 
  Volume<T>::operator()(const typename Volume<T>::size_type k,
			const typename Volume<T>::size_type i,
			const typename Volume<T>::size_type j)
  {
    return (*matrix_)[k][i][j];
  }
  
  template<typename T>
  inline
  typename Volume<T>::const_reference 
  Volume<T>::operator()(const typename Volume<T>::size_type k,
			const typename Volume<T>::size_type i,
			const typename Volume<T>::size_type j) const
  {
    return (*matrix_)[k][i][j];
  }
  
  ///////////////////////////////////////////
  
  template<typename T>
  inline
  std::string 
  Volume<T>::name() const {return "Volume";} 
 
  
  template<typename T>
  inline
  typename Volume<T>::size_type 
  Volume<T>::dim1() const {return matrix_->dim1();} 

  template<typename T>
  inline
  typename Volume<T>::size_type 
  Volume<T>::slices() const {return matrix_->dim1();} 

  template<typename T>
  inline
  typename Volume<T>::size_type 
  Volume<T>::dim2() const {return matrix_->dim2();} 

  template<typename T>
  inline
  typename Volume<T>::size_type 
  Volume<T>::rows() const {return matrix_->dim2();} 

  template<typename T>
  inline
  typename Volume<T>::size_type 
  Volume<T>::dim3() const {return matrix_->dim3();;} 

  template<typename T>
  inline
  typename Volume<T>::size_type 
  Volume<T>::cols() const {return matrix_->dim3();} 

  template<typename T>
  inline
  typename Volume<T>::size_type 
  Volume<T>::columns() const {return matrix_->dim3();;} 


  template<typename T>
  inline
  typename Volume<T>::size_type 
  Volume<T>::size() const {return matrix_->size();}

  template<typename T>
  inline
  typename Volume<T>::size_type 
  Volume<T>::max_size() const {return matrix_->max_size();}

  template<typename T>
  inline
  typename Volume<T>::size_type 
  Volume<T>::slice_size() const {return matrix_->slice_size();}


  template<typename T>
  inline
  bool Volume<T>::empty()const {return matrix_->empty();}

  template<typename T>
  inline
  void Volume<T>::swap(Volume<T>& M)
  {
    matrix_->swap(*(M.matrix_));
  }


   /** \name EqualityComparable functions */
  /* @{ */
  template<typename T>
  inline
  bool operator==(const Volume<T>& x, 
		  const Volume<T>& y)
  {
    return ( x.size() == y.size()
	     && std::equal(x.begin(),x.end(),y.begin())); 
  }

  template<typename T>
  inline
  bool operator!=(const Volume<T>& x, 
		  const Volume<T>& y)
  {
    return !(x == y);
  }
  /* @} */

  
  /** \name LessThanComparable functions */
  /* @{ */
  template<typename T>
  inline
  bool operator<(const Volume<T>& x, 
		 const Volume<T>& y)
  {
    return std::lexicographical_compare(x.begin(), x.end(),
					y.begin(), y.end());
  }
  
  
  template<typename T>
  inline
  bool operator>(const Volume<T>& x, 
		 const Volume<T>& y)
  {
    return (y < x);
  }
  
  template<typename T>
  inline
  bool operator<=(const Volume<T>& x, 
		  const Volume<T>& y)
  {
    return !(y < x);
  }

  template<typename T>
  inline
  bool operator>=(const Volume<T>& x, 
		  const Volume<T>& y)
  {
    return !(x < y);
  } 
  /* @} */
  ////////////////////////////////////////////


  ////////////////////////////////////////////
  // arithmetic and mathematic operators
  ////////////////////////////////////////////
  template<typename T>
  inline
  Volume<T>& Volume<T>::operator+=(const T& val)
  {
    std::transform(matrix_->begin(),matrix_->end(),matrix_->begin(),std::bind2nd(std::plus<T>(),val));
    return *this;
  }
  
  template<typename T>
  inline
  Volume<T>& Volume<T>::operator-=(const T& val)
  {
    std::transform(matrix_->begin(),matrix_->end(),matrix_->begin(),std::bind2nd(std::minus<T>(),val));
    return *this;
  }

  template<typename T>
  inline
  Volume<T>& Volume<T>::operator*=(const T& val)
  {
    std::transform(matrix_->begin(),matrix_->end(),matrix_->begin(),std::bind2nd(std::multiplies<T>(),val));
    return *this;
  }
  
  template<typename T>
  inline
  Volume<T>& Volume<T>::operator/=(const T& val)
  {
    std::transform(matrix_->begin(),matrix_->end(),matrix_->begin(),std::bind2nd(std::multiplies<T>(),T(1)/val));
    return *this;
  }
  
  template<typename T>
  inline
  Volume<T> Volume<T>::operator-() const
  {
    Volume<T> tmp(*this);
    std::transform(matrix_->begin(),matrix_->end(),tmp.begin(),std::negate<T>());
    return tmp;
  }

  template<typename T>
  inline
  Volume<T>& Volume<T>::operator+=(const Volume<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    assert(this->dim3() == rhs.dim3());
   
    std::transform(matrix_->begin(),matrix_->end(),(*(rhs.matrix_)).begin(),matrix_->begin(),std::plus<T>());
    return *this;
  }
  
  template<typename T>
  inline
  Volume<T>& Volume<T>::operator-=(const Volume<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    assert(this->dim3() == rhs.dim3());
    std::transform(matrix_->begin(),matrix_->end(),(*(rhs.matrix_)).begin(),matrix_->begin(),std::minus<T>());
    return *this;
  }
  
  template<typename T>
  inline
  Volume<T>& Volume<T>::operator*=(const Volume<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    assert(this->dim3() == rhs.dim3());
    std::transform(matrix_->begin(),matrix_->end(),(*(rhs.matrix_)).begin(),matrix_->begin(),std::multiplies<T>());
    return *this;
  }
  
  template<typename T>
  inline
  Volume<T>& Volume<T>::operator/=(const Volume<T>& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(matrix_->begin(),matrix_->end(),(*(rhs.matrix_)).begin(),matrix_->begin(),std::divides<T>());
    return *this;
  }
	
 

  template<typename T>
  inline
  T& Volume<T>::min() const
  {
    assert(matrix_->size() != 0);
    return *std::min_element(matrix_->begin(),matrix_->end());
  }

  template<typename T>
  inline
  T& Volume<T>::max() const
  {
    assert(matrix_->size() != 0);
    return *std::max_element(matrix_->begin(),matrix_->end());
  }

  template<typename T>
  inline
  T Volume<T>::sum() const
  {
    assert(matrix_->size() != 0);
    return std::accumulate(matrix_->begin(),matrix_->end(),T());
  }

  
  template<typename T>
  inline
  Volume<T>& Volume<T>::apply(T (*fun)(T))
  {
    slip::apply(this->begin(),this->end(),this->begin(),fun);
    return *this;
  }

  template<typename T>
  inline
  Volume<T>& Volume<T>::apply(T (*fun)(const T&))
  {
    slip::apply(this->begin(),this->end(),this->begin(),fun);
    return *this;
  }


 

  template<typename T>
  inline
  Volume<T> operator+(const Volume<T>& M1, 
		      const Volume<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    assert(M1.dim3() == M2.dim3());
 
    Volume<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::plus<T>());
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> operator+(const Volume<T>& M1, 
		      const T& val)
  {
    Volume<T> tmp(M1);
    tmp+=val;
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> operator+(const T& val,
		      const Volume<T>& M1)
  {
    return M1 + val;
  }


  template<typename T>
  inline
  Volume<T> operator-(const Volume<T>& M1, 
		      const Volume<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    assert(M1.dim3() == M2.dim3());
      
    Volume<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::minus<T>());
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> operator-(const Volume<T>& M1, 
		      const T& val)
  {
    Volume<T> tmp(M1);
    tmp-=val;
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> operator-(const T& val,
		      const Volume<T>& M1)
  {
    return -(M1 - val);
  }

  template<typename T>
  inline
  Volume<T> operator*(const Volume<T>& M1, 
		      const Volume<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    assert(M1.dim3() == M2.dim3());
 
    Volume<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::multiplies<T>());
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> operator*(const Volume<T>& M1, 
		      const T& val)
  {
    Volume<T> tmp(M1);
    tmp*=val;
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> operator*(const T& val,
		      const Volume<T>& M1)
  {
    return M1 * val;
  }

  template<typename T>
  inline
  Volume<T> operator/(const Volume<T>& M1, 
		      const Volume<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    assert(M1.dim3() == M2.dim3());
 
    Volume<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::divides<T>());
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> operator/(const Volume<T>& M1, 
		      const T& val)
  {
    Volume<T> tmp(M1);
    tmp/=val;
    return tmp;
  }


  template<typename T>
  inline
  T& min(const Volume<T>& M1)
  {
    return M1.min();
  }

  template<typename T>
  inline
  T& max(const Volume<T>& M1)
  {
    return M1.max();
  }

  template<typename T>
  inline
  Volume<T> abs(const Volume<T>& M)
  {
  
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::abs);
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> sqrt(const Volume<T>& M)
  {
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::sqrt);
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> cos(const Volume<T>& M)
  {
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::cos);
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> acos(const Volume<T>& M)
  {
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::acos);
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> sin(const Volume<T>& M)
  {
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::sin);
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> asin(const Volume<T>& M)
  {
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::asin);
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> tan(const Volume<T>& M)
  {
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::tan);
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> atan(const Volume<T>& M)
  {
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::atan);
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> exp(const Volume<T>& M)
  {
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::exp);
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> log(const Volume<T>& M)
  {
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::log);
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> cosh(const Volume<T>& M)
  {
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::cosh);
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> sinh(const Volume<T>& M)
  {
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::sinh);
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> tanh(const Volume<T>& M)
  {
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::tanh);
    return tmp;
  }

  template<typename T>
  inline
  Volume<T> log10(const Volume<T>& M)
  {
    Volume<T> tmp(M.dim1(),M.dim2(),M.dim3());
    slip::apply(M.begin(),M.end(),tmp.begin(),std::log10);
    return tmp;
  }


 
}//slip::

#endif //SLIP_VOLUME_HPP
