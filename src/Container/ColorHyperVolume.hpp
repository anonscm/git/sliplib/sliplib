/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/*!
 ** \file ColorHyperVolume.hpp
 ** \brief Provides a class to manipulate Color volumes.
 ** \version Fluex 1.0
 ** \date 2013/03/18
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 */

#ifndef SLIP_COLORHYPERVOLUME_HPP_
#define SLIP_COLORHYPERVOLUME_HPP_

#include "Color.hpp"
#include "GenericMultiComponent4d.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/base_object.hpp>

namespace slip {

template <typename T>
class ColorHyperVolume;

/*!
 ** \ingroup Containers Containers4d Fluex MultiComponent4dContainers
 ** \class ColorHyperVolume
 ** \version Fluex 1.0
 ** \date 2013/03/18
 ** \since 1.4.0
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \brief This is a color hyper volume (4D) class.
 ** This container defines STL Bidirectionnal iterators begin and end.
 ** It defines also 4d extensions of the RandomAccess Iterators called first_front_upper_left and last_back_bottom_right(). As a consequence,
 ** the bracket element access is replaced by the triple bracket element access.
 ** It is a specialization of GenericMulitComponent4d using Color blocks.
 ** \author Denis Arrivault <denis.arrivault_AT_inria.fr>
 ** \date 2013/08/28
 ** \param T Type of object in the ColorHyperVolume
 ** \par Axis conventions:
 ** \image html iterator4d_conventions.jpg "axis and notation conventions"
 ** \image latex iterator4d_conventions.eps "axis and notation conventions" width=5cm
 */

template <typename T>
class ColorHyperVolume:public slip::GenericMultiComponent4d<slip::Color<T> >
{
public :

	typedef slip::Color<T> value_type;
	typedef ColorHyperVolume<T> self;
	typedef slip::GenericMultiComponent4d<slip::Color<T> > base;

	typedef value_type* pointer;
	typedef const value_type* const_pointer;
	typedef value_type& reference;
	typedef const value_type& const_reference;

	typedef T color_value_type;
	typedef color_value_type* color_pointer;
	typedef const color_value_type* const_color_pointer;
	typedef color_value_type& color_reference;
	typedef const color_value_type const_color_reference;
	typedef slip::kstride_iterator<color_pointer,3> color_iterator;
	typedef slip::kstride_iterator<const_color_pointer,3> const_color_iterator;

	typedef ptrdiff_t difference_type;
	typedef std::size_t size_type;

	static const std::size_t DIM = 4;

	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** \brief Constructs a %ColorHyperVolume.
	 */
	ColorHyperVolume():
		base()
	{}

	/*!
	 ** \brief Constructs a %ColorHyperVolume.
	 ** \param slabs first dimension of the %ColorHyperVolume
	 ** \param slices second dimension of the %ColorHyperVolume
	 ** \param rows third dimension of the %ColorHyperVolume
	 ** \param cols fourth dimension of the %ColorHyperVolume
	 ** \pre if slice is
	 */
	ColorHyperVolume(const size_type slabs, const size_type slices, const size_type rows,
			const size_type cols):
				base(slabs,slices,rows,cols)
	{}

	/*!
	 ** \brief Constructs a %ColorHyperVolume initialized by the scalar value \a val.
	 ** \param slabs first dimension of the %ColorHyperVolume
	 ** \param slices second dimension of the %ColorHyperVolume
	 ** \param rows third dimension of the %ColorHyperVolume
	 ** \param cols fourth dimension of the %ColorHyperVolume
	 ** \param val initialization value of the elements
	 */
	ColorHyperVolume(const size_type slabs, const size_type slices, const size_type rows,
			const size_type cols, const slip::Color<T>& val):
				base(slabs,slices,rows,cols,val)
	{}

	/*!
	 ** \brief Constructs a %ColorHyperVolume initialized by an array \a val.
	 ** \param slabs first dimension of the %ColorHyperVolume
	 ** \param slices second dimension of the %ColorHyperVolume
	 ** \param rows third dimension of the %ColorHyperVolume
	 ** \param cols fourth dimension of the %ColorHyperVolume
	 ** \param val initialization linear array value of the elements
	 */
	ColorHyperVolume(const size_type slabs, const size_type slices, const size_type rows,
			const size_type cols, const T* val):
				base(slabs,slices,rows,cols,val)
	{}

	/*!
	 ** \brief Constructs a %ColorHyperVolume initialized by an array \a val.
	 ** \param slabs first dimension of the %ColorHyperVolume
	 ** \param slices second dimension of the %ColorHyperVolume
	 ** \param rows third dimension of the %ColorHyperVolume
	 ** \param cols fourth dimension of the %ColorHyperVolume
	 ** \param val initialization array value of the elements
	 */
	ColorHyperVolume(const size_type slabs, const size_type slices, const size_type rows,
			const size_type cols, const slip::Color<T>* val):
				base(slabs,slices,rows,cols,val)
	{}


	/**
	 ** \brief  Contructs a %ColorHyperVolume from a range.
	 ** \param slabs first dimension of the %ColorHyperVolume
	 ** \param slices second dimension of the %ColorHyperVolume
	 ** \param rows third dimension of the %ColorHyperVolume
	 ** \param cols fourth dimension of the %ColorHyperVolume
	 ** \param  first  An input iterator.
	 ** \param  last  An input iterator.
	 **
	 ** Create a %ColorHyperVolume consisting of copies of the elements from
	 ** [first,last).
	 */
	template<typename InputIterator>
	ColorHyperVolume(const size_type slabs, const size_type slices, const size_type rows,
			const size_type cols, InputIterator first, InputIterator last):
			base(slabs,slices,rows,cols,first,last)
			{}

	/**
	 ** \brief  Contructs a %ColorHyperVolume from a 3 ranges.
	 ** \param slabs first dimension of the %ColorHyperVolume
	 ** \param slices second dimension of the %ColorHyperVolume
	 ** \param rows third dimension of the %ColorHyperVolume
	 ** \param cols fourth dimension of the %ColorHyperVolume
	 ** \param  first1  An input iterator.
	 ** \param  last1  An input iterator.
	 ** \param  first2  An input iterator.
	 ** \param  first3  An input iterator.
	 **
	 ** Create a %ColorHyperVolume consisting of copies of the elements from
	 ** [first1,last1),
	 ** [first2,first2 + (last1 - first1)),
	 ** [first3, first3 + (last1 - first1).
	 ** */
	template<typename InputIterator>
	ColorHyperVolume(const size_type slabs, const size_type slices, const size_type rows,
			const size_type cols, InputIterator first1, InputIterator last1,
			InputIterator first2, InputIterator first3):
			base(slabs,slices,rows,cols)
			{
		std::vector<InputIterator> first_iterators_list(4);
		first_iterators_list[0] = first1;
		first_iterators_list[1] = first2;
		first_iterators_list[2] = first3;
		this->fill(first_iterators_list,last1);
			}


	/*!
	 ** \brief Constructs a copy of the %ColorHyperVolume \a rhs
	 */
	ColorHyperVolume(const self& rhs):
		base(rhs)
	{}

	/*!
	 ** \brief Destructor of the %ColorHyperVolume
	 */
	~ColorHyperVolume()
	{}


	/*@} End Constructors */

	/*!
	 ** \brief Returns the name of the class
	 **
	 */
	std::string name() const;

	/**
	 ** \name  Assignment operators and methods
	 */
	/*@{*/

	/*!
	 ** \brief Affects all the element of the %ColorHyperVolume by val
	 ** \param val affectation value
	 ** \return reference to corresponding %ColorHyperVolume
	 */
	self& operator=(const slip::Color<T>& val);


	/*!
	 ** \brief Affects all the element of the %ColorHyperVolume by val
	 ** \param val affectation value
	 ** \return reference to corresponding %ColorHyperVolume
	 */
	self& operator=(const T& val);


	/*@} End Assignment operators and methods*/

	/**
	 ** \name  Arithmetic operators
	 */
	/*@{*/


	/*!
	 ** \brief Add val to each element of the %ColorHyperVolume
	 ** \param val value
	 ** \return reference to the resulting %ColorHyperVolume
	 */
	self& operator+=(const T& val);
	self& operator-=(const T& val);
	self& operator*=(const T& val);
	self& operator/=(const T& val);



	self  operator-() const;

	/*!
	 ** \brief Add val to each element of the %ColorHyperVolume
	 ** \param val value
	 ** \return reference to the resulting %ColorHyperVolume
	 */
	self& operator+=(const slip::Color<T>& val);
	self& operator-=(const slip::Color<T>& val);
	self& operator*=(const slip::Color<T>& val);
	self& operator/=(const slip::Color<T>& val);


	self& operator+=(const self& rhs);
	self& operator-=(const self& rhs);
	self& operator*=(const self& rhs);
	self& operator/=(const self& rhs);


	/*@} End Arithmetic operators */

  private :
   friend class boost::serialization::access;
 template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::GenericMultiComponent4d<slip::Color<T> > >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::GenericMultiComponent4d<slip::Color<T> > >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()

};

///double alias
typedef slip::ColorHyperVolume<double> ColorHyperVolume_d;
///float alias
typedef slip::ColorHyperVolume<float> ColorHyperVolume_f;
///long alias
typedef slip::ColorHyperVolume<long> ColorHyperVolume_l;
///unsigned long alias
typedef slip::ColorHyperVolume<unsigned long> ColorHyperVolume_ul;
///short alias
typedef slip::ColorHyperVolume<short> ColorHyperVolume_s;
///unsigned long alias
typedef slip::ColorHyperVolume<unsigned short> ColorHyperVolume_us;
///int alias
typedef slip::ColorHyperVolume<int> ColorHyperVolume_i;
///unsigned int alias
typedef slip::ColorHyperVolume<unsigned int> ColorHyperVolume_ui;
///char alias
typedef slip::ColorHyperVolume<char> ColorHyperVolume_c;
///unsigned char alias
typedef slip::ColorHyperVolume<unsigned char> ColorHyperVolume_uc;

/*!
 ** \brief pointwise addition of two %ColorHyperVolume
 ** \param M1 first %ColorHyperVolume
 ** \param M2 second %ColorHyperVolume
 ** \pre M1.dim1() == M2.dim1()
 ** \pre M1.dim2() == M2.dim2()
 ** \pre M1.dim3() == M2.dim3()
 ** \pre M1.dim4() == M2.dim4()
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator+(const ColorHyperVolume<T>& M1,
		const ColorHyperVolume<T>& M2);

/*!
 ** \brief addition of a scalar to each element of a %ColorHyperVolume
 ** \param M1 the %ColorHyperVolume
 ** \param val the scalar
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator+(const ColorHyperVolume<T>& M1,
		const T& val);

/*!
 ** \brief addition of a scalar to each element of a %ColorHyperVolume
 ** \param val the scalar
 ** \param M1 the %ColorHyperVolume
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator+(const T& val,
		const ColorHyperVolume<T>& M1);

/*!
 ** \brief addition of a %Color block to each element of a %ColorHyperVolume
 ** \param M1 the %ColorHyperVolume
 ** \param val the %Color block
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator+(const ColorHyperVolume<T>& M1,
		const slip::Color<T>& val);

/*!
 ** \brief addition of a %Color block to each element of a %ColorHyperVolume
 ** \param val the %Color block
 ** \param M1 the %ColorHyperVolume
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator+(const slip::Color<T>& val,
		const ColorHyperVolume<T>& M1);

/*!
 ** \brief pointwise subtraction of two %ColorHyperVolume
 ** \param M1 first %ColorHyperVolume
 ** \param M2 second %ColorHyperVolume
 ** \pre M1.dim1() == M2.dim1()
 ** \pre M1.dim2() == M2.dim2()
 ** \pre M1.dim3() == M2.dim3()
 ** \pre M1.dim4() == M2.dim4()
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator-(const ColorHyperVolume<T>& M1,
		const ColorHyperVolume<T>& M2);

/*!
 ** \brief subtraction of a scalar to each element of a %ColorHyperVolume
 ** \param M1 the %ColorHyperVolume
 ** \param val the scalar
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator-(const ColorHyperVolume<T>& M1,
		const T& val);

/*!
 ** \brief subtraction of a scalar to each element of a %ColorHyperVolume
 ** \param val the scalar
 ** \param M1 the %ColorHyperVolume
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator-(const T& val,
		const ColorHyperVolume<T>& M1);

/*!
 ** \brief subtraction of a %Color block to each element of a %ColorHyperVolume
 ** \param M1 the %ColorHyperVolume
 ** \param val the %Color block
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator-(const ColorHyperVolume<T>& M1,
		const slip::Color<T>& val);

/*!
 ** \brief pointwise multiplication of two %ColorHyperVolume
 ** \param M1 first %ColorHyperVolume
 ** \param M2 second %ColorHyperVolume
 ** \pre M1.dim1() == M2.dim1()
 ** \pre M1.dim2() == M2.dim2()
 ** \pre M1.dim3() == M2.dim3()
 ** \pre M1.dim4() == M2.dim4()
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator*(const ColorHyperVolume<T>& M1,
		const ColorHyperVolume<T>& M2);

/*!
 ** \brief multiplication of a scalar to each element of a %ColorHyperVolume
 ** \param M1 the %ColorHyperVolume
 ** \param val the scalar
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator*(const ColorHyperVolume<T>& M1,
		const T& val);

/*!
 ** \brief multiplication of a scalar to each element of a %ColorHyperVolume
 ** \param val the scalar
 ** \param M1 the %ColorHyperVolume
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator*(const T& val,
		const ColorHyperVolume<T>& M1);

/*!
 ** \brief multiplication of a %Color block to each element of a %ColorHyperVolume
 ** \param M1 the %ColorHyperVolume
 ** \param val the %Color block
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator*(const ColorHyperVolume<T>& M1,
		const slip::Color<T>& val);

/*!
 ** \brief multiplication of a %Color block to each element of a %ColorHyperVolume
 ** \param val the %Color block
 ** \param M1 the %ColorHyperVolume
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator*(const slip::Color<T>& val,
		const ColorHyperVolume<T>& M1);

/*!
 ** \brief pointwise division of two %ColorHyperVolume
 ** \param M1 first %ColorHyperVolume
 ** \param M2 seconf %ColorHyperVolume
 ** \pre M1.dim1() == M2.dim1()
 ** \pre M1.dim2() == M2.dim2()
 ** \pre M1.dim3() == M2.dim3()
 ** \pre M1.dim4() == M2.dim4()
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator/(const ColorHyperVolume<T>& M1,
		const ColorHyperVolume<T>& M2);

/*!
 ** \brief division of a scalar to each element of a %ColorHyperVolume
 ** \param M1 the %ColorHyperVolume
 ** \param val the scalar
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator/(const ColorHyperVolume<T>& M1,
		const T& val);

/*!
 ** \brief division of each element of a %ColorHyperVolume by a %Color block
 ** \param M1 the %ColorHyperVolume
 ** \param val the %Color block
 ** \return resulting %ColorHyperVolume
 */
template<typename T>
ColorHyperVolume<T> operator/(const ColorHyperVolume<T>& M1,
		const slip::Color<T>& val);

}  // namespace slip

namespace slip {

template<typename T>
inline
std::string ColorHyperVolume<T>::name() const{
	return "ColorHyperVolume";
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator=(const slip::Color<T>& val){
	if(this->size() > 0)
		std::fill_n(this->begin(),this->size(),val);
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator=(const T& val){
	if(this->size() > 0)
		std::fill_n(this->begin(),this->size(),val);
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator+=(const T& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::plus<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator-=(const T& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::minus<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator*=(const T& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::multiplies<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator/=(const T& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::divides<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T> ColorHyperVolume<T>::operator-() const{
	ColorHyperVolume<T> tmp(*this);
	std::transform(this->begin(),this->end(),tmp.begin(),std::negate<slip::Color<T> >());
	return tmp;
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator+=(const slip::Color<T>& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::plus<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator-=(const slip::Color<T>& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::minus<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator*=(const slip::Color<T>& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::multiplies<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator/=(const slip::Color<T>& val){
	std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::divides<slip::Color<T> >(),val));
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator+=(const ColorHyperVolume<T>& rhs){
	assert(this->dim1() == rhs.dim1());
	assert(this->dim2() == rhs.dim2());
	assert(this->dim3() == rhs.dim3());
	assert(this->dim4() == rhs.dim4());
	std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::plus<slip::Color<T> >());
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator-=(const ColorHyperVolume<T>& rhs){
	assert(this->dim1() == rhs.dim1());
	assert(this->dim2() == rhs.dim2());
	assert(this->dim3() == rhs.dim3());
	assert(this->dim4() == rhs.dim4());
	std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::minus<slip::Color<T> >());
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator*=(const ColorHyperVolume<T>& rhs){
	assert(this->dim1() == rhs.dim1());
	assert(this->dim2() == rhs.dim2());
	assert(this->dim3() == rhs.dim3());
	assert(this->dim4() == rhs.dim4());
	std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::multiplies<slip::Color<T> >());
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T>& ColorHyperVolume<T>::operator/=(const ColorHyperVolume<T>& rhs){
	assert(this->dim1() == rhs.dim1());
	assert(this->dim2() == rhs.dim2());
	assert(this->dim3() == rhs.dim3());
	assert(this->dim4() == rhs.dim4());
	std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::divides<slip::Color<T> >());
	return *this;
}

template<typename T>
inline
ColorHyperVolume<T> operator+(const ColorHyperVolume<T>& M1,
		const ColorHyperVolume<T>& M2){
	assert(M1.dim1() == M2.dim1());
	assert(M1.dim2() == M2.dim2());
	assert(M1.dim3() == M2.dim3());
	assert(M1.dim4() == M2.dim4());
	ColorHyperVolume<T> tmp(M1.dim1(),M1.dim2(),M1.dim3(),M1.dim4());
	std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::plus<slip::Color<T> >());
	return tmp;
}

template<typename T>
inline
ColorHyperVolume<T> operator+(const ColorHyperVolume<T>& M1,
		const T& val){
	ColorHyperVolume<T> tmp = M1;
	tmp += val;
	return tmp;
}

template<typename T>
inline
ColorHyperVolume<T> operator+(const T& val,
		const ColorHyperVolume<T>& M1){
	return M1 + val;
}

template<typename T>
inline
ColorHyperVolume<T> operator+(const ColorHyperVolume<T>& M1,
		const slip::Color<T>& val){
	ColorHyperVolume<T>  tmp = M1;
	tmp += val;
	return tmp;
}

template<typename T>
inline
ColorHyperVolume<T> operator+(const slip::Color<T>& val,
		const ColorHyperVolume<T>& M1){
	return M1 + val;
}

template<typename T>
inline
ColorHyperVolume<T> operator-(const ColorHyperVolume<T>& M1,
		const ColorHyperVolume<T>& M2){
	assert(M1.dim1() == M2.dim1());
	assert(M1.dim2() == M2.dim2());
	assert(M1.dim3() == M2.dim3());
	assert(M1.dim4() == M2.dim4());
	ColorHyperVolume<T> tmp(M1.dim1(),M1.dim2(),M1.dim3(),M1.dim4());
	std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::minus<slip::Color<T> >());
	return tmp;
}

template<typename T>
inline
ColorHyperVolume<T> operator-(const ColorHyperVolume<T>& M1,
		const T& val){
	ColorHyperVolume<T>  tmp = M1;
	tmp -= val;
	return tmp;
}

template<typename T>
inline
ColorHyperVolume<T> operator-(const T& val,
		const ColorHyperVolume<T>& M1){
	return -(M1 - val);
}

template<typename T>
inline
ColorHyperVolume<T> operator-(const ColorHyperVolume<T>& M1,
		const slip::Color<T>& val){
	ColorHyperVolume<T> tmp = M1;
	tmp -= val;
	return tmp;
}

template<typename T>
inline
ColorHyperVolume<T> operator*(const ColorHyperVolume<T>& M1,
		const ColorHyperVolume<T>& M2){
	assert(M1.dim1() == M2.dim1());
	assert(M1.dim2() == M2.dim2());
	assert(M1.dim3() == M2.dim3());
	assert(M1.dim4() == M2.dim4());
	ColorHyperVolume<T> tmp(M1.dim1(),M1.dim2(),M1.dim3(),M1.dim4());
	std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::multiplies<slip::Color<T> >());
	return tmp;
}

template<typename T>
inline
ColorHyperVolume<T> operator*(const ColorHyperVolume<T>& M1,
		const T& val){
	ColorHyperVolume<T>  tmp = M1;
	tmp *= val;
	return tmp;
}

template<typename T>
inline
ColorHyperVolume<T> operator*(const T& val,
		const ColorHyperVolume<T>& M1){
	return M1 * val;
}

template<typename T>
inline
ColorHyperVolume<T> operator*(const ColorHyperVolume<T>& M1,
		const slip::Color<T>& val){
	ColorHyperVolume<T>  tmp = M1;
	tmp *= val;
	return tmp;
}

template<typename T>
inline
ColorHyperVolume<T> operator*(const slip::Color<T>& val,
		const ColorHyperVolume<T>& M1){
	return M1 * val;
}

template<typename T>
inline
ColorHyperVolume<T> operator/(const ColorHyperVolume<T>& M1,
		const ColorHyperVolume<T>& M2){
	assert(M1.dim1() == M2.dim1());
	assert(M1.dim2() == M2.dim2());
	assert(M1.dim3() == M2.dim3());
	assert(M1.dim4() == M2.dim4());
	ColorHyperVolume<T> tmp(M1.dim1(),M1.dim2(),M1.dim3(),M1.dim4());
	std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::divides<slip::Color<T> >());
	return tmp;
}

template<typename T>
inline
ColorHyperVolume<T> operator/(const ColorHyperVolume<T>& M1,
		const T& val){
	ColorHyperVolume<T>  tmp = M1;
	tmp /= val;
	return tmp;
}

template<typename T>
inline
ColorHyperVolume<T> operator/(const ColorHyperVolume<T>& M1,
		const slip::Color<T>& val){
	ColorHyperVolume<T>  tmp = M1;
	tmp /= val;
	return tmp;
}

}  // namespace slip

#endif /* SLIP_COLORHYPERVOLUME_HPP_ */
