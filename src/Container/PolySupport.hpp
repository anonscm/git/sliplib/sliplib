/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/** 
 * \file PolySupport.hpp
 * 
 * \brief Provides a class to handle Support of Polynomials.
 * 
 */
#ifndef SLIP_POLYSUPPORT_HPP
#define SLIP_POLYSUPPORT_HPP

#include <iostream>

namespace slip
{

template<class T> class PolySupport;

template<class T>
std::ostream& operator << (std::ostream& os,
			   const PolySupport<T>& support);


/*! \class PolySupport
** \brief This class modelised closed support of Polynomials [a, b] with a < b.
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.1
** \date 2016/11/21
** \since 1.5.0
** \param T DataType of the support
**
*/
template<class T>
class PolySupport
{
public:
  typedef T value_type;
  typedef PolySupport<T> self;
  typedef const PolySupport<T> const_self;
public:
   /**
   ** \name Constructors & Destructors
   */
  /*@{*/
  /*!
  ** \brief Constructs a %PolySupport
  */
  PolySupport():
    a_(),
    b_()
  {}
  /*!
  ** \brief Constructs a support [a,b]
  ** \param a first value of the support
  ** \param b second value of the support
  ** \pre (a < b)
  */
  PolySupport(const T& a, 
	      const T& b):
    a_(a),
    b_(b)
  {}

  /*!
  ** \brief Constructs a copy of the %PolySupport \a rhs
  */
  PolySupport(const self& support):
    a_(support.a_),
    b_(support.b_)
  {}

  
  /*!
  ** \brief Destructor of the %PolySupport 
  */
  virtual ~PolySupport()
  {}

  /*!
  ** \brief Assigns a %PolySupport in \a rhs
  **
  ** \param rhs %PolySupport to get the values from.
  ** \return 
  */
  self& operator = (const self& support)
  {
    if (this == &support)
      {
	return *this;
      }
  
    this->a_ = support.a();
    this->b_ = support.b();
    
    return *this;
  }

  /*@} End Constructors */
  /**
   ** \name accessors/mutators
   */
  /*@{*/
  /*!
   * \brief accessor to the first value of the support
   */
   const T& a(void) const
  {
    return this->a_;
  }

  /*!
   * \brief accessor to the second value of the support
   */
  const T& b(void) const
  {
    return this->b_;
  }
 /*!
   * \brief accessor to the first value of the support
   */
  const T& min(void) const
  {
    return this->a_;
  }
 /*!
   * \brief accessor to the second value of the support
   */
  const T& max(void) const
  {
    return this->b_;
  }
  /*!
   * \brief mutator of the first value of the support
   ** \param min new first value
   */
  void min(const T& min)
  {
    this->a_ = min;
  }
  /*!
   * \brief mutator of the second value of the support
   ** \param min new second value
   */
  void max(const T& max)
  {
    this->b_ = max;
  }
  /*!
   * \brief mutator of the first value of the support
   ** \param min new first value
   */
   void a(const T& min)
  {
    this->a_ = min;
  }
   /*!
   * \brief mutator of the second value of the support
   ** \param max new second value
   */
  void b(const T& max)
  {
    this->b_ = max;
  }
 /*!
   * \brief set the values of the support [a,b]
   ** \param a first value of the support
   ** \param b second value of the support
   ** \pre a < b
   */
  void set(const T& a, 
	   const T& b)
  {
    this->a_ = a;
    this->b_ = b;
  }
  /*@} End  accessors/mutators*/
   /**
   ** \name i/o operators
   */
  /*@{*/
  friend std::ostream& operator << <>(std::ostream& os,
				      const self& support);
  /*@} End i/o operators */

protected:
  T a_;
  T b_;
};




/**
 * 
 */
template<class T>
std::ostream& operator << (std::ostream& os, 
			   const PolySupport<T>& support)
{
  os << "[" << support.a() << " ; " << support.b() << "] ";

  return os;
}

}//::slip
#endif // POLYSUPPORT_HPP
