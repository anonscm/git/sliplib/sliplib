/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file GenericMultiComponent3d.hpp
 * 
 * \brief Provides a class to manipulate multicomponent 3d containers.
 * 
 */
#ifndef SLIP_GENERICMULTICOMPONENT3D_HPP
#define SLIP_GENERICMULTICOMPONENT3D_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <cmath>
#include <string>
#include <vector>
#include <cstddef>
#include "Matrix3d.hpp"
#include "Matrix.hpp"
#include "stride_iterator.hpp"
#include "kstride_iterator.hpp"
#include "iterator2d_box.hpp"
#include "apply.hpp"
#include "iterator3d_plane.hpp"
#include "iterator3d_box.hpp"
#include "iterator3d_range.hpp"

#include "component_iterator3d_box.hpp"
#include "component_iterator3d_range.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template<class Block>
class stride_iterator;

//  template<class Block>
//  class iterator3d_box;

template <class Block>
class DPoint3d;

template <class Block>
class Point3d;

template <class Block>
class Box3d;

template <typename Block>
class GenericMultiComponent3d;

template <typename Block>
class Matrix3d;

template <typename Block>
std::ostream& operator<<(std::ostream & out, const slip::GenericMultiComponent3d<Block>& a);

template<typename Block>
bool operator==(const slip::GenericMultiComponent3d<Block>& x,
		const slip::GenericMultiComponent3d<Block>& y);

template<typename Block>
bool operator!=(const slip::GenericMultiComponent3d<Block>& x,
		const slip::GenericMultiComponent3d<Block>& y);

// template<typename Block>
// bool operator<(const GenericMultiComponent3d<Block>& x,
// 	       const GenericMultiComponent3d<Block>& y);

// template<typename Block>
// bool operator>(const GenericMultiComponent3d<Block>& x,
// 	       const GenericMultiComponent3d<Block>& y);

// template<typename Block>
// bool operator<=(const GenericMultiComponent3d<Block>& x,
// 		const GenericMultiComponent3d<Block>& y);

// template<typename Block>
// bool operator>=(const GenericMultiComponent3d<Block>& x,
// 		const GenericMultiComponent3d<Block>& y);

/*!
 *  @defgroup MultiComponent3dContainers Multi-component 3d Containers
 *  @brief 3d multicomponents containers having the same interface
 *  @{
 */
/*! \class GenericMultiComponent3d
 **  \ingroup Containers Containers3d MultiComponent3dContainers 
 ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
 ** \version 0.0.4
 ** \date 2014/03/25
 ** \since 1.0.0
 **\brief This is a GenericMultiComponent3d class.
 ** This container statisfies the BidirectionnalContainer concepts of the STL.
 ** It is also an 3d extension of the RandomAccessContainer concept. That is
 ** to say the bracket element access is replaced by the triple bracket element
 ** access.
 ** Data are stored using a %Matrix3d of Block. It is essentially an internal
 ** class used to define slip::DenseVector3dField3d.
 ** But it can be used to define other 3d Multicomponent structures.
 ** \param Block Type of object in the GenericMultiComponent3d
 **        Must be a static block
 ** \par Axis conventions:
 ** \image html iterator3d_conventions.jpg "axis and notation conventions"
 ** \image latex iterator3d_conventions.eps "axis and notation conventions" width=5cm
 **
 */
template <typename Block>
class GenericMultiComponent3d
{
public :

	typedef Block value_type;
	typedef GenericMultiComponent3d<Block> self;
	typedef const GenericMultiComponent3d<Block> const_self;
	typedef value_type* pointer;
	typedef value_type const * const_pointer;
	typedef value_type& reference;
	typedef value_type const & const_reference;

	typedef ptrdiff_t difference_type;
	typedef std::size_t size_type;

	typedef pointer iterator;
	typedef const_pointer const_iterator;

	typedef std::reverse_iterator<iterator> reverse_iterator;
	typedef std::reverse_iterator<const_iterator> const_reverse_iterator;


	//slice, row and col iterator
	typedef slip::stride_iterator<pointer> slice_iterator;
	typedef slip::stride_iterator<const_pointer> const_slice_iterator;
	typedef pointer row_iterator;
	typedef const_pointer const_row_iterator;
	typedef slip::stride_iterator<pointer> col_iterator;
	typedef slip::stride_iterator<const_pointer> const_col_iterator;
	typedef slip::stride_iterator<slice_iterator> slice_range_iterator;
	typedef slip::stride_iterator<const_slice_iterator> const_slice_range_iterator;
	typedef slip::stride_iterator<pointer> row_range_iterator;
	typedef slip::stride_iterator<const_pointer> const_row_range_iterator;
	typedef slip::stride_iterator<col_iterator> col_range_iterator;
	typedef slip::stride_iterator<const_col_iterator> const_col_range_iterator;

	typedef std::reverse_iterator<slice_iterator> reverse_slice_iterator;
	typedef std::reverse_iterator<const_slice_iterator> const_reverse_slice_iterator;
	typedef std::reverse_iterator<iterator> reverse_row_iterator;
	typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
	typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
	typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;
	typedef std::reverse_iterator<slice_range_iterator> reverse_slice_range_iterator;
	typedef std::reverse_iterator<const_slice_range_iterator> const_reverse_slice_range_iterator;
	typedef std::reverse_iterator<row_range_iterator> reverse_row_range_iterator;
	typedef std::reverse_iterator<const_row_range_iterator> const_reverse_row_range_iterator;
	typedef std::reverse_iterator<col_range_iterator> reverse_col_range_iterator;
	typedef std::reverse_iterator<const_col_range_iterator> const_reverse_col_range_iterator;

#ifdef ALL_PLANE_ITERATOR3D 
	//generic 1d iterator
	typedef slip::stride_iterator<pointer> iterator1d;
	typedef slip::stride_iterator<const_pointer> const_iterator1d;
	typedef std::reverse_iterator<iterator1d> reverse_iterator1d;
	typedef std::reverse_iterator<const_iterator1d> const_reverse_iterator1d;
#endif //ALL_PLANE_ITERATOR3D  

	//iterator 2d
	typedef typename slip::Array3d<Block>::iterator2d iterator2d;
	typedef typename slip::Array3d<Block>::const_iterator2d const_iterator2d;
	typedef std::reverse_iterator<iterator2d> reverse_iterator2d;
	typedef std::reverse_iterator<const_iterator2d> const_reverse_iterator2d;

	//iterator 3d
	typedef typename slip::Array3d<Block>::iterator3d iterator3d;
	typedef typename slip::Array3d<Block>::const_iterator3d const_iterator3d;
	typedef typename slip::Array3d<Block>::iterator3d_range iterator3d_range;
	typedef typename slip::Array3d<Block>::const_iterator3d_range const_iterator3d_range;

	typedef std::reverse_iterator<iterator3d> reverse_iterator3d;
	typedef std::reverse_iterator<const_iterator3d> const_reverse_iterator3d;
	typedef std::reverse_iterator<iterator3d_range> reverse_iterator3d_range;
	typedef std::reverse_iterator<const_iterator3d_range> const_reverse_iterator3d_range;

	//Component iterators
	typedef typename Block::value_type block_value_type;
	typedef block_value_type* block_pointer;
	typedef const block_value_type* const_block_pointer;
	typedef block_value_type& block_reference;
	typedef const block_value_type const_block_reference;
	typedef slip::kstride_iterator<typename Block::pointer,Block::SIZE> component_iterator;
	typedef slip::kstride_iterator<typename Block::const_pointer,Block::SIZE> const_component_iterator;

	typedef std::reverse_iterator<component_iterator> reverse_component_iterator;
	typedef std::reverse_iterator<const_component_iterator> const_reverse_component_iterator;

	//component slice, row and col iterator

	typedef slip::stride_iterator<block_pointer> component_slice_iterator;
	typedef slip::stride_iterator<const_block_pointer> const_component_slice_iterator;
	typedef slip::stride_iterator<block_pointer> component_slice_range_iterator;
	typedef slip::stride_iterator<const_block_pointer> const_component_slice_range_iterator;
	typedef component_iterator component_row_iterator;
	typedef const_component_iterator const_component_row_iterator;
	typedef slip::stride_iterator<block_pointer> component_row_range_iterator;
	typedef slip::stride_iterator<const_block_pointer> const_component_row_range_iterator;
	typedef slip::stride_iterator<block_pointer> component_col_iterator;
	typedef slip::stride_iterator<const_block_pointer> const_component_col_iterator;
	typedef slip::stride_iterator<block_pointer> component_col_range_iterator;
	typedef slip::stride_iterator<const_block_pointer> const_component_col_range_iterator;

	typedef std::reverse_iterator<component_slice_iterator> reverse_component_slice_iterator;
	typedef std::reverse_iterator<const_component_slice_iterator> const_reverse_component_slice_iterator;
	typedef std::reverse_iterator<component_slice_range_iterator> reverse_component_slice_range_iterator;
	typedef std::reverse_iterator<const_component_slice_range_iterator> const_reverse_component_slice_range_iterator;
	typedef std::reverse_iterator<component_row_iterator> reverse_component_row_iterator;
	typedef std::reverse_iterator<const_component_row_iterator> const_reverse_component_row_iterator;
	typedef std::reverse_iterator<component_row_range_iterator> reverse_component_row_range_iterator;
	typedef std::reverse_iterator<const_component_row_range_iterator> const_reverse_component_row_range_iterator;
	typedef std::reverse_iterator<component_col_iterator> reverse_component_col_iterator;
	typedef std::reverse_iterator<const_component_col_iterator> const_reverse_component_col_iterator;
	typedef std::reverse_iterator<component_col_range_iterator> reverse_component_col_range_iterator;
	typedef std::reverse_iterator<const_component_col_range_iterator> const_reverse_component_col_range_iterator;

	//component iterator 3d

	typedef slip::component_iterator3d_box<self,Block::SIZE> component_iterator3d;
	typedef std::reverse_iterator<component_iterator3d> reverse_component_iterator3d;
	typedef slip::const_component_iterator3d_box<const_self,Block::SIZE> const_component_iterator3d;
	typedef std::reverse_iterator<const_component_iterator3d> const_reverse_component_iterator3d;

	typedef slip::component_iterator3d_range<self,Block::SIZE> component_iterator3d_range;
	typedef std::reverse_iterator<component_iterator3d_range> reverse_component_iterator3d_range;
	typedef slip::const_component_iterator3d_range<const_self,Block::SIZE> const_component_iterator3d_range;
	typedef std::reverse_iterator<const_component_iterator3d_range> const_reverse_component_iterator3d_range;


	//default iterator of the container
	typedef iterator3d default_iterator;
	typedef const_iterator3d const_default_iterator;


	static const std::size_t DIM = 3;
	static const std::size_t COMPONENTS = Block::SIZE;

public:
	/**
	 ** \name Constructors & Destructors
	 */
	/*@{*/

	/*!
	 ** \brief Constructs a %GenericMultiComponent3d.
	 */
	GenericMultiComponent3d();

	/*!
	 ** \brief Constructs a %GenericMultiComponent3d.
	 ** \param slices first dimension of the %GenericMultiComponent3d
	 ** \param rows second dimension of the %GenericMultiComponent3d
	 ** \param cols third dimension of the %GenericMultiComponent3d
	 */
	GenericMultiComponent3d(const size_type slices,
			const size_type rows,
			const size_type cols);

	/*!
	 ** \brief Constructs a %GenericMultiComponent3d initialized by the scalar value \a val.
	 ** \param slices first dimension of the %GenericMultiComponent3d
	 ** \param rows second dimension of the %GenericMultiComponent3d
	 ** \param cols third dimension of the %GenericMultiComponent3d
	 ** \param val initialization value of the elements
	 */
	GenericMultiComponent3d(const size_type slices,
			const size_type rows,
			const size_type cols,
			const Block& val);

	/*!
	 ** \brief Constructs a %GenericMultiComponent3d initialized by an array \a val.
	 ** \param slices first dimension of the %GenericMultiComponent3d
	 ** \param rows second dimension of the %GenericMultiComponent3d
	 ** \param cols third dimension of the %GenericMultiComponent3d
	 ** \param val initialization linear array value of the elements
	 */
	GenericMultiComponent3d(const size_type slices,
			const size_type rows,
			const size_type cols,
			typename Block::const_pointer val);

	/*!
	 ** \brief Constructs a %GenericMultiComponent3d initialized by an array \a val.
	 ** \param slices first dimension of the %GenericMultiComponent3d
	 ** \param rows second dimension of the %GenericMultiComponent3d
	 ** \param cols third dimension of the %GenericMultiComponent3d
	 ** \param val initialization array value of the elements
	 */
	GenericMultiComponent3d(const size_type slices,
			const size_type rows,
			const size_type cols,
			const Block* val);


	/**
	 ** \brief  Contructs a %GenericMultiComponent3d from a range.
	 ** \param slices first dimension of the %GenericMultiComponent3d
	 ** \param rows second dimension of the %GenericMultiComponent3d
	 ** \param cols third dimension of the %GenericMultiComponent3d
	 ** \param  first  An input iterator.
	 ** \param  last  An input iterator.
	 **
	 ** Create a %GenericMultiComponent3d consisting of copies of the elements from
	 ** [first,last).
	 */
	template<typename InputIterator>
	GenericMultiComponent3d(const size_type slices,
			const size_type rows,
			const size_type cols,
			InputIterator first,
			InputIterator last)
			{
		matrix_ = new slip::Matrix3d<Block>(slices,rows,cols,first,last);
			}

	/**
	 ** \brief  Contructs a %GenericMultiComponent3d from a 3 ranges.
	 ** \param slices first dimension of the %GenericMultiComponent3d
	 ** \param rows second dimension of the %GenericMultiComponent3d
	 ** \param cols third dimension of the %GenericMultiComponent3d
	 ** \param  first_iterators_list  A std::vector of the first input iterators.
	 ** \param  last  An input iterator.
	 **
	 **
	 ** Create a %GenericMultiComponent3d consisting of copies of the elements from
	 ** [first_iterators_list[0],last),
	 ** [first_iterators_list[1],first_iterators_list[1] + (last - first_iterators_list[0]),
	 ** ...
	 */
	template<typename InputIterator>
	GenericMultiComponent3d(const size_type slices,
			const size_type rows,
			const size_type cols,
			std::vector<InputIterator> first_iterators_list,
			InputIterator last)

			{
		matrix_ = new slip::Matrix3d<Block>(slices,rows,cols);
		this->fill(first_iterators_list,last);
			}


	/*!
	 ** \brief Constructs a copy of the %GenericMultiComponent3d \a rhs
	 */
	GenericMultiComponent3d(const self& rhs);

	/*!
	 ** \brief Destructor of the %GenericMultiComponent3d
	 */
	virtual ~GenericMultiComponent3d();


	/*@} End Constructors */


	/*!
	 ** \brief Resizes a GenericMultiComponent3d.
	 ** \param slices first dimension of the %GenericMultiComponent3d
	 ** \param rows second dimension of the %GenericMultiComponent3d
	 ** \param cols third dimension of the %GenericMultiComponent3d
	 ** \param val new value for all the elements
	 */
	void resize(const size_type slices,
			const size_type rows,
			const size_type cols,
			const Block& val = Block());

	/**
	 ** \name One dimensional global iterators
	 */
	/*@{*/

	//****************************************************************************
	//                          One dimensional iterators
	//****************************************************************************



	//----------------------Global iterators------------------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element in the %GenericMultiComponent3d. Iteration is done in ordinary
	 **  element order.
	 ** \return begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	iterator begin();

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  first element in the %GenericMultiComponent3d.  Iteration is done in ordinary
	 **  element order.
	 ** \return const begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	const_iterator begin() const;

	/*!
	 **  \brief Returns a read/write iterator that points one past the last
	 **  element in the %GenericMultiComponent3d.  Iteration is done in ordinary
	 **  element order.
	 ** \return end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	iterator end();

	/*!
	 **  \brief Returns a read-only (constant) iterator that points one past
	 **  the last element in the %GenericMultiComponent3d.  Iteration is done in
	 **  ordinary element order.
	 ** \return const end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 ** \endcode
	 */
	const_iterator end() const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the
	 **  last element in the %GenericMultiComponent3d.  Iteration is done in reverse
	 **  element order.
	 ** \return reverse begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	reverse_iterator rbegin();

	/*!
	 **  \brief Returns a read-only (constant) reverse iterator that points
	 **  to the last element in the %GenericMultiComponent3d.  Iteration is done in
	 **  reverse element order.
	 ** \return const reverse begin iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	const_reverse_iterator rbegin() const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to one
	 **  before the first element in the %GenericMultiComponent3d.  Iteration is done
	 **  in reverse element order.
	 **  \return reverse end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	reverse_iterator rend();

	/*!
	 **  \brief Returns a read-only (constant) reverse iterator that points
	 **  to one before the first element in the %GenericMultiComponent3d.  Iteration
	 **  is done in reverse element order.
	 **  \return const reverse end iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 ** //copy A1 in S
	 ** std::copy(A1.begin(),A1.end(),S.begin());
	 ** \endcode
	 */
	const_reverse_iterator rend() const;

	/*@} End One dimensional global iterators */
	/**
	 ** \name One dimensional slice iterators
	 */
	/*@{*/
	//--------------------One dimensional slice iterators----------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 **  //copy A1 in S
	 **  std::copy(A1.begin(),A1.end(),S.begin());
	 **  \endcode
	 */
	slice_iterator slice_begin(const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the first
	 **  element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 **  //copy A1 in S
	 **  std::copy(A1.begin(),A1.end(),S.begin());
	 **  \endcode
	 */
	const_slice_iterator slice_begin(const size_type row, const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the one past the end
	 **  element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 **  //copy A1 in S
	 **  std::copy(A1.begin(),A1.end(),S.begin());
	 **  \endcode
	 */
	slice_iterator slice_end(const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the one past the end
	 **  element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 **  //copy A1 in S
	 **  std::copy(A1.begin(),A1.end(),S.begin());
	 **  \endcode
	 */
	const_slice_iterator slice_end(const size_type row, const size_type col) const;


	/*!
	 **  \brief Returns a read/write iterator that points to the
	 **  last element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 ** 	Iteration is done in reverse element order (decreasing slice number).
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 ** 	\return reverse_slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 **  //copy A1 in S
	 **  std::copy(A1.rbegin(),A1.rend(),S.rbegin());
	 **  \endcode
	 */
	reverse_slice_iterator slice_rbegin(const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  last element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **	Iteration is done in reverse element order (decreasing slice number).
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_reverse_slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 **  //copy A1 in S
	 **  std::copy(A1.rbegin(),A1.rend(),S.rbegin());
	 **  \endcode
	 */
	const_reverse_slice_iterator slice_rbegin(const size_type row, const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the
	 **  one before the first element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order (decreasing slice number).
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **  \return reverse_slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 **  //copy A1 in S
	 **  std::copy(A1.rbegin(),A1.rend(),S.rbegin());
	 **  \endcode
	 */
	reverse_slice_iterator slice_rend(const size_type row, const size_type col);


	/*!
	 **  \brief Returns a read (constant) iterator that points to the
	 **  one before the first element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order (decreasing slice number).
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_reverse_slice_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A2(10,9,5);
	 **  slip::GenericMultiComponent3d<slip::color<double> > S(10,9,5);
	 **  //copy A1 in S
	 **  std::copy(A1.rbegin(),A1.rend(),S.rbegin());
	 **  \endcode
	 */
	const_reverse_slice_iterator slice_rend(const size_type row, const size_type col) const;

	/*@} End One dimensional slice iterators */

	/**
	 ** \name One dimensional row iterators
	 */
	/*@{*/
	//-------------------row iterators----------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	row_iterator row_begin(const size_type slice,
			const size_type row);

	/*!
	 **  \brief Returns a read_only iterator that points to the first
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin const_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_row_iterator row_begin(const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	row_iterator row_end(const size_type slice,
			const size_type row);


	/*!
	 **  \brief Returns a read_only iterator that points to the past-the-end
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end const_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_row_iterator row_end(const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the last
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	reverse_row_iterator row_rbegin(const size_type slice,
			const size_type row);

	/*!
	 **  \brief Returns a read_only reverse iterator that points to the last
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return begin const_reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_reverse_row_iterator row_rbegin(const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	reverse_row_iterator row_rend(const size_type slice,
			const size_type row);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the first
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 **  \param slice The index of the slice.
	 **  \param row The index of the row.
	 **  \return end const_reverse_row_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_reverse_row_iterator row_rend(const size_type slice,
			const size_type row) const;


	/*@} End One dimensional row iterators */
	/**
	 ** \name One dimensional col iterators
	 */
	/*@{*/
	//-------------------Constant col iterators----------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	col_iterator col_begin(const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only iterator that points to the first
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin const_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_col_iterator col_begin(const size_type slice,
			const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	col_iterator col_end(const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only iterator that points to the past-the-end
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end const_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_col_iterator col_end(const size_type slice,
			const size_type col) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the last
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	reverse_col_iterator col_rbegin(const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the last
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return begin const_reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_reverse_col_iterator col_rbegin(const size_type slice,
			const size_type col) const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	reverse_col_iterator col_rend(const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the first
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 **  \param slice The index of the slice.
	 **  \param col The index of the column
	 **  \return end const_reverse_col_iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 */
	const_reverse_col_iterator col_rend(const size_type slice,
			const size_type col) const;


	/*@} End One dimensional col iterators */

	/**
	 ** \name One dimensional slice range iterators
	 */
	/*@{*/
	//------------------------slice range iterators -----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(4,8,8);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the line (0,0) of A1 iterated according to the
	 ** //range in the line (1,1) of A2
	 ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),A2.slice_begin(1,1));
	 ** \endcode
	 */
	slice_range_iterator slice_begin(const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points one past the end
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(4,8,8);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the line (0,0) of A1 iterated according to the
	 ** //range in the line (1,1) of A2
	 ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),A2.slice_begin(1,1));
	 ** \endcode
	 */
	slice_range_iterator slice_end(const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the first
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A2(4,8,8);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the line (0,0) of A1 iterated according to the
	 ** //range in the line (1,1) of A2
	 ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),A2.slice_begin(1,1));
	 ** \endcode
	 */
	const_slice_range_iterator slice_begin(const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the last
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A2(4,8,8);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the line (0,0) of A1 iterated according to the
	 ** //range in the line (1,1) of A2
	 ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),A2.slice_begin(1,1));
	 ** \endcode
	 */
	const_slice_range_iterator slice_end(const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the last
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in the reverse element order according to the
	 ** %Range.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(4,8,8);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the line (0,0) of A1 iterated according to the
	 ** //range in the line (1,1) of A2
	 ** std::copy(A1.slice_rbegin(0,0,range),A1.slice_rend(0,0,range),A2.slice_rbegin(1,1));
	 ** \endcode
	 */
	reverse_slice_range_iterator slice_rbegin(const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(4,8,8);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the line (0,0) of A1 iterated according to the
	 ** //range in the line (1,1) of A2
	 ** std::copy(A1.slice_rbegin(0,0,range),A1.slice_rend(0,0,range),A2.slice_rbegin(1,1));
	 ** \endcode
	 */
	reverse_slice_range_iterator slice_rend(const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the last
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A2(4,8,8);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the line (0,0) of A1 iterated according to the
	 ** //range in the line (1,1) of A2
	 ** std::copy(A1.slice_rbegin(0,0,range),A1.slice_rend(0,0,range),A2.slice_rbegin(1,1));
	 ** \endcode
	 */
	const_reverse_slice_range_iterator slice_rbegin(const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the lastto the first
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A2(4,8,8);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the line (0,0) of A1 iterated according to the
	 ** //range in the line (1,1) of A2
	 ** std::copy(A1.slice_rbegin(0,0,range),A1.slice_rend(0,0,range),A2.slice_rbegin(1,1));
	 ** \endcode
	 */
	const_reverse_slice_range_iterator slice_rend(const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*@} End One dimensional slice range iterators */

	/**
	 ** \name One dimensional row range iterators
	 */
	/*@{*/
	//------------------------row range iterators -----------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the %Range \a range of the row \a row in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(8,4,5);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
	 ** //range in the row 1 of the slice 1 of A2
	 ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
	 ** \endcode
	 */
	row_range_iterator row_begin(const size_type slice,const size_type row,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read/write iterator that points one past the end
	 **  element of the %Range \a range of the row \a row in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return end row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(8,4,5);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
	 ** //range in the row 1 of the slice 1 of A2
	 ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
	 ** \endcode
	 */
	row_range_iterator row_end(const size_type slice,const size_type row,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the row \a row in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin const_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(8,4,5);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
	 ** //range in the row 1 of the slice 1 of A2
	 ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
	 ** \endcode
	 */
	const_row_range_iterator row_begin(const size_type slice,const size_type row,
			const slip::Range<int>& range) const;


	/*!
	 **  \brief Returns a read_only iterator that points one past the last
	 **  element of the %Range range of the row \a row in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param row Row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return begin const_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(8,4,5);
	 **  slip::Range<int> range(0,A1.dim3()-1,2);
	 ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
	 ** //range in the row 1 of the slice 1 of A2
	 ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
	 ** \endcode
	 */
	const_row_range_iterator row_end(const size_type slice,const size_type row,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read-write iterator that points to the last
	 ** element of the %Range \a range of the row of a slice \a row and slice in the %GenericMultiComponent3d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	reverse_row_range_iterator row_rbegin(const size_type slice,const size_type row,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-write iterator that points one before
	 **  the first element of the %Range \a range of the row of a slice \a row in the
	 **  %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	reverse_row_range_iterator row_rend(const size_type slice,const size_type row,
			const slip::Range<int>& range);



	/*!
	 **  \brief Returns a read-only iterator that points to the last
	 **  element of the %Range \a range of the row  of a slice \a row in the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return begin const_reverse_row_range_iterator value
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	const_reverse_row_range_iterator row_rbegin(const size_type slice,const size_type row,
			const slip::Range<int>& range) const;


	/*!
	 **  \brief Returns a read-only iterator that points one before the first
	 **  element of the %Range \a range of the row of a slice \a row in the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return const_reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	const_reverse_row_range_iterator row_rend(const size_type slice,const size_type row,
			const slip::Range<int>& range) const;

	/*@} End One dimensional row range iterators */
	/**
	 ** \name One dimensional col range iterators
	 */
	/*@{*/
	//------------------------col range iterators -----------------------


	/*!
	 **  \brief Returns a read-write iterator that points to the first
	 **  element of the %Range \a range of the col \a col in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate
	 ** \return begin col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(4,8,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
	 ** //range in the column 1 of the slice 1 of A2
	 ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
	 ** \endcode
	 */
	col_range_iterator col_begin(const size_type slice,const size_type col,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read-write iterator that points to the past
	 **  the end element of the %Range \a range of the col \a col in the
	 **  %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(4,8,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
	 ** //range in the column 1 of the slice 1 of A2
	 ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
	 ** \endcode
	 */
	col_range_iterator col_end(const size_type slice,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the col \a col in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(4,8,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
	 ** //range in the column 1 of the slice 1 of A2
	 ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
	 ** \endcode
	 */
	const_col_range_iterator col_begin(const size_type slice,const size_type col,
			const slip::Range<int>& range) const;


	/*!
	 **  \brief Returns a read-only iterator that points to the past
	 **  the end element of the %Range \a range of the col \a col in
	 **  the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate
	 ** \return begin const_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::color<double> > const A1(8,8,8);
	 **  slip::GenericMultiComponent3d<slip::color<double> > A2(4,8,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
	 ** //range in the column 1 of the slice 1 of A2
	 ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
	 ** \endcode
	 */
	const_col_range_iterator col_end(const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 **  \brief Returns a read-write iterator that points to the last
	 **  element of the %Range \a range of the col of a slice \a col in the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	reverse_col_range_iterator col_rbegin(const size_type slice,const size_type col,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read-write iterator that points to one before
	 **  the first element of the %Range range of the col of a slice \a col in the
	 **  %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	reverse_col_range_iterator col_rend(const size_type slice,const size_type col,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read_only iterator that points to the last
	 **  element of the %Range \& range of the col of a slice \a col in the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	const_reverse_col_range_iterator
	col_rbegin(const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the col of a slice \a col in the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return const_reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	const_reverse_col_range_iterator col_rend(const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*@} End One dimensional col range iterators */

	//****************************************************************************
	//                          One dimensional plane iterators
	//****************************************************************************

	/**
	 ** \name One dimensional global plane iterators
	 */
	/*@{*/

	//----------------------Global plane iterators------------------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element in the in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 **  \param slice the slice coordinate of the plane
	 **  \return begin iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **   slip::GenericMultiComponent3d<slip::color<double> > A1(2,3,2);
	 **   slip::GenericMultiComponent3d<slip::color<double> > A2(2,3,2);
	 **   //copy the first plane (in slice direction) of A1 in A2
	 **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
	 **  \endcode
	 */
	iterator plane_begin(const size_type slice);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  first element in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 **  \param slice the slice coordinate of the plane
	 **  \return const begin iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **   slip::GenericMultiComponent3d<slip::color<double> > const A1(2,3,2);
	 **   slip::GenericMultiComponent3d<slip::color<double> > A2(2,3,2);
	 **   //copy the first plane (in slice direction) of A1 in A2
	 **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
	 **  \endcode
	 */
	const_iterator plane_begin(const size_type slice) const;

	/*!
	 **  \brief Returns a read/write iterator that points one past the last
	 **  element in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 **  \param slice the slice coordinate of the plane
	 **  \return end iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **   slip::GenericMultiComponent3d<slip::color<double> > A1(2,3,2);
	 **   slip::GenericMultiComponent3d<slip::color<double> > A2(2,3,2);
	 **   //copy the first plane (in slice direction) of A1 in A2
	 **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
	 **  \endcode
	 */
	iterator plane_end(const size_type slice);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points one past
	 **  the last element in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 **  \param slice the slice coordinate of the plane
	 **  \return const end iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **   slip::GenericMultiComponent3d<slip::color<double> > const A1(2,3,2);
	 **   slip::GenericMultiComponent3d<slip::color<double> > A2(2,3,2);
	 **   //copy the first plane (in slice direction) of A1 in A2
	 **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
	 **  \endcode
	 */
	const_iterator plane_end(const size_type slice) const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the
	 **  last element in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 **  \param slice the slice coordinate of the plane
	 **  \return reverse begin iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **   slip::GenericMultiComponent3d<slip::color<double> > A1(2,3,2);
	 **   slip::GenericMultiComponent3d<slip::color<double> > A2(2,3,2);
	 **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
	 **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
	 **  \endcode
	 */
	reverse_iterator plane_rbegin(const size_type slice);

	/*!
	 **  \brief Returns a read-only (constant) reverse iterator that points
	 **  to the last element in the slice plane k of the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 ** \param slice the slice coordinate of the plane
	 ** \return const reverse begin iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **   slip::GenericMultiComponent3d<slip::color<double> > const A1(2,3,2);
	 **   slip::GenericMultiComponent3d<slip::color<double> > A2(2,3,2);
	 **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
	 **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
	 **  \endcode
	 */
	const_reverse_iterator plane_rbegin(const size_type slice) const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to one
	 **  before the first element in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 **  \param slice the slice coordinate of the plane
	 **  \return reverse end iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **   slip::GenericMultiComponent3d<slip::color<double> > A1(2,3,2);
	 **   slip::GenericMultiComponent3d<slip::color<double> > A2(2,3,2);
	 **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
	 **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
	 **  \endcode
	 */
	reverse_iterator plane_rend(const size_type slice);

	/*!
	 **  \brief Returns a read-only (constant) reverse iterator that points
	 **  to one before the first element in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 **  \param slice the slice coordinate of the plane
	 **  \return const reverse end iterator value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **
	 **   slip::GenericMultiComponent3d<slip::color<double> > const A1(2,3,2);
	 **   slip::GenericMultiComponent3d<slip::color<double> > A2(2,3,2);
	 **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
	 **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
	 **  \endcode
	 */
	const_reverse_iterator plane_rend(const size_type slice) const;

	/*@} End One dimensional global plane iterators */

#ifdef ALL_PLANE_ITERATOR3D
	/**
	 ** \name One dimensional row and col plane iterators
	 */
	/*@{*/
	//-------------------row and col plane iterators----------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the row \a row of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \return begin const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type row) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the row \a row of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \return begin iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type row);

	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the row \a row of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \return end iterator1d value
	 */
	iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type row);

	/*!
	 ** \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the row \a row of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \return begin const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type row) const;


	/*!
	 **  \brief Returns a read/write reverse_iterator that points to the last
	 **  element of the row \a row of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \return begin reverse_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type row);

	/*!
	 **  \brief Returns a read/write const reverse iterator that points to the last
	 **  element of the row \a row of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \return begin reverse_const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type row) const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the row \a row of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \return end iterator1d value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type row);

	/*!
	 ** \brief Returns a read/write const reverse iterator that points to the first
	 **  element of the row \a row of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \return begin reverse_const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type row) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the col \a col of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col
	 ** \return begin iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type col);

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the col \a col of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \return begin const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the col \a col of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \return end iterator1d value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type col);

	/*!
	 ** \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the col \a col of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \return begin const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type col) const;


	/*!
	 **  \brief Returns a read/write reverse_iterator that points to the last
	 **  element of the col \a col of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \return begin reverse_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type col);

	/*!
	 **  \brief Returns a read/write const reverse iterator that points to the last
	 **  element of the col \a col of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \return begin reverse_const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type col) const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the col \a col of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \return end iterator1d value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type col);

	/*!
	 ** \brief Returns a read/write const reverse iterator that points to the first
	 **  element of the col \a col of a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \return begin reverse_const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type col) const;

	/*@} End One dimensional plane row and col iterators */

	/**
	 ** \name One dimensional plane box iterators
	 */
	/*@{*/
	//-------------------plane box iterators----------



	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the row \a row of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \return begin const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type row, const Box2d<int> & b) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the row \a row of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \param b The box within the plane
	 ** \return begin iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type row, const Box2d<int> & b);



	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the row \a row of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \return end iterator1d value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type row, const Box2d<int> & b);

	/*!
	 ** \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the row \a row of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \param b The box within the plane
	 ** \return begin const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type row, const Box2d<int> & b) const;


	/*!
	 **  \brief Returns a read/write reverse_iterator that points to the last
	 **  element of the row \a row of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \param b The box within the plane
	 ** \param b The box within the plane
	 ** \return begin reverse_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type row, const Box2d<int> & b);

	/*!
	 **  \brief Returns a read/write const reverse iterator that points to the last
	 **  element of the row \a row of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \param b The box within the plane
	 ** \return begin reverse_const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type row, const Box2d<int> & b) const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the row \a row of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \param b The box within the plane
	 ** \return end iterator1d value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type row, const Box2d<int> & b);

	/*!
	 ** \brief Returns a read/write const reverse iterator that points to the first
	 **  element of the row \a row of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param row The index of the row.
	 ** \param b The box within the plane
	 ** \return begin reverse_const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type row, const Box2d<int> & b) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the col \a col of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \param b The box within the plane
	 ** \return begin iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type col, const Box2d<int> & b);

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the col \a col of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \return begin const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type col, const Box2d<int> & b) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the col \a col of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \param b The box within the plane
	 ** \return end iterator1d value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type col, const Box2d<int> & b);

	/*!
	 ** \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the col \a col of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \param b The box within the plane
	 ** \return begin const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type col, const Box2d<int> & b) const;


	/*!
	 **  \brief Returns a read/write reverse_iterator that points to the last
	 **  element of the col \a col of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \param b The box within the plane
	 ** \return begin reverse_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type col, const Box2d<int> & b);

	/*!
	 **  \brief Returns a read/write const reverse iterator that points to the last
	 **  element of the col \a col of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \param b The box within the plane
	 ** \return begin reverse_const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type col, const Box2d<int> & b) const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the col \a col of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \param b The box within the plane
	 ** \return end iterator1d value
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type col, const Box2d<int> & b);

	/*!
	 ** \brief Returns a read/write const reverse iterator that points to the first
	 **  element of the col \a col of a box within a plane \a plane in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \param col The index of the col.
	 ** \param b The box within the plane
	 ** \return begin reverse_const_iterator1d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with RandomAccessIterator
	 **           algorithms.
	 */
	const_reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,
			const size_type col, const Box2d<int> & b) const;


	/*@} End One dimensional plane box iterators */

#endif //ALL_PLANE_ITERATOR3D

	//****************************************************************************
	//                          Two dimensional plane iterators
	//****************************************************************************

	/**
	 ** \name two dimensionnal plane iterators : Global iterators
	 */
	/*@{*/

	//------------------------ Global iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the plane in the %GenericMultiComponent3d. It points to the upper left
	 **  element of the plane
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \return begin iterator2d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   //Print the 2nd plane of M in the KI_PLANE direction
	 **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
	 **             slip::KI_PLANE,2),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate);

	/*!
	 **  \brief Returns a read/write iterator that points to the last
	 **  element of the plane in the %GenericMultiComponent3d. It points to past the end element of
	 **  the bottom right element of the plane
	 **  Iteration is done in ordinary element order.
	 ** \param P number of the plane axe (PLANE_ORIENTATION).
	 ** \param plane_coordinate The constant coordinate
	 ** \return begin iterator2d value
	 ** \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   //Print the 2nd plane of M in the KI_PLANE direction
	 **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
	 **             slip::KI_PLANE,2),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate);

	/*!
	 **  \brief Returns a read/write const iterator that points to the first
	 **  element of the plane in the %GenericMultiComponent3d. It points to the upper left
	 **  element of the plane
	 **  Iteration is done in ordinary element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \return begin const_iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   //Print the 2nd plane of M in the KI_PLANE direction
	 **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
	 **             slip::KI_PLANE,2),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate) const;

	/*!
	 **  \brief Returns a read/write const iterator that points to the last
	 **  element of the plane in the %GenericMultiComponent3d. It points to past the end element of
	 **  the bottom right element of the plane
	 **  Iteration is done in ordinary element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \return begin const_iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   //Print the 2nd plane of M in the KI_PLANE direction
	 **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
	 **             slip::KI_PLANE,2),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate) const;

	/*!
	 **  \brief Returns a read/write reverse_iterator that points to the bottom right
	 **  element of the plane in the %GenericMultiComponent3d.
	 **  Iteration is done in backward element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \return begin iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
	 **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
	 **             slip::KI_PLANE,2),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate);

	/*!
	 **  \brief Returns a read/write reverse_iterator that points to the upper left
	 **  element of the plane in the %GenericMultiComponent3d.
	 **  Iteration is done in backward element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \return begin iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
	 **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
	 **             slip::KI_PLANE,2),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate);

	/*!
	 **  \brief Returns a read/write const reverse iterator that points to the bottom
	 **  right element of the plane in the %GenericMultiComponent3d.
	 **  Iteration is done in backward element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \return begin const_iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
	 **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
	 **             slip::KI_PLANE,2),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate) const;

	/*!
	 **  \brief Returns a read/write const reverse iterator that points to the upper
	 **  left element of the plane in the %GenericMultiComponent3d.
	 **  Iteration is done in backward element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \return begin const_iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example:
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
	 **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
	 **             slip::KI_PLANE,2),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate) const;

	/*@} End two dimensionnal plane iterators : Global iterators */

	/**
	 ** \name two dimensionnal plane iterators : box iterators
	 */
	/*@{*/

	//------------------------ Box iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of a box within a plane in the %GenericMultiComponent3d. It points to the upper left
	 **  element of the box
	 **  Iteration is done in ordinary element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \param b The box within the plane
	 **  \return begin iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \pre b must be within the plane
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   slip::Box2d<int> b(1,1,3,3);
	 **   //Print the 2nd plane of M in the KI_PLANE direction within a box
	 **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
	 **             slip::KI_PLANE,2,b),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
			, const Box2d<int> & b);

	/*!
	 **  \brief Returns a read/write iterator that points to the last
	 **  element of a box within a plane in the %GenericMultiComponent3d. It points to past the end element of
	 **  the bottom right element of the plane
	 **  Iteration is done in ordinary element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \param b The box within the plane
	 **  \return begin iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \pre b must be within the plane
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   slip::Box2d<int> b(1,1,3,3);
	 **   //Print the 2nd plane of M in the KI_PLANE direction within a box
	 **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
	 **             slip::KI_PLANE,2,b),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
			, const Box2d<int> & b);

	/*!
	 **  \brief Returns a read/write const iterator that points to the first
	 **  element of a box within a plane in the %GenericMultiComponent3d. It points to the upper left
	 **  element of the plane
	 **  Iteration is done in ordinary element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \param b The box within the plane
	 **  \return begin const_iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \pre b must be within the plane
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   slip::Box2d<int> b(1,1,3,3);
	 **   //Print the 2nd plane of M in the KI_PLANE direction within a box
	 **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
	 **             slip::KI_PLANE,2,b),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
			, const Box2d<int> & b) const;

	/*!
	 **  \brief Returns a read/write const iterator that points to the last
	 **  element of a box within a plane in the %GenericMultiComponent3d. It points to past the end element of
	 **  the bottom right element of the plane
	 **  Iteration is done in ordinary element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \param b The box within the plane
	 **  \return begin const_iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \pre b must be within the plane
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   slip::Box2d<int> b(1,1,3,3);
	 **   //Print the 2nd plane of M in the KI_PLANE direction within a box
	 **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
	 **             slip::KI_PLANE,2,b),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
			, const Box2d<int> & b) const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the last
	 **  element of a box within a plane in the %GenericMultiComponent3d. It points to the
	 **  bottom right element of the box
	 **  Iteration is done in backward element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \param b The box within the plane
	 **  \return begin reverse_iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \pre b must be within the plane
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   slip::Box2d<int> b(1,1,3,3);
	 **   //Print the 2nd plane of M in the KI_PLANE direction within a box in reverse order
	 **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
	 **             slip::KI_PLANE,2,b),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
			, const Box2d<int> & b);

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of a box within a plane in the %GenericMultiComponent3d. It points to the upper
	 **  left element of the plane
	 **  Iteration is done in backward element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \param b The box within the plane
	 **  \return begin reverse_iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \pre b must be within the plane
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   slip::Box2d<int> b(1,1,3,3);
	 **   //Print the 2nd plane of M in the KI_PLANE direction within a box in reverse order
	 **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
	 **             slip::KI_PLANE,2,b),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
			, const Box2d<int> & b);

	/*!
	 **  \brief Returns a read/write const reverse iterator that points to the last
	 **  element of a box within a plane in the %GenericMultiComponent3d. It points to the bottom right
	 **  element of the plane
	 **  Iteration is done in backward element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \param b The box within the plane
	 **  \return begin const_reverse_iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \pre b must be within the plane
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   slip::Box2d<int> b(1,1,3,3);
	 **   //Print the 2nd plane of M in the KI_PLANE direction within a box in reverse order
	 **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
	 **             slip::KI_PLANE,2,b),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
			, const Box2d<int> & b) const;

	/*!
	 **  \brief Returns a read/write const reverse iterator that points to the first
	 **  element of a box within a plane in the %GenericMultiComponent3d. It points to
	 **  the bottom right element of the plane
	 **  Iteration is done in backward element order.
	 **  \param P number of the plane axe (PLANE_ORIENTATION).
	 **  \param plane_coordinate The constant coordinate
	 **  \param b The box within the plane
	 **  \return begin const_reverse_iterator2d value
	 **  \pre P must be 0,1 or 2
	 **  \pre b must be within the plane
	 **  \remarks This iterator is compatible with BidirectionnalAccessIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   slip::Box2d<int> b(1,1,3,3);
	 **   //Print the 2nd plane of M in the KI_PLANE direction within a box in reverse order
	 **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
	 **             slip::KI_PLANE,2,b),std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
			, const Box2d<int> & b) const;

	/*@} End two dimensionnal plane iterators : box iterators */

	//****************************************************************************
	//                          Three dimensional iterators
	//****************************************************************************

	/**
	 ** \name three dimensionnal iterators : Global iterators
	 */
	/*@{*/

	//------------------------ Global iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator3d that points to the first
	 **  element of the %GenericMultiComponent3d. It points to the front upper left element of
	 **  the %GenericMultiComponent3d.
	 **
	 **  \return begin iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   //Print M
	 **   std::copy(M.front_upper_left(),M.back_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator3d front_upper_left();


	/*!
	 **  \brief Returns a read/write iterator3d that points to the past
	 **  the end element of the %GenericMultiComponent3d. It points to past the end element of
	 **  the back bottom right element of the %GenericMultiComponent3d.
	 **
	 **  \return begin iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   //Print M
	 **   std::copy(M.front_upper_left(),M.back_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator3d back_bottom_right();


	/*!
	 **  \brief Returns a read-only iterator3d that points to the first
	 **  element of the %GenericMultiComponent3d. It points to the front upper left element of
	 **  the %GenericMultiComponent3d.
	 **
	 **  \return begin const iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   //Print M
	 **   std::copy(M.front_upper_left(),M.back_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_iterator3d front_upper_left() const;


	/*!
	 **  \brief Returns a read-only iterator3d that points to the past
	 **  the end element of the %GenericMultiComponent3d. It points to past the end element of
	 **  the back bottom right element of the %GenericMultiComponent3d.
	 **
	 **  \return begin const iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   //Print M
	 **   std::copy(M.front_upper_left(),M.back_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_iterator3d back_bottom_right() const;

	/*!
	 **  \brief Returns a read/write reverse iterator3d. It points to the
	 **   back bottom right element of the %GenericMultiComponent3d.
	 *    Iteration is done within the %GenericMultiComponent3d in the reverse order.
	 **
	 **  \return reverse_iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   //Print M in reverse order
	 **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator3d rfront_upper_left();

	/*!
	 **  \brief Returns a read/write reverse iterator3d. It points to past the
	 **  front upper left element of the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse order.
	 **
	 **  \return reverse iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   //Print M in reverse order
	 **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator3d rback_bottom_right();

	/*!
	 **  \brief Returns a read only reverse iterator3d that points.  It points
	 **  to the back bottom right element of the %GenericMultiComponent3d.
	 **  Iteration is done within the %GenericMultiComponent3d in the reverse order.
	 **
	 **  \return const_reverse_iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   //Print M in reverse order
	 **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator3d rfront_upper_left() const;


	/*!
	 **  \brief Returns a read only reverse iterator3d. It points to past the
	 **  front upper left element of the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \return const reverse iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   //Print M in reverse order
	 **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator3d rback_bottom_right() const;

	/*@} End three dimensionnal iterators : Global iterators */

	/**
	 ** \name three dimensionnal iterators : Box iterators
	 */
	/*@{*/

	//------------------------ Box iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator3d that points to the first
	 **  element of the %GenericMultiComponent3d. It points to the front upper left element of
	 **  the \a %Box3d associated to the %GenericMultiComponent3d.
	 **
	 **  \param box A %Box3d defining the range of indices to iterate
	 **         within the %GenericMultiComponent3d.
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **
	 **  \return end iterator3d value
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   slip::Box3d<int> b(1,1,1,2,2,3);
	 **   //Print M within a box
	 **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator3d front_upper_left(const Box3d<int>& box);


	/*!
	 **  \brief Returns a read/write iterator3d that points to the past
	 **  the end element of the %GenericMultiComponent3d. It points to past the end element of
	 **  the back bottom right element of the \a %Box3d associated to the %GenericMultiComponent3d.
	 **
	 **  \param box a %Box3d defining the range of indices to iterate
	 **         within the %GenericMultiComponent3d.
	 **
	 **  \return end iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   slip::Box3d<int> b(1,1,1,2,2,3);
	 **   //Print M within a box
	 **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 **
	 */
	iterator3d back_bottom_right(const Box3d<int>& box);


	/*!
	 **  \brief Returns a read only iterator3d that points to the first
	 **   element of the %GenericMultiComponent3d. It points to the front upper left element of
	 **   the \a %Box3d associated to the %GenericMultiComponent3d.
	 **
	 **  \param box a %Box3d defining the range of indices to iterate
	 **         within the %GenericMultiComponent3d.
	 **  \return end const iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   slip::Box3d<int> b(1,1,1,2,2,3);
	 **   //Print M within a box
	 **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 **
	 */
	const_iterator3d front_upper_left(const Box3d<int>& box) const;


	/*!
	 **  \brief Returns a read only iterator3d that points to the past
	 **   the end element of the %GenericMultiComponent3d. It points to past the end element of
	 **   the back bottom right element of the \a %Box3d associated to the %GenericMultiComponent3d.
	 **
	 **  \param box a %Box3d defining the range of indices to iterate
	 **         within the %GenericMultiComponent3d.
	 **  \return end const iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   slip::Box3d<int> b(1,1,1,2,2,3);
	 **   //Print M within a box
	 **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 **
	 */
	const_iterator3d back_bottom_right(const Box3d<int>& box) const;



	/*!
	 **  \brief Returns a read/write reverse iterator3d. It points to the
	 **   back bottom right element of the \a %Box3d associated to the %GenericMultiComponent3d.
	 **   Iteration is done in the reverse order.
	 **
	 **  \param box a %Box3d defining the range of indices to iterate
	 **         within the %GenericMultiComponent3d.
	 **
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 **
	 **  \return reverse iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   slip::Box3d<int> b(1,1,1,2,2,3);
	 **   //Print M within a box in reverse order
	 **   std::copy(M.rfront_upper_left(b),M.rback_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator3d rfront_upper_left(const Box3d<int>& box);

	/*!
	 **  \brief Returns a read/write reverse iterator3d. It points to one
	 **   before the front upper left element of the %Box3d \a box associated to
	 **   the %GenericMultiComponent3d.
	 **
	 **  \param box A %Box3d defining the range of indices to iterate
	 **         within the %GenericMultiComponent3d.
	 **
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 **
	 **  \return reverse iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   slip::Box3d<int> b(1,1,1,2,2,3);
	 **   //Print M within a box in reverse order
	 **   std::copy(M.rfront_upper_left(b),M.rback_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator3d rback_bottom_right(const Box3d<int>& box);

	/*!
	 **  \brief Returns a read only reverse iterator3d. It points to the
	 **   back bottom right element of the %Box3d \a box associated to the %GenericMultiComponent3d.
	 **   Iteration is done in the reverse order.
	 **
	 **  \param box A %Box3d defining the range of indices to iterate
	 **         within the %GenericMultiComponent3d.
	 **
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 **
	 **  \return const reverse iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   slip::Box3d<int> b(1,1,1,2,2,3);
	 **   //Print M within a box in reverse order
	 **   std::copy(M.rfront_upper_left(b),M.rback_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator3d rfront_upper_left(const Box3d<int>& box) const;


	/*!
	 **  \brief Returns a read-only reverse iterator3d. It points to one
	 **   before the front upper left element of the %Box3d
	 **   \a box associated to the %GenericMultiComponent3d.
	 **
	 **  \param box A %Box3d defining the range of indices to iterate
	 **         within the %GenericMultiComponent3d.
	 **
	 **  \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 **
	 **  \return const reverse iterator3d value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   slip::Box3d<int> b(1,1,1,2,2,3);
	 **   //Print M within a box in reverse order
	 **   std::copy(M.rfront_upper_left(b),M.rback_bottom_right(b),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator3d rback_bottom_right(const Box3d<int>& box) const;


	/*@} End three dimensionnal iterators : Box iterators */

	/**
	 ** \name three dimensionnal iterators : Range iterators
	 */

	/*@{*/

	//------------------------ Range iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator3d_range that points to the
	 **  front upper left element of the ranges \a slice_range, \a row_range and \a col_range
	 **  associated to the %GenericMultiComponent3d.
	 **
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 **  \return iterator3d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator3d_range front_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range);

	/*!
	 **  \brief Returns a read/write iterator3d_range that points to the
	 **   past the end back bottom right element of the ranges \a slice_range, \a row_range
	 **   and \a col_range associated to the %GenericMultiComponent3d.
	 **
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 **  \return iterator3d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	iterator3d_range back_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range);

	/*!
	 **  \brief Returns a read-only iterator3d_range that points to the
	 **   to the front upper left element of the ranges \a slice_range, \a row_range and \a col_range
	 **   associated to the %GenericMultiComponent3d.
	 **
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 **  \return const_iterator3d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_iterator3d_range front_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range) const;


	/*!
	 **  \brief Returns a read-only iterator3d_range that points to the past
	 **  the end back bottom right element of the ranges \a slice_range, \a row_range and \a col_range
	 **  associated to the %GenericMultiComponent3d.
	 **
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 **  \return const_iterator3d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range
	 **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_iterator3d_range back_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range) const;

	/*!
	 **  \brief Returns a read/write reverse_iterator3d_range that points to the
	 **  past the back bottom right element of the ranges \a row_range and
	 **  \a col_range associated to the %GenericMultiComponent3d. Iteration is done in the
	 **  reverse order.
	 **
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 **  \return reverse_iterator3d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator3d_range rfront_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range);

	/*!
	 **  \brief Returns a read/write reverse_iterator3d_range that points
	 **   to one before the front upper left element of the ranges \a row_range
	 **   and \a col_range associated to the %GenericMultiComponent3d. Iteration is done
	 **   in the reverse order.
	 **
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 **  \return reverse_iterator3d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > M(3,4,5);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	reverse_iterator3d_range rback_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range);

	/*!
	 **  \brief Returns a read-only reverse_iterator3d_range that points
	 **   to the past the back bottom right element of the ranges \a row_range and
	 **   \a col_range associated to the %GenericMultiComponent3d. Iteration is done in the
	 **   reverse order.
	 **
	 **  \param slice_range The range of the slices.
	 **  \param row_range The range of the rows.
	 **  \param col_range The range of the columns.
	 **
	 **  \pre slice_range, row_range and col_range  must be valid.
	 **  \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 **  \return const_reverse_iterator3d_range value
	 **  \remarks This iterator is compatible with BidirectionalIterator
	 **           algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator3d_range rfront_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range) const;

	/*!
	 **  \brief Returns a read-only reverse_iterator3d_range that points
	 **  to one before the front upper left element of the ranges \a row_range
	 **  and \a col_range associated to the %GenericMultiComponent3d.Iteration is done in
	 **  the reverse order.
	 **
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 ** \return const_reverse_iterator3d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **  \par Example
	 **  \code
	 **   slip::GenericMultiComponent3d<slip::color<double> > const M(3,4,5);
	 **   slip::Range<int> sr(0,2,2);
	 **   slip::Range<int> rr(0,3,2);
	 **   slip::Range<int> cr(0,4,2);
	 **   //Print M within a range in reverse order
	 **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
	 **             std::ostream_iterator<slip::color<double> >(std::cout," "));
	 **  \endcode
	 */
	const_reverse_iterator3d_range rback_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range) const;


	/*@} End three dimensionnal iterators : Range iterators */

	//**************************************************************************************************************
	//**************************************************************************************************************
	//************************                   Component iterator              ***********************************
	//**************************************************************************************************************
	//**************************************************************************************************************

	/**
	 ** \name One dimensional component global iterators
	 */
	/*@{*/

	//------------------------ Component iterators------------------------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element in the %GenericMultiComponent3d.  Iteration is done in ordinary
	 **  element order.
	 ** \param component The component number.
	 ** \return begin component_iterator value
	 */
	component_iterator begin(const std::size_t component);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  first element in the %GenericMultiComponent3d.  Iteration is done in ordinary
	 **  element order.
	 ** \param component The component number.
	 ** \return const begin component_iterator value
	 */
	const_component_iterator begin(const std::size_t component) const;

	/*!
	 **  \brief Returns a read/write iterator that points one past the last
	 **  element in the %GenericMultiComponent3d.  Iteration is done in ordinary
	 **  element order.
	 ** \param component The component number.
	 ** \return end component_iterator value
	 */
	component_iterator end(const std::size_t component);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points one past
	 **  the last element in the %GenericMultiComponent3d.  Iteration is done in
	 **  ordinary element order.
	 ** \param component The component number.
	 ** \return const end component_iterator value
	 */
	const_component_iterator end(const std::size_t component) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element in the %GenericMultiComponent3d.  Iteration is done in ordinary
	 **  element order.
	 ** \param component The component number.
	 ** \return begin reverse_component_iterator value
	 */
	reverse_component_iterator rbegin(const std::size_t component);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  first element in the %GenericMultiComponent3d.  Iteration is done in ordinary
	 **  element order.
	 ** \param component The component number.
	 ** \return const begin reverse_component_iterator value
	 */
	const_reverse_component_iterator rbegin(const std::size_t component) const;

	/*!
	 **  \brief Returns a read/write iterator that points one past the last
	 **  element in the %GenericMultiComponent3d.  Iteration is done in ordinary
	 **  element order.
	 ** \param component The component number.
	 ** \return end reverse_component_iterator value
	 */
	reverse_component_iterator rend(const std::size_t component);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points one past
	 **  the last element in the %GenericMultiComponent3d.  Iteration is done in
	 **  ordinary element order.
	 ** \param component The component number.
	 ** \return const end reverse_component_iterator value
	 */
	const_reverse_component_iterator rend(const std::size_t component) const;



	/*@} End One dimensional component global iterators */

	/**
	 ** \name One dimensional component slice iterators
	 */
	/*@{*/
	//--------------------One dimensional component slice iterators----------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 **  \param component The component number.
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return slice_iterator value
	 */
	component_slice_iterator slice_begin(const std::size_t component, const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the first
	 **  element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 **  \param component The component number.
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_slice_iterator value
	 */
	const_component_slice_iterator slice_begin(const std::size_t component, const size_type row, const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the one past the end
	 **  element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 **  \param component The component number.
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return slice_iterator value
	 */
	component_slice_iterator slice_end(const std::size_t component, const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the one past the end
	 **  element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **	Iteration is done in ordinary element order (increasing slice number).
	 **  \param component The component number.
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_slice_iterator value
	 */
	const_component_slice_iterator slice_end(const std::size_t component, const size_type row, const size_type col) const;


	/*!
	 **  \brief Returns a read/write iterator that points to the
	 **  last element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 ** 	Iteration is done in reverse element order (decreasing slice number).
	 **  \param component The component number.
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 ** 	\return reverse_slice_iterator value
	 */
	reverse_component_slice_iterator slice_rbegin(const std::size_t component, const size_type row, const size_type col);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  last element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **	Iteration is done in reverse element order (decreasing slice number).
	 **  \param component The component number.
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_reverse_slice_iterator value
	 */
	const_reverse_component_slice_iterator slice_rbegin(const std::size_t component, const size_type row, const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the
	 **  one before the first element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order (decreasing slice number).
	 **  \param component The component number.
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 ** \return reverse_slice_iterator value
	 */
	reverse_component_slice_iterator slice_rend(const std::size_t component, const size_type row, const size_type col);


	/*!
	 **  \brief Returns a read (constant) iterator that points to the
	 **  one before the first element of the line (row,col) threw the slices in the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order (decreasing slice number).
	 **  \param component The component number.
	 **	\param row row coordinate of the line
	 **	\param col col coordinate of the line
	 **	\return const_reverse_slice_iterator value
	 */
	const_reverse_component_slice_iterator slice_rend(const std::size_t component, const size_type row, const size_type col) const;

	/*@} End One dimensional component slice iterators */

	/**
	 ** \name One dimensional component row iterators
	 */
	/*@{*/
	//-------------------row component iterators----------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row.
	 ** \return begin row_iterator value
	 */
	component_row_iterator row_begin(const std::size_t component, const size_type slice,
			const size_type row);

	/*!
	 **  \brief Returns a read_only iterator that points to the first
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row.
	 ** \return begin const_row_iterator value
	 */
	const_component_row_iterator row_begin(const std::size_t component, const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 **  \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row.
	 ** \return end row_iterator value
	 */
	component_row_iterator row_end(const std::size_t component, const size_type slice,
			const size_type row);


	/*!
	 **  \brief Returns a read_only iterator that points to the past-the-end
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row.
	 ** \param component The component number.
	 ** \return end const_row_iterator value
	 */
	const_component_row_iterator row_end(const std::size_t component, const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the last
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row.
	 ** \return begin reverse_row_iterator value
	 */
	reverse_component_row_iterator row_rbegin(const std::size_t component, const size_type slice,
			const size_type row);

	/*!
	 **  \brief Returns a read_only reverse iterator that points to the last
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row.
	 ** \return begin const_reverse_row_iterator value
	 */
	const_reverse_component_row_iterator row_rbegin(const std::size_t component, const size_type slice,
			const size_type row) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row.
	 ** \return end reverse_row_iterator value
	 */
	reverse_component_row_iterator row_rend(const std::size_t component, const size_type slice,
			const size_type row);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the first
	 **  element of the row \a row of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 **  \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row.
	 ** \return end const_reverse_row_iterator value
	 */
	const_reverse_component_row_iterator row_rend(const std::size_t component, const size_type slice,
			const size_type row) const;


	/*@} End One dimensional component row iterators */
	/**
	 ** \name One dimensional component col iterators
	 */
	/*@{*/
	//-------------------Constant component col iterators----------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column
	 ** \return begin col_iterator value
	 */
	component_col_iterator col_begin(const std::size_t component, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only iterator that points to the first
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 **\param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column
	 ** \return begin const_col_iterator value
	 */
	const_component_col_iterator col_begin(const std::size_t component, const size_type slice,
			const size_type col) const;

	/*!
	 **  \brief Returns a read/write iterator that points to the past-the-end
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column
	 ** \return end col_iterator value
	 */
	component_col_iterator col_end(const std::size_t component, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only iterator that points to the past-the-end
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column
	 ** \return end const_col_iterator value
	 */
	const_component_col_iterator col_end(const std::size_t component, const size_type slice,
			const size_type col) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the last
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column
	 ** \return begin reverse_col_iterator value
	 */
	reverse_component_col_iterator col_rbegin(const std::size_t component, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the last
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column
	 ** \return begin const_reverse_col_iterator value
	 */
	const_reverse_component_col_iterator col_rbegin(const std::size_t component, const size_type slice,
			const size_type col) const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to the first
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column
	 ** \return end reverse_col_iterator value
	 */
	reverse_component_col_iterator col_rend(const std::size_t component, const size_type slice,
			const size_type col);


	/*!
	 **  \brief Returns a read_only reverse iterator that points to the first
	 **  element of the column \a column of the slice \a slice in the %GenericMultiComponent3d.
	 **  Iteration is done modulo the number of columns.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column
	 ** \return end const_reverse_col_iterator value
	 */
	const_reverse_component_col_iterator col_rend(const std::size_t component, const size_type slice,
			const size_type col) const;


	/*@} End One dimensional component col iterators */

	/**
	 ** \name One dimensional component slice range iterators
	 */
	/*@{*/
	//------------------------component slice range iterators -----------------------

	/*!
	 ** \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::Array3d<double> A1(8,8,8);
	 **  slip::Array3d<double> A2(4,8,8);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the line (0,0) of A1 iterated according to the
	 ** //range in the line (1,1) of A2
	 ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),A2.slice_begin(1,1));
	 ** \endcode
	 */
	component_slice_range_iterator slice_begin(const std::size_t component, const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points one past the end
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::Array3d<double> A1(8,8,8);
	 **  slip::Array3d<double> A2(4,8,8);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the line (0,0) of A1 iterated according to the
	 ** //range in the line (1,1) of A2
	 ** std::copy(A1.slice_range_begin(0,0,range),A1.slice_range_end(0,0,range),A2.slice_begin(1,1));
	 ** \endcode
	 */
	component_slice_range_iterator slice_end(const std::size_t component, const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the first
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::Array3d<double> const A1(M); //M is an already existing %GenericMultiComponent3d
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //display the elements of the line (0,0) of A1 iterated according to the range
	 ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 */
	const_component_slice_range_iterator slice_begin(const std::size_t component, const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the last
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in ordinary element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::Array3d<double> const A1(M); //M is an already existing %GenericMultiComponent3d
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //display the elements of the line (0,0) of A1 iterated according to the range
	 ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),std::ostream_iterator<double>(std::cout," "));
	 ** \endcode
	 */
	const_component_slice_range_iterator slice_end(const std::size_t component, const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*!
	 ** \brief Returns a read/write iterator that points to the last
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in the reverse element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	reverse_component_slice_range_iterator slice_rbegin(const std::size_t component, const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	reverse_component_slice_range_iterator slice_rend(const std::size_t component, const size_type row,const size_type col,
			const slip::Range<int>& range);


	/*!
	 ** \brief Returns a read only (constant) iterator that points to the last
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return begin const_reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	const_reverse_component_slice_range_iterator slice_rbegin(const std::size_t component, const size_type row,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read_only iterator that points one past the lastto the first
	 ** element of the %Range \a range of the of the line (row,col)
	 ** threw the slices in the %GenericMultiComponent3d.
	 ** Iteration is done in reverse element order according to the
	 ** %Range.
	 ** \param component The component number.
	 ** \param row row coordinate of the line
	 ** \param col col coordinate of the line
	 ** \param range %Range of the line to iterate.
	 ** \return end const_reverse_slice_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre the range must be compatible with the slice dimensions
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	const_reverse_component_slice_range_iterator slice_rend(const std::size_t component, const size_type row,const size_type col,
			const slip::Range<int>& range) const;


	/*@} End One dimensional component slice range iterators */

	/**
	 ** \name One dimensional component row range iterators
	 */
	/*@{*/
	//------------------------component row range iterators -----------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element of the %Range \a range of the row \a row in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::Array3d<double> A1(8,8,8);
	 **  slip::Array3d<double> A2(8,4,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the row 0 of A1 iterated according to the
	 ** //range in the row 1 of A2
	 ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
	 ** \endcode
	 */
	component_row_range_iterator row_begin(const std::size_t component, const size_type slice,const size_type row,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read/write iterator that points one past the end
	 **  element of the %Range \a range of the row \a row in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return end row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::Array3d<double> A1(8,8,8);
	 **  slip::Array3d<double> A2(8,4,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the row 0 of A1 iterated according to the
	 ** //range in the row 1 of A2
	 ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
	 ** \endcode
	 */
	component_row_range_iterator row_end(const std::size_t component, const size_type slice,const size_type row,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the row \a row in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return begin const_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::Array3d<double> A1(8,8,8);
	 **  slip::Array3d<double> A2(8,4,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the row 0 of A1 iterated according to the
	 ** //range in the row 1 of A2
	 ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
	 ** \endcode
	 */
	const_component_row_range_iterator row_begin(const std::size_t component, const size_type slice,const size_type row,
			const slip::Range<int>& range) const;


	/*!
	 **  \brief Returns a read_only iterator that points one past the last
	 **  element of the %Range range of the row \a row in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row Row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return begin const_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::Array3d<double> A1(8,8,8);
	 **  slip::Array3d<double> A2(8,4,5);
	 **  slip::Range<int> range(0,A1.dim2()-1,2);
	 ** //copy the the elements of the row 0 of A1 iterated according to the
	 ** //range in the row 1 of A2
	 ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
	 ** \endcode
	 */
	const_component_row_range_iterator row_end(const std::size_t component, const size_type slice,const size_type row,
			const slip::Range<int>& range) const;

	/*!
	 ** \brief Returns a read-write iterator that points to the last
	 ** element of the %Range \a range of the row of a slice \a row and slice in the %GenericMultiComponent3d.
	 ** Iteration is done in the reverse element order according to the %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	reverse_component_row_range_iterator row_rbegin(const std::size_t component, const size_type slice,const size_type row,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-write iterator that points one before
	 **  the first element of the %Range \a range of the row of a slice \a row in the
	 **  %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 **  \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate.
	 ** \return reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	reverse_component_row_range_iterator row_rend(const std::size_t component, const size_type slice,const size_type row,
			const slip::Range<int>& range);



	/*!
	 **  \brief Returns a read-only iterator that points to the last
	 **  element of the %Range \a range of the row  of a slice \a row in the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return begin const_reverse_row_range_iterator value
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	const_reverse_component_row_range_iterator row_rbegin(const std::size_t component, const size_type slice,const size_type row,
			const slip::Range<int>& range) const;


	/*!
	 **  \brief Returns a read-only iterator that points one before the first
	 **  element of the %Range \a range of the row of a slice \a row in the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param row The index of the row to iterate.
	 ** \param range %Range of the row to iterate
	 ** \return const_reverse_row_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre row must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	const_reverse_component_row_range_iterator row_rend(const std::size_t component, const size_type slice,const size_type row,
			const slip::Range<int>& range) const;

	/*@} End One dimensional component row range iterators */
	/**
	 ** \name One dimensional component col range iterators
	 */
	/*@{*/
	//------------------------component col range iterators -----------------------


	/*!
	 **  \brief Returns a read-write iterator that points to the first
	 **  element of the %Range \a range of the col \a col in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate
	 ** \return begin col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::Array3d<double> A1(8,8,8);
	 **  slip::Array3d<double> A2(4,8,5);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the column 0 of A1 iterated according to the
	 ** //range in the column 1 of A2
	 ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
	 ** \endcode
	 */
	component_col_range_iterator col_begin(const std::size_t component, const size_type slice,const size_type col,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read-write iterator that points to the past
	 **  the end element of the %Range \a range of the col \a col in the
	 **  %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::Array3d<double> A1(8,8,8);
	 **  slip::Array3d<double> A2(4,8,5);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the column 0 of A1 iterated according to the
	 ** //range in the column 1 of A2
	 ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
	 ** \endcode
	 */
	component_col_range_iterator col_end(const std::size_t component, const size_type slice,const size_type col,
			const slip::Range<int>& range);


	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the col \a col in the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::Array3d<double> A1(8,8,8);
	 **  slip::Array3d<double> A2(4,8,5);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the column 0 of A1 iterated according to the
	 ** //range in the column 1 of A2
	 ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
	 ** \endcode
	 */
	const_component_col_range_iterator col_begin(const std::size_t component, const size_type slice,const size_type col,
			const slip::Range<int>& range) const;


	/*!
	 **  \brief Returns a read-only iterator that points to the past
	 **  the end element of the %Range \a range of the col \a col in
	 **  the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 ** \code
	 **
	 **  slip::Array3d<double> A1(8,8,8);
	 **  slip::Array3d<double> A2(4,8,5);
	 **  slip::Range<int> range(0,A1.dim1()-1,2);
	 ** //copy the the elements of the column 0 of A1 iterated according to the
	 ** //range in the column 1 of A2
	 ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
	 ** \endcode
	 */
	const_component_col_range_iterator col_end(const std::size_t component, const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 **  \brief Returns a read-write iterator that points to the last
	 **  element of the %Range \a range of the col of a slice \a col in the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	reverse_component_col_range_iterator col_rbegin(const std::size_t component, const size_type slice,const size_type col,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read-write iterator that points to one before
	 **  the first element of the %Range range of the col of a slice \a col in the
	 **  %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	reverse_component_col_range_iterator col_rend(const std::size_t component, const size_type slice,const size_type col,
			const slip::Range<int>& range);

	/*!
	 **  \brief Returns a read_only iterator that points to the last
	 **  element of the %Range \& range of the col of a slice \a col in the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return begin const_reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	const_reverse_component_col_range_iterator col_rbegin(const std::size_t component, const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*!
	 **  \brief Returns a read-only iterator that points to the first
	 **  element of the %Range \a range of the col of a slice \a col in the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse element order according to the
	 **  %Range.
	 ** \param component The component number.
	 ** \param slice The index of the slice.
	 ** \param col The index of the column to iterate.
	 ** \param range %Range of the column to iterate.
	 ** \return const_reverse_col_range_iterator value
	 ** \remarks This iterator is compatible with RandomAccessIterator
	 **          algorithms.
	 **
	 ** \pre col must be compatible with the range of the %GenericMultiComponent3d.
	 ** \pre The range must be inside the whole range of the %GenericMultiComponent3d.
	 */
	const_reverse_component_col_range_iterator col_rend(const std::size_t component, const size_type slice,const size_type col,
			const slip::Range<int>& range) const;

	/*@} End One dimensional component col range iterators */

	//****************************************************************************
	//                          One dimensional component plane iterators
	//****************************************************************************

	/**
	 ** \name One dimensional component global plane iterators
	 */
	/*@{*/

	//----------------------Component global plane iterators------------------------------

	/*!
	 **  \brief Returns a read/write iterator that points to the first
	 **  element in the in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param component The component number.
	 ** \param slice the slice coordinate of the plane
	 ** \return begin component iterator value
	 */
	component_iterator plane_begin(const std::size_t component, const size_type slice);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points to the
	 **  first element in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param component The component number.
	 ** \param slice the slice coordinate of the plane
	 ** \return const begin component iterator value
	 */
	const_component_iterator plane_begin(const std::size_t component, const size_type slice) const;

	/*!
	 **  \brief Returns a read/write iterator that points one past the last
	 **  element in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param component The component number.
	 ** \param slice the slice coordinate of the plane
	 ** \return end component iterator value
	 */
	component_iterator plane_end(const std::size_t component, const size_type slice);

	/*!
	 **  \brief Returns a read-only (constant) iterator that points one past
	 **  the last element in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in ordinary element order.
	 ** \param component The component number.
	 ** \param slice the slice coordinate of the plane
	 ** \return const end component iterator value
	 */
	const_component_iterator plane_end(const std::size_t component, const size_type slice) const;


	/*!
	 **  \brief Returns a read/write reverse iterator that points to the
	 **  last element in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 ** \param component The component number.
	 ** \param slice the slice coordinate of the plane
	 ** \return reverse begin component iterator value
	 */
	reverse_component_iterator plane_rbegin(const std::size_t component, const size_type slice);

	/*!
	 **  \brief Returns a read-only (constant) reverse iterator that points
	 **  to the last element in the slice plane k of the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 ** \param component The component number.
	 ** \param slice the slice coordinate of the plane
	 ** \return const reverse begin component iterator value
	 */
	const_reverse_component_iterator plane_rbegin(const std::size_t component, const size_type slice) const;

	/*!
	 **  \brief Returns a read/write reverse iterator that points to one
	 **  before the first element in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 ** \param component The component number.
	 **  \param slice the slice coordinate of the plane
	 **  \return reverse end component iterator value
	 */
	reverse_component_iterator plane_rend(const std::size_t component, const size_type slice);

	/*!
	 **  \brief Returns a read-only (constant) reverse iterator that points
	 **  to one before the first element in the slice plane of the %GenericMultiComponent3d.
	 **  Iteration is done in reverse element order.
	 ** \param component The component number.
	 **  \param slice the slice coordinate of the plane
	 **  \return const reverse end component iterator value
	 */
	const_reverse_component_iterator plane_rend(const std::size_t component, const size_type slice) const;

	/*@} End One dimensional component global plane iterators */

	//****************************************************************************
	//                          Three dimensional component iterators
	//****************************************************************************

	/**
	 ** \name Three dimensionnal component iterators : Global component iterators
	 */
	/*@{*/

	//------------------------ Global component iterators------------------------------------

	/*!
	 **  \brief Returns a read/write component_iterator3d that points to the first
	 **  element of the %GenericMultiComponent3d. It points to the front upper left element of
	 **  the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \return begin iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::Vector3d<double> > A1(8,8,4);
	 **  slip::GenericMultiComponent3d<slip::Vector3d<double> > A2(8,8,4);
	 ** //copy the first component of the elements of A1 in A2
	 ** std::copy(A1.front_upper_left(0),A1.back_bottom_right(0),A2.front_upper_left(0));
	 ** \endcode
	 */
	component_iterator3d front_upper_left(const std::size_t component);

	/*!
	 **  \brief Returns a read/write component_iterator3d that points to the past
	 **  the end element of the %GenericMultiComponent3d. It points to past the end element of
	 **  the back bottom right element of the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \return begin iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 ** \par Example:
	 ** \code
	 **
	 **  slip::GenericMultiComponent3d<slip::Vector3d<double> > A1(8,8,4);
	 **  slip::GenericMultiComponent3d<slip::Vector3d<double> > A2(8,8,4);
	 ** //copy the first component of the elements of A1 in A2
	 ** std::copy(A1.front_upper_left(0),A1.back_bottom_right(0),A2.front_upper_left(0));
	 ** \endcode
	 */
	component_iterator3d back_bottom_right(const std::size_t component);

	/*!
	 **  \brief Returns a read only const_component_iterator3d that points to the first
	 **  element of the %GenericMultiComponent3d. It points to the front upper left element of
	 **  the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \return begin iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_component_iterator3d front_upper_left(const std::size_t component) const;

	/*!
	 **  \brief Returns a read only const_component_iterator3d that points to the past
	 **  the end element of the %GenericMultiComponent3d. It points to past the end element of
	 **  the back bottom right element of the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \return begin iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_component_iterator3d back_bottom_right(const std::size_t component) const;

	/*!
	 **  \brief Returns a read/write reverse component iterator3d. It points to the
	 **   back bottom right element of the %GenericMultiComponent3d.
	 *    Iteration is done within the %GenericMultiComponent3d in the reverse order.
	 **
	 ** \param component The component number.
	 ** \return reverse_component_iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	reverse_component_iterator3d rfront_upper_left(const std::size_t component);

	/*!
	 **  \brief Returns a read/write reverse component iterator3d. It points to past the
	 **  front upper left element of the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \param component The component number.
	 ** \return reverse component iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	reverse_component_iterator3d rback_bottom_right(const std::size_t component);

	/*!
	 **  \brief Returns a read only reverse component iterator3d that points.  It points
	 **  to the back bottom right element of the %GenericMultiComponent3d.
	 **  Iteration is done within the %GenericMultiComponent3d in the reverse order.
	 **
	 ** \param component The component number.
	 ** \return const_reverse_component_iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_reverse_component_iterator3d rfront_upper_left(const std::size_t component) const;


	/*!
	 **  \brief Returns a read only reverse component iterator3d. It points to past the
	 **  front upper left element of the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \param component The component number.
	 ** \return const reverse component iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_reverse_component_iterator3d rback_bottom_right(const std::size_t component) const;

	//------------------------ Box component iterators------------------------------------

	/*!
	 **  \brief Returns a read/write component iterator3d that points to the first
	 **  element of the %GenericMultiComponent3d. It points to the front upper left element of
	 **  the \a %Box3d associated to the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param box A %Box3d defining the range of indices to iterate
	 **        within the %GenericMultiComponent3d.
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **
	 ** \return end iterator3d value
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 */
	component_iterator3d front_upper_left(const std::size_t component,
			const Box3d<int>& box);

	/*!
	 **  \brief Returns a read/write component iterator3d that points to the past
	 **  the end element of the %GenericMultiComponent3d. It points to past the end element of
	 **  the back bottom right element of the \a %Box3d associated to the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param box a %Box3d defining the range of indices to iterate
	 **        within the %GenericMultiComponent3d.
	 **
	 ** \return end iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 **
	 */
	component_iterator3d back_bottom_right(const std::size_t component,
			const Box3d<int>& box);

	/*!
	 **  \brief Returns a read only component iterator3d that points to the first
	 **  element of the %GenericMultiComponent3d. It points to the front upper left element of
	 **  the \a %Box3d associated to the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param box a %Box3d defining the range of indices to iterate
	 **        within the %GenericMultiComponent3d.
	 ** \return end const iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 */
	const_component_iterator3d front_upper_left(const std::size_t component,
			const Box3d<int>& box) const;

	/*!
	 **  \brief Returns a read only component iterator3d that points to the past
	 **  the end element of the %GenericMultiComponent3d. It points to past the end element of
	 **  the back bottom right element of the \a %Box3d associated to the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param box a %Box3d defining the range of indices to iterate
	 **        within the %GenericMultiComponent3d.
	 ** \return end const iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 **
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 */
	const_component_iterator3d back_bottom_right(const std::size_t component,
			const Box3d<int>& box) const;

	/*!
	 **  \brief Returns a read/write reverse component iterator3d. It points to the
	 **  back bottom right element of the \a %Box3d associated to the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \param component The component number.
	 ** \param box a %Box3d defining the range of indices to iterate
	 **        within the %GenericMultiComponent3d.
	 **
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 **
	 ** \return reverse iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	reverse_component_iterator3d rfront_upper_left(const std::size_t component,
			const Box3d<int>& box);

	/*!
	 **  \brief Returns a read/write reverse component iterator3d. It points to one
	 **  before the front upper left element of the %Box3d \a box associated to
	 **  the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param box A %Box3d defining the range of indices to iterate
	 **        within the %GenericMultiComponent3d.
	 **
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 **
	 ** \return reverse iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	reverse_component_iterator3d rback_bottom_right(const std::size_t component,
			const Box3d<int>& box);

	/*!
	 **  \brief Returns a read only reverse component iterator3d. It points to the
	 **  back bottom right element of the %Box3d \a box associated to the %GenericMultiComponent3d.
	 **  Iteration is done in the reverse order.
	 **
	 ** \param component The component number.
	 ** \param box A %Box3d defining the range of indices to iterate
	 **        within the %GenericMultiComponent3d.
	 **
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 **
	 ** \return const reverse iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_reverse_component_iterator3d rfront_upper_left(const std::size_t component,
			const Box3d<int>& box) const;


	/*!
	 **  \brief Returns a read-only reverse component iterator3d. It points to one
	 **  before the front upper left element of the %Box3d \a box associated to
	 **  the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param box A %Box3d defining the range of indices to iterate
	 **        within the %GenericMultiComponent3d.
	 **
	 ** \pre The box indices must be inside the range of the %GenericMultiComponent3d ones.
	 **
	 ** \return const reverse iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_reverse_component_iterator3d rback_bottom_right(const std::size_t component,
			const Box3d<int>& box) const;

	//------------------------ Range component iterators------------------------------------

	/*!
	 **  \brief Returns a read/write component_iterator3d_range that points to the
	 **  front upper left element of the ranges \a slice_range, \a row_range and \a col_range
	 **  associated to the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 ** \return iterator3d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	component_iterator3d_range front_upper_left(const std::size_t component,
			const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range);

	/*!
	 **  \brief Returns a read/write component_iterator3d_range that points to the
	 **   past the end back bottom right element of the ranges \a slice_range, \a row_range
	 **   and \a col_range associated to the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 ** \return iterator3d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	component_iterator3d_range back_bottom_right(const std::size_t component,
			const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range);

	/*!
	 **  \brief Returns a read/write component_const_iterator3d_range that points to the
	 **  front upper left element of the ranges \a slice_range, \a row_range and \a col_range
	 **  associated to the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 ** \return iterator3d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_component_iterator3d_range front_upper_left(const std::size_t component,
			const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range) const;

	/*!
	 **  \brief Returns a read/write const_component iterator3d_range that points to the
	 **   past the end back bottom right element of the ranges \a slice_range, \a row_range
	 **   and \a col_range associated to the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 ** \return iterator3d_range value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_component_iterator3d_range back_bottom_right(const std::size_t component,
			const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range) const;

	/*!
	 **  \brief Returns a read/write reverse_component_iterator3d_range. It points to the
	 **  back bottom right element of the ranges \a slice_range, \a row_range
	 **   and \a col_range associated to the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 ** \return reverse iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	reverse_component_iterator3d_range rfront_upper_left(const std::size_t component,
			const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range);

	/*!
	 **  \brief Returns a read/write reverse_component_iterator3d_range. It points to one
	 **  before the front upper left element of the ranges \a slice_range, \a row_range
	 **   and \a col_range associated to the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 ** \return reverse iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	reverse_component_iterator3d_range rback_bottom_right(const std::size_t component,
			const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range);

	/*!
	 **  \brief Returns a read/write reverse_const_component_iterator3d_range. It points to the
	 **  back bottom right element of the ranges \a slice_range, \a row_range
	 **   and \a col_range associated to the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 ** \return reverse iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_reverse_component_iterator3d_range rfront_upper_left(const std::size_t component,
			const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range) const;

	/*!
	 **  \brief Returns a read/write reverse_const_component_iterator3d_range. It points to one
	 **  before the front upper left element of the ranges \a slice_range, \a row_range
	 **   and \a col_range associated to the %GenericMultiComponent3d.
	 **
	 ** \param component The component number.
	 ** \param slice_range The range of the slices.
	 ** \param row_range The range of the rows.
	 ** \param col_range The range of the columns.
	 **
	 ** \pre slice_range, row_range and col_range  must be valid.
	 ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent3d ones.
	 **
	 ** \return reverse iterator3d value
	 ** \remarks This iterator is compatible with BidirectionalIterator
	 **          algorithms.
	 */
	const_reverse_component_iterator3d_range rback_bottom_right(const std::size_t component,
			const Range<int>& slice_range, const Range<int>& row_range,
			const Range<int>& col_range) const;

	/*@} End Three dimensionnal component iterators : Global component iterators */

	/**
	 ** \name i/o operators
	 */
	/*@{*/

	/*!
	 ** \brief Write the GenericMultiComponent3d to the ouput stream
	 ** \param out output stream
	 ** \param a GenericMultiComponent3d to write to the output stream
	 */
	friend std::ostream& operator<< <>(std::ostream & out, const self& a);

	/*@} End i/o operators */

	/**
	 ** \name  Assignment operators and methods
	 */
	/*@{*/

	/*!
	 ** \brief Assign a GenericMultiComponent3d.
	 **
	 ** Assign elements of GenericMultiComponent3d in \a rhs
	 **
	 ** \param rhs GenericMultiComponent3d to get the values from.
	 ** \return
	 */
	self& operator=(const self & rhs);

	/*!
	 ** \brief Affects all the element of the %GenericMultiComponent3d by val
	 ** \param val affectation value
	 ** \return reference to corresponding %GenericMultiComponent3d
	 */
	self& operator=(const Block& val);


	/*!
	 ** \brief Affects all the element of the %GenericMultiComponent3d by val
	 ** \param val affectation value
	 ** \return reference to corresponding %GenericMultiComponent3d
	 */
	self& operator=(const typename Block::value_type& val);



	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with copies of value
	 ** \param value  A reference-to-const of arbitrary type.
	 */
	void fill(const Block& value)
	{
		std::fill_n(this->begin(),size(),value);
	}

	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of
	 **        the value array
	 ** \param value  A pointer of arbitrary type.
	 */
	void fill(const typename Block::pointer value)
	{
		std::copy(value,value + size(), (typename Block::pointer)begin());
	}

	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of
	 **        the value array
	 ** \param value  A pointer of arbitrary type.
	 */
	void fill(const Block* value)
	{
		std::copy(value,value + size(), this->begin());
	}

	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of the range [first,last)
	 **  \param  first  An input iterator.
	 **  \param  last   An input iterator.
	 **
	 **
	 */
	template<typename InputIterator>
	void fill(InputIterator first,
			InputIterator last)
	{
		std::copy(first,last,this->begin());
	}

	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of the range [first,last)
	 **  \param  first_iterators_list  An input iterator list.
	 **  \param  last   An input iterator.
	 **
	 **
	 */
	template<typename InputIterator>
	void fill(std::vector<InputIterator> first_iterators_list,
			InputIterator last)
	{
		std::vector<typename slip::kstride_iterator<typename Block::pointer,Block::SIZE> > iterators_list(Block::SIZE);

		for(std::size_t component = 0; component < Block::SIZE; ++component)
		{
			iterators_list[component]= slip::kstride_iterator<typename Block::pointer,Block::SIZE>((typename Block::pointer)this->begin() + component);
		}


		while(first_iterators_list[0] != last)
		{
			for(std::size_t component = 0; component < Block::SIZE; ++component)
			{
				*iterators_list[component]++ = *first_iterators_list[component]++;
			}
		}
		iterators_list.clear();
	}


	/*!
	 ** \brief Fills the container range [begin(),begin()+size())
	 **        with a copy of the range [first,last)
	 ** \param component The component number.
	 ** \param  first  An input iterator.
	 ** \param  last   An input iterator.
	 **
	 **
	 */
	template<typename InputIterator>
	void fill(std::size_t component,
			InputIterator first,
			InputIterator last)
	{
		std::copy(first,last,this->begin(component));
	}

	/*@} End Assignment operators and methods*/

	/**
	 ** \name Comparison operators
	 */
	/*@{*/
	/*!
	 ** \brief GenericMultiComponent3d equality comparison
	 ** \param x A %GenericMultiComponent3d
	 ** \param y A %GenericMultiComponent3d of the same type of \a x
	 ** \return true iff the size and the elements of the Arrays are equal
	 ** \pre x.size() == y.size()
	 */
	friend bool operator== <>(const GenericMultiComponent3d<Block>& x,
			const GenericMultiComponent3d<Block>& y);

	/*!
	 ** \brief GenericMultiComponent3d inequality comparison
	 ** \param x A %GenericMultiComponent3d
	 ** \param y A %GenericMultiComponent3d of the same type of \a x
	 ** \return true if !(x == y)
	 ** \pre x.size() == y.size()
	 */
	friend bool operator!= <>(const GenericMultiComponent3d<Block>& x,
			const GenericMultiComponent3d<Block>& y);

	//  /*!
	//   ** \brief Less than comparison operator (GenericMultiComponent3d ordering relation)
	//   ** \param x A %GenericMultiComponent3d
	//   ** \param y A %GenericMultiComponent3d of the same type of \a x
	//   ** \return true iff \a x is lexicographically less than \a y
	//   ** \pre x.size() == y.size()
	//   */
	//   friend bool operator< <>(const GenericMultiComponent3d<Block>& x,
	// 			   const GenericMultiComponent3d<Block>& y);

	//  /*!
	//   ** \brief More than comparison operator
	//   ** \param x A %GenericMultiComponent3d
	//   ** \param y A %GenericMultiComponent3d of the same type of \a x
	//   ** \return true iff y > x
	//   ** \pre x.size() == y.size()
	//   */
	//   friend bool operator> <>(const GenericMultiComponent3d<Block>& x,
	// 			   const GenericMultiComponent3d<Block>& y);

	//   /*!
	//   ** \brief Less than equal comparison operator
	//   ** \param x A %GenericMultiComponent3d
	//   ** \param y A %GenericMultiComponent3d of the same type of \a x
	//   ** \return true iff !(y > x)
	//   ** \pre x.size() == y.size()
	//   */
	//   friend bool operator<= <>(const GenericMultiComponent3d<Block>& x,
	// 			    const GenericMultiComponent3d<Block>& y);

	//   /*!
	//   ** \brief More than equal comparison operator
	//   ** \param x A %GenericMultiComponent3d
	//   ** \param y A %GenericMultiComponent3d of the same type of \a x
	//   ** \return true iff !(x < y)
	//   ** \pre x.size() == y.size()
	//   */
	//   friend bool operator>= <>(const GenericMultiComponent3d<Block>& x,
	// 			    const GenericMultiComponent3d<Block>& y);


	/*@} Comparison operators */

	/**
	 ** \name  Element access operators
	 */
	/*@{*/
	/*!
	 ** \brief Subscript access to the slice datas contained in the %GenericMultiComponent3d.
	 ** \param k The index of the slice for which data should be accessed.
	 ** \return Read/write pointer to the slice data.
	 ** \pre k < slices()
	 **
	 ** This operator allows for easy, 3d array-style, data access.
	 ** Note that data access with this operator is unchecked and
	 ** out_of_range lookups are not defined.
	 */
	pointer* operator[](const size_type k);

	/*!
	 ** \brief Subscript access to the slice datas contained in the %GenericMultiComponent3d.
	 ** \param k The index of the slice for which data should be accessed.
	 ** \return Read-only (constant) pointer* to the slice data.
	 ** \pre k < slices()
	 **
	 ** This operator allows for easy, 3d array-style, data access.
	 ** Note that data access with this operator is unchecked and
	 ** out_of_range lookups are not defined.
	 */
	const_pointer const * operator[](const size_type k) const;
	//  const_pointer* operator[](const size_type i) const ;
	/*!
	 ** \brief Subscript access to the data contained in the %GenericMultiComponent3d.
	 ** \param k The index of the slice for which the data should be accessed.
	 ** \param i The index of the row for which the data should be accessed.
	 ** \param j The index of the column for which the data should be accessed.
	 ** \return Read/Write reference to data.
	 ** \pre k < slices()
	 ** \pre i < rows()
	 ** \pre j < cols()
	 ** This operator allows for easy, 3d array-style, data access.
	 ** Note that data access with this operator is unchecked and
	 ** out_of_range lookups are not defined.
	 */
	reference operator()(const size_type k,
			const size_type i,
			const size_type j);

	/*!
	 ** \brief Subscript access to the data contained in the %GenericMultiComponent3d.
	 ** \param k The index of the slice for which the data should be accessed.
	 ** \param i The index of the row for which the data should be accessed.
	 ** \param j The index of the column for which the data should be accessed.
	 ** \return Read_only (constant) reference to data.
	 ** \pre k < slices()
	 ** \pre i < rows()
	 ** \pre j < cols()
	 ** This operator allows for easy, 3d array-style, data access.
	 ** Note that data access with this operator is unchecked and
	 ** out_of_range lookups are not defined.
	 */
	const_reference operator()(const size_type k,
			const size_type i,
			const size_type j) const;

	/*@} End Element access operators */

	/*!
	 ** \brief Returns the number of slices (first dimension size)
	 **        in the %GenericMultiComponent3d
	 */
	size_type dim1() const;

	/*!
	 ** \brief Returns the number of slices (first dimension size)
	 **        in the %GenericMultiComponent3d
	 */
	size_type slices() const;


	/*!
	 ** \brief Returns the number of rows (second dimension size)
	 **        in the %GenericMultiComponent3d
	 */
	size_type dim2() const;


	/*!
	 ** \brief Returns the number of rows (first dimension size)
	 **        in the %GenericMultiComponent3d
	 */
	size_type rows() const;


	/*!
	 ** \brief Returns the number of columns (third dimension size)
	 **        in the %GenericMultiComponent3d
	 */
	size_type dim3() const;

	/*!
	 ** \brief Returns the number of columns (third dimension size)
	 **        in the %GenericMultiComponent3d
	 */
	size_type columns() const;

	/*!
	 ** \brief Returns the number of columns (third dimension size)
	 **        in the %GenericMultiComponent3d
	 */
	size_type cols() const;


	/*!
	 ** \brief Returns the number of elements in the %GenericMultiComponent3d
	 */
	size_type size() const;

	/*!
	 ** \brief Returns the maximal size (number of elements) in the %GenericMultiComponent3d
	 */
	size_type max_size() const;

	/*!
	 ** \brief Returns the number of elements in a slice of the %GenericMultiComponent3d
	 */
	size_type slice_size() const;


	/*!
	 ** \brief Returns true if the %GenericMultiComponent3d is empty.  (Thus size() == 0)
	 */
	bool empty()const;

	/*!
	 ** \brief Swaps data with another %GenericMultiComponent3d.
	 ** \param M A %GenericMultiComponent3d of the same element type
	 */
	void swap(self& M);


	/*!
	 ** \brief Returns the min elements of the %GenericMultiComponent3d
	 ** according to the operator <
	 ** \pre size() != 0
	 */
	Block min() const;


	/*!
	 ** \brief Returns the max elements of the GenericMultiComponent3d
	 ** according to the operator <
	 ** \pre size() != 0
	 */
	Block max() const;

	//  /*!
	//   ** \brief Returns the sums of the elements of the %GenericMultiComponent3d
	//   ** \pre size() != 0
	//   */
	//   Block sum() const;

	/*!
	 ** \brief Applys the one-parameter C-function \a fun
	 **        to each element of the %GenericMultiComponent3d
	 ** \param fun The one-parameter C function
	 ** \return the resulting %GenericMultiComponent3d
	 */
	GenericMultiComponent3d<Block>& apply(Block(*fun)(Block));

	/*!
	 ** \brief Applys the one-parameter C-function \a fun
	 **        to each element of the %GenericMultiComponent3d
	 ** \param fun The one-const-parameter C function
	 ** \return the resulting %GenericMultiComponent3d
	 */
	GenericMultiComponent3d<Block>& apply(Block(*fun)(const Block&));


private:
	slip::Matrix3d<Block>* matrix_;

 private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & this->matrix_;
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & this->matrix_;
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};
/** @} */ // end of MultiComponent3dContainer group
}//slip::


namespace slip
{
template<typename Block>
inline
GenericMultiComponent3d<Block>::GenericMultiComponent3d():
matrix_(new slip::Matrix3d<Block>())
{}

template<typename Block>
inline
GenericMultiComponent3d<Block>::GenericMultiComponent3d(const typename GenericMultiComponent3d<Block>::size_type slices,
		const typename GenericMultiComponent3d<Block>::size_type rows,
		const typename GenericMultiComponent3d<Block>::size_type cols):
		matrix_(new slip::Matrix3d<Block>(slices,rows,cols))
		{}

template<typename Block>
inline
GenericMultiComponent3d<Block>::GenericMultiComponent3d(const typename GenericMultiComponent3d<Block>::size_type slices,
		const typename GenericMultiComponent3d<Block>::size_type rows,
		const typename GenericMultiComponent3d<Block>::size_type cols,
		typename Block::const_pointer val):
		matrix_(new slip::Matrix3d<Block>(slices,rows,cols))
		{
	typename slip::Matrix3d<Block>::iterator it = matrix_->begin();
	typename slip::Matrix3d<Block>::iterator ite = matrix_->end();

	for(;it!=ite; it++)
	{
		for(typename GenericMultiComponent3d<Block>::size_type component = 0; component < Block::SIZE; ++component)
		{
			(*it)[component] = *val++;
		}
	}
		}

template<typename Block>
inline
GenericMultiComponent3d<Block>::GenericMultiComponent3d(const typename GenericMultiComponent3d<Block>::size_type slices,
		const typename GenericMultiComponent3d<Block>::size_type rows,
		const typename GenericMultiComponent3d<Block>::size_type cols,
		const Block& val):
		matrix_(new slip::Matrix3d<Block>(slices,rows,cols,val))
		{}

template<typename Block>
inline
GenericMultiComponent3d<Block>::GenericMultiComponent3d(const typename GenericMultiComponent3d<Block>::size_type slices,
		const typename GenericMultiComponent3d<Block>::size_type rows,
		const typename GenericMultiComponent3d<Block>::size_type cols,
		const Block* val):
		matrix_(new slip::Matrix3d<Block>(slices,rows,cols,val))
		{}

template<typename Block>
inline
GenericMultiComponent3d<Block>::GenericMultiComponent3d(const GenericMultiComponent3d<Block>& rhs):
matrix_(new slip::Matrix3d<Block>((*rhs.matrix_)))
{}

template<typename Block>
inline
GenericMultiComponent3d<Block>::~GenericMultiComponent3d()
{
	delete matrix_;
}

template<typename Block>
inline
GenericMultiComponent3d<Block>& GenericMultiComponent3d<Block>::operator=(const GenericMultiComponent3d<Block> & rhs)
{
	if(this != &rhs)
	{
		*matrix_ = *(rhs.matrix_);
	}
	return *this;
}

template<typename Block>
inline
GenericMultiComponent3d<Block>& GenericMultiComponent3d<Block>::operator=(const Block& val)
{
	std::fill_n(matrix_->begin(),matrix_->size(),val);
	return *this;
}

template<typename Block>
inline
GenericMultiComponent3d<Block>& GenericMultiComponent3d<Block>::operator=(const typename Block::value_type& val)
{
	std::fill_n(matrix_->begin(),matrix_->size(),val);
	return *this;
}

template<typename Block>
inline
void GenericMultiComponent3d<Block>::resize(const typename GenericMultiComponent3d<Block>::size_type slices,
		const typename GenericMultiComponent3d<Block>::size_type rows,
		const typename GenericMultiComponent3d<Block>::size_type cols,
		const Block& val)
		{
	matrix_->resize(slices,rows,cols,val);
		}

//////////////////////////////////////////
//   Iterators
//////////////////////////////////////////

//****************************************************************************
//                          One dimensional iterators
//****************************************************************************



//----------------------Global iterators------------------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator
GenericMultiComponent3d<Block>::begin()
{
	return matrix_->begin();
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator
GenericMultiComponent3d<Block>::end()
{
	return matrix_->end();
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator
GenericMultiComponent3d<Block>::begin() const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->begin();
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator
GenericMultiComponent3d<Block>::end() const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->end();
}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator
GenericMultiComponent3d<Block>::rbegin()
{
	return typename GenericMultiComponent3d<Block>::reverse_iterator(this->end());
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator
GenericMultiComponent3d<Block>::rend()
{
	return typename GenericMultiComponent3d<Block>::reverse_iterator(this->begin());
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator
GenericMultiComponent3d<Block>::rbegin() const
{
	return typename GenericMultiComponent3d<Block>::const_reverse_iterator(this->end());
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator
GenericMultiComponent3d<Block>::rend() const
{
	return typename GenericMultiComponent3d<Block>::const_reverse_iterator(this->begin());
}

//--------------------slice iterators----------------------

template<typename T>
inline
typename GenericMultiComponent3d<T>::slice_iterator
GenericMultiComponent3d<T>::slice_begin(const GenericMultiComponent3d<T>::size_type row,
		const GenericMultiComponent3d<T>::size_type col)
		{
	return matrix_->slice_begin(row,col);
		}

template<typename T>
inline
typename GenericMultiComponent3d<T>::const_slice_iterator
GenericMultiComponent3d<T>::slice_begin(const GenericMultiComponent3d<T>::size_type row,
		const GenericMultiComponent3d<T>::size_type col) const
		{
	Matrix3d<T> const * tp(matrix_);
	return tp->slice_begin(row,col);
		}

template<typename T>
inline
typename GenericMultiComponent3d<T>::slice_iterator
GenericMultiComponent3d<T>::slice_end(const GenericMultiComponent3d<T>::size_type row,
		const GenericMultiComponent3d<T>::size_type col)
		{
	return matrix_->slice_end(row,col);
		}

template<typename T>
inline
typename GenericMultiComponent3d<T>::const_slice_iterator
GenericMultiComponent3d<T>::slice_end(const GenericMultiComponent3d<T>::size_type row,
		const GenericMultiComponent3d<T>::size_type col) const
		{
	Matrix3d<T> const * tp(matrix_);
	return tp->slice_end(row,col);
		}


template<typename T>
inline
typename GenericMultiComponent3d<T>::reverse_slice_iterator
GenericMultiComponent3d<T>::slice_rbegin(const GenericMultiComponent3d<T>::size_type row,
		const GenericMultiComponent3d<T>::size_type col)
		{
	return matrix_->slice_rbegin(row,col);
		}

template<typename T>
inline
typename GenericMultiComponent3d<T>::const_reverse_slice_iterator
GenericMultiComponent3d<T>::slice_rbegin(const GenericMultiComponent3d<T>::size_type row,
		const GenericMultiComponent3d<T>::size_type col) const
		{
	Matrix3d<T> const * tp(matrix_);
	return tp->slice_rbegin(row,col);
		}

template<typename T>
inline
typename GenericMultiComponent3d<T>::reverse_slice_iterator
GenericMultiComponent3d<T>::slice_rend(const GenericMultiComponent3d<T>::size_type row,
		const GenericMultiComponent3d<T>::size_type col)
		{
	return matrix_->slice_rend(row,col);
		}


template<typename T>
inline
typename GenericMultiComponent3d<T>::const_reverse_slice_iterator
GenericMultiComponent3d<T>::slice_rend(const GenericMultiComponent3d<T>::size_type row,
		const GenericMultiComponent3d<T>::size_type col) const
		{
	Matrix3d<T> const * tp(matrix_);
	return tp->slice_rend(row,col);
		}

//--------------------row iterators----------------------


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::row_iterator
GenericMultiComponent3d<Block>::row_begin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row)
		{
	return matrix_->row_begin(slice,row);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_row_iterator
GenericMultiComponent3d<Block>::row_begin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->row_begin(slice,row);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::row_iterator
GenericMultiComponent3d<Block>::row_end(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row)
		{
	return matrix_->row_end(slice,row);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_row_iterator
GenericMultiComponent3d<Block>::row_end(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->row_end(slice,row);
		}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_row_iterator
GenericMultiComponent3d<Block>::row_rbegin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row)
		{
	return matrix_->row_rbegin(slice,row);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_row_iterator
GenericMultiComponent3d<Block>::row_rbegin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->row_rbegin(slice,row);
		}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_row_iterator
GenericMultiComponent3d<Block>::row_rend(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row)
		{
	return matrix_->row_rend(slice,row);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_row_iterator
GenericMultiComponent3d<Block>::row_rend(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->row_rend(slice,row);
		}

//-------------------- col iterators----------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::col_iterator
GenericMultiComponent3d<Block>::col_begin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col)
		{
	return matrix_->col_begin(slice,col);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_col_iterator
GenericMultiComponent3d<Block>::col_begin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->col_begin(slice,col);
		}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::col_iterator
GenericMultiComponent3d<Block>::col_end(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col)
		{
	return matrix_->col_end(slice,col);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_col_iterator
GenericMultiComponent3d<Block>::col_end(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->col_end(slice,col);
		}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_col_iterator
GenericMultiComponent3d<Block>::col_rbegin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col)
		{
	return matrix_->col_rbegin(slice,col);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_col_iterator
GenericMultiComponent3d<Block>::col_rbegin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->col_rbegin(slice,col);
		}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_col_iterator
GenericMultiComponent3d<Block>::col_rend(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col)
		{
	return matrix_->col_rend(slice,col);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_col_iterator
GenericMultiComponent3d<Block>::col_rend(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->col_rend(slice,col);
		}

//--------------------Constant slice range iterators----------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::slice_range_iterator
GenericMultiComponent3d<Block>::slice_begin(const typename GenericMultiComponent3d<Block>::size_type row,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return matrix_->slice_begin(row,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_slice_range_iterator
GenericMultiComponent3d<Block>::slice_begin(const typename GenericMultiComponent3d<Block>::size_type row,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->slice_begin(row,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::slice_range_iterator
GenericMultiComponent3d<Block>::slice_end(const typename GenericMultiComponent3d<Block>::size_type row,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return matrix_->slice_end(row,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_slice_range_iterator
GenericMultiComponent3d<Block>::slice_end(const typename GenericMultiComponent3d<Block>::size_type row,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->slice_end(row,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_slice_range_iterator
GenericMultiComponent3d<Block>::slice_rbegin(const typename GenericMultiComponent3d<Block>::size_type row,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return matrix_->slice_rbegin(row,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_slice_range_iterator
GenericMultiComponent3d<Block>::slice_rbegin(const typename GenericMultiComponent3d<Block>::size_type row,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->slice_rbegin(row,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_slice_range_iterator
GenericMultiComponent3d<Block>::slice_rend(const typename GenericMultiComponent3d<Block>::size_type row,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return matrix_->slice_rend(row,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_slice_range_iterator
GenericMultiComponent3d<Block>::slice_rend(const typename GenericMultiComponent3d<Block>::size_type row,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->slice_rend(row,col,range);
		}


//--------------------row range iterators----------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::row_range_iterator
GenericMultiComponent3d<Block>::row_begin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range)
		{
	return matrix_->row_begin(slice,row,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_row_range_iterator
GenericMultiComponent3d<Block>::row_begin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->row_begin(slice,row,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::row_range_iterator
GenericMultiComponent3d<Block>::row_end(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range)
		{
	return matrix_->row_end(slice,row,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_row_range_iterator
GenericMultiComponent3d<Block>::row_end(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->row_end(slice,row,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_row_range_iterator
GenericMultiComponent3d<Block>::row_rbegin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range)
		{
	return matrix_->row_rbegin(slice,row,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_row_range_iterator
GenericMultiComponent3d<Block>::row_rbegin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->row_rbegin(slice,row,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_row_range_iterator
GenericMultiComponent3d<Block>::row_rend(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range)
		{
	return matrix_->row_rend(slice,row,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_row_range_iterator
GenericMultiComponent3d<Block>::row_rend(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->row_rend(slice,row,range);
		}

//--------------------col range iterators----------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::col_range_iterator
GenericMultiComponent3d<Block>::col_begin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return matrix_->col_begin(slice,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_col_range_iterator
GenericMultiComponent3d<Block>::col_begin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->col_begin(slice,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::col_range_iterator
GenericMultiComponent3d<Block>::col_end(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return matrix_->col_end(slice,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_col_range_iterator
GenericMultiComponent3d<Block>::col_end(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->col_end(slice,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_col_range_iterator
GenericMultiComponent3d<Block>::col_rbegin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return matrix_->col_rbegin(slice,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_col_range_iterator
GenericMultiComponent3d<Block>::col_rbegin(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->col_rbegin(slice,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_col_range_iterator
GenericMultiComponent3d<Block>::col_rend(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return matrix_->col_rend(slice,col,range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_col_range_iterator
GenericMultiComponent3d<Block>::col_rend(const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->col_rend(slice,col,range);
		}

//-------------------plane global iterators----------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator
GenericMultiComponent3d<Block>::plane_begin(const typename GenericMultiComponent3d<Block>::size_type slice)
{
	return matrix_->plane_begin(slice);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator
GenericMultiComponent3d<Block>::plane_begin(const typename GenericMultiComponent3d<Block>::size_type slice) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_begin(slice);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator
GenericMultiComponent3d<Block>::plane_end(const typename GenericMultiComponent3d<Block>::size_type slice)
{
	return matrix_->plane_end(slice);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator
GenericMultiComponent3d<Block>::plane_end(const typename GenericMultiComponent3d<Block>::size_type slice) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_end(slice);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator
GenericMultiComponent3d<Block>::plane_rbegin(const typename GenericMultiComponent3d<Block>::size_type slice)
{
	return matrix_->plane_rbegin(slice);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator
GenericMultiComponent3d<Block>::plane_rbegin(const typename GenericMultiComponent3d<Block>::size_type slice) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_rbegin(slice);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator
GenericMultiComponent3d<Block>::plane_rend(const typename GenericMultiComponent3d<Block>::size_type slice)
{
	return matrix_->plane_rend(slice);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator
GenericMultiComponent3d<Block>::plane_rend(const typename GenericMultiComponent3d<Block>::size_type slice) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_rend(slice);
}

#ifdef ALL_PLANE_ITERATOR3D

//-------------------plane row and col iterators----------


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator1d
GenericMultiComponent3d<Block>::plane_row_begin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type row)
		{
	return matrix_->plane_row_begin(P,plane_coordinate,row);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator1d
GenericMultiComponent3d<Block>::plane_row_begin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type row) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_row_begin(P,plane_coordinate,row);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator1d
GenericMultiComponent3d<Block>::plane_row_end(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type row)
		{
	return matrix_->plane_row_end(P,plane_coordinate,row);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator1d
GenericMultiComponent3d<Block>::plane_row_end(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type row) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_row_end(P,plane_coordinate,row);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator1d
GenericMultiComponent3d<Block>::plane_row_rbegin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type row)
		{
	return matrix_->plane_row_rbegin(P,plane_coordinate,row);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator1d
GenericMultiComponent3d<Block>::plane_row_rbegin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type row) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_row_rbegin(P,plane_coordinate,row);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator1d
GenericMultiComponent3d<Block>::plane_row_rend(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type row)
		{
	return matrix_->plane_row_rend(P,plane_coordinate,row);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator1d
GenericMultiComponent3d<Block>::plane_row_rend(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type row) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_row_rend(P,plane_coordinate,row);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator1d
GenericMultiComponent3d<Block>::plane_col_begin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type col)
		{
	return matrix_->plane_col_begin(P,plane_coordinate,col);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator1d
GenericMultiComponent3d<Block>::plane_col_begin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type col) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_col_begin(P,plane_coordinate,col);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator1d
GenericMultiComponent3d<Block>::plane_col_end(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type col)
		{
	return matrix_->plane_col_end(P,plane_coordinate,col);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator1d
GenericMultiComponent3d<Block>::plane_col_end(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type col) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_col_end(P,plane_coordinate,col);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator1d
GenericMultiComponent3d<Block>::plane_col_rbegin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type col)
		{
	return matrix_->plane_col_rbegin(P,plane_coordinate,col);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator1d
GenericMultiComponent3d<Block>::plane_col_rbegin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type col) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_col_rbegin(P,plane_coordinate,col);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator1d
GenericMultiComponent3d<Block>::plane_col_rend(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type col)
		{
	return matrix_->plane_col_rend(P,plane_coordinate,col);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator1d
GenericMultiComponent3d<Block>::plane_col_rend(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const typename GenericMultiComponent3d<Block>::size_type col) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_col_rend(P,plane_coordinate,col);
		}

//-------------------plane box iterators----------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator1d
GenericMultiComponent3d<Block>::plane_row_begin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type row, const Box2d<int> & b) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_row_begin(P,plane_coordinate,row,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator1d
GenericMultiComponent3d<Block>::plane_row_begin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type row, const Box2d<int> & b)
		{
	return matrix_->plane_row_begin(P,plane_coordinate,row,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator1d
GenericMultiComponent3d<Block>:: plane_row_end(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type row, const Box2d<int> & b)
		{
	return matrix_->plane_row_end(P,plane_coordinate,row,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator1d
GenericMultiComponent3d<Block>::plane_row_end(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type row, const Box2d<int> & b) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_row_end(P,plane_coordinate,row,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator1d
GenericMultiComponent3d<Block>::plane_row_rbegin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type row, const Box2d<int> & b)
		{
	return matrix_->plane_row_rbegin(P,plane_coordinate,row,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator1d
GenericMultiComponent3d<Block>::plane_row_rbegin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type row, const Box2d<int> & b) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_row_rbegin(P,plane_coordinate,row,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator1d
GenericMultiComponent3d<Block>::plane_row_rend(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type row, const Box2d<int> & b)
		{
	return matrix_->plane_row_rend(P,plane_coordinate,row,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator1d
GenericMultiComponent3d<Block>::plane_row_rend(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type row, const Box2d<int> & b) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_row_rend(P,plane_coordinate,row,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator1d
GenericMultiComponent3d<Block>::plane_col_begin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type col, const Box2d<int> & b)
		{
	return matrix_->plane_col_begin(P,plane_coordinate,col,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator1d
GenericMultiComponent3d<Block>::plane_col_begin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type col, const Box2d<int> & b) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_col_begin(P,plane_coordinate,col,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator1d
GenericMultiComponent3d<Block>::plane_col_end(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type col, const Box2d<int> & b)
		{
	return matrix_->plane_col_end(P,plane_coordinate,col,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator1d
GenericMultiComponent3d<Block>::plane_col_end(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type col, const Box2d<int> & b) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_col_end(P,plane_coordinate,col,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator1d
GenericMultiComponent3d<Block>::plane_col_rbegin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type col, const Box2d<int> & b)
		{
	return matrix_->plane_col_rbegin(P,plane_coordinate,col,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator1d
GenericMultiComponent3d<Block>::plane_col_rbegin(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type col, const Box2d<int> & b) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_col_rbegin(P,plane_coordinate,col,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator1d
GenericMultiComponent3d<Block>::plane_col_rend(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type col, const Box2d<int> & b)
		{
	return matrix_->plane_col_rend(P,plane_coordinate,col,b);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator1d
GenericMultiComponent3d<Block>::plane_col_rend(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const size_type col, const Box2d<int> & b) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_col_rend(P,plane_coordinate,col,b);
		}

#endif //ALL_PLANE_ITERATOR3D

//****************************************************************************
//                          Two dimensional plane iterators
//****************************************************************************

//------------------------ Global iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator2d
GenericMultiComponent3d<Block>::plane_upper_left(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate)
{
	return matrix_->plane_upper_left(P,plane_coordinate);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator2d
GenericMultiComponent3d<Block>::plane_bottom_right(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate)
{
	return matrix_->plane_bottom_right(P,plane_coordinate);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator2d
GenericMultiComponent3d<Block>::plane_upper_left(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_upper_left(P,plane_coordinate);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator2d
GenericMultiComponent3d<Block>::plane_bottom_right(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_bottom_right(P,plane_coordinate);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator2d
GenericMultiComponent3d<Block>::plane_rupper_left(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate)
{
	return matrix_->plane_rupper_left(P,plane_coordinate);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator2d
GenericMultiComponent3d<Block>::plane_rbottom_right(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate)
{
	return matrix_->plane_rbottom_right(P,plane_coordinate);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator2d
GenericMultiComponent3d<Block>::plane_rupper_left(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_rupper_left(P,plane_coordinate);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator2d
GenericMultiComponent3d<Block>::plane_rbottom_right(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_rbottom_right(P,plane_coordinate);
}

//------------------------ Box iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator2d
GenericMultiComponent3d<Block>::plane_upper_left(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate, const Box2d<int>& b)
{
	return matrix_->plane_upper_left(P,plane_coordinate,b);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator2d
GenericMultiComponent3d<Block>::plane_bottom_right(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate, const Box2d<int>& b)
{
	return matrix_->plane_bottom_right(P,plane_coordinate,b);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator2d
GenericMultiComponent3d<Block>::plane_upper_left(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate, const Box2d<int>& b) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_upper_left(P,plane_coordinate,b);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator2d
GenericMultiComponent3d<Block>::plane_bottom_right(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate, const Box2d<int>& b) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_bottom_right(P,plane_coordinate,b);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator2d
GenericMultiComponent3d<Block>::plane_rupper_left(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate, const Box2d<int>& b)
{
	return matrix_->plane_rupper_left(P,plane_coordinate,b);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator2d
GenericMultiComponent3d<Block>::plane_rbottom_right(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate, const Box2d<int>& b)
{
	return matrix_->plane_rbottom_right(P,plane_coordinate,b);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator2d
GenericMultiComponent3d<Block>::plane_rupper_left(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate, const Box2d<int>& b) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_rupper_left(P,plane_coordinate,b);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator2d
GenericMultiComponent3d<Block>::plane_rbottom_right(PLANE_ORIENTATION P, const typename GenericMultiComponent3d<Block>::size_type plane_coordinate,
		const Box2d<int>& b) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->plane_rbottom_right(P,plane_coordinate,b);
		}

//****************************************************************************
//                          Three dimensional iterators
//****************************************************************************

//------------------------ Global iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator3d GenericMultiComponent3d<Block>::front_upper_left()
{
	return matrix_->front_upper_left();
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator3d GenericMultiComponent3d<Block>::front_upper_left() const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->front_upper_left();
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator3d GenericMultiComponent3d<Block>::back_bottom_right()
{
	return matrix_->back_bottom_right();
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator3d GenericMultiComponent3d<Block>::back_bottom_right() const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->back_bottom_right();
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator3d
GenericMultiComponent3d<Block>::rback_bottom_right()
{
	return matrix_->rback_bottom_right();
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator3d
GenericMultiComponent3d<Block>::rback_bottom_right() const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->rback_bottom_right();
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator3d
GenericMultiComponent3d<Block>::rfront_upper_left()
{
	return matrix_->rfront_upper_left();
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator3d
GenericMultiComponent3d<Block>::rfront_upper_left() const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->rfront_upper_left();
}

//------------------------ Box iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator3d GenericMultiComponent3d<Block>::front_upper_left(const Box3d<int>& box)
{
	return matrix_->front_upper_left(box);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator3d GenericMultiComponent3d<Block>::front_upper_left(const Box3d<int>& box) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->front_upper_left(box);
}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator3d
GenericMultiComponent3d<Block>::back_bottom_right(const Box3d<int>& box)
{
	return matrix_->back_bottom_right(box);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator3d
GenericMultiComponent3d<Block>::back_bottom_right(const Box3d<int>& box) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->back_bottom_right(box);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator3d
GenericMultiComponent3d<Block>::rback_bottom_right(const Box3d<int>& box)
{
	return matrix_->rback_bottom_right(box);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator3d
GenericMultiComponent3d<Block>::rback_bottom_right(const Box3d<int>& box) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->rback_bottom_right(box);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator3d
GenericMultiComponent3d<Block>::rfront_upper_left(const Box3d<int>& box)
{
	return matrix_->rfront_upper_left(box);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator3d
GenericMultiComponent3d<Block>::rfront_upper_left(const Box3d<int>& box) const
{
	Matrix3d<Block> const * tp(matrix_);
	return tp->rfront_upper_left(box);
}

//------------------------ Range iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator3d_range
GenericMultiComponent3d<Block>::front_upper_left(const Range<int>& slice_range,
		const Range<int>& row_range,
		const Range<int>& col_range)
		{
	return matrix_->front_upper_left(slice_range,row_range,col_range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::iterator3d_range
GenericMultiComponent3d<Block>::back_bottom_right(const Range<int>& slice_range,
		const Range<int>& row_range,
		const Range<int>& col_range)
		{
	return matrix_->back_bottom_right(slice_range,row_range,col_range);
		}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator3d_range
GenericMultiComponent3d<Block>::front_upper_left(const Range<int>& slice_range,
		const Range<int>& row_range,
		const Range<int>& col_range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->front_upper_left(slice_range,row_range,col_range);
		}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_iterator3d_range
GenericMultiComponent3d<Block>::back_bottom_right(const Range<int>& slice_range,
		const Range<int>& row_range,
		const Range<int>& col_range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->back_bottom_right(slice_range,row_range,col_range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator3d_range
GenericMultiComponent3d<Block>::rfront_upper_left(const Range<int>& slice_range,
		const Range<int>& row_range,
		const Range<int>& col_range)
		{
	return matrix_->rfront_upper_left(slice_range,row_range,col_range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator3d_range
GenericMultiComponent3d<Block>::rfront_upper_left(const Range<int>& slice_range,
		const Range<int>& row_range,
		const Range<int>& col_range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->rfront_upper_left(slice_range,row_range,col_range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_iterator3d_range
GenericMultiComponent3d<Block>::rback_bottom_right(const Range<int>& slice_range,
		const Range<int>& row_range,
		const Range<int>& col_range)
		{
	return matrix_->rback_bottom_right(slice_range,row_range,col_range);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_iterator3d_range
GenericMultiComponent3d<Block>::rback_bottom_right(const Range<int>& slice_range,
		const Range<int>& row_range,
		const Range<int>& col_range) const
		{
	Matrix3d<Block> const * tp(matrix_);
	return tp->rback_bottom_right(slice_range,row_range,col_range);
		}


//****************************************************************************
//                          Component iterator
//****************************************************************************

//------------------------ One dimensional global component iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_iterator
GenericMultiComponent3d<Block>::begin(const std::size_t component)
{
	return typename GenericMultiComponent3d<Block>::component_iterator((typename Block::pointer)begin() + component);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_iterator
GenericMultiComponent3d<Block>::end(const std::size_t component)
{
	return typename GenericMultiComponent3d<Block>::component_iterator((typename Block::pointer)end() + component);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_iterator
GenericMultiComponent3d<Block>::begin(const std::size_t component) const
{
	return typename GenericMultiComponent3d<Block>::const_component_iterator((typename Block::const_pointer)begin() + component);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_iterator
GenericMultiComponent3d<Block>::end(const std::size_t component) const
{
	return typename GenericMultiComponent3d<Block>::const_component_iterator((typename Block::const_pointer)end() + component);
}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_iterator
GenericMultiComponent3d<Block>::rbegin(const std::size_t component)
{
	return  typename GenericMultiComponent3d<Block>::reverse_component_iterator(this->end(component));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_iterator
GenericMultiComponent3d<Block>::rend(const std::size_t component)
{
	return  typename GenericMultiComponent3d<Block>::reverse_component_iterator(this->begin(component));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_iterator
GenericMultiComponent3d<Block>::rbegin(const std::size_t component) const
{
	return  typename GenericMultiComponent3d<Block>::const_reverse_component_iterator(this->end(component));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_iterator
GenericMultiComponent3d<Block>::rend(const std::size_t component) const
{
	return  typename GenericMultiComponent3d<Block>::const_reverse_component_iterator(this->begin(component));
}


//--------------------One dimensional component slice iterators----------------------


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_slice_iterator
GenericMultiComponent3d<Block>::slice_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row, const typename GenericMultiComponent3d<Block>::size_type col)
{
	return slip::stride_iterator<typename GenericMultiComponent3d<Block>::block_pointer>(&((*this)[0][row][col][component]), Block::SIZE * this->dim2() * this->dim3());
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_slice_iterator
GenericMultiComponent3d<Block>::slice_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row, const typename GenericMultiComponent3d<Block>::size_type col) const
{
	return slip::stride_iterator<typename GenericMultiComponent3d<Block>::const_block_pointer>(&((*this)[0][row][col][component]), Block::SIZE * this->dim2() * this->dim3());
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_slice_iterator
GenericMultiComponent3d<Block>::slice_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row, const typename GenericMultiComponent3d<Block>::size_type col)
{
	return ++(slip::stride_iterator<typename GenericMultiComponent3d<Block>::block_pointer>(&((*this)[this->dim1()-1][row][col][component]), Block::SIZE * this->dim2() * this->dim3()));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_slice_iterator
GenericMultiComponent3d<Block>::slice_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row, const typename GenericMultiComponent3d<Block>::size_type col) const
{
	return ++(slip::stride_iterator<typename GenericMultiComponent3d<Block>::const_block_pointer>(&((*this)[this->dim1()-1][row][col][component]), Block::SIZE * this->dim2() * this->dim3()));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_slice_iterator
GenericMultiComponent3d<Block>::slice_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row, const typename GenericMultiComponent3d<Block>::size_type col)
{
	return typename GenericMultiComponent3d<Block>::reverse_component_slice_iterator(this->slice_end(component,row,col));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_slice_iterator
GenericMultiComponent3d<Block>::slice_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row, const typename GenericMultiComponent3d<Block>::size_type col) const
{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_slice_iterator(this->slice_end(component,row,col));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_slice_iterator
GenericMultiComponent3d<Block>::slice_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row, const typename GenericMultiComponent3d<Block>::size_type col)
{
	return typename GenericMultiComponent3d<Block>::reverse_component_slice_iterator(this->slice_begin(component,row,col));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_slice_iterator
GenericMultiComponent3d<Block>::slice_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row, const typename GenericMultiComponent3d<Block>::size_type col) const
{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_slice_iterator(this->slice_begin(component,row,col));
}


//-------------------row component iterators----------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_row_iterator
GenericMultiComponent3d<Block>::row_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row)
		{
	return typename GenericMultiComponent3d<Block>::component_row_iterator((typename Block::pointer)this->row_begin(slice,row) + component);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_row_iterator
GenericMultiComponent3d<Block>::row_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row) const
		{
	return typename GenericMultiComponent3d<Block>::const_component_row_iterator((typename Block::const_pointer)this->row_begin(slice,row) + component);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_row_iterator
GenericMultiComponent3d<Block>::row_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row)
		{
	return typename GenericMultiComponent3d<Block>::component_row_iterator((typename Block::pointer)this->row_end(slice,row) + component);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_row_iterator
GenericMultiComponent3d<Block>::row_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row) const
		{
	return typename GenericMultiComponent3d<Block>::const_component_row_iterator((typename Block::const_pointer)this->row_end(slice,row) + component);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_row_iterator
GenericMultiComponent3d<Block>::row_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row)
		{
	return typename GenericMultiComponent3d<Block>::reverse_component_row_iterator(this->row_end(component,slice,row));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_row_iterator
GenericMultiComponent3d<Block>::row_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row) const
		{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_row_iterator(this->row_end(component,slice,row));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_row_iterator
GenericMultiComponent3d<Block>::row_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row)
		{
	return typename GenericMultiComponent3d<Block>::reverse_component_row_iterator(this->row_begin(component,slice,row));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_row_iterator
GenericMultiComponent3d<Block>::row_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type row) const
		{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_row_iterator(this->row_begin(component,slice,row));
		}

//-------------------Constant component col iterators----------


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_col_iterator
GenericMultiComponent3d<Block>::col_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col)
		{
	return slip::stride_iterator<typename GenericMultiComponent3d<Block>::block_pointer>(&((*this)[slice][0][col][component]), Block::SIZE * this->dim3());
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_col_iterator
GenericMultiComponent3d<Block>::col_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col) const
		{
	return slip::stride_iterator<typename GenericMultiComponent3d<Block>::const_block_pointer>(&((*this)[slice][0][col][component]), Block::SIZE * this->dim3());
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_col_iterator
GenericMultiComponent3d<Block>::col_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col)
		{
	return ++(slip::stride_iterator<typename GenericMultiComponent3d<Block>::block_pointer>(&((*this)[slice][this->dim2()-1][col][component]), Block::SIZE * this->dim3()));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_col_iterator
GenericMultiComponent3d<Block>::col_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col) const
		{
	return ++(slip::stride_iterator<typename GenericMultiComponent3d<Block>::const_block_pointer>(&((*this)[slice][this->dim2()-1][col][component]), Block::SIZE * this->dim3()));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_col_iterator
GenericMultiComponent3d<Block>::col_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col)
		{
	return typename GenericMultiComponent3d<Block>::reverse_component_col_iterator(this->col_end(component,slice,col));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_col_iterator
GenericMultiComponent3d<Block>::col_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col) const
		{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_col_iterator(this->col_end(component,slice,col));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_col_iterator
GenericMultiComponent3d<Block>::col_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col)
		{
	return typename GenericMultiComponent3d<Block>::reverse_component_col_iterator(this->col_begin(component,slice,col));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_col_iterator
GenericMultiComponent3d<Block>::col_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,
		const typename GenericMultiComponent3d<Block>::size_type col) const
		{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_col_iterator(this->col_begin(component,slice,col));
		}

//------------------------component slice range iterators -----------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_slice_range_iterator
GenericMultiComponent3d<Block>::slice_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return slip::stride_iterator<typename GenericMultiComponent3d<Block>::block_pointer>(&((*this)[range.start()][row][col][component]), Block::SIZE * this->dim2() * this->dim3() * range.stride());
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_slice_range_iterator
GenericMultiComponent3d<Block>::slice_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return ++(slip::stride_iterator<typename GenericMultiComponent3d<Block>::block_pointer>(&((*this)[range.start() + range.iterations() * range.stride()][row][col][component]), Block::SIZE * this->dim2() * this->dim3() * range.stride()));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_slice_range_iterator
GenericMultiComponent3d<Block>::slice_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	return slip::stride_iterator<typename GenericMultiComponent3d<Block>::const_block_pointer>(&((*this)[range.start()][row][col][component]), Block::SIZE * this->dim2() * this->dim3() * range.stride());
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_slice_range_iterator
GenericMultiComponent3d<Block>::slice_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	return ++(slip::stride_iterator<typename GenericMultiComponent3d<Block>::const_block_pointer>(&((*this)[range.start() + range.iterations() * range.stride()][row][col][component]), Block::SIZE * this->dim2() * this->dim3() * range.stride()));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_slice_range_iterator
GenericMultiComponent3d<Block>::slice_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return typename GenericMultiComponent3d<Block>::reverse_component_slice_range_iterator(this->slice_end(component,row,col,range));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_slice_range_iterator
GenericMultiComponent3d<Block>::slice_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return typename GenericMultiComponent3d<Block>::reverse_component_slice_range_iterator(this->slice_begin(component,row,col,range));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_slice_range_iterator
GenericMultiComponent3d<Block>::slice_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_slice_range_iterator(this->slice_end(component,row,col,range));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_slice_range_iterator
GenericMultiComponent3d<Block>::slice_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type row,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_slice_range_iterator(this->slice_begin(component,row,col,range));
		}

//------------------------component row range iterators -----------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_row_range_iterator
GenericMultiComponent3d<Block>::row_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range)
		{
	return slip::stride_iterator<typename GenericMultiComponent3d<Block>::block_pointer>(&((*this)[slice][row][range.start()][component]), Block::SIZE * range.stride());
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_row_range_iterator
GenericMultiComponent3d<Block>::row_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range)
		{
	return ++(slip::stride_iterator<typename GenericMultiComponent3d<Block>::block_pointer>(&((*this)[slice][row][range.start()+ range.iterations() * range.stride()][component]), Block::SIZE * range.stride()));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_row_range_iterator
GenericMultiComponent3d<Block>::row_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range) const
		{
	return slip::stride_iterator<typename GenericMultiComponent3d<Block>::const_block_pointer>(&((*this)[slice][row][range.start()][component]), Block::SIZE * range.stride());
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_row_range_iterator
GenericMultiComponent3d<Block>::row_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range) const
		{
	return ++(slip::stride_iterator<typename GenericMultiComponent3d<Block>::const_block_pointer>(&((*this)[slice][row][range.start()+ range.iterations() * range.stride()][component]), Block::SIZE * range.stride()));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_row_range_iterator
GenericMultiComponent3d<Block>::row_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range)
		{
	return typename GenericMultiComponent3d<Block>::reverse_component_row_range_iterator(this->row_end(component,slice,row,range));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_row_range_iterator
GenericMultiComponent3d<Block>::row_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range)
		{
	return typename GenericMultiComponent3d<Block>::reverse_component_row_range_iterator(this->row_begin(component,slice,row,range));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_row_range_iterator
GenericMultiComponent3d<Block>::row_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range) const
		{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_row_range_iterator(this->row_end(component,slice,row,range));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_row_range_iterator
GenericMultiComponent3d<Block>::row_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type row,
		const slip::Range<int>& range) const
		{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_row_range_iterator(this->row_begin(component,slice,row,range));
		}

//------------------------component col range iterators -----------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_col_range_iterator
GenericMultiComponent3d<Block>::col_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return slip::stride_iterator<typename GenericMultiComponent3d<Block>::block_pointer>(&((*this)[slice][range.start()][col][component]), Block::SIZE * this->dim3() * range.stride());
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_col_range_iterator
GenericMultiComponent3d<Block>::col_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return ++(slip::stride_iterator<typename GenericMultiComponent3d<Block>::block_pointer>(&((*this)[slice][range.start() + range.iterations() * range.stride()][col][component]), Block::SIZE * this->dim3() * range.stride()));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_col_range_iterator
GenericMultiComponent3d<Block>::col_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	return slip::stride_iterator<typename GenericMultiComponent3d<Block>::const_block_pointer>(&((*this)[slice][range.start()][col][component]), Block::SIZE * this->dim3() * range.stride());
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_col_range_iterator
GenericMultiComponent3d<Block>::col_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	return ++(slip::stride_iterator<typename GenericMultiComponent3d<Block>::const_block_pointer>(&((*this)[slice][range.start() + range.iterations() * range.stride()][col][component]), Block::SIZE * this->dim3() * range.stride()));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_col_range_iterator
GenericMultiComponent3d<Block>::col_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return typename GenericMultiComponent3d<Block>::reverse_component_col_range_iterator(this->col_end(component,slice,col,range));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_col_range_iterator
GenericMultiComponent3d<Block>::col_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range)
		{
	return typename GenericMultiComponent3d<Block>::reverse_component_col_range_iterator(this->col_begin(component,slice,col,range));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_col_range_iterator
GenericMultiComponent3d<Block>::col_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_col_range_iterator(this->col_end(component,slice,col,range));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_col_range_iterator
GenericMultiComponent3d<Block>::col_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice,const typename GenericMultiComponent3d<Block>::size_type col,
		const slip::Range<int>& range) const
		{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_col_range_iterator(this->col_begin(component,slice,col,range));
		}

//------------------------component global plane iterators -----------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_iterator
GenericMultiComponent3d<Block>::plane_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice)
{
	return typename GenericMultiComponent3d<Block>::component_iterator((typename Block::pointer)this->plane_begin(slice) + component);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_iterator
GenericMultiComponent3d<Block>::plane_begin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice) const
{
	return typename GenericMultiComponent3d<Block>::const_component_iterator((typename Block::const_pointer)this->plane_begin(slice) + component);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_iterator
GenericMultiComponent3d<Block>::plane_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice)
{
	return typename GenericMultiComponent3d<Block>::component_iterator((typename Block::pointer)this->plane_end(slice) + component);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_iterator
GenericMultiComponent3d<Block>::plane_end(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice) const
{
	return typename GenericMultiComponent3d<Block>::const_component_iterator((typename Block::const_pointer)this->plane_end(slice) + component);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_iterator
GenericMultiComponent3d<Block>::plane_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice)
{
	return  typename GenericMultiComponent3d<Block>::reverse_component_iterator(this->plane_end(component,slice));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_iterator
GenericMultiComponent3d<Block>::plane_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice)
{
	return  typename GenericMultiComponent3d<Block>::reverse_component_iterator(this->plane_begin(component,slice));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_iterator
GenericMultiComponent3d<Block>::plane_rbegin(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice) const
{
	return  typename GenericMultiComponent3d<Block>::const_reverse_component_iterator(this->plane_end(component,slice));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_iterator
GenericMultiComponent3d<Block>::plane_rend(const std::size_t component, const typename GenericMultiComponent3d<Block>::size_type slice) const
{
	return  typename GenericMultiComponent3d<Block>::const_reverse_component_iterator(this->plane_begin(component,slice));
}

//****************************************************************************
//                          Three dimensional component iterators
//****************************************************************************

//------------------------ Global component iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_iterator3d
GenericMultiComponent3d<Block>::front_upper_left(const std::size_t component){
	return typename GenericMultiComponent3d<Block>::component_iterator3d
			(this,component,Box3d<int>(0,0,0,this->dim1()-1,this->dim2()-1,this->dim3()-1));
}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_iterator3d
GenericMultiComponent3d<Block>::back_bottom_right(const std::size_t component){
	DPoint3d<int> dp(this->dim1(),this->dim2(),this->dim3());
	typename GenericMultiComponent3d<Block>::component_iterator3d it =
			(*this).front_upper_left(component) + dp;
	return it;
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_iterator3d
GenericMultiComponent3d<Block>::front_upper_left(const std::size_t component) const{
	return typename GenericMultiComponent3d<Block>::const_component_iterator3d
			(this,component,Box3d<int>(0,0,0,this->dim1()-1,this->dim2()-1,this->dim3()-1));
}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_iterator3d
GenericMultiComponent3d<Block>::back_bottom_right(const std::size_t component) const{
	DPoint3d<int> dp(this->dim1(),this->dim2(),this->dim3());
	typename GenericMultiComponent3d<Block>::const_component_iterator3d it =
			(*this).front_upper_left(component) + dp;
	return it;
}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_iterator3d
GenericMultiComponent3d<Block>::rfront_upper_left(const std::size_t component){
	DPoint3d<int> dp(1,1,0);
	return typename GenericMultiComponent3d<Block>::reverse_component_iterator3d
			(this->back_bottom_right(component) - dp);
}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_iterator3d
GenericMultiComponent3d<Block>::rback_bottom_right(const std::size_t component){
	return typename GenericMultiComponent3d<Block>::reverse_component_iterator3d
			(this->front_upper_left(component));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_iterator3d
GenericMultiComponent3d<Block>::rfront_upper_left(const std::size_t component) const{
	DPoint3d<int> dp(1,1,0);
	return typename GenericMultiComponent3d<Block>::const_reverse_component_iterator3d
			(this->back_bottom_right(component) - dp);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_iterator3d
GenericMultiComponent3d<Block>::rback_bottom_right(const std::size_t component) const{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_iterator3d
			(this->front_upper_left(component));
}

//------------------------ Box component iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_iterator3d
GenericMultiComponent3d<Block>::front_upper_left(const std::size_t component,
		const Box3d<int>& box){
	return typename GenericMultiComponent3d<Block>::component_iterator3d(this,component,box);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_iterator3d
GenericMultiComponent3d<Block>::back_bottom_right(const std::size_t component,
		const Box3d<int>& box){
	DPoint3d<int> dp(box.depth(),box.height(),box.width());
	return (typename GenericMultiComponent3d<Block>::component_iterator3d
			((*this).front_upper_left(component,box) + dp));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_iterator3d
GenericMultiComponent3d<Block>::front_upper_left(const std::size_t component,
		const Box3d<int>& box) const{
	return typename GenericMultiComponent3d<Block>::const_component_iterator3d(this,component,box);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_iterator3d
GenericMultiComponent3d<Block>::back_bottom_right(const std::size_t component,
		const Box3d<int>& box) const{
	DPoint3d<int> dp(box.depth(),box.height(),box.width());
	return (typename GenericMultiComponent3d<Block>::const_component_iterator3d
			((*this).front_upper_left(component,box) + dp));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_iterator3d
GenericMultiComponent3d<Block>::rback_bottom_right(const std::size_t component,
		const slip::Box3d<int>& box)
		{
	return typename GenericMultiComponent3d<Block>::reverse_component_iterator3d
			(this->front_upper_left(component,box));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_iterator3d
GenericMultiComponent3d<Block>::rfront_upper_left(const std::size_t component,
		const slip::Box3d<int>& box)
		{
	slip::DPoint3d<int> dp(1,1,0);
	return typename GenericMultiComponent3d<Block>::reverse_component_iterator3d
			(this->back_bottom_right(component,box) - dp);
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_iterator3d
GenericMultiComponent3d<Block>::rback_bottom_right(const std::size_t component,
		const slip::Box3d<int>& box) const
		{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_iterator3d
			(this->front_upper_left(component,box));
		}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_iterator3d
GenericMultiComponent3d<Block>::rfront_upper_left(const std::size_t component,
		const slip::Box3d<int>& box) const
		{
	slip::DPoint3d<int> dp(1,1,0);
	return typename GenericMultiComponent3d<Block>::const_reverse_component_iterator3d
			(this->back_bottom_right(component,box) - dp);
		}

//------------------------ Range component iterators------------------------------------

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_iterator3d_range
GenericMultiComponent3d<Block>::front_upper_left(const std::size_t component,
		const Range<int>& slice_range, const Range<int>& row_range,
		const Range<int>& col_range){
	return typename GenericMultiComponent3d<Block>::component_iterator3d_range
			(this,component,slice_range,row_range,col_range);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::component_iterator3d_range
GenericMultiComponent3d<Block>::back_bottom_right(const std::size_t component,
		const Range<int>& slice_range, const Range<int>& row_range,
		const Range<int>& col_range){
	DPoint3d<int> dp(slice_range.iterations()+1,row_range.iterations()+1,col_range.iterations()+1);
	return typename GenericMultiComponent3d<Block>::component_iterator3d_range
			((*this).front_upper_left(component,slice_range,row_range,col_range) + dp);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_iterator3d_range
GenericMultiComponent3d<Block>::front_upper_left(const std::size_t component,
		const Range<int>& slice_range, const Range<int>& row_range,
		const Range<int>& col_range) const{
	return typename GenericMultiComponent3d<Block>::const_component_iterator3d_range
			(this,component,slice_range,row_range,col_range);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_component_iterator3d_range
GenericMultiComponent3d<Block>::back_bottom_right(const std::size_t component,
		const Range<int>& slice_range, const Range<int>& row_range,
		const Range<int>& col_range) const{
	DPoint3d<int> dp(slice_range.iterations()+1,row_range.iterations()+1,col_range.iterations()+1);
	return typename GenericMultiComponent3d<Block>::const_component_iterator3d_range
			((*this).front_upper_left(component,slice_range,row_range,col_range) + dp);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_iterator3d_range
GenericMultiComponent3d<Block>::rfront_upper_left(const std::size_t component,
		const Range<int>& slice_range, const Range<int>& row_range,
		const Range<int>& col_range){
	slip::DPoint3d<int> dp(1,1,0);
	return typename GenericMultiComponent3d<Block>::reverse_component_iterator3d_range
			(this->back_bottom_right(component,slice_range,row_range,col_range) - dp);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reverse_component_iterator3d_range
GenericMultiComponent3d<Block>::rback_bottom_right(const std::size_t component,
		const Range<int>& slice_range, const Range<int>& row_range,
		const Range<int>& col_range){
	return typename GenericMultiComponent3d<Block>::reverse_component_iterator3d_range
			(this->front_upper_left(component,slice_range,row_range,col_range));
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_iterator3d_range
GenericMultiComponent3d<Block>::rfront_upper_left(const std::size_t component,
		const Range<int>& slice_range, const Range<int>& row_range,
		const Range<int>& col_range) const{
	slip::DPoint3d<int> dp(1,1,0);
	return typename GenericMultiComponent3d<Block>::const_reverse_component_iterator3d_range
			(this->back_bottom_right(component,slice_range,row_range,col_range) - dp);
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reverse_component_iterator3d_range
GenericMultiComponent3d<Block>::rback_bottom_right(const std::size_t component,
		const Range<int>& slice_range, const Range<int>& row_range,
		const Range<int>& col_range) const{
	return typename GenericMultiComponent3d<Block>::const_reverse_component_iterator3d_range
			(this->front_upper_left(component,slice_range,row_range,col_range));
}
///////////////////////////////////////////


template <typename Block>
inline
std::ostream& operator<<(std::ostream & out,
		const GenericMultiComponent3d<Block>& a)
{
	out << *(a.matrix_) << std::endl;
	return out;
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::pointer* GenericMultiComponent3d<Block>::operator[](const typename GenericMultiComponent3d<Block>::size_type k)
{
	return (*matrix_)[k];
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_pointer const* GenericMultiComponent3d<Block>::operator[](const typename GenericMultiComponent3d<Block>::size_type k) const
{
	Matrix3d<Block> const *tp(matrix_);
	return (*tp)[k];
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::reference GenericMultiComponent3d<Block>::operator()(const typename GenericMultiComponent3d<Block>::size_type k,
		const typename GenericMultiComponent3d<Block>::size_type i,
		const typename GenericMultiComponent3d<Block>::size_type j)
{
	return (*matrix_)[k][i][j];
}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::const_reference GenericMultiComponent3d<Block>::operator()(const typename GenericMultiComponent3d<Block>::size_type k,
		const typename GenericMultiComponent3d<Block>::size_type i,
		const typename GenericMultiComponent3d<Block>::size_type j) const
{
	return (*matrix_)[k][i][j];
}


template<typename Block>
inline
typename GenericMultiComponent3d<Block>::size_type GenericMultiComponent3d<Block>::dim1() const {return matrix_->dim1();}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::size_type GenericMultiComponent3d<Block>::slices() const {return matrix_->dim1();}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::size_type GenericMultiComponent3d<Block>::rows() const {return matrix_->dim2();}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::size_type GenericMultiComponent3d<Block>::dim2() const {return matrix_->dim2();}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::size_type GenericMultiComponent3d<Block>::dim3() const {return matrix_->dim3();}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::size_type GenericMultiComponent3d<Block>::columns() const {return matrix_->dim3();}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::size_type GenericMultiComponent3d<Block>::cols() const {return matrix_->dim3();}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::size_type GenericMultiComponent3d<Block>::size() const {return matrix_->size();}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::size_type GenericMultiComponent3d<Block>::max_size() const {return matrix_->max_size();}

template<typename Block>
inline
typename GenericMultiComponent3d<Block>::size_type GenericMultiComponent3d<Block>::slice_size() const {return matrix_->slice_size();}

template<typename Block>
inline
bool GenericMultiComponent3d<Block>::empty()const {return matrix_->empty();}

template<typename Block>
inline
void GenericMultiComponent3d<Block>::swap(GenericMultiComponent3d<Block>& M)
{
	matrix_->swap(*(M.matrix_));
}

template<typename Block>
inline
Block GenericMultiComponent3d<Block>::min() const
{
	Block tmp;
	for(typename GenericMultiComponent3d<Block>::size_type component = 0; component < Block::SIZE; ++component)
	{
		typename Block::value_type min = *std::min_element(this->begin(component),this->end(component));
		tmp[component] = min;
	}
	return tmp;
}

template<typename Block>
inline
Block GenericMultiComponent3d<Block>::max() const
{
	Block tmp;
	for(typename GenericMultiComponent3d<Block>::size_type component = 0; component < Block::SIZE; ++component)
	{
		typename Block::value_type max = *std::max_element(this->begin(component),this->end(component));
		tmp[component] = max;
	}
	return tmp;
}

template<typename Block>
inline
GenericMultiComponent3d<Block>& GenericMultiComponent3d<Block>::apply(Block(*fun)(Block))
{
	matrix_->apply(fun);
	return *this;
}

template<typename Block>
inline
GenericMultiComponent3d<Block>& GenericMultiComponent3d<Block>::apply(Block(*fun)(const Block&))
{
	matrix_->apply(fun);
	return *this;
}

template<typename Block>
inline
bool operator==(const GenericMultiComponent3d<Block>& x,
		const GenericMultiComponent3d<Block>& y)
		{
	return ( x.size() == y.size()
			&& std::equal(x.begin(),x.end(),y.begin()));
		}

template<typename Block>
inline
bool operator!=(const GenericMultiComponent3d<Block>& x,
		const GenericMultiComponent3d<Block>& y)
		{
	return !(x == y);
		}


// template<typename Block>
// inline
// bool operator<(const GenericMultiComponent3d<Block>& x,
// 	       const GenericMultiComponent3d<Block>& y)
// {
//   return std::lexicographical_compare(x.begin(), x.end(),
// 				      y.begin(), y.end());
// }

// template<typename Block>
// inline
// bool operator>(const GenericMultiComponent3d<Block>& x,
// 	       const GenericMultiComponent3d<Block>& y)
// {
//   return (y < x);
// }


// template<typename Block>
// inline
// bool operator<=(const GenericMultiComponent3d<Block>& x,
// 		const GenericMultiComponent3d<Block>& y)
// {
//   return !(y < x);
// }


// template<typename Block>
// inline
// bool operator>=(const GenericMultiComponent3d<Block>& x,
// 		const GenericMultiComponent3d<Block>& y)
// {
//   return !(x < y);
// }
template <typename Block>
struct L2_dist<typename Block::value_type, slip::GenericMultiComponent3d<Block> >
{
	typename Block::value_type
	operator()(const slip::GenericMultiComponent3d<Block>& __x,
			const slip::GenericMultiComponent3d<Block>& __y) const

	{
		typename Block::value_type dist = slip::L22_distance<Block>(__x.begin(),__x.end(),__y.begin());
		return std::sqrt(dist);
	}
};
}//slip::

#endif //SLIP_GENERICMULTICOMPONENT3D_HPP
