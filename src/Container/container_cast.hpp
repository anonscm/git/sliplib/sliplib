/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file container_cast.hpp
 * 
 * \brief Provides some algorithms to get the default iterators associated with  containers.
 * \since 1.4.0
 * \date 2014/04/02
 * 
 */
#ifndef SLIP_CONTAINER_CAST_HPP
#define SLIP_CONTAINER_CAST_HPP

#include "iterator_types.hpp"
#include "DPoint2d.hpp"
#include "DPoint3d.hpp"

namespace slip
{
  //-----------------------------------------------------------
  //  Cast a container into its default iterators
  //-----------------------------------------------------------
  template<typename>
  struct __container_iterator_cast
  {
    template<typename Container1, 
	     typename Iterator1>
    static void 
    container_iterator_cast(Container1& container,
			    Iterator1& first,
			    Iterator1& last)

    {
      // first = container.begin();
      // last  = container.end();
    }
  };

  template<>
  struct __container_iterator_cast<std::random_access_iterator_tag>
  {
    template<typename Container1, 
	     typename Iterator1>
    static void 
    container_iterator_cast(Container1& container,
			    Iterator1& first,
			    Iterator1& last)
    {
      first = container.begin();
      last  = container.end();
    }
  };

  template<>
  struct __container_iterator_cast<std::random_access_iterator2d_tag>
  {
    template<typename Container1, 
	     typename Iterator1>
    static void 
    container_iterator_cast(Container1& container,
			    Iterator1& first,
			    Iterator1& last)
    {
      first = container.upper_left();
      last  = container.bottom_right();
    }
  };

  template<>
  struct __container_iterator_cast<std::random_access_iterator3d_tag>
  {
    template<typename Container1, 
	     typename Iterator1>
    static void 
    container_iterator_cast(Container1& container,
			    Iterator1& first,
			    Iterator1& last)
    {
      first = container.front_upper_left();
      last  = container.back_bottom_right();
    }
  };


  /*!
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2010/05/10
  ** \brief Get the default iterators of a SLIP container.
  ** \param cont Container.
  ** \param first iterator to the first element of the container.
  ** \param last iterator to one past-the-end element of the container.
  ** \par Example 1:
  ** \code
  ** slip::Array<double> V(5);
  ** slip::iota(V.begin(),V.end(),1.0,1.0);
  ** std::cout<<"V = \n"<<V<<std::endl;
  ** slip::Array<double>::default_iterator firstV;
  ** slip::Array<double>::default_iterator lastV;
  ** slip::container_cast(V,firstV,lastV);
  **
  ** for(; firstV != lastV; ++firstV)
  **  {
  **   std::cout<<*firstV<<" ";
  **  }
  ** std::cout<<std::endl;
  ** \endcode
  ** \par Example 2:
  ** \code
  ** slip::Array2d<double> A(5,6);
  ** slip::iota(A.begin(),A.end(),1.0,1.0);
  ** std::cout<<"A = \n"<<A<<std::endl;
  ** slip::Array2d<double>::default_iterator firstA;
  ** slip::Array2d<double>::default_iterator lastA;
  ** slip::container_cast(A,firstA,lastA);
  ** 
  ** for(; firstA != lastA; ++firstA)
  **  {
  **  std::cout<<"("<<firstA.i()<<","<<firstA.j()<<") ";
  ** }
  ** std::cout<<std::endl;
  ** \endcode

  ** \par Example 3:
  ** \code
  ** slip::Array3d<double> Vol(2,5,6);
  ** slip::iota(Vol.begin(),Vol.end(),1.0,1.0);
  ** std::cout<<"Vol = \n"<<Vol<<std::endl;
  ** slip::Array3d<double>::default_iterator firstVol;
  ** slip::Array3d<double>::default_iterator lastVol;
  ** slip::container_cast(Vol,firstVol,lastVol);
  **
  ** for(; firstVol != lastVol; ++firstVol)
  ** {
  **   std::cout<<"("<<firstVol.k()<<","<<firstVol.i()<<","<<firstVol.j()<<") ";
  ** }
  ** std::cout<<std::endl;
  ** \endcode
  */
  template<typename Container, typename _II>
  void container_cast(Container& cont, _II& first, _II& last)
  {
    typedef typename std::iterator_traits<_II>::iterator_category _Category;
    __container_iterator_cast<_Category>::container_iterator_cast(cont,first,last);
  }
 



  //-----------------------------------------------------------
  //  Cast a container into its reverse iterators
  //-----------------------------------------------------------
  template<typename>
  struct __reverse_container_iterator_cast
  {
    template<typename Container1, 
	     typename Iterator1>
    static void 
    reverse_container_iterator_cast(Container1& container,
				     Iterator1& first,
				     Iterator1& last)

    {
      first = container.rbegin();
      last  = container.rend();
    }
  };

  template<>
  struct __reverse_container_iterator_cast<std::random_access_iterator_tag>
  {
    template<typename Container1, 
	     typename Iterator1>
    static void 
    reverse_container_iterator_cast(Container1& container,
				     Iterator1& first,
				     Iterator1& last)
    {
      first = container.rbegin();
      last  = container.rend();
    }
  };

  template<>
  struct __reverse_container_iterator_cast<std::random_access_iterator2d_tag>
  {
    template<typename Container1, 
	     typename Iterator1>
    static void 
    reverse_container_iterator_cast(Container1& container,
				     Iterator1& first,
				     Iterator1& last)
    {
      slip::DPoint2d<int> dp(1,0);
      first = container.bottom_right() - dp;
      last  = container.upper_left();
      //can not access to i() and j() function like that
      //for that we should have to define our own reverse_iterator2d
      // first = container.rbottom_right();
      // last  = container.rupper_left();
    }
  };

  template<>
  struct __reverse_container_iterator_cast<std::random_access_iterator3d_tag>
  {
    template<typename Container1, 
	     typename Iterator1>
    static void 
    reverse_container_iterator_cast(Container1& container,
			    Iterator1& first,
			    Iterator1& last)
    {
      // first = container.rfront_upper_left();
      // last  = container.rback_bottom_right();
      //can not access to k(), i() and j() function like that
      //for that we should have to define our own reverse_iterator3d
       DPoint3d<int> dp(1,1,0);
       first = container.back_bottom_right() - dp;
       last = container.front_upper_left();
    }
  };



  /*!
  ** \author Thibault Roulier <troulier_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2010/05/10
  ** \brief Get the reverse default iterators of a SLIP container.
  ** \param cont Container.
  ** \param first reverse iterator to the first element of the container.
  ** \param last reverse iterator to one past-the-end element of the container.
  ** \par Example:
  // ** \code
  // ** slip::GrayscaleImage<double> I;
  // ** I.read("lena.png");
  // ** slip::GrayscaleImage<double>::default_iterator first;
  // ** slip::GrayscaleImage<double>::default_iterator last;
  // ** slip::container_cast(I,first,last);
  // ** \endcode
  */
  template<typename Container, typename _II>
  void reverse_container_cast(Container& cont, _II& first, _II& last)
  {
    typedef typename std::iterator_traits<_II>::iterator_category _Category;
    __reverse_container_iterator_cast<_Category>::reverse_container_iterator_cast(cont,first,last);
  }
 


  //DEPRECATED !!!!!
  //-----------------------------------------------------------
  //  Cast a container into its reverse iterators
  //-----------------------------------------------------------
  template<typename>
  struct __container_iterator_castR
  {
    template<typename Container1, 
	     typename Iterator1>
    static void 
    container_iterator_castR(Container1& container,
			    Iterator1& first,
			    Iterator1& last)

    {
      first = container.rbegin();
      last  = container.rend();
    }
  };

  template<>
  struct __container_iterator_castR<std::random_access_iterator_tag>
  {
    template<typename Container1, 
	     typename Iterator1>
    static void 
    container_iterator_castR(Container1& container,
			    Iterator1& first,
			    Iterator1& last)
    {
      first = container.rbegin();
      last  = container.rend();
    }
  };

  template<>
  struct __container_iterator_castR<std::random_access_iterator2d_tag>
  {
    template<typename Container1, 
	     typename Iterator1>
    static void 
    container_iterator_castR(Container1& container,
			    Iterator1& first,
			    Iterator1& last)
    {
      slip::DPoint2d<int> dp(1,0);
      first = container.bottom_right() - dp;
      last  = container.upper_left();
    }
  };

  template<>
  struct __container_iterator_castR<std::random_access_iterator3d_tag>
  {
    template<typename Container1, 
	     typename Iterator1>
    static void 
    container_iterator_castR(Container1& container,
			    Iterator1& first,
			    Iterator1& last)
    {
      first = container.rfront_upper_left();
      last  = container.rback_bottom_right();
    }
  };



  /*!
  ** \author Thibault Roulier <troulier_AT_sic.univ-poitiers.fr>
  ** \version 0.0.1
  ** \date 2010/05/10
  ** \brief Get the reverse default iterators of a SLIP container.
  ** \param cont Container.
  ** \param first reverse iterator to the first element of the container.
  ** \param last reverse iterator to one past-the-end element of the container.
  ** \par Example:
  // ** \code
  // ** slip::GrayscaleImage<double> I;
  // ** I.read("lena.png");
  // ** slip::GrayscaleImage<double>::default_iterator first;
  // ** slip::GrayscaleImage<double>::default_iterator last;
  // ** slip::container_castR(I,first,last);
  // ** \endcode
  */
  template<typename Container, typename _II>
  void container_castR(Container& cont, _II& first, _II& last)
  {
    typedef typename std::iterator_traits<_II>::iterator_category _Category;
    __container_iterator_castR<_Category>::container_iterator_castR(cont,first,last);
  }

}//::slip

#endif //CONTAINER_CAST_HPP
