/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file DenseVector3dField3d.hpp
 * 
 * \brief Provides a class to manipulate 3d dense vector fields.
 * 
 */
#ifndef SLIP_DENSEVECTOR3DFIELD3D_HPP
#define SLIP_DENSEVECTOR3DFIELD3D_HPP

#include <iostream>
#include <fstream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <cmath>
#include <string>
#include <cstddef>
#include "Matrix3d.hpp"
#include "stride_iterator.hpp"
#include "kstride_iterator.hpp"
#include "iterator3d_box.hpp"
#include "iterator3d_range.hpp"
#include "apply.hpp"
#include "Vector3d.hpp"
#include "GenericMultiComponent3d.hpp"
#include "derivatives.hpp"
#include "arithmetic_op.hpp"
#include "io_tools.hpp"
#include "linear_algebra_traits.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/base_object.hpp>

namespace slip
{

template<class T>
class stride_iterator;

template<class T>
class iterator3d_box;

template<typename T>
class iterator3d_range;

template<typename T>
class const_iterator3d_box;

template<typename T>
class const_iterator3d_range;

template <class T>
class DPoint3d;

template <class T>
class Point3d;

template <class T>
class Box3d;

template <typename T>
class DenseVector3dField3d;



/*! \class DenseVector3dField3d
 **  \ingroup Containers Containers3d MultiComponent3dContainers  
** \brief This is a Dense %Vector3d Field.
** This container statisfies the BidirectionnalContainer concepts of the STL.
** It is also an 3d extension of the RandomAccessContainer concept. That is
** to say the bracket element access is replaced by the triple bracket element 
** access.
** It is a specialization of GenericMultiComponent3d using Vector3d blocks.
** It implements arithmetic and mathematical operators and read/write methods (tecplot and gnuplot file format...).
** Contrary to slip::RegularVector3dField3d, this container is not associated with a grid.
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.4
** \date 2014/12/08
** \param T Type of object in the DenseVector3dField3d
 ** \par Axis conventions:
** \image html iterator3d_conventions.jpg "axis and notation conventions"
** \image latex iterator3d_conventions.eps "axis and notation conventions" width=5cm
  **
*/
template <typename T>
class DenseVector3dField3d:public slip::GenericMultiComponent3d<slip::Vector3d<T> >
{
public :

  typedef slip::Vector3d<T> value_type;
  typedef DenseVector3dField3d<T> self;
  typedef GenericMultiComponent3d<slip::Vector3d<T> > base;


  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;
  
  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;


  typedef pointer row_iterator;
  typedef const_pointer const_row_iterator;
  typedef slip::stride_iterator<pointer> col_iterator;
  typedef slip::stride_iterator<const_pointer> const_col_iterator;

  typedef std::reverse_iterator<iterator> reverse_row_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
  typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
  typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;

  typedef typename slip::GenericMultiComponent3d<slip::Vector3d<T> >::iterator3d iterator3d;
 typedef typename slip::GenericMultiComponent3d<slip::Vector3d<T> >::const_iterator2d const_iterator3d;

  typedef T vector3d_value_type;
  typedef vector3d_value_type* vector3d_pointer;
  typedef const vector3d_value_type* const_vector3d_pointer;
  typedef vector3d_value_type& vector3d_reference;
  typedef const vector3d_value_type const_vector3d_reference;
  typedef slip::kstride_iterator<vector3d_pointer,3> vector3d_iterator;
  typedef slip::kstride_iterator<const_vector3d_pointer,3> const_vector3d_iterator;

  //typedef typename slip::lin_alg_traits<value_type>::value_type norm_type;
  typedef typename slip::Vector3d<T>::norm_type norm_type;

  static const std::size_t DIM = 3;
public:
 /**
  ** \name Constructors & Destructors
  */
 /*@{*/

  /*!
  ** \brief Constructs a %DenseVector3dField3d.
  */
  DenseVector3dField3d():
    base()
  {}
 
  /*!
  ** \brief Constructs a %DenseVector3dField3d.
  ** \param slices first dimension of the %GenericMultiComponent3d
  ** \param rows second dimension of the %GenericMultiComponent3d
  ** \param cols third dimension of the %GenericMultiComponent3d
  */
  DenseVector3dField3d(const size_type slices,
		       const size_type rows,
		       const size_type cols):
    base(slices,rows,cols)
  {}
  
  /*!
  ** \brief Constructs a %DenseVector3dField3d initialized by the scalar value \a val.
  ** \param slices first dimension of the %GenericMultiComponent3d
  ** \param rows second dimension of the %GenericMultiComponent3d
  ** \param cols third dimension of the %GenericMultiComponent3d
  ** \param val initialization value of the elements 
  */
  DenseVector3dField3d(const size_type slices,
		       const size_type rows,
		       const size_type cols,
		       const slip::Vector3d<T>& val):
    base(slices,rows,cols,val)
  {}
 
  /*!
  ** \brief Constructs a %DenseVector3dField3d initialized by an array \a val.
  ** \param slices first dimension of the %GenericMultiComponent3d
  ** \param rows second dimension of the %GenericMultiComponent3d
  ** \param cols third dimension of the %GenericMultiComponent3d
  ** \param val initialization linear array value of the elements 
  */
  DenseVector3dField3d(const size_type slices,
		       const size_type rows,
		       const size_type cols,
		       const T* val):
    base(slices,rows,cols,val)
  {}
 
  /*!
  ** \brief Constructs a %DenseVector3dField3d initialized by an array \a val.
  ** \param slices first dimension of the %GenericMultiComponent3d
  ** \param rows second dimension of the %GenericMultiComponent3d
  ** \param cols third dimension of the %GenericMultiComponent3d
  ** \param val initialization array value of the elements 
  */
  DenseVector3dField3d(const size_type slices,
		       const size_type rows,
		       const size_type cols,
		       const slip::Vector3d<T>* val):
    base(slices,rows,cols,val)
  {}
 

  /**
  **  \brief  Contructs a %DenseVector3dField3d from a range.
  ** \param slices first dimension of the %GenericMultiComponent3d
  ** \param rows second dimension of the %GenericMultiComponent3d
  ** \param cols third dimension of the %GenericMultiComponent3d
  **  \param  first  An input iterator.
  **  \param  last  An input iterator.
  **
  ** Create a %DenseVector3dField3d consisting of copies of the elements from
  ** [first,last).
  */
  template<typename InputIterator>
  DenseVector3dField3d(const size_type slices,
		       const size_type rows,
		       const size_type cols,
		       InputIterator first,
		       InputIterator last):
    base(slices,rows,cols,first,last)
  {} 



  
 /**
  **  \brief  Contructs a %DenseVector3dField3d from a 3 ranges.
  ** \param slices first dimension of the %GenericMultiComponent3d
  ** \param rows second dimension of the %GenericMultiComponent3d
  ** \param cols third dimension of the %GenericMultiComponent3d
  **  \param  first1  An input iterator.
  **  \param  last1  An input iterator.
  **  \param  first2  An input iterator.
  **  \param  first3  An input iterator.
 
  **
  ** Create a %DenseVector3dField3d consisting of copies of the elements from
  ** [first1,last1), 
  ** [first2,first2 + (last1 - first1)), 
  ** [first3,first3 + (last1 - first1)), 
  */
  template<typename InputIterator>
  DenseVector3dField3d(const size_type slices,
		       const size_type rows,
		       const size_type cols,
		       InputIterator first1,
		       InputIterator last1,
		       InputIterator first2,
		       InputIterator first3):
    base(slices,rows,cols)
  {
    
    std::vector<InputIterator> first_iterators_list(3);
    first_iterators_list[0] = first1;
    first_iterators_list[1] = first2;
    first_iterators_list[2] = first3;
    this->fill(first_iterators_list,last1);
  
  } 


  /*!
   ** \brief Constructs a copy of the %DenseVector3dField3d \a rhs
  */
  DenseVector3dField3d(const self& rhs):
    base(rhs)
  {}

  /*!
  ** \brief Destructor of the %DenseVector3dField3d
  */
  ~DenseVector3dField3d()
  {}
  

  /*@} End Constructors */


  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;



  /**
   ** \name i/o operators
   */
  /*@{*/
  
 
  /*!
  ** \brief Writes a DenseVector3dField3d to a tecplot file path name
  ** \param file_path_name
  ** \param title
  ** \param zone
  ** 
  ** \par The data format is the following:
  **
  ** TITLE= title 
  **
  ** VARIABLES= X Y Z U V W 
  **
  ** ZONE T= zone, K= dim1(), I= dim2(), J= dim3() 
  **
  ** x y z Vx Vy Vz
  */ 
  void write_tecplot(const std::string& file_path_name,
		     const std::string& title,
		     const std::string& zone);


 

  /*!
  ** \brief Reads a DenseVector3dField3d from a tecplot file path name
  ** \param file_path_name
  ** \par The data format is the following:
  **
  ** TITLE= title 
  **
  ** VARIABLES= X Y Z U V W 
  **
  ** ZONE T= zone, K= dim1(), I= dim2(), J= dim3() 
  **
  ** x y z Vx Vy Vz
  */ 
  void read_tecplot(const std::string& file_path_name);


  /*@} End i/o operators */

  /**
   ** \name  Assignment operators and methods
   */
  /*@{*/

 

  /*!
  ** \brief Affects all the element of the %DenseVector3dField3d by val
  ** \param val affectation value
  ** \return reference to corresponding %DenseVector3dField3d
  */
  self& operator=(const slip::Vector3d<T>& val);


  /*!
  ** \brief Affects all the element of the %DenseVector3dField3d by val
  ** \param val affectation value
  ** \return reference to corresponding %DenseVector3dField3d
  */
  self& operator=(const T& val);



  
 
  /*@} End Assignment operators and methods*/

  /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Subscript access to first element of 
  ** the data contained in the %DenseVector3dField3d.
  ** \param k The index of the slice for which the data should be accessed. 
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the column for which the data should be accessed.
  ** \return Read/Write reference to data.
   ** \pre k < slices()
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 3d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  T& Vx1(const size_type k,
	 const size_type i,
	 const size_type j);

   /*!
  ** \brief Subscript access to first element of 
  ** the data contained in the %DenseVector3dField3d.
  ** \param k The index of the slice for which the data should be accessed. 
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the column for which the data should be accessed.
  ** \return Read const_reference to data.
   ** \pre k < slices()
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 3d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const T& Vx1(const size_type k,
	       const size_type i,
	       const size_type j) const;
 /*!
  ** \brief Subscript access to first element of 
  ** the data contained in the %DenseVector3dField3d.
  ** \param k The index of the slice for which the data should be accessed. 
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the column for which the data should be accessed.
  ** \return Read/Write reference to data.
   ** \pre k < slices()
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 3d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  T& u(const size_type k,
       const size_type i,
       const size_type j);

   /*!
  ** \brief Subscript access to first element of 
  ** the data contained in the %DenseVector3dField3d.
  ** \param k The index of the slice for which the data should be accessed. 
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the column for which the data should be accessed.
  ** \return Read const_reference to data.
   ** \pre k < slices()
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 3d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const T& u(const size_type k,
	     const size_type i,
	     const size_type j) const;
 
 
  /*!
  ** \brief Subscript access to second element of 
  ** the data contained in the %DenseVector3dField3d.
  ** \param k The index of the slice for which the data should be accessed. 
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the column for which the data should be accessed.
  ** \return Read/Write reference to data.
   ** \pre k < slices()
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 3d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  T& Vx2(const size_type k,
	 const size_type i,
	 const size_type j);

   /*!
  ** \brief Subscript access to second element of 
  ** the data contained in the %DenseVector3dField3d.
  ** \param k The index of the slice for which the data should be accessed. 
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the column for which the data should be accessed.
  ** \return Read const_reference to data.
   ** \pre k < slices()
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 3d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const T& Vx2(const size_type k,
	       const size_type i,
	       const size_type j) const;
  /*!
  ** \brief Subscript access to second element of 
  ** the data contained in the %DenseVector3dField3d.
  ** \param k The index of the slice for which the data should be accessed. 
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the column for which the data should be accessed.
  ** \return Read/Write reference to data.
   ** \pre k < slices()
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 3d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  T& v(const size_type k,
       const size_type i,
       const size_type j);

   /*!
  ** \brief Subscript access to second element of 
  ** the data contained in the %DenseVector3dField3d.
  ** \param k The index of the slice for which the data should be accessed. 
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the column for which the data should be accessed.
  ** \return Read const_reference to data.
   ** \pre k < slices()
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 3d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const T& v(const size_type k,
	     const size_type i,
	     const size_type j) const;

 
 
  /*!
  ** \brief Subscript access to third element of 
  ** the data contained in the %DenseVector3dField3d.
  ** \param k The index of the slice for which the data should be accessed. 
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the column for which the data should be accessed.
  ** \return Read/Write reference to data.
   ** \pre k < slices()
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 3d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  T& Vx3(const size_type k,
	 const size_type i,
	 const size_type j);

   /*!
  ** \brief Subscript access to third element of 
  ** the data contained in the %DenseVector3dField3d.
  ** \param k The index of the slice for which the data should be accessed. 
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the column for which the data should be accessed.
  ** \return Read const_reference to data.
   ** \pre k < slices()
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 3d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const T& Vx3(const size_type k,
	       const size_type i,
	       const size_type j) const;

  /*!
  ** \brief Subscript access to third element of 
  ** the data contained in the %DenseVector3dField3d.
  ** \param k The index of the slice for which the data should be accessed. 
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the column for which the data should be accessed.
  ** \return Read/Write reference to data.
   ** \pre k < slices()
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 3d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  T& w(const size_type k,
	 const size_type i,
	 const size_type j);

   /*!
  ** \brief Subscript access to third element of 
  ** the data contained in the %DenseVector3dField3d.
  ** \param k The index of the slice for which the data should be accessed. 
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the column for which the data should be accessed.
  ** \return Read const_reference to data.
   ** \pre k < slices()
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 3d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const T& w(const size_type k,
	     const size_type i,
	     const size_type j) const;

  /*!
  ** \brief Subscript access to a local norm contained in the
  ** %DenseVector3dField3d.
  ** \param k The index of the slice for which the data should be accessed. 
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read_only (constant) reference to data.
  ** \pre k < slices()
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 3d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  norm_type norm(const size_type k,
		 const size_type i,
		 const size_type j) const;
 
/*@} End Element access operators */

 /**
  ** \name Comparison operators
 */
  /*@{*/


   /*@} Comparison operators */

  
 /**
   ** \name  Arithmetic operators
   */
  /*@{*/
  /*!
  ** \brief Add val to each element of the %DenseVector3dField3d  
  ** \param val value
  ** \return reference to the resulting %DenseVector3dField3d
  */
  self& operator+=(const T& val);
  self& operator-=(const T& val);
  self& operator*=(const T& val);
  self& operator/=(const T& val);

//   self& operator%=(const T& val);
//   self& operator^=(const T& val);
//   self& operator&=(const T& val);
//   self& operator|=(const T& val);
//   self& operator<<=(const T& val);
//   self& operator>>=(const T& val);


   self  operator-() const;
//   self  operator!() const;

  /*!
  ** \brief Add val to each element of the %DenseVector3dField3d  
  ** \param val value
  ** \return reference to the resulting %DenseVector3dField3d
  */
  self& operator+=(const slip::Vector3d<T>& val);
  self& operator-=(const slip::Vector3d<T>& val);
  self& operator*=(const slip::Vector3d<T>& val);
  self& operator/=(const slip::Vector3d<T>& val);

  

  self& operator+=(const self& rhs);
  self& operator-=(const self& rhs);
  self& operator*=(const self& rhs);
  self& operator/=(const self& rhs);
    

  /*@} End Arithmetic operators */
 
/**
   ** \name  Mathematical operators
   */
  /*@{*/

 

  /*!
   ** \brief Computes Eucliean norm of each element in the field and write it
   ** to a Container3D.
   ** \author Benoit Tremblais <benoit.tremblais_AT_sic.univ-poitiers.fr>
   ** \date 2008/05/25
   ** \version 0.0.1
   ** \param result A Container3D which contain the norm of each element.
   */
  template<typename Container3D>
  void norm(Container3D& result)
  {
    //typedef slip::Vector3d<double> vector;
    std::transform(this->begin(),this->end(),result.begin(),std::mem_fun_ref(&value_type::Euclidean_norm));

  }

  
   /*@} End Mathematic operators */
private :
   friend class boost::serialization::access;
 template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    { 
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::GenericMultiComponent3d<slip::Vector3d<T> > >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    { 
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::GenericMultiComponent3d<slip::Vector3d<T> > >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
 };

///double alias
  typedef slip::DenseVector3dField3d<double> DenseVector3dField3d_d;
  ///float alias
  typedef slip::DenseVector3dField3d<float> DenseVector3dField3d_f;
  ///long alias
  typedef slip::DenseVector3dField3d<long> DenseVector3dField3d_l;
  ///unsigned long alias
  typedef slip::DenseVector3dField3d<unsigned long> DenseVector3dField3d_ul;
  ///short alias
  typedef slip::DenseVector3dField3d<short> DenseVector3dField3d_s;
  ///unsigned long alias
  typedef slip::DenseVector3dField3d<unsigned short> DenseVector3dField3d_us;
  ///int alias
  typedef slip::DenseVector3dField3d<int> DenseVector3dField3d_i;
  ///unsigned int alias
  typedef slip::DenseVector3dField3d<unsigned int> DenseVector3dField3d_ui;
  ///char alias
  typedef slip::DenseVector3dField3d<char> DenseVector3dField3d_c;
  ///unsigned char alias
  typedef slip::DenseVector3dField3d<unsigned char> DenseVector3dField3d_uc;


}//slip::

namespace slip{
 
/** \name Arithmetical DenseVector3dField3d operators */
  /* @{ */
/*!
  ** \brief addition of a scalar to each element of a %DenseVector3dField3d
  ** \param M1 the %DenseVector3dField3d 
  ** \param val the scalar
  ** \return resulting %DenseVector3dField3d
  */
template<typename T>
DenseVector3dField3d<T> operator+(const DenseVector3dField3d<T>& M1, 
				  const T& val);

 /*!
  ** \brief addition of a scalar to each element of a %DenseVector3dField3d
  ** \param val the scalar
  ** \param M1 the %DenseVector3dField3d  
  ** \return resulting %DenseVector3dField3d
  */
  template<typename T>
  DenseVector3dField3d<T> operator+(const T& val, 
				    const DenseVector3dField3d<T>& M1);


/*!
  ** \brief substraction of a scalar to each element of a %DenseVector3dField3d
  ** \param M1 the %DenseVector3dField3d 
  ** \param val the scalar
  ** \return resulting %DenseVector3dField3d
  */
template<typename T>
DenseVector3dField3d<T> operator-(const DenseVector3dField3d<T>& M1, 
				  const T& val);

 /*!
  ** \brief substraction of a scalar to each element of a %DenseVector3dField3d
  ** \param val the scalar
  ** \param M1 the %DenseVector3dField3d  
  ** \return resulting %DenseVector3dField3d
  */
  template<typename T>
  DenseVector3dField3d<T> operator-(const T& val, 
				    const DenseVector3dField3d<T>& M1);



   /*!
  ** \brief multiplication of a scalar to each element of a %DenseVector3dField3d
  ** \param M1 the %DenseVector3dField3d 
  ** \param val the scalar
  ** \return resulting %DenseVector3dField3d
  */
  template<typename T>
  DenseVector3dField3d<T> operator*(const DenseVector3dField3d<T>& M1, 
				    const T& val);

 /*!
  ** \brief multiplication of a scalar to each element of a %DenseVector3dField3d
  ** \param val the scalar
  ** \param M1 the %DenseVector3dField3d  
  ** \return resulting %DenseVector3dField3d
  */
  template<typename T>
  DenseVector3dField3d<T> operator*(const T& val, 
				    const DenseVector3dField3d<T>& M1);
  


 /*!
  ** \brief division of a scalar to each element of a %DenseVector3dField3d
  ** \param M1 the %DenseVector3dField3d 
  ** \param val the scalar
  ** \return resulting %DenseVector3dField3d
  */
  template<typename T>
  DenseVector3dField3d<T> operator/(const DenseVector3dField3d<T>& M1, 
				    const T& val);


 /*!
  ** \brief pointwise addition of two %DenseVector3dField3d
  ** \param M1 first %DenseVector3dField3d 
  ** \param M2 seconf %DenseVector3dField3d
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %DenseVector3dField3d
  */
template<typename T>
DenseVector3dField3d<T> operator+(const DenseVector3dField3d<T>& M1, 
				  const DenseVector3dField3d<T>& M2);

/*!
  ** \brief addition of a %Vector3d to each element of a %DenseVector3dField3d
  ** \param M1 the %DenseVector3dField3d 
  ** \param val the %Vector3d
  ** \return resulting %DenseVector3dField3d
  */
template<typename T>
DenseVector3dField3d<T> operator+(const DenseVector3dField3d<T>& M1, 
				  const slip::Vector3d<T>& val);

 /*!
  ** \brief addition of a %Vector3d to each element of a %DenseVector3dField3d
  ** \param val the %Vector3d
  ** \param M1 the %DenseVector3dField3d  
  ** \return resulting %DenseVector3dField3d
  */
  template<typename T>
  DenseVector3dField3d<T> operator+(const slip::Vector3d<T>& val, 
				    const DenseVector3dField3d<T>& M1);
  

 
/*!
  ** \brief pointwise substraction of two %DenseVector3dField3d
  ** \param M1 first %DenseVector3dField3d 
  ** \param M2 seconf %DenseVector3dField3d
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %DenseVector3dField3d
  */
template<typename T>
DenseVector3dField3d<T> operator-(const DenseVector3dField3d<T>& M1, 
				  const DenseVector3dField3d<T>& M2);

 
  /*!
  ** \brief substraction of a %Vector3d to each element of a %DenseVector3dField3d
  ** \param M1 the %DenseVector3dField3d 
  ** \param val the %Vector3d
  ** \return resulting %DenseVector3dField3d
  */
template<typename T>
DenseVector3dField3d<T> operator-(const DenseVector3dField3d<T>& M1, 
				  const slip::Vector3d<T>& val);

 /*!
  ** \brief substraction of a %Vector3d to each element of a %DenseVector3dField3d
  ** \param val the %Vector3d
  ** \param M1 the %DenseVector3dField3d  
  ** \return resulting %DenseVector3dField3d
  */
  template<typename T>
  DenseVector3dField3d<T> operator-(const slip::Vector3d<T>& val, 
				    const DenseVector3dField3d<T>& M1);
  

  
/*!
  ** \brief pointwise multiplication of two %DenseVector3dField3d
  ** \param M1 first %DenseVector3dField3d 
  ** \param M2 seconf %DenseVector3dField3d
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %DenseVector3dField3d
  */
  template<typename T>
  DenseVector3dField3d<T> operator*(const DenseVector3dField3d<T>& M1, 
		      const DenseVector3dField3d<T>& M2);


   /*!
  ** \brief multiplication of a %Vector3d to each element of a %DenseVector3dField3d
  ** \param M1 the %DenseVector3dField3d 
  ** \param val the %Vector3d
  ** \return resulting %DenseVector3dField3d
  */
template<typename T>
DenseVector3dField3d<T> operator*(const DenseVector3dField3d<T>& M1, 
				  const slip::Vector3d<T>& val);

 /*!
  ** \brief multiplication of a %Vector3d to each element of a %DenseVector3dField3d
  ** \param val the %Vector3d
  ** \param M1 the %DenseVector3dField3d  
  ** \return resulting %DenseVector3dField3d
  */
  template<typename T>
  DenseVector3dField3d<T> operator*(const slip::Vector3d<T>& val, 
				    const DenseVector3dField3d<T>& M1);
  


  /*!
  ** \brief pointwise division of two %DenseVector3dField3d
  ** \param M1 first %DenseVector3dField3d 
  ** \param M2 seconf %DenseVector3dField3d
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %DenseVector3dField3d
  */
  template<typename T>
  DenseVector3dField3d<T> operator/(const DenseVector3dField3d<T>& M1, 
		      const DenseVector3dField3d<T>& M2);

    /*!
  ** \brief division of a %Vector3d to each element of a %DenseVector3dField3d
  ** \param M1 the %DenseVector3dField3d 
  ** \param val the %Vector3d
  ** \return resulting %DenseVector3dField3d
  */
template<typename T>
DenseVector3dField3d<T> operator/(const DenseVector3dField3d<T>& M1, 
				  const slip::Vector3d<T>& val);

 
/* @} */

}//slip::

namespace slip
{



  template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator=(const slip::Vector3d<T>& val)
  {
    std::fill_n(this->begin(),this->size(),val);
    return *this;
  }
  
 
 template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator=(const T& val)
  {
    std::fill_n(this->begin(),this->size(),val);
    return *this;
  }


  template<typename T>
  inline
  T& DenseVector3dField3d<T>::Vx1(const typename DenseVector3dField3d::size_type k,
				  const typename DenseVector3dField3d::size_type i,
				  const typename DenseVector3dField3d::size_type j)
  {
    return (*this)[k][i][j][0];
  }

  template<typename T>
  inline
  const T& DenseVector3dField3d<T>::Vx1(const typename DenseVector3dField3d::size_type k,
					const typename DenseVector3dField3d::size_type i,
					const typename DenseVector3dField3d::size_type j) const
  {
    return (*this)[k][i][j][0];
  }
 template<typename T>
  inline
  T& DenseVector3dField3d<T>::u(const typename DenseVector3dField3d::size_type k,
				  const typename DenseVector3dField3d::size_type i,
				  const typename DenseVector3dField3d::size_type j)
  {
    return this->Vx1(k,i,j);
  }

  template<typename T>
  inline
  const T& DenseVector3dField3d<T>::u(const typename DenseVector3dField3d::size_type k,
					const typename DenseVector3dField3d::size_type i,
					const typename DenseVector3dField3d::size_type j) const
  {
    return this->Vx1(k,i,j);
  }

   template<typename T>
  inline
  T& DenseVector3dField3d<T>::Vx2(const typename DenseVector3dField3d::size_type k,
				  const typename DenseVector3dField3d::size_type i,
				  const typename DenseVector3dField3d::size_type j)
  {
    return (*this)[k][i][j][1];
  }


  template<typename T>
  inline
  const T& DenseVector3dField3d<T>::Vx2(const typename DenseVector3dField3d::size_type k,
					const typename DenseVector3dField3d::size_type i,
					const typename DenseVector3dField3d::size_type j) const
  {
    return (*this)[k][i][j][1];
  }

 template<typename T>
 inline
  T& DenseVector3dField3d<T>::v(const typename DenseVector3dField3d::size_type k,
				  const typename DenseVector3dField3d::size_type i,
				  const typename DenseVector3dField3d::size_type j)
  {
    return this->Vx2(k,i,j);
  }


  template<typename T>
  inline
  const T& DenseVector3dField3d<T>::v(const typename DenseVector3dField3d::size_type k,
					const typename DenseVector3dField3d::size_type i,
					const typename DenseVector3dField3d::size_type j) const
  {
    return this->Vx2(k,i,j);
  }

  template<typename T>
  inline
  T& DenseVector3dField3d<T>::Vx3(const typename DenseVector3dField3d::size_type k,
				  const typename DenseVector3dField3d::size_type i,
				  const typename DenseVector3dField3d::size_type j)
  {
    return (*this)[k][i][j][2];
  }

   template<typename T>
  inline
  const T& DenseVector3dField3d<T>::Vx3(const typename DenseVector3dField3d::size_type k,
					const typename DenseVector3dField3d::size_type i,
					const typename DenseVector3dField3d::size_type j) const
  {
    return (*this)[k][i][j][2];
  } 
 template<typename T>
  inline
  T& DenseVector3dField3d<T>::w(const typename DenseVector3dField3d::size_type k,
				  const typename DenseVector3dField3d::size_type i,
				  const typename DenseVector3dField3d::size_type j)
  {
    return this->Vx3(k,i,j);
  }

   template<typename T>
  inline
  const T& DenseVector3dField3d<T>::w(const typename DenseVector3dField3d::size_type k,
					const typename DenseVector3dField3d::size_type i,
					const typename DenseVector3dField3d::size_type j) const
  {
    return this->Vx3(k,i,j);
  } 
 template<typename T>
  inline
  typename DenseVector3dField3d<T>::norm_type 
  DenseVector3dField3d<T>::norm(const typename DenseVector3dField3d::size_type k,
				const typename DenseVector3dField3d::size_type i,
				const typename DenseVector3dField3d::size_type j) const
  {
    return (*this)[k][i][j].Euclidean_norm();
  }
  
 
  template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator+=(const T&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::plus<slip::Vector3d<T> >(),val));
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator-=(const T&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::minus<slip::Vector3d<T> >(),val));
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator*=(const T&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::multiplies<slip::Vector3d<T> >(),val));
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator/=(const T&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::divides<slip::Vector3d<T> >(),val));
    return *this;
  }



 template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator+=(const slip::Vector3d<T>&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::plus<slip::Vector3d<T> >(),val));
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator-=(const slip::Vector3d<T>&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::minus<slip::Vector3d<T> >(),val));
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator*=(const slip::Vector3d<T>&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::multiplies<slip::Vector3d<T> >(),val));
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator/=(const slip::Vector3d<T>&val)
  {
     std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::divides<slip::Vector3d<T> >(),val));
    return *this;
  }
  

  
  template<typename T>
  inline
  DenseVector3dField3d<T> DenseVector3dField3d<T>::operator-() const
  {
    DenseVector3dField3d<T> tmp(*this);
    std::transform(this->begin(),this->end(),tmp.begin(),std::negate<slip::Vector3d<T> >());
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator+=(const DenseVector3dField3d<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2()); 
    assert(this->dim3() == rhs.dim3()); 
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::plus<typename DenseVector3dField3d::value_type>());
    return *this;
  }

   template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator-=(const DenseVector3dField3d<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    assert(this->dim3() == rhs.dim3()); 
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::minus<typename DenseVector3dField3d::value_type>());
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator*=(const DenseVector3dField3d<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    assert(this->dim3() == rhs.dim3()); 
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::multiplies<typename DenseVector3dField3d::value_type>());
    return *this;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T>& DenseVector3dField3d<T>::operator/=(const DenseVector3dField3d<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    assert(this->dim3() == rhs.dim3()); 
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::divides<typename DenseVector3dField3d::value_type>());
    return *this;
  }

  template<typename T>
  inline
  std::string 
  DenseVector3dField3d<T>::name() const {return "DenseVector3dField3d";} 
  

 //  template<typename T>
//   inline
//   void DenseVector3dField3d<T>::write_gnuplot(const std::string& file_path_name)
//   {
//     slip::write_gnuplot_vect3d<DenseVector3dField3d<T> >(*this,file_path_name);
//   }

  template<typename T>
  inline
  void DenseVector3dField3d<T>::write_tecplot(const std::string& file_path_name,
					      const std::string& title,
					      const std::string& zone)
  {
    slip::write_tecplot_vect3d<DenseVector3dField3d<T> >(*this,file_path_name,
							 title,zone);
 
  }

  //  template<typename T>
//   inline
//   void DenseVector3dField3d<T>::read_gnuplot(const std::string& file_path_name)
//   {
//     slip::read_gnuplot_vect3d<DenseVector3dField3d<T> >(file_path_name,*this);
//   }

  template<typename T>
  inline
  void DenseVector3dField3d<T>::read_tecplot(const std::string& file_path_name)
  {
    slip::read_tecplot_vect3d<DenseVector3dField3d<T> >(file_path_name,*this);
  }


  template<typename T>
  inline
  DenseVector3dField3d<T> operator+(const DenseVector3dField3d<T>& M1, 
				    const T& val)
  {
    DenseVector3dField3d<T>  tmp = M1;
    tmp += val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T> operator+(const T& val,
				    const DenseVector3dField3d<T>& M1)
  {
    return M1 + val;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T> operator-(const DenseVector3dField3d<T>& M1, 
				    const T& val)
  {
    DenseVector3dField3d<T>  tmp = M1;
    tmp -= val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T> operator-(const T& val,
				    const DenseVector3dField3d<T>& M1)
  {
    return -(M1 - val);
  }


  template<typename T>
  inline
  DenseVector3dField3d<T> operator*(const DenseVector3dField3d<T>& M1, 
				    const T& val)
  {
    DenseVector3dField3d<T>  tmp = M1;
    tmp *= val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T> operator*(const T& val,
				    const DenseVector3dField3d<T>& M1)
  {
    return M1 * val;
  }


  template<typename T>
  inline
  DenseVector3dField3d<T> operator/(const DenseVector3dField3d<T>& M1, 
				    const T& val)
  {
    DenseVector3dField3d<T>  tmp = M1;
    tmp /= val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T> operator+(const DenseVector3dField3d<T>& M1, 
				    const DenseVector3dField3d<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    assert(M1.dim3() == M2.dim3());
    DenseVector3dField3d<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::plus<slip::Vector3d<T> >());
    return tmp;
  }


  template<typename T>
  inline
  DenseVector3dField3d<T> operator+(const DenseVector3dField3d<T>& M1, 
			      const slip::Vector3d<T>&val)
  {
    DenseVector3dField3d<T>  tmp = M1;
    tmp += val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T> operator+(const slip::Vector3d<T>&val,
			      const DenseVector3dField3d<T>& M1)
  {
    return M1 + val;
  }



 template<typename T>
  inline
  DenseVector3dField3d<T> operator-(const DenseVector3dField3d<T>& M1, 
				    const DenseVector3dField3d<T>& M2)
  {
     assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    assert(M1.dim3() == M2.dim3());
    DenseVector3dField3d<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::minus<slip::Vector3d<T> >());
    return tmp;
  }


  template<typename T>
  inline
  DenseVector3dField3d<T> operator-(const DenseVector3dField3d<T>& M1, 
			      const slip::Vector3d<T>&val)
  {
    DenseVector3dField3d<T>  tmp = M1;
    tmp -= val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T> operator-(const slip::Vector3d<T>&val,
			      const DenseVector3dField3d<T>& M1)
  {
    return -(M1 - val);
  }


  template<typename T>
  inline
  DenseVector3dField3d<T> operator*(const DenseVector3dField3d<T>& M1, 
				    const DenseVector3dField3d<T>& M2)
  {
     assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    assert(M1.dim3() == M2.dim3());
    DenseVector3dField3d<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::multiplies<slip::Vector3d<T> >());
    return tmp;
  }


  template<typename T>
  inline
  DenseVector3dField3d<T> operator*(const DenseVector3dField3d<T>& M1, 
				    const slip::Vector3d<T>&val)
  {
    DenseVector3dField3d<T>  tmp = M1;
    tmp *= val;
    return tmp;
  }

  template<typename T>
  inline
  DenseVector3dField3d<T> operator*(const slip::Vector3d<T>&val,
				    const DenseVector3dField3d<T>& M1)
  {
    return M1 * val;
  }


 template<typename T>
  inline
  DenseVector3dField3d<T> operator/(const DenseVector3dField3d<T>& M1, 
				    const DenseVector3dField3d<T>& M2)
  {
     assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    assert(M1.dim3() == M2.dim3());
    DenseVector3dField3d<T> tmp(M1.dim1(),M1.dim2(),M1.dim3());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::divides<slip::Vector3d<T> >());
    return tmp;
  }


  template<typename T>
  inline
  DenseVector3dField3d<T> operator/(const DenseVector3dField3d<T>& M1, 
				    const slip::Vector3d<T>&val)
  {
    DenseVector3dField3d<T>  tmp = M1;
    tmp /= val;
    return tmp;
  }




}//slip::

#endif //SLIP_DENSEVECTOR3DFIELD3D_HPP
