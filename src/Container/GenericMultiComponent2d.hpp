/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file GenericMultiComponent2d.hpp
 * 
 * \brief Provides a class to manipulate multicomponent 2d containers.
 * 
 */
#ifndef SLIP_GENERICMULTICOMPONENT2D_HPP
#define SLIP_GENERICMULTICOMPONENT2D_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <cmath>
#include <string>
#include <vector>
#include <cstddef>
#include "Matrix.hpp"
#include "stride_iterator.hpp"
#include "kstride_iterator.hpp"
#include "iterator2d_box.hpp"
#include "iterator2d_range.hpp"
#include "component_iterator2d_box.hpp"
#include "component_iterator2d_range.hpp"
#include "apply.hpp"
#include "Range.hpp"
#include "Box2d.hpp"
#include "Point2d.hpp"
#include "DPoint2d.hpp"
#include "compare.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template<class Block>
class stride_iterator;

template<class Block>
class iterator2d_box;

template<class T>
class const_iterator2d_box;

template<class T>
class const_iterator2d_range;

template <class Block>
class DPoint2d;

template <class Block>
class Point2d;

template <class Block>
class Box2d;

template <typename Block>
class GenericMultiComponent2d;

template <typename Block>
class Matrix;

template <typename Block>
std::ostream& operator<<(std::ostream & out, const slip::GenericMultiComponent2d<Block>& a);

template<typename Block>
bool operator==(const slip::GenericMultiComponent2d<Block>& x, 
		const slip::GenericMultiComponent2d<Block>& y);

template<typename Block>
bool operator!=(const slip::GenericMultiComponent2d<Block>& x, 
		const slip::GenericMultiComponent2d<Block>& y);

// template<typename Block>
// bool operator<(const slip::GenericMultiComponent2d<Block>& x, 
// 	       const slip::GenericMultiComponent2d<Block>& y);

// template<typename sBlock>
// bool operator>(const slip::GenericMultiComponent2d<Block>& x, 
// 	       const slip::GenericMultiComponent2d<Block>& y);

// template<typename Block>
// bool operator<=(const slip::GenericMultiComponent2d<Block>& x, 
// 		const slip::GenericMultiComponent2d<Block>& y);

// template<typename Block>
// bool operator>=(const slip::GenericMultiComponent2d<Block>& x, 
// 		const slip::GenericMultiComponent2d<Block>& y);

/*!
 *  @defgroup MultiComponent2dContainers Multi-component 2d Containers
 *  @brief 2d multicomponents containers having the same interface
 *  @{
 */
/*! \class GenericMultiComponent2d
**  \ingroup Containers Containers2d MultiComponent2dContainers 
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \author Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>
** \version 0.0.1
** \date 2007/02/24
** \since 1.0.0
** \brief This is a %GenericMultiComponent2d class.
** This container statisfies the BidirectionnalContainer concepts of the STL.
** It is also an 2d extension of the RandomAccessContainer concept. That is
** to say the bracket element access is replaced by the double bracket element 
** access.
** Data are stored using a %Matrix of Block. It is essentially an internal
** class used to define slip::ColorImage, slip::MultispectralImage, 
** slip::DenseVector2dField2d, slip::DenseVector3dField2d. 
** But it can be used to define other 2d Multicomponent structures.

** \param Block Type of object in the GenericMultiComponent2d 
**        Must be a static block
** \par Axis conventions:
** \image html iterator2d_conventions.jpg "axis and notation conventions"
** \image latex iterator2d_conventions.eps "axis and notation conventions" width=5cm
**
*/
template <typename Block>
class GenericMultiComponent2d
{
public :

  typedef Block value_type;
  typedef GenericMultiComponent2d<Block> self;
  typedef const GenericMultiComponent2d<Block> const_self;
  typedef value_type* pointer;
  typedef value_type const * const_pointer;
  typedef value_type& reference;
  typedef value_type const & const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;
  
  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;


  typedef pointer row_iterator;
  typedef const_pointer const_row_iterator;
  typedef slip::stride_iterator<pointer> col_iterator;
  typedef slip::stride_iterator<const_pointer> const_col_iterator;

 
  typedef typename slip::Matrix<Block>::iterator2d iterator2d;
  typedef typename slip::Matrix<Block>::const_iterator2d const_iterator2d;
  typedef slip::stride_iterator<pointer> row_range_iterator;
  typedef slip::stride_iterator<const_pointer> const_row_range_iterator;
  typedef slip::stride_iterator<col_iterator> col_range_iterator;
  typedef slip::stride_iterator<const_col_iterator> const_col_range_iterator;
  typedef typename slip::Matrix<Block>::iterator2d_range iterator2d_range;
  typedef typename slip::Matrix<Block>::const_iterator2d_range const_iterator2d_range;

  typedef typename Block::value_type block_value_type;
  typedef block_value_type* block_pointer;
  typedef const block_value_type* const_block_pointer;
  typedef block_value_type& block_reference;
  typedef const block_value_type const_block_reference;
  typedef slip::kstride_iterator<typename Block::pointer,Block::SIZE> component_iterator;
  typedef slip::kstride_iterator<typename Block::const_pointer,Block::SIZE> const_component_iterator;

  typedef std::reverse_iterator<iterator> reverse_row_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
  typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
  typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;
  typedef std::reverse_iterator<iterator2d> reverse_iterator2d;
  typedef std::reverse_iterator<const_iterator2d> const_reverse_iterator2d;
  typedef std::reverse_iterator<row_range_iterator> reverse_row_range_iterator;
  typedef std::reverse_iterator<const_row_range_iterator> const_reverse_row_range_iterator;
  typedef std::reverse_iterator<col_range_iterator> reverse_col_range_iterator;
  typedef std::reverse_iterator<const_col_range_iterator> const_reverse_col_range_iterator;
  typedef std::reverse_iterator<iterator2d_range> reverse_iterator2d_range;
  typedef std::reverse_iterator<const_iterator2d_range> const_reverse_iterator2d_range;
  typedef std::reverse_iterator<component_iterator> reverse_component_iterator;
  typedef std::reverse_iterator<const_component_iterator> const_reverse_component_iterator;

  typedef component_iterator component_row_iterator;
  typedef const_component_iterator const_component_row_iterator;
  typedef slip::stride_iterator<block_pointer> component_row_range_iterator;
  typedef slip::stride_iterator<const_block_pointer> const_component_row_range_iterator;
  typedef std::reverse_iterator<component_row_iterator> reverse_component_row_iterator;
  typedef std::reverse_iterator<const_component_row_iterator> const_reverse_component_row_iterator;
  typedef std::reverse_iterator<component_row_range_iterator> reverse_component_row_range_iterator;
  typedef std::reverse_iterator<const_component_row_range_iterator> const_reverse_component_row_range_iterator;

  typedef slip::stride_iterator<block_pointer> component_col_iterator;
  typedef slip::stride_iterator<const_block_pointer> const_component_col_iterator;
  typedef slip::stride_iterator<block_pointer> component_col_range_iterator;
  typedef slip::stride_iterator<const_block_pointer> const_component_col_range_iterator;
  typedef std::reverse_iterator<component_col_iterator> reverse_component_col_iterator;
  typedef std::reverse_iterator<const_component_col_iterator> const_reverse_component_col_iterator;
  typedef std::reverse_iterator<component_col_range_iterator> reverse_component_col_range_iterator;
  typedef std::reverse_iterator<const_component_col_range_iterator> const_reverse_component_col_range_iterator;

  typedef slip::component_iterator2d_box<self,Block::SIZE> component_iterator2d;
  typedef std::reverse_iterator<component_iterator2d> reverse_component_iterator2d;
  typedef slip::const_component_iterator2d_box<const_self,Block::SIZE> const_component_iterator2d;
  typedef std::reverse_iterator<const_component_iterator2d> const_reverse_component_iterator2d;

  typedef slip::component_iterator2d_range<self,Block::SIZE> component_iterator2d_range;
  typedef std::reverse_iterator<component_iterator2d_range> reverse_component_iterator2d_range;
  typedef slip::const_component_iterator2d_range<const_self,Block::SIZE> const_component_iterator2d_range;
  typedef std::reverse_iterator<const_component_iterator2d_range> const_reverse_component_iterator2d_range;
  
  //default iterator of the container
  typedef iterator2d default_iterator;
  typedef const_iterator2d const_default_iterator;
  
 static const std::size_t DIM = 2;
 static const std::size_t COMPONENTS = Block::SIZE;
public:
 /**
  ** \name Constructors & Destructors
  */
 /*@{*/

  /*!
  ** \brief Constructs a %GenericMultiComponent2d.
  */
  GenericMultiComponent2d();
 
  /*!
  ** \brief Constructs a %GenericMultiComponent2d.
  ** \param height first dimension of the %GenericMultiComponent2d
  ** \param width second dimension of the %GenericMultiComponent2d
  */
  GenericMultiComponent2d(const size_type height,
			  const size_type width);
  
  /*!
  ** \brief Constructs a %GenericMultiComponent2d initialized by the scalar value \a val.
  ** \param height first dimension of the %GenericMultiComponent2d
  ** \param width second dimension of the %GenericMultiComponent2d
  ** \param val initialization value of the elements 
  */
  GenericMultiComponent2d(const size_type height,
			  const size_type width, 
			  const Block& val);
 
  /*!
  ** \brief Constructs a %GenericMultiComponent2d initialized by an array \a val.
  ** \param height first dimension of the %GenericMultiComponent2d
  ** \param width second dimension of the %GenericMultiComponent2d
  ** \param val initialization linear array value of the elements 
  */
  GenericMultiComponent2d(const size_type height,
			  const size_type width, 
			  typename Block::const_pointer val);
 
  /*!
  ** \brief Constructs a %GenericMultiComponent2d initialized by an array \a val.
  ** \param height first dimension of the %GenericMultiComponent2d
  ** \param width second dimension of the %GenericMultiComponent2d
  ** \param val initialization array value of the elements 
  */
  GenericMultiComponent2d(const size_type height,
			  const size_type width, 
			  const Block* val);
 

  /**
   ** \brief  Contructs a %GenericMultiComponent2d from a range.
   ** \param height first dimension of the %GenericMultiComponent2d
   ** \param width second dimension of the %GenericMultiComponent2d
   ** \param  first  An input iterator.
   ** \param  last  An input iterator.
   **
   ** Create a %GenericMultiComponent2d consisting of copies of the elements from
   ** [first,last).
   */
  template<typename InputIterator>
  GenericMultiComponent2d(const size_type height,
			  const size_type width, 
			  InputIterator first,
			  InputIterator last)
  {
    matrix_ = new slip::Matrix<Block>(height,width,first,last);
  } 

 /**
  ** \brief  Contructs a %GenericMultiComponent2d from a 3 ranges.
  ** \param height first dimension of the %GenericMultiComponent2d
  ** \param width second dimension of the %GenericMultiComponent2d
  ** \param  first_iterators_list  A std::vector of the first input iterators.
  ** \param  last  An input iterator.
  **
  **
  ** Create a %GenericMultiComponent2d consisting of copies of the elements from
  ** [first_iterators_list[0],last), 
  ** [first_iterators_list[1],first_iterators_list[1] + (last - first_iterators_list[0]), 
  ** ...
  */
  template<typename InputIterator>
  GenericMultiComponent2d(const size_type height,
			  const size_type width, 
			  std::vector<InputIterator> first_iterators_list,
			  InputIterator last)

  {
    matrix_ = new slip::Matrix<Block>(height,width);
    this->fill(first_iterators_list,last);
  } 


  /*!
  ** \brief Constructs a copy of the %GenericMultiComponent2d \a rhs
  */
  GenericMultiComponent2d(const self& rhs);

  /*!
  ** \brief Destructor of the %GenericMultiComponent2d
  */
  virtual ~GenericMultiComponent2d();
  

  /*@} End Constructors */


  /*!
  ** \brief Resizes a GenericMultiComponent2d.
  ** \param height new first dimension
  ** \param width new second dimension
  ** \param val new value for all the elements
  */ 
  void resize(const size_type height,
	      const size_type width,
	      const Block& val = Block());

  /**
   ** \name iterators
   */
   /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \return begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > S(10,9);
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<slip::Vector2d<double> >());
  ** \endcode
  */
  iterator begin();


  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \return end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > S(10,9);
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<slip::Vector2d<double> >());
  ** \endcode
  */
  iterator end();

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** 
  ** \return const begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > S(10,9);
  ** //copy the sum of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<slip::Vector2d<double> >());
  ** \endcode
  */
  const_iterator begin() const;

  
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %GenericMultiComponent2d.  Iteration is done in
  **  ordinary element order.
  ** \return const end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > S(10,9);
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<slip::Vector2d<double> >());
  ** \endcode
  */
  const_iterator end() const;
  
  /*!
  **  \brief Returns a read/write reverse iterator that points to the
  **  last element in the %GenericMultiComponent2d.  Iteration is done in reverse
  **  element order.
  ** \return reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > S(10,9);
  ** //copy the sum of the elements of A1 and A2 
  ** //in S (iterated in the reverse order element)
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.rbegin(),
  **                std::plus<slip::Vector2d<double> >());
  ** \endcode
  */
  reverse_iterator rbegin();
  
  /*!
  **  \brief Returns a read/write reverse iterator that points to one
  **  before the first element in the %GenericMultiComponent2d.  Iteration is done
  **  in reverse element order.
  **  \return reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > S(10,9);
  ** //copy the sum of the elements of A1 and A2 
  ** //in S (iterated in the reverse order element)
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.rbegin(),
  **                std::plus<slip::Vector2d<double> >());
  ** \endcode
  */
  reverse_iterator rend();
  
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to the last element in the %GenericMultiComponent2d.  Iteration is done in
  **  reverse element order.
  ** \return const reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > S(10,9);
  ** //copy the sum of the elements of A1 (iterated in the reverse order element)
  ** //and A2 in S 
  ** std::transform(A1.rbegin(),A1.rend(),
  **                A2.begin(),S.begin(),
  **                std::plus<slip::Vector2d<double> >());
  ** \endcode
  */
  const_reverse_iterator rbegin() const;

  
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to one before the first element in the %GenericMultiComponent2d.  Iteration
  **  is done in reverse element order.
  **  \return const reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > S(10,9);
  ** //copy the sum of the elements of A1 (iterated in the reverse order element)
  ** //and A2 in S 
  ** std::transform(A1.rbegin(),A1.rend(),
  **                A2.begin(),S.begin(),
  **                std::plus<slip::Vector2d<double> >());
  ** \endcode
  */
  const_reverse_iterator rend() const;

 
 /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  row_iterator row_begin(const size_type row);

  
  /*!
  **  \brief Returns a read/write iterator that points one past
  **  the end element of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order.
  ** \param row  The index of the row to iterate.
  ** \return begin row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  row_iterator row_end(const size_type row);


  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  const_row_iterator row_begin(const size_type row) const;


  /*!
  **  \brief Returns a read-only iterator that points one past
  **  the end element of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  const_row_iterator row_end(const size_type row) const;
  
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the column \a column in the %GenericMultiComponent2d.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  col_iterator col_begin(const size_type col);


  /*!
  **  \brief Returns a read/write iterator that points one past
  **  the end element of the column \a column in the %GenericMultiComponent2d.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  col_iterator col_end(const size_type col);

  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the column \a column in the %GenericMultiComponent2d.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin const_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  const_col_iterator col_begin(const size_type col) const;



  /*!
  **  \brief Returns a read-only iterator that points one past
  **  the end element of the column \a column in the %GenericMultiComponent2d.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column 
  ** \return begin const_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(10,9);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  const_col_iterator col_end(const size_type col) const;


  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the %Range \a range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return begin row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  row_range_iterator row_begin(const size_type row,
			       const slip::Range<int>& range);



  /*!
  **  \brief Returns a read/write iterator that points one past the end
  **  element of the %Range \a range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return end row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  row_range_iterator row_end(const size_type row,
			     const slip::Range<int>& range);


  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return begin const_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  const_row_range_iterator row_begin(const size_type row,
				     const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read_only iterator that points one past the last
  **  element of the %Range range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row Row to iterate.
  ** \param range %Range of the row to iterate
  ** \return begin const_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  const_row_range_iterator row_end(const size_type row,
				   const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read-write iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate
  ** \return begin col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  col_range_iterator col_begin(const size_type col,
			       const slip::Range<int>& range);

  /*!
  **  \brief Returns a read-write iterator that points to the past
  **  the end element of the %Range \a range of the col \a col in the 
  **  %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  col_range_iterator col_end(const size_type col,
			     const slip::Range<int>& range);




  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin const_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  const_col_range_iterator col_begin(const size_type col,
				     const slip::Range<int>& range) const;

 
  /*!
  **  \brief Returns a read-only iterator that points to the past
  **  the end element of the %Range \a range of the col \a col in 
  **  the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate
  ** \return begin const_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  const_col_range_iterator col_end(const size_type col,
				   const slip::Range<int>& range) const;

  

  /*!
  **  \brief Returns a read/write reverse iterator that points to the 
  **  last element of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return begin reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  */
  reverse_row_iterator row_rbegin(const size_type row);


  /*!
  **  \brief Returns a read/write reverse iterator that points one past
  **  the first element of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return end reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  */
  reverse_row_iterator row_rend(const size_type row);

  /*!
  **  \brief Returns a read-only reverse iterator that points to the last
  **  element of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  */
  const_reverse_row_iterator row_rbegin(const size_type row) const;

  /*!
  **  \brief Returns a read-only reverse iterator that points one past
  **  the first element of the row \a row in the %GenericMultiComponent2d. 
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return end const_reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  */
  const_reverse_row_iterator row_rend(const size_type row) const;

  /*!
  **  \brief Returns a read/write reverse iterator that points to the last
  **  element of the column \a column in the %GenericMultiComponent2d.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return begin col_reverse_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  */
  reverse_col_iterator col_rbegin(const size_type col);

  
  /*!
  **  \brief Returns a read/write reverse iterator that points one past
  **  the first element of the column \a column in the %GenericMultiComponent2d.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return end reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  */
  reverse_col_iterator col_rend(const size_type col);

  /*!
  **  \brief Returns a read-only reverse iterator that points to the last
  **  element of the column \a column in the %GenericMultiComponent2d.  
  **  Iteration is done modulo the number of columns and in the reverse 
  ** element order.
  ** \param col The index of the column to iterate.
  ** \return begin const_reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  */
  const_reverse_col_iterator col_rbegin(const size_type col) const;

    
  /*!
  **  \brief Returns a read-only reverse iterator that points one past
  **  the first element of the column \a column in the %GenericMultiComponent2d.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return end const_reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  */
  const_reverse_col_iterator col_rend(const size_type col) const;
 

  /*!
  **  \brief Returns a read-write iterator that points to the last
  **  element of the %Range \a range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  reverse_row_range_iterator row_rbegin(const size_type row,
					const slip::Range<int>& range);

 
  /*!
  **  \brief Returns a read-write iterator that points one before
  **  the first element of the %Range \a range of the row \a row in the 
  **  %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  reverse_row_range_iterator row_rend(const size_type row,
				      const slip::Range<int>& range);



  /*!
  **  \brief Returns a read-only iterator that points to the last
  **  element of the %Range \a range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate
  ** \return begin const_reverse_row_range_iterator value
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  const_reverse_row_range_iterator row_rbegin(const size_type row,
					      const slip::Range<int>& range) const;


   /*!
  **  \brief Returns a read-only iterator that points one before the first
  **  element of the %Range \a range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate
  ** \return const_reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  const_reverse_row_range_iterator row_rend(const size_type row,
					    const slip::Range<int>& range) const;
 
 


  /*!
  **  \brief Returns a read-write iterator that points to the last
  **  element of the %Range \a range of the col \a col in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  reverse_col_range_iterator col_rbegin(const size_type col,
					const slip::Range<int>& range);

  /*!
  **  \brief Returns a read-write iterator that points to one before 
  **  the first element of the %Range range of the col \a col in the 
  **  %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  reverse_col_range_iterator col_rend(const size_type col,
				      const slip::Range<int>& range);


  /*!
  **  \brief Returns a read_only iterator that points to the last
  **  element of the %Range \& range of the col \a col in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin const_reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  const_reverse_col_range_iterator 
  col_rbegin(const size_type col,
	     const slip::Range<int>& range) const;

 
  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return const_reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  const_reverse_col_range_iterator col_rend(const size_type col,
					    const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read/write iterator2d that points to the first
  **  element of the %GenericMultiComponent2d. It points to the upper left element of
  **  the %GenericMultiComponent2d.
  **  
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  iterator2d upper_left();
  
  
  /*!
  **  \brief Returns a read/write iterator2d that points to the past
  **  the end element of the %GenericMultiComponent2d. It points to past the end element of
  **  the bottom right element of the %GenericMultiComponent2d.
  **  
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  iterator2d bottom_right();


  /*!
  **  \brief Returns a read-only iterator2d that points to the first
  **  element of the %GenericMultiComponent2d. It points to the upper left element of
  **  the %GenericMultiComponent2d.
  **  
  ** \return begin const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  const_iterator2d upper_left() const;
  
 
  /*!
  **  \brief Returns a read-only iterator2d that points to the past
  **  the end element of the %GenericMultiComponent2d. It points to past the end element of
  **  the bottom right element of the %GenericMultiComponent2d.
  **  
  ** \return begin const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  const_iterator2d bottom_right() const;

  /*!
  **  \brief Returns a read/write iterator2d that points to the first
  **  element of the %GenericMultiComponent2d. It points to the upper left element of
  **  the \a %Box2d associated to the %GenericMultiComponent2d.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **  
  ** \return end iterator2d value
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
 */
  iterator2d upper_left(const Box2d<int>& box);

 
  /*!
  **  \brief Returns a read/write iterator2d that points to the past
  **  the end element of the %GenericMultiComponent2d. It points to past the end element of
  **  the bottom right element of the \a %Box2d associated to the %GenericMultiComponent2d.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  **
  ** \return end iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  **  
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  iterator2d bottom_right(const Box2d<int>& box);


  /*!
  **  \brief Returns a read only iterator2d that points to the first
  **  element of the %GenericMultiComponent2d. It points to the upper left element of
  **  the \a %Box2d associated to the %GenericMultiComponent2d.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  ** \return end const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  ** 
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  const_iterator2d upper_left(const Box2d<int>& box) const;

 
  /*!
  **  \brief Returns a read only iterator2d that points to the past
  **  the end element of the %GenericMultiComponent2d. It points to past the end element of
  **  the bottom right element of the \a %Box2d associated to the %GenericMultiComponent2d.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  ** \return end const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  ** 
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  const_iterator2d bottom_right(const Box2d<int>& box) const;


  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **  upper left element of the ranges \a row_range and \a col_range 
  **  associated to the %GenericMultiComponent2d.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range upper_left(const Range<int>& row_range,
			      const Range<int>& col_range);

  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **   past the end bottom right element of the ranges \a row_range 
  **   and \a col_range associated to the %GenericMultiComponent2d.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range bottom_right(const Range<int>& row_range,
				const Range<int>& col_range);


  /*!
  **  \brief Returns a read-only iterator2d_range that points to the
  **   to the upper left element of the ranges \a row_range and \a col_range 
  **   associated to the %GenericMultiComponent2d.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid.
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range upper_left(const Range<int>& row_range,
				    const Range<int>& col_range) const;


 /*!
  **  \brief Returns a read-only iterator2d_range that points to the past
  **  the end bottom right element of the ranges \a row_range and \a col_range 
  **  associated to the %GenericMultiComponent2d.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range bottom_right(const Range<int>& row_range,
				      const Range<int>& col_range) const;


 


  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **  upper left element of the %Range \a range associated to the %GenericMultiComponent2d.
  **  The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range and must be valid. 
  ** \pre The range indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range upper_left(const Range<int>& range);
 

  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **   past the end bottom right element of the %Range \a range 
  **   associated to the %GenericMultiComponent2d.
  **   The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range bottom_right(const Range<int>& range);

 

  
  /*!
  **  \brief Returns a read-only iterator2d_range that points to the
  **   to the upper left element of the %Range \a range
  **   associated to the %GenericMultiComponent2d.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range must be valid.
  ** \pre The range indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range upper_left(const Range<int>& range) const;

 

  /*!
  **  \brief Returns a read-only const_iterator2d_range that points to the 
  **   past the end bottom right element of the %Range \a range 
  **   associated to the %GenericMultiComponent2d.
  **   The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range bottom_right(const Range<int>& range) const;

 
  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to the 
  **   bottom right element of the %GenericMultiComponent2d. 
  *    Iteration is done within the %GenericMultiComponent2d in the reverse order.
  **  
  ** \return reverse_iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rupper_left();
  
  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to past the 
  **  upper left element of the %GenericMultiComponent2d.
  **  Iteration is done in the reverse order.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rbottom_right();

  /*!
  **  \brief Returns a read only reverse iterator2d that points.  It points 
  **  to the bottom right element of the %GenericMultiComponent2d. 
  **  Iteration is done within the %GenericMultiComponent2d in the reverse order.
  **  
  ** \return const_reverse_iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rupper_left() const;
 
 
  /*!
  **  \brief Returns a read only reverse iterator2d. It points to past the 
  **  upper left element of the %GenericMultiComponent2d.
  **  Iteration is done in the reverse order.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rbottom_right() const;
  

  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to the 
  **  bottom right element of the \a %Box2d associated to the %GenericMultiComponent2d.
  **  Iteration is done in the reverse order.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  **
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rupper_left(const Box2d<int>& box);

  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to one
  **  before the upper left element of the %Box2d \a box associated to 
  **  the %GenericMultiComponent2d.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  **
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rbottom_right(const Box2d<int>& box);

  /*!
  **  \brief Returns a read only reverse iterator2d. It points to the 
  **  bottom right element of the %Box2d \a box associated to the %GenericMultiComponent2d.
  **  Iteration is done in the reverse order.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  **
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rupper_left(const Box2d<int>& box) const;


  /*!
  **  \brief Returns a read-only reverse iterator2d. It points to one 
  **  before the element of the bottom right element of the %Box2d 
  **  \a box associated to the %GenericMultiComponent2d.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  **
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rbottom_right(const Box2d<int>& box) const;

 
  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to the 
  **  past the bottom right element of the ranges \a row_range and 
  **  \a col_range associated to the %GenericMultiComponent2d. Iteration is done in the 
  **  reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rupper_left(const Range<int>& row_range,
				       const Range<int>& col_range);


 
  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points
  **  to one before the upper left element of the ranges \a row_range 
  **  and \a col_range associated to the %GenericMultiComponent2d. Iteration is done
  **  in the reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rbottom_right(const Range<int>& row_range,
					 const Range<int>& col_range);


 
  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points 
  **   to the past the bottom right element of the ranges \a row_range and 
  **   \a col_range associated to the %GenericMultiComponent2d. Iteration is done in the 
  **   reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid.
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rupper_left(const Range<int>& row_range,
					     const Range<int>& col_range) const;

  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points 
  **  to one before the upper left element of the ranges \a row_range 
  **  and \a col_range associated to the %GenericMultiComponent2d.Iteration is done in 
  **  the reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rbottom_right(const Range<int>& row_range,
					       const Range<int>& col_range) const;


 
 /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to the 
  **  bottom right element of the %Range \a range associated to the %GenericMultiComponent2d.
  **  The same range is applied for rows and cols. Iteration is done in the
  **  reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range and must be valid. 
  ** \pre The range indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rupper_left(const Range<int>& range);



  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to  
  **   one before the upper left element of the %Range \a range 
  **   associated to the %GenericMultiComponent2d.
  **   The same range is applied for rows and cols. Iteration is done
  **   in the reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rbottom_right(const Range<int>& range);
  

  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points to the
  **   to the bottom right element of the %Range \a range
  **   associated to the %GenericMultiComponent2d.
  **  The same range is applied for rows and cols. Iteration is done in the
  **  reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range must be valid.
  ** \pre The range indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rupper_left(const Range<int>& range) const;


  /*!
  **  \brief Returns a read_only reverse_iterator2d_range that points 
  **   to one before the upper left element of the %Range \a range 
  **   associated to the %GenericMultiComponent2d.
  **   The same range is applied for rows and cols. Iteration is done in the
  **   reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rbottom_right(const Range<int>& range) const;
  
  //--------------------------------------------------------------------

  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \return begin component_iterator value
  */
  component_iterator begin(const std::size_t component);

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \return const begin component_iterator value
  */
  const_component_iterator begin(const std::size_t component) const;

  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \return end component_iterator value
  */
  component_iterator end(const std::size_t component);
 
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %GenericMultiComponent2d.  Iteration is done in
  **  ordinary element order.
  ** \param component The component number.
  ** \return const end component_iterator value
  */
   const_component_iterator end(const std::size_t component) const;
  
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \return begin reverse_component_iterator value
  */
  reverse_component_iterator rbegin(const std::size_t component);

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \return const begin reverse_component_iterator value
  */
  const_reverse_component_iterator rbegin(const std::size_t component) const;

  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \return end reverse_component_iterator value
  */
  reverse_component_iterator rend(const std::size_t component);
 
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %GenericMultiComponent2d.  Iteration is done in
  **  ordinary element order.
  ** \param component The component number.
  ** \return const end reverse_component_iterator value
  */
  const_reverse_component_iterator rend(const std::size_t component) const;
  

  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \param row The row within the component.
  ** \return begin component_row_iterator value.
  */
  component_row_iterator row_begin(const std::size_t component, 
			       const std::size_t row);

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \param row The row within the component.
  ** \return const begin component_row_iterator value
  */
  const_component_row_iterator row_begin(const std::size_t component,
				     const std::size_t row) const;

  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \param row The row within the component.
  ** \return end component_row_iterator value
  */
  component_row_iterator row_end(const std::size_t component,
			     const std::size_t row);
 
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %GenericMultiComponent2d.  Iteration is done in
  **  ordinary element order.
  ** \param component The component number.
  ** \param row The row within the component.
  ** \return const end component_row_iterator value
  */
   const_component_row_iterator row_end(const std::size_t component,
				    const std::size_t row) const;


     /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \param row The row within the component.
  ** \return reverse begin component_row_iterator value.
  */
  reverse_component_row_iterator row_rbegin(const std::size_t component, 
					const std::size_t row);

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \param row The row within the component.
  ** \return const reverse begin component_row_iterator value
  */
  const_reverse_component_row_iterator row_rbegin(const std::size_t component,
					      const std::size_t row) const;

  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \param row The row within the component.
  ** \return end reverse component_row_iterator value
  */
  reverse_component_row_iterator row_rend(const std::size_t component,
				      const std::size_t row);
  
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %GenericMultiComponent2d.  Iteration is done in
  **  ordinary element order.
  ** \param component The component number.
  ** \param row The row within the component.
  ** \return const reverse end component_row_iterator value
  */
  const_reverse_component_row_iterator row_rend(const std::size_t component,
					    const std::size_t row) const;


     /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \param col The col within the component.
  ** \return begin component_col_iterator value.
  */
  component_col_iterator col_begin(const std::size_t component, 
  			       const std::size_t col);

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \param col The col within the component.
  ** \return const begin component_col_iterator value
  */
  const_component_col_iterator col_begin(const std::size_t component,
  				     const std::size_t col) const;

  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %GenericMultiComponent2d.  Iteration is done in ordinary
  **  element order.
  ** \param component The component number.
  ** \param col The col within the component.
  ** \return end component_col_iterator value
  */
  component_col_iterator col_end(const std::size_t component,
  			     const std::size_t col);
 
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %GenericMultiComponent2d.  Iteration is done in
  **  ordinary element order.
  ** \param component The component number.
  ** \param col The col within the component.
  ** \return const end component_col_iterator value
  */
  const_component_col_iterator col_end(const std::size_t component,
  				   const std::size_t col) const;



  /*!
  **  \brief Returns a read/write iterator that points to the  last element of the col \a col 
  **  in the %GenericMultiComponent2d.  Iteration is done in reverse
  **  element order.
  ** \param component The component number.
  ** \param col The col within the component.
  ** \return begin reverse_component_col_iterator value.
  */
  reverse_component_col_iterator col_rbegin(const std::size_t component, 
					const std::size_t col);

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  last element of the col \a col in the %GenericMultiComponent2d.  
  **  Iteration is done in reverse element order.
  ** \param component The component number.
  ** \param col The col within the component.
  ** \return const reverse_component_col_iterator value
  */
  const_reverse_component_col_iterator col_rbegin(const std::size_t component,
					      const std::size_t col) const;

  /*!
  **  \brief Returns a read/write iterator that points one before the first
  **  element of the col \a col in the %GenericMultiComponent2d.  
  ** Iteration is done in the reverse element order.
  ** \param component The component number.
  ** \param col The col within the component.
  ** \return end component_col_iterator value
  */
  reverse_component_col_iterator col_rend(const std::size_t component,
				      const std::size_t col);
 
  /*!
  **  \brief Returns a read-only (constant) iterator that points one before
  **  the first element of the col \a col in the %GenericMultiComponent2d.  
  ** Iteration is done in ordinary element order.
  ** \param component The component number.
  ** \param col The col within the component.
  ** \return const end component_col_iterator value
  */
  const_reverse_component_col_iterator col_rend(const std::size_t component,
					    const std::size_t col) const;

  //##############################################################################"

  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the %Range \a range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return begin component_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the first component of the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(0,1));
  ** \endcode
  */
  component_row_range_iterator row_begin(const std::size_t component, const size_type row,
					 const slip::Range<int>& range);

  /*!
  **  \brief Returns a read/write iterator that points one past the end
  **  element of the %Range \a range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return end component_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the first component of the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(0,1));
  ** \endcode
  */
  component_row_range_iterator row_end(const std::size_t component, const size_type row,
			     const slip::Range<int>& range);

  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return begin const_component_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the first component of the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(0,1));
  ** \endcode
  */
  const_component_row_range_iterator row_begin(const std::size_t component, const size_type row,
				     const slip::Range<int>& range) const;

  /*!
  **  \brief Returns a read_only iterator that points one past the last
  **  element of the %Range range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param row Row to iterate.
  ** \param range %Range of the row to iterate
  ** \return begin const_component_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the first component of the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(0,1));
  ** \endcode
  */
  const_component_row_range_iterator row_end(const std::size_t component, const size_type row,
				   const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read-write iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate
  ** \return begin component_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the first component of the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(0,1));
  ** \endcode
  */
  component_col_range_iterator col_begin(const std::size_t component, const size_type col,
			       const slip::Range<int>& range);

  /*!
  **  \brief Returns a read-write iterator that points to the past
  **  the end element of the %Range \a range of the col \a col in the 
  **  %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin component_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the first component of the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(0,1));
  ** \endcode
  */
  component_col_range_iterator col_end(const std::size_t component, const size_type col,
			     const slip::Range<int>& range);

  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin const_component_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the first component of the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(0,1));
  ** \endcode
  */
  const_component_col_range_iterator col_begin(const std::size_t component, const size_type col,
					       const slip::Range<int>& range) const;
 
  /*!
  **  \brief Returns a read-only iterator that points to the past
  **  the end element of the %Range \a range of the col \a col in 
  **  the %GenericMultiComponent2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate
  ** \return begin const_component_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the first component of the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(0,1));
  ** \endcode
  */
  const_component_col_range_iterator col_end(const std::size_t component, const size_type col,
					     const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read-write iterator that points to the last
  **  element of the %Range \a range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return reverse_component_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  reverse_component_row_range_iterator row_rbegin(const std::size_t component, const size_type row,
						  const slip::Range<int>& range);

 
  /*!
  **  \brief Returns a read-write iterator that points one before
  **  the first element of the %Range \a range of the row \a row in the 
  **  %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return reverse_component_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  reverse_component_row_range_iterator row_rend(const std::size_t component, const size_type row,
						const slip::Range<int>& range);



  /*!
  **  \brief Returns a read-only iterator that points to the last
  **  element of the %Range \a range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate
  ** \return begin const_reverse_component_row_range_iterator value
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  const_reverse_component_row_range_iterator row_rbegin(const std::size_t component, const size_type row,
							const slip::Range<int>& range) const;


   /*!
  **  \brief Returns a read-only iterator that points one before the first
  **  element of the %Range \a range of the row \a row in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate
  ** \return const_reverse_component_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  const_reverse_component_row_range_iterator row_rend(const std::size_t component, const size_type row,
						      const slip::Range<int>& range) const;
 
 


  /*!
  **  \brief Returns a read-write iterator that points to the last
  **  element of the %Range \a range of the col \a col in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin reverse_component_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  reverse_component_col_range_iterator col_rbegin(const std::size_t component, const size_type col,
						  const slip::Range<int>& range);

  /*!
  **  \brief Returns a read-write iterator that points to one before 
  **  the first element of the %Range range of the col \a col in the 
  **  %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return reverse_component_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  reverse_component_col_range_iterator col_rend(const std::size_t component, const size_type col,
						const slip::Range<int>& range);


  /*!
  **  \brief Returns a read_only iterator that points to the last
  **  element of the %Range \& range of the col \a col in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param component The component number.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin const_reverse_component_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  const_reverse_component_col_range_iterator col_rbegin(const std::size_t component, const size_type col,
							const slip::Range<int>& range) const;

 
  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %GenericMultiComponent2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range
  ** \param component The component number.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return const_reverse_component_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %GenericMultiComponent2d.
  ** \pre The range must be inside the whole range of the %GenericMultiComponent2d.
  */
  const_reverse_component_col_range_iterator col_rend(const std::size_t component, const size_type col,
						      const slip::Range<int>& range) const;

  /*!
  **  \brief Returns a read/write component_iterator2d that points to the first
  **  element of the %GenericMultiComponent2d. It points to the upper left element of
  **  the %GenericMultiComponent2d.
  **  
  ** \param component The component number.
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,8);
  ** //copy the first component of the elements of A1 in A2
  ** std::copy(A1.upper_left(0),A1.bottom_right(0),A2.upper_left(0));
  ** \endcode
  */
  component_iterator2d upper_left(const std::size_t component);
  
  /*!
  **  \brief Returns a read/write component_iterator2d that points to the past
  **  the end element of the %GenericMultiComponent2d. It points to past the end element of
  **  the bottom right element of the %GenericMultiComponent2d.
  **  
  ** \param component The component number.
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A1(8,8);
  **  slip::GenericMultiComponent2d<slip::Vector2d<double> > A2(8,8);
  ** //copy the first component of the elements of A1 in A2
  ** std::copy(A1.upper_left(0),A1.bottom_right(0),A2.upper_left(0));
  ** \endcode
  */
  component_iterator2d bottom_right(const std::size_t component);

  /*!
  **  \brief Returns a read only const_component_iterator2d that points to the first
  **  element of the %GenericMultiComponent2d. It points to the upper left element of
  **  the %GenericMultiComponent2d.
  **  
  ** \param component The component number.
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_component_iterator2d upper_left(const std::size_t component) const;
  
  /*!
  **  \brief Returns a read only const_component_iterator2d that points to the past
  **  the end element of the %GenericMultiComponent2d. It points to past the end element of
  **  the bottom right element of the %GenericMultiComponent2d.
  **  
  ** \param component The component number.
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_component_iterator2d bottom_right(const std::size_t component) const;

  /*!
  **  \brief Returns a read/write reverse component iterator2d. It points to the 
  **   bottom right element of the %GenericMultiComponent2d. 
  *    Iteration is done within the %GenericMultiComponent2d in the reverse order.
  **  
  ** \param component The component number.
  ** \return reverse_component_iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_component_iterator2d rupper_left(const std::size_t component);
  
  /*!
  **  \brief Returns a read/write reverse component iterator2d. It points to past the 
  **  upper left element of the %GenericMultiComponent2d.
  **  Iteration is done in the reverse order.
  **  
  ** \param component The component number.
  ** \return reverse component iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_component_iterator2d rbottom_right(const std::size_t component);

  /*!
  **  \brief Returns a read only reverse component iterator2d that points.  It points 
  **  to the bottom right element of the %GenericMultiComponent2d. 
  **  Iteration is done within the %GenericMultiComponent2d in the reverse order.
  **  
  ** \param component The component number.
  ** \return const_reverse_component_iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_component_iterator2d rupper_left(const std::size_t component) const;
 
 
  /*!
  **  \brief Returns a read only reverse component iterator2d. It points to past the 
  **  upper left element of the %GenericMultiComponent2d.
  **  Iteration is done in the reverse order.
  **  
  ** \param component The component number.
  ** \return const reverse component iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_component_iterator2d rbottom_right(const std::size_t component) const;

  /*!
  **  \brief Returns a read/write component iterator2d that points to the first
  **  element of the %GenericMultiComponent2d. It points to the upper left element of
  **  the \a %Box2d associated to the %GenericMultiComponent2d.
  **
  ** \param component The component number.
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **  
  ** \return end iterator2d value
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  */
  component_iterator2d upper_left(const std::size_t component, 
				  const Box2d<int>& box);
 
  /*!
  **  \brief Returns a read/write component iterator2d that points to the past
  **  the end element of the %GenericMultiComponent2d. It points to past the end element of
  **  the bottom right element of the \a %Box2d associated to the %GenericMultiComponent2d.
  ** 
  ** \param component The component number.
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  **
  ** \return end iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  **  
  */
  component_iterator2d bottom_right(const std::size_t component, 
				    const Box2d<int>& box);

  /*!
  **  \brief Returns a read only component iterator2d that points to the first
  **  element of the %GenericMultiComponent2d. It points to the upper left element of
  **  the \a %Box2d associated to the %GenericMultiComponent2d.
  **
  ** \param component The component number.
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  ** \return end const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  */
  const_component_iterator2d upper_left(const std::size_t component, 
					const Box2d<int>& box) const;
 
  /*!
  **  \brief Returns a read only component iterator2d that points to the past
  **  the end element of the %GenericMultiComponent2d. It points to past the end element of
  **  the bottom right element of the \a %Box2d associated to the %GenericMultiComponent2d.
  **
  ** \param component The component number.
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  ** \return end const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  */
  const_component_iterator2d bottom_right(const std::size_t component, 
					  const Box2d<int>& box) const;


  /*!
  **  \brief Returns a read/write component iterator2d_range that points to the 
  **  upper left element of the ranges \a row_range and \a col_range 
  **  associated to the %GenericMultiComponent2d.
  **
  ** \param component The component number.
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  component_iterator2d_range upper_left(const std::size_t component, 
					const Range<int>& row_range,
					const Range<int>& col_range);

  /*!
  **  \brief Returns a read/write component iterator2d_range that points to the 
  **   past the end bottom right element of the ranges \a row_range 
  **   and \a col_range associated to the %GenericMultiComponent2d.
  **
  ** \param component The component number.
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  component_iterator2d_range bottom_right(const std::size_t component, 
					  const Range<int>& row_range,
					  const Range<int>& col_range);

  /*!
  **  \brief Returns a read-only component iterator2d_range that points to the
  **   to the upper left element of the ranges \a row_range and \a col_range 
  **   associated to the %GenericMultiComponent2d.
  **
  ** \param component The component number.
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid.
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_component_iterator2d_range upper_left(const std::size_t component, 
					      const Range<int>& row_range,
					      const Range<int>& col_range) const;

 /*!
  **  \brief Returns a read-only component iterator2d_range that points to the past
  **  the end bottom right element of the ranges \a row_range and \a col_range 
  **  associated to the %GenericMultiComponent2d.
  **
  ** \param component The component number.
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_component_iterator2d_range bottom_right(const std::size_t component, 
						const Range<int>& row_range,
						const Range<int>& col_range) const;

  /*!
  **  \brief Returns a read/write reverse component iterator2d. It points to the 
  **  bottom right element of the \a %Box2d associated to the %GenericMultiComponent2d.
  **  Iteration is done in the reverse order.
  **
  ** \param component The component number.
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  **
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_component_iterator2d rupper_left(const std::size_t component, 
					   const Box2d<int>& box);

  /*!
  **  \brief Returns a read/write reverse component iterator2d. It points to one
  **  before the upper left element of the %Box2d \a box associated to 
  **  the %GenericMultiComponent2d.
  **
  ** \param component The component number.
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  **
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_component_iterator2d rbottom_right(const std::size_t component, 
					     const Box2d<int>& box);

  /*!
  **  \brief Returns a read only reverse component iterator2d. It points to the 
  **  bottom right element of the %Box2d \a box associated to the %GenericMultiComponent2d.
  **  Iteration is done in the reverse order.
  **
  ** \param component The component number.
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  **
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_component_iterator2d rupper_left(const std::size_t component, 
						 const Box2d<int>& box) const;


  /*!
  **  \brief Returns a read-only reverse component iterator2d. It points to one 
  **  before the element of the bottom right element of the %Box2d 
  **  \a box associated to the %GenericMultiComponent2d.
  **
  ** \param component The component number.
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %GenericMultiComponent2d.
  **
  ** \pre The box indices must be inside the range of the %GenericMultiComponent2d ones.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_component_iterator2d rbottom_right(const std::size_t component, 
						   const Box2d<int>& box) const;

 
  /*!
  **  \brief Returns a read/write reverse component iterator2d_range that points to the 
  **  past the bottom right element of the ranges \a row_range and 
  **  \a col_range associated to the %GenericMultiComponent2d. Iteration is done in the 
  **  reverse order.
  **
  ** \param component The component number.
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_component_iterator2d_range rupper_left(const std::size_t component, 
						 const Range<int>& row_range,
						 const Range<int>& col_range);

  /*!
  **  \brief Returns a read/write reverse_component_iterator2d_range that points
  **  to one before the upper left element of the ranges \a row_range 
  **  and \a col_range associated to the %GenericMultiComponent2d. Iteration is done
  **  in the reverse order.
  **
  ** \param component The component number.
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_component_iterator2d_range rbottom_right(const std::size_t component, 
						   const Range<int>& row_range,
						   const Range<int>& col_range);

  /*!
  **  \brief Returns a read-only reverse_component_iterator2d_range that points 
  **   to the past the bottom right element of the ranges \a row_range and 
  **  \a col_range associated to the %GenericMultiComponent2d. Iteration is done in the 
  **   reverse order.
  **
  ** \param component The component number.
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid.
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_component_iterator2d_range rupper_left(const std::size_t component, 
						       const Range<int>& row_range,
						       const Range<int>& col_range) const;

  /*!
  **  \brief Returns a read-only reverse_component_iterator2d_range that points 
  **  to one before the upper left element of the ranges \a row_range 
  **  and \a col_range associated to the %GenericMultiComponent2d.Iteration is done in 
  **  the reverse order.
  **
  ** \param component The component number.
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_component_iterator2d_range rbottom_right(const std::size_t component, 
							 const Range<int>& row_range,
							 const Range<int>& col_range) const;


 /*@} End iterators */

  /**
   ** \name i/o operators
   */
  /*@{*/
  
  /*!
  ** \brief Write the GenericMultiComponent2d to the ouput stream
  ** \param out output stream
  ** \param a GenericMultiComponent2d to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, const self& a);
 
  /*@} End i/o operators */

  /**
   ** \name  Assignment operators and methods
   */
  /*@{*/

  /*!
  ** \brief Assign a GenericMultiComponent2d.
  **
  ** Assign elements of GenericMultiComponent2d in \a rhs
  **
  ** \param rhs GenericMultiComponent2d to get the values from.
  ** \return 
  */
  self& operator=(const self & rhs);

  /*!
  ** \brief Affects all the element of the %GenericMultiComponent2d by val
  ** \param val affectation value
  ** \return reference to corresponding %GenericMultiComponent2d
  */
  self& operator=(const Block& val);


  /*!
  ** \brief Affects all the element of the %GenericMultiComponent2d by val
  ** \param val affectation value
  ** \return reference to corresponding %GenericMultiComponent2d
  */
  self& operator=(const typename Block::value_type& val);



   /*!
   ** \brief Fills the container range [begin(),begin()+size()) 
   **        with copies of value
   ** \param value  A reference-to-const of arbitrary type.
  */
  void fill(const Block& value)
  {
    std::fill_n(this->begin(),size(),value);
  } 
 
  /*!
  ** \brief Fills the container range [begin(),begin()+size())
  **        with a copy of
  **        the value array
  ** \param value  A pointer of arbitrary type.
  */
  void fill(const typename Block::pointer value)
  {
    std::copy(value,value + size(), (typename Block::pointer)begin());
  } 
  
 /*!
  ** \brief Fills the container range [begin(),begin()+size())
  **        with a copy of
  **        the value array
  ** \param value  A pointer of arbitrary type.
  */
  void fill(const Block* value)
  {
    std::copy(value,value + size(), this->begin());
  } 

  /*!
  ** \brief Fills the container range [begin(),begin()+size()) 
  **        with a copy of the range [first,last)
  **  \param  first  An input iterator.
  **  \param  last   An input iterator.
  **   
  **
  */
  template<typename InputIterator>
  void fill(InputIterator first,
	    InputIterator last)
  {
    std::copy(first,last,this->begin());
  }

  /*!
  ** \brief Fills the container range [begin(),begin()+size()) 
  **        with a copy of the range [first,last)
  **  \param  first_iterators_list  An input iterator list.
  **  \param  last   An input iterator.
  **   
  **
  */
  template<typename InputIterator>
  void fill(std::vector<InputIterator> first_iterators_list,
	    InputIterator last)
  {
    std::vector<typename slip::kstride_iterator<typename Block::pointer,Block::SIZE> > iterators_list(Block::SIZE);

    for(std::size_t component = 0; component < Block::SIZE; ++component)
      {
	iterators_list[component]= slip::kstride_iterator<typename Block::pointer,Block::SIZE>((typename Block::pointer)this->begin() + component);
      }
	
    
    while(first_iterators_list[0] != last)
	  {
	    for(std::size_t component = 0; component < Block::SIZE; ++component)
	      {
		*iterators_list[component]++ = *first_iterators_list[component]++;
	      }
	  }
    iterators_list.clear();
  }


/*!
  ** \brief Fills the container range [begin(),begin()+size()) 
  **        with a copy of the range [first,last)
  ** \param component The component number.
  ** \param  first  An input iterator.
  ** \param  last   An input iterator.
  **   
  **
  */
  template<typename InputIterator>
  void fill(std::size_t component,
	    InputIterator first,
	    InputIterator last)
  {
    std::copy(first,last,this->begin(component));
  }

  /*@} End Assignment operators and methods*/

 /**
  ** \name Comparison operators
 */
  /*@{*/
  /*!
  ** \brief GenericMultiComponent2d equality comparison
  ** \param x A %GenericMultiComponent2d
  ** \param y A %GenericMultiComponent2d of the same type of \a x
  ** \return true iff the size and the elements of the Arrays are equal
  ** \pre x.size() == y.size()
  */
  friend bool operator== <>(const GenericMultiComponent2d<Block>& x, 
			    const GenericMultiComponent2d<Block>& y);

 /*!
  ** \brief GenericMultiComponent2d inequality comparison
  ** \param x A %GenericMultiComponent2d
  ** \param y A %GenericMultiComponent2d of the same type of \a x
  ** \return true if !(x == y) 
  ** \pre x.size() == y.size()
  */
  friend bool operator!= <>(const GenericMultiComponent2d<Block>& x, 
			    const GenericMultiComponent2d<Block>& y);

//  /*!
//   ** \brief Less than comparison operator (GenericMultiComponent2d ordering relation)
//   ** \param x A %GenericMultiComponent2d
//   ** \param y A %GenericMultiComponent2d of the same type of \a x
//   ** \return true iff \a x is lexicographically less than \a y
//   ** \pre x.size() == y.size()
//   */
//   friend bool operator< <>(const GenericMultiComponent2d<Block>& x, 
// 			   const GenericMultiComponent2d<Block>& y);

//  /*!
//   ** \brief More than comparison operator
//   ** \param x A %GenericMultiComponent2d
//   ** \param y A %GenericMultiComponent2d of the same type of \a x
//   ** \return true iff y > x 
//   ** \pre x.size() == y.size()
//   */
//   friend bool operator> <>(const GenericMultiComponent2d<Block>& x, 
// 			   const GenericMultiComponent2d<Block>& y);

//   /*!
//   ** \brief Less than equal comparison operator
//   ** \param x A %GenericMultiComponent2d
//   ** \param y A %GenericMultiComponent2d of the same type of \a x
//   ** \return true iff !(y > x) 
//   ** \pre x.size() == y.size()
//   */
//   friend bool operator<= <>(const GenericMultiComponent2d<Block>& x, 
// 			    const GenericMultiComponent2d<Block>& y);

//   /*!
//   ** \brief More than equal comparison operator
//   ** \param x A %GenericMultiComponent2d
//   ** \param y A %GenericMultiComponent2d of the same type of \a x
//   ** \return true iff !(x < y) 
//   ** \pre x.size() == y.size()
//   */
//   friend bool operator>= <>(const GenericMultiComponent2d<Block>& x, 
// 			    const GenericMultiComponent2d<Block>& y);


   /*@} Comparison operators */

  /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Subscript access to the row datas contained in the %GenericMultiComponent2d.
  ** \param i The index of the row for which data should be accessed.
  ** \return Read/write pointer to the row data.
  ** \pre i < rows()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  pointer operator[](const size_type i);

  /*!
  ** \brief Subscript access to the row datas contained in the %GenericMultiComponent2d.
  ** \param i The index of the row for which data should be accessed.
  ** \return Read-only (constant) pointer to the row data.
  ** \pre i < rows()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_pointer operator[](const size_type i) const;


  /*!
  ** \brief Subscript access to the data contained in the %GenericMultiComponent2d.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read/write reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const size_type i,
		       const size_type j);
 
  /*!
  ** \brief Subscript access to the data contained in the %GenericMultiComponent2d.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read_only (constant) reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const size_type i,
			     const size_type j) const;


  /*!
  ** \brief Subscript access to the data contained in the %GenericMultiComponent2d.
  ** \param point2d A %Point2d which indicate the subscripts of the data
  **        to access.
  ** \return Read/write reference to data.
  ** \pre point2d must be defined in the range of the %GenericMultiComponent2d.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const Point2d<size_type>& point2d);
 
  /*!
  ** \brief Subscript access to the data contained in the %GenericMultiComponent2d.
  ** \param point2d A %Point2d which indicate the subscripts of the data
  **        to access.
  ** \return Read_only (constant) reference to data.
  ** \pre point2d must be defined in the range of the %GenericMultiComponent2d.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const Point2d<size_type>& point2d) const;

   /*!
  ** \brief Subscript access to the data contained in the %GenericMultiComponent2d.
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  ** \return a copy of the range.
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %GenericMultiComponent2d ones.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  self operator()(const Range<int>& row_range,
		  const Range<int>& col_range);
 

  /*@} End Element access operators */

 /*!
   ** \brief Returns the number of rows (first dimension size) 
   **        in the %GenericMultiComponent2d
   */
  size_type dim1() const;

  /*!
   ** \brief Returns the number of rows (first dimension size) 
   **        in the %GenericMultiComponent2d
   */
  size_type rows() const;
  
  /*!
   ** \brief Returns the height (first dimension size) 
   **        in the %GenericMultiComponent2d
   */
  size_type height() const;
  
  /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %GenericMultiComponent2d
  */
  size_type dim2() const;

   /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %GenericMultiComponent2d
  */
  size_type columns() const;
 
  /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %GenericMultiComponent2d
  */
  size_type cols() const;

  /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %GenericMultiComponent2d
  */
  size_type width() const;

  /*!
  ** \brief Returns the number of elements in the %GenericMultiComponent2d
  */
  size_type size() const;

  /*!
  ** \brief Returns the maximal size (number of elements) in the %GenericMultiComponent2d
  */
  size_type max_size() const;

  /*!
  ** \brief Returns true if the %GenericMultiComponent2d is empty.  (Thus size() == 0)
  */
  bool empty()const;

  /*!
  ** \brief Swaps data with another %GenericMultiComponent2d.
  ** \param M A %GenericMultiComponent2d of the same element type
  */
  void swap(self& M);


  /*!
  ** \brief Returns the min elements of the %GenericMultiComponent2d 
  ** according to the operator <
  ** \pre size() != 0
  */
  Block min() const;


  /*!
  ** \brief Returns the max elements of the GenericMultiComponent2d 
  ** according to the operator <
  ** \pre size() != 0
  */
  Block max() const;

 //  /*!
//   ** \brief Returns the sums of the elements of the %GenericMultiComponent2d 
//   ** \pre size() != 0
//   */
//   Block sum() const;

  /*!
  ** \brief Applys the one-parameter C-function \a fun 
  **        to each element of the %GenericMultiComponent2d
  ** \param fun The one-parameter C function 
  ** \return the resulting %GenericMultiComponent2d
  */
  GenericMultiComponent2d<Block>& apply(Block(*fun)(Block));

  /*!
  ** \brief Applys the one-parameter C-function \a fun 
  **        to each element of the %GenericMultiComponent2d
  ** \param fun The one-const-parameter C function 
  ** \return the resulting %GenericMultiComponent2d
  */
  GenericMultiComponent2d<Block>& apply(Block(*fun)(const Block&));


  //private:
private:
  slip::Matrix<Block>* matrix_;

   private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & this->matrix_;
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & this->matrix_;
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()

};

/** @} */ // end of MultiComponent2dContainer group
}//slip::


namespace slip
{
  template<typename Block>
  inline
  GenericMultiComponent2d<Block>::GenericMultiComponent2d():
    matrix_(new slip::Matrix<Block>())
  {}

  template<typename Block>
  inline
  GenericMultiComponent2d<Block>::GenericMultiComponent2d(const typename GenericMultiComponent2d<Block>::size_type height,
							  const typename GenericMultiComponent2d<Block>::size_type width):
    matrix_(new slip::Matrix<Block>(height,width))
  {}
  
  template<typename Block>
  inline
  GenericMultiComponent2d<Block>::GenericMultiComponent2d(const typename GenericMultiComponent2d<Block>::size_type height,
							  const typename GenericMultiComponent2d<Block>::size_type width, 
						      typename Block::const_pointer val):
    matrix_(new slip::Matrix<Block>(height,width))
  {
    typename slip::Matrix<Block>::iterator it = matrix_->begin();
    typename slip::Matrix<Block>::iterator ite = matrix_->end();
    
    for(;it!=ite; it++)
      {
	for(typename GenericMultiComponent2d<Block>::size_type component = 0; component < Block::SIZE; ++component)
	  {
	    (*it)[component] = *val++;
	  }
      }
  }

  template<typename Block>
  inline
  GenericMultiComponent2d<Block>::GenericMultiComponent2d(const typename GenericMultiComponent2d<Block>::size_type height,
						      const typename GenericMultiComponent2d<Block>::size_type width, 
						      const Block& val):
    matrix_(new slip::Matrix<Block>(height,width,val))
  {}

  template<typename Block>
  inline
  GenericMultiComponent2d<Block>::GenericMultiComponent2d(const typename GenericMultiComponent2d<Block>::size_type height,
						      const typename GenericMultiComponent2d<Block>::size_type width, 
						      const Block* val):
    matrix_(new slip::Matrix<Block>(height,width,val))   
  {}
  
  template<typename Block>
  inline
  GenericMultiComponent2d<Block>::GenericMultiComponent2d(const GenericMultiComponent2d<Block>& rhs):
    matrix_(new slip::Matrix<Block>((*rhs.matrix_)))
  {}

  template<typename Block>
  inline
  GenericMultiComponent2d<Block>::~GenericMultiComponent2d()
  {
    delete matrix_;
  }

  template<typename Block>
  inline
  GenericMultiComponent2d<Block>& GenericMultiComponent2d<Block>::operator=(const GenericMultiComponent2d<Block> & rhs)
  {
    if(this != &rhs)
      {
	*matrix_ = *(rhs.matrix_);
      }
    return *this;
  }

  template<typename Block>
  inline
  GenericMultiComponent2d<Block>& GenericMultiComponent2d<Block>::operator=(const Block& val)
  {
    std::fill_n(matrix_->begin(),matrix_->size(),val);
    return *this;
  }

   template<typename Block>
  inline
  GenericMultiComponent2d<Block>& GenericMultiComponent2d<Block>::operator=(const typename Block::value_type& val)
  {
    std::fill_n(matrix_->begin(),matrix_->size(),val);
    return *this;
  }
  
  template<typename Block>
  inline
  void GenericMultiComponent2d<Block>::resize(const typename GenericMultiComponent2d<Block>::size_type height,
					      const typename GenericMultiComponent2d<Block>::size_type width,
					      const Block& val)
  {
    matrix_->resize(height,width,val);
  }

  //---------------------------------- 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::iterator GenericMultiComponent2d<Block>::begin()
  {
    return matrix_->begin();
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::iterator GenericMultiComponent2d<Block>::end()
  {
    return matrix_->end();
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_iterator GenericMultiComponent2d<Block>::begin() const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->begin();
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_iterator GenericMultiComponent2d<Block>::end() const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->end();
  }
  

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_iterator GenericMultiComponent2d<Block>::rbegin()
  {
    return typename GenericMultiComponent2d<Block>::reverse_iterator(this->end());
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_iterator GenericMultiComponent2d<Block>::rend()
  {
    return typename GenericMultiComponent2d<Block>::reverse_iterator(this->begin());
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_iterator GenericMultiComponent2d<Block>::rbegin() const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_iterator(this->end());
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_iterator GenericMultiComponent2d<Block>::rend() const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_iterator(this->begin());
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::row_iterator 
  GenericMultiComponent2d<Block>::row_begin(const typename GenericMultiComponent2d<Block>::size_type row)
  {
    return matrix_->row_begin(row);
  }

 template<typename Block>
 inline
 typename GenericMultiComponent2d<Block>::row_iterator 
 GenericMultiComponent2d<Block>::row_end(const typename GenericMultiComponent2d<Block>::size_type row)
  {
    return matrix_->row_end(row);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::col_iterator 
  GenericMultiComponent2d<Block>::col_begin(const typename GenericMultiComponent2d<Block>::size_type col)
  {
    return matrix_->col_begin(col);
  }


  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::col_iterator
  GenericMultiComponent2d<Block>::col_end(const typename GenericMultiComponent2d<Block>::size_type col)
  {
    return matrix_->col_end(col);
  }


 template<typename Block>
 inline
 typename GenericMultiComponent2d<Block>::const_row_iterator
 GenericMultiComponent2d<Block>::row_begin(const typename GenericMultiComponent2d<Block>::size_type row) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->row_begin(row);
  }

 template<typename Block>
 inline
 typename GenericMultiComponent2d<Block>::const_row_iterator 
 GenericMultiComponent2d<Block>::row_end(const typename GenericMultiComponent2d<Block>::size_type row) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->row_end(row);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_col_iterator GenericMultiComponent2d<Block>::col_begin(const typename GenericMultiComponent2d<Block>::size_type col) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->col_begin(col);
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_col_iterator GenericMultiComponent2d<Block>::col_end(const typename GenericMultiComponent2d<Block>::size_type col) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->col_end(col);
  }


  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_row_iterator
  GenericMultiComponent2d<Block>::row_rbegin(const typename GenericMultiComponent2d<Block>::size_type row)
  {
    return matrix_->row_rbegin(row);
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_row_iterator 
  GenericMultiComponent2d<Block>::row_rbegin(const typename GenericMultiComponent2d<Block>::size_type row) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->row_rbegin(row);
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_row_iterator GenericMultiComponent2d<Block>::row_rend(const typename GenericMultiComponent2d<Block>::size_type row)
  {
    return matrix_->row_rend(row);
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_row_iterator
  GenericMultiComponent2d<Block>::row_rend(const typename GenericMultiComponent2d<Block>::size_type row) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->row_rend(row);
  }

  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_col_iterator GenericMultiComponent2d<Block>::col_rbegin(const typename GenericMultiComponent2d<Block>::size_type col)
  {
    return matrix_->col_rbegin(col);
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_col_iterator GenericMultiComponent2d<Block>::col_rbegin(const typename GenericMultiComponent2d<Block>::size_type col) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->col_rbegin(col);
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_col_iterator 
  GenericMultiComponent2d<Block>::col_rend(const typename GenericMultiComponent2d<Block>::size_type col)
  {
    return matrix_->col_rend(col);
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_col_iterator
  GenericMultiComponent2d<Block>::col_rend(const typename GenericMultiComponent2d<Block>::size_type col) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->col_rend(col);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::row_range_iterator 
  GenericMultiComponent2d<Block>::row_begin(const typename GenericMultiComponent2d<Block>::size_type row, const slip::Range<int>& range)
  {
    return matrix_->row_begin(row,range);
  }

   template<typename Block>
  inline
   typename GenericMultiComponent2d<Block>::const_row_range_iterator 
   GenericMultiComponent2d<Block>::row_begin(const typename GenericMultiComponent2d<Block>::size_type row,
			const slip::Range<int>& range) const
   {
     slip::Matrix<Block> const * tp(matrix_);
     return tp->row_begin(row,range);
   }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::col_range_iterator 
  GenericMultiComponent2d<Block>::col_begin(const typename GenericMultiComponent2d<Block>::size_type col,
		       const slip::Range<int>& range)
  {
    return matrix_->col_begin(col,range);
  }

   template<typename Block>
  inline
   typename GenericMultiComponent2d<Block>::const_col_range_iterator 
   GenericMultiComponent2d<Block>::col_begin(const typename GenericMultiComponent2d<Block>::size_type col,
			const slip::Range<int>& range) const
   {
     slip::Matrix<Block> const * tp(matrix_);
     return tp->col_begin(col,range);
   }
  

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::row_range_iterator 
  GenericMultiComponent2d<Block>::row_end(const typename GenericMultiComponent2d<Block>::size_type row,
		       const slip::Range<int>& range)
  {
    return matrix_->row_end(row,range);
  }

   template<typename Block>
  inline
   typename GenericMultiComponent2d<Block>::const_row_range_iterator 
   GenericMultiComponent2d<Block>::row_end(const typename GenericMultiComponent2d<Block>::size_type row,
			const slip::Range<int>& range) const
   {
     slip::Matrix<Block> const * tp(matrix_);
     return tp->row_end(row,range);
   }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::col_range_iterator 
  GenericMultiComponent2d<Block>::col_end(const typename GenericMultiComponent2d<Block>::size_type col,
		     const slip::Range<int>& range)
  {
    return matrix_->col_end(col,range);
  }

   template<typename Block>
  inline
   typename GenericMultiComponent2d<Block>::const_col_range_iterator 
   GenericMultiComponent2d<Block>::col_end(const typename GenericMultiComponent2d<Block>::size_type col,
		      const slip::Range<int>& range) const
   {
     slip::Matrix<Block> const * tp(matrix_);
     return tp->col_end(col,range);
   }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_row_range_iterator 
  GenericMultiComponent2d<Block>::row_rbegin(const typename GenericMultiComponent2d<Block>::size_type row,
			const slip::Range<int>& range)
  {
     return typename GenericMultiComponent2d<Block>::reverse_row_range_iterator(this->row_end(row,range));
  }

 template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_row_range_iterator 
  GenericMultiComponent2d<Block>::row_rbegin(const typename GenericMultiComponent2d<Block>::size_type row,
			 const slip::Range<int>& range) const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_row_range_iterator(this->row_end(row,range));
  }


  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_col_range_iterator 
  GenericMultiComponent2d<Block>::col_rbegin(const typename GenericMultiComponent2d<Block>::size_type col,
			 const slip::Range<int>& range)
  {
    return typename GenericMultiComponent2d<Block>::reverse_col_range_iterator(this->col_end(col,range));
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_col_range_iterator 
  GenericMultiComponent2d<Block>::col_rbegin(const typename GenericMultiComponent2d<Block>::size_type col,
			 const slip::Range<int>& range) const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_col_range_iterator(this->col_end(col,range));
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_row_range_iterator 
  GenericMultiComponent2d<Block>::row_rend(const typename GenericMultiComponent2d<Block>::size_type row,
		       const slip::Range<int>& range)
  {
    return typename GenericMultiComponent2d<Block>::reverse_row_range_iterator(this->row_begin(row,range));
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_row_range_iterator 
  GenericMultiComponent2d<Block>::row_rend(const typename GenericMultiComponent2d<Block>::size_type row,
			 const slip::Range<int>& range) const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_row_range_iterator(this->row_begin(row,range));
  }


  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_col_range_iterator 
  GenericMultiComponent2d<Block>::col_rend(const typename GenericMultiComponent2d<Block>::size_type col,
		       const slip::Range<int>& range)
  {
    return typename GenericMultiComponent2d<Block>::reverse_col_range_iterator(this->col_begin(col,range));
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_col_range_iterator 
  GenericMultiComponent2d<Block>::col_rend(const typename GenericMultiComponent2d<Block>::size_type col,
		       const slip::Range<int>& range) const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_col_range_iterator(this->col_begin(col,range));
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::iterator2d GenericMultiComponent2d<Block>::upper_left()
  {
    return matrix_->upper_left();
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_iterator2d GenericMultiComponent2d<Block>::upper_left() const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->upper_left();
  }


  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::iterator2d GenericMultiComponent2d<Block>::bottom_right()
  {
    return matrix_->bottom_right();
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_iterator2d GenericMultiComponent2d<Block>::bottom_right() const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->bottom_right();
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::iterator2d GenericMultiComponent2d<Block>::upper_left(const Box2d<int>& box)
  {
    return matrix_->upper_left(box);
  }


  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_iterator2d GenericMultiComponent2d<Block>::upper_left(const Box2d<int>& box) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->upper_left(box);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::iterator2d GenericMultiComponent2d<Block>::bottom_right(const Box2d<int>& box)
  {
    return matrix_->bottom_right(box);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_iterator2d GenericMultiComponent2d<Block>::bottom_right(const Box2d<int>& box) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->bottom_right(box);
  }


  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::iterator2d_range 
  GenericMultiComponent2d<Block>::upper_left(const Range<int>& row_range,
			 const Range<int>& col_range)
  {
    return matrix_->upper_left(row_range,col_range);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_iterator2d_range 
  GenericMultiComponent2d<Block>::upper_left(const Range<int>& row_range,
			const Range<int>& col_range) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->upper_left(row_range,col_range);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::iterator2d_range 
  GenericMultiComponent2d<Block>::bottom_right(const Range<int>& row_range,
			  const Range<int>& col_range)
  {
    return matrix_->bottom_right(row_range,col_range);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_iterator2d_range 
  GenericMultiComponent2d<Block>::bottom_right(const Range<int>& row_range,
			  const Range<int>& col_range) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->bottom_right(row_range,col_range);
  }

   template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::iterator2d_range 
  GenericMultiComponent2d<Block>::upper_left(const Range<int>& range)
  {
    return matrix_->upper_left(range);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_iterator2d_range 
  GenericMultiComponent2d<Block>::upper_left(const Range<int>& range) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->upper_left(range);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::iterator2d_range 
  GenericMultiComponent2d<Block>::bottom_right(const Range<int>& range)
  {
    return matrix_->bottom_right(range);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_iterator2d_range 
  GenericMultiComponent2d<Block>::bottom_right(const Range<int>& range) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->bottom_right(range);
  }

  
   template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_iterator2d 
  GenericMultiComponent2d<Block>::rbottom_right()
  {
    return matrix_->rbottom_right();
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_iterator2d 
  GenericMultiComponent2d<Block>::rbottom_right() const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->rbottom_right();
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_iterator2d 
  GenericMultiComponent2d<Block>::rupper_left()
  {
    return matrix_->rupper_left();
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_iterator2d 
  GenericMultiComponent2d<Block>::rupper_left() const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->rupper_left();
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_iterator2d 
  GenericMultiComponent2d<Block>::rbottom_right(const Box2d<int>& box)
  {
    return matrix_->rbottom_right(box);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_iterator2d 
  GenericMultiComponent2d<Block>::rbottom_right(const Box2d<int>& box) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->rbottom_right(box);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_iterator2d 
  GenericMultiComponent2d<Block>::rupper_left(const Box2d<int>& box)
  {
    return matrix_->rupper_left(box);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_iterator2d 
  GenericMultiComponent2d<Block>::rupper_left(const Box2d<int>& box) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->rupper_left(box);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_iterator2d_range 
  GenericMultiComponent2d<Block>::rupper_left(const Range<int>& row_range,
			  const Range<int>& col_range)
  {
    return matrix_->rupper_left(row_range,col_range);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_iterator2d_range 
  GenericMultiComponent2d<Block>::rupper_left(const Range<int>& row_range,
			 const Range<int>& col_range) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->rupper_left(row_range,col_range);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_iterator2d_range 
  GenericMultiComponent2d<Block>::rbottom_right(const Range<int>& row_range,
			    const Range<int>& col_range)
  {
     return matrix_->rbottom_right(row_range,col_range);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_iterator2d_range 
  GenericMultiComponent2d<Block>::rbottom_right(const Range<int>& row_range,
			    const Range<int>& col_range) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->rbottom_right(row_range,col_range);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_iterator2d_range 
  GenericMultiComponent2d<Block>::rupper_left(const Range<int>& range)
  {
    return this->rupper_left(range,range);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_iterator2d_range 
  GenericMultiComponent2d<Block>::rupper_left(const Range<int>& range) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->rupper_left(range,range);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_iterator2d_range 
  GenericMultiComponent2d<Block>::rbottom_right(const Range<int>& range)
  {
    return this->rbottom_right(range,range);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_iterator2d_range 
  GenericMultiComponent2d<Block>::rbottom_right(const Range<int>& range) const
  {
    slip::Matrix<Block> const * tp(matrix_);
    return tp->rbottom_right(range,range);
  }




  //------------------------------------------------------------------
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_iterator 
  GenericMultiComponent2d<Block>::begin(const typename GenericMultiComponent2d<Block>::size_type component)
  {
    return typename GenericMultiComponent2d<Block>::component_iterator((typename Block::pointer)begin() + component);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_iterator 
  GenericMultiComponent2d<Block>::end(const typename GenericMultiComponent2d<Block>::size_type component)
  {
    return typename GenericMultiComponent2d<Block>::component_iterator((typename Block::pointer)end() + component);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_iterator
  GenericMultiComponent2d<Block>::begin(const typename GenericMultiComponent2d<Block>::size_type component) const
  {
    return typename GenericMultiComponent2d<Block>::const_component_iterator((typename Block::const_pointer)begin() + component);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_iterator
  GenericMultiComponent2d<Block>::end(const typename GenericMultiComponent2d<Block>::size_type component) const
  {
    return typename GenericMultiComponent2d<Block>::const_component_iterator((typename Block::const_pointer)end() + component);
  }
  
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_iterator
  GenericMultiComponent2d<Block>::rbegin(const typename GenericMultiComponent2d<Block>::size_type component)
  {
    return  typename GenericMultiComponent2d<Block>::reverse_component_iterator(this->end(component));
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_iterator
  GenericMultiComponent2d<Block>::rend(const typename GenericMultiComponent2d<Block>::size_type component)
  {
    return  typename GenericMultiComponent2d<Block>::reverse_component_iterator(this->begin(component));
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_iterator
  GenericMultiComponent2d<Block>::rbegin(const typename GenericMultiComponent2d<Block>::size_type component) const
  {
    return  typename GenericMultiComponent2d<Block>::const_reverse_component_iterator(this->end(component));
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_iterator
  GenericMultiComponent2d<Block>::rend(const typename GenericMultiComponent2d<Block>::size_type component) const
  {
    return  typename GenericMultiComponent2d<Block>::const_reverse_component_iterator(this->begin(component));
  }
 

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_row_iterator
  GenericMultiComponent2d<Block>::row_begin(const typename GenericMultiComponent2d<Block>::size_type component,
					     const typename GenericMultiComponent2d<Block>::size_type row)
  {
    return  typename GenericMultiComponent2d<Block>::component_row_iterator((typename Block::pointer)this->row_begin(row) + component);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_row_iterator
  GenericMultiComponent2d<Block>::row_end(const typename GenericMultiComponent2d<Block>::size_type component,
					  const typename GenericMultiComponent2d<Block>::size_type row)
  {
    return typename GenericMultiComponent2d<Block>::component_row_iterator((typename Block::pointer)this->row_end(row) + component);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_row_iterator
  GenericMultiComponent2d<Block>::row_begin(const typename GenericMultiComponent2d<Block>::size_type component,
					    const typename GenericMultiComponent2d<Block>::size_type row) const
  {
    return typename GenericMultiComponent2d<Block>::const_component_row_iterator((typename Block::const_pointer)this->row_begin(row) + component);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_row_iterator
  GenericMultiComponent2d<Block>::row_end(const typename GenericMultiComponent2d<Block>::size_type component,
					  const typename GenericMultiComponent2d<Block>::size_type row) const
  {
    return   typename GenericMultiComponent2d<Block>::const_component_row_iterator((typename Block::const_pointer)this->row_end(row) + component);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_row_iterator
  GenericMultiComponent2d<Block>::row_rbegin(const typename GenericMultiComponent2d<Block>::size_type component,
					     const typename GenericMultiComponent2d<Block>::size_type row)
  {
    return typename GenericMultiComponent2d<Block>::reverse_component_row_iterator(this->row_end(component,row));
  }
 
  template<typename Block>
  inline
   typename GenericMultiComponent2d<Block>::reverse_component_row_iterator
  GenericMultiComponent2d<Block>::row_rend(const typename GenericMultiComponent2d<Block>::size_type component,
					   const typename GenericMultiComponent2d<Block>::size_type row)
  {
    return  typename GenericMultiComponent2d<Block>::reverse_component_row_iterator(this->row_begin(component,row));
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_row_iterator
  GenericMultiComponent2d<Block>::row_rbegin(const typename GenericMultiComponent2d<Block>::size_type component,
					     const typename GenericMultiComponent2d<Block>::size_type row) const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_component_row_iterator(this->row_end(component,row));
  }
 
  template<typename Block>
  inline
   typename GenericMultiComponent2d<Block>::const_reverse_component_row_iterator
  GenericMultiComponent2d<Block>::row_rend(const typename GenericMultiComponent2d<Block>::size_type component,
					   const typename GenericMultiComponent2d<Block>::size_type row) const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_component_row_iterator(this->row_begin(component,row));
  }
 
  template<typename Block>
  inline
  typename slip::GenericMultiComponent2d<Block>::component_col_iterator 
  GenericMultiComponent2d<Block>::col_begin(const typename GenericMultiComponent2d<Block>::size_type component,
					    const typename GenericMultiComponent2d<Block>::size_type col)
  {
    return slip::stride_iterator<typename GenericMultiComponent2d<Block>::block_pointer>(&((*this)[0][col][component]), Block::SIZE * this->dim2());
  }



  template<typename Block>
  inline
  typename slip::GenericMultiComponent2d<Block>::const_component_col_iterator 
  GenericMultiComponent2d<Block>::col_begin(const typename GenericMultiComponent2d<Block>::size_type component,
					    const typename GenericMultiComponent2d<Block>::size_type col) const
  {
    return slip::stride_iterator<typename GenericMultiComponent2d<Block>::const_block_pointer>(&((*this)[0][col][component]), Block::SIZE * this->dim2());
  }


  template<typename Block>
  inline
  typename slip::GenericMultiComponent2d<Block>::component_col_iterator
  GenericMultiComponent2d<Block>::col_end(const typename GenericMultiComponent2d<Block>::size_type component,
					  const typename GenericMultiComponent2d<Block>::size_type col)
  {
       return ++(slip::stride_iterator<typename GenericMultiComponent2d<Block>::block_pointer>(&((*this)[this->dim1()-1][col][component]), Block::SIZE * this->dim2()));
  }


  template<typename Block>
  inline
  typename slip::GenericMultiComponent2d<Block>::const_component_col_iterator
  GenericMultiComponent2d<Block>::col_end(const typename GenericMultiComponent2d<Block>::size_type component,
					  const typename GenericMultiComponent2d<Block>::size_type col) const
  {
    return ++slip::stride_iterator<typename GenericMultiComponent2d<Block>::const_block_pointer>(&((*this)[this->dim1()-1][col][component]), Block::SIZE * this->dim2());
  }


  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_col_iterator
  GenericMultiComponent2d<Block>::col_rbegin(const typename GenericMultiComponent2d<Block>::size_type component,
					     const typename GenericMultiComponent2d<Block>::size_type col)
  {
    return typename GenericMultiComponent2d<Block>::reverse_component_col_iterator(this->col_end(component,col));
  }
 
  template<typename Block>
  inline
   typename GenericMultiComponent2d<Block>::reverse_component_col_iterator
  GenericMultiComponent2d<Block>::col_rend(const typename GenericMultiComponent2d<Block>::size_type component,
					   const typename GenericMultiComponent2d<Block>::size_type col)
  {
    return  typename GenericMultiComponent2d<Block>::reverse_component_col_iterator(this->col_begin(component,col));
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_col_iterator
  GenericMultiComponent2d<Block>::col_rbegin(const typename GenericMultiComponent2d<Block>::size_type component,
					     const typename GenericMultiComponent2d<Block>::size_type col) const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_component_col_iterator(this->col_end(component,col));
  }
 
  template<typename Block>
  inline
   typename GenericMultiComponent2d<Block>::const_reverse_component_col_iterator
  GenericMultiComponent2d<Block>::col_rend(const typename GenericMultiComponent2d<Block>::size_type component,
					   const typename GenericMultiComponent2d<Block>::size_type col) const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_component_col_iterator(this->col_begin(component,col));
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_row_range_iterator 
  GenericMultiComponent2d<Block>::row_begin(const std::size_t component, const size_type row,
					    const slip::Range<int>& range)
  {
    return slip::stride_iterator<typename GenericMultiComponent2d<Block>::block_pointer>(&((*this)[row][range.start()][component]), Block::SIZE * range.stride());
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_row_range_iterator 
  GenericMultiComponent2d<Block>::row_end(const std::size_t component, const size_type row,
					  const slip::Range<int>& range)
  { 
    return ++(slip::stride_iterator<typename GenericMultiComponent2d<Block>::block_pointer>(&((*this)[row][range.stop()][component]), Block::SIZE * range.stride()));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_row_range_iterator 
  GenericMultiComponent2d<Block>::row_begin(const std::size_t component, const size_type row,
					    const slip::Range<int>& range) const
  {
    return slip::stride_iterator<typename GenericMultiComponent2d<Block>::const_block_pointer>(&((*this)[row][range.start()][component]), Block::SIZE * range.stride());
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_row_range_iterator 
  GenericMultiComponent2d<Block>::row_end(const std::size_t component, const size_type row,
					  const slip::Range<int>& range) const
  {
    return ++(slip::stride_iterator<typename GenericMultiComponent2d<Block>::const_block_pointer>(&((*this)[row][range.stop()][component]), Block::SIZE * range.stride()));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_col_range_iterator 
  GenericMultiComponent2d<Block>::col_begin(const std::size_t component, const size_type col,
					    const slip::Range<int>& range)
  {
    return slip::stride_iterator<typename GenericMultiComponent2d<Block>::block_pointer>(&((*this)[range.start()][col][component]), Block::SIZE * this->dim2() * range.stride());
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_col_range_iterator 
  GenericMultiComponent2d<Block>::col_end(const std::size_t component, const size_type col,
					  const slip::Range<int>& range)
  { 
    return ++(slip::stride_iterator<typename GenericMultiComponent2d<Block>::block_pointer>(&((*this)[range.stop()][col][component]), Block::SIZE * this->dim2() * range.stride()));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_col_range_iterator 
  GenericMultiComponent2d<Block>::col_begin(const std::size_t component, const size_type col,
					    const slip::Range<int>& range) const
  {
    return slip::stride_iterator<typename GenericMultiComponent2d<Block>::const_block_pointer>(&((*this)[range.start()][col][component]), Block::SIZE * this->dim2() * range.stride());
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_col_range_iterator 
  GenericMultiComponent2d<Block>::col_end(const std::size_t component, const size_type col,
					  const slip::Range<int>& range) const
  {
    return ++(slip::stride_iterator<typename GenericMultiComponent2d<Block>::const_block_pointer>(&((*this)[range.stop()][col][component]), Block::SIZE * this->dim2() * range.stride()));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_row_range_iterator 
  GenericMultiComponent2d<Block>::row_rbegin(const std::size_t component, const size_type row,
					     const slip::Range<int>& range)
  { 
    return typename GenericMultiComponent2d<Block>::reverse_component_row_range_iterator(this->row_end(component,row,range));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_row_range_iterator 
  GenericMultiComponent2d<Block>::row_rend(const std::size_t component, const size_type row,
					   const slip::Range<int>& range)
  {
    return typename GenericMultiComponent2d<Block>::reverse_component_row_range_iterator(this->row_begin(component,row,range));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_row_range_iterator 
  GenericMultiComponent2d<Block>::row_rbegin(const std::size_t component, const size_type row,
					     const slip::Range<int>& range) const
  { 
    return typename GenericMultiComponent2d<Block>::const_reverse_component_row_range_iterator(this->row_end(component,row,range));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_row_range_iterator 
  GenericMultiComponent2d<Block>::row_rend(const std::size_t component, const size_type row,
					   const slip::Range<int>& range) const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_component_row_range_iterator(this->row_begin(component,row,range));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_col_range_iterator 
  GenericMultiComponent2d<Block>::col_rbegin(const std::size_t component, const size_type col,
					     const slip::Range<int>& range)
  { 
    return typename GenericMultiComponent2d<Block>::reverse_component_col_range_iterator(this->col_end(component,col,range));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_col_range_iterator 
  GenericMultiComponent2d<Block>::col_rend(const std::size_t component, const size_type col,
					   const slip::Range<int>& range)
  { 
    return typename GenericMultiComponent2d<Block>::reverse_component_col_range_iterator(this->col_begin(component,col,range));
  }
  
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_col_range_iterator 
  GenericMultiComponent2d<Block>::col_rbegin(const std::size_t component, const size_type col,
					     const slip::Range<int>& range) const
  { 
    return typename GenericMultiComponent2d<Block>::const_reverse_component_col_range_iterator(this->col_end(component,col,range));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_col_range_iterator 
  GenericMultiComponent2d<Block>::col_rend(const std::size_t component, const size_type col,
					   const slip::Range<int>& range) const
  { 
    return typename GenericMultiComponent2d<Block>::const_reverse_component_col_range_iterator(this->col_begin(component,col,range));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_iterator2d 
  GenericMultiComponent2d<Block>::upper_left(const std::size_t component)
  {
    return typename GenericMultiComponent2d<Block>::component_iterator2d(this,component,Box2d<int>(0,0,this->dim1()-1,this->dim2()-1));
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_iterator2d 
  GenericMultiComponent2d<Block>::bottom_right(const std::size_t component)
  {
    DPoint2d<int> dp(this->dim1(),this->dim2());
    typename GenericMultiComponent2d<Block>::component_iterator2d it = (*this).upper_left(component) + dp;
    return it;
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_iterator2d 
  GenericMultiComponent2d<Block>::upper_left(const std::size_t component) const
  {
    return typename GenericMultiComponent2d<Block>::const_component_iterator2d(this,component,Box2d<int>(0,0,this->dim1()-1,this->dim2()-1));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_iterator2d 
  GenericMultiComponent2d<Block>::bottom_right(const std::size_t component) const
  {
    DPoint2d<int> dp(this->dim1(),this->dim2());
    typename GenericMultiComponent2d<Block>::const_component_iterator2d it = (*this).upper_left(component) + dp;
    return it;
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_iterator2d
  GenericMultiComponent2d<Block>::rupper_left(const std::size_t component)
  { 
    DPoint2d<int> dp(1,0);
    return typename GenericMultiComponent2d<Block>::reverse_component_iterator2d(this->bottom_right(component) - dp);
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_iterator2d 
  GenericMultiComponent2d<Block>::rbottom_right(const std::size_t component)
  {
    return typename GenericMultiComponent2d<Block>::reverse_component_iterator2d(this->upper_left(component));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_iterator2d 
  GenericMultiComponent2d<Block>::rupper_left(const std::size_t component) const
  {
    DPoint2d<int> dp(1,0);
    return typename GenericMultiComponent2d<Block>::const_reverse_component_iterator2d(this->bottom_right(component) - dp);
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_iterator2d 
  GenericMultiComponent2d<Block>::rbottom_right(const std::size_t component) const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_component_iterator2d(this->upper_left(component));
  }
 

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_iterator2d  
  GenericMultiComponent2d<Block>::upper_left(const std::size_t component, 
					     const slip::Box2d<int>& box)
  {
    return typename GenericMultiComponent2d<Block>::component_iterator2d(this,component,box);
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_iterator2d  
  GenericMultiComponent2d<Block>::bottom_right(const std::size_t component, 
					       const slip::Box2d<int>& box)
  {
    DPoint2d<int> dp(box.height(),box.width());
    return (typename GenericMultiComponent2d<Block>::component_iterator2d((*this).upper_left(component,box) + dp));
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_iterator2d  
  GenericMultiComponent2d<Block>::upper_left(const std::size_t component, 
					     const slip::Box2d<int>& box) const
  {
    return typename GenericMultiComponent2d<Block>::const_component_iterator2d(this,component,box);
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_iterator2d  
  GenericMultiComponent2d<Block>::bottom_right(const std::size_t component, 
					       const slip::Box2d<int>& box) const
  {
    DPoint2d<int> dp(box.height(),box.width());
    return (typename GenericMultiComponent2d<Block>::const_component_iterator2d((*this).upper_left(component,box) + dp));
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_iterator2d_range  
  GenericMultiComponent2d<Block>::upper_left(const std::size_t component, 
					     const slip::Range<int>& row_range,
					     const slip::Range<int>& col_range)
  {
    return typename GenericMultiComponent2d<Block>::component_iterator2d_range(this,component,row_range,col_range);
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::component_iterator2d_range  
  GenericMultiComponent2d<Block>::bottom_right(const std::size_t component, 
					       const slip::Range<int>& row_range,
					       const slip::Range<int>& col_range)
  {
    DPoint2d<int> dp(row_range.iterations()+1,col_range.iterations()+1);
    return typename GenericMultiComponent2d<Block>::component_iterator2d_range((*this).upper_left(component,row_range,col_range) + dp);
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_iterator2d_range  
  GenericMultiComponent2d<Block>::upper_left(const std::size_t component, 
					     const slip::Range<int>& row_range,
					     const slip::Range<int>& col_range) const
  {
    return typename GenericMultiComponent2d<Block>::const_component_iterator2d_range(this,component,row_range,col_range);
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_component_iterator2d_range  
  GenericMultiComponent2d<Block>::bottom_right(const std::size_t component, 
					       const slip::Range<int>& row_range,
					       const slip::Range<int>& col_range) const
  {
    DPoint2d<int> dp(row_range.iterations()+1,col_range.iterations()+1);
    return typename GenericMultiComponent2d<Block>::const_component_iterator2d_range((*this).upper_left(component,row_range,col_range) + dp);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_iterator2d  
  GenericMultiComponent2d<Block>::rbottom_right(const std::size_t component, 
						const slip::Box2d<int>& box)
  {
    return typename GenericMultiComponent2d<Block>::reverse_component_iterator2d(this->upper_left(component,box));
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_iterator2d  
  GenericMultiComponent2d<Block>::rupper_left(const std::size_t component, 
					      const slip::Box2d<int>& box)
  {
    slip::DPoint2d<int> dp(1,0);
    return typename GenericMultiComponent2d<Block>::reverse_component_iterator2d(this->bottom_right(component,box) - dp);
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_iterator2d  
  GenericMultiComponent2d<Block>::rbottom_right(const std::size_t component, 
						const slip::Box2d<int>& box) const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_component_iterator2d(this->upper_left(component,box));
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_iterator2d  
  GenericMultiComponent2d<Block>::rupper_left(const std::size_t component, 
					      const slip::Box2d<int>& box) const
  {
    slip::DPoint2d<int> dp(1,0);
    return typename GenericMultiComponent2d<Block>::const_reverse_component_iterator2d(this->bottom_right(component,box) - dp);
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_iterator2d_range  
  GenericMultiComponent2d<Block>::rbottom_right(const std::size_t component, 
						const slip::Range<int>& row_range,
						const slip::Range<int>& col_range)
  {
    return typename GenericMultiComponent2d<Block>::reverse_component_iterator2d_range(this->upper_left(component,row_range,col_range));
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reverse_component_iterator2d_range  
  GenericMultiComponent2d<Block>::rupper_left(const std::size_t component, 
					      const slip::Range<int>& row_range,
					      const slip::Range<int>& col_range)
  {
    slip::DPoint2d<int> dp(1,0);
    return typename GenericMultiComponent2d<Block>::reverse_component_iterator2d_range(this->bottom_right(component,row_range,col_range) - dp);
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_iterator2d_range  
  GenericMultiComponent2d<Block>::rbottom_right(const std::size_t component, 
						const slip::Range<int>& row_range,
						const slip::Range<int>& col_range) const
  {
    return typename GenericMultiComponent2d<Block>::const_reverse_component_iterator2d_range(this->upper_left(component,row_range,col_range));
  }
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reverse_component_iterator2d_range  
  GenericMultiComponent2d<Block>::rupper_left(const std::size_t component, 
					      const slip::Range<int>& row_range,
					      const slip::Range<int>& col_range) const
  {
    slip::DPoint2d<int> dp(1,0);
    return typename GenericMultiComponent2d<Block>::const_reverse_component_iterator2d_range(this->bottom_right(component,row_range,col_range) - dp);
  }
 




  template <typename Block>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const GenericMultiComponent2d<Block>& a)
  {
    out<<*(a.matrix_)<<std::endl;
    return out;
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::pointer GenericMultiComponent2d<Block>::operator[](const typename GenericMultiComponent2d<Block>::size_type i)
  {
    return (*matrix_)[i];
  }
  
  template<typename Block>
  inline
   typename GenericMultiComponent2d<Block>::const_pointer GenericMultiComponent2d<Block>::operator[](const typename GenericMultiComponent2d<Block>::size_type i) const 
  {
    return (*matrix_)[i];
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reference GenericMultiComponent2d<Block>::operator()(const typename GenericMultiComponent2d<Block>::size_type i,
						    const typename GenericMultiComponent2d<Block>::size_type j)
  {
    return (*matrix_)[i][j];
  }
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reference GenericMultiComponent2d<Block>::operator()(const typename GenericMultiComponent2d<Block>::size_type i,
							  const typename GenericMultiComponent2d<Block>::size_type j) const
  {
    return (*matrix_)[i][j];
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::reference GenericMultiComponent2d<Block>::operator()(const Point2d<typename GenericMultiComponent2d<Block>::size_type>& point2d)
  {
    return (*matrix_)(point2d);
  }

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::const_reference GenericMultiComponent2d<Block>::operator()(const Point2d<typename GenericMultiComponent2d<Block>::size_type>& point2d) const
  {
    return (*matrix_)(point2d);
  }

  template<typename Block>
  inline
  GenericMultiComponent2d<Block> 
  GenericMultiComponent2d<Block>::operator()(const Range<int>& row_range,
					     const Range<int>& col_range)
  {
    assert((*this)[0] != 0);
    assert(row_range.is_valid());
    assert(row_range.start() < int(this->dim2()));
    assert(row_range.stop()  < int(this->dim2()));
    assert(col_range.is_valid());
    assert(col_range.start() < int(this->dim1()));
    assert(col_range.stop()  < int(this->dim1()));
    std::size_t rows = row_range.iterations();
    std::size_t cols = col_range.iterations();
    return GenericMultiComponent2d<Block>(rows+1,cols+1,matrix_->upper_left(row_range,col_range),
		       matrix_->bottom_right(row_range,col_range));
  } 
  
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::size_type GenericMultiComponent2d<Block>::dim1() const {return matrix_->dim1();} 
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::size_type GenericMultiComponent2d<Block>::height() const {return matrix_->dim1();} 
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::size_type GenericMultiComponent2d<Block>::rows() const {return matrix_->dim1();} 
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::size_type GenericMultiComponent2d<Block>::dim2() const {return matrix_->dim2();} 
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::size_type GenericMultiComponent2d<Block>::width() const {return matrix_->dim2();} 
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::size_type GenericMultiComponent2d<Block>::columns() const {return matrix_->dim2();} 

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::size_type GenericMultiComponent2d<Block>::cols() const {return matrix_->dim2();} 
 
  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::size_type GenericMultiComponent2d<Block>::size() const {return matrix_->size();}

  template<typename Block>
  inline
  typename GenericMultiComponent2d<Block>::size_type GenericMultiComponent2d<Block>::max_size() const {return matrix_->max_size();}

  
  template<typename Block>
  inline
  bool GenericMultiComponent2d<Block>::empty()const {return matrix_->empty();}

  template<typename Block>
  inline
  void GenericMultiComponent2d<Block>::swap(GenericMultiComponent2d<Block>& M)
  {
    matrix_->swap(*(M.matrix_));
  }



  template<typename Block>
  inline
  Block GenericMultiComponent2d<Block>::min() const
  {
    Block tmp;
    for(typename GenericMultiComponent2d<Block>::size_type component = 0; component < Block::SIZE; ++component)
      {
	typename Block::value_type min = *std::min_element(this->begin(component),this->end(component));
	tmp[component] = min;
      }
    return tmp;
  }


  template<typename Block>
  inline
  Block GenericMultiComponent2d<Block>::max() const
  {
    Block tmp;
    for(typename GenericMultiComponent2d<Block>::size_type component = 0; component < Block::SIZE; ++component)
      {
	typename Block::value_type max = *std::max_element(this->begin(component),this->end(component));
	tmp[component] = max;
      }
    return tmp;
  }


  template<typename Block>
  inline
  GenericMultiComponent2d<Block>& GenericMultiComponent2d<Block>::apply(Block(*fun)(Block))
  {
    matrix_->apply(fun);
    return *this;
  }

  template<typename Block>
  inline
  GenericMultiComponent2d<Block>& GenericMultiComponent2d<Block>::apply(Block(*fun)(const Block&))
  {
    matrix_->apply(fun);
    return *this;
  }


  template<typename Block>
  inline
  bool operator==(const GenericMultiComponent2d<Block>& x, 
		  const GenericMultiComponent2d<Block>& y)
  {
    return ( x.size() == y.size()
	     && std::equal(x.begin(),x.end(),y.begin())); 
  }
  
  template<typename Block>
  inline
  bool operator!=(const GenericMultiComponent2d<Block>& x, 
		  const GenericMultiComponent2d<Block>& y)
  {
    return !(x == y);
  }
  
  
  // template<typename Block>
// inline
// bool operator<(const GenericMultiComponent2d<Block>& x, 
// 	       const GenericMultiComponent2d<Block>& y)
// {
//   return std::lexicographical_compare(x.begin(), x.end(),
// 				      y.begin(), y.end());
// }

// template<typename Block>
// inline
// bool operator>(const GenericMultiComponent2d<Block>& x, 
// 	       const GenericMultiComponent2d<Block>& y)
// {
//   return (y < x);
// }


// template<typename Block>
// inline
// bool operator<=(const GenericMultiComponent2d<Block>& x, 
// 		const GenericMultiComponent2d<Block>& y)
// {
//   return !(y < x);
// }


// template<typename Block>
// inline
// bool operator>=(const GenericMultiComponent2d<Block>& x, 
// 		const GenericMultiComponent2d<Block>& y)
// {
//   return !(x < y);
// } 


 


}//slip::

#endif //SLIP_GENERICMULTICOMPONENT2D_HPP
