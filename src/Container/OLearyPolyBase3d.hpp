/** 
 * \file OLearyPolyBase3d.hpp
 * 
 * \brief Provides a class to handle approximation of 3d ranges using O'Leary orthonormal polynomials base.
 * 
 */
#ifndef SLIP_OLEARY_POLYBASE3D_HPP
#define SLIP_OLEARY_POLYBASE3D_HPP

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Polynomial.hpp"
#include "macros.hpp"
#include "Block.hpp"
#include "Vector.hpp"
#include "Matrix.hpp"
#include "Matrix3d.hpp"
#include "linear_algebra.hpp"
#include "polynomial_algo.hpp"
#include "discrete_polynomial_algos.hpp"

#include "OLearyPolyBase1d.hpp"

namespace slip
{
template <typename T>
class OLearyPolyBase3d;
  
template <typename T>
std::ostream& operator<<(std::ostream & out, 
			 const OLearyPolyBase3d<T>& a);
  
template <typename T>
class OLearyPolyBase3d;
  
/*! \class OLearyPolyBase3d
** 
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.1
** \date 2017/11/20
** \brief a class to handle approximation of 3d range using OLeary orthogonal polynomials base
** \param T Type of data to approximate.
*/
template <typename T>
class OLearyPolyBase3d
{
  
public:
  
  typedef OLearyPolyBase3d<T> self;
  typedef const self const_self;

   /**
   ** \name Constructors & Destructors
   */
  /*@{*
    /*!
    ** \brief Constructs a OLearyPolyBase1d.
    */
  OLearyPolyBase3d():
    x_range_size_(1),
    y_range_size_(1),
    z_range_size_(1),
    size_(0),
    degree_(0),
    x_base_(nullptr),
    y_base_(nullptr),
    z_base_(nullptr),
    poly_evaluations_(nullptr),
    base_storage_(slip::POLY_BASE_1D_RAM_STORAGE),
    powers_(nullptr)
  {
    this->allocate(slip::BASE_REORTHOGONALIZATION_QR_HOUSEHOLDER);
  }

   /*!
    ** \brief Constructs a OLearyPolyBase3d of degree (x_range_size * y_range_size*z_range_size).
    ** \param x_range_size x size of the 3d container or range.
    ** \param y_range_size y size of the 3d container or range.
    ** \param z_range_size z size of the 3d container or range.
    */
  OLearyPolyBase3d(const std::size_t& x_range_size,
		   const std::size_t& y_range_size,
		   const std::size_t& z_range_size):
    x_range_size_(x_range_size),
    y_range_size_(y_range_size),
    z_range_size_(z_range_size),
    size_(x_range_size*y_range_size*z_range_size),
    degree_(x_range_size+y_range_size+z_range_size),
    x_base_(nullptr),
    y_base_(nullptr),
    z_base_(nullptr),
    poly_evaluations_(new std::map<slip::block<std::size_t,3>, slip::Matrix3d<T> >()),
    base_storage_(slip::POLY_BASE_1D_RAM_STORAGE),
    powers_(new slip::Matrix<std::size_t>(this->size_,3))
  {
    this->allocate(slip::BASE_REORTHOGONALIZATION_QR_HOUSEHOLDER);
  }

  /*!
    ** \brief Constructs a OLearyPolyBase3d 
    ** \param x_range_size x size of the 3d container or range.
    ** \param y_range_size y size of the 3d container or range.  
    ** \param z_range_size z size of the 3d container or range.
   
    ** \param reortho_method Reorthogonalisation method:
    */
  OLearyPolyBase3d(const std::size_t& x_range_size,
		   const std::size_t& y_range_size,
		   const std::size_t& z_range_size,
		   const slip::BASE_REORTHOGONALIZATION_METHOD& reortho_method):
    x_range_size_(x_range_size),
    y_range_size_(y_range_size),
    z_range_size_(z_range_size),
    size_(x_range_size*y_range_size*z_range_size),
    degree_(x_range_size+y_range_size+z_range_size),
    x_base_(nullptr),
    y_base_(nullptr),
    z_base_(nullptr),
    poly_evaluations_(new std::map<slip::block<std::size_t,3>, slip::Matrix3d<T> >()),
    base_storage_(slip::POLY_BASE_1D_RAM_STORAGE),
    powers_(new slip::Matrix<std::size_t>(this->size_,3))
  {
    this->allocate(reortho_method);
  }

 /*!
    ** \brief Constructs a OLearyPolyBase3d 
    ** \param x_range_size x size of the 3d container or range.
    ** \param y_range_size y size of the 3d container or range. 
    ** \param z_range_size z size of the 3d container or range.
    ** \param reortho_method Reorthogonalisation method:
    ** \param base_storage Base storage type
    */
  OLearyPolyBase3d(const std::size_t& x_range_size,
		   const std::size_t& y_range_size,
		   const std::size_t& z_range_size,
		   const slip::BASE_REORTHOGONALIZATION_METHOD& reortho_method,
		   const slip::POLY_BASE_STORAGE& base_storage):
    x_range_size_(x_range_size),
    y_range_size_(y_range_size),
    z_range_size_(z_range_size),
    size_(x_range_size*y_range_size*z_range_size),
    degree_(x_range_size+y_range_size+z_range_size),
    x_base_(nullptr),
    y_base_(nullptr),
    z_base_(nullptr),
    poly_evaluations_(new std::map<slip::block<std::size_t,3>, slip::Matrix3d<T> >()),
    base_storage_(base_storage),
    powers_(new slip::Matrix<std::size_t>(this->size_,3))
  {
     this->allocate(reortho_method);
  }
 
  /*!
    ** \brief Copy constructor
    ** \param other %OLearyPolyBase3d.
    */
  OLearyPolyBase3d(const self& other):
    x_range_size_(other.x_range_size_),
    y_range_size_(other.y_range_size_),
    z_range_size_(other.z_range_size_),
    size_(other.size_),
    degree_(other.degree_),
    x_base_(new slip::OLearyPolyBase1d<T>(*other.x_base_)),
    y_base_(nullptr),
    z_base_(nullptr),
    poly_evaluations_(new std::map<slip::block<std::size_t,3>, slip::Matrix3d<T> >(*other.poly_evaluations_)),
    base_storage_(other.base_storage_),
    powers_(new slip::Matrix<std::size_t>(*other.powers_))
  {
    if(this->x_range_size_ == this->y_range_size_)
      {
	if(this->x_range_size_ == this->z_range_size_)
	  {
	    //x_range_size == y_range_size == z_range_size
	  }
	else
	  {
	    //x_range_size == y_range_size, z_range_size !=
	    this->z_base_ = new slip::OLearyPolyBase1d<T>(*other.z_base_);
	  }
      }
    else
      {
	if(this->x_range_size_ == this->z_range_size_)
	  {
	    //x_range_size == z_range_size, y_range_size !=
	    this->y_base_ = new slip::OLearyPolyBase1d<T>(*other.y_base_);
	  }
	else
	  {
	    if(this->y_range_size_ == this->z_range_size_)
	      {
		//z_range_size == y_range_size, x_range_size !=
		this->y_base_ = new slip::OLearyPolyBase1d<T>(*other.y_base_);
	
	      }
	    else
	      {
		//x_range_size != y_range_size != z_range_size
		this->y_base_ = new slip::OLearyPolyBase1d<T>(*other.y_base_);
		this->z_base_ = new slip::OLearyPolyBase1d<T>(*other.z_base_);
	       
	
	      }
	  }
      }
    // if(other.y_base_ != nullptr)
    //   {
    // 	this->y_base_ = new slip::OLearyPolyBase1d<T>(*other.y_base_);
    //   }
    // if(other.z_base_ != nullptr)
    //   {
    // 	this->z_base_ = new slip::OLearyPolyBase1d<T>(*other.z_base_);
    //   }
  }


  /*!
  ** \brief Destructor of the OLearyPolyBase3d
  */
  ~OLearyPolyBase3d()
  {
    this->desallocate();
  }
/*@} End Constructors */

    /*!
  ** \brief Assignement operator
  ** \param other %OLearyPolyBase3d
  */
  self& 
  operator=(const self& other)
  {
    if(this == &other)
      {
	return *this;
      }
    this->desallocate();
    this->x_range_size_ = other.x_range_size_;
    this->y_range_size_ = other.y_range_size_;
    this->z_range_size_ = other.z_range_size_;
    this->size_ = other.size_;
    this->degree_ = other.degree_;
    this->x_base_ = new slip::OLearyPolyBase1d<T>(*other.x_base_);
    if(this->x_range_size_ == this->y_range_size_)
      {
	if(this->x_range_size_ == this->z_range_size_)
	  {
	    //x_range_size == y_range_size == z_range_size
	  }
	else
	  {
	    //x_range_size == y_range_size, z_range_size !=
	    this->z_base_ = new slip::OLearyPolyBase1d<T>(*other.z_base_);
	  }
      }
    else
      {
	if(this->x_range_size_ == this->z_range_size_)
	  {
	    //x_range_size == z_range_size, y_range_size !=
	    this->y_base_ = new slip::OLearyPolyBase1d<T>(*other.y_base_);
	  }
	else
	  {
	    if(this->y_range_size_ == this->z_range_size_)
	      {
		//z_range_size == y_range_size, x_range_size !=
		this->y_base_ = new slip::OLearyPolyBase1d<T>(*other.y_base_);
	
	      }
	    else
	      {
		//x_range_size != y_range_size != z_range_size
		this->y_base_ = new slip::OLearyPolyBase1d<T>(*other.y_base_);
		this->z_base_ = new slip::OLearyPolyBase1d<T>(*other.z_base_);
	       
	
	      }
	  }
      }
    
    // this->y_base_ = nullptr;
    // this->z_base_ = nullptr;
    // if(other.y_base_ != nullptr)
    //   {
    // 	this->y_base_ = new slip::OLearyPolyBase1d<T>(*other.y_base_);
    //   }
    // if(other.z_base_ != nullptr)
    //   {
    // 	this->z_base_ = new slip::OLearyPolyBase1d<T>(*other.z_base_);
    //   }
    
    this->poly_evaluations_ = new std::map<slip::block<std::size_t,3>, slip::Matrix3d<T> >(*other.poly_evaluations_);
    this->base_storage_ = other.base_storage_;
    this->powers_ = new slip::Matrix<std::size_t>(*other.powers_);
    return *this;
  }


  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const
  {
    return "OLearyPolyBase3d";
  }


  /*!
  ** \brief Returns the x range size of the range
  **
  */
  std::size_t x_range_size() const
  {
    return this->x_range_size_;
  }

  /*!
  ** \brief Returns the  y range size of the range
  **
  */
  std::size_t y_range_size() const
  {
    return this->y_range_size_;
  }

  /*!
  ** \brief Returns the  z range size of the range
  **
  */
  std::size_t z_range_size() const
  {
    return this->z_range_size_;
  }
   /*!
  ** \brief Returns the size of the base (number of polynomials)
  **
  */
  std::size_t size() const
  {
    return this->size_;
  }

  /*!
  ** \brief Returns the degree of the base.
  **
  */
  std::size_t degree() const
  {
    return this->degree_;
  }

 const slip:: POLY_BASE_STORAGE& base_storage() const
  {
    return this->base_storage_;
  }

  const slip::BASE_REORTHOGONALIZATION_METHOD& reorthogonalization_method() const
  {
    return (this->x_base_)->reorthogonalization_method();
  }
  /**
   ** \name i/o operators
   */
  /*@{*/
  
  /*!
  ** \brief Write the %OLearyPolyBase3d to the ouput stream
  ** \param out output std::ostream
  ** \param a %OLearyPolyBase3d to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const self& a);
  /*@} End i/o operators */
  
   /*!
  ** \brief Generate the polynomial base.
  */
  void generate()
  {
    if(this->x_base_ != nullptr)
      {
	(this->x_base_)->generate();
      }
    if(this->x_range_size_ == this->y_range_size_)
      {
	if(this->x_range_size_ == this->z_range_size_)
	  {
	    //x_range_size == y_range_size == z_range_size
	  }
	else
	  {
	    //x_range_size == y_range_size, z_range_size !=
	    (this->z_base_)->generate();
	  }
      }
    else
      {
	if(this->x_range_size_ == this->z_range_size_)
	  {
	    //x_range_size == z_range_size, y_range_size !=
	    (this->y_base_)->generate();
	  }
	else
	  {
	    if(this->y_range_size_ == this->z_range_size_)
	      {
		//z_range_size == y_range_size, x_range_size !=
		(this->y_base_)->generate();
	
	      }
	    else
	      {
		//x_range_size != y_range_size != z_range_size
		(this->y_base_)->generate();
		(this->z_base_)->generate();
	
	      }
	  }
      }
    // if(this->y_base_ != nullptr)
    //   {
    // 	(this->y_base_)->generate();
    //   }
    // if(this->z_base_ != nullptr)
    //   {
    // 	(this->z_base_)->generate();
    //   }
    if(this->x_base_ != nullptr)
      {
    	this->compute_polynomials();
      }
    if(this->base_storage_ == slip::POLY_BASE_ND_RAM_STORAGE)
      {
	this->compute_3d_polynomials();
      }
    else
      {
	this->init_powers();
      }
  }
/*!
  ** \brief Computes the inner_product between Pi and Pj.
  ** \param i index of the first polynomial.
  ** \param j index of the second polynomial.
  ** \return the value of the inner product.
  ** \pre (i >  this->size_)
  ** \pre (j >  this->size_)
  */
  T inner_product(const std::size_t& i, const std::size_t& j) const
  {
    assert(i < this->size_);
    assert(j < this->size_);
    T result = T();

    slip::block<std::size_t, 3> ind_i;
    ind_i[0] = (*(this->powers_))[i][0];
    ind_i[1] = (*(this->powers_))[i][1];
    ind_i[2] = (*(this->powers_))[i][2];
    
    slip::block<std::size_t, 3> ind_j;
    ind_j[0] = (*(this->powers_))[j][0];
    ind_j[1] = (*(this->powers_))[j][1];
    ind_j[2] = (*(this->powers_))[j][2];
    if(this->base_storage_ == slip::POLY_BASE_ND_RAM_STORAGE)
      {
	
	typename std::map<slip::block<std::size_t,3>, slip::Matrix3d<T> >::iterator iti  = 
	  (this->poly_evaluations_)->find(ind_i);
	typename std::map<slip::block<std::size_t,3>, slip::Matrix3d<T> >::iterator itj  = 
	  (this->poly_evaluations_)->find(ind_j);
	result = std::inner_product((iti->second).begin(),(iti->second).end(),
				    (itj->second).begin(),T());								 
      }
    else
      {
	slip::Matrix3d<T> Pi(this->z_range_size_,
			     this->y_range_size_,
			     this->x_range_size_,
			     T());
	this->evaluate(ind_i[0],ind_i[1],ind_i[2],
		       Pi.front_upper_left(),
		       Pi.back_bottom_right());
	//std::cout<<"Pi("<<ind_i[0]<<","<<ind_i[1]<<","<<ind_i[2]<<") = \n"<<Pi<<std::endl;
	slip::Matrix3d<T> Pj(this->z_range_size_,
			     this->y_range_size_,
			     this->x_range_size_,
			     T());
	this->evaluate(ind_j[0],ind_j[1],ind_j[2],
		       Pj.front_upper_left(),
		       Pj.back_bottom_right());
	//std::cout<<"Pj("<<ind_j[0]<<","<<ind_j[1]<<","<<ind_j[2]<<") = \n"<<Pj<<std::endl;
	result = std::inner_product(Pi.begin(),Pi.end(),Pj.begin(),T());

      }
    return result;
			   
  }

 /*!
  ** \brief Computes the Gram matrix of the polynomial base.
  ** \param Gram The resulting Gram matrix.
  */
  template<typename Matrix>
  void orthogonality_matrix(Matrix& Gram) const
  {
    Gram.resize(this->size_,this->size_);
    for(std::size_t i = 0; i < this->size_; ++i)
      {
	for(std::size_t j = 0; j <= i; ++j)
	  {
	    Gram[i][j] =  (this->inner_product)(i,j);
	    Gram[j][i] = Gram[i][j];
	  }
      }
  }

 /*!
  ** \brief Indicate if the polynomial basis is orthogonal according to the tolerance \a tol.
  ** \param tol Tolerance of the orthogonality .
  ** \result True if orthogonal, false else.
  */
  bool is_orthogonal(const T tol 
		   = slip::epsilon<T>()) const
  {
    slip::Matrix<T> Gram;
    slip::OLearyPolyBase3d<T>::orthogonality_matrix(Gram);
    return slip::is_diagonal(Gram,tol);
  }

/*!
  ** \brief Returns the orthogonality precision of the base.
  ** If orthogonality precision > 1e-1 returns 1e-1.
  ** \return orthogonality precision 
  */
  T orthogonality_precision() const
  {
  bool is_ortho = false;
  T eps = slip::epsilon<T>(); 
  slip::Matrix<T> Gram;
  slip::OLearyPolyBase3d<T>::orthogonality_matrix(Gram);
  while( (eps < static_cast<T>(1e-1)) && (!is_ortho))
    {
      is_ortho = slip::is_diagonal(Gram,eps);
      eps*=static_cast<T>(10.0);
    }
  if(eps > 1e-1)
    {
      eps = static_cast<T>(1e-1);
    }
  else
    {
      eps /= static_cast<T>(10.0);
    }
  return eps;
  }

/*!
  ** \brief Returns Gram matrix condition number
  ** \return condition number
  */
  T gram_cond() const
  {
     slip::Matrix<T> Gram;
     slip::OLearyPolyBase3d<T>::orthogonality_matrix(Gram);
    return Gram.cond();
  }


  /*!
  ** \brief Project data onto the P(i,j) polynomial of the base.
  ** \param dup RandomAccessIterator3d on the data.
  ** \param bbot RandomAccessIterator3d on the data.
  ** \param k index of the x polynomial.
  ** \param i index of the y polynomial.
  ** \param j index of the z polynomial.
  */
  template <typename RandomAccessIterator3d>
  T project(RandomAccessIterator3d fup,
	    RandomAccessIterator3d bbot,
	    const std::size_t& k,
	    const std::size_t& i,
	    const std::size_t& j)
  {
    slip::block<std::size_t,3> pkij;
    pkij[0] = k;
    pkij[1] = i;
    pkij[2] = j;
    T result = T();
    if(base_storage_ == slip::POLY_BASE_ND_RAM_STORAGE)
      {
	typename std::map<slip::block<std::size_t,3>, slip::Matrix3d<T> >::iterator it  = (this->poly_evaluations_)->find(pkij); 
	
	assert(it != (this->poly_evaluations_)->end());
	
	result = std::inner_product((it->second).begin(),
				      (it->second).end(),
				      fup,
				      T());
     

      }
    else
      {
	 const std::size_t slices = static_cast<std::size_t>((bbot-fup)[0]);
	 const std::size_t rows   = static_cast<std::size_t>((bbot-fup)[1]);
	 slip::Array<T> tmpz(slices);
	 slip::Array<T> tmpy(rows);
	
	 result = this->project(fup,bbot,k,i,j,tmpy,tmpz);
      }
    return result;
   
  }

   /*!
  ** \brief Project data onto the polynomial of the base until degree \a degree_max.
  ** \param fup RandomAccessIterator3d on the data.
  ** \param bbot RandomAccessIterator3d on the data.
  ** \param x_degree_max x maximal degree of projection.
  ** \param y_degree_max y maximal degree of projection.
  ** \param z_degree_max z maximal degree of projection.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \pre (coef_last - coef_first) <= this->size()
  ** \pre degree_max <= this->degree()
  */
  template <typename RandomAccessIterator3d,
  	    typename RandomAccessIterator>
  void project(RandomAccessIterator3d fup,
  	       RandomAccessIterator3d bbot,
  	       const std::size_t& x_degree_max,
	       const std::size_t& y_degree_max,
	       const std::size_t& z_degree_max,
  	       RandomAccessIterator coef_first,
  	       RandomAccessIterator coef_last)
  {
     const std::size_t slices = static_cast<std::size_t>((bbot-fup)[0]);
     slip::Array<T> tmpz(slices);
     const std::size_t rows = static_cast<std::size_t>((bbot-fup)[1]);
     slip::Array<T> tmpy(rows);

    for(std::size_t k = 0; k <= x_degree_max; ++k)
      {
	for(std::size_t i= 0; i <= y_degree_max; ++i)
	  {
	    for(std::size_t j= 0; j <= z_degree_max; ++j, ++coef_first)
	      {
		*coef_first = this->project(fup,bbot,k,i,j,tmpy,tmpz);
		//std::cout<<"(k,i,j) = ("<<k<<","<<i<<","<<j<<")"<<std::endl;
	      }
	  } 
      }

  }

/*!
  ** \brief Reconstruct the data from a coefficients range.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \param x_degree x degree.
  ** \param y_degree y degree.
  ** \param z_degree z degree.
  ** \param fup RandomAccessIterator3d on the data.
  ** \param bbot RandomAccessIterator3d on the data.
  ** \pre x_degree <= x_range_size
  ** \pre y_degree <= y_range_size
  ** \pre z_degree <= z_range_size
  ** 
  */
  template <typename RandomAccessIterator,
	     typename RandomAccessIterator3d>
  void reconstruct(RandomAccessIterator coef_first,
		   RandomAccessIterator coef_last,
		   const std::size_t& x_degree,
		   const std::size_t& y_degree,
		   const std::size_t& z_degree,
		   RandomAccessIterator3d fup,
		   RandomAccessIterator3d bbot)
  {
    assert(x_degree <= this->x_range_size_); 
    assert(y_degree <= this->y_range_size_);
    assert(z_degree <= this->z_range_size_);
    std::fill(fup,bbot,T());
    const std::size_t slices = static_cast<std::size_t>((bbot-fup)[0]);
    const std::size_t rows = static_cast<std::size_t>((bbot-fup)[1]);
    const std::size_t cols = static_cast<std::size_t>((bbot-fup)[2]);
    
    slip::DPoint3d<int> d;
    if(this->base_storage_ == slip::POLY_BASE_1D_RAM_STORAGE)
      {
	slip::Matrix3d<T> result(slices,rows,cols,T());

	    for(std::size_t x = 0; x <= x_degree; ++x)
	      {
		for(std::size_t y = 0; y <= y_degree; ++y)
		  {
		    for(std::size_t z = 0; z <= z_degree; ++z,++coef_first)
		      {
			slip::rank1_tensorial_product((this->z_base_)->poly_rbegin(z),
						      (this->z_base_)->poly_rend(z),
						      (this->y_base_)->poly_rbegin(y),
						      (this->y_base_)->poly_rend(y),
						      (this->x_base_)->poly_begin(x),
						      (this->x_base_)->poly_end(x),
						      result.front_upper_left(),
						      result.back_bottom_right());
			for(std::size_t k = 0; k < slices; ++k)
			  {
			    d[0] = k;
			    for(std::size_t i = 0; i < rows; ++i)
			      {
				d[1] = i;
				for(std::size_t j = 0; j < cols; ++j)
				  {
				    d[2] = j;
				    fup[d] += *coef_first *result[k][i][j];
			 
				  }
			      }
			  }
		      }
		  }
	      }
      }
    else
      {
	slip::block<std::size_t,3> pkij;
	for(std::size_t x = 0; x <= x_degree; ++x)
	  {
	    pkij[0] = x;
	    for(std::size_t y = 0; y <= y_degree; ++y)
	      {
		pkij[1] = y;
		for(std::size_t z = 0; z <= z_degree; ++z,++coef_first)
		  {
		    pkij[2] = z;
		    //	std::cout<<"(x,y,z) = ("<<x<<","<<y<<","<<z<<"), coeff_first = "<<*coef_first<<std::endl;
		    //std::cout<<"pkij = "<<pkij<<std::endl;
		    typename std::map<slip::block<std::size_t,3>, slip::Matrix3d<T> >::iterator it  =  (this->poly_evaluations_)->find(pkij);
		    //	std::cout<<(it->second)<<std::endl;
		    for(std::size_t k = 0; k < slices; ++k)
		      {
			d[0] = k;
			for(std::size_t i = 0; i < rows; ++i)
			  {
			    d[1] = i;
			    for(std::size_t j = 0; j < cols; ++j)
			      {
				d[2] = j;
				fup[d] += *coef_first *(it->second)[k][i][j];
				    
			      }
			  }
		      }
		  
		  }
	      }
	  }
      }
    
  }
  
  
protected:
  std::size_t x_range_size_; ///number of columns of the container3d or range
  std::size_t y_range_size_;///number of rows of the container3d or range
  std::size_t z_range_size_;///number of slices of the container3d or range
  std::size_t size_;///size of the base : number of polynomials
  int degree_;///degree of the base
  slip::OLearyPolyBase1d<T>* x_base_;
  slip::OLearyPolyBase1d<T>* y_base_;
  slip::OLearyPolyBase1d<T>* z_base_;
  std::map<slip::block<std::size_t,3>, slip::Matrix3d<T> >* poly_evaluations_; ///map which associate an %Matrix<T> containning polynomial evaluation at each range site to a block containing 3 polynomial indices
  slip::POLY_BASE_STORAGE base_storage_;
  slip::Matrix<std::size_t>* powers_; /// size_ x 3 matrix containing the spatial polynomial degrees for each polynomial of the base
private:

  void allocate(const slip::BASE_REORTHOGONALIZATION_METHOD& reortho_method)
  {
    this->x_base_ = new slip::OLearyPolyBase1d<T>(this->x_range_size_,reortho_method);
    //  if(this->x_range_size_ == this->y_range_size_)
    //   {
    // 	if(this->x_range_size_ != this->z_range_size_)
    // 	  {
    // 	    this->z_base_ = new slip::OLearyPolyBase1d<T>(this->z_range_size_,reortho_method);
    // 	  }
    // 	else
    // 	  {
    // 	    this->y_base_ = new slip::OLearyPolyBase1d<T>(this->y_range_size_,reortho_method);
    // 	  }
    //   }
    // else
    //   {
    // 	if(this->x_range_size_ != this->z_range_size_)
    // 	  {
    // 	    if(this->y_range_size_ != this->z_range_size_)
    // 	      {
    // 		this->y_base_ = new slip::OLearyPolyBase1d<T>(this->y_range_size_,reortho_method);
    // 		this->z_base_ = new slip::OLearyPolyBase1d<T>(this->z_range_size_,reortho_method);
    // 	      }
    // 	    else
    // 	      {
    // 		this->y_base_ = new slip::OLearyPolyBase1d<T>(this->y_range_size_,reortho_method);
    // 	      }
    // 	  }
    // 	else
    // 	  {
    // 	    this->y_base_ = new slip::OLearyPolyBase1d<T>(this->y_range_size_,reortho_method);
    // 	  }
    //   }
    if(this->x_range_size_ == this->y_range_size_)
      {
	if(this->x_range_size_ == this->z_range_size_)
	  {
	    //x_range_size == y_range_size == z_range_size
	    this->y_base_ = this->x_base_;
	    this->z_base_ = this->x_base_;
	  }
	else
	  {
	    //x_range_size == y_range_size, z_range_size !=
	    this->y_base_ = this->x_base_;
	    this->z_base_ = new slip::OLearyPolyBase1d<T>(this->z_range_size_,reortho_method);
	  }
      }
    else
      {
	if(this->x_range_size_ == this->z_range_size_)
	  {
	    //x_range_size == z_range_size, y_range_size !=
	    this->z_base_ = this->x_base_;
	    this->y_base_ = new slip::OLearyPolyBase1d<T>(this->y_range_size_,reortho_method);
	  }
	else
	  {
	    if(this->y_range_size_ == this->z_range_size_)
	      {
		//z_range_size == y_range_size, x_range_size !=
		 this->y_base_ = new slip::OLearyPolyBase1d<T>(this->y_range_size_,reortho_method);
		 this->z_base_ = this->y_base_;
	      }
	    else
	      {
		//x_range_size != y_range_size != z_range_size
		 this->y_base_ = new slip::OLearyPolyBase1d<T>(this->y_range_size_,reortho_method);
		 this->z_base_ = new slip::OLearyPolyBase1d<T>(this->z_range_size_,reortho_method);
	      }
	  }
      }
  }
   /*!
  ** \brief Desallocate the polynomial base data.
  */
  void desallocate()
  {
    //  if(this->x_base_ != nullptr)
    //   {
    // 	delete this->x_base_;
    //   }
    // if(this->y_base_ != nullptr)
    //   {
    // 	delete this->y_base_;
    //   }
    //  if(this->z_base_ != nullptr)
    //   {
    // 	delete this->z_base_;
    //   }
    // if(this->poly_evaluations_ != nullptr)
    //   {
    // 	delete this->poly_evaluations_;
    //   }

    // if(this->powers_ != nullptr)
    //   {
    // 	delete this->powers_;
    //   }
    if(this->x_range_size_ == this->y_range_size_)
      {
	if(this->x_range_size_ == this->z_range_size_)
	  {
	    //x_range_size == y_range_size == z_range_size
	  }
	else
	  {
	    //x_range_size == y_range_size, z_range_size !=
	    delete this->z_base_;
	  }
      }
    else
      {
	if(this->x_range_size_ == this->z_range_size_)
	  {
	    //x_range_size == z_range_size, y_range_size !=
	    delete this->y_base_;
	  }
	else
	  {
	    if(this->y_range_size_ == this->z_range_size_)
	      {
		//z_range_size == y_range_size, x_range_size !=
		delete this->y_base_;
	      }
	    else
	      {
		//x_range_size != y_range_size != z_range_size
		
		delete this->y_base_;
		delete this->z_base_;
	      }
	  }
      }
    if(this->x_base_ != nullptr)
      {
	delete this->x_base_;
      }

     if(this->poly_evaluations_ != nullptr)
      {
    	delete this->poly_evaluations_;
      }

    if(this->powers_ != nullptr)
      {
    	delete this->powers_;
      }
  }
  
 
  void compute_3d_polynomials()
  {
    slip::Matrix3d<T> P(this->z_range_size_,
			this->y_range_size_,
			this->x_range_size_);
    slip::block<std::size_t,3> indices;
   
    for(std::size_t k = 0, kk = 0; k < this->x_range_size_; ++k)
      {
	for(std::size_t i = 0; i < this->y_range_size_; ++i)
	  {
	    for(std::size_t j = 0; j < this->z_range_size_; ++j,++kk)
	      {
		(*(this->powers_))[kk][0] = k;
		(*(this->powers_))[kk][1] = i;
		(*(this->powers_))[kk][2] = j;
		indices[0] = k;
		indices[1] = i;
		indices[2] = j;
		//outer_product(Pk,Pi,Pj)
		slip::rank1_tensorial_product((this->z_base_)->poly_rbegin(j),
					      (this->z_base_)->poly_rend(j),
					      (this->y_base_)->poly_rbegin(i),
					      (this->y_base_)->poly_rend(i),
					      (this->x_base_)->poly_begin(k),
					      (this->x_base_)->poly_end(k),
					      P.front_upper_left(),
					      P.back_bottom_right());
		(*this->poly_evaluations_)[indices] = P;
	      }
	  }
      }
	 
  }
  

  void init_powers()
  {
     for(std::size_t k = 0, kk = 0; k < this->x_range_size_; ++k)
       {
	 for(std::size_t i = 0; i < this->y_range_size_; ++i)
	   {
	     for(std::size_t j = 0; j < this->z_range_size_; ++j,++kk)
	       {
		 (*(this->powers_))[k][0] = k;
		 (*(this->powers_))[k][1] = i;
		 (*(this->powers_))[k][2] = j;
		 
	       }
	   }
       }
  }
  /*!
  ** \brief Computes the polynomial basis.
  */
  void compute_polynomials()
  {
     switch(this->base_storage())
	{
	case slip::POLY_BASE_1D_RAM_STORAGE:
	  break;
	case slip::POLY_BASE_ND_RAM_STORAGE:  
	  this->compute_3d_polynomials();
	  break;
	default:
	  this->compute_3d_polynomials();
	  break;
	};
  }
  template <typename RandomAccessIterator3d>
  T project(RandomAccessIterator3d fup,
	    RandomAccessIterator3d bbot,
	    const std::size_t& k,
	    const std::size_t& i,
	    const std::size_t& j,
	    slip::Array<T>& tmpy,
	    slip::Array<T>& tmpz)
  {
    
    const std::size_t rows   = tmpy.size();
    const std::size_t slices = tmpz.size();
   
    
    for(std::size_t s = 0; s < slices; ++s)
      {
	for(std::size_t r = 0; r < rows; ++r)
	  {
	    tmpy[r] =  (this->x_base_)->project(fup.row_begin(s,r),fup.row_end(s,r),k);
	  }
	tmpz[s] =  (this->y_base_)->project(tmpy.rbegin(),tmpy.rend(),i);
      }
    
    return (this->z_base_)->project(tmpz.rbegin(),tmpz.rend(),j);
    }

  template <typename RandomAccessIterator3d>
  void evaluate(const std::size_t& k,
  		const std::size_t& i,
  		const std::size_t& j,
  		RandomAccessIterator3d fup,
  		RandomAccessIterator3d bbot) const
  {

    slip::rank1_tensorial_product((this->z_base_)->poly_rbegin(j),
				  (this->z_base_)->poly_rend(j),
				  (this->y_base_)->poly_rbegin(i),
				  (this->y_base_)->poly_rend(i),
				  (this->x_base_)->poly_begin(k),
				  (this->x_base_)->poly_end(k),
				  fup,bbot);
  }
  
  };
}//::slip


namespace slip
{
  template <typename T>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const OLearyPolyBase3d<T>& b)
  {
    out<<"Number of polynomials = "<<b.size()<<std::endl;
    out<<"Base degree = "<<b.degree()<<std::endl;
     
    out<<"x range size = "<<b.x_range_size()<<std::endl;
    out<<"y range size = "<<b.y_range_size()<<std::endl;
    out<<"z range size = "<<b.z_range_size()<<std::endl;
    
     out<<"Reorthogonalization method:\n";
      switch((b.x_base_)->reorthogonalization_method())
      {
      case slip::BASE_REORTHOGONALIZATION_NONE:
	out<<"none"<<std::endl;
	break;
      case slip::BASE_REORTHOGONALIZATION_GRAM_SCHMIDT:
	out<<"Gram-Schmidt"<<std::endl;
	break;
      case slip::BASE_REORTHOGONALIZATION_QR_HOUSEHOLDER:
	out<<"QR Householder"<<std::endl;
	break;
      case slip::BASE_REORTHOGONALIZATION_SVD:
	out<<"SVD"<<std::endl;
	break;
      case slip::BASE_REORTHOGONALIZATION_PROCRUSTES:
	out<<"Procrustes"<<std::endl;
	break;
      };
      out<<"Base storage method:\n";
      switch(b.base_storage())
	{
	case slip::POLY_BASE_1D_RAM_STORAGE:
	  out<<"1D RAM storage"<<std::endl;
	  break;
	case slip::POLY_BASE_ND_RAM_STORAGE:  
	  out<<"ND RAM storage"<<std::endl;
	  break;
	case slip::POLY_BASE_ND_FILE_STORAGE:  
	  out<<"ND FILE storage"<<std::endl;
	  break;
	};
      if(b.x_base_ != nullptr)
	{
	  out<<"----------"<<std::endl;
	  out<<"x base:\n"<<*(b.x_base_)<<std::endl;
	  out<<"----------"<<std::endl;
	}
      if(b.y_base_ != nullptr)
	{
	  out<<"y base:\n"<<*(b.y_base_)<<std::endl;
	  out<<"----------"<<std::endl;
	}
      if(b.z_base_ != nullptr)
	{
	  out<<"z base:\n"<<*(b.z_base_)<<std::endl;
	  out<<"----------"<<std::endl;
	}
     
     if(b.poly_evaluations_ != nullptr)
       {
	 typename std::map<slip::block<std::size_t,3>, slip::Matrix3d<T> >::iterator it  = 
	   (b.poly_evaluations_)->begin();
    
	 for(;it!=(b.poly_evaluations_)->end();++it)
	   {
	     out<<"P"<<(*it).first<<":"<<std::endl;
	     out<<(*it).second<<std::endl;
	     out<<std::endl;
	   }
    
       }
     
    return out;
  }
}//::slip

#endif //SLIP_OLEARY_POLYBASE3D_HPP
