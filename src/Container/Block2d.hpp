/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Block2d.hpp
 * 
 * \brief Provides a class to manipulate 2d static and generic arrays.
 * 
 */

#ifndef SLIP_BLOCK2D_HPP
#define SLIP_BLOCK2D_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <string>
#include <cstddef>
#include "Box2d.hpp"
#include "Point2d.hpp"
#include "DPoint2d.hpp"
#include "Range.hpp"
#include "kstride_iterator.hpp"
#include "stride_iterator.hpp"
#include "iterator2d_box.hpp"
#include "iterator2d_range.hpp"
#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template<class T, int NC>
class kstride_iterator;

template<class T>
class iterator2d_box;

template<class T>
class iterator2d_range;

template<class T>
class const_iterator2d_box;

template<class T>
class const_iterator2d_range;


template <class T>
class DPoint2d;

template <class T>
class Point2d;

template <typename T, std::size_t NR, std::size_t NC>
struct block2d;

template <typename T, std::size_t NR, std::size_t NC>
std::ostream& operator<<(std::ostream & out,
			 const slip::block2d<T,NR,NC>& b);

template <typename T, std::size_t NR, std::size_t NC>
bool operator==(const slip:: block2d<T,NR,NC>& x, 
		const slip::block2d<T,NR,NC>& y);

template <typename T, std::size_t NR, std::size_t NC>
bool operator!=(const slip::block2d<T,NR,NC>& x, 
		const slip::block2d<T,NR,NC>& y);

template <typename T, std::size_t NR, std::size_t NC>
bool operator<(const slip::block2d<T,NR,NC>& x, 
	       const slip::block2d<T,NR,NC>& y);

template <typename T, std::size_t NR, std::size_t NC>
bool operator>(const slip::block2d<T,NR,NC>& x, 
	       const slip::block2d<T,NR,NC>& y);

template <typename T, std::size_t NR, std::size_t NC>
bool operator<=(const slip::block2d<T,NR,NC>& x, 
		const slip::block2d<T,NR,NC>& y);

template <typename T, std::size_t NR, std::size_t NC>
bool operator>=(const slip::block2d<T,NR,NC>& x, 
		const slip::block2d<T,NR,NC>& y);

/*! \struct block2d
**  \ingroup Containers Containers2d
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/03/15
** \since 1.0.0
** \brief This is a two-dimensional static and generic container.
** This container statisfies the BidirectionnalContainer concepts of
** the STL.
** It is also an 2d extension of the RandomAccessContainer concept. That is
** to say the bracket element access is replaced by the double bracket element 
** access.
** \param T Type of object in the %block2d 
** \param NR number of row of the %block2d
** \param NC number of column of the %block2d
** \par Axis conventions:
** \image html iterator2d_conventions.jpg "axis and notation conventions"
** \image latex iterator2d_conventions.eps "axis and notation conventions" width=5cm
**
*/
template <typename T, std::size_t NR, std::size_t NC>
struct block2d
{
  typedef T value_type;
  typedef block2d<T,NR,NC> self;
  typedef const block2d<T,NR,NC> const_self;
  
  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;

  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;


  typedef pointer row_iterator;
  typedef const_pointer const_row_iterator;
  typedef slip::kstride_iterator<pointer,NC> col_iterator;
  typedef slip::kstride_iterator<const_pointer,NC> const_col_iterator;


  
  typedef slip::iterator2d_box<self> iterator2d;
  typedef slip::const_iterator2d_box<const_self> const_iterator2d;
 
  typedef slip::stride_iterator<pointer> row_range_iterator;
  typedef slip::stride_iterator<const_pointer> const_row_range_iterator;
  typedef slip::stride_iterator<col_iterator> col_range_iterator;
  typedef slip::stride_iterator<const_col_iterator> const_col_range_iterator;
  typedef slip::iterator2d_range<self> iterator2d_range;
  typedef slip::const_iterator2d_range<const_self> const_iterator2d_range;


  typedef std::reverse_iterator<iterator> reverse_row_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
  typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
  typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;
   typedef std::reverse_iterator<iterator2d> reverse_iterator2d;
  typedef std::reverse_iterator<const_iterator2d> const_reverse_iterator2d;
  typedef std::reverse_iterator<row_range_iterator> reverse_row_range_iterator;
  typedef std::reverse_iterator<const_row_range_iterator> const_reverse_row_range_iterator;
  typedef std::reverse_iterator<col_range_iterator> reverse_col_range_iterator;
  typedef std::reverse_iterator<const_col_range_iterator> const_reverse_col_range_iterator;
  typedef std::reverse_iterator<iterator2d_range> reverse_iterator2d_range;
  typedef std::reverse_iterator<const_iterator2d_range> const_reverse_iterator2d_range;
  

  //default iterator of the container
  typedef iterator2d default_iterator;
  typedef const_iterator2d const_default_iterator;

  /// Size of the block2d
  static const std::size_t SIZE = NR * NC;

  static const std::size_t DIM = 2;
/**
   ** \name iterators
   */
  /*@{*/
 
 
 
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %block2d.  Iteration is done in ordinary
  **  element order.
  ** \return begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  **  slip::block2d<double,10,9> S;
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  iterator begin() {return data;}
  

 
  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %block2d.  Iteration is done in ordinary
  **  element order.
  ** 
  ** \return const begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  **  slip::block2d<double,10,9> S;
  ** //copy the sum of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_iterator begin() const {return data;}

 
  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %block2d.  Iteration is done in ordinary
  **  element order.
  ** \return end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  **  slip::block2d<double,10,9> S;
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  iterator end() {return data + SIZE;}
 
 
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %block2d.  Iteration is done in
  **  ordinary element order.
  ** \return const end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  **  slip::block2d<double,10,9> S;
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_iterator end() const {return data + SIZE;}


  /*!
  **  \brief Returns a read/write reverse iterator that points to the
  **  last element in the %block2d.  Iteration is done in reverse
  **  element order.
  ** \return reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  **  slip::block2d<double,10,9> S;
  ** //copy the sum of the elements of A1 and A2 
  ** //in S (iterated in the reverse order element)
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.rbegin(),
  **                std::plus<double>());
  ** \endcode
  */
  reverse_iterator rbegin() {return reverse_iterator(this->end());}

   /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to the last element in the %block2d.  Iteration is done in
  **  reverse element order.
  ** \return const reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  **  slip::block2d<double,10,9> S;
  ** //copy the sum of the elements of A1 (iterated in the reverse order element)
  ** //and A2 in S 
  ** std::transform(A1.rbegin(),A1.rend(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_reverse_iterator rbegin() const {return const_reverse_iterator(this->end());}

   /*!
  **  \brief Returns a read/write reverse iterator that points to one
  **  before the first element in the %block2d.  Iteration is done
  **  in reverse element order.
  **  \return reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  **  slip::block2d<double,10,9> S;
  ** //copy the sum of the elements of A1 and A2 
  ** //in S (iterated in the reverse order element)
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.rbegin(),
  **                std::plus<double>());
  ** \endcode
  */
  reverse_iterator rend() {return reverse_iterator(this->begin());}

  
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to one before the first element in the %block2d.  Iteration
  **  is done in reverse element order.
  **  \return const reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  **  slip::block2d<double,10,9> S;
  ** //copy the sum of the elements of A1 (iterated in the reverse order element)
  ** //and A2 in S 
  ** std::transform(A1.rbegin(),A1.rend(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_reverse_iterator rend() const {return const_reverse_iterator(this->begin());}
  


   /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the row \a row in the %block2d.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  row_iterator row_begin(const size_type row) 
  {
    return this->data + NC * row;
  }

  
  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the row \a row in the %block2d.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  const_row_iterator row_begin(const size_type row) const
  {
    return this->data + NC * row;
  }


   /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the column \a column in the %block2d.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  col_iterator col_begin(const size_type col)
  {
    return col_iterator(this->data + col);    
  }


  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the column \a column in the %block2d.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin const_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  const_col_iterator col_begin(const size_type col) const
  {
    return const_col_iterator(this->data + col);
  }

   /*!
  **  \brief Returns a read/write iterator that points one past
  **  the end element of the row \a row in the %block2d.  
  **  Iteration is done in ordinary element order.
  ** \param row  The index of the row to iterate.
  ** \return begin row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  row_iterator row_end(const size_type row)
  {
    return  this->row_begin(row) + NR - 1;
  }


    /*!
  **  \brief Returns a read-only iterator that points one past
  **  the end element of the row \a row in the %block2d.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  const_row_iterator row_end(const size_type row) const
  {
     return  this->row_begin(row) + NR - 1;
  }

   /*!
  **  \brief Returns a read/write iterator that points one past
  **  the end element of the column \a column in the %block2d.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  col_iterator col_end(const size_type col)
  {
    return col_iterator(this->data + NR * (NR - 1) + col);  
  }


  /*!
  **  \brief Returns a read-only iterator that points one past
  **  the end element of the column \a column in the %block2d.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column 
  ** \return begin const_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,10,9> A1;
  **  slip::block2d<double,10,9> A2;
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  const_col_iterator col_end(const size_type col) const
  {
    return const_col_iterator(this->data + NR *(NR - 1) + col);
  }



   /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the %Range \a range of the row \a row in the %block2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return begin row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,8,4> A2;
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  row_range_iterator row_begin(const size_type row,
			       const slip::Range<int>& range)
  {
    assert(row < NR);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < int(NC));
    assert(range.stop()  < int(NC));
    return slip::stride_iterator<typename block2d<T,NR,NC>::row_iterator>(this->row_begin(row) + range.start(),range.stride());
  }

   /*!
  **  \brief Returns a read/write iterator that points one past the end
  **  element of the %Range \a range of the row \a row in the %block2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return end row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,8,4> A2;
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  row_range_iterator row_end(const size_type row,
			     const slip::Range<int>& range)
  {
    assert(row < NR);
    assert(range.start() < int(NC));
    assert(range.stop()  < int(NC));
    return ++(slip::stride_iterator<typename block2d<T,NR,NC>::row_iterator>(this->row_begin(row) + range.start() + range.iterations() * range.stride(),range.stride()));
  }

   /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the row \a row in the %block2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return begin const_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,8,4> A2;
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  const_row_range_iterator row_begin(const size_type row,
				     const slip::Range<int>& range) const
  {
    assert(row < NR);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < int(NC));
    assert(range.stop()  < int(NC));
    return slip::stride_iterator<typename block2d<T,NR,NC>::const_row_iterator>(this->row_begin(row) + range.start(),range.stride());
  }



  /*!
  **  \brief Returns a read_only iterator that points one past the last
  **  element of the %Range range of the row \a row in the %block2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row Row to iterate.
  ** \param range %Range of the row to iterate
  ** \return begin const_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \pre row must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,8,4> A2;
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  const_row_range_iterator row_end(const size_type row,
				   const slip::Range<int>& range) const
  {
    assert(row < NR);
    assert(range.start() < int(NC));
    assert(range.stop()  < int(NC));
    return ++(slip::stride_iterator<typename block2d<T,NR,NC>::const_row_iterator>(this->row_begin(row) + range.start() + range.stride() * range.iterations(),range.stride()));
  }


  /*!
  **  \brief Returns a read-write iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %block2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate
  ** \return begin col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,4,8> A2;
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  col_range_iterator col_begin(const size_type col,
			       const slip::Range<int>& range)
  {
    assert(col < NC);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < int(NR));
    assert(range.stop()  < int(NR));
    return slip::stride_iterator<typename block2d<T,NR,NC>::col_iterator>(this->col_begin(col) + range.start(),range.stride());
  }

  /*!
  **  \brief Returns a read-write iterator that points to the past
  **  the end element of the %Range \a range of the col \a col in the 
  **  %block2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,4,8> A2;
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  col_range_iterator col_end(const size_type col,
			     const slip::Range<int>& range)
  {
    assert(col < NC);
    assert(range.start() < int(NR));
    assert(range.stop()  < int(NR));
    return ++(slip::stride_iterator<typename block2d<T,NR,NC>::col_iterator>(this->col_begin(col) + range.start() + range.stride() * range.iterations(),range.stride()));
  }




  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %block2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin const_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,4,8> A2;
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  const_col_range_iterator col_begin(const size_type col,
				     const slip::Range<int>& range) const
  {
    assert(col < NC);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < int(NR));
    assert(range.stop()  < int(NR));
    return slip::stride_iterator<typename block2d<T,NR,NC>::const_col_iterator>(this->col_begin(col) + range.start(),range.stride());
  }

 
  /*!
  **  \brief Returns a read-only iterator that points to the past
  **  the end element of the %Range \a range of the col \a col in 
  **  the %block2d.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate
  ** \return begin const_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,4,8> A2;
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  const_col_range_iterator col_end(const size_type col,
				   const slip::Range<int>& range) const
  {
    assert(col < NC);
    assert(range.start() < int(NR));
    assert(range.stop()  < int(NR));
    return ++(slip::stride_iterator<typename block2d<T,NR,NC>::const_col_iterator>(this->col_begin(col) + range.start() + range.stride() * range.iterations(),range.stride()));
  }

  

   /*!
  **  \brief Returns a read/write reverse iterator that points to the 
  **  last element of the row \a row in the %block2d.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return begin reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %block2d.
  */
  reverse_row_iterator row_rbegin(const size_type row)
  {
     return reverse_row_iterator(this->row_end(row));
  }


  /*!
  **  \brief Returns a read-only reverse iterator that points to the last
  **  element of the row \a row in the %block2d.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %block2d.
  */
  const_reverse_row_iterator row_rbegin(const size_type row) const
  {
    return const_reverse_iterator(this->row_end(row));
  }

  /*!
  **  \brief Returns a read/write reverse iterator that points to the last
  **  element of the column \a column in the %block2d.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return begin col_reverse_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  */
  reverse_col_iterator col_rbegin(const size_type col)
  {
    return reverse_col_iterator(this->col_end(col));
  }

   /*!
  **  \brief Returns a read-only reverse iterator that points to the last
  **  element of the column \a column in the %block2d.  
  **  Iteration is done modulo the number of columns and in the reverse 
  ** element order.
  ** \param col The index of the column to iterate.
  ** \return begin const_reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  */
  const_reverse_col_iterator col_rbegin(const size_type col) const
  {
    return const_reverse_col_iterator(this->col_end(col));
  }

    /*!
  **  \brief Returns a read/write reverse iterator that points one past
  **  the first element of the row \a row in the %block2d.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return end reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %block2d.
  */
  reverse_row_iterator row_rend(const size_type row)
  {
    return reverse_row_iterator(this->row_begin(row)); 
  }

   /*!
  **  \brief Returns a read-only reverse iterator that points one past
  **  the first element of the row \a row in the %block2d. 
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return end const_reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %block2d.
  */
  const_reverse_row_iterator row_rend(const size_type row) const
  {
     return const_reverse_row_iterator(this->row_begin(row));
  }

   /*!
  **  \brief Returns a read/write reverse iterator that points one past
  **  the first element of the column \a column in the %block2d.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return end reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  */
  reverse_col_iterator col_rend(const size_type col)
  {
    return reverse_col_iterator(this->col_begin(col));
  }

   /*!
  **  \brief Returns a read-only reverse iterator that points one past
  **  the first element of the column \a column in the %block2d.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return end const_reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  */
  const_reverse_col_iterator col_rend(const size_type col) const
  {
    return const_reverse_col_iterator(this->col_begin(col)); 
  }

   /*!
  **  \brief Returns a read-write iterator that points to the last
  **  element of the %Range \a range of the row \a row in the %block2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  */
  reverse_row_range_iterator row_rbegin(const size_type row,
					const slip::Range<int>& range)
  {
    return typename block2d<T,NR,NC>::reverse_row_range_iterator(this->row_end(row,range));
  }

 
  /*!
  **  \brief Returns a read-write iterator that points one before
  **  the first element of the %Range \a range of the row \a row in the 
  **  %block2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  */
  reverse_row_range_iterator row_rend(const size_type row,
				      const slip::Range<int>& range)
  {
    return typename block2d<T,NR,NC>::reverse_row_range_iterator(this->row_begin(row,range));
  }



  /*!
  **  \brief Returns a read-only iterator that points to the last
  **  element of the %Range \a range of the row \a row in the %block2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate
  ** \return begin const_reverse_row_range_iterator value
  **
  ** \pre row must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  */
  const_reverse_row_range_iterator row_rbegin(const size_type row,
					      const slip::Range<int>& range) const
  {
    return typename block2d<T,NR,NC>::const_reverse_row_range_iterator(this->row_end(row,range));
  }


   /*!
  **  \brief Returns a read-only iterator that points one before the first
  **  element of the %Range \a range of the row \a row in the %block2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate
  ** \return const_reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  */
  const_reverse_row_range_iterator row_rend(const size_type row,
					    const slip::Range<int>& range) const
  {
    return typename block2d<T,NR,NC>::const_reverse_row_range_iterator(this->row_begin(row,range));
  }
 
 


  /*!
  **  \brief Returns a read-write iterator that points to the last
  **  element of the %Range \a range of the col \a col in the %block2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  */
  reverse_col_range_iterator col_rbegin(const size_type col,
					const slip::Range<int>& range)
  {
     return typename block2d<T,NR,NC>::reverse_col_range_iterator(this->col_end(col,range));
  }

  /*!
  **  \brief Returns a read-write iterator that points to one before 
  **  the first element of the %Range range of the col \a col in the 
  **  %block2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  */
  reverse_col_range_iterator col_rend(const size_type col,
				      const slip::Range<int>& range)
  {
     return typename block2d<T,NR,NC>::reverse_col_range_iterator(this->col_begin(col,range));
  }


  /*!
  **  \brief Returns a read_only iterator that points to the last
  **  element of the %Range \& range of the col \a col in the %block2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin const_reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  */
  const_reverse_col_range_iterator 
  col_rbegin(const size_type col,
	     const slip::Range<int>& range) const
  {
    return typename block2d<T,NR,NC>::const_reverse_col_range_iterator(this->col_end(col,range));
  }

 
  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %block2d.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return const_reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %block2d.
  ** \pre The range must be inside the whole range of the %block2d.
  */
  const_reverse_col_range_iterator col_rend(const size_type col,
					    const slip::Range<int>& range) const
  {
    return typename block2d<T,NR,NC>::const_reverse_col_range_iterator(this->col_begin(col,range));
  }


 
  /*!
  **  \brief Returns a read/write iterator2d that points to the first
  **  element of the %block2d. It points to the upper left element of
  **  the %block2d.
  **  
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,8,8> A2;
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  iterator2d upper_left()
  {
    return iterator2d(this,Box2d<int>(0,0,NR-1,NC-1));
  }

   /*!
  **  \brief Returns a read-only iterator2d that points to the first
  **  element of the %block2d. It points to the upper left element of
  **  the %block2d.
  **  
  ** \return begin const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,8,8> A2;
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  const_iterator2d upper_left() const
  {
    return const_iterator2d(this,Box2d<int>(0,0,NR-1,NC-1));
  }

   /*!
  **  \brief Returns a read/write iterator2d that points to the past
  **  the end element of the %block2d. It points to past the end element of
  **  the bottom right element of the %block2d.
  **  
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,8,8> A2;
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  iterator2d bottom_right()
  {
    DPoint2d<int> dp(NR,NC);
    iterator2d it = (*this).upper_left() + dp;
    return it;
  }

  /*!
  **  \brief Returns a read-only iterator2d that points to the past
  **  the end element of the %block2d. It points to past the end element of
  **  the bottom right element of the %block2d.
  **  
  ** \return begin const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,8,8> A2;
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  const_iterator2d bottom_right() const
  {
    DPoint2d<int> dp(NR,NC);
    const_iterator2d it = (*this).upper_left() + dp;
    return it;
  }


   /*!
  **  \brief Returns a read/write iterator2d that points to the first
  **  element of the %block2d. It points to the upper left element of
  **  the \a %Box2d associated to the %block2d.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %block2d.
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **  
  ** \return end iterator2d value
  ** \pre The box indices must be inside the range of the %block2d ones.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,8,8> A2;
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
 */
  iterator2d upper_left(const Box2d<int>& box)
  {
    return iterator2d(this,box);
  }

   /*!
  **  \brief Returns a read only iterator2d that points to the first
  **  element of the %block2d. It points to the upper left element of
  **  the \a %Box2d associated to the %block2d.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %block2d.
  ** \return end const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **
  ** \pre The box indices must be inside the range of the %block2d ones.
  ** 
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,3,2> A2;
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  const_iterator2d upper_left(const Box2d<int>& box) const
  {
    return const_iterator2d(this,box);
  }

   /*!
  **  \brief Returns a read/write iterator2d that points to the past
  **  the end element of the %block2d. It points to past the end element of
  **  the bottom right element of the \a %Box2d associated to the %block2d.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %block2d.
  **
  ** \return end iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \pre The box indices must be inside the range of the %block2d ones.
  **  
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,3,2> A2;
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  iterator2d bottom_right(const Box2d<int>& box)
  {
    DPoint2d<int> dp(box.height(),box.width());
    iterator2d it = (*this).upper_left(box) + dp;
    return it;
  }

    /*!
  **  \brief Returns a read only iterator2d that points to the past
  **  the end element of the %block2d. It points to past the end element of
  **  the bottom right element of the \a %Box2d associated to the %block2d.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %block2d.
  ** \return end const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **
  ** \pre The box indices must be inside the range of the %block2d ones.
  ** 
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,3,2> A2;
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  const_iterator2d bottom_right(const Box2d<int>& box) const
  {
    DPoint2d<int> dp(box.height(),box.width());
    const_iterator2d it = (*this).upper_left(box) + dp;
    return it;
  }


   /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **  upper left element of the ranges \a row_range and \a col_range 
  **  associated to the %block2d.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %block2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,4,4> A2;
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range upper_left(const Range<int>& row_range,
			      const Range<int>& col_range)
  {
    return typename block2d<T,NR,NC>::iterator2d_range(this,row_range,col_range);
  }

  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **   past the end bottom right element of the ranges \a row_range 
  **   and \a col_range associated to the %block2d.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %block2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,4,4> A2;
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range bottom_right(const Range<int>& row_range,
				const Range<int>& col_range)
  {
     DPoint2d<int> dp(row_range.iterations()+1,col_range.iterations()+1);
     return typename block2d<T,NR,NC>::iterator2d_range((*this).upper_left(row_range,col_range) + dp);
  }


  /*!
  **  \brief Returns a read-only iterator2d_range that points to the
  **   to the upper left element of the ranges \a row_range and \a col_range 
  **   associated to the %block2d.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid.
  ** \pre The ranges indices must be inside the ranges of the %block2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,4,4> A2;
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range upper_left(const Range<int>& row_range,
				    const Range<int>& col_range) const
  {
    return typename block2d<T,NR,NC>::const_iterator2d_range(this,row_range,col_range);
  }


 /*!
  **  \brief Returns a read-only iterator2d_range that points to the past
  **  the end bottom right element of the ranges \a row_range and \a col_range 
  **  associated to the %block2d.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %block2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,4,4> A2;
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range bottom_right(const Range<int>& row_range,
				      const Range<int>& col_range) const
  {
    DPoint2d<int> dp(row_range.iterations()+1,col_range.iterations()+1);
    return typename block2d<T,NR,NC>::const_iterator2d_range((*this).upper_left(row_range,col_range) + dp);
  }



 


  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **  upper left element of the %Range \a range associated to the %block2d.
  **  The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range and must be valid. 
  ** \pre The range indices must be inside the ranges of the %block2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,4,4> A2;
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range upper_left(const Range<int>& range)
  {
    return this->upper_left(range,range);
  }
 

  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **   past the end bottom right element of the %Range \a range 
  **   associated to the %block2d.
  **   The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %block2d ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,4,4> A2;
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range bottom_right(const Range<int>& range)
  {
    return this->bottom_right(range,range);
  }

 

  
  /*!
  **  \brief Returns a read-only iterator2d_range that points to the
  **   to the upper left element of the %Range \a range
  **   associated to the %block2d.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range must be valid.
  ** \pre The range indices must be inside the ranges of the %block2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,4,4> A2;
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range upper_left(const Range<int>& range) const
  {
    return this->upper_left(range,range);
  }

 

  /*!
  **  \brief Returns a read-only const_iterator2d_range that points to the 
  **   past the end bottom right element of the %Range \a range 
  **   associated to the %block2d.
  **   The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %block2d ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::block2d<double,8,8> A1;
  **  slip::block2d<double,4,4> A2;
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range bottom_right(const Range<int>& range) const
  {
    return this->bottom_right(range,range);
  }


   /*!
  **  \brief Returns a read/write reverse iterator2d. It points to the 
  **   bottom right element of the %block2d. 
  *    Iteration is done within the %block2d in the reverse order.
  **  
  ** \return reverse_iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rupper_left()
  {
    slip::DPoint2d<int> dp(1,0);
    return typename block2d<T,NR,NC>::reverse_iterator2d(this->bottom_right() - dp);
  }
  
  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to past the 
  **  upper left element of the %block2d.
  **  Iteration is done in the reverse order.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rbottom_right()
  {
    return typename block2d<T,NR,NC>::reverse_iterator2d(this->upper_left());
  }

  /*!
  **  \brief Returns a read only reverse iterator2d that points.  It points 
  **  to the bottom right element of the %block2d. 
  **  Iteration is done within the %block2d in the reverse order.
  **  
  ** \return const_reverse_iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rupper_left() const
  {
    slip::DPoint2d<int> dp(1,0);
    return typename block2d<T,NR,NC>::const_reverse_iterator2d(this->bottom_right() - dp);
  }
 
 
  /*!
  **  \brief Returns a read only reverse iterator2d. It points to past the 
  **  upper left element of the %block2d.
  **  Iteration is done in the reverse order.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rbottom_right() const
  {
    return typename block2d<T,NR,NC>::const_reverse_iterator2d(this->upper_left());
  }
  

  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to the 
  **  bottom right element of the \a %Box2d associated to the %block2d.
  **  Iteration is done in the reverse order.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %block2d.
  **
  ** \pre The box indices must be inside the range of the %block2d ones.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rupper_left(const Box2d<int>& box)
  {
    slip::DPoint2d<int> dp(1,0);
    return typename block2d<T,NR,NC>::reverse_iterator2d(this->bottom_right(box) - dp);
  }

  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to one
  **  before the upper left element of the %Box2d \a box associated to 
  **  the %block2d.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %block2d.
  **
  ** \pre The box indices must be inside the range of the %block2d ones.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rbottom_right(const Box2d<int>& box)
  {
    return typename block2d<T,NR,NC>::reverse_iterator2d(this->upper_left(box));
  }

  /*!
  **  \brief Returns a read only reverse iterator2d. It points to the 
  **  bottom right element of the %Box2d \a box associated to the %block2d.
  **  Iteration is done in the reverse order.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %block2d.
  **
  ** \pre The box indices must be inside the range of the %block2d ones.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rupper_left(const Box2d<int>& box) const
  {
    slip::DPoint2d<int> dp(1,0);
    return typename block2d<T,NR,NC>::const_reverse_iterator2d(this->bottom_right(box) - dp);
  }


  /*!
  **  \brief Returns a read-only reverse iterator2d. It points to one 
  **  before the element of the bottom right element of the %Box2d 
  **  \a box associated to the %block2d.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %block2d.
  **
  ** \pre The box indices must be inside the range of the %block2d ones.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rbottom_right(const Box2d<int>& box) const
  {
    return typename block2d<T,NR,NC>::const_reverse_iterator2d(this->upper_left(box));
  }

 
  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to the 
  **  past the bottom right element of the ranges \a row_range and 
  **  \a col_range associated to the %block2d. Iteration is done in the 
  **  reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %block2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rupper_left(const Range<int>& row_range,
				       const Range<int>& col_range)
  {
    slip::DPoint2d<int> dp(1,0);
    return typename block2d<T,NR,NC>::reverse_iterator2d_range(this->bottom_right(row_range,col_range) - dp);
  }


 
  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points
  **  to one before the upper left element of the ranges \a row_range 
  **  and \a col_range associated to the %block2d. Iteration is done
  **  in the reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %block2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rbottom_right(const Range<int>& row_range,
					 const Range<int>& col_range)
  {
      return typename block2d<T,NR,NC>::reverse_iterator2d_range(this->upper_left(row_range,col_range));
  }


 
  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points 
  **   to the past the bottom right element of the ranges \a row_range and 
  **   \a col_range associated to the %block2d. Iteration is done in the 
  **   reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid.
  ** \pre The ranges indices must be inside the ranges of the %block2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rupper_left(const Range<int>& row_range,
					     const Range<int>& col_range) const
  {
    slip::DPoint2d<int> dp(1,0);
    return typename block2d<T,NR,NC>::const_reverse_iterator2d_range(this->bottom_right(row_range,col_range) - dp);
  }

  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points 
  **  to one before the upper left element of the ranges \a row_range 
  **  and \a col_range associated to the %block2d.Iteration is done in 
  **  the reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %block2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rbottom_right(const Range<int>& row_range,
					       const Range<int>& col_range) const
  {
    return typename block2d<T,NR,NC>::const_reverse_iterator2d_range(this->upper_left(row_range,col_range));
  }


 
 /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to the 
  **  bottom right element of the %Range \a range associated to the %block2d.
  **  The same range is applied for rows and cols. Iteration is done in the
  **  reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range and must be valid. 
  ** \pre The range indices must be inside the ranges of the %block2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rupper_left(const Range<int>& range)
  {
    return this->rupper_left(range,range);
  }



  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to  
  **   one before the upper left element of the %Range \a range 
  **   associated to the %block2d.
  **   The same range is applied for rows and cols. Iteration is done
  **   in the reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %block2d ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rbottom_right(const Range<int>& range)
  {
     return this->rbottom_right(range,range);
  }

  

  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points to the
  **   to the bottom right element of the %Range \a range
  **   associated to the %block2d.
  **  The same range is applied for rows and cols. Iteration is done in the
  **  reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range must be valid.
  ** \pre The range indices must be inside the ranges of the %block2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rupper_left(const Range<int>& range) const
  {
    return this->rupper_left(range,range);
  }


  /*!
  **  \brief Returns a read_only reverse_iterator2d_range that points 
  **   to one before the upper left element of the %Range \a range 
  **   associated to the %block2d.
  **   The same range is applied for rows and cols. Iteration is done in the
  **   reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %block2d ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rbottom_right(const Range<int>& range) const
  {
    return this->rbottom_right(range,range);
  }
  
 

  /*@} End iterators */


  /**
   ** \name i/o operators
   */
  /*@{*/
  
  /*!
  ** \brief Write the %block2d to an ouput stream
  ** \param out output std::ostream
  ** \param b %block2d to write to an output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const block2d<T,NR,NC>& b);
 /*@} End i/o operators */

/**
   ** \name  Assignment methods
   */
  /*@{*/

  /*!
  ** \brief Fills the container range [begin(),begin()+SIZE) with copies of value
  ** \param value  A reference-to-const of arbitrary type.
  */
  void fill(const T& value)
  {
    std::fill_n(this->begin(),SIZE,value);
  } 
  
 /*!
  ** \brief Fills the container range [begin(),begin()+SIZE) with a copy of
  **        the value array
  ** \param value  A pointer of arbitrary type.
  */
  void fill(const T* value)
  {
    std::copy(value,value + SIZE, this->begin());
  } 

  /*!
  ** \brief Fills the container range [begin(),begin()+SIZE) with a copy of
  **        the range [first,last)
  **  \param  first  An input iterator.
  **  \param  last   An input iterator.
  **   
  **
  */
  template<typename InputIterator>
  void fill(InputIterator first,
	    InputIterator last)
  {
    std::copy(first,last, this->begin());
  }

  /*!
  ** \brief Assign all the elments of the %block2d by value
  **
  **
  ** \param value A reference-to-const of arbitrary type.
  ** \return 
  */
  self& operator=(const T& value)
  {
    this->fill(value);
    return *this;
  }
  /*@} End Assignment operators and methods*/

 
  /**
   ** \name Comparison operators
   */
  /*@{*/
  /*!
  ** \brief Block2d equality comparison
  ** \param x A %block2d
  ** \param y A %block2d of the same type of \a x
  ** \return true iff the size and the elements of the blocks are equal
  */
  friend bool operator== <>(const block2d<T,NR,NC>& x, 
			    const block2d<T,NR,NC>& y);
 

  /*!
  ** \brief Block2d inequality comparison
  ** \param x A %block2d
  ** \param y A %block2d of the same type of \a x
  ** \return true if !(x == y) 
  */
  friend bool operator!= <>(const block2d<T,NR,NC>& x, 
			    const block2d<T,NR,NC>& y);
  /*!
  ** \brief Less than comparison operator (block2d ordering relation)
  ** \param x A %block2d
  ** \param y A %block2d of the same type of \a x
  ** \return true iff \a x is lexicographically less than \a y
  */
  friend bool operator< <>(const block2d<T,NR,NC>& x, 
			   const block2d<T,NR,NC>& y);

  /*!
  ** \brief More than comparison operator
  ** \param x A %block2d
  ** \param y A %block2d of the same type of \a x
  ** \return true iff y > x 
  */
  friend bool operator> <>(const block2d<T,NR,NC>& x, 
			   const block2d<T,NR,NC>&y);

  /*!
  ** \brief Less than equal comparison operator
  ** \param x A %block2d
  ** \param y A %block2d of the same type of \a x
  ** \return true iff !(y > x) 
  */
  friend bool operator<= <>(const block2d<T,NR,NC>& x, 
			    const block2d<T,NR,NC>& y);

  /*!
  ** \brief More than equal comparison operator
  ** \param x A %block2d
  ** \param y A %block2d of the same type of \a x
  ** \return true iff !(x < y) 
  */
  friend bool operator>= <>(const block2d<T,NR,NC>& x, 
			    const block2d<T,NR,NC>&y);

  /*@} Comparison operators */

  
  /**
   ** \name Element access operators
   */
  /*@{*/
 
  /*!
  ** \brief Subscript access to the row datas contained in the %block2d.
  ** \param i The index of the row for which data should be accessed.
  ** \return Read/write pointer to the row data.
  ** \pre i < NR
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  pointer operator[](const size_type i) 
  {
    assert(i < NR);
    return this->data + NC * i;
  }

  /*!
  ** \brief Subscript access to the row datas contained in the %block2d.
  ** \param i The index of the row for which data should be accessed.
  ** \return Read-only (constant) pointer to the row data.
  ** \pre i < NR
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_pointer operator[](const size_type i) const
  {
    assert(i < NR);
    return this->data + NC * i;
  }

 
  /*!
  ** \brief Subscript access to the data contained in the %block2d.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read/write reference to data.
  ** \pre i < NR
  ** \pre j < NC
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const size_type i,
		       const size_type j)
  {
    assert(i < NR);
    assert(j < NC);
    return this->data[i * NC + j];
  }
   
  /*!
  ** \brief Subscript access to the data contained in the %block2d.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read_only (constant) reference to data.
  ** \pre i < NR
  ** \pre j < NC
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const size_type i,
			     const size_type j) const
  {
    assert(i < NR);
    assert(j < NC);
    return this->data[i * NC + j];
  }

   /*!
  ** \brief Subscript access to the data contained in the %block2d.
  ** \param point2d A %Point2d which indicate the subscripts of the data
  **        to access.
  ** \return Read/write reference to data.
  ** \pre point2d must be defined in the range of the %block2d.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const Point2d<size_type>& point2d)
  {
    assert(this->data != 0);
    assert(point2d[0] < NR);
    assert(point2d[1] < NC);
    return this->data[point2d[0] * NC + point2d[1]];
  }
 
  /*!
  ** \brief Subscript access to the data contained in the %block2d.
  ** \param point2d A %Point2d which indicate the subscripts of the data
  **        to access.
  ** \return Read_only (constant) reference to data.
  ** \pre point2d must be defined in the range of the %block2d.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const Point2d<size_type>& point2d) const
  {
    assert(this->data != 0);
    assert(point2d[0] < NR);
    assert(point2d[1] < NC);
    return this->data[point2d[0] * NC + point2d[1]];
  }




 /*@} End element access operators */

  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const {return "block2d";}

  /*!
  ** \brief Returns the number of rows (first dimension size) 
  **        in the %block2d
  */
  static size_type dim1() {return NR;}
  /*!
  ** \brief Returns the number of rows (first dimension size) 
  **        in the %block2d
  */
  static size_type rows() {return NR;}
  /*!
  ** \brief Returns the number of columns (second dimension size)
  **        in the %block2d
  */
  static size_type dim2() {return NC;}
  /*!
  ** \brief Returns the number of columns (second dimension size)
  **        in the %block2d
  */
  static size_type columns() {return NC;}

  /*!
   ** \brief Returns the number of columns (second dimension size)
  **        in the %block2d
  */
  static size_type cols() {return NC;}
  
  /*!
  ** \brief Returns the number of elements in the %block2d
  */
  static size_type size() {return SIZE;}
   
  /*!
  ** \brief Returns the maximal size (number of elements) in the %block2d
  */
  static size_type max_size() {return SIZE;}

  /*!
  ** \brief Returns true if the %block is empty.  (Thus size == 0)
  */
  static bool empty(){return SIZE == 0;}

  /*!
  ** \brief Swaps data with another %block.
  ** \param x A %block of the same element type
  */
  void swap(block2d<T,NR,NC>& x)
  {
     std::swap_ranges(begin(),end(),x.begin());
  }
 
  /// Data array storage of the block2d
  T data[SIZE];
private:
  friend class boost::serialization::access;
  template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & data;
	}
    }
  template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
       if(version >= 0)
	{
	  ar & data;
	}
    }
  
  BOOST_SERIALIZATION_SPLIT_MEMBER()
};


}//slip::

namespace slip
{
 /** \name input/output operators */
  /* @{ */
  template<typename T,std::size_t NR, std::size_t NC>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const block2d<T,NR,NC>& b)
  {
    for(std::size_t i = 0; i < NR; ++i)
      {
	for(std::size_t j = 0; j < NC; ++j)
	  {
	    assert( (i * NC + j) < NR*NC);
	    out<<b.data[i * NC + j]<<" ";
	  }
	out<<std::endl;
      }    	
    return out;
  }
/* @} */
/** \name EqualityComparable functions */
  /* @{ */
  template <typename T, std::size_t NR, std::size_t NC>
  inline
  bool operator==(const block2d<T,NR,NC>& x, 
		  const block2d<T,NR,NC>& y)
  {
    return ( x.size() == y.size()
	     && std::equal(x.begin(),x.end(),y.begin())); 
  }

  template <typename T, std::size_t NR, std::size_t NC>
  inline
  bool operator!=(const block2d<T,NR,NC>& x, 
		  const block2d<T,NR,NC>& y)
  {
    return !(x == y);
  }

/* @} */

  /** \name LessThanComparable functions */
  /* @{ */
  template <typename T, std::size_t NR, std::size_t NC>
  inline
  bool operator<(const block2d<T,NR,NC>& x, 
		 const block2d<T,NR,NC>& y)
  {
    return std::lexicographical_compare(x.begin(), x.end(),
					y.begin(), y.end());
  }

  template <typename T, std::size_t NR, std::size_t NC>
  inline
  bool operator>(const block2d<T,NR,NC>& x, 
		 const block2d<T,NR,NC>& y)
  {
    return (y < x);
  }

  template <typename T, std::size_t NR, std::size_t NC>
  inline
  bool operator<=(const block2d<T,NR,NC>& x, 
		  const block2d<T,NR,NC>& y)
  {
    return !(y < x);
  }

  template <typename T, std::size_t NR, std::size_t NC>
  inline
  bool operator>=(const block2d<T,NR,NC>& x, 
		  const block2d<T,NR,NC>& y)
  {
    return !(x < y);
  }
  /* @} */

}//slip::

#endif //SLIP_BLOCK2D_HPP
