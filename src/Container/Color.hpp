/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file Color.hpp
 * 
 * \brief Provides an class to manipulate Colors.
 * 
 */
#ifndef SLIP_COLOR_HPP
#define SLIP_COLOR_HPP

#include <iostream>
#include <iterator>
#include <string>
#include <cassert>
#include <algorithm>
#include <cstddef>
#include "Block.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template <typename T>
struct Color;

template <typename T>
std::ostream& operator<<(std::ostream & out,const Color<T>& b);

template <typename T>
bool operator==(const Color<T>& x, const Color<T>& y);

template <typename T>
bool operator!=(const Color<T>& x, const Color<T>& y);

// template <typename T, std::size_t NR, std::size_t NC>
// bool operator<(const Color<T>& x, const Color<T>& y);

// template <typename T, std::size_t NR, std::size_t NC>
// bool operator>(const Color<T>& x, const Color<T>& y);

// template <typename T, std::size_t NR, std::size_t NC>
// bool operator<=(const Color<T>& x, const Color<T>& y);

// template <typename T, std::size_t NR, std::size_t NC>
// bool operator>=(const Color<T>& x, const Color<T>& y);

/*! \class Color
**  \ingroup Containers MiscellaneousContainers
** \brief This is a %Color struct
** 
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.4
** \date 2014/03/17
** \since 1.0.0
** \param T Type of the Color
*/
template <typename T>
struct Color
{
  typedef T value_type;
  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;

  typedef Color<T> self;
  static const std::size_t SIZE = 3;
   /**
   ** \name iterators
   */
  /*@{*/

 /*!
  ** \brief Accessor to the begin iterator
  ** \return Begin iterator value
  */
  iterator begin();
   /*!
  ** \brief Accessor to the end iterator
  ** \return End iterator value
  */
  iterator end();

 /*!
  ** \brief Accessor to the begin iterator
  ** \return Const begin iterator value
  */
  const_iterator begin() const;
   /*!
  ** \brief Accessor to the const end iterator
  ** \return Const end iterator value
  */
  const_iterator end() const;

   /*@} End iterators */

  /**
   ** \name Constructors
   */
  /*@{*/

  /*!
  ** \brief Default constructor
  ** init all the components to 0
  */
  Color();

  /*!
  ** \brief Constructor
  ** init all the components to val
  ** \param val initial value
  */
  Color(const T& val);

   /*!
  ** \brief Constructor
  ** init all the components to val
  ** \param x1 initial value of the first color plane
  ** \param x2 initial value of the second color plane
  ** \param x3 initial value of the second color plane
  */
  Color(const T& x1,
	const T& x2,
	const T& x3);

  /*!
  ** \brief Constructor
  ** init the Color with the val array
  ** \param val initial value
  */
  Color(const T* val);

//   /*!
//   ** \brief Copy constructor
//   ** \param rhs a Color object
//   */
//   Color(const Color<T>& rhs);
  
  


  /*@} End Constructors */

   /**
   ** \name Assign
   */
  /*@{*/
  self& operator=(const self& rhs)
  {
    colors = rhs.colors;
    return *this;
  }

  /**
   ** assign val to all the color components.
   ** \param val value to assign.
   ** \return a Color
   */
  self& operator=(const T& val)
    {
      std::fill(colors.begin(),colors.end(),val);
      // colors[0] = val;
      // colors[1] = val;
      // colors[2] = val;
      return *this;
    }
  /*@} End Assign */
 /**
 ** \name Operators
 */
 /*@{*/


 /*!
  ** \brief Write the Color to an ouput stream
  ** \param out output stream
  ** \param b Color to write to an output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, const Color<T>& b);

 /*!
  ** \brief Equality Compare operator
  ** \param x first Color to compare
  ** \param y second Color to compare
  ** return true if x == y 
  ** \pre x.dim() == y.dim()
  */
  friend bool operator== <>(const Color<T>& x, const Color<T>& y);

 /*!
  ** \brief Equality Compare operator
  ** \param x first Color to compare
  ** \param y second Color to compare
  ** return true if x != y 
  ** \pre x.dim() == y.dim()
  */
  friend bool operator!= <>(const Color<T>& x, const Color<T>& y);


 /**
   ** \name  Element access operators
   */
  /*@{*/
 /*!
  ** \brief Returns the i'th value of the Color 
  ** \param i index of the color plane
  ** \return pointer to the \a i'th row of the Color.
  ** \pre i < 3
  */
  T& operator[](const std::size_t i);

  /*!
  ** \brief Returns the i'th value of the Color 
  ** \param i index of the color plane
  ** \return pointer to the \a i'th row of the Color.
  ** \pre i < 3
  */
  const T& operator[](const std::size_t i) const;

 
  /*@} End Operators */

  /*!
  ** \brief Accessor to the first plane value of the Color 
  ** \return Returns the first plane value of the Color 
  */
  const T& get_x1() const;
  const T& get_red() const;

  /*!
  ** \brief  Accessor to the second plane value of the Color 
  ** \return Returns the second plane value of the Color 
  */
  const T& get_x2() const;
  const T& get_green() const;
    
  /*!
  ** \brief  Accessor to the third plane value of the Color 
  ** \return Returns the third plane value of the Color 
  */
  const T& get_x3() const;
  const T& get_blue() const;
    

  /*!
  ** \brief  Mutator of the first plane value of the Color 
  ** \param x1 the first plane value of the Color 
  */
  void set_x1(const T& x1);
  void set_red(const T& r);

  /*!
  ** \brief  Mutator of the second plane value of the Color 
  ** \param x2 the second plane value of the Color 
  */
  void set_x2(const T& x2);
  void set_green(const T& g);

  /*!
  ** \brief  Mutator of the third plane value of the Color 
  ** \param x3 the third plane value of the Color 
  */
  void set_x3(const T& x3);
  void set_blue(const T& b);

  /*!
  ** \brief  Mutator of the three plane values of the Color 
  ** \param x1 the first plane value of the Color 
  ** \param x2 the second plane value of the Color 
  ** \param x3 the third plane value of the Color 
  */
  void set(const T& x1, 
	   const T& x2,
	   const T& x3);
 
  /*@} End Element access operators */


/**
   ** \name  Arithmetic operators
   */
  /*@{*/

  /*!
  ** \brief Add val to each element of the %Color  
  ** \param val value
  ** \return reference to the resulting %Color
  */
  self& operator+=(const T& val)
  {
    std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::plus<T>(),val));
    return *this;
  }
   
  self& operator-=(const T& val)
  {
    std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::minus<T>(),val));
    return *this;
  }

  self& operator*=(const T& val)
  {
    std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::multiplies<T>(),val));
    return *this;
  }
  
  self& operator/=(const T& val)
  {
    std::transform(this->begin(),this->end(),this->begin(),std::bind2nd(std::divides<T>(),val));
    return *this;
  }
  //   self& operator%=(const T& val);
  //   self& operator^=(const T& val);
  //   self& operator&=(const T& val);
  //   self& operator|=(const T& val);
  //   self& operator<<=(const T& val);
  //   self& operator>>=(const T& val);

  self  operator-() const
  {
    self tmp = *this;
    std::transform(this->begin(),this->end(),tmp.begin(),std::negate<T>());
    return tmp;
  }
  //self  operator!() const;

  self& operator+=(const self& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::plus<T>());
    return *this;
  }

  self& operator-=(const self& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::minus<T>());
    return *this;
  }
  
  self& operator*=(const self& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::multiplies<T>());
    return *this;
  }
  
  self& operator/=(const self& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(this->begin(),this->end(),rhs.begin(),this->begin(),std::divides<T>());
    return *this;
  }    

  /*@} End Arithmetical operators */

  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;

  /// Returns the size (number of elements) of the Color
  std::size_t size() const;
  /// Returns the maximal size (number of elements) of the Color
  size_t max_size() const;
  /// Returns true if the Color size is 0
  bool empty() const;

  ///Returns true if all components are equal
  bool is_grayscale() const;

  /*!
  ** \brief Swaps the contents of two Color
  ** \param C Color to swap with
  */
  void swap(Color<T>& C);
 
  /*!
  ** \brief Fills the Color with copies of value
  ** \param value  A reference-to-const of arbitrary type T
  */
  void fill(const T& value);
  
 /*!
  ** \brief Fills the Color with a copy of
  **        the value array
  ** \param value  A pointer of arbitrary type T.
  */
  void fill(const T* value);



   /// Data array storage of the Color
  slip::block<T,3> colors;

   private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & colors;
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & colors;
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()

};

///double alias
  typedef slip::Color<double> Color_d;
  ///float alias
  typedef slip::Color<float> Color_f;
  ///long alias
  typedef slip::Color<long> Color_l;
  ///unsigned long alias
  typedef slip::Color<unsigned long> Color_ul;
  ///short alias
  typedef slip::Color<short> Color_s;
  ///unsigned long alias
  typedef slip::Color<unsigned short> Color_us;
  ///int alias
  typedef slip::Color<int> Color_i;
  ///unsigned int alias
  typedef slip::Color<unsigned int> Color_ui;
  ///char alias
  typedef slip::Color<char> Color_c;
  ///unsigned char alias
  typedef slip::Color<unsigned char> Color_uc;


}//slip::

namespace slip
{
  template<typename T>
  inline
  T* Color<T>::begin()
  {
    return colors.begin();
  }

  template<typename T>
  inline
  T* Color<T>::end()
  {
    return colors.end();
  }
  
  template<typename T>
  inline
  const T* Color<T>::begin() const
  {
    return colors.begin();
  }

  template<typename T>
  inline
  const T* Color<T>::end() const
  {
    return colors.end();
  }
  
   /** \name input/output operators */
  /* @{ */
  template<typename T>
  inline
  std::ostream& operator<<(std::ostream & out, const Color<T>& b)
  {
    out<<b.colors;
    return out;
  }
  /* @} */

  template<typename T>
  inline
  Color<T>::Color()
  {
    colors.fill(T(0));
  }

  template<typename T>
  inline
  Color<T>::Color(const T& val)
  {
    colors.fill(val);
  }

  template<typename T>
  inline
  Color<T>::Color(const T& x1,
		  const T& x2,
		  const T& x3)
  {
    colors[0] = x1;
    colors[1] = x2;
    colors[2] = x3;
  }

  template<typename T>
  inline
  Color<T>::Color(const T* val)
  {
    assert(val != 0);
    colors.fill(val);
  }

 //  template<typename T>
//   inline
//   Color<T>::Color(const Color<T>& rhs)
//   {
//     colors[0] = rhs.colors[0];
//     colors[1] = rhs.colors[1];
//     colors[2] = rhs.colors[2];
//   }



/** \name Color arithmetic operations */
  /* @{ */

template<typename T>
inline
Color<T> operator+(const Color<T>& V1, 
		       const Color<T>& V2)
{
  Color<T> tmp;
  std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::plus<T>());
  return tmp;
}

template<typename T>
inline
Color<T> operator+(const Color<T>& V1, 
		       const T& val)
{
  Color<T> tmp = V1;
  tmp+=val;
  return tmp;
}

template<typename T>
inline
Color<T> operator+(const T& val,
		       const Color<T>& V1)
{
  return V1 + val;
}

template<typename T>
inline
Color<T> operator-(const Color<T>& V1, 
		       const Color<T>& V2)
{
    Color<T> tmp;
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::minus<T>());
    return tmp;
}

template<typename T>
inline
Color<T> operator-(const Color<T>& V1, 
		    const T& val)
{
    Color<T> tmp = V1;
    tmp-=val;
    return tmp;
}

template<typename T>
inline
Color<T> operator-(const T& val,
		       const Color<T>& V1)
{
  return -(V1 - val);
}

template<typename T>
inline
Color<T> operator*(const Color<T>& V1, 
		       const Color<T>& V2)
{
    Color<T> tmp;
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::multiplies<T>());
    return tmp;
}

template<typename T>
inline
Color<T> operator*(const Color<T>& V1, 
		       const T& val)
{
    Color<T> tmp = V1;
    tmp*=val;
    return tmp;
}

template<typename T>
inline
Color<T> operator*(const T& val,
		       const Color<T>& V1)
{
  return V1 * val;
}

template<typename T>
inline
Color<T> operator/(const Color<T>& V1, 
		       const Color<T>& V2)
{
    Color<T> tmp;
    std::transform(V1.begin(),V1.end(),V2.begin(),tmp.begin(),std::divides<T>());
    return tmp;
}

template<typename T>
inline
Color<T> operator/(const Color<T>& V1, 
		    const T& val)
{
    Color<T> tmp = V1;
    tmp/=val;
    return tmp;
}
/* @} */

/** \name EqualityComparable Color functions */
  /* @{ */

  template<typename T>
  inline
  bool operator==(const Color<T>& x, 
		  const Color<T>& y)
  {
    return (x.colors == y.colors);
  }

  template<typename T>
  inline
  bool operator!=(const Color<T>& x, 
		  const Color<T>& y)
  {
     return (x.colors != y.colors);
  }

/* @} */

  template<typename T>
  inline
  T& Color<T>::operator[](const std::size_t i)
  {
    return colors[i];
  }

  template<typename T>
  inline
  const T& Color<T>::operator[](const std::size_t i) const
  {
    return colors[i];
  }


  template<typename T>
  inline
  const T& Color<T>::get_x1() const {return colors[0];}

  template<typename T>
  inline
  const T& Color<T>::get_red() const {return colors[0];}

   template<typename T>
  inline
  const T& Color<T>::get_x2() const {return colors[1];}

  template<typename T>
  inline
  const T& Color<T>::get_green() const {return colors[1];}

  template<typename T>
  inline
  const T& Color<T>::get_x3() const {return colors[2];}

  template<typename T>
  inline
  const T& Color<T>::get_blue() const {return colors[2];}

  template<typename T>
  inline
  void Color<T>::set_x1(const T& x1){colors[0] = x1;}

  template<typename T>
  inline
  void Color<T>::set_red(const T& r){colors[0] = r;}

  template<typename T>
  inline
  void Color<T>::set_x2(const T& x2){colors[1] = x2;}

  template<typename T>
  inline
  void Color<T>::set_green(const T& g){colors[1] = g;}

  template<typename T>
  inline
  void Color<T>::set_x3(const T& x3){colors[2] = x3;}

  template<typename T>
  inline
  void Color<T>::set_blue(const T& b){colors[2] = b;}


  template<typename T>
  inline
  void Color<T>::set(const T& r,
		     const T& g,
		     const T& b)
  {
    colors[0] = r;
    colors[1] = g;
    colors[2] = b;
  }

  
  template<typename T>
  inline
  std::string 
  Color<T>::name() const {return "Color";} 
  

  template<typename T>
  inline
  std::size_t Color<T>::size() const {return colors.size();}

  template<typename T>
  inline
  std::size_t Color<T>::max_size() const {return colors.max_size();}

  template<typename T>
  inline
  bool Color<T>::empty() const {return colors.empty();}

  template<typename T>
  inline
  bool Color<T>::is_grayscale() const 
  {
    return ((colors[0] == colors[1]) && (colors[0] == colors[2]));
  }


  template<typename T>
  inline
  void Color<T>::swap(Color<T>& C)
  {
    colors.swap(C.colors);
  }
  
  template<typename T>
  inline
  void Color<T>::fill(const T& val) 
  {
    colors.fill(val);
  }

  template<typename T>
  inline
  void Color<T>::fill(const T* val) 
  {
    colors.fill(val);
  }

  	

 

   
}//slip::

#endif //SLIP_COLOR_HPP
