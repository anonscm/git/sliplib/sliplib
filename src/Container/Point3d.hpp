/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/*! \file Point3d.hpp
**
** \brief Provides a class to modelize 3d points.
*/
#ifndef SLIP_POINT3D_HPP
#define SLIP_POINT3D_HPP

#include <iostream>
#include <cassert>
#include <string>
#include "Point.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/base_object.hpp>

namespace slip
{
/*! \class Point3d
**  \ingroup Containers MiscellaneousContainers
** \brief This is a point3d class, a specialized version of 
** Point<CoordType,DIM> with DIM = 3
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/03/15
** \since 1.0.0
** \param CoordType Type of the coordinates of the Point3d 
*/
template <typename CoordType>
class Point3d:public Point<CoordType,3>
{
public :
  typedef Point3d<CoordType> self;
  typedef Point<CoordType,3> base;
  
  /**
  ** \name Constructors & Destructors
  */
  /*@{*/

  /*!
  ** \brief Constructs a Point3d.
  */
  Point3d();
 
  /*!
  ** \brief Constructs a Point from a CoordType array
  ** \param array
  */
  Point3d(const CoordType* array);

  /*!
  ** \brief Constructs a Point3d.
  ** \param x1 first coordinate of the Point3d
  ** \param x2 second coordinate of the Point3d
  ** \param x3 third coordinate of the Point3d
  */
  Point3d(const CoordType& x1,
	  const CoordType& x2,
	  const CoordType& x3);

  /*@} End Constructors */

  /**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Accessor of the first coordinate of Point3d. 
  ** \param x the first coordinate of the Point3d
  */
  void x1(const CoordType& x);  
  /*!
  ** \brief Accessor/Mutator of the first coordinate of Point3d.
  ** \return reference to the first coordinate of the Point3d
  */
  CoordType& x1();
  /*!
  ** \brief Accessor/Mutator of the first coordinate of Point3d.
  ** \return reference to the first coordinate of the Point3d
  */
  const CoordType& x1()const;

  /*!
  ** \brief Accessor of the second coordinate of Point3d. 
  ** \param x the second coordinate of the Point3d
  */
  void x2(const CoordType& x);  

  /*!
  ** \brief Accessor/Mutator of the second coordinate of Point3d.
  ** \return reference to the second coordinate of the Point3d
  */
  CoordType& x2();

  /*!
  ** \brief Accessor/Mutator of the first coordinate of Point3d.
  ** \return reference to the first coordinate of the Point3d
  */
  const CoordType& x2() const;

  /*!
  ** \brief Accessor of the third coordinate of Point3d. 
  ** \param x the third coordinate of the Point3d
  */
  void x3(const CoordType& x);  

  /*!
  ** \brief Accessor/Mutator of the second coordinate of Point3d.
  ** \return reference to the second coordinate of the Point3d
  */
  CoordType& x3();
  /*!
  ** \brief Accessor/Mutator of the first coordinate of Point3d.
  ** \return reference to the first coordinate of the Point3d
  */
  const CoordType& x3() const;
   /*@} End Element access operators */
  
   /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;
 private:
  friend class boost::serialization::access;
 template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::Point<CoordType,3> >(*this);
	}
    }
   template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::Point<CoordType,3> >(*this);
	}
    }
  BOOST_SERIALIZATION_SPLIT_MEMBER()
};
 ///double alias
  typedef slip::Point3d<double> Point3d_d;
  ///float alias
  typedef slip::Point3d<float> Point3d_f;
  ///long alias
  typedef slip::Point3d<long> Point3d_l;
  ///unsigned long alias
  typedef slip::Point3d<unsigned long> Point3d_ul;
  ///short alias
  typedef slip::Point3d<short> Point3d_s;
  ///unsigned long alias
  typedef slip::Point3d<unsigned short> Point3d_us;
  ///int alias
  typedef slip::Point3d<int> Point3d_i;
  ///unsigned int alias
  typedef slip::Point3d<unsigned int> Point3d_ui;
  ///char alias
  typedef slip::Point3d<char> Point3d_c;
  ///unsigned char alias
  typedef slip::Point3d<unsigned char> Point3d_uc;
}//slip::

namespace slip
{
  template<typename CoordType>
  inline
  Point3d<CoordType>::Point3d()
  {
    this->coord_[0] = CoordType(0);
    this->coord_[1] = CoordType(0);
    this->coord_[2] = CoordType(0);
  }

  template<typename CoordType>
  inline
  Point3d<CoordType>::Point3d(const CoordType* array):
    Point<CoordType,3>(array)
  {}


  template<typename CoordType>
  inline
  Point3d<CoordType>::Point3d(const CoordType& x1,
			      const CoordType& x2,
			      const CoordType& x3)
  {
     this->coord_[0] = CoordType(x1);
     this->coord_[1] = CoordType(x2);
     this->coord_[2] = CoordType(x3);
  }
  
  template<typename CoordType>
  inline
  CoordType& Point3d<CoordType>::x1() {return this->coord_[0];}

  template<typename CoordType>
  inline
  const CoordType& Point3d<CoordType>::x1() const {return this->coord_[0];}

  template<typename CoordType>
  inline
  CoordType& Point3d<CoordType>::x2() {return this->coord_[1];}
  
  template<typename CoordType>
  inline
  const CoordType& Point3d<CoordType>::x2() const {return this->coord_[1];}


  template<typename CoordType>
  inline
  CoordType& Point3d<CoordType>::x3() {return this->coord_[2];}

  template<typename CoordType>
  inline
  const CoordType& Point3d<CoordType>::x3() const {return this->coord_[2];}

  template<typename CoordType>
  inline
  void Point3d<CoordType>::x1(const CoordType& x1) {this->coord_[0]=x1;}

  template<typename CoordType>
  inline
  void Point3d<CoordType>::x2(const CoordType& x2) {this->coord_[1]=x2;}

  template<typename CoordType>
  inline
  void Point3d<CoordType>::x3(const CoordType& x3) {this->coord_[2]=x3;}

  template<typename CoordType>
  inline
  std::string 
  Point3d<CoordType>::name() const {return "Point3d";} 


 
}//slip::

#endif //SLIP_POINT3D_HPP
