/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Matrix.hpp
 * 
 * \brief Provides a class to manipulate Numerical Matrix.
 * 
 */
#ifndef SLIP_MATRIX_HPP
#define SLIP_MATRIX_HPP

#include <iostream>
#include <iterator>
#include <cassert>
#include <numeric>
#include <algorithm>
#include <cmath>
#include <string>
#include <cstddef>
#include "Array2d.hpp"
#include "Point2d.hpp"
#include "DPoint2d.hpp"
#include "stride_iterator.hpp"
#include "iterator2d_box.hpp"
#include "Range.hpp"
#include "iterator2d_range.hpp"
#include "apply.hpp"
#include "norms.hpp"
#include "linear_algebra.hpp"
#include "linear_algebra_traits.hpp"
#include "linear_algebra_eigen.hpp"
#include "linear_algebra_svd.hpp"
#include "complex_cast.hpp"


#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{

template<class T>
class stride_iterator;

template<class T>
class iterator2d_box;

template<class T>
class iterator2d_range;

template<class T>
class const_iterator2d_box;

template<class T>
class const_iterator2d_range;

template <class T>
class DPoint2d;

template <class T>
class Point2d;

template <class T>
class Box2d;

template <typename T>
class Matrix;

template <typename T>
class Array2d;

template <typename T>
std::ostream& operator<<(std::ostream & out, const slip::Matrix<T>& a);

template<typename T>
bool operator==(const slip::Matrix<T>& x, 
		const slip::Matrix<T>& y);

template<typename T>
bool operator!=(const slip::Matrix<T>& x, 
		const slip::Matrix<T>& y);

template<typename T>
bool operator<(const slip::Matrix<T>& x, 
	       const slip::Matrix<T>& y);

template<typename T>
bool operator>(const slip::Matrix<T>& x, 
	       const slip::Matrix<T>& y);

template<typename T>
bool operator<=(const slip::Matrix<T>& x, 
		const slip::Matrix<T>& y);

template<typename T>
bool operator>=(const slip::Matrix<T>& x, 
		const slip::Matrix<T>& y);

/*! \class Matrix
**  \ingroup Containers Containers2d MathematicalContainer
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.1
** \date 2007/10/29
** \since 1.0.0
** \brief Numerical matrix class. 
** This container statisfies the BidirectionnalContainer concepts of the STL.
** It is also an 2d extension of the RandomAccessContainer concept. That is
** to say the bracket element access is replaced by the double bracket element 
** access. It extends 
** the interface of Array2d adding arithmetical: +=, -=, *=, /=,+,-,/,*... 
** and mathematical operators: min, max, abs, sqrt, cos, acos, sin, asin, 
** tan, atan, exp, log, cosh, sinh, tanh, log10, sum, apply...
** \param T. Type of object in the Matrix 
** \par Axis conventions:
** \image html iterator2d_conventions.jpg "axis and notation conventions"
** \image latex iterator2d_conventions.eps "axis and notation conventions" width=5cm
**
**
*/
template <typename T>
class Matrix
{
public :

  typedef T value_type;
  typedef Matrix<T> self;
  typedef const Matrix<T> const_self;

  typedef value_type* pointer;
  typedef value_type const * const_pointer;
  typedef value_type& reference;
  typedef value_type const & const_reference;

  typedef ptrdiff_t difference_type;
  typedef std::size_t size_type;

  typedef pointer iterator;
  typedef const_pointer const_iterator;
  
  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

  typedef pointer row_iterator;
  typedef const_pointer const_row_iterator;
  typedef slip::stride_iterator<pointer> col_iterator;
  typedef slip::stride_iterator<const_pointer> const_col_iterator;

  
  typedef typename slip::Array2d<T>::iterator2d iterator2d;
  typedef typename slip::Array2d<T>::const_iterator2d const_iterator2d;
  typedef slip::stride_iterator<pointer> row_range_iterator;
  typedef slip::stride_iterator<const_pointer> const_row_range_iterator;
  typedef slip::stride_iterator<col_iterator> col_range_iterator;
  typedef slip::stride_iterator<const_col_iterator> const_col_range_iterator;
  typedef typename slip::Array2d<T>::iterator2d_range iterator2d_range;
  typedef typename slip::Array2d<T>::const_iterator2d_range const_iterator2d_range;


  typedef std::reverse_iterator<iterator> reverse_row_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
  typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
  typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;
  typedef std::reverse_iterator<iterator2d> reverse_iterator2d;
  typedef std::reverse_iterator<const_iterator2d> const_reverse_iterator2d;
  typedef std::reverse_iterator<row_range_iterator> reverse_row_range_iterator;
  typedef std::reverse_iterator<const_row_range_iterator> const_reverse_row_range_iterator;
  typedef std::reverse_iterator<col_range_iterator> reverse_col_range_iterator;
  typedef std::reverse_iterator<const_col_range_iterator> const_reverse_col_range_iterator;
  typedef std::reverse_iterator<iterator2d_range> reverse_iterator2d_range;
  typedef std::reverse_iterator<const_iterator2d_range> const_reverse_iterator2d_range;
  
  typedef typename slip::lin_alg_traits<value_type>::value_type norm_type;

  
  //default iterator of the container
  typedef iterator2d default_iterator;
  typedef const_iterator2d const_default_iterator;

  static const std::size_t DIM = 2;
public:
 /**
 ** \name Constructors & Destructors
 */
 /*@{*/
  
  /*!
  ** \brief Constructs a %Matrix.
  */
  Matrix();
 
  /*!
&  ** \brief Constructs a %Matrix.
  ** \param d1 first dimension of the %Matrix
  ** \param d2 second dimension of the %Matrix
  */
  Matrix(const size_type d1,
	 const size_type d2);
  
  /*!
  ** \brief Constructs a %Matrix initialized by the scalar value \a val.
  ** \param d1 first dimension of the %Matrix
  ** \param d2 second dimension of the %Matrix
  ** \param val initialization value of the elements 
  */
  Matrix(const size_type d1,
	 const size_type d2, 
	 const T& val);
 
  /*!
  ** \brief Constructs a %Matrix initialized by an array \a val.
  ** \param d1 first dimension of the %Matrix
  ** \param d2 second dimension of the %Matrix
  ** \param val initialization array value of the elements 
  */
  Matrix(const size_type d1,
	 const size_type d2, 
	 const T* val);

 /**
  **  \brief  Contructs a %Matrix from a range.
  ** \param d1 first dimension of the %Matrix
  ** \param d2 second dimension of the %Matrix
  **  \param  first  An input iterator.
  **  \param  last  An input iterator.
  **
  ** Create a %Matrix consisting of copies of the elements from
  ** [first,last).
  */
  template<typename InputIterator>
  Matrix(const size_type d1,
	 const size_type d2, 
	 InputIterator first,
	 InputIterator last)
  {
    array_ = new slip::Array2d<T>(d1,d2,first,last);
  } 
 /*!
  ** \brief Constructs a copy of the %Matrix \a rhs
  */
  Matrix(const self& rhs);

 /*!
  ** \brief Destructor of the %Matrix
  */
  ~Matrix();
  

  /*@} End Constructors */


  /*!
  ** \brief Resizes a %Matrix.
  ** \param d1 new first dimension
  ** \param d2 new second dimension
  ** \param val new value for all the elements
  */ 
  void resize(const size_type d1,
	      const size_type d2,
	      const T& val = T());


  /**
   ** \name iterators
   */
  /*@{*/
 
   /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element in the %Matrix.  Iteration is done in ordinary
  **  element order.
  ** \return begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  **  slip::Matrix<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  iterator begin();


  /*!
  **  \brief Returns a read/write iterator that points one past the last
  **  element in the %Matrix.  Iteration is done in ordinary
  **  element order.
  ** \return end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  **  slip::Matrix<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  iterator end();

  /*!
  **  \brief Returns a read-only (constant) iterator that points to the
  **  first element in the %Matrix.  Iteration is done in ordinary
  **  element order.
  ** 
  ** \return const begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  **  slip::Matrix<double> S(10,9);
  ** //copy the sum of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_iterator begin() const;

  
  /*!
  **  \brief Returns a read-only (constant) iterator that points one past
  **  the last element in the %Matrix.  Iteration is done in
  **  ordinary element order.
  ** \return const end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  **  slip::Matrix<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 in S
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_iterator end() const;
  
  /*!
  **  \brief Returns a read/write reverse iterator that points to the
  **  last element in the %Matrix.  Iteration is done in reverse
  **  element order.
  ** \return reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  **  slip::Matrix<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 
  ** //in S (iterated in the reverse order element)
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.rbegin(),
  **                std::plus<double>());
  ** \endcode
  */
  reverse_iterator rbegin();
  
  /*!
  **  \brief Returns a read/write reverse iterator that points to one
  **  before the first element in the %Matrix.  Iteration is done
  **  in reverse element order.
  **  \return reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  **  slip::Matrix<double> S(10,9);
  ** //copy the sum of the elements of A1 and A2 
  ** //in S (iterated in the reverse order element)
  ** std::transform(A1.begin(),A1.end(),
  **                A2.begin(),S.rbegin(),
  **                std::plus<double>());
  ** \endcode
  */
  reverse_iterator rend();
  
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to the last element in the %Matrix.  Iteration is done in
  **  reverse element order.
  ** \return const reverse begin iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  **  slip::Matrix<double> S(10,9);
  ** //copy the sum of the elements of A1 (iterated in the reverse order element)
  ** //and A2 in S 
  ** std::transform(A1.rbegin(),A1.rend(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_reverse_iterator rbegin() const;

  
  /*!
  **  \brief Returns a read-only (constant) reverse iterator that points
  **  to one before the first element in the %Matrix.  Iteration
  **  is done in reverse element order.
  **  \return const reverse end iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  **  slip::Matrix<double> S(10,9);
  ** //copy the sum of the elements of A1 (iterated in the reverse order element)
  ** //and A2 in S 
  ** std::transform(A1.rbegin(),A1.rend(),
  **                A2.begin(),S.begin(),
  **                std::plus<double>());
  ** \endcode
  */
  const_reverse_iterator rend() const;

 
 /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the row \a row in the %Matrix.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  row_iterator row_begin(const size_type row);

  
  /*!
  **  \brief Returns a read/write iterator that points one past
  **  the end element of the row \a row in the %Matrix.  
  **  Iteration is done in ordinary element order.
  ** \param row  The index of the row to iterate.
  ** \return begin row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  row_iterator row_end(const size_type row);


  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the row \a row in the %Matrix.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  const_row_iterator row_begin(const size_type row) const;


  /*!
  **  \brief Returns a read-only iterator that points one past
  **  the end element of the row \a row in the %Matrix.  
  **  Iteration is done in ordinary element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  ** //copy the elements of the row 0 of A1 in the row 1 of A2
  ** std::copy(A1.row_begin(0),A1.row_end(0),A2.row_begin(1));
  ** \endcode
  */
  const_row_iterator row_end(const size_type row) const;
  
  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the column \a column in the %Matrix.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  col_iterator col_begin(const size_type col);


  /*!
  **  \brief Returns a read/write iterator that points one past
  **  the end element of the column \a column in the %Matrix.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  col_iterator col_end(const size_type col);

  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the column \a column in the %Matrix.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column to iterate.
  ** \return begin const_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  const_col_iterator col_begin(const size_type col) const;



  /*!
  **  \brief Returns a read-only iterator that points one past
  **  the end element of the column \a column in the %Matrix.  
  **  Iteration is done modulo the number of columns.
  ** \param col The index of the column 
  ** \return begin const_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(10,9);
  **  slip::Matrix<double> A2(10,9);
  ** //copy the elements of the column 0 of A1 in the column 1 of A2
  ** std::copy(A1.col_begin(0),A1.col_end(0),A2.col_begin(1));
  ** \endcode
  */
  const_col_iterator col_end(const size_type col) const;


  /*!
  **  \brief Returns a read/write iterator that points to the first
  **  element of the %Range \a range of the row \a row in the %Matrix.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return begin row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  row_range_iterator row_begin(const size_type row,
			       const slip::Range<int>& range);



  /*!
  **  \brief Returns a read/write iterator that points one past the end
  **  element of the %Range \a range of the row \a row in the %Matrix.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return end row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  row_range_iterator row_end(const size_type row,
			     const slip::Range<int>& range);


  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the row \a row in the %Matrix.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return begin const_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  const_row_range_iterator row_begin(const size_type row,
				     const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read_only iterator that points one past the last
  **  element of the %Range range of the row \a row in the %Matrix.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param row Row to iterate.
  ** \param range %Range of the row to iterate
  ** \return begin const_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** \pre row must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(8,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of the row 0 of A1 iterated according to the
  ** //range in the row 1 of A2
  ** std::copy(A1.row_begin(0,range),A1.row_end(0,range),A2.row_begin(1));
  ** \endcode
  */
  const_row_range_iterator row_end(const size_type row,
				   const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read-write iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %Matrix.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate
  ** \return begin col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  col_range_iterator col_begin(const size_type col,
			       const slip::Range<int>& range);

  /*!
  **  \brief Returns a read-write iterator that points to the past
  **  the end element of the %Range \a range of the col \a col in the 
  **  %Matrix.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  col_range_iterator col_end(const size_type col,
			     const slip::Range<int>& range);




  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %Matrix.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin const_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  const_col_range_iterator col_begin(const size_type col,
				     const slip::Range<int>& range) const;

 
  /*!
  **  \brief Returns a read-only iterator that points to the past
  **  the end element of the %Range \a range of the col \a col in 
  **  the %Matrix.  
  **  Iteration is done in ordinary element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate
  ** \return begin const_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(4,8);
  **  slip::Range<int> range(0,A1.dim1()-1,2);
  ** //copy the elements of the column 0 of A1 iterated according to the
  ** //range in the column 1 of A2
  ** std::copy(A1.col_begin(0,range),A1.col_end(0,range),A2.col_begin(1));
  ** \endcode
  */
  const_col_range_iterator col_end(const size_type col,
				   const slip::Range<int>& range) const;

  

  /*!
  **  \brief Returns a read/write reverse iterator that points to the 
  **  last element of the row \a row in the %Matrix.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return begin reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Matrix.
  */
  reverse_row_iterator row_rbegin(const size_type row);


  /*!
  **  \brief Returns a read/write reverse iterator that points one past
  **  the first element of the row \a row in the %Matrix.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return end reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Matrix.
  */
  reverse_row_iterator row_rend(const size_type row);

  /*!
  **  \brief Returns a read-only reverse iterator that points to the last
  **  element of the row \a row in the %Matrix.  
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return begin const_reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %Matrix.
  */
  const_reverse_row_iterator row_rbegin(const size_type row) const;

  /*!
  **  \brief Returns a read-only reverse iterator that points one past
  **  the first element of the row \a row in the %Matrix. 
  **  Iteration is done in the reverse element order.
  ** \param row The index of the row to iterate.
  ** \return end const_reverse_row_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Matrix.
  */
  const_reverse_row_iterator row_rend(const size_type row) const;

  /*!
  **  \brief Returns a read/write reverse iterator that points to the last
  **  element of the column \a column in the %Matrix.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return begin col_reverse_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  */
  reverse_col_iterator col_rbegin(const size_type col);

  
  /*!
  **  \brief Returns a read/write reverse iterator that points one past
  **  the first element of the column \a column in the %Matrix.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return end reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  */
  reverse_col_iterator col_rend(const size_type col);

  /*!
  **  \brief Returns a read-only reverse iterator that points to the last
  **  element of the column \a column in the %Matrix.  
  **  Iteration is done modulo the number of columns and in the reverse 
  ** element order.
  ** \param col The index of the column to iterate.
  ** \return begin const_reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  */
  const_reverse_col_iterator col_rbegin(const size_type col) const;

    
  /*!
  **  \brief Returns a read-only reverse iterator that points one past
  **  the first element of the column \a column in the %Matrix.  
  **  Iteration is done modulo the number of columns and in the reverse 
  **  element order.
  ** \param col The index of the column to iterate.
  ** \return end const_reverse_col_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  */
  const_reverse_col_iterator col_rend(const size_type col) const;
 

  /*!
  **  \brief Returns a read-write iterator that points to the last
  **  element of the %Range \a range of the row \a row in the %Matrix.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  */
  reverse_row_range_iterator row_rbegin(const size_type row,
					const slip::Range<int>& range);

 
  /*!
  **  \brief Returns a read-write iterator that points one before
  **  the first element of the %Range \a range of the row \a row in the 
  **  %Matrix.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate.
  ** \return reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  ** 
  ** \pre row must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  */
  reverse_row_range_iterator row_rend(const size_type row,
				      const slip::Range<int>& range);



  /*!
  **  \brief Returns a read-only iterator that points to the last
  **  element of the %Range \a range of the row \a row in the %Matrix.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate
  ** \return begin const_reverse_row_range_iterator value
  **
  ** \pre row must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  */
  const_reverse_row_range_iterator row_rbegin(const size_type row,
					      const slip::Range<int>& range) const;


   /*!
  **  \brief Returns a read-only iterator that points one before the first
  **  element of the %Range \a range of the row \a row in the %Matrix.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param row The index of the row to iterate.
  ** \param range %Range of the row to iterate
  ** \return const_reverse_row_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre row must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  */
  const_reverse_row_range_iterator row_rend(const size_type row,
					    const slip::Range<int>& range) const;
 
 


  /*!
  **  \brief Returns a read-write iterator that points to the last
  **  element of the %Range \a range of the col \a col in the %Matrix.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  */
  reverse_col_range_iterator col_rbegin(const size_type col,
					const slip::Range<int>& range);

  /*!
  **  \brief Returns a read-write iterator that points to one before 
  **  the first element of the %Range range of the col \a col in the 
  **  %Matrix.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  */
  reverse_col_range_iterator col_rend(const size_type col,
				      const slip::Range<int>& range);


  /*!
  **  \brief Returns a read_only iterator that points to the last
  **  element of the %Range \& range of the col \a col in the %Matrix.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return begin const_reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  */
  const_reverse_col_range_iterator 
  col_rbegin(const size_type col,
	     const slip::Range<int>& range) const;

 
  /*!
  **  \brief Returns a read-only iterator that points to the first
  **  element of the %Range \a range of the col \a col in the %Matrix.  
  **  Iteration is done in the reverse element order according to the
  **  %Range.
  ** \param col The index of the column to iterate.
  ** \param range %Range of the column to iterate.
  ** \return const_reverse_col_range_iterator value
  ** \remarks This iterator is compatible with RandomAccessIterator 
  **          algorithms.
  **
  ** \pre col must be compatible with the range of the %Matrix.
  ** \pre The range must be inside the whole range of the %Matrix.
  */
  const_reverse_col_range_iterator col_rend(const size_type col,
					    const slip::Range<int>& range) const;


  /*!
  **  \brief Returns a read/write iterator2d that points to the first
  **  element of the %Matrix. It points to the upper left element of
  **  the %Matrix.
  **  
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  iterator2d upper_left();
  
  
  /*!
  **  \brief Returns a read/write iterator2d that points to the past
  **  the end element of the %Matrix. It points to past the end element of
  **  the bottom right element of the %Matrix.
  **  
  ** \return begin iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  iterator2d bottom_right();


  /*!
  **  \brief Returns a read-only iterator2d that points to the first
  **  element of the %Matrix. It points to the upper left element of
  **  the %Matrix.
  **  
  ** \return begin const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  const_iterator2d upper_left() const;
  
 
  /*!
  **  \brief Returns a read-only iterator2d that points to the past
  **  the end element of the %Matrix. It points to past the end element of
  **  the bottom right element of the %Matrix.
  **  
  ** \return begin const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(8,8);
  ** //copy the elements of A1 in A2
  ** std::copy(A1.upper_left(),A1.bottom_right(),A2.upper_left());
  ** \endcode
  */
  const_iterator2d bottom_right() const;

  /*!
  **  \brief Returns a read/write iterator2d that points to the first
  **  element of the %Matrix. It points to the upper left element of
  **  the \a %Box2d associated to the %Matrix.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %Matrix.
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **  
  ** \return end iterator2d value
  ** \pre The box indices must be inside the range of the %Matrix ones.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
 */
  iterator2d upper_left(const Box2d<int>& box);

 
  /*!
  **  \brief Returns a read/write iterator2d that points to the past
  **  the end element of the %Matrix. It points to past the end element of
  **  the bottom right element of the \a %Box2d associated to the %Matrix.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %Matrix.
  **
  ** \return end iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \pre The box indices must be inside the range of the %Matrix ones.
  **  
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  iterator2d bottom_right(const Box2d<int>& box);


  /*!
  **  \brief Returns a read only iterator2d that points to the first
  **  element of the %Matrix. It points to the upper left element of
  **  the \a %Box2d associated to the %Matrix.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %Matrix.
  ** \return end const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **
  ** \pre The box indices must be inside the range of the %Matrix ones.
  ** 
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  const_iterator2d upper_left(const Box2d<int>& box) const;

 
  /*!
  **  \brief Returns a read only iterator2d that points to the past
  **  the end element of the %Matrix. It points to past the end element of
  **  the bottom right element of the \a %Box2d associated to the %Matrix.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %Matrix.
  ** \return end const iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  **
  ** \pre The box indices must be inside the range of the %Matrix ones.
  ** 
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(3,2);
  **  slip::Box2d<int> box(1,1,3,2);
  ** //copy the elements of A1 inside the box which upper_left element 
  ** //is located in (1,1) and the bottom right element is in (3,2)
  ** std::copy(A1.upper_left(box),A1.bottom_right(box),A2.upper_left());
  ** \endcode
  */
  const_iterator2d bottom_right(const Box2d<int>& box) const;


  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **  upper left element of the ranges \a row_range and \a col_range 
  **  associated to the %Matrix.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range upper_left(const Range<int>& row_range,
			      const Range<int>& col_range);

  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **   past the end bottom right element of the ranges \a row_range 
  **   and \a col_range associated to the %Matrix.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range bottom_right(const Range<int>& row_range,
				const Range<int>& col_range);


  /*!
  **  \brief Returns a read-only iterator2d_range that points to the
  **   to the upper left element of the ranges \a row_range and \a col_range 
  **   associated to the %Matrix.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid.
  ** \pre The ranges indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range upper_left(const Range<int>& row_range,
				    const Range<int>& col_range) const;


 /*!
  **  \brief Returns a read-only iterator2d_range that points to the past
  **  the end bottom right element of the ranges \a row_range and \a col_range 
  **  associated to the %Matrix.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(4,4);
  **  slip::Range<int> row_range(0,A1.dim2()-1,2);
  **  slip::Range<int> col_range(0,A1.dim1()-1,2);
  ** //copy the elements of A1 within the ranges row_range and col_range
  ** //in A2
  ** std::copy(A1.upper_left(row_range,col_range),
  ** A1.bottom_right(row_range,col_range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range bottom_right(const Range<int>& row_range,
				      const Range<int>& col_range) const;


 


  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **  upper left element of the %Range \a range associated to the %Matrix.
  **  The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range and must be valid. 
  ** \pre The range indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range upper_left(const Range<int>& range);
 

  /*!
  **  \brief Returns a read/write iterator2d_range that points to the 
  **   past the end bottom right element of the %Range \a range 
  **   associated to the %Matrix.
  **   The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  iterator2d_range bottom_right(const Range<int>& range);

 

  
  /*!
  **  \brief Returns a read-only iterator2d_range that points to the
  **   to the upper left element of the %Range \a range
  **   associated to the %Matrix.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range must be valid.
  ** \pre The range indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range upper_left(const Range<int>& range) const;

 

  /*!
  **  \brief Returns a read-only const_iterator2d_range that points to the 
  **   past the end bottom right element of the %Range \a range 
  **   associated to the %Matrix.
  **   The same range is applied for rows and cols. 
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return const_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  ** \par Example:
  ** \code
  ** 
  **  slip::Matrix<double> A1(8,8);
  **  slip::Matrix<double> A2(4,4);
  **  slip::Range<int> range(0,A1.dim2()-1,2);
  ** //copy the elements of A1 within the range range
  ** //in A2
  ** std::copy(A1.upper_left(range),A1.bottom_right(range),A2.upper_left());
  ** \endcode
  */
  const_iterator2d_range bottom_right(const Range<int>& range) const;

 
  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to the 
  **   bottom right element of the %Matrix. 
  *    Iteration is done within the %Matrix in the reverse order.
  **  
  ** \return reverse_iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rupper_left();
  
  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to past the 
  **  upper left element of the %Matrix.
  **  Iteration is done in the reverse order.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rbottom_right();

  /*!
  **  \brief Returns a read only reverse iterator2d that points.  It points 
  **  to the bottom right element of the %Matrix. 
  **  Iteration is done within the %Matrix in the reverse order.
  **  
  ** \return const_reverse_iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rupper_left() const;
 
 
  /*!
  **  \brief Returns a read only reverse iterator2d. It points to past the 
  **  upper left element of the %Matrix.
  **  Iteration is done in the reverse order.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rbottom_right() const;
  

  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to the 
  **  bottom right element of the \a %Box2d associated to the %Matrix.
  **  Iteration is done in the reverse order.
  **
  ** \param box a %Box2d defining the range of indices to iterate
  **        within the %Matrix.
  **
  ** \pre The box indices must be inside the range of the %Matrix ones.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rupper_left(const Box2d<int>& box);

  /*!
  **  \brief Returns a read/write reverse iterator2d. It points to one
  **  before the upper left element of the %Box2d \a box associated to 
  **  the %Matrix.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %Matrix.
  **
  ** \pre The box indices must be inside the range of the %Matrix ones.
  **  
  ** \return reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d rbottom_right(const Box2d<int>& box);

  /*!
  **  \brief Returns a read only reverse iterator2d. It points to the 
  **  bottom right element of the %Box2d \a box associated to the %Matrix.
  **  Iteration is done in the reverse order.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %Matrix.
  **
  ** \pre The box indices must be inside the range of the %Matrix ones.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rupper_left(const Box2d<int>& box) const;


  /*!
  **  \brief Returns a read-only reverse iterator2d. It points to one 
  **  before the element of the bottom right element of the %Box2d 
  **  \a box associated to the %Matrix.
  **
  ** \param box A %Box2d defining the range of indices to iterate
  **        within the %Matrix.
  **
  ** \pre The box indices must be inside the range of the %Matrix ones.
  **  
  ** \return const reverse iterator2d value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d rbottom_right(const Box2d<int>& box) const;

 
  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to the 
  **  past the bottom right element of the ranges \a row_range and 
  **  \a col_range associated to the %Matrix. Iteration is done in the 
  **  reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rupper_left(const Range<int>& row_range,
				       const Range<int>& col_range);


 
  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points
  **  to one before the upper left element of the ranges \a row_range 
  **  and \a col_range associated to the %Matrix. Iteration is done
  **  in the reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rbottom_right(const Range<int>& row_range,
					 const Range<int>& col_range);


 
  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points 
  **   to the past the bottom right element of the ranges \a row_range and 
  **   \a col_range associated to the %Matrix. Iteration is done in the 
  **   reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid.
  ** \pre The ranges indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rupper_left(const Range<int>& row_range,
					     const Range<int>& col_range) const;

  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points 
  **  to one before the upper left element of the ranges \a row_range 
  **  and \a col_range associated to the %Matrix.Iteration is done in 
  **  the reverse order.
  **
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  **       
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rbottom_right(const Range<int>& row_range,
					       const Range<int>& col_range) const;


 
 /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to the 
  **  bottom right element of the %Range \a range associated to the %Matrix.
  **  The same range is applied for rows and cols. Iteration is done in the
  **  reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range and must be valid. 
  ** \pre The range indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rupper_left(const Range<int>& range);



  /*!
  **  \brief Returns a read/write reverse_iterator2d_range that points to  
  **   one before the upper left element of the %Range \a range 
  **   associated to the %Matrix.
  **   The same range is applied for rows and cols. Iteration is done
  **   in the reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  reverse_iterator2d_range rbottom_right(const Range<int>& range);
  

  /*!
  **  \brief Returns a read-only reverse_iterator2d_range that points to the
  **   to the bottom right element of the %Range \a range
  **   associated to the %Matrix.
  **  The same range is applied for rows and cols. Iteration is done in the
  **  reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range must be valid.
  ** \pre The range indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rupper_left(const Range<int>& range) const;


  /*!
  **  \brief Returns a read_only reverse_iterator2d_range that points 
  **   to one before the upper left element of the %Range \a range 
  **   associated to the %Matrix.
  **   The same range is applied for rows and cols. Iteration is done in the
  **   reverse order.
  **
  ** \param range The range of the rows and the cols.
  **       
  ** \pre range  must be valid. 
  ** \pre The range indices must be inside the ranges of the %Matrix ones.
  **  
  ** \return const_reverse_iterator2d_range value
  ** \remarks This iterator is compatible with BidirectionalIterator 
  **          algorithms.
  */
  const_reverse_iterator2d_range rbottom_right(const Range<int>& range) const;
  


  /*@} End iterators */

  /**
   ** \name i/o operators
   */
  /*@{*/
 /*!
  ** \brief Write the %Matrix to the ouput stream
  ** \param out output std::ostream
  ** \param a %Matrix to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const self& a);

  /*@} End i/o operators */

  /**
   ** \name  Assignment operators and methods
   */
  /*@{*/
  /*!
  ** \brief Assign a %Matrix.
  **
  ** Assign elements of %Matrix in \a rhs
  **
  ** \param rhs %Matrix to get the values from.
  ** \return 
  */
  Matrix& operator=(const self & rhs);

  /*!
  ** \brief Affects all the element of the %Matrix by val
  ** \param val affectation value
  ** \return reference to corresponding %Matrix
  */
  self& operator=(const T& val);

  

   /*!
   ** \brief Fills the container range [begin(),begin()+size()) 
   **        with copies of value
   ** \param value  A reference-to-const of arbitrary type.
  */
  void fill(const T& value)
  {
    std::fill_n(this->begin(),this->size(),value);
  } 
  
 /*!
  ** \brief Fills the container range [begin(),begin()+size())
  **        with a copy of
  **        the value array
  ** \param value  A pointer of arbitrary type.
  */
  void fill(const T* value)
  {
    std::copy(value,value + this->size(), this->begin());
  } 

  /*!
  ** \brief Fills the container range [begin(),begin()+size()) 
  **        with a copy of the range [first,last)
  **  \param  first  An input iterator.
  **  \param  last   An input iterator.
  **   
  **
  */
  template<typename InputIterator>
  void fill(InputIterator first,
	    InputIterator last)
  {
    std::copy(first,last, this->begin());
  }
  /*@} End Assignment operators and methods*/


 /**
  ** \name Comparison operators
 */
  /*@{*/

  /*!
  ** \brief Matrix equality comparison
  ** \param x A %Matrix
  ** \param y A %Matrix of the same type of \a x
  ** \return true iff the size and the elements of the Arrays are equal
  */
  friend bool operator== <>(const Matrix<T>& x, 
			    const Matrix<T>& y);

 /*!
  ** \brief Matrix inequality comparison
  ** \param x A %Matrix
  ** \param y A %Matrix of the same type of \a x
  ** \return true if !(x == y) 
  */
  friend bool operator!= <>(const Matrix<T>& x, 
			    const Matrix<T>& y);

 /*!
  ** \brief Less than comparison operator (Matrix ordering relation)
  ** \param x A %Matrix
  ** \param y A %Matrix of the same type of \a x
  ** \return true iff \a x is lexicographically less than \a y
  */
  friend bool operator< <>(const Matrix<T>& x, 
			   const Matrix<T>& y);

 /*!
  ** \brief More than comparison operator
  ** \param x A %Matrix
  ** \param y A %Matrix of the same type of \a x
  ** \return true iff y > x 
  */
  friend bool operator> <>(const Matrix<T>& x, 
			   const Matrix<T>& y);

  /*!
  ** \brief Less than equal comparison operator
  ** \param x A %Matrix
  ** \param y A %Matrix of the same type of \a x
  ** \return true iff !(y > x) 
  */
  friend bool operator<= <>(const Matrix<T>& x, 
			    const Matrix<T>& y);

 /*!
  ** \brief More than equal comparison operator
  ** \param x A %Matrix
  ** \param y A %Matrix of the same type of \a x
  ** \return true iff !(x < y) 
  */
  friend bool operator>= <>(const Matrix<T>& x, 
			    const Matrix<T>& y);


  /*@} Comparison operators */
 
/**
   ** \name  Element access operators
   */
  /*@{*/
  /*!
  ** \brief Subscript access to the row datas contained in the %Matrix.
  ** \param i The index of the row for which data should be accessed.
  ** \return Read/write pointer to the row data.
  ** \pre i < rows()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  pointer operator[](const size_type i);

  /*!
  ** \brief Subscript access to the row datas contained in the %Matrix.
  ** \param i The index of the row for which data should be accessed.
  ** \return Read-only (constant) pointer to the row data.
  ** \pre i < rows()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_pointer operator[](const size_type i) const;


  /*!
  ** \brief Subscript access to the data contained in the %Matrix.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read/write reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const size_type i,
		       const size_type j);
 
  /*!
  ** \brief Subscript access to the data contained in the %Matrix.
  ** \param i The index of the row for which the data should be accessed. 
  ** \param j The index of the columns for which the data should be accessed. 
  ** \return Read_only (constant) reference to data.
  ** \pre i < rows()
  ** \pre j < columns()
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const size_type i,
			     const size_type j) const;

   /*!
  ** \brief Subscript access to the data contained in the %Matrix.
  ** \param point2d A %Point2d which indicate the subscripts of the data
  **        to access.
  ** \return Read/write reference to data.
  ** \pre point2d must be defined in the range of the %Matrix.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  reference operator()(const Point2d<size_type>& point2d);
 
  /*!
  ** \brief Subscript access to the data contained in the %Matrix.
  ** \param point2d A %Point2d which indicate the subscripts of the data
  **        to access.
  ** \return Read_only (constant) reference to data.
  ** \pre point2d must be defined in the range of the %Matrix.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  const_reference operator()(const Point2d<size_type>& point2d) const;


  /*!
  ** \brief Subscript access to the data contained in the %Matrix.
  ** \param row_range The range of the rows.
  ** \param col_range The range of the columns.
  ** \return a copy of the range.
  ** \pre row_range and col_range  must be valid. 
  ** \pre The ranges indices must be inside the ranges of the %Matrix ones.
  **
  ** This operator allows for easy, 2d array-style, data access.
  ** Note that data access with this operator is unchecked and
  ** out_of_range lookups are not defined. 
  */
  self operator()(const Range<int>& row_range,
		  const Range<int>& col_range);
 

  /*@} End Element access operators */
  
 /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const;

  /*!
   ** \brief Returns the number of rows (first dimension size) 
   **        in the %Matrix
   */
  size_type dim1() const;

  /*!
   ** \brief Returns the number of rows (first dimension size) 
   **        in the %Matrix
   */
  size_type rows() const;
  
  /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %Matrix
  */
  size_type dim2() const;

   /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %Matrix
  */
  size_type columns() const;

  /*!
  ** \brief Returns the number of columns (second dimension size) 
  **        in the %Matrix
  */
  size_type cols() const;

  /*!
  ** \brief Returns the number of elements in the %Matrix
  */
  size_type size() const;

  /*!
  ** \brief Returns the maximal size (number of elements) in the %Matrix
  */
  size_type max_size() const;

  /*!
  ** \brief Returns true if the %Matrix is empty.  (Thus size() == 0)
  */
  bool empty()const;

  /*!
  ** \brief Swaps data with another %Matrix.
  ** \param M A %Matrix of the same element type
  */
  void swap(self& M);

 
  /**
   ** \name  Arithmetic operators
   */
  /*@{*/
  /*!
  ** \brief Add val to each element of the Matrix  
  ** \param val value
  ** \return reference to the resulting Matrix
  */
  self& operator+=(const T& val);
  self& operator-=(const T& val);
  self& operator*=(const T& val);
  self& operator/=(const T& val);
//   self& operator%=(const T& val);
//   self& operator^=(const T& val);
//   self& operator&=(const T& val);
//   self& operator|=(const T& val);
//   self& operator<<=(const T& val);
//   self& operator>>=(const T& val);


  self  operator-() const;
  //self  operator!() const;

  

  self& operator+=(const self& rhs);
  self& operator-=(const self& rhs);
  self& operator*=(const self& rhs);
  self& operator/=(const self& rhs);
    

  /*@} End Arithmetic operators */

  /**
   ** \name  Mathematical operators
   */
  /*@{*/ 
  /*!
  ** \brief Returns the min element of the %Matrix
  **  according to the operator <, if the T is complex, it returns the
  **  element with the minimal magnitude.
  ** \pre size() != 0
  */
  T& min() const;


  /*!
  ** \brief Returns the max element of the %Matrix 
  ** according to the operator >, , if the T is complex, it returns the
  ** element with the maximal magnitude.
  ** \pre size() != 0
  */
  T& max() const;

  /*!
  ** \brief Returns the sum of the elements of the %Matrix 
  ** \pre size() != 0
  */
  T sum() const;

  /*!
  ** \brief Returns the trace of the elements of the %Matrix 
  ** \pre this->row() == this->cols()
  ** \pre size() != 0
  */
  T trace() const;

  /*!
  ** \brief Returns the determinant of the %Matrix 
  ** \pre this->row() == this->cols()
  ** \pre size() != 0
  */
  T det() const;

  /*!
  ** \brief Returns the condition number \f$\frac{\mu_n}{\mu_k} \f$ of the %Matrix where k is the rank of the matrix.
  ** \pre this->row() == this->cols()
  ** \pre size() != 0
  */
  norm_type cond() const;


  /*!
  ** \brief Returns the rank of the %Matrix.
  ** \pre this->row() == this->cols()
  ** \pre size() != 0
  ** \par Computes the number of singular values that are greater than \f$max(rows,cols)*\mu_{max}*\epsilon\f$
  */
  size_type rank() const;

   /*!
  ** \brief Returns the inverse of the %Matrix 
  ** \pre this->row() == this->cols()
  ** \pre size() != 0
  */
  self inv() const;

 /*!
  ** \brief Returns the L1 norm (\f$\max_j\sum_i |a_{ij}|\f$) of the elements of the %Matrix 
  ** \pre size() != 0
  */
  norm_type L1_norm() const
  {
    assert(this->size() != 0);
    return slip::row_norm(*this);
  }

  /*!
  ** \brief Returns the L2 norm (\f$\sqrt(\max \{\lambda_i(MM^*\})\f$) of the elements of the %Matrix 
  ** \pre size() != 0
  */
  norm_type L2_norm() const
  {
    assert(this->size() != 0);
    slip::Array2d<T> U(this->rows(),this->cols());
    slip::Array2d<T> V(this->cols(),this->cols());
    slip::Array<T> W(this->cols());
    slip::svd(*this,U,W.begin(),W.end(),V);
    return std::abs(W[0]);
  }

  /*!
  ** \brief Returns the infinite norm (\f$\max_i\sum_j |a_{ij}|\f$) of the elements of the %Matrix 
  ** \pre size() != 0
  */
  norm_type infinite_norm() const
  {
    return slip::col_norm(*this);
  }
  
  /*!
  ** \brief Returns the Frobenius norm of  the %Matrix \f$\sum_{i,j} \bar{a_{ij}}a_{i,j}\f$
  ** \pre size() != 0
  */
  norm_type frobenius_norm() const
  {
    return slip::frobenius_norm(*this);
  }

  /*!
  ** \brief Returns the spectral radius of  the %Matrix (\f$\max_i
  ** |\lambda_{i}|\f$) 
  ** \pre size() != 0
  */
  norm_type spectral_radius() const
  {
    return slip::spectral_radius(*this);
  }
    
  /*!
  ** \brief Applys the one-parameter C-function \a fun 
  **        to each element of the %Matrix
  ** \param fun The one-parameter C function 
  ** \return the resulting %Matrix
  ** \par Example:
  ** \code
  ** slip::Matrix<double> M(7,7);
  ** //fill M with values from 1 to 49 with step 1
  ** slip::iota(M.begin(),M.end(),1.0,1.0);
  ** //apply std::sqrt to each element of M
  ** M.apply(std::sqrt);
  **
  ** \endcode
  */
  Matrix<T>& apply(T (*fun)(T));

  /*!
  ** \brief Applys the one-parameter C-function \a fun 
  **        to each element of the %Matrix
  ** \param fun The one-const-parameter C function 
  ** \return the resulting %Matrix
  ** \par Example:
  ** \code
  ** slip::Matrix<double> M(7,7);
  ** //fill M with values from 1 to 49 with step 1
  ** slip::iota(M.begin(),M.end(),1.0,1.0);
  ** //apply std::sqrt to each element of M
  ** M.apply(std::sqrt);
  **
  ** \endcode
  */
  Matrix<T>& apply(T (*fun)(const T&));

  /*@} End Mathematical operators */
 
 private:
  Array2d<T>* array_;

  private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & this->array_;
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & this->array_;
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

///double alias
  typedef slip::Matrix<double> Matrix_d;
  ///float alias
  typedef slip::Matrix<float> Matrix_f;
  ///long alias
  typedef slip::Matrix<long> Matrix_l;
  ///unsigned long alias
  typedef slip::Matrix<unsigned long> Matrix_ul;
  ///short alias
  typedef slip::Matrix<short> Matrix_s;
  ///unsigned long alias
  typedef slip::Matrix<unsigned short> Matrix_us;
  ///int alias
  typedef slip::Matrix<int> Matrix_i;
  ///unsigned int alias
  typedef slip::Matrix<unsigned int> Matrix_ui;
  ///char alias
  typedef slip::Matrix<char> Matrix_c;
  ///unsigned char alias
  typedef slip::Matrix<unsigned char> Matrix_uc;

}//slip::
namespace slip{
/**
 ** \name Arithmetical operators
 */
 /*@{*/
  /*!
  ** \brief pointwise addition of two %Matrix
  ** \param M1 first %Matrix 
  ** \param M2 seconf %Matrix
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %Matrix
  */
template<typename T>
Matrix<T> operator+(const Matrix<T>& M1, 
		    const Matrix<T>& M2);

 /*!
  ** \brief addition of a scalar to each element of a %Matrix
  ** \param M1 the %Matrix 
  ** \param val the scalar
  ** \return resulting %Matrix
  */
template<typename T>
Matrix<T> operator+(const Matrix<T>& M1, 
		    const T& val);

 /*!
  ** \brief addition of a scalar to each element of a %Matrix
  ** \param val the scalar
  ** \param M1 the %Matrix  
  ** \return resulting %Matrix
  */
  template<typename T>
  Matrix<T> operator+(const T& val, 
		      const Matrix<T>& M1);
  
/*!
  ** \brief pointwise substraction of two %Matrix
  ** \param M1 first %Matrix 
  ** \param M2 seconf %Matrix
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %Matrix
  */
template<typename T>
Matrix<T> operator-(const Matrix<T>& M1, 
		    const Matrix<T>& M2);

 /*!
  ** \brief substraction of a scalar to each element of a %Matrix
  ** \param M1 the %Matrix 
  ** \param val the scalar
  ** \return resulting %Matrix
  */
template<typename T>
Matrix<T> operator-(const Matrix<T>& M1, 
		    const T& val);

 /*!
  ** \brief substraction of a scalar to each element of a %Matrix
  ** \param val the scalar
  ** \param M1 the %Matrix  
  ** \return resulting %Matrix
  */
  template<typename T>
  Matrix<T> operator-(const T& val, 
		      const Matrix<T>& M1);
  
/*!
  ** \brief pointwise multiplication of two %Matrix
  ** \param M1 first %Matrix 
  ** \param M2 seconf %Matrix
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %Matrix
  */
  template<typename T>
  Matrix<T> operator*(const Matrix<T>& M1, 
		      const Matrix<T>& M2);

 /*!
  ** \brief multiplication of a scalar to each element of a %Matrix
  ** \param M1 the %Matrix 
  ** \param val the scalar
  ** \return resulting %Matrix
  */
  template<typename T>
  Matrix<T> operator*(const Matrix<T>& M1, 
		      const T& val);

 /*!
  ** \brief multiplication of a scalar to each element of a %Matrix
  ** \param val the scalar
  ** \param M1 the %Matrix  
  ** \return resulting %Matrix
  */
  template<typename T>
  Matrix<T> operator*(const T& val, 
		      const Matrix<T>& M1);
  
  /*!
  ** \brief pointwise division of two %Matrix
  ** \param M1 first %Matrix 
  ** \param M2 seconf %Matrix
  ** \pre M1.dim1() == M2.dim1()
  ** \pre M1.dim2() == M2.dim2() 
  ** \return resulting %Matrix
  */
  template<typename T>
  Matrix<T> operator/(const Matrix<T>& M1, 
		      const Matrix<T>& M2);

 /*!
  ** \brief division of a scalar to each element of a %Matrix
  ** \param M1 the %Matrix 
  ** \param val the scalar
  ** \return resulting %Matrix
  */
  template<typename T>
  Matrix<T> operator/(const Matrix<T>& M1, 
		      const T& val);
/* @} */

  /*!
  ** \relates Matrix
  ** \brief Returns the min element of a Matrix, if the T is complex, it returns  ** the element with the minimal magnitude.
  ** \param M1 the Matrix  
  ** \return the min element
  */
  template<typename T>
  T& min(const Matrix<T>& M1);
  
  /*!
  ** \relates Matrix
  ** \brief Returns the max element of a Matrix, if the T is complex, it returns
  **  the element with the maximal magnitude.
  ** \param M1 the Matrix  
  ** \return the min element
  */
  template<typename T>
  T& max(const Matrix<T>& M1);

  /*!
  ** \relates Matrix
  ** \brief Returns the abs value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  template<typename T>
  Matrix<typename slip::lin_alg_traits<T>::value_type> abs(const Matrix<T>& V);

  /*!
  ** \relates Matrix
  ** \brief Returns the sqrt value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  template<typename T>
  Matrix<T> sqrt(const Matrix<T>& V);

  /*!
  ** \relates Matrix
  ** \brief Returns the cos value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  template<typename T>
  Matrix<T> cos(const Matrix<T>& V);

  /*!
  ** \relates Matrix
  ** \brief Returns the acos value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  Matrix<float> acos(const Matrix<float>& V);
  /*!
  ** \relates Matrix
  ** \brief Returns the acos value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  Matrix<double> acos(const Matrix<double>& V);
  /*!
  ** \relates Matrix
  ** \brief Returns the acos value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  Matrix<long double> acos(const Matrix<long double>& V);
  

  /*!
  ** \relates Matrix
  ** \brief Returns the sin value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  template<typename T>
  Matrix<T> sin(const Matrix<T>& V);

 
/*!
  ** \relates Matrix
  ** \brief Returns the asin value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  Matrix<float> asin(const Matrix<float>& V);
  /*!
  ** \relates Matrix
  ** \brief Returns the asin value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  Matrix<double> asin(const Matrix<double>& V);
  /*!
  ** \relates Matrix
  ** \brief Returns the asin value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  Matrix<long double> asin(const Matrix<long double>& V);
  

  /*!
  ** \relates Matrix
  ** \brief Returns the tan value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  template<typename T>
  Matrix<T> tan(const Matrix<T>& V);

 
  /*!
  ** \relates Matrix
  ** \brief Returns the atan value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  Matrix<float> atan(const Matrix<float>& V);
  /*!
  ** \relates Matrix
  ** \brief Returns the atan value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  Matrix<double> atan(const Matrix<double>& V);
  /*!
  ** \relates Matrix
  ** \brief Returns the atan value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  Matrix<long double> atan(const Matrix<long double>& V);
  

  /*!
  ** \relates Matrix
  ** \brief Returns the exp value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  template<typename T>
  Matrix<T> exp(const Matrix<T>& V);

  /*!
  ** \relates Matrix
  ** \brief Returns the log value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  template<typename T>
  Matrix<T> log(const Matrix<T>& V);

  /*!
  ** \relates Matrix
  ** \brief Returns the cosh value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  template<typename T>
  Matrix<T> cosh(const Matrix<T>& V);

  /*!
  ** \relates Matrix
  ** \brief Returns the sinh value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  template<typename T>
  Matrix<T> sinh(const Matrix<T>& V);

  /*!
  ** \relates Matrix
  ** \brief Returns the tanh value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  template<typename T>
  Matrix<T> tanh(const Matrix<T>& V);

  /*!
  ** \relates Matrix
  ** \brief Returns the log10 value of each element of the %Matrix
  ** \param V The %Matrix  
  ** \return the resulting %Matrix
  */
  template<typename T>
  Matrix<T> log10(const Matrix<T>& V);
  

}//slip::

namespace slip
{
  template<typename T>
  inline
  Matrix<T>::Matrix():
    array_(new slip::Array2d<T>())
  {}

  template<typename T>
  inline
  Matrix<T>::Matrix(const typename Matrix<T>::size_type d1,
		    const typename Matrix<T>::size_type d2):
    array_(new slip::Array2d<T>(d1,d2))
  {}
  
  template<typename T>
  inline
  Matrix<T>::Matrix(const typename Matrix<T>::size_type d1,
		    const typename Matrix<T>::size_type d2, 
		    const T& val):
    array_(new slip::Array2d<T>(d1,d2,val))
  {}

  template<typename T>
  inline
  Matrix<T>::Matrix(const typename Matrix<T>::size_type d1,
		    const typename Matrix<T>::size_type d2, 
		    const T* val):
    array_(new slip::Array2d<T>(d1,d2,val))   
  {}
  
  template<typename T>
  inline
  Matrix<T>::Matrix(const Matrix<T>& rhs):
    array_(new slip::Array2d<T>((*rhs.array_)))
  {}

  template<typename T>
  inline
  Matrix<T>::~Matrix()
  {
    delete array_;
  }
  
  template<typename T>
  inline
  Matrix<T>& Matrix<T>::operator=(const Matrix<T> & rhs)
  {
    if(this != &rhs)
      {
	*array_ = *(rhs.array_);
      }
    return *this;
  }

  template<typename T>
  inline
  Matrix<T>& Matrix<T>::operator=(const T& val)
  {
    std::fill_n((*array_)[0],array_->size(),val);
    return *this;
  }
  
  template<typename T>
  inline
  void Matrix<T>::resize(const typename Matrix<T>::size_type d1,
			 const typename Matrix<T>::size_type d2,
			 const T& val)
  {
    array_->resize(d1,d2,val);
  }


  template<typename T>
  inline
  typename Matrix<T>::iterator Matrix<T>::begin()
  {
    return array_->begin();
  }

  template<typename T>
  inline
  typename Matrix<T>::iterator Matrix<T>::end()
  {
    return array_->end();
  }
  
  template<typename T>
  inline
  typename Matrix<T>::const_iterator Matrix<T>::begin() const
  {
    Array2d<T> const * tp(array_);
    return tp->begin();
  }

  template<typename T>
  inline
  typename Matrix<T>::const_iterator Matrix<T>::end() const
  {
    Array2d<T> const * tp(array_);
    return tp->end();
  }
  

  template<typename T>
  inline
  typename Matrix<T>::reverse_iterator Matrix<T>::rbegin()
  {
    return typename Matrix<T>::reverse_iterator(this->end());
  }

  template<typename T>
  inline
  typename Matrix<T>::reverse_iterator Matrix<T>::rend()
  {
    return typename Matrix<T>::reverse_iterator(this->begin());
  }

  template<typename T>
  inline
  typename Matrix<T>::const_reverse_iterator Matrix<T>::rbegin() const
  {
    return typename Matrix<T>::const_reverse_iterator(this->end());
  }

  template<typename T>
  inline
  typename Matrix<T>::const_reverse_iterator Matrix<T>::rend() const
  {
    return typename Matrix<T>::const_reverse_iterator(this->begin());
  }

  template<typename T>
  inline
  typename Matrix<T>::row_iterator 
  Matrix<T>::row_begin(const typename Matrix<T>::size_type row)
  {
    return array_->row_begin(row);
  }

 template<typename T>
 inline
 typename Matrix<T>::row_iterator 
 Matrix<T>::row_end(const typename Matrix<T>::size_type row)
  {
    return array_->row_end(row);
  }

  template<typename T>
  inline
  typename Matrix<T>::col_iterator 
  Matrix<T>::col_begin(const typename Matrix<T>::size_type col)
  {
    return array_->col_begin(col);
  }


  template<typename T>
  inline
  typename Matrix<T>::col_iterator
  Matrix<T>::col_end(const typename Matrix<T>::size_type col)
  {
    return array_->col_end(col);
  }


 template<typename T>
 inline
 typename Matrix<T>::const_row_iterator
 Matrix<T>::row_begin(const typename Matrix<T>::size_type row) const
  {
    Array2d<T> const * tp(array_);
    return tp->row_begin(row);
  }

 template<typename T>
 inline
 typename Matrix<T>::const_row_iterator 
 Matrix<T>::row_end(const typename Matrix<T>::size_type row) const
  {
    Array2d<T> const * tp(array_);
    return tp->row_end(row);
  }

  template<typename T>
  inline
  typename Matrix<T>::const_col_iterator Matrix<T>::col_begin(const typename Matrix<T>::size_type col) const
  {
    Array2d<T> const * tp(array_);
    return tp->col_begin(col);
  }
  
  template<typename T>
  inline
  typename Matrix<T>::const_col_iterator Matrix<T>::col_end(const typename Matrix<T>::size_type col) const
  {
    Array2d<T> const * tp(array_);
    return tp->col_end(col);
  }


  template<typename T>
  inline
  typename Matrix<T>::reverse_row_iterator
  Matrix<T>::row_rbegin(const typename Matrix<T>::size_type row)
  {
    return array_->row_rbegin(row);
  }
 
  template<typename T>
  inline
  typename Matrix<T>::const_reverse_row_iterator 
  Matrix<T>::row_rbegin(const typename Matrix<T>::size_type row) const
  {
    Array2d<T> const * tp(array_);
    return tp->row_rbegin(row);
  }
  
  template<typename T>
  inline
  typename Matrix<T>::reverse_row_iterator Matrix<T>::row_rend(const typename Matrix<T>::size_type row)
  {
    return array_->row_rend(row);
  }
 
  template<typename T>
  inline
  typename Matrix<T>::const_reverse_row_iterator
  Matrix<T>::row_rend(const typename Matrix<T>::size_type row) const
  {
    Array2d<T> const * tp(array_);
    return tp->row_rend(row);
  }

  
  template<typename T>
  inline
  typename Matrix<T>::reverse_col_iterator Matrix<T>::col_rbegin(const typename Matrix<T>::size_type col)
  {
    return array_->col_rbegin(col);
  }
 
  template<typename T>
  inline
  typename Matrix<T>::const_reverse_col_iterator 
  Matrix<T>::col_rbegin(const typename Matrix<T>::size_type col) const
  {
    Array2d<T> const * tp(array_);
    return tp->col_rbegin(col);
  }
  
  template<typename T>
  inline
  typename Matrix<T>::reverse_col_iterator 
  Matrix<T>::col_rend(const typename Matrix<T>::size_type col)
  {
    return array_->col_rend(col);
  }
 
  template<typename T>
  inline
  typename Matrix<T>::const_reverse_col_iterator
  Matrix<T>::col_rend(const typename Matrix<T>::size_type col) const
  {
    Array2d<T> const * tp(array_);
    return tp->col_rend(col);
  }

  template<typename T>
  inline
  typename Matrix<T>::row_range_iterator 
  Matrix<T>::row_begin(const typename Matrix<T>::size_type row,
		       const slip::Range<int>& range)
  {
    return array_->row_begin(row,range);
  }

   template<typename T>
  inline
   typename Matrix<T>::const_row_range_iterator 
   Matrix<T>::row_begin(const typename Matrix<T>::size_type row,
			const slip::Range<int>& range) const
   {
     Array2d<T> const * tp(array_);
     return tp->row_begin(row,range);
   }

  template<typename T>
  inline
  typename Matrix<T>::col_range_iterator 
  Matrix<T>::col_begin(const typename Matrix<T>::size_type col,
		       const slip::Range<int>& range)
  {
    return array_->col_begin(col,range);
  }

   template<typename T>
  inline
   typename Matrix<T>::const_col_range_iterator 
   Matrix<T>::col_begin(const typename Matrix<T>::size_type col,
			const slip::Range<int>& range) const
   {
     Array2d<T> const * tp(array_);
     return tp->col_begin(col,range);
   }
  

  template<typename T>
  inline
  typename Matrix<T>::row_range_iterator 
  Matrix<T>::row_end(const typename Matrix<T>::size_type row,
		       const slip::Range<int>& range)
  {
    return array_->row_end(row,range);
  }

   template<typename T>
  inline
   typename Matrix<T>::const_row_range_iterator 
   Matrix<T>::row_end(const typename Matrix<T>::size_type row,
			const slip::Range<int>& range) const
   {
     Array2d<T> const * tp(array_);
     return tp->row_end(row,range);
   }

  template<typename T>
  inline
  typename Matrix<T>::col_range_iterator 
  Matrix<T>::col_end(const typename Matrix<T>::size_type col,
		     const slip::Range<int>& range)
  {
    return array_->col_end(col,range);
  }

   template<typename T>
  inline
   typename Matrix<T>::const_col_range_iterator 
   Matrix<T>::col_end(const typename Matrix<T>::size_type col,
		      const slip::Range<int>& range) const
   {
     Array2d<T> const * tp(array_);
     return tp->col_end(col,range);
   }

  template<typename T>
  inline
  typename Matrix<T>::reverse_row_range_iterator 
  Matrix<T>::row_rbegin(const typename Matrix<T>::size_type row,
			const slip::Range<int>& range)
  {
     return typename Matrix<T>::reverse_row_range_iterator(this->row_end(row,range));
  }

 template<typename T>
  inline
  typename Matrix<T>::const_reverse_row_range_iterator 
  Matrix<T>::row_rbegin(const typename Matrix<T>::size_type row,
			 const slip::Range<int>& range) const
  {
    return typename Matrix<T>::const_reverse_row_range_iterator(this->row_end(row,range));
  }


  template<typename T>
  inline
  typename Matrix<T>::reverse_col_range_iterator 
  Matrix<T>::col_rbegin(const typename Matrix<T>::size_type col,
			 const slip::Range<int>& range)
  {
    return typename Matrix<T>::reverse_col_range_iterator(this->col_end(col,range));
  }

  template<typename T>
  inline
  typename Matrix<T>::const_reverse_col_range_iterator 
  Matrix<T>::col_rbegin(const typename Matrix<T>::size_type col,
			 const slip::Range<int>& range) const
  {
    return typename Matrix<T>::const_reverse_col_range_iterator(this->col_end(col,range));
  }

  template<typename T>
  inline
  typename Matrix<T>::reverse_row_range_iterator 
  Matrix<T>::row_rend(const typename Matrix<T>::size_type row,
		       const slip::Range<int>& range)
  {
    return typename Matrix<T>::reverse_row_range_iterator(this->row_begin(row,range));
  }

  template<typename T>
  inline
  typename Matrix<T>::const_reverse_row_range_iterator 
  Matrix<T>::row_rend(const typename Matrix<T>::size_type row,
			 const slip::Range<int>& range) const
  {
    return typename Matrix<T>::const_reverse_row_range_iterator(this->row_begin(row,range));
  }


  template<typename T>
  inline
  typename Matrix<T>::reverse_col_range_iterator 
  Matrix<T>::col_rend(const typename Matrix<T>::size_type col,
		       const slip::Range<int>& range)
  {
    return typename Matrix<T>::reverse_col_range_iterator(this->col_begin(col,range));
  }

  template<typename T>
  inline
  typename Matrix<T>::const_reverse_col_range_iterator 
  Matrix<T>::col_rend(const typename Matrix<T>::size_type col,
		       const slip::Range<int>& range) const
  {
    return typename Matrix<T>::const_reverse_col_range_iterator(this->col_begin(col,range));
  }

  template<typename T>
  inline
  typename Matrix<T>::iterator2d Matrix<T>::upper_left()
  {
    return array_->upper_left();
  }

  template<typename T>
  inline
  typename Matrix<T>::const_iterator2d Matrix<T>::upper_left() const
  {
    Array2d<T> const * tp(array_);
    return tp->upper_left();
  }


  template<typename T>
  inline
  typename Matrix<T>::iterator2d Matrix<T>::bottom_right()
  {
    return array_->bottom_right();
  }

  template<typename T>
  inline
  typename Matrix<T>::const_iterator2d Matrix<T>::bottom_right() const
  {
    Array2d<T> const * tp(array_);
    return tp->bottom_right();
  }

  template<typename T>
  inline
  typename Matrix<T>::iterator2d Matrix<T>::upper_left(const Box2d<int>& box)
  {
    return array_->upper_left(box);
  }


  template<typename T>
  inline
  typename Matrix<T>::const_iterator2d Matrix<T>::upper_left(const Box2d<int>& box) const
  {
    Array2d<T> const * tp(array_);
    return tp->upper_left(box);
  }

  template<typename T>
  inline
  typename Matrix<T>::iterator2d Matrix<T>::bottom_right(const Box2d<int>& box)
  {
    return array_->bottom_right(box);
  }

  template<typename T>
  inline
  typename Matrix<T>::const_iterator2d Matrix<T>::bottom_right(const Box2d<int>& box) const
  {
    Array2d<T> const * tp(array_);
    return tp->bottom_right(box);
  }


  template<typename T>
  inline
  typename Matrix<T>::iterator2d_range 
  Matrix<T>::upper_left(const Range<int>& row_range,
			 const Range<int>& col_range)
  {
    return array_->upper_left(row_range,col_range);
  }

  template<typename T>
  inline
  typename Matrix<T>::const_iterator2d_range 
  Matrix<T>::upper_left(const Range<int>& row_range,
			const Range<int>& col_range) const
  {
    Array2d<T> const * tp(array_);
    return tp->upper_left(row_range,col_range);
  }

  template<typename T>
  inline
  typename Matrix<T>::iterator2d_range 
  Matrix<T>::bottom_right(const Range<int>& row_range,
			  const Range<int>& col_range)
  {
    return array_->bottom_right(row_range,col_range);
  }

  template<typename T>
  inline
  typename Matrix<T>::const_iterator2d_range 
  Matrix<T>::bottom_right(const Range<int>& row_range,
			  const Range<int>& col_range) const
  {
    Array2d<T> const * tp(array_);
    return tp->bottom_right(row_range,col_range);
  }

   template<typename T>
  inline
  typename Matrix<T>::iterator2d_range 
  Matrix<T>::upper_left(const Range<int>& range)
  {
    return array_->upper_left(range);
  }

  template<typename T>
  inline
  typename Matrix<T>::const_iterator2d_range 
  Matrix<T>::upper_left(const Range<int>& range) const
  {
    Array2d<T> const * tp(array_);
    return tp->upper_left(range);
  }

  template<typename T>
  inline
  typename Matrix<T>::iterator2d_range 
  Matrix<T>::bottom_right(const Range<int>& range)
  {
    return array_->bottom_right(range);
  }

  template<typename T>
  inline
  typename Matrix<T>::const_iterator2d_range 
  Matrix<T>::bottom_right(const Range<int>& range) const
  {
    Array2d<T> const * tp(array_);
    return tp->bottom_right(range);
  }

  
   template<typename T>
  inline
  typename Matrix<T>::reverse_iterator2d 
  Matrix<T>::rbottom_right()
  {
    return array_->rbottom_right();
  }

  template<typename T>
  inline
  typename Matrix<T>::const_reverse_iterator2d 
  Matrix<T>::rbottom_right() const
  {
    Array2d<T> const * tp(array_);
    return tp->rbottom_right();
  }

  template<typename T>
  inline
  typename Matrix<T>::reverse_iterator2d 
  Matrix<T>::rupper_left()
  {
    return array_->rupper_left();
  }

  template<typename T>
  inline
  typename Matrix<T>::const_reverse_iterator2d 
  Matrix<T>::rupper_left() const
  {
    Array2d<T> const * tp(array_);
    return tp->rupper_left();
  }

  template<typename T>
  inline
  typename Matrix<T>::reverse_iterator2d 
  Matrix<T>::rbottom_right(const Box2d<int>& box)
  {
    return array_->rbottom_right(box);
  }

  template<typename T>
  inline
  typename Matrix<T>::const_reverse_iterator2d 
  Matrix<T>::rbottom_right(const Box2d<int>& box) const
  {
    Array2d<T> const * tp(array_);
    return tp->rbottom_right(box);
  }

  template<typename T>
  inline
  typename Matrix<T>::reverse_iterator2d 
  Matrix<T>::rupper_left(const Box2d<int>& box)
  {
    return array_->rupper_left(box);
  }

  template<typename T>
  inline
  typename Matrix<T>::const_reverse_iterator2d 
  Matrix<T>::rupper_left(const Box2d<int>& box) const
  {
    Array2d<T> const * tp(array_);
    return tp->rupper_left(box);
  }

  template<typename T>
  inline
  typename Matrix<T>::reverse_iterator2d_range 
  Matrix<T>::rupper_left(const Range<int>& row_range,
			  const Range<int>& col_range)
  {
    return array_->rupper_left(row_range,col_range);
  }

  template<typename T>
  inline
  typename Matrix<T>::const_reverse_iterator2d_range 
  Matrix<T>::rupper_left(const Range<int>& row_range,
			 const Range<int>& col_range) const
  {
    Array2d<T> const * tp(array_);
    return tp->rupper_left(row_range,col_range);
  }

  template<typename T>
  inline
  typename Matrix<T>::reverse_iterator2d_range 
  Matrix<T>::rbottom_right(const Range<int>& row_range,
			    const Range<int>& col_range)
  {
    return array_->rbottom_right(row_range,col_range); 
  }

  template<typename T>
  inline
  typename Matrix<T>::const_reverse_iterator2d_range 
  Matrix<T>::rbottom_right(const Range<int>& row_range,
			    const Range<int>& col_range) const
  {
    Array2d<T> const * tp(array_);
    return tp->rbottom_right(row_range,col_range); 
  }

  template<typename T>
  inline
  typename Matrix<T>::reverse_iterator2d_range 
  Matrix<T>::rupper_left(const Range<int>& range)
  {
    return this->rupper_left(range,range);
  }

  template<typename T>
  inline
  typename Matrix<T>::const_reverse_iterator2d_range 
  Matrix<T>::rupper_left(const Range<int>& range) const
  {
    return this->rupper_left(range,range);
  }

  template<typename T>
  inline
  typename Matrix<T>::reverse_iterator2d_range 
  Matrix<T>::rbottom_right(const Range<int>& range)
  {
    return this->rbottom_right(range,range);
  }

  template<typename T>
  inline
  typename Matrix<T>::const_reverse_iterator2d_range 
  Matrix<T>::rbottom_right(const Range<int>& range) const
  {
    return this->rbottom_right(range,range);
  }

  template <typename T>
  inline
  std::ostream& operator<<(std::ostream & out, const Matrix<T>& a)
  {
    out<<*(a.array_);
    return out;
  }



  template<typename T>
  inline
  typename Matrix<T>::pointer 
  Matrix<T>::operator[](const typename Matrix<T>::size_type i)
  {
    return (*array_)[i];
  }
  
  template<typename T>
  inline
  typename Matrix<T>::const_pointer 
  Matrix<T>::operator[](const typename Matrix<T>::size_type i) const 
  {
    return (*array_)[i];
  }
  
  template<typename T>
  inline
  typename Matrix<T>::reference 
  Matrix<T>::operator()(const typename Matrix<T>::size_type i,
			const typename Matrix<T>::size_type j)
  {
    return (*array_)[i][j];
  }
  
  template<typename T>
  inline
  typename Matrix<T>::const_reference 
  Matrix<T>::operator()(const typename Matrix<T>::size_type i,
			const typename Matrix<T>::size_type j) const
  {
    return (*array_)[i][j];
  }

  template<typename T>
  inline
  typename Matrix<T>::reference 
  Matrix<T>::operator()(const Point2d<typename Matrix<T>::size_type>& point2d)
   {
     return (*array_)(point2d);
   }

  template<typename T>
  inline
  typename Matrix<T>::const_reference 
  Matrix<T>::operator()(const Point2d<typename Matrix<T>::size_type>& point2d) const
   {
     return (*array_)(point2d);
   }


  template<typename T>
  inline
  Matrix<T> 
  Matrix<T>::operator()(const Range<int>& row_range,
			 const Range<int>& col_range)
  {
    assert((*this)[0] != 0);
    assert(row_range.is_valid());
    assert(row_range.start() < int(this->dim2()));
    assert(row_range.stop()  < int(this->dim2()));
    assert(col_range.is_valid());
    assert(col_range.start() < int(this->dim1()));
    assert(col_range.stop()  < int(this->dim1()));
    std::size_t rows = row_range.iterations();
    std::size_t cols = col_range.iterations();
    return Matrix<T>(rows+1,cols+1,array_->upper_left(row_range,col_range),
		       array_->bottom_right(row_range,col_range));
  } 

  template<typename T>
  inline
  std::string 
  Matrix<T>::name() const {return "Matrix";} 
 

  template<typename T>
  inline
  typename Matrix<T>::size_type Matrix<T>::dim1() const {return array_->dim1();} 
  
  template<typename T>
  inline
  typename Matrix<T>::size_type Matrix<T>::rows() const {return this->dim1();} 
  

  template<typename T>
  inline
  typename Matrix<T>::size_type Matrix<T>::dim2() const {return array_->dim2();} 
  
  template<typename T>
  inline
  typename Matrix<T>::size_type Matrix<T>::columns() const {return this->dim2();} 
 
  template<typename T>
  inline
  typename Matrix<T>::size_type Matrix<T>::cols() const {return this->dim2();} 
 
  template<typename T>
  inline
  typename Matrix<T>::size_type Matrix<T>::size() const {return array_->size();}

  template<typename T>
  inline
  typename Matrix<T>::size_type Matrix<T>::max_size() const {return array_->max_size();}


  template<typename T>
  inline
  bool Matrix<T>::empty()const {return array_->empty();}

  template<typename T>
  inline
  void Matrix<T>::swap(Matrix<T>& M)
  {
    array_->swap(*(M.array_));
  }


  template<typename T>
  inline
  Matrix<T>& Matrix<T>::operator+=(const T& val)
  {
    std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::plus<T>(),val));
    return *this;
  }
  
  template<typename T>
  inline
  Matrix<T>& Matrix<T>::operator-=(const T& val)
  {
    std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::minus<T>(),val));
    return *this;
  }

  template<typename T>
  inline
  Matrix<T>& Matrix<T>::operator*=(const T& val)
  {
    std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::multiplies<T>(),val));
    return *this;
  }
  
  template<typename T>
  inline
  Matrix<T>& Matrix<T>::operator/=(const T& val)
  {
    std::transform(array_->begin(),array_->end(),array_->begin(),std::bind2nd(std::divides<T>(),val));
    return *this;
  }
  
  template<typename T>
  inline
  Matrix<T> Matrix<T>::operator-() const
  {
    Matrix<T> tmp(*this);
    std::transform(array_->begin(),array_->end(),tmp.begin(),std::negate<T>());
    return tmp;
  }

  template<typename T>
  inline
  Matrix<T>& Matrix<T>::operator+=(const Matrix<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::plus<T>());
    return *this;
  }
  
  template<typename T>
  inline
  Matrix<T>& Matrix<T>::operator-=(const Matrix<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::minus<T>());
    return *this;
  }
  
  template<typename T>
  inline
  Matrix<T>& Matrix<T>::operator*=(const Matrix<T>& rhs)
  {
    assert(this->dim1() == rhs.dim1());
    assert(this->dim2() == rhs.dim2());
    std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::multiplies<T>());
    return *this;
  }
  
  template<typename T>
  inline
  Matrix<T>& Matrix<T>::operator/=(const Matrix<T>& rhs)
  {
    assert(this->size() == rhs.size());
    std::transform(array_->begin(),array_->end(),(*(rhs.array_)).begin(),array_->begin(),std::divides<T>());
    return *this;
  }
	
 

  template<typename T>
  inline
  T& Matrix<T>::min() const
  {
    assert(array_->size() != 0);
    return *std::min_element(array_->begin(),array_->end(),std::less<T>());
  }

  template<typename T>
  inline
  T& Matrix<T>::max() const
  {
    assert(array_->size() != 0);
    return *std::max_element(array_->begin(),array_->end(),std::less<T>());
  }

  template<typename T>
  inline
  T Matrix<T>::sum() const
  {
    assert(array_->size() != 0);
    return std::accumulate(array_->begin(),array_->end(),T());
  }

  template<typename T>
  inline
  T Matrix<T>::trace() const
  {
    assert(array_->size() != 0);
    assert(this->rows() == this->cols());
    T sum = (*this)[0][0];
    for(std::size_t i = 1; i < this->rows(); ++i)
      {
	sum += (*this)[i][i];
      }
    return sum;
  }

  template<typename T>
  inline
  T Matrix<T>::det() const
  {
    assert(array_->size() != 0);
    assert(this->rows() == this->cols());
    return T(slip::lu_det(*this));
  }

  template<typename T>
  inline
  typename Matrix<T>::size_type Matrix<T>::rank() const
  {
    typename Matrix<T>::size_type r = typename Matrix<T>::size_type(0);
    //computes the SVD
    slip::Array2d<T> U(this->rows(),this->cols());
    slip::Array2d<T> V(this->cols(),this->cols());
    slip::Array<T> W(this->cols());
    slip::svd(*this,U,W.begin(),W.end(),V);
    norm_type max = std::abs(W[0]);
    norm_type tol = norm_type(std::max(this->rows(),this->cols())) * max * slip::epsilon<T>();
    for(std::size_t i = 0; i <  W.size(); ++i)
      {
	norm_type tmp = std::abs(W[i]);
	if(tmp > tol) 
	  {
	    r = (i + 1);
	  }
	else
	  {
	    break;
	  }
      }
    return r;
  }

  template<typename T>
  inline
  typename Matrix<T>::norm_type Matrix<T>::cond() const
  {
    assert(array_->size() != 0);
    assert(this->rows() == this->cols());
    slip::Array2d<T> U(this->rows(),this->cols());
    slip::Array2d<T> V(this->cols(),this->cols());
    slip::Array<T> W(this->cols());
    slip::svd(*this,U,W.begin(),W.end(),V);
    norm_type max = std::abs(W[0]);
    norm_type min = std::abs(W[0]);
    //norm_type cond = norm_type();
    norm_type tol = norm_type(std::max(this->rows(),this->cols())) * max * slip::epsilon<T>();
  
    for(std::size_t i = 1; i <  W.size(); ++i)
      {
	norm_type tmp = std::abs(W[i]);
	if(tmp > tol) 
	  {
	    min = tmp;
	  }
	else
	  {
	    break;
	  }
      }
    
    return max/min;
    
  }

  template<typename T>
  inline
  Matrix<T> Matrix<T>::inv() const
  {
    assert(array_->size() != 0);
    assert(this->rows() == this->cols());
    slip::Matrix<T> tmp(this->rows(),this->cols());
    slip::lu_inv(*this,tmp);
    return tmp;
  }

  template<typename T>
  inline
  Matrix<T>& Matrix<T>::apply(T (*fun)(T))
  {
    slip::apply(this->begin(),this->end(),this->begin(),fun);
    return *this;
  }

  template<typename T>
  inline
  Matrix<T>& Matrix<T>::apply(T (*fun)(const T&))
  {
    slip::apply(this->begin(),this->end(),this->begin(),fun);
    return *this;
  }



  template<typename T>
  inline
  Matrix<T> operator+(const Matrix<T>& M1, 
		      const Matrix<T>& M2)
  {
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    Matrix<T> tmp(M1.dim1(),M1.dim2());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::plus<T>());
    return tmp;
  }

  template<typename T>
  inline
  Matrix<T> operator+(const Matrix<T>& M1, 
		      const T& val)
  {
    Matrix<T> tmp(M1);
    tmp+=val;
    return tmp;
  }

  template<typename T>
  inline
  Matrix<T> operator+(const T& val,
		      const Matrix<T>& M1)
  {
    return M1 + val;
  }


template<typename T>
inline
Matrix<T> operator-(const Matrix<T>& M1, 
		    const Matrix<T>& M2)
{
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    Matrix<T> tmp(M1.dim1(),M1.dim2());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::minus<T>());
    return tmp;
}

template<typename T>
inline
Matrix<T> operator-(const Matrix<T>& M1, 
		    const T& val)
{
    Matrix<T> tmp(M1);
    tmp-=val;
    return tmp;
}

template<typename T>
inline
Matrix<T> operator-(const T& val,
		    const Matrix<T>& M1)
{
  return -(M1 - val);
}

template<typename T>
inline
Matrix<T> operator*(const Matrix<T>& M1, 
		    const Matrix<T>& M2)
{
    assert(M1.dim1() == M2.dim1());
    assert(M1.dim2() == M2.dim2());
    Matrix<T> tmp(M1.dim1(),M1.dim2());
    std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::multiplies<T>());
    return tmp;
}

template<typename T>
inline
Matrix<T> operator*(const Matrix<T>& M1, 
		    const T& val)
{
  Matrix<T> tmp(M1.dim1(),M1.dim2(),
		M1.begin(),M1.end());
    tmp*=val;
    return tmp;
}

template<typename T>
inline
Matrix<T> operator*(const T& val,
		    const Matrix<T>& M1)
{
  return M1 * val;
}

template<typename T>
inline
Matrix<T> operator/(const Matrix<T>& M1, 
		    const Matrix<T>& M2)
{
  assert(M1.dim1() == M2.dim1());
  assert(M1.dim2() == M2.dim2());
  Matrix<T> tmp(M1.dim1(),M1.dim2());
  std::transform(M1.begin(),M1.end(),M2.begin(),tmp.begin(),std::divides<T>());
    return tmp;
}

template<typename T>
inline
Matrix<T> operator/(const Matrix<T>& M1, 
		    const T& val)
{
  Matrix<T> tmp(M1.dim1(),M1.dim2(),
		M1.begin(),M1.end());
    tmp/=val;
    return tmp;
}

// template<typename T>
// inline
// Matrix<T> operator/(const T& val,
// 		    const Matrix<T>& M1)
// {
//   return M1 / val;
// }


  template<typename T>
  inline
  T& min(const Matrix<T>& M1)
  {
    return M1.min();
  }

template<typename T>
inline
T& max(const Matrix<T>& M1)
{
  return M1.max();
}

template<typename T>
inline
Matrix<typename slip::lin_alg_traits<T>::value_type> abs(const Matrix<T>& M)
{
  
  Matrix<typename slip::lin_alg_traits<T>::value_type> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::abs);
  return tmp;
}

template<typename T>
inline
Matrix<T> sqrt(const Matrix<T>& M)
{
  Matrix<T> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::sqrt);
  return tmp;
}

template<typename T>
inline
Matrix<T> cos(const Matrix<T>& M)
{
  Matrix<T> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::cos);
  return tmp;
}


inline
Matrix<float> acos(const Matrix<float>& M)
{
  Matrix<float> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::acos);
  return tmp;
}

inline
Matrix<double> acos(const Matrix<double>& M)
{
  Matrix<double> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::acos);
  return tmp;
}

inline
Matrix<long double> acos(const Matrix<long double>& M)
{
  Matrix<long double> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::acos);
  return tmp;
}

template<typename T>
inline
Matrix<T> sin(const Matrix<T>& M)
{
  Matrix<T> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::sin);
  return tmp;
}


inline
Matrix<float> asin(const Matrix<float>& M)
{
  Matrix<float> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::asin);
  return tmp;
}

inline
Matrix<double> asin(const Matrix<double>& M)
{
  Matrix<double> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::asin);
  return tmp;
}

inline
Matrix<long double> asin(const Matrix<long double>& M)
{
  Matrix<long double> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::asin);
  return tmp;
}

template<typename T>
inline
Matrix<T> tan(const Matrix<T>& M)
{
  Matrix<T> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::tan);
  return tmp;
}


inline
Matrix<float> atan(const Matrix<float>& M)
{
  Matrix<float> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::atan);
  return tmp;
}

inline
Matrix<double> atan(const Matrix<double>& M)
{
  Matrix<double> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::atan);
  return tmp;
}

inline
Matrix<long double> atan(const Matrix<long double>& M)
{
  Matrix<long double> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::atan);
  return tmp;
}

template<typename T>
inline
Matrix<T> exp(const Matrix<T>& M)
{
  Matrix<T> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::exp);
  return tmp;
}

template<typename T>
inline
Matrix<T> log(const Matrix<T>& M)
{
  Matrix<T> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::log);
  return tmp;
}

template<typename T>
inline
Matrix<T> cosh(const Matrix<T>& M)
{
  Matrix<T> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::cosh);
  return tmp;
}

template<typename T>
inline
Matrix<T> sinh(const Matrix<T>& M)
{
  Matrix<T> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::sinh);
  return tmp;
}

template<typename T>
inline
Matrix<T> tanh(const Matrix<T>& M)
{
  Matrix<T> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::tanh);
  return tmp;
}

template<typename T>
inline
Matrix<T> log10(const Matrix<T>& M)
{
  Matrix<T> tmp(M.dim1(),M.dim2());
  slip::apply(M.begin(),M.end(),tmp.begin(),std::log10);
  return tmp;
}
/** \name EqualityComparable functions */
  /* @{ */
template<typename T>
inline
bool operator==(const Matrix<T>& x, 
		const Matrix<T>& y)
{
  return ( x.size() == y.size()
	   && std::equal(x.begin(),x.end(),y.begin())); 
}

template<typename T>
inline
bool operator!=(const Matrix<T>& x, 
		const Matrix<T>& y)
{
  return !(x == y);
}

/* @} */

/** \name LessThanComparable functions */
  /* @{ */
template<typename T>
inline
bool operator<(const Matrix<T>& x, 
	       const Matrix<T>& y)
{
  return std::lexicographical_compare(x.begin(), x.end(),
				      y.begin(), y.end());
}

template<typename T>
inline
bool operator>(const Matrix<T>& x, 
	       const Matrix<T>& y)
{
  return (y < x);
}

template<typename T>
inline
bool operator<=(const Matrix<T>& x, 
		const Matrix<T>& y)
{
  return !(y < x);
}

template<typename T>
inline
bool operator>=(const Matrix<T>& x, 
		const Matrix<T>& y)
{
  return !(x < y);
} 
/* @} */
}//slip::

#endif //SLIP_MATRIX_HPP
