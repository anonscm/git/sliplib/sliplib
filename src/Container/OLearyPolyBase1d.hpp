/** 
 * \file OLearyPolyBase1d.hpp
 * 
 * \brief Provides a class to handle approximation of 1d range by OLeary orthogonal polynomials base.
 * 
 */

#ifndef SLIP_OLEARY_POLYBASE1D_HPP
#define SLIP_OLEARY_POLYBASE1D_HPP

#include <iostream>
#include <string>
#include <vector>
#include "Polynomial.hpp"
#include "Vector.hpp"
#include "Matrix.hpp"
#include "arithmetic_op.hpp"
#include "linear_algebra.hpp"
#include "linear_algebra_svd.hpp"
#include "linear_least_squares.hpp"
#include "macros.hpp"
#include "polynomial_algo.hpp"
#include "discrete_polynomial_algos.hpp"




namespace slip
{
template <typename T>
class OLearyPolyBase1d;
  
template <typename T>
std::ostream& operator<<(std::ostream & out, 
			 const OLearyPolyBase1d<T>& a);
  
template <typename T>
class OLearyPolyBase1d;

/*! \class OLearyPolyBase1d
** 
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.1
** \date 2017/10/30
** \brief a class to handle approximation of 1d range by OLeary orthogonal polynomials base
** \param T Type of data to approximate.
*/
  template <typename T>
class OLearyPolyBase1d
{
  
public:
  
  typedef OLearyPolyBase1d<T> self;
  typedef const self const_self;

   /**
   ** \name Constructors & Destructors
   */

   /*@{*
    /*!
    ** \brief Constructs a OLearyPolyBase1d.
    */
  OLearyPolyBase1d():
    range_size_(0),
    size_(0),
    degree_(-1),
    collocations_(0),
    poly_evaluations_(0),
    reortho_method_(slip::BASE_REORTHOGONALIZATION_NONE)
  {
  }
 
  /*!
    ** \brief Constructs a OLearyPolyBase1d of degree (range_size - 1).
    ** \param range_size size of the 1d container or range.
    */
  OLearyPolyBase1d(const std::size_t range_size):
    range_size_(range_size),
    size_(range_size),
    degree_(range_size-1),
    collocations_(new slip::Vector<T>(range_size)),
    poly_evaluations_(new slip::Matrix<T>(range_size,
					  range_size,
					  T())),
    reortho_method_(slip::BASE_REORTHOGONALIZATION_QR_HOUSEHOLDER)
  {
  }

 /*!
    ** \brief Constructs a OLearyPolyBase1d of degree (range_size - 1).
    ** \param range_size size of the 1d container or range.
    ** \param reortho_method Reorthogonalisation method:
    */
  OLearyPolyBase1d(const std::size_t range_size,
		   const slip::BASE_REORTHOGONALIZATION_METHOD& reortho_method):
    range_size_(range_size),
    size_(range_size),
    degree_(range_size-1),
    collocations_(new slip::Vector<T>(range_size)),
    poly_evaluations_(new slip::Matrix<T>(range_size,
					  range_size,
					  T())),
    reortho_method_(reortho_method)
  {
  }
 
   /*!
    ** \brief Copy constructor
    ** \param other %OLearyPolyBase1d.
    */
  OLearyPolyBase1d(const self& other):
    range_size_(other.range_size_),
    size_(other.size_),
    degree_(other.degree_),
    collocations_(new slip::Vector<T>(*(other.collocations_))),
    poly_evaluations_(new slip::Matrix<T>(*(other.poly_evaluations_))),
    reortho_method_(other.reortho_method_)
  {
  }

  /*!
  ** \brief Destructor of the 
  */
  virtual ~OLearyPolyBase1d()
  {
    this->desallocate();
  }
  
/*@} End Constructors */
  
  /**
   ** \name Assignment operators
   */
  /*@{*
     /*!
    ** \brief Copy constructor
    ** \param other %OLearyPolyBase1d.
    */
  self& operator=(const self& other)
  {
     if(this == &other)
       {
	 return *this;
       }

     this->desallocate();

     this->range_size_ = other.range_size_;
     this->size_ = other.size_;
     this->degree_=other.degree_;
     
     this->collocations_ = new slip::Vector<T>(*(other.collocations_));
    
     this->poly_evaluations_ = new slip::Matrix<T>(*(other.poly_evaluations_));
     this->reortho_method_ = other.reortho_method_;
     return *this;
  }
 /*@} End Assignment operators */


   /**
   ** \name Element access operators
   */
  /*@{*
 
  /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const
  {
    return "OLearyPolyBase1d";
  }
  
  /*!
  ** \brief Returns the degree of the Base
  **
  */
  int degree() const
  {
    return slip::OLearyPolyBase1d<T>::degree_;
  }

  /*!
  ** \brief Returns the order of the Base
  **
  ** \remarks Same value as degree
  */
  int order() const
  {
    return slip::OLearyPolyBase1d<T>::degree_;
  }

  /*!
  ** \brief Returns the size of the base.
  **
  */
  std::size_t size() const
  {
    return slip::OLearyPolyBase1d<T>::size_;
  }

   /*!
  ** \brief Returns the size of the domain
  **
  */
  std::size_t range_size() const
  {
    return slip::OLearyPolyBase1d<T>::range_size_;
  }

  const slip::BASE_REORTHOGONALIZATION_METHOD& reorthogonalization_method() const
  {
    return this->reortho_method_;
  }
  
   /*!
  ** \brief First iterator on the i-th polynomial evaluations on the range.
  ** \param i Index of the polynomial of the base.
  ** \return slip::Matrix<T>::row_iterator 
  */
  typename slip::Matrix<T>::row_iterator 
  poly_begin(const std::size_t i) const

  {
    return (this->poly_evaluations_)->row_begin(i);
  }

  

  /*!
  ** \brief Past-the-end iterator on the i-th polynomial evaluations on the range.
  ** \param i Index of the polynomial of the base.
  ** \return slip::Matrix<T>::row_iterator 
  */
  typename slip::Matrix<T>::row_iterator 
  poly_end(const std::size_t i) const

  {
    return (this->poly_evaluations_)->row_end(i);
  }

   /*!
  ** \brief First reverse iterator on the i-th polynomial evaluations on the range.
  ** \param i Index of the polynomial of the base.
  ** \return slip::Matrix<T>::reverse_row_iterator 
  */
  typename slip::Matrix<T>::reverse_row_iterator 
  poly_rbegin(const std::size_t i) const

  {
    return (this->poly_evaluations_)->row_rbegin(i);
  }

/*!
  ** \brief Past-the-end reverse iterator on the i-th polynomial evaluations on the range.
  ** \param i Index of the polynomial of the base.
  ** \return slip::Matrix<T>::reverse_row_iterator 
  */
  typename slip::Matrix<T>::reverse_row_iterator 
  poly_rend(const std::size_t i) const

  {
    return (this->poly_evaluations_)->row_rend(i);
  }
  
  /*!
  ** \brief Returns the evaluation of the i-th polynomial of the base at the j-th location.
  ** \param i Index of the polynomial of the base.
  ** \param j Index within the data range.
  ** \returns  the evaluation of the i-th polynomial of the base at the j-th location
  */
  T get_poly(const std::size_t i,
	     const std::size_t j) const
  {
    assert(i < this->size_);
    assert(j < this->range_size_);
    return (*this->poly_evaluations_)[i][j];
  }

  template <typename Container2d>
  void get_base(Container2d& base)
  {
    base.resize(this->range_size_,this->size_);
    for(std::size_t i = 0; i < this->size_; ++i)
      {
	std::copy((this->poly_evaluations_)->row_begin(i),
		  (this->poly_evaluations_)->row_end(i),
		  base.col_begin(i));
      }
		    
  }

/*@} End Element access operators */
  
 
 /**
   ** \name i/o operators
   */
  /*@{*/
  
  /*!
  ** \brief Write the %OLearyPolyBase1d to the ouput stream
  ** \param out output std::ostream
  ** \param a %OLearyPolyBase1d to write to the output stream
  */
  friend std::ostream& operator<< <>(std::ostream & out, 
				     const self& a);
  /*@} End i/o operators */


  /*!
  ** \brief Generate the polynomial base.
  */
  void generate()
  {
  
    slip::OLearyPolyBase1d<T>::compute_collocations();
    slip::OLearyPolyBase1d<T>::compute_polynomials();
     switch(this->reortho_method_)
      {
      case slip::BASE_REORTHOGONALIZATION_NONE:
	break;
      case slip::BASE_REORTHOGONALIZATION_GRAM_SCHMIDT:
	slip::OLearyPolyBase1d<T>::gram_schmidt_qr_orthogonalization();
	break;
      case slip::BASE_REORTHOGONALIZATION_QR_HOUSEHOLDER:
	slip::OLearyPolyBase1d<T>::householder_qr_orthogonalization();
	break;
      case slip::BASE_REORTHOGONALIZATION_SVD:
	slip::OLearyPolyBase1d<T>::svd_orthogonalization();
	break;
      case slip::BASE_REORTHOGONALIZATION_PROCRUSTES:
	slip::OLearyPolyBase1d<T>::Procrustes_orthogonalization();
	break;
	
      };
  }

  
  /*!
  ** \brief Project data onto the P(i) polynomial of the base.
  ** \param first RandomAccessIterator on the data.
  ** \param last RandomAccessIterator on the data.
  ** \param i index of the polynomial.
  */
  template <typename RandomAccessIterator1>
  typename std::iterator_traits<RandomAccessIterator1>::value_type 
  project(RandomAccessIterator1 first,
	  RandomAccessIterator1 last,
	  const std::size_t i)
  {
    assert(i <= this->degree_);
    
	return slip::inner_product(first,last,
				   slip::OLearyPolyBase1d<T>::poly_evaluations_->row_begin(i));

  }



  /*!
  ** \brief Project data onto the P(i) polynomial of the base.
  ** \param first RandomAccessIterator on the data.
  ** \param last RandomAccessIterator on the data.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \pre (coef_last - coef_first) <= this->size()
  */
  template <typename RandomAccessIterator1,
	    typename RandomAccessIterator2>
  void project(RandomAccessIterator1 first,
	       RandomAccessIterator1 last,
	       RandomAccessIterator2 coef_first,
	       RandomAccessIterator2 coef_last)
  {
    assert(static_cast<std::size_t>(coef_last - coef_first) <= this->size_);
  
    for(std::size_t i = 0;coef_first!=coef_last; ++i, ++coef_first)
      {
	*coef_first = this->project(first,last,i);
      }
  }
 
  /*!
  ** \brief Reconstruct the data from a coefficients range.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \param K number of coefficients to use for recontruct the data.
  ** \param first RandomAccessIterator on the data.
  ** \param last RandomAccessIterator on the data.
 s.
  ** \pre K <= (coef_last - coef_first)
  */
  template <typename RandomAccessIterator1,
	     typename RandomAccessIterator2>
  void reconstruct(RandomAccessIterator1 coef_first,
		   RandomAccessIterator1 coef_last,
		   const std::size_t K,
		   RandomAccessIterator2 first,
		   RandomAccessIterator2 last)
  {
    assert(K <= static_cast<std::size_t>(coef_last-coef_first)); 
    for(std::size_t j = 0; first != last; ++j, ++first)
      {
	*first = slip::inner_product(coef_first,coef_first+K,
				     slip::OLearyPolyBase1d<T>::poly_evaluations_->col_begin(j));
      }
  }

  

  /*!
  ** \brief Computes the inner_product between Pi and Pj.
  ** \param i index of the first polynomial.
  ** \param j index of the second polynomial.
  ** \return the value of the inner product.
  ** \pre (i >  this->size_)
  ** \pre (j >  this->size_)
  */
  T inner_product(const std::size_t i, const std::size_t j) const
  {
    assert(i < this->size_);
    assert(j < this->size_);
    return slip::inner_product(slip::OLearyPolyBase1d<T>::poly_evaluations_->row_begin(i),
			       slip::OLearyPolyBase1d<T>::poly_evaluations_->row_end(i),
			       slip::OLearyPolyBase1d<T>::poly_evaluations_->row_begin(j));
  }

  /*!
  ** \brief Computes the Gram matrix of the polynomial base.
  ** \param Gram The resulting Gram matrix.
  */
  template<typename Matrix>
  void orthogonality_matrix(Matrix& Gram) const
  {
    Gram.resize(slip::OLearyPolyBase1d<T>::size_,slip::OLearyPolyBase1d<T>::size_);
    for(std::size_t i = 0; i < slip::OLearyPolyBase1d<T>::size_; ++i)
      {
	for(std::size_t j = 0; j <= i; ++j)
	  {
	    Gram[i][j] =  slip::OLearyPolyBase1d<T>::inner_product(i,j);
	    Gram[j][i] = Gram[i][j];
	  }
      }
  }

 
  /*!
  ** \brief Indicate if the polynomial basis is orthogonal according to the tolerance \a tol.
  ** \param tol Tolerance of the orthogonality .
  ** \result True if orthogonal, false else.
  */
  bool is_orthogonal(const T tol 
		   = slip::epsilon<T>()) const
  {
    slip::Matrix<T> Gram;
    slip::OLearyPolyBase1d<T>::orthogonality_matrix(Gram);
    return slip::is_diagonal(Gram,tol);
  }


/*!
  ** \brief Returns the orthogonality precision of the base.
  ** If orthogonality precision > 1e-1 returns 1e-1.
  ** \return orthogonality precision 
  */
  T orthogonality_precision() const
  {
  bool is_ortho = false;
  T eps = slip::epsilon<T>(); 
  slip::Matrix<T> Gram;
  this->orthogonality_matrix(Gram);
  while( (eps < static_cast<T>(1e-1)) && (!is_ortho))
    {
      is_ortho = slip::is_diagonal(Gram,eps);
      eps*=static_cast<T>(10.0);
    }
  if(eps > 1e-1)
    {
      eps = static_cast<T>(1e-1);
    }
  else
    {
      eps /= static_cast<T>(10.0);
    }
  return eps;
  }

   /*!
  ** \brief Returns the rank of the matrix of polynomials
  ** \return rank
  */
  T rank() const
  {
    return (this->poly_evaluations_)->rank();
  }

  /*!
  ** \brief Returns Gram matrix condition number
  ** \return condition number
  */
  T gram_cond() const
  {
     slip::Matrix<T> Gram;
     this->orthogonality_matrix(Gram);
    return Gram.cond();
  }

  /*!
  ** \brief Returns condition number of the base
  ** \return condition number
  */
  T cond() const
  {
    return (this->poly_evaluations_)->cond();
  }

  //Approximate memory size requirements: to calculate...
  //N: size of the range
  //D: degree of the base
private:
  std::size_t range_size_;               ///size of the range
  std::size_t size_;///size of the base : number of polynomials
  int degree_;              ///degree of the base
  slip::Vector<T>* collocations_; ///collocations values
  //of the base: (Pi(collocations_[0]), Pi(collocations_[1]),...)
  slip::Matrix<T>* poly_evaluations_; 
  slip::BASE_REORTHOGONALIZATION_METHOD reortho_method_;
private:
  /*!
  ** \brief Computes the collocations on the range.
  */
  void compute_collocations()
  {
    slip::uniform_collocations(slip::OLearyPolyBase1d<T>::collocations_->begin(),
				   slip::OLearyPolyBase1d<T>::collocations_->end(),-slip::constants<T>::one(),slip::constants<T>::one());
  }
  
 
  /*!
  ** \brief Computes the polynomial basis.
  */
  void compute_polynomials()
  {
    const std::size_t N = this->range_size();
    //compute p0
    const T c0 = slip::constants<T>::one()/std::sqrt(static_cast<T>(N));
    std::fill_n(slip::OLearyPolyBase1d<T>::poly_evaluations_->row_begin(0),N,c0);
    if(this->range_size_ > 1)
      {  
    const T c1 = std::sqrt(static_cast<T>(3*(N-1))/static_cast<T>(N*(N+1)));
    slip::multiplies_scalar(slip::OLearyPolyBase1d<T>::collocations_->begin(),
			    slip::OLearyPolyBase1d<T>::collocations_->end(),
    
			    c1,
			    slip::OLearyPolyBase1d<T>::poly_evaluations_->row_begin(1));
    //start recurrence
    slip::Vector<T> pt(N);
    slip::Vector<T> beta_pnm2(N);
    const std::size_t D = this->degree();
    for(std::size_t i = 2; i <= D; ++i)
      {
	slip::multiplies(slip::OLearyPolyBase1d<T>::poly_evaluations_->row_begin(1),
			 slip::OLearyPolyBase1d<T>::poly_evaluations_->row_end(1),
			 slip::OLearyPolyBase1d<T>::poly_evaluations_->row_begin(i-1),
			 pt.begin());
	
	T gamma = slip::inner_product(pt.begin(),pt.end(),slip::OLearyPolyBase1d<T>::poly_evaluations_->row_begin(i-2));
	T ptTpt =  slip::inner_product(pt.begin(),pt.end(),pt.begin());
	T alpha = slip::constants<T>::one()/std::sqrt(ptTpt-gamma*gamma);
	T beta = -gamma * alpha;
	
	slip::multiplies_scalar(pt.begin(),pt.end(),alpha,pt.begin());
	slip::multiplies_scalar(slip::OLearyPolyBase1d<T>::poly_evaluations_->row_begin(i-2),
				slip::OLearyPolyBase1d<T>::poly_evaluations_->row_end(i-2),
				beta,
				beta_pnm2.begin());
	slip::plus(pt.begin(),pt.end(),beta_pnm2.begin(),slip::OLearyPolyBase1d<T>::poly_evaluations_->row_begin(i));
	
				      
      }
      }
  }

  /*!
  ** \brief Desallocate the polynomial base data.
  */
  void desallocate()
  {
    if(this->collocations_ != 0)
       {
	 delete this->collocations_;
       }
  
  if(this->poly_evaluations_ != 0)
       {
	 delete this->poly_evaluations_;
       }
   
     }
   
  template <typename RowIterator>
  void modified_gram_schmidt_normalized(const std::vector<std::pair<RowIterator,RowIterator> >& base)
  {
   
    slip::discrete_poly_inner_prod<RowIterator,RowIterator,T> inner_prod;
     const std::size_t  init_base_first_size = base.size();
       
     //std::copy(poly_evaluations_->row_begin(0),poly_evaluations_->row_begin(0),
     //       poly_evaluations_->row_begin(0));

     for(std::size_t k = 0; k < init_base_first_size; ++k)
       {
	  T PkPk = std::sqrt(inner_prod(base[k],base[k]));
	  slip::divides_scalar(base[k].first,base[k].second,
			       PkPk,
			       base[k].first);
	  for(std::size_t q = (k + 1); q < init_base_first_size; ++q)
	    {
	      T PkPq = inner_prod(base[q],base[k]);
	      RowIterator it_baseq = base[q].first;
	      RowIterator it_basek = base[k].first;
	      for(; it_baseq != base[q].second; ++it_baseq, ++it_basek)
		{
		  *it_baseq = *it_baseq - PkPq * *it_basek;
		}
	    }
       }

  }
/*!
  ** \brief Gram-Schmidt orthogonormalisation.
  */
 void gram_schmidt_qr_orthogonalization()
  {
    typedef typename slip::Matrix<T>::row_iterator RowIterator;
    std::vector<std::pair<RowIterator,RowIterator> > base(this->size_);
  for(std::size_t i = 0; i < this->size_; ++i)
    {
      base[i].first  = poly_evaluations_->row_begin(i);
      base[i].second = poly_evaluations_->row_end(i);
      
    }
   this->modified_gram_schmidt_normalized(base);
 

   }
  
  void householder_qr_orthogonalization()
  {
    slip::Matrix<T> Q(this->range_size_,this->range_size_);
    slip::Matrix<T> R(this->range_size_,this->range_size_);
    slip::householder_qr(*(this->poly_evaluations_),Q,R);
    *(this->poly_evaluations_) = Q;
  }
  
 void svd_orthogonalization()
  {
    slip::Matrix<T> U(this->range_size_,this->range_size_);
    slip::Vector<T> S(this->range_size_);
    slip::Matrix<T> V(this->range_size_,this->range_size_);
    
    slip::svd(*(this->poly_evaluations_),U,S.begin(),S.end(),V);
    *(this->poly_evaluations_) = U;
  }
      
 void Procrustes_orthogonalization()
  {
    slip::Matrix<T> U(this->range_size_,this->range_size_);
    slip::Vector<T> S(this->range_size_);
    slip::Matrix<T> V(this->range_size_,this->range_size_);
    
    slip::svd(*(this->poly_evaluations_),U,S.begin(),S.end(),V);
    slip::matrix_hmatrix_multiplies(U,V,*(this->poly_evaluations_));
   
  }
};

}//::slip


namespace slip
{
  template <typename T>
  inline
  std::ostream& operator<<(std::ostream & out, 
			   const OLearyPolyBase1d<T>& b)
  {
    out<<"Number of polynomials = "<<b.size()<<std::endl;
    out<<"Base degree = "<<b.degree()<<std::endl;
     
    out<<"Range size = "<<b.range_size()<<std::endl;
    

    out<<"Collocations = \n";
    if(b.collocations_ != 0)
      {
	out<<*(b.collocations_)<<std::endl;
      }

    if(b.poly_evaluations_ != 0)
      {
	out<<"Polynomial evaluations =\n"<<*(b.poly_evaluations_)<<std::endl;
      }
    out<<"Reorthogonalization method:\n";
    switch(b.reortho_method_)
      {
      case slip::BASE_REORTHOGONALIZATION_NONE:
	out<<"none"<<std::endl;
	break;
      case slip::BASE_REORTHOGONALIZATION_GRAM_SCHMIDT:
	out<<"Gram-Schmidt"<<std::endl;
	break;
      case slip::BASE_REORTHOGONALIZATION_QR_HOUSEHOLDER:
	out<<"QR Householder"<<std::endl;
	break;
      case slip::BASE_REORTHOGONALIZATION_SVD:
	out<<"SVD"<<std::endl;
	break;
      case slip::BASE_REORTHOGONALIZATION_PROCRUSTES:
	out<<"Procrustes"<<std::endl;
	break;
      };
    return out;
  }
}//::slip

#endif //SLIP_OLEARY_POLYBASE1D_HPP
