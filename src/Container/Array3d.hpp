/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */



/** 
 * \file Array3d.hpp
 * 
 * \brief Provides a class to manipulate 3d dynamic and generic arrays.
 * 
 */
#ifndef SLIP_ARRAY3D_HPP
#define SLIP_ARRAY3D_HPP
#include <iostream>
#include <iterator>
#include <cassert>
#include <string>
#include <cstddef>
#include "Box3d.hpp"
#include "Point3d.hpp"
#include "DPoint3d.hpp"
#include "stride_iterator.hpp"
#include "iterator3d_plane.hpp"
#include "iterator3d_box.hpp"
#include "iterator3d_range.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/version.hpp>

namespace slip
{
  
  template<class T>
  class stride_iterator;

  template<class T>
  class iterator2d_box;

  template<class T>
  class const_iterator2d_box;

  template <typename T>
  class Array3d;

  template <typename T>
  std::ostream& operator<<(std::ostream & out, 
			   const slip::Array3d<T>& a);

  template<typename T>
  bool operator==(const slip::Array3d<T>& x, 
		  const slip::Array3d<T>& y);

  template<typename T>
  bool operator!=(const slip::Array3d<T>& x, 
		  const slip::Array3d<T>& y);

  template<typename T>
  bool operator<(const slip::Array3d<T>& x, 
		 const slip::Array3d<T>& y);

  template<typename T>
  bool operator>(const slip::Array3d<T>& x, 
		 const slip::Array3d<T>& y);

  template<typename T>
  bool operator<=(const slip::Array3d<T>& x, 
		  const slip::Array3d<T>& y);

  template<typename T>
  bool operator>=(const slip::Array3d<T>& x, 
		  const slip::Array3d<T>& y);

/*!
 *  @defgroup Containers3d 3d Containers
 *  @brief 3d containers having the same interface
 *  @{
 */
  /*! \class Array3d
  **  \ingroup Containers Containers3d
  ** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
  ** \version 0.0.3
  ** \date 2014/03/15
  ** \since 1.0.0
  ** \brief This is a three-dimensional dynamic and generic container.
  ** This container statisfies the BidirectionnalContainer concepts of the STL.
  ** It is also an 2d extension of the RandomAccessContainer concept. That is
  ** to say the bracket element access is replaced by the triple
  ** bracket element access.
  ** \param T Type of the elements in the %Array3d.
  ** \par Axis conventions:
  ** \image html iterator3d_conventions.jpg "axis and notation conventions"
  ** \image latex iterator3d_conventions.eps "axis and notation conventions" width=5cm
  **
  */
  template <typename T>
  class Array3d
  {
  public:

    typedef T value_type;
    typedef Array3d<T> self;
    typedef const Array3d<T> const_self;

    typedef value_type & reference;
    typedef value_type const & const_reference;

    typedef value_type * pointer;
    typedef value_type const * const_pointer;

    typedef ptrdiff_t difference_type;
    typedef std::size_t size_type;

    typedef value_type * iterator;
    typedef value_type const * const_iterator;
    
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    //slice, row and col iterator
    typedef stride_iterator<pointer> slice_iterator;
    typedef stride_iterator<const_pointer> const_slice_iterator;
    typedef pointer row_iterator;
    typedef const_pointer const_row_iterator;
    typedef slip::stride_iterator<pointer> col_iterator;
    typedef slip::stride_iterator<const_pointer> const_col_iterator;
    typedef slip::stride_iterator<slice_iterator> slice_range_iterator;
    typedef slip::stride_iterator<const_slice_iterator> const_slice_range_iterator;
    typedef slip::stride_iterator<pointer> row_range_iterator;
    typedef slip::stride_iterator<const_pointer> const_row_range_iterator;
    typedef slip::stride_iterator<col_iterator> col_range_iterator;
    typedef slip::stride_iterator<const_col_iterator> const_col_range_iterator;

    typedef std::reverse_iterator<slice_iterator> reverse_slice_iterator;
    typedef std::reverse_iterator<const_slice_iterator> const_reverse_slice_iterator;
    typedef std::reverse_iterator<iterator> reverse_row_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_row_iterator;
    typedef std::reverse_iterator<col_iterator> reverse_col_iterator;
    typedef std::reverse_iterator<const_col_iterator> const_reverse_col_iterator;
    typedef std::reverse_iterator<slice_range_iterator> reverse_slice_range_iterator;
    typedef std::reverse_iterator<const_slice_range_iterator> const_reverse_slice_range_iterator;
    typedef std::reverse_iterator<row_range_iterator> reverse_row_range_iterator;
    typedef std::reverse_iterator<const_row_range_iterator> const_reverse_row_range_iterator;
    typedef std::reverse_iterator<col_range_iterator> reverse_col_range_iterator;
    typedef std::reverse_iterator<const_col_range_iterator> const_reverse_col_range_iterator;
    
#ifdef ALL_PLANE_ITERATOR3D
  //generic 1d iterator
    typedef slip::stride_iterator<pointer> iterator1d;
    typedef slip::stride_iterator<const_pointer> const_iterator1d;
    
    typedef std::reverse_iterator<iterator1d> reverse_iterator1d;
    typedef std::reverse_iterator<const_iterator1d> const_reverse_iterator1d;
#endif //ALL_PLANE_ITERATOR3D

    //iterator 2d
    typedef slip::iterator3d_plane<self> iterator2d;
    typedef slip::const_iterator3d_plane<const_self> const_iterator2d;

    typedef std::reverse_iterator<iterator2d> reverse_iterator2d;
    typedef std::reverse_iterator<const_iterator2d> const_reverse_iterator2d;

    //iterator 3d
    typedef slip::iterator3d_box<self> iterator3d;
    typedef slip::const_iterator3d_box<const_self> const_iterator3d;
    typedef slip::iterator3d_range<self> iterator3d_range;
    typedef slip::const_iterator3d_range<const_self> const_iterator3d_range;

    typedef std::reverse_iterator<iterator3d> reverse_iterator3d;
    typedef std::reverse_iterator<const_iterator3d> const_reverse_iterator3d;
    typedef std::reverse_iterator<iterator3d_range> reverse_iterator3d_range;
    typedef std::reverse_iterator<const_iterator3d_range> const_reverse_iterator3d_range;

    //default iterator of the container
    typedef iterator3d default_iterator;
    typedef const_iterator3d const_default_iterator;
   
    static const std::size_t DIM = 3;

    /**
     ** \name Constructors & Destructors
     */
    /*@{*/
  
    /*!
    ** \brief Constructs a %Array3d.
    */
    Array3d();

    /*!
    ** \brief Constructs a %Array3d.
    ** \param d1 first dimension of the %Array3d
    ** \param d2 second dimension of the %Array3d
    ** \param d3 third dimension of the %Array3d
    ** 
    ** \par The %Array3d is initialized by the default value of T. 
    */
    Array3d(const std::size_t d1,
	    const std::size_t d2,
	    const std::size_t d3);

    /*!
    ** \brief Constructs a %Array3d initialized by the scalar value \a val.
    ** \param d1 first dimension of the %Array3d
    ** \param d2 second dimension of the %Array3d
    ** \param d3 third dimension of the %Array3d
    ** \param val initialization value of the elements 
    */
    Array3d(const std::size_t d1,
	    const std::size_t d2,
	    const std::size_t d3,
	    const T& val);
    /*!
    ** \brief Constructs a %Array3d initialized by an array \a val.
    ** \param d1 first dimension of the %Array3d
    ** \param d2 second dimension of the %Array3d
    ** \param d3 third dimension of the %Array3d
    ** \param val initialization array value of the elements 
    */  
    Array3d(const std::size_t d1,
	    const std::size_t d2,
	    const std::size_t d3,
	    const T* val);

    /**
     **  \brief  Contructs a %Array3d from a range.
     ** \param d1 first dimension of the %Array3d
     ** \param d2 second dimension of the %Array3d
     ** \param d3 third dimension of the %Array3d
 
     **  \param  first  An input iterator.
     **  \param  last  An input iterator.
     **  \date 2014/03/13
     ** Create a %Array3d consisting of copies of the elements from
     ** [first,last).
     */
    template<typename InputIterator>
    Array3d(const size_type d1,
	    const size_type d2,
	    const size_type d3,
	    InputIterator first,
	    InputIterator last):
      d1_(d1),d2_(d2),d3_(d3),size_(d1 * d2 * d3), slice_size_(d2 * d3)
    {
      this->allocate();
      std::fill_n(this->begin(),this->size_,T());
      std::copy(first,last, this->begin());
    }
  
    /*!
    ** \brief Constructs a copy of the Array3d \a rhs
    */
    Array3d(const Array3d<T>& rhs);
  

    /*!
    ** \brief Destructor of the Array3d
    */
    ~Array3d();
 

    /*@} End Constructors */

 
    /*!
    ** \brief Resizes a %Array3d.
    ** \param d1 new first dimension
    ** \param d2 new second dimension
    ** \param d3 new third dimension
 
    ** \param val new value for all the elements
    */ 
    void resize(std::size_t d1,
		std::size_t d2,
		std::size_t d3,
		const T& val = T()); 
    /**
     ** \name One dimensional global iterators
     */
    /*@{*/

    //****************************************************************************
    //                          One dimensional iterators
    //****************************************************************************



    //----------------------Global iterators------------------------------

    /*!
    **  \brief Returns a read-only (constant) iterator that points to the
    **  first element in the %Array3d.  Iteration is done in ordinary
    **  element order.
    ** \return const begin iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> const A1(10,9,5);
    **  slip::Array3d<double> const A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.begin(),A1.end(),
    **                A2.begin(),S.begin(),
    **                std::plus<double>());
    ** \endcode
    */
    const_iterator begin() const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element in the %Array3d.  Iteration is done in ordinary
    **  element order.
    ** \return begin iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> A1(10,9,5);
    **  slip::Array3d<double> A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.begin(),A1.end(),
    **                A2.begin(),S.begin(),
    **                std::plus<double>());
    ** \endcode
    */
    iterator begin();

    /*!
    **  \brief Returns a read/write iterator that points one past the last
    **  element in the %Array3d.  Iteration is done in ordinary
    **  element order.
    ** \return end iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> A1(10,9,5);
    **  slip::Array3d<double> A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.begin(),A1.end(),
    **                A2.begin(),S.begin(),
    **                std::plus<double>());
    ** \endcode
    */
     iterator end();
 
    /*!
    **  \brief Returns a read-only (constant) iterator that points one past
    **  the last element in the %Array3d.  Iteration is done in
    **  ordinary element order.
    ** \return const end iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> const A1(10,9,5);
    **  slip::Array3d<double> const A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.begin(),A1.end(),
    **                A2.begin(),S.begin(),
    **                std::plus<double>());
    ** \endcode
    */
    const_iterator end() const;
  
    /*!
    **  \brief Returns a read/write reverse iterator that points to the
    **  last element in the %Array3d. Iteration is done in reverse
    **  element order.
    ** \return reverse begin iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> A1(10,9,5);
    **  slip::Array3d<double> A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.rbegin(),A1.rend(),
    **                A2.rbegin(),S.rbegin(),
    **                std::plus<double>());
    ** \endcode
    */
    reverse_iterator rbegin();
  
    /*!
    **  \brief Returns a read-only (constant) reverse iterator that points
    **  to the last element in the %Array3d.  Iteration is done in
    **  reverse element order.
    ** \return const reverse begin iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> const A1(10,9,5);
    **  slip::Array3d<double> const A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.rbegin(),A1.rend(),
    **                A2.rbegin(),S.rbegin(),
    **                std::plus<double>());
    ** \endcode
    */
    const_reverse_iterator rbegin() const;

    /*!
    **  \brief Returns a read/write reverse iterator that points to one
    **  before the first element in the %Array3d.  Iteration is done
    **  in reverse element order.
    **  \return reverse end iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> A1(10,9,5);
    **  slip::Array3d<double> A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.rbegin(),A1.rend(),
    **                A2.rbegin(),S.rbegin(),
    **                std::plus<double>());
    ** \endcode
    */
    reverse_iterator rend();

    /*!
    **  \brief Returns a read-only (constant) reverse iterator that points
    **  to one before the first element in the %Array3d.  Iteration
    **  is done in reverse element order.
    **  \return const reverse end iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> const A1(10,9,5);
    **  slip::Array3d<double> const A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    ** //copy the sum of A1 and A2 in S
    ** std::transform(A1.rbegin(),A1.rend(),
    **                A2.rbegin(),S.rbegin(),
    **                std::plus<double>());
    ** \endcode
    */
    const_reverse_iterator rend() const;

    /*@} End One dimensional global iterators */
    /**
     ** \name One dimensional slice iterators
     */
    /*@{*/
    //--------------------One dimensional slice iterators----------------------
 
  
    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the line (row,col) threw the slices in the %Array3d.  
    **	Iteration is done in ordinary element order (increasing slice number).
    **	\param row  row coordinate of the line
    **	\param col  col coordinate of the line
    ** 	\return slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Array3d<double> A1(10,9,5);
    **  slip::Array3d<double> A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 in the first slice of S
    **  std::transform(A1.begin_slice(0),A1.end_slice(0),
    **                A2.begin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    slice_iterator slice_begin(const size_type row, const size_type col);

    /*!
    **  \brief Returns a read-only (constant) iterator that points to the first
    **  element of the line (row,col) threw the slices in the %Array3d.  
    **	Iteration is done in ordinary element order (increasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return const_slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Array3d<double> const A1(10,9,5);
    **  slip::Array3d<double> const A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 in the first slice of S
    **  std::transform(A1.begin_slice(0),A1.end_slice(0),
    **                A2.begin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    const_slice_iterator slice_begin(const size_type row, const size_type col) const;
 
    /*!
    **  \brief Returns a read/write iterator that points to the one past the end
    **  element of the line (row,col) threw the slices in the %Array3d.  
    **	Iteration is done in ordinary element order (increasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Array3d<double> A1(10,9,5);
    **  slip::Array3d<double> A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 in the first slice of S
    **  std::transform(A1.begin_slice(0),A1.end_slice(0),
    **                A2.begin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    slice_iterator slice_end(const size_type row, const size_type col);
 
    /*!
    **  \brief Returns a read-only (constant) iterator that points to the one past the end
    **  element of the line (row,col) threw the slices in the %Array3d.  
    **	Iteration is done in ordinary element order (increasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return const_slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Array3d<double> const A1(10,9,5);
    **  slip::Array3d<double> const A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 in the first slice of S
    **  std::transform(A1.begin_slice(0),A1.end_slice(0),
    **                A2.begin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    const_slice_iterator slice_end(const size_type row, const size_type col) const;
 

    /*!
    **  \brief Returns a read/write iterator that points to the
    **  last element of the line (row,col) threw the slices in the %Array3d.
    ** 	Iteration is done in reverse element order (decreasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    ** 	\return reverse_slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Array3d<double> A1(10,9,5);
    **  slip::Array3d<double> A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 
    **  in the first slice of S in reverse order
    **  std::transform(A1.rbegin_slice(0),A1.rend_slice(0),
    **                A2.rbegin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    reverse_slice_iterator slice_rbegin(const size_type row, const size_type col);

    /*!
    **  \brief Returns a read-only (constant) iterator that points to the
    **  last element of the line (row,col) threw the slices in the %Array3d.
    **	Iteration is done in reverse element order (decreasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return const_reverse_slice_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Array3d<double> const A1(10,9,5);
    **  slip::Array3d<double> const A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 
    **  in the first slice of S in reverse order
    **  std::transform(A1.rbegin_slice(0),A1.rend_slice(0),
    **                A2.rbegin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    const_reverse_slice_iterator slice_rbegin(const size_type row, const size_type col) const;

    /*!
    **  \brief Returns a read/write iterator that points to the
    **  one before the first element of the line (row,col) threw the slices in the %Array3d.
    **  Iteration is done in reverse element order (decreasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **  \return reverse_slice_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Array3d<double> A1(10,9,5);
    **  slip::Array3d<double> A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 
    **  in the first slice of S in reverse order
    **  std::transform(A1.rbegin_slice(0),A1.rend_slice(0),
    **                A2.rbegin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    reverse_slice_iterator slice_rend(const size_type row, const size_type col);
 
  
    /*!
    **  \brief Returns a read (constant) iterator that points to the
    **  one before the first element of the line (row,col) threw the slices in the %Array3d.
    **  Iteration is done in reverse element order (decreasing slice number).
    **	\param row row coordinate of the line
    **	\param col col coordinate of the line
    **	\return const_reverse_slice_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **  \par Example:
    **  \code
    ** 
    **  slip::Array3d<double> const A1(10,9,5);
    **  slip::Array3d<double> const A2(10,9,5);
    **  slip::Array3d<double> S(10,9,5);
    **  //copy the sum of the first slice of A1 and A2 
    **  in the first slice of S in reverse order
    **  std::transform(A1.rbegin_slice(0),A1.rend_slice(0),
    **                A2.rbegin_slice(0),S.begin_slice(0),
    **                std::plus<double>());
    **  \endcode
    */
    const_reverse_slice_iterator slice_rend(const size_type row, const size_type col) const;

    /*@} End One dimensional slice iterators */

    /**
     ** \name One dimensional row iterators
     */
    /*@{*/
    //-------------------row iterators----------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of the slice \a slice in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row.
    **  \return begin row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    row_iterator row_begin(const size_type slice,
			   const size_type row);

    /*!
    **  \brief Returns a read_only iterator that points to the first
    **  element of the row \a row of the slice \a slice in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row. 
    **  \return begin const_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_row_iterator row_begin(const size_type slice,
				 const size_type row) const;

 
    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the row \a row of the slice \a slice in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row.
    **  \return end row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    row_iterator row_end(const size_type slice,
			 const size_type row);


    /*!
    **  \brief Returns a read_only iterator that points to the past-the-end
    **  element of the row \a row of the slice \a slice in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row. 
    **  \return end const_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_row_iterator row_end(const size_type slice,
			       const size_type row) const;


    /*!
    **  \brief Returns a read/write reverse iterator that points to the last
    **  element of the row \a row of the slice \a slice in the %Array3d.  
    **  Iteration is done in reverse element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row.
    **  \return begin reverse_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    reverse_row_iterator row_rbegin(const size_type slice,
				    const size_type row);

    /*!
    **  \brief Returns a read_only reverse iterator that points to the last
    **  element of the row \a row of the slice \a slice in the %Array3d.  
    **  Iteration is done in reverse element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row. 
    **  \return begin const_reverse_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_reverse_row_iterator row_rbegin(const size_type slice,
					  const size_type row) const;

 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the row \a row of the slice \a slice in the %Array3d.  
    **  Iteration is done in reverse element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row.
    **  \return end reverse_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    reverse_row_iterator row_rend(const size_type slice,
				  const size_type row);


    /*!
    **  \brief Returns a read_only reverse iterator that points to the first
    **  element of the row \a row of the slice \a slice in the %Array3d.  
    **  Iteration is done in reverse element order.
    **  \param slice The index of the slice.
    **  \param row The index of the row. 
    **  \return end const_reverse_row_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_reverse_row_iterator row_rend(const size_type slice,
					const size_type row) const;

    /*@} End One dimensional row iterators */
    /**
     ** \name One dimensional col iterators
     */
    /*@{*/
    //-------------------col iterators----------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the column \a column of the slice \a slice in the %Array3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return begin col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    col_iterator col_begin(const size_type slice,
			   const size_type col);


    /*!
    **  \brief Returns a read_only iterator that points to the first
    **  element of the column \a column of the slice \a slice in the %Array3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return begin const_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_col_iterator col_begin(const size_type slice,
				 const size_type col) const;

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the column \a column of the slice \a slice in the %Array3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return end col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    col_iterator col_end(const size_type slice,
			 const size_type col);


    /*!
    **  \brief Returns a read_only iterator that points to the past-the-end
    **  element of the column \a column of the slice \a slice in the %Array3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return end const_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_col_iterator col_end(const size_type slice,
			       const size_type col) const;


    /*!
    **  \brief Returns a read/write reverse iterator that points to the last
    **  element of the column \a column of the slice \a slice in the %Array3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return begin reverse_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    reverse_col_iterator col_rbegin(const size_type slice,
				    const size_type col);


    /*!
    **  \brief Returns a read_only reverse iterator that points to the last
    **  element of the column \a column of the slice \a slice in the %Array3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return begin const_reverse_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_reverse_col_iterator col_rbegin(const size_type slice,
					  const size_type col) const;

    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the column \a column of the slice \a slice in the %Array3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return end reverse_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    reverse_col_iterator col_rend(const size_type slice,
				  const size_type col);


    /*!
    **  \brief Returns a read_only reverse iterator that points to the first
    **  element of the column \a column of the slice \a slice in the %Array3d.  
    **  Iteration is done modulo the number of columns.
    **  \param slice The index of the slice.
    **  \param col The index of the column 
    **  \return end const_reverse_col_iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    */
    const_reverse_col_iterator col_rend(const size_type slice,
					const size_type col) const;

    /*@} End One dimensional col iterators */


   /**
     ** \name One dimensional slice range iterators
     */
    /*@{*/
   //------------------------slice range iterators -----------------------

    /*!
    ** \brief Returns a read/write iterator that points to the first
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Array3d.
    ** Iteration is done in ordinary element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return begin slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Array3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> A1(8,8,8);
    **  slip::Array3d<double> A2(4,8,8);
    **  slip::Range<int> range(0,A1.dim1()-1,2);
    ** //copy the the elements of the line (0,0) of A1 iterated according to the
    ** //range in the line (1,1) of A2
    ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),A2.slice_begin(1,1));
    ** \endcode
    */
    slice_range_iterator slice_begin(const size_type row,const size_type col,
				     const slip::Range<int>& range);


    /*!
    **  \brief Returns a read/write iterator that points one past the end
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Array3d.
    ** Iteration is done in ordinary element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return end slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Array3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> A1(8,8,8);
    **  slip::Array3d<double> A2(4,8,8);
    **  slip::Range<int> range(0,A1.dim1()-1,2);
    ** //copy the the elements of the line (0,0) of A1 iterated according to the
    ** //range in the line (1,1) of A2
    ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),A2.slice_begin(1,1));
    ** \endcode
    */
    slice_range_iterator slice_end(const size_type row,const size_type col,
				   const slip::Range<int>& range);


    /*!
    ** \brief Returns a read only (constant) iterator that points to the first
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Array3d.
    ** Iteration is done in ordinary element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return begin const_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Array3d.
    ** \par Example:
    ** \code
    **  
    **  slip::Array3d<double> const A1(M); //M is an already existing %Array3d
    **  slip::Range<int> range(0,A1.dim1()-1,2);
    ** //display the elements of the line (0,0) of A1 iterated according to the range
    ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),
    ** std::ostream_iterator<double>(std::cout," "));
    ** \endcode
    */
    const_slice_range_iterator slice_begin(const size_type row,const size_type col,
					   const slip::Range<int>& range) const;

    /*!
    ** \brief Returns a read_only iterator that points one past the last
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Array3d.
    ** Iteration is done in ordinary element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return end const_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Array3d.
    ** \par Example:
    ** \code
    **  
    **  slip::Array3d<double> const A1(M); //M is an already existing %Array3d
    **  slip::Range<int> range(0,A1.dim1()-1,2);
    ** //display the elements of the line (0,0) of A1 iterated according to the range
    ** std::copy(A1.slice_begin(0,0,range),A1.slice_end(0,0,range),
    ** std::ostream_iterator<double>(std::cout," "));
    ** \endcode
    */
    const_slice_range_iterator slice_end(const size_type row,const size_type col,
					 const slip::Range<int>& range) const;


    /*!
    ** \brief Returns a read/write iterator that points to the last
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Array3d.
    ** Iteration is done in the reverse element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return begin reverse_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Array3d.
    */
    reverse_slice_range_iterator slice_rbegin(const size_type row,const size_type col,
					      const slip::Range<int>& range);


    /*!
    **  \brief Returns a read/write iterator that points to the first
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Array3d.
    ** Iteration is done in reverse element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return end reverse_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Array3d.
    */
    reverse_slice_range_iterator slice_rend(const size_type row,const size_type col,
					   const slip::Range<int>& range);


    /*!
    ** \brief Returns a read only (constant) iterator that points to the last
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Array3d.
    ** Iteration is done in reverse element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return begin const_reverse_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Array3d.
    */
    const_reverse_slice_range_iterator slice_rbegin(const size_type row,const size_type col,
						   const slip::Range<int>& range) const;

    /*!
    ** \brief Returns a read_only iterator that points one past the lastto the first
    ** element of the %Range \a range of the of the line (row,col) 
    ** threw the slices in the %Array3d.
    ** Iteration is done in reverse element order according to the
    ** %Range.
    ** \param row row coordinate of the line
    ** \param col col coordinate of the line
    ** \param range %Range of the line to iterate.
    ** \return end const_reverse_slice_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre the range must be compatible with the slice dimensions
    ** \pre The range must be inside the whole range of the %Array3d.
    */
    const_reverse_slice_range_iterator slice_rend(const size_type row,const size_type col,
						 const slip::Range<int>& range) const;

 
    /*@} End One dimensional slice range iterators */

    /**
     ** \name One dimensional row range iterators
     */
    /*@{*/
   //------------------------row range iterators -----------------------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the %Range \a range of the row \a row in the %Array3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return begin row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre row must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> A1(8,8,8);
    **  slip::Array3d<double> A2(8,4,5);
    **  slip::Range<int> range(0,A1.dim3()-1,2);
    ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
    ** //range in the row 1 of the slice 1 of A2
    ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
    ** \endcode
    */
    row_range_iterator row_begin(const size_type slice,const size_type row,
				 const slip::Range<int>& range);

    /*!
    **  \brief Returns a read/write iterator that points one past the end
    **  element of the %Range \a range of the row \a row in the %Array3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return end row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre row must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> A1(8,8,8);
    **  slip::Array3d<double> A2(8,4,5);
    **  slip::Range<int> range(0,A1.dim3()-1,2);
    ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
    ** //range in the row 1 of the slice 1 of A2
    ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
    ** \endcode
    */
    row_range_iterator row_end(const size_type slice,const size_type row,
			       const slip::Range<int>& range);


    /*!
    **  \brief Returns a read-only iterator that points to the first
    **  element of the %Range \a range of the row \a row in the %Array3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return begin const_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** 
    ** \pre row must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> const A1(8,8,8);
    **  slip::Array3d<double> A2(8,4,5);
    **  slip::Range<int> range(0,A1.dim3()-1,2);
    ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
    ** //range in the row 1 of the slice 1 of A2
    ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
    ** \endcode
    */
    const_row_range_iterator row_begin(const size_type slice,const size_type row,
				       const slip::Range<int>& range) const;


    /*!
    **  \brief Returns a read_only iterator that points one past the last
    **  element of the %Range range of the row \a row in the %Array3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row Row to iterate.
    ** \param range %Range of the row to iterate
    ** \return begin const_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** \pre row must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> const A1(8,8,8);
    **  slip::Array3d<double> A2(8,4,5);
    **  slip::Range<int> range(0,A1.dim3()-1,2);
    ** //copy the the elements of the row 0 of the slice 0 of A1 iterated according to the
    ** //range in the row 1 of the slice 1 of A2
    ** std::copy(A1.row_begin(0,0,range),A1.row_end(0,0,range),A2.row_begin(1,1));
    ** \endcode
    */
    const_row_range_iterator row_end(const size_type slice,const size_type row,
				     const slip::Range<int>& range) const;
 
    /*!
    ** \brief Returns a read-write iterator that points to the last
    ** element of the %Range \a range of the row of a slice \a row and slice in the %Array3d.  
    ** Iteration is done in the reverse element order according to the %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return reverse_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre row must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    */
    reverse_row_range_iterator row_rbegin(const size_type slice,const size_type row,
					  const slip::Range<int>& range);
  
  
    /*!
    **  \brief Returns a read-write iterator that points one before
    **  the first element of the %Range \a range of the row of a slice \a row in the 
    **  %Array3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate.
    ** \return reverse_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    ** 
    ** \pre row must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    */
    reverse_row_range_iterator row_rend(const size_type slice,const size_type row,
					const slip::Range<int>& range);



    /*!
    **  \brief Returns a read-only iterator that points to the last
    **  element of the %Range \a range of the row  of a slice \a row in the %Array3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate
    ** \return begin const_reverse_row_range_iterator value
    **
    ** \pre row must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    */
    const_reverse_row_range_iterator row_rbegin(const size_type slice,const size_type row,
						const slip::Range<int>& range) const;


    /*!
    **  \brief Returns a read-only iterator that points one before the first
    **  element of the %Range \a range of the row of a slice \a row in the %Array3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param row The index of the row to iterate.
    ** \param range %Range of the row to iterate
    ** \return const_reverse_row_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre row must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    */
    const_reverse_row_range_iterator row_rend(const size_type slice,const size_type row,
					      const slip::Range<int>& range) const;
 
    /*@} End One dimensional row range iterators */
    /**
     ** \name One dimensional col range iterators
     */
    /*@{*/
    //------------------------col range iterators -----------------------
  
    /*!
    **  \brief Returns a read-write iterator that points to the first
    **  element of the %Range \a range of the col \a col in the %Array3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate
    ** \return begin col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> A1(8,8,8);
    **  slip::Array3d<double> A2(4,8,5);
    **  slip::Range<int> range(0,A1.dim2()-1,2);
    ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
    ** //range in the column 1 of the slice 1 of A2
    ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
    ** \endcode
    */
    col_range_iterator col_begin(const size_type slice,const size_type col,
				 const slip::Range<int>& range);

    /*!
    **  \brief Returns a read-write iterator that points to the past
    **  the end element of the %Range \a range of the col \a col in the 
    **  %Array3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return begin col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> A1(8,8,8);
    **  slip::Array3d<double> A2(4,8,5);
    **  slip::Range<int> range(0,A1.dim2()-1,2);
    ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
    ** //range in the column 1 of the slice 1 of A2
    ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
    ** \endcode
    */
    col_range_iterator col_end(const size_type slice,const size_type col,
			       const slip::Range<int>& range);


    /*!
    **  \brief Returns a read-only iterator that points to the first
    **  element of the %Range \a range of the col \a col in the %Array3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return begin const_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> const A1(8,8,8);
    **  slip::Array3d<double> A2(4,8,5);
    **  slip::Range<int> range(0,A1.dim2()-1,2);
    ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
    ** //range in the column 1 of the slice 1 of A2
    ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
    ** \endcode
    */
    const_col_range_iterator col_begin(const size_type slice,const size_type col,
				       const slip::Range<int>& range) const;
    
    /*!
    **  \brief Returns a read-only iterator that points to the past
    **  the end element of the %Range \a range of the col \a col in 
    **  the %Array3d.  
    **  Iteration is done in ordinary element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate
    ** \return begin const_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    ** \par Example:
    ** \code
    ** 
    **  slip::Array3d<double> const A1(8,8,8);
    **  slip::Array3d<double> A2(4,8,5);
    **  slip::Range<int> range(0,A1.dim2()-1,2);
    ** //copy the the elements of the column 0 of the slice 0 of A1 iterated according to the
    ** //range in the column 1 of the slice 1 of A2
    ** std::copy(A1.col_begin(0,0,range),A1.col_end(0,0,range),A2.col_begin(1,1));
    ** \endcode
    */
    const_col_range_iterator col_end(const size_type slice,const size_type col,
				     const slip::Range<int>& range) const;

    /*!
    **  \brief Returns a read-write iterator that points to the last
    **  element of the %Range \a range of the col of a slice \a col in the %Array3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return begin reverse_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    */
    reverse_col_range_iterator col_rbegin(const size_type slice,const size_type col,
					  const slip::Range<int>& range);

    /*!
    **  \brief Returns a read-write iterator that points to one before 
    **  the first element of the %Range range of the col of a slice \a col in the 
    **  %Array3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return reverse_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    */
    reverse_col_range_iterator col_rend(const size_type slice,const size_type col,
					const slip::Range<int>& range);

    /*!
    **  \brief Returns a read_only iterator that points to the last
    **  element of the %Range \& range of the col of a slice \a col in the %Array3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return begin const_reverse_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    */
    const_reverse_col_range_iterator col_rbegin(const size_type slice,const size_type col,
						const slip::Range<int>& range) const;
 
    /*!
    **  \brief Returns a read-only iterator that points to the first
    **  element of the %Range \a range of the col of a slice \a col in the %Array3d.  
    **  Iteration is done in the reverse element order according to the
    **  %Range.
    ** \param slice The index of the slice.
    ** \param col The index of the column to iterate.
    ** \param range %Range of the column to iterate.
    ** \return const_reverse_col_range_iterator value
    ** \remarks This iterator is compatible with RandomAccessIterator 
    **          algorithms.
    **
    ** \pre col must be compatible with the range of the %Array3d.
    ** \pre The range must be inside the whole range of the %Array3d.
    */
    const_reverse_col_range_iterator col_rend(const size_type slice,const size_type col,
					      const slip::Range<int>& range) const;

    /*@} End One dimensional col range iterators */

    //****************************************************************************
    //                          One dimensional plane iterators
    //****************************************************************************
   
    /**
     ** \name One dimensional global plane iterators
     */
    /*@{*/

    //----------------------Global plane iterators------------------------------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element in the in the slice plane of the %Array3d. 
    **  Iteration is done in ordinary element order.
    **  \param slice the slice coordinate of the plane
    **  \return begin iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Array3d<double> A1(2,3,2);
    **   slip::Array3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2
    **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
    **  \endcode
    */
    iterator plane_begin(const size_type slice);

    /*!
    **  \brief Returns a read-only (constant) iterator that points to the
    **  first element in the slice plane of the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param slice the slice coordinate of the plane
    **  \return const begin iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Array3d<double> const A1(2,3,2);
    **   slip::Array3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2
    **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
    **  \endcode
    */
    const_iterator plane_begin(const size_type slice) const;

    /*!
    **  \brief Returns a read/write iterator that points one past the last
    **  element in the slice plane of the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param slice the slice coordinate of the plane
    **  \return end iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Array3d<double> A1(2,3,2);
    **   slip::Array3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2
    **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
    **  \endcode
    */
    iterator plane_end(const size_type slice);
 
    /*!
    **  \brief Returns a read-only (constant) iterator that points one past
    **  the last element in the slice plane of the %Array3d.
    **  Iteration is done in ordinary element order.
    **  \param slice the slice coordinate of the plane
    **  \return const end iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Array3d<double> const A1(2,3,2);
    **   slip::Array3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2
    **   std::copy(A1.plane_begin(0),A1.plane_end(0),A2.plane_begin(0));
    **  \endcode
    */
    const_iterator plane_end(const size_type slice) const;
  
    /*!
    **  \brief Returns a read/write reverse iterator that points to the
    **  last element in the slice plane of the %Array3d.
    **  Iteration is done in reverse element order.
    **  \param slice the slice coordinate of the plane
    **  \return reverse begin iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Array3d<double> A1(2,3,2);
    **   slip::Array3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
    **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
    **  \endcode
    */
    reverse_iterator plane_rbegin(const size_type slice);
  
    /*!
    **  \brief Returns a read-only (constant) reverse iterator that points
    **  to the last element in the slice plane k of the %Array3d.
    **  Iteration is done in reverse element order.
    ** \param slice the slice coordinate of the plane
    ** \return const reverse begin iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Array3d<double> const A1(2,3,2);
    **   slip::Array3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
    **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
    **  \endcode
    */
    const_reverse_iterator plane_rbegin(const size_type slice) const;

    /*!
    **  \brief Returns a read/write reverse iterator that points to one
    **  before the first element in the slice plane of the %Array3d.
    **  Iteration is done in reverse element order.
    **  \param slice the slice coordinate of the plane
    **  \return reverse end iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Array3d<double> A1(2,3,2);
    **   slip::Array3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
    **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
    **  \endcode
    */
    reverse_iterator plane_rend(const size_type slice);

    /*!
    **  \brief Returns a read-only (constant) reverse iterator that points
    **  to one before the first element in the slice plane of the %Array3d.
    **  Iteration is done in reverse element order.
    **  \param slice the slice coordinate of the plane
    **  \return const reverse end iterator value
    **  \remarks This iterator is compatible with RandomAccessIterator 
    **           algorithms.
    **  \par Example:
    **  \code
    ** 
    **   slip::Array3d<double> const A1(2,3,2);
    **   slip::Array3d<double> A2(2,3,2);
    **   //copy the first plane (in slice direction) of A1 in A2 in reverse order
    **   std::copy(A1.plane_rbegin(0),A1.plane_rend(0),A2.plane_begin(0));
    **  \endcode
    */
    const_reverse_iterator plane_rend(const size_type slice) const;

    /*@} End One dimensional global plane iterators */


#ifdef ALL_PLANE_ITERATOR3D
    
    /**
     ** \name One dimensional row and col plane iterators
     */
    /*@{*/
    //-------------------row and col plane iterators----------
    
      
    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
				     const size_type row) const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type row);
    
    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the row \a row of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return end iterator1d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type row);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the row \a row of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type row) const;


    /*!
    **  \brief Returns a read/write reverse_iterator that points to the last
    **  element of the row \a row of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin reverse_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type row);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **  element of the row \a row of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					      const size_type row) const;
 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the row \a row of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type row);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the first
    **  element of the row \a row of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type row) const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the col \a col of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type col);

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the col \a col of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
				     const size_type col) const;
    
    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the col \a col of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type col);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the col \a col of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type col) const;


    /*!
    **  \brief Returns a read/write reverse_iterator that points to the last
    **  element of the col \a col of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin reverse_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type col);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **  element of the col \a col of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin reverse_const_iterator1d value
    */
    const_reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					      const size_type col) const;
 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the col \a col of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate, const size_type col);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the first
    **  element of the col \a col of a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,const size_type col) const;

    /*@} End One dimensional plane row and col iterators */
    
    /**
     ** \name One dimensional plane box iterators
     */
    /*@{*/
    //-------------------plane box iterators----------

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of a box within a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
				     const size_type row, const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the row \a row of a box within a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return begin iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_row_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type row, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the row \a row of a box within a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate, 
			     const size_type row, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **   element of the row \a row of a box within a plane \a plane in the %Array3d.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_row_end(PLANE_ORIENTATION P, const size_type plane_coordinate,
				   const size_type row, const Box2d<int> & b) const;


    /*!
    **  \brief Returns a read/write reverse_iterator that points to the last
    **  element of the row \a row of a box within a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \param b The box within the plane
    **  \return begin reverse_iterator1d value
    */
    reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					const size_type row, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **  element of the row \a row of a box within a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_row_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					      const size_type row, const Box2d<int> & b) const;
 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of the row \a row of a box within a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return end iterator1d value
    */
    reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,
				      const size_type row, const Box2d<int> & b);

    /*!
    ** \brief Returns a read/write const reverse iterator that points to the first
    **  element of the row \a row of a box within a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param row The index of the row.
    **  \param b The box within the plane
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_row_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,
					    const size_type row, const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the col \a col of a box within a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin iterator1d value
    */
    iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
			       const size_type col, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of the col \a col of a box within a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_col_begin(PLANE_ORIENTATION P, const size_type plane_coordinate,
				     const size_type col, const Box2d<int> & b) const;
    
    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **  element of the col \a col of a box within a plane \a plane in the %Array3d.  
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate, 
			     const size_type col, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the past-the-end
    **   element of the col \a col of a box within a plane \a plane in the %Array3d.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_iterator1d plane_col_end(PLANE_ORIENTATION P, const size_type plane_coordinate,
				   const size_type col, const Box2d<int> & b) const;


    /*!
    **  \brief Returns a read/write reverse_iterator that points to the last
    **   element of the col \a col of a box within a plane \a plane in the %Array3d.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin reverse_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					const size_type col, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **   element of the col \a col of a box within a plane \a plane in the %Array3d.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_col_rbegin(PLANE_ORIENTATION P, const size_type plane_coordinate,
					      const size_type col, const Box2d<int> & b) const;
 
    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **   element of the col \a col of a box within a plane \a plane in the %Array3d.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return end iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate, 
				      const size_type col, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the first
    **   element of the col \a col of a box within a plane \a plane in the %Array3d.  
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param col The index of the col.
    **  \param b The box within the plane
    **  \return begin reverse_const_iterator1d value
    **  \remarks This iterator is compatible with RandomAccessIterator
    **           algorithms.
    */
    const_reverse_iterator1d plane_col_rend(PLANE_ORIENTATION P, const size_type plane_coordinate,
					    const size_type col, const Box2d<int> & b) const;


    /*@} End One dimensional plane box iterators */

#endif //ALL_PLANE_ITERATOR3D

    //****************************************************************************
    //                          Two dimensional plane iterators
    //****************************************************************************
    
    /**
     ** \name two dimensionnal plane iterators : Global iterators
     */
    /*@{*/

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **   element of the plane in the %Array3d. It points to the upper left 
    **   element of the plane
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate);

    /*!
    **  \brief Returns a read/write iterator that points to the last
    **   element of the plane in the %Array3d. It points to past the end element of
    **   the bottom right element of the plane
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate);

    /*!
    **  \brief Returns a read/write const iterator that points to the first
    **   element of the plane in the %Array3d. It points to the upper left 
    **   element of the plane
    **   Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate) const;

    /*!
    **  \brief Returns a read/write const iterator that points to the last
    **  element of the plane in the %Array3d. It points to past the end element of
    **  the bottom right element of the plane
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2),M.plane_bottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate) const;

    /*!
    **  \brief Returns a read/write reverse_iterator that points to the bottom right
    **  element of the plane in the %Array3d.
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example:
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate);

    /*!
    **  \brief Returns a read/write reverse_iterator that points to the upper left
    **  element of the plane in the %Array3d.
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example :
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the bottom
    **  right element of the plane in the %Array3d.
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example :
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
    **             slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate) const;

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the upper
    **  left element of the plane in the %Array3d.
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   //Print the 2nd plane of M in the KI_PLANE direction in reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2),M.plane_rbottom_right(
                   slip::KI_PLANE,2),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate) const;


    /*@} End two dimensional plane iterators : Global iterators */
    
    /**
     ** \name two dimensionnal plane iterators : box iterators
     */
    /*@{*/

    /*!
    **  \brief Returns a read/write iterator that points to the first
    **  element of a box within a plane in the %Array3d. It points to the upper left 
    **  element of the box
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
				, const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write iterator that points to the last
    **  element of a box within a plane in the %Array3d. It points to past the end element of
    **  the bottom right element of the plane
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
				  , const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const iterator that points to the first
    **  element of a box within a plane in the %Array3d. It points to the upper left 
    **  element of the plane
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator2d plane_upper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
				      , const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write const iterator that points to the last
    **  element of a box within a plane in the %Array3d. It points to past the end element of
    **  the bottom right element of the plane
    **  Iteration is done in ordinary element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin const_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box
    **   std::copy(M.plane_upper_left(slip::KI_PLANE,2,b),M.plane_bottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator2d plane_bottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
					, const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write reverse iterator that points to the last
    **  element of a box within a plane in the %Array3d. It points to the  
    **  bottom right element of the box
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin reverse_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box in a reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
					 , const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write reverse iterator that points to the first
    **  element of a box within a plane in the %Array3d. It points to the upper
    **  left element of the plane
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin reverse_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box in a reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
					   , const Box2d<int> & b);

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the last
    **  element of a box within a plane in the %Array3d. It points to the bottom right 
    **  element of the plane
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin const_reverse_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box in a reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator2d plane_rupper_left(PLANE_ORIENTATION P, const size_type plane_coordinate
					       , const Box2d<int> & b) const;

    /*!
    **  \brief Returns a read/write const reverse iterator that points to the first
    **  element of a box within a plane in the %Array3d. It points to
    **  the bottom right element of the plane
    **  Iteration is done in backward element order.
    **  \param P number of the plane axe (PLANE_ORIENTATION).
    **  \param plane_coordinate The constant coordinate
    **  \param b The box within the plane
    **  \return begin const_reverse_iterator2d value
    **  \pre P must be 0,1 or 2
    **  \pre b must be within the plane
    **  \remarks This iterator is compatible with BidirectionnalAccessIterator
    **           algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   slip::Box2d<int> b(1,1,3,3);
    **   //Print the 2nd plane of M in the KI_PLANE direction within a box in a reverse order
    **   std::copy(M.plane_rupper_left(slip::KI_PLANE,2,b),M.plane_rbottom_right(
    **             slip::KI_PLANE,2,b),std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator2d plane_rbottom_right(PLANE_ORIENTATION P, const size_type plane_coordinate
						 , const Box2d<int> & b) const;
    

    /*@} End two dimensionnal plane iterators : box iterators */
    
    //****************************************************************************
    //                          Three dimensional iterators
    //****************************************************************************
   
    /**
     ** \name three dimensionnal iterators : Global iterators
     */
    /*@{*/

    //------------------------ Global iterators------------------------------------

    /*!
    **  \brief Returns a read/write iterator3d that points to the first
    **  element of the %Array3d. It points to the front upper left element of
    **  the %Array3d.
    **  
    **  \return begin iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   //Print M
    **   std::copy(M.front_upper_left(),M.back_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d front_upper_left();
  
  
    /*!
    **  \brief Returns a read/write iterator3d that points to the past
    **  the end element of the %Array3d. It points to past the end element of
    **  the back bottom right element of the %Array3d.
    **  
    **  \return begin iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   //Print M
    **   std::copy(M.front_upper_left(),M.back_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d back_bottom_right();


    /*!
    **  \brief Returns a read-only iterator3d that points to the first
    **  element of the %Array3d. It points to the front upper left element of
    **  the %Array3d.
    **  
    **  \return begin const iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   //Print M
    **   std::copy(M.front_upper_left(),M.back_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d front_upper_left() const;
  
 
    /*!
    **  \brief Returns a read-only iterator3d that points to the past
    **  the end element of the %Array3d. It points to past the end element of
    **  the back bottom right element of the %Array3d.
    **  
    **  \return begin const iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   //Print M
    **   std::copy(M.front_upper_left(),M.back_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d back_bottom_right() const;

    /*!
    **  \brief Returns a read/write reverse iterator3d. It points to the 
    **   back bottom right element of the %Array3d. 
    *    Iteration is done within the %Array3d in the reverse order.
    **  
    **  \return reverse_iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   //Print M in a reverse order
    **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator3d rfront_upper_left();
  
    /*!
    **  \brief Returns a read/write reverse iterator3d. It points to past the 
    **  front upper left element of the %Array3d.
    **  Iteration is done in the reverse order.
    **  
    **  \return reverse iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   //Print M in a reverse order
    **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    **/
    reverse_iterator3d rback_bottom_right();

    /*!
    **  \brief Returns a read only reverse iterator3d that points.  It points 
    **  to the back bottom right element of the %Array3d. 
    **  Iteration is done within the %Array3d in the reverse order.
    **  
    ** \return const_reverse_iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   //Print M in a reverse order
    **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator3d rfront_upper_left() const;
 
 
    /*!
    **  \brief Returns a read only reverse iterator3d. It points to past the 
    **  front upper left element of the %Array3d.
    **  Iteration is done in the reverse order.
    **  
    ** \return const reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   //Print M in a reverse order
    **   std::copy(M.rfront_upper_left(),M.rback_bottom_right(),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator3d rback_bottom_right() const;

    /*@} End three dimensionnal iterators : Global iterators */
    
    /**
     ** \name three dimensionnal iterators : Box iterators
     */
    /*@{*/
    
    //------------------------ Box iterators------------------------------------

    /*!
    **  \brief Returns a read/write iterator3d that points to the first
    **  element of the %Array3d. It points to the front upper left element of
    **  the \a %Box3d associated to the %Array3d.
    **
    **  \param box A %Box3d defining the range of indices to iterate
    **         within the %Array3d.
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \return end iterator3d value
    **  \pre The box indices must be inside the range of the %Array3d ones.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   slip::Box3d<int> b(1,1,1,2,2,3);
    **   //Print M within a box
    **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d front_upper_left(const Box3d<int>& box);

 
    /*!
    **  \brief Returns a read/write iterator3d that points to the past
    **  the end element of the %Array3d. It points to past the end element of
    **  the back bottom right element of the \a %Box3d associated to the %Array3d.
    **
    **  \param box a %Box3d defining the range of indices to iterate
    **         within the %Array3d.
    **
    **  \return end iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \pre The box indices must be inside the range of the %Array3d ones.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   slip::Box3d<int> b(1,1,1,2,2,3);
    **   //Print M within a box
    **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d back_bottom_right(const Box3d<int>& box);


    /*!
    **  \brief Returns a read only iterator3d that points to the first
    **  element of the %Array3d. It points to the front upper left element of
    **  the \a %Box3d associated to the %Array3d.
    **
    **  \param box a %Box3d defining the range of indices to iterate
    **         within the %Array3d.
    **  \return end const iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **
    **  \pre The box indices must be inside the range of the %Array3d ones.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   slip::Box3d<int> b(1,1,1,2,2,4);
    **   //Print M within a box
    **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d front_upper_left(const Box3d<int>& box) const;

 
    /*!
    **  \brief Returns a read only iterator3d that points to the past
    **  the end element of the %Array3d. It points to past the end element of
    **  the back bottom right element of the \a %Box3d associated to the %Array3d.
    **
    **  \param box a %Box3d defining the range of indices to iterate
    **         within the %Array3d.
    **  \return end const iterator3d value
    **  \remarks This iterator is compatible with BidirectionalIterator 
    **           algorithms.
    **
    **  \pre The box indices must be inside the range of the %Array3d ones.
    ** 
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   slip::Box3d<int> b(1,1,1,2,2,4);
    **   //Print M within a box
    **   std::copy(M.front_upper_left(b),M.back_bottom_right(b),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d back_bottom_right(const Box3d<int>& box) const;

  

    /*!
    **  \brief Returns a read/write reverse iterator3d. It points to the 
    **  back bottom right element of the \a %Box3d associated to the %Array3d.
    **  Iteration is done in the reverse order.
    **
    ** \param box a %Box3d defining the range of indices to iterate
    **        within the %Array3d.
    **
    ** \pre The box indices must be inside the range of the %Array3d ones.
    **  
    ** \return reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    */
    reverse_iterator3d rfront_upper_left(const Box3d<int>& box);

    /*!
    **  \brief Returns a read/write reverse iterator3d. It points to one
    **  before the front upper left element of the %Box3d \a box associated to 
    **  the %Array3d.
    **
    ** \param box A %Box3d defining the range of indices to iterate
    **        within the %Array3d.
    **
    ** \pre The box indices must be inside the range of the %Array3d ones.
    **  
    ** \return reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    */
    reverse_iterator3d rback_bottom_right(const Box3d<int>& box);

    /*!
    **  \brief Returns a read only reverse iterator3d. It points to the 
    **  back bottom right element of the %Box3d \a box associated to the %Array3d.
    **  Iteration is done in the reverse order.
    **
    ** \param box A %Box3d defining the range of indices to iterate
    **        within the %Array3d.
    **
    ** \pre The box indices must be inside the range of the %Array3d ones.
    **  
    ** \return const reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    */
    const_reverse_iterator3d rfront_upper_left(const Box3d<int>& box) const;


    /*!
    **  \brief Returns a read-only reverse iterator3d. It points to one 
    **  before the front element of the bottom right element of the %Box3d 
    **  \a box associated to the %Array3d.
    **
    ** \param box A %Box3d defining the range of indices to iterate
    **        within the %Array3d.
    **
    ** \pre The box indices must be inside the range of the %Array3d ones.
    **  
    ** \return const reverse iterator3d value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    */
    const_reverse_iterator3d rback_bottom_right(const Box3d<int>& box) const;

    
    /*@} End three dimensionnal iterators : Box iterators */
    
    /**
     ** \name three dimensionnal iterators : Range iterators
     */
    
    /*@{*/
    
    //------------------------ Range iterators------------------------------------
 
    /*!
    **  \brief Returns a read/write iterator3d_range that points to the 
    **  front upper left element of the ranges \a slice_range, \a row_range and \a col_range 
    **  associated to the %Array3d.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range  row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Array3d ones.
    **  
    ** \return iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range
    **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d_range front_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
				const Range<int>& col_range);

    /*!
    **  \brief Returns a read/write iterator3d_range that points to the 
    **   past the end back bottom right element of the ranges \a slice_range, \a row_range 
    **   and \a col_range associated to the %Array3d.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Array3d ones.
    **  
    ** \return iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range
    **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    iterator3d_range back_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
				  const Range<int>& col_range);


    /*!
    **  \brief Returns a read-only iterator3d_range that points to the
    **   to the front upper left element of the ranges \a slice_range, \a row_range and \a col_range 
    **   associated to the %Array3d.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid.
    ** \pre The ranges indices must be inside the ranges of the %Array3d ones.
    **  
    ** \return const_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range
    **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d_range front_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
				      const Range<int>& col_range) const;


    /*!
    **  \brief Returns a read-only iterator3d_range that points to the past
    **  the end back bottom right element of the ranges \a slice_range, \a row_range and \a col_range 
    **  associated to the %Array3d.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Array3d ones.
    **  
    ** \return const_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range
    **   std::copy(M.front_upper_left(sr,rr,cr),M.back_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_iterator3d_range back_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
					const Range<int>& col_range) const;

    /*!
    **  \brief Returns a read/write reverse_iterator3d_range that points to the 
    **  past the back bottom right element of the ranges \a row_range and 
    **  \a col_range associated to the %Array3d. Iteration is done in the 
    **  reverse order.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Array3d ones.
    **  
    ** \return reverse_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range in reverse order
    **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator3d_range rfront_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
					 const Range<int>& col_range);
    
    /*!
    **  \brief Returns a read/write reverse_iterator3d_range that points
    **  to one before the  front upper left element of the ranges \a row_range 
    **  and \a col_range associated to the %Array3d. Iteration is done
    **  in the reverse order.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Array3d ones.
    **  
    ** \return reverse_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range in reverse order
    **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    reverse_iterator3d_range rback_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
					   const Range<int>& col_range);
    
    /*!
    **  \brief Returns a read-only reverse_iterator3d_range that points 
    **   to the past the back bottom right element of the ranges \a row_range and 
    **   \a col_range associated to the %Array3d. Iteration is done in the 
    **   reverse order.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid.
    ** \pre The ranges indices must be inside the ranges of the %Array3d ones.
    **  
    ** \return const_reverse_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range in reverse order
    **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator3d_range rfront_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
					       const Range<int>& col_range) const;
    
    /*!
    **  \brief Returns a read-only reverse_iterator3d_range that points 
    **  to one before the front upper left element of the ranges \a row_range 
    **  and \a col_range associated to the %Array3d.Iteration is done in 
    **  the reverse order.
    **
    ** \param slice_range The range of the slices.
    ** \param row_range The range of the rows.
    ** \param col_range The range of the columns.
    **       
    ** \pre slice_range, row_range and col_range  must be valid. 
    ** \pre The ranges indices must be inside the ranges of the %Array3d ones.
    **  
    ** \return const_reverse_iterator3d_range value
    ** \remarks This iterator is compatible with BidirectionalIterator 
    **          algorithms.
    **  \par Example
    **  \code  
    **   slip::Array3d<double> const M(3,4,5);
    **   slip::Range<int> sr(0,2,2);
    **   slip::Range<int> rr(0,3,2);
    **   slip::Range<int> cr(0,4,2);
    **   //Print M within a range in reverse order
    **   std::copy(M.rfront_upper_left(sr,rr,cr),M.rback_bottom_right(sr,rr,cr),
    **             std::ostream_iterator<double>(std::cout," "));
    **  \endcode
    */
    const_reverse_iterator3d_range rback_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
						 const Range<int>& col_range) const;
    
    
    /*@} End three dimensionnal iterators : Range iterators */
    
    //********************************************************************
    
    /**
     ** \name i/o operators
     */
    /*@{*/
    /*!
    ** \brief Write the %Array3d to the ouput stream
    ** \param out output std::ostream
    ** \param a %Array3d to write to the output stream
    */
    friend std::ostream& operator<< <>(std::ostream & out, 
				       const self& a);

    /*@} End i/o operators */

    /**
     ** \name  Assignment operators and methods
     */
    /*@{*/
    /*!
    ** \brief Assign a %Array3d.
    **
    ** Assign elements of %Array3d in \a rhs
    **
    ** \param rhs %Array3d to get the values from.
    ** \return 
    */
    self& operator=(const Array3d<T> & rhs);



    /*!
    ** \brief Assign all the elments of the %Array3d by value
    **
    **
    ** \param value A reference-to-const of arbitrary type.
    ** \return 
    */
    self& operator=(const T& value);


    /*!
    ** \brief Fills the container range [begin(),begin()+size()) 
    **        with copies of value
    ** \param value  A reference-to-const of arbitrary type.
    */
    void fill(const T& value)
    {
      std::fill_n(this->begin(),this->size_,value);
    } 
  
    /*!
    ** \brief Fills the container range [begin(),begin()+size())
    **        with a copy of
    **        the value array
    ** \param value  A pointer of arbitrary type.
    */
    void fill(const T* value)
    {
      std::copy(value,value + this->size_, this->begin());
    } 

    /*!
    ** \brief Fills the container range [begin(),begin()+size()) 
    **        with a copy of the range [first,last)
    **  \param  first  An input iterator.
    **  \param  last   An input iterator.
    **   
    **
    */
    template<typename InputIterator>
    void fill(InputIterator first,
	      InputIterator last)
    {
      std::copy(first,last, this->begin());
    }
    /*@} End Assignment operators and methods*/



    /**
     ** \name Comparison operators
     */
    /*@{*/
    /*!
    ** \brief %Array3d equality comparison
    ** \param x A %Array3d
    ** \param y A %Array3d of the same type of \a x
    ** \return true iff the size and the elements of the Arrays are equal
    */
    friend bool operator== <>(const Array3d<T>& x, 
			      const Array3d<T>& y);

    /*!
    ** \brief %Array3d inequality comparison
    ** \param x A %Array3d
    ** \param y A %Array3d of the same type of \a x
    ** \return true if !(x == y) 
    */
    friend bool operator!= <>(const Array3d<T>& x, 
			      const Array3d<T>& y);

    /*!
    ** \brief Less than comparison operator (%Array3d ordering relation)
    ** \param x A %Array3d
    ** \param y A %Array3d of the same type of \a x
    ** \return true iff \a x is lexicographically less than \a y
    */
    friend bool operator< <>(const Array3d<T>& x, 
			     const Array3d<T>& y);

    /*!
    ** \brief More than comparison operator
    ** \param x A %Array3d
    ** \param y A %Array3d of the same type of \a x
    ** \return true iff y > x 
    */
    friend bool operator> <>(const Array3d<T>& x, 
			     const Array3d<T>& y);

    /*!
    ** \brief Less than equal comparison operator
    ** \param x A %Array3d
    ** \param y A %Array3d of the same type of \a x
    ** \return true iff !(y > x) 
    */
    friend bool operator<= <>(const Array3d<T>& x, 
			      const Array3d<T>& y);

    /*!
    ** \brief More than equal comparison operator
    ** \param x A %Array3d
    ** \param y A %Array3d of the same type of \a x
    ** \return true iff !(x < y) 
    */
    friend bool operator>= <>(const Array3d<T>& x, 
			      const Array3d<T>& y);


    /*@} Comparison operators */
  
    /**
     ** \name  Element access operators
     */
    /*@{*/

    T** operator[](const size_type k);

    const T* const* operator[](const size_type k) const;
  

    /*!
    ** \brief Subscript access to the data contained in the %Array3d.
    ** \param k The index of the slice for which the data should be accessed. 
    ** \param i The index of the row for which the data should be accessed. 
    ** \param j The index of the column for which the data should be accessed.
    ** \return Read/Write reference to data.
    ** \pre k < slices()
    ** \pre i < rows()
    ** \pre j < cols()
    ** This operator allows for easy, 3d array-style, data access.
    ** Note that data access with this operator is unchecked and
    ** out_of_range lookups are not defined. 
    */
    reference operator()(const size_type k,
			 const size_type i,
			 const size_type j);

    /*!
    ** \brief Subscript access to the data contained in the %Array3d.
    ** \param k The index of the slice for which the data should be accessed. 
    ** \param i The index of the row for which the data should be accessed. 
    ** \param j The index of the column for which the data should be accessed.
    ** \return Read_only (constant) reference to data.
    ** \pre k < slices()
    ** \pre i < rows()
    ** \pre j < cols()
    ** This operator allows for easy, 3d array-style, data access.
    ** Note that data access with this operator is unchecked and
    ** out_of_range lookups are not defined. 
    */
    const_reference operator()(const size_type k,
			       const size_type i,
			       const size_type j) const;
	
    /*@} End Element access operators */
  
    /*!
    ** \brief Returns the name of the class 
    **       
    */
    std::string name() const;

    /*!
    ** \brief Returns the number of slices (first dimension size) 
    **        in the %Array3d
    */
    size_type dim1() const;
  
    /*!
    ** \brief Returns the number of slices (first dimension size) 
    **        in the %Array3d
    */
    size_type slices() const;
 
    /*!
    ** \brief Returns the number of rows (second dimension size) 
    **        in the %Array3d
    */
    size_type dim2() const;

    /*!
    ** \brief Returns the number of rows (second dimension size) 
    **        in the %Array3d
    */
    size_type rows() const;

    /*!
    ** \brief Returns the number of columns (third dimension size) 
    **        in the %Array3d
    */
    size_type dim3() const;

    /*!
    ** \brief Returns the number of columns (third dimension size) 
    **        in the %Array3d
    */
    size_type cols() const;

    /*!
    ** \brief Returns the number of columns (third dimension size) 
    **        in the %Array3d
    */
    size_type columns() const;


  
    /*!
    ** \brief Returns the number of elements in the %Array3d
    */
    size_type size() const;


    /*!
    ** \brief Returns the maximal size (number of elements) in the %Array3d
    */
    size_type max_size() const;


    /*!
    ** \brief Returns the number of elements in a slice of the %Array3d
    */
    size_type slice_size() const;

    /*!
    ** \brief Returns true if the %Array3d is empty.  (Thus size() == 0)
    */
    bool empty()const;
  
    /*!
    ** \brief Swaps data with another %Array3d.
    ** \param M A %Array3d of the same element type
    **
    ** \pre dim1() == M.dim1()
    ** \pre dim2() == M.dim2()
    ** \pre dim3() == M.dim3()
    */
    void swap(Array3d& M);
 
 


  private:
    size_type d1_;
    size_type d2_;
    size_type d3_;
    size_type size_;
    size_type slice_size_;
    T*** data_;

    /// Allocates the  memory to store the elements of the Array3d
    void allocate();

    /// Desallocates the memory
    void desallocate();

    /*!
    ** \brief Copy the attributes of the Array3d \a rhs to the Array3d
    ** \param rhs the Array3d from which the attributes are copied
    */
    void copy_attributes(const self& rhs);
    
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
      if(version >= 0)
	{
	  ar & this->d1_ & this->d2_ & this->d3_ & this->size_ & this->slice_size_;
	  for (size_t i = 0; i < this->size_; ++i)
	    {
	      ar & (**data_)[i];
	    }
	}
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
      if(version >= 0)
	{
	  ar & this->d1_ & this->d2_ & this->d3_ & this->size_ & this->slice_size_;
	  desallocate();
	  allocate();
	  for (size_t i = 0; i < this->size_; ++i)
	    {
	      ar & (**data_)[i];
	    }
	}
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
  };
/** @} */ // end of Containers3d group

  ///double alias
  typedef slip::Array3d<double> Array3d_d;
  ///float alias
  typedef slip::Array3d<float> Array3d_f;
  ///long alias
  typedef slip::Array3d<long> Array3d_l;
  ///unsigned long alias
  typedef slip::Array3d<unsigned long> Array3d_ul;
  ///short alias
  typedef slip::Array3d<short> Array3d_s;
  ///unsigned long alias
  typedef slip::Array3d<unsigned short> Array3d_us;
  ///int alias
  typedef slip::Array3d<int> Array3d_i;
  ///unsigned int alias
  typedef slip::Array3d<unsigned int> Array3d_ui;
  ///char alias
  typedef slip::Array3d<char> Array3d_c;
  ///unsigned char alias
  typedef slip::Array3d<unsigned char> Array3d_uc;
}//slip::


namespace slip
{

  //////////////////////////////////////////
  //   Constructors
  //////////////////////////////////////////
  template<typename T>
  inline
  Array3d<T>::Array3d():
    d1_(0),d2_(0),d3_(0),size_(0),slice_size_(0),data_(0)
  {}

  
  template<typename T>
  inline
  Array3d<T>::Array3d(const typename Array3d<T>::size_type d1,
		      const typename Array3d<T>::size_type d2,
		      const typename Array3d<T>::size_type d3):
    d1_(d1),d2_(d2),d3_(d3),size_(d1 * d2 * d3),slice_size_(d2*d3)
  {
    this->allocate();
    std::fill_n(this->data_[0][0],this->size_,T());
  }

  template<typename T>
  inline
  Array3d<T>::Array3d(const typename Array3d<T>::size_type d1,
		      const typename Array3d<T>::size_type d2,
		      const typename Array3d<T>::size_type d3,
		      const T& val):
    d1_(d1),d2_(d2),d3_(d3),size_(d1 * d2 * d3),slice_size_(d2*d3)
  {
    this->allocate();
    std::fill_n(this->data_[0][0],this->size_,val);
  }  

  template<typename T>
  inline
  Array3d<T>::Array3d(const typename Array3d<T>::size_type d1,
		      const typename Array3d<T>::size_type d2,
		      const typename Array3d<T>::size_type d3,
		      const T* val):
    d1_(d1),d2_(d2),d3_(d3),size_(d1 * d2 * d3),slice_size_(d2*d3)
  {
    this->allocate();
    std::copy(val,val + this->size_, this->data_[0][0]); 
  }

  template<typename T>
  inline
  Array3d<T>::Array3d(const Array3d<T>& rhs):
    d1_(rhs.d1_),d2_(rhs.d2_),d3_(rhs.d3_),size_(rhs.size_),slice_size_(rhs.slice_size_)
  {
    this->allocate();
    if(this->size_ != 0)
      {
	std::copy(rhs.data_[0][0],rhs.data_[0][0] + this->size_,this->data_[0][0]);
      }
  }

  template<typename T>
  inline
  Array3d<T>::~Array3d()
  {
    this->desallocate();
  }

  ///////////////////////////////////////////
  

  //////////////////////////////////////////
  //   Assignment operators
  //////////////////////////////////////////
  template<typename T>
  inline
  Array3d<T>&  Array3d<T>::operator=(const Array3d<T> & rhs)
  {
    if(this != &rhs)
      {
	if( this->d1_ != rhs.d1_ || this->d2_ != rhs.d2_ || this->d3_ != rhs.d3_)
	  {
	    this->desallocate();
	    this->copy_attributes(rhs);
	    this->allocate();
	  }
	if(rhs.size_ != 0)
	  {
	    std::copy(rhs.data_[0][0],rhs.data_[0][0] + this->size_,this->data_[0][0]);
	  }
      }
    return *this;
  }

  template<typename T>
  inline
  Array3d<T>& Array3d<T>::operator=(const T& value)
  {
    this->fill(value);
    return *this;
  }
  ///////////////////////////////////////////
  
  
  template<typename T>
  inline
  void Array3d<T>::resize(const typename Array3d<T>::size_type d1,
			  const typename Array3d<T>::size_type d2,
			  const typename Array3d<T>::size_type d3,
			  const T& val)
  {
    if( this->d1_ != d1 || this->d2_ != d2 || this->d3_ != d3)
      {
	this->desallocate();
	this->d1_ = d1;
	this->d2_ = d2;
	this->d3_ = d3;
	this->size_ = d1 * d2 * d3;
	this->slice_size_ = d2 * d3;
	this->allocate();
      }
    if( this->d1_ != 0 && this->d2_ != 0 && this->d3_ != 0)
      {
	std::fill_n(this->data_[0][0],this->size_,val);
      }
    
  }



  //////////////////////////////////////////
  //   Iterators
  //////////////////////////////////////////

  //****************************************************************************
  //                          One dimensional iterators
  //****************************************************************************
  
  

  //----------------------Global iterators------------------------------
  
  template<typename T>
  inline
  typename Array3d<T>::const_iterator Array3d<T>::begin() const
  {
    return this->data_[0][0];
  }

  template<typename T>
  inline
  typename Array3d<T>::const_iterator Array3d<T>::end() const
  {
    return this->data_[0][0] + this->size_;
  }
   
  template<typename T>
  inline
  typename Array3d<T>::iterator Array3d<T>::begin()
  {
    return this->data_[0][0];
  }

  template<typename T>
  inline
  typename Array3d<T>::iterator Array3d<T>::end()
  {
    return this->data_[0][0] + this->size_;
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator Array3d<T>::rbegin()
  {
    return typename Array3d<T>::reverse_iterator(this->end());
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator Array3d<T>::rbegin() const
  {
    return typename Array3d<T>::const_reverse_iterator(this->end());
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator Array3d<T>::rend()
  {
    return typename Array3d<T>::reverse_iterator(this->begin());
  }

  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator Array3d<T>::rend() const
  {
    return  typename Array3d<T>::const_reverse_iterator(this->begin());
  }

  //--------------------Constant slice global iterators----------------------

  template<typename T>
  inline
  typename Array3d<T>::slice_iterator 
  Array3d<T>::slice_begin(const typename Array3d<T>::size_type row, 
			  const typename Array3d<T>::size_type col)
  {
    assert(row < this->d2_);
    assert(col < this->d3_);
    return typename Array3d<T>::slice_iterator(this->data_[0][row] + col,this->d2_*this->d3_);
  }
 
  template<typename T>
  inline
  typename Array3d<T>::const_slice_iterator 
  Array3d<T>::slice_begin(const typename Array3d<T>::size_type row,
			  const typename Array3d<T>::size_type col) const
  {
    assert(row < this->d2_);
    assert(col < this->d3_);
    return typename Array3d<T>::const_slice_iterator(this->data_[0][row] + col,this->d2_*this->d3_);
  }
 
  template<typename T>
  inline
  typename Array3d<T>::slice_iterator 
  Array3d<T>::slice_end(const typename Array3d<T>::size_type row,
			const typename Array3d<T>::size_type col)
  {
   assert(row < this->d2_);
   assert(col < this->d3_);
   return (this->slice_begin(row,col) + this->d1_);
  }
 
  template<typename T>
  inline
  typename Array3d<T>::const_slice_iterator 
  Array3d<T>::slice_end(const typename Array3d<T>::size_type row,
			const typename Array3d<T>::size_type col) const
  {
   assert(row < this->d2_);
   assert(col < this->d3_);
   slip::Array3d<T> const * tp(this); 
   return (tp->slice_begin(row,col) + this->d1_);
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_slice_iterator 
  Array3d<T>::slice_rbegin(const typename Array3d<T>::size_type row,
			   const typename Array3d<T>::size_type col)
  {
   assert(row < this->d2_);
   assert(col < this->d3_);
   return typename Array3d<T>::reverse_slice_iterator(this->slice_end(row,col));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_slice_iterator 
  Array3d<T>::slice_rbegin(const typename Array3d<T>::size_type row,
			   const typename Array3d<T>::size_type col) const
  {
   assert(row < this->d2_);
   assert(col < this->d3_);
   return typename Array3d<T>::const_reverse_slice_iterator(this->slice_end(row,col));
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_slice_iterator 
  Array3d<T>::slice_rend(const typename Array3d<T>::size_type row,
			 const typename Array3d<T>::size_type col)
  {
   assert(row < this->d2_);
   assert(col < this->d3_);
   return typename Array3d<T>::reverse_slice_iterator(this->slice_begin(row,col));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_slice_iterator 
  Array3d<T>::slice_rend(const typename Array3d<T>::size_type row,
			 const typename Array3d<T>::size_type col) const
  {
   assert(row < this->d2_);
   assert(col < this->d3_);
   return typename Array3d<T>::const_reverse_slice_iterator(this->slice_begin(row,col));
  }

  //--------------------simple row iterators----------------------

  template<typename T>
  inline
  typename Array3d<T>::row_iterator 
  Array3d<T>::row_begin(const typename Array3d<T>::size_type slice,
			const typename Array3d<T>::size_type row)
  {
    assert(slice < this->d1_);
    assert(row   < this->d2_);
    return this->data_[slice][row];
  }

  template<typename T>
  inline
  typename Array3d<T>::const_row_iterator 
  Array3d<T>::row_begin(const typename Array3d<T>::size_type slice,
			const typename Array3d<T>::size_type row) const
  {
    assert(slice < this->d1_);
    assert(row   < this->d2_);
    return this->data_[slice][row];
  }

  template<typename T>
  inline
  typename Array3d<T>::row_iterator 
  Array3d<T>::row_end(const typename Array3d<T>::size_type slice,
		      const typename Array3d<T>::size_type row)
  {
    assert(slice < this->d1_);
    assert(row   < this->d2_);
    return this->data_[slice][row] + this->d3_;
  }

  template<typename T>
  inline
  typename Array3d<T>::const_row_iterator 
  Array3d<T>::row_end(const typename Array3d<T>::size_type slice,
		      const typename Array3d<T>::size_type row) const
  {
    assert(slice < this->d1_);
    assert(row   < this->d2_);
    return this->data_[slice][row] + this->d3_;
  }


  template<typename T>
  inline
  typename Array3d<T>::reverse_row_iterator 
  Array3d<T>::row_rbegin(const typename Array3d<T>::size_type slice,
			 const typename Array3d<T>::size_type row)
  {
    assert(slice < this->d1_);
    assert(row   < this->d2_);
    return typename Array3d<T>::reverse_row_iterator(this->row_end(slice,row));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_row_iterator 
  Array3d<T>::row_rbegin(const typename Array3d<T>::size_type slice,
			 const typename Array3d<T>::size_type row) const
  {
    assert(slice < this->d1_);
    assert(row   < this->d2_);
    return typename Array3d<T>::const_reverse_row_iterator(this->row_end(slice,row));
  }


  template<typename T>
  inline
  typename Array3d<T>::reverse_row_iterator 
  Array3d<T>::row_rend(const typename Array3d<T>::size_type slice,
		       const typename Array3d<T>::size_type row)
  {
    assert(slice < this->d1_);
    assert(row   < this->d2_);
    return typename Array3d<T>::reverse_row_iterator(this->row_begin(slice,row));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_row_iterator 
  Array3d<T>::row_rend(const typename Array3d<T>::size_type slice,
		       const typename Array3d<T>::size_type row) const
  {
    assert(slice < this->d1_);
    assert(row   < this->d2_);
    return typename Array3d<T>::const_reverse_row_iterator(this->row_begin(slice,row));
  }

  //--------------------Simple col iterators----------------------

  template<typename T>
  inline
  typename Array3d<T>::col_iterator 
  Array3d<T>::col_begin(const typename Array3d<T>::size_type slice,
			const typename Array3d<T>::size_type col)
  {
    assert(slice < this->d1_);
    assert(col < this->d3_);
    return typename Array3d<T>::col_iterator(this->data_[slice][0] + col,this->d3_);
  }

  template<typename T>
  inline
  typename Array3d<T>::const_col_iterator 
  Array3d<T>::col_begin(const typename Array3d<T>::size_type slice,
			const typename Array3d<T>::size_type col) const
  {
    assert(slice < this->d1_);
    assert(col < this->d3_);
    return typename Array3d<T>::const_col_iterator(this->data_[slice][0] + col,this->d3_);
  }


  template<typename T>
  inline
  typename Array3d<T>::col_iterator 
  Array3d<T>::col_end(const typename Array3d<T>::size_type slice,
		      const typename Array3d<T>::size_type col)
  {
    assert(slice < this->d1_);
    assert(col < this->d3_);
    return ++(typename Array3d<T>::col_iterator(this->data_[slice][this->d2_- 1] + col,this->d3_));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_col_iterator 
  Array3d<T>::col_end(const typename Array3d<T>::size_type slice,
		      const typename Array3d<T>::size_type col) const
  {
    assert(slice < this->d1_);
    assert(col < this->d3_);
    return ++(typename Array3d<T>::const_col_iterator(this->data_[slice][this->d2_- 1] + col,this->d3_));

  }
 
  template<typename T>
  inline
  typename Array3d<T>::reverse_col_iterator 
  Array3d<T>::col_rbegin(const typename Array3d<T>::size_type slice,
			 const typename Array3d<T>::size_type col)
  {
    assert(slice < this->d1_);
    assert(col   < this->d3_);
    return typename Array3d<T>::reverse_col_iterator(this->col_end(slice,col));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_col_iterator 
  Array3d<T>::col_rbegin(const typename Array3d<T>::size_type slice,
			 const typename Array3d<T>::size_type col) const
  {
    assert(slice < this->d1_);
    assert(col   < this->d3_);
    return typename Array3d<T>::const_reverse_col_iterator(this->col_end(slice,col));
  }


  template<typename T>
  inline
  typename Array3d<T>::reverse_col_iterator 
  Array3d<T>::col_rend(const typename Array3d<T>::size_type slice,
		       const typename Array3d<T>::size_type col)
  {
    assert(slice < this->d1_);
    assert(col   < this->d3_);
    return typename Array3d<T>::reverse_col_iterator(this->col_begin(slice,col));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_col_iterator 
  Array3d<T>::col_rend(const typename Array3d<T>::size_type slice,
		       const typename Array3d<T>::size_type col) const
  {
    assert(slice < this->d1_);
    assert(col   < this->d3_);
    return typename Array3d<T>::const_reverse_col_iterator(this->col_begin(slice,col));
  }

 
  //--------------------Constant slice range iterators----------------------

  template<typename T>
  inline
  typename Array3d<T>::slice_range_iterator 
  Array3d<T>::slice_begin(const typename Array3d<T>::size_type row, const typename Array3d<T>::size_type col,
			  const slip::Range<int>& range)
  {
    assert(row < this->d2_);
    assert(col < this->d3_);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < (int)this->d1_);
    assert(range.stop()  < (int)this->d1_);
    return slip::stride_iterator<typename Array3d<T>::slice_iterator>(this->slice_begin(row,col) + range.start(),range.stride());
  }

  template<typename T>
  inline
  typename Array3d<T>::const_slice_range_iterator 
  Array3d<T>::slice_begin(const typename Array3d<T>::size_type row, const typename Array3d<T>::size_type col,
			  const slip::Range<int>& range) const
  {
    assert(row < this->d2_);
    assert(col < this->d3_);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < (int)this->d1_);
    assert(range.stop() < (int)this->d1_);
    return slip::stride_iterator<typename Array3d<T>::const_slice_iterator>(this->slice_begin(row,col) + range.start(),range.stride());
  }

  template<typename T>
  inline
  typename Array3d<T>::slice_range_iterator 
  Array3d<T>::slice_end(const typename Array3d<T>::size_type row, const typename Array3d<T>::size_type col,
			const slip::Range<int>& range)
  {
    assert(row < this->d2_);
    assert(col < this->d3_);
    assert(range.start() < (int)this->d1_);
    assert(range.stop()  < (int)this->d1_);
    return ++(slip::stride_iterator<typename Array3d<T>::slice_iterator>(this->slice_begin(row,col,range) + range.iterations()));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_slice_range_iterator 
  Array3d<T>::slice_end(const typename Array3d<T>::size_type row, const typename Array3d<T>::size_type col,
		      const slip::Range<int>& range) const
  {
    assert(row < this->d2_);
    assert(col < this->d3_);
    assert(range.start() < (int)this->d1_);
    assert(range.stop()  < (int)this->d1_);
    return ++(slip::stride_iterator<typename Array3d<T>::const_slice_iterator>(this->slice_begin(row,col,range) + range.iterations()));
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_slice_range_iterator 
  Array3d<T>::slice_rbegin(const typename Array3d<T>::size_type row, const typename Array3d<T>::size_type col,
			   const slip::Range<int>& range)
  {
    assert(row < this->d2_);
    assert(col < this->d3_);
    assert(range.start() < (int)this->d1_);
    return typename Array3d<T>::reverse_slice_range_iterator(this->slice_end(row,col,range));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_slice_range_iterator 
  Array3d<T>::slice_rbegin(const typename Array3d<T>::size_type row, const typename Array3d<T>::size_type col,
			   const slip::Range<int>& range) const
  {
    assert(row < this->d2_);
    assert(col < this->d3_);
    assert(range.start() < (int)this->d1_);
    return typename Array3d<T>::const_reverse_slice_range_iterator(this->slice_end(row,col,range));
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_slice_range_iterator 
  Array3d<T>::slice_rend(const typename Array3d<T>::size_type row, const typename Array3d<T>::size_type col,
			 const slip::Range<int>& range)
  {
    assert(row < this->d2_);
    assert(col < this->d3_);
    assert(range.start() < (int)this->d1_);
    return typename Array3d<T>::reverse_slice_range_iterator(this->slice_begin(row,col,range));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_slice_range_iterator 
  Array3d<T>::slice_rend(const typename Array3d<T>::size_type row, const typename Array3d<T>::size_type col,
			 const slip::Range<int>& range) const
  {
    assert(row < this->d2_);
    assert(col < this->d3_);
    assert(range.start() < (int)this->d1_);
    return typename Array3d<T>::const_reverse_slice_range_iterator(this->slice_begin(row,col,range));
  }
  
  //--------------------Constant row range iterators----------------------

  template<typename T>
  inline
  typename Array3d<T>::row_range_iterator 
  Array3d<T>::row_begin(const typename Array3d<T>::size_type slice, const typename Array3d<T>::size_type row,const slip::Range<int>& range)
  {
    assert(row < this->d2_);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < (int)this->d3_);
    assert(range.stop()  < (int)this->d3_);
    return slip::stride_iterator<typename Array3d<T>::row_iterator>(this->row_begin(slice,row) + range.start(),range.stride());
  }

  template<typename T>
  inline
  typename Array3d<T>::const_row_range_iterator 
  Array3d<T>::row_begin(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type row,
			const slip::Range<int>& range) const
  {
    assert(row < this->d2_);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < (int)this->d3_);
    assert(range.stop() < (int)this->d3_);
    return slip::stride_iterator<typename Array3d<T>::const_row_iterator>(this->row_begin(slice,row) + range.start(),range.stride());
  }

  template<typename T>
  inline
  typename Array3d<T>::row_range_iterator 
  Array3d<T>::row_end(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type row,
		      const slip::Range<int>& range)
  {
    assert(row < this->d2_);
    assert(range.start() < (int)this->d3_);
    assert(range.stop()  < (int)this->d3_);
    return ++(slip::stride_iterator<typename Array3d<T>::row_iterator>(this->row_begin(slice,row) + range.start() + range.stride() * range.iterations(),range.stride()));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_row_range_iterator 
  Array3d<T>::row_end(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type row,
		      const slip::Range<int>& range) const
  {
    assert(row < this->d2_);
    assert(range.start() < (int)this->d3_);
    assert(range.stop()  < (int)this->d3_);
    return ++(slip::stride_iterator<typename Array3d<T>::const_row_iterator>(this->row_begin(slice,row) + range.start() + range.stride() * range.iterations(),range.stride()));
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_row_range_iterator 
  Array3d<T>::row_rbegin(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type row,
			 const slip::Range<int>& range)
  {
    assert(row < this->d2_);
    assert(range.start() < (int)this->d3_);
    return typename Array3d<T>::reverse_row_range_iterator(this->row_end(slice,row,range));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_row_range_iterator 
  Array3d<T>::row_rbegin(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type row,
			 const slip::Range<int>& range) const
  {
    assert(row < this->d2_);
    assert(range.start() < (int)this->d3_);
    return typename Array3d<T>::const_reverse_row_range_iterator(this->row_end(slice,row,range));
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_row_range_iterator 
  Array3d<T>::row_rend(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type row,
		       const slip::Range<int>& range)
  {
    assert(row < this->d2_);
    assert(range.start() < (int)this->d3_);
    return typename Array3d<T>::reverse_row_range_iterator(this->row_begin(slice,row,range));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_row_range_iterator 
  Array3d<T>::row_rend(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type row,
		       const slip::Range<int>& range) const
  {
    assert(row < this->d2_);
    assert(range.start() < (int)this->d3_);
    return typename Array3d<T>::const_reverse_row_range_iterator(this->row_begin(slice,row,range));
  }

  //--------------------Constant col range iterators----------------------  

  template<typename T>
  inline
  typename Array3d<T>::col_range_iterator 
  Array3d<T>::col_begin(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type col,
			const slip::Range<int>& range)
  {
    assert(col < this->d3_);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < (int)this->d2_);
    assert(range.stop()  < (int)this->d2_);
    return slip::stride_iterator<typename Array3d<T>::col_iterator>(this->col_begin(slice,col) + range.start(),range.stride());
  }

  template<typename T>
  inline
  typename Array3d<T>::const_col_range_iterator 
  Array3d<T>::col_begin(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type col,
			const slip::Range<int>& range) const
  {
    assert(col < this->d3_);
    assert(range.is_valid());
    assert(range.start() >= 0);
    assert(range.stop()  >= 0);
    assert(range.start() < (int)this->d2_);
    assert(range.stop()  < (int)this->d2_);
    return slip::stride_iterator<typename Array3d<T>::const_col_iterator>(this->col_begin(slice,col) + range.start(),range.stride());
  }
 
  template<typename T>
  inline
  typename Array3d<T>::col_range_iterator 
  Array3d<T>::col_end(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type col,
		      const slip::Range<int>& range)
  {
    assert(col < this->d3_);
    assert(range.start() < (int)this->d2_);
    assert(range.stop()  < (int)this->d2_);
    return ++(slip::stride_iterator<typename Array3d<T>::col_iterator>(this->col_begin(slice,col) + range.start() + range.stride() * range.iterations(),range.stride()));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_col_range_iterator 
  Array3d<T>::col_end(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type col,
		      const slip::Range<int>& range) const
  {
    assert(col < this->d3_);
    assert(range.start() < (int)this->d2_);
    assert(range.stop()  < (int)this->d2_);
    return ++(slip::stride_iterator<typename Array3d<T>::const_col_iterator>(this->col_begin(slice,col) + range.start() + range.stride() * range.iterations(),range.stride()));
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_col_range_iterator 
  Array3d<T>::col_rbegin(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type col,
			 const slip::Range<int>& range)
  {
    assert(col < this->d3_);
    assert(range.start() < (int)this->d2_);
    return typename Array3d<T>::reverse_col_range_iterator(this->col_end(slice,col,range));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_col_range_iterator 
  Array3d<T>::col_rbegin(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type col,
			 const slip::Range<int>& range) const
  {
    assert(col < this->d3_);
    assert(range.start() < (int)this->d2_);
    return typename Array3d<T>::const_reverse_col_range_iterator(this->col_end(slice,col,range));
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_col_range_iterator 
  Array3d<T>::col_rend(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type col,
		       const slip::Range<int>& range)
  {
    assert(col < this->d3_);
    assert(range.start() < (int)this->d2_);
    return typename Array3d<T>::reverse_col_range_iterator(this->col_begin(slice,col,range));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_col_range_iterator 
  Array3d<T>::col_rend(const typename Array3d<T>::size_type slice,const typename Array3d<T>::size_type col,
		       const slip::Range<int>& range) const
  {
    assert(col < this->d3_);
    assert(range.start() < (int)this->d2_);
    return typename Array3d<T>::const_reverse_col_range_iterator(this->col_begin(slice,col,range));
  }
 
  //-------------------plane global iterators----------

  template<typename T>
  inline
  typename Array3d<T>::iterator 
  Array3d<T>::plane_begin(const typename Array3d<T>::size_type slice)
  {
   return this->data_[slice][0];
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_iterator 
  Array3d<T>::plane_begin(const typename Array3d<T>::size_type slice) const
  {
    return this->data_[slice][0];
  }
  
  template<typename T>
  inline
  typename Array3d<T>::iterator 
  Array3d<T>::plane_end(const typename Array3d<T>::size_type slice)
  {
    return this->data_[slice][0] + this->slice_size_;
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_iterator 
  Array3d<T>::plane_end(const typename Array3d<T>::size_type slice) const
  {
    return this->data_[slice][0] + this->slice_size_;
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator 
  Array3d<T>::plane_rbegin(const typename Array3d<T>::size_type slice)
  {
    return typename Array3d<T>::reverse_iterator(this->plane_end(slice));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator 
  Array3d<T>::plane_rbegin(const typename Array3d<T>::size_type slice) const
  {
    return typename Array3d<T>::const_reverse_iterator(this->plane_end(slice));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator 
  Array3d<T>::plane_rend(const typename Array3d<T>::size_type slice)
  {
    return typename Array3d<T>::reverse_iterator(this->plane_begin(slice));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator 
  Array3d<T>::plane_rend(const typename Array3d<T>::size_type slice) const
  {
    return typename Array3d<T>::const_reverse_iterator(this->plane_begin(slice));
  }

#ifdef ALL_PLANE_ITERATOR3D

  //-------------------plane row and col iterators----------

  template<typename T>
  inline
  typename Array3d<T>::iterator1d 
  Array3d<T>::plane_row_begin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			      const typename Array3d<T>::size_type row)
  {
    if(P == 0)
      return typename Array3d<T>::iterator1d (this->data_[plane_coordinate][row],1);
    if(P == 1)
      return typename Array3d<T>::iterator1d (this->data_[row][plane_coordinate],1);
    return typename Array3d<T>::iterator1d (this->data_[row][0] + plane_coordinate,this->d3_);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_iterator1d  
  Array3d<T>::plane_row_begin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			      const typename Array3d<T>::size_type row) const
  {
    if(P == 0)
      return typename Array3d<T>::const_iterator1d (this->data_[plane_coordinate][row],1);
    if(P == 1)
      return typename Array3d<T>::const_iterator1d (this->data_[row][plane_coordinate],1);
    return typename Array3d<T>::const_iterator1d (this->data_[row][0] + plane_coordinate,this->d3_);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::iterator1d 
  Array3d<T>::plane_row_end(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, 
			    const typename Array3d<T>::size_type row)
  {
    if(P==2)
      return this->plane_row_begin(P,plane_coordinate,row) + this->d2_; 
    return this->plane_row_begin(P,plane_coordinate,row) + this->d3_; 
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_iterator1d 
  Array3d<T>::plane_row_end(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			    const typename Array3d<T>::size_type row) const
  { 
    if(P==2)
      return this->plane_row_begin(P,plane_coordinate,row) + this->d2_; 
    return this->plane_row_begin(P,plane_coordinate,row) + this->d3_; 
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator1d 
  Array3d<T>::plane_row_rbegin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			       const typename Array3d<T>::size_type row)
  {
    return typename Array3d<T>::reverse_iterator1d(this->plane_row_end(P,plane_coordinate,row));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator1d 
  Array3d<T>::plane_row_rbegin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			    const typename Array3d<T>::size_type row) const
  {
    return typename Array3d<T>::const_reverse_iterator1d(this->plane_row_end(P,plane_coordinate,row));
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator1d 
  Array3d<T>::plane_row_rend(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, 
		 const typename Array3d<T>::size_type row)
  {
    return typename Array3d<T>::reverse_iterator1d(this->plane_row_begin(P,plane_coordinate,row));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator1d 
  Array3d<T>::plane_row_rend(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			  const typename Array3d<T>::size_type row) const
  {
    return typename Array3d<T>::const_reverse_iterator1d(this->plane_row_begin(P,plane_coordinate,row));
  }

  template<typename T>
  inline
  typename Array3d<T>::iterator1d 
  Array3d<T>::plane_col_begin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			   const typename Array3d<T>::size_type col)
  {
    if(P == 0)
      return typename Array3d<T>::iterator1d (this->data_[plane_coordinate][0] + col,this->d3_);
    if(P == 1)
      return typename Array3d<T>::iterator1d (this->data_[0][plane_coordinate] + col,this->d2_ * this->d3_);
    return typename Array3d<T>::iterator1d (this->data_[0][col] + plane_coordinate,this->d2_ * this->d3_);
  }

  template<typename T>
  inline
  typename Array3d<T>::const_iterator1d 
  Array3d<T>::plane_col_begin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			   const typename Array3d<T>::size_type col) const
  { 
    // std::cout << "const" << std::endl;
    if(P == 0)
      return typename Array3d<T>::const_iterator1d (this->data_[plane_coordinate][0] + col,this->d3_);
    if(P == 1)
      return typename Array3d<T>::const_iterator1d (this->data_[0][plane_coordinate] + col,this->d2_ * this->d3_);
    return typename Array3d<T>::const_iterator1d (this->data_[0][col] + plane_coordinate,this->d2_ * this->d3_);
  }
    
  template<typename T>
  inline
  typename Array3d<T>::iterator1d 
  Array3d<T>::plane_col_end(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, 
			 const typename Array3d<T>::size_type col)
  {
    if(P == 0)
      return this->Array3d::plane_col_begin(P,plane_coordinate,col) + this->d2_;
    return this->Array3d::plane_col_begin(P,plane_coordinate,col) + this->d1_;
  }

  template<typename T>
  inline
  typename Array3d<T>::const_iterator1d 
  Array3d<T>::plane_col_end(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			 const typename Array3d<T>::size_type col) const
  {
    if(P == 0)
      return this->Array3d::plane_col_begin(P,plane_coordinate,col) + this->d2_;
    return this->Array3d::plane_col_begin(P,plane_coordinate,col) + this->d1_;
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator1d 
  Array3d<T>::plane_col_rbegin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			    const typename Array3d<T>::size_type col)
  {
    return typename Array3d<T>::reverse_iterator1d(this->plane_col_end(P,plane_coordinate,col));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator1d 
  Array3d<T>::plane_col_rbegin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			    const typename Array3d<T>::size_type col) const
  {
    return typename Array3d<T>::const_reverse_iterator1d(this->plane_col_end(P,plane_coordinate,col));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator1d 
  Array3d<T>::plane_col_rend(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, 
			  const typename Array3d<T>::size_type col)
  { 
    return typename Array3d<T>::reverse_iterator1d(this->plane_col_begin(P,plane_coordinate,col));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator1d 
  Array3d<T>::plane_col_rend(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			  const typename Array3d<T>::size_type col) const
  {
    return typename Array3d<T>::const_reverse_iterator1d(this->plane_col_begin(P,plane_coordinate,col));
  }

  //-------------------plane row and col box iterators----------

  template<typename T>
  inline
  typename Array3d<T>::const_iterator1d 
  Array3d<T>::plane_row_begin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			      const size_type row, const Box2d<int> & b) const
  {
    if(P == 0)
      return typename Array3d<T>::const_iterator1d (this->data_[plane_coordinate][b.upper_left()[0] + row] + b.upper_left()[1],1);
    if(P == 1)
      return typename Array3d<T>::const_iterator1d (this->data_[b.upper_left()[0] + row][plane_coordinate] + b.upper_left()[1],1);
    return typename Array3d<T>::const_iterator1d (this->data_[b.upper_left()[0] + row][b.upper_left()[1]] + plane_coordinate,this->d3_);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::iterator1d 
  Array3d<T>::plane_row_begin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			      const size_type row, const Box2d<int> & b)
  {
    if(P == 0)
      return typename Array3d<T>::iterator1d (this->data_[plane_coordinate][b.upper_left()[0] + row] + b.upper_left()[1],1);
    if(P == 1)
      return typename Array3d<T>::iterator1d (this->data_[b.upper_left()[0] + row][plane_coordinate] + b.upper_left()[1],1);
    return typename Array3d<T>::iterator1d (this->data_[b.upper_left()[0] + row][b.upper_left()[1]] + plane_coordinate,this->d3_);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::iterator1d
  Array3d<T>:: plane_row_end(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, 
			     const size_type row, const Box2d<int> & b)
  {
    return this->plane_row_begin(P,plane_coordinate,row,b) + b.width(); 
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_iterator1d 
  Array3d<T>::plane_row_end(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			    const size_type row, const Box2d<int> & b) const
  {
    return this->plane_row_begin(P,plane_coordinate,row,b) + b.width(); 
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator1d
  Array3d<T>::plane_row_rbegin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			       const size_type row, const Box2d<int> & b)
  {
    return typename Array3d<T>::reverse_iterator1d(this->plane_row_end(P,plane_coordinate,row,b));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator1d
  Array3d<T>::plane_row_rbegin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			       const size_type row, const Box2d<int> & b) const
  {
    return typename Array3d<T>::const_reverse_iterator1d(this->plane_row_end(P,plane_coordinate,row,b));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator1d
  Array3d<T>::plane_row_rend(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			     const size_type row, const Box2d<int> & b)
  {
    return typename Array3d<T>::reverse_iterator1d(this->plane_row_begin(P,plane_coordinate,row,b));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator1d
  Array3d<T>::plane_row_rend(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			     const size_type row, const Box2d<int> & b) const
  {
    return typename Array3d<T>::const_reverse_iterator1d(this->plane_row_begin(P,plane_coordinate,row,b));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::iterator1d
  Array3d<T>::plane_col_begin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			      const size_type col, const Box2d<int> & b)
  {
    if(P == 0)
      return typename Array3d<T>::iterator1d (this->data_[plane_coordinate][b.upper_left()[0]] + b.upper_left()[1] + col,this->d3_);
    if(P == 1)
      return typename Array3d<T>::iterator1d (this->data_[b.upper_left()[0]][plane_coordinate] + b.upper_left()[1] + col,this->d2_ * this->d3_);
    return typename Array3d<T>::iterator1d (this->data_[b.upper_left()[0]][b.upper_left()[1] + col] + plane_coordinate,this->d2_ * this->d3_);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_iterator1d
  Array3d<T>::plane_col_begin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			      const size_type col, const Box2d<int> & b) const
  {
    if(P == 0)
      return typename Array3d<T>::const_iterator1d (this->data_[plane_coordinate][b.upper_left()[0]] + b.upper_left()[1] + col,this->d3_);
    if(P == 1)
      return typename Array3d<T>::const_iterator1d (this->data_[b.upper_left()[0]][plane_coordinate] + b.upper_left()[1] + col,this->d2_ * this->d3_);
    return typename Array3d<T>::const_iterator1d (this->data_[b.upper_left()[0]][b.upper_left()[1] + col] + plane_coordinate,this->d2_ * this->d3_);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::iterator1d
  Array3d<T>::plane_col_end(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, 
			    const size_type col, const Box2d<int> & b)
  {
    return this->Array3d::plane_col_begin(P,plane_coordinate,col,b) + b.height(); 
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_iterator1d
  Array3d<T>::plane_col_end(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			    const size_type col, const Box2d<int> & b) const
  {
    return this->Array3d::plane_col_begin(P,plane_coordinate,col,b) + b.height(); 
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator1d
  Array3d<T>::plane_col_rbegin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			       const size_type col, const Box2d<int> & b)
  {
    return typename Array3d<T>::reverse_iterator1d(this->plane_col_end(P,plane_coordinate,col,b));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator1d
  Array3d<T>::plane_col_rbegin(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			       const size_type col, const Box2d<int> & b) const
  {
    return typename Array3d<T>::const_reverse_iterator1d(this->plane_col_end(P,plane_coordinate,col,b));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator1d
  Array3d<T>::plane_col_rend(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, 
			     const size_type col, const Box2d<int> & b)
  {
    return typename Array3d<T>::reverse_iterator1d(this->plane_col_begin(P,plane_coordinate,col,b));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator1d
  Array3d<T>::plane_col_rend(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate,
			     const size_type col, const Box2d<int> & b) const
  {
    return typename Array3d<T>::const_reverse_iterator1d(this->plane_col_begin(P,plane_coordinate,col,b));
  }

#endif //ALL_PLANE_ITERATOR3D
 
  //****************************************************************************
  //                          Two dimensional iterators
  //****************************************************************************
  
  template<typename T>
  inline
  typename Array3d<T>::iterator2d 
  Array3d<T>::plane_upper_left(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate)
  {
    if(P==0)
      {
	Box3d<int> b(plane_coordinate,0,0,plane_coordinate,this->d2_-1,this->d3_-1);
	return typename Array3d<T>::iterator2d(this,b);
      }
    if(P==1)
      {
	Box3d<int> b(0,plane_coordinate,0,this->d1_-1,plane_coordinate,this->d3_-1);
	return typename Array3d<T>::iterator2d(this,b);
      } 
    Box3d<int> b(0,0,plane_coordinate,this->d1_-1,this->d2_-1,plane_coordinate);
    return typename Array3d<T>::iterator2d(this,b);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::iterator2d 
  Array3d<T>::plane_bottom_right(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate)
  { 
    typename Array3d<T>::iterator2d it = this->plane_upper_left(P,plane_coordinate);
    if(P==0)
      {
	DPoint2d<int> dp(this->d2_,this->d3_);
	return it + dp;
      }
    if(P==1)
      {
	DPoint2d<int> dp(this->d1_,this->d3_);
	return it + dp;
      }
    DPoint2d<int> dp(this->d1_,this->d2_);
    return it + dp;
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_iterator2d 
  Array3d<T>::plane_upper_left(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate) const
  { 
    if(P==0)
      {
	Box3d<int> b(plane_coordinate,0,0,plane_coordinate,this->d2_-1,this->d3_-1);
	return typename Array3d<T>::const_iterator2d(this,b);
      }
    if(P==1)
      {
	Box3d<int> b(0,plane_coordinate,0,this->d1_-1,plane_coordinate,this->d3_-1);
	return typename Array3d<T>::const_iterator2d(this,b);
      } 
    Box3d<int> b(0,0,plane_coordinate,this->d1_-1,this->d2_-1,plane_coordinate);
    return typename Array3d<T>::const_iterator2d(this,b);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_iterator2d 
  Array3d<T>::plane_bottom_right(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate) const
  {
    typename Array3d<T>::const_iterator2d it = this->plane_upper_left(P,plane_coordinate);
    if(P==0)
      {
	DPoint2d<int> dp(this->d2_,this->d3_);
	return it + dp;
      }
    if(P==1)
      {
	DPoint2d<int> dp(this->d1_,this->d3_);
	return it + dp;
      }
    DPoint2d<int> dp(this->d1_,this->d2_);
    return it + dp;
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator2d 
  Array3d<T>::plane_rupper_left(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate)
  {
    DPoint2d<int> dp(1,0);
    return typename Array3d<T>::reverse_iterator2d(this->plane_bottom_right(P,plane_coordinate) - dp);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator2d 
  Array3d<T>::plane_rbottom_right(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate)
  { 
    return typename Array3d<T>::reverse_iterator2d(this->plane_upper_left(P,plane_coordinate));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator2d 
  Array3d<T>::plane_rupper_left(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate) const
  { 
    DPoint2d<int> dp(1,0);
    return typename Array3d<T>::const_reverse_iterator2d(this->plane_bottom_right(P,plane_coordinate) - dp);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator2d 
  Array3d<T>::plane_rbottom_right(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate) const
  {
    return typename Array3d<T>::const_reverse_iterator2d(this->plane_upper_left(P,plane_coordinate));
  }
  
  //-------------------- Box plane iterators 2d----------------------  

  template<typename T>
  inline
  typename Array3d<T>::iterator2d 
  Array3d<T>::plane_upper_left(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, const Box2d<int>& b)
  {
    return typename Array3d<T>::iterator2d(this,P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::iterator2d 
  Array3d<T>::plane_bottom_right(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, const Box2d<int>& b)
  { 
    DPoint2d<int> dp(b.height(),b.width());
    typename Array3d<T>::iterator2d it = this->plane_upper_left(P,plane_coordinate,b);
    return it + dp;
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_iterator2d 
  Array3d<T>::plane_upper_left(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, const Box2d<int>& b) const
  {
    return typename Array3d<T>::const_iterator2d(this,P,plane_coordinate,b);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_iterator2d 
  Array3d<T>::plane_bottom_right(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, const Box2d<int>& b) const
  {
    DPoint2d<int> dp(b.height(),b.width());
    typename Array3d<T>::const_iterator2d it = this->plane_upper_left(P,plane_coordinate,b);
    return it + dp;
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator2d 
  Array3d<T>::plane_rupper_left(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, const Box2d<int>& b)
  {
    DPoint2d<int> dp(1,0);
    return typename Array3d<T>::reverse_iterator2d(this->plane_bottom_right(P,plane_coordinate,b)-dp);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator2d 
  Array3d<T>::plane_rbottom_right(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, const Box2d<int>& b)
  { 
    return typename Array3d<T>::reverse_iterator2d(this->plane_upper_left(P,plane_coordinate,b));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator2d 
  Array3d<T>::plane_rupper_left(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, const Box2d<int>& b) const
  {
    DPoint2d<int> dp(1,0);
    return typename Array3d<T>::const_reverse_iterator2d(this->plane_bottom_right(P,plane_coordinate,b)-dp);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator2d 
  Array3d<T>::plane_rbottom_right(PLANE_ORIENTATION P, const typename Array3d<T>::size_type plane_coordinate, const Box2d<int>& b) const
  {
    return typename Array3d<T>::const_reverse_iterator2d(this->plane_upper_left(P,plane_coordinate,b));
  }

  //****************************************************************************
  //                          Three dimensional iterators
  //****************************************************************************
  
  //------------------------ Global iterators------------------------------------

  template<typename T>
  inline
  typename Array3d<T>::iterator3d Array3d<T>::front_upper_left()
  {
    return typename Array3d<T>::iterator3d(this,Box3d<int>(0,0,0,this->d1_-1,this->d2_-1,this->d3_-1));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_iterator3d Array3d<T>::front_upper_left() const
  {
    return typename Array3d<T>::const_iterator3d(this,Box3d<int>(0,0,0,this->d1_-1,this->d2_-1,this->d3_-1));
  }


  template<typename T>
  inline
  typename Array3d<T>::iterator3d Array3d<T>::back_bottom_right()
  {
    DPoint3d<int> dp(this->d1_,this->d2_,this->d3_);
    typename Array3d<T>::iterator3d it = (*this).front_upper_left() + dp;
    return it;
  }

  template<typename T>
  inline
  typename Array3d<T>::const_iterator3d Array3d<T>::back_bottom_right() const
  {
    DPoint3d<int> dp(this->d1_,this->d2_,this->d3_);
    typename Array3d<T>::const_iterator3d it = (*this).front_upper_left() + dp;
    return it;
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator3d 
  Array3d<T>::rback_bottom_right()
  {
    return typename Array3d<T>::reverse_iterator3d(this->front_upper_left());
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator3d 
  Array3d<T>::rback_bottom_right() const
  {
    return typename Array3d<T>::const_reverse_iterator3d(this->front_upper_left());
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator3d 
  Array3d<T>::rfront_upper_left()
  {
    DPoint3d<int> dp(1,1,0);
    return typename Array3d<T>::reverse_iterator3d(this->back_bottom_right() - dp);
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator3d 
  Array3d<T>::rfront_upper_left() const
  {
    DPoint3d<int> dp(1,1,0);
    return typename Array3d<T>::const_reverse_iterator3d(this->back_bottom_right() - dp);
  }

  //------------------------ Box iterators------------------------------------

  template<typename T>
  inline
  typename Array3d<T>::iterator3d Array3d<T>::front_upper_left(const Box3d<int>& box)
  {
    return typename Array3d<T>::iterator3d(this,box);
  }

  template<typename T>
  inline
  typename Array3d<T>::const_iterator3d Array3d<T>::front_upper_left(const Box3d<int>& box) const
  {
    return typename Array3d<T>::const_iterator3d(this,box);
  }


  template<typename T>
  inline
  typename Array3d<T>::iterator3d 
  Array3d<T>::back_bottom_right(const Box3d<int>& box)
  {
    DPoint3d<int> dp(box.depth(),box.height(),box.width());
    typename Array3d<T>::iterator3d it = (*this).front_upper_left(box) + dp;
    return it;
  }

  template<typename T>
  inline
  typename Array3d<T>::const_iterator3d 
  Array3d<T>::back_bottom_right(const Box3d<int>& box) const
  {
    DPoint3d<int> dp(box.depth(),box.height(),box.width());
    typename Array3d<T>::const_iterator3d it = (*this).front_upper_left(box) + dp;
    return it;
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator3d 
  Array3d<T>::rback_bottom_right(const Box3d<int>& box)
  {
    return typename Array3d<T>::reverse_iterator3d(this->front_upper_left(box));
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator3d 
  Array3d<T>::rback_bottom_right(const Box3d<int>& box) const
  {
    return typename Array3d<T>::const_reverse_iterator3d(this->front_upper_left(box));
  }

  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator3d 
  Array3d<T>::rfront_upper_left(const Box3d<int>& box)
  {
    DPoint3d<int> dp(1,1,0);
    return typename Array3d<T>::reverse_iterator3d(this->back_bottom_right(box) - dp);
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator3d 
  Array3d<T>::rfront_upper_left(const Box3d<int>& box) const
  {
    DPoint3d<int> dp(1,1,0);
    return typename Array3d<T>::const_reverse_iterator3d(this->back_bottom_right(box) - dp);
  }

  //------------------------ Range iterators------------------------------------

  template<typename T>
  inline
  typename Array3d<T>::iterator3d_range 
  Array3d<T>::front_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
			 const Range<int>& col_range)
  {
    return typename Array3d<T>::iterator3d_range(this,slice_range,row_range,col_range);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::iterator3d_range 
  Array3d<T>::back_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
			   const Range<int>& col_range)
  {
    DPoint3d<int> dp(slice_range.iterations()+1,row_range.iterations()+1,col_range.iterations()+1);
    return  typename Array3d<T>::iterator3d_range((*this).front_upper_left(slice_range,row_range,col_range) + dp);
  }
  

  template<typename T>
  inline
  typename Array3d<T>::const_iterator3d_range 
  Array3d<T>::front_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
			 const Range<int>& col_range) const
  {
    return typename Array3d<T>::const_iterator3d_range(this,slice_range,row_range,col_range);
  }


  template<typename T>
  inline
  typename Array3d<T>::const_iterator3d_range 
  Array3d<T>::back_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
			   const Range<int>& col_range) const
  {
     DPoint3d<int> dp(slice_range.iterations()+1,row_range.iterations()+1,col_range.iterations()+1);
     return  typename Array3d<T>::const_iterator3d_range((*this).front_upper_left(slice_range,row_range,col_range) + dp);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator3d_range 
  Array3d<T>::rfront_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
			  const Range<int>& col_range)
  {
    slip::DPoint3d<int> dp(1,1,0);
   return typename Array3d<T>::reverse_iterator3d_range(this->back_bottom_right(slice_range,row_range,col_range) - dp);
   }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator3d_range 
  Array3d<T>::rfront_upper_left(const Range<int>& slice_range, const Range<int>& row_range,
			  const Range<int>& col_range) const
  {
    slip::DPoint3d<int> dp(1,1,0);
    return typename Array3d<T>::const_reverse_iterator3d_range(this->back_bottom_right(slice_range,row_range,col_range)- dp);
  }
  
  template<typename T>
  inline
  typename Array3d<T>::reverse_iterator3d_range 
  Array3d<T>::rback_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
			    const Range<int>& col_range)
  {
    return typename Array3d<T>::reverse_iterator3d_range(this->front_upper_left(slice_range,row_range,col_range));
  }
  
  template<typename T>
  inline
  typename Array3d<T>::const_reverse_iterator3d_range 
  Array3d<T>::rback_bottom_right(const Range<int>& slice_range, const Range<int>& row_range,
			    const Range<int>& col_range) const
  {
    return typename Array3d<T>::const_reverse_iterator3d_range(this->front_upper_left(slice_range,row_range,col_range));
  }

  
  ///////////////////////////////////////////


  ////////////////////////////////////////////
  // i/o operators
  ////////////////////////////////////////////
  /** \name input/output operators */
  /* @{ */
  template <typename T>
  inline
  std::ostream& operator<<(std::ostream & out, const Array3d<T>& a)
  {
    for(std::size_t i = 0; i < a.d1_; ++i)
      {
	for(std::size_t j = 0; j < a.d2_; ++j)
	  {
	    for(std::size_t k = 0; k < a.d3_; ++k)
	      {
		out<<a.data_[i][j][k]<<" ";
	      }
	    out<<std::endl;
	  }
	out<<std::endl;
      }    	
    out<<std::endl;
    return out;
  }
  /* @} */
  ///////////////////////////////////////////


  ////////////////////////////////////////////
  // Elements access operators
  ////////////////////////////////////////////
  template<typename T>
  inline
  T** Array3d<T>::operator[](const typename Array3d<T>::size_type k)
  {
    assert(this->data_ != 0);
    assert(k < this->d1_);
    return this->data_[k];
  }
  
  template<typename T>
  inline
  const T* const* Array3d<T>::operator[](const typename Array3d<T>::size_type k) const 
  {
    assert(this->data_ != 0);
    assert(k < this->d1_);
    return this->data_[k];
  }
  


  template<typename T>
  inline
  typename Array3d<T>::reference 
  Array3d<T>::operator()(const typename Array3d<T>::size_type k,
			 const typename Array3d<T>::size_type i,
			 const typename Array3d<T>::size_type j)
  {
    assert(this->data_ != 0);
    assert(k < this->d1_);
    assert(i < this->d2_);
    assert(j < this->d3_);
    return this->data_[k][i][j];
  }

  template<typename T>
  inline
  typename Array3d<T>::const_reference 
  Array3d<T>::operator()(const typename Array3d<T>::size_type k,
			 const typename Array3d<T>::size_type i,
			 const typename Array3d<T>::size_type j) const
  {
    assert(this->data_ != 0);
    assert(k < this->d1_);
    assert(i < this->d2_);
    assert(j < this->d3_);
    return this->data_[k][i][j];
  }
  ///////////////////////////////////////////

  template<typename T>
  inline
  std::string 
  Array3d<T>::name() const {return "Array3d";} 
  

  template<typename T>
  inline
  typename Array3d<T>::size_type 
  Array3d<T>::dim1() const {return this->d1_;} 

  template<typename T>
  inline
  typename Array3d<T>::size_type 
  Array3d<T>::slices() const {return this->dim1();} 

  template<typename T>
  inline
  typename Array3d<T>::size_type 
  Array3d<T>::dim2() const {return this->d2_;} 

  template<typename T>
  inline
  typename Array3d<T>::size_type 
  Array3d<T>::rows() const {return this->dim2();} 

  template<typename T>
  inline
  typename Array3d<T>::size_type 
  Array3d<T>::dim3() const {return this->d3_;} 

  template<typename T>
  inline
  typename Array3d<T>::size_type 
  Array3d<T>::cols() const {return this->dim3();} 

  template<typename T>
  inline
  typename Array3d<T>::size_type 
  Array3d<T>::columns() const {return this->dim3();} 


  template<typename T>
  inline
  typename Array3d<T>::size_type 
  Array3d<T>::size() const {return this->size_;}

  template<typename T>
  inline
  typename Array3d<T>::size_type 
  Array3d<T>::max_size() const 
  {
    return typename Array3d<T>::size_type(-1)/sizeof(T);
  }

  template<typename T>
  inline
  typename Array3d<T>::size_type 
  Array3d<T>::slice_size() const {return this->slice_size_;}


  template<typename T>
  inline
  bool Array3d<T>::empty()const {return this->size_ == 0;}

  template<typename T>
  inline
  void Array3d<T>::swap(Array3d<T>& M)
  {
    assert(this->d1_ == M.d1_);
    assert(this->d2_ == M.d2_);
    assert(this->d3_ == M.d3_);
    std::swap_ranges(this->begin(),this->end(),M.begin());
  }


  ////////////////////////////////////////////
  // comparison operators
  ////////////////////////////////////////////
  /** \name EqualityComparable functions */
  /* @{ */
  template<typename T>
  inline
  bool operator==(const Array3d<T>& x, 
		  const Array3d<T>& y)
  {
    return ( x.size() == y.size()
	     && std::equal(x.begin(),x.end(),y.begin())); 
  }

  template<typename T>
  inline
  bool operator!=(const Array3d<T>& x, 
		  const Array3d<T>& y)
  {
    return !(x == y);
  }
  /* @} */
  /** \name LessThanComparable functions */
  /* @{ */
  template<typename T>
  inline
  bool operator<(const Array3d<T>& x, 
		 const Array3d<T>& y)
  {
    return std::lexicographical_compare(x.begin(), x.end(),
					y.begin(), y.end());
  }
  
  
  template<typename T>
  inline
  bool operator>(const Array3d<T>& x, 
		 const Array3d<T>& y)
  {
    return (y < x);
  }
  
  template<typename T>
  inline
  bool operator<=(const Array3d<T>& x, 
		  const Array3d<T>& y)
  {
    return !(y < x);
  }

  template<typename T>
  inline
  bool operator>=(const Array3d<T>& x, 
		  const Array3d<T>& y)
  {
    return !(x < y);
  } 
  /* @} */

  ////////////////////////////////////////////


  ////////////////////////////////////////////
  // private methods
  ////////////////////////////////////////////

  template<typename T>
  inline
  void Array3d<T>::allocate()
  {
    if( this->d1_ != 0 && this->d2_ != 0 && this->d3_ != 0)
      {
	this->data_ = new T**[this->d1_];
	this->data_[0] = new T*[this->d1_ * this->d2_];
	this->data_[0][0] = new T[this->d1_ * this->d2_ * this->d3_];
    
	for(std::size_t j = 1; j < this->d2_; ++j)
	  {
	    this->data_[0][j] = this->data_[0][j-1] + this->d3_;
	  }
	for(std::size_t i = 1; i < this->d1_; ++i)
	  {
	    this->data_[i] = this->data_[i-1] + this->d2_;
	    this->data_[i][0] = this->data_[i-1][0] + this->d2_*this->d3_;
	    for(std::size_t j = 1; j < this->d2_; ++j)
	      {
		this->data_[i][j] = this->data_[i][j-1] + this->d3_;
	      }
	  }
      }
    else
      {
	this->data_ = 0;
      }
  }

  template<typename T>
  inline
  void Array3d<T>::desallocate()
  {
    if(this->data_ != 0)
      {
	delete[] (this->data_[0][0]);
	delete[] (this->data_[0]);
	delete[] (this->data_);
      }
  }

  template<typename T>
  inline
  void Array3d<T>::copy_attributes(const Array3d<T>& rhs)
  {
    this->d1_ = rhs.d1_;
    this->d2_ = rhs.d2_;
    this->d3_ = rhs.d3_;
    this->size_ = rhs.size_;
    this->slice_size_ = rhs.slice_size_;
  }


  ////////////////////////////////////////////

}//slip::

#endif //SLIP_ARRAY3D_HH
