#ifndef SLIP_SVG_IMAGE_DESCRIPTOR_TYPES_HPP_
#define SLIP_SVG_IMAGE_DESCRIPTOR_TYPES_HPP_

#include "svg_shapes.hpp"
//#include "numeric_trait.hpp"
//#include "utils_color.hpp"
#include "utils_compilation.hpp"
#include "Point2d.hpp"
#include "Color.hpp"

#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

namespace slip
{


  /**
   ** @brief Class for drawing shapes into an image in SVG format
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   **/
  template< typename Real>
  class SVGImageDescriptorType
  {
  public:

    /**
     ** @brief Constructor
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     **/
    SVGImageDescriptorType() ;


    /**
     ** @brief Set size of the image
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param h new height
     ** @param w new width
     **/
    void resize( const size_t h , const size_t w ) ;


    /**
     ** @brief Set size of the image
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param h new height
     ** @param w new width
     **/
    void SetDimensions( const size_t w , const size_t h ) ;


    /**
     ** @brief Add a circle to the image
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param center Center of the circle
     ** @param rad Radius of the circle
     ** @param col Color of the circle
     **/
    void AddCircle( const slip::Point2d<Real> & center , const Real rad , const slip::Color<unsigned char> & col = slip::Color<unsigned char>(0,0,0)) ;


    /**
     ** @brief Add a line to the image
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param p1 First point of the line
     ** @param p2 Second point of the line
     ** @param width Width of the line
     ** @param col Color of the line
     **/
    void AddLine( const slip::Point2d<Real> & p1 , const slip::Point2d<Real> & p2 , const Real width = static_cast<Real>( 1 ) , const slip::Color<unsigned char> & col = slip::Color<unsigned char>(0,0,0)) ;


    /**
     ** @brief Add an ellipse to the image
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param center Center of the ellipse
     ** @param major_axis Major axis dimension
     ** @param minor_axis Minor axis dimension
     ** @param orient Orientation of the major axis (relative to the X axis)
     ** @param col Color of the ellipse
     **/
    void AddEllipse( const slip::Point2d<Real> & center , const Real major_axis , const Real minor_axis , const Real orient , const slip::Color<unsigned char> & col = slip::Color<unsigned char>(0,0,0)) ;


    // /**
    //  ** @brief Add an elliptical arc to the image
    //  ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
    //  ** @date 2014/07/03
    //  ** @version 0.0.1
    //  ** @param center Center of the elliptical arc
    //  ** @param major_axis Major axis dimension of the base ellipse
    //  ** @param minor_axis Minor axis dimension of the base ellipse
    //  ** @param orient Orientation of the major axis of the base ellipse (relative to the X axis)
    //  ** @param ell_start Point near the beggining of the elliptical arc
    //  ** @param ell_end Point near the end of the elliptical arc
    //  ** @param col Color of the elliptical arc
    //  **/
    // void AddEllipticalArc( const slip::Point2d<Real> & center , const Real major_axis , const Real minor_axis , const Real orient , const slip::Point2d<Real> & ell_start , const slip::Point2d<Real> & ell_end , const slip::Color<unsigned char> & col = slip::Color<unsigned char>(0,0,0)) ;


    /**
     ** @brief Add a circular arc to the image
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param center Center of the base circle
     ** @param radius Radius of the base circle
     ** @param angle_start Angular start of the circular arc
     ** @param angle_end Angular end of the circular arc
     ** @param ell_start First point position of the circular arc
     ** @param ell_end Second point position of the circular arc
     ** @param col Color the circular arc
     **/
    void AddCircularArc( const slip::Point2d<Real> & center , const Real radius , const Real angle_start , const Real angle_end , const slip::Point2d<Real> & ell_start , const slip::Point2d<Real> & ell_end , const slip::Color<unsigned char> & col = slip::Color<unsigned char>(0,0,0)) ;


    /**
     ** @brief Add an axis aligned rectangle
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param lower lower point of the rectangle
     ** @param higher higher point of the rectangle
     ** @param col Color of the rectangle
     **/
    void AddAxisAlignedRectangle( const slip::Point2d<Real> & lower , const slip::Point2d<Real> & higher , const slip::Color<unsigned char> & col = slip::Color<unsigned char>(0,0,0)) ;


    /**
     ** @brief Write image to a file
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/07/03
     ** @version 0.0.1
     ** @param filename name of the file to write
     **/
    void Write( const std::string & filename ) const ;


    /**
    ** @brief Write a list of image to a file
    ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
    ** @date 2014/06/11
    ** @version 0.0.1
    ** @param kpts Input images to export
    ** @param file_name Output file name
    ** @param binary Value is ignored by default (it is just here for signature compatibility)
    ** @return true if export is correct | false if there was an error during export
    **/
    static bool write( const std::vector< SVGImageDescriptorType > & kpts , const std::string & file_name , const bool binary = true ) ;

    /**
     ** @brief Read a list of image from a file
     ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
     ** @date 2014/06/11
     ** @version 0.0.1
     ** @param file_name Input file_name
     ** @param kpts Output image vector
     ** @param binary Value is ignored by default (it is just here for signature compatibility)
     ** @return true if read was correct | false if there was an error during import
     **/
    static bool read( const std::string & file_name , std::vector< SVGImageDescriptorType > & kpts , const bool binary = true ) ;

    /**
     ** Holds dimensions of the image
     **/
    size_t _width ;
    size_t _height ;

    // List of lines
    std::vector< SVGLine<Real> > _lines ;

    // List of circles
    std::vector< SVGCircle<Real> > _circles ;

    // List of rect
    std::vector< SVGRect<Real> > _rects ;

    // List of ellipses
    std::vector< SVGEllipse<Real> > _ellipses ;

    // // List of elliptical arc
    // std::vector< SVGEllipticalArc<Real> > _elliptical_arcs ;

    // List of circular arcs
    std::vector< SVGCircularArc<Real> > _circular_arcs ;

  } ;


  /**
   ** @brief Constructor
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   **/
  template< typename Real>
  SVGImageDescriptorType<Real>::SVGImageDescriptorType( )
  {

  }


  /**
   ** @brief Set size of the image
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param h new height
   ** @param w new width
   **/
  template< typename Real>
  void SVGImageDescriptorType<Real>::resize( const size_t h , const size_t w )
  {
    _width = w ;
    _height = h ;
  }


  /**
   ** @brief Set size of the image
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param h new height
   ** @param w new width
   **/
  template< typename Real>
  void SVGImageDescriptorType<Real>::SetDimensions( const size_t w , const size_t h )
  {
    _width = w ;
    _height = h ;
  }


  /**
   ** @brief Add a circle to the image
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param center Center of the circle
   ** @param rad Radius of the circle
   ** @param col Color of the circle
   **/
  template< typename Real>
  void SVGImageDescriptorType<Real>::AddCircle( const slip::Point2d<Real> & center , const Real rad , const slip::Color<unsigned char> & col )
  {
    _circles.push_back( SVGCircle<Real>( center[0] , center[1] , rad , col ) ) ;
  }


  /**
   ** @brief Add a line to the image
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param p1 First point of the line
   ** @param p2 Second point of the line
   ** @param width Width of the line
   ** @param col Color of the line
   **/
  template< typename Real>
  void SVGImageDescriptorType<Real>::AddLine( const slip::Point2d<Real> & p1 , const slip::Point2d<Real> & p2 , const Real width , const slip::Color<unsigned char> & col )
  {
    _lines.push_back( SVGLine<Real>( p1[0] , p1[1] , p2[0] , p2[1] , width , col ) ) ;
  }


  /**
   ** @brief Add an axis aligned rectangle
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param lower lower point of the rectangle
   ** @param higher higher point of the rectangle
   ** @param col Color of the rectangle
   **/
  template< typename Real>
  void SVGImageDescriptorType<Real>::AddAxisAlignedRectangle( const slip::Point2d<Real> & lower , const slip::Point2d<Real> & higher , const slip::Color<unsigned char> & col )
  {
    const Real width = std::abs( lower[0] - higher[0] ) ;
    const Real height = std::abs( lower[1] - higher[1] ) ;

    const Real minx = std::min( lower[0] , higher[0] ) ;
    const Real miny = std::min( lower[1] , higher[1] ) ;

    _rects.push_back( SVGRect<Real>( minx , miny , width , height , col ) ) ;
  }


  // /**
  //  ** @brief Add an elliptical arc to the image
  //  ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
  //  ** @date 2014/07/03
  //  ** @version 0.0.1
  //  ** @param center Center of the elliptical arc
  //  ** @param major_axis Major axis dimension of the base ellipse
  //  ** @param minor_axis Minor axis dimension of the base ellipse
  //  ** @param orient Orientation of the major axis of the base ellipse (relative to the X axis)
  //  ** @param ell_start Point near the beggining of the elliptical arc
  //  ** @param ell_end Point near the end of the elliptical arc
  //  ** @param col Color of the elliptical arc
  //  **/
  // template< typename Real >
  // void SVGImageDescriptorType<Real>::AddEllipticalArc( const slip::Point2d<Real> & center , const Real major_axis , const Real minor_axis , const Real orient , const slip::Point2d<Real> & ell_start , const slip::Point2d<Real> & ell_end , const slip::Color<unsigned char> & col )
  // {
  //   _elliptical_arcs.push_back( SVGEllipticalArc<Real>( center[0] , center[1] , major_axis , minor_axis , orient , ell_start[0] , ell_start[1] , ell_end[0] , ell_end[1] , col ) ) ;
  // }


  /**
   ** @brief Add a circular arc to the image
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param center Center of the base circle
   ** @param radius Radius of the base circle
   ** @param angle_start Angular start of the circular arc
   ** @param angle_end Angular end of the circular arc
   ** @param ell_start First point position of the circular arc
   ** @param ell_end Second point position of the circular arc
   ** @param col Color the circular arc
   **/
  template< typename Real >
  void SVGImageDescriptorType<Real>::AddCircularArc( const slip::Point2d<Real> & center , const Real radius , const Real angle_start , const Real angle_end , const slip::Point2d<Real> & ell_start , const slip::Point2d<Real> & ell_end , const slip::Color<unsigned char> & col )
  {
    _circular_arcs.push_back( SVGCircularArc<Real>( center[0] , center[1] , radius , angle_start , angle_end , ell_start[0] , ell_start[1] , ell_end[0] , ell_end[1] , col ) ) ;
  }


  /**
   ** @brief Add an ellipse to the image
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param center Center of the ellipse
   ** @param major_axis Major axis dimension
   ** @param minor_axis Minor axis dimension
   ** @param orient Orientation of the major axis (relative to the X axis)
   ** @param col Color of the ellipse
   **/
  template< typename Real>
  void SVGImageDescriptorType<Real>::AddEllipse( const slip::Point2d<Real> & center , const Real major_axis , const Real minor_axis , const Real orient , const slip::Color<unsigned char> & col )
  {
    _ellipses.push_back( SVGEllipse<Real>( center[0] , center[1] , major_axis , minor_axis , orient , col ) ) ;
  }




  /**
  ** @brief Write a list of image to a file
  ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
  ** @date 2014/06/11
  ** @version 0.0.1
  ** @param kpts Input images to export
  ** @param file_name Output file name
  ** @param binary Value is ignored by default (it is just here for signature compatibility)
  ** @return true if export is correct | false if there was an error during export
  **/
  template< typename Real>
  bool SVGImageDescriptorType<Real>::write( const std::vector< SVGImageDescriptorType > & kpts , const std::string & file_name , const bool UNUSED( binary ) )
  {
    // get extension and base_name
    const size_t _file_ext_pos = file_name.find_last_of( "." ) ;
    std::string output_ext ;
    std::string base_name ;
    if( _file_ext_pos == std::string::npos )
    {
      output_ext = ".svg" ;
      base_name = file_name ;
    }
    else
    {
      output_ext = file_name.substr( _file_ext_pos ) ;
      base_name = file_name.substr( 0 , _file_ext_pos ) ;
    }


    for( size_t i = 0 ; i < kpts.size() ; ++i )
    {
      std::stringstream str ;
      str << base_name << "_" << i << "_" << output_ext ;

      kpts[i].Write( str.str() ) ;
    }
    return true ;
  }


  /**
   ** @brief Read a list of image from a file
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/06/11
   ** @version 0.0.1
   ** @param file_name Input file_name
   ** @param kpts Output image vector
   ** @param binary Value is ignored by default (it is just here for signature compatibility)
   ** @return true if read was correct | false if there was an error during import
   **/
  template< typename Real>
  bool SVGImageDescriptorType<Real>::read( const std::string & file_name , std::vector< SVGImageDescriptorType > & kpts , const bool binary )
  {
    std::cerr << "No read allowed for the moment on SVG files" << std::endl ;
    return false ;
  }


  /**
   ** @brief Write image to a file
   ** @author Romuald Perrot <romuald.perrot_AT_univ-poitiers.fr>
   ** @date 2014/07/03
   ** @version 0.0.1
   ** @param filename name of the file to write
   **/
  template< typename Real>
  void SVGImageDescriptorType<Real>::Write( const std::string & filename ) const
  {
    std::ofstream file( filename.c_str() ) ;

    if( ! file.good() )
    {
      std::cerr << "File is not good for writing" << std::endl ;
      exit( EXIT_FAILURE ) ;
    }

    // Header
    file << "<?xml version=\"1.0\"?>" << std::endl ;
    file << "<svg xmlns=\"http://www.w3.org/2000/svg\" width =\"" << _width << "\" height=\"" << _height << "\" >" << std::endl ;

    /*
     * Background
     */
    file << "<rect x=\"0\" y=\"0\" width=\"" << _width << "\" height=\"" << _height << "\" fill=\"white\" stroke=\"none\" />" << std::endl;

    // Lines
    for( size_t i = 0 ; i < _lines.size() ; ++i )
    {
      file << "  " << _lines[i] << std::endl ;
    }

    // Circles
    for( size_t i = 0 ; i < _circles.size() ; ++i )
    {
      file << "  " << _circles[i] << std::endl ;
    }

    // Rects
    for( size_t i = 0 ; i < _rects.size() ; ++i )
    {
      file << "  " << _rects[i] << std::endl ;
    }

    // Ellipses
    for( size_t i = 0 ; i < _ellipses.size() ; ++i )
    {
      file << "  " << _ellipses[i] << std::endl ;
    }

    // // Elliptical arcs
    // for( size_t i = 0 ; i < _elliptical_arcs.size() ; ++i )
    // {
    //   file << "  " << _elliptical_arcs[i] << std::endl ;
    // }

    // Circular arcs
    for( size_t i = 0 ; i < _circular_arcs.size() ; ++i )
    {
      file << "  " << _circular_arcs[i] << std::endl ;
    }

    file << "</svg>" << std::endl;
    file.close();
  }

}//::slip

#endif// SLIP_SVG_IMAGE_DESCRIPTOR_TYPES_HPP_
