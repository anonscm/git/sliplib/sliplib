/** 
 * \file ContinuousLegendreBase2d.hpp
 * 
 * \brief Provides a class to handle global approximation of 2d container by continuous Legendre polynomials.
 * 
 */
#ifndef SLIP_CONTINUOUS_LEGENDRE_BASE2D_HPP
#define SLIP_CONTINUOUS_LEGENDRE_BASE2D_HPP

#include <iostream>
#include <string>
#include "ContinuousPolyBase2d.hpp"
#include "ContinuousLegendreBase1d.hpp"
#include "PolySupport.hpp"

namespace slip
{
template <typename T>
class ContinuousLegendreBase2d;

template<typename T>
class ContinuousLegendreBase2d: public slip::ContinuousPolyBase2d<T>
{

public:
  typedef ContinuousLegendreBase2d<T> self;
  typedef const self const_self;
  typedef typename slip::ContinuousPolyBase2d<T> base;

 /**
   ** \name Constructors & Destructors
   */
  /*@{*
    /*!
    ** \brief Constructs a ContinuousLegendreBase2d.
    */
  ContinuousLegendreBase2d():
    base()
  {
    this->allocate(slip::PolySupport<T>(),slip::PolySupport<T>());
  }


   ContinuousLegendreBase2d(const std::size_t range_size_row,
			    const std::size_t range_size_col,
			    const std::size_t degree,
			    const bool normalized,
			    const bool monic,
			    const slip::POLY_BASE_GENERATION_METHOD& method,
			    const slip::POLY_INTEGRATION_METHOD& integration_method,
			    const slip::PolySupport<T>& support_row,
			    const slip::PolySupport<T>& support_col):
     base(range_size_row,range_size_col,degree,normalized,monic,method,integration_method)
  {
    this->allocate(support_row,support_col);
  }

  ContinuousLegendreBase2d(const std::size_t range_size_row,
			   const std::size_t range_size_col,
			   const std::size_t row_degree,
			   const std::size_t col_degree,
			   const bool normalized,
			   const bool monic,
			   const slip::POLY_BASE_GENERATION_METHOD& method,
			   const slip::POLY_INTEGRATION_METHOD& integration_method,
			    const slip::PolySupport<T>& support_row,
			    const slip::PolySupport<T>& support_col):
    base(range_size_row,range_size_col,row_degree,col_degree,normalized,monic,method,integration_method)
  {
    this->allocate(support_row,support_col);
  }

   /*!
    ** \brief Copy constructor
    ** \param other %ContinuousPolyLegendre2d.
    */
  ContinuousLegendreBase2d(const self& other):
    base(other)
  {
    
    (*(this->all_1d_bases_))[0] = new slip::ContinuousLegendreBase1d<T,2>(*dynamic_cast<slip::ContinuousLegendreBase1d<T,2>*>((*(other.all_1d_bases_))[0]));
    (*(this->all_1d_bases_))[1] = new slip::ContinuousLegendreBase1d<T,2>(*dynamic_cast<slip::ContinuousLegendreBase1d<T,2>*>((*(other.all_1d_bases_))[1]));
  }


  /*!
   ** \brief Destructor 
   */
  virtual ~ContinuousLegendreBase2d()
  {
    //this->desallocate();
  }
 /*!
  ** \brief Copy constructor
  ** \param other %ContinuousLegendreBase2d.
  */
  self& operator=(const self& other)
  {
     if(this != &other)
       {
	 
	 base::operator=(other);
	 this->desallocate();
	 (*(this->all_1d_bases_))[0] = new slip::ContinuousLegendreBase1d<T,2>(*dynamic_cast<slip::ContinuousLegendreBase1d<T,2>*>((*(other.all_1d_bases_))[0]));
	 (*(this->all_1d_bases_))[1] = new slip::ContinuousLegendreBase1d<T,2>(*dynamic_cast<slip::ContinuousLegendreBase1d<T,2>*>((*(other.all_1d_bases_))[1]));
	 //this->copy_attributes(other);
       }
     return *this;
       
  }
  /*@} End Constructors */


private:
  /*!
  ** \brief Allocate the polynomial base data.
  */
  void allocate(const slip::PolySupport<T>& support_row,
		const slip::PolySupport<T>& support_col)
  {
    (*(this->all_1d_bases_))[0] =
    	  new slip::ContinuousLegendreBase1d<T,2>(static_cast<std::size_t>(2),
    						      this->row_degree_,
    						      this->normalized_,
    						      this->monic_,
    						      this->method_,
    						      this->range_size_row_,
    						      support_row,
    						      this->integration_method_);

    (*(this->all_1d_bases_))[1] =
      new slip::ContinuousLegendreBase1d<T,2>(static_cast<std::size_t>(1),
    						  this->col_degree_,
    						  this->normalized_,
    						  this->monic_,
    						  this->method_,
    						  this->range_size_col_,
    						  support_col,
    						  this->integration_method_);

   

  }
  
  
  /*!
  ** \brief Desallocate the polynomial base data.
  */
  void desallocate()
  {
  
  
  }
};

}//::slip

#endif //SLIP_CONTINUOUS_LEGENDRE_BASE2D_HPP
