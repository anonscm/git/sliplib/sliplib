/*
 * Copyright(c):
 * Signal Image and Communications (SIC) Department 
 * http://www.sic.sp2mi.univ-poitiers.fr/ 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - XLIM  Institute UMR CNRS 7252  http://www.xlim.fr/ 
 * 
 * and 
 * 
 * D2 Fluid, Thermic and Combustion 
 *  - University of Poitiers, France http://www.univ-poitiers.fr 
 *  - PPRIME Institute -  UPR CNRS 3346  http://www.pprime.fr
 *  - ISAE-ENSMA http://www.ensma.fr
 *
 * Contributor(s):
 * The SLIP team,
 * Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>,
 * Laurent David <laurent.david_AT_lea.univ-poitiers.fr>, 
 * Ludovic Chatellier <ludovic.chatellier_AT_univ-poitiers.fr>, 
 * Lionel Thomas <lionel.thomas_AT_univ-poitiers.fr>, 
 * Denis Arrivault <arrivault_AT_sic.univ-poitiers.fr>, 
 * Julien Dombre <julien.dombre_AT_univ-poitiers.fr>.
 *
 * Description:
 * The Simple Library of Image Processing (SLIP) is a new image processing 
 * library. It is written in the C++ language following as much as possible 
 * the ISO/ANSI C++ standard. It is consequently compatible with any system  
 * satisfying the ANSI C++ complience. It works on different Unix , Linux ,  
 * Mircrosoft Windows and Mac OS X plateforms. SLIP is a research library that  
 * was created by the Signal, Image and Communications (SIC) departement of  
 * the XLIM, UMR 7252 CNRS Institute in collaboration with the Fluids, Thermic  
 * and Combustion departement of the P', UPR 3346 CNRS Institute of the  
 * University of Poitiers.
 * 
 * The SLIP Library source code has been registered to the APP (French Agency 
 * for the Protection of Programs) by the University of Poitiers and CNRS, 
 * under  registration number IDDN.FR.001.300034.000.S.P.2010.000.21000.

 * http://www.sic.sp2mi.univ-poitiers.fr/slip/
 *
 * This software is governed by the CeCILL-C license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL-C 
 * license as circulated by CEA, CNRS and INRIA at the following URL 
 * http://www.cecill.info.
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */


/** 
 * \file PinholeDLTCamera.hpp
 * 
 * \brief Provides a Pinhole Direct Linear Transformation (DLT) camera model class.
 * 
 */

#ifndef SLIP_PINHOLEDLTCAMERA_HPP
#define SLIP_PINHOLEDLTCAMERA_HPP

#include <iostream>
#include <cassert>
#include "Point2d.hpp"
#include "Point3d.hpp"
#include "Matrix.hpp"
#include "io_tools.hpp"
#include "linear_algebra.hpp"

#include "CameraModel.hpp"
#include "camera_algo.hpp"
#include "PinholeCamera.hpp"

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/base_object.hpp>

namespace slip 
{
  template <typename Type>
  class PinholeDLTCamera;

  template <typename Type>
  std::ostream& operator<<(std::ostream & out, const PinholeDLTCamera<Type>& c);

/*! \class PinholeDLTCamera
 **  \ingroup Containers Camera
** \author Gomit Guilaume <guillaume.gomit_AT_univ-poitiers.fr>
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2014/04/02
** \since 1.2.0
** \brief Define a %PinholeDLTCamera class
** \param Type Type of the coordinates of the PinholeDLTCamera
** \remarks Abdel-Aziz, Y. I. & Karara, H. M., Direct linear transformation into object space coordinates in   close-range photogrammetry. Proc. Symposium on Close-Range Photo-grammetry. p.1-18 (1971)
*/
	template <typename Type>
	class PinholeDLTCamera: public PinholeCamera<Type> 
	{
	  typedef PinholeDLTCamera<Type> self;
	  typedef PinholeCamera<Type> base;

		public:
 		/**
 		** \name Constructors & Destructors
 		*/
 		/*@{*/

 		/*!
  		** \brief Default constructor of PinholeDLTCamera
  		*/
	  PinholeDLTCamera():base()
	  {
	  }
	  /*!
	  ** \brief Constructs a copy of the PinholeDLTCamera \a rhs
	  ** \param rhs an other PinholeDLTCamera
	  */
	  PinholeDLTCamera(const self& rhs):base(rhs)
	  {}
	  
	  /**
	   ** \name  Assignment operators and methods
	   */
	  /*@{*/
	  /*!
	  ** \brief Assign a PinholeDLTCamera.
	  **
	  ** Assign elements of PinholeDLTCamera in \a rhs
	  **
	  ** \param rhs PinholeDLTCamera to get the values from.
	  ** \return self.
	  */
	  self& operator=(const self & rhs)
	  {
	    if(this != &rhs)
	      {
		this->base::operator=(rhs);
		
	      }
	    return *this;
	  }
	  
	  /*!
	  ** \brief Clone the %PinholeDLTCamera
	  ** \return new pointer towards a copy of this %PinholeCamera
	  ** \remark must be detroyed after use
	  */
	  virtual base* clone () const
	  {
	    return (new self(*this));
	  }   
	  /*@} End Assignment operators and methods*/


	  
	  /**
	   ** \name  Computation methods
	   */
	  /*@{*/
	  /*!
	  ** \brief Computes the parameters of the pinhole camera model
	  ** \param P Matrix containing the input data.
	  ** \param s Structure containing different parameter settings.
	  ** \remarks Format:
	  ** \f$(x,y)\f$ are the image coordinates, \f$(X,Y,Z)\f$ are the world coordinates.
	  ** \f[
	  ** \begin{array}{ccccc}
	  ** \cdots & \cdots & \cdots & \cdots & \cdots \\
	  ** x_{i} & y_{i} & X_{i} & Y_{i} & Z_{i}\\
	  ** \cdots & \cdots & \cdots & \cdots & \cdots\\
	  ** \end{array}
	  ** \f]
	  */
	  void compute(const slip::Matrix<Type>& P) 
	  {
	    getpars_DLT_norm(P,*base::calibration_matrix_);
	    this->update_d_calibration_matrix();
	    this->update_origin();
	  }
	  
private:
    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    { 
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::PinholeCamera<Type> >(*this);
	}
    }
  template<class Archive>
    void load(Archive & ar, const unsigned int version)
    { 
      if(version >= 0)
	{
	  ar & boost::serialization::base_object<slip::PinholeCamera<Type> >(*this);
	}
    }
   BOOST_SERIALIZATION_SPLIT_MEMBER()

	};

}//slip::

namespace slip 
{
  template<typename Type>
  std::ostream& operator<<(std::ostream & out, const slip::PinholeDLTCamera<Type>& c) 
  {
    out<<*c.calibration_matrix_<<std::endl;
    return out;
  }
}//slip::
#endif //SLIP_PINHOLEDLTCAMERA_HPP
