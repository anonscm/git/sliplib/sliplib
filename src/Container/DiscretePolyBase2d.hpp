/** 
 * \file DiscretePolyBase2d.hpp
 * 
 * \brief Provides a class to handle global approximation of 2d container by orthogonal polynomials base.
 * 
 */

#ifndef SLIP_DISCRETEPOLYBASE2D_HPP
#define SLIP_DISCRETEPOLYBASE2D_HPP

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Polynomial.hpp"
#include "Array.hpp"
#include "Array2d.hpp"
#include "DPoint2d.hpp"
#include "arithmetic_op.hpp"
#include "linear_algebra.hpp"
#include "macros.hpp"
#include "Block.hpp"
#include "polynomial_algo.hpp"
#include "poly_weight_functors.hpp"
#include "DiscretePolyBase1d.hpp"




namespace slip
{
  template<typename RandomAccessIterator1,
	   typename RandomAccessIterator2,
	   typename T> 
  struct discrete_poly2d_inner_prod : 
    public std::binary_function<std::pair<RandomAccessIterator1,RandomAccessIterator1>, 
				std::pair<RandomAccessIterator2,RandomAccessIterator2>,T>
  {
    template< typename RandomAccessIterator3, typename RandomAccessIterator4>
    discrete_poly2d_inner_prod(RandomAccessIterator3 row_weight_first,
			       RandomAccessIterator3 row_weight_last,
			       RandomAccessIterator4 col_weight_first,
			       RandomAccessIterator4 col_weight_last):
      weight_(new slip::Array2d<T>(static_cast<std::size_t>(row_weight_last-row_weight_first),
				   static_cast<std::size_t>(col_weight_last-col_weight_first)))
    {
      slip::outer_product(row_weight_first, row_weight_last,
			  col_weight_first, col_weight_last,
			  this->weight_->upper_left(),this->weight_->bottom_right());
    }

    //for sparse base
    template< typename RandomAccessIterator3, 
	      typename RandomAccessIterator4,
	      typename RandomAccessIterator2d>
    discrete_poly2d_inner_prod(RandomAccessIterator3 row_weight_first,
			       RandomAccessIterator3 row_weight_last,
			       RandomAccessIterator4 col_weight_first,
			       RandomAccessIterator4 col_weight_last,
			       RandomAccessIterator2d mask_upper_left,
			       RandomAccessIterator2d mask_bottom_right,
			       typename std::iterator_traits<RandomAccessIterator2d>::value_type true_value = typename std::iterator_traits<RandomAccessIterator2d>::value_type(1)):
      weight_(new slip::Array2d<T>(static_cast<std::size_t>(row_weight_last-row_weight_first),
				   static_cast<std::size_t>(col_weight_last-col_weight_first)))
    {
      typedef typename std::iterator_traits<RandomAccessIterator2d>::value_type mask_value_type;
      slip::outer_product(row_weight_first, row_weight_last,
			  col_weight_first, col_weight_last,
			  this->weight_->upper_left(),this->weight_->bottom_right());
      std::replace_if(this->weight_->begin(),
		      this->weight_->end(), 
		      std::bind2nd(std::not_equal_to<mask_value_type>(), true_value),	      T());
    }
    T operator() (const std::pair<RandomAccessIterator1,RandomAccessIterator1>& Pi,
		  const std::pair<RandomAccessIterator2,RandomAccessIterator2>& Pj)
  {



    T result = T();
    typename slip::Array2d<T>::const_iterator it_weight = this->weight_->begin();
    RandomAccessIterator1 it_pi = Pi.first;
    RandomAccessIterator2 it_pj = Pj.first;
    for (; it_pi != Pi.second; 
	   ++it_pi, 
	   ++it_pj,
	   ++it_weight)
      {
	result = result + *it_weight * (*it_pi * *it_pj);
      }
    return result;

  }
		  
    ~discrete_poly2d_inner_prod()
    {
      if(this->weight_ != 0)
	{
	  delete this->weight_;
	}
    }

    slip::Array2d<T>* weight_;

 };


}


namespace slip
{

/*! \class DiscretePolyBase2d
** 
** \author Benoit Tremblais <tremblais_AT_sic.univ-poitiers.fr>
** \version 0.0.2
** \date 2011/08/26
** \brief a class to handle global approximation of 2d container by orthogonal polynomials base
** \param T Type of data to approximate.
** \param PolyWeightRow Weight functor for the row polynomial.
** \param PolyCollocationRow Collocation functor for the row polynomial.
** \param PolyWeightCol Weight functor for the column polynomial.
** \param PolyCollocationCol Collocation functor for the column polynomial.
*/
  template <typename T, typename PolyWeightRow, typename PolyCollocationRow,
                        typename PolyWeightCol, typename PolyCollocationCol>
  class DiscretePolyBase2d 
{
  
public:

 

  typedef DiscretePolyBase2d<T, PolyWeightRow, PolyCollocationRow,
			        PolyWeightCol, PolyCollocationCol> self;
  typedef const self const_self;

  

   /**
   ** \name Constructors & Destructors
   */
  /*@{*
    /*!
    ** \brief Constructs a DiscretePolyBase2d.
    */
  DiscretePolyBase2d():
    rows_(0),
    cols_(0),
    size_(0),
    degree_(-1),
    row_degree_(-1),
    col_degree_(-1),
    row_base_(new slip::DiscretePolyBase1d<T,PolyWeightRow,PolyCollocationRow>()),
    col_base_(new slip::DiscretePolyBase1d<T,PolyWeightCol,PolyCollocationCol>()),
    poly_evaluations_(0),
    poly_w_evaluations_(0),
    pkpk_(0),
    powers_(0),
    normalized_(false)
  {
  }
 
  /*!
    ** \brief Constructs a DiscretePolyBase2d.
    ** \param rows Number of rows of the 2d container.
    ** \param cols Number of cols of the 2d container.
    ** \param row_degree Degree of the row polynomial.
    ** \param poly_weight_row_fun Weight functor for the row polynomial.
    ** \param poly_collocation_row_fun Collocation functor for the row polynomial.
    ** \param col_degree Degree of the column polynomial.
    ** \param poly_weight_col_fun Weight functor for the column polynomial.
    ** \param poly_collocation_col_fun Collocation functor for the column polynomial.
    ** \param normalized Boolean indicated if the base is normalized or not.
    */
  DiscretePolyBase2d(const std::size_t rows,
		     const std::size_t cols,
		     const int row_degree,
		     const PolyWeightRow& poly_weight_row_fun,
		     const PolyCollocationRow& poly_collocation_row_fun,
		     const int col_degree,
		     const PolyWeightCol& poly_weight_col_fun,
		     const PolyCollocationCol& poly_collocation_col_fun,
		     
		     const bool normalized):
    rows_(rows),
    cols_(cols),
    size_((row_degree+1)*(col_degree + 1)),
    degree_(row_degree + col_degree),
    row_degree_(row_degree),
    col_degree_(col_degree),
    row_base_(new slip::DiscretePolyBase1d<T,PolyWeightRow,PolyCollocationRow>(rows,row_degree,poly_weight_row_fun,poly_collocation_row_fun,normalized)),
    col_base_(new slip::DiscretePolyBase1d<T,PolyWeightCol,PolyCollocationCol>(cols,col_degree,poly_weight_col_fun,poly_collocation_col_fun,normalized)),
    poly_evaluations_(new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >()),
    poly_w_evaluations_(new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >()),
    pkpk_(new slip::Array<T>(this->size_)),
    powers_(new slip::Array2d<std::size_t>(this->size_,2)),
    normalized_(normalized)
  {
  }

   /*!
    ** \brief Constructs a DiscretePolyBase2d.
    ** \param rows Number of rows of the 2d container.
    ** \param cols Number of cols of the 2d container.
    ** \param degree Degree of the row and column polynomials.
    ** \param poly_weight_row_fun Weight functor for the row polynomial.
    ** \param poly_collocation_row_fun Collocation functor for the row polynomial.
    ** \param poly_weight_col_fun Weight functor for the column polynomial.
    ** \param poly_collocation_col_fun Collocation functor for the column polynomial.
    ** \param normalized Boolean indicated if the base is normalized or not.
  
    
    */
  DiscretePolyBase2d(const std::size_t rows,
		     const std::size_t cols,
		     const int degree,
		     const PolyWeightRow& poly_weight_row_fun,
		     const PolyCollocationRow& poly_collocation_row_fun,
		     const PolyWeightCol& poly_weight_col_fun,
		     const PolyCollocationCol& poly_collocation_col_fun,
		     const bool normalized):
    rows_(rows),
    cols_(cols),
    size_((degree+1)*(degree + 2)/2),
    degree_(degree),
    row_degree_(degree),
    col_degree_(degree),
    row_base_(new slip::DiscretePolyBase1d<T,PolyWeightRow,PolyCollocationRow>(rows,degree,poly_weight_row_fun,poly_collocation_row_fun,normalized)),
    col_base_(new slip::DiscretePolyBase1d<T,PolyWeightCol,PolyCollocationCol>(cols,degree,poly_weight_col_fun,poly_collocation_col_fun,normalized)),
     poly_evaluations_(new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >()),
    poly_w_evaluations_(new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >()),
    pkpk_(new slip::Array<T>(this->size_)),
    powers_(new slip::Array2d<std::size_t>(this->size_,2)),
    normalized_(normalized)
  {
  }

   /*!
    ** \brief Constructs a DiscretePolyBase2d.
    ** \param rows Number of rows of the 2d container.
    ** \param cols Number of cols of the 2d container.
    ** \param degree Degree of the row and column polynomials.
    ** \param poly_weight_fun Weight functor for the polynomial (the same weight functor is used for both row and column).
    ** \param poly_collocation_fun Collocation functor for the polynomial (the same collocation functor is used for both row and column).
    ** \param normalized Boolean indicated if the base is normalized or not.

    */
  DiscretePolyBase2d(const std::size_t rows,
		     const std::size_t cols,
		     const int degree,
		     const PolyWeightRow& poly_weight_fun,
		     const PolyCollocationRow& poly_collocation_fun,
		     const bool normalized):
    rows_(rows),
    cols_(cols),
    size_((degree+1)*(degree + 2)/2),
    degree_(degree),
    row_degree_(degree),
    col_degree_(degree),
    row_base_(new slip::DiscretePolyBase1d<T,PolyWeightRow,PolyCollocationRow>(rows,degree,poly_weight_fun,poly_collocation_fun,normalized)),
    col_base_(0),
     poly_evaluations_(new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >()),
    poly_w_evaluations_(new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >()),
    pkpk_(new slip::Array<T>(this->size_)),
    powers_(new slip::Array2d<std::size_t>(this->size_,2)),
    normalized_(normalized)
  {
  }

   void init_canonical(const std::size_t rows,
		       const std::size_t cols,
		       const int row_degree,
		       const PolyWeightRow& poly_weight_row_fun,
		       const PolyCollocationRow& poly_collocation_row_fun,
		       const int col_degree,
		       const PolyWeightCol& poly_weight_col_fun,
		       const PolyCollocationCol& poly_collocation_col_fun)

  {
    //initializations and allocations
    this->rows_ = rows;
    this->cols_ = cols;
    this->size_ = (row_degree+1)*(col_degree + 1);
    this->degree_  = row_degree + col_degree;
    this->row_degree_ = row_degree;
    this->col_degree_ = col_degree;
    this->row_base_->init_canonical(rows,
				    row_degree,
				    poly_weight_row_fun,
				    poly_collocation_row_fun);
    this->col_base_->init_canonical(cols,
				    col_degree,
				    poly_weight_col_fun,
				    poly_collocation_col_fun);
    
    this->poly_evaluations_   = new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >();
    this->poly_w_evaluations_ = new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >();
    this->pkpk_ = new slip::Array<T>(this->size_);
    this->powers_  = new slip::Array2d<std::size_t>(this->size_,2);
    this->normalized_ = false;

    this->compute_polynomials();

    //pkpk_ 
    this->update_pkpk();
  }

  //sparse version
  template <typename RandomAccessIterator2d>
 void init_canonical(const std::size_t rows,
		     const std::size_t cols,
		     const int row_degree,
		     const PolyWeightRow& poly_weight_row_fun,
		     const PolyCollocationRow& poly_collocation_row_fun,
		     const int col_degree,
		     const PolyWeightCol& poly_weight_col_fun,
		     const PolyCollocationCol& poly_collocation_col_fun,
		     RandomAccessIterator2d mask_upper_left,
		     RandomAccessIterator2d mask_bottom_right,
		     typename std::iterator_traits<RandomAccessIterator2d>::value_type true_value =  typename std::iterator_traits<RandomAccessIterator2d>::value_type(1))

  {
    //initializations and allocations
    this->rows_ = rows;
    this->cols_ = cols;
    this->size_ = (row_degree+1)*(col_degree + 1);
    this->degree_  = row_degree + col_degree;
    this->row_degree_ = row_degree;
    this->col_degree_ = col_degree;
    this->row_base_->init_canonical(rows,
				    row_degree,
				    poly_weight_row_fun,
				    poly_collocation_row_fun);
    this->col_base_->init_canonical(cols,
				    col_degree,
				    poly_weight_col_fun,
				    poly_collocation_col_fun);
    
    this->poly_evaluations_   = new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >();
    this->poly_w_evaluations_ = new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >();
    this->pkpk_ = new slip::Array<T>(this->size_);
    this->powers_  = new slip::Array2d<std::size_t>(this->size_,2);
    this->normalized_ = false;

    this->compute_polynomials(mask_upper_left, 
			      mask_bottom_right,
			      true_value);

    //pkpk_ 
    this->update_pkpk();
  }

 /*!
  ** \brief Copy constructor.
  ** \param other %DiscretePolyBase2d.
  */
  DiscretePolyBase2d(const DiscretePolyBase2d& other):
    rows_(other.rows_),
    cols_(other.cols_),
    size_(other.size_),
    degree_(other.degree_),
    row_degree_(other.row_degree_),
    col_degree_(other.col_degree_),
    row_base_(new slip::DiscretePolyBase1d<T,PolyWeightRow,PolyCollocationRow>(*other.row_base_)),
    col_base_(new slip::DiscretePolyBase1d<T,PolyWeightCol,PolyCollocationCol>(*other.col_base_)),
    poly_evaluations_(new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >(*other.poly_evaluations_)),
    poly_w_evaluations_(new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >(*other.poly_w_evaluations_)),
    pkpk_(new slip::Array<T>(*other.pkpk_)),
    powers_(new slip::Array2d<std::size_t>(*other.powers_)),
    normalized_(other.normalized_)
  {
    
  }
  /*!
  ** \brief Destructor of the Legendre
  */
  virtual ~DiscretePolyBase2d()
  {
    this->desallocate();
   
  }
  
/*@} End Constructors */
  

  /*!
  ** \brief Assignement operator
  ** \param other %DiscretePolyBase2d.
  */
  self& 
  operator=(const self& other)
  {
     if(this == &other)
       {
	 return *this;
       }
     this->desallocate();
     this->rows_ = other.rows_;
     this->cols_ = other.cols_;
     this->size_ = other.size_;
     this->degree_ = other.degree_;
     this->row_degree_ = other.row_degree_;
     this->col_degree_ = other.col_degree_;
     this->row_base_ = new slip::DiscretePolyBase1d<T,PolyWeightRow,PolyCollocationRow>(*other.row_base_);
     this->col_base_ = new slip::DiscretePolyBase1d<T,PolyWeightCol,PolyCollocationCol>(*other.col_base_);
     this->poly_evaluations_ = new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >(*other.poly_evaluations_);
     this->poly_w_evaluations_ = new std::map<slip::block<std::size_t,2>, slip::Array2d<T> >(*other.poly_w_evaluations_);
     this->powers_ = new slip::Array2d<std::size_t>(*other.powers_);
     this->pkpk_ = (new slip::Array<T>(*other.pkpk_)),
     this->normalized_ = other.normalized_;
  
     return *this;
  }

  // template<typename RandomAccessIterator>
//   T evaluate(const T& x,
//   	     const T& y,
//   	     RandomAccessIterator coef_first,
//   	     RandomAccessIterator coef_last) const
//   {
//     T p_at_x = this->base1_.evaluate(x,
//   }


  //  template<typename RandomAccessIterator1,
// 	    typename RandomAccessIterator2,
// 	    typename RandomAccessIterator3,
// 	    typename RandomAccessIterator4>
//   void evaluate(RandomAccessIterator1 x_first, 
// 		RandomAccessIterator1 x_last,
// 		RandomAccessIterator2 y_first,
// 		RandomAccessIterator2 y_last,
// 		RandomAccessIterator3 coef_first,
// 		RandomAccessIterator3 coef_last,
// 		RandomAccessIterator4 f_first,
// 		RandomAccessIterator4 f_last) const
//   {
//   }


 // template<typename RandomAccessIterator>
//   T derivative(const T& x,
//   	     const T& y,
//   	     RandomAccessIterator coef_first,
//   	     RandomAccessIterator coef_last) const
//   {
//     T p_at_x = this->base1_.evaluate(x,
//   }

  void gram_schmidt()
  {
   
    typedef typename slip::Array2d<T>::iterator Iterator;
    std::vector<std::pair<Iterator,Iterator> > base(this->size_);
  for(std::size_t i = 0; i < this->size_; ++i)
    {
      slip::block<std::size_t, 2> ind_i;
      ind_i[0] = (*(this->powers_))[i][0];
      ind_i[1] = (*(this->powers_))[i][1];
      typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it  = 
      (this->poly_evaluations_)->find(ind_i);
      base[i].first  = (it->second).begin();
      base[i].second = (it->second).end();
    }
 
  //orthogonalize
  this->modified_gram_schmidt(base);


  //update poly_w_evaluations_
  this->update_poly_w_evaluations();
   //update_pkpk
  this->update_pkpk();



  }

  //sparse version
  template <typename RandomAccessIterator2d>
   void gram_schmidt(RandomAccessIterator2d mask_upper_left,
		     RandomAccessIterator2d mask_bottom_right,
		     typename std::iterator_traits<RandomAccessIterator2d>::value_type true_value =  typename std::iterator_traits<RandomAccessIterator2d>::value_type(1))
  {
   
    typedef typename slip::Array2d<T>::iterator Iterator;
    std::vector<std::pair<Iterator,Iterator> > base(this->size_);
  for(std::size_t i = 0; i < this->size_; ++i)
    {
      slip::block<std::size_t, 2> ind_i;
      ind_i[0] = (*(this->powers_))[i][0];
      ind_i[1] = (*(this->powers_))[i][1];
      typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it  = 
      (this->poly_evaluations_)->find(ind_i);
      base[i].first  = (it->second).begin();
      base[i].second = (it->second).end();
    }
 
  //orthogonalize
  this->modified_gram_schmidt(base,
			      mask_upper_left,
			      mask_bottom_right,
			      true_value);


  //update poly_w_evaluations_
  this->update_poly_w_evaluations(mask_upper_left,
				  mask_bottom_right,
				  true_value);
   //update_pkpk
  this->update_pkpk();



  }
  
  void gram_schmidt_normalized()
  {
      typedef typename slip::Array2d<T>::iterator Iterator;
    std::vector<std::pair<Iterator,Iterator> > base(this->size_);
  for(std::size_t i = 0; i < this->size_; ++i)
    {
      slip::block<std::size_t, 2> ind_i;
      ind_i[0] = (*(this->powers_))[i][0];
      ind_i[1] = (*(this->powers_))[i][1];
      typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it  = 
      (this->poly_evaluations_)->find(ind_i);
      base[i].first  = (it->second).begin();
      base[i].second = (it->second).end();
    }
 
  this->modified_gram_schmidt_normalized(base);
  this->normalized_ = true;

   //update poly_w_evaluations_
  this->update_poly_w_evaluations();
   //update_pkpk
  this->update_pkpk();

  }


  //sparse version
   template <typename RandomAccessIterator2d>
    void gram_schmidt_normalized(RandomAccessIterator2d mask_upper_left,
		     RandomAccessIterator2d mask_bottom_right,
		     typename std::iterator_traits<RandomAccessIterator2d>::value_type true_value =  typename std::iterator_traits<RandomAccessIterator2d>::value_type(1))
  {
      typedef typename slip::Array2d<T>::iterator Iterator;
    std::vector<std::pair<Iterator,Iterator> > base(this->size_);
  for(std::size_t i = 0; i < this->size_; ++i)
    {
      slip::block<std::size_t, 2> ind_i;
      ind_i[0] = (*(this->powers_))[i][0];
      ind_i[1] = (*(this->powers_))[i][1];
      typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it  = 
      (this->poly_evaluations_)->find(ind_i);
      base[i].first  = (it->second).begin();
      base[i].second = (it->second).end();
    }
 
  this->modified_gram_schmidt_normalized(base,
			      mask_upper_left,
			      mask_bottom_right,
			      true_value);
  this->normalized_ = true;

   //update poly_w_evaluations_
  this->update_poly_w_evaluations(mask_upper_left,
				  mask_bottom_right,
				  true_value);
   //update_pkpk
  this->update_pkpk();

  }


  /*!
  ** \brief Generate the polynomial base.
  */
  void generate()
  {
    if(this->row_base_ != 0)
      {
	(this->row_base_)->generate();
      }
    if(this->col_base_ != 0)
      {
	(this->col_base_)->generate();
      }
    if(this->row_base_ != 0)
      {
	this->compute_polynomials();
      }
    
  }


 /*!
  ** \brief Generate the sparse polynomial base according to the mask
  ** \param mask_upper_left RandomAccessIterator2d to the mask where compute the polynomial basis.
  ** \param mask_bottom_right RandomAccessIterator2d to the mask where compute the polynomial basis
  ** \param true_value value where weight of the polybomial inner_product won't be put to 0 
  */
  template <typename RandomAccessIterator2d>
  void generate(RandomAccessIterator2d mask_upper_left,
		RandomAccessIterator2d mask_bottom_right,
		typename std::iterator_traits<RandomAccessIterator2d>::value_type true_value =  typename std::iterator_traits<RandomAccessIterator2d>::value_type(1))
  {
    if(this->row_base_ != 0)
      {
	(this->row_base_)->generate();
      }
    if(this->col_base_ != 0)
      {
	(this->col_base_)->generate();
      }
    if(this->row_base_ != 0)
      {
	this->compute_polynomials(mask_upper_left,mask_bottom_right,true_value);
      }
    
  }

  /*!
  ** \brief Project data onto the P(i,j) polynomial of the base.
  ** \param up RandomAccessIterator2d on the data.
  ** \param bot RandomAccessIterator2d on the data.
  ** \param i index of the row polynomial.
  ** \param j index of the column polynomial.
  */
  template <typename RandomAccessIterator2d>
  T project(RandomAccessIterator2d up,
	    RandomAccessIterator2d bot,
	    const std::size_t i,
	    const std::size_t j)
  {
    slip::block<std::size_t,2> pij;
    pij[0] = i;
    pij[1] = j;
    typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it_w  = (this->poly_w_evaluations_)->find(pij); 

    assert(it_w != (this->poly_w_evaluations_)->end());

    T result = std::inner_product((it_w->second).begin(),
				  (it_w->second).end(),
				  up,
				  T());
     if(!this->normalized_)
      {
	if(   (this->degree_ == this->row_degree_) 
	   && (this->degree_ == this->col_degree_) )
	  {
	    //step within the slice 
	    //step = i*((2D+3) - i)/2 = (D+1)(D+2)/2 - (D+1-i)(D+2-i)/2
	    //	    std::size_t	step = ((2*(this->col_degree_ + 1) - (i-1)) * i)/2;
	    std::size_t	step = ((this->degree_ + this->degree_ + 3 - i) * i)/2;
	    result /= (*this->pkpk_)[step + j];
	  }
	else
	  {
	    std::size_t k = i*(this->col_degree_ + 1) + j;
	    result /= (*this->pkpk_)[k];
	  }
      }
     return result;
  }


  /*!
  ** \brief Project data onto the P(i,j) polynomial of the base.
  ** \param up RandomAccessIterator2d on the data.
  ** \param bot RandomAccessIterator2d on the data.
  ** \param row_degree_max maximal degree of projection onto the rows.
  ** \param col_degree_max maximal degree of projection onto the columns.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \pre (coef_last - coef_first) <= this->size()
  */
  template <typename RandomAccessIterator2d,
	    typename RandomAccessIterator>
  void project(RandomAccessIterator2d up,
	       RandomAccessIterator2d bot,
	       const std::size_t row_degree_max,
	       const std::size_t col_degree_max,
	       RandomAccessIterator coef_first,
	       RandomAccessIterator coef_last)
  {
    assert(static_cast<std::size_t>(coef_last - coef_first) <= this->size_);
   
    if(this->normalized_)
      {
		typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it_w  = 
	  (this->poly_w_evaluations_)->begin();
	for(;it_w!=(this->poly_w_evaluations_)->end();++it_w)
	  {
	    if( ((it_w->first)[0] <= row_degree_max)  && 
	        ((it_w->first)[1] <= col_degree_max) )
                  {
		 *coef_first++ = std::inner_product((it_w->second).begin(),(it_w->second).end(),up,T());
		    
		    
		  }
	  }

      }
    else
      {
	typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it_w  = 
	  (this->poly_w_evaluations_)->begin();
	std::size_t kk = 0;
	for(;it_w!=(this->poly_w_evaluations_)->end();++it_w)
	  {
	    if( ((it_w->first)[0] <= row_degree_max)  && 
	        ((it_w->first)[1] <= col_degree_max) )
                  {
		
		    *coef_first++ = std::inner_product((it_w->second).begin(),(it_w->second).end(),up,T()) / (*this->pkpk_)[kk];
		    ++kk;
		    
		  }
	  }

      }
    
    
  }
 
 /*!
  ** \brief Project data onto the P(i,j) polynomial of the base.
  ** \param up RandomAccessIterator2d on the data.
  ** \param bot RandomAccessIterator2d on the data.
  ** \param degree_max maximal degree of projection.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \pre (coef_last - coef_first) <= this->size()
  ** \todo VERIFY
  */
  template <typename RandomAccessIterator2d,
	    typename RandomAccessIterator>
  void project(RandomAccessIterator2d up,
	       RandomAccessIterator2d bot,
	       const std::size_t degree_max,
	       RandomAccessIterator coef_first,
	       RandomAccessIterator coef_last)
  {
    assert(static_cast<std::size_t>(coef_last - coef_first) <= this->size_);
   
    if(this->normalized_)
      {
		typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it_w  = 
	  (this->poly_w_evaluations_)->begin();
	for(;it_w!=(this->poly_w_evaluations_)->end();++it_w)
	  {
	    if(((it_w->first)[0] + (it_w->first)[1]) <= degree_max)
                  {
		 *coef_first++ = std::inner_product((it_w->second).begin(),(it_w->second).end(),up,T());
		    
		    
		  }
	  }

      }
    else
      {
	typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it_w  = 
	  (this->poly_w_evaluations_)->begin();
	std::size_t kk = 0;
	for(;it_w!=(this->poly_w_evaluations_)->end();++it_w)
	  {
	    if(((it_w->first)[0] + (it_w->first)[1]) <= degree_max)
                  {
		
		    *coef_first++ = std::inner_product((it_w->second).begin(),(it_w->second).end(),up,T()) / (*this->pkpk_)[kk];
		    ++kk;
		    
		  }
	  }

      }
    
    
  }

  /*!
  ** \brief Reconstruct the data from a coefficients range.
  ** \param coef_first RandomAccessIterator to the projected coefficients.
  ** \param coef_last RandomAccessIterator to the projected coefficients.
  ** \param K number of coefficients to use for recontruct the data.
  ** \param up RandomAccessIterator2d on the data.
  ** \param bot RandomAccessIterator2d on the data.
  ** \pre K <= (coef_last - coef_first)
  */
  template <typename RandomAccessIterator,
	     typename RandomAccessIterator2d>
  void reconstruct(RandomAccessIterator coef_first,
		   RandomAccessIterator coef_last,
		   const std::size_t K,
		   RandomAccessIterator2d up,
		   RandomAccessIterator2d bot)
  {
    assert(K <= static_cast<std::size_t>(coef_last-coef_first)); 
    
    slip::DPoint2d<int> d;
    slip::block<std::size_t,2> pij;
    for(std::size_t i = 0; i < this->rows_; ++i)
      {
	d[0] = i;
	for(std::size_t j = 0; j < this->cols_; ++j)
	  {
	    d[1] = j;
	    T s = T();
	    RandomAccessIterator it_coeff = coef_first;
	    for(std::size_t k = 0; k < K; ++k, ++it_coeff)
	      {
		pij[0] = (*(this->powers_))[k][0];
		pij[1] = (*(this->powers_))[k][1];
		typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it  = 
		  (this->poly_evaluations_)->find(pij); 
		s += *it_coeff * (it->second)[i][j];
	      }
	    up[d] = s;
	  }
      }
  
  }

  //sparse version
template <typename RandomAccessIterator,
	  typename RandomAccessIterator2d1,
	  typename RandomAccessIterator2d2>
  void reconstruct(RandomAccessIterator coef_first,
		   RandomAccessIterator coef_last,
		   const std::size_t K,
		   RandomAccessIterator2d1 mask_up,
		   RandomAccessIterator2d1 mask_bot,
		   typename std::iterator_traits<RandomAccessIterator2d1>::value_type true_value,
		   RandomAccessIterator2d2 up,
		   RandomAccessIterator2d2 bot)
  {
    assert(K <= static_cast<std::size_t>(coef_last-coef_first)); 
    
    slip::DPoint2d<int> d;
    slip::block<std::size_t,2> pij;
    for(std::size_t i = 0; i < this->rows_; ++i)
      {
	d[0] = i;
	for(std::size_t j = 0; j < this->cols_; ++j)
	  {
	    d[1] = j;
	    T s = T();
	    RandomAccessIterator it_coeff = coef_first;
	    for(std::size_t k = 0; k < K; ++k, ++it_coeff)
	      {
		pij[0] = (*(this->powers_))[k][0];
		pij[1] = (*(this->powers_))[k][1];
		typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it  = 
		  (this->poly_evaluations_)->find(pij); 
		if(mask_up[d] == true_value)
		  {
		    s += *it_coeff * (it->second)[i][j];
		  }
		else
		  {
		    s += static_cast<T>(0.0);
		  }
	      }
	    up[d] = s;
	  }
      }
  
  }

 /*!
  ** \brief Returns the name of the class 
  **       
  */
  std::string name() const
  {
    return "DiscretePolyBase2d";
  }
  
  /*!
  ** \brief Returns the degree of the Base
  **
  */
  int degree() const
  {
    return this->degree_;
  }

  /*!
  ** \brief Returns the order of the Base
  **
  ** \remarks Same value as degree
  */
  int order() const
  {
    return this->degree_;
  }

 /*!
  ** \brief Returns the size of the base.
  **
  */
  std::size_t size() const
  {
    return this->size_;
  }

/*!
  ** \brief Returns the row number of the range
  **
  */
  std::size_t rows() const
  {
    return this->rows_;
  }

  /*!
  ** \brief Returns the column number of the range
  **
  */
  std::size_t cols() const
  {
    return this->cols_;
  }
 
  /*!
  ** \brief Returns the column number of the range
  **
  */
  std::size_t columns() const
  {
    return this->cols_;
  }

  /*!
  ** \brief Returns true if the base is normalized, 
  ** else returns false
  **
  */
  bool is_normalized() const
  {
    return this->normalized_;
  }

  /*!
  ** \brief Print polynomial basis features.
  **
  */
  void print()
  {
    std::cout<<"Rows = "<<this->rows_<<std::endl;
    std::cout<<"Columns = "<<this->cols_<<std::endl;
    std::cout<<"Base degree = "<<this->degree_<<std::endl;
    std::cout<<"Number of polynomials = "<<this->size_<<std::endl;
    std::cout<<"normalized = "<<this->normalized_<<std::endl;
    
    // std::cout<<"Row base properties:\n";
    // if(this->row_base_ != 0)
    //   {
    // 	(this->row_base_)->print();
    //   }
    // std::cout<<"Column base properties:\n";
    // if( this->col_base_ != 0)
    //   {
    // 	(this->col_base_)->print();
    //   }
    // else
    //   {
    // 	if(this->row_base_ != 0)
    // 	  {
    // 	    (this->row_base_)->print();
    // 	  }
    //   }
    //    slip::block<std::size_t,2> p00;
    //p00.fill(0);
    std::cout<<std::endl;
    std::cout<<"Polynomials:"<<std::endl;
    std::cout<<std::endl;
    typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it  = 
      (this->poly_evaluations_)->begin();
    
    for(;it!=(this->poly_evaluations_)->end();++it)
      {
	std::cout<<"P"<<(*it).first<<":"<<std::endl;
	std::cout<<(*it).second<<std::endl;
	std::cout<<std::endl;
      }

    std::cout<<std::endl;
    std::cout<<"Weighted Polynomials:"<<std::endl;
    std::cout<<std::endl;
    typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it_w  = 
      (this->poly_w_evaluations_)->begin();
    
    for(;it_w!=(this->poly_w_evaluations_)->end();++it_w)
      {
	std::cout<<"PW"<<(*it_w).first<<":"<<std::endl;
	std::cout<<(*it_w).second<<std::endl;
	std::cout<<std::endl;
      }
    
    std::cout<<std::endl;
    std::cout<<"(Pk,Pk):"<<std::endl;
    std::cout<<std::endl;
    std::cout<<*(this->pkpk_)<<std::endl;
    
  }
 
  /*!
  ** \brief Computes the inner_product between Pi and Pj.
  ** \param i index of the first polynomial.
  ** \param j index of the second polynomial.
  ** \return the value of the inner product.
  ** \pre (i >  this->size_)
  ** \pre (j >  this->size_)
  */
  T inner_product(const std::size_t i, const std::size_t j)
  {
    assert(i < this->size_);
    assert(j < this->size_);
    
    slip::block<std::size_t, 2> ind_i;
    ind_i[0] = (*(this->powers_))[i][0];
    ind_i[1] = (*(this->powers_))[i][1];
    
    slip::block<std::size_t, 2> ind_j;
    ind_j[0] = (*(this->powers_))[j][0];
    ind_j[1] = (*(this->powers_))[j][1];
 
    typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it  = 
      (this->poly_evaluations_)->find(ind_i);
    typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator it_w  = 
      (this->poly_w_evaluations_)->find(ind_j);
    
    return std::inner_product((it->second).begin(),(it->second).end(),
			      (it_w->second).begin(),T());
    
  }
 
  /*!
  ** \brief Computes the Gram matrix of the polynomial base.
  ** \param Gram The resulting Gram matrix.
  */
  template<typename Matrix>
  void orthogonality_matrix(Matrix& Gram)
  {
    Gram.resize(this->size_,this->size_);
    for(std::size_t i = 0; i < this->size_; ++i)
      {
	for(std::size_t j = 0; j <= i; ++j)
	  {
	    Gram[i][j] =  (this->inner_product)(i,j);
	    Gram[j][i] = Gram[i][j];
	  }
      }
  }
 
  /*!
  ** \brief Indicate if the polynomial basis is orthogonal according to the tolerance \a tol.
  ** \param tol Tolerance of the orthogonality .
  ** \result True if orthogonal, false else.
  */
  bool is_orthogonal(const T tol 
		   = slip::epsilon<T>())
  {
    slip::Matrix<T> Gram;
    this->orthogonality_matrix(Gram);
    return slip::is_diagonal(Gram,tol);
  }

private:
  std::size_t rows_;               ///number of rows of the container2d
  std::size_t cols_;               ///number of cols of the
				   ///container2d
  std::size_t size_;///size of the base : number of polynomials
  int degree_;              ///degree of the base
  int row_degree_; ///degree of the row base
  int col_degree_; ///degree of the col base
  slip::DiscretePolyBase1d<T,PolyWeightRow,PolyCollocationRow>* row_base_;
  slip::DiscretePolyBase1d<T,PolyWeightCol,PolyCollocationCol>* col_base_;
  std::map<slip::block<std::size_t,2>, slip::Array2d<T> >* poly_evaluations_; ///map which associate an %Array2d<T> containning polynomial evaluation at each range site to a block containing 2 polynomial indices
  std::map<slip::block<std::size_t,2>, slip::Array2d<T> >* poly_w_evaluations_;///map which associate an %Array2d<T> containning polynomial evaluation multiplies by the corresponding weight at each range site to a block containing 2 polynomial indices
  slip::Array<T>* pkpk_; ///(Pk,Pk) inner_products
  slip::Array2d<std::size_t>* powers_; /// size_ x 2 matrix containing the spatial polynomial degrees for each polynomial of the base
  bool normalized_; ///boolean indicated if the base is normalized or not
 
  

private:
  
 /*!
  ** \brief Desallocate the polynomial base data.
  */
  void desallocate()
  {
     if(this->row_base_ != 0)
      {
	delete this->row_base_;
      }
    if(this->col_base_ != 0)
      {
	delete this->col_base_;
      }
    if(this->poly_evaluations_ != 0)
      {
	delete this->poly_evaluations_;
      }
    if(this->poly_w_evaluations_ != 0)
      {
	delete this->poly_w_evaluations_;
      }
    if(this->pkpk_ != 0)
      {
	delete this->pkpk_;
      }
    if(this->powers_ != 0)
      {
	delete this->powers_;
      }
  } 
 
  /*!
  ** \brief Computes the polynomial basis.
  */
  void compute_polynomials()
  {
    if(this->col_base_ != 0)
      {
	slip::Array2d<T> P(this->rows_,this->cols_);
	slip::Array2d<T> PW(this->rows_,this->cols_);
		
	for(std::size_t i = 0, k = 0; i <= this->row_degree_; ++i)
	  {
	    for(std::size_t j = 0; (j <= this->col_degree_) && (i+j <= this->degree_); ++j, ++k)
	      {
		(*(this->powers_))[k][0] = i;
		(*(this->powers_))[k][1] = j;
		slip::block<std::size_t,2> indices;
		indices[0] = i;
		indices[1] = j;
		
		for(std::size_t r = 0; r < this->rows_; ++r)
		  {
		    for(std::size_t c = 0; c < this->cols_; ++c)
		      {
			P[r][c] = 
			  (this->row_base_)->get_poly(i,r) * (this->col_base_)->get_poly(j,c);
			PW[r][c] = 
			  (this->row_base_)->get_poly_w(i,r) * (this->col_base_)->get_poly_w(j,c);
			
		      }
		  }
		(*this->poly_evaluations_)[indices] = P;
		(*this->poly_w_evaluations_)[indices] = PW;
		
	      }
	  }
      }
  
    else
      {
	for(std::size_t i = 0, k = 0; i <= this->row_degree_; ++i)
	  {
	    for(std::size_t j = 0; (j <= this->col_degree_) && (i+j <= this->degree_); ++j, ++k)
	      {
		(*(this->powers_))[k][0] = i;
		(*(this->powers_))[k][1] = j;
		slip::block<std::size_t,2> indices;
		indices[0] = i;
		indices[1] = j;
		slip::Array2d<T> P(this->rows_,this->cols_);
		slip::Array2d<T> PW(this->rows_,this->cols_);
		
		for(std::size_t r = 0; r < this->rows_; ++r)
		  {
		    for(std::size_t c = 0; c < this->cols_; ++c)
		      {
			P[r][c] = 
			  (this->row_base_)->get_poly(i,r) * (this->row_base_)->get_poly(j,c);
			PW[r][c] = 
			  (this->row_base_)->get_poly_w(i,r) * (this->row_base_)->get_poly_w(j,c);
		      }
		  }
		(*this->poly_evaluations_)[indices] = P;
		(*this->poly_w_evaluations_)[indices] = PW;
	      }
	  }
      }

    	if(this->normalized_)
	  {
	    std::fill_n((this->pkpk_)->begin(),this->size_,static_cast<T>(1.0));
	  }
	else
	  {
	    typename slip::Array<T>::iterator it_pkpkb = (this->pkpk_)->begin();
	    typename slip::Array<T>::iterator it_pkpke = (this->pkpk_)->end();
	    std::size_t k = 0;
	    for(; it_pkpkb != it_pkpke; ++it_pkpkb, ++k)
	      {
		*it_pkpkb = this->inner_product(k,k);
	      }
	  }
  }

  /*!
  ** \brief Computes the polynomial basis over a mask 
  ** \param mask_upper_left RandomAccessIterator2d to the mask where compute the polynomial basis.
  ** \param mask_bottom_right RandomAccessIterator2d to the mask where compute the polynomial basis
  ** \param true_value value where weight of the polybomial inner_product won't be put to 0 
  */
  template <typename RandomAccessIterator2d>
  void compute_polynomials(RandomAccessIterator2d mask_upper_left,
			   RandomAccessIterator2d mask_bottom_right,
			   typename std::iterator_traits<RandomAccessIterator2d>::value_type true_value =  typename std::iterator_traits<RandomAccessIterator2d>::value_type(1))
  {
     this->compute_polynomials();
    typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator poly_w_first = (*this->poly_w_evaluations_).begin();
     typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator poly_w_last = (*this->poly_w_evaluations_).end();

     for(; poly_w_first != poly_w_last; ++poly_w_first)
       {
	 typename slip::Array2d<T>::iterator2d poly_w_up = (*poly_w_first).second.upper_left();
	 
    
	 RandomAccessIterator2d mask_up = mask_upper_left;
	
	 for(;mask_up != mask_bottom_right; ++mask_up,++poly_w_up)
	   {
	     if(*mask_up != true_value)
	       {
		 *poly_w_up = T();
	       }
	     
	   }
       }

       typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator poly_first = (*this->poly_evaluations_).begin();
     typename std::map<slip::block<std::size_t,2>, slip::Array2d<T> >::iterator poly_last = (*this->poly_evaluations_).end();

     for(; poly_first != poly_last; ++poly_first)
       {
	 typename slip::Array2d<T>::iterator2d poly_up = (*poly_first).second.upper_left();
	 
    
	 RandomAccessIterator2d mask_up = mask_upper_left;
	
	 for(;mask_up != mask_bottom_right; ++mask_up,++poly_up)
	   {
	     if(*mask_up != true_value)
	       {
		 *poly_up = T();
	       }
	     
	   }
       }
  }

 template <typename Iterator>
  void modified_gram_schmidt(const std::vector<std::pair<Iterator,Iterator> >& base)
  {
    slip::Array2d<T> Pk_tmp(this->rows_,this->cols_);
    std::pair<Iterator,Iterator> tmp;
    tmp.first = Pk_tmp.begin();
    tmp.second = Pk_tmp.end();
    
     slip::discrete_poly2d_inner_prod<Iterator,Iterator,T> 
       inner_prod(this->row_base_->weights_begin(),
		  this->row_base_->weights_end(),
		  this->col_base_->weights_begin(),
		  this->col_base_->weights_end());
     std::size_t  init_base_first_size = base.size();
       
     //???
     // std::copy(poly_evaluations_->row_begin(0),poly_evaluations_->row_begin(0),
// 	       poly_evaluations_->row_begin(0));

     for(std::size_t k = 0; k < init_base_first_size; ++k)
       {
	  T PkPk = std::sqrt(inner_prod(base[k],base[k]));
	  slip::divides_scalar(base[k].first,base[k].second,
			       PkPk,
			       tmp.first);
	  for(std::size_t q = (k + 1); q < init_base_first_size; ++q)
	    {
	      T PkPq = inner_prod(base[q],tmp);
	      Iterator it_basek = Pk_tmp.begin();
	      Iterator it_baseq = base[q].first;
	      for(; it_baseq != base[q].second; ++it_baseq, ++it_basek)
		{
		  *it_baseq = *it_baseq - PkPq * *it_basek;
		}
	    }
       }
  }


  //sparse version
  template <typename Iterator, 
	    typename RandomAccessIterator2d>
void modified_gram_schmidt(const std::vector<std::pair<Iterator,Iterator> >& base,
			   RandomAccessIterator2d mask_upper_left,
			   RandomAccessIterator2d mask_bottom_right,
			   typename std::iterator_traits<RandomAccessIterator2d>::value_type true_value =  typename std::iterator_traits<RandomAccessIterator2d>::value_type(1))
  {
    slip::Array2d<T> Pk_tmp(this->rows_,this->cols_);
    std::pair<Iterator,Iterator> tmp;
    tmp.first = Pk_tmp.begin();
    tmp.second = Pk_tmp.end();
    
     slip::discrete_poly2d_inner_prod<Iterator,Iterator,T> 
       inner_prod(this->row_base_->weights_begin(),
		  this->row_base_->weights_end(),
		  this->col_base_->weights_begin(),
		  this->col_base_->weights_end(),
		  mask_upper_left, mask_bottom_right,
		  true_value);
     std::size_t  init_base_first_size = base.size();
       
     //???
     // std::copy(poly_evaluations_->row_begin(0),poly_evaluations_->row_begin(0),
// 	       poly_evaluations_->row_begin(0));

     for(std::size_t k = 0; k < init_base_first_size; ++k)
       {
	  T PkPk = std::sqrt(inner_prod(base[k],base[k]));
	  slip::divides_scalar(base[k].first,base[k].second,
			       PkPk,
			       tmp.first);
	  for(std::size_t q = (k + 1); q < init_base_first_size; ++q)
	    {
	      T PkPq = inner_prod(base[q],tmp);
	      Iterator it_basek = Pk_tmp.begin();
	      Iterator it_baseq = base[q].first;
	      for(; it_baseq != base[q].second; ++it_baseq, ++it_basek)
		{
		  *it_baseq = *it_baseq - PkPq * *it_basek;
		}
	    }
       }
  }

template <typename Iterator>
  void modified_gram_schmidt_normalized(const std::vector<std::pair<Iterator,Iterator> >& base)
  {
   
    
     slip::discrete_poly2d_inner_prod<Iterator,Iterator,T> 
       inner_prod(this->row_base_->weights_begin(),
		  this->row_base_->weights_end(),
		  this->col_base_->weights_begin(),
		  this->col_base_->weights_end());
     std::size_t  init_base_first_size = base.size();
       
     //???
     // std::copy(poly_evaluations_->row_begin(0),poly_evaluations_->row_begin(0),
// 	       poly_evaluations_->row_begin(0));

     for(std::size_t k = 0; k < init_base_first_size; ++k)
       {
	  T PkPk = std::sqrt(inner_prod(base[k],base[k]));
	  slip::divides_scalar(base[k].first,base[k].second,
			       PkPk,
			        base[k].first);
	  for(std::size_t q = (k + 1); q < init_base_first_size; ++q)
	    {
	      T PkPq = inner_prod(base[q],base[k]);
	      Iterator it_basek = base[k].first;
	      Iterator it_baseq = base[q].first;
	      for(; it_baseq != base[q].second; ++it_baseq, ++it_basek)
		{
		  *it_baseq = *it_baseq - PkPq * *it_basek;
		}
	    }
       }
  }

  //sparse version
  template <typename Iterator,
	    typename RandomAccessIterator2d>
  void modified_gram_schmidt_normalized(const std::vector<std::pair<Iterator,Iterator> >& base,
			   RandomAccessIterator2d mask_upper_left,
			   RandomAccessIterator2d mask_bottom_right,
			   typename std::iterator_traits<RandomAccessIterator2d>::value_type true_value =  typename std::iterator_traits<RandomAccessIterator2d>::value_type(1))
  {
   
    
     slip::discrete_poly2d_inner_prod<Iterator,Iterator,T> 
       inner_prod(this->row_base_->weights_begin(),
		  this->row_base_->weights_end(),
		  this->col_base_->weights_begin(),
		  this->col_base_->weights_end(),
		  mask_upper_left, mask_bottom_right,
		  true_value);
     std::size_t  init_base_first_size = base.size();
       
     //???
     // std::copy(poly_evaluations_->row_begin(0),poly_evaluations_->row_begin(0),
// 	       poly_evaluations_->row_begin(0));

     for(std::size_t k = 0; k < init_base_first_size; ++k)
       {
	  T PkPk = std::sqrt(inner_prod(base[k],base[k]));
	  slip::divides_scalar(base[k].first,base[k].second,
			       PkPk,
			        base[k].first);
	  for(std::size_t q = (k + 1); q < init_base_first_size; ++q)
	    {
	      T PkPq = inner_prod(base[q],base[k]);
	      Iterator it_basek = base[k].first;
	      Iterator it_baseq = base[q].first;
	      for(; it_baseq != base[q].second; ++it_baseq, ++it_basek)
		{
		  *it_baseq = *it_baseq - PkPq * *it_basek;
		}
	    }
       }
  }

void update_pkpk()
  {
    for(std::size_t k = 0; k < this->size_; ++k)
     {
       (*(this->pkpk_))[k] = this->inner_product(k,k);
     }
  }

  void update_poly_w_evaluations()
  {
    for(std::size_t n = 0; n < this->size_; ++n)
      {
	slip::block<std::size_t, 2> ind_n;
	ind_n[0] = (*(this->powers_))[n][0];
	ind_n[1] = (*(this->powers_))[n][1];
	for(std::size_t i = 0; i < this->rows_; ++i)
	  {
	    slip::multiplies(((*poly_evaluations_)[ind_n]).row_begin(i),
			     ((*poly_evaluations_)[ind_n]).row_end(i),
			     this->row_base_->weights_begin(),
			     ((*poly_w_evaluations_)[ind_n]).row_begin(i));
	  }
	for(std::size_t j = 0; j < this->cols_; ++j)
	  {
	    slip::multiplies(((*poly_w_evaluations_)[ind_n]).col_begin(j),
			     ((*poly_w_evaluations_)[ind_n]).col_end(j),
			     this->col_base_->weights_begin(),
			     ((*poly_w_evaluations_)[ind_n]).col_begin(j));
	  }
	
      }
  }

  //sparse version
   template <typename RandomAccessIterator2d>
   void update_poly_w_evaluations(RandomAccessIterator2d mask_upper_left,
  				  RandomAccessIterator2d mask_bottom_right,
  				  typename std::iterator_traits<RandomAccessIterator2d>::value_type true_value =  typename std::iterator_traits<RandomAccessIterator2d>::value_type(1))
  {
 typedef typename std::iterator_traits<RandomAccessIterator2d>::value_type mask_value_type;
    const std::size_t rows = static_cast<std::size_t>((mask_bottom_right-mask_upper_left)[0]);
    const std::size_t cols = static_cast<std::size_t>((mask_bottom_right-mask_upper_left)[1]);
    
    slip::Array2d<T> w_mask(rows,cols);
     slip::outer_product(this->row_base_->weights_begin(),  
			 this->row_base_->weights_end(),
			 this->col_base_->weights_begin(),
			 this->col_base_->weights_end(),
			 w_mask.upper_left(),w_mask.bottom_right());
     std::replace_if(w_mask.begin(),w_mask.end(), 
		     std::bind2nd(std::not_equal_to<mask_value_type>(),true_value),
		     T());
		     
     
    for(std::size_t n = 0; n < this->size_; ++n)
      {
  	slip::block<std::size_t, 2> ind_n;
  	ind_n[0] = (*(this->powers_))[n][0];
  	ind_n[1] = (*(this->powers_))[n][1];
    	slip::multiplies(((*poly_evaluations_)[ind_n]).begin(),
  			 ((*poly_evaluations_)[ind_n]).end(),
  			 w_mask.begin(),
  			 ((*poly_w_evaluations_)[ind_n]).begin());
	 
	
      }
  }
};

typedef slip::DiscretePolyBase2d<double,
                               slip::LegendreWeight<double>,
                               slip::UniformCollocations<double>,
                               slip::LegendreWeight<double>,
				     slip::UniformCollocations<double> > DLegendreBase2d_d;

typedef slip::DiscretePolyBase2d<float,
                               slip::LegendreWeight<float>,
                               slip::UniformCollocations<float>,
                               slip::LegendreWeight<float>,
				     slip::UniformCollocations<float> > DLegendreBase2d_f;

}//::slip


#endif //SLIP_DISCRETEPOLYBASE2D_HPP
